<?PHP

/**
 * antiSpamCheck
 *
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @param	array Array of fields to check for illegal content
 * @param	boolean Whether to die in case illegal content is found
 * @param	array Array with attributes to check on (newlines=true,headers=true,html=false,email=false)
 * @return	array Returns an array with error messages. In case dieOnFound is true it will trigger an error with
 *				E_USER_ERROR and it exits.
 */

function antiSpamCheck($fields, $dieOnFound = true, $checks = array('newlines'=>true,'headers'=>true,'html'=>false,'email'=>false)) {

	// Remove injected headers
	$not_allowed = array('bcc','Content-Type','cc','to');

	$retval = array();
	$emailCheck = 0;

	foreach ($fields as $k => $v) {
		// anti newline
		if(isset($checks['newlines']) && $checks['newlines']==true){
			if (preg_match("/[\r|\n]/m", $fields[$k]))
				 $retval[] = 'Error: Newlines are not allowed in fields that are used in header information';
		}

		// anti injection
		if(isset($checks['headers']) && $checks['headers']==true){
			 if (0 != preg_match_all('/('.implode('|',$not_allowed).'):/im', $fields[$k], $matches)){
				  $retval[] = 'Error: These ('.join(',', $matches[1]).') headers are not allowed';
			 }
		}

		// anti html injection
		if(isset($checks['html']) && $checks['html']==true){
			if( strip_tags($fields[$k]) != $fields[$k]){
				$retval[] = 'Error: HTML is not allowed';
			}
		}

		// anti html injection
		if(isset($checks['email']) && $checks['email']==true){
			if(preg_match('/([A-Za-z0-9](([\w.-][^._-]{2,}){0,61})[A-Za-z0-9])@([A-Za-z0-9]([A-Za-z0-9-]{0,61})?[A-Za-z0-9]\.)+([A-Za-z]{2,6})/i',$v)){
				$emailCheck++;
			}
		}
	}
	// end anti header injection

if(isset($checks['email']) && $checks['email']==true && count($fields)>5 && $emailCheck>0 && ($emailCheck/count($fields))*100 >= 50 ){
	$retval[]='Too many fields filled with email addresses';
}

if(count($retval)>0 && $dieOnFound === true){
	trigger_error(implode("\n",$retval), E_USER_ERROR);
	exit;
}

return $retval;
}

?>
