<?PHP
/**
 * Web Power Database wrapper
 *
 * This is a small and simple database wrapper used by Web Power. Used to do some basic database handling
 * and error-handling to shorten the code used in de actual php script returning a fixed result as array or string.
 *
 * @package	WebPower
 * @version	0.1
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @copyright	Web Power 2005
 * @link	http://www.webpower.nl
 * @access	public
 */
class wpDb {
	/**
	 * Database resource
	 * @var resource
	 */
	var $db				=	NULL;

	/**
	 * Number of affected rows
	 * @var integer
	 */
	var $affectedRows	=	NULL;
	/**
	 * Number of rows
	 * @var integer
	 */
	var $numRows		=	NULL;
	/**
	 * Last inserted id
	 * @var integer
	 */
	var $lastId			=	NULL;
	/**
	 * Last DB result resource
	 * @var resource
	 */
	var $res				=	NULL;
	/**
	 * Number of found rows found with MYSQL_CALC_FOUND_ROWS within select query
	 * @var integer
	 */
	var $foundRows		=	false;
	/**
	 * Last query executed
	 * @var integer
	 */
	var $lastSql		=	'';
	/**
	 * List of query's with there execution time
	 * @var array
	 */
	var $queryList		= array();
	/**
	 * Count the number of query's fired
	 * @var array
	 */
	var $queryCount	=	array('u'=>0,'i'=>0);

	/**
	 * Debug on or off
	 * @var boolean
	 */
	var $debug			= false;	

	/**
	 * Web Power Database class
	 *
	 * Creates a wpDb object
	 */
	


	function wpDb(){

	}

	function mailDebug($email){
		if($this->debug){
			$msg = "Number of query's: ".$this->getQueryCount(true);
			$msg.= "\n\n";
			foreach($this->queryList as $query){
				$msg.=trim(preg_replace("/\s+/",' ',$query['sql']))." - time: {$query['time']}\n";
			}
			mail($email,'debug wbDb on '.(!empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'commandline'),$msg);				
		}
		
	}

	/**
	 * steal a link 
	 *
	 * Steal a link resource from an other DB object
	 *
	 * @access	public
	 * @param	resource DB link resource
	 * @return	boolean	Returns true on succes
	 */
	function stealLink(&$link){
		if(is_resource($link)){
			$this->db = &$link;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Connect
	 *
	 * Makes a db connection
	 *
	 * @access	public
	 * @param	string Username
	 * @param	string Password
	 * @param	string Database name
	 * @param	string Host/Server
	 * @param	boolean use persistent connection (default:false)
	 */
	function connect($user,$password,$db,$host='localhost',$persistent=false){
		if(!$persistent){
			$this->db = mysql_connect($host,$user,$password) or trigger_error('Could not connect to server', E_USER_ERROR);
		} else {
			$this->db = mysql_pconnect($host,$user,$password) or trigger_error('Could not connect to server', E_USER_ERROR);
		}
		if(!$this->db){
			die('Could not connect to server '.mysql_error());
		} else {
			if(!mysql_select_db($db,$this->db)){
				trigger_error('Could not select database');
				die('Could not connect select database');
			}
		}		
	}

	/**
	 * getArray is used to return an array of fetched rows from a result
	 *
	 * @access	public
	 * @param	string SQL select query
	 * @return	array Returns a multidimensional associative array of fetched rows
	 */	
	function getArray($sql){
		$this->res = $this->query($sql,$this->db) or trigger_error( mysql_error(), E_USER_ERROR);
		if(!$this->res){
			return array();
		} else {
			$retval = array();
			$this->numRows = mysql_num_rows($this->res);
			while($record = mysql_fetch_array($this->res,MYSQL_ASSOC)){
				$retval[]=$record;
			}
			unset($record);
			return $retval;
		}
	}

	/**
	 * getList is used to return an array of 1 column and multiple rows from a result
	 *
	 * @access	public
	 * @param	string SQL select query
	 * @return	array Returns a nummeric array of fetched rows (1, first column)
	 */	
	function getList($sql){
		$this->res = $this->query($sql,$this->db) or trigger_error( mysql_error(), E_USER_ERROR);
		if(!$this->res){
			return array();
		} else {
			$retval = array();
			$this->numRows = mysql_num_rows($this->res);
			while($record = mysql_fetch_array($this->res,MYSQL_NUM)){
				$retval[]=$record[0];
			}
			unset($record);
			return $retval;
		}
	}
	/**
	 * getRow is used to return an array of fetched columns from a result
	 *
	 * @access	public
	 * @param	string SQL select query
	 * @return	array Returns an associative array of fetched columns
	 */	
	function getRow($sql){
		$this->res = $this->query($sql,$this->db) or trigger_error( mysql_error(), E_USER_ERROR);
		if(!$this->res){
			trigger_error('getRow query didn\'t return a result, wrong type of query? query:'.$sql,E_USER_WARNING);
			
		} else {
			$this->numRows = mysql_num_rows($this->res);
			if($this->numRows>=1){
				if($this->numRows>1){
					trigger_error('getRow query returned more then one row, this is not suppose to happen. Query: '.$sql,E_USER_WARNING);
				}
				return mysql_fetch_array($this->res,MYSQL_ASSOC);
			} else {
				return array();	
			}		
		}
	}

	/**
	 * getOne is used to fetch only the first column on the first row of a result, like result by count()
	 *
	 * @access	public
	 * @param	string SQL select query
	 * @return	string Result value
	 */
	function getOne($sql){
		$this->res = $this->query($sql,$this->db) or trigger_error( mysql_error(), E_USER_ERROR);
		if(!$this->res){
			return false;
		} else {
			$this->numRows = mysql_num_rows($this->res);
			if($this->numRows>=1){
				if($this->numRows>1){
					trigger_error('getOne query returned more then one row, this is not suppose to happen',E_USER_WARNING);
				}
				return mysql_result($this->res,0);
			} else {
				trigger_error('getOne query didn\'t return a result, wrong type of query?',E_USER_WARNING);
				return false;
			}
		}
	}

	/**
	 * getLastInsertId returns the last insert id from a insert query on a auto_incremental table
	 *
	 * @access	public
	 * @return	integer insert id
	 */
	function getLastInsertId(){
		return $this->lastId;
	}

	/**
	 * getAffectedRows returns the number of affected rows, affected by the last INSERT/UPDATE or DELETE query
	 *
	 * @access	public
	 * @return	integer number of affected rows
	 */
	function getAffectedRows(){
		return $this->affectedRows;
	}

	/**
	 * getNumRows returns the number rows, that are in the last resultset of a select query
	 *
	 * @access	public
	 * @return	integer number of rows
	 */
	function getNumRows(){
		return $this->numRows;
	}

	/**
	 * getFoundRows returns the total number rows, that are in the last resultset of a select query
	 * LIMIT within the query does not affect this number, but you have to use SQL_CALC_FOUND_ROWS within your
	 * SELECT query otherwise this method will return false.
	 *
	 * @access	public
	 * @return	mixed number of rows in case it can determine the total number, false otherwise
	 */
	function getFoundRows(){
		if(eregi('SQL_CALC_FOUND_ROWS',$this->lastSql)){
			$res = $this->query('SELECT FOUND_ROWS()',false);
			return mysql_result($res,0);
		}
		return false;
	}

	/**
	 *	Execute an insert query. 
	 * Use getLastInsertId to get the id created on an auto incremental table
	 *
	 * @access	public
	 * @param	string SQL insert query
	 * @return	boolean Returns true when there are no errors.
	 */
	function insert($sql){
		$this->res = $this->query($sql,$this->db) or trigger_error( mysql_error(), E_USER_ERROR);
		$this->lastId = mysql_insert_id($this->db);
		$this->affectedRows = mysql_affected_rows($this->db);
		if($this->res){
			return true;
		} else {
			return false;
		}
	}

	/**
	 *	Execute an update query. 
	 * Use getAffectedRows to get the number of affected rows by this update
	 *
	 * @access	public
	 * @param	string SQL update query
	 * @return	boolean Returns true when there are no errors.
	 */
	function update($sql){
		$this->res = $this->query($sql,$this->db) or trigger_error( mysql_error(), E_USER_ERROR);
		$this->affectedRows = mysql_affected_rows($this->db);
		if($this->res){
			return true;
		} else {
			return false;
		}
	}

	/**
	 *	Execute a delete query. 
	 * Use getAffectedRows to get the number of affected rows by this update
	 *
	 * @access	public
	 * @param	string SQL delete query
	 * @return	boolean Returns true when there are no errors.
	 */
	function delete($sql){
		return $this->insert($sql);
	}

	/**
	 *	Execute a delete query. 
	 * Use getAffectedRows to get the number of affected rows by this update
	 *
	 * @access	private
	 * @param	string SQL query
	 * @return	resource Returns the DB resource
	 */
	function query($sql,$log=true){
		if($log){
			$this->lastSql = $sql;
			$this->queryCount['u']++;
		} else {
			$this->queryCount['i']++;
		}
		if($this->debug){
			$this->queryList[] = array('sql'=>$sql,'time'=>-1);
		}
		if($this->debug) $startTime = microtime();
		$res = mysql_query($sql,$this->db) or trigger_error( mysql_error().' query:'.$sql, E_USER_ERROR);
		if($this->debug) {
			$endTime = microtime();
			$this->queryList[count($this->queryList)-1]['time']=$endTime - $startTime;
			$endTime = $startTime = NULL;
		}
		return $res;
	}

	/**
	 * getQueryCount, Gets the number of query's triggered
	 *
	 * @access	public
	 * @param	boolean Count internal query's used by the class itself
	 * @return	integer number of query's fired
	 */
	function getQueryCount($showInternal=false){
		if($showInternal){
			return $this->queryCount['i']+$this->queryCount['u'];
		} else {
			return $this->queryCount['u'];
		}
	}

	function quote($str){
		return mysql_escape_string($str);
	}

}
?>
