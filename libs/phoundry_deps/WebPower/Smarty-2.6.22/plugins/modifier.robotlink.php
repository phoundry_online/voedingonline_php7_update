<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty robotlink modifier plugin
 *
 * Type:     modifier<br>
 * Name:     round<br>
 * Purpose:  get a string as fake html link<br>
 * Input:<br>
 *         - string: input integer
 * @param string
 * @return string
 */
function smarty_modifier_robotlink($str)
{
	//$str = strtolower(substr($str,0,40));
	$str = preg_replace('/[^a-z��������������������0-9_\-]/i', '_', $str);
	$str = strtr($str, '��������������������', 'aaaaeeeeiiiioooouuuu');
	$str = preg_replace('/_{2,}/', '_', $str);
	$str = preg_replace(array('/^_+/', '/_+$/'), array('',''), $str);
	return $str . '.html';
}

/* vim: set expandtab: */

?>
