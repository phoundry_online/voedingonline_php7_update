<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {pager} function plugin
 *
 * Type:     function<br>
 * Name:     pager<br>
 * Date:     March 25, 2005
 * Purpose:  Generic page scroller 
 *
 * Input:<br>
 *         - limit = Results per page
 *         - url   = url
 *         - cur_page = current page number
 *         - page_attr = page parameter to replace (default page)
 *         - rowcount = rows in the resultset
 *         - active_class = class set incase of active page 
 *
 * Examples:
 * <pre>
 * {pager limit="10" current_page="1" rowcount="50"}
 * </pre>
 * @version  1.2
 * @author   J&oslash;rgen Teunis
 * @param    array
 * @param    Smarty
 * @return   string
 */
function smarty_function_pager($params, &$smarty)
{
    $extra = '';
    if (!isset($params['limit'])) {
        $smarty->trigger_error("pager: missing 'limit' parameter");
        return;
    } else {
        $limit = $params['limit'];
    }
	 if($limit==0) return '';
    if (empty($params['cur_page'])) {
        $smarty->trigger_error("pager: missing 'cur_page' parameter");
        return;
    } else {
        $cur_page = $params['cur_page'];
    }
    if (empty($params['rowcount'])) {
        $smarty->trigger_error("pager: missing 'rowcount' parameter");
        return;
    } else {
        $rowcount = $params['rowcount'];
    }
    if (empty($params['url'])) {
        $smarty->trigger_error("pager: missing 'url' parameter");
        return;
    } else {
        $url = $params['url'];
    }
    if (empty($params['page_attr'])) {
		$page_attr = 'page';
	 } else {
		$page_attr = $params['page_attr'];
	 }
    if (!empty($params['active_class'])) {
		$active_class = ' class="'.$params['active_class'].'"';
	 } else {
		$active_class = '';
	 }
    if (!empty($params['delimiter'])) {
		$delimiter = $params['delimiter'];
	 } else {
		$delimiter = '';
	 }
    if (!empty($params['max'])) {
		$max = $params['max'];
	 } else {
		$max = 0;
	 }
	$html = $links = $previous = $next = '';
	$pages = ceil($rowcount / $limit);

	$start_i = $cur_page - ceil($max/2);
	$end_i = $cur_page + ceil($max/2);

	if($end_i<$max)
		$end_i=$max;
	if($end_i>$pages)
		$end_i=$pages;
	if($start_i+$max>$pages)
		$start_i=$pages-$max;
	if($max == 0){
		$max = $pages;
		$start_i=1;
		$end_i=$pages;
	}

	if(preg_match("/[\?|\&]$page_attr=(\d+)/", $url)){
	  $url = preg_replace("/(\?|\&)$page_attr=\d+/i", "\\1{$page_attr}=%page%", $url);
	} else {
		$delim = (ereg('\?',$url)) ? '&' : '?';
		$url .=$delim."$page_attr=%page%";
	}
	$url = str_replace('&', '&amp;', $url);
	for($i = 1; $i <= $pages; $i++){
		if($i>=$start_i && $i<=$end_i){
			$class = ($i == $params['cur_page']) ? $active_class : '';
			$title = ($i == $params['cur_page']) ? ' title="" style="cursor:help"' : ' title=""';
			if($i == $cur_page){
				$links .= '<b '.$class.'>' . $i . '</b> ';
			} else {
				$links .= '<a href="' . str_replace('%page%', $i, $url). '"' . $class . '"' . $title . '>' . $i . '</a> ';
			}
			if($i < $end_i){
				$links .= $delimiter;
			}
		}
	}

	$previous = ($params['cur_page'] - 1 > 0) ? '<a href="' . str_replace('%page%', ($params['cur_page'] - 1), $url). '" title="">&laquo;</a> ' : '';
	$next = ($params['cur_page'] + 1 <= $pages) ? '<a href="' . str_replace('%page%', ($params['cur_page'] + 1), $url). '" title="">&raquo;</a> ' : '';

return $previous . $links . $next;

}

/* vim: set expandtab: */

?>
