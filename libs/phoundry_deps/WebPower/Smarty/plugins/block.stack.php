<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     block.stack.php
 * Type:     block
 * Name:     stack
 * Purpose:  stack a block of text for use with postfilter
 * -------------------------------------------------------------
 */
function smarty_block_stack($params, $content, &$smarty, &$repeat)
{
	 $ident = isset($params['name']) ? $params['name'] : 'unknown';
	 if (isset($content)) {
	   if(!isset($GLOBALS['stack'])){
	      $GLOBALS['stack']=array();
	   }
	   if(!isset($GLOBALS['stack'][$ident])){
  	   	 $GLOBALS['stack'][$ident]=array();
	   }
		if(isset($params['assign'])){
	   	$GLOBALS['stack'][$ident][$params['assign']]=$content;
		} else {
	   	$GLOBALS['stack'][$ident][]=$content;
		}
    }
    return '<!-- replace stack: '.$ident.' -->';
}


?> 
