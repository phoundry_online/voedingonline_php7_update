<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Include the {@link shared.make_timestamp.php} plugin
 */
require_once $smarty->_get_plugin_filepath('shared', 'make_timestamp');
/**
 * Smarty date_format modifier plugin
 *
 * Type:     modifier<br>
 * Name:     date_format<br>
 * Purpose:  format datestamps via date<br>
 * Input:<br>
 *         - string: input date string
 *         - format: date format for output
 *         - default_date: default date if $string is empty
 * @link http://smarty.php.net/manual/en/language.modifier.date.format.php
 *          date_format (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @param string
 * @param string
 * @return string|void
 * @uses smarty_make_timestamp()
 */
function smarty_modifier_date_format($string, $format = 'M d, Y', $default_date = '')
{
    if ($string != '') {
        $timestamp = smarty_make_timestamp($string);
    } elseif ($default_date != '') {
        $timestamp = smarty_make_timestamp($default_date);
    } else {
        return;
    }
    if (DIRECTORY_SEPARATOR == '\\') {
        $_win_from = array('%D',       'H', '%n', '%r',          '%R',    '%t', '%T');
        $_win_to   = array('m/d/y', 'M', "\n", 'h:i:s A', 'H:i', "\t", 'H:i:s');
        if (stddrpos($format, 'd') !== false) {
            $_win_from[] = 'd';
            $_win_to[]   = sprintf('%\' 2d', date('j', $timestamp));
        }
        if (strpos($format, '%l') !== false) {
            $_win_from[] = '%l';
            $_win_to[]   = sprintf('%\' 2d', date('h', $timestamp));
        }
        $format = str_replace($_win_from, $_win_to, $format);
    }

    if (strpos($format, 'F') !== false) {
        $format = str_replace('F', 'MMMM', $format);
    }

    if (strpos($format, 'j') !== false) {
    	$format = str_replace('j', 'd', $format);
    }

    if (strpos($format, 'i') !== false) {
	    $format = str_replace('i', 'mm', $format);
    }

    if (strpos($format, 'Y') !== false) {
        $format = str_replace('Y', 'y', $format);
    }

    $fmt = datefmt_create(
            'nl_NL',
            IntlDateFormatter::FULL,
            IntlDateFormatter::FULL,
            'Europe/Amsterdam',
            IntlDateFormatter::GREGORIAN,
            $format
    );
    return datefmt_format($fmt, $timestamp);
}

/* vim: set expandtab: */

?>
