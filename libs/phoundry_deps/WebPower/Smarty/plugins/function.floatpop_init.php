<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {popup_init} function plugin
 *
 * Type:     function<br>
 * Name:     floatpop_init<br>
 * Purpose:  initialize floating popup
 *
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_floatpop_init($params, &$smarty)
{
	$zindex	= 1000;
	$name		= 'floatPop';

    if (!empty($params['zindex'])) {
        $zindex = $params['zindex'];
    }
    if (!empty($params['name'])) {
        $name = $params['name'];
    }
    
    if (!empty($params['js'])) {
	 		$retval='';
			if(!empty($params['css'])){
				$retval.='<link rel="stylesheet" href="'.$params['css'].'" type="text/css" />';
			}
         $retval.='<script type="text/javascript" src="'.$params['js'].'"></script>' . "\n";
			return $retval;
    } else {
        $smarty->trigger_error("floatpop_init: missing src parameter");
    }
}

/* vim: set expandtab: */

?>
