<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty xmlamp modifier plugin
 *
 * Type:     modifier<br>
 * Name:     xmlamp<br>
 * Purpose:  Convert & to &amp; 
 * 
 * @param string
 * @return string
 */

function smarty_modifier_xmlamp($string)
{
	$str = preg_replace('/& /',"&amp; ",$str);
	return preg_replace('/(&[^;]*? )/e',"str_replace('&','&amp;','\\1')",$str);
}

?>
