<?

/*
 * For info about tags available for use in FORM's:
 * http://www.htdig.org/hts_form.html
 *
 * For info about search templates:
 * http://www.htdig.org/hts_templates.html
 *
 * For info about configuration options:
 * http://www.htdig.org/attrs.html
 *
 * In order to skip indexing some parts of your documents,
 * use <!--htdig_noindex--> and <!--/htdig_noindex-->.
 *
 */

define('HTSEARCH', '/usr/local/lib/php/WebPower/htsearch');

class HTdigResult {
	var $index, $url, $title, $modified, $size, $score, $percent, $excerpt;

	function HTdigResult($index, $result) {
		$this->index = $index;
		$tags = array('url', 'title', 'modified', 'size', 'score', 'percent', 'excerpt');
		foreach ($tags as $tag) {
			$tagU = strtoupper($tag);
			if (preg_match("'<$tagU>(.*)</$tagU>'s", $result, $reg))
				$this->$tag = $reg[1];
		}
	}
}

class HTdig {
	var $first_match, $last_match, $args, $config, $method, $sort, $matchesperpage, $restrict, $exclude;
	var $matches, $nr_pages, $cur_page, $words , $syntaxerror, $results;

	function HTdig($config = '', $method='and', $sort='score', $matchesperpage=10, $restrict='', $exclude='') {
		$this->syntaxerror = false;
		$this->args = $this->results = array();
		$this->first_match = $this->last_match = 0;
			
		$this->config = (isset($_REQUEST['config'])) ? $_REQUEST['config'] : $config;
		$this->method = (isset($_REQUEST['method'])) ? $_REQUEST['method'] : $method;
		$this->sort   = (isset($_REQUEST['sort'])) ? $_REQUEST['sort'] : $sort;
		$this->matchesperpage = (isset($_REQUEST['matchesperpage'])) ? $_REQUEST['matchesperpage'] : $matchesperpage;
		$this->restrict = (isset($_REQUEST['restrict'])) ? $_REQUEST['restrict'] : $restrict;
		$this->exclude = (isset($_REQUEST['exclude'])) ? $_REQUEST['exclude'] : $exclude;
			
		$this->args['config'] = $this->config;
		$this->args['method'] = $this->method;
		$this->args['sort'] = $this->sort;
		$this->args['matchesperpage'] = $this->matchesperpage;
		$this->args['restrict'] = $this->restrict;
		$this->args['exclude'] = $this->exclude;
		$this->args['words'] = '';
		$this->args['page'] = 1;
	}

	function init($header) {
		$tags = array('matches', 'nr_pages', 'cur_page', 'words', 'syntaxerror');
		foreach ($tags as $tag) {
			$tagU = strtoupper($tag);
			if (preg_match("'<$tagU>(.*)</$tagU>'s", $header, $reg))
				$this->$tag = $reg[1];
		}
	}

	function setResults($results) {
		if (preg_match_all("'<RESULT NR=\"([0-9]+)\">(.*?)</RESULT>'s", $results, $reg)) {
			for ($x = 0; $x < count($reg[1]); $x++) {
				if ($x == 0)
					$this->first_match = $reg[1][$x];
				$this->results[] = new HTdigResult($reg[1][$x],$reg[2][$x]);
				if ($x == count($reg[1])-1)
					$this->last_match = $reg[1][$x];
			}
		}
	}

	function getQS($page = NULL) {
		$this->args['page'] = ($page === NULL) ? $_REQUEST['page'] : $page;
		$args = '';	$x = 0;
		foreach($this->args as $name=>$value) {
			if ($name == 'config' || empty($value))
				continue;
			if ($x++ != 0)	$args .= '&amp;';
			$args .= $name . '=' . urlencode(stripslashes($value));
		}
		return $args;
	}

	function doDig($words) {
		$this->words = $words;
		$this->args['page'] = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
		$this->args['words'] = (isset($_REQUEST['words'])) ? $_REQUEST['words'] : $this->words;

		$args = '';	$x = 0;
		foreach($this->args as $name=>$value) {
			if ($x++ != 0)	$args .= ';';
			$args .= $name . '=' . $value;
		}
			
		//$command = HTSEARCH." \"format=php;$args\"";
		$command = HTSEARCH." -c ".CONFIG_DIR."/htdig.{$this->config}.conf \"format=php;$args\"";
		$cnt = `$command`;

		$cnt = preg_replace("'Content-type: text/html\r\n\r\n'", '', $cnt);
		if (strstr($cnt, '<NOMATCH>'))
			$this->matches = 0;
		elseif (strstr($cnt, '<SYNTAXERROR>')) {
			$this->matches = 0;
			$this->syntaxerror = true;
		}
		else {
			if (preg_match("'<HEADER>(.*)</HEADER>'s", $cnt, $reg))
				$this->init($reg[1]);
			if (preg_match("'<RESULTSET>(.*)</RESULTSET>'s", $cnt, $reg))
				$this->setResults($reg[1]);
		}
	}
}
?>
