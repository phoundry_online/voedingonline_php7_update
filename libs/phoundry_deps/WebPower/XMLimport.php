<?PHP
/**
 * PreCheck if data seems like xml.
 *
 * Try's to find out if it's parsable in the firstplace, before
 * feeding the data to the xml parser
 *
 * @author Web Power B.V. | Jørgen Teunis
 * @param string $str possible xml string
 * @return boolean true if it seems like xml, false otherwise
 *
 */
function seems_xml($str){
   $str = trim($str);
   if(preg_match('/<\?xml.*?version.*?\?>/i',$str)){
      return true;
   }
	if(preg_match('/^<.*?>.*<\/.*?>$/is',$str)){
		return true;
	}
   return false;
}

/**
 * XML entiteit.
 *
 * Contains xml entiteit with data and information about how to store it
 *
 * @author Web Power B.V. | Jørgen Teunis
 * @param string $name Name of the entity
 * @param string $parent Id of the parent node
 *
 */

class xml_entiteit {
var $id,$name,$action=NULL,$config,$data,$query_executed=false,$query_succesfull=false,$sql_errno=0,$sql_error='',$dbId=NULL;

	function xml_entiteit($name,$parent,&$stack){
		$this->id = uniqid($name.'_');
		$this->name = $name;
		$this->parent = $parent;
		$this->stack = &$stack;
		return $this->id;
	}

	function set_data($data){
		$this->data = $data;
	}

	function set_config($config){
		$this->config = $config;
	}

	function set_attributes($attributes){
		$this->attributes = $attributes;
		if(isset($attributes['action'])){
			$this->setAction($attributes['action']);
		}
	}

	function setAction($action){
		$this->action = strtolower($action);
	}

	function getDbId(){
		return $this->dbId;
	}

	function getFirstDbParent(){
		while($this->parent){
			if(isset($this->stack[$this->parent])){
				if(!empty($this->stack[$this->parent]->config['destination_table'])){
					return $this->parent;
				} else {
					$this->parent = $this->stack[$this->parent]->parent;
				}
			} else {
				return false;
			}
		}
		return false;
	}

	function hasData(){
		if(count($this->data)>0){
			return true;
		}
		return false;
	}

	function getSql($override_action=''){
		if(!empty($override_action)){
			$this->action=$override_action;
		}
		if(empty($this->action)){
			print 'empty action ';
			// als er geen action is voor deze node, probeer de action van de parentnode over te nemen
			if(isset($this->stack[$this->parent]->action)){
				print 'parent action '.$this->stack[$this->parent]->action;
				$this->action = $this->stack[$this->parent]->action;
			} else {
				print 'default insert action';
				// zo niet, dan default insert
				$this->action = 'insert';
			}
		}
		$sql = '';
		$fields	= array_keys($this->config["attribute_matching"]);
		$columns	= array_values($this->config["attribute_matching"]);
		print "-----------------------\n";

		switch($this->action){
			case 'insert':
				$sql.= 'INSERT INTO '.$this->config['destination_table'].' ('.implode(',',$columns).') ';
				$sql.= 'VALUES (';
				foreach($fields as $index=>$field){
					if($field{0}!='$' && isset($this->data[$field])){
						$fields[$index]="'".mysql_escape_string($this->data[$field])."'";
					} elseif(ereg('$',$this->config['attribute_matching'][$field])){
						if($this->parent){
							if($field=='$parent_id'){
								$fields[$index]="'".mysql_escape_string($this->stack[$this->parent]->getDbId())."'";
							}
						}						
					}
				}
				$sql.= implode(',',$fields);
				$sql.= ')';
			break;
			case 'update':
				$fields = $where = array();

				foreach($this->config["attribute_matching"] as $xml_field => $database_column){
					$fields[]="`{$database_column}`= '".mysql_escape_string($this->data[$xml_field])."'";
				}
				$sql = "UPDATE {$this->config['destination_table']} SET ".implode(',',$fields);
				if(isset($this->config["primarykeys"]) 
						&& is_array($this->config["primarykeys"]) 
							&& count($this->config["primarykeys"]>0))
				{
					foreach($this->config["primarykeys"] as $database_column => $xml_field ){
						$where[]="`{$database_column}`= '".mysql_escape_string($this->data[$xml_field])."'";
					}
					$sql.= " WHERE ".implode(' AND ',$where);
				}
			break;
			case 'delete':
				$sql = "DELETE FROM {$this->config['destination_table']} ";
				if(isset($this->config["primarykeys"]) 
						&& is_array($this->config["primarykeys"]) 
							&& count($this->config["primarykeys"]>0))
				{
					foreach($this->config["primarykeys"] as $database_column => $xml_field ){
						$where[]="`{$database_column}`= '".mysql_escape_string($this->data[$xml_field])."'";
					}
					$sql.= " WHERE ".implode(' AND ',$where);
				}
			break;
		}
		return $sql;
	}

	function storeToDb($auto_update=false){
		$parentdb = $this->getFirstDbParent();
		
		if(!empty($this->parent) && $parentdb ){
			if(!$this->stack[$parentdb]->query_executed || !$this->stack[$parentdb]->query_succesfull){
				print 'Warning, this object has a parent which is not inserted or updated correctly, error: '.$this->stack[$parentdb]->sql_error.' - '.$this->stack[$parentdb]->sql_errno."\n";
				return false;
			}
		}
		$res = mysql_query($this->getSql());

		if($auto_update && mysql_errno()==1062){
			$res = mysql_query($this->getSql('update'));
		}
		$this->sql_errno = mysql_errno();
		$this->sql_error = mysql_error();
		$this->query_executed=true;
		
		if($this->sql_errno==0){
			$this->query_succesfull=true;
			if(mysql_insert_id()){
				$this->dbId=mysql_insert_id();
			}
			$this->stack[$this->id]=$this;
			return true;
		} else {
			$this->stack[$this->id]=$this;
			return false;
		}
	}

}


class XMLImport {
var $xml = '',$stack=array(), $config=array(),$xmlObj,$configSettings='',$config=array();

	function XMLImport($xml){
		$xmlObj = domxml_open_mem($xml); // xml openen
		$this->xmlObj = $this->domxml_xmlarray($xmlObj); // xml naar een array parsen		

		if(!$this->getConfig()){
			$this = NULL;
		} else {
			print 'using config for '.$this->configSettings."\n";
		}
	}

	function ready(){
		if(!empty($this->configSettings) && !empty($this->xmlObj)){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get configuration for xml storage to database.
	 *
	 * A long description
	 *
	 * @author Web Power B.V. | Jørgen Teunis
	 * @return array Array with config parameters
	 *
	 */

	function getConfig(){
		if(!isset($this->xmlObj[0]['tagname'])){
			return false;
		} else {
			$this->configSettings = $this->xmlObj[0]['tagname'];
		}
		$sql ='
			SELECT 
					xc.* 
			FROM 
					xml_config_file xcf 
			INNER JOIN 
					xml_config xc  ON xcf.id=xc.file_id ';
		$sql.='WHERE xcf.xml_roottag="'.mysql_escape_string($this->configSettings).'" ORDER BY parent_id';
		$res = mysql_query($sql);

		$parents = array();
		$tmp = array();

		if($res){
			while($record = mysql_fetch_array($res,MYSQL_ASSOC)){
				$tmp[$record['id']]=$record;
			}
			foreach($tmp as $record){
				if(is_numeric($record['parent_id'])){
					$path = $this->getParents($record,$tmp);
					$parents[$path]=$record;
				} else {
					$parents[$record['tag']]=$record;
				}
			}
		}
		unset($tmp);

		foreach($parents as $index => $record){
				$matching = array();
				$p_key = array();

				if(!empty($record['destination_table'])){
					$config[$index]['destination_table']=$record['destination_table'];
					$tmp_matching = split("\n",$record['attribute_matching']);
					foreach($tmp_matching as $match){
						$tmp = split('=',trim($match));
						if(!empty($tmp[0]))
							$matching[$tmp[0]]=$tmp[1];
					}
					unset($tmp_matching);
					$config[$index]['attribute_matching']=$matching;
					$tmp_p_key = preg_split("/[\s,;]/",$record['primarykey']);	
					foreach($tmp_p_key as $key){
						if(in_array($key,$matching)){
							$p_key[$key]=array_search($key,$matching);
						}
					}
					$config[$index]['primarykeys']=$p_key;
				} else {
					$config[$index]=NULL;
				}

				if(!empty($record['pre_event'])){
					$config[$index]['pre_event'] = $record['pre_event'];
				}

				if(!empty($record['post_event'])){
					$config[$index]['post_event'] = $record['post_event'];
				}
		}

		if(isset($config) && is_array($config)){
			$this->config=$config;
			if(count($this->config)>0){
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * Get parents for a given config xml node.
	 *
	 * @author Web Power B.V. | Jørgen Teunis
	 * @param array $record Record of the current config
	 * @param array $tmp Array of al config records for the current file
	 * @return string Pipe seperated string of hierarchie
	 * @access private
	 *
	 */

	function getParents($record,&$tmp){
		$path = array();
		while(isset($tmp[$record['id']])){
			$path[]=$tmp[$record['id']]['tag'];
			if(!isset($tmp[$record['parent_id']])){
				break;
			} else {
				$record = $tmp[$record['parent_id']];
			}
		}
		$path = array_reverse($path);
		return implode('|',$path);
	}

	/**
	 * Get xml object
	 *
	 * Returns an object from given xml data
	 *
	 * @author Web Power B.V. | Jørgen Teunis
	 * @param string $branch xmldata
	 * @return object of xml node
	 *
	 */

   function domxml_xmlarray($branch) {
       $branch = $branch->first_child();

       while ($branch) {
           if (!($branch->is_blank_node())) {
               $objptr = &$object[];

               switch ($branch->node_type()) {
                   case XML_TEXT_NODE:
                       $objptr['cdata'] = $branch->node_value();
                       break;
                   case XML_ELEMENT_NODE:
                       $objptr['tagname'] = $branch->node_name();
                       if($branch->has_attributes()){
                        $attrib = $branch->attributes ();
                        $objptr[$attrib[0]->name]=$attrib[0]->value;
                        unset($attrib);
                       }
                       break;
               }

               if ($branch->has_child_nodes()) {
                   $result = $this->domxml_xmlarray($branch);

                   foreach ($result as $values) {
                       array_push($objptr, $values);
                   }
               }
           }

           $branch = $branch->next_sibling();
       }

       return $object;
   }


	/**
	 * Place xmlobjects in a stack array. 
	 *
	 * This functions itherates through a xml object and generates xml
	 * object out of it and places them on a stack variable
	 *
	 * @author Web Power B.V. | Jørgen Teunis
	 * @param array $resultArray Parsed xml
	 * @param array $stack Reference to the stack variable
	 * @param string $parent Id of the parent object
	 * @return bool always true
	 *
	 */

	function stackObjects($resultArray=NULL,$parent=false,$path=array()){

		if(!isset($resultArray)){
			$resultArray = $this->xmlObj;
		}

		foreach($resultArray as $index=>$entiteit){
			if($index==='tagname'){
				$name = $entiteit;
				continue;
			}


			$data = array();
			$attribute = array();

			foreach($entiteit as $index=>$subtag){
				if($index==='tagname'){
					$node_obj = new xml_entiteit($subtag,$parent,$this->stack);
					$name = $subtag;
					$path[]=$name;
					
					$this->stack[$node_obj->id]=NULL;

					if(isset($this->config[implode('|',$path)])){
						$node_obj->set_config($this->config[ implode('|',$path) ]);						
					} 
					continue;
				}

				if(is_string($subtag)){
					$attribute[$index]=$subtag;
					continue;
				}
				if(isset($subtag[0]['cdata'])){
					$data[$subtag['tagname']]=$subtag[0]['cdata'];
				} else {
					$node = array(	'tagname'=>(!empty($name)) ? $name : '',
										0=>$subtag
									);
					$this->stackObjects($node,$node_obj->id,$path);
				}
				$node_obj->set_data($data);
				$node_obj->set_attributes($attribute);
			}
			$this->stack[$node_obj->id]=$node_obj;
		}
	return true;
	}

	/**
	 * Show what's on stack.
	 *
	 * @author Web Power B.V. | Jørgen Teunis
	 * @return string vardump of the stack array
	 * @access public
	 *
	 */

	function showStack(){
		$this->stack = array_reverse($this->stack);
		ob_start();
		var_dump($this->stack);
		$data = ob_get_contents();
		ob_clean();
		return $data;
	}


	/**
	 * Writes stack to database.
	 *
	 * Write the collected xmltree to the database
	 *
	 * @author Web Power B.V. | Jørgen Teunis
	 * @return boolean Did we succeed?
	 * @access public
	 *
	 */

	function writeStackToDb($auto_update=false){
		// elk object in de stack in de database opslaan
		foreach($this->stack as $object){
			// pre event:
			if(is_object($object) && !empty($object->config['pre_event'])){
				print "executing php code\n";
				$php_code = "?><?PHP\n".$object->config['pre_event']."\n?><?";
				eval($php_code);
			} 

			// data storage:
			if(is_object($object) && $object->hasData() && $object->storeToDb($auto_update)){
				print $object->name." stored\n";
			} 
	
			if(is_object($object) && !empty($object->config['post_event'])){
				print "executing php code\n";
				$php_code = "?><?PHP\n".$object->config['post_event']."\n?><?";
				eval($php_code);
			} 
			// post event:

		}
	}


}
?>
