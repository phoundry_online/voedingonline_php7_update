<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty html entities modifier plugin
 *
 * Type:     modifier<br>
 * Name:     htmlentities<br>
 * Purpose:  Convert all applicable characters to HTML entities
 * 
 * @param string
 * @param style
 * @param charset
 * @return string
 */

function smarty_modifier_htmlentities($string,$style=ENT_COMPAT,$charset='ISO-8859-1')
{
    return htmlentities($string,$style,$charset);
}

?>
