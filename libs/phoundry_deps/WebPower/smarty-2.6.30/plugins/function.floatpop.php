<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {popup} function plugin
 *
 * Type:     function<br>
 * Name:     floatpop<br>
 * Purpose:  make text pop up in windows via overlib
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_floatpop($params, &$smarty)
{
    $append = '';
    $text	= 'Popup';
    $url		= 'this';
    $trigger= 'onclick';
	 $height = 200;
	 $width	= 200;
    foreach ($params as $_key=>$_value) {
        switch ($_key) {
		  		case 'height':
					$height = $_value;
				break;
		  		case 'width':
					$width = $_value;
				break;
		  		case 'text':
					$text = $_value;
				break;
		  		case 'url':
					if(!empty($_value))
						$url = "'".$_value."'";
				break;
		  		case 'trigger':
					$trigger = $_value;
				break;

            default:
                $smarty->trigger_error("[popup] unknown parameter $_key", E_USER_WARNING);
        }
    }

    if (empty($text) && empty($function)) {
        $smarty->trigger_error("floatpop: attribute 'text' or 'function' required");
        return false;
    }

    if (empty($trigger)) { $trigger = "onmouseover"; }

    $retval = $trigger . '="floatPop(event,this,'.$width.','.$height.',\''.htmlspecialchars(preg_replace(array("!'!","![\r\n]!"),array("\'",'\r'),$text)).'\'';
    $retval .= $append . ','.$url.'); return false"';
    if ($trigger == 'onmouseover')
       $retval .= ' onmouseout="killPopup();"';


    return $retval;
}

/* vim: set expandtab: */

?>
