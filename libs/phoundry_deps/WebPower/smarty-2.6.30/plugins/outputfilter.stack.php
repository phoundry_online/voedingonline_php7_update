<?

function smarty_outputfilter_stack($tpl_source, &$smarty) {
   // Now replace the matched language strings with the entry in the file
   return preg_replace_callback('/%%STACK:(\w+)?%%/is', '_compile_stack', $tpl_source);
}

function _compile_stack($data) {
   $retval = '';
   if(isset($GLOBALS['stack'][$data[1]])){
      foreach($GLOBALS['stack'][$data[1]] as $value ){
         $retval.=$value."\n";
      }
   }
   return $retval;
}

?>
