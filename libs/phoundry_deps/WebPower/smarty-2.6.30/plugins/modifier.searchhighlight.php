<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty highlight keywords searchfor by htdig or google modifier plugin
 *
 * Type:     modifier<br>
 * Name:     searchhighlight<br>
 * Purpose:  Highlights keywords searched for by htdig or google 
 * 
 * @param string
 * @return string
 */

function smarty_modifier_searchhighlight($str)
{
	$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
	if (preg_match('/(words|q)=([^&]+)/', $ref, $reg)) {
		$words = explode(' ', urldecode($reg[2]));
		foreach($words as $word)
			$word = preg_quote($word,'/');
			$str = preg_replace("/(?![^<]+>)({$word})(?![^<]+>)/i", "<span class=\"hilite\">$1</span>", $str);
	}
	return $str;
}

?>
