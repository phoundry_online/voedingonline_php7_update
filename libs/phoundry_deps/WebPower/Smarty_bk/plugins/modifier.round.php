<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty round modifier plugin
 *
 * Type:     modifier<br>
 * Name:     round<br>
 * Purpose:  format number via round<br>
 * Input:<br>
 *         - string: input integer
 *         - decimals: decemals to round 
 * @param string
 * @return float
 */
function smarty_modifier_round($integer, $decimals=2)
{
	return round($integer,$decimals);
}

/* vim: set expandtab: */

?>
