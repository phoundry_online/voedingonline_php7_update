<?

function smarty_outputfilter_stack_html($tpl_source, &$smarty) {
   // replace html stacks
   if(preg_match_all('/<!-- +stack +name="(\w+)" *(?:assign="(\w+)" *)*-->(.*?)<!-- +\/stack.*?-->/is', $tpl_source, $matches, PREG_SET_ORDER)){
      foreach($matches as $match){
         if(empty($match[2])){
            $GLOBALS['stack'][$match[1]][] = trim($match[3]);
         } else {
            $GLOBALS['stack'][$match[1]][$match[2]] = trim($match[3]);
         }
      }
      // replace all stacked stuff away
      $tpl_source = preg_replace('/<!-- +stack .*?<!-- +\/stack.*?-->/is','', $tpl_source);
   }
   // Now replace the matched language strings with the entry in the file
   return preg_replace_callback('/%%HTML_STACK:(\w+)?%%/is', '_compile_html_stack', $tpl_source);
}

function _compile_html_stack($data) {
   $retval = '';
   if(isset($GLOBALS['stack'][$data[1]])){
      foreach($GLOBALS['stack'][$data[1]] as $value ){
         $retval.=$value."\n";
      }
   }
   return $retval;
}

?>
