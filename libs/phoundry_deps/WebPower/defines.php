<?
// Defines

function __getProjectDirScript(){
   $foo= (split('/',$_SERVER["PWD"]));
	return realpath("/{$foo[1]}/{$foo[2]}/");
}

define('PROJECT_DIR',   !empty($_SERVER['DOCUMENT_ROOT']) ? realpath(dirname($_SERVER['DOCUMENT_ROOT'])) : __getProjectDirScript() );
define('TEMPLATE_DIR',  PROJECT_DIR.'/templates/');
define('INCLUDE_DIR',   PROJECT_DIR.'/include/');
define('TMP_DIR',       PROJECT_DIR.'/tmp/');
define('CACHE_DIR',     TMP_DIR.'cache/');
define('CONFIG_DIR',		PROJECT_DIR.'/config/');
//define('HTDIGDB_DIR',	PROJECT_DIR.'/HTdig/db/');
define('PHP_UPLOAD_DIR',TMP_DIR.'php/');

?>
