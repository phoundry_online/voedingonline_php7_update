<?php

// format data according to given format

function get_date($format,$timestamp) 
{
   return date($format,$timestamp);
}

function smarty_function_html_get_date($params, &$smarty) 
{
	if(isset($params['timestamp']) && $params['timestamp']=='now'){
		$params['timestamp']=time();
	}
	if(empty($params['timestamp'])){
		if(defined('UTS')){
			$params['timestamp'] = UTS;
		} else {
			$params['timestamp'] = time();
		}
	}   
   // assign template variable
   return get_date($params['format'],$params['timestamp']);
}
?>
