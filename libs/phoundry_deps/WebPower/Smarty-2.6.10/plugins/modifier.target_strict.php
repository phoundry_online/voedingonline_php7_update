<?PHP

/*
* Make targets in anchor tags strict mode compatible
* @param String HTML
*/
function smarty_modifier_target_strict($_text){

        if (preg_match_all("/<\s*(a|area|form)\s+[^>]+>/i", $_text, $_matches, PREG_OFFSET_CAPTURE)) {
                $_posd = 0;
                $_chunks = array();
                foreach($_matches[0] as $_match) {
                        $_posp  = $_match[1];
                        $_pos   = $_match[1] - $_posd;
                        $_tag   = $_oldTag = $_match[0];
                        $_tagl  = strtolower($_tag);

                        // Skip tags without 'href' or 'action' attribute:
                        if ((!strpos($_tagl, 'href') && !strpos($_tagl, 'action')) || !preg_match('/(href|action)\s*=\s*(?:"([^"]+)"|([^\'">\s]+)|\'([^\']+)\')/i', $_tag, $_reg)){
                                continue;
                        }

                        $_url  = $_reg[2] ? $_reg[2] : ($_reg[3] ? $_reg[3] : $_reg[4]);
                        $_urll = strtolower($_url);

                        // Continue when URL starts with #, mailto:, ftp: of javascript:
                        if (strpos($_urll, '#') === 0 || strpos($_urll, 'mailto:') === 0 || strpos($_urll, 'javascript:') === 0 || strpos($_urll, 'ftp:') === 0) {
                                continue;
                        }

                        // Read TARGET tag:
                        if ((strtolower($_reg[1]) == 'href' || strtolower($_reg[1]) == 'action') && preg_match('/\s?target\s*=\s*(?:"([^"]+)"|([^\'">\s]+)|\'([^\']+)\')/i', $_tag, $_target)) {
                        	// Remove the TARGET attribute from the hyperlink:
                        	$_tag = str_replace($_target[0], '', $_tag);
									$_trgt = substr($_target[1], 0, 1) == '_' ? $_target[1] : '_target_'.$_target[1];

									// check if tag has CLASS attribute
									if(preg_match('/class\s*=\s*(?:"([^"]+)"|([^\'">\s]+)|\'([^\']+)\')/i', $_tag, $_class, PREG_OFFSET_CAPTURE)){
										// class already exists?
										if(!stristr($_class[1][0], $_trgt)){
											// add class
											$_tag = str_replace($_class[0][0], ' class="'. $_class[1][0].' '.$_trgt.'"', $_tag);
										}
									} else {
										$_tag = substr($_tag, 0, -1). sprintf(' class="%s">', $_trgt);
									}

                        }

                        $_chunks[] = substr($_text, 0, $_pos);
                        $_chunks[] = str_replace($_reg[0], "{$_reg[1]}=\"{$_url}\"", $_tag); // New tag
                        $_len      = strlen($_oldTag);
                        $_text     = substr($_text, $_pos + $_len);
                        $_posd     = $_posp + $_len;

                } // end foreach

                $_chunks[] = $_text;
                $_text = implode('', $_chunks);

        }
        if (preg_match_all("/<\s*(img)\s+[^>]+>/i", $_text, $_matches, PREG_OFFSET_CAPTURE)) {
                $_posd = 0;
                $_chunks = array();
                foreach($_matches[0] as $_match) {
                        $_posp  = $_match[1];
                        $_pos   = $_match[1] - $_posd;
                        $_tag   = $_oldTag = $_match[0];
                        $_tagl  = strtolower($_tag);

                        // Skip tags without 'href' or 'action' attribute:
                        if ((!strpos($_tagl, 'src')) || !preg_match('/(src)\s*=\s*(?:"([^"]+)"|([^\'">\s]+)|\'([^\']+)\')/i', $_tag, $_reg)){
                                continue;
                        }

                        $_url  = $_reg[2] ? $_reg[2] : ($_reg[3] ? $_reg[3] : $_reg[4]);
                        $_urll = strtolower($_url);

                        // Read BORDER tag:
                        if ((strtolower($_reg[1]) == 'src') && preg_match('/\s?border\s*=\s*(?:"([^"]+)"|([0-9]+)|\'([^\']+)\')/i', $_tag, $_border)) {
                        	// Remove the BORDER attribute from the hyperlink:
                        	$_tag = str_replace($_border[0], '', $_tag);
							$border = $_border[1];

								// check if tag has CLASS attribute
								if(preg_match('/style\s*=\s*(?:"([^"]+)"|([^\'">\s]+)|\'([^\']+)\')/i', $_tag, $_style, PREG_OFFSET_CAPTURE)){
									// style already exists?
									if(!stristr($_style[1][0], 'border:')){
										// add style
										$_tag = str_replace($_style[0][0], ' style="'. $_style[1][0].' ;border:'.$border.'"', $_tag);
									}
								} else {

									if(substr($_tag, -2, 1)=='/'){
										$pos = -2;
									} else {
										$pos = -1;
									}
									$_tag = substr($_tag, 0, $pos). sprintf(' style="border:%s" />', $border);
								}
                        }

                        $_chunks[] = substr($_text, 0, $_pos);
                        $_chunks[] = str_replace($_reg[0], "{$_reg[1]}=\"{$_url}\"", $_tag); // New tag
                        $_len      = strlen($_oldTag);
                        $_text     = substr($_text, $_pos + $_len);
                        $_posd     = $_posp + $_len;

                } // end foreach

                $_chunks[] = $_text;
                $_text = implode('', $_chunks);

        }
        return $_text;
}
?>
