<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty htmlspecialchars modifier plugin
 *
 * Type:     modifier<br>
 * Name:     htmlentities<br>
 * Purpose:  Convert special characters to HTML entities
 * 
 * @param string
 * @param style
 * @param charset
 * @return string
 */

function smarty_modifier_htmlspecialchars($string,$style=ENT_COMPAT,$charset='ISO-8859-1')
{
    return htmlspecialchars($string,$style,$charset);
}

?>
