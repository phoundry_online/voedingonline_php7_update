<?php

/**
* smarty_prefilter_i18n()
* This function takes the language file, and rips it into the template
* $GLOBALS['_NG_LANGUAGE_'] is not unset anymore
*
* @param $tpl_source
* @return
* @version 0.0.4
**/
function smarty_prefilter_i18n($tpl_source, &$smarty) {
	if (!is_object($GLOBALS['__LANGUAGE_'])) {
		die("Error loading Multilanguage Support");
	}
	// Now replace the matched language strings with the entry in the file
	return preg_replace_callback('/##(.+?)#[a-zA-Z0-9_()|\-| ]*?#/', '_compile_lang', $tpl_source);
}

/**
* _compile_lang
* Called by smarty_prefilter_i18n function it processes every language
* identifier, and inserts the language string in its place.
*
*/
function _compile_lang($key) {
	return trim($GLOBALS['__LANGUAGE_']->getTranslation($key[1]));
}

class wpLanguage {
	var $_translationTable			= Array();	// currently loaded translation table
	var $_supportedLanguages		= Array();	// array of all supported languages
	var $_defaultLocale				= 'nl';		// the default language
	var $_currentLocale				= NULL;		// currently set locale
	var $_currentLanguage			= NULL;		// currently loaded language
	var $_loadedTranslationTables	= Array();	// array of all loaded translation tables
	var $_languageTable = Array(					// array of language to file associations
			'en'		=> 'en', // engels
			'en-us'	=> 'en', // engels
			'en-gb'	=> 'en', // engels
			'hr'		=>	'cr', // croatisch
			'cs'		=>	'cz', // tjechisch
			'da'		=>	'de', // deens
			'nl-be'	=>	'nl', // nederlands
			'nl'		=>	'nl', // nederlands
			'fi'		=>	'fi', // fins
			'fr-be'	=>	'fr', // frans
			'fr-ca'	=>	'fr', // frans
			'fr'		=>	'fr', // frans
			'fr-lu'	=>	'fr', // frans
			'fr-mc'	=>	'fr', // frans
			'fr-ch'	=>	'fr', // frans
			'de-at'	=>	'de', // duits
			'de'		=>	'de', // duits
			'de-li'	=>	'de', // duits
			'de-lu'	=>	'de', // duits
			'de-ch'	=>	'de', // duits
			'dk'		=>	'dn', // deens
			'es'		=>	'es', // spaans
			'pt_br'	=> 'pt', // Portugees braziliaans
			'pt'		=> 'pt', // Portugees
			'no'		=>	'no', // noors
			'pl'		=>	'po', // pools
			'sv'		=>	'sw', // zweeds
			'tr'		=>	'tu', // tuks
			'ru'		=>	'ru', // russisch
			'tr'		=>	'it', // italiaans
			);


	function wpLanguage($locale,$languageDir='languages/'){
		if(!empty($languageDir)){
			$this->languageDir = rtrim($languageDir,"\t\n\r\0\x0B/");
		}
		foreach ($this->_languageTable as $lang){
			$this->_translationTable[$lang] = Array();
		}
		$this->_defaultLocale = 'nl';

		if (empty($locale)){
		  $locale = $this->getHTTPAcceptLanguage();
		}
		$this->setCurrentLocale($locale);
	}

	function getAvailableLocales() {
		return array_keys($this->_languageTable);
	}

	function getAvailableLanguages() {
		return array_unique(array_values($this->_languageTable));
	}

	function getCurrentLanguage() {
		return $this->_currentLanguage;
	}

	function setCurrentLanguage($language) {
		$this->_currentLanguage = $language;
	}

	function getCurrentLocale() {
		return $this->_currentLocale;
	}

	 function setCurrentLocale($locale) {
		$language = &$this->_languageTable[$locale];
		if (empty($language)) {
		  die ("LANGUAGE Error: Unsupported locale '$locale'");
		}
		$this->_currentLocale = $locale;
		return $this->setCurrentLanguage($language);
	 }

	 function getDefaultLocale() {
		return $this->_defaultLocale;
	 }

	function getHTTPAcceptLanguage() {
		if(isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])){
		$langs = explode(';', $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
		$locales = $this->getAvailableLocales();
		foreach ($langs as $value_and_quality) {
			 // Loop through all the languages, to see if any match our supported ones
			 $values = explode(',', $value_and_quality);
			 foreach ($values as $value) {
				if (in_array($value, $locales)){
					 // If found, return the language
					 return $value;
				}
			 }
		}
		}
		// If we can't find a supported language, we use the default
	return $this->getDefaultLocale();
	}

	function loadCurrentTranslationTable() {
		$this->_loadTranslationTable($this->getCurrentLocale());
	}

	// Warning: parameter positions are changed!
	function loadTranslationTable($dictionary) {
		if(empty($this->_currentLocale)){
			$locale = $this->getDefaultLocale();
		} else {
			$locale = $this->_currentLocale;
		}
		// Select corresponding language
		$language = $this->_languageTable[$locale];

		// Set path and filename of the language file
		$path = $this->languageDir."/{$language}/{$dictionary}.lng";
		// _loadTranslationTable() does the rest
		$this->_loadTranslationTable($locale, $path);
	}

	// Warning: parameter positions are changed!
	function unloadTranslationTable($locale) {
		// This method is only a placeholder and wants to be overwritten by YOU! ;-)
		$this->_unloadTranslationTable($locale);
	}

	function getTranslation($key,$dictionary=NULL) {
		$trans = &$this->_translationTable[$this->_currentLanguage];
		if (is_array($trans)) {
			if(count($trans)==0 && !is_null($dictionary)){
				if(is_array($dictionary)){
					foreach($dictionary as $dict){
						$this->loadTranslationTable($dict);
					} 
				} else {
					$this->loadTranslationTable($dictionary);
				}
			}
			if (isset($trans[$key])) {
				return $trans[$key];
			}
		}
	return $key;
	}



	// Warning: parameter positions are changed!
	function _loadTranslationTable($locale, $path='') {
		if (empty($locale)){
		  $locale = $this->getDefaultLocale();
		}
		$language = $this->_languageTable[$locale];
		if (empty($language)) {
		  die ("LANGUAGE Error: Unsupported locale '$locale'");
		}
		if (!is_array($this->_translationTable[$language])) {
		  die ("LANGUAGE Error: Language '$language' not available");
		}
		if(empty($path)){
		  $path = 'languages/'.$this->_languageTable[$locale].'/global.lng';
		}
		if (isset($this->_loadedTranslationTables[$language])) {
		  if (in_array($path, $this->_loadedTranslationTables[$language])) {
			 // Translation table was already loaded
			 return true;
		  }
		}
		if (file_exists($path)) {
		  $entries = file($path);
		  if(count($entries)==0){
				trigger_error('No entries in dictionary '.$path ,E_USER_NOTICE);
			}
		  if(!is_array($this->_translationTable[$language])){
				$this->_translationTable[$language] = Array();
		  }
		  $this->_loadedTranslationTables[$language][] = $path;
		  foreach ($entries as $row) {
			 if (substr(ltrim($row),0,2) == '//') // ignore comments
				continue;
			 
          $keyValuePair = explode('=', $row, 2);
			 
          // multiline values: the first line with an equal sign '=' will start a new key=value pair
			 if (sizeof($keyValuePair) == 1) {
				$this->_translationTable[$language][$key] .= ' ' . chop($keyValuePair[0]);
				continue;
			 }
			 
          $key = trim($keyValuePair[0]);
			 $value = $keyValuePair[1];
			 
          // when there are more ='ses glue $keyValuePair[1];
			 unset($keyValuePair[0]);
			 $value = implode ("", $keyValuePair);

			 if (!empty($key)) {
				if(isset($this->_translationTable[$language][$key])){
					trigger_error("Word {$key} is already defined",E_USER_WARNING);
				}
				$this->_translationTable[$language][$key] = chop($value);
			 }
			}
			if(!in_array($language,$this->_supportedLanguages)){
				$this->_supportedLanguages[]=$language;
			}
		return true;
		} else {
			trigger_error("Dictionairy file {$path} not found",E_USER_NOTICE);
		}
	return false;
	}

	// Warning: parameter positions are changed!
	function _unloadTranslationTable($locale) {
		$language = $this->_languageTable[$locale];
		if (empty($language)) {
		  die ("LANGUAGE Error: Unsupported locale '$locale'");
		}
		unset($this->_translationTable[$language]);
	return true;
	}

// EOC
}


?>
