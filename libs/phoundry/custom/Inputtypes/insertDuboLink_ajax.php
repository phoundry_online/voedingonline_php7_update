<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	
switch($_GET['type']) {
	case 'onderdeel':
		// Onderdeel was selected, show HTML for pagina pulldown:
		$value = $_GET['value'];
		$html =<<<EOH
<select name="pagina" onchange="setPagina(this)">
<option value="">[Maak een keuze]</option>
<option value="1">Pagina 1</option>
<option value="2">Pagina 2</option>
<option value="3">Pagina 3</option>
<option value="4">Pagina 4</option>
</select>
EOH;
		break;
	
	case 'pagina':
		// Pagina was selected, show HTML for subpagina pulldown:
		$value = $_GET['value'];
		$html =<<<EOH
<select name="subpagina">
<option value="">[Maak een keuze]</option>
<option value="a">SubPagina a</option>
<option value="b">SubPagina b</option>
<option value="c">SubPagina c</option>
<option value="d">SubPagina d</option>
</select>
EOH;
		break;
	default:
		$html = '[Unknown]';
}

print $html;

?>
