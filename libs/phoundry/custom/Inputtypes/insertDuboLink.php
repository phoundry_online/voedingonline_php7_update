<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Insert Dubo Link</title>
<style type="text/css">

body, td, button, select, input {
	font: Icon;
}

body {
   background: #fff;
   border: 0;
   margin: 3px;
}

legend {
	font-weight: bold;
}

select {
	width: 200px;
}

</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript">
//<![CDATA[

var isMidas = navigator.userAgent.indexOf('Gecko') != -1, 
	 editorArgs = isMidas ? window.opener.winArgs : window.dialogArguments,
	 editor = editorArgs['editor'];

function setOnderdeel(el) {
	if (el.value == '') return; // No option selected
	$.ajax({type:'GET', url:'insertDuboLink_ajax.php', data:{type:'onderdeel',value:el.value,_rnd:Math.random()}, dataType:'html', success:function(data){
		$('#paginaDiv').html(data).parent('fieldset').fadeIn();
	}});
}

function setPagina(el) {
	if (el.value == '') return; // No option selected
	$.ajax({type:'GET', url:'insertDuboLink_ajax.php', data:{type:'pagina',value:el.value,_rnd:Math.random()}, dataType:'html', success:function(data){
		$('#subpaginaDiv').html(data).parent('fieldset').fadeIn();
		$('#submitBut').prop('disabled',false);
	}});
}

function insertLink(el) {
	var val = $('#subpaginaDiv select').val();
	if (val == '') return; // No option selected

	var url = '{id=' + val + '}';
	
	if (isMidas) {
		editor.insertHTML('<a href="' + url + '">', '</a>', 1);
	}
	else {
		var S=editor.doc.selection,R=S.createRange(),txt,ms=0,me=0;
		if (S.type!='Control'){
			txt=R.text;
			if(txt!=''){
				while(/^\s/.test(txt)) {
           		txt=txt.substr(1);
           		ms++
        		}
        		while(/\s$/.test(txt)) {
           		txt=txt.substring(0,txt.length-1);
           		me--
        		}
        		R.moveStart('character',ms);
        		R.moveEnd('character',me);
        		R.select()
			}
		}
		editor.doc.execCommand('CreateLink',false,url);
	}
	window.close();
}

//]]>
</script>
</head>
<body>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="get">

<fieldset><legend>Onderdeel</legend>
<div id="onderdeelDiv">
<select name="onderdeel" onchange="setOnderdeel(this)">
<option value="">[Maak uw keuze]</option>
<option value="projecten">projecten</option>
</select>
</div>
</fieldset>

<fieldset style="display:none"><legend>Pagina</legend>
<div id="paginaDiv"></div>
</fieldset>

<fieldset style="display:none"><legend>Subpagina</legend>
<div id="subpaginaDiv"></div>
</fieldset>

<p align="right">
<input id="submitBut" type="button" value="<?= word(82) ?>" onclick="insertLink()" disabled="disabled" />
<input type="button" value="<?= word(46) ?>" onclick="window.close()" />
</p>

</form>
</body>
</html>
