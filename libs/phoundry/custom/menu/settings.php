<?php
	/*
	 * The settings here are translated into DB-queries automagically.
	 * You need a table that looks like this to make this plugin work:
	 * <----->
	 * +-----+-----------+-------------+----------+
	 * | id  | parent_id |    name     | order_by |
	 * | int |    int    | varchar(80) |   int    |
	 * +-----+-----------+-------------+----------+
	 *                                    site_menu
	 *
	 * The settings you enter are translated into a recursive query like this:
	 *    SELECT id, name, parent_id 
	 *    FROM site_menu 
	 *    WHERE parent_id IS NULL ORDER BY order_by;
	 */

	$menuSettings = array(
		'title'       => 'Menu configuratie',
		'width'       => 400,			// In pixels
		'height'      => 400,   		// In pixels
		'rootName'    => 'Menu',		// Name of the root element.
		'tableName'   => 'site_menu',
		'keyField'    => 'id',
		'parentField' => 'parent_id',
		'orderField'  => 'order_by',
		'showField'   => 'name'
	);
?>
