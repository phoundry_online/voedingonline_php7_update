<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'settings.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Shortcut for $menuSettings:
	$MS = &$menuSettings;
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= $MS['title'] ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= $PHprefs['url'] ?>/core/xtree/codebase/dhtmlxtree.css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/xtree/codebase/dhtmlxcommon.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/xtree/codebase/dhtmlxtree.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/xtree/codebase/ext/dhtmlxtree_xw.js"></script>
<script type="text/javascript">
//<![CDATA[

var tree0, curTree, cnt = 100000;

function addFolder(tree) {
	curTree = tree;
	makePopup(null,300,100,'Folder',null,'<form onsubmit="return false"><fieldset><legend><b>Folder</b></legend><input type="text" name="folder" style="width:270px" /></fieldset><p align="right"><input type="button" value="Ok" class="okBut" onclick="addFolder2(this.form[\'folder\'].value);killPopup()" /><input type="button" class="okBut" value="Cancel" onclick="killPopup()" /></p></form>');
}

function addFolder2(name) {
	var parent = curTree.getSelectedItemId();
	curTree.insertNewItem(parent,cnt++,name,0,'folderClosed.gif',0,0,'SELECT,TOP');
}

function renameItem(tree) {
	curTree = tree;
	var id = tree.getSelectedItemId();
	if (!id) return;
	if (id == 'froot') { alert('You cannot rename the root!'); return }
	makePopup(null,300,100,'Name',null,'<form onsubmit="return false"><fieldset><legend><b>New name</b></legend><input type="text" name="name" value="'+tree.getItemText(id)+'" style="width:270px" /></fieldset><p align="right"><input type="button" value="Ok" class="okBut" onclick="renameItem2(this.form[\'name\'].value);killPopup()" /><input type="button" class="okBut" value="Cancel" onclick="killPopup()" /></p></form>');
}

function renameItem2(name) {
	var id = curTree.getSelectedItemId();
	curTree.setItemText(id, name);
}

function deleteItem(tree) {
	var id = tree.getSelectedItemId();
	if (id == 'froot') { alert('You cannot delete the root!'); return }
	tree.deleteItem(id);
}

function moveUp(tree) {
	var id = tree.getSelectedItemId();
	if (!id) return;
	tree.moveItem(id, 'up_strict');
	tree.selectItem(id);
}

function moveDown(tree) {
	var id = tree.getSelectedItemId();
	if (!id) return;
	tree.moveItem(id, 'down_strict');
	tree.selectItem(id);
}

_W.onload=function(){
	tree0=new dhtmlXTreeObject('tree0','100%','100%',0);
	tree0.setImagePath('<?= $PHprefs['url'] ?>/core/xtree/codebase/imgs/');
	tree0.enableTreeLines(true);
	tree0.enableDragAndDrop(true);
	tree0.loadXML('tree.php?<?= QS(0) ?>');
}

function checkForm(F)
{
	tree0.setSerializationLevel(false,false,true);
	F['menu'].value = tree0.serializeTree();
	return true;
}

//]]>
</script>
</head>
<body class="frames">
<form action="menu.php?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="menu" />
<div id="headerFrame" class="headerFrame">
<?= getHeader($MS['title']); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<fieldset><legend><b><?= $MS['rootName'] ?></b></legend>

<button type="button" onclick="addFolder(tree0);return false" title="New folder"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/folder_add.png" alt="New folder"  /></button>
<button type="button" onclick="renameItem(tree0);return false" title="Rename item"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/textfield_rename.png" alt="Rename item" /></button>
<button type="button" onclick="deleteItem(tree0);return false" title="Delete item"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/delete.png" alt="Delete item" /></button>
<button type="button" onclick="moveUp(tree0);return false" title="Move up"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/control_play_blue_up.png" alt="Move up"  /></button>
<button type="button" onclick="moveDown(tree0);return false" title="Move down"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/control_play_blue_down.png" alt="Move down" /></button>

<div id="tree0" style="width:500px;height:400px;background:#f5f5f5;border:1px solid silver"></div>

</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input class="okBut" type="submit" value="<?= word(82) ?>" />
	</td>
	<td align="right">
	<input type="button" value="<?= word(46) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
