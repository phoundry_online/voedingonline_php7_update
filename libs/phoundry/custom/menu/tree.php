<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'settings.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Shortcut for $menuSettings:
	$MS = &$menuSettings;

	$MS['sql'] = "SELECT {$MS['keyField']}, {$MS['parentField']}, {$MS['showField']} FROM {$MS['tableName']} WHERE {$MS['parentField']} IS NULL";
	if (!empty($MS['orderField']))
		$MS['sql'] .= " ORDER BY {$MS['orderField']}, {$MS['keyField']}";
	else
		$MS['sql'] .= " ORDER BY {$MS['keyField']}";

	function getMenuXML($sql, $curid = '') {
		global $db, $MS;
		$xml = '';
		$mysql = $sql;
		if ($curid != '') {
			$mysql = str_replace("{$MS['parentField']} IS NULL", "{$MS['parentField']} = $curid", $mysql);
		}
		$cur = $db->Query($mysql)
			or trigger_error("Query $mysql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			//print '--'.htmlspecialchars($db->FetchResult($cur,$x,$MS['showField']))."--\n";
			$xml .= '<item id="' . $db->FetchResult($cur,$x,$MS['keyField']) . '" text="' . PH::htmlspecialchars($db->FetchResult($cur,$x,$MS['showField'])) . '" im0="folderClosed.gif">';
			$xml .= getMenuXML($sql, $db->FetchResult($cur,$x,$MS['keyField']));
			$xml .= "</item>\n";
		}
		return $xml;
	}


	if (stristr($_SERVER['HTTP_ACCEPT'],'application/xhtml+xml')) 
	{
		header('Content-type: application/xhtml+xml');
	}
	else
	{
		header('Content-type: text/xml');
	}
	print '<?xml version="1.0" encoding="iso-8859-1"?>' . "\n";


?>
<tree id="0">
	<item id="froot" text="/" open="1">
	<?php
		print getMenuXML($MS['sql']);
		/*
		$sql = "SELECT id, name, description FROM phoundry_table";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$items = array();
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$items[$db->FetchResult($cur,$x,'id')] = $pm->word($db->FetchResult($cur,$x,'description'));
		}
		// Sort items alphabetically:
		asort($items);
		foreach($items as $id=>$desc) {
			$id = 'i' . $x . '_' . $id;
			print '<item text="' . PH::htmlspecialchars($desc) . '" id="' . $id . '" />';
		}
		*/
	?>
	</item>
</tree>
