<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'settings.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Shortcut for $menuSettings:
	$MS = &$menuSettings;

	function getMenuIDs() {
		global $db, $MS;

		// What menu items do we already have?
		$ids = array();
		$sql = "SELECT {$MS['keyField']} FROM {$MS['tableName']}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$ids[] = (int)$db->FetchResult($cur,$x,0);
		}
		return $ids;
	}

   function storeTree($xml, $knownIDs, &$xmlIDs) {
      global $db, $MS, $PHprefs;

      $stack = array();
		$xmlIDs = array();

		$xml = str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="' . $PHprefs['charset'] . '"?>', $xml);

      $p = xml_parser_create($PHprefs['charset']);
      $res = xml_parse_into_struct($p, $xml, $vals, $index);
		if (!$res) {
			die('Error while parsing XML: <pre>' . $xml . '</pre>');

		}
      xml_parser_free($p);

		$ordr = 1;

      foreach($vals as $val) {
         if ($val['tag'] != 'ITEM') continue; // We only 'understand' ITEMs
         if (isset($val['attributes']) && $val['attributes']['ID'] == 'froot' && $val['level'] == 2) continue;

         if ($val['type'] == 'complete' || $val['type'] == 'open') {
            $id = (int)$val['attributes']['ID'];
				$xmlIDs[] = $id;
            $parent = count($stack) > 0 ? $stack[count($stack)-1] : 'NULL';
            $name = $val['attributes']['TEXT'];

				if (in_array($id, $knownIDs)) {
					// Update:
					$sql = "UPDATE {$MS['tableName']} SET {$MS['showField']} = '" . escDBquote($name) . "', {$MS['orderField']} = $ordr, {$MS['parentField']} = $parent WHERE {$MS['keyField']} = $id";
					$cur = $db->Query($sql)
						or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
					$stackID = $id;
				}
				else {
					// Insert:
					$sql = "INSERT INTO {$MS['tableName']} ({$MS['keyField']}, {$MS['parentField']}, {$MS['showField']}, {$MS['orderField']}) VALUES (NULL, {$parent}, '" . escDBquote($name) . "', $ordr)";
					$cur = $db->Query($sql)
						or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
					$stackID = $db->getLastInsertId($MS['tableName'], 'id');
				}
         }

         if ($val['type'] == 'open') {
            // Start branch:
            array_push($stack, $stackID);
         }
         elseif ($val['type'] == 'close') {
            // End branch:
            array_pop($stack);
         }
			$ordr++;
      }
   }

	// What menu items do we already have?
	$oldIDs = getMenuIDs();

	// Store tree items:
	storeTree($_POST['menu'], $oldIDs, $newIDs);

	// Delete nodes that were removed:
	$toDel = array_diff($oldIDs, $newIDs);
	foreach($toDel as $DBid) {
		$sql = "DELETE FROM {$MS['tableName']} WHERE {$MS['keyField']} = $DBid";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}

	header('Location: index.php?' . QS(0));
?>
