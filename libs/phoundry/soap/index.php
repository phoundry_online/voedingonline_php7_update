<?php

ini_set('soap.wsdl_cache_enabled','0');

if (!isset($HTTP_RAW_POST_DATA)){
   // PHP 5.2.2 has a bug, it does not populate HTTP_RAW_POST_DATA:
   $HTTP_RAW_POST_DATA = file_get_contents('php://input');
}

$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}

// Include, but don't start session:
$dontStartSession = true;
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once "{$PHprefs['distDir']}/core/include/PhoundryLogin.php";

class PhoundrySoapAPI {
	protected $PL;

	public function __construct() 
	{
		$this->PL = new PhoundryLogin(false);
	}

	public function getLoginToken($username, $password) 
	{
		return $this->PL->getLoginToken($username, $password);
	}
}

$wsdlFile = "{$PHprefs['tmpDir']}/phoundry.wsdl";
if (!file_exists($wsdlFile)) {
	// Generate WSDL:
	$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
	$location = $protocol . $_SERVER['HTTP_HOST'] . $PHprefs['url'] . '/soap/';

	$wsdl = file_get_contents('skeleton.wsdl');
	$wsdl = str_replace('{$location}', $location, $wsdl);

	file_put_contents($wsdlFile, $wsdl);
}

$server = new SoapServer($wsdlFile, array('encoding'=>'iso-8859-1'));
$server->setClass('PhoundrySoapAPI');
$server->handle();
