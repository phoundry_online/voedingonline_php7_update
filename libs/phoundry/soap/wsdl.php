<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}

	$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
	$location = $protocol . $_SERVER['HTTP_HOST'] . $PHprefs['url'] . '/soap/';

	$wsdl = file_get_contents('skeleton.wsdl');
	$wsdl = str_replace('{$location}', $location, $wsdl);

	header('Content-type: text/xml');
	print $wsdl;
