<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once "{$PHprefs['distDir']}/core/include/PhoundryLogin.php";

header("Content-type: text/html; charset={$PHprefs['charset']}");

$PL = new PhoundryLogin();
$activeLang = $PHprefs['languages'][0];
if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) 
{
	$tmp = explode('-', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
	// Check for lang in request object, if it doenst exists use default lang from prefs
	if (!isset($_REQUEST['lang']) && !isset($PL->cookie['lang']) && in_array($tmp[0], $PHprefs['languages'])) 
	{
   	$activeLang = $tmp[0];
	} else if (isset($_REQUEST['lang']) && preg_match('/^[a-z]{2}$/', $_REQUEST['lang'])) 
	{
   	$activeLang = $_REQUEST['lang'];
	} else if (isset($PL->cookie['lang']) && preg_match('/^[a-z]{2}$/', $PL->cookie['lang'])) 
	{
   	$activeLang = $PL->cookie['lang'];
	}
}

include $PHprefs['distDir'] . '/core/lang/' . $activeLang . '/words.php';
@include_once $PHprefs['distDir'] . '/core/lang/' . $activeLang . '/words.php';
@include_once $PHprefs['instDir'] . '/custom/lang/' . $activeLang . '/words.php';

// Has this license's expiration date passed?
if (isset($PHprefs['expireDate']) && preg_match('/^\d{4}-\d{2}-\d{2}$/', $PHprefs['expireDate'])) 
{
	if (date('Y-m-d') > $PHprefs['expireDate']) 
	{
		PH::phoundryErrorPrinter(word(204, $PHprefs['expireDate']), true);
	}
}

   
$url = $PHprefs['url'] === '' ? '/' : $PHprefs['url'];
if (substr($url, -1) != '/') { $url .= '/'; } // Add slash if not present

$redirectPage = '';
if (isset($_REQUEST['_page']) && !preg_match('~://~', $_REQUEST['_page']) && !preg_match('~^//~', $_REQUEST['_page'])) {
	$redirectPage = $_REQUEST['_page'];
}

if ($redirectPage) {
	$url .= '?_page=' . rawurlencode($redirectPage);
}

if (!isset($_SESSION['mobile'])) 
{
	$_SESSION['mobile'] = false;
	if (preg_match('/(Android|iPhone)/i', $_SERVER['HTTP_USER_AGENT'])) 
	{
		$_SESSION['mobile'] = true;
	}
}
if (isset($_GET['mobile']) && $_GET['mobile'] == 'false') 
{
	$_SESSION['mobile'] = false;
}

// User wants to log out:
if (isset($_GET['logout'])) 
{
	$PL->logout();
	$_SESSION = array('PHlogout'=>true);
	header('Location: ' . $url);
}
// SU login via auto.webpower.nl (SMS):
elseif (!empty($_GET['show_sms'])) 
{
	print $PL->getLoginPage($PL->loginMsg, "$('#sms').show();");
}
elseif (!empty($_GET['show_otp']))
{
	print $PL->getLoginPage($PL->loginMsg, "$('#otp').show();");
}
// SU login via auth.webpower.nl:
elseif (!empty($_GET['su']) && isset($_SESSION['su'])) 
{
	if ($PL->checkSUlogin2($_GET['su'])) {
		if ($redirectPage) {
			$url = $redirectPage;
		}
		$PL->startPhoundrySession();
		header('Location: ' . $url);
	}
	else
	{
		print $PL->getLoginPage($PL->loginMsg);
	}
}
// Single-signon attempt with token:
elseif (isset($_REQUEST['token'])) 
{
	if ($PL->checkTokenLogin($_REQUEST['token'])) 
	{
		$PL->startPhoundrySession(isset($_REQUEST['lang']) ? $_REQUEST['lang'] : null);
		if ($redirectPage) {
			$url = $redirectPage;
		}
		header('Location: ' . $url);
	}
	else
	{
		print $PL->getLoginPage($PL->loginMsg);
	}
}
// User logs in by clicking a login-link from the login email:
elseif (isset($_REQUEST['login1']) || isset($_REQUEST['login2'])) 
{
	if ($PL->checkDelayedLogin(isset($_REQUEST['login2']) ? $_REQUEST['login2'] : $_REQUEST['login1'], isset($_REQUEST['login2']) ? 'multi' : 'once')) 
	{
		// Show frameset:
		$PL->startPhoundrySession();
		header('Location: ' . $url);
	}
	else 
	{
		print $PL->getLoginPage($PL->loginMsg);
	}
}
// User is already logged in:
elseif ($PL->isLoggedIn()) 
{
	$framesetPage = $PHprefs['customUrls']['home'];
	if ($redirectPage) {
		$framesetPage = $_REQUEST['_page'];
	}
	if ($PL->needsPasswordChange($PHSes->userId)) 
	{
		$framesetPage = "{$PHprefs['url']}/core/changePwd.php?expire=true";
	}
	print $PL->getFrameset($framesetPage);
}
elseif (isset($_REQUEST['username']) && isset($_REQUEST['password']))
{
	$username = $_REQUEST['username'];
	$password = $_REQUEST['password'];
	// Remember me:
	$PL->setCookieVar('username', isset($_REQUEST['remember_me']) ? $username : null); 
	if (isset($_REQUEST['lang'])) { $PL->setCookieVar('lang', $_REQUEST['lang']); }
	if (!empty($_REQUEST['sms_login_code'])) {
		$ok = $PL->checkSMSlogin($username, $password, $_REQUEST['sms_login_code']);
	}
	elseif (!empty($_REQUEST['otp_login_code'])) {
		$ok = $PL->checkOTPlogin($username, $password, $_REQUEST['otp_login_code']);
	}
	else
	{
		$ok = $PL->checkLogin($username, $password, !empty($_REQUEST['signature']) ? $_REQUEST['signature'] : '');
	}
	if ($ok === true) 
	{
		// Valid login:
		$PL->startPhoundrySession();
		header('Location: ' . $url);
	}
	else
	{
		$onload = ($ok == 'email_confirm') ? "$(':input').prop('disabled', true);" : '';
		print $PL->getLoginPage($PL->getLoginMessage(), $onload);
	}
}
else
{
	// User is not logged in yet.
	if (isset($_SESSION['PHlogout'])) 
	{
		unset($_SESSION['PHlogout']);
		print $PL->getLoginPage(word(552));
	}
	else
	{
		print $PL->getLoginPage();
	}
}
