<?php
namespace WebPower\Phoundry\ErrorHandling;

function mail() {
	MailerErrorObserverTest::$mail[] = func_get_args();
}

class MailerErrorObserverTest extends \PHPUnit_Framework_TestCase
{
	public static $mail;

	protected function setUp()
	{
		self::$mail = array();
	}

	public function testConstructObserver()
	{
		$observer = new MailerErrorObserver(array('christiaan.baartse@webpower.nl'), array());
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testConstruct_invalidRecipient()
	{
		new MailerErrorObserver(array('invalid'), array());
	}

	public function testNotify()
	{
		$observer = new MailerErrorObserver(array('christiaan.baartse@webpower.nl'), array());

		$error = new \ErrorVoedingonline(E_USER_ERROR, 'errstr', 'errfile', 1337, array());

		$source = 'Source';
		$backtrace = array();

		$observer->notify($error, $source, $backtrace);

		$this->assertEquals(1, count(self::$mail));
		list($to, $subject, $message, $headers) = self::$mail[0];

		$this->assertEquals('christiaan.baartse@webpower.nl', $to);
		$this->assertEquals('Error Mailer: errstr', $subject);
		$this->assertContains('256: errstr in file: errfile at line: 1337', $message);
		$this->assertContains('URL: Probably Commandline', $message);
		$this->assertContains('backtrace.txt', $message);

		$this->assertContains('From: Error Mailer <errormailer@webpower.nl>', $headers);
		$this->assertContains('Content-Type: multipart/mixed;', $headers);
		$this->assertContains('Multipart_Boundary', $headers);
	}
}
