<?php
namespace WebPower\Phoundry\ErrorHandling;

use WebPower\Phoundry\ErrorHandling\ErrorObserver;

function ini_set($key, $value) {
	ErrorHandlerTest::$iniSet = array($key, $value);
}
function set_error_handler($callback) {
	ErrorHandlerTest::$setErrorHandler = $callback;
}
function set_exception_handler($callback) {
	ErrorHandlerTest::$setExceptionHandler = $callback;
}
function register_shutdown_function($callback) {
	ErrorHandlerTest::$registerShutdownFunction = $callback;
}
class ErrorHandlerTest extends \PHPUnit_Framework_TestCase
{
	public static $iniSet;
	public static $setErrorHandler;
	public static $setExceptionHandler;
	public static $registerShutdownFunction;
	public static $errorReporting;

	/** @var ErrorObserver|\PHPUnit_Framework_MockObject_MockObject */
	private $observer;
	/** @var ErrorHandler */
	private $handler;

	protected function setUp()
	{
		self::$iniSet = null;
		self::$setErrorHandler = null;
		self::$setExceptionHandler = null;
		self::$registerShutdownFunction = null;
		self::$errorReporting = E_ALL;

		$this->observer = $this->getMock('WebPower\Phoundry\ErrorHandling\ErrorObserver');
		$this->handler = new ErrorHandler();
	}

	public function testRegister()
	{
		$this->handler->register();

		$this->assertEquals(array('display_errors', 0), self::$iniSet);
		$this->assertEquals(array($this->handler, 'handleError'), self::$setErrorHandler);
		$this->assertEquals(array($this->handler, 'handleException'), self::$setExceptionHandler);
		$this->assertEquals(array($this->handler, 'shutdown'), self::$registerShutdownFunction);
	}

	public function testHandleErrorDoesNotHandleStrictAndSupressedErrors()
	{
		$res = $this->handler->handleError(E_STRICT, 'Fout', __FILE__, 18);
		$this->assertFalse($res);

		$res = @$this->handler->handleError(E_ERROR, 'Fout', __FILE__, 18);
		$this->assertFalse($res);
	}

	public function testHandleError()
	{
		$this->handler->addObserver($this->observer);

		$args = null;
		$this->observer->expects($this->once())->method('notify')->will($this->returnCallback(function() use(&$args) {
			$args = func_get_args();
		}));

		$res = $this->handler->handleError(E_ERROR, 'Fout', __FILE__, 66);
		$this->assertTrue($res);

		$this->assertInternalType('array', $args);
		list($error, $source, $backtrace) = $args;
		$this->assertTrue($error instanceof Error);
		$this->assertStringEqualsFile(__DIR__.'/source', $source);
		$this->assertInternalType('array', $backtrace);
	}
}
