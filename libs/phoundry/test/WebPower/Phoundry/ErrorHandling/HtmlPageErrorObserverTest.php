<?php
namespace WebPower\Phoundry\ErrorHandling;

class HtmlPageErrorObserverTest extends \PHPUnit_Framework_TestCase
{
	public function testConstruction()
	{
		$observer = new HtmlPageErrorObserver(
			'utf-8',
			false
		);
		$observer->setTemplate(__DIR__ . '/template.php');

		$error = new \ErrorVoedingonline(
			E_USER_ERROR,
			'Fout Melding',
			__FILE__,
			18,
			array('backtrace' => 1)
		);

		ob_start();
		$observer->notify($error, 'Source', array('backtrace' => 2));
		$output = ob_get_clean();
		$this->assertEquals(var_export(array(
			'code' => E_USER_ERROR,
			'message' => 'Fout Melding',
			'file' => __FILE__,
			'line' => 18,
			'backtrace' => '',
			'source' => '',
		), true), $output);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testConstruction_templateNotFound()
	{
		$observer = new HtmlPageErrorObserver('utf-8', true);
		$observer->setTemplate('nonexisting');
	}
}
