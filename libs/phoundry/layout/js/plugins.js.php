jQuery(function($){
	var block = $('#addBut[onclick="addBlock()"]');

	if (block.length === 0) {
		// Set up fieldsets class
		$('fieldset, .fieldset').filter(function() {
			return $(this).parent('.fieldset-wrapper').length == 0;
		}).wrap('<div class="fieldset-wrapper">');
		$('.fieldset-wrapper:has(>fieldset>legend .req)').addClass('fieldset-wrapper-req');

		var wrapFieldsets = function (selector, wrapper, dont_wrap_parent) {
			var adjenctive, fieldsets = $(selector).filter(function() {
				return $(this).parent(dont_wrap_parent).length == 0;
			});
			while(fieldsets.length > 0) {
				adjenctive = $(fieldsets.get(0)).nextAll(selector).andSelf();
				adjenctive.wrapAll(wrapper);
				fieldsets = fieldsets.not(adjenctive);
			}
		};

		wrapFieldsets('.fieldset-wrapper:has(.field)', '<div class="fieldsets-wrapper-fields">', '.fieldsets-wrapper-fields');
		wrapFieldsets('.fieldset-wrapper:not(:has(.field))', '<div class="fieldsets-wrapper">', '.fieldsets-wrapper');
	}

	if (typeof(popUpCal) !== 'undefined') {
		popUpCal.buttonImage = '<?=$PHprefs['url']?>/layout/pics/calendar-time.png';
	}

	$('.menu-handle').bind('click', function() {
		var $this, fs, open;
		if (parent) {
			$this = $(this);
			fs = parent.document.getElementById('frames');
			if (fs) {
		        open = fs.className != 'menuclosed';
		        fs.className = open ? 'menuclosed' : '';
		        $this.toggleClass('menu-closed', open);
			}
		}
	});
});
