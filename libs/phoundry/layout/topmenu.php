<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	checkAccess();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= $PRODUCT['name'] ?> Top Menu</title>
<style type="text/css">

body {
	background: #cfcfcf;
	margin: 0;
	padding:	0;
	color: #000;
	border: 0;
	-moz-user-select:	none;
	border-bottom: 1px solid #a0a0a0;
}

td {
	font-family: Tahoma,Futura,Helvetica,Sans-serif;
	font-size: 12px;
	white-space: nowrap;
	vertical-align: middle;
}

.but {
	border: 1px solid #cfcfcf;
	padding: 0 4px 0 4px;
}

.but img, .buthi img, .butlo img {
	vertical-align: middle;
}

.buthi {
	padding: 0 4px 0 4px;
	border-left: 1px solid buttonhighlight;
	border-top: 1px solid buttonhighlight;
	border-right: 1px solid buttonshadow;
	border-bottom: 1px solid buttonshadow;
}

.butlo {
	padding: 0 4px 0 4px;
	border: 1px inset window;
}

</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript">

var menuOn = true, showClock = false, userInfo = '', timer;

function logout() {
	if (confirm('<?= escSquote(word(135 /* Do you want to log out? */)) ?>')) {
		top.document.location.href = '<?= $PHprefs['url'] ?>/?logout=1';
	}
}

function clockTick() {
	var X = new Date(), DF = '<?= $g_dateFormat ?>', days = ['<?= implode("','", array(word(256), word(257), word(258), word(259), word(260), word(261), word(262))) ?>'];
	var D = X.getDay();     D = (D == 0) ? 6 : D - 1;
	var H = X.getHours();   if (H < 10) H = '0' + H;
	var M = X.getMinutes(); if (M < 10) M = '0' + M;
	var S = X.getSeconds(); if (S < 10) S = '0' + S;
	var Y = X.getYear();    if (Y < 1900) Y += 1900;
	$('#statusEl').html(days[D] + ', ' + DF.replace(/DD/, X.getDate()).replace(/MM/, X.getMonth()+1).replace(/YYYY/, Y) + ' ' + H + ':' + M + ':' + S);
}

function openManual(url) {
	var pageID = top.frames['PHcontent'].document.getElementById('pageID'),
	    sep = url.indexOf('?') == -1 ? '?' : '&';
	if (pageID) {
		url += sep + 'pageID=' + escape(pageID.content);
	}
	openCommunity(url,850,540);
}

$(function(){
	$('body').bind('selectstart', function(){return false}).bind('dragstart', function(){return false});
	userInfo = $('#statusEl').html();
	$('td.but').add('td.butlo').css('cursor','default');
	$('td.but').mouseover(function(){this.className='buthi'}).mouseout(function(){this.className='but'}).mousedown(function(){this.className='butlo'}).mouseup(function(){this.className='buthi'});
	$('td#menuBut').click(function(){
		var fs = parent.document.getElementById('mainFS');
		fs.cols = (menuOn) ? '0,*' : '158,*';
		this.className = (menuOn) ? 'but' : 'butlo';
		if (menuOn)
			$(this).mouseover(function(){this.className='buthi'}).mouseout(function(){this.className='but'});
		else
			$(this).unbind('mouseover').unbind('mouseout');
		menuOn = !menuOn;
	});
	$('#statusEl').click(function(){
		showClock = !showClock;
		if (showClock) {
			clockTick();
			timer = setInterval(function(){clockTick()}, 1000);
		}
		else {
			clearTimeout(timer);
			$(this).html(userInfo);
		}
	});
});

</script>
</head>
<body>
<?php if (strtolower($PRODUCT['name']) == 'dmdelivery' && $PHSes->userId == 1) { ?>
<div>
	<input style="width:24px;font:11px Tahoma,Futura,Verdana;background:#7dc8ff" type="text" onchange="parent.PHmenu.findCampaign(this.value);this.value=''" title="Enter campaign ID" />
</div>
<?php } ?>

<div class="button" onclick="parent.PHcontent.document.location.href='<?= $PHprefs['customUrls']['home'] ?>'">
	<?= word(275 /* Home */) ?>
</div>

<div id="menuButton" class="button">
	<span style="background:url(<?= $PHprefs['url'] ?>/core/pics/menu_on.png) left 3px no-repeat;padding-left:14px">
	<?= word(276 /* Menu */) ?>
	</span>
</div>

<?php
	if (is_array($PHprefs['manualUrl'])) {
		foreach($PHprefs['manualUrl'] as $name=>$manualUrl) {
			print '<div class="button" title="' . $name . ' ' . word(277) . '" onclick="openManual(\'' . $manualUrl . '\')">' . word(277 /* Manual */) . '</div>';
		}
	}
	else {
		print '<div class="button" title="' . $PRODUCT['name'] . ' ' . word(277) . '" onclick="openManual(\'' . $PHprefs['manualUrl'] . '\')">' . word(277 /* Manual */) . '</div>';
	}
?>

<?php if (strtolower($PRODUCT['name']) == 'dmdelivery') { ?>
<div class="button" onclick="openCommunity('http://portal.webpower.nl/manuals/dmdelivery/QuickReference_<?= $PHSes->lang ?>.pdf')">
	<?= word(282 /* QuickReference */) ?>
</div>

<?php if ($DMDprefs['licenseOwner'] == 'cn') { ?>
<div class="button" onclick="openCommunity('https://corporatecn.dmdelivery.com/videotour/',1030,660)">
	<?= word(1763 /* Videotour */) ?>
</div>
<?php } ?>
<?php } ?>

<?php
if (!empty($PHprefs['DMfactsUserid'])) {
	$url = 'http://login.dmfacts.nl/dmfacts.php?' . PH::WPencode('u=' . $PHprefs['DMfactsUserid'] . '&p=' . $PHprefs['DMfactsPasswd'] . '&i=' . $_SERVER['REMOTE_ADDR']);
?>
<div class="button" onclick="_W.open('<?= $url ?>','DMfactsWin')"><img src="<?= $PHprefs['customUrls']['pics'] ?>/dmfacts.gif" alt="" width="18" height="18" />&nbsp;DMfacts</div>
	<?php
}
?>
<?php
	// SU-users cannot change their password.
if (!isset($PHprefs['blockPwdUpd']) && !preg_match('/^su\./', $PHSes->loginName)) {
?>
<div class="button" onclick="parent.PHcontent.location='<?= $PHprefs['url'] ?>/core/changePwd.php'">
	<?= word(280 /* Password */) ?>
</div>
<?php } ?>

<div class="button" onclick="parent.PHcontent.location='<?= $PHprefs['url'] ?>/core/support.php?type=support'">
	<?= word(322 /* Support */) ?>
</div>

<div class="button" onclick="logout()">
	<?= word(281 /* Logout */) ?>
</div>

<div id="statusEl" style="float:right">
	<?= word(99 /* User */) ?>: <?= PH::htmlspecialchars($PHSes->userName) ?>
</div>

</body>
</html>
