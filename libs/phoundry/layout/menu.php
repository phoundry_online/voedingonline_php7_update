<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}

header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once "{$PHprefs['distDir']}/core/include/PhoundryMenu.class.php";

// Make sure the current user in the current role has access to this page:
checkAccess();

/*************************** Add/Delete shortcut **************************/

if (isset($_GET['_addShortcut']) || isset($_GET['_delShortcut'])) {
	// Add or remove shortcut:
	if (isset($_GET['_addShortcut'])) {
		$sql = "INSERT INTO phoundry_user_shortcut VALUES({$PHSes->userId}, " . (int)$_GET['_addShortcut'] . ")";
	}
	elseif (isset($_GET['_delShortcut'])) {
		$sql = "DELETE FROM phoundry_user_shortcut WHERE user_id = {$PHSes->userId} AND table_id = " . (int)$_GET['_delShortcut'];
	}
	$res = $db->Query($sql);
	// Do not catch error: shortcut might already be there!
}
/************************** /Add/Delete shortcut **************************/

$DMDcid = null;
if (isset($_GET['DMDcid'])) {
	if ($_GET['DMDcid'] === '') { $DMDcid = null; unset($_GET['DMDcid']); }
	else { $DMDcid = (int)$_GET['DMDcid']; }
}

$cur = $db->Query('DESC brickwork_site');
$prefix = $cur ? 'brickwork_' : '';
$sites = array();
$site_identifier = null;

$sql = sprintf('
	SELECT
		identifier,
		name
	FROM
		%1$ssite
	WHERE
		(SELECT count(*) FROM %1$ssite_phoundry_user WHERE phoundry_user_id = %2$d) = 0
	OR
		identifier IN (SELECT site_identifier FROM %1$ssite_phoundry_user WHERE phoundry_user_id = %2$d)
	ORDER BY name',
	$prefix, $PHSes->userId
);
$cur = $db->Query($sql);
if ($cur) {
	$site_identifier = isset($_GET['site_identifier']) ? $_GET['site_identifier'] : '';
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$sites[$db->FetchResult($cur,$x,'identifier')] = $db->FetchResult($cur,$x,'name');
	}
	if (count($sites) == 1) { $site_identifier = $_GET['site_identifier'] = key($sites); }
	$db->FreeResult($cur);
}

// TODO move DMdelivery haxors
if (class_exists('DMD')) {
	$background = '#fff';
	if (null !== $DMDcid) {
		// Reset Stats-specific session settings:
		unset($_SESSION['statsPerPage'], $_SESSION['statsDMDmid'], $_SESSION['statsDMDsmid'], $_SESSION['statsDMDsid'], $_SESSION['statsDMDabid']);
		$PHenv = DMD::getCampaignInfo($DMDcid);

		if ($DMDcid == 0) {
			$_SESSION['NRrecipient_egroup'] = $_SESSION['NRrecipient'] = 0;
		}
		else {
			// How many records in the recipient & recipient_egroup tables?
			$sql = "SELECT COUNT(*) FROM recipient_egroup_{$DMDcid}";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$_SESSION['NRrecipient_egroup'] = (int)$db->FetchResult($cur,0,0);
			$sql = $PHenv['overall'] ?
				"SELECT COUNT(*) FROM recipient_campaign_overall WHERE campaign_id = {$DMDcid}" :
				"SELECT COUNT(*) FROM recipient_{$DMDcid}";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$_SESSION['NRrecipient'] = (int)$db->FetchResult($cur,0,0);
		}
	}
}
$_show = isset($_GET['_show']) ? $_GET['_show'] : (!is_null($DMDprefs) && $DMDprefs['identifier'] == 'cendris' && $PHSes->userId == 1 ? 'recent' : 'all');

$pm = new PhoundryMenu($PHSes->lang);

$serviceMenus = $pm->readMenu('service', $PHSes->userId, $PHSes->groupId, $PHSes->isAdmin);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Menu</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/menu.css" type="text/css" />
<script type="text/javascript" src="../core/csjs/global.js.php"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.accordion.pack.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.treeview.pack.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.cookie.pack.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.uniform.min.js"></script>
<script type="text/javascript">
var _D = document, _W = window, _P = parent, curPath = '', DMDcid = <?= null === $DMDcid ? 'null' : $DMDcid ?>, loc = _P.frames['PHcontent'].location.href;

<?php
	if (!isset($_GET['init']) && (isset($_GET['setGroupId']) || null !== $DMDcid)) {
		print "_P.frames['PHcontent'].location.href = '{$PHprefs['customUrls']['home']}';\n";
	}
?>
<?php if (isset($_GET['site_identifier'])) { ?>
if (/site_identifier=([^&]+)/.test(loc)) {
	_P.frames['PHcontent'].location.href=loc.replace(/site_identifier=([^&]+)/, 'site_identifier=<?= rawurlencode($_GET['site_identifier']) ?>');
}
<?php } ?>

<?php if (class_exists('DMD') && null !== $DMDcid) { ?>
try {parent.frames['PHcontent'].checkPrepayBalance()} catch(e) {};
<?php } ?>

try {
   _D.execCommand('BackgroundImageCache', false, true);
} catch(e) {}

<?php if (class_exists('DMD')): ?>
function setMenu(rawPath, search) {
	if ($('#tab0').attr('class').indexOf('current') == -1) return;

	var p, params1 = search.substr(1).split('&'), params2 = [], rawPath = rawPath.replace('edit.php',''), path = rawPath, el, nrMatches, href, fldrID;
	for(p in params1) {
		if (/^TID=\d+/.test(params1[p]) || /^DMDcid=\d+/.test(params1[p]) || /^PHfltr_\d+=([^&$]*)/.test(params1[p])) {
			params2.push(params1[p]);
		}
	}
	if (params2.length > 0) path += '?' + params2.join('&');
	if (!accordion || path == curPath) return;

	$("#tab0Div li a[href*='"+rawPath+"']").each(function() {
		href = $(this).attr('href'); nrMatches = 0;
		for(p in params2) {
			if (new RegExp(params2[p]+'($|&)').test(href)) {
				nrMatches++;
			}
		}
		if (!/PHfltr_\d+=/.test(href) && /PHfltr_\d+=/.test(search)) { nrMatches++; }
		if (nrMatches == params2.length) {
			el = $(this).eq(0); return false;
		}
	});
	if (!el) return;

	fldrID = el.parents('.listMenu').parent().prev('a.head').attr('id');
	if ($('#' + fldrID).attr('class').indexOf('selected') == -1) {
		accordion.activate('#' + fldrID);
	}
	el.trigger('click');
	curPath = path;
}

function setCampaign(el) {
	var sel = $('.campaignSelect');
	sel.val($(el).val());
	var option = el.options[el.selectedIndex];
	if (option.title == 'inactive') {
		alert('<?= escSquote(word(1001 /* Inactive campaign */)) ?>');
	}
	el.form.submit();
}

function findCampaign(val) {
	var sel = $('.campaignSelect');
	if (sel.find("option[value='"+val+"']").length) {
		sel.val(val);
		setCampaign(sel[0]);
	}
}
<?php endif ?>

function setGroup(el) {
	var option = el.options[el.selectedIndex];
	$(el.form).append('<input type="hidden" name="setGroupId" value="'+option.value+'" />');
	el.form.submit();
}

function setGroupSelect(state) {
	$('#groupSelect').prop('disabled', !state ? 'disabled' : '');
}

var curTab = 0, accordion;
function setTab(tab) {
	$('#tab'+curTab+'Div').hide();
	$('#tab'+curTab).removeClass('current');
	$('[data-tab]').removeClass('current');
	curTab = tab;
	$('#tab'+curTab+'Div').show();
	$('#tab'+curTab).addClass('current');
	$('[data-tab="tab'+curTab+'"]').addClass('current');
}

$(function(){
	if ($('#tab10Div').length === 0) {
		$('#tab10').hide();
	}

	// Read cookie, determine current tab & pane:
	var menuStatus = $.cookie('menuStatus');
	if (!menuStatus) {
		menuStatus = 'tab=0|pane=0';
	}
	menuStatus = menuStatus.match(/tab=(-?\d+)\|pane=(-?\d+)/);
	curTab = parseInt(menuStatus[1],10);

	$('.listMenu').treeview({
		collapsed: true,
		unique: true
	}).removeClass('treeview');

	// Configure accordion(s) after the treeviews:
	active = parseInt(menuStatus[2],10);
	active = active == -1 ? 0 : active;

	var head = $('.head'), top = head.length == 0 ? 0 : head.eq(0).offset()['top'];
	accordion = $('.paneContainer').accordion({
		alwaysOpen:false,
      header: 'a.head',
      clearStyle: false,
      autoHeight: false,
      fillSpace: false,
      animated: 'slide',
      active: active
	});

	// Open all hyperlinks in the PHcontent frame:
	$('a[href]').prop('target','PHcontent');

	// Hide the non-active tabs. Do this after accordion initialisation, 
	// otherwise the accordion cannot determine the height of the panes!
	$('.paneContainer:not(:eq('+curTab+'))').hide();
	setTab(curTab);

	$(window).bind('unload', function(){
		var curPane = $('#tab'+curTab+'Div');
		curPane = $('a.head',curPane).index($('a.selected',curPane).get(0));
		$.cookie('menuStatus', 'tab='+curTab+'|pane='+curPane);
	});

	$('div > a, li > a').bind('click', function(e) {
		$('div.selected, li.selected').removeClass('selected');
		$(this).parent().addClass('selected');
	});

	// Remove loading overlay:
	$('#loading').remove();

	$('#menuToggle').click(function(){
		var fs = parent.document.getElementById('frames'),
			open = fs.className != 'menuclosed';
		fs.className = open ? 'menuclosed' : '';
		if (open) {
			setTimeout(function() {
				$('html').toggleClass('closed', true);
			}, 1000);
		} else {
			$('html').toggleClass('closed', false);
		}
	});

	$(window).bind('resize', function() {
		var height = $(this).height(),
			topOffset = $('.tabs-wrapper').offset().top + $('.tabs-wrapper').height();
		$('.paneContainers').css('height', ((height - topOffset) - $('#footerBar').outerHeight()) + 'px');
	}).trigger('resize');

	<?if (isset($_GET['_addShortcut'])):?>
	$('.shortcuts').fadeOut().fadeIn();
	<?endif?>

	<?php if (empty($_GET)) { ?>
	$('#campaignSelect')
	.fadeTo(350,0.11)
	.fadeTo(350,1)
	.fadeTo(350,0.11)
	.fadeTo(350,1);
	<?php } ?>

	$('span.manual[data-manual-url]').live('click', function(e) {
		openManual($(this).attr('data-manual-url'));
	});
});
</script>

<!--[if lt IE 9]>
<script>
$(document).ready(function() {
	$("select").each(function() {
		el = $(this);
		el.data("origWidth", el.outerWidth()) // IE 8 can haz padding
	}) .mousedown(function(){
		$(this).css("width", "auto");
   }).bind("blur change", function() {
		el = $(this);
		el.css("width", el.data("origWidth"));
	});
});
</script>
<![endif]-->

<?php if (isset($_GET['width'])):?>
<style>
body {
	width: <?php echo (int) $_GET['width']?>px;
}
.closed body {
	width: auto;
}
</style>
<?php endif?>
<?php if (!empty($DMDprefs['resellerLogo'])): ?>
<style>
#footerLogo {
	background-image: url("<?php echo $DMDprefs['resellerLogo']?>");
}
</style>
<?php endif ?>
</head>
<body>
<div id="footerLogo"></div>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="get">
<div class="userInfo">
	<div class="username item"><span><?php echo PH::htmlspecialchars(word(368, $PHSes->userName))?></span></div>
	<?php
	$support_url = $PHprefs['url']. '/core/support.php?type=support';
	if (isset($PHprefs['customUrls']['support'])) {
		$support_url = trim($PHprefs['customUrls']['support']);
	}
	if (!empty($support_url)) {?>
	<div id="settings" class="item"><span><a href="<?php echo $support_url?>" target="PHcontent"><?php echo word(322)?></a></span></div>
	<?php } ?>
	<div id="logout" class="item"><a href="<?=$PHprefs['url']?>/?logout=1"><?php echo word(370)?></a></div>
</div>

<div id="loading" style="width:100%;height:100%;position:absolute;top:0;left:0;background:#fff"></div>
<?php
	$serviceMenus = $pm->readMenu('service', $PHSes->userId, $PHSes->groupId, $PHSes->isAdmin);
	if (!isset($DMDprefs['wizard'])) {
		foreach($serviceMenus as $id=>$serviceMenu) {
			if ($serviceMenu->table_id == 40) {
				unset($serviceMenus[$id]); break;
			}
		}
	}
?>

<div class="tabs-wrapper">
<ul class="tabs">
<?php
	foreach($PHprefs['menuTabs'] as $tabID=>$tabLabel) {
		print '<li id="tab' . $tabID . '" class="tab-'.htmlspecialchars(strtolower($tabLabel)).'" title="' . strtolower(htmlspecialchars($tabLabel)) . '">';
		print '<a href="#" onclick="setTab(' . $tabID . ');this.blur();return false">' . PH::htmlspecialchars($tabLabel) . '</a>';
		print "</li>\n";
	}
?>
<li id="tab8" title="Service"><a href="#" onclick="setTab(8);this.blur();return false"><img class="icon" src="<?= $PHprefs['customUrls']['pics'] ?>/service.png" alt="Service" title="Service" /></a></li>
<?php if ($PHSes->isAdmin) { ?>
<li id="tab9" title="Admin"><a href="#" onclick="setTab(9);this.blur();return false"><img class="icon" src="<?= $PHprefs['customUrls']['pics'] ?>/admin.png" alt="Admin" title="Admin" /></a></li>
<?php } ?>
<li id="tab10" title="Custom Work"><a href="#" onclick="setTab(10);this.blur();return false">Custom Work</a></li>
</ul>
<div style="clear: both;"></div>
</div>

<div class="paneContainers">
<?php
$first = true;
$misc = false;
foreach($PHprefs['menuTabs'] as $tabID=>$tabLabel) {
	if (strtolower($tabLabel) == 'dmdelivery') {
		$menus = DMD::getDMdeliveryMenu($DMDcid, $tabID);
		list($misc, $menus) = DMD::splitMenuInMiscAndNotMisc($menus);
	} else {
		$menus = $pm->readMenu('tab' . $tabID, $PHSes->userId, $PHSes->groupId, $PHSes->isAdmin);
	}
	?>
	<!-- <?= $tabLabel ?> menu -->
	<div class="paneContainer" id="tab<?= $tabID ?>Div">
	<?php if (strtolower($tabLabel) == 'dmdelivery'): ?>
		<div class="shortcuts">
		<?php
			$shortcuts = $pm->getShortcuts($PHSes->userId, null !== $DMDcid && $DMDcid != 0 ? $DMDcid : null);
			if (count($shortcuts) > 0) {
				foreach($shortcuts as $name=>$url) {?>

					<a href="<?=htmlspecialchars($url)?>"><?=$pm->word($name)?></a>
				<?php
				}
			}
		?>
		</div>
		<?php	include 'elements/campaign_select.php'; ?>

		<?php if ($DMDprefs['identifier'] == 'cendris' && $PHSes->userId == 1): ?>
			<div>
				<small>
					<input id="show_recent_campaigns" type="radio" name="_show" value="recent" <?= ($_show == 'recent') ? 'checked="checked"' : '' ?> onclick="this.form.submit()" />
					<label for="show_recent_campaigns"><?= word(1592 /* Recent */) ?></label>
					<input id="show_all_campaigns" type="radio" name="_show" value="all" <?= ($_show == 'all') ? 'checked="checked"' : '' ?> onclick="this.form.submit()" />
					<label for="show_all_campaigns"><?= word(163 /* All */) ?></label>
				</small>
			</div>
		<?php endif ?>
	<?php endif ?>
	<?php if (strtolower($tabLabel) == 'phoundry' && count($sites) > 1 && !empty($menus)):?>
		<div class="selectWrapper">
			<select name="site_identifier" id="siteSelect" title="<?=word(290 /* Website */)?>" onchange="this.form.submit()">
				<option value=""><?=word(291 /* Select website */)?></option>
				<? foreach ($sites as $id => $name):
				$selected = $site_identifier == $id ? ' selected="selected"' : ''?>
				<option value="<?=$id?>" <?=$selected?>><?=htmlspecialchars($name)?></option>
				<? endforeach ?>
			</select>
		</div>
		<?php $shortcuts = $pm->getShortcuts($PHSes->userId);
		if (count($shortcuts)) { ?>
			<div class="shortcuts">
				<?foreach($shortcuts as $name => $url):?>
				<a href="<?=htmlspecialchars($url)?>"><?=$pm->word($name)?></a>
				<?endforeach?>
			</div>
		<?php } ?>
	<?php endif ?>


<?php
	if (strtolower($tabLabel) != 'phoundry' || (strtolower($tabLabel) == 'phoundry' && (is_null($site_identifier) || $site_identifier !== ''))) {
		foreach($menus as $idx => $menu) {
			if ($menu->depth == 1 && is_null($menu->table_id)) {
				print '<a href="#" class="head"><span>' . PH::htmlspecialchars($pm->word($menu->name)) . '</span></a>';
				print '<div>';
				if (isset($menus[$idx+1]) && $menus[$idx+1]->depth >= 2) {
					print '<ul class="listMenu" id="menu' . $tabID . '_' . $idx . '">';
					print $pm->getMenuTree($menus, $idx+1);
					print '</ul>';
				}
				else {
					print '<p style="margin:6px">[none]</p>';
				}
				print '</div>';
			}
		}
	}
?>
	<!-- /<?= $tabLabel ?> menu -->
</div>
<?php } /* foreach menuTabs */ ?>

<!-- Service menu -->
<div class="paneContainer" id="tab8Div">
<?php if (!empty($serviceMenus)) { ?>
<a href="#" class="head"><span>Service</span></a>
<div><ul class="listMenu" id="menu8_1">
<?php 
	$order = array();
	foreach($serviceMenus as $i => $menu) {
		$order[$i] = $pm->word($menu->name);
	}
	asort($order);
	foreach(array_keys($order) as $i) {
		$menu = $serviceMenus[$i];
		$name  = $pm->word($menu->name);
		print '<li><a href="' . PH::htmlspecialchars($menu->url) . '">' . PH::htmlspecialchars($pm->word($name)) . '</a></li>';
	}
?>
</ul>
</div>
<?php } ?>
<a class="head"><span><?php echo PH::htmlspecialchars(word(369))?></span></a>
<div>
    <ul class="listMenu" id="menu8_2">
		<?if (count($PHSes->groups) > 1):?>
		<li>
            <span>
                <select id="groupSelect" onchange="setGroup(this)" title="<?=word(52)?>">
                    <?foreach ($PHSes->groups as $id => $name):
                    $selected = ($PHSes->groupId == $id) ? ' selected="selected" ' : ' ';
                    ?>
                    <option <?=$selected?> value="<?=$id?>"><?=$name?></option>
                    <?endforeach?>
                </select>
            </span>
		</li>
		<?endif?>
		<li><a href="<?php echo $PHprefs['url']?>/core/changePwd.php" target="PHcontent"><?php echo word(371)?></a></li>
	</ul>
</div>
</div>
<!-- /Service menu -->

<?php if ($PHSes->isAdmin) { ?>
	<!-- Admin(Brickwork) menu -->
	<div class="paneContainer" id="tab9Div">
	<a href="#" class="head"><span>Admin</span></a>
	<div><ul class="listMenu" id="menu9_1">
	<?php 
		$menus = $pm->readMenu('admin', $PHSes->userId, $PHSes->groupId, $PHSes->isAdmin);
		$order = array();
		foreach($menus as $i => $menu) {
			$order[$i] = $pm->word($menu->name);
		}
		asort($order);
		foreach(array_keys($order) as $i) {
			$menu = $menus[$i];
			$name  = $pm->word($menu->name);
			print '<li><a href="' . PH::htmlspecialchars($menu->url) . '">' . PH::htmlspecialchars($pm->word($name)) . '</a></li>';
		}
	?>
	</ul>
	</div>

	<?php 
		$menus = $pm->readMenu('admin_brickwork', $PHSes->userId, $PHSes->groupId, $PHSes->isAdmin);
		if (!empty($menus)) {
	?>
	<a href="#" class="head"><span>Brickwork Admin</span></a>
	<div><ul class="listMenu" id="menu9_2">
	<?php
		$order = array();
		foreach($menus as $i => $menu) {
			$order[$i] = $pm->word($menu->name);
		}
		asort($order);
		foreach(array_keys($order) as $i) {
			$menu = $menus[$i];
			$name  = $pm->word($menu->name);
			print '<li><a href="' . PH::htmlspecialchars($menu->url) . '">' . PH::htmlspecialchars($pm->word($name)) . '</a></li>';
		}
	?>
	</ul>
	</div>
	<?php } ?>
	</div>
	<!-- /Admin(Brickwork) menu -->
<?php } ?>

<?php if ($misc) { ?>
<div class="paneContainer" id="tab10Div">
	<?php	include 'elements/campaign_select.php'; ?>
	<?php
	foreach($misc as $idx => $menu) {
		if ($menu->depth == 1 && is_null($menu->table_id)) {
			print '<a href="#" class="head"><span>' . PH::htmlspecialchars($pm->word($menu->name)) . '</span></a>';
			print '<div>';
			if (isset($misc[$idx+1]) && $misc[$idx+1]->depth >= 2) {
				print '<ul class="listMenu" id="menu' . $tabID . '_' . $idx . '">';
				print $pm->getMenuTree($misc, $idx+1);
				print '</ul>';
			}
			else {
				print '<p style="margin:6px">[none]</p>';
			}
			print '</div>';
		}
	}
	?>
</div>
<?php } ?>


</div>
</form>
<div id="footerBar">
	<a href="<?= $PHprefs['customUrls']['home'] ?>" target="PHcontent" class="ptr icon home" title="<?= word(275 /* Home */) ?>"></a>
	<?php
	global $PHprefs, $PRODUCT;
	if (is_array($PHprefs['manualUrl'])) {
		foreach($PHprefs['manualUrl'] as $name => $manualUrl) {
			print '<span class="ptr manual icon" title="' . $name . ' ' . word(277) . '" data-manual-url="' . PH::htmlspecialchars($manualUrl) . '"></span>';
		}
	} else if (!empty($PHprefs['manualUrl'])) {
		print '<span class="ptr manual icon" title="' . word(277) . '" data-manual-url="' . PH::htmlspecialchars($PHprefs['manualUrl']) . '"></span>';
	}
	?>
	<?php if (strtolower($PRODUCT['name']) == 'dmdelivery'): ?>
	
		<?php if (!empty($PHprefs['customUrls']['quickReference'])): ?>
		<?php $quickRefUrl = sprintf($PHprefs['customUrls']['quickReference'], $PHSes->lang); ?>
		<span class="icon ptr quick-reference" title="<?= word(282 /* QuickReference */) ?>" onclick="openCommunity('<?=$quickRefUrl?>')">
		</span>
		<?php endif; ?>

		<?php if (!empty($PHprefs['customUrls']['videoTour'])): ?>
		<span class="icon ptr video-tour" title="<?= word(1763 /* Videotour */) ?>" onclick="openCommunity('<?=$PHprefs['customUrls']['videoTour']?>', 1030, 660)">
		</span>
		<?php endif; ?>
	
		<?php if ($PHSes->userId == 1): ?>
		<input class="WPonly" style="width: 20px;" type="text" onchange="parent.PHmenu.findCampaign(this.value);this.value=''" title="Enter campaign ID" />
		<?php endif; ?>
	
	<?php endif; ?>
	
</div>
</body>
</html>
