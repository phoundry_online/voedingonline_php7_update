<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	checkAccess();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="home" />
<title><?= $PRODUCT['name'] ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<style type="text/css">
.win-wrapper {
	float: left;
	width: 50%;
}

.win-wrapper:nth-child(even) .win {
	margin-left: 9px;
}

.win-wrapper:nth-child(odd) .win {
	margin-right: 9px;
}

.winTitle {
	font-size: 20px;
	text-transform: lowercase;
	line-height: 32px;
	color: #6E6F71;
}

.winContain {
	margin: 0;
	border-top: 2px solid #BCCF3F;
	border-bottom: 6px solid #BCCF3F;
}

.winContent {
	overflow: auto;
	position: relative;
	border-left: 1px solid #AEAFB3;
	border-right: 1px solid #AEAFB3;
	height: 196px;
	padding: 10px;
}

#prepay_expiry_container {
	position:absolute;
	top:64px;
	right:74px;
	font-size:14px;
	font-weight:bold;
	color:#56267D;
	z-index:99;
}

</style>
<script type="text/javascript" src="../core/csjs/global.js.php"></script>
<script type="text/javascript" src="../core/csjs/jquery.cookie.pack.js"></script>
<script type="text/javascript">

var curWin, cookieName = 'PHhome2';

Array.prototype.______array = '______array';

var JSON = {
	stringify: function (arg) {
		var c, i, l, s = '', v;

		switch (typeof arg) {
		case 'object':
			if (arg) {
				if (arg.______array == '______array') {
					for (i = 0; i < arg.length; ++i) {
						v = this.stringify(arg[i]);
						if (s) {
							s += ',';
						}
						s += v;
					}
					return '[' + s + ']';
				} else if (typeof arg.toString != 'undefined') {
					for (i in arg) {
						v = arg[i];
						if (typeof v != 'undefined' && typeof v != 'function') {
							v = this.stringify(v);
							if (s) {
								s += ',';
							}
							s += this.stringify(i) + ':' + v;
						}
					}
					return '{' + s + '}';
				}
			}
			return 'null';
		case 'number':
			return isFinite(arg) ? String(arg) : 'null';
		case 'string':
			l = arg.length;
			s = '"';
			for (i = 0; i < l; i += 1) {
				c = arg.charAt(i);
				if (c >= ' ') {
					if (c == '\\' || c == '"') {
						s += '\\';
					}
					s += c;
				} else {
					switch (c) {
						case '\b':
							s += '\\b';
							break;
						case '\f':
							s += '\\f';
							break;
						case '\n':
							s += '\\n';
							break;
						case '\r':
							s += '\\r';
							break;
						case '\t':
							s += '\\t';
							break;
						default:
							c = c.charCodeAt();
							s += '\\u00' + Math.floor(c / 16).toString(16) +
								(c % 16).toString(16);
					}
				}
			}
			return s + '"';
		case 'boolean':
			return String(arg);
		default:
			return 'null';
		}
	}
};

var WINs = {
	// x: x-position of window
	// y: y-position of window
	// z: z-index of window
	// v: visible (1) or not (0)
	struct: {
		<?php 
			if (isset($PHprefs['widgets'])) {
			$widget_js_str = '';
			foreach ($PHprefs['widgets'] as $id => $widget_content) 
			{ 
				$widget_js_str .= "{$id}:{{$widget_content['start_position']},z:999,v:1},";
			}
			print $widget_js_str = rtrim($widget_js_str, ',')."\n";
			}
		?>
	},

	get:function(name, key) {
		return this.struct[name][key];
	},

	set:function(name, key, val) {
		this.struct[name][key] = val;
		return this;
	},

	load:function() {
		var cv = $.cookie(cookieName);
		if (cv) eval('this.struct = '+cv);
		return this;
	},

	save:function() {
		$.cookie(cookieName, JSON.stringify(this.struct), {expires:365});
		return this;
	}
};

function getWinBut(id, src, alt) {
	return '<img class="winBut" src="../core/icons/' + src + '" alt="' + alt + '" title="' + alt + '" onclick="showWindow(this,\''+id+'\');" />';
}

function showWindow(button,id) {
	var $win = $('#'+id), $button = $(button), l = $button.offset()['left'];
	$button.remove();

	$('<div style="position:absolute;width:20px;height:20px;left:'+l+';top:0;background:#eee;border:1px dotted #cfcfcf;z-index:99999"></div>').appendTo('#contentFrame').animate({top:$win.css('top'),left:$win.css('left'),width:$win.css('width'),height:$win.height()},500,'linear',function(){
		$(this).remove();
		$win.show();
		WINs.set(id,'v',1).save();
	});
}

$(function(){
	var iconEl = $('#windowIcons'),
		icons = {'pn':['newspaper.png','Phoundry nieuws'],'ga1':['report.png','Google Analytics'],'ga2':['report.png','Google Analytics']};
	WINs.load();

	$('.win')
	.each(function(){
		var $win = $(this), id = $win.attr('id'), z = parseInt(WINs.get(id,'z'),10);
		if (z > dragZ) dragZ = z+1;
		$win.css({left:WINs.get(id,'x')+'px',top:WINs.get(id,'y')+'px',zIndex:z});
		if(WINs.get(id,'v') == 1) {
			$win.show();
		}
	})
	.find('.winTitle')
	.prop('unselectable','on')
	.bind('mousedown', function(e){
		curWin = $(this).parent();
		dragX = e.clientX-parseInt(curWin.css('left'),10);
		dragY = e.clientY-parseInt(curWin.css('top'),10);
		curWin.css({cursor:'move',zIndex:dragZ++});
		$(document).bind('mousemove',function(e){
			var x = e.clientX-dragX, y = e.clientY-dragY;
			if (y < 0) y = 0;
			curWin.css({left:x+'px',top:y+'px'});
		});
	})
	.bind('mouseup',function(e){
		$(document).unbind('mousemove');
		if(curWin) curWin.css('cursor','');
		curWin = null;
		var $win = $(this).parent(), id = $win.attr('id');
		WINs.set(id,'x',parseInt($win.css('left'),10)).set(id,'y',parseInt($win.css('top'),10)).set(id,'z',$win.css('zIndex')).save();
	})
	.find('.closeBut')
	.bind('mouseover', function() {
		this.style.backgroundPosition='0 -16px'
	})
	.bind('mouseout', function() {
		this.style.backgroundPosition='0 0'
	})
	.bind('click', function(e){
		curWin = $(this).parent().parent();
		var w = curWin.css('width'), h = curWin.height()+'px', l = curWin.css('left'), t = curWin.css('top');
		curWin.hide();
		$('<div style="position:absolute;width:'+w+';height:'+h+';left:'+l+';top:'+t+';background:#eee;border:1px dotted #cfcfcf;z-index:99999"></div>').appendTo('#contentFrame').animate({top:'-20px',left:($('img',iconEl).length*20)+'px',width:0,height:0},500,'linear',function(){
			$(this).remove();
			var id = curWin.attr('id');
			WINs.set(id, 'v', 0).save();
			$('#windowIcons').append(getWinBut(id, icons[id][0], icons[id][1]));
		});
	});

	for(var i in icons) {
		if ($('#'+i).css('display') == 'none') {
			iconEl.append(getWinBut(i, icons[i][0], icons[i][1]));
		}
	}

	try {_P.frames['PHmenu'].setGroupSelect(true)} catch(e) {};

});

$(function(){
	try {_P.frames['PHmenu'].setGroupSelect(true)} catch(e) {};
	
<?php
	if (isset($_GET['_msg']) && !isset($_GET['groupId'])) {
		print 'alert(' . json_encode(word((int)$_GET['_msg'])) . ");\n";
	}
?>
});

</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?php
	print getHeader(isset($PHprefs['product']) ? $PHprefs['product'] : 'Phoundry');
?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td style="padding-left: 10px;"><?=$PRODUCT['name']?> <?= $PRODUCT['version'] ?><?php /* - <?= $PRODUCT['date']  ?>*/?></td>
<td align="right">
<?php include $PHprefs['distDir'] .'/layout/elements/manual_button.php' ?>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame" style="overflow:hidden">
<!-- Content -->

<?php
	if (isset($PHprefs['widgets']) && is_array($PHprefs['widgets'])) {
	foreach ($PHprefs['widgets'] as $widget_id => $widget)
	{
?>
	<div class="win-wrapper">
	<div id="<?= $widget_id; ?>" class="win">
		<div class="winTitle"><?php
			if (!empty($widget['title'])) {
				if (is_numeric($widget['title'])) {
					echo word((int) $widget['title']);
				} else {
					echo $widget['title'];
				}
			}
?></div>
		<div class="winContain"><div class="winContent" style="padding:0;overflow:auto;position:relative;background-color:#FFFFFF;">

		</div>
		</div>
	</div>
	</div>

	<script type="text/javascript">

	$(function() {
		$('div#<?= $widget_id; ?> div.winContent')
			.css({
				'background-image':'url(<?= $PHprefs['url'] ?>/core/icons/loading_icon.gif)',
				'background-repeat':'no-repeat',
				'background-position':'170px 92px'
			})
			.load('<?= $PHprefs['url']  ?>/core/widgets/<?= $widget['location'] ?>?widget_id=<?= $widget_id ?>&_rnd='+Math.random(),
				function () {
					$(this).css('background-image','none');
				}
			);
	});

	</script>

<?php
	}}
?>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	&copy; 2000 - <?= date('Y') ?> by <a href="http://www.phoundry.nl" target="_blank">phoundry</a>
	</td>
	<td align="right">
	</td>
</tr></table>
</div>

</body>
</html>
