		<div class="selectWrapper">
		<select class="campaignSelect" name="DMDcid" onchange="setCampaign(this)" title="<?= word(1000 /* Campaign */) ?>">
			<option value="">[<?= word(1156 /* Select campaign */) ?>]</option>
			<?php
			$sql = "SELECT overall FROM phoundry_user WHERE id = {$PHSes->userId}";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			// Does current user have access to Overall campaigns?
			$accessOverall = ($db->FetchResult($cur,0,'overall') == 1);
			if ($accessOverall) {
				$selected = (isset($_GET['DMDcid']) && $_GET['DMDcid'] === '0') ? ' selected="selected" ' : ' ';
				print '<option title="active" ' . $selected . 'value="0" style="background-color:black;color:white">-- ' . word(1002) . ' --</option>';
			}
			print DMD::getCampaignOptions($PHSes->userId, isset($_GET['DMDcid']) ? $_GET['DMDcid'] : -1, $_show);
			?>
		</select>
		</div>