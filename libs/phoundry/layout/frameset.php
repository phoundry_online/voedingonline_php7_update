<?php
/*
Available variables
$url, $PHprefs, $DMDprefs, $PRODUCT, $PHSes;
 */
$head = '<meta http-equiv="X-UA-Compatible" content="IE=edge" >';
$head .= '<meta name="apple-mobile-web-app-capable" content="yes">';
if (isset($DMDprefs)) {
	$head .= '<link rel="apple-touch-icon" href="/x/pics/dmd-icon.png" />';
	if ($DMDprefs['enableRSS']) {
		$domain = !empty($DMDprefs['localDomain']) ? $DMDprefs['localDomain'] : $DMDprefs['domain'];
		$head .= '<link rel="alternate" type="application/rss+xml" title="RSS all sent mailings" href="http://' . $domain . '/x/rss/?show=all" />' . "\n";
		$head .= '<link rel="alternate" type="application/rss+xml" title="RSS test mailings" href="http://' . $domain . '/x/rss/?show=test" />' . "\n";
		$head .= '<link rel="alternate" type="application/rss+xml" title="RSS final mailings" href="http://' . $domain . '/x/rss/?show=final" />' . "\n";
	}
}

$menuWidth = isset($PHprefs['menuWidth']) ? $PHprefs['menuWidth'] : 190;
$menuclosed = '';
if (preg_match('/(&|\?)closeMenu/', $url)) {
	$menuclosed = 'menuclosed';
}

$menuUrl = $PHprefs['customUrls']['menu'] . '?init=true&width='.$menuWidth;

if (preg_match('/DMDcid=(\d+)/', $url, $reg)) {
	$menuUrl .= '&amp;DMDcid=' . $reg[1];
}

$sessionAlive = "setInterval('sessionAlive()', 900000); // Refresh session every 5 mins";
if (isset($PHprefs['disableSessionKeepAlive']) && $PHprefs['disableSessionKeepAlive'] == true) {
	$sessionAlive = '';
}
if (isset($DMDprefs['extraSecure']) && $DMDprefs['extraSecure']) {
	$sessionAlive = '';
}

$title = isset($PHprefs['title']) ? $PHprefs['title'] : $PRODUCT['name'];
?>
<!DOCTYPE html>
<html>
<head>
<?php echo $head?>
<meta name="robots" content="noarchive,nofollow,noindex" />
<meta http-equiv="content-type" content="text/html; charset=<?php echo $PHprefs['charset']?>" />
<link rel="shortcut icon" href="<?php echo $PHprefs['customUrls']['pics']?>/favicon.ico" type="image/x-icon" />
<title><?php echo $title?></title>
<script type="text/javascript">

function ajax(url) {
	var http = false;
	if(navigator.appName == 'Microsoft Internet Explorer') {
		http = new ActiveXObject('Microsoft.XMLHTTP');
	} else {
		http = new XMLHttpRequest();
	}
	if (http) {
		http.open('GET', url);
		http.send(null);
	}
}

function doLock(lock, qs) {
	lock = lock ? 1 : 0;
	ajax('<?php echo $PHprefs['url']?>/core/doLock.php?'+qs+'&lock='+lock);
}

function sessionAlive() {
	ajax('<?php echo $PHprefs['url']?>/core/sessionAlive.php?'+Math.random());
}

function syncMenu(url) {
	try {
		var loc = window.frames['PHcontent'].location;
		window.frames['PHmenu'].setMenu(loc.pathname,loc.search);
	} catch(e) {}
}

<?php echo $sessionAlive?>

</script>
<style type="text/css">
html, body {
	position: relative;
	overflow:auto;
	padding: 0;
	margin: 0;
	width: 100%;
	height: 100%;
}

iframe {
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	border: 0;
	overflow: auto;
}

#topmenu {
	position: absolute;
	top: 0;
	lef: 0;
	width: 0;
	height: 0;
	overflow: hidden;
}

#menu {
	position: absolute;
	top: 0;
	left: 0;
	width: <?php echo $menuWidth?>px;
	bottom: 0;

	-webkit-transition: width 1s ease-in-out;
	-moz-transition: width 1s ease-in-out;
	-o-transition: width 1s ease-in-out;
	-ms-transition: width 1s ease-in-out;
	transition: width 1s ease-in-out;
}

#content {
	position: absolute;
	top: 0;
	right: 0;
	left: <?php echo $menuWidth?>px;
	bottom: 0;
	-webkit-transition: left 1s ease-in-out;
	-moz-transition: left 1s ease-in-out;
	-o-transition: left 1s ease-in-out;
	-ms-transition: left 1s ease-in-out;
	transition: left 1s ease-in-out;
}

.menuclosed #menu {
	/*width: 0;*/
}

.menuclosed #content {
	left: 0;
}
</style>
</head>
<?php if ($PHSes->kind == 'wizard'):?>
<frameset rows="*" frameborder="0" framespacing="0" border="0" id="topFS">
    <frame name="PHcontent" src="<?php echo $PHprefs['url']?>/dmdelivery/wiz_wizard/?TID=41" scrolling="auto" />
</frameset>
<?php else: ?>
<body>
<div id="topmenu"><iframe name="PHtopmenu" src="<?php echo $PHprefs['customUrls']['topmenu']?>" frameborder="0" scrolling="no"></iframe></div>
<div id="frames" class="<?php echo $menuclosed?>">
	<div id="menu"><iframe name="PHmenu" src="<?php echo $menuUrl?>" frameborder="0" scrolling="no"></iframe></div>
	<div id="content"><iframe name="PHcontent" src="<?php echo PH::htmlspecialchars($url)?>" onload="syncMenu(this.src)" frameborder="0" scrolling="auto"></iframe></div>
</div>
</body>
<?php endif ?>
</html>
