<?php
global $PHprefs;

	if (!isset($PHprefs)) {
		error_log('PHprefs not defined in ' . __FILE__);
		exit();
	}
	$errors = array();
	if (!isset($PHprefs['checkConfig']) || $PHprefs['checkConfig'] == true) {
        //
        // Any configuration errors?
        //

		// Make sure all required modules/extensions are available in this
		// PHP config:
		$exts = get_loaded_extensions();
		// xml:            for parsing the default plugins
		// json:				 for JSON
		// mysql:          for database communication
		// gd:             for statistics (pie-images)
		// pspell:         for Spelling Checks
		// imap:           for imap_8bit (quoted printable encoding)
		$needed  = array('xml', 'standard', 'pcre', 'zlib', 'gd');
		foreach($needed as $need) {
			if (!in_array($need, $exts)) {
				$errors[] = 'PHP module ' . $need . ' is missing!';
			}
		}
		if (!function_exists('shell_exec')) {
			$errors[] = "Function 'shell_exec' is disabled in this PHP-config!";
		}
		if (!empty($pageVars['message'])) {
			$errors[] = $pageVars['message'];
		}
	}

	$pwdUserID = false;
	if (!empty($_GET['resetPwd']))
	{
		// Make sure we can decode the resetPwd argument:
		$dec = PH::WPdecode(trim($_GET['resetPwd']));
		// Format: lang_userID_timestamp_passwordHash
		if (false === $dec || !preg_match('/^(\w{2})_(\d+)_(\d+)_(.+)$/', $dec, $reg))
		{
			$errors[] = 'The reset password link is invalid (corrupt).';
		}
		else {
			$pwdUserID = $reg[2];
		}
	}

PH::getHeader('text/html');
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en-us" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta name="robots" content="noindex,nofollow,noarchive" />
<link rel="shortcut icon" href="<?= $pageVars['favicon'] ?>" type="image/x-icon" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= $pageVars['title'] ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/login.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.inlineWindow.min.js"></script>
<script type="text/javascript">

if (self != top) {
	top.location.href = self.location.href;
}

var errors = <?= json_encode($errors) ?>;

function killPopup()
{
	if (!window.popup) return false;
	window.popup.close();
}

function forgotPassword(e,url)
{
	var el = $("select[name='lang']"), lang = (el.length > 0) ? $("select[name='lang']").val() : $(":hidden[name='lang']").val();
	window.popup = $.fn.inlineWindow({
		windowTitle : <?= json_encode(word(519 /* Forgot password ? */)) ?>,
		url : url+'?lang='+lang,
		width : 300,
		height : 150,
		event : e,
		modal : false
	});
}

function checkForm(F)
{
	var fields = ['username', 'password', 'sms_login_code'], okay = true, f, field;
	for (f = 0; f < fields.length; f++) {
		field = $('#'+fields[f]+':visible');
		if (field.length > 0) {
			if ($.trim(field.val()) == '') {
				field.addClass('formError').focus();
				okay = false;
			}
			else if (fields[f] == 'sms_login_code' && !/^\d{3}-\d{3}-\d{3}$/.test(field.val())) {
				alert('Invalid SMS login token. Did you enter the dashes?');
				okay = false;
			}
			else {
				field.removeClass('formError');
			}
		}
	}
	return okay;
}

$(function(){
	var F = document.forms[0], len = errors.length;
	$(':input').prop('disabled', false); // Make sure all form elements are enabled

	// Show upgrade warning to IE6 users:
	if (document.all && (/msie (6|7)./i).test(navigator.appVersion) && window.ActiveXObject) {
		$('#oldIE').fadeIn().fadeOut().fadeIn().fadeOut().fadeIn();
	}

	<?php list($_name, $_token) = PH::startCSRF(); ?>
	CSRF.insert('<?= $_name ?>', '<?= $_token ?>');

	if (len > 0)
	{
		var html = '';
		if (len > 1) html += '<ul>';
		for (var x = 0; x < errors.length; x++)
		{
			html += (len > 1) ? '<li>' + errors[x] + '</li>' : errors[x];
		}
		if (len > 1) html += '</ul>';
		$('#errors').html(html).css('opacity',0.85).fadeIn('slow').bind('click', function(){$(this).fadeOut('slow');});
	}
	$('#js').show();
	<?= $pageVars['onload'] ?>
	$("input[type='text']:first").focus();

	<?php if (false !== $pwdUserID) { ?>
	window.popup = $.fn.inlineWindow({
		windowTitle : 'Reset password',
		url : '<?= $PHprefs['url'] ?>/core/popups/resetPassword.php?<?= QS(0) ?>',
		width : 400,
		height : 250,
		event : null,
		modal : false
	});
	<?php } ?>
});

</script>
<?php
$isReseller = isset($PHprefs['product']) && strtolower($PHprefs['product']) == 'dmdelivery' && !empty($DMDprefs['resellerLogo']);
if ($isReseller):?>
<style>
#logo {
	background-image: url(<?php echo $DMDprefs['resellerLogo']?>);
}
</style>
<?php endif ?>
</head>
<body id="contentFrame" class="<?php $bgs = array('shanghai', 'shanghai2', 'amsterdam', 'stockholm'); echo $bgs[array_rand($bgs)]; unset($bgs);?>">
<?php if (!$isReseller):?>
<div id="footer">
<a id="same-logo" href="http://www.emailexperience.org/eec-projects/member-roundtables/support-adoption-of-metrics-for-email-project"></a>
<div>
	<div><?= $pageVars['product'] . ' ' . $pageVars['version'] ?> |
	<?php include('_copyright.php'); ?></div>
	<div><?= isset($_SERVER['REMOTE_ADDR']) ? ' IP: ' . PH::getRemoteAddr() : '' ?></div>
</div>
<!--<a id="phoundry-logo" href="<?=$phoundry_url?>"></a>-->
</div>
<?php endif ?>
<form action="<?= $PHprefs['url'] ?>/" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="signature" value="<?= $pageVars['signature'] ?>" />
<input type="hidden" name="lang" value="<?= $PHprefs['languages'][0] ?>" />
<?php if (isset($_REQUEST['_page'])) { ?>
<input type="hidden" name="_page" value="<?= PH::htmlspecialchars($_REQUEST['_page']) ?>" />
<?php } ?>
<table class="kader-outer">
<tr>
<td class="kader">
	<div class="inner">
		<div class="inner-inner">
			<div id="logo"><?=$pageVars['product']?></div>
			<noscript>
			<div style="background:#eee;color:red;font-weight:bold;padding:3px">
		   <?php print(word(437, $pageVars['product'])) // Enable javascript ?></div>
			</noscript>

			<div id="js" class="inner-inner-inner" style="display: none;">
				<div id="oldIE" style="display: none;">
				<fieldset>
				<?php print word(438, 'http://www.microsoft.com/Windows/internet-explorer/' /* Upgrade IE6 */) ?>
				</fieldset>
				</div>

				<?if ($errors):?>
				<div class="errors">
				<fieldset class="field">
					<?foreach ($errors as $error):?>
					<div><?=$error?></div>
					<?endforeach?>
				</fieldset>
				</div>
				<?endif?>


				<div class="fieldsets-wrapper-fields">

					<div class="fieldset-wrapper fieldset-wrapper-req">
					<fieldset class="field">
					<legend><b class="req"><?php print word(517) // Username ?></b></legend>
					<input type="text" id="username" name="username" maxlength="80" value="<?= PH::htmlspecialchars($pageVars['username']) ?>" />
					</fieldset>
					</div>

					<div class="fieldset-wrapper fieldset-wrapper-req">
					<fieldset class="field">
                    <a href="<?= $PHprefs['url'] ?>/core/popups/forgotPassword.php" onclick="forgotPassword(event,this.href);return false" class="pw_reminder" tabindex="1"><?php print word(519) // forgot password ?></a>
					<legend><b class="req"><?php print word(518) // Password ?></b></legend>
					<?php $autoComplete = (isset($DMDprefs['extraSecure']) && $DMDprefs['extraSecure']) ? ' autocomplete="off" ' : ''; ?>
					<input type="password" id="password" name="password" maxlength="80" <?=$autoComplete?> />
					</fieldset>
					</div>

					<div class="fieldset-wrapper fieldset-wrapper-req">
					<fieldset id="sms" style="display:none" class="field">
					<legend><b class="req">SMS login token</b></legend>
					<input type="text" id="sms_login_code" name="sms_login_code" maxlength="11" />
					</fieldset>
					</div>

					<div class="fieldset-wrapper fieldset-wrapper-req">
					<fieldset id="otp" style="display:none" class="field">
					<legend><b class="req">OTP login token</b></legend>
					<input type="text" id="otp_login_code" name="otp_login_code" maxlength="11" />
					</fieldset>
					</div>

					<div class="fieldset-wrapper">
					<?php if (count($PHprefs['languages']) > 1) {
						print '<fieldset>';
						print '<legend><b class="req">' . word(434) /* Language */ . '</b></legend>';
						print '<select name="lang">';
						foreach($PHprefs['languages'] as $lang) {
							$seld = ($pageVars['lang'] == $lang) ? ' selected="selected" ' : ' ';
							print '<option' . $seld . 'value="' . $lang . '">' . $pageVars['langs'][$lang] . '</option>';
						}
						print '</select></fieldset>';
					} ?>
					</div>

					<div class="buttons">
                        <label class="skinned-selcheck" for="remember_me"><input id="remember_me" type="checkbox" name="remember_me" value="1"<?= !empty($pageVars['username']) ? ' checked="checked"' : '' ?> /><span><?php print word(435) // Remember me ?></span></label>
						<input class="btn" id="submitBut" type="submit" value="<?php print word(436) /* Log in */ ?>">
						<div style="clear: both;"></div>
					</div>

				</div>

			</div>
		</div>
	</div>
</td>
</tr>
</table>


</form>
</body>
</html>
