<?php
	$phoundry_url = 'http://www.phoundry.nl/';
?>
<?php if (!isset($PHprefs['customUrls']['copyright'])): ?>
	Copyright &copy; 2000 - <?= date('Y') ?> by <a href="<?=$phoundry_url?>" target="_blank">phoundry</a>
<?php elseif (!empty($PHprefs['customUrls']['copyright'])):  ?>
	<?=$PHprefs['customUrls']['copyright']?>
<?php endif; ?>
