<?php
   $inc = @include_once('PREFS.php');
   if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
   }
	header("Content-type: text/html; charset={$PHprefs['charset']}");
   require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);

	$CID = (int)$_GET['CID'];
	$RID = $_GET['RID'];

	$found = false;
	foreach($phoundry->cols as $col) {
		if ($col->id == $_GET['CID']) {
			$found = true;
			break;
		}
	}
	if (!$found) {
		die("Column {$_GET['CID']} does not exist!");
	}

	if (isset($_GET['value'])) {
		// Store new value:
		$phoundry->doEvent('pre-update', $RID, $RID);
		$sql = "UPDATE {$phoundry->name} SET {$col->name} = '" . escDBquote($_GET['value']) . "' WHERE {$phoundry->key} = '" . escDBquote($RID) . "'";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$phoundry->doEvent('post-update', $RID, $RID);
	}

	// Fetch current value from DB:
	$sql = "SELECT {$col->name} FROM {$phoundry->name} WHERE {$phoundry->key} = '" . escDBquote($RID) . "'";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$value = $GLOBALS['RECORD_ID'] =  $db->FetchResult($cur,0,0);

	if (isset($_GET['value'])) {
		// Print new value:
		print $col->inputtype->getValue('search', $value);
	}
	else {
		$html = $col->inputtype->getInputHtml('view', $value, $includes);
		$html = preg_replace('/<select([^>]+)>/', '<select id="cid' . $CID . '" onchange="setInlineSelectValue(this)">', $html);
		print $html;exit;

		// Fire query:
		$cur = $db->Query($editQuery)
			or trigger_error("Query $editQuery failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$selected = ($db->FetchResult($cur,$x,$extra['optionvalue']) == $value) ? ' selected="selected"' : '';
			$html .= '<option value="' . PH::htmlspecialchars($db->FetchResult($cur,$x,$extra['optionvalue'])) . '"' . $selected . '>';
			foreach($extra['optiontext'] as $text) {
				$html .= PH::htmlspecialchars(PH::langWord($db->FetchResult($cur,$x,$text))) . ' ';
			}
			$html .= "</option>\n";
		}
		$html .= '</select>';

		print $html;
	}
?>
