<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'insert');
	$RID = $_GET['RID'];

	// Instantiate Phoundry opbject:
   $phoundry = new Phoundry($TID);
	$pageTitle = $phoundry->getPageHeader('copy', word(109));

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="<?= preg_replace('/_\d+$/', '', $phoundry->name) ?>.copy" />
<title><?= $pageTitle['title'] ?></title>
<?php
	// Assemble page contents:
	$page = $phoundry->copyPage($RID);
?>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<?php
	foreach($page['cssFiles'] as $cssFile)
		print '<link rel="stylesheet" href="' . $cssFile . '" type="text/css" />' . "\n";
?>
<script type="text/javascript" src="csjs/global.js.php"></script>
<?php
	foreach($page['jsFiles'] as $jsFile)
		print '<script type="text/javascript" src="' . $jsFile . '"></script>' . "\n";
?>
<script type="text/javascript">

var TID = <?= $TID ?>, RID = '<?= escSquote($RID) ?>';
<?php
	foreach($page['jsCode'] as $jsCode) {
		print $jsCode . "\n";
	}
?>

// Start javascript syntax checks
<?= $phoundry->getJSchecks('copy'); ?>
// End javascript syntax checks

$(function() {
	try {parent.frames['PHmenu'].setGroupSelect(false)} catch(e) {}
	var F = $('#inputForm');

	$("span[id$='_cOuNtEr']").each(function() {
		showCharCount(F.find('#'+this.id.replace(/_cOuNtEr$/, ''))[0]);
	});

	F
	.bind('keypress', function(){formIsDirty=true})
	.bind('submit', function(){return checkForm(this)})
	.find(":input[type!='button']:visible:enabled:first").focus()
});

</script>
</head>
<body class="frames" onbeforeunload="if(formIsDirty)return '<?= escSquote(word(79)) /* Form data changed without submission */ ?>'">
<form id="inputForm" action="<?=$PHprefs['url']?>/core/copyRes.php?<?= QS(1) ?>" method="post" enctype="multipart/form-data">
<div id="headerFrame" class="headerFrame">
<?= getHeader($pageTitle['title'], $pageTitle['extra'], $pageTitle['filters']); 
?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b><?= word(32) ?></b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?= $page['html'] ?>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<?if (!empty($phoundry->extra['versioning'])):?>
	<?if (isset($_GET['_versioning']['parent_id'])):?>
	<input name="_versioning[parent_id]" type="hidden" value="<?=htmlspecialchars($_GET['_versioning']['parent_id'])?>" />
	<?endif?>
	<?if ($PHSes->isAdmin || PH::getAccessRights($TID, array('b'))):?>
	<input name="_versioning[publish]" type="submit" class="okBut" value="Publiceren" />
	<?endif?>
	<input name="_versioning[ready]" type="submit" class="okBut" value="Klaarzetten" title="Klaarzetten voor publicatie" />
	<input name="_versioning[save]" type="submit" class="okBut" value="Opslaan" title="Opslaan maar nog niet klaar voor publicatie" />
	<?else:?>
	<input type="submit" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>" />
	<?endif?>
	</td>
	<td align="right">
	<?php include 'cancelBut.php';?>
	</td>
</tr></table>
</div>

</form>
</body>
</html>
