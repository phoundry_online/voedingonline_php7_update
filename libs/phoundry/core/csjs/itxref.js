function MSmove(el,up) {
	var idx = el.selectedIndex, nxidx = idx+(up ? -1 : 1);
	if (idx == -1) return;
	if (nxidx<0) nxidx=el.length-1;
	if (nxidx>=el.length) nxidx=0;
	var oldVal = el[idx].value, oldText = el[idx].text;
	el[idx].value = el[nxidx].value;
	el[idx].text  = el[nxidx].text;
	el[nxidx].value = oldVal;
	el[nxidx].text  = oldText;
	el.selectedIndex = nxidx;
	MSupdate(el);
}
function MSupdate(el) {
	if (!el) el = callbackEl;
	var value = '';
	for (x = 0; x < el.options.length; x++) {
		if (x > 0) value += '|';
		value += el.options[x].value;
	}
	document.forms[0][el.name.replace(/^dsp_/,'')].value = value;
}
function MSdelete(el) {
	var idx = el.selectedIndex;
	if (idx == -1)
		return;
	el[idx] = null;	
	MSupdate(el);
}
