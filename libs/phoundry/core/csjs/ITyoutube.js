function stripYouTubeID(el) {
	var youtubeField = $(el);
	if(youtubeField.val() != '') {
		var youtubeKey = /.+watch[&?]v=([\w-]+)/gi.exec(youtubeField.val()); 	
		var youtubeField_valid = /^([-a-z0-9])+$/i.exec(youtubeField.val());
		if(youtubeKey && youtubeKey[1]) {
			youtubeField.val(youtubeKey[1]);
			youtubeField.css('background-color', '#FFFFFF'); 
		} else if (null == youtubeField_valid) {
			youtubeField.css('background-color', '#EF777C');
			return false;
		} else {
			youtubeField.css('background-color', '#FFFFFF');
		}
	}
}
