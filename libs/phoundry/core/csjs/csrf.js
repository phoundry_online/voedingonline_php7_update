;var CSRF = {

types: [ ["a", "href" ] ],

// dynamic/ajax requests overriding the send function appending the id as a request header
registerAjax: function(paramName, csrfId) 
{
	/*
	XMLHttpRequest.prototype._open = XMLHttpRequest.prototype.open;
	XMLHttpRequest.prototype.open = function(method, url, async, user, pass) 
	{
		this._open.apply(this, arguments);
	}
	XMLHttpRequest.prototype._send = XMLHttpRequest.prototype.send;
	XMLHttpRequest.prototype.send = function(data) 
	{
		if(this.onsend != null) 
		{
			this.onsend.apply(this, arguments);
		}
		this._send.apply(this, arguments);
	}
	XMLHttpRequest.prototype.onsend = function(data) 
	{
		// add the id as a request header (don't want (know how) to modify the url)
		this.setRequestHeader(paramName, csrfId);
	}
	*/
	if (typeof jQuery != 'undefined') {
		$.ajaxSetup({
			'beforeSend': function(xhr) {           
				xhr.setRequestHeader(paramName, csrfId);
			}
		});
	}
},

// adds the csrfId to all known refernce nodes
addToNodes: function(paramName, csrfId) 
{
	// iterate through all known types, e.g. "a"
	for (i = 0; i < CSRF.types.length; i++) 
	{
		var j, name = CSRF.types[i][0], nodes = document.getElementsByTagName(name);
		if (nodes != null) 
		{
			for (j = 0; j < nodes.length; j++) 
			{
				// process all attributes for these nodes, e.g. "href" for nodes of type "a"
				var ai = 1;
				while (CSRF.types[i][ai] != null) 
				{
					var attribute = nodes[j].getAttribute(CSRF.types[i][ai]);
					if (attribute != null) 
					{
						var fragment = null, newattribute, update = false;
						if (attribute.indexOf("#") != -1) 
						{
							// extract the fragment and append it afterwards
							fragment = attribute.substr(attribute.indexOf("#"));
							attribute = attribute.substr(0, attribute.indexOf("#") - 1);
						}
						if(attribute.indexOf("?") == -1) 
						{
							// first query parameter
							newattribute = attribute + "?" + paramName + "=" + csrfId;
							// don't add id if it is not required (may disturb other js code)
							// update = true;
						}
						else
						{
							// append to existing query paramter
							newattribute = attribute + "&" + paramName + "=" + csrfId;
							update = true;
						}
						if (update)	
						{
							if (fragment) 
							{
								newattribute = newattribute + fragment;
							}
							nodes[j].setAttribute(CSRF.types[i][ai], newattribute);
						}
					}
					ai++;
				}
			}
		}
	}
},

// adds the csrfId as a hidden field to every form
addToForms: function(paramName, csrfId) 
{
	var nodes = document.getElementsByTagName('form');
	for (var i = 0; i < nodes.length; i++) 
	{
		var link = document.createElement('input');
		link.setAttribute('type', 'hidden');
		link.setAttribute('name', paramName);
		link.setAttribute('value', csrfId);
		nodes[i].appendChild(link);
	}
},

insert: function(paramName, csrfId) 
{
	// register callbacks when sending data by the browser
	CSRF.registerAjax(paramName, csrfId);

	// simple references
	//CSRF.addToNodes(paramName, csrfId);

	// forms
	CSRF.addToForms(paramName, csrfId);
}

};
