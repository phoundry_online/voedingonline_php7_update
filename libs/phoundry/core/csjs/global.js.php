<?php
	ob_start();
	
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	if (isset($PHprefs['PHenvFunction'])) {
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
		$DMDcid = isset($_GET['DMDcid']) ? (int)$_GET['DMDcid'] : -1;
	}

	list($CSRFname, $CSRFtoken) = PH::startCSRF();

	// Make sure the current user in the current role has access to this page:
	checkAccess();
?>
var _W=window,_D=document,_P=parent,popup,is,op,dragZ=999,formIsDirty=false,useLocks=<?= ($PHprefs['useLocks']) ? 'true' : 'false' ?>,infoTimer, curInfoDiv, curInfoIcon,CSRFname=<?= json_encode($CSRFname) ?>,CSRFtoken=<?= json_encode($CSRFtoken) ?>;

try {
	_D.execCommand('BackgroundImageCache', false, true);
} catch(e) {}

<?php if (!isset($_GET['noFrames'])) { ?>
if (location==top.location) {
	top.location='<?= $PHprefs['url'] ?>/?_page='+escape(location.pathname+location.search+location.hash);
}
<?php }

	// Include jQuery & jQuery plugins:
	include 'jquery.pack.js';
	include 'jquery.cookie.pack.js';
	include 'jquery.inlineWindow.min.js';
	include 'jquery.color.pack.js';
	include 'jquery.uniform.min.js';
	include 'csrf.js';
	if (is_dir("{$PHprefs['distDir']}/layout/js")) {
		chdir("{$PHprefs['distDir']}/layout/js");
		foreach(glob('*.{js,php}', GLOB_BRACE) as $file) {
			include $file;
		}
	}
	if (is_dir("{$PHprefs['instDir']}/custom/csjs")) {
		chdir("{$PHprefs['instDir']}/custom/csjs");
		foreach(glob('*.{js,php}', GLOB_BRACE) as $file) {
			include $file;
		}
	}
?>

/* DEPRECATED: please use jQuery instead: */
function gE(e){return _D.getElementById(e)}
function sE(e){e.style.visibility='visible'}
function hE(e){e.style.visibility='hidden'}
function SE(e){e.style.display=''}
function HE(e){e.style.display='none'}
function sX(e,x){e.style.left=x+'px'}
function sY(e,y){e.style.top=y+'px'}
function sW(e,w){e.style.width=w+'px'}
function sH(e,h){e.style.height=h+'px'}
function wH(e,h){e.innerHTML=h}
/* /DEPRECATED: please use jQuery instead */

function alertError(msg, width, height)
{
	width = width || 500;
	height = height || 300;
	var h = height - 25 - (2*20); // Substract padding (2*20) and footer height (25)
	msg = '<div style="overflow:auto;height:'+h+'px;padding:20px">' + msg + '</div>';
	msg += '<div style="margin-top:5px;padding-left:20px"><button onclick="killPopup()">' + <?= json_encode(word(82 /*Ok*/)) ?> + '</button></div>';
	makePopup(null, width, height, <?= json_encode(word(127 /* Error */)) ?>, null, msg, true);
}

function makePopup(event, width, height, title, url, html, stack) 
{
	if (!stack) {
		while(killPopup());
	}
	var popup = $.fn.inlineWindow({
		windowTitle : title,
		url : url,
		width : width,
		height : height,
		content : html,
		event : event,
		modal : stack
	});
	
	if (!window.popup) {
		window.popup = popup;
	} else {
		if ("close" in window.popup) {
			window.popup = [window.popup];
		}
		window.popup.push(popup);
	}
	return popup;
}

function killPopup() {
	if (!window.popup) {
		return false;
	}
	var popup;
	if ("close" in window.popup) {
		popup = window.popup;
		window.popup = null;
	} else {
		popup = window.popup.pop();
	}
	popup.close();
	return true;
}

if (typeof String.prototype.trim == 'undefined') {
	String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };
}

// what = 'home' or 'back'
function cancelMe(what) {
	if (what == 'back' && history.length > 0) {
		if (_D.referrer) {
			_D.location = _D.referrer;
		}
		else {
			history.back();
		}
	}
	else {
	<?php
		print "_D.location='" . $PHprefs['customUrls']['home'] . "';\n";
	?>
	}
}

function storeScrollPos() {
	try {
		var sf = _P.frames['PHtopmenu'];
		sf.scrollPos = $('#contentFrame')[0].scrollTop;
		sf.scrollTID = _D.location.href.match(/TID=(\d+)/)[1];
	} catch(e) {}
}

function restoreScrollPos() {
	try {
		var sf = _P.frames['PHtopmenu'];
		if (_D.location.href.match(/TID=(\d+)/)[1] == sf.scrollTID) {
			$('#contentFrame')[0].scrollTop = sf.scrollPos;
		}
	} catch(e) {}
}

function shortCut(el, TID) {
	el = $(el);
	var isAdd;
	if (el.is('img')) {
		var txt = el.attr('alt'), isAdd = txt.indexOf('+') != -1;
		el.attr('alt', isAdd ? txt.replace(/\+/, '-') : txt.replace(/-/, '+'));
		el.attr('src', '<?= $PHprefs['url'] ?>/core/icons/' + (isAdd ? 'shortcut_delete.png' : 'shortcut_add.png'));
	} else {
		isAdd = !el.hasClass('shortcut-del'),
		el.toggleClass('shortcut-del');
	}
	url = '<?= $PHprefs['customUrls']['menu'] ?>?<?= QS(0) ?>';
	url += (isAdd) ? '&_addShortcut=' + TID : '&_delShortcut=' + TID;
	_P.frames['PHmenu'].location = url;
}

function hasLock(RID) {
	if (!useLocks) { return false; }
	var response;
	$.ajax({url:'<?= $PHprefs['url'] ?>/core/checkLock.php?<?= QS(0,'RID') ?>', type:'POST', data:{RID:RID}, dataType:'json', async:false, success:function(data){response=data}});
	return response;
}

function applyFilter(id, value, isDate, string_id) {
	<?php
		$dPos = strpos(word(251), 'DD');
		$mPos = strpos(word(251), 'MM');
		$yPos = strpos(word(251), 'YYYY');
	?>
	var F = _D.forms[0], dateFormat = '<?= word(251) ?>';
	if (isDate && value != '') {
		var D, M, Y, tmp;
		D = value.substring(<?= $dPos ?>,<?= $dPos+2 ?>);
		M = value.substring(<?= $mPos ?>,<?= $mPos+2 ?>);
		Y = value.substring(<?= $yPos ?>,<?= $yPos+4 ?>);
		d = parseInt(D,10);
		m = parseInt(M,10)-1;
		y = parseInt(Y,10);
		tmp = new Date(y,m,d);
		if ((tmp.getFullYear()==y) && (tmp.getMonth()==m) && (tmp.getDate()==d)) {
			value = Y+'-'+M+'-'+D;
		}
		else {
			alert('<?= escSquote(word(300 /* Invalid date */)) ?>');
			return false;
		}
	}
    if (F['PHfltr_'+string_id]) {
        F['PHfltr_'+string_id].value = value;
    }
    F['PHfltr_'+id].value = value;
	F.submit();
}

function notify(img, RID)
{
	var url = '<?= $PHprefs['url'] ?>/core/notify.php?<?= QS(0) ?>&RID='+escape(RID)+'&'+Math.random();
	// See if the notification is on or off:
	$.ajax({url:url+'&check=1', dataType:'html', success:function(msg) {
		if (confirm(msg == 1 ? '<?= escSquote(word(217)) ?>' : '<?= escSquote(word(216)) ?>'))
		img.src = url;
	}});
}

function setResults(nrRecords,nrPages,curPage,recordInfo) {
	$('#recordIndex').html(recordInfo);
	var el = _D.forms[0]['PHpage'], x;
	if (nrPages == 0) {
		if (nrRecords == 0) {
			var colspan = $('#recordTBL tr:first th,#recordTBL tr:first td').length;
			$('#recordTBL tr:last').after('<tr class="odd"><td colspan="' + colspan + '">' + recordInfo + '</td></tr>');
			el.disabled = true;
			$('#exportSel').prop('disabled',true);
		}
		$('#prevBut,#nextBut').prop('disabled',true);
		return;
	}
	$('#prevBut').prop('disabled', curPage <= 0);
	$('#nextBut').prop('disabled', curPage == -1 || curPage+1 == nrPages);
}

function exportData(what) {
	switch(what) {
		case 'csv':
			makePopup(null,420,470,'<?= escSquote(word(144)) ?>', '<?= $PHprefs['url'] ?>/core/popups/export_csv.php?<?= QS(0) ?>');
			break;
		case 'pdf':
			makePopup(null,420,160,'<?= escSquote(word(74)) ?>', '<?= $PHprefs['url'] ?>/core/popups/export_pdf.php?<?= QS(0) ?>');
			break;
	}
}

function importData(what) {
	switch(what) {
		case 'csv':
			makePopup(null,420,320,'<?= escSquote(word(166)) ?>', '<?= $PHprefs['url'] ?>/core/popups/import_csv.php?<?= QS(0) ?>');
			break;
	}
}

function setDel(evt,cb) {
	evt.cancelBubble = true;
	setDelButton();
}

function setDelButton() {
	var cbs = $('form')[0]['_id[]'], x, el = $('#delBut')[0], oneChecked = false;
	if(!cbs.length && cbs.checked) oneChecked=true;
	for (x = 0; x < cbs.length; x++) {
		if (cbs[x].checked) {
			oneChecked = true; break;
		}
	}
	if (el && activeRow == null) el.disabled = !oneChecked;
}

function setAllTrash(state) {
	var cbs = $('form')[0]['_id[]'], x;
	if (!cbs) return;
	if (!cbs.length) { cbs.checked=state; }
	for (x = 0; x < cbs.length; x++)
		cbs[x].checked = state;
	setDelButton();
}

function openManual(url) {
	var pageID = top.frames['PHcontent'].document.getElementById('pageID'),
       sep = url.indexOf('?') == -1 ? '?' : '&';
	if (pageID) {
		url += sep + 'pageID=' + escape(pageID.content);
	}
	openCommunity(url);
}

function openCommunity(url, width, height) {
	var anchor = null;
	if (!width) width = 842; if (!height) height = 508;
	if (url.indexOf('#') != -1) {
		url = url.split('#');
		var anchor = url[1];
		url = url[0];
	}

	$.ajax({url:'<?= $PHprefs['url'] ?>/core/getCommunityUrl.php?url='+escape(url), dataType:'html',async:false, success:function(data){url=data}});
	if (anchor)
		url += '#' + anchor;
	openWin(url,width,height,0);
}

function openWin(u, w, h, s) {
	var p='toolbar=no,width='+w+',height='+h+',resizable=yes';
	if(s)p+=',scrollbars=yes';
	win=window.open(u,'Phoundry',p);
	setTimeout('if(win!=null&&!win.closed)win.focus()',250);
}

function showCharCount(el) {
	var len = el.value.length, maxlen = el.getAttribute('maxlength');
	var msg = maxlen && len > maxlen ? '<b style="color:red">'+len+'</b>' : len;
	if (maxlen) msg += ' / ' + maxlen;
	_D.getElementById(el.name+'_cOuNtEr').innerHTML=msg;
}

<?php if ($PHprefs['nrNotes'] > 0) { ?>

var dragNote,dragNoteX=0,dragNoteY=0;

function initNotes() 
{
	setTimeout(function() {
		$('#notesPane').addClass('closed');
	}, 1000);
	$('#notesPane').bind('click', function() {
		if ($(this).is('.closed')) {
			$(this).removeClass('closed');
		}
	});
	$('#notesGrippie').bind('click', function(e) {
		e.stopPropagation();
		$('#notesPane').toggleClass('closed');
	});
}

function editNote(id) {
	makePopup(null,340,330,'<?= escSquote(word(197)) ?>','<?= $PHprefs['url'] ?>/core/popups/editNote.php?<?= QS(0) ?>&NID='+id);
}

function delNote(el,id) {
	if (confirm('<?= escSquote(word(24)) ?>')) {
		$(el).parents('dl').remove();
		if($('#notesPane dl').length == 0) {
			$('#notesPane,#notesGrippie').remove();
		}
		$.ajax({url:'<?= $PHprefs['url'] ?>/core/popups/editNote.php?<?= QS(0) ?>&del&NID='+id});
	}
}

<?php } else { ?>

function initNotes(){};

<?php } ?>

<?php if (isset($PHprefs['product']) && $PHprefs['product'] == 'DMdelivery') { ?>

function setBusy(context) {
   // Disable select-boxes.
	if (!context) context = document;
	if ($.browser.msie) {
		$(context).find('select').each(function(){
			if(this.type == 'select-multiple') {
				for (var i = this.options.length-1; i >= 0; i--) {// IE reverses this somehow...
					if (this.options[i].selected)
						$(this).after('<input type="hidden" name="'+this.name+'" value="'+this[i].value+'" />');
				}
			}
			else
				$(this).after('<input type="hidden" name="'+this.name+'" value="'+this.value+'" />');
			$(this).prop('disabled',true);
		});
	}

	var w = 300, h = 12, blocks = 12, i,
	html = '<div style="position:relative;overflow:hidden;width:'+w+'px;height:'+h+'px;background-color:#fff;border:1px solid #000"><span id="waitBlocks" style="left:-'+(h*2+1)+'px;position:absolute">';
	for(i=0; i < blocks; i++){
		html += '<span style="background-color:#900;left:-'+((h*i)+i)+'px;position:absolute;width:'+h+'px;height:'+h+'px;'
		html += $.browser.msie?'filter:alpha(opacity='+(100-i*(100/blocks))+')':'opacity:'+((100-i*(100/blocks))/100);
		html += '"></span>';
	}
	html += '</span></div>';

	// Show 'one moment please' layer:
   $(_D.body).append($('<div id="busy" style="position:absolute;left:0;top:0;width:100%;height:100%;z-index:999;background:#000;cursor:wait">').css({opacity:0.6}).append($('<div style="position:absolute;left:50%;top:50%;width:300px;height:46px;margin:-18px 0 0 -150px;border:1px solid red;background:white;font:20pt Arial,Helvetica,Sans-serif;padding:8px;text-align:center;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px">').html('<b><?= escSquote(word(192)) ?></b>'+html)));

	setInterval(function(){
		var el = $('#waitBlocks'), l = parseInt(el.css('left'),10);
		el.css('left', l+h+1-(blocks*h+blocks)>w ? -(h*2+1)+'px' : (l+h+1)+'px');
	},50);

	return true;
}
var thumbTimer = thumbMid = null;
function mailingThumbnail(DMDmid,x,y,appendTo,fadeTime) {
	if (!fadeTime) fadeTime = 0;
	thumbMid = DMDmid;
	clearTimeout(thumbTimer);
	if ($('#mailingThumb').length==0) {
		$('<div id="mailingThumb"></div>')
		.append('<img style="position:absolute;left:143px;top:7px;width:13px;height:13px" src="<?= $PHprefs['url'] ?>/core/pics/pixel.gif" class="ptr" onclick="$(this.parentNode).fadeOut()" />')
		.append('<div></div>')
		.appendTo($(appendTo));
	}
	$('#mailingThumb')
	.css({left:x,top:y})
	.fadeIn()
	.find('div')
	.html('<img src="<?= $DMDprefs['mailingUrl'] ?>/<?= $DMDcid ?>/thumbnail/m_'+DMDmid+'_105x140.jpg?' + Math.random() + '" onerror="_mailingThumbnail2('+DMDmid+','+fadeTime+')" onload="_mailingThumbnail3('+fadeTime+')" />');
}

function _mailingThumbnail2(DMDmid, fadeTime) {
	$('#mailingThumb div').html('<img src="<?= $PHprefs['url'] ?>/dmdelivery/pics/loading.gif" style="margin-top:60px;width:18px;height:18px" />');
	$.ajax({url:'/x/thumb.php?DMDcid=<?= $DMDcid ?>&kind=ajax&size=105x140&DMDmid='+DMDmid, dataType:'html', success:function(imgSrc) {
		if (DMDmid != thumbMid) return;
		$('#mailingThumb div')
		.html('<img src="'+imgSrc+'" class="ptr" onclick="makePopup(null,-50,-50,\'<?= escSquote(word(1508 /* Preview */)) ?>\',\'/x/thumb.php?<?= QS(0,'DMDmid') ?>&kind=redirect&DMDmid='+DMDmid+'\')" />');
		_mailingThumbnail3(fadeTime);
	}});
}

function _mailingThumbnail3(fadeTime) {
	thumbTimer = fadeTime ? setTimeout("$('#mailingThumb').fadeOut()",fadeTime) : null;
}

<?php } ?>

//
// FORMS:
//

function selectAll(el) {
	var x;
	if (el.type == 'select-multiple') {
		// Multiple-select box:
   	for (x = 0; x < el.length; x++)
      	el.options[x].selected = true;
	}
	else {
		// Checkboxes:
		var len = (typeof el.length == 'undefined') ? 1 : el.length;
		for (x = 0; x < len; x++) {
			if (len == 1)
				el.checked = true;
			else
				el[x].checked = true;
		}
	}
}

var callbackEl;
function newOption(value, row, text) {
	if (!callbackEl) return;
	var len = callbackEl.options.length;
	for (var x = 0; x < len; x++) {
		if (callbackEl[x].text == text && callbackEl[x].value == value) {
			return;
		}
	}
	callbackEl[len] = new Option(text,value);
	callbackEl[len].selected = true;
}

function moveTo(selname, dir)
{
	var moved = true, fr_sel, to_sel, value = '';
	fr_sel = _D.forms[0][(dir == 'left' ? 'dsp_' : 'all_') + selname];
	to_sel = _D.forms[0][(dir == 'left' ? 'all_' : 'dsp_') + selname];
	while(moved) {
		if (fr_sel.length == 0) break;
		for (var x = 0; x < fr_sel.length; x++) {
			if (fr_sel.options[x].selected) {
				to_sel.options[to_sel.length] = new Option(fr_sel.options[x].text, fr_sel.options[x].value);
				fr_sel.options[x] = null;
				moved = true;
				break;
			}
			moved = false;
		}
	}
	to_sel = _D.forms[0]['dsp_'+selname];
	for (x = 0; x < to_sel.length; x++) {
		if (x > 0) value += '|';
		value += to_sel[x].value;
	}
	_D.forms[0][selname].value=value;
}

function _updateDate(F, name) {
	F[name].value = F[name+'_year'].value + '-' + F[name+'_month'].value + '-' + F[name+'_day'].value;
}

function useTreeFile(path, colname) {
	var F = _D.forms[0];
	F[colname].value = F['dsp_'+colname].value =  path;
	killPopup();
}

function selectTreeLeaf(name, el) {
	$('#'+name+'_tree li a').removeClass('selected');
	if(el){
		el.className = 'selected';
	}
	_D.forms[0][name].value = el ? el.title : '';
}

function checkForm(F) {
	if (typeof PEsubmit == 'function'){
		// The PhoundryEditor attached the 'PEsubmit' event, remove it
		// in order to be able to call it before actually running this function.
		if(F.removeEventListener)
			F.removeEventListener('submit',PEsubmit,false);
		else
			F.detachEvent('onsubmit', PEsubmit);
		PEsubmit();
	}
	if (typeof checkFormExtra == 'function') {
		if (!checkFormExtra(F)) {
			return false;
		}
	}
	
	var x, el, id, props, dt, img, value, dir, errMsg, fn, desc, name, tmp;
	formErrors = [];
	for (x = 0; x < F.elements.length; x++) {
		el = F.elements[x];
		name = el.name;
		alt = el.getAttribute('alt');
		if (el.tagName == 'FIELDSET' || alt == null || alt == '') continue;
		formErrors[name] = '';
		props = alt.split('|');
		dt = props[1]; // Datatype
		value = '';

		desc = el.title ? el.title : $(el).parents('fieldset').find('b').html();

		if (dt == 'filter') {
			F[name].value = getFilterJSON();
		}

		if ('textarea|hidden|password|select-multiple|select-one'.indexOf(el.type) != -1) {
			value = el.value;
			if (el.type == 'password') {
				if (F[name+'_cNfRm_'] && value != F[name+'_cNfRm_'].value)
					formErrors[name] += '<?= escSquote(word(63 /* Passwords do not match */)) ?>'.replace(/#1/, desc) + '\n';
				value = value.replace(/^(MD5|CRYPT):/,'');
			}
		}
		else if ('radio|checkbox'.indexOf(el.type) != -1) {
			tmp = F[name];
			if (typeof tmp.length == 'undefined') {
				if (tmp.checked) {
					value = tmp.value;
					if (value == '') {value = 0;}
				};
			}
			else {
				for(i=0;i<tmp.length;i++) {
					if(tmp[i].checked) {
						value=tmp[i].value;
						if (value == '') value = 0;
						break;}
				}
			}
		}

		// Check required
		if (props[0] == 1) {
			if (typeof value == 'string' && value.replace(/^\s+|\s+$/g, '') == '')
				formErrors[name] += '<?= escSquote(word(43 /* Required field is empty */)) ?>'.replace(/#1/, desc) + ' ';
		}

		// Check maxlength:
		if (props[2]) {
			if (value.length > props[2])
				formErrors[name] += '<?= escSquote(word(44 /* Field contains too many characters */)) ?>'.replace(/#1/,desc).replace(/#2/,value.length).replace(/#3/,props[2]) + ' ';
		}

		if (value != '' && dt && dt == 'url' && /^(http|https|ftp):\/\/$/i.test(value)) { 
			value = el.value = ''; 
		}
		// Check syntax:
		if (value != '' && dt) {
			// We are checking for a global function
			if (typeof window['checkDT'+dt] == 'function') {
				errMsg = window['checkDT'+dt].call(el, desc, value, el);
				if (errMsg != '') {
					formErrors[name] += errMsg;
				}
			}
		}

		if (typeof F['dsp_'+name] != 'undefined')
			el = F['dsp_'+name];
		el.className = (formErrors[name] == '') ? '' : 'formError';
		$(el).unbind('mouseover').unbind('mouseout');
		if (formErrors[name] != '') {
			$(el).mouseover(function(){
				$('#checkTD').html('&bull; ' + formErrors[this.name.replace(/Img$/,'')]);
			}).mouseout(function(){
				$('#checkTD').html('&nbsp;');
			});
		}
	}

	errMsg = '';
	for(x in formErrors) {
		if (typeof formErrors[x] != 'function' && formErrors[x] != '') {
			errMsg += formErrors[x] + "\n";
		}
	}
	
	$('.fieldset-wrapper-formError').removeClass('fieldset-wrapper-formError');
	$('.fieldset-wrapper:has(.formError)').addClass('fieldset-wrapper-formError');
	
	if (errMsg == '') {
		if (typeof checkFormPost == 'function') {
			if (!checkFormPost(F)) {
				return false;
			}
		}
		// Do a setTimeout so that the submit buttons will be disabled
		// after the submit has initiated so that the values of the
		// submit buttons are also submitted
		setTimeout(function(){
			$('input[type=submit]').prop('disabled',true);
		}, 0);
		formIsDirty = false;
		return true;
	}
	alert(errMsg);
	return false;
}

function setInlineSelectEvent(el) {
	var cid = el.className.split(' ')[1].replace('cid',''),
		 el = $(el),
		 td = el.parents('td'),
		 tr = el.parents('tr'),
		 rid = (/row\d+_(.+)/.test(tr[0].id)) ? RegExp.$1 : '';
	td.html('<img src="<?= $PHprefs['url'] ?>/core/icons/throbber.gif" />');
	$.get('<?= $PHprefs['url'] ?>/core/swapSelect.php?<?= QS(0) ?>', {CID:cid,RID:rid}, function(data){
		td.html(data);
	});
	return false;
}

function setInlineSelectValue(el) {
	var cid = el.id.replace('cid',''),
	    value = el.value;
	el = $(el);
	var td = el.parents('td'),
	    tr = el.parents('tr'),
		 rid = (/row\d+_(.+)/.test(tr[0].id)) ? RegExp.$1 : '';
	if (value.trim() == '') return;

	td.html('<img src="<?= $PHprefs['url'] ?>/core/icons/throbber.gif" />');

	$.get('<?= $PHprefs['url'] ?>/core/swapSelect.php?<?= QS(0) ?>', {CID:cid,RID:rid,value:value}, function(data){
		var newel = $(data);
		td.html(newel.bind('click', function(){setInlineSelectEvent(newel[0])}));
	});
}

$(function(){
	// Re-enable submit buttons (for Firefox, after hitting Back button):
	$("#footerButtons input[type='submit']:not('.disabled'),#footerButtons input[type='button']:not('.disabled')").prop('disabled',false);

	$.ajaxSetup({
		cache: false
	});

	// CSRF protection:
   CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');

	<?php if (!isset($PHprefs['product']) || $PHprefs['product'] != 'DMdelivery') { ?>
	// The following lines are a 'hack' for old (< 6.0) Phoundry Plugins, that
	// don't have #headerButtons, #footerButtons. We use jQuery to fix the CSS.
	$('.buttons:not(#headerButtons,#footerButtons):first').attr('id', 'headerButtons');
	$('.buttons:not(#headerButtons,#footerButtons):last').attr('id', 'footerButtons');
	<?php } ?>

	// Initialise popup calendar(s):
	$('.popupcal').each(function(){
		$(this).calendar({autoPopUp:'button'});
	});

	$('.swapRadio').each(function(){
		$(this).bind('click', function() {
			var el = $(this),
				 td = el.parents('td'),
				 tr = el.parents('tr'),
				 cid = this.className.split(' ')[1].replace('cid',''),
				 rid = (/row\d+_(.+)/.test(tr[0].id)) ? RegExp.$1 : '';
			td.animate({backgroundColor:'#e62b2d'},500);
			$.get('<?= $PHprefs['url'] ?>/core/swapRadio.php?<?= QS(0) ?>', {'CID':cid, 'RID':rid}, function(data){
				if (data) el.html(data);
				td.animate({backgroundColor:tr.css('backgroundColor')},500);
			});
			return false;
		});
	});

	$('.swapSelect').each(function(){
		$(this).bind('click', function(){setInlineSelectEvent(this)});
	});

	// Move popup-properties to 'popupProps' attribute:
	$('.popupWin').each(function(){
		var props = this.title.split('|');
		$(this).data('popupProps', this.title);
		this.title = props[0];
	});

	$('input.checkStrength').each(function(){
		var id = this.name + 'Bar', $this = $(this);
		$this
		.bind('keydown', function() {
			if ($('#'+id).length == 0) {
			$this
			.after('<div id="'+id+'" style="margin:0 5px;display:inline-block;width:100px;height:10px;background:url(<?= $PHprefs['url'] ?>/core/pics/strength.png)"></div><span id="'+id+'Desc"></span>')
			.bind('blur', function() { $('#'+id+',#'+id+'Desc').remove(); })
			.bind('keyup', function() {
				var pwd = $.trim($this.val()), pos = 0, desc;
				if (pwd.length==0) desc = '<?= escSquote(word(314 /* Type password */)) ?>';
				else if (!/(?=.{6,}).*/.test(pwd)) desc = '<?= escSquote(word(315 /* Use more characters */)) ?>';
				else if (/^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\W).*$/.test(pwd)) { desc = '<?= escSquote(word(316 /* Strong password */)) ?>'; pos = 10; }
				else if (/^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$/.test(pwd)) { desc = '<?= escSquote(word(317 /* Medium password */)) ?>'; pos = 20; }
				else { desc = '<?= escSquote(word(318 /* Weak password */)) ?>'; pos = 30; }
				$('#'+id).attr('title',desc).css({backgroundPosition:'0 '+pos+'px'});
				$('#'+id+'Desc').text(desc);
			});
			}
		});
	});
	
	// Set up info divs.
	$('fieldset.field>div.miniinfo').each(function() {
		var strongs = $(this).find('>strong');
		if (strongs.length > 0) {
			strongs.parent('div.miniinfo').show();
		}
		else {
			$(this).parent('fieldset').find('legend')
			.prepend('<img class="info2" src="<?= $PHprefs['customUrls']['pics'] ?>/info2.png" width="16" height="14" align="right" style="cursor:help" />')
			.bind('click', function(){showFieldsetInfo(this,0)})
			.bind('mouseover',function(){showFieldsetInfo(this,750)})
			.bind('mouseout',function(){clearTimeout(infoTimer)});
		}
	});
	
	// Show filter tables:
	$('.filter-table').css('visibility','visible');
	
	$('span.icon.email-inactive-icon, span.icon.email-active-icon').live('click', function() {
		var RID = parseInt($(this).closest('tr').attr('data-rid'), 10);
		var url = '<?= $PHprefs['url'] ?>/core/notify.php?<?= QS(0) ?>&RID='+escape(RID);
		$.ajax({ url: url });
		if ($(this).is('.email-inactive-icon')) {
			$(this).removeClass('email-inactive-icon').addClass('email-active-icon');
		} else {
			$(this).removeClass('email-active-icon').addClass('email-inactive-icon');
		}
	});
	
	$('span.icon.email-all-icon, span.icon.email-all-active-icon').live('click', function() {
		var RID = 'any';
		var url = '<?= $PHprefs['url'] ?>/core/notify.php?<?= QS(0) ?>&RID='+escape(RID);
		$.ajax({ url: url });
		if ($(this).is('.email-all-icon')) {
			$(this).removeClass('email-all-icon').addClass('email-all-active-icon');
		} else {
			$(this).removeClass('email-all-active-icon').addClass('email-all-icon');
		}
	});
});

function showFieldsetInfo(el,delay) {
	infoTimer = setTimeout(function(){
		if (curInfoIcon) curInfoIcon.show();

		curInfoIcon = $(el).find('.info2');
		curInfoIcon.hide();

		var infoDiv = $(el).parent('fieldset').find('div.miniinfo:first');
		if (curInfoDiv && infoDiv[0] == curInfoDiv) { return; }
		$(curInfoDiv).hide();
		infoDiv.fadeIn();	
		curInfoDiv = infoDiv[0];
	}, delay);
}

// Setup jQuery live events (so we don't need to wait for the page to load):

// Make popups for all class="popupWin" elements:
$('.popupWin').live('click', function(event) {
	var props = $(this).data('popupProps') || this.title;
	props = props.split('|');
	var url = props[4] || this.href;
	killPopup();
	popup = $.fn.inlineWindow({windowTitle:props[0],width:props[1],height:props[2],url:url,modal:props[3]&&props[3]==1,event:event});
	return false;
});

$('.ITpriority .moveUp, .ITpriority .moveDown').live('click', function() {
	var me = $(this).parents('.ITpriority:first'),
		colName = me.attr('data-colname'),
		tr = me.parents('tr:first'),
		isup = (/moveUp/.test(this.className)), 
	    ref = isup ? $(tr).prev('tr').prev('tr') : $(tr).next('tr'),
	    myCellIndex = me.parents('td:first')[0].cellIndex;
	ref.each( function() {
		tr[0].className = tr[0].rowIndex % 2 == 1 ? 'even' : 'odd';
		var switched = (isup)? $(this).next('tr')[0] : $(this)[0],
			swPrioSpan = $($(switched).children('td').get(myCellIndex)).find('.prio'),
		    myPrioSpan = me.find('.prio'),
		    swPrio = swPrioSpan.html(),
		    myPrio = myPrioSpan.html();
		    
		if (myPrio == swPrio) {
			if (isup) {
				swPrio++;
			}
			else {
				myPrio++;
			}
		}
		swPrioSpan.html(myPrio);
		myPrioSpan.html(swPrio);
		// row swapping..
		$(tr).insertAfter($(this));
		// class swapping..
		switched.className = tr[0].rowIndex % 2 == 1 ? 'even' : 'odd';
		// ajax call to update server:
		$.get(
			'swapPrio.php?<?= QS(0, 'TID=') ?>',
			{
				'isup' : isup,
				'colName' : colName,
				'TID' : tr.closest('table').attr('data-tid'),
				'RID1' : tr.attr('data-rid'),
				'RID2' : $(switched).attr('data-rid')
			}
		);
	});
});

// Add Print Stylesheet:
if (!$.browser.msie || $.browser.version >= 9) {
$('head').append('<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/print.css" media="print" type="text/css" />'); // Causes mixed content warning on IE7/8!
}
<?php if (isset($_GET['DMDwizard'])) { ?>
$('head').append('<style type="text/css"> .hideDMDwizard { display:none; } </style>');
<?php } ?>


<?php
$content = ob_get_contents();
ob_end_clean();

$checksum	= md5($content);
$etag			= (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'] : '') ;
if ($etag == $checksum) {
	header("HTTP/1.1 304 Not Modified");
	exit;
}

$expires = date("D, d M Y H:i:s ", (time() + (7 * 86400)) ) . 'GMT';
header("Expires: " . $expires);
header('Content-Type: application/javascript; charset=' . $PHprefs['charset']);
header('Etag: ' . $checksum);

echo $content;
