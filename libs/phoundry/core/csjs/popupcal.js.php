<?php
   $inc = @include_once('PREFS.php');
   if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
   }
   require_once "{$PHprefs['distDir']}/core/include/common.php";

   header('Content-type: application/x-javascript; charset=' . $PHprefs['charset']);
?>
/* MarcGrabanski.com v2.5 */
/* Pop-Up Calendar Built from Scratch by Marc Grabanski */
/* Enhanced by Keith Wood (kbwood@iprimus.com.au). */
/* Time picker functionality added by Gregor Petrin*/
/* Under the Creative Commons Licence http://creativecommons.org/licenses/by/3.0/
	Share or Remix it but please Attribute the authors. */
var popUpCal = {
	selectedDay: 0,
	selectedMonth: 0, // 0-11
	selectedYear: 0, // 4-digit year
	selectedHour:0,
	selectedMinute:0,
	clearText: '<?= word(228) ?>', // Display text for clear link
	closeText: '<?= word(48) ?>', // Display text for close link
	prevText: '&lt;&lt;', // Display text for previous month link
	nextText: '&gt;&gt;', // Display text for next month link
	currentText: '<?= word(211) ?>', // Display text for current month link
	appendText: '', // Display text following the input box, e.g. showing the format
	buttonText: '...', // Text for trigger button
	buttonImage: '<?= $PHprefs['url'] ?>/core/icons/calendar.png', // URL for trigger button image
	buttonImageOnly: true, // True if the image appears alone, false if it appears on a button
	//dayNames: ['Su','Mo','Tu','We','Th','Fr','Sa'], // Names of days starting at Sunday
	dayNames: [<?php foreach(array(word(262), word(256), word(257), word(258), word(259), word(260), word(261)) as $day) { print "'" . substr($day,0,3) . "',"; } ?>], // Names of days starting at Sunday
	monthNames: ['<?php print implode("','", array(word(263), word(264), word(265), word(266), word(267), word(268), word(269), word(270), word(271), word(272), word(273), word(274))) ?>'], // Names of months

	//monthNames: 'January','February','March','April','May','June','July','August','September','October','November','December'], // Names of months
	//dateFormat: 'DD-MM-YYYY hh:mm',
	dateFormat: 'DD-MM-YYYY',
	//dateFormat: 'DMY-', // First three are day, month, year in the required order, fourth is the separator, e.g. US would be 'MDY/'
	yearRange: '-10:+10', // Range of years to display in drop-down, either relative to current year (-nn:+nn) or absolute (nnnn:nnnn)
	changeMonth: true, // True if month can be selected directly, false if only prev/next
	changeYear: true, // True if year can be selected directly, false if only prev/next
	firstDay: 1, // The first day of the week, Sun = 0, Mon = 1, ...
	changeFirstDay: false, // True to click on day name to change, false to remain as set
	showOtherMonths: false, // True to show dates in other months, false to leave blank
	minDate: null, // The earliest selectable date, or null for no limit
	maxDate: null, // The latest selectable date, or null for no limit
	speed: 'medium', // Speed of display/closure
	autoPopUp: 'focus', // 'focus' for popup on focus, 'button' for trigger button, or 'both' for either
	closeAtTop: true, // True to have the clear/close at the top, false to have them at the bottom
	customDate: null, // Function that takes a date and returns an array with [0] = true if selectable, false if not,
		// [1] = custom CSS class name(s) or '', e.g. popUpCal.noWeekends
	fieldSettings: null, // Function that takes an input field and returns a set of custom settings for the calendar

	getWeekday : function(el) {
		var $input = $(el); days = ['<?= word(262) ?>', '<?= word(256) ?>', '<?= word(257) ?>', '<?= word(258) ?>', '<?= word(259) ?>', '<?= word(260) ?>', '<?= word(261) ?>'];

		if (el.value != '') {
			var pattern = $input.data('pattern') || $input.attr('PHpattern');
			var dpos = pattern.indexOf('DD'), d = parseInt(el.value.substring(dpos, dpos+2),10),
			mpos = pattern.indexOf('MM'), m = parseInt(el.value.substring(mpos, mpos+2),10)-1,
			ypos = pattern.indexOf('YYYY'), y = parseInt(el.value.substring(ypos, ypos+4),10),
			tmp = new Date(y,m,d), day = tmp.getDay();
			if (!isNaN(day)) {
				$('#'+el.name+'_weekday').text(days[day]);
			}
			else {
				$('#'+el.name+'_weekday').text('');
			}
		}
	},

	/* Format and display the given date. */
	formatDate: function(day, month, year, hour, minute) {
		month++; // adjust javascript month
		var dateString = this.dateFormat;

		var dateString = dateString
			.replace('DD', (day < 10 ? '0' : '') + day)
			.replace('MM', (month < 10 ? '0' : '') + month)
			.replace('YYYY', year);
		if (this.dateFormat.indexOf('hh') != -1) {
			dateString = dateString
			.replace('hh', (hour < 10 ? '0' : '') + hour)
			.replace('mm', (minute < 10 ? '0' : '') + minute)
		}
		return dateString;
	},
	
	/*Parses a string and returns a Date object*/
	parseDate: function(Text) {
		if (!Text) {
			return new Date();
		}
		var DD = parseInt(Text.substr(this.dateFormat.indexOf('DD'),2),10);
		var MM = parseInt(Text.substr(this.dateFormat.indexOf('MM'),2),10)-1;
		var YYYY = parseInt(Text.substr(this.dateFormat.indexOf('YYYY'),4),10);

		if (this.dateFormat.indexOf('hh') != -1) {
			// We want time as well:
			var hh = parseInt(Text.substr(this.dateFormat.indexOf('hh'),2),10);
			var mm = parseInt(Text.substr(this.dateFormat.indexOf('mm'),2),10);
			if (isNaN(hh)) hh = 0;
			if (isNaN(mm)) mm = 0;
			return new Date(YYYY,MM,DD,hh,mm);
		}
		else {
			return new Date(YYYY,MM,DD);
		}
	},
	
	/* Parse existing date and initialise calendar. */
	setDateFromField: function() {
		var currentDate = this.parseDate(this.input.val());
		this.currentDay = currentDate.getDate();
		this.currentMonth = currentDate.getMonth();
		this.currentYear = currentDate.getFullYear();
		if (this.dateFormat.indexOf('hh') != -1) {
			this.currentHour = currentDate.getHours();
			this.currentMinute = currentDate.getMinutes();
		}

		this.selectedDay = this.currentDay;
		this.selectedMonth = this.currentMonth;
		this.selectedYear = this.currentYear;
		if (this.currentHour != null && this.currentMinute != null) {
			this.selectedHour = this.currentHour;
			this.selectedMinute = this.currentMinute;
		}
		this.adjustDate(0, 'D', true);
	},
	
	/* Update the input field with the selected date. */
	/* Edited by Gregor Petrin to allow a custom input field as a parameter  - this can be used to display a date at start*/
	selectDate: function(Input) {
		if (!Input) var Input = this.input;
		var maxlen = this.input.attr('maxlength');
		var value  = this.dateFormat.indexOf('hh') != -1 ?
		             this.formatDate(this.selectedDay, this.selectedMonth, this.selectedYear, this.selectedHour, this.selectedMinute) :
						 this.formatDate(this.selectedDay, this.selectedMonth, this.selectedYear);
		if (maxlen && maxlen > 0) {
			value = value.substr(0,maxlen);
		}
		this.hideCalendar(this.speed);
		Input.val(value);
		Input.trigger('change');
		Input.focus();
	},
	
	/* Construct and display the calendar. */
	showCalendar: function() {
		this.popUpShowing = true;
		// build the calendar HTML
		var timeSelect = '';
		if (this.dateFormat.indexOf('hh') != -1) {
			timeSelect += '<div id="calendar_time"><select id="calendar_hour">';
			
			for (var a = 0; a<24; a++) {
				if (this.selectedHour == a) timeSelect += '<option value="' + a + '" selected="selected">' + a  + '</option>';
				else timeSelect += '<option value="' + a + '">' + a + '</option>';
			}
			
			timeSelect += '</select> : <select id="calendar_minute">';
			for (var a = 0; a <= 59; a++) {
				if (this.selectedMinute == a) timeSelect += '<option value="' + a + '" selected="selected">' +  ((a >= 10) ? a : ('0' + a)) + '</option>';
				else timeSelect += '<option value="' + a + '">' +  ((a >= 10) ? a : ('0' + a)) + '</option>';
			}
			timeSelect +='</select></div>';
		}

		var html = (this.closeAtTop ? '<div id="calendar_control">' +
			'<a id="calendar_clear">' + this.clearText + '</a>' +
			'<a id="calendar_close">' + this.closeText + '</a></div>' : '') + 
			'<div id="calendar_links"><table cellspacing="0" cellpadding="3" width="100%"><tr><td id="calendar_prev" align="left">' + this.prevText + '</td>' +
			'<td id="calendar_current" align="center">' + this.currentText + '</td>' +
			'<td id="calendar_next" align="right">' + this.nextText + '</td></tr></table></div>' +
			'<div id="calendar_header">';
		if (!this.changeMonth) {
			html += this.monthNames[this.selectedMonth] + '&nbsp;';
		}
		else {
			var inMinYear = (this.minDate && this.minDate.getFullYear() == this.selectedYear);
			var inMaxYear = (this.maxDate && this.maxDate.getFullYear() == this.selectedYear);
			html += '<select id="calendar_newMonth">';
			for (var month = 0; month < 12; month++) {
				if ((!inMinYear || month >= this.minDate.getMonth()) &&
						(!inMaxYear || month <= this.maxDate.getMonth())) {
					html += '<option value="' + month + '"' + 
						(month == this.selectedMonth ? ' selected="selected"' : '') + 
						'>' + this.monthNames[month] + '</option>';
				}
			}
			html += '</select>';
		}
		if (!this.changeYear) {
			html += this.selectedYear;
		}
		else {
			// determine range of years to display
			var years = this.yearRange.split(':');
			var year = 0;
			var endYear = 0;
			if (years.length != 2) {
				year = this.selectedYear - 10;
				endYear = this.selectedYear + 10;
			}
			else if (years[0].charAt(0) == '+' || years[0].charAt(0) == '-') {
				year = this.selectedYear + parseInt(years[0],10);
				endYear = this.selectedYear + parseInt(years[1],10);
			}
			else {
				year = parseInt(years[0],10);
				endYear = parseInt(years[1],10);
			}
			year = (this.minDate ? Math.max(year, this.minDate.getFullYear()) : year);
			endYear = (this.maxDate ? Math.min(endYear, this.maxDate.getFullYear()) : endYear);
			html += '<select id="calendar_newYear">';
			for (; year <= endYear; year++) {
				html += '<option value="' + year + '"' + 
					(year == this.selectedYear ? ' selected="selected"' : '') + 
					'>' + year + '</option>';
			}
			html += '</select>';
		}
		html += '</div>' + timeSelect + '<table id="calendar" cellpadding="0" cellspacing="0"><thead>' +
			'<tr class="calendar_titleRow">';
		for (var dow = 0; dow < 7; dow++) {
			html += '<td>' + (this.changeFirstDay ? '<a>' : '') + 
				this.dayNames[(dow + this.firstDay) % 7].substr(0,2) + (this.changeFirstDay ? '</a>' : '') + '</td>';
		}
		html += '</tr></thead><tbody>';
		var daysInMonth = this.getDaysInMonth(this.selectedYear, this.selectedMonth);
		this.selectedDay = Math.min(this.selectedDay, daysInMonth);
		var leadDays = (this.getFirstDayOfMonth(this.selectedYear, this.selectedMonth) - this.firstDay + 7) % 7;
		var currentDate = new Date(this.currentYear, this.currentMonth, this.currentDay);
		var selectedDate = new Date(this.selectedYear, this.selectedMonth, this.selectedDay);
		var printDate = new Date(this.selectedYear, this.selectedMonth, 1 - leadDays);
		var numRows = Math.ceil((leadDays + daysInMonth) / 7); // calculate the number of rows to generate
		var today = new Date();
		today = new Date(today.getFullYear(), today.getMonth(), today.getDate()); // clear time
		for (var row = 0; row < numRows; row++) { // create calendar rows
			html += '<tr class="calendar_daysRow">';
			for (var dow = 0; dow < 7; dow++) { // create calendar days
				var customSettings = (this.customDate ? this.customDate(printDate) : [true, '']);
				var otherMonth = (printDate.getMonth() != this.selectedMonth);
				var unselectable = otherMonth || !customSettings[0] || 
					(this.minDate && printDate < this.minDate) || 
					(this.maxDate && printDate > this.maxDate);
				html += '<td class="calendar_daysCell' + 
					((dow + this.firstDay + 6) % 7 >= 5 ? ' calendar_weekEndCell' : '') + // highlight weekends
					(otherMonth ? ' calendar_otherMonth' : '') + // highlight days from other months
					(printDate.getTime() == selectedDate.getTime() ? ' calendar_daysCellOver' : '') + // highlight selected day
					(unselectable ? ' calendar_unselectable' : '') +  // highlight unselectable days
					(!otherMonth || this.showOtherMonths ? ' ' + customSettings[1] : '') + '"' + // highlight custom dates
					(printDate.getTime() == currentDate.getTime() ? ' id="calendar_currentDay"' : // highlight current day
					(printDate.getTime() == today.getTime() ? ' id="calendar_today"' : '')) + '>' + // highlight today (if different)
					(otherMonth ? (this.showOtherMonths ? printDate.getDate() : '&nbsp;') : // display for other months
					(unselectable ? printDate.getDate() : '<a>' + printDate.getDate() + '</a>')) + '</td>'; // display for this month
				printDate.setDate(printDate.getDate() + 1);
			}
			html += '</tr>';

		}
		html += '</tbody></table><!--[if lte IE 6.5]><iframe src="javascript:false;" id="calendar_cover"></iframe><![endif]-->' +
			(this.closeAtTop ? '' : '<div id="calendar_control"><a id="calendar_clear">' + this.clearText + '</a>' +
			'<a id="calendar_close">' + this.closeText + '</a></div>');

		// add calendar to element to calendar Div
		$('#calendar_div').empty().append(html).show(this.speed);
		this.input[0].focus();
		this.setupActions();
	}, // end showCalendar
	
	/* Initialisation. */
	init: function() {
		this.popUpShowing = false;
		this.lastInput = null;
		this.disabledInputs = [];
		$('body').append('<div id="calendar_div" style="position:absolute"></div>');
		$(document).mousedown(popUpCal.checkExternalClick);
	},
	
	/* Pop-up the calendar for a given input field. */
	showFor: function(target) {
		var input = (target.nodeName && target.nodeName.toLowerCase() == 'input' ? target : this);
		if (input.nodeName.toLowerCase() != 'input') { // find from button/image trigger
			input = $(input).prev('input')[0];
		}
		if (popUpCal.lastInput == input) { // already here
			return;
		}
		for (var i = 0; i < popUpCal.disabledInputs.length; i++) {  // check not disabled
			if (popUpCal.disabledInputs[i] == input) {
				return;
			}
		}
		popUpCal.input = $(input);
		popUpCal.hideCalendar();
		popUpCal.lastInput = input;
		popUpCal.setDateFromField();

		var offset = $(input).offset();
		if (offset['top'] + popUpCal.input.height()+170 > $(window).height()) {
			// Show calendar above input:
			$('#calendar_div').css({left:offset['left']+'px',top:offset['top']-170+'px'});
		}
		else {
			// Show calendar below input:
			$('#calendar_div').css({left:offset['left']+'px',top:offset['top']+popUpCal.input.height()+4+'px'});
		}
		$.extend(popUpCal, (popUpCal.fieldSettings ? popUpCal.fieldSettings(input) : {}));
		popUpCal.showCalendar(); 
	},
	
	/* Handle keystrokes. */
	doKeyDown: function(e) {
		if (popUpCal.popUpShowing) {
			switch (e.keyCode) {
				case 9:  popUpCal.hideCalendar(); break; // hide on tab out
				case 13: popUpCal.selectDate(); break; // select the value on enter
				case 27: popUpCal.hideCalendar(popUpCal.speed); break; // hide on escape
				case 33: popUpCal.adjustDate(-1, (e.ctrlKey ? 'Y' : 'M')); break; // previous month/year on page up/+ ctrl
				case 34: popUpCal.adjustDate(+1, (e.ctrlKey ? 'Y' : 'M')); break; // next month/year on page down/+ ctrl
				case 35: if (e.ctrlKey) $('#calendar_clear').click(); break; // clear on ctrl+end
				case 36: if (e.ctrlKey) $('#calendar_current').click(); break; // current on ctrl+home
				case 37: if (e.ctrlKey) popUpCal.adjustDate(-1, 'D'); break; // -1 day on ctrl+left
				case 38: if (e.ctrlKey) popUpCal.adjustDate(-7, 'D'); break; // -1 week on ctrl+up
				case 39: if (e.ctrlKey) popUpCal.adjustDate(+1, 'D'); break; // +1 day on ctrl+right
				case 40: if (e.ctrlKey) popUpCal.adjustDate(+7, 'D'); break; // +1 week on ctrl+down
			}
		}
		else if (e.keyCode == 36 && e.ctrlKey) { // display the calendar on ctrl+home
			popUpCal.showFor(this);
		}
	},
		
	/* Filter entered characters. */
	doKeyPress: function(e) {
		if (e.ctrlKey) return true; // Allow copy/paste
		var chr = String.fromCharCode(e.charCode == undefined ? e.keyCode : e.charCode);
		return (chr <= ' ' || chr == ':' || chr == '-' || chr == popUpCal.dateFormat.charAt(2) || (chr >= '0' && chr <= '9')); // only allow numbers and separator
	},
	
	/* Attach the calendar to an input field. */
	connectCalendar: function(target) {
		var $input = $(target);
		$input.after('<span class="calendar_append">' + this.appendText + '</span>');

		var pattern = $input.data('pattern') || $input.attr('PHpattern');
		if (pattern) { this.dateFormat = pattern; }

		if (this.autoPopUp == 'focus' || this.autoPopUp == 'both') { // pop-up calendar when in the marked fields
			$input.focus(this.showFor);
		}
		if (this.autoPopUp == 'button' || this.autoPopUp == 'both') { // pop-up calendar when button clicked
			$input
				.wrap('<span class="calendar_wrap"></span>')
				.after(this.buttonImageOnly ? '<img class="calendar_trigger" src="' + 
				this.buttonImage + '" alt="' + this.buttonText + '" title="' + this.buttonText + '"/>' :
				'<button class="calendar_trigger">' + (this.buttonImage != '' ? 
				'<img src="' + this.buttonImage + '" alt="' + this.buttonText + '" title="' + this.buttonText + '"/>' : 
				this.buttonText) + '</button>');
			$((this.buttonImageOnly ? 'img' : 'button') + '.calendar_trigger', $input.parent('span')).bind('click',this.showFor);
		}
		$input.keydown(this.doKeyDown).keypress(this.doKeyPress);
	},
	
	/* Enable the input field(s) for entry. */
	enableFor: function(inputs) {
		inputs = (inputs.jquery ? inputs : $(inputs));
		inputs.each(function() {
			this.disabled = false;
			$('../button.calendar_trigger', this).each(function() { this.disabled = false; });
			$('../img.calendar_trigger', this).each(function() { $(this).css('opacity', '1.0'); });
			var $this = this;
			popUpCal.disabledInputs = $.map(popUpCal.disabledInputs, 
				function(value) { return (value == $this ? null : value); }); // delete entry
		});
		return false;
	},
	
	/* Disable the input field(s) from entry. */
	disableFor: function(inputs) {
		inputs = (inputs.jquery ? inputs : $(inputs));
		inputs.each(function() {
			this.disabled = true;
			$('../button.calendar_trigger', this).each(function() { this.disabled = true; });
			$('../img.calendar_trigger', this).each(function() { $(this).css('opacity', '0.5'); });
			var $this = this;
			popUpCal.disabledInputs = $.map(popUpCal.disabledInputs, 
				function(value) { return (value == $this ? null : value); }); // delete entry
			popUpCal.disabledInputs[popUpCal.disabledInputs.length] = this;
		});
		return false;
	},
	
	/* Connect behaviours to the calendar. */
	setupActions: function() {
		if (this.dateFormat.indexOf('hh') != -1) {
			$('#calendar_hour').change(function() {	//change hour
				popUpCal.selecting = false;
				popUpCal.selectedHour = this.options[this.selectedIndex].value - 0;
				popUpCal.adjustDate(); 
				popUpCal.input.val(popUpCal.formatDate(popUpCal.selectedDay, popUpCal.selectedMonth, popUpCal.selectedYear, popUpCal.selectedHour, popUpCal.selectedMinute));
			}).click(this.selectMonthYear);
			$("#calendar_minute").change(function() {	//change minute
				popUpCal.selecting = false;
				popUpCal.selectedMinute = this.options[this.selectedIndex].value - 0;
				popUpCal.adjustDate(); 
				popUpCal.input.val(popUpCal.formatDate(popUpCal.selectedDay, popUpCal.selectedMonth, popUpCal.selectedYear, popUpCal.selectedHour, popUpCal.selectedMinute));
			}).click(this.selectMonthYear);
		}
		$('#calendar_clear').click(function() { // clear button link
			popUpCal.clearDate();
		});
		$('#calendar_close').click(function() { // close button link
			popUpCal.hideCalendar(popUpCal.speed);
		});
		$('#calendar_prev').click(function() { // setup navigation links
			popUpCal.adjustDate(-1, 'M'); 
		});
		$('#calendar_next').click(function() {
			popUpCal.adjustDate(+1, 'M'); 
		});
		$('#calendar_current').click(function() { // back to today
			popUpCal.selectedDay = new Date().getDate();
			popUpCal.selectedMonth = new Date().getMonth();
			popUpCal.selectedYear = new Date().getFullYear();
			popUpCal.adjustDate(); 
		});
		$('#calendar_newMonth').change(function() { // change month
			popUpCal.selecting = false;
			popUpCal.selectedMonth = this.options[this.selectedIndex].value - 0;
			popUpCal.adjustDate(); 
		}).click(this.selectMonthYear);
		$('#calendar_newYear').change(function() { // change year
			popUpCal.selecting = false;
			popUpCal.selectedYear = this.options[this.selectedIndex].value - 0;
			popUpCal.adjustDate(); 
		}).click(this.selectMonthYear);
		$('.calendar_titleRow a').click(function() { // change first day of week
			for (var i = 0; i < 7; i++) {
				if (popUpCal.dayNames[i] == this.firstChild.nodeValue) {
					popUpCal.firstDay = i; 
				}
			}
			popUpCal.showCalendar();
		});
		$('.calendar_daysRow td:has(a)').hover( // highlight current day
			function() {
				$(this).addClass('calendar_daysCellOver');
			}, function() {
				$(this).removeClass('calendar_daysCellOver');
		})
		.click(function() { // select day
			popUpCal.selectedDay = $("a",this).html();
			popUpCal.selectDate();
		});
		
	},
	
	/* Hide the calendar from view. */
	hideCalendar: function(speed) {
		if (this.popUpShowing) {
			$('#calendar_div').hide(speed);
			this.popUpShowing = false;
			this.lastInput = null;
		}
	},
	
	/* Restore input focus after not changing month/year. */
	selectMonthYear: function() { 
		if (popUpCal.selecting) {
			popUpCal.input[0].focus(); 
		}
		popUpCal.selecting = !popUpCal.selecting;
	},
	
	/* Erase the input field and hide the calendar. */
	clearDate: function() {
		this.hideCalendar(this.speed);
		this.input.val('');		
	},
	
	/* Close calendar if clicked elsewhere. */
	checkExternalClick: function(event) {
		if (popUpCal.popUpShowing) {
			var node = event.target;
			var cal = $('#calendar_div')[0];
			while (node && node != cal && node.className != 'calendar_trigger') {
				node = node.parentNode;
			}
			if (!node) {
				popUpCal.hideCalendar();
			}
		}
	},
	
	/* Set as customDate function to prevent selection of weekends. */
	noWeekends: function(date) {
		var day = date.getDay();
		return [(day > 0 && day < 6), ''];
	},
	
	/* Ensure numbers are not treated as octal. */
	trimNumber: function(value) {
		if (value == '')
			return '';
		while (value.charAt(0) == '0') {
			value = value.substring(1);
		}
		return value;
	},
	
	/* Adjust one of the date sub-fields. */
	adjustDate: function(offset, period, dontShow) {
		if (period == 'M' && this.selectedDay > 28 && this.selectedMonth < 10) {
			this.selectedDay = this.getDaysInMonth(this.selectedYear, this.selectedMonth+offset);
		}
		if (this.dateFormat.indexOf('hh') != -1) {
			var date = new Date(this.selectedYear + (period == 'Y' ? offset : 0), 
			this.selectedMonth + (period == 'M' ? offset : 0), 
			this.selectedDay + (period == 'D' ? offset : 0),
			this.selectedHour + (period == 'H' ? offset : 0),
			this.selectedMinute + (period == 'MIN' ? offset : 0));
		} else {
			var date = new Date(this.selectedYear + (period == 'Y' ? offset : 0), 
			this.selectedMonth + (period == 'M' ? offset : 0), 
			this.selectedDay + (period == 'D' ? offset : 0));
		}
		// ensure it is within the bounds set
		date = (this.minDate && date < this.minDate ? this.minDate : date);
		date = (this.maxDate && date > this.maxDate ? this.maxDate : date);
		this.selectedDay = date.getDate();
		this.selectedMonth = date.getMonth();
		this.selectedYear = date.getFullYear();
		if (this.dateFormat.indexOf('hh') != -1) {
			this.selectedHour = date.getHours();
			this.selectedMinute = date.getMinutes();
		}
		
		if (!dontShow) {
			this.showCalendar();
		}
	},

	/* Find the number of days in a given month. */
	getDaysInMonth: function(year, month) {
		return 32 - new Date(year, month, 32).getDate();
	},
	
	/* Find the day of the week of the first of a month. */
	getFirstDayOfMonth: function(year, month) {
		return new Date(year, month, 1).getDay();
	}
	
};

/* Attach the calendar to a jQuery selection. */
$.fn.calendar = function(settings) {
	// customise the calendar object
	$.extend(popUpCal, settings || {});
	// attach the calendar to each nominated input element
	return this.each(function() {
		if (this.nodeName.toLowerCase() == 'input') {
			popUpCal.connectCalendar(this);
		}
	});
};

/* Initialise the calendar. */
$(document).ready(function() {
   popUpCal.init();
});
