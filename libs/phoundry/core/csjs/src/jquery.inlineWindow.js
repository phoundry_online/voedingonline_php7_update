(function($){
	var zindex = 0;
	
	function InlineWindow(options) {
		var self = this;
		this.node = $('<div class="inlineWindow normal" style="display:none"></div>');
		
		// get the highest z-index of the page
		zindex = Math.max.apply(Math, $('*').map(function() {
			return parseInt($(this).css('z-index'), 10) || 0;
		}).get());
		
		if(options.modal) {
			this.modal = $('<div class="inlineWindow-overlay"></div>').appendTo('body')
			.css({zIndex: ++zindex, position: 'absolute', top: 0, left: 0});
			this.node.addClass('modal');
		}
		
		this.node.css({position:'absolute', zIndex: ++zindex}).
		bind('mousedown', function(){
			$(this).css('z-index', ++zindex);
		});

		this.titlebar = $('<div class="titlebar" unselectable="on"></div>')
		.appendTo(this.node)
		.bind('dblclick', function(){ self.maximize(); });
		if(options.draggable) {
			this.titlebar.bind('mousedown', function(e){
				//self.titlebar.css('cursor','move');
				if(self.isMaximized()) {
					return
				}
				e = e ? e : window.event;
				var lastMouseX = e.clientX,
				lastMouseY = e.clientY,
				moveCatcher = false;
				
				if(options.url) {
					var moveCatcher = $('<div>'),
					iframe = self.node.find('iframe');
					moveCatcher.css({
						position: 'absolute',
						width: '100%',
						height: '100%'
					});
					self.node.find('.content').prepend(moveCatcher);
				}
			
				$(document).
				bind('mousemove.inlineWindow', function(e){
					e = e ? e : window.event;
					var
					newx = parseInt(self.node.css('left'), 10) + (e.clientX - lastMouseX),
					newy = parseInt(self.node.css('top'), 10) + (e.clientY - lastMouseY);
					lastMouseX = e.clientX;
					lastMouseY = e.clientY;
					self.moveTo(newx, newy);
				}).
				bind('mouseup.inlineWindow', function(){
					//self.titlebar.css('cursor','default');
					$(this).unbind('.inlineWindow');
					if(moveCatcher) {
						moveCatcher.remove();
					}
				});
			});
		}
		
		if(options.windowTitle) {
			this.titlebar.html('<span id="windowTitle">'+options.windowTitle+'</span>');
		}
		if(options.maximizeButton) {
			$('<a href="#" class="maximize"></a>').appendTo(this.titlebar).
			bind('click', function(){ self.maximize(); return false; });
		}
		if(options.closeButton) {
			$('<a href="#" class="close"></a>').appendTo(this.titlebar).
			bind('click', function(){ self.close(); return false; });
		}
		this.content = $('<div class="content" style="overflow:hidden"></div>').appendTo(this.node);
		if(options.content) {
			this.content.html(options.content);
		}
		else if(options.url) {
			var iframe = $('<iframe frameborder="0" width="100%" height="100%"></iframe>');
			this.content.append(iframe);
			iframe.attr('src', options.url);
		}
		
		this.statusbar = $('<div class="statusbar"></div>').appendTo(this.node);
		if(options.resizeable) {
			this.resizeHandle = $('<div class="resize" unselectable="on"></div>').
			appendTo(this.statusbar).
			bind('mousedown', function(e){
				if(self.isMaximized()) {
					return;
				}
				e = e ? e : window.event;
				var lastMouseX = e.clientX,
				lastMouseY = e.clientY,
				moveCatcher = false;
				
				if(options.url) {
					var moveCatcher = $('<div>'),
					iframe = self.node.find('iframe');
					moveCatcher.css({
						position: 'absolute',
						width: '100%',
						height: '100%'
					});
					self.node.find('.content').prepend(moveCatcher);
				}
				
				$(document).
				bind('mousemove.inlineWindow', function(e){
					e = e ? e : window.event;
					var
					neww = parseInt(self.node.css('width'), 10) + (e.clientX - lastMouseX),
					newh = parseInt(self.node.css('height'), 10) + (e.clientY - lastMouseY);
					lastMouseX = e.clientX;
					lastMouseY = e.clientY;
					self.resize(neww, newh);
				}).
				bind('mouseup.inlineWindow', function(){
					$(this).unbind('.inlineWindow');
					if(moveCatcher) {
						moveCatcher.remove();
					}
				});
			});
		}
		
		this.closed = false;
		
		this.node.appendTo('body').fadeIn(250);
		
		var width = options.width,
		height = options.height,
		left = options.left,
		top = options.top;


		var $win = $(window), resizeInner = true;
		if(width < 0) {
			left = Math.round(Math.abs(width)/2);
			width += $win.width();
		}
		if(height < 0) {
			top = Math.round(Math.abs(height)/2);
			height += $win.height();
			resizeInner = false;
		}
		
		this.moveTo(left, top);
		resizeInner ? this.resizeInner(width, height) :
			this.resize(width, height);
		
		if(options.maximize) {
			this.maximize();
		}
	};
	
	InlineWindow.prototype = {
		moveTo : function(left, top) {
			if(this.closed) {
				throw new Error("Window is closed so it can't be moved");
			}
			this.node.css({left: left+'px', top: top+'px'});
			this.node.triggerHandler('moveto.inlineWindow');
			return this;
		},
		resize : function(width, height) {
			if(width < 80) {
				width = 80;
			}
			if(height < 38) {
				height = 38;
			}
			if(this.closed) {
				throw new Error("Window is closed so it can't be resized");
			}
			
			this.node.css({width: width+(jQuery.support.boxModel?0:2)+'px', height: height+(jQuery.support.boxModel?0:2)+'px'});
			
			this.content.css({
				width: width+'px',
				height: height-(this.titlebar.height() +
					this.statusbar.height())+'px'});
			this.node.triggerHandler('resize.inlineWindow');

			// For IE6 & 7:
			this.node.find('iframe').css({ width:width+'px', height:(height-this.statusbar.height())+'px'});

			return this;
		},
		resizeInner : function(width, height) {
			return this.resize(width,
				height + this.titlebar.height() + this.statusbar.height());
		},
		maximize : function() {
			if(this.closed) {
				throw new Error("Window is closed so it can't be maximized");
			}
			if(this.isMaximized()) {
				this.resize(this._lastSize.width, this._lastSize.height);
				this.moveTo(this._lastSize.left, this._lastSize.top);
				delete this._lastSize;
				$(window).unbind('resize.inlineWindow', this._keepMaximized);
				delete this._keepMaximized;
				this.node.addClass('normal');
			}
			else {
				var self = this, win = $(window), offset = this.node.offset();
				this._lastSize = {
						left: offset.left,
						top: offset.top,
						width: this.node.width(),
						height: this.node.height()};
				this.moveTo(0, 0);
				this._keepMaximized = function(){
					// First keep space for the scrollbars
					self.resize(win.width() - 20, win.height() - 20);
					self.resize(win.width(), win.height());
				};
				win.bind('resize.inlineWindow', this._keepMaximized);
				this._keepMaximized();
				this.node.removeClass('normal');
			}
			this.node.triggerHandler('maximize.inlineWindow');
		},
		isMaximized : function() {
			return ('_lastSize' in this && '_keepMaximized' in this);
		},
		close : function() {
			if('_keepMaximized' in this) {
				$(window).unbind('resize.inlineWindow', this._keepMaximized);
			}
			if('modal' in this) {
				this.modal.remove();
			}
			/*
			if(this.closed) {
				throw new Error("Window is closed so it can't be closed");
			}
			*/
			this.node.triggerHandler('close.inlineWindow');
			this.node.remove();
			this.closed = true;
		}
	};
	
	$.fn.inlineWindow = function(options) {
		// Cast to integer:
		if (options.left)   options.left   = parseInt(options.left,10);
		if (options.top)    options.top    = parseInt(options.top,10);
		if (options.width)  options.width  = parseInt(options.width,10);
		if (options.height) options.height = parseInt(options.height,10);
		if (options.width == 0 && options.height == 0) {
			options.maximize = true;
			options.maximizeButton = false;
		}
		/*
		if(this.length) {
			return this.filter('a').each(function(){
				var opts = $.extend({}, $.fn.inlineWindow.defaults, options);
				if(!opts.url && $(this).is('a[href]:not([href^=#])')) {
					opts.url = $(this).attr('href');
					
					if(!opts.windowTitle && $(this).is('[title]')) {
						opts.windowTitle = $(this).attr('title');
					}
				}
				
				$(this).bind('click', function(e){
					e.preventDefault();
					$(this).data('inlineWindow', new InlineWindow(opts));
					return false;
				});
			}).end();
		}
		else {
		*/
			var jqW = $(window), ww = jqW.width(), wh = jqW.height();
			if (!options.left && !options.top) {
				if (options.event) {
					options.left = options.event.clientX; options.top = options.event.clientY;
				}
				else {
					// Center popup in viewport:
					options.left = (ww - options.width) / 2;
					options.top = (wh - options.height) / 2;
				}
			}

			// Make sure we stay within the viewport
			if (options.left + options.width > ww) options.left -= options.width;
			if (options.left < 0) options.left = 0;

			if (options.top + options.height > wh) options.top -= options.height;
			if (options.top < 0) options.top = 0;

			return new InlineWindow($.extend({}, $.fn.inlineWindow.defaults, options));
		//}
	};
	
	$.inlineWindow = $.fn.inlineWindow;
	
	$.fn.inlineWindow.defaults = {
		windowTitle : '',
		content : '',
		url : '',
		modal: false,
		maximize: false,
		maximizeButton : true,
		closeButton : true,
		statusBar : true,
		draggable : true,
		resizeable : true,
		width : 200,
		height : 200,
		left : 0,
		top : 0,
		event: {}
	};

	
	// Backward compatible layer with $.fn.InlinePopup
	$.fn.InlinePopup = function(opts) {
		var options = {};

		if(opts.width) options.width = opts.width;
		if(opts.height) options.height = opts.height;
		if(opts.title) options.windowTitle = opts.title;
		if(opts.modal) options.modal = opts.modal;
		if(opts.event) options.event = opts.event;
		if(opts.html) options.content = opts.html;
		if(opts.url) options.url = opts.url;
		
		window.popup = $.fn.inlineWindow(options);
		
		return window.popup;
	};
})(jQuery);
