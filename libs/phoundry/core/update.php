<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'update');

	$RID = $_GET['RID'];

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);
	
	// If versioning is enabled load the corresponding version
	if (!empty($phoundry->extra['versioning'])) {
		$service = $phoundry->getVersioningService();
		if (!isset($_GET['_versioning']['parent_id'])) {
			$version = $service->getLatestVersion($_GET['TID'], $RID);
			if ($version) {
				$_REQUEST['_versioning']['parent_id'] = $version->id;
				$_GET['_versioning']['parent_id'] = $version->id;
				$phoundry->loadVersion($version);
			}
		}
		else {
			$version = $service->getVersion($_GET['_versioning']['parent_id']);
			if ($version && $version->record_id === $RID) {
				$phoundry->loadVersion($version);
			}
		}
	}
   
	// Execurte 'pre-updateForm' event:
	$eventRes = $phoundry->doEvent('pre-updateForm', $RID);
	if ($eventRes !== true) {
		PH::phoundryErrorPrinter($eventRes, true);
	}

	$pageTitle = $phoundry->getPageHeader('update', word(7));

	// Record locks?
	$onunload = '';
	if ($PHprefs['useLocks']) {
		$onunload = 'onunload="top.doLock(false,\'' . QS(1) . '\')"';
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="<?= preg_replace('/_\d+$/', '', $phoundry->name) ?>.update" />
<title><?= $pageTitle['title'] ?></title>
<?php
	// Assemble page contents:
	$page = $phoundry->updatePage($RID);
?>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<?php
	foreach($page['cssFiles'] as $cssFile) {
		print '<link rel="stylesheet" href="' . $cssFile . '" type="text/css" />' . "\n";
	}
?>
<script type="text/javascript" src="csjs/global.js.php?<?= QS(1) ?>"></script>
<?php
	foreach($page['jsFiles'] as $jsFile) {
		print '<script type="text/javascript" src="' . $jsFile . '"></script>' . "\n";
	}
?>
<script type="text/javascript">

var TID = <?=(int) $TID ?>,
	RID = <?=json_encode($RID) ?>;

<?php
	if ($PHprefs['useLocks']) {
		print "top.doLock(true, '" . QS(0) . "');\n";
	}

	foreach($page['jsCode'] as $jsCode) {
		print $jsCode . "\n";
	}
?>

// Start javascript syntax checks
<?= $phoundry->getJSchecks('update'); ?>
// End javascript syntax checks

$(function() {
	try {parent.frames['PHmenu'].setGroupSelect(false)} catch(e) {}
	var F = $('#inputForm');
	
	$("span[id$='_cOuNtEr']").each(function() {
		showCharCount(F.find('#'+this.id.replace(/_cOuNtEr$/, ''))[0]);
	});

	F
	.bind('keypress', function(){formIsDirty=true})
	.bind('submit', function(){return checkForm(this);})
	.find(":input[type!='button']:visible:enabled:first").focus();

	<?if (!empty($phoundry->extra['versioning']) &&
		isset($phoundry->extra['versioning_message']) &&
		$phoundry->extra['versioning_message'] !== "off"):?>
	$('[data-versioning-state]').bind('click', function() {
		$('[name="_versioning[state]"]').val($(this).attr('data-versioning-state'));
	});
	var okFromPopup = false;
	window.checkFormPost = function() {
		if (okFromPopup) {
			return true;
		}
		var required = <?=json_encode(isset($phoundry->extra['versioning_message']) && $phoundry->extra['versioning_message'] === "required")?>,
			req_msg = <?=json_encode(ucfirst(word(360/* beschrijving van wijziging is verplicht */)))?>;
		$.inlineWindow({
			windowTitle : (required ? '*' : '') + <?=json_encode(ucfirst(word(359 /* beschrijving van wijziging */)))?>,
			content : '<label id="versioning-message"><textarea></textarea></label>' +
				'<input id="versioning-message-ok" type="button" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>">',
			modal: true,
			maximize: false,
			maximizeButton : false,
			closeButton : true,
			statusBar : true,
			draggable : false,
			resizeable : false,
			width : 350,
			height : 200
		});
		$('#versioning-message-ok').bind('click', function(){
			var val = $.trim($('#versioning-message textarea').val());
			if (val || !required) {
				$("[name='_versioning[message]']").val(val);
				okFromPopup = true;
				F.trigger('submit');
				okFromPopup = false;
			} else {
				alert(<?=json_encode(ucfirst(word(360/* beschrijving van wijziging is verplicht */)))?>);
			}
		});
		return false;
	};
	<?endif?>
	
	$("#versioning").bind("change", function(){
		window.location = $(this).val();
	});

	$("#trailBut").bind("click", function(){
		window.location = '<?=$PHprefs['url']?>/core/versioning/trail.php?<?=QS(false)?>';
	});

	var versioning_popup;
	$(".version_message").bind("click", function(e) {
		if (versioning_popup) {
			versioning_popup.close();
			versioning_popup = null;
		}
		else {
			versioning_popup = $.inlineWindow({
				event: e,
				content: this.title.replace(/\n/g, '<br>')
			});
		}
	});
});

</script>
</head>
<body class="frames" onbeforeunload="if(formIsDirty)return '<?= escSquote(word(79)) /* Form data changed without submission */ ?>'" <?= $onunload ?>>
<form id="inputForm" action="<?=$PHprefs['url']?>/core/updateRes.php?<?= QS(1) ?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="_editNext" value="0" />

<div id="headerFrame" class="headerFrame">
<?= getHeader($pageTitle['title'], $pageTitle['extra'], $pageTitle['filters']);
?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b><?= word(32) ?></b></td>
<td align="right">
	<?if (isset($_GET['_versioning']['parent_id']) && isset($service)):
		$ver = $service->getVersion($_GET['_versioning']['parent_id']);
		$live = $service->getLiveVersion($ver->table_id, $ver->record_id);
		$versions = $service->getTrail($ver->trail_id);
	?>
	<span title="<?=htmlspecialchars($ver->message)?>" class="version_message ptr" style="display: inline-block;">
		<?=htmlspecialchars(PH::abbr($ver->message,100))?>
	</span>
	<select id="versioning">
		<?
		$statusses = array('draft' => word(361), 'pending' => word(362),
			'published' => word(363));
		foreach ($versions as $version):
		$classes = array();
		if ($version->user_id == $PHSes->userId) {
			$classes[] = 'versioning-own';
		}
		if ($version->id == $live->id) {
			$classes[] = 'versioning-published';
		}
		
		$selected = ($version->id == $_GET['_versioning']['parent_id']) ? ' selected="selected"' : '';
		?>
		<option value="<?=htmlspecialchars($phoundry->uUrl)?>?<?=QS(true, "_versioning[parent_id]=".$version->id)?>" class="<?=htmlspecialchars(implode(" ", $classes))?>" <?=$selected?>>
			<?=$version->revision?> <?=$version->user_name?> <?=$version->date?><?if (!empty($PHprefs['enableVersioningWorkflow'])):?> (<?=$statusses[$version->status]?>)<?endif?>
		</option>
		<?endforeach?>
	</select>
	<input type="button" id="trailBut" value="<?=ucfirst(word(364 /* versies */))?>" />
	<?endif?>
	<?php
		$shownRIDs = isset($PHSes->shownRIDs[$TID]) ? $PHSes->shownRIDs[$TID] : array();
		$hasNext = false;
		if (($key = array_search($RID, $shownRIDs)) !== false) {
			if(isset($shownRIDs[$key-1])) {
				print '<input type="button" value="&lt;&lt;" onclick="document.location=\'' . $_SERVER['PHP_SELF'] . '?' . QS(1,'RID=' . urlencode($shownRIDs[$key-1])) . '\'" />';
			}
			else {
				print '<input type="button" value="&lt;&lt;" disabled="disabled" />';
			}
			if(isset($shownRIDs[$key+1])) {
				$hasNext = true;
				print '<input type="button" value="&gt;&gt;" onclick="document.location=\'' . $_SERVER['PHP_SELF'] . '?' . QS(1,'RID=' . urlencode($shownRIDs[$key+1])) . '\'" />';
			}
			else {
				print '<input type="button" value="&gt;&gt;" disabled="disabled" />';
			}
		}
	?>
	<?php include $PHprefs['distDir'] .'/layout/elements/manual_button.php' ?>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?= $page['html']; ?>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<?if (!empty($phoundry->extra['versioning'])):?>
	<input name="_versioning[message]" value="" type="hidden" />
	<input name="_versioning[state]" value="" type="hidden" />
	<?if (isset($_GET['_versioning']['parent_id'])):?>
	<input name="_versioning[parent_id]" type="hidden" value="<?=htmlspecialchars($_GET['_versioning']['parent_id'])?>" />
	<?endif?>
	<?if (!empty($PHprefs['enableVersioningWorkflow'])):?>
	<?if ($PHSes->isAdmin || PH::getAccessRights($TID, array('b'))):?>
	<input type="submit" data-versioning-state="publish" class="okBut" value="<?=ucfirst(word(351 /* Publiceer */))?>" />
	<?endif?>
	<input type="submit" data-versioning-state="pending" class="okBut" value="<?=ucfirst(word(355 /* klaarzetten */))?>" title="<?=ucfirst(word(356 /* Klaarzetten voor publicatie */))?>" />
	<input type="submit" data-versioning-state="draft" class="okBut" value="<?=ucfirst(word(357 /* opslaan */))?>" title="<?=ucfirst(word(358 /* opslaan maar nog niet klaar voor publicatie */))?>" />
	<?else:?>
	<input type="submit" data-versioning-state="publish" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>" onclick="this.form['_editNext'].value=0" />
	<?if ($hasNext):?>
	<input type="submit" data-versioning-state="publish" value="<?=htmlspecialchars(word(106))?>" onclick="this.form['_editNext'].value=1" />
	<?endif?>
	<?endif?>
	
	<?else:?>
	<input type="submit" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>" onclick="this.form['_editNext'].value=0" />
	<?php
		if ($hasNext) {
			print '<input type="submit" value="' . PH::htmlspecialchars(word(106)) . '" onclick="this.form[\'_editNext\'].value=1" />';
		}
	?>
	<?endif?>
	</td>
	<td align="right">
	<?php include 'cancelBut.php'?>
	</td>
</tr></table>
</div>

</form>
</body>
</html>
