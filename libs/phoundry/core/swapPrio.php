<?php
   $inc = @include_once('PREFS.php');
   if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
   }
	header("Content-type: text/html; charset={$PHprefs['charset']}");
   require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
	
	$TID = getTID();

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);

	$colName = escDBquote($_GET['colName']);

	$RID1 = is_numeric($_GET['RID1']) ? (int)$_GET['RID1'] : "'" . escDBquote($RID1) . "'";
	$RID2 = is_numeric($_GET['RID2']) ? (int)$_GET['RID2'] : "'" . escDBquote($RID2) . "'";

	$sql = "SELECT {$colName} FROM {$phoundry->name} WHERE {$phoundry->key} = {$RID1}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$prio1 = $db->FetchResult($cur,0,0);

	$sql = "SELECT {$colName} FROM {$phoundry->name} WHERE {$phoundry->key} = {$RID2}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$prio2 = $db->FetchResult($cur,0,0);

	if ($prio1 == $prio2) {
		if ($_GET['isup'] == 'true') {
			$prio2++;
		}
		else {
			$prio1++;
		}
	}

	$sql = "UPDATE {$phoundry->name} SET {$colName} = {$prio2} WHERE {$phoundry->key} = {$RID1}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	$sql = "UPDATE {$phoundry->name} SET {$colName} = {$prio1} WHERE {$phoundry->key} = {$RID2}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	
	print 'done';
