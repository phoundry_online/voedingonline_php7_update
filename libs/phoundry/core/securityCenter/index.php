<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	$TID = getTID();
	checkAccess($TID);

	// Determine current security settings:
	$settings = array();
	$sql = "SELECT * FROM phoundry_setting WHERE type = 'security'";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$db->FetchResultAssoc($cur,$row,$x);
		$settings[$row['name']] = $row['value'];
	}

	// Save new settings:
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ($_POST['safe_login'] != $settings['safe_login']) {
			// 'safe_login' was changed:
			$value = $_POST['safe_login'] == 0 ? 0 : 1;
			$history = date('Y-m-d H-i-s') . ' - ' . PH::getRemoteAddr() . ": Setting changed to {$value} by {$PHSes->userName}.\n";
			$sql = "UPDATE phoundry_setting SET value = {$value}, history = CONCAT(history, '" . escDBquote($history) . "') WHERE name = 'safe_login'";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$settings['safe_login'] = $value;
		}

		if ($_POST['password_reset_interval'] != $settings['password_reset_interval']) {
			// 'reset_interval' was changed:
			$value = (int)$_POST['password_reset_interval'];
			$history = date('Y-m-d H-i-s') . ' - ' . PH::getRemoteAddr() . ": Setting changed to {$value} by {$PHSes->userName}.\n";
			$sql = "UPDATE phoundry_setting SET value = {$value}, history = CONCAT(history, '" . escDBquote($history) . "') WHERE name = 'password_reset_interval'";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$settings['password_reset_interval'] = $value;
		}
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="securitySettings" />
<title><?= word(441 /* Security center */) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">

</script>
</head>
<body class="frames">
<form action="<?= $_SERVER['PHP_SELF'] . '?' . QS(1) ?>" method="post">

<div id="headerFrame" class="headerFrame">
<?= getHeader(word(441 /* Security center */)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?php if ($_SERVER['REQUEST_METHOD'] == 'POST') { ?>
<div class="info"><?= word(451 /* Settings saved */) ?></div>
<?php } ?>

<fieldset>
<legend><b class="req"><?= word(444 /* Confirmed login */) ?></b></legend>
<div class="miniinfo">
<?= word(445) ?>
</div>
<p style="font-size:150%">
	<input id="safe_login_1" type="radio" name="safe_login"<?= $settings['safe_login'] == 1 ? ' checked="checked" ' : ' ' ?>value="1"><label style="color:green" for="safe_login_1"><?= word(442 /* enable */) ?></label>
	<input id="safe_login_0" type="radio" name="safe_login"<?= $settings['safe_login'] == 0 ? ' checked="checked" ' : ' ' ?>value="0"><label style="color:red" for="safe_login_0"><?= word(443 /* disable */) ?></label>
</p>
</fieldset>

<fieldset>
<legend><b class="req"><?= word(447 /* Password reset interval */) ?></b></legend>
<div class="miniinfo">
<?= word(446) ?>
</div>
<p style="font-size:150%">
<?php
	$select = '<select name="password_reset_interval" style="font-size:100%">';
	foreach(array(0=>word(449 /* never */), 90=>word(450, '90' /* every 90 days */), 180=>word(450, '180' /* every 180 days */)) as $value=>$name) {
		$seld = $settings['password_reset_interval'] == $value ? ' selected="selected" ' : ' ';
		$select .= '<option' . $seld . 'value="' . $value . '" style="color:' . ($value == 0 ? 'red' : 'green') . '">' . $name . '</option>';
	}
	$select .= '</select>';
	print word(448, $select);
?>
</p>
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<button class="okBut" type="submit"><?= word(82 /* Ok */) ?></button>
	</td>
	<td align="right">
	</td>
</tr></table>
</div>

</form>
</body>
</html>
