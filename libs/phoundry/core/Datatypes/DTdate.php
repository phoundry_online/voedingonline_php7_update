<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTdate extends Datatype
{

	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITreadonly',
		'ITselect',
		'ITtext',
		'ITcalendar'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getExplanation() 
	{
		return word(252 /* dd-mm-jjjj */);
	}

	public function getDefaultValue() 
	{
		global $g_dateFormat;

		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}

		$dateFormat = $g_dateFormat;
		$dateFormat = str_replace(array('DD', 'MM', 'YYYY'), array('d', 'm', 'Y'), $dateFormat);
		$now = time();

		if ($this->default == '{$TODAY}' || $this->default == '{$MOD_DATE}') {
			return date($dateFormat, $now);
		}
		if (preg_match('/\{\$TODAY(([+-])([0-9]+)([MDYmsh]))?\}/', $this->default, $reg) && isset($reg[2])) {
			switch ($reg[4]) {
				case 'Y': // Year
					$diff = "date('H'),date('i'),date('s'),date('m'),date('d'),date('Y')$reg[2]$reg[3]";
					break;
				case 'M': // Month
					$diff = "date('H'),date('i'),date('s'),date('m')$reg[2]$reg[3],date('d'),date('Y')";
					break;
				case 'D': // Day
					$diff = "date('H'),date('i'),date('s'),date('m'),date('d')$reg[2]$reg[3],date('Y')";
					break;
			}
			eval("\$date = mktime($diff);");
			return date($dateFormat, $date);

		}
		return $this->default;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		global $g_dateFormat;

		$value = substr($source[$this->name],0,strlen($g_dateFormat));
		$d = (int)substr($value,strpos($g_dateFormat, 'DD'),2);
		$m = (int)substr($value,strpos($g_dateFormat, 'MM'),2);
		$y = (int)substr($value,strpos($g_dateFormat, 'YYYY'),4);

		if (!checkdate($m,$d,$y)) {
			$errors[] = word(38, langWord($this->column->description));
		}
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string A string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		global $g_dateFormat;

		$dPos = strpos($g_dateFormat, 'DD');
		$mPos = strpos($g_dateFormat, 'MM');
		$yPos = strpos($g_dateFormat, 'YYYY');
		$flen = strlen($g_dateFormat);

		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(38));
		$jscode =<<<EOJ
		function checkDTdate(desc, value) {
			var D, M, Y, tmp;
			value = value.substring(0,$flen);
			D = parseInt(value.substring($dPos,$dPos+2),10);
			M = parseInt(value.substring($mPos,$mPos+2),10)-1;
			Y = parseInt(value.substring($yPos,$yPos+4),10);
			tmp = new Date(Y,M,D);
			if ((tmp.getFullYear()==Y) && (tmp.getMonth()==M) && (tmp.getDate()==D))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1)
	{
		global $db, $g_dateFormat;

		if (preg_match('/^\{\$MOD_/', $this->default) && 
		    ($page == 'insert' || $page == 'update' || $page == 'copy')) {
			return $this->getDefaultValue();
		}

		$value = $db->FetchDateResult($cur,$row,$this->name); // $value is now in YYYY-MM-DD format

		if (empty($value)) {
			return '';
		}

		$html = PH::fromISOdate($g_dateFormat, $value);
		$show_age = isset($this->extra['show_age']) ? $this->extra['show_age'] : null;
		if (
			($page == 'export' || $page == 'search' || $page == 'view') && $show_age >= 1
		) {
			$age = $this->getAge($value);
			if ($age !== false) {
				if ($show_age == 2) {
					$html = $age;
				}
				else {
					$html .= ' (' . $age . ')';
				}
			}
		}
		
		return $html;
	}

	/**
	 * Calculate age in years
	 * 
	 * @return number|boolean return false when fails
	 */
	public function getAge($date)
	{
		$ts = strtotime($date);
		if ($ts) {
			$year = date('Y', $ts);
			$month = date('m', $ts);
			$day = date('d', $ts);
			
			$year_diff = date('Y') - $year;
			$month_diff = date('m') - $month;
			$day_diff = date('d') - $day;
			
			if ($month_diff < 0) $year_diff--;
			elseif (($month_diff == 0) && ($day_diff < 0)) $year_diff--;
			return $year_diff;
		}
		return false;
	}
	
	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1) 
	{
		global $g_dateFormat;

		if ($page == 'update' && !isset($source[$this->name])) {
			$fields[$this->name] = $this->name; return;
		}

		$value = isset($source[$this->name]) ? $source[$this->name] : '';
		if ($value === '' && !$this->column->required) {
			$fields[$this->name] = isset($this->column->DBprops['default']) ? "'" . escSquote($this->column->DBprops['default']) . "'" : 'NULL';
		}
		else {
			$fields[$this->name] = "'" . escDBquote(PH::toISOdate($g_dateFormat, $value)) . "'";
		}
	}

	public function getExtra() 
	{
		$show_age = 0;
		$adminInfo = parent::getExtra();
		if (isset($adminInfo['default'])) {
			$adminInfo['default']['help'] = true;
		}
		
		if (isset($this->extra['show_age'])) {
			$show_age = $this->extra['show_age'];
		}  
		
		$adminInfo['show_age'] = array(
			'name'	=> 'DTshow_age',
			'label'  => 'Show age?',
			'required' => true,
			'info'   => 'Do you want to show age in years?',
			'input' => array('type'=>'select', 'options'=>array(0=>'no', 1=>'yes', 2=>'age only'), 'selected'=>$show_age) 
		); 
		return $adminInfo;
	}

	public function setExtra(&$msg)
	{
		$extra = array(
			'default' => trim($_POST['DTdefault']),
			'show_age' => (int)$_POST['DTshow_age']
		);
		return $extra;
	}
	 
	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'DTdefault':
				$help =<<<EOH
<table class="view-table">
<tr>
	<th>Default value</th>
	<th>Meaning</th>
</tr>
<tr class="even">
	<td>{\$TODAY}</td>
	<td>The current date (at time of input).</td>
</tr>
<tr class="odd">
	<td nowrap="nowrap">{\$TODAY[+|-]x[Y|M|D]}</td>
	<td>The date (at time of input), minus or plus a period x.<br />
	Y = year, M = month, D = day.
	</td>
</tr>
<tr class="even">
	<td>{\$MOD_DATE}</td>
	<td>Comparable with {\$TODAY}, but now the date field will be updated
	every time the record is modified.</td>
</tr>
</table>
EOH;
				break;
			default:
				$help = 'No help found for ' . $what . '!';
				break;
		}
		return $help;
	}
}
