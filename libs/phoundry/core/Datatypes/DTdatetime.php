<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTdatetime extends Datatype
{
	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITreadonly',
		'ITselect',
		'ITtext',
		'ITcalendar'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getExplanation() 
	{
		return word(252 /* dd-mm-jjjj */) . ' hh:mm';
	}

	public function getDefaultValue() 
	{
		global $g_dateFormat;

		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}

		$dateFormat =  $g_dateFormat. ' H:i';
		$dateFormat = str_replace(array('DD', 'MM', 'YYYY'), array('d', 'm', 'Y'), $dateFormat);
		$now = time();
		if ($this->default == '{$TODAY}' || $this->default == '{$MOD_DATE}') {
			return date($dateFormat, $now);
		}
		if (preg_match('/\{\$TODAY(([+-])([0-9]+)([MDYmsh]))?\}/', $this->default, $reg) && isset($reg[2])) {
			switch ($reg[4]) {
				case 'Y': // Year
					$diff = "date('H'),date('i'),date('s'),date('m'),date('d'),date('Y')$reg[2]$reg[3]";
					break;
				case 'M': // Month
					$diff = "date('H'),date('i'),date('s'),date('m')$reg[2]$reg[3],date('d'),date('Y')";
					break;
				case 'D': // Day
					$diff = "date('H'),date('i'),date('s'),date('m'),date('d')$reg[2]$reg[3],date('Y')";
					break;
				case 'h': // Hour
					$diff = "date('H')$reg[2]$reg[3],date('i'),date('s'),date('m'),date('d'),date('Y')";
					break;
				case 'm': // Minute
					$diff = "date('H'),date('i')$reg[2]$reg[3],date('s'),date('m'),date('d'),date('Y')";
					break;
			}
			eval("\$date = mktime($diff);");
			return date($dateFormat, $date);
		}
		return $this->default;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		global $g_dateFormat;

		$value = trim($source[$this->name]);

		// Check date:
		$d = (int)substr($value,strpos($g_dateFormat, 'DD'),2);
		$m = (int)substr($value,strpos($g_dateFormat, 'MM'),2);
		$y = (int)substr($value,strpos($g_dateFormat, 'YYYY'),4);
		if (!checkdate($m,$d,$y)) {
			$errors[] = word(39, PH::langWord($this->column->description));
		}

		// Check time (empty time means midnight: 00:00:00)
		if (strlen($value) > strlen($g_dateFormat)) {
			$h = (int)substr($value,11,2);
			$m = (int)substr($value,14,2);
			if ($h < 0 || $h > 23 || $m < 0 || $m > 59) {
				$errors[] = word(39, PH::langWord($this->column->description));
			}
		}
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		global $g_dateFormat;

		$dPos = strpos($g_dateFormat, 'DD');
		$mPos = strpos($g_dateFormat, 'MM');
		$yPos = strpos($g_dateFormat, 'YYYY');

		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(39));
		$jscode =<<<EOJ
		function checkDTdatetime(desc, value) {
			var D, M, Y, h, m, s, tmp, timeOK, dateOK;
			// 1. Check date.
			D = parseInt(value.substring($dPos,$dPos+2),10);
			M = parseInt(value.substring($mPos,$mPos+2),10)-1;
			Y = parseInt(value.substring($yPos,$yPos+4),10);
			tmp = new Date(Y,M,D);
			dateOK = ((tmp.getFullYear()==Y) && (tmp.getMonth()==M) && (tmp.getDate()==D))
			// 2. Check time.
			if (value.length > 10) {
				h = value.substring(11,13);
				m = value.substring(14,16);
				tmp = new Date(0,0,1,h,m,0);
				timeOK = ((tmp.getHours()==h) && (tmp.getMinutes()==m));
			}
			else
				timeOK = true;
			if (dateOK && timeOK)
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}

    public function getValue($page, $keyval, $cur, $row, $strlength = -1)
	{
		global $db, $g_dateFormat;

		if (preg_match('/^\{\$MOD_/', $this->default) &&
		    ($page == 'insert' || $page == 'update' || $page == 'copy')) {
			return $this->getDefaultValue();
		}

		$value = $db->FetchDatetimeResult($cur,$row,$this->name); // $value is now be in YYYY-MM-DD format

		if (empty($value) || $value == '0000-00-00 00:00:00') {
			return '';
		}

		return PH::fromISOdate($g_dateFormat . ' hh:mm', $value);
	}

	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1) 
	{
		global $g_dateFormat;

		if ($page == 'update' && !isset($source[$this->name])) {
			$fields[$this->name] = $this->name; return;
		}

		$value = isset($source[$this->name]) ? $source[$this->name] : '';
		if ($value === '' && !$this->column->required) {
			$fields[$this->name] = isset($this->column->DBprops['default']) ? "'" . escSquote($this->column->DBprops['default']) . "'" : 'NULL';
		}
		else {
			$fields[$this->name] = "'" . escDBquote(PH::toISOdate($g_dateFormat . ' hh:mm', $value)) . "'";
		}
	}

	public function getExtra() 
	{
		$adminInfo = parent::getExtra();
		if (isset($adminInfo['default']))
			$adminInfo['default']['help'] = true;
		return $adminInfo;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'DTdefault':
				$help =<<<EOH
<table class="view-table">
<tr>
	<th>Default value</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$TODAY}</td>
	<td>The current date and time (at time of input).</td>
</tr>
<tr class="even">
	<td nowrap="nowrap">{\$TODAY[+|-]x[Y|M|D|h|m]}</td>
	<td>The date (at time of input), minus or plus a period x.<br />
	Y = year, M = month, D = day, h = hour, m = minute.
	</td>
</tr>
<tr class="odd">
	<td>{\$MOD_DATE}</td>
	<td>Comparable with {\$TODAY}, but now the date/time field will be updated
	every time the record is modified.</td>
</tr>
</table>
EOH;
				break;
			default:
				$help = 'No help found for ' . $what . '!';
				break;
		}
		return $help;
	}
}
