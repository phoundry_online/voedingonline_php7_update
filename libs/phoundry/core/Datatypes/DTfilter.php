<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTfilter extends Datatype
{
	public $compatibleInputtypes = array(
		'ITfilter'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}


   /**
    * @param $page(string) Either 'search', 'insert', 'update', 'view' or 'xml'
    */
   public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
      global $db;
		$value = $db->FetchResult($cur,$row,$this->name);

		switch($page) {
			case 'search':
			case 'view':
				return PH::filter2where($value);
			default:
				return $value;
		}
   }
}
