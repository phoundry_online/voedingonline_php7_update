<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTipaddress extends Datatype
{
	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITpassword',
		'ITreadonly',
		'ITselect',
		'ITtext'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	/*
	 * @param $page(string) Either 'search', 'insert', 'update', 'view' or 'xml'
	 */
	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		global $db;
		$ip = $db->FetchResult($cur,$row,$this->name);
		if (is_numeric($ip)) {
			$ip = long2ip($ip);
		}
		return $ip;
	}

	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1) 
	{
		$ip = $source[$this->name];
		if ($ip && $this->column->DBprops['type'] == 'integer') {
			// Convert IP address to int:
			$source[$this->name] = ip2long($ip);
		}

		return parent::getSQLvalue($page, $source, $fields, $extras, $oldKey);
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		if (!preg_match('/^([0-9]{1,3}\.){0,3}[0-9]{1,3}\.?$/', $source[$this->name])) {
			$errors[] = word(107, PH::langWord($this->column->description));
		}
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(107));
		$jscode =<<<EOJ
		function checkDTipaddress(desc, value) {
			if (/^([0-9]{1,3}\\.){0,3}[0-9]{1,3}\\.?\$/i.test(value))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}
}
