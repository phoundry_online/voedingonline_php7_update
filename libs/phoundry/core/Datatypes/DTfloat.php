<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTfloat extends Datatype
{
	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpriority',
		'ITradio',
		'ITreadonly',
		'ITselect',
		'ITtext',
		'ITslider'
	);

	/*
	 * Ranges explained:
	 *
	 * var range = array(-10,20); // All from -10 (inclusive) to +20 (inclusive)
	 *	var range = array(0,'*'); // 0 and up
	 * var range = array('*',0); // 0 and below
	 * var range = array('*','*'); // Anything (both negative and positive)
	 */
	public $range = null;

	public function __construct(&$column)
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}

		if (isset($this->extra['range'])) {
			$this->range = $this->extra['range'];
		}
	}

	public function getDefaultValue() 
	{
		global $db, $PHSes;

		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}

		if (preg_match('/\{\$(SELECT.*)\}$/i', $this->default, $reg)) {
			$sql = PH::evaluate($reg[1]);
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if ($db->EndOfResult($cur))
				return '';
			return (float)$db->FetchResult($cur,0,0);
		}

		switch($this->default) {
			case '{$USER_ID}':
				return (int)$PHSes->userId;
			case '{$GROUP_ID}':
				return (int)$PHSes->groupId;
			default:
				return $this->default;
		}
	}

	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1)
	{
		$fields[$this->name] = array_key_exists($this->name, $source) ? ($source[$this->name] === '' ? 'NULL' : $source[$this->name]) : 'NULL';
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		$value = $source[$this->name];
		if (!preg_match('/^-?[0-9]+([\.,][0-9]+)?$/', $value)) {
			$errors[] = word(165, PH::langWord($this->column->description));
		}
		else {
			$source[$this->name] = str_replace(',', '.', $source[$this->name]);
		}

		if (!is_null($this->range)) {
			$from = $this->range[0];
			$till = $this->range[1];

			if ($from != '*') {
				if ($value < $from) {
					$errors[] = word(112, PH::langWord($this->column->description), $from);
				}
			}

			if ($till != '*') {
				if ($value > $till) {
					$errors[] = word(113, PH::langWord($this->column->description), $till);
				}
			}
		}
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(165));
		$jscode =<<<EOJ
		function checkDTfloat(desc, value) {
			if (/^-?[0-9]+([\.,][0-9]+)?\$/.test(value))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}

	public function getExtra() 
	{
		$adminInfo = parent::getExtra();
		if (isset($adminInfo['default'])) {
			$adminInfo['default']['help'] = true;
		}
		
		$range = '0,*';
		if (!empty($this->extra)) {
			if (isset($this->extra['range'])) {
				$range = $this->extra['range'][0] . ',' . $this->extra['range'][1];
			}
		}

		$adminInfo['range'] = array(
			'name'     => 'DTrange',
			'label'    => 'Allowed range',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the allowed range (use * as a wildcard)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$range)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$extra = parent::setExtra($msg);

		// Do not check if the field has been entered; rely on Javascript here.
		if (!preg_match('/^(\*|-?[0-9]+),(\*|-?[0-9]+)$/', $_POST['DTrange'])) {
			$msg = 'Incorrect syntax for range!';
			return false;
		}
		$range = explode(',', $_POST['DTrange']);
		$from = $range[0];
		$till = $range[1];

		if ($from != '*' && $till != '*') {
			if ($from >= $till) {
				$msg = 'Range-from is greater than range-till!';
				return false;
			}
		}

		$extra['range'] = array($from,$till);

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'DTdefault':
				$help =<<<EOH
<table class="view-table">
<tr>
	<th>Default value</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$USER_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_user</tt>) of the current user.
	</td>
</tr>
<tr class="even">
	<td>{\$GROUP_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_group</tt>) of the current user.</td>
</tr>
<tr class="odd">
	<td>{\$SELECT field FROM table WHERE ....}</td>
	<td>You can enter any SQL query here, which should start with <tt>\$SELECT</tt>. The query should return only one row and one field, and the result needs to be an integer.</td>
</tr>
</table>
EOH;
				break;
			default:
				$help = 'No help found for ' . $what . '!';
				break;
		}
		return $help;
	}
}
