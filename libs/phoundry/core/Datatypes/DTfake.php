<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTfake extends Datatype
{
	public $js_code, $html_code, $view_html_code, $edit_html_code, $compatibleInputtypes = array();

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}

		if (count($this->extra) > 0) {
			$this->js_code   = $this->extra['js_code'];
			$this->html_code = $this->extra['html_code'];
			$this->view_html_code = isset($this->extra['view_html_code']) ?
				$this->extra['view_html_code'] : null;
				
			$this->edit_html_code = isset($this->extra['edit_html_code']) ?
				$this->extra['edit_html_code'] : null;
		}
	}

	public function getSQLpart($page) 
	{
		return "'" . $this->name . "'";
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1)
	{
		if($page == 'search') {
			return $this->html_code;
		}
		else if($page == 'view') {
			return $this->view_html_code;
		}
		else if($page == 'update' || $page == 'insert') {
			return $this->edit_html_code;
		}
		return '';
	}

	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1) 	
	{
		return;
	}

	public function getJScode($page) 
	{
		return $this->js_code;
	}
}
