<?php
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";
require_once "{$PHprefs['distDir']}/core/include/DB/MetaBase.php";
require_once "{$PHprefs['distDir']}/core/include/Tagging/Service.php";
require_once "{$PHprefs['distDir']}/core/include/PhoundryContentNodeService.php";

class DTXtags extends Datatype
{
	/**
	 * @var Tagging_Service
	 */
	private $tagging;
	
	/**
	 * @var PhoundryContentNodeService
	 */
	private $content_node_service;
	
	/**
	 * @var string string id of the phoundry_table
	 */
	private $string_id;
	
	/**
	 * @var int phoundry_table id
	 */
	private $tid;
	
	/**
	 * @var int record id
	 */
	private $rid;
	
	public $compatibleInputtypes = array(
		'ITXtags',
		'ITtext'
	);

	public function __construct(Column $column, array $extra = null)
	{
		parent::__construct($column, $extra);
		
		$db = new DB_MetaBase($GLOBALS['db']);
		$this->content_node_service = new PhoundryContentNodeService($db);
		$this->tagging = new Tagging_Service($db);
		
		$this->tid = isset($_GET['TID']) ? $_GET['TID'] : null;
		$this->string_id = PH::getTableStringIdById($this->tid);
		$this->rid = isset($_GET['RID']) ? $_GET['RID'] : null;
	}
	
	public function getSQLpart($page) 
	{
		return "'{$this->name}'";
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1)
	{
		return $keyval;
	}

	/**
	 * For a cross reference we shouldnt be editing the $fields array
	 */
	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1)
	{
		return;
	}
	
	/**
	 * This is called after all other datatypes are saved
	 * @param string $page
	 * @param array $source
	 * @param array $fields
	 * @param int $key
	 */
	public function customSave($page, &$source, &$fields, $key)
	{
		if ($page === "insert" || $page === "update") {
			$node = $this->getContentNode($key);
		
			$tag_names = isset($source[$this->name]) ? $source[$this->name] : array();
			if (!is_array($tag_names)) {
				$tag_names = explode(",", $tag_names);
			}
			$tag_names = array_unique(array_map("trim", $tag_names));
			
			$tags = array();
			foreach ($tag_names as $name) {
				if ($name) {
					try {
						$tag = $this->tagging->getTagByName($name);
					}
					catch (Tagging_Exception $e) {
						$tag = new Tagging_Tag(null, $name);
						$this->tagging->storeTag($tag);
					}
					$tags[] = $tag;
				}
			}
			
			$this->tagging->setTagsForContentNode($tags, $node);
		}
	}
	
	public function customSearch($keyword, $prefix, $tid, $key)
	{
		$nodes = $this->tagging->getContentNodesWithTagsLikeName(
			str_replace(array('*', '?'), array('%', '_'), $keyword),
			PH::getTableStringIdById($tid)
		);
		
		$ids = array();
		foreach ($nodes as $coll) {
			$ids[] = (int) $coll->rid;
		}

		if ($ids) {
			return sprintf('
				%s IN(%s)
				', $key, implode(",", $ids)
			);
		}
		return false;
	}

	/**
	 * Delete the associated records in the cross reference table.
	 */
	public function deleteStuff($keyName, $keyVal) 
	{
		$node = $this->content_node_service->getByStringIdAndRecordId(
			$this->string_id, $keyVal);
			
		// Delete all relations to tags
		$this->tagging->setTagsForContentNode(array(), $node);
		
		$this->content_node_service->delete($node);
	}

	public function getSelected($keyValue) 
	{
		if (isset($_REQUEST['_' . $this->name])) {
			$tag_names = explode(',', $_REQUEST['_' . $this->name]);
			return array_unique(array_map("trim", $tag_names));
		}
		
		if (!$keyValue) {
			return array();
		}
		
		$node = $this->getContentNode($keyValue);
		
		$selected = array();
		foreach ($this->tagging->getTagsForContentNode($node) as $tag) {
			$selected[] = $tag->name;
		}
		
		return $selected;
	}
	
	public function getExtra() 
	{
		return array();
	}

	public function setExtra(&$msg) 
	{
		$msg = '';
		return array();
	}
	
	public function getAllTags()
	{
		return $this->tagging->getAllTags();
	}
	
	/**
	 * Get the PhoundryContentNode for a given key
	 * @param int $key
	 * @return PhoundryContentNode
	 */
	private function getContentNode($key)
	{
		return $this->content_node_service->
			getByStringIdAndRecordId($this->string_id, $key, true);
	}
}
