<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTfile extends Datatype
{
	public $compatibleInputtypes = array(
		'ITfile'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	/**
	 * @param $page(string) Either 'search', 'insert', 'update' or 'view'
	 */
	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		global $db, $PHprefs;
		$fileName = $html = $db->FetchResult($cur,$row,$this->name);
		if ($fileName === '')
			return '';

		$fileURL = PH::urlencodeURL($PHprefs['uploadUrl'] . $fileName);
		switch($page) {
			case 'search':
				// Default: show hyperlink:
				$url =  $PHprefs['url'] . '/core/popups/viewFile.php?' . QS(1,"CID={$this->column->id}&RID={$keyval}");
				$html = '<a href="' . $url . '" class="popupWin" title="' . ucfirst(word(49)) . '|400|300|0" target="phoundry">' . $fileName . '</a>';
				if ((isset($this->extra['thumb_width']) || isset($this->extra['thumb_height'])) && preg_match('/\.(gif|png|jpg|jpeg)$/i', $fileName, $reg)) {
					// Show thumbnail
					$thumbFile = dirname($fileName) . '/thumb_' . preg_replace('/' . $reg[1] . '$/', 'png', basename($fileName));
					if (file_exists($PHprefs['uploadDir'] . $thumbFile)) {
						$url = $PHprefs['url'] . '/core/popups/viewFile.php?' . QS(1,"CID={$this->column->id}&RID={$keyval}");
						$html = '<img src="' . $PHprefs['uploadUrl'] . PH::urlencodeURL($thumbFile) . '" alt="' . $fileName . '" class="ptr popupWin" title="' . ucfirst(word(49)) . '|400|300|0|' . $url . '" />';
					}
				}
				break;
			case 'view':
				if (preg_match('/\.(gif|png|jpe?g)$/i', $fileName ? $fileName : ''))
					$html = '<img src="' . $fileURL . '" alt="' . $fileName . '" />';
				else
					$html = '<a href="' . $fileURL . '" target="phoundry">' . $fileName . '</a>';
				break;
		}
		return $html;
	}

	public function getExtra() 
	{
		global $PHprefs;

		$savedir = '';
		$max_upload_size = '';
		$select_extensions = '';
		$upload_extensions = '';
		$thumb_width = $thumb_height = '';
		$allow_upload = true;
		$allow_makedir = false;
		if (count($this->extra)) {
			if (isset($this->extra['savedir']))
				$savedir = $this->extra['savedir'];
			if (isset($this->extra['select_extensions']))
				$select_extensions = implode('|',$this->extra['select_extensions']);
			if (isset($this->extra['upload_extensions']))
				$upload_extensions = implode('|',$this->extra['upload_extensions']);
			if (isset($this->extra['thumb_width']))
				$thumb_width = $this->extra['thumb_width'];
			if (isset($this->extra['thumb_height']))
				$thumb_height = $this->extra['thumb_height'];
			if (isset($this->extra['allow_upload']))
				$allow_upload = $this->extra['allow_upload'];
			if (isset($this->extra['allow_makedir']))
				$allow_makedir = $this->extra['allow_makedir'];
			if (isset($this->extra['max_upload_size']))
				$max_upload_size = $this->extra['max_upload_size'];
		}
		$adminInfo = array();
		$adminInfo['savedir'] = array(
			'name'	=> 'DTsavedir',
			'label'  => 'Upload directory',
			'required' => false,
			'info'   => 'Enter the path of the directory where the uploaded files will be saved, relative to ' . $PHprefs['uploadDir'] . '/<br />You can use the variables {$USER_ID} and {$GROUP_ID} for personal directories.',
			'input'  => array('type'=>'text', 'size'=>40, 'maxlength'=>80, 'value'=>$savedir)
		);
		$adminInfo['select_extensions'] = array(
			'name'   => 'DTselect_extensions',
			'label'  => 'Allowed extensions',
			'required' => false,
			'info'   => 'Enter a pipe-separated list of file-extensions the user can select (gif|jpg|jpeg|png). Leave empty if you allow all extensions to be selected.',
			'input'  => array('type'=>'text', 'size'=>40, 'maxlength'=>80, 'value'=>$select_extensions)
		);
		$adminInfo['upload_extensions'] = array(
			'name'	=> 'DTupload_extensions',
			'label'	=> 'Allowed upload extensions',
			'required' => false,
			'info'	=> 'Enter a pipe-separated list of file-extensions the user can upload (gif|jpg|jpeg|png). Leave empty if you allow all extensions to be uploaded.',
			'input'  => array('type'=>'text', 'size'=>40, 'maxlength'=>80, 'value'=>$upload_extensions)
		);
		$adminInfo['thumb_width'] = array(
			'name'   => 'DTthumb_width',
			'label'  => 'Width of thumbnail',
			'required' => false,
			'info'   => 'Enter the width of the thumbnails to generate, in pixels. Leave empty if you don\'t want thumbnails to be generated.',
			'input' => array('type'=>'text', 'size'=>4, 'maxlength'=>4, 'value'=>$thumb_width)
		);
		$adminInfo['thumb_height'] = array(
			'name'   => 'DTthumb_height',
			'label'  => 'Height of thumbnail',
			'required' => false,
			'info'   => 'Enter the height of the thumbnails to generate, in pixels. Leave empty if you don\'t want thumbnails to be generated.',
			'input' => array('type'=>'text', 'size'=>4, 'maxlength'=>4, 'value'=>$thumb_height)
		);
		$allow_upload = $allow_upload ? 1 : 0;
		$adminInfo['allow_upload'] = array(
			'name'   => 'DTallow_upload',
			'label'  => 'Are uploads allowed?',
			'required' => true,
		 	'input' => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$allow_upload)
		);
		$allow_makedir = $allow_makedir ? 1 : 0;
		$adminInfo['allow_makedir'] = array(
			'name'   => 'DTallow_makedir',
			'label'  => 'Can directories within the upload directory be created?',
			'required' => true,
		 	'input' => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$allow_makedir)
		);
		$adminInfo['max_upload_size'] = array(
			'name'   => 'DTmax_upload_size',
			'label'  => 'Maximum allowed upload size (in MB)',
			'required' => false,
		 	'input' => array('type'=>'text', 'size'=>4, 'maxlength'=>4, 'value'=>$max_upload_size)
		);
		return $adminInfo;
	}
	
	public function setExtra(&$msg) 
	{
		global $PHprefs;
		$msg = '';

		$savedir = $_POST['DTsavedir'];
		// Make sure the savedir does not start with a '/':
		if (!empty($savedir) && $savedir[0] == '/') {
			$savedir = substr($savedir,1);
		}

		/*
		if (!file_exists($PHprefs['uploadDir'] . '/' . $savedir)) {
			$res = $this->column->makedirs($PHprefs['uploadDir'] . '/' . $savedir);
			if (!$res) {
				$msg = "Unable to create directory {$PHprefs['uploadDir']}/$savedir!";
				return false;
			}
		}
		*/

		$extra = array(
			'savedir' => $savedir,
			'allow_upload' => ($_POST['DTallow_upload'] == 1) ? true : false,
			'allow_makedir' => ($_POST['DTallow_makedir'] == 1) ? true : false
		);
		if (!empty($_POST['DTselect_extensions']))
			$extra['select_extensions'] = explode('|',$_POST['DTselect_extensions']);
		if (!empty($_POST['DTupload_extensions']))
			$extra['upload_extensions'] = explode('|',$_POST['DTupload_extensions']);
		if (!empty($_POST['DTthumb_width'])) {
			if (!preg_match('/^\d+$/', $_POST['DTthumb_width'])) {
				$msg = 'Thumbnail-width needs to be an integer!';
				return false;
			}
			$extra['thumb_width'] = $_POST['DTthumb_width'];
		}
		if (!empty($_POST['DTthumb_height'])) {
			if (!preg_match('/^\d+$/', $_POST['DTthumb_height'])) {
				$msg = 'Thumbnail-height needs to be an integer!';
				return false;
			}
			$extra['thumb_height'] = $_POST['DTthumb_height'];
		}
		if (!empty($_POST['DTmax_upload_size'])) {
			if (!preg_match('/^\d+(\.\d+)?$/', $_POST['DTmax_upload_size'])) {
				$msg = 'Upload size needs to be a float!';
				return false;
			}
			$extra['max_upload_size'] = $_POST['DTmax_upload_size'];
		}

		return $extra;
	}
}
