<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTemail extends Datatype
{
	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITpassword',
		'ITreadonly',
		'ITselect',
		'ITtext'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getDefaultValue() 
	{
		global $db, $PHSes;

		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}

		if (preg_match('/\{\$(SELECT.*)\}$/i', $this->default, $reg)) {
			$sql = $reg[1];
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if ($db->EndOfResult($cur))
				return '';
			$default = '';
			$nrf = $db->NumberOfColumns($cur);
			for ($x = 0; $x < $nrf; $x++) {
				if ($default != '') $default .= ' ';
				$default .= $db->FetchResult($cur,0,$x);
			}
			return $default;
		}
		switch($this->default) {
			case '{$USER_EMAIL}':
				return $PHSes->email;
			default:
				return $this->default;
		}
	}

	public function getExplanation() 
	{
		return 'user@server.com';
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		global $db, $PHprefs;
		$email = $db->FetchResult($cur,$row,$this->name);
		if (empty($email))
			return '';
		if ($page == 'search' || $page == 'view') {
			$href = "mailto:$email";
			if ($strlength != -1 && strlen($email) > $strlength) {
				if (function_exists('mb_substr')) {
					$email = mb_substr($email, 0, $strlength, 'utf-8');
				} 
				else {
					$email = substr($email, 0, $strlength, $PHprefs['charset']) . '...';
				}
			}
			return '<a href="' . $href . '">' . $email . '</a>';
		}
		else
			return $email;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		if (!preg_match("/^(([a-z0-9\xA0-\xFF\-_\.\+!#$%&\'\*\-\/=\?\^_`\{\|\}~]+)|(\"[a-z0-9\-_ \.\+!#$%&@\'\*\-\/=\?\^_`\{\|\}~]+)\")@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-z0-9\-]+\.)+([a-z]{2,6})))$/i", $source[$this->name])) {
			$errors[] = word(36, PH::langWord($this->column->description));
		}
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(36));
		$jscode =<<<EOJ
		function checkDTemail(desc, value) {
			// trim
			value = value.replace(/^\s+/,'').replace(/\s+$/,'');
			if (/^(([a-z0-9\xA0-\xFF\-_\.\+!#$%&\'\*\-\/=\?\^_`\{\|\}~]+)|("[a-z0-9\-_ \.\+!#$%&@\'\*\-\/=\?\^_`\{\|\}~]+)")@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-z0-9\-]+\.)+([a-z]{2,6})))$/i.test(value))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}

	public function getExtra() 
	{
		$adminInfo = parent::getExtra();
		if (isset($adminInfo['default']))
			$adminInfo['default']['help'] = true;
		return $adminInfo;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'DTdefault':
				$help =<<<EOH
<table class="view-table">
<tr>
	<th>Default value</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$USER_EMAIL}</td>
	<td>The e-mail address (field <tt>e_mail</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$SELECT field FROM table WHERE ....}</td>
	<td>You can enter any SQL query here, which should start with <tt>\$SELECT</tt>. The query should return only one row, and the fields that it returns will be concatenated with a space to make a default value.</td>
</tr>
</table>
EOH;
				break;
			default:
				$help = 'No help found for ' . $what . '!';
				break;
		}
		return $help;
	}

}
