<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTXchildren extends Datatype
{
	public $compatibleInputtypes = array(
		'ITXchildren',
	);
	
	public function __construct($column)
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
		$this->isXref = true;
		$this->xrefTable = $this->name;
	}
	
	public function getSQLpart($page)
	{
		return "'{$this->name}'";
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1)
	{
		global $db;
		$parent = 'id';
		if (isset($this->extra['parent'])) {
			$parent = $this->extra['parent'];
		}
		if ($page) {
			return $db->FetchResult($cur, $row, $parent);
		}
	}

    /**
     * For a cross reference we shouldnt be editing the $fields array
     * @param string $page
     * @param array $source
     * @param array $fields
     * @param array $extras
     * @param int $oldKey
     */
    public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1)
	{
		return;
	}
	
	/**
	 * This is called after all other datatypes are saved
	 * @param string $page
	 * @param array $source
	 * @param array $fields
	 * @param int $key
	 */
	public function customSave($page, &$source, &$fields, $key)
	{
	}
	
	/**
	 * Generate a where part for the search query
	 * 
	 * @param string $keyword the search string DB quoted
	 * @param string $prefix prefix to prefix column names with
	 * @param int $tid table_id of the table being searched in
	 * @param string $key Column name of the Primary used by phoundry eg 'id'
	 */
	public function customSearch(/*$keyword, $prefix, $tid, $key*/)
	{
		return false;
	}
	
	public function getExtra()
	{
		global $db;
		$sql = "SELECT
				string_id, description
			FROM
				phoundry_table
			ORDER BY name ASC";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		
		$tables = array();
		for ($i = 0; !$db->EndOfResult($cur); $i++) {
			$row = array();
			$db->FetchResultAssoc($cur, $row, $i, true);
			$tables[$row['string_id']] = PH::langWord($row['description']).
				" (".$row['string_id'].")";
		}
		
		$table = $_GET['xref'];
		if (isset($this->extra['table'])) {
			$table = $this->extra['table'];
		}

		$cols_parent = $this->getColumnsForTable($_GET['tableName']);
		
		$parent = 'id';
		if (isset($this->extra['parent'])) {
			$parent = $this->extra['parent'];
		}

		$cols_child = $this->getColumnsForTable($_GET['xref']);
		
		$child = $_GET['tableName'].'_id';
		if (isset($this->extra['child'])) {
			$child = $this->extra['child'];
		}
		
		$adminInfo = array();
		$adminInfo['table'] = array(
			'name' => 'DTtable',
			'label' => 'Table',
			'required' => true,
			'syntax' => 'text',
			'input' => array(
				'type' => 'select',
				'options' => $tables,
				'selected' => $table
			)
		);
		
		$adminInfo['parent'] = array(
			'name' => 'DTparent',
			'label' => $_GET['tableName'].' column',
			'required' => true,
			'syntax' => 'text',
			'input' => array(
				'type' => 'select',
				'options' => $cols_parent,
				'selected' => $parent
			)
		);
		
		$adminInfo['child'] = array(
			'name' => 'DTchild',
			'label' => $_GET['xref'].' parent id column',
			'required' => true,
			'syntax' => 'text',
			'input' => array(
				'type' => 'select',
				'options' => $cols_child,
				'selected' => $child
			)
		);
		
		return $adminInfo;
	}

	private function getColumnsForTable($tablename)
	{
		global $db;
		$ret = array('' => '[select]');
		$columns = array();
		$db->ListTableFields(mysql_real_escape_string($tablename), $columns) or trigger_error(
			"Cannot list table fields for table {$tablename}: "
				. $db->Error
			(),
			E_USER_ERROR
		);
		foreach ($columns as $column) {
			$ret[$column] = $column;
		}
		return $ret;
	}


	public function setExtra(&$msg) 
	{
		$msg = '';
		$extra = array(
			'table' => $_POST['DTtable'],
			'parent' => $_POST['DTparent'],
			'child' => $_POST['DTchild'],
		);

		return $extra;
	}
}
