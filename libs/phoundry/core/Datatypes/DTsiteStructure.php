<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTsiteStructure extends Datatype
{
	public $compatibleInputtypes = array(
		'ITsiteStructure',
		'ITselect',
		'ITrecursiveselect',
		'ITbigselect'
	);

	public function getDefaultValue() 
	{
		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}

		return $this->default;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors)
	{
		global $db;
		$value = (int) $source[$this->name];
		
		$sql = sprintf(
			"SELECT id FROM brickwork_site_structure WHERE id = %d",
			$value
		);
		$cur = $db->query($sql) or
			trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		
		if($db->EndOfResult($cur)) {
			$errors[] = word(11801 /* Record not found */);
		}
	}

	public function getExtraHelp($what) 
	{
		return '';
	}
}
