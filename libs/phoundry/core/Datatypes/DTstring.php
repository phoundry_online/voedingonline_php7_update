<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTstring extends Datatype
{
	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITcheckbox',
		'ITpassword',
		'ITreadonly',
		'ITselect',
		'ITyoutube',
		'ITtext',
		'ITtextarea',
		'ITtiny_mce',
		'ITmultipleselect',
		'ITcodemirror',
		'ITtwitterOauthToken',
		'ITmarkitup',
		'ITcontentBuilder'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getDefaultValue() 
	{
		global $db, $PHSes;

		if (isset($_REQUEST['_' . $this->name])) 
		{
			return $_REQUEST['_' . $this->name];
		}

		if (preg_match('/\{\$(SELECT.*)\}$/i', $this->default, $reg)) 
		{
			$sql = PH::evaluate($reg[1]);
			if (preg_match('/\{\$([a-z_][\w]+)\}/i', $sql)) 
			{
				// There's still a dollar-variable in the SQL, this will lead
				// to invalid SQL. Thus: skip default value in that case.
				return '';
			}
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if ($db->EndOfResult($cur)) 
			{
				return '';
			}
			$default = '';
			$nrf = $db->NumberOfColumns($cur);
			for ($x = 0; $x < $nrf; $x++) 
			{
				if ($default != '') { $default .= ' '; }
				$default .= $db->FetchResult($cur,0,$x);
			}
			return $default;
		}
		switch($this->default) 
		{
			case '{$USER_NAME}':
			case '{$MOD_USER_NAME}':
				return $PHSes->userName;
			case '{$USER_ID}':
				return $PHSes->userId;
			case '{$USER_EMAIL}':
				return $PHSes->email;
			case '{$GROUP_ID}':
				return $PHSes->groupId;
			case '{$UNIQID}':
				return uniqid('');
			default:
				return $this->default;
		}
	}

	/**
	 * @param $page(string) Either 'search', 'insert', 'update', 'view' or 'xml'.
	 */
	public function getValue($page, $keyval, $cur, $row, $strlen = -1) 
	{
		if (preg_match('/^\{\$MOD_/', $this->default) && ($page == 'insert' || $page == 'update')) 
		{
			return $this->getDefaultValue();
		}

		$display  = isset($this->extra['display'])  ? $this->extra['display']  : 'literal';
		$value = parent::getValue($page, $keyval, $cur, $row, $strlen);

		switch($page) 
		{
			case 'search':
				return $value != null ? strip_tags($value) : '';
			case 'view':
				if ($display == 'html') 
				{
					return $value;
				}
				elseif ($display == 'code') 
				{
					return '<pre style="margin:0">' . PH::htmlspecialchars($value) . '</pre>';
				}
				else 
				{
					return PH::htmlspecialchars($value);
				}
			case 'xml':
				return PH::htmlspecialchars($value);
			default:
				return $value;
		}
	}

	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1)
	{
		$value = isset($source[$this->name]) ? $source[$this->name] : '';
		//if (empty($value) || $value === 'NULL') {
		/*
		 * Do *NOT* use this here: if (empty($value) || $value === 'NULL')
		 * We want
		 *   UPDATE ... SET history = history WHERE id = ...
		 * We do not want
		 *   UPDATE ... SET history = NULL WHERE id = ...
		 * 
		 * Why? Because we might change the history in a pre-update event!
		 * In the latter case, this change is overwritten with NULL!
		 * 
		 */
		if ($value === '' && !$this->column->required)
		{
			$fields[$this->name] = isset($this->column->DBprops['default']) ? "'" . escSquote($this->column->DBprops['default']) . "'" : 'NULL';
			return;
		}

		if ($page == 'update' && $value == '[Dummy|0|Password]' /* Dummy password, see also ITpasword.php */) 
		{
			// User did not change password:
			$fields[$this->name] = $this->name;
			return;
		}

		switch($this->extra['encoding']) 
		{
			case 'password_hash':
				$fields[$this->name] = "'" . escDBquote(password_hash($value, PASSWORD_DEFAULT)) . "'";
				break;
			case 'md5':
				$fields[$this->name] = "'" . escDBquote(md5($value)) . "'";
				break;
			case 'sha1':
				$fields[$this->name] = "'" . escDBquote(sha1($value)) . "'";
				break;
			case 'crypt':
				$fields[$this->name] = "'" . escDBquote(crypt($value)) . "'";
				break;
			case 'wpsha1':
				// The 'WP sha1' encoding needs a user-id. Therefore, we need to insert first.
				// Result of the insert is an ID, which we can then use to set (update) the password.
				$fields[$this->name] = "''";
				$extras[] = "UPDATE {$this->column->tableName} SET {$this->name} = SHA1(CONCAT({\$RECORD_ID}, '" . PH::$pwdSalt . "','" . md5($value) . "')) WHERE id = {\$RECORD_ID}";
				break;
				
			default:
				if (isset($source[$this->name]) && is_array($source[$this->name])) 
				{
					$tmpVals = array();
					foreach($source[$this->name] as $val) 
					{
						$tmpVals[] = trim($val);
					}
					$fields[$this->name] = "'" . escDBquote(implode(',', $tmpVals)) . "'";
				}
				else 
				{
					parent::getSQLvalue($page, $source, $fields, $extras, $oldKey);
				}
		}
	}

	public function checkSyntaxPHP($page, &$source, &$errors)
	{
		if (!empty($this->extra['encoding'])) 
		{
			$password = trim($source[$this->name]);
			$desc = PH::langWord($this->column->description);
			$errors = array_merge($errors, PH::checkPasswordStrength($password, $desc));
		}
	}

	public function getExtra() 
	{
		$adminInfo = parent::getExtra();
		$encoding = '';
		$display = 'literal';
		if ($this->extra !== null && count($this->extra)) {
			if (isset($this->extra['encoding'])) {
				$encoding = $this->extra['encoding'];
			}
			if (isset($this->extra['display'])) {
				$display = $this->extra['display'];
			}
		}
		if (isset($adminInfo['default'])) {
			$adminInfo['default']['help'] = true;
		}
		$adminInfo['display'] = array(
			'name'   => 'DTdisplay',
			'label'  => 'Display',
			'required' => true,
			'info'   => 'How to display the value for this field.',
			'input'  => array('type'=>'select', 'options'=>array('literal'=>'literal', 'html'=>'html', 'code'=>'code'), 'selected'=>$display)
		);
		$adminInfo['encoding'] = array(
			'name'	=> 'DTencoding',
			'label'  => 'Hashing',
			'required' => true,
			'info'   => 'What hashing algorithm to apply before storage in database?<br>Password_hash is the actual only secure option.',
			'input'  => array('type'=>'select', 'options'=>array(''=>'[none]', 'password_hash' => 'password_hash', 'md5'=>'md5', 'sha1'=>'sha1', 'wpsha1'=>'Web Power sha1', 'crypt'=>'crypt'), 'selected'=>$encoding)
		);
		return $adminInfo;
	}
	
	public function setExtra(&$msg)
	{
		$extra = parent::setExtra($msg);
		$display  = $_POST['DTdisplay'];
		$encoding = $_POST['DTencoding'];
		if ($display == 'literal') { $display = ''; }
		if ($encoding == '[none]') { $encoding = ''; }
		$extra['display'] = $display;
		$extra['encoding'] = $encoding;
		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'DTdefault':
				$help =<<<EOH
<table class="view-table">
<tr>
	<th>Default value</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$USER_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$USER_NAME}</td>
	<td>The full name (field <tt>name</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="odd">
	<td>{\$USER_EMAIL}</td>
	<td>The e-mail address (field <tt>e_mail</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$MOD_USER_NAME}</td>
	<td>Comparable with {\$USER_NAME}, but now the field will be updated
	every time the record is modified.</td>
</tr>
<tr class="odd">
	<td>{\$GROUP_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_group</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$UNIQID}</td>
	<td>A unique id (13 charachters long).</td>
</tr>
<tr class="odd">
	<td>{\$SELECT field FROM table WHERE ....}</td>
	<td>You can enter any SQL query here, which should start with <tt>\$SELECT</tt>. The query should return only one row, and the fields that it returns will be concatenated with a space to make a default value.</td>
</tr>
</table>
EOH;
				break;
			default:
				$help = 'No help found for ' . $what . '!';
				break;
		}
		return $help;
	}
}
