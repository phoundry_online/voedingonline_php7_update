<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTgoogleMapCoords extends Datatype
{
	public $compatibleInputtypes = array(
		'ITtext'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getExplanation() 
	{
		return '52.2254335563159,6.200237274169922';
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		if (!preg_match('/^\-?\d+\.\d+,\-?\d+\.\d+$/', $source[$this->name])) {
			$errors[] = word(221, PH::langWord($this->column->description));
		}
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		$msg = escSquote(word(221));
		$jscode =<<<EOJ
		function checkDTgoogleMapCoords(desc, value) {
			if (/^\\-?\\d+\\.\\d+,\\-?\\d+\\.\\d+$/i.test(value))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}

	public function getHelperLinks($page, $key = null)
	{
		global $PHprefs;

		$url = $PHprefs['url'] . '/core/popups/googleMapCoords.php?' . QS(1,"field={$this->name}");

		return array('<a href="#" tabindex="1" onclick="makePopup(event,650,-100,\'' . ucfirst(word(222)) . '\',\'' . $url . '&amp;coords=\'+_D.forms[0][\'' . $this->name . '\'].value);return false">' . word(222) . '</a>');
	}
}
