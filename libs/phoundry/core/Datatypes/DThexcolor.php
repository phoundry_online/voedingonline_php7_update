<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DThexcolor extends Datatype
{
	public $compatibleInputtypes = array(
		'ITbigselect',
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITpassword',
		'ITreadonly',
		'ITselect',
		'ITtext',
		'ITcolor'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		global $db;
		$color = $db->FetchResult($cur,$row,$this->name);
		if (empty($color))
			return '';
		if ($page == 'search' || $page == 'view') {
			// Negate color for readability:
			$r = dechex(255 - hexdec(substr($color,1,2)));
			$g = dechex(255 - hexdec(substr($color,3,2)));
			$b = dechex(255 - hexdec(substr($color,5,2)));
			return '<div style="border:1px solid #000;width:50px;background:' . $color . ';color:#' . $r . $g . $b . ';padding:2px">' . $color . '</div>';
		}
		else
			return $color;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		if (!preg_match('/^#[ABCDEF0123456789]{6}$/i', $source[$this->name]))
			$errors[] = word(22, PH::langWord($this->column->description));
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(22));
		$jscode =<<<EOJ
		function checkDThexcolor(desc, value) {
			if (/^#[ABCDEF0123456789]{6}\$/i.test(value))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}
	
}
