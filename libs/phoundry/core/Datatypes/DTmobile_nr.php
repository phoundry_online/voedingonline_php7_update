<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTmobile_nr extends Datatype
{
	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITpassword',
		'ITreadonly',
		'ITselect',
		'ITtext'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getExplanation() 
	{
		return '+31612345678';
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		if (!preg_match("/^\+\d{10,13}$/", $source[$this->name])) {
			$errors[] = word(230, PH::langWord($this->column->description));
		}
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(230));
		$jscode =<<<EOJ
		function checkDTmobile_nr(desc, value) {
			value = value.replace(/^\s+/,'').replace(/\s+$/,'');
			if (/^\+\d{10,13}\$/.test(value))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}

	public function getExtra() 
	{
		$adminInfo = parent::getExtra();
		if (isset($adminInfo['default']))
			$adminInfo['default']['help'] = true;
		return $adminInfo;
	}
}
