<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTtime extends Datatype
{
	public $timeFormat = 'H:i';
	public $compatibleInputtypes = array(
		'ITbigselect',
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITreadonly',
		'ITselect',
		'ITtext'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getExplanation() 
	{
		return 'hh:mm';
	}

	public function getDefaultValue() 
	{
		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}

		$now = time();

		if ($this->default == '{$NOW}')
			return date($this->timeFormat, $now);
		if (preg_match('/\{\$NOW(([+-])([0-9]+)([hm]))?\}/', $this->default, $reg) && isset($reg[2])) {
			switch ($reg[4]) {
				case 'h': // Hour
					$diff = "date('H')$reg[2]$reg[3],date('i'),date('s'),date('m'),date('d'),date('Y')";
					break;
				case 'm': // minutes
					$diff = "date('H'),date('i')$reg[2]$reg[3],date('s'),date('m'),date('d'),date('Y')";
					break;
			}
			eval("\$time = mktime($diff);");
			return date($this->timeFormat, $time);
		}
		return $this->default;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		if (!preg_match("/^([0-9]{2}):([0-9]{2})$/", $source[$this->name], $reg) || $reg[0] > 23 || $reg[1] > 59)
			$errors[] = word(246, PH::langWord($this->column->description));
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(246));
		$jscode =<<<EOJ
		function checkDTtime(desc, value) {
			var re = new RegExp('^([0-9]{2}):([0-9]{2})$');
			if (!re.test(value) || RegExp.$1 > 23 || RegExp.$2 > 59)
				return '$msg'.replace(/#1/, desc);
			else
				return '';
		}
EOJ;
		return $jscode;
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1)
	{
		global $db;
		return substr($db->FetchDateResult($cur,$row,$this->name), 0, 5);
	}

	public function getExtra() 
	{
		$adminInfo = parent::getExtra();
		if (isset($adminInfo['default']))
			$adminInfo['default']['help'] = true;
		return $adminInfo;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'DTdefault':
				$help =<<<EOH
<table class="view-table">
<tr>
	<th>Default value</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$NOW}</td>
	<td>The current time (at time of input).</td>
</tr>
<tr class="even">
	<td nowrap="nowrap">{\$NOW[+|-]x[h|m]}</td>
	<td>The time (at time of input), minus or plus a period x.<br />
	h = hour, m = minute.
	</td>
</tr>
</table>
EOH;
				break;
			default:
				$help = 'No help found for ' . $what . '!';
				break;
		}
		return $help;
	}
}
