<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTfileupload extends Datatype
{
	public $compatibleInputtypes = array(
		'ITfileupload'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	/**
	 * @param $page(string) Either 'search', 'insert', 'update' or 'view'
	 */
	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		global $db, $PHprefs, $PHSes;
		$html = $fileName = $db->FetchResult($cur,$row,$this->name);
		if ($fileName === '')
			return '';

		$saveDir = PH::evaluate($this->extra['savedir']);
		$fileURL = PH::urlencodeURL($PHprefs['uploadUrl'] . '/' . $saveDir . '/' . $fileName);
		switch($page) {
			case 'search':
				// Show hyperlink
				$url =  $PHprefs['url'] . '/core/popups/viewFile.php?' . QS(1,"CID={$this->column->id}&RID={$keyval}");
				$html = '<a href="' . $url . '" class="popupWin" title="' . ucfirst(word(49)) . '|400|300|0" target="phoundry">' . $fileName . '</a>';
				if ((isset($this->extra['thumb_width']) || isset($this->extra['thumb_height'])) && preg_match('/\.(gif|png|jpg|jpeg)$/i', $fileName, $reg)) {
					// Show thumbnail
					$thumbFile = '/' . $saveDir . '/thumb_' . preg_replace('/' . $reg[1] . '$/', 'png', basename($fileName));
					if (file_exists($PHprefs['uploadDir'] . $thumbFile)) {
						$url = $PHprefs['url'] . '/core/popups/viewFile.php?' . QS(1,"CID={$this->column->id}&RID={$keyval}");
						$html = '<img src="' . $PHprefs['uploadUrl'] . PH::urlencodeURL($thumbFile) . '" alt="' . $fileName . '" class="popupWin ptr" title="' . ucfirst(word(49)) . '|400|300|0|' . $url . '" />';
					}
				}
				break;
			case 'view':
				if (preg_match('/\.(gif|png|jpg|jpeg)$/', $fileName))
					$html = '<img src="' . $fileURL . '" alt="' . $fileName . '" />';
				else
					$html = '<a href="' . $fileURL . '" target="phoundry">' . $fileName . '</a>';
				break;
		}
		return $html;
	}

	public function checkRequired($source) 
	{
		if (isset($source['old_' . $this->name]) && $source['old_' . $this->name] !== '')
			return false;
		if ($_FILES[$this->name]['error'] != 0)
			return true;
		return false;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		global $PHprefs, $PHSes;

		if ($_FILES[$this->name]['error'] == 0) { // (0 == UPLOAD_ERR_OK)
			$orgName = $_FILES[$this->name]['name'];
			// Make sure the file's extension is correct:
			if (isset($this->extra['upload_extensions'])) {
				if (!preg_match('/\.(' . implode('|', $this->extra['upload_extensions']) . ')$/i', $orgName)) {
					$errors[] = word(114, $orgName, implode(', ', $this->extra['upload_extensions']));
				}
			}

			// Make sure filename doesn't contain invalid charachters:
			if (preg_match('/:\*\?\"<>\!\//', $orgName)) {
				$errors[] = word(42, strtolower($this->column->description));
			}

			$saveDir = PH::evaluate($this->extra['savedir']);
			// Make sure file doesn't exist already:
			$newName = $PHprefs['uploadDir'] . '/' . $saveDir . '/' . $orgName;
			if (($page == 'insert' || ($page == 'update' && $_POST['old_' . $this->name] != $orgName)) && file_exists($newName)) {
				$errors[] = word(18, $orgName, strtolower($this->column->description));
			}
		}
		else {
			switch($_FILES[$this->name]['error']) {
				case 1: // UPLOAD_ERR_INI_SIZE, UPLOAD_ERR_FORM_SIZE
				case 2:
					$errors[] = 'Uploaded file ' . $_FILES[$this->name]['name'] . ' exceeds the maximum of ' . str_replace('M', 'MB', ini_get('upload_max_filesize')) . '!';
					break;
				case 3: // UPLOAD_ERR_PARTIAL
					$errors[] = 'Uploaded file ' . $_FILES[$this->name]['name'] . ' was only partially uploaded!';
					break;
			}
		}
	}	
	
	/**
	 * @param $page(string) Either insert, copy or update.
	 */
	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1) 
	{
		global $db, $PHprefs, $PHSes;
	
		$fileName = '';
		if (($page == 'update' || $page == 'copy') && !empty($source['old_' . $this->name]))
			$fileName = $source['old_' . $this->name];
		if ($_FILES[$this->name]['error'] == 0) { // (0 == UPLOAD_ERR_OK)
			$fileName = $_FILES[$this->name]['name'];
		}

		if (!$this->column->required && isset($source['none_' . $this->name]) && ($page == 'update' || $page == 'copy') && $_FILES[$this->name]['error'] == 4) {
			$fileName = '';
		}
		
		if (empty($fileName) && !$this->column->required)
			$fields[$this->name] = 'NULL';
		else
			$fields[$this->name] = "'" . escDBquote($fileName) . "'";
				
		// Delete image that was linked to this record before:
		if ( ($page == 'update' || $page == 'copy') && (
			     (!$this->column->required && isset($source['none_' . $this->name])) ||
			     ( $_FILES[$this->name]['error'] == 0) // (0 == UPLOAD_ERR_OK)
			   )
		   )
		{
			$db->GetTableIndexDefinition($this->column->tableName, 'PRIMARY', $index)
				or trigger_error("Cannot get index for table {$this->name}: " . $db->Error(), E_USER_ERROR);
			$keyName = array_keys($index['FIELDS']);
			$this->deleteStuff($keyName[0], $oldKey);
		}
		
		if ($_FILES[$this->name]['error'] == 0) { // (0 == UPLOAD_ERR_OK)
			// Save uploaded file in correct directory:
			$orgName = preg_replace('/.(asp|phtml|php|php3|php4|pl|cgi|cfml|cfm)$/i','.html',$_FILES[$this->name]['name']);
			$phpName = $_FILES[$this->name]['tmp_name'];
			
			$saveDir = PH::evaluate($this->extra['savedir']);
			$saveDir = $PHprefs['uploadDir'] . '/' . $saveDir . '/';

			if (!file_exists($saveDir)) {
				$res = $this->column->makedirs($saveDir);
	         if (!$res) {
	            $msg = "Unable to create directory {$saveDir}!";
	            return false;
				}
			}

			move_uploaded_file($phpName, $saveDir . $orgName);
			chmod($saveDir . $orgName, 0644);

			// Do we need to generate a thumbnail (in PNG format)?
			if (preg_match('/\.(gif|png|jpg|jpeg)$/i', $orgName, $reg) &&
             function_exists('imagepng') &&
            (isset($this->extra['thumb_width']) || isset($this->extra['thumb_height']))) {
				$ext = $reg[1];
				include_once "{$PHprefs['distDir']}/core/include/Thumbnail.php";
				$thumb = new Thumbnail("$saveDir/$orgName");
				if (isset($this->extra['thumb_width']) && isset($this->extra['thumb_height']))
					$thumb->sizeBoth($this->extra['thumb_width'], $this->extra['thumb_height']);
				elseif (isset($this->extra['thumb_width']))
					$thumb->sizeWidth($this->extra['thumb_width']);
				elseif (isset($this->extra['thumb_height']))
					$thumb->sizeHeight($this->extra['thumb_height']);
				$thumbName = 'thumb_' . preg_replace('/' . $ext . '$/', 'png', $orgName);
				$thumb->save("$saveDir/$thumbName", 'PNG');
				chmod("$saveDir/$thumbName", 0644);
			}
		}
	}

	/**
	 * Delete stuff that is associated with this datatypes.
	 * For normal datatypes, this doesn't do anything. For files and cross references,
	 * the associated records and files can be deleted.
	 *
	 * @author Arjan Haverkamp
	 * @param $keyVal(string) The key-value for the record this column is in.
	 */
	public function deleteStuff($keyName, $keyVal) 
	{
		global $db, $PHprefs, $PHSes;

		$saveDir = PH::evaluate($this->extra['savedir']);
		
		$sql = "SELECT {$this->name} FROM {$this->column->tableName} WHERE {$keyName} = '" . escDBquote($keyVal) . "'";
		$cur = @$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur)) {
			$fileName = $db->FetchResult($cur,0,0);

			$delFile = $PHprefs['uploadDir'] . '/' . $saveDir . '/' . $fileName;
			@unlink($delFile);

			if (preg_match('/\.(gif|png|jpg|jpeg)$/i', $fileName, $reg)) {
				$ext = $reg[1];
				$delFile = $PHprefs['uploadDir'] . '/' . $saveDir . '/thumb_' . preg_replace('/' . $ext . '$/', 'png', $fileName);
				@unlink($delFile);
			}
		}
	}

	public function getExtra() 
	{
		global $PHprefs;

		$savedir = '';
		$upload_extensions = '';
		$allow_upload = true;
		$thumb_width = $thumb_height = '';

		if (count($this->extra)) {
			if (isset($this->extra['savedir']))
				$savedir = $this->extra['savedir'];
			if (isset($this->extra['upload_extensions']))
				$upload_extensions = implode('|',$this->extra['upload_extensions']);
			if (isset($this->extra['thumb_width']))
				$thumb_width = $this->extra['thumb_width'];
			if (isset($this->extra['thumb_height']))
				$thumb_height = $this->extra['thumb_height'];
		}
		$adminInfo = array();
		$adminInfo['savedir'] = array(
			'name'	=> 'DTsavedir',
			'label'  => 'Upload directory',
			'required' => true,
			'info'   => 'Enter the path of the directory where the uploaded files will be saved, relative to ' . $PHprefs['uploadDir'] . '/<br />You can use the variables {$USER_ID} and {$GROUP_ID} for personal directories.',
			'input'  => array('type'=>'text', 'size'=>40, 'maxlength'=>80, 'value'=>$savedir)
		);
		$adminInfo['upload_extensions'] = array(
			'name'	=> 'DTupload_extensions',
			'label'	=> 'Allowed upload extensions',
			'required' => false,
			'info'	=> 'Enter a pipe-separated list of file-extensions the user can upload (gif|jpg|jpeg|png). Leave empty if you allow all extensions to be uploaded.',
			'input'  => array('type'=>'text', 'size'=>40, 'maxlength'=>80, 'value'=>$upload_extensions)
		);
		$adminInfo['thumb_width'] = array(
			'name'   => 'DTthumb_width',
			'label'  => 'Width of thumbnail',
			'required' => false,
			'info'   => 'Enter the width of the thumbnails to generate, in pixels. Leave empty if you don\'t want thumbnails to be generated.',
			'input' => array('type'=>'text', 'size'=>4, 'maxlength'=>4, 'value'=>$thumb_width)
		);
		$adminInfo['thumb_height'] = array(
			'name'   => 'DTthumb_height',
			'label'  => 'Height of thumbnail',
			'required' => false,
			'info'   => 'Enter the height of the thumbnails to generate, in pixels. Leave empty if you don\'t want thumbnails to be generated.',
			'input' => array('type'=>'text', 'size'=>4, 'maxlength'=>4, 'value'=>$thumb_height)
		);

		return $adminInfo;
	}
	
	public function setExtra(&$msg) 
	{
		global $PHprefs;
		$msg = '';

		$savedir = $_POST['DTsavedir'];
		// Make sure the savedir does not start with a '/':
		if ($savedir{0} == '/')
			$savedir = substr($savedir,1);

		if (!file_exists($PHprefs['uploadDir'] . '/' . $savedir)) {
			$res = $this->column->makedirs($PHprefs['uploadDir'] . '/' . $savedir);
			if (!$res) {
				$msg = "Unable to create directory {$PHprefs['uploadDir']}/$savedir!";
				return false;
			}
		}
		$extra = array(
			'savedir' => $savedir
		);
		if (!empty($_POST['DTupload_extensions']))
			$extra['upload_extensions'] = explode('|',$_POST['DTupload_extensions']);
		if (!empty($_POST['DTthumb_width'])) {
			if (!preg_match('/^\d+$/', $_POST['DTthumb_width'])) {
				$msg = 'Thumbnail-width needs to be an integer!';
				return false;
			}
			$extra['thumb_width'] = $_POST['DTthumb_width'];
		}
		if (!empty($_POST['DTthumb_height'])) {
			if (!preg_match('/^\d+$/', $_POST['DTthumb_height'])) {
				$msg = 'Thumbnail-height needs to be an integer!';
				return false;
			}
			$extra['thumb_height'] = $_POST['DTthumb_height'];
		}

		return $extra;
	}
}
