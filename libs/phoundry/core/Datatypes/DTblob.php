<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTblob extends Datatype
{
	public $compatibleInputtypes = array(
		'ITfileupload'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	/**
	 * @param $page(string) Either 'search', 'insert', 'update' or 'view'
	 */
	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		global $db, $PHprefs, $PHSes;
		$html = $fileName = $db->FetchResult($cur,$row,$this->name);
		if ($fileName === '')
			return '';

		$fileURL = $PHprefs['url'] . '/core/blob.php?' . QS(1,"CID={$this->column->id}");
		switch($page) {
			case 'search':
				// Show hyperlink
				$url =  $PHprefs['url'] . '/core/popups/viewFile.php?' . QS(1,"CID={$this->column->id}&RID={$keyval}");
				$html = '<a href="' . $url . '" class="popupWin" title="' . ucfirst(word(49)) . '|400|300|0" target="phoundry">' . $fileName . '</a>';
				break;
			case 'view':
				if (preg_match('/\.(gif|png|jpg|jpeg)$/', $fileName))
					$html = '<img src="' . $fileURL . '" alt="' . $fileName . '" />';
				else
					$html = '<a href="' . $fileURL . '" target="phoundry">' . $fileName . '</a>';
				break;
		}
		return $html;
	}

	public function checkRequired($source) 
	{
		if (isset($source['old_' . $this->name]) && $source['old_' . $this->name] !== '')
			return false;
		if ($_FILES[$this->name]['error'] != 0)
			return true;
		return false;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		global $PHprefs, $PHSes;

		if ($_FILES[$this->name]['error'] == 0) { // (0 == UPLOAD_ERR_OK)
			$orgName = $_FILES[$this->name]['name'];
			// Make sure the file's extension is correct:
			if (isset($this->extra['upload_extensions'])) {
				if (!preg_match('/\.(' . implode('|', $this->extra['upload_extensions']) . ')$/i', $orgName)) {
					$errors[] = word(114, $orgName, implode(', ', $this->extra['upload_extensions']));
				}
			}
		}
		else {
			switch($_FILES[$this->name]['error']) {
				case 1: // UPLOAD_ERR_INI_SIZE, UPLOAD_ERR_FORM_SIZE
				case 2:
					$errors[] = 'Uploaded file ' . $_FILES[$this->name]['name'] . ' exceeds the maximum of ' . str_replace('M', 'MB', ini_get('upload_max_filesize')) . '!';
					break;
				case 3: // UPLOAD_ERR_PARTIAL
					$errors[] = 'Uploaded file ' . $_FILES[$this->name]['name'] . ' was only partially uploaded!';
					break;
			}
		}
	}
	
	/**
	 * @param $page(string) Either insert, copy or update.
	 */
	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1) 	
	{
		global $db, $PHprefs, $PHSes, $PRODUCT;
		
		$fileName = '';
		if (($page == 'update' || $page == 'copy') && !empty($source['old_' . $this->name]))
			$fileName = $source['old_' . $this->name];
		if ($_FILES[$this->name]['error'] == 0) { // (0 == UPLOAD_ERR_OK)
			$fileName = $_FILES[$this->name]['name'];
		}

		if (!$this->column->required && isset($source['none_' . $this->name]) && ($page == 'update' || $page == 'copy') && $_FILES[$this->name]['error'] == 4) {
			$fileName = '';
		}
		
		if (empty($fileName) && !$this->column->required) {
			$fields[$this->name] = $fields[$this->name . '_blob'] = 'NULL';
		}
		else {
			$fields[$this->name] = "'" . escDBquote($fileName) . "'";
			if ($_FILES[$this->name]['error'] == 0) { // (0 == UPLOAD_ERR_OK)
				if(!is_uploaded_file($_FILES[$this->name]['tmp_name'])) {
					trigger_error("File {$this->name} was not uploaded via {$PRODUCT['name']}!", E_USER_ERROR, true);
				}
				$fileContents = file_get_contents($_FILES[$this->name]['tmp_name']);
				$fields[$this->name . '_blob'] = "'" . escDBquote($fileContents) ."'";
			}
		}
	}

	public function getExtra() 
	{
		global $PHprefs;

		$upload_extensions = '';
		$allow_upload = true;

		if (count($this->extra)) {
			if (isset($this->extra['upload_extensions']))
				$upload_extensions = implode('|',$this->extra['upload_extensions']);
		}
		$adminInfo = array();
		$adminInfo['upload_extensions'] = array(
			'name'	=> 'DTupload_extensions',
			'label'	=> 'Allowed upload extensions',
			'required' => false,
			'info'	=> 'Enter a pipe-separated list of file-extensions the user can upload (gif|jpg|jpeg|png). Leave empty if you allow all extensions to be uploaded.',
			'input'  => array('type'=>'text', 'size'=>40, 'maxlength'=>80, 'value'=>$upload_extensions)
		);

		return $adminInfo;
	}
	
	public function setExtra(&$msg) 	
	{
		global $PHprefs;
		$msg = '';

		$extra = array();
		if (!empty($_POST['DTupload_extensions']))
			$extra['upload_extensions'] = explode('|',$_POST['DTupload_extensions']);

		return $extra;
	}
}
