<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTAfilter extends Datatype
{
	public $compatibleInputtypes = array(
		'ITAfilter'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function checkRequired($value) 
	{
		return false;
	}

	// Action is either 'insert' or 'update'.
	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1)
	{
		global $db, $PHenv, $PHprefs;
	
		if (isset($source['table_id'])) {
			$sql = "SELECT name FROM phoundry_table WHERE id = " . (int)$source['table_id'];
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$tableName = $db->FetchResult($cur,0,'name');
		}
		else
			$tableName = $this->column->inputtype->extra['table'];

		// For DMdelivery:
		if (isset($PHprefs['product']) && $PHprefs['product'] == 'DMdelivery' && isset($PHenv['DMDcid'])) {
			$tableName = ($tableName == 'recipient_{$CAMPAIGN_ID}') ? $PHenv['rcptTable'] : str_replace('{$CAMPAIGN_ID}', $PHenv['DMDcid'], $tableName);
		}

		// Determine number of columns in selected table:
		$db->ListTableFields($tableName, $DBcolnames)
			or trigger_error("Cannot list table fields for table $tableName: " . $db->Error(), E_USER_ERROR);
		$nrCols = count($DBcolnames);
	
		$parts = array();
		for ($x = 0; $x < $nrCols; $x++) {
			if (!empty($source[$x . 'a_fieldName']) && !empty($source[$x . 'a_operator'])) {
				// Both field and operator have been selected.
				$parts[$x . 'a'] = array(
					'fieldname' => $source[$x . 'a_fieldName'],
					'operator'  => $source[$x . 'a_operator'],
					'value'		=> $source[$x . 'a_value']
				);
			}
			if (!empty($source[$x . 'b_fieldName']) && !empty($source[$x . 'b_operator'])) {
				// Both field and operator have been selected.
				$parts[$x . 'b'] = array(
					'fieldname' => $source[$x . 'b_fieldName'],
					'operator'  => $source[$x . 'b_operator'],
					'value'		=> $source[$x . 'b_value']
				);
			}
		}
		$fields[$this->name] = "'" . escDBquote(serialize($parts)) . "'";
		if (empty($parts) && !empty($source[$this->name])) {
			parent::getSQLvalue($page, $source, $fields, $extras, $oldKey);
		}
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		global $db;
		$value = $db->FetchResult($cur,$row,$this->name);
		if (empty($value)) {
			return '';
		}
		$data = PH::is_serialized($value) ? @unserialize($value) : $value;
		if ($data === false) {
			return $value;
		}
		return $data;
	}

}
