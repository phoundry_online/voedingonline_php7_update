<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTdate_iso extends Datatype
{

	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITreadonly',
		'ITselect',
		'ITtext',
		'ITcalendar'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getExplanation() 
	{
		return 'YYYY-MM-DD';
	}

	public function getDefaultValue() 
	{
		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}

		$now = time();

		if ($this->default == '{$TODAY}' || $this->default == '{$MOD_DATE}')
			return date('Y-m-d', $now);
		if (preg_match('/\{\$TODAY(([+-])([0-9]+)([MDYmsh]))?\}/', $this->default, $reg) && isset($reg[2])) {
			switch ($reg[4]) {
				case 'Y': // Year
					$diff = "date('H'),date('i'),date('s'),date('m'),date('d'),date('Y')$reg[2]$reg[3]";
					break;
				case 'M': // Month
					$diff = "date('H'),date('i'),date('s'),date('m')$reg[2]$reg[3],date('d'),date('Y')";
					break;
				case 'D': // Day
					$diff = "date('H'),date('i'),date('s'),date('m'),date('d')$reg[2]$reg[3],date('Y')";
					break;
			}
			eval("\$date = mktime($diff);");
			return date('Y-m-d', $date);
		}
		return $this->default;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		$value = $source[$this->name];

		preg_match('/(\d{4})-(\d{2})-(\d{2})/', $value, $reg);
		if (!checkdate($reg[2],$reg[3],$reg[1])) {
			$errors[] = word(38, PH::langWord($this->column->description));
		}
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string A string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) {
		$flen = 10;

		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(38));
		$jscode =<<<EOJ
		function checkDTdate(desc, value) {
			var D, M, Y, tmp;
			value = value.substring(0,$flen);
			D = parseInt(value.substring(8,10),10);
			M = parseInt(value.substring(5,7),10)-1;
			Y = parseInt(value.substring(0,4),10);
			tmp = new Date(Y,M,D);
			if ((tmp.getFullYear()==Y) && (tmp.getMonth()==M) && (tmp.getDate()==D))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}

	public function getValue($page, $keyval, $cur, $row, $strlength = -1)
	{
		global $db;

		if (preg_match('/^\{\$MOD_/', $this->default) && 
		    ($page == 'insert' || $page == 'update' || $page == 'copy')) {
			return $this->getDefaultValue();
		}

		$value = $db->FetchDateResult($cur,$row,$this->name); // $value is now in YYYY-MM-DD format

		if (empty($value) || $value == '0000-00-00') {
			return '';
		}

		return $value;
	}

	public function getExtra() {
		$adminInfo = parent::getExtra();
		if (isset($adminInfo['default'])) {
			$adminInfo['default']['help'] = true;
		}
		return $adminInfo;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'DTdefault':
				$help =<<<EOH
<table class="view-table">
<tr>
	<th>Default value</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$TODAY}</td>
	<td>The current date (at time of input).</td>
</tr>
<tr class="even">
	<td nowrap="nowrap">{\$TODAY[+|-]x[Y|M|D]}</td>
	<td>The date (at time of input), minus or plus a period x.<br />
	Y = year, M = month, D = day.
	</td>
</tr>
<tr class="odd">
	<td>{\$MOD_DATE}</td>
	<td>Comparable with {\$TODAY}, but now the date field will be updated
	every time the record is modified.</td>
</tr>
</table>
EOH;
				break;
			default:
				$help = 'No help found for ' . $what . '!';
				break;
		}
		return $help;
	}
}
