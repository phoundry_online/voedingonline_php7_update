<?php
global $PHprefs;

if (!isset($PHprefs)) {
    error_log('PHprefs not defined in ' . __FILE__);
    exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTurl extends Datatype
{
    public $compatibleInputtypes = array(
        'ITbigselect',
        'IThidden',
        'ITpassthru',
        'ITradio',
        'ITpassword',
        'ITreadonly',
        'ITselect',
        'ITtext'
    );

    public function __construct(&$column)
    {
        // Call parent constructor:
        if (!is_null($column)) {
            parent::__construct($column);
        }
    }

    public function getExplanation()
    {
        return false; //'http://www.server.com';
    }

    public function getValue($page, $keyval, $cur, $row, $strlength = -1)
    {
        global $db;
        $url = $db->FetchResult($cur,$row,$this->name);
        if (empty($url))
            return '';
        if ($page == 'search' || $page == 'view') {
            $href = $url;
            if (preg_match('/\.(gif|jpg|jpeg|png)$/i', $href)) {
                $url = '<img src="' . $href . '" alt="" height="50" border="0" />';
            }
            elseif ($strlength != -1 && strlen($url) > $strlength) {
                $url = substr($url, 0, $strlength) . '...';
            }
            return '<a href="' . $href . '" target="_blank">' . $url . '</a>';
        }
        else
            return $url;
    }

    /**
     * Check this Datatype's syntax.
     *
     * @param $page(string) Either 'insert' or 'update'.
     * @param $source(array) An associative array that contains the 'raw' value.
     *                       Most commonly, $_POST or $_GET are used here.
     * @param $errors(array) A reference to the array that will contain the error
     *                       messages.
     */
    public function checkSyntaxPHP($page, &$source, &$errors)
    {
        /*
		// If just 'http://' was entered (commonly the default value), make it empty:
		if (preg_match('/^(((http|https|ftp):\/\/)|(news:)|(mailto:))$/i', $source[$this->name]))
			$source[$this->name] = '';
		elseif (!preg_match('/^(((http|https|ftp):\/\/)|(news:)|(mailto:))[^\\\\]+$/i', $source[$this->name]))
			$errors[] = word(37, PH::langWord($this->column->description));
        */
    }

    /**
     * Retrieve Javascript code to syntactically check this datatype.
     *
     * @author Arjan Haverkamp
     * @param $page(string) Either 'insert' or 'update'
     * @return string of Javascript code, representing this column's syntax check
     */
    public function checkSyntaxJS($page)
    {
        // Don't forget to escape the $-character!!!
        $msg = escSquote(word(37));
        $site_identifier = (isset($_GET['site_identifier']) ? $_GET['site_identifier'] : 'default');
        $jscode =<<<EOJ
		/*
		function checkDTurl(desc, value) {
			if (/^(((http|https|ftp):\/\/)|(news:)|(mailto:))[^\\\\]+\$/i.test(value))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
		*/
		$(function() {
		    var activeClickLink = $('#select_internal_{$this->column->id}');
		    var activeInputField = $('input[name={$this->name}]');
            activeClickLink.on('click', function(e) {
                var callback = 'linkInternStructureCallback';
                window[callback] = function(url) {
                    activeInputField.val(url);
                    killPopup();
                };
                var win = makePopup(null, 580, 450, 'Link', '/phoundry//custom/brickwork/link_intern/?site_identifier={$site_identifier}&type=structure.id&callback=parent.' + callback, null, true);
            });
        });
EOJ;
        return $jscode;
    }

    public function getHelperLinks($page, $key = null)
    {
        return array(
            '<a href="#" tabindex="1" onclick="var el=_D.forms[0][\'' . $this->name . '\'];if(el.value)makePopup(event,400,400,el.value,el.value);return false">'.word(10119).'</a>',
            '<a href="#" id="select_internal_'.$this->column->id.'" tabindex="2">'.word(10150).'</a>'
        );
    }
}
