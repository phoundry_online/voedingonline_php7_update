<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Datatype.php";

class DTzipcode extends Datatype
{
	public $compatibleInputtypes = array(
		'ITbigselect', 
		'IThidden',
		'ITpassthru',
		'ITradio',
		'ITpassword',
		'ITreadonly',
		'ITselect',
		'ITtext'
	);

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors)
	{
		if (!preg_match('/' . $this->extra['format'] . '/i', $source[$this->name]))
			$errors[] = word(41, PH::langWord($this->column->description));
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @return string of Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		// Don't forget to escape the $-character!!!
		$msg = escSquote(word(41));
		$format = $this->extra['format'];
		$jscode =<<<EOJ
		function checkDTzipcode(desc, value) {
			if (/$format/i.test(value))
				return '';
			else
				return '$msg'.replace(/#1/, desc);
		}
EOJ;
		return $jscode;
	}

	public function getExtra() 
	{
		$adminInfo = parent::getExtra();

		$format = word(255); /* zipcodeFormat */
		if (count($this->extra)) {
			if (isset($this->extra['format']))
				$format = $this->extra['format'];
		}

		$adminInfo['format'] = array(
			'name'	=> 'DTformat',
			'label'  => 'Zipcode format',
			'required' => true,
			'info'   => 'Enter a regular expression for syntactically validating the zipcode (the validation is case insensitive).',
			'input'  => array('type'=>'text', 'size'=>40, 'maxlength'=>80, 'value'=>$format)
		);
		return $adminInfo;
	}


	public function setExtra(&$msg) 
	{
		$extra = parent::setExtra($msg);
		$extra['format'] = $_POST['DTformat'];

		return $extra;
	}
}
