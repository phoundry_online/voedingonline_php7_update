<?php
if (isset($_GET['popup'])) {
	$click = "parent.killPopup()";
} else if (isset($_GET['callback'])) {
    if ($_SERVER['HTTPS'] == 'on')
        $_GET['callback'] = str_replace('http:', 'https:', $_GET['callback']);
    $callback = $_GET['callback'];
	if ($callback && ($callback[0] === '/' ||
			stripos($callback, 'http://') === 0 ||
			stripos($callback, 'https://') === 0)) {
		$click = "window.location='".$_GET['callback']."';";
	}
}
$text = word(46 /* Cancel */);
if (!isset($click)) {
	if (strpos($phoundry->rUrl, $_SERVER['PHP_SELF']) === 0) {
		$click = "window.location='" . $PHprefs['customUrls']['home'] . "'";
	} else {
		$click = "window.location='" . $phoundry->rUrl . '?' . QS(0,'RID') . "'";
		$text = word(45 /* Back */);
	}
}
?>
<input id="cancelBut" type="button" value="<?= PH::htmlspecialchars($text) ?>" onclick="<?=htmlspecialchars($click)?>" />
