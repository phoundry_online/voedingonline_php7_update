<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'notify');
	$RID = $_GET['RID'];

	if (isset($PHprefs['PHenvFunction'])) {
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	}

	$DMDcampaignRelated = (isset($PHenv) && $TID > 9 && $TID <= 27);

	// Determine if there's a notification already. If so, remove it. If not, add it.

	$sql = "SELECT COUNT(*) FROM phoundry_notify WHERE user_id = " . (int)$PHSes->userId . " AND table_id = {$TID} AND keyval = '" . escDBquote($RID) . "'";
	if ($DMDcampaignRelated) {
		$sql .= " AND campaign_id = {$PHenv['DMDcid']}";
	}
	$cur = $db->Query($sql)
		or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql");

	if (isset($_GET['check'])) {
		print $db->FetchResult($cur,0,0);
		exit;
	}

	if ($db->FetchResult($cur,0,0) == 0)
	{
		$newId = PH::determineNewKeyVal('phoundry_notify', 'id');
		if ($DMDcampaignRelated) {
			$sql = "INSERT INTO phoundry_notify (id, table_id, keyval, user_id, campaign_id) VALUES ($newId, $TID, '$RID', {$PHSes->userId}, {$PHenv['DMDcid']})";
		}
		else {
			$sql = "INSERT INTO phoundry_notify (id, table_id, keyval, user_id) VALUES ($newId, $TID, '$RID', {$PHSes->userId})";
		}
		$img = 'email_new.png';
	}
	else
	{
		if ($DMDcampaignRelated) {
			$sql = "DELETE FROM phoundry_notify WHERE table_id = $TID AND keyval = '" . escDBquote($RID) . "' AND user_id = " . (int)$PHSes->userId . " AND campaign_id = {$PHenv['DMDcid']}";
		}
		else {
			$sql = "DELETE FROM phoundry_notify WHERE table_id = $TID AND keyval = '" . escDBquote($RID) . "' AND user_id = " . (int)$PHSes->userId;
		}
		$img = 'email.png';
	}
	
	$cur = $db->Query($sql)
		or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql");

	header('Content-type: image/png');
	fpassthru(fopen("icons/$img", 'r'));
?>
