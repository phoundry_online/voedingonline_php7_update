<?php

$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";

if (!isset($_GET['widget_id'])) {
	header('x', true, 404);
	exit;
}
$widget_id = $_GET['widget_id'];
if (!isset($PHprefs['widgets'][$widget_id])) {
	header('x', true, 404);
	exit;
}
$widget = $PHprefs['widgets'][$widget_id];
$cacheFile = "{$PHprefs['tmpDir']}/{$widget_id}_{$PHSes->lang}.cache";

if (file_exists($cacheFile) && (time() - filemtime($cacheFile) < 600))
{
	
}
else {

ob_start();

require_once 'google_analytics/gapi.class.php';

$ga = new gapi($widget['config']['ga_email'],$widget['config']['ga_password']);

/**
 * Note: OR || operators are calculated first, before AND &&.
 * There are no brackets () for precedence and no quotes are
 * required around parameters.
 * 
 * Do not use brackets () for precedence, these are only valid for 
 * use in regular expressions operators!
 * 
 * The below filter represented in normal PHP logic would be:
 * country == 'United States' && ( browser == 'Firefox || browser == 'Chrome')
 */

//$filter = 'country == United States && browser == Firefox || browser == Chrome';

$ga->requestReportData(
	$widget['config']['ga_profile_id'],
	array('date', 'browser'),
	array('pageviews'),
	'date',
	'',
	date('Y-m-d', strtotime('1 month ago')),
	date('Y-m-d', time()),
	1,
	1000
);

$data = array();
$colors = array(
	'880000', '008800', '000088', 'C300FF','00FFFF', 'FF0000','00FF00','0000FF',
	'440000', '004400', '000044', '444400','004444', '888800','008888','BB0000',
	'00BB00', '0000BB'
);

$max_per_date = array();

foreach($ga->getResults() as $result) {
	$browsers[$result->getBrowser()] = 0;
}

for ($i = date('Y-m-d', strtotime('1 month ago')) ; $i < date('Y-m-d') ; $i = date('Y-m-d', strtotime($i." + 1 day")))
{
	$data[$i] = array();
}

foreach($ga->getResults() as $result) {
	$data[date('Y-m-d', strtotime($result->getDate()))][$result->getBrowser()] = $result->getPageviews();
}

$chd = '&chd=t:';
$chxl = '&chxl=0:';
$chco = '&chco=';

foreach($data as $day => $date) 
{
	$chxl .= '|'.date('d', strtotime($day));
}

foreach ($browsers as $browser => $dummy) 
{	
	foreach($data as $day => $date) 
	{
		if (!isset($max_per_date[$day])) 
		{
			$max_per_date[$day] = 0;
		}

		if (isset($date[$browser]))
		{
			$chd .= $date[$browser].',';	
			$max_per_date[$day] += $date[$browser];
		}
		else
		{
			$chd .= '0,';
		}		
	}	

	$chd = rtrim($chd, ',');
	$chd .= '|';
	$color = array_pop($colors);
	$chco .= $color.",";
	$legend[$browser] = $color;
}
$chd = rtrim($chd, '|');
$chco = rtrim($chco, ',');

?>

<div style="text-align:center;padding:5px">
<?= word(341 /* Page requests in last month */) ?>:
<div style="margin-top:5px">
	<img src="http://chart.apis.google.com/chart?chs=325x110&chbh=5&chxt=x,y&chds=0,<?= max($max_per_date)+20; ?>&cht=bvs&chxp=0,0&chxl=0:|<?= date('d-m', strtotime('1 month ago')); ?>|<?= date('d-m', strtotime('3 week ago')); ?>|<?= date('d-m', strtotime('2 week ago')); ?>|<?= date('d-m', strtotime('1 week ago')); ?>|<?= date('d-m'); ?>|1:||<?= round((max($max_per_date)+20)/2); ?>|<?= max($max_per_date)+20; ?><?= $chco; ?><?= $chd; ?>" />
</div>

<div style="margin-top:5px">
<?php foreach ($legend as $browser => $color) { ?>
	<div style="float:left">
	<span style="float:left;display:block;width:10px;height:10px;border:1px solid #777;background:#<?= $color; ?>"></span>
	<span style="float:left;margin:0 4px"><?= $browser; ?></span>
	</div>
<?php } ?>
</div>

</div>

<?php

$widget_ouput = ob_get_clean();
file_put_contents($cacheFile, $widget_ouput);
}

checkAccess();
print file_get_contents($cacheFile);