<?php

$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";
if (!isset($_GET['widget_id'])) {
	header('x', true, 404);
	exit;
}
$widget_id = $_GET['widget_id'];
if (!isset($PHprefs['widgets'][$widget_id])) {
	header('x', true, 404);
	exit;
}
$widget = $PHprefs['widgets'][$widget_id];
$cacheFile = "{$PHprefs['tmpDir']}/{$widget_id}_{$PHSes->lang}.cache";

if (file_exists($cacheFile) &&  (time() - filemtime($cacheFile) < 600))
{
}
else
{

ob_start();

require_once 'google_analytics/gapi.class.php';

$ga = new gapi($widget['config']['ga_email'],$widget['config']['ga_password']);

/**
 * Note: OR || operators are calculated first, before AND &&.
 * There are no brackets () for precedence and no quotes are
 * required around parameters.
 * 
 * Do not use brackets () for precedence, these are only valid for 
 * use in regular expressions operators!
 * 
 * The below filter represented in normal PHP logic would be:
 * country == 'United States' && ( browser == 'Firefox || browser == 'Chrome')
 */

//$filter = 'country == United States && browser == Firefox || browser == Chrome';
$ga->requestReportData(
	$widget['config']['ga_profile_id'],
	array('browser'),
	array('pageviews','visits'),
	'-visits',
	'',
	date('Y-m-d', strtotime('1 month ago')),
	date('Y-m-d', time())
);

?>
<table class="view-table" style="width:320px;margin:5px">
<tr class="odd">
  <th align="left"><?= word(342 /* Browser */) ?></th>
  <th><?= word(343 /* Pageviews */) ?></th>
  <th><?= word(344 /* Visits */) ?></th>
</tr>
<?php
$class = true;
foreach($ga->getResults() as $result) {
?>
<tr class="<?= ($class = !$class) ? 'even' : 'odd' ?>">
  <td><?= $result ?></td>
  <td align="right"><?= $result->getPageviews() ?></td>
  <td align="right"><?= $result->getVisits() ?></td>
</tr>
<?php
}
?>
<tr class="<?= ($class = !$class) ? 'even' : 'odd' ?>">
  <td align="left"><b><?= word(345 /* Total pageviews */) ?></b></td>
  <td align="right"><b><?= $ga->getPageviews() ?></b></td>
  <td></td>
</tr>
<tr class="<?= ($class = !$class) ? 'even' : 'odd' ?>">
  <td align="left"><b><?= word(346 /* Total visits */) ?></b></th>
  <td></td>
  <td align="right"><b><?= $ga->getVisits() ?></b></td>
</tr>
<tr class="<?= ($class = !$class) ? 'even' : 'odd' ?>">
  <td align="left"><?= word(347 /* Last update */) ?></td>
  <td colspan="2" align="right"><?= date("d M, H:i:s", strtotime($ga->getUpdated())); ?></td>
</tr>
</table>

<?php

$widget_ouput = ob_get_clean();
file_put_contents($cacheFile, $widget_ouput);
}

checkAccess();
echo file_get_contents($cacheFile);
