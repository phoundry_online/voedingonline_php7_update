<?php
	
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";

checkAccess();

$widget_id = $_GET['widget_id'];
$widget = $PHprefs['widgets'][$widget_id];

if ($PHprefs['showNews']) 
{ 
	include $PHprefs['distDir'] . '/core/include/PhoundryNewsWindow.php';
	$html = PhoundryNewsWindow::getHTML();
	
	if (!empty($html)) 
	{
		echo $html;
	}
} 
