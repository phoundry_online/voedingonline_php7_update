<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess();
	
	$productName = strtolower($PRODUCT['name']);
	
	$type = (isset($_GET['type']) && $_GET['type'] == 'bug') ? 'bug' : 'support';
	$subject = $PRODUCT['name'];
	if ($productName == 'dmdelivery') {
		$subject .= ' ' . $DMDprefs['identifier'];
	}
	
	
	// Determine user data:
	$userName = $userEmail = $userPhone = '';
	$sql = "SELECT name, tel_nr FROM phoundry_user WHERE id = {$PHSes->userId}";
	$cur = $db->Query($sql);
	if ($cur) {
		$userName = $db->FetchResult($cur,0,'name');
		$userPhone = $db->FetchResult($cur,0,'tel_nr');
	}

	if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		// Send email:
		if ($type == 'bug') {
			$rcpt = isset($PHprefs['errorRcpts']) ? $PHprefs['errorRcpts'] : ($productName == 'dmdelivery' ? 'scripterrors@dmdelivery.com' : 'support@cherry-marketing.nl');
		} else {
			// Phoundry
            // @TODO: Is this right?
			$rcpt = 'support@phoundry.nl';
		}

		$html = '';
		if (!empty($_SESSION['error'])) 
		{
			$subject .= ' error';
			$html .= '<h3 style="color:blue;border-bottom:1px solid blue">Error data</h3><dl>';
			foreach($_SESSION['error'] as $name=>$value) 
			{
				$html .= '<dt><b>' . PH::htmlspecialchars($name) . '</b></dt>';
				$html .= '<dd><pre style="font-size:9pt;margin:0">' . PH::htmlspecialchars($value) . '</pre></dd>';
			}
			$html .= '</dl>';
			unset($_SESSION['error']);
		}
		else {
			$subject .= ' feedback';
		}
		if (isset($_POST['Type'])) {
			$subject .= '-' . $_POST['Type'];
		}
		$subject .= '-' . date('Y-m-d');

		$html .= '<h3 style="color:blue;border-bottom:1px solid blue">User provided data</h3><dl>';
		foreach($_POST as $name=>$value) 
		{
			$html .= '<dt><b>' . PH::htmlspecialchars($name) . '</b></dt>';
			$html .= '<dd><pre style="font-size:9pt;margin:0">' . PH::htmlspecialchars($value) . '</pre></dd>';
		}
		$html .= '</dl>';

		$attach = null;
		if (!empty($_FILES['Screenshot']['name'])) {
			$attach = $PHprefs['tmpDir'] . '/' . $_FILES['Screenshot']['name'];
			move_uploaded_file($_FILES['Screenshot']['tmp_name'], $attach);
		}

		PH::sendHTMLemail($rcpt, $subject, $html, $attach, $_POST['UserEmail'], null, null, array('Reply-To'=>$PHSes->email));
		if (!is_null($attach)) {
			unlink($attach);
		}

		header('Location: ' . $PHprefs['customUrls']['home'] . '?_msg=330' /* Message sent */);
		exit;
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(322) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="csjs/global.js.php?<?= QS(1) ?>&amp;noFrames"></script>
<script type="text/javascript">

function init() {
	try {_P.frames['PHmenu'].setGroupSelect(false)} catch(e) {}
	$(':submit').prop('disabled',false);

	<?php if (isset($PHprefs['product']) && strtolower($PHprefs['product']) == 'dmdelivery'): ?>
	try {
		var cid = parent.frames['PHmenu'].DMDcid;
		if (cid) {
			$("select[name='Campaign']").val(cid);
		}
	} catch(e) {};


	$('#campaign').bind('change', function(){
		var sel = $('<select/>', {id : 'mailing', 'name' : 'Mailing', 'style' : 'width:200px'});
		var newOption = jQuery('<option/>', {
			value       : 'n/a',
			text        : '<?= word(329 /* N/A */) ?>'
		}).appendTo(sel);

		var pField = $(this).parent('fieldset');
		var cid    = this.value.match('[0-9]+');
		
		try {
			cid = cid[0];
		} catch (e) {return;}

		jQuery.ajax({
			url		: '/admin/dmdelivery/mailing/campaignMailings.php',
			data		: {'cid' : cid},
			type		: 'POST',
			context	: document.body,
			success : function (response) {
				$('#mailing').remove();
				$('#mailingTextId').remove();
				
				if (0 == response) {
					return;
				}
				
				var obj = eval('(' + response + ')');
				if ($.isEmptyObject(obj)) {
					return;
				}

				jQuery.each(obj, function(key, o) {
					var newOption = jQuery('<option/>', {
						value       : '(' + o.id + ') ' + unescape(o.name),
						text        : unescape(o.name)
					}).appendTo(sel);
				});
				
				pField.append('<div id ="mailingTextId"></br>' + '<?= word(1096 /* Mailings */) ?>' + '</br></div>');
				sel.appendTo(pField);
			}
		});

	});
	<?php endif?>
	
}

</script>
</head>
<body class="frames" onload="init()">
<form enctype="multipart/form-data" action="?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="Product" value="<?= $PRODUCT['name'] . ' ' . $PRODUCT['version'] ?>" />
<input type="hidden" name="UserAgent" value="<?= $_SERVER['HTTP_USER_AGENT'] ?>" />
<input type="hidden" name="URL" value="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] ?>" />

<div id="headerFrame" class="headerFrame">
<?= getHeader(word($type == 'bug' ? 331 : 322)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b><?= word(32) ?></b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<fieldset class="field"><legend><b class="req"><?= word(99 /* User */) ?></b></legend>
<?= PH::htmlspecialchars($userName); ?>
<input type="hidden" name="UserName" value="<?= PH::htmlspecialchars($userName) ?>" />
</fieldset>

<fieldset class="field"><legend><b class="req"><?= word(98 /* Email address */) ?></b></legend>
<?= $PHSes->email; ?>
<input type="hidden" name="UserEmail" value="<?= PH::htmlspecialchars($PHSes->email) ?>" />
</fieldset>

<fieldset class="field"><legend><b><?= word(323 /* Phone number */) ?></b></legend>
<div class="miniinfo"><?= word(324 /* Please enter your phone number, so we can call you in case we need more info. */) ?></div>
<input type="text" name="UserPhone" size="60" value="<?= PH::htmlspecialchars($userPhone) ?>" />
</fieldset>

<?php if ($productName == 'dmdelivery') { ?>

<fieldset class="field"><legend><b class="req"><?= word(1000 /* Campaign */) ?></b></legend>
<select id="campaign" name="Campaign" style="width:200px">
<option value="n/a">[<?= word(329 /* N/A */) ?>]</option>
<?php
	$campaigns = DMD::getCampaignsForUser($PHSes->userId);
	foreach($campaigns as $cid=>$campaign) {
		$cName = PH::htmlspecialchars($campaign['name']);
		print '<option value="(' . $cid . ') ' . $cName . ' ">' . $cName . '</option>';
	}
?>
</select>
</fieldset>

<?php if ($type != 'bug') { ?>
<fieldset class="field"><legend><b class="req"><?= word(1863 /* Type of question */) ?></b></legend>
<input id="type0" type="radio" name="Type" value="Operational" alt="1|string" /> <label for="type0"><?= word(1864) ?></label><br />
<input id="type1" type="radio" name="Type" value="Deliverability" /> <label for="type1"><?= word(1865) ?></label><br />
<input id="type2" type="radio" name="Type" value="Coding" /> <label for="type2"><?= word(1866) ?></label><br />
<input id="type3" type="radio" name="Type" value="Webservice" /> <label for="type3"><?= word(1867) ?></label><br />
<?php if ($productName == 'dmdelivery' && $DMDprefs['licenseOwner'] == 'cn') { ?>
<input id="type5" type="radio" name="Type" value="Complaint" /> <label for="type5">投诉</label><br />
<?php } ?>
<input id="type4" type="radio" name="Type" value="Other" /> <label for="type4"><?= word(1868) ?></label>
</fieldset>

<?php }} ?>

<fieldset class="field"><legend><b class="req"><?= word($type == 'bug' ? 331 /* Bug report */ : 325 /* Question or remarks */) ?></b></legend>
<?php
	if ($type == 'bug' && $PHSes->userId == 1) 
	{// Web Power users can see their errors:
		print '<div class="WPonly">';
		print $_SESSION['error']['Error type'] . ': ' . $_SESSION['error']['Error message'] . ' in ' . $_SESSION['error']['Error file'] . ' on line ' . $_SESSION['error']['Error line'];
		print '</div>';
?>
<div class="miniinfo"><?= word(326 /* The more details you provide, the better we can help */) ?></div>
<?php } ?>
<textarea name="Question" cols="80" rows="15" alt="1|string">
<?php
	if ($type == 'bug' && !empty($_SESSION['error'])) 
	{
		print "Description of problem:\n\n\nSteps to reproduce:\n1.\n2.\n3.";
	}
	elseif ($type == 'support' && !empty($_GET['question'])) 
	{
		print PH::htmlspecialchars($_GET['question']);
	}
?>
</textarea>
</fieldset>

<fieldset class="field"><legend><b><?= word(327 /* Screenshot/Attachment */) ?></b></legend>
<div class="miniinfo"><?= word(328 /* A screenshot often helps a lot */) ?>.</div>
<input type="file" name="Screenshot" size="60" />
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td><input type="submit" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>" /></td>
	<td align="right">
	<input type="button" value="<?= PH::htmlspecialchars(word(46)) ?>" onclick="cancelMe('home')" /></td>
	</td>
</tr></table>
</div>

</form>
</body>
</html>
