<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";
   
$data = json_decode($_GET['D'], true);

if (!array_key_exists('folder', $data)) {
	die('Permission denied!');
}

if (empty($PHprefs['uploadDir'])) {
	trigger_error('$PHprefs[\'uploadDir\'] is empty', E_USER_ERROR);
	exit;
}

$rootDir = $PHprefs['uploadDir'] . '/' . PH::WPdecode($data['folder']);
if (!file_exists($rootDir)) {
	mkdir($rootDir, 0775, true);
}

$_SESSION['jsTree'] = array(
	'asciiOnly' => array_key_exists('asciiOnly', $data) ? $data['asciiOnly'] : $PHprefs['charset'] != 'utf-8',
	'rootDir' => $rootDir,
	'rootUrl' => $PHprefs['uploadUrl'] . '/' . PH::WPdecode($data['folder']),
	'allowFiles' => array_key_exists('allowFiles', $data) ? $data['allowFiles'] : true,
	'allowFolders' => array_key_exists('allowFolders', $data) ? $data['allowFolders'] : true,
	'allowedWidth' => array_key_exists('allowedWidth', $data) ? $data['allowedWidth'] : null,
	'allowedHeight' => array_key_exists('allowedHeight', $data) ? $data['allowedHeight'] : null,
	'thumbnailWidth' => array_key_exists('thumbnailWidth', $data) ? $data['thumbnailWidth'] : null,
	'thumbnailHeight' => array_key_exists('thumbnailHeight', $data) ? $data['thumbnailHeight'] : null,
	'allowedSize' => array_key_exists('allowedSize', $data) ? $data['allowedSize'] : null,
	'selectExtensions' => array_key_exists('selectExtensions', $data) ? explode(',', $data['selectExtensions']) : array(),
	'uploadExtensions' => array_key_exists('uploadExtensions', $data) ? explode(',', $data['uploadExtensions']) : array(),
	'skipPattern' => array_key_exists('skipPattern', $data) ? $data['skipPattern'] : '/(^thumb_(.*)\.png$)|(^__edit__)/'
);

PH::trigger_redirect("{$PHprefs['url']}/core/jsTree/?mode=media_browser&lang={$PHSes->lang}&" . mt_rand(0, 9999));