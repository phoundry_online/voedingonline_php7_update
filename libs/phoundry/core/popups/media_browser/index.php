<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";
$get = $_GET;
$type = isset($get['type']) ? $get['type'] : 'link';
$jstree = isset($get['jstree']) ? $get['jstree'] : null;
$callback = !empty($get['callback']) ? $get['callback'] : 'tinymce';
$browsers = isset($PHprefs['mediabrowser'][$type]) ? $PHprefs['mediabrowser'][$type] : array();
unset($get['callback'], $get['jstree']);
$qs = PH::htmlspecialchars(http_build_query($get));
header("Content-type: text/html; charset={$PHprefs['charset']}");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Media browser</title>

<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/layout/css/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/layout/css/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/layout/css/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>

<?php if ($callback == 'tinymce'): ?>
<?php if (file_exists("{$PHprefs['distDir']}/core/Inputtypes/tiny_mce/tiny_mce_popup.js")) {?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/Inputtypes/tiny_mce/tiny_mce_popup.js"></script>
<?php } else {?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/tiny_mce/tiny_mce_popup.js"></script>
<?php }?>
<?php endif?>
<script type="text/javascript">
jQuery(function($){
	$('div#tabstrip a').click(function(e){
		var tab = $(this).parent('li'), pane = $('#pane_'+tab.attr('id').replace(/^tab_/, ''));
		if(pane && pane.length > 0) {
			$('div#tabstrip li').removeClass('active');
			tab.addClass('active');
			$('div.pane').hide();
			pane.show();
			this.blur();
		}
		return false;
	});
});
function setUrl(url) {
<?php if ($callback == 'tinymce'): ?>
	url = encodeURI(url);
	var win = tinyMCEPopup.getWindowArg('window');

	// insert information now
	var input = win.document.getElementById(tinyMCEPopup.getWindowArg('input'));
	input.value = url;
	input.onchange();
	input.focus();

	// close popup window
	tinyMCEPopup.close();
<?php else: ?>
	<?php echo $callback ?>(url);
	if (window.opener) {
		window.close();
	}
	try {
		parent.killPopup();
	} catch(e) {}
<?php endif?>
}

function closePopup() {
	<?php if ($callback == 'tinymce'): ?>
	tinyMCEPopup.close();
	<?php else: ?>
	if (window.opener) {
		window.close();
	}
	try {
		parent.killPopup();
	} catch(e) {}
	<?php endif?>
}
</script>
<style type="text/css">
html, body {
	background: #fff;
}
iframe {
	border: 0;
	margin: 0;
	padding: 0;
}

#contentFrame {
	position: absolute;
	left: 0;
	right: 0;
	top: 43px;
	bottom: 28px;
}

iframe,
.pane {
	position: absolute;
	left: 0;
	right: 0;
	bottom: 0;
	top: 0;
	width: 100%;
	height: 100%;
}

</style>
</head>
<body class="frames">
	<div id="tabstrip" class="tabstrip-bg">
		<ul>
			<li id="tab_files" class="active"><a href="#"><?=word(150)?></a></li>
			<?php foreach($browsers as $id => $browser):?>
			<li id="tab_<?php echo $id ?>" class=""><a href="#"><?php echo is_numeric($browser['title']) ? word($browser['title']) : PH::htmlspecialchars($browser['title'])?></a></li>
			<?php endforeach;?>
		</ul>
	</div>
	<div id="contentFrame" class="">
		<div id="pane_files" class="pane" style="display: block;">
			<iframe src="<?=$PHprefs['url']?>/core/popups/media_browser/jstree.php?D=<?=urlencode($jstree)?>"></iframe>
		</div>
		<?php foreach($browsers as $id => $browser):?>
		<div id="pane_<?php echo $id ?>" class="pane" style="display : none;">
			<iframe src="<?php echo $browser['url']?>?<?php echo $qs?>"></iframe>
		</div>
		<?php endforeach;?>
	</div>
	<div id="footerFrame" class="footerFrame">
		<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
			<td>
			</td>
			<td align="right">
			<input type="button" value="<?=word(46)?>" onclick="closePopup();" />
			</td>
		</tr></table>
	</div>
</body>
</html>