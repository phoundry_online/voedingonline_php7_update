<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= ucfirst(word(29)) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<style type="text/css">

body {
	overflow: hidden;
}

#rainbowDiv {
	position:absolute;
	top:23px;
	left:247px;
	width:20px;
	height:200px;
	background:url(../pics/rainbow.png) no-repeat;
}

#satValDiv {
	height: 200px;
	width: 200px;
	position: absolute;
	left: 6px;
	top: 16px;
}
.gradient {
	height: 200px;
	width: 200px;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale src='../pics/gradient.png');
}
.gradient[class] {
	background: url(../pics/gradient.png) no-repeat;
}

#hueDiv {
	position: absolute;
	top: 16px;
	left: 229px;
	height: 218px;
	width: 40px;
}

#labelsDiv {
	position: absolute;
	top: 18px;
	left: 440px;
	color: #333;
}

input {
	color: #333;
	width:50px;
	border: 1px solid #ccc;
	padding: 2px;
	margin-bottom: 5px;
}

.bluedot {
	position: relative;
	z-index: 3;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale src='../pics/bluedot.png');
	height: 21px;
	width: 21px;
}
.bluedot[class] {
	background: url(../pics/bluedot.png) no-repeat;
}

.bluearrow {
	position: relative;
	z-index: 3;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale src='../pics/bluearrow.png');
	height: 21px;
	width: 23px;
}
.bluearrow[class] {
	background: url(../pics/bluearrow.png) no-repeat;
}

.satvalbg {
	height: 200px;
	width: 200px;
	background: #f00;
	position: absolute;
	left: 9px;
	top: 7px;
}

.colorbox {
	height:50px;
	border:1px inset window;
	cursor: pointer;
	cursor: hand;
	background: #fff;
}

.colorhex {
	text-align: center;
}

#matchesDiv {
	position: absolute;
	top: 245px;
	left: 13px;
	width:524px;
}

.websafe {
   width: 11px;
   height: 11px;
   cursor: pointer;
   cursor: hand;
}

#websafeDiv {
	position: absolute;
	top: 23px;
	left: 290px;
}

#legend1Div {
	position: absolute;
	left: 14px;
	top: 2px;
}

#legend2Div {
	position: absolute;
	left: 14px;
	top: 230px;
}

#legend1Div td, #legend2Div td {
	text-align: center;
	font-weight: bold;
	background: buttonface;
}

</style>

<script type="text/javascript">
//<![CDATA[


// Heavily based on:
//	ColourMod v2.0 - DHTML Dynamic Color Picker/Selector
//	� 2005 ColourMod.com
//	Produced By The Noah Institute (www.noahinstitute.org)
//	Design/Programming By Stephen Hallgren (www.teevio.net)

var _W=window,_D=document,rgb=[],hsv=[],H=null,S,V,R,G,B,hex,isDrag=false;
_D.onmousedown=selectmouse;
_D.onmouseup=function(){if(isDrag){matchColors(H,S,V)};isDrag=false};

function gE(el){return _D.getElementById(el)}
function sX(e,x){e.style.left=x+'px'}
function sY(e,y){e.style.top=y+'px'}

function Stretch(Q, L, c) { var S = Q; if (c.length>0) while (S.length<L) { S = c+S } return S }
function StrU(X, M, N) { // X>=0.0
var T, S=new String(Math.round(X*Number("1e"+N))) 
if (S.search && S.search(/\D/)!=-1) { return ''+X } 
with (new String(Stretch(S, M+N, '0'))) return substring(0, T=(length-N)) + '.' + substring(T) }
function Sign(X) { return X<0 ? '-' : ''; } 
function StrS(X, M, N) { return Sign(X)+StrU(Math.abs(X), M, N) } 
Number.prototype.toFixed= new Function('n','return StrS(this,1,n)');

function selectmouse(e) {
	var src;
	if (e && e.target) {
		src = e.target;
		if (src.nodeType == 3)
			src = src.parentNode;
	}
	else
	{
		e = _W.event;
		src = e.srcElement;
	}

	switch(src.id) {
		case 'bluedot':
			isDrag = true;
			svUpdate(e.clientX-16, e.clientY-26);
			_D.onmousemove = satValSlider;
			break;
		case 'gradientDiv':
			svUpdate(e.clientX-16, e.clientY-26);
			break;
		case 'bluearrow':
			isDrag = true;
			hUpdate(e.clientY-23);
			_D.onmousemove = hueSlider;
			break;
		case 'hueDiv':
			if (e.clientY-23 >= 0 && e.clientY-23 <= 200)
				hUpdate(e.clientY-23);
			break;
	}
}

function satValSlider(e) {
	if (!isDrag) return;
	if (!e) e=_W.event;
	var el = gE('bluedot'), x = e.clientX-16, y = e.clientY-26;
	x = (x < 0) ? 0 : (x > 200 ? 200 : x);
	y = (y < 0) ? 0 : (y > 200 ? 200 : y);
	sX(el, x); 
	svUpdate(x, y);
}

function hueSlider(e) {
	if (!isDrag) return;
	if (!e) e=_W.event;
	var el = gE('bluearrow'), y = e.clientY-23;
	y = (y < 0) ? 0 : (y > 200 ? 200 : y);
	sY(el, y);
	hUpdate(y);
}

function inputUpdate(field, color) {
	var str;

	if (color == 'hex') {
		if (!/^#[a-f0-9]{6}$/.test(field.value)) {
			field.value = '#000000';
		}
		str = field.value.replace(/^#/,'');
	}
	else {
		if (!/^[^0-9\.\-]+$/g.test(field.value)) {
			field.value = field.value.replace(/([^0-9\.\-])/g, '');
		}
		str = parseFloat(field.value);
	}

	if (color == 'hex') {
		hexUpdate(str);
	} else if (color == 'rgb') {
		if (str > 255)	field.value = 255;
		R = parseInt(gE('red').value);
		G = parseInt(gE('green').value);
		B = parseInt(gE('blue').value);
		rgbUpdate(R,G,B);
	} else if (color == 'sv') {
		if (str > 100)	field.value = 100;
		H = parseFloat(gE('hue').value);
		S = parseFloat(gE('saturation').value);
		V = parseFloat(gE('value').value);
		hsvUpdate(H,S,V);
	} else if (color == 'hue') {
		if (str > 360)	field.value = 360;
		H = parseFloat(gE('hue').value);
		S = parseFloat(gE('saturation').value);
		V = parseFloat(gE('value').value);
		hsvUpdate(H,S,V);
	}
}

////////Update Functions

function svUpdate(dotX,dotY) {
	//Set Hue/Sat/Val values
	H = parseFloat(gE('hue').value);
	S = dotX/2;
	V = (100-(dotY/2));
	
	//Calculate hexadecimal number
	rgb = HSVtoRGB(H,S,V);
	hex = RGBtoHEX(rgb['red'],rgb['green'],rgb['blue']);
	var el = gE('bluedot');
	sX(el,dotX);
	sY(el,dotY);
	updateAll(rgb['red'],rgb['green'],rgb['blue'], hex, H, S, V);
}

function hUpdate(sliderY) {
	//Set Hue/Sat/Val values
	H = (sliderY/200)*360;
	S = parseFloat(gE('saturation').value);
	V = parseFloat(gE('value').value);

	//Calculate hexadecimal number
	rgb = HSVtoRGB(H,S,V);
	hex = RGBtoHEX(rgb['red'],rgb['green'],rgb['blue']);
	sY(gE('bluearrow'), sliderY);
	updateAll(rgb['red'],rgb['green'],rgb['blue'], hex, H, S, V);
}

function hsvUpdate(H,S,V) {
	var el = gE('bluedot');
	rgb = HSVtoRGB(H, S, V);
	hex = RGBtoHEX(rgb['red'],rgb['green'],rgb['blue']);
	sX(el, S*2);
	sY(el, (-V+100)*2);
	sY(gE('bluearrow'), (H/360)*200);
	updateAll(rgb['red'],rgb['green'],rgb['blue'], hex, H, S, V);
}

function hexUpdate(hex) {
	var el = gE('bluedot');
	rgb = HEXtoRGB(hex);
	hsv = RGBtoHSV(rgb['red'],rgb['green'],rgb['blue']);
	sX(el, hsv['sat']*2);
	sY(el, (-hsv['val']+100)*2);
	sY(gE('bluearrow'), (hsv['hue']/360)*200);
	updateAll(rgb['red'],rgb['green'],rgb['blue'], hex, hsv['hue'], hsv['sat'], hsv['val']);
}

function rgbUpdate(R,G,B) {
	var el = gE('bluedot');
	hsv = RGBtoHSV(R,G,B);
	hex = RGBtoHEX(R,G,B);
	sX(el, hsv['sat']*2);
	sY(el, (-hsv['val']+100)*2);
	sY(gE('bluearrow'), (hsv['hue']/360)*200);
	updateAll(R,G,B, hex, hsv['hue'], hsv['sat'], hsv['val']);
}

// Updates displayed values
function updateAll(R, G, B, hex, H, S, V) {
	if (R > 255) R = 255;
	if (G > 255) G = 255;
	if (B > 255) B = 255;
	var gradrgb = HSVtoRGB(H,100,100);
	var gradHex = RGBtoHEX(gradrgb['red'],gradrgb['green'],gradrgb['blue']);
	gE('saturation').value = (S).toFixed(1);
	gE('value').value = (V).toFixed(1);
	gE('hue').value = (H).toFixed(1);
	gE('red').value = R;
	gE('green').value = G;
	gE('blue').value = B;
	gE('hex').value = '#' + hex.toLowerCase();
	gE('satvalbg').style.backgroundColor = '#'+gradHex;
	gE('box0').style.backgroundColor = '#'+hex;
	gE('hex0').innerHTML = '#'+hex;
	if(!isDrag) matchColors(H,S,V);
}

////////Color Conversion Functions

function HSVtoRGB(H, S, V) {
   var F, P, Q, R, T;
   H = H/360;
   S = S/100;
   V = V/100;

   if (S <= 0) {
      V = Math.round(V*255);
      return {'red':V,'green':V,'blue':V};
   } else {
      if (H >= 1.0) H = 0;
      H = 6 * H;
      F = H - Math.floor(H);
      P = (256 * V * (1.0 - S));
      Q = (256 * V * (1.0 - (S * F)));
      T = (256 * V * (1.0 - (S * (1.0 - F))));
      V = (256 * V);
      switch(Math.floor(H)) {
          case 0: R = V; G = T; B = P; break;
          case 1: R = Q; G = V; B = P; break;
          case 2: R = P; G = V; B = T; break;
          case 3: R = P; G = Q; B = V; break;
          case 4: R = T; G = P; B = V; break;
          case 5: R = V; G = P; B = Q; break;
      }
      return {'red':Math.round(R),'green':Math.round(G),'blue':Math.round(B)};
   }
}


function RGBtoHEX(R,G,B) {
	return(toHex(R)+toHex(G)+toHex(B));
}

function toHex(N) {
	if (N==null) 
		return '00';
	N=parseInt(N); 
	if (N==0 || isNaN(N)) 
		return '00';
	N=Math.max(0,N); 
	N=Math.min(N,255); 
	N=Math.round(N);
	return '0123456789abcdef'.charAt((N-N%16)/16)+'0123456789abcdef'.charAt(N%16);
}

function HEXtoRGB(H) { 
	var hexR = H.substr(0,2);
	rgb['red'] = parseInt(hexR.substring(0,2),16);
	var hexG = H.substr(2,2);
	rgb['green'] = parseInt(hexG.substring(0,2),16);
	var hexB = H.substr(4,2);
	rgb['blue'] = parseInt(hexB.substring(0,2),16);
	return rgb;
}

function RGBtoHSV (R,G,B) {
   var max = Math.max(R,G,B), min = Math.min(R,G,B), delta = max-min, V = Math.round((max / 255) * 100), S = (max != 0) ? Math.round(delta/max * 100) : 0;

   if(S == 0)
      H = 0;
   else{
      if(R == max)
         H = (G - B)/delta;
      else if(G == max)
         H = 2 + (B - R)/delta;
      else if(B == max)
         H = 4 + (R - G)/delta;
      H = Math.round(H * 60);
      if(H > 360)
         H = 360;
      if(H < 0)
         H += 360;
   }
   return {'hue':H, 'sat':S, 'val':V};
}


/** Matching colors **/
function rc(x,y){return (x > y) ? y : Math.max(0,x)}

function matchColors(h,s,v){
	h=parseInt(h);s=parseInt(s);v=parseInt(v);
	var y=new Object(),z=new Object(),box,clr;
	y.v = z.v = (v > 70) ? v - 30 : v + 30;
	box = HSVtoRGB(h,s,y.v), clr = '#' + RGBtoHEX(box['red'], box['green'], box['blue']);
	gE('box1').style.background=clr; gE('hex1').innerHTML=clr;
	y.v = v;

	if(h >= 0 && h < 30) {
		z.h = y.h = h+20;
		z.s = y.s = s;
	}
	else if(h >= 30 && h < 60) {
		z.h = y.h = h + 150;
		y.s = rc(s-30,100);
		y.v = rc(v-20,100);
		z.s = rc(s-70,100);
		z.v = rc(v+20,100);
	}
	else if(h >= 60 && h < 180) {
		z.h = y.h = h - 40;
		y.s = z.s = s;
	}
	else if(h >= 180 && h < 220) {
		z.h = h - 170;
		y.h = h - 160;
		z.s = y.s = s;
	}
	else if(h >= 220 && h < 300) {
		z.h = y.h = h;
		z.s = y.s = rc(s-60,100);
	}
	else if(h >= 300){
		y.s = z.s = s > 50 ? s - 40 : s + 40;
		z.h = y.h = (h+20)%360;
	}
	box = HSVtoRGB(y.h,y.s,y.v), clr = '#' + RGBtoHEX(box['red'], box['green'], box['blue']);
	gE('box2').style.background=clr; gE('hex2').innerHTML=clr;
	box = HSVtoRGB(z.h,z.s,z.v), clr = '#' + RGBtoHEX(box['red'], box['green'], box['blue']);
	gE('box3').style.background=clr; gE('hex3').innerHTML=clr;
	box = HSVtoRGB(0,0,100-v), clr = '#' + RGBtoHEX(box['red'], box['green'], box['blue']);
	gE('box4').style.background=clr; gE('hex4').innerHTML=clr;
	box = HSVtoRGB(0,0,v), clr = '#' + RGBtoHEX(box['red'], box['green'], box['blue']);
	gE('box5').style.background=clr; gE('hex5').innerHTML=clr;
}

function substColor(m,r,g,b){
   return toHex(r) + toHex(g) + toHex(b)
}

function setColor(clr) {
	if(!clr)return;
	window.status=clr;
	hexUpdate(clr.replace('#','').replace(/rgb\(([0-9]+),\s*([0-9]+),\s*([0-9]+)\)/g,substColor));
}

function submitColor() {
	var color = gE('hex').value;
	parent._D.forms[0]['<?= $_GET['field'] ?>'].value = color;
	if (/^#[ABCDEFabcdef0123456789]{6}$/.test(color))
		parent._D.getElementById('<?= $_GET['field'] ?>Clr').style.background = color;
	parent.killPopup();
}

function init() {
<?php
	if (!empty($_GET['color']) && preg_match('/^#[ABCDEF0123456789]{6}$/i', $_GET['color'])) {
		print "hexUpdate('{$_GET['color']}'.replace(/#/,''));\n";
	}
?>
}

//]]>
</script>

</head>
<body onselectstart="return false" onload="init()">

<div id="rainbowDiv"></div>

<div id="satValDiv">
	<div id="bluedot" class="bluedot"></div>
	<div id="satvalbg" class="satvalbg">
		<div id="gradientDiv" class="gradient"></div>
	</div>
</div>
   
<div id="hueDiv">
	<div id="bluearrow" class="bluearrow"></div>
</div>

<div id="labelsDiv">
	<table>
	<tr><td>Hex:</td><td><input type="text" name="hex" id="hex" value="#ffffff" maxlength="7" onblur="inputUpdate(this, 'hex')" /></td></tr>
	<tr><td>Hue:</td><td><input type="text" name="hue" id="hue" value="0" maxlength="5" onblur="inputUpdate(this, 'hue')" /></td></tr>
	<tr><td>Sat:</td><td><input type="text" name="saturation" id="saturation" value="0" maxlength="5" onblur="inputUpdate(this, 'sv')" /></td></tr>
	<tr><td>Val:</td><td><input type="text" name="value" id="value" value="100" maxlength="5" onblur="inputUpdate(this, 'sv')" /></td></tr>
	<tr><td>Red:</td><td><input type="text" name="red" id="red" value="255" maxlength="3" onblur="inputUpdate(this, 'rgb')" /></td></tr>
	<tr><td>Green:</td><td><input type="text" name="green" id="green" value="255" maxlength="3" onblur="inputUpdate(this, 'rgb')" /></td></tr>
	<tr><td>Blue:</td><td><input type="text" name="blue" id="blue" value="255" maxlength="3" onblur="inputUpdate(this, 'rgb')" /></td></tr>
	</table>
</div>

<div id="matchesDiv">
<table cellspacing="2" cellpadding="0">
<tr>
	<td id="box0" class="colorbox" style="cursor:default;width:198px">&nbsp;</td>
	<td id="box1" class="colorbox" style="width:60px" title="Click to make this color the primary color" onclick="setColor(this.style.backgroundColor)">&nbsp;</td>
	<td id="box2" class="colorbox" style="width:60px"title="Click to make this color the primary color" onclick="setColor(this.style.backgroundColor)">&nbsp;</td>
	<td id="box3" class="colorbox" style="width:60px"title="Click to make this color the primary color" onclick="setColor(this.style.backgroundColor)">&nbsp;</td>
	<td id="box4" class="colorbox" style="width:60px"title="Click to make this color the primary color" onclick="setColor(this.style.backgroundColor)">&nbsp;</td>
	<td id="box5" class="colorbox" style="width:60px"title="Click to make this color the primary color" onclick="setColor(this.style.backgroundColor)">&nbsp;</td>
</tr>
<tr>
	<td class="colorhex" id="hex0">#ffffff</td>
	<td class="colorhex" id="hex1">#8a0c0c</td>
	<td class="colorhex" id="hex2">#8a0c0c</td>
	<td class="colorhex" id="hex3">#8a0c0c</td>
	<td class="colorhex" id="hex4">#8a0c0c</td>
	<td class="colorhex" id="hex5">#8a0c0c</td>
</tr>
</table>
<p>
	<button type="button" style="float:right" onclick="parent.killPopup()"><?= word(46) ?></button>
	<button type="button" class="okBut" onclick="submitColor()"><?= word(82) ?></button>
</p>
</div>

<table id="websafeDiv" cellpadding="0" cellspacing="0" border="0">
<script type="text/javascript">
//<![CDATA[
	var clr = ['00','33','66','99','cc','ff'], b = -1, i = 0, x, y;
	for (y = 0; y < 18; y++)
	{
		_D.write('<tr>\n');
		for (x = 0; x < 12; x++)
		{
			if (i++%6==0)	b++;
			if (b >= 6)	b = 0;
			bgc = '#'+clr[Math.floor(y/3)] + clr[b] + clr[x%6];
			//_D.write('<td class="websafe" bgcolor="'+bgc+'" onclick="setColor(\''+bgc+'\')" onmouseover="_W.status=\''+bgc+'\';return true"></td>');
			_D.write('<td class="websafe" bgcolor="'+bgc+'" onclick="setColor(\''+bgc+'\')"></td>');
		}
		_D.write('</tr>\n');
	}
//]]>
</script>
</table>

<table id="legend1Div" cellspacing="1" cellpadding="0"><tr>
<td width="200">saturation</td>
<td width="75">hue</td>
<td width="132">216 websafe colors</td>
<td width="109">data</td>
</tr></table>

<table id="legend2Div" cellspacing="1" cellpadding="0"><tr>
<td width="200">selected color</td>
<td width="318">matching colors</td>
</tr></table>

</body>
</html>
