<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction'])) {
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	}

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'import');

	$phoundry = new Phoundry($TID);

	$phpName = $_FILES['csv_file']['tmp_name'];
	$orgName = $_FILES['csv_file']['name'];
	$charset = $_POST['charset'] == 'iso-8859-1' ? 'iso-8859-1' : 'utf-8';
	$enclosure = $_POST['enclosure'][0];
	$fieldSep = $_POST['fieldSep'];
	if ($fieldSep == '\t') { $fieldSep = "\t"; } else { $fieldSep = $fieldSep[0]; }

	$tmpDir = dirname($phpName);
	$errors = array();
	// Make sure extension is '.csv':
	if (!preg_match('/(\.csv)$/i', $orgName)) {
		$errors[] = word(168);
	}

	if (!empty($errors)) {
		$errMsg = '<ul>';
		foreach($errors as $error) {
			$errMsg .= '<li>' . $error . '</li>';
		}
		$errMsg .= '</ul>';
		trigger_error($errMsg, E_USER_ERROR);
	}

	// Remove old (> 1 hr) import files that might be wandering around...
	if (is_dir($tmpDir)) {
		$handle = opendir($tmpDir);
		while(($file = readdir($handle)) !== false) {
			if (preg_match('/^import_/', $file)) {
				if (time() - 3600 > filemtime("$tmpDir/$file")) {
					PH::RmDirR("$tmpDir/$file");
				}
			}
		}
		closedir($handle);
	}

	$newName = 'import_' . basename($phpName);
	move_uploaded_file($phpName, "{$PHprefs['tmpDir']}/{$newName}");
	chmod("{$PHprefs['tmpDir']}/{$newName}", 0644);

	// Convert charset?
	if ($charset != $PHprefs['charset']) 
	{
		$exec = isset($PHprefs['paths']['iconv']) ? $PHprefs['paths']['iconv'] : 'iconv';
		$cmd = "{$exec} -c -f " . escapeshellarg($charset) . ' -t ' . escapeshellarg($PHprefs['charset']) . " {$PHprefs['tmpDir']}/{$newName} > {$PHprefs['tmpDir']}/{$newName}.tmp";
		shell_exec($cmd);
		rename("{$PHprefs['tmpDir']}/{$newName}.tmp", "{$PHprefs['tmpDir']}/{$newName}");
	}

	$CSVcols = array();
	//ini_set('auto_detect_line_endings',true);
	if ($fp = fopen("{$PHprefs['tmpDir']}/{$newName}", 'r')) 
	{
		$CSVcols = fgetcsv($fp, 0, $fieldSep, $enclosure);
		fclose($fp);
	}

	list($CSRFname, $CSRFtoken) = PH::startCSRF();
	
	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(166) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript">

var nrCSVfields = <?= count($CSVcols) ?>;

function in_array(field, arr) {
	for (var x = 0; x < arr.length; x++) {
		if (arr[x] == field)
			return true;
	}
	return false;
}

function checkForm(F) {
	var chosen = [], errMsg = '', value = '';
	for (var x = 0; x < nrCSVfields; x++) {
		value = F['CSV_' + x].options[F['CSV_' + x].selectedIndex].value;
		if (value == -1)
			continue;
		if (in_array(value, chosen)) {
			alert('<?= escSquote(word(174)) ?>');
			return false;
		}
		else
			chosen[chosen.length] = value;
	}
	$("button[type='submit']").prop('disabled',true);
	return true;
}

$(function() {
	CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');
});

</script>
</head>
<body>
<form action="import_csvRes.php?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="orgName" value="<?= PH::htmlspecialchars($orgName) ?>" />
<input type="hidden" name="csv_file" value="<?= $newName ?>" />
<input type="hidden" name="fieldSep" value="<?= PH::htmlspecialchars($fieldSep) ?>" />
<input type="hidden" name="enclosure" value="<?= PH::htmlspecialchars($enclosure) ?>" />

<fieldset>
<legend><b class="req"><?= word(169) ?></b></legend>
<?= word(170) ?>
<table cellspacing="2" cellpadding="1" border="0">
<tr>
	<th>CSV-field</th>
	<th>Database-field</th>
</tr>
<?php
	$x = 0;
	foreach($CSVcols as $CSVcol) {
		print '<tr class="' . (($x % 2 == 0) ? 'even' : 'odd') . '">';
		print '<td align="right" nowrap="nowrap">' . ($x+1) . ': ' . $CSVcol . ':</td>';
		print '<td><select name="CSV_' . $x . '">';
		print '<option value="-1">-- Skip --</option>';
		foreach($phoundry->cols as $col) {
			if ($col->datatype->isXref) continue;
			if (strtolower(get_class($col->datatype)) == 'dtfake') continue;
			$name = $col->name;
			$selected = (preg_replace('/[\W_]+/', '', strtolower($name)) == preg_replace('/[\W_]+/', '', strtolower($CSVcol))) ? ' selected="selected" ' : ' ';
			print '<option' . $selected . ' value="' . $name . '">' . $name . '</option>';
		}

		print '</select></td>';
		print "</tr>\n";
		$x++;
	}
?>
</table>
</fieldset>

<fieldset>
<legend><b class="req"><?= word(171) ?></b></legend>
<input type="radio" name="skip1st" value="1" checked="checked" /><?= word(173) ?>
<input type="radio" name="skip1st" value="0" /><?= word(172) ?>
</fieldset>

<p>
<button style="float:right" type="button" onclick="parent.killPopup()"><?= word(46) ?></button>
<button type="submit" class="okBut"><?= word(82) ?></button>
</p>
</form>
</body>
</html>
