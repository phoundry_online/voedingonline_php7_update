<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'import');

	list($CSRFname, $CSRFtoken) = PH::startCSRF();

	PH::getHeader('text/html');
?>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(166) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript">

function submitForm(F) {
	if (F.fieldSep.value == '') {
		alert('<?= escSquote(word(43, 'Field separator')) ?>');
		return false;
	}
	if (F.enclosure.value == '') {
		alert('<?= escSquote(word(43, 'Text delimiter')) ?>');
		return false;
	}
	$("button[type='submit']").prop('disabled',true);
	return true;
}

$(function() {
	CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');
});

</script>
</head>
<body style="overflow:hidden">
<form action="import_csv2.php?<?= QS(1) ?>" method="post" enctype="multipart/form-data" onsubmit="return submitForm(this)">

<fieldset><legend><b class="req"><?= word(88) ?></b></legend>
<?= word(167) ?>
</fieldset>

<fieldset><legend><b class="req"><?= word(103) ?></b></legend>
<input type="file" name="csv_file" size="42" />
</fieldset>

<fieldset><legend><b class="req">Field separator</b></legend>
<input type="text" name="fieldSep" value="<?= $g_csvFieldSep ?>" size="2" maxlength="4" />
</fieldset>

<fieldset><legend><b class="req">Text delimiter</b></legend>
<input type="text" name="enclosure" value="&quot;" size="2" maxlength="4" />
</fieldset>

<fieldset><legend><b class="req"><?= word(301 /* Charset */) ?></b></legend>
<select name="charset">
<?php
	$opts = array('iso-8859-1', 'utf-8');
	foreach($opts as $opt) {
		$selected = $PHprefs['charset'] == $opt ? ' selected="selected" ' : ' ';
		print '<option' . $selected . 'value="' . $opt . '">' . $opt . '</option>';
	}
?>
</select>
</fieldset>

<p>
<button type="button" style="float:right" onclick="parent.killPopup()"><?= word(46) ?></button>
<button type="submit" class="okBut"><?= word(82) ?></button>
</p>
</form>
</body>
</html>
