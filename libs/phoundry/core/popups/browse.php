<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once "{$PHprefs['distDir']}/core/include/Column.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$CID = $_REQUEST['CID'];

	// Instantiate Column object:
	$column = new Column($_GET['CID']);
	$extra  = $column->datatype->extra;
	$saveDir = PH::evaluate($extra['savedir']);
	$rootDir = $PHprefs['uploadDir'] . '/' . $saveDir;
	$rootUrl = $PHprefs['uploadUrl'] . '/' . $saveDir;

	if (!file_exists($rootDir)) {
		mkdir($rootDir, 0775, true);
	}

	// Set SESSION vars for jsTree Explorer:
	$_SESSION['jsTree'] = array(
		'asciiOnly' => $PHprefs['charset'] != 'utf-8',
		'rootDir' => $rootDir,
		'rootUrl' => $rootUrl,
		'allowFiles' => false,
		'allowFolders' => false,
		'allowedWidth' => null,
		'allowedHeight' => null,
		'useFileFunction' => "parent.useTreeFile('{\$path}'.replace(/^" . str_replace('/', '\\\/', preg_quote($PHprefs['uploadUrl'])) . "/,''), '{$column->name}')"
	);
	$_SESSION['jsTree']['selectExtensions'] = isset($extra['select_extensions']) ? $extra['select_extensions'] : array();
	$_SESSION['jsTree']['uploadExtensions'] = isset($extra['upload_extensions']) ? $extra['upload_extensions'] : array();
	$_SESSION['jsTree']['allowFolders'] = $extra['allow_makedir'] ? true : false;
	$_SESSION['jsTree']['allowFiles'] = $extra['allow_upload'] ? true : false;
	$_SESSION['jsTree']['thumbnailWidth'] = isset($extra['thumb_width']) ? $extra['thumb_width'] : 0;
	$_SESSION['jsTree']['thumbnailHeight'] = isset($extra['thumb_height']) ? $extra['thumb_height'] : 0;
	$_SESSION['jsTree']['allowedSize'] = isset($extra['max_upload_size']) ? $extra['max_upload_size'] : null;

	header('Location: ../jsTree/?lang=' . $PHSes->lang . '&' . mt_rand(0,9999));
