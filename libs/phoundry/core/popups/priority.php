<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	$CID = (int)$_REQUEST['CID'];

	// Fetch name and query:
	$sql = "SELECT name, description, inputtype_extra FROM phoundry_column WHERE id = $CID";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$inputName = $db->FetchResult($cur,0,'name');
	$inputDesc = $db->FetchResult($cur,0,'description');
	$inputExtra = unserialize($db->FetchResult($cur,0,'inputtype_extra'));
	$order = $inputExtra['order'];
	
	function priorityRow($colspan, $prio) {
		return '<tr title="' . $prio . '" onclick="setPriority(' . $prio . ')" onmouseover="this.className=\'hilite\'" onmouseout="this.className=\'\'"><td colspan="' . $colspan . '"><img src="../pics/pixel.gif" title="' . $prio . '" alt="' . $prio . '" width="1" height="4" /></td></tr>';
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title>Priorities</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<style type="text/css">

.hilite { background: red; cursor: pointer; cursor: hand }
.active { background: #0c2e82; color: #fff }

</style>
<script type="text/javascript">

function setPriority(prio) {
	parent.document.forms[0]['<?= $inputName ?>'].value = prio;
	parent.killPopup();
}

</script>
</head>
<body>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="get">
<input type="hidden" name="TID" value="<?= $TID ?>" />
<input type="hidden" name="CID" value="<?= $CID ?>" />

<fieldset>
<legend><b><?= PH::langWord($inputDesc) ?></b></legend>
<table class="view-table" width="100%">

<?php
	$prios = array();
	$sql = PH::evaluate($inputExtra['query']);
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$nrf = $db->NumberOfColumns($cur);
	print '<tr>';
	for ($y = 0; $y < $nrf; $y++) {
      print '<th>' . ucfirst($db->FetchColumnName($cur,$y)) . '</th>';
	}
	print "</tr>\n";

	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$texts = array();
		for ($y = 1; $y < $nrf; $y++) {
			$texts[] = $db->FetchResult($cur,$x,$y) . ' ';
		}
		$prios[] = array('prio'=>(float)$db->FetchResult($cur,$x,0), 'texts'=>$texts);
	}

	$cnt = count($prios);
	if ($cnt == 0) {
		print '<tr><td colspan="' . $nrf . '"><button type="button" onclick="setPriority(1000)">Set Priority</button></td></tr>';
	}
	else {
		if ($order == 'desc') {
			$prios[$cnt] = array('prio'=>0, 'texts'=>array());
		}
		else {
			$prios[$cnt] = array('prio'=>$prios[$cnt-1]['prio']+20, 'texts'=>array());
		}
		for ($x = 0; $x < $cnt; $x++) {
			if ($x == 0) {
				if ($order == 'desc') {
					print priorityRow($nrf, round($prios[$x]['prio']/10)*10 + 10);
				}
				else {
					print priorityRow($nrf, $prios[$x]['prio'] / 2);
				}
			}

			$class = ($x & 1) ? 'odd' : 'even';
			if (!empty($_GET['prio']) && $_GET['prio'] == $prios[$x]['prio']) {
				$class = 'active';
			}

			print '<tr class="' . $class . '">';
			print '<td>' . $prios[$x]['prio'] . '</td>';
			foreach($prios[$x]['texts'] as $text) {
				print '<td>' . PH::htmlspecialchars($text) . '</td>';
			}
			print "</tr>\n";
			if ($order == 'desc') {
				print priorityRow($nrf, ($prios[$x+1]['prio'] + $prios[$x]['prio']) / 2);
			}
			else {
				print priorityRow($nrf, ($prios[$x]['prio'] + $prios[$x+1]['prio']) / 2);
			}
		}
	}
?>
</table>
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</p>

</form>
</body>
</html>
