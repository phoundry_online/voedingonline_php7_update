<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$coords = array('52.126157647243524','5.565465688705444'); // WP, Barneveld
	if (isset($_GET['coords']) && strpos($_GET['coords'], ',') !== false) {
		$coords = explode(',', $_GET['coords']);
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(222 /* Google Map coordinates */) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;region=NL&amp;lang=<?= $PHSes->lang ?>"></script>
<script type="text/javascript">

var geocoder = new google.maps.Geocoder(), map, marker;

function geocodePosition(pos) {
	geocoder.geocode({
		latLng: pos
	}, function(responses) {
		if (responses && responses.length > 0) {
			updateMarkerAddress(responses[0].formatted_address);
		} else {
			//updateMarkerAddress('Cannot determine address at this location.');
			updateMarkerAddress('');
		}
	});
}

function updateMarkerAddress(address) {
	document.getElementById('address').value = address;
}

function updateMarkerPosition(latLng) {
	document.getElementById('coordX').value = latLng.lat();
	document.getElementById('coordY').value = latLng.lng();
}

function showAddress(address) {
	geocoder.geocode({
		'address': address,
		'partialmatch': true},
	 geocodeResult);
}

function geocodeResult(results, status) {
	if (status == 'OK' && results.length > 0) {
		map.fitBounds(results[0].geometry.viewport);
		marker.setPosition(map.getCenter());
		updateMarkerPosition(map.getCenter());
		geocodePosition(marker.getPosition());
	} else {
		alert("Geocode was not successful for the following reason: " + status);
	}
}

function init() {
	var latLng = new google.maps.LatLng(<?= number_format($coords[0],14,'.','') ?>,<?= number_format($coords[1],14,'.','') ?>);
	map = new google.maps.Map(document.getElementById('mapCanvas'), {
		zoom: 16,
		center: latLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	marker = new google.maps.Marker({
		position: latLng,
		//title: 'Point A',
		map: map,
		draggable: true
	});
 
	// Update current position info.
	updateMarkerPosition(latLng);
	geocodePosition(latLng);
  
	google.maps.event.addListener(marker, 'drag', function() {
		updateMarkerPosition(marker.getPosition());
	});

	google.maps.event.addListener(marker, 'dragend', function() {
		geocodePosition(marker.getPosition());
	});
}

function setCoords(F) {
	if (F['coordX'].value && F['coordY'].value) {
		parent.document.forms[0]['<?= $_GET['field'] ?>'].value = F['coordX'].value + ',' + F['coordY'].value;
	}
	else {
		parent.document.forms[0]['<?= $_GET['field'] ?>'].value = '';
	}
	parent.killPopup();
}

</script>
</head>
<body onload="init()">
<form onsubmit="showAddress(this['address'].value);return false">

<fieldset><legend><b>1. <?= word(223 /* Enter an address */) ?></b></legend>
<table cellspacing="0" cellpadding="0"><tr>
<td valign="top"><input type="text" size="60" id="address" name="address" />
<input type="submit" value="<?= word(10 /* Search */) ?>" />
<td valign="top" style="padding-left:10px">
(- Kalverstraat, Amsterdam, NL<br />
&nbsp;- Tour Eiffel, Paris, FR<br />
&nbsp;- Blair Ave, Sunnyvale, CA)
</td></tr></table>
</fieldset>

<fieldset><legend><b>2. <?= word(224 /* Drag the red marker to the exact location */) ?></b></legend>
<div id="mapCanvas" style="width:100%;height:480px"></div>
</fieldset>

<fieldset><legend><b>3. <?= word(222 /* Google map coordinates */) ?></b></legend>
X: <input type="text" id="coordX" name="coordX" value="<?= empty($_GET['coords']) ? '' : $coords[0] ?>" readonly="readonly" style="border:none" />
Y: <input type="text" id="coordY" name="coordY" value="<?= empty($_GET['coords']) ? '' : $coords[1] ?>" readonly="readonly" style="border:none" />
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
<input type="button" class="okBut" value="<?= word(82) ?>" onclick="setCoords(this.form)" />
</p>

</form>
</body>
</html>
