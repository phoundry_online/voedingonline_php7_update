<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'export');

	list($CSRFname, $CSRFtoken) = PH::startCSRF();

	$phoundry = new Phoundry($TID);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title><?= word(74) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript">

var _D = document, _F = null;

function init() {
	CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');
	_F = _D.forms[0]; var P = parent.document.forms[0];
	_F['PHshow'].value = P['PHshow'].value;
	_F['PHordr'].value = P['PHordr'].value;
	<?php	if (isset($_GET['RID'])) {	?>
	_F['RIDs'].value  = '<?= $_GET['RID'] ?>';
	<?php } else { ?>
	_F['RIDs'].value  = parent.document.getElementById('_RIDs').innerHTML;
	<?php } ?>
}

</script>
</head>
<body style="overflow:hidden" onload="init()">
<form action="export_pdfRes.php?<?= QS(1) ?>" method="post" target="_blank">
<input type="hidden" name="RIDs" value="" />
<input type="hidden" name="FORMAT" value=".pdf" />
<input type="hidden" name="PHshow" value="" />
<input type="hidden" name="PHordr" value="" />

<fieldset><legend><b class="req">Font</b></legend>
Family: <select name="fontFamily">
<option value="Helvetica">Helvetica</option>
<option value="Courier">Courier</option>
<option value="Times">Times</option>
<option value="Symbol">Symbol</option>
</select>
Size:
<select name="fontSize">
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7" selected="selected">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select> points
</fieldset>

<?php if (isset($_GET['RID'])) { ?>
<input type="hidden" name="pagePerRecord" value="0" />
<?php } else { ?>
<fieldset><legend><b class="req"><?= word(21) ?>?</b></legend>
<input type="radio" name="pagePerRecord" value="1" checked="checked" /><?= word(1) ?>
<input type="radio" name="pagePerRecord" value="0" /><?= word(0) ?>
</fieldset>
<? } ?>

<p>
<input style="float:right" type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
<input type="submit" class="okBut" name="submit" value="<?= word(82) ?>" />
</p>
</form>
</body>
</html>
