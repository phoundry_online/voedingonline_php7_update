<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	checkAccess();

	$yid = '';
	if (preg_match('/.+watch[&?]v=([\w-]+)/', $_GET['yid'], $reg)) {
		$yid = $reg[1];
	}
	else {
		$yid = $_GET['yid'];
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title>Youtube</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body style="margin:0;padding:0">
<object width="425" height="344">
	<param name="movie" value="http://www.youtube.com/v/<?= $yid ?>&amp;hl=nl&amp;fs=1" />
	<param name="allowFullScreen" value="true" />
	<param name="allowscriptaccess" value="always" />
	<embed src="http://www.youtube.com/v/<?= $yid ?>&amp;hl=nl&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed>
</object>
</body>
</html>
