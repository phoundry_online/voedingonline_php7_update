<?php
	set_time_limit(0);
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'import');

	$phoundry = new Phoundry($TID);

	// Some global variables:
	$tmpDir  = $PHprefs['tmpDir'];
	$csvFile = $tmpDir . '/' . str_replace(array('.','/'), array('',''), $_POST['csv_file']);

	$fieldSep = ($_POST['fieldSep'] == '\t') ? "\t" : $_POST['fieldSep'];
	$enclosure = $_POST['enclosure'];
	
	// Match CSV-fields with DB-fields:
	$fields = array();
	foreach($_POST as $name=>$value) {
		if (preg_match('/^CSV_([0-9]+)$/', $name, $reg) && $value != -1) {
			$fields[$reg[1]] = $value;
		}
	}

	// Make sure all notnull fields are present in the CSV file:
	$extras = array();
	$errors = array();
	foreach($phoundry->cols as $col) {
		if ($col->datatype->isXref) {
			if ($col->required && !empty($col->datatype->default)) {
				$extras[] = "INSERT INTO {$col->datatype->xrefTable} ({$col->datatype->srcCol}, {$col->datatype->dstCol}) VALUES ('{\$RID}', '" . escDBquote($col->datatype->default) . "')";
			}
			continue;
		}
		if ($col->required && $col->name != $phoundry->key && !in_array($col->name, $fields)) {
			$errors[] = word(124, $col->name);
		}
	}

	// Remove old (> 1 hr) import files that might be wandering around...
	if (is_dir($tmpDir)) {
		$handle = opendir($tmpDir);
		while(($file = readdir($handle)) !== false) {
			if (preg_match('/^import_/', $file)) {
				if (time() - 3600 > filemtime("$tmpDir/$file")) {
					unlink("$tmpDir/$file");
				}
			}
		}
		closedir($handle);
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(166) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body>
<?php if (count($errors)) { ?>
<fieldset><legend><b><?= word(127) ?></b></legend><ul>
<?php
	foreach ($errors as $error)
		print '<li>' . $error . '</li>';
?>
</ul></fieldset>
<p>
<input style="float:right" type="button" value="<?= word(45) ?>" onclick="document.location='import_csv.php?TID=<?= $TID ?>'" />
</p>
<?php } else { ?>
<h2><?= word(142) ?>...</h2>
<dl>
<?php
	// Here we start the actual import-process:

	//ini_set('auto_detect_line_endings',true);
	$fp = fopen($csvFile, 'r')
		or trigger_error("Unable to open file $csvFile", E_USER_ERROR);
	if ($_POST['skip1st'] == 1) {
		// Skip first record.
		$data = fgetcsv($fp, 0, $fieldSep, $enclosure);
	}
	$recordsRead = $recordsFail = 0;
	while($data = fgetcsv($fp, 0, $fieldSep, $enclosure)) {
		$recordsRead++;
		$vals = array();

		if (count($data) == 1 && empty($data[0])) {
			print '<em>Warning: empty line!</em><br />';
			continue;
		}

		foreach($fields as $index=>$value) {
			$fieldVal = isset($data[$index]) ? trim($data[$index]) : '';
			$vals[$fields[$index]] = escDBquote($fieldVal);
		}
		$sql = "INSERT INTO {$phoundry->name} (" . implode(',', $fields) . ") VALUES ('" . implode("','", $vals) . "')";
		print '<dt>' . PH::htmlspecialchars($sql) . '</dt><dd>';
		$res = $db->Query($sql);
		if ($res) {
			print '<b style="color:blue">Success!</b><br />';
			$RID = $db->GetLastInsertId($phoundry->name, $phoundry->key);

			foreach($extras as $extra) {
				$sql = str_replace('{$RID}', $RID, $extra);
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			}

			$eventRes = $phoundry->doEvent('post-insert', $RID);
			if ($eventRes !== true) {
				print '<em>Error: post-insert event failed for record ' . $RID . '!</em><br />';
			}
		}
		else {
			$recordsFail++;
			print '<b style="color:red">Failed:</b> ' . $db->Error() . '<br />';
		}
		print '</dd>';
	}
	fclose($fp);

	$msg = word(182, $recordsRead, $recordsFail);
?>
</dl>
<hr noshade="noshade" size="1" style="color:#000;background:#000" />
<b><?= $msg ?></b>
<script type="text/javascript">

alert('<?= $msg ?>');

</script>

<p>
<input type="button" class="okBut" value="<?= word(82) ?>" onclick="parent.location.reload()" />
</p>
<?php } ?>
</body>
</html>
