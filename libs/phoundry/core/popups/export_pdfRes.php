<?php
	set_time_limit(0);
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	session_cache_limiter('public'); // Fix weird IE download problems
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'export');

	$phoundry = new Phoundry($TID);

	$html = '<html xmlns="http://www.w3.org/1999/xhtml">';
	$html .= '<head>';
	$html .= '<title>' . $phoundry->description . '</title>';
	$html .= '<meta name="author" content="Web Power" />';
	$html .= '<meta name="copyright" content="Copyright ' . date('Y') . ' by Web Power" />';
	$html .= '<meta name="generator" content="' . $PRODUCT['name'] . ' (' .  $PRODUCT['version'] . ')" />';
	$html .= '</head><body>';
	$html .= $phoundry->viewPDFpage(explode('|', $_REQUEST['RIDs']), explode(',',$_REQUEST['PHshow']), $_REQUEST['PHordr'], $_REQUEST['pagePerRecord']);
	$html .= '</body></html>';

	//print $html; exit;

	// HTMLdoc only supports iso-8859-1:
	if ($PHprefs['charset'] == 'utf-8') {
		$html = PH::iconv('utf-8', 'iso-8859-1', $html);
	}

	$tmpName = $PHprefs['tmpDir'] . '/' . uniqid('PDF') . '.pdf';

	$fp = fopen($tmpName, 'w');
	if ($fp) {
		fwrite($fp, $html);
		fclose($fp);
	}
	else 
		die ("Could not open $tmpName");
	
	$path = realpath(dirname($_SERVER['DOCUMENT_ROOT'])) . ';' . $PHprefs['distDir'] . ';' . $PHprefs['instDir'];

	$exec = isset($PHprefs['paths']['htmldoc']) ? $PHprefs['paths']['htmldoc'] : 'htmldoc';
	$cmd = "$exec --path \"$path\" --quiet --format pdf14 --charset iso-8859-1 --left 10mm --fontsize {$_REQUEST['fontSize']} --size A4 --bodyfont {$_REQUEST['fontFamily']} --textfont {$_REQUEST['fontFamily']} --linkcolor black --footer ./d --header .t. --webpage " . escapeshellarg($tmpName);

	header("Content-type: application/pdf");
	header("Content-Disposition: inline; filename=\"{$phoundry->name}.pdf\"");
	print shell_exec("($cmd) 2>&1");
	unlink($tmpName);
