<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	if (!isset($_SESSION['error'])) {
		die('Cannot send error (missing from SESSION)!');
	}

	if (isset($PHprefs['errorRcpts'])) {
		$rcpt = $PHprefs['errorRcpts'];
	}
	elseif ($PRODUCT['name'] == 'DMdelivery') {
		$rcpt = 'scripterrors@dmdelivery.com';
	}
	else {
		$rcpt = 'support@cherry-marketing.nl';
	}
	PH::sendHTMLemail($rcpt, "PHP error {$PRODUCT['name']}", $_SESSION['error'], null, 'zwartgat@webpower.nl');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Send error</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body style="overflow:hidden">

<fieldset>
<legend><b>Error sent</b></legend>
The error has been sent to Web Power. They will try to fix the problem as soon as possible.<br />
Your email address (<?= $PHSes->email ?>) was also sent to them, so they can contact you if needed.
</fieldset>

<p>
<button type="button" style="float:right" class="okBut" onclick="window.close()"><?= word(82) ?></button>
</p>
</body>
</html>
