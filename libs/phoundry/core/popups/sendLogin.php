<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$uid = (int)$_GET['RID'];
	$sql = "SELECT username, e_mail FROM phoundry_user WHERE id = {$uid}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$email = $db->FetchResult($cur,0,'e_mail');
	$username = $db->FetchResult($cur,0,'username');

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$password = PH::generatePassword();

		$sql = "UPDATE phoundry_user SET password = '" . sha1($uid . PH::$pwdSalt . md5($password)) . "' WHERE id = {$uid}";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		$rcpts = array();
		if (in_array('user', $_POST['rcpt'])) {
			$rcpts[] = $email;
		}
		if (in_array('myself', $_POST['rcpt'])) {
			$rcpts[] = $PHSes->email;
		}

		$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
		$url = $protocol . $_SERVER['HTTP_HOST'] . $PHprefs['url'];

		$body = word(308, $PRODUCT['name'], $url, $username, $password);
		PH::sendHTMLemail($rcpts, word(350, $PRODUCT['name'] /* login credentials */), $body);

		print "<script>with(parent){setTimeout(function(){alert('" . escSquote(word(307 /* Login credentials sent */)) . "')}, 50);killPopup()}</script>";
		exit;
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title><?= word(309, $PRODUCT['name'] /*  login credentials */) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<script type="text/javascript" src="<?=$PHprefs['url']?>/core/csjs/global.js.php?<?=QS()?>"></script>
</head>
<body>
<form action="<?= $_SERVER['PHP_SELF'] . '?' . QS(1) ?>" method="post">
<div class="info" style="font-size:11px">
<?= word(311, $PRODUCT['name']) ?>
</div>
<fieldset>
<legend><b><?= word(310 /* Recipient(s) */) ?></b></legend>
<input type="checkbox" name="rcpt[]" value="user" checked="checked" /> <?= $email ?>
<br />
<input type="checkbox" name="rcpt[]" value="myself" /> <?= $PHSes->email ?>
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(46) /* Cancel */ ?>" onclick="parent.killPopup()" />
<input class="okBut" type="submit" value="<?= word(82 /* Ok */) ?>" />
</p>

</form>
</body>
</html>
