<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	if (empty($_GET['resetPwd'])) {
		die('Invalid request.');
	}

	$user = array('lang'=>'en');
	$errors = array();

	// Make sure we can decode the resetPwd argument:
	$dec = PH::WPdecode($_GET['resetPwd']);
	// Format: lang_userID_timestamp_passwordHash
	if (false === $dec || !preg_match('/^(\w{2})_(\d+)_(\d+)_(.+)$/', $dec, $reg))
	{
		$errors[] = 'The reset password link is invalid (corrupt).';
	}
	else
	{
		$file = "{$PHprefs['distDir']}/core/lang/{$reg[1]}/words.php";
		if (!file_exists($file)) {
			$file = "{$PHprefs['distDir']}/core/lang/en/words.php";
		}
		include $file;

		if ((int)$reg[3] < time() - 7200) {
			$errors[] = word(456 /* The reset password link has expired */);
		}
		else
		{
			$user['id'] = (int)$reg[2];
			$sql = "SELECT name, password FROM phoundry_user WHERE id = {$user['id']}";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$user['name'] = $db->FetchResult($cur,0,'name');

			if (sha1($db->FetchResult($cur,0,'password')) != $reg[4]) {
				$errors[] = word(457 /* You already reset your password */);
			}
		}
	}

	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// Check if the new passwords aren't empty.
		if (trim($_POST['new_pwd']) == '' || trim($_POST['new_pwd_cNfRm_']) == '')
		{
			$errors[] = word(66);
		}
		// Check if the new passwords are equal.
		elseif ($_POST['new_pwd'] != $_POST['new_pwd_cNfRm_'])
		{
			$errors[] = word(67);
		}
		if (empty($errors))
		{
			// Check password strength:
			$errors = array_merge($errors, PH::checkPasswordStrength($_POST['new_pwd'], word(71) . ' ' . word(72)));
		}

		if (empty($errors))
		{
			$newSha1Password = sha1($user['id'] . PH::$pwdSalt . md5($_POST['new_pwd']));
			$sql = "UPDATE phoundry_user SET password = '" . escDBquote($newSha1Password) . "', password_set = '" . date('Y-m-d H-i-s') . "' WHERE id = {$user['id']}";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$msg = word(286 /* Your password has been changed */);
		}
	}

    list($CSRFname, $CSRFtoken) = PH::startCSRF();
	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(285 /* Reset password */) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript">

function init()
{
	document.forms[0]['new_pwd'].focus();
    CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken?>');
}

</script>
</head>
<body style="overflow:hidden" onload="init()">
<form action="?<?= QS(1) ?>" method="post" onsubmit="return submitForm(this)">
<?php
	if (!empty($errors)) {
		print '<ul style="padding:0;margin-left:2em;color:#f00">';
		print '<li>' . implode('</li><li>', $errors) . '</li>';
		print '</ul>';
		if ($_SERVER['REQUEST_METHOD'] == 'GET') {
			print '</body></html>';
			exit;
		}
	}
?>

<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($errors))
	{
		print '<p>' . $msg . '</p>';
		print '<input style="float:right" type="button" value="' . word(82 /* Ok */) . '" onclick="parent.killPopup()" />';
		print '</body></html>';
		exit;
	}
?>
<div class="miniinfo">
<?= word(254, $user['name'] /* Please enter a new password for user #1 */) ?>
</div>

<fieldset><legend><b class="req"><?= word(71) . ' ' . word(72) ?></b></legend>
<input class="checkStrength" type="password" name="new_pwd" size="40" maxlength="24" />
</fieldset>

<fieldset class="field"><legend><b class="req"><?= word(62) . ' ' . word(71) . ' ' . word(72) ?></b></legend>
<input type="password" name="new_pwd_cNfRm_" size="40" maxlength="24" />
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(46 /* Cancel */) ?>" onclick="parent.killPopup()" />
<input class="okBut" type="submit" value="<?= word(82 /* Ok */) ?>" />
</p>

</form>
</body>
</html>
