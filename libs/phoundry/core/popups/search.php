<?php
	/**
	 * Explanation of meaning phoundry_column.searchable
	 * 
	 *   Searchable   | In_overview | DB-value
	 *   =============+=============+=========
	 *   enabled      |    hide     |    0
	 *   enabled      |    show     |    1
	 *   disabled     |    hide     |    2
	 *   disabled     |    show     |    3
	 *   disabled_all |    hide     |    4
	 *   disabled_all |    show     |    5
	 */

	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title><?= word(10) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../csjs/global.js.php?noFrames"></script>
<script type="text/javascript">

var _D = document;

function submitForm(F) {
	var l, x, s_in = '', show = '', Fparent = parent._D.forms[0];
	l = F['PHshow'].length;
	for (x = 0; x < l; x++) {
		if (F['PHshow'][x].checked) {
			if (show != '') show += ',';
			show += F['PHshow'][x].value;
		}
	}
	if (show == '')
		return false;

	l = F['PHs_in'].length;
	for (x = 0; x < l; x++) {
		if (F['PHs_in'][x].checked) {
			if (s_in != '') s_in += ',';
			s_in += F['PHs_in'][x].value;
		}
	}
	Fparent['PHs_in'].value = s_in;
	Fparent['PHshow'].value = show;
	Fparent['PHsrch'].value = F['PHsrch'].value.replace(/^\s+|\s+$/g, '');
	Fparent['PHpage'].value = 0;
	$('.submitBut').prop('disabled',true);
	Fparent.submit();
	return false;
}

function checkAll(what) {
	var F = _D.forms[0], l, x;
	l = F['PH' + what].length;
	for (x = 0; x < l; x++) {
		F['PH' + what][x].checked = true;
	}
}

function init() {
	var Fself = _D.forms[0], Fparent = parent._D.forms[0], x, show = [], s_in = [];
	show = Fparent['PHshow'].value; if (show != '') show = show.split(',');
	s_in = Fparent['PHs_in'].value; if (s_in != '') s_in = s_in.split(',');
	Fself['PHsrch'].value = Fparent['PHsrch'].value;
	for (x = 0; x < show.length; x++) {
		$('#PHshow_' + show[x]).prop('checked',true);
	}
	for (x = 0; x < s_in.length; x++) {
		$('#PHs_in_' + s_in[x]).prop('checked',true);
	}
	Fself['PHsrch'].focus();
}

</script>
</head>
<body onload="init()">
<div id="searchDiv">
<form action="records.php" method="post" onsubmit="return submitForm(this)">
<fieldset>
<legend><b class="req"><?= word(96) ?></b> (<a href="#" onclick="el=_D.forms[0]['PHsrch'];el.value='';el.focus();return false"><?= word(161) ?></a>, <a href="#" onclick="$('#searchDiv,#helpDiv').toggle();return false">help</a>)</legend>
	<div class="miniinfo"><?= word(236 /* Use wildcards (* and ?) to search inside texts: *searchterm* */) ?></div>
	<input type="text" name="PHsrch" id="PHsrch" size="20" />
	<input class="submitBut" type="submit" value="<?= word(10) ?>" />
</fieldset>

<fieldset>
<legend><b class="req"><?= word(136) ?></b></legend>

<table class="view-table" width="100%">
<tr>
	<th><?= word(137) ?></th>
	<th><a href="#" onclick="checkAll('show');return false"><?= word(138) ?></a></th>
	<th><a href="#" onclick="checkAll('s_in');return false"><?= word(139) ?></a></th>
</tr>

<?php
	$x = 0;
	foreach($phoundry->cols as $col) {
		if (strtolower(get_class($col->datatype)) == 'dtfake') continue;
		$showSearch = true;
		if ($col->datatype->isXref) { $showSearch = false; }
		if (isset($col->inputtype->extra['query']) && preg_match('/^SELECT/i', $col->inputtype->extra['query']) && (isset($col->inputtype->extra['searchable']) && $col->inputtype->extra['searchable'] != true)) { $showSearch = false; }
		if (in_array($col->searchable, array(4,5))) { $showSearch = false; }

		if (isset($col->rights['view'])) {
			print '<tr class="' . ($x & 1 ? 'even' : 'odd') . '">';
			print '<td>' . $col->description . '</td>';
			print '<td align="center"><input type="checkbox" id="PHshow_' . $col->name . '" name="PHshow" value="' . $col->name . '" /></td>';
			if ($showSearch) {
				print '<td align="center"><input type="checkbox" id="PHs_in_' . $col->name . '" name="PHs_in" value="' . $col->name . '" /></td>';
			} else {
				print '<td></td>';
			}
			print "</tr>\n";
			$x++;
		}
	}
?>
</table>
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
<input class="submitBut" type="submit" value="<?= word(10) ?>" />
</p>
</form>
</div>

<div id="helpDiv" style="position:absolute;top:6px;left:6px;display:none">
<?php include "{$PHprefs['distDir']}/core/lang/{$PHSes->lang}/searchHelp.php"; ?>
</div>

</body>
</html>
