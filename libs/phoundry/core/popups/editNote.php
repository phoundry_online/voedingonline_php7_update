<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		$till_date = $_POST['till_date'];
		$day   = substr($till_date, strpos($g_dateFormat, 'DD'), 2);
		$month = substr($till_date, strpos($g_dateFormat, 'MM'), 2);
		$year  = substr($till_date, strpos($g_dateFormat, 'YYYY'), 4);
		$till_date = "$year-$month-$day";

		if (isset($_GET['NID'])) {
			$sql = "UPDATE phoundry_note SET till_date = '" . escDBquote($till_date) . "', title = '" . escDBquote($_POST['title']) . "', contents = '" . escDBquote($_POST['contents']) . "' WHERE id = " . (int)$_GET['NID'];
		}
		else {
			if (isset($PHprefs['product']) && $PHprefs['product'] == 'DMdelivery') {
				$DMDcid = 0;
				if (isset($PHenv) && isset($PHenv['DMDcid'])) {
					$DMDcid = $PHenv['DMDcid'];
				}
				$sql = "INSERT INTO phoundry_note VALUES(NULL, $DMDcid, $TID, {$PHSes->userId}, '" . escDBquote($till_date) . "', '" . escDBquote($_POST['title']) . "', '" . escDBquote($_POST['contents']) . "')";
			}
			else {
				$sql = "INSERT INTO phoundry_note VALUES(NULL, $TID, {$PHSes->userId}, '" . escDBquote($till_date) . "', '" . escDBquote($_POST['title']) . "', '" . escDBquote($_POST['contents']) . "')";
			}
		}
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		print '<script type="text/javascript">' . "\n";
		print "//<![CDATA[\n";
		print "parent.location.reload();\n";
		print "//]]>\n";
		print "</script>\n";
		exit();
	}

	$dateFormat = str_replace(array('DD','MM','YYYY'), array('d','m','Y'), $g_dateFormat);
	if (isset($_GET['NID'])) {
		if (isset($_GET['del'])) {
			$sql = "DELETE FROM phoundry_note WHERE id = " . (int)$_GET['NID'];
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			exit();
		}
		$sql = "SELECT till_date, title, contents FROM phoundry_note WHERE id = " . (int)$_GET['NID'];
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$db->FetchResultAssoc($cur,$row,0);
		$row['till_date'] = date($dateFormat, strtotime($row['till_date']));
	}
	else {
		$row = array();
		$row['till_date'] = date($dateFormat, time()+(86400*30));
		$row['title'] = $row['contents'] = '';
	}
	
	list($CSRFname, $CSRFtoken) = PH::startCSRF();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(isset($_GET['NID']) ? 197 : 196) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript">

<?php
	$dPos = strpos($g_dateFormat, 'DD');
	$mPos = strpos($g_dateFormat, 'MM');
	$yPos = strpos($g_dateFormat, 'YYYY');
?>

if (typeof String.prototype.trim == 'undefined') {
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };
}

function checkDTdate(desc, value) {
	var D, M, Y, tmp;
	D = parseInt(value.substring(<?= $dPos ?>,<?= $dPos ?>+2),10);
	M = parseInt(value.substring(<?= $mPos ?>,<?= $mPos ?>+2),10)-1;
	Y = parseInt(value.substring(<?= $yPos ?>,<?= $yPos ?>+4),10);
	tmp = new Date(Y,M,D);
	if ((tmp.getFullYear()==Y) && (tmp.getMonth()==M) && (tmp.getDate()==D))
		return '';
	else
		return '<?= escSquote(word(38)) ?>'.replace(/#1/, desc) + '\n';
}

function submitForm(F) {
	var errMsg = '';
	if (F['title'].value.trim() == '')
		errMsg += '<?= escSquote(word(43, word(198))) ?>\n';
	errMsg += checkDTdate('<?= escSquote(word(200)) ?>', F['till_date'].value);
	if (F['contents'].value.trim() == '')
		errMsg += '<?= escSquote(word(43, word(199))) ?>\n';
	if (errMsg == '')
		return true;
	alert(errMsg);
	return false;
}

function init()
{
	CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');
	document.forms[0]['title'].focus();
}

</script>
</head>
<body style="overflow:hidden" onload="init()">
<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= QS(1) ?>" method="post" onsubmit="return submitForm(this)">

<div class="fieldset-wrapper fieldset-wrapper-req">
<fieldset><legend><b class="req"><?= word(198) ?></b></legend>
<input type="text" name="title" size="40" maxlength="60" style="width:98%" value="<?= PH::htmlspecialchars($row['title']) ?>" />
</fieldset>
</div>

<div class="fieldset-wrapper fieldset-wrapper-req">
<fieldset><legend><b class="req"><?= word(200) ?></b></legend>
<input type="text" name="till_date" size="10" maxlength="10" value="<?= $row['till_date'] ?>" />
</fieldset>
</div>

<div class="fieldset-wrapper fieldset-wrapper-req">
<fieldset><legend><b class="req"><?= word(199) ?></b></legend>
<textarea class="editNote-contents" name="contents" cols="40" rows="8" style="width:98%"><?= PH::htmlspecialchars($row['contents']) ?></textarea>
</fieldset>
</div>

<p>
<input style="float:right" type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
<input class="okBut" type="submit" value="<?= word(82) ?>" />
</p>
</form>
</body>
</html>
