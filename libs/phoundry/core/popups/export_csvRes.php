<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	session_cache_limiter('public'); // Fix weird IE download problems
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
	
	set_time_limit(0);

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'export');
  
	$phoundry = new Phoundry($TID);
	$db = PH::DBconnect();
   
	$fieldSep	= $_POST['fieldSep'];
	$enclosure	= $_POST['enclosure'];
	$charset		= $_POST['charset'];
	$raw        = $_POST['raw'] == 1;
	$showHeader	= $_POST['showHeader'] == 1;
	$save       = isset($_POST['action_save']);
	
	$errors = array();
	if (empty($fieldSep)) {
		$errors[] = word(43, 'field separator');
	}
	if (empty($enclosure)) {
		$errors[] = word(43, 'text delimiter');
	}

	if (count($errors)) {
		$errMsg = '<ul>';
		foreach($errors as $error) {
			$errMsg .= '<li>' . $error . '</li>';
		}
		$errMsg .= '</ul>';
		trigger_error($errMsg, E_USER_ERROR);
	}
   
	$fieldSep = stripslashes($fieldSep);
	$enclosure = stripslashes($enclosure);

	$request = $phoundry->get2struct($_GET);
	$query = $phoundry->makeQuery($request);
   
	if ($save) {
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Disposition: attachment; filename={$phoundry->name}.csv");
		header("Content-type: application/csv; charset={$charset}");
		header("Content-Transfer-Encoding: binary"); 
		//$db->SetCharset($charset == 'utf-8' ? 'utf8' : 'latin1');
	} else {
		PH::getHeader('text/html');
		echo '<!DOCTYPE html>' . "\n";
		echo '<html>';
		echo '<head>';
		echo '<meta http-equiv="content-type" content="text/html; charset=' . $PHprefs['charset'] . '" />';
		echo '<title>CSV</title>';
		echo '<link rel="stylesheet" href="' . $PHprefs['customUrls']['css'] . '/popup.css" type="text/css" />';
		echo '</head>';
		echo '<body>';
		echo "\n<xmp>\n";
	}

	$fields = array();
	foreach($phoundry->cols as $col) {
		if (strtolower(get_class($col->datatype)) == 'dtfake' ||
		    !in_array($col->name, $request['show']) ||
			($col->datatype->isXref && $raw)
			)
		{
			continue;
		}
		$fields[$col->name] = str_replace($enclosure, "{$enclosure}{$enclosure}", $col->description);
	}

   if ($raw) {
		$ands = array();
		foreach($phoundry->filters as $filter) {
			if ($filter['type'] == 'system') {
				$ands = array_merge($ands, PH::getFilter(unserialize($filter['wherepart'])));
			}
		}
		$sql = sprintf('SELECT `%s` FROM `%s`', implode('`,`', array_keys($fields)), $phoundry->name);
		if (!empty($ands)) {
			$sql .= ' WHERE ' . implode(' AND ', $ands);
		}
		PH::CSVexport($sql, true, $fieldSep, $enclosure, $charset);
   }
	else {
		if ($showHeader) {
			// Draw column headers:
			print implode($fieldSep, $fields) . PHP_EOL;
		}

      $row = 0;
      $cur = $db->Query($query['recordSQL'])
         or trigger_error("Query {$query['recordSQL']} failed: " . $db->Error(), E_USER_ERROR);
      while(!$db->EndOfResult($cur)) {
         $x = 0;
         $recordID = @$db->FetchResult($cur,$row,0);
         foreach($phoundry->cols as $col) {
				if (strtolower(get_class($col->datatype)) == 'dtfake' || !in_array($col->name, $request['show'])) {
               continue;
				}
            if ($x > 0) print $fieldSep;
            $GLOBALS['RECORD_ID'] = is_null($db->FetchResult($cur,$row,$col->name)) ? 'NULL' : $db->FetchResult($cur,$row,$col->name);

            $cnt = $col->getValue('export',$recordID,$cur,$row,$PHprefs['recordStrlen']);
            $cnt = str_replace(array("\r", "\n", $enclosure), array('', ' ', "$enclosure$enclosure"), $cnt);
            //$cnt = stripslashes($cnt);
            if (strpos($cnt, $enclosure) !== false || strpos($cnt, $fieldSep) !== false) {
               print $enclosure . $cnt . $enclosure;
				}
            else {
               print $cnt;
				}
            $x++;
         }
         print "\r\n";
         $row++;
      }
   }   
      
	if (!$save) {
		echo '</xmp><p><button type="button" style="float:right" onclick="if(history.length)history.back();else window.close()">' . word(45) . '</button></p></body></html>';
	}
