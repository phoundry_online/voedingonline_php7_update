<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$RID = isset($_GET['RID']) ? (int)$_GET['RID'] : null;
	$CID = (int)$_GET['CID'];

	$sql = "SELECT t.name as tname, c.name as cname, c.inputtype, c.datatype_extra FROM phoundry_table t, phoundry_column c WHERE t.id = {$TID} AND c.id = {$CID} AND c.table_id = t.id";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if ($db->EndOfResult($cur)) {
		PH::phoundryErrorPrinter('Cannot find file to preview!', true);
	}

	$cname = $db->FetchResult($cur,0,'cname');
	$tname = $db->FetchResult($cur,0,'tname');
	$DTextra = unserialize($db->FetchResult($cur,0,'datatype_extra'));
	$InputType = $db->FetchResult($cur,0,'inputtype');

	$db->GetTableIndexDefinition($tname, 'PRIMARY', $index)
		or trigger_error("Cannot get index for table {$tname}: " . $db->Error(), E_USER_ERROR);
	$key = array_keys($index['FIELDS']);
	$key = $key[0];

	/*
	 * $cname contains the filename.
	 * If datatype == 'blob', there should be a {$cname}_blob field as well, which contains the binary file.
	 */
	$isDBfile = false;
	if (!empty($_GET['file'])) {
		$fileName = PH::evaluate($_GET['file']);
	}
	else {
		$sql = "SELECT * FROM {$tname} WHERE {$key} = {$RID}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$fileName = $db->FetchResult($cur,0,$cname);
		if ($fileContents = @$db->FetchResult($cur,0,$cname . '_blob')) {
			$isDBfile = true;
		}
		else {
			// Is there a 'savedir' defined?
			if (isset($DTextra['savedir'])) {
				$saveDir = PH::evaluate($DTextra['savedir']);
				if (isset($PHprefs['product']) && $PHprefs['product'] == 'DMdelivery' && isset($PHenv['DMDcid'])) {
					$saveDir = str_replace('{$CAMPAIGN_ID}', $PHenv['DMDcid'], $saveDir);
				}
				if($InputType == 'ITfileupload' && !preg_match("|^/{$saveDir}|", $fileName)) {
					$fileName = $saveDir . '/' . $fileName;
				}
			}
		}
	}

	$filePath = $PHprefs['uploadDir'] . '/' . $fileName;
	$ext = strtolower(substr($fileName, strrpos($fileName, '.')+1));
	$imageExts = array('gif', 'jpg', 'jpeg', 'png');

	// Make sure file still exists:
	if (!$isDBfile && !file_exists($filePath)) {
		$assetTag = '<p><b style="color:red">' . word(91) /* This file no longer exists on disk! */ . '</b></p>';
	}
	else {

	if (in_array($ext, $imageExts) || $ext == 'swf') {
		if ($isDBfile) {
			// Temporarily store the file on disk in order to get the imagesize:
			$tmpName = tempnam($PHprefs['tmpDir'], 'imgsize');
			if ($fp = fopen($tmpName, 'wb')) {
				fwrite($fp, $fileContents);
				fclose($fp);
				$size = getimagesize($tmpName);
				unlink($tmpName);
			}
		}
		else {
			$size = getimagesize($filePath);
			$mtime = filemtime($filePath);
		}

		$width = $size[0];
		$height = $size[1];
		$shrink = false;

		if ($width > 370 || $height > 262) {
			$shrink = true;
			$factor = max($width/370, $height/262);
			$width = floor($width/$factor);
			$height = floor($height/$factor);
		}
	}

	$src = $isDBfile ? $PHprefs['url'] . '/core/blob.php?' . QS(1) : $PHprefs['uploadUrl'] . '/' . PH::urlencodeURL($fileName);
	$src = preg_replace('/[\/]+/', '/', $src);
	$dateFormat = $g_dateFormat . ' H:i';

	if (in_array($ext, $imageExts)) {
		$alt = $size[0] . ' x ' . $size[1] . ' pixels';
		if ($shrink) {
			$alt .= " [resized]";
		}
		if (!$isDBfile) {
			$alt .= "\n" . date($dateFormat, $mtime);
		}
		if ($shrink) {
			$assetTag = '<a href="' . $src . '" onclick="window.open(this.href,\'Preview\',\'width=' . ($size[0]+10) . ',height=' . ($size[1]+10) . ',status=no\');return false"><img src="' . $src . '" alt="' . $alt . '" width="' . $width . '" height="' . $height . '" alt="' . $alt . '" border="0" /></a>';
		}
		else {
			$assetTag = '<img src="' . $src . '" alt="' . $alt . '" width="' . $width . '" height="' . $height . '" />';
		}
	}
	else if ($ext == 'swf') {
		$assetTag =<<< EOA
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="{$width}" height="{$height}" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0">
		<param name="movie" value="{$src}" />
		<param name="quality" value="high" />
		<embed src="{$src}" quality="high" width="{$width}" height="{$height}" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash">
		</embed></object>
EOA;
	}
	else {
		$file = isset($_GET['file']) ? str_replace('..', '', $_GET['file']) : null;
		if (!is_null($file)) {
			$src = $PHprefs['uploadUrl'] . '/' . PH::urlencodeURL($file);
		}
		$assetTag = '<iframe src="' . $src . '" width="100%" height="100%" frameborder="0" style="border:1px inset window"></iframe>';
	}


	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title>Preview</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body style="overflow:hidden;height:100%;margin:0;padding:0">

<?php if (in_array($ext, $imageExts) || $ext == 'swf') { ?>
<fieldset style="margin:6px">
<legend><b><?= PH::htmlspecialchars($fileName) ?></b></legend>
<center>
<?php } ?>

<?= $assetTag ?>

<?php if (in_array($ext, $imageExts) || $ext == 'swf') { ?>
</center>
</fieldset>
<?php } ?>

</body>
</html>
