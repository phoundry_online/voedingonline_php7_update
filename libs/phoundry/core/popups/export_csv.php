<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction'])) {
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	}

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'export');

	list($CSRFname, $CSRFtoken) = PH::startCSRF();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(144) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript">

var _F = null;

function init() {
	CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');
	_F = document.forms[0]; var P = parent.document.forms[0];
	_F['PHshow'].value = P['PHshow'].value;
	_F['PHordr'].value = P['PHordr'].value;
	<?php	if (isset($_GET['RID'])) {	?>
	_F['RIDs'].value  = '<?= $_GET['RID'] ?>';
	<?php } else { ?>
	_F['RIDs'].value  = parent.document.getElementById('_RIDs').innerHTML;
	<?php } ?>
}

</script>
</head>
<body onload="init()" style="overflow:hidden">
<form action="export_csvRes.php?<?= QS(1) ?>" method="post">
<input type="hidden" name="RIDs" value="" />
<input type="hidden" name="PHshow" value="" />
<input type="hidden" name="PHordr" value="" />

<fieldset><legend><b class="req"><?= word(88) /* Explanation */ ?></b></legend>
<?= word(93) ?>
</fieldset>

<fieldset><legend><b class="req"><?= word(205) /* Export format */ ?></b></legend>
<table cellpadding="0" cellspacing="2">
<tr><td valign="top"><input type="radio" name="raw" value="0" checked="checked" /></td><td><?= PH::htmlspecialchars(word(206)) ?></td></tr>
<tr><td valign="top"><input type="radio" name="raw" value="1" /></td><td><?= PH::htmlspecialchars(word(207, $PRODUCT['name'])) ?></td></tr>
</table>
</fieldset>

<fieldset><legend><b class="req"><?= word(87) /* Column names on first row */ ?></b></legend>
<input type="radio" name="showHeader" value="0" /><?= word(0) ?>
<input type="radio" name="showHeader" value="1" checked="checked" /><?= word(1) ?>
</fieldset>

<fieldset><legend><b class="req">Field separator</b></legend>
<input type="text" name="fieldSep" value="<?= $g_csvFieldSep ?>" size="2" maxlength="4" />
</fieldset>

<fieldset><legend><b class="req">Text delimiter</b></legend>
<input type="text" name="enclosure" value="&quot;" size="2" maxlength="4" />
</fieldset>

<fieldset><legend><b class="req"><?= word(301 /* Charset */) ?></b></legend>
<?= word(313 /* Select iso-8859-1 if you want to open CSV file in Excel */) ?><br />
<select name="charset">
<?php
	$opts = array('iso-8859-1', 'utf-8');
	foreach($opts as $opt) {
		$selected = $PHprefs['charset'] == $opt ? ' selected="selected" ' : ' ';
		print '<option' . $selected . 'value="' . $opt . '">' . $opt . '</option>';
	}
?>
</select>
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
<input type="submit" name="action_view" value="<?= word(9) ?>" onclick="_F.target=(event.shiftKey)?'_blank':'_self'" />
<input type="submit" name="action_save" value="<?= word(89) ?>" onclick="_F.target='_self'" />
</p>
</form>
</body>
</html>
