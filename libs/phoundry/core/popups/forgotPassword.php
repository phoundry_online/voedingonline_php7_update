<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$lang = isset($_GET['lang']) ? substr($_GET['lang'],0,2) : $PHprefs['languages'][0];
	$file = "{$PHprefs['distDir']}/core/lang/{$lang}/words.php";
	if (!file_exists($file)) {
		$file = "{$PHprefs['distDir']}/core/lang/en/words.php";
	}
	include $file;

	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		if (!isset($_SESSION['secret']) || $_SESSION['secret'] != $_POST['secret']) {
			die('Invalid request!');
		}
		$email = !empty($_POST['email']) ? trim($_POST['email']) : '';
		$error = false;
		$sql = "SELECT id, name, username, e_mail, password FROM phoundry_user WHERE e_mail = '" . escDBquote($email) . "' AND username != 'webpower'";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		// Give same feedback for found and non-found users, making it impossible to guess
		// usernames by comparing response from the forgot password dialogue.

		if (!$db->EndOfResult($cur))
		{// User found, send 'change your password' email:
			$id = $db->FetchResult($cur,0,'id');
			$hash = PH::WPencode($lang . '_' . $id . '_' . time() . '_' . sha1($db->FetchResult($cur,0,'password')));

			// Send email:
			$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
			$body = word(287, $db->FetchResult($cur,0,'name'), $PRODUCT['name'], "{$protocol}{$_SERVER['HTTP_HOST']}{$PHprefs['url']}?resetPwd=" . $hash);
			PH::sendHTMLemail($db->FetchResult($cur,0,'e_mail'), word(126, $PRODUCT['name']), nl2br($body));
		}
		$msg = word(288, $PRODUCT['name']);
	}
	else {
		$_SESSION['secret'] = uniqid('');
	}

    list($CSRFname, $CSRFtoken) = PH::startCSRF();
	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(283 /* Forgot password */) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript">

function init()
{
	document.forms[0]['email'].focus();
    CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');
}

</script>
</head>
<body style="overflow:hidden" onload="init()">
<form action="?<?= QS(1) ?>" method="post" onsubmit="return submitForm(this)">

<?php if ($_SERVER['REQUEST_METHOD'] == 'GET') { ?>
<input type="hidden" name="secret" value="<?= $_SESSION['secret'] ?>" />

<div class="miniinfo">
<?= word(284 /* Enter your email address to receive a new password */) ?>
</div>
<fieldset><legend><b class="req"><?= word(98 /* Email address */) ?></b></legend>
<input type="text" id="email" name="email" size="40" maxlength="60" style="width:98%" />
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
<input class="okBut" type="submit" value="<?= word(82) ?>" />
</p>
<?php } else { ?>

<?= '<p>' . $msg . '</p>' ?>

<p align="right">
<?php if ($error) { ?>
<input class="okBut" type="button" value="<?= word(45) ?>" onclick="history.back()" />
<?php } ?>
<input class="okBut" type="button" value="<?= word(82) ?>" onclick="parent.killPopup()" />
</p>

<?php } ?>
</form>
</body>
</html>
