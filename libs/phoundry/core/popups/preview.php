<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	checkAccess();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html" />
<title>Preview</title>
</head>
<body>
<script type="text/javascript">
	<?php if(isset($_GET['type']) && $_GET['type'] == 'codemirror') { ?>
	document.write(parent.CM<?= $_GET['field'] ?>.getValue());
	<?php } else { ?>
	document.write(parent.document.forms[0]['<?= $_GET['field'] ?>'].value);
	<?php } ?>
</script>
</body>
</html>
