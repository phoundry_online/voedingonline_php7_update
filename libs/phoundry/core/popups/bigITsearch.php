<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();

	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$CID = (int)$_REQUEST['CID'];

	// Fetch xref name & description:
	$sql = "SELECT name, description, inputtype FROM phoundry_column WHERE id = $CID";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$inputName = $db->FetchResult($cur,0,'name');
	$inputDesc = $db->FetchResult($cur,0,'description');
	$inputtype = $db->FetchResult($cur,0,'inputtype');
	$db->FreeResult($cur);

    list($csrfName, $csrfToken) = PH::startCSRF();
	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title>Duallist</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript">

_D = document;
<?php if ($inputtype == 'ITbigselect') { ?>
function newOption(id) {
	parent.document.forms[0]['<?= $inputName ?>'].value = id;
	parent.document.forms[0]['dsp_<?= $inputName ?>'].value = _D.getElementById('ID_'+id).innerHTML;
	parent.killPopup();
}
<?php } else { ?>
function newOption(id) {
	parent.newOption(id, {}, _D.getElementById('ID_'+id).innerHTML);
	parent.MSupdate();
}
<?php } ?>

function init() {
    _D.forms[0]['_srch'].focus();
    CSRF.insert('<?= $csrfName ?>', '<?= $csrfToken ?>');
}
</script>
</head>
<body onload="init()">
<form action="<?= $_SERVER['PHP_SELF'] . '?' . QS(1) ?>" method="post">

<fieldset>
<legend><b class="req"><?= word(96) ?></b> (<a href="#" onclick="el=_D.forms[0]['_srch'];el.value='';el.focus();return false"><?= word(161) ?></a>)</legend>
	<input type="text" name="_srch" size="20" value="<?= (empty($_POST['_srch'])) ? '' : PH::htmlspecialchars($_POST['_srch']) ?>" style="width:95%" />
	<br /><small><?= word(130, PH::langWord($inputDesc)) ?></small>
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
<input type="submit" class="okBut" value="<?= word(82) ?>" />
</p>

</form>
<?php
if (isset($_POST['_srch'])) {
	print '<fieldset><legend><b>' . word(26) . '</b></legend>';
	$sql = "SELECT datatype_extra, inputtype_extra FROM phoundry_column WHERE id = $CID";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$DTextra = unserialize($db->FetchResult($cur,0,'datatype_extra'));
	$ITextra = unserialize($db->FetchResult($cur,0,'inputtype_extra'));

	$query = $ITextra['query'];
	if (preg_match('/^(.*)\[(.*)\](.*)$/s', $query, $reg)) {
		$query = $reg[1] . $reg[3];
	}

	$from   = PH::parseSQL($query, 'from');
	$select = PH::parseSQL($query, 'select');
   $order  = PH::parseSQL($query, 'order_by');
   $where  = PH::parseSQL($query, 'where');

	$likes = array();
	foreach($ITextra['optiontext'] as $key=>$val) {
		$likes[] = "{$val} LIKE '%" . escDBquote($_POST['_srch']) . "%'";
	}

	$sql = "SELECT {$select} FROM {$from} WHERE ";
	$sql .= '(' . implode(' OR ', $likes) . ')';
	if (!empty($where))
		$sql .= " AND ({$where})";
	if (!empty($order))
		$sql .= " ORDER BY {$order}";

	$sql = PH::evaluate($sql);
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$nrf = $db->NumberOfColumns($cur);

	if ($db->EndOfResult($cur))
		print '<b>' . word(25) . '</b>';
	else {
		print '<table width="100%" cellspacing="1" cellpadding="3">';
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$text = '';
			$id = $db->FetchResult($cur,$x,$ITextra['optionvalue']);
			foreach ($ITextra['optiontext'] as $fld)
				$text .= $db->FetchResult($cur,$x,$fld) . ' ';
			$text = trim($text);
			print '<tr class="' . ($x & 1 ? 'odd' : 'even') . '"><td>';
			print '<a id="ID_' . $id . '" href="#" onclick="newOption(\'' . $id . '\');return false">' . $text . '</a>';
			print '</td></tr>';
		}
		print '</table>';
	}
	print '</fieldset>';
	$db->FreeResult($cur);
}
?>

</body>
</html>
