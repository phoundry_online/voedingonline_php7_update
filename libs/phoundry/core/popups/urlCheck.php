<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
/* @var $PHprefs array */
require_once "{$PHprefs['distDir']}/core/include/common.php";
/* @var $db metabase_database_class */
/* @var $PHSes PHSes */


// Make sure the current user in the current role has access to this page:
checkAccess(null, '');

$errors = array();
if (empty($_GET['_link'])) {
	$errors[] = "Missing Link argument";
} else {
	$link = (array) $_GET['_link'];
}

if (empty($_GET['_protocol'])) {
	$errors[] = 'Missing protocol argument';
} else {
	$protocol = $_GET['_protocol'];
}

if (empty($_GET['_domain'])) {
	$errors[] = 'Missing domain argument';
} else {
	$domain = $_GET['_domain'];
}

$in = isset($_GET['_in']) ? $_GET['_in'] : null;
	
PH::getHeader('text/html');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title>Check Url</title>
<link type="text/css" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" rel="stylesheet" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<script type="text/javascript" src="<?=$PHprefs['url']?>/core/csjs/global.js.php?<?=QS()?>"></script>
<style>
.loading {
	background-image: url('<?=$PHprefs['url']?>/core/icons/throbber_large.gif');
	background-position: center center;
	background-repeat: no-repeat;
}
</style>
<script>
jQuery(function($){
	var link = (<?=json_encode($link)?>),
		protocol = (<?=json_encode($protocol)?>),
		domain = (<?=json_encode($domain)?>),
		tids = (<?=json_encode($in)?>),
		url = '<?=$PHprefs['url']?>/core/preview.php?<?=QS(false, 'popup=close&TID=__TID__&RID=__RID__')?>';
	
	if (link && protocol && domain) {
		$('#loading').show();

		var init = function(data) {
			$('#contentFrame').removeClass('loading');
			if (data.errors && data.errors.length !== 0) {
				$('#errors').html('<li>' + data.errors.join('</li><li>') + '</li>').show();
			} else if (data.results) {
				$('#errors').hide();
				if (data.results.length === 0) {
					$('#no-results').show();
				} else {
					$('#results table').remove();

					$.each(data.results, function(resurl, results) {
						var html = $('<p>'+resurl+'</p>'+
								'<table class="view-table" '+
								'style="width: 100%;">'+
								'<thead>'+
								'<tr><th><?=ucfirst(word(521))?></th>'+
								'<th><?=ucfirst(word(520))?></th></tr>'+
								'</thead>'+
								'<tbody></tbody></table>');

						var tbody = html.find('tbody');
						
						$.each(results, function(i, result) {
							var tr = $('<tr>', {'class' : (i%2 ?'even':'odd')});

							tr.append('<td class="ptr">'+result.table.desc+'</td>');
							tr.append('<td class="ptr">'+result.record.id+'</td>');
							tr.data(
								'urlCheck.href',
								url.replace('__TID__', result.table.id).
									replace('__RID__', result.record.id)
							);
							tbody.append(tr);
						});
						$('#results').append(html);
					});
					
					$('#results').show();
				}
			}

			if (data.links && data.links.join) {
				$('span.links').text(data.links.join(', '));
			}
		}
		
		if (!window.parent || !window.parent.urlCheckInitialLoad) {
			$.ajax({
				url : '<?=$PHprefs['url']?>/core/check_urls/ajax_check_url.php',
				dataType : 'json',
				data : {
					link : link,
					protocol : protocol,
					domain : domain,
					'in' : tids ? tids : ''
				},
				success : init,
				error : function() {
					$('#contentFrame').removeClass('loading');
					$('#errors').html('<li>Error while fetching data</li>').show();
				}
			});
		} else {
			init(window.parent.urlCheckInitialLoad);
			delete window.parent.urlCheckInitialLoad;
		}

		$('#results tbody tr').live('click', function(e){
			e.preventDefault();
			window.parent.makePopup(
				null,
				-20,
				-20,
				(<?=json_encode(ucfirst(word(523 /* Gevonden record */)))?>),
				$(this).data('urlCheck.href'),
				null,
				true
			);
		});

		$('button.reload').bind('click', function(e){
			e.preventDefault();
			window.location.reload();
		});
	}
});
</script>
</head>
<body>
<div id="headerFrame" class="headerFrame">
<?php
$text = implode(', ', $link);
$abbr = strlen($text) <= 63 ? $text : substr($text, 0, 60) . '...';
?>
<?=getHeader(ucfirst(word(522 /* Zoek urls */)), '<span title="'.htmlspecialchars($text).'">'.$abbr.'</span>')?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
</td>
<td align="right">
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame loading">
<!-- Content -->
<ul id="errors" style="display: <?=empty($errors) ? 'none' : 'block'?>;">
<?foreach ($errors as $error):?>
<li><?=$error?></li>
<?endforeach?>
</ul>
<?if (!$errors):?>
<p id="no-results" style="display: none;"><?=word(25)?></p>
<div id="results" style="display: none;">
<div class="info"><?=word(isset($_GET['description']) ? $_GET['description'] : 524)?></div>
</div>
<?endif?>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
		<button class="reload"><?=word(305 /* Refresh */)?></button>
	</td>
	<td align="right">
<?php
	$text = word(48 /* Close */);
	$click = "parent.killPopup()";
?>
	<button type="button" onclick="<?=$click?>"><?=htmlspecialchars($text)?></button>
	</td>
</tr></table>
</div>
</body>
</html>
