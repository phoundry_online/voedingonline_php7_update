<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	checkAccess();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title>Password generator</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<script type="text/javascript">

var _W = window, _D = document;

function generatePassword(len) {
	var special = '~!@#$%^&*()-_=+{}[]|.,;:<>?/';
	var letters = 'abcdefghijklmnopqrstuvwxyz';
	var caps    = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var numbers = '1234567890';
	var password = caps.charAt(Math.random()*caps.length);
	for (var x = 0; x < len - 2; x++) {
		password += letters.charAt(Math.random()*letters.length);
	}
	password += numbers.charAt(Math.random()*numbers.length);
	password += special.charAt(Math.random()*special.length);
	return password;
}

function usePassword() {
	var F=parent.document.forms[0], pass = _D.getElementById('pwdEl').innerHTML;
	F['<?= $_GET['field'] ?>'].value = F['<?= $_GET['field'] ?>_cNfRm_'].value = pass;
	if(_W.clipboardData) {
		_W.clipboardData.setData('Text', pass);
	}
	parent.killPopup();
}

function init() {
	var F=parent.document.forms[0];
	_D.getElementById('pwdEl').innerHTML=generatePassword(8);
}

</script>
</head>
<body onload="init()" style="overflow:hidden">
<fieldset>
<legend><b><?= PH::htmlspecialchars($_GET['desc']) ?></b></legend>
<?php

	print word(115, PH::htmlspecialchars($_GET['desc']));
	print '<p align="center"><b id="pwdEl" style="font-size:12pt"></b></p>';
	
?>
</fieldset>

<p>
<input style="float:right" type="button" value="<?= word(48) /* Close */ ?>" onclick="parent.killPopup()" />
<input type="button" value="<?= word(176) /* Regenerate */ ?>" onclick="_D.getElementById('pwdEl').innerHTML=generatePassword(8)" />
<input type="button" value="<?= word(188) /* Use this password */ ?>" onclick="usePassword()" />
</p>

</body>
</html>
