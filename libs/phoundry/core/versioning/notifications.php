<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once "{$PHprefs['distDir']}/core/include/DB/MetaBase.php";
require_once "{$PHprefs['distDir']}/core/include/Versioning/NotifyService.php";

$service = new Versioning_NotifyService(new DB_MetaBase($db));

$tid = getTID();

if ($tid) {
	$status = $service->getStatus($tid, $PHSes->userId);
	$service->setStatus($tid, $PHSes->userId, !$status);
}

PH::trigger_redirect($_SERVER['HTTP_REFERER']);
