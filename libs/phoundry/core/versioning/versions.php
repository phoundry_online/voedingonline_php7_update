<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

$TID = getTID();
// Make sure the current user in the current role has access to this page:
checkAccess($TID);

$phoundry = new Phoundry($TID);
if ($service = $phoundry->getVersioningService()) {
	if (isset($_GET['table_id'])) {
		$ids = $service->getOrphanVersionIds($_GET['table_id']);
	}
	else {
		if (isset($_GET['site_identifier'])) {
			$ids = $service->getReadyTrails(null, array('site_identifier' => $_GET['site_identifier']));
		} else {
			$ids = $service->getReadyTrails();
		}
	}
	
	$PHSes->limitRIDs[$TID] = $ids ? $ids : array(-1);
}

$url = $phoundry->rUrl."?".QS(false);
header('HTTP/1.1 302 Found', true, 302);
header('URI: '.$url);
header('Location: '.$url);