<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once "{$PHprefs['distDir']}/core/include/DB/MetaBase.php";
require_once "{$PHprefs['distDir']}/core/include/Versioning/Service.php";

$metabase = new DB_MetaBase($db);
$service = new Versioning_Service($metabase);
$version = $service->getVersion($_GET['RID']);

$get = $_GET;
unset($get['RID']);
$get['TID'] = $version->table_id;
$get['_versioning'] = array('parent_id' => $version->id);

if ($version->record_id !== null) {
	$get['RID'] = $version->record_id;
	$page = 'update';
}
else {
	$page = 'insert';
}

$url = $PHprefs['url'].'/core/'.$page.'.php?'.http_build_query($get);

header('HTTP/1.1 302 Found', true, 302);
header('URI: '.$url);
header('Location: '.$url);
