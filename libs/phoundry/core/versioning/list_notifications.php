<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}

require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once "{$PHprefs['distDir']}/core/include/common.php";
checkAccess();

require_once "{$PHprefs['distDir']}/core/include/DB/MetaBase.php";

$metabase = new DB_MetaBase($db);

if (isset($_POST['ok_but'])) {
	$metabase->iQuery(sprintf(
		'DELETE FROM phoundry_versioning_notify WHERE user_id = %d',
		$PHSes->userId
	));
	if (isset($_POST['tables'])) {
		$values = array_map('intval', $_POST['tables']);
		$metabase->iQuery(sprintf('
			INSERT INTO phoundry_versioning_notify (table_id, user_id)
			VALUES %s',
			"(".implode(", {$PHSes->userId}), (", $values).", {$PHSes->userId})"
		));
	}
}

$tables = $metabase->iQuery(sprintf('
	SELECT t.id, t.description, t.extra
	FROM phoundry_group_table AS gt
	INNER JOIN phoundry_table AS t
	ON gt.table_id = t.id
	AND t.is_plugin = 0
	WHERE gt.group_id = %d
	AND gt.rights LIKE "%%u%%"',
	$PHSes->groupId
));

$selected = $metabase->iQuery(sprintf('
	SELECT table_id FROM phoundry_versioning_notify WHERE user_id = %d',
	$PHSes->userId
))->getSelect('table_id');

PH::getHeader('text/html');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="list_notifications" />
<title><?=ucwords(word(352/* versie beheer */))?> <?=ucwords(word(353/* notificaties */))?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?=$PHprefs['url']?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">
//<![CDATA[
jQuery(function($){
	$('#check_all').bind('click', function(e){
		$('[name="tables[]"]').attr('checked', $(this).attr('checked'));
	});
});
//]]>
</script>
<style type="text/css">

</style>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?=getHeader(ucwords(word(353/* notificaties */)), ucwords(word(352/* versie beheer */)))?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>

</td>
<td align="right">

</td>
</tr></table>
</div>
<form action="?<?=QS(true)?>" method="post">
<div id="contentFrame" class="contentFrame">
<!-- Content -->
<table class="sort-table" cellspacing="0">
<thead><tr>
	<th><input id="check_all" type="checkbox"<?if(count($tables) === count($selected)):?> checked="checked"<?endif?> /></th>
	<th><?=ucfirst(word(354/* tabel*/))?></th>
</tr></thead>
<tbody>
<?$i = 0; foreach ($tables as $table):
if (!$table->extra || !PH::is_serialized($table->extra) && !($ser = @unserialize($table->extra)) || empty($ser['versioning'])) {
	continue;
}
$i += 1;
?>
<tr class="<?=$i%2 ? 'odd' : 'even'?>">
	<td>
		<input id="table_<?=$table->id?>" name="tables[]" value="<?=$table->id?>" type="checkbox"<?if(in_array($table->id, $selected)):?> checked="checked"<?endif?> />
	</td>
	<td><label for="table_<?=$table->id?>"><?=htmlspecialchars(PH::langWord($table->description))?></label></td>
</tr>
<?endforeach?>
</tbody>
</table>
<!-- /Content -->
</div>
<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td><input name="ok_but" type="submit" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>" /></td>
	<td align="right">
	<button type="button" onclick="_D.location='<?=$PHprefs['url']?>/layout/home.php'"><?= PH::htmlspecialchars(word(45)) ?></button>
	</td>
</tr></table>
</div>
</form>
</body>
</html>
