<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	$RID = isset($_GET['RID']) ? $_GET['RID'] : null;
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'view');

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);
	
	// load the corresponding version
	$service = $phoundry->getVersioningService();
	
	if (isset($_GET['_trail_id'])) {
		$trail_id = $_GET['_trail_id'];
	}
	else {
		if (isset($_GET['_versioning']['parent_id'])) {
			$version = $service->getVersion($_GET['_versioning']['parent_id']);
		}
		else {
			$version = $service->getLatestVersion($_GET['TID'], $RID);
		}
		
		$trail_id = null;
		if ($version) {
			$trail_id = $version->trail_id;
		}
	}
	if ($trail_id) {
		$versions = $service->getTrail($trail_id);
	}
	else {
		$versions = array();
	}
	
	$version_a = false;
	if (isset($_REQUEST['version']['a'])) {
		foreach ($versions as $version) {
			if ($version->id == $_REQUEST['version']['a']) {
				$version_a = $version;
			}
		}
	}
	$version_b = false;
	if (isset($_REQUEST['version']['b'])) {
		foreach ($versions as $version) {
			if ($version->id == $_REQUEST['version']['b']) {
				$version_b = $version;
			}
		}
	}
	
	$wfMsgItems = require "{$PHprefs['distDir']}/core/include/htmldiff/lang_{$PHSes->lang}.php";
	
	// Callbacks used by htmldiff
	function wfMsg($key) {
		global $wfMsgItems;
		return isset($wfMsgItems[$key]) ? ucfirst($wfMsgItems[$key]) : '!'.$key.'!';
	}
	
	function wfMsgExt($key, $options) {
		$args = func_get_args();
		array_shift($args);
		array_shift($args);
		$options = (array) $options;
		
		$message = wfMsg($key);
		$message = wfMsgReplaceArgs($message, $args);
		switch ($options[0]) {
			case 'parseinline':
				return $message;
			case 'escapenoentities':
				return html_entity_decode($message);
		}
	}
	
	function wfMsgReplaceArgs($message, $args) {
		# Fix windows line-endings
 		# Some messages are split with explode("\n", $msg)
 		$message = str_replace("\r", '', $message);
 	
 		// Replace arguments
 		if (count($args)) {
 			if (is_array($args[0])) {
 				$args = array_values($args[0]);
	 		}
	 		$replacementKeys = array();
	 		foreach ($args as $n => $param) {
	 			$replacementKeys['$' . ( $n + 1 )] = $param;
	 		}
	 		$message = strtr($message, $replacementKeys);
 		}
 	
 		return $message;
 	}
	
	require_once "{$PHprefs['distDir']}/core/include/htmldiff/html_diff.php";
	
	$pageTitle = $phoundry->getPageHeader('view');

	PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="<?= preg_replace('/_\d+$/', '', $phoundry->name) ?>.view" />
<title><?= $pageTitle['title'] ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?=$PHprefs['url']?>/core/csjs/daisydiff/css/diff.css" type="text/css" />
<script type="text/javascript" src="<?=$PHprefs['url']?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">
//<![CDATA[
var TID = <?= $TID ?>, RID = '<?= escSquote($_GET['RID']) ?>';

jQuery(function($){
	var updateCheckboxes = function() {
		$('[name="version[a]"], [name="version[b]"]').css('visibility', 'visible')
		
		$('.version-row:has(~.version-row [name="version[b]"]:checked) [name="version[a]"]').
		add('.version-row:has([name="version[a]"]:checked) ~ .version-row [name="version[b]"]').
		add('.version-row:has([name="version[b]"]:checked) [name="version[a]"]').
		add('.version-row:has([name="version[a]"]:checked) [name="version[b]"]').
		css('visibility', 'hidden');
	};
	$('[name="version[a]"], [name="version[b]"]').live('click', updateCheckboxes);
	updateCheckboxes();

	var updateOverlays = function(){
		$('img[data-changetype]').each(function() {
			var changetype = $(this).attr('data-changetype');
			if (changetype == "diff-removed-image" || changetype == "diff-added-image") {
				var wrapper;
				if (!$(this).parent().is('.diff-removed-image, .diff-added-image')) {
					$(this).wrap('<span class="'+changetype+'"></span>');
					$(this).parent().css({
						width : $(this).width() + 'px',
						height : $(this).height() + 'px'
					}).prepend('<span>');
				}
			}
		});
	}

    $(window).bind("onresize", updateOverlays);
    updateOverlays();
});
//]]>
</script>
<style type="text/css">
fieldset.field > img {
	display: block;
	height: auto;
	max-width: 400px;
}

span.tip {
	display: none;
	position: absolute;
	border: 1px solid gray;
	background: #fff;
	width: 200px;
}

.changelist,
.changelist .changelist-item {
	display: block;
}

.diff-html-changed:hover span.tip {
	display: block;
	z-index: 10;
}

.diff-added-image,
.diff-removed-image {
	
}

.version-info-wrapper {
	float: left;
	width: 50%;
}
.version-info {
	padding: 20px;
	border: 1px solid #DBDBDB;
	background: #f3f3f3; /* Old browsers */
	background: -moz-linear-gradient(top,  #f3f3f3 0%, #ebebeb 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f3f3f3), color-stop(100%,#ebebeb)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  #f3f3f3 0%,#ebebeb 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  #f3f3f3 0%,#ebebeb 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  #f3f3f3 0%,#ebebeb 100%); /* IE10+ */
	background: linear-gradient(top,  #f3f3f3 0%,#ebebeb 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f3f3f3', endColorstr='#ebebeb',GradientType=0 ); /* IE6-8 */
}

.version-info-wrapper:first-child .version-info {
	margin: 0 14px 0 0;
}
.version-info-wrapper:last-child .version-info {
	margin: 0 0 0 14px;
}

.clear {
	clear: both;
}

</style>
</head>
<body class="frames">
<form action="?<?=QS(true, 'callback='.urlencode($_SERVER['REQUEST_URI']))?>" method="post">
<div id="headerFrame" class="headerFrame">
<?if (!$version_a && !$version_b):?>
<?=getHeader($pageTitle['title'], ucfirst(word(364)), $pageTitle['filters']); ?>
<?else:?>
<?=getHeader($pageTitle['title'], ucfirst(word(367)), $pageTitle['filters']); ?>
<?endif?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
</td>
<td align="right">

</td>
</tr></table>
</div>
<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?if (!$version_a && !$version_b):?>
<?if (count($versions) > 1):?>
<table class="sort-table" cellspacing="0">
<thead>
<tr>
	<th></th>
	<th></th>
	<th>Versie</th>
	<th>Datum</th>
	<th>Gebruiker</th>
	<?if (!empty($PHprefs['enableVersioningWorkflow'])):?>
	<th>Status</th>
	<?endif?>
	<th>Message</th>
</tr>
</thead>
<tbody>
<?/* @var $version Versioning_Version */ foreach ($versions as $i => $version):?>
<tr class="version-row">
	<td><input type="radio" name="version[a]" value="<?=$version->id?>"<?=$i === 1 ? ' checked="checked"' : ''?> /></td>
	<td><input type="radio" name="version[b]" value="<?=$version->id?>"<?=$i === 0 ? ' checked="checked"' : ''?> /></td>
	<td><?=(int)$version->revision?></td>
	<td><?=$version->date?></td>
	<td><?=$version->user_name?></td>
	<?if (!empty($PHprefs['enableVersioningWorkflow'])):?>
	<td><?=$version->getStatusString()?></td>
	<?endif?>
	<td><?if ($version->message):?>
	<span class="version-message" title="<?=htmlspecialchars($version->message)?>"><?=htmlspecialchars(PH::abbr($version->message, 100))?></span>
	<?endif?></td>
</tr>
<?endforeach?>
</tbody>
</table>
<?else:?>
<div><?=word(25)?></div>
<?endif?>
<?else:?>
<div>
	<? /* @var $version Versioning_Version */
	foreach (array($version_a, $version_b) as $version):?>
	<div class="version-info-wrapper">
		<div class="version-info">
			<div>
				<a href="<?=$PHprefs['url']?>/core/versioning/update_version.php?<?=QS(true, 'TID=&RID='.$version->id)?>" title="Wijzig">
					Version <?=(int) $version->revision?> by <?=htmlspecialchars(ucwords($version->user_name))?>
				</a>
			</div>
			<div>on <?=date('H-i, m F Y', strtotime($version->date))?></div>
			<div>Message: <?=htmlspecialchars($version->message)?></div>
			<?if (!empty($PHprefs['enableVersioningWorkflow'])):?>
			<div>State: <?=ucfirst($version->status)?></div>
			<?endif?>
		</div>
	</div>
	<?endforeach?>
	<div class="clear"></div>
</div>
<div>
<?php 
	$diffs = array();
	$skipDiffs = array();
	$fields = $extras = array();
	foreach($phoundry->cols as $col) {
		$col->getSQLvalue('update', $_POST, $fields, $extras, $version_a->data[$phoundry->key]);
		if (isset($col->datatype->extra['default']) &&
			 preg_match('/^{\$MOD_/', $col->datatype->extra['default'])) 
		{
			$skipDiffs[$col->name] = true;
		}
	}
	/* @var $version_a Versioning_Version */
	$phoundry->loadVersion($version_a);
	$row_a = $phoundry->_getRowValues($RID);
	$phoundry->loadVersion($version_b);
	$row_b = $phoundry->_getRowValues($RID);
	foreach($phoundry->cols as $col) {
		$val = $row_a[$col->name];
		$oldval = $row_b[$col->name];
		if (empty($skipDiffs[$col->name]) && $val != $oldval) {
			$diffs[$col->name] = array(
				'diff' => html_diff($val, $oldval, true),
				'old' => $val,
				'new' => $oldval
			);
		}
	}
	// $phoundry->loadVersion($version_b); // This version in still loaded
	$page_b = $phoundry->viewPage($RID, null, false, false, $diffs);
	echo $page_b['html'];
?>
</div>
<?endif?>
<!-- /Content -->
</div>
<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<?if (!$version_a && !$version_b):?>
	<input type="submit" value="<?=ucfirst(word(366 /* compare */))?>" />
	<?endif?>
	</td>
	<td align="right">
	<?php include '../cancelBut.php' ?>
	</td>
</tr></table>
</div>
</form>
</body>
</html>
