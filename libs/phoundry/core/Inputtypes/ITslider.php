<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITslider extends Inputtype
{
	public $width, $label, $stepsize, $range;

	public function __construct(&$column) {
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
			$this->label = '';
			
			if (!empty($this->extra)) {
				$this->width = $this->extra['width'];
				$this->stepsize = $this->extra['stepsize'];
				// Inherit range from datatype:
				$this->range = $this->column->datatype->range;
				if (!empty($this->extra['label'])) {
					$this->label = $this->extra['label'];
				}
			}
		}
	}

	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $PHprefs;

		$includes['jsFiles'][] = $PHprefs['url'] . '/core/csjs/slider.min.js';
		$includes['cssFiles'][] = $PHprefs['customUrls']['css'] . '/slider.css';

		// Get default value:
		$value = PH::htmlspecialchars(parent::getInputHtml($page, $value, $includes));
	
		$alt = $this->column->getJScheckAltTag();
		$html  = '<table width="100%"><tr><td style="width:' . $this->width . '"><div class="slider" id="slider' . $this->name . '" style="width:100%" tabindex="1">' .
					'<input name="' . $this->name . '" class="slider-input" id="slider-input' . $this->name . '" />' .
					'</div></td><td><span id="slider' . $this->name .'Dsp">' . $value . '</span> ' . $this->label . '</td></tr></table>';

		$band = $this->range[1] - $this->range[0];
		$blockSize = $band/(floor($band/$this->stepsize)/2);

		$html .=<<<EOH
		<script type="text/javascript">
		var Slider{$this->name} = new Slider($('#slider{$this->name}')[0],$('#slider-input{$this->name}')[0]); 
		Slider{$this->name}.setMinimum({$this->range[0]});
		Slider{$this->name}.setMaximum({$this->range[1]});
		Slider{$this->name}.setValue({$value});
		Slider{$this->name}.setUnitIncrement({$this->stepsize});
		Slider{$this->name}.setBlockIncrement({$blockSize});
		Slider{$this->name}.onchange = function() {
			$('#slider{$this->name}Dsp').text(Slider{$this->name}.getValue());
		}
		$(window).bind('resize', function() { Slider{$this->name}.recalculate(); });
		</script>
EOH;


		return $html;
	}

	public function getExtra() 
	{
		// Structure of $extra:
		// $extra = array(
		//    'size'=>20,
		//		'maxlength'=>50
		// );

		$width = '50%'; $label = ''; $stepsize = 1;
		if (!empty($this->extra)) {
			if (isset($this->extra['width'])) {
				$width = $this->extra['width'];
			}
			if (isset($this->extra['stepsize'])) {
				$stepsize = $this->extra['stepsize'];
			}
			if (isset($this->extra['label'])) {
				$label = $this->extra['label'];
			}
		}
		$adminInfo = array();
		$adminInfo['width'] = array(
			'name'     => 'ITwidth',
			'label'    => 'Width',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Enter the width of the slider, as a CSS expression (50%, 100px)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$width)
		);
		$adminInfo['stepsize'] = array(
			'name'     => 'ITstepsize',
			'label'    => 'Step size',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Enter the step size (tick size)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$stepsize)
		);
		$adminInfo['label'] = array(
			'name'     => 'ITlabel',
			'label'    => 'Label',
			'required' => false,
			'syntax'   => 'string',
			'info'     => 'Enter a label, which will be displayed as a \'legend\' behind the slider',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$label)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'width' => trim($_POST['ITwidth']),
			'stepsize' => trim($_POST['ITstepsize']),
			'label' => trim($_POST['ITlabel'])
		);

		return $extra;
	}
}
