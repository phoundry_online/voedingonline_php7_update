<?php

final class ITcontentBuilder extends Inputtype
{
    public function getInputHtml($page, $value = '', &$includes)
    {
        global $PHprefs;
        $value = parent::getInputHtml($page, $value, $includes);

        $includes['jsFiles']['srcdoc-polyfill'] = $PHprefs['url'].'/core/csjs/srcdoc-polyfill.min.js';
        $includes['jsFiles']['ITcontentBuilder'] = $PHprefs['url'].'/core/Inputtypes/contentBuilder/ITcontentBuilder.js';

        $contentBuilderUrl = $PHprefs['url'].'/core/Inputtypes/contentBuilder/?'.QS(false);
        $imageDir  = empty($this->extra['imageDir']) ? '' : PH::WPencode(PH::evaluate($this->extra['imageDir']));
        $attachDir = empty($this->extra['attachDir']) ? '' : PH::WPencode(PH::evaluate($this->extra['attachDir']));

        $jstreeImage = array('folder' => $imageDir, 'uploadExtensions' => 'png,gif,jpg,jpeg', 'selectExtensions' => 'png,gif,jpg,jpeg');
        $jstreeLink = array('folder' => $attachDir, 'uploadExtensions' => '', 'selectExtensions' => '');

        $stylesheets = $this->getExtraVal('stylesheets');
        $stylesheets = array_filter(array_map('trim', explode("\n", $stylesheets)));

        $colors = $this->getExtraVal('colors');
        $colors = array_filter(array_map('trim', explode("\n", $colors)));

        $options = array(
            'contentBuilderUrl' => $contentBuilderUrl,
            'stylesheets' => $stylesheets,
            'style' => $this->getExtraVal('style'),
            'snippetFile' => $this->getExtraVal('snippetFile'),
            'fileselect' => $PHprefs['url'].'/core/popups/media_browser/?'.QS(false, 'type=link&jstree='.json_encode($jstreeLink).'&callback=__CALLBACK__'),
            'imageselect' => $PHprefs['url'].'/core/popups/media_browser/?'.QS(false, 'type=image&jstree='.json_encode($jstreeImage).'&callback=__CALLBACK__'),
            'colors' => $colors,
            'containerClass' => $this->getExtraVal('containerClass'),
        );

        $e = function($value) {
            return htmlspecialchars($value, ENT_COMPAT, 'UTF-8');
        };

        // Encode <, >, ', &, and " for RFC4627-compliant JSON, which may also be embedded into HTML.
        $encodingOptions = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT;
        ob_start();
        ?>

<div data-contentbuilder="<?=$e(json_encode($options, $encodingOptions))?>">
    <input type="hidden" name="<?=$e($this->name)?>" value="<?=$e($value)?>" />
    <iframe
        width="100%" height="400" marginheight="0" marginwidth="0"
        style="border: 1px solid #bccf3f;"></iframe>
</div>

        <?php
        return ob_get_clean();
    }

    /**
     * @param string $page
     * @return array
     */
    public function getHelperLinks($page)
    {
        $helperLinks = parent::getHelperLinks($page);

        $helperLinks[] = '<a data-contentbuilder-open href="#">'.word(7 /* Edit */).'</a>';

        return $helperLinks;
    }

    /**
     * @return array
     */
    public function getExtra()
    {
        global $PHprefs;

        $adminInfo = array();

        $adminInfo['snippetFile'] = array(
            'name'     => 'ITsnippetFile',
            'label'    => 'Snippet file',
            'required' => true,
            'syntax'   => 'string',
            'info'     => 'Enter the relative url to the snippetFile to load for the ContentBuilder.',
            'help'     => false,
            'input'    => array('type'=>'text', 'size'=>60, 'maxlength'=>255, 'value'=>$this->getExtraVal('snippetFile', '/phoundry/core/Inputtypes/contentBuilder/assets/minimalist-basic/snippets.html'))
        );

        $adminInfo['stylesheets'] = array(
            'name'     => 'ITstylesheets',
            'label'    => 'Stylesheets',
            'required' => false,
            'syntax'   => 'string',
            'info'     => 'Stylesheets to load. One per line.',
            'help'     => false,
            'input'    => array('type'=>'textarea', 'cols'=>60, 'rows'=>10, 'value'=> $this->getExtraVal('stylesheets'))
        );

        $adminInfo['style'] = array(
            'name'     => 'ITstyle',
            'label'    => 'Style',
            'required' => false,
            'syntax'   => 'string',
            'info'     => 'Extra styles to load.',
            'help'     => false,
            'input'    => array('type'=>'textarea', 'cols'=>110, 'rows'=>15, 'value'=> $this->getExtraVal('style'))
        );

        $adminInfo['imageDir'] = array(
            'name'     => 'ITimageDir',
            'label'    => 'Image directory',
            'required' => false,
            'syntax'   => 'string',
            'info'     => 'Enter the image directory, relative to ' . $PHprefs['uploadDir'] . '/',
            'help'     => true,
            'input'    => array('type'=>'text', 'size'=>60, 'maxlength'=>255, 'value'=>$this->getExtraVal('imageDir'))
        );
        $adminInfo['attachDir'] = array(
            'name'     => 'ITattachDir',
            'label'    => 'Attachment directory',
            'required' => false,
            'syntax'   => 'string',
            'info'     => 'Enter the attachment directory, relative to ' . $PHprefs['uploadDir'] . '/',
            'help'     => true,
            'input'    => array('type'=>'text', 'size'=>60, 'maxlength'=>255, 'value'=>$this->getExtraVal('attachDir'))
        );

        $adminInfo['colors'] = array(
            'name'     => 'ITcolors',
            'label'    => 'Colors',
            'required' => false,
            'syntax'   => 'string',
            'info'     => 'Colors for the colorpicker. One per line.',
            'help'     => false,
            'input'    => array('type'=>'textarea', 'cols'=>15, 'rows'=>10, 'value'=> $this->getExtraVal('colors'))
        );

        $adminInfo['containerClass'] = array(
            'name'     => 'ITcontainerClass',
            'label'    => 'Container class',
            'required' => false,
            'syntax'   => 'string',
            'info'     => 'A class or multiple classes seperated by spaces for the container div. Can be used to fake the context the content will have on the site',
            'help'     => false,
            'input'    => array('type'=>'text', 'size'=>60, 'maxlength'=>512, 'value'=> $this->getExtraVal('containerClass', 'contentBuilder'))
        );

        return $adminInfo;
    }

    public function setExtra(&$msg)
    {
        $msg = '';

        $extra = array(
            'snippetFile' => trim($_POST['ITsnippetFile']),
            'stylesheets' => trim($_POST['ITstylesheets']),
            'style'    => trim($_POST['ITstyle']),
            'imageDir'  => trim($_POST['ITimageDir'], '/') . '/',
            'attachDir' => trim($_POST['ITattachDir'], '/') . '/',
            'colors' => trim($_POST['ITcolors']),
            'containerClass' => trim($_POST['ITcontainerClass']),
        );

        return $extra;
    }

    private function getExtraVal($key, $default = '')
    {
        return is_array($this->extra) && array_key_exists($key, $this->extra) ?
            $this->extra[$key] : $default;
    }

    public function getExtraHelp($what)
    {
        global $PHprefs;

        switch($what) {
            case 'ITimageDir':
                $help =<<<EOH
<h6>Image directory</h6>
If you want to allow for insertion of images in the editor, use this property. Set it to the path to the
directory the &quot;Image Properties&quot; dialog should start browsing in. This directory is relative to the <b>\$PHprefs['uploadDir']</b> directory.<br />
You can use the variables {\$USER_ID} and {\$GROUP_ID} for personal directories.
EOH;
                break;
            case 'ITattachDir':
                $help =<<<EOH
<h6>Attachment directory</h6>
If you want to allow for linking to attachments in the editor, use this property. Set it to the path to the
directory the &quot;Attachment Properties&quot; dialog should start browsing in. This directory is relative to the <b>\$PHprefs['uploadDir']</b> directory.<br />
You can use the variables {\$USER_ID} and {\$GROUP_ID} for personal directories.
EOH;
                break;
            default:
                $help = 'No help found for ' . $what . '!';
                break;
        }

        return $help;
    }
}
