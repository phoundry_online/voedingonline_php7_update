<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITfile extends Inputtype
{
	public $size, $maxlength = -1;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
			if (!empty($this->extra['maxlength']))
				$this->maxlength = $this->extra['maxlength'];
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		$alt = $this->column->getJScheckAltTag();
		if ($value === '' && $page == 'insert') {
			$value = $this->column->datatype->getDefaultValue();
		}
//		$value = PH::htmlspecialchars(parent::getInputHtml($page, $value, $includes));
		$html = '<input type="hidden" name="' . $this->name . '" alt="' . $alt . '" value="' . $value . '" />';
		$html .= '<input name="dsp_' . $this->name . '" type="text" disabled="disabled" size="' . $this->extra['size'] . '" value="' . $value . '"';
		if ($this->maxlength != -1)
			$html .= ' maxlength="' . $this->maxlength . '"';
		$html .= ' />';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$helpers = array();
		$url = $PHprefs['url'] . '/core/popups/browse.php?' . QS(1,"CID={$this->column->id}");
		$helpers[] = '<a href="#" tabindex="1" onclick="winArgs=[];makePopup(event,600,415,\'' . escSquote(ucfirst(word(160))) . '\',\'' . $url . '&amp;file=\'+escape(document.forms[0][\'' . $this->name . '\'].value));return false">browse</a>';

		$url = $PHprefs['url'] . '/core/popups/viewFile.php?' . QS(1,"CID={$this->column->id}");
		$helpers[] = '<a href="#" tabindex="1" onclick="var val=document.forms[0][\'' . $this->name . '\'].value,ext=val.substring(val.lastIndexOf(\'.\')+1,val.length).toLowerCase(),isImg=\'gif|jpg|jpeg|png\'.indexOf(ext)!=-1;if(val!=\'\')makePopup(event,isImg?400:$(window).width()-50,isImg?300:$(window).height()-50,\'' . escSquote(ucfirst(word(49))) . '\',\'' . $url . '&amp;file=\'+escape(val));return false">' . word(49) . '</a>';

		if (!$this->column->required)
			$helpers[] = '<a href="#" tabindex="1" onclick="document.forms[0][\'' . $this->name . '\'].value=document.forms[0][\'dsp_' . $this->name . '\'].value=\'\';return false">' . word(161) . '</a>';
		return $helpers;
	}

	public function getExtra() 
	{
		$maxlength = isset($this->column->DBprops['length']) ? $this->column->DBprops['length'] : '';
		$size = ($maxlength == '') ? 60 : min(60, $maxlength);
		$value = isset($this->column->DBprops['default']) ? $this->column->DBprops['default'] : '';
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['maxlength']))
				$maxlength = $this->extra['maxlength'];
			if (isset($this->extra['value']))
				$value = $this->extra['value'];
		}
		$adminInfo = array();
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['maxlength'] = array(
			'name'     => 'ITmaxlength',
			'label'    => 'Max. length',
			'required' => false,
			'syntax'   => 'integer',
			'info'     => 'Enter the maximum input-length of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$maxlength)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'size'      => (int)$_POST['ITsize'],
			'maxlength' => trim($_POST['ITmaxlength'])
		);

		return $extra;
	}
}
