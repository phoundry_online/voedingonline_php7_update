<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITmulticheckbox extends Inputtype
{
	public $query, $help, $queryFields;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getValue($page, $value, $keyval = null)
	{
		global $db, $PHprefs;

		$values = explode(',', $value);

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				$html = '';
				$sql = PH::evaluate($this->extra['query']);
				// Pipe-separated option=value pairs:
				$pairs = explode('|', $sql);
				foreach($pairs as $pair) {
					$pair = explode('=', $pair, 2);
					if (in_array($pair[0], $values)) {
						if ($html != '') $html .= ', ';
						$html .= PH::langWord($pair[1]);
					}
				}
				if ($page == 'search' && strlen($value) > $PHprefs['recordStrlen']) {
					$html .= '...';
				}
				return $html;
			default:
				return $value;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;
		$alt = $this->column->getJScheckAltTag();
		$selected = explode(',', $value);
		$sql = PH::evaluate($this->extra['query']);

		$html = '';
		// Pipe-separated option=value pairs:
		$pairs = explode('|', $sql);
		$x = 0;
		foreach($pairs as $pair) {
			$label = $this->name . '_' . $x++;
			$pair = explode('=', $pair, 2);
			$seld = in_array($pair[0], $selected) ? ' checked="checked" ' : ' ';
			$html .= '<input id="' . $label . '"' . $seld . 'type="checkbox" name="' . $this->name . '[]" alt="' . $alt . '" value="' . PH::htmlspecialchars($pair[0]) . '" /> <label for="' . $label . '">' . $pair[1] . '</label><br />';
		}
		return $html;
	}

	public function getHelperLinks($page) 
	{
		return array();
	}

	public function getExtra() 
	{
		global $db;
		
		$query = null;
		if (count($this->extra)) {
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
		}
		$adminInfo = array();
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the the options (pipe separated name=value pairs) in the ' . substr(get_class($this),2),
			'help'     => false,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'query'     => trim($_POST['ITquery'])
		);

		return $extra;
	}
}
