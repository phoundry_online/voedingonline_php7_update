<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";
require_once "{$PHprefs['distDir']}/core/Inputtypes/filter/FilterConverter.php";

class ITfilter extends Inputtype
{
	public $size, $maxlength = -1;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
			if (!empty($this->extra['maxlength']))
				$this->maxlength = $this->extra['maxlength'];
		}
	}

	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $PHprefs, $PHenv, $db, $DMDprefs;
		$TID = getTID();
		$useUTF8 = ($PHprefs['charset']=='utf-8');
		$targetTable = $this->extra['targetTable'];

		// For DMdelivery:
		if (isset($PHprefs['product']) && $PHprefs['product'] == 'DMdelivery' && isset($PHenv['DMDcid'])) {
			$targetTable = ($targetTable == 'recipient_{$CAMPAIGN_ID}') ? $PHenv['rcptTable'] : str_replace('{$CAMPAIGN_ID}', $PHenv['DMDcid'], $targetTable);
		}

		$json = ($value) ? $value : false;

		$alt = $this->column->getJScheckAltTag();

		$includes['jsFiles'][] = $PHprefs['url'] . '/core/Inputtypes/filter/script/jquery.json.pack.js';
		$includes['jsFiles'][] = $PHprefs['url'] . '/core/Inputtypes/filter/script/FilterItemGroup.js';
		$includes['jsFiles'][] = $PHprefs['url'] . '/core/Inputtypes/filter/script/FilterPopup.js';
		$includes['jsFiles'][] = $PHprefs['url'] . '/core/Inputtypes/filter/script/FilterEditPopup.js';
		$includes['jsFiles'][] = $PHprefs['url'] . '/core/Inputtypes/filter/script/FilterUndo.js';
		$includes['jsFiles'][] = $PHprefs['url'] . '/core/Inputtypes/filter/script/FilterClipboard.js';
		$includes['jsFiles'][] = $PHprefs['url'] . '/core/Inputtypes/filter/script/logic.js';

		$includes['cssFiles'][] = $PHprefs['url'] . '/core/Inputtypes/filter/script/filter.css';

		ob_start();
?>
		<script type="text/javascript">
		var targetTable = '<?=$targetTable?>',
		    useUTF8 = <?=$useUTF8?'true':'false'?>;

<?php
			$timeTrigger = isset($DMDprefs) && $DMDprefs['enableTriggers'];
			require('filter/script/FilterConstants.js.php');
			require('filter/script/TableColumns.js.php');
			require('filter/script/RawFilterObj.js.php');
?>
			var filterTestPending = false;

			$( function() {

				// add test button functionality..
				$('#aTestFilter').click( function() {
					if (filterTestPending) return;
					var json = getFilterJSON();

					this.oldVal = this.innerHTML;
					this.innerHTML = 'Wait...';
					filterTestPending = true;

					var params = {
							url: 'Inputtypes/filter/ajax.php?TID=<?=$TID?>&targetTable=<?= urlencode($targetTable) ?>&op=test',
							type: 'post',
							data: {'json': json, 'useUTF8':useUTF8},
							dataType: 'json',
							timeout: 25000,
							error: function(a,descr) {
								alert('An error occured.');
							},
							success: function(o) {
								if (o.error) {
									alert('AJAX reply: '+ o.error);
									alert(o.where);
								} else
									alert('Matching recipients: ' + o.hits);
									// alert(o.where);
							},
							complete: function() {
								var but = $('#aTestFilter')[0];
								filterTestPending = false;
								but.innerHTML = but.oldVal;
							}
					};
					$.ajax(params);
				});
			});
		</script>
		<div id="filterToolbar">
			<!-- <a href="#" onclick="location.reload()">reload</a><br/> -->
			<!-- BEGIN: clipboard buttons -->
				<input type="button" value="copy" id="copyBut" disabled="disabled" />
				<input type="button" value="cut" id="cutBut" disabled="disabled" />
				<input type="button" value="delete" id="delBut" disabled="disabled" />
			<!-- END: clipboard buttons -->
			|
			<!-- BEGIN: undo/redo buttons -->
				<input type="button" value="undo" id="undoBut" disabled="disabled" />
				<input type="button" value="redo" id="redoBut" disabled="disabled" />
			<!-- END: undo/redo buttons -->
		</div>

		<div id="rfilter"></div>
<?php
		$html = ob_get_clean();

		$html .= '<input type="hidden" name="' . $this->name . '" value="' . PH::htmlspecialchars($value) . '" alt="' . $this->column->getJScheckAltTag() . '" />';

		return $html;
	}

	public function getHelperLinks($page) 
	{
		return array('<a id="aTestFilter" href="#" tabindex="1">' . 'Test filter' . '</a>');
   }

	public function getExtra() 
	{
		$targetTable = '';
		if (count($this->extra)) {
			if (isset($this->extra['targetTable']))
				$targetTable = $this->extra['targetTable'];
		}
		$adminInfo = array();

		$adminInfo['targetTable'] = array(
			'name'     => 'ITtargetTable',
			'label'    => 'Target table',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Enter the name of the table this filter should operate on',
			'input'    => array('type'=>'text', 'size'=>60, 'maxlength'=>80, 'value'=>$targetTable)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'targetTable' => trim($_POST['ITtargetTable'])
		);

		return $extra;
	}
}
