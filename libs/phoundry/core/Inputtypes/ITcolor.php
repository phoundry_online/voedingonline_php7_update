<?php
global $PHprefs;
	
if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/Inputtypes/ITtext.php";

class ITcolor extends ITtext
{
	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		if ($value === '')
			$value = $this->column->datatype->getDefaultValue();
		$alt = $this->column->getJScheckAltTag();
		$html = '<input name="' . $this->name . '" type="text" alt="' . $alt . '" size="' . $this->extra['size'] . '" value="' . PH::htmlspecialchars($value) . '"';
		if ($this->maxlength != -1)
			$html .= ' maxlength="' . $this->maxlength . '"';
		$html .= ' onblur="try{gE(\'' . $this->name . 'Clr\').style.background=this.value}catch(e){}" />';
		$html .= '<img align="middle" id="' . $this->name . 'Clr" src="pics/pixel.gif" width="15" height="15" style="background:' . $value . ';border:1px solid #000" />';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		if (!isset($this->extra['updatable']) || $this->extra['updatable'] == true || $page != 'update') {
			$url = $PHprefs['url'] . '/core/popups/colorpick.php?' . QS(1,"field={$this->name}");
			return array('<a href="#" tabindex="1" onclick="makePopup(event,550,360,\'' . ucfirst(escSquote(word(29))) . '\',\'' . $url . '&amp;color=\'+escape(_D.forms[0][\'' . $this->name . '\'].value));return false">' . word(29) . '</a>');
		}
		else
			return array();
	}
}
