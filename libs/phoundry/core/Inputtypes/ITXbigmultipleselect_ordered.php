<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/Inputtypes/ITXmultipleselect.php";

class ITXbigmultipleselect_ordered extends ITXmultipleselect
{
	public $query, $help, $queryFields;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getValue($page, $keyValue, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				$selected = $this->column->datatype->getSelected($keyValue);
				$options = array();
				$sql = PH::evaluate($this->extra['query']);
				$cur = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				for ($x = 0; !$db->EndOfResult($cur); $x++) {
					if (($idx = array_search($db->FetchResult($cur,$x,$this->extra['optionvalue']), $selected)) !== false) {
						$option = '';
						foreach($this->extra['searchtext'] as $text)
							$option .= PH::langWord($db->FetchResult($cur,$x,$text)) . ' ';
						$options[$idx] = rtrim($option);
					}
				}
				ksort($options);
				return implode(', ', $options);

			default:
				return $keyValue;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db, $PHprefs;
		$alt = $this->column->getJScheckAltTag();
		$includes['jsFiles'][strtolower(get_class($this))] = $PHprefs['url'] . '/core/csjs/itxref.js';
		$selected = $this->column->datatype->getSelected($value);
		$options = array();
		$sql = PH::evaluate($this->extra['query']);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			if (($idx = array_search($db->FetchResult($cur,$x,$this->extra['optionvalue']), $selected)) !== false) {
				$option = '';
				foreach($this->extra['optiontext'] as $text) {
					$option .= PH::langWord($db->FetchResult($cur,$x,$text)) . ' ';
				}
				$options[$idx] = array('value'=>$db->FetchResult($cur,$x,$this->extra['optionvalue']), 'text'=>rtrim($option));
			}
		}
		ksort($options);
		$html = '<table class="ITXbigmultipleselect" cellspacing="0" cellpadding="0"><tr>';
		$html .= '<td valign="top">
			<span class="icon ptr moveUp" onclick="MSmove(_D.forms[0][\'dsp_' . $this->name . '\'],1)"></span><br>
			<span class="icon ptr moveDown" onclick="MSmove(_D.forms[0][\'dsp_' . $this->name . '\'],0)"></span><br><br>
		</td>';
		$html .= '<td rowspan="2"><select size="' . $this->extra['size'] . '" name="dsp_' . $this->name . '">';
		foreach($options as $option) {
			$html .= '<option value="' . PH::htmlspecialchars($option['value']) . '">' . PH::htmlspecialchars(PH::langWord($option['text'])) . '</option>';
		}
		$html .= '</select></td>';
		$html .= '</tr><tr>';
		$html .= '<td valign="bottom"><span class="icon ptr delete" onclick="MSdelete(_D.forms[0][\'dsp_' . $this->name . '\'])"></span></td>';
		$html .= '</tr></table>';
		$html .= '<input type="hidden" name="' . $this->name . '" alt="' . $alt . '" value="' . PH::htmlspecialchars(implode('|', $selected)) . '" />';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $db, $PHSes, $PHprefs;

		$url = $PHprefs['url'] . '/core/popups/bigITsearch.php?' . QS(1,"CID={$this->column->id}");
		$helpers = array('<a href="' . $url . '" tabindex="1" class="popupWin" title="' . ucfirst(word(10)) . '|400|240|0" onclick="callbackEl=_D.forms[0][\'dsp_' . $this->name . '\']">' . strtolower(word(10)) . '</a>');
		$mayAdd = false;

		if (empty($this->extra['addTable']))
			$mayAdd = false;
		elseif ($PHSes->groupId == -1)
			$mayAdd = true;
		else {
			// Make sure current user in current role may insert into $extra['addTable'];
			$sql = "SELECT rights FROM phoundry_group_table WHERE table_id = " . (int)$this->extra['addTable'] . " AND group_id = {$PHSes->groupId}";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur) && strpos($db->FetchResult($cur,0,'rights'), 'i') !== false)
				$mayAdd = true;
		}

		if ($mayAdd) {
			$url = $PHprefs['url'] . '/core/insert.php?' . QS(1,"TID={$this->extra['addTable']}&CID={$this->column->id}") . '&amp;noFrames&amp;callback=parent.newOption';
			$helpers[] = '<a href="' . $url . '" class="popupWin" title="' . ucfirst(word(6)) . '|600|400|0" onclick="callbackEl=_D.forms[0][\'dsp_' . $this->name . '\']">' . strtolower(word(6)) . '</a>';
		}
		return $helpers;
	}
}
