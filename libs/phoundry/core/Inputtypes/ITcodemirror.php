<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITcodemirror extends Inputtype
{
	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes)
	{
		global $PHprefs;
		$_id = strtolower(get_class($this));
		$includes['jsFiles'][$_id.'1'] = "{$PHprefs['url']}/core/csjs/codemirror/lib/codemirror.js";
		$includes['jsFiles'][$_id.'2'] = "{$PHprefs['url']}/core/csjs/codemirror/mode/xml/xml.js";
		$includes['jsFiles'][$_id.'3'] = "{$PHprefs['url']}/core/csjs/codemirror/mode/javascript/javascript.js";
		$includes['jsFiles'][$_id.'4'] = "{$PHprefs['url']}/core/csjs/codemirror/mode/css/css.js";
		$includes['jsFiles'][$_id.'5'] = "{$PHprefs['url']}/core/csjs/codemirror/mode/clike/clike.js";
		$includes['jsFiles'][$_id.'6'] = "{$PHprefs['url']}/core/csjs/codemirror/mode/php/php.js";
		$includes['cssFiles'][$_id.'1'] = "{$PHprefs['url']}/core/csjs/codemirror/lib/codemirror.css";

		// Get default value:
		//$value = parent::getInputHtml($page, evaluate($value), $includes);
		if ($value === '' && $page == 'insert') {
			$value = $this->column->datatype->getDefaultValue();
		}

		if (isset($this->extra['updatable']) && $this->extra['updatable'] == false && $page == 'update') {
			return '<input name="' . $this->name . '" type="hidden" value="' . PH::htmlspecialchars($value) . '" /><pre>' . PH::htmlspecialchars($value) . '</pre>';
		}

		$alt = $this->column->getJScheckAltTag();

		$html = '<textarea id="' . $this->name . '" name="' . $this->name . '" alt="' . $alt . '" style="width:' . $this->extra['width'] . ';height:' . $this->extra['height'] . '">';
		if ($value !== '') {
			$html .= PH::htmlspecialchars($value);
		}
		$html .= '</textarea>';

		$html .=<<<EOH
		<script type="text/javascript">
		var CM{$this->name} = CodeMirror.fromTextArea(document.getElementById('{$this->name}'), {
			mode: 'application/x-httpd-php',
			lineNumbers: true,
			lineWrapping: true,
			indentUnit: 1,
			tabSize: 1,
			extraKeys: {'Tab':'insertTab'},
			matchBrackets: true
		});
		</script>
EOH;

		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$helpers = array();

		if ($this->extra['preview']) {
			$url = $PHprefs['url'] . '/core/popups/preview.php?' . QS(2,"field={$this->name}&type=codemirror");
	      $helpers[] = '<a href="' . $url . '" tabindex="1" class="popupWin" title="' . ucfirst(word(49)) . '|300|200|0">' . word(49) . '</a>';
		}

		return $helpers;
   }
	
	public function getExtra() 
	{
		$width     = '100%';
		$height    = '300px';
		$preview   = false;
		$updatable = true;
		if (count($this->extra)) {
			if (isset($this->extra['height']))
				$height = $this->extra['height'];
			if (isset($this->extra['width']))
				$width = $this->extra['width'];
			if (isset($this->extra['preview']))
				$preview = $this->extra['preview'] ? 1 : 0;
			if (isset($this->extra['updatable']))
				$updatable = $this->extra['updatable'];
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['width'] = array(
			'name'     => 'ITwidth',
			'label'    => 'Width',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the width of the editor (CSS expression)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$width)
		);
		$adminInfo['height'] = array(
			'name'     => 'ITheight',
			'label'    => 'Height',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the height of the editor (CSS expression)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$height)
		);
		$adminInfo['preview'] = array(
			'name'     => 'ITpreview',
			'label'    => 'HTML preview?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Does this textarea need a preview-link?',
			'input'    => array('type'=>'select', 'options'=>array(0=>'no',1=>'yes'), 'selected'=>$preview)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false,
			'width'     => trim($_POST['ITwidth']),
			'height'    => trim($_POST['ITheight']),
			'preview'   => ($_POST['ITpreview'] == 1) ? true : false
		);

		return $extra;
	}
}
