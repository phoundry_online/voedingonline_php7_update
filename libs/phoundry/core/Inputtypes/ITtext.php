<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITtext extends Inputtype
{
	public $size, $maxlength = -1;

	public function __construct(&$column) {
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
			if (!empty($this->extra['maxlength'])) {
				$this->maxlength = $this->extra['maxlength'];
			}
		}
	}

	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		// Get default value:
		$value = PH::htmlspecialchars(parent::getInputHtml($page, $value, $includes));

		if (isset($this->extra['updatable']) && $this->extra['updatable'] == false && $page == 'update') 
		{
			return '<input id="' . $this->name . '" name="' . $this->name . '" type="hidden" value="' . $value . '" /><b>' . $value . '</b>';
		}

		$alt = $this->column->getJScheckAltTag();
		$html = '<input id="' . $this->name . '" name="' . $this->name . '" type="text" alt="' . $alt . '" size="' . $this->extra['size'] . '" value="' . $value . '"';
		if ($this->maxlength != -1) 
		{
			$html .= ' maxlength="' . $this->maxlength . '"';
		}
		if (($pattern = $this->column->datatype->getPattern()) !== '') 
		{
			$html .= ' data-pattern="' . PH::htmlspecialchars($pattern) . '"';
		}

		if (isset($this->extra['counter']) && $this->extra['counter']) 
		{
			$html .= ' onkeyup="showCharCount(this)"';
		}

		$html .= ' />';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$helpers = array();
		if (isset($this->extra['counter']) && $this->extra['counter']) {
			$helpers[] = '<span id="' . $this->name . '_cOuNtEr"></span>';
		}

		return $helpers;
   }

	public function getExtra() 
	{
		// Structure of $extra:
		// $extra = array(
		//    'size'=>20,
		//		'maxlength'=>50
		// );

		$maxlength = isset($this->column->DBprops['length']) ? $this->column->DBprops['length'] : '';
		$size = ($maxlength == '') ? 60 : min(60, $maxlength);
		$updatable = true;
		$counter   = false;
		if (count($this->extra)) {
			if (isset($this->extra['size'])) {
				$size = $this->extra['size'];
			}
			if (isset($this->extra['maxlength'])) {
				$maxlength = $this->extra['maxlength'];
			}
			if (isset($this->extra['updatable'])) {
				$updatable = $this->extra['updatable']; 
			}
			if (isset($this->extra['counter'])) {
				$counter = $this->extra['counter'] ? 1 : 0;
			}
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['maxlength'] = array(
			'name'     => 'ITmaxlength',
			'label'    => 'Max. length',
			'required' => false,
			'syntax'   => 'integer',
			'info'     => 'Enter the maximum input-length of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$maxlength)
		);
		$adminInfo['counter'] = array(
			'name'     => 'ITcounter',
			'label'    => 'Character counter?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Do you want to show a character counter?',
			'input'    => array('type'=>'select', 'options'=>array(0=>'no',1=>'yes'), 'selected'=>$counter)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false,
			'size'      => (int)$_POST['ITsize'],
			'maxlength' => trim($_POST['ITmaxlength'])
		);

		if (isset($_POST['ITcounter'])) {
			$extra['counter'] = ($_POST['ITcounter'] == 1) ? true : false;
		}

		return $extra;
	}
}
