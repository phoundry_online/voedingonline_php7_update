<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";
/**
 * Markitup input type
 *
 * @Author Ilan Rivers
 */
class ITmarkitup extends Inputtype
{	
	/**
	 * Render html to display for this input type.
	 *
	 *
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $PHprefs;

		if ($value === '' && $page == 'insert') {
			$value = $this->column->datatype->getDefaultValue();
		}

		if (isset($this->extra['updatable']) && $this->extra['updatable'] == false && $page == 'update') {
			return '<input name="' . $this->name . '" type="hidden" value="' . PH::htmlspecialchars($value) . '" />' . $value;
		}
		
		$className = strtolower(get_class($this));
		$includes['jsFiles'][$className] = $PHprefs['url'] . '/core/Inputtypes/markitup/jquery.markitup.js';
		$includes['cssFiles'][$className] = $PHprefs['url'] . '/core/Inputtypes/markitup/skins/markitup/style.css';
		$includes['cssFiles'][$className . $this->extra['markItUpSet']] = $PHprefs['url'] . '/core/Inputtypes/markitup/sets/' . $this->extra['markItUpSet'] . '/style.css';		
		
        //Include scripts/styles	
        $html  = '';
		$html .= '<script type="text/javascript" src="' 
		      . $PHprefs['url'] 
			  . '/core/Inputtypes/markitup/sets/' 
			  . $this->extra['markItUpSet'] 
			  . '/set.js"></script>';
		$html .= '<script type="text/javascript">var markitupSettings' . $this->name . ' = mySettings;</script>';
		$options = "";
		
		//Initialise the text area
		$html .= '<script type="text/javascript">';
		$html .= '$(document).ready(function(){$(\'#' 
		      . $this->name 
			  . '\').markItUp(markitupSettings' 
              . $this->name 
			  . ','
			  . $this->extra['config']
			  .');});';
		$html .= '</script>';
		
		//Build textarea
		$html .= '<textarea id="' 
		      . $this->name 
			  . '" name="' 
			  . $this->name 
			  . '" cols="60" rows="10" style="width:' 
			  . $this->extra['width']  
			  . ';height:' 
			  . $this->extra['height'] 
			  . '">'
		      . PH::htmlspecialchars($value)
		      . '</textarea>';
		
		return $html;
	}
	
	/**
	 * Get extra options
	 *
	 *
	 */
	public function getExtra() 
	{
		$width       = '90%';
		$height      = '250px';
		$updatable   = true;
		$config      = '{}';
		$markItUpSet = 'default';
		
		if (! empty($this->extra)) {
			if (isset($this->extra['width'])) {
				$width = $this->extra['width'];
			}
			if (isset($this->extra['height'])) {
				$height = $this->extra['height'];
			}
			if (isset($this->extra['updatable'])) {
				$updatable = $this->extra['updatable'];
			}
			if (isset($this->extra['config'])) {
				$config = $this->extra['config'];
			}
            if (isset($this->extra['markItUpSet'])) {
				$markItUpSet = $this->extra['markItUpSet'];
			}		
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['cols'] = array(
			'name'     => 'ITwidth',
			'label'    => 'Width',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Enter the width of the Markitup editor (CSS expression: 400px or 80%)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$width)
		);
		$adminInfo['rows'] = array(
			'name'     => 'ITheight',
			'label'    => 'Height',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Enter the height of the Markitup editor (CSS expression: 200px or 20%)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$height)
		);
		
		$adminInfo['markItUpSet'] = array(
			'name'     => 'ITmarkItUpSets',
			'label'    => 'Markitup sets',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Choose an available marktitup set',
			'input'    => array('type'=>'select', 'options' => array(
			                  'default'   => 'Basic',
							  'html'      => 'HTML',
							  'wiki'      => 'Wiki',
							  'css'       => 'CSS',
							  'xbbcode'   => 'XBB Code',
							  'markdown'  => 'Markdown',
							  'dotclear'  => 'Dotclear'
						  ), 'selected' => $markItUpSet)
		);
		
		$adminInfo['config'] = array(
			'name'     => 'ITconfig',
			'label'    => 'Config',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Configure the Markitup editor.',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>110, 'rows'=>15, 'value'=>$config)
		);

		return $adminInfo;
	}
    
	/**
	 * Set extra options
	 *
	 *
	 */
	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'updatable'    => ($_POST['ITupdatable'] == 1),
			'width'        => trim($_POST['ITwidth']),
			'height'       => trim($_POST['ITheight']),
			'config'       => trim($_POST['ITconfig']),
			'markItUpSet'  => trim($_POST['ITmarkItUpSets'])
		);

		return $extra;
	}
}
