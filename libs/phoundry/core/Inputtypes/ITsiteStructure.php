<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once INCLUDE_DIR.'/functions/autoloader.include.php';
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITsiteStructure extends Inputtype
{
	private $items;
	
	public function getValue($page, $value, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				if (is_null($value)) {
					return '';
				}
				$id = (int) $value;
				$path = array();
				$structure = ActiveRecord::factory('Model_Site_Structure', $id);
				
				while ($structure->isLoaded()) {
					array_unshift($path, $structure['name']);
					$structure = $structure->parent;
				}
				
				return implode(" / ", $path);
			default:
				return $value;
		}
	}

	/**
	 * Creates the links in the legend part of the fieldset
	 */
	public function getHelperLinks($page) 
	{
		$helpers = array();
		
		if (($page == 'update' || $page == 'insert' || $page == 'copy') && !$this->column->required) {
			$helpers[] = '<a href="#" onclick="return false;">' . word(161) . '</a>';
		}
		return $helpers;
	}
	
	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		if(empty($_REQUEST['site_identifier'])) {
			PH::phoundryErrorPrinter("Site Structure needs a site", true);
		}
		
		global $PHprefs;
		$value = parent::getInputHtml($page, $value, $includes);

		$includes['jsFiles'][strtolower(get_class($this))] = $PHprefs['url'].
			'/core/csjs/jquery.treeview.pack.js';

		$alt = $this->column->getJScheckAltTag();
		$menu = array();
		
		$sitestructures = ActiveRecord::factory("Model_Site_Structure")->
			where('site_identifier', $_REQUEST['site_identifier'])->
			whereNull('parent_id')->get();
		
		$this->items = 0;
		foreach($sitestructures as $ss) {
			$menu[] = $this->getMenuItem($ss, $value);
			$this->items += 1;
		}
		
		$height = $this->items > 20 ? '200px' : 'auto';
		$menu = implode("\n", $menu);
		
		return <<<EOH
		<input type="hidden" name="{$this->name}" value="{$value}" alt="{$alt}" />
		<div style="height: {$height};overflow:auto">
			<ul id="{$this->name}_tree">
				{$menu}
			</ul></div>
		<script type="text/javascript">
		jQuery(function($){
			$('#{$this->name}_tree').treeview({collapsed:false,unique:true}).
			each(function(i, tree){
				$(tree).parents("fieldset.field:first").find("legend a").
				bind("click", function(e){
					$('input[name="{$this->name}"]').val("");
					$("#{$this->name}_tree .www, .page").removeClass("selected");
				});
				$(".www, .page", tree).live("click", function(){
					$(".www, .page", tree).removeClass("selected");
					$(this).addClass("selected");
					$('input[name="{$this->name}"]').val($(this).attr('title'));
				});
			});
		});
		</script>
EOH;
	}

	public function getMenuItem(Model_Site_Structure $structure, $value) 
	{
		if($structure->isLoaded()) {
			$class = $structure['id'] == $value ? 'selected' : '';
			
			$children = array();
			foreach($structure->children as $child) {
				$children[] = $this->getMenuItem($child, $value);
				$this->items += 1;
			}
			if($children) {
				$children = "<ul>".implode("\n", $children)."</ul>\n";
			}
			else {
				$children = '';
			}
			
			$icon = $GLOBALS['PHprefs']['url'].
				'/custom/brickwork/site_structure/pics/';
			#{$this->name}_tree
			$url = count($structure->urls) > count($structure->pages);
			$icon .= $url ? 'www' : 'page';
			
			if($structure['show_menu']) {
				$icon .= '_m';
			}
			if($structure['show_sitemap']) {
				$icon .= '_s';
			}
			if(!$url && $structure['secure']) {
				$icon .= '_x';
			}
			$icon .= '.gif';
			
			return '<li><a href="#" title="'.$structure['id'].'" class="'.
				($url?'www':'page').' '.$class
				.'" style="background-image: url('.$icon.
				'); background-position: 3px center; '.
				'background-repeat: no-repeat; padding-left: 17px;">'.
				htmlspecialchars($structure['name']).
				'</a>'.$children.'</li>';
		}
	}
	
//	public function getExtra() 
//	{
//		$adminInfo = array();
//		$adminInfo['wherepart'] = array(
//			'name'     => 'ITwherepart',
//			'label'    => 'Extra where part',
//			'required' => false,
//			'syntax'   => 'string',
//			'info'     => 'Extra stuff added to the where part',
//			'help'     => true,
//			'input'    => array(
//				'type'=>'textarea',
//				'cols'=>110,
//				'rows'=>15,
//				'value'=> isset($this->extra['wherepart']) ?
//					$this->extra['wherepart'] : ''
//			)
//		);
//		
//		return $adminInfo;
//	}
//	
//	public function setExtra(&$msg) 
//	{
//		$msg = '';
//		$extra = array(
//			'wherepart' => $_POST['ITwherepart']
//		);
//
//		return $extra;
//	}
}
