<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
if (!class_exists("Phoundry")) {
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
}
if (!class_exists("Inputtype")) {
	require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";
}

class ITXchildren extends Inputtype
{
	public $query, $help, $queryFields;
	
	/**
	 * @var int Table id of foreign table
	 */
	private $tid;

	/**
	 * @var Phoundry
	 */
	private $phoundry;

	private $parentId;

	public function __construct(Column $column = null)
	{
		if (!is_null($column)) {
			parent::__construct($column);
		}
		
		if (isset($this->column->datatype->extra['table'])) {
			$string_id = $this->column->datatype->extra['table'];
			$this->tid = PH::getTableIdByStringId($string_id);
			$this->phoundry = new Phoundry($this->tid);
		}
	}

	/**
	 * Translate a database value into a human-readable format.
	 *
	 * @author Arjan Haverkamp
	 * @param string $page Either 'search', 'insert', 'update' or 'view'
	 * @param string $value The database value
	 * @param string|void $keyval
	 * @return string A human-readable presentation of the database value
	 */
	public function getValue($page, $value, $keyval = null)
	{
		$this->parentId = $value;
		return $this->countChildRecords($value);
	}

	/**
	 * Create a FIELDSET containing a representation of this inputtype.
	 *
	 * @param string $page (pdf, view, update, insert, copy)
	 * @param string $value The value for the inputtype.
	 * @param bool $readOnly Whether this inputtype is read-only (non-editable)
	 * @param array $includes A reference to an array with this structure:
	 *                         'jsCode'=>array(),
	 *                         'jsFiles'=>array(),
	 *						   'cssFiles'=>array()
	 *							Add entries to those arrays in order to have
	 *							them included in the <HEAD> of the edit-page.
	 * @return HTML-code for a FIELDSET containing the inputtype.
	 * @returns string
	 */
	public function getHtml($page, $value = '', $readOnly = false,
		&$includes = array()) 
	{
		global $PHprefs;
		$key = $this->parentId;
		$value = $this->recordTable($key);
		
		if ($readOnly || $page == 'view') {
			if ($value === '' || is_null($value))
				$value = '&nbsp;';
			$html  = '<fieldset class="field"><legend><b>'.
				htmlspecialchars($this->column->description).'</b></legend>';
			$html .= $value;
			$html .= "\n</fieldset>\n\n";
			return $html;
		}
		switch ($page) {
			case 'insert':
				return '<fieldset><legend><b>' .
					htmlspecialchars($this->column->description) .
					'</b></legend>' .
					word(550) .
					'</fieldset>';
			case 'update':
			case 'copy':
				$html  = '<fieldset id="ITXchildren_'.$this->column->id.
					'" class="field"><legend><b' .
					($this->column->required ? ' class="req"' : '') . '>' .
					htmlspecialchars($this->column->description) . '</b>';
				
				$helpers = array_merge(
					$this->getHelperLinks($page, $key),
					$this->column->datatype->getHelperLinks($page)
				);
				
				if (count($helpers))
					$html .= ' (<span>' . implode(', ', $helpers) . '</span>)';
				$html .= '</legend>' . "\n";
				if (!empty($this->column->info)) {
					$html .= '<div class="miniinfo">'.
						$this->column->info.'</div>';
				}
				$html .= $this->getInputHtml($page, $value, $includes);
				$explanation = $this->column->datatype->getExplanation();
				if (!empty($explanation))
					$html .= ' (' . $explanation . ')';
				$html .= "\n</fieldset>\n\n";
				
				$uUrl = $this->getupdateUrl($key);
				$includes['jsCode'][] =
					'var itxchildren_'.$this->column->id.' = '.
					'function() {
					var a = $("#ITXchildren_'.$this->column->id.' legend span");
					a.replaceWith("<img src=\"'.$PHprefs['url'].
						'/core/icons/throbber.gif\">");
					jQuery.ajax({url: "" + location, success: function(data) {
						var html = $(data).find("#ITXchildren_'.
							$this->column->id.'").html();
						$("#ITXchildren_'.$this->column->id.'").html(html);
						
					}});
					};
					
					$("#ITXchildren_'.$this->column->id.' tbody tr").live("dblclick", function(e) {
						makePopup(e, -40, -40, "'.word(7).'", "'.$uUrl.'&RID="+$(this).data("rid"));
					});
					';
				
				break;
			default:
				return $value;
		}
		return $html;
	}

	/**
	 * Get hyperlinks for helper-GUIs in FIELDSETs.
	 *
	 * @param string $page The page (update, insert, copy) to get helpers for.
	 * @return array List of helper-links.
	 */
	public function getHelperLinks($page, $key = null)
	{
		global $db, $PHprefs, $PHSes;
		if (in_array($page, array('update', 'view'))) {
			$sql = sprintf(
				'SELECT %s AS id FROM %s WHERE %s = "%s"',
				$this->phoundry->key,
				$this->phoundry->name,
				$this->column->datatype->extra['child'],
				mysql_real_escape_string($key)
			);
			$cur = $db->Query($sql) or trigger_error(
				"Query {$sql} failed: ".$db->Error(), E_USER_ERROR
			);
		
			$ids = array();
			for ($len = $db->NumberOfRows($cur), $row = 0; $row < $len; $row++) {
				$ids[] = $db->FetchResult($cur, $row, 'id');
			}
			
			$index = 't_'.$this->tid.'_'.
				$this->column->datatype->extra['child'].'_'.$key;
			
			unset($PHSes->limitRIDs[$this->tid]);
			$PHSes->limitRIDs[$index] = $ids ? $ids : array(-1);
			
			// Filter the child records on our ID
			$get = array(
				'TID' => $this->tid,
				'site_identifier' => isset($_GET['site_identifier']) ? $_GET['site_identifier'] : null,
				'PHlimitRIDs' => $index,
				$this->column->datatype->extra['child'] => $key,
				'popup' => 'close',
				'callback' => 'parent.itxchildren_'.$this->column->id
			);
			
			$url = $this->phoundry->rUrl.'?'.http_build_query($get);
			$iUrl = $this->phoundry->iUrl.'?'.http_build_query($get);
			
			return array(
				'<a href="'.htmlspecialchars($iUrl).
					'" class="popupWin ptr" title="'.word(6).'|-40|-40">'.
					word(6).'</a>',
				'<a href="'.htmlspecialchars($url).
					'" class="popupWin ptr" title="'.word(551).'|-40|-40">'.
					word(551).'</a>'
			);
		}
		return array();
	}
	
	private function getUpdateUrl($key)
	{
		$get = array(
			'TID' => $this->tid,
			'site_identifier' => isset($_GET['site_identifier']) ? $_GET['site_identifier'] : null,
			$this->column->datatype->extra['child'] => $key,
			'popup' => 'close',
			'callback' => 'parent.itxchildren_'.$this->column->id
		);
			
		return $this->phoundry->uUrl.'?'.http_build_query($get);
	}
	
	/**
	 * Generate the recordTable as generated by phoundry for the child records
	 * @param mixed $keyval
	 * @return string html
	 */
	private function recordTable($keyval)
	{
		global $db, $PHprefs;
		// Filter the child records on our ID
		$get = array(
			'TID' => $this->tid,
			'site_identifier' => isset($_GET['site_identifier']) ? $_GET['site_identifier'] : null,
			'PHs_in' => $this->column->datatype->extra['child'],
			'PHsrch' => $keyval,
			$this->column->datatype->extra['child'] => $keyval
		);
		$struct = $this->phoundry->get2struct($get);
		$query = $this->phoundry->makeQuery($struct);
		
		$html = array(
			'<table class="sort-table" cellspacing="0" data-tid="'.$this->tid.'">',
			'<tbody>', '<tr>'
		);

		$original_tid = $_GET['TID'];
		$_GET['TID'] = $this->tid;
		
		// Draw column headers:
		foreach ($this->phoundry->cols as $col) {
			if (!in_array($col->name, $struct['show']))
				continue;

			$html[] = '<th nowrap="nowrap">'.
				htmlspecialchars($col->description).
				"</th>";
		}
		$html[] = "</tr>";
		
		$cur = $db->Query($query['recordSQL']) or trigger_error(
			"Query {$query['recordSQL']} failed: ".$db->Error(), E_USER_ERROR
		);
		
		for ($len = $db->NumberOfRows($cur), $row = 0; $row < $len; $row++) {
			$recordID = @$db->FetchResult($cur, $row, 0);
			$html[] = '<tr data-rid="'.htmlspecialchars($recordID).
				'" class="'.($row & 1 ? 'even' : 'odd').'">';
			
			/* @var $col Column */
			foreach ($this->phoundry->cols as $col) {
				if (!in_array($col->name, $struct['show'])) {
					continue;
				}
				if (strtolower(get_class($col->datatype)) != 'dtfake') {
					$GLOBALS['RECORD_ID'] = is_null(
						$db->FetchResult($cur, $row, $col->name)
					) ? 'NULL' : $db->FetchResult($cur, $row, $col->name);
				}
				$html[] = '<td'.
					(strtolower(get_class($col->inputtype)) == 'itradio' ?
						' align="center"' : ''
					).'>'.
					$col->getValue(
						'search', $recordID, $cur, $row,
						$PHprefs['recordStrlen']
					).'</td>';
			}
			$html[] = '</tr>';
		}
		
		$html[] = '</tbody>';
		$html[] = '</table>';

		$_GET['TID'] = $original_tid;
		return implode("\n", $html);
	}
	
	private function countChildRecords($keyval)
	{
		global $db, $PHprefs;
		// Filter the child records on our ID
		$get = array(
			'TID' => $this->tid,
			'site_identifier' => $_GET['site_identifier'],
			'PHs_in' => $this->column->datatype->extra['child'],
			'PHsrch' => $keyval,
			$this->column->datatype->extra['child'] => $keyval
		);
		$struct = $this->phoundry->get2struct($get);
		$query = $this->phoundry->makeQuery($struct);
		
		$cur = $db->Query($query['countSQL']) or trigger_error(
			"Query {$query['countSQL']} failed: ".$db->Error(), E_USER_ERROR
		);
		
		return $db->FetchResult($cur, 0, 0);
	}
}
