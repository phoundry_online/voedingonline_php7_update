<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITfileupload extends Inputtype
{
	public $size, $maxlength = -1;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
			if (!empty($this->extra['maxlength']))
				$this->maxlength = $this->extra['maxlength'];
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $PHprefs, $PHSes; 
		
		$alt = $this->column->getJScheckAltTag();
		$value = PH::htmlspecialchars(parent::getInputHtml($page, $value, $includes));
		$html = '';
		if (($page == 'update' || $page == 'copy') && $value !== '') {
			$saveDir = '';
			if (isset($this->column->datatype->extra['savedir'])) {
				$saveDir = $this->column->datatype->extra['savedir'];
				$saveDir = str_replace(array('{$USER_ID}','{$GROUP_ID}'), array($PHSes->userId, $PHSes->groupId), $saveDir);
			}

			$images = array('gif', 'jpg', 'jpeg', 'png', 'swf');
			$ext = strtolower(substr($value, strrpos($value,'.')+1));
			$w = in_array($ext, $images) ? '400' : '$(_W).width()-50';
			$h = in_array($ext, $images) ? '300' : '$(_W).height()-50';

			$url = $PHprefs['url'] . '/core/popups/viewFile.php?' . QS(1,"CID={$this->column->id}");
			$html .= word(70) . ' ' . strtolower($this->column->description) . ': ' . $value; // Current file
			$html .= ' (<a href="' . $url . '" onclick="makePopup(event,' . $w . ',' . $h . ',\'' . escSquote(ucfirst(word(49))) . '\',\'' . $url . '&amp;file=' . urlencode('/' . $saveDir . '/' . $value) . '\');return false">' . word(49) . '</a>)<br />';
		}
		$html .= '<input type="hidden" name="old_' . $this->name . '" value="' . $value . '" />';
		$html .= '<input name="' . $this->name . '" type="file" size="' . $this->extra['size'] . '" value="' . $value . '"';
		if ($this->maxlength != -1)
			$html .= ' maxlength="' . $this->maxlength . '"';
		$html .= ' />';
		$html .= ' (Max ' . str_replace('M', 'MB', ini_get('upload_max_filesize')) . ')';
		if (!$this->column->required && $value !== '' && ($page == 'update' || $page == 'copy')) {
			$html .= '<br /><input type="checkbox" name="none_' . $this->name . '" value="1" />' . word(186, strtolower($this->column->description), $value);
		}
		return $html;
	}

		
	public function getExtra() 
	{
		$maxlength = isset($this->column->DBprops['length']) ? $this->column->DBprops['length'] : '';
		$size = ($maxlength == '') ? 60 : min(60, $maxlength);
		$value = isset($this->column->DBprops['default']) ? $this->column->DBprops['default'] : '';
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['maxlength']))
				$maxlength = $this->extra['maxlength'];
			if (isset($this->extra['value']))
				$value = $this->extra['value'];
		}
		$adminInfo = array();
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['maxlength'] = array(
			'name'     => 'ITmaxlength',
			'label'    => 'Max. length',
			'required' => false,
			'syntax'   => 'integer',
			'info'     => 'Enter the maximum input-length of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$maxlength)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'size'      => (int)$_POST['ITsize'],
			'maxlength' => trim($_POST['ITmaxlength'])
		);

		return $extra;
	}
}
