<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";
require_once "{$PHprefs['distDir']}/core/include/sqlParser/Parser.php";
require_once "{$PHprefs['distDir']}/core/include/sqlParser/Compiler.php";

class ITtree extends Inputtype
{
	private $pear;

	public $query, $queryFields, $nullQuery, $fromTable, $sqlStruct;

	public function __construct(&$column)
	{
		$this->pear = new Pear();

		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);

			if (isset($this->extra['query']) && isset($this->extra['parent'])) {
				$this->initQuery();
			}
		}
	}

	private function initQuery()
	{
		// Insert 'WHERE parent IS NULL' into query:
		$parser = new SQL_Parser();

		$this->sqlStruct = $parser->parse(PH::evaluate($this->extra['query']));

		$insWhere = array(
			'arg_1' => array('value'=>$this->extra['parent'], 'type'=>'ident'),
			'op' => 'is',
			'arg_2' => array('value'=>'', 'type'=>'null')
		);



		if($this->pear->isError($this->sqlStruct)) {
			trigger_error($this->sqlStruct->message, E_USER_ERROR);
		}

		if (!isset($this->sqlStruct['where_clause']))
			$this->sqlStruct['where_clause'] = $insWhere;
		else {
			$this->sqlStruct['where_clause'] = array(
				'arg_1' => $insWhere,
				'op' => 'and',
				'arg_2' => $this->sqlStruct['where_clause']
			);
		}

		$compiler = new SQL_Compiler();
		$this->nullQuery = $compiler->compile($this->sqlStruct);
		$this->fromTable = $this->sqlStruct['table_names'][0];
	}

	public function getValue($page, $value, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				if (is_null($value))
					return '';
				// Determine parents:
				$values = array();
				$parents = array();
				do {
					if (in_array($value, $values)) {
						// Loop
						trigger_error('Loop detected for record '.$values[0].' in column '.$this->name, E_USER_WARNING);
						break;
					}
					$values[] = (int) $value;
					$sql = PH::evaluate(str_replace($this->extra['parent'] . ' is null', $this->extra['optionvalue'] . '= ' . (int)$value, $this->nullQuery));
					$cur = $db->Query($sql)
						or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
					if (!$db->EndOfResult($cur)) {
						$value = $db->FetchResult($cur,0,$this->extra['parent']);
						$text = '';
						foreach($this->extra['searchtext'] as $txt)
							$text .= $db->FetchResult($cur,0,$txt) . ' ';
						$parents[] = trim($text);
					}
					else
						$value = null;
				} while (!is_null($value));
				return implode(' &raquo; ', array_reverse($parents));
			default:
				return $value;
		}
	}

	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes)
	{
		global $PHprefs;

		// Get default value:
		$value = parent::getInputHtml($page, $value, $includes);

		$includes['jsFiles'][strtolower(get_class($this))] = $PHprefs['url'] . '/core/csjs/jquery.treeview.pack.js';

		$alt = $this->column->getJScheckAltTag();
		$html = '<input type="hidden" name="' . $this->name . '" value="' . $value . '" alt="' . $alt . '" />';

		$menu = $this->getMenu();
		$height = count($menu) > 20 ? '200px' : 'auto';

		// If tableName used in $this->extra['query'] is equal to current tablename,
		// then we have the risk of going recursive.
		$JScode = '';

		if ($page == 'update' && in_array($this->column->tableName, $this->sqlStruct['table_names'])) {
			$JScode = "
			// Prevent recursion:
			.find(\"a[title='\"+RID+\"']+ul\")
			.find(\"a\")
			.add(\"a[title='\"+RID+\"']\")
			.attr('onclick', 'return false')
			.css('text-decoration', 'line-through')";
		}

		$html .= '<div style="height:' . $height . ';overflow:auto"><ul id="' . $this->name . '_tree">' . $this->getMenuTree($menu, 0, $value) . '</ul></div>';
		$html .= "
		<script type=\"text/javascript\">
		$(function(){
			$('#{$this->name}_tree')
			.treeview({collapsed:false,unique:true})
			{$JScode}
		});
		</script>";

		return $html;
	}

	public function getMenuTree($menus, $idx = 0, $value)
	{
		if(empty($menus)) return '';
		$curDepth = $startDepth = $menus[$idx]['depth'];
		$str = ''; $cnt = count($menus);
		for ($x = $idx; $x < $cnt; $x++) {
			$item = $menus[$x];
			if ($item['depth'] < $startDepth) break;
			if ($item['depth'] > $startDepth) continue;

			$class = $item['id'] == $value ? 'selected' : '';
			$str .= '<li><a class="' . $class . '" href="#" title="' . $item['id'] . '" onclick="selectTreeLeaf(\'' . $this->name . '\',this);return false">' . PH::htmlspecialchars($item['text']) . '</a>';
			if (isset($menus[$x+1]) && $menus[$x+1]['depth'] > $curDepth) {
				$str .= "<ul>\n" . $this->getMenuTree($menus, $x+1, $value) . "</ul>\n";
			}
			$str .= "</li>\n";
		}

		return $str;
	}

	public function getHelperLinks($page)
	{
		$helpers = array();

		if (($page == 'update' || $page == 'copy') && !$this->column->required) {
			$helpers[] = '<a href="#" onclick="selectTreeLeaf(\'' . $this->name . '\');return false">' . word(161) . '</a>';
		}
		return $helpers;
	}

	public function getMenu($depth = 1, $curid = '')
	{
		global $db;

		$menu = array();

		$sql = PH::evaluate($this->nullQuery);
		if ($curid != '') {
			$sql = str_replace('is null', '= ' . (int)$curid, $sql);
		}
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$nextid = $db->FetchResult($cur,$x,$this->extra['optionvalue']);
			$text = '';
			foreach($this->extra['optiontext'] as $txt) {
				$text .= $db->FetchResult($cur,$x,$txt) . ' ';
			}
			$parent = $db->FetchResult($cur,$x,$this->extra['parent']);

			$menu[] = array('id' => $nextid,
				             'text' => trim($text),
								 'parent' => is_null($parent) ? 0 : $parent,
								 'depth' => $depth
				            );
	      $menu = array_merge($menu, $this->getMenu($depth+1,$nextid));
		}
		return $menu;
	}

	public function getExtra()
	{
		$query = null;
		$queryFields = null;
		if (count($this->extra)) {
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
			if (isset($this->extra['optionvalue'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'parent'      => $this->extra['parent'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
		}
		$adminInfo = array();
      $adminInfo['query'] = array(
         'name'     => 'ITquery',
         'label'    => 'Tree-query',
         'required' => true,
         'syntax'   => 'text',
         'info'     => 'Enter the query for the nodes in the tree (<a href="#" onclick="makePopup(null,300,200,\'Tree Query\',\'recursiveSelectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
         'help'     => true,
         'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
      );
      $adminInfo['queryFields'] = array(
         'name'     => 'ITqueryFields',
         //'required' => true,
         //'syntax'   => 'text',
         'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
      );

		return $adminInfo;
	}

	public function setExtra(&$msg)
	{
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (preg_match('/^SELECT/i', $query)) {
			if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
				$msg = 'Option value, option text and search text are not defined!';
				return false;
			}
		}
		$struct = PH::javascript2php($_POST['ITqueryFields']);
      $extra = array(
         'query'     => $query
      );

		if (isset($struct['optionvalue']))
			$extra['optionvalue'] = (string)$struct['optionvalue'];
		if (isset($struct['parent']))
			$extra['parent'] = (string)$struct['parent'];
		if (isset($struct['optiontext']))
			$extra['optiontext'] = $struct['optiontext'];
		if (isset($struct['searchtext']))
			$extra['searchtext'] = $struct['searchtext'];

		return $extra;
	}

	public function getExtraHelp($what)
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<p>
You need to define a 'lookup' query, some SQL that generates a hierarchical
tree structure. Make sure the <tt>SELECT</tt> clause of the query contains
both the record-id and the parent-id, and at least one other field to display
in the tree as the node-text.
</p>
<pre>
SELECT id, name, parent_id FROM site_menu ORDER BY id
</pre>
<p>
Do not forget to click 'edit' in order to determine what fields in the
query defined here mean what!
</p>
EOH;
				break;
			default:
				$help = 'No help found for ' . $what . '!';
				break;
		}
		return $help;
	}
}
