var vnuDialog = {

	preInit : function() {
		tinyMCEPopup.requireLangPack();
	},

	init : function(ed) {
		var F = document.forms[0], ed = tinyMCEPopup.editor, dom = ed.dom, n = ed.selection.getNode();

		F.action+=tinyMCEPopup.getWin().document.location.search;
		F['imgDir'].value = ed.getParam('phoundry').image['folder'];
		F['width'].value = dom.getAttrib(n, 'width');
		F['height'].value = dom.getAttrib(n, 'height');
	}
};

vnuDialog.preInit();
tinyMCEPopup.onInit.add(vnuDialog.init, vnuDialog);

