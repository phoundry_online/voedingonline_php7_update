<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		error_log('Not a POST method in ' . __FILE__);
		exit();
	}

	$lang = $PHSes->lang;
	$saveDir = $PHprefs['uploadDir'] . '/' . PH::WPdecode($_POST['imgDir']);
	$width = (int)$_POST['width'];
	$height = (int)$_POST['height'];
	$orgName = str_replace(',', '_', $_FILES['imgFile']['name']);
	$phpName = $_FILES['imgFile']['tmp_name'];
	$maxSize = 50000;
	$file = $saveDir . '/' . $orgName;
	$url  = $PHprefs['uploadUrl'] . '/' . PH::WPdecode($_POST['imgDir']) . '/' . $orgName;

	if($_FILES['imgFile']['error'] == 1) {
		die('Filesize exceeds maximum server upload limit.');
   }
	if($_FILES['imgFile']['error'] != 0) {
		die('Something went wrong while uploading your image');
	}
	if(@move_uploaded_file($phpName, "$file")) {
		chmod("$file", 0644);
	}

	$VNUwords = array(
		'nl' => array(
			1 => 'De afbeelding die u heeft ge-upload kan niet verwerkt worden. De afbeelding mag maximaal '. $height .' pixels hoog zijn en '. $width .' pixels breed.',
			2 => 'De afbeelding die u heeft ge-upload kan niet verwerkt worden. De afbeelding mag maximaal 50kb groot zijn.',
		),
		'en' => array(
			1 => 'The image you have uploaded could not be processed. The image may not be taller than '. $height .' pixels and not be wider than '. $width .' pixels.',
			2 => 'The image you have uploaded could not be processed. The image may not be larger than 50kb.',
		),
	);
	
	// Get info of the image that has been uploaded.
	$size = getimagesize($file);
	$errors = array();
	if($size[0] > $width || $size[1] > $height) {
		$errors[] = PH::htmlspecialchars($VNUwords[$lang][1]);
	}

	if($_FILES['imgFile']['size'] > $maxSize) {
		$errors[] = PH::htmlspecialchars($VNUwords[$lang][2]);
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>{#vnu_dlg.image_replace}</title>
	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
<script type="text/javascript">

tinyMCEPopup.requireLangPack();

<?php if (empty($errors)) { ?>
	var ed = tinyMCEPopup.editor, dom = ed.dom, n = ed.selection.getNode();
	var args = {src:'<?= $url ?>'};
	ed.dom.setAttribs(n, args);
	tinyMCEPopup.close();
<?php } ?>

</script>
</head>
<body style="display:none">
<?php
	if (!empty($errors)) {
		print '<ul><li>' . implode("</li>\n<li>", $errors) . '</li></ul>';
      if(file_exists($file)) {
			unlink($file);
		}
	}
?>

<div class="mceActionPanel">

	<div style="float: right">
		<input type="button" id="cancel" name="cancel" value="{#vnu_dlg.back}" onclick="document.location='img_replace.htm';return false" />
	</div>
</div>

</body>
</html>
