/**
 * @author Web Power (Arjan Haverkamp)
 * @copyright Copyright © 2004-2009, Web Power BV, All rights reserved.
 */

(function() {
	// Load plugin specific language pack
	tinymce.PluginManager.requireLangPack('WPvnu');

	tinymce.create('tinymce.plugins.WPvnuPlugin', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
			this.editor = ed;

			ed.addCommand('mceVNUimgReplace', function() {
				ed.windowManager.open({
					file: url + '/img_replace.htm',
					width: 300,
					height:150,
					inline: true
				},{
					plugin_url: url
				})
			});

			ed.addButton('VNUimg_replace', {
				title : ed.getLang('vnu.image_replace'),
				image : url + '/img/vnu_img_replace.gif',
				cmd   : 'mceVNUimgReplace'
			});

			// Add a node change handler, selects the button in the UI when a image is selected
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setDisabled('VNUimg_replace', n.nodeName != 'IMG');
			});
		},

		/**
		 * Creates control instances based in the incomming name. This method is normally not
		 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
		 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
		 * method can be used to create those.
		 *
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {
			return null;
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname : 'VNU plugin',
				author : 'Web Power (Arjan Haverkamp)',
				authorurl : 'http://www.webpower.nl',
				infourl : 'http://www.webpower.nl',
				version : '2.0 (2009-09-22)'
			};
		}

	});

	// Register plugin
	tinymce.PluginManager.add('WPvnu', tinymce.plugins.WPvnuPlugin);
})();
