tinyMCE.addI18n('nl.vnu_dlg',{
	image_replace: 'Vervang afbeelding',
	select_img: 'Kies afbeelding (GIF, JPG of PNG)',
	load: 'Laad',
	error_select_img: 'Selecteer een afbeelding (met extensie .gif, .jpg, .jpeg of .png)!',
	back: 'Terug'
});
