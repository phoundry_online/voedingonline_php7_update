<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once getenv('CONFIG_DIR') . '/PREFS.php';
	}
	header('Content-type: application/json; charset=utf-8'); // JSON Page; always utf-8
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess();

	if (!isset($HTTP_RAW_POST_DATA)){
		// PHP 5.2.2 has a bug, it does not populate HTTP_RAW_POST_DATA:
		$HTTP_RAW_POST_DATA = file_get_contents('php://input');
	}

	$args = json_decode($HTTP_RAW_POST_DATA);
	$html = urldecode($args->params->html);

	$ch = curl_init('http://inlinestyler.torchboxapps.com/styler/convert/');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, 'returnraw=1&source=' . rawurlencode($html));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	$html = trim(curl_exec($ch));
	$curl_error = curl_error($ch);
	curl_close($ch);

	$html = str_replace(
		array('&lt;', '&gt;', '&quot;'),
		array('<', '>', '"'),
		$html
	);

	if (!empty($curl_error)) {
		print json_encode(array('error'=>'CSS inliner error: ' . $curl_error));
		exit;
	}

	print json_encode(array('result'=>array('html'=>$html)));
?>
