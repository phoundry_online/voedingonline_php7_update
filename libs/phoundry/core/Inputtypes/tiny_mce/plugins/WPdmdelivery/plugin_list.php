<?php
   $inc = @include_once('PREFS.php');
   if ($inc === false) {
      require_once(getenv('CONFIG_DIR') . '/PREFS.php');
   }
   header("Content-type: application/x-javascript; charset={$PHprefs['charset']}");
   require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$DMDcid = $PHenv['DMDcid'];

	$Plang = explode(',', $PHenv['lang']);
	$Plang = in_array($PHSes->lang, $Plang) ? $PHSes->lang : $Plang[0];

	$tableInfo = DMD::getTableInfo($PHenv['rcptTable'], $Plang);

	// What language is the mailing?
	if ($TID == 14 || $TID == 31) {
		// Inserting a plugin in a mailing:
		$sql = "SELECT lang FROM mailing WHERE id = " . (int)$_GET['RID'];
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$lang = $db->FetchResult($cur,0,'lang');
	}
	elseif ($TID == 13) {
		// Inserting a plugin in a template:
		$lang = $Plang;
	}
	elseif ($TID == 15) {
		// Inserting a plugin in a plugin:
		$lang = $_GET['PHfltr_18'];
	}

	$links = array(
		"['Web link','{\$WEBLINK}']"
	);
	if ($DMDprefs['enablePDF']) {
		$links[] = "['PDF link','{\$PDFLINK}']";
	}

	if ($PHenv['overall']) {
		$sql = "SELECT name, is_active, campaign_id FROM plugin WHERE campaign_id = {$DMDcid} AND lang = '{$lang}' AND linkable = 1 UNION SELECT name, is_active, campaign_id FROM plugin WHERE campaign_id = 0 AND lang = '{$lang}' AND linkable = 1 ORDER BY campaign_id DESC, name ASC";
	}
	else {
		$sql = "SELECT name, is_active FROM plugin WHERE campaign_id = {$DMDcid} AND lang = '{$lang}' AND linkable = 1 ORDER BY name ASC";
	}
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$name = escSquote($db->FetchResult($cur,$x,'name'));
		$links[] = "['Plugin {$name}','{\$PLUGINLINK={$name}}']";
	}

	// Links to events:
	if (isset($DMDprefs['enableEvents']) && $DMDprefs['enableEvents'] == true) {
		$sql = "SELECT id, name, lang FROM dmd_event WHERE campaign_id = {$DMDcid} AND close_date >= NOW() ORDER BY name ASC";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$name = escSquote($db->FetchResult($cur,$x,'name'));
			$links[] = "['Event {$name}, public (" . $db->FetchResult($cur,$x,'lang') . ")','{\$EVENTLINK_PUBLIC={$name}}']";
			$links[] = "['Event {$name}, personalized (" . $db->FetchResult($cur,$x,'lang') . ")','{\$EVENTLINK={$name}}']";
		}
	}

	// Links to landingpages (if available):
	$sql = "SELECT id, name, lang FROM dmd_landingpage WHERE campaign_id = {$DMDcid} ORDER BY id DESC";
	$cur = $db->Query($sql);
	if ($cur) {
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$name = escSquote($db->FetchResult($cur,$x,'name'));
			$links[] = "['Landingpage {$name} (" . $db->FetchResult($cur,$x,'lang') . ")','{\$LANDINGPAGELINK={$name}}']";
		}
	}

	print 'var tinyMCELinkList = [' . implode(',', $links) . "];\n";
?>
