<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once getenv('CONFIG_DIR') . '/PREFS.php';
	}
	header('Content-type: application/json; charset=utf-8'); // JSON Page; always utf-8
    require_once "{$PHprefs['distDir']}/core/include/PH.class.php";
    $_POST['CSRF'] = PH::WPencode('load_url.php');
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess();

	if (!isset($HTTP_RAW_POST_DATA)){
		// PHP 5.2.2 has a bug, it does not populate HTTP_RAW_POST_DATA:
		$HTTP_RAW_POST_DATA = file_get_contents('php://input');
	}

	$args = json_decode($HTTP_RAW_POST_DATA);
	$url  = $args->params->url;

	if ($url{0} == '/') {
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $url;
	}
	else if (!preg_match('/^https?:\/\//i', $url)) {
		$url = 'http://' . $url;
	}

	$slurp = slurpHTML($url, $errorMsg);
	if ($slurp === false) {
		// Some error
		print json_encode(array('error'=>$errorMsg));
		exit;
	}
	elseif (preg_match('/<\s*frameset[^>]*>/i', $slurp['html'])) {
		print json_encode(array('error'=>"{$url} contains a frameset, it cannot be opened!"));
		exit;
	}
	else {
		$html = $slurp['html'];
		// Strip <xml version> header:
		$html = preg_replace('/^<\?xml\s+version="1.0"([^>]+)>/i', '', $html);

		// Convert HTML to utf-8 (this page is called via JSON, which expects utf-8)
		switch($slurp['charset']) {
			case 'utf-8':
				// Do nothing
				break;
			default:
				$html = PH::iconv($slurp['charset'], 'utf-8', $html);
				break;
		}
		print json_encode(array('result'=>array('html'=>$html)));
	}

function fixUrlsInHtml($html, $base, $path = '/')
{
	// Substitute (no)script/style blocks with placeholders:
   preg_match_all('/<(noscript|script|style)([^>]+|)>([\s\S]*?)<\/\\1>/i', $html, $codeBlocks);
   foreach(($codeBlocks = $codeBlocks[0]) as $i => $codeBlock) {
      $html = str_replace($codeBlock, '<!--[CODEBLOCK'.$i.']-->', $html);
   }

	if (preg_match_all("/<\s*(a|area|body|form|img|table|td|link|object|embed|input)\s+[^>]+>/i", $html, $_matches, PREG_OFFSET_CAPTURE)) {
		$_posd = 0;
		$_chunks = array();
		foreach($_matches[0] as $_match) {
			$_posp  = $_match[1];
			$_pos   = $_match[1] - $_posd;
			$_tag   = $_oldTag = $_match[0];
			$_tagl  = strtolower($_tag);

			// Skip tags without 'href', 'action', 'src' or 'background' attribute:
			if (!preg_match('/(href|action|src|background|data)\s*=\s*(?:"([^"]+)"|([^\'">\s]+)|\'([^\']+)\')/i', $_tag, $_reg)){
				continue;
			}

			$_url  = $_reg[2] ? $_reg[2] : ($_reg[3] ? $_reg[3] : $_reg[4]);
			$_urll = strtolower($_url);

			// Continue when URL starts with #, mailto:, ftp: of javascript:
			if (strpos($_urll, '#') === 0 || strpos($_urll, 'mailto:') === 0 || strpos($_urll, 'javascript:') === 0 || strpos($_urll, 'ftp:') === 0 || strpos($_urll, 'itpc:') === 0 || substr($_urll, 0, 10) == '/x/plugin/') {
				continue;
			}

			if (substr($_urll, 0, 7) != 'http://' && substr($_urll, 0, 8) != 'https://' && substr($_urll,0,2) != '{$') {
				// Make HREF absolute:
				if ($_url{0} != '/') {
					$_url = $path . $_url;
				}
				$_url = $base . str_replace('//','/',$_url);
			}

			$_chunks[] = substr($html, 0, $_pos);
			$_chunks[] = str_replace($_reg[0], "{$_reg[1]}=\"{$_url}\"", $_tag); // New tag
			$_len      = strlen($_oldTag);
			$html      = substr($html, $_pos + $_len);
			$_posd     = $_posp + $_len;
		} // end foreach
		$_chunks[] = $html;
		$html = implode('', $_chunks);
	}

	// Substitute url() style tags:
	if (preg_match_all('/url\(([^\)]+)\)/i', $html, $reg)) {
		$i = 0;
		foreach($reg[1] as $match) {
			if (($match{0} == "'" && substr($match, -1) == "'") ||
				($match{0} == '"' && substr($match, -1) == '"')) {
				// Remove quotes (they shouldn't be there in the 1st place!):
				$match = substr($match, 1, strlen($match)-2);
			}
			if (preg_match('/^https?:\/\/|\/x\//', $match)) continue;

			$match = ($match{0} == '/') ? $base . $match : $base . $path . $match;
			$html = str_replace($reg[0][$i], 'url(' . $match . ')', $html);

			$i++;
		}
	}

	// Put codeblocks back:
   foreach($codeBlocks as $idx=>$codeBlock) {
      $html = str_replace('<!--[CODEBLOCK'.$idx.']-->', $codeBlock, $html);
   }

	return $html;
}

/**
 * Fetch HTML from a given URL.
 *
 * @author Arjan Haverkamp
 *
 * @param $url(string) The URL to fetch
 * @param $error(string) Reference to error message
 * @return The HTML that was fetched, or false in case of an error
 * @returns mixed
 */
function slurpHTML($url, &$error) {
   //
   // Slurp the HTML using CURL:
   //
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)');
   curl_setopt($ch, CURLOPT_FAILONERROR, 1);
   @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
   curl_setopt($ch, CURLOPT_TIMEOUT, 60);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
   $html        = trim(curl_exec($ch));
   $status      = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
   $curl_error  = curl_error($ch);
   curl_close($ch);

	$charset = 'utf-8';
	if (preg_match('/charset=([^ ]+)/', $contentType, $reg)) {
		$charset = strtolower($reg[1]);
	}
	elseif (preg_match('/<meta\s+http-equiv[^;]+;\s*charset=([^">]+)/i', $html, $reg)) {
		$charset = strtolower($reg[1]);
	}

   // Make sure no CURL errors occured:
   if (!empty($curl_error)) {
      $error = "CURL error: {$curl_error} while connecting to {$url}";
      return false;
   }
   // Check for a valid HTTP status:
   if (!($status >= 200 && $status < 300)) {
      $error = "URL {$url} returned HTTP-status $status";
      return false;
   }

	//
	// Substitute paths in HTML:
	//
	$url_props = parse_url($url);

	$host = $url_props['scheme'] . '://' . $url_props['host'];
	$path = isset($url_props['path']) ? $url_props['path'] : '/';

	if (preg_match('/\.(php|php4|php3|phtml|cgi|cfml|pl|html?|jsp|asp|cfm)$/i', $path)) {
		$path = dirname($path);
	}
	if (substr($path, -1, 1) != '/'){
		$path .= '/';
	}

	return array('charset'=>$charset, 'html'=>fixUrlsInHtml($html, $host, $path));
}

?>
