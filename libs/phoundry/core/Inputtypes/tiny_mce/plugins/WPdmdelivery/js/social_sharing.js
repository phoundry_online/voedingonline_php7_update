var SocialSharingDialog = {
	preInit : function() {
		tinyMCEPopup.requireLangPack();
	},

	init : function(ed) {
		this.editor = ed;
	},

	insertSharing : function() {
		var ed = this.editor, ctxt = $('#social_panel');

		tinyMCEPopup.restoreSelection();

		var url   = $("input[name='url']", ctxt).val();
		var title = $("input[name='title']", ctxt).val();
      	var orientation = $('input[name=orientation]', ctxt)[0].checked ? 'horizontal' : 'vertical';

		if ('' != url && !/^https?:\/\//i.test(url)) {
			tinyMCEPopup.alert('dmdelivery_dlg.url_invalid');
			return;
		}
      
		var html = '<table border="0">';
		if (orientation == 'horizontal') { html += '<tr>'; }
      
		$(':checkbox:checked', ctxt).each(function() {
         
			var site = $(this).val();
            
			if (orientation == 'vertical') { html += '<tr>'; }
         
			html += '<td>';
			html += '<img src="/x/pics/social/'+site+'_sample.png" rel="socialshare-'+site+'" title="' + title + '-' + site + '" data-url="' + url + '" />';
			html += '</td>';
         
			if (orientation == 'vertical') { html += '</tr>'; }    
         
		});
      
		if (orientation == 'horizontal') { html += '</tr>'; }
		html += '</table>';
      
		ed.execCommand('mceBeginUndoLevel');
		ed.execCommand('mceInsertContent', false, html);
		ed.execCommand('mceEndUndoLevel');

		tinyMCEPopup.close();
	}
}

SocialSharingDialog.preInit();
tinyMCEPopup.onInit.add(SocialSharingDialog.init, SocialSharingDialog);