var SocialBookmarkDialog = {
	preInit : function() {
		tinyMCEPopup.requireLangPack();
	},

	init : function(ed) {
		this.editor = ed;
		var licenseOwner = ed.getParam('dmdelivery')['licenseOwner'];
		$('.'+licenseOwner).show();
	},

	insertBookmarks : function() {
		var ed = this.editor, ctxt = $('#bookmarks_panel');;

		tinyMCEPopup.restoreSelection();

		var name, title, orientation = $('input[name=orientation]', ctxt)[0].checked ? 'horizontal' : 'vertical', html = '<table border="0">';
		if (orientation == 'horizontal') { html += '<tr>'; }
		$(':checkbox:checked', ctxt).each(function() {
			name   = $(this).val(); 
            title  = $(this).attr('title');
            var cTitle = $('input[name=title]', ctxt).val();
            var cUrl   = $('input[name=url]', ctxt).val();
			var cContent = $('input[name=content]', ctxt).val();
			var cPic = $('input[name=pic]', ctxt).val();
            
            var socialLink = name;
            if ('' != cUrl) {
               socialLink += '|' + escape(cUrl);
            }
            if ('' != cTitle) {
               socialLink += '|' + escape(cTitle);
               title = cTitle;
            }
			if ('' != cContent) {
				socialLink += '|' + escape(cContent);
			}
			if ('' != cPic) {
				socialLink += '|' + escape(cPic);
			}
            
            if (orientation == 'vertical') { html += '<tr>'; }
			html += '<td><a href="{$SOCIALLINK=' + socialLink + '}" title="[' + name + ']">';
			if (name == 'linkedInShare') {
				html += '<img src="/x/pics/social/' + name + '.png" alt="'+title+'" title="'+title+'" border="0" width="60" height="20" />';
			}
			else if (name == 'facebookShare') {
				html += '<img src="/x/pics/social/' + name + '.png" alt="'+title+'" title="'+title+'" border="0" width="48" height="20" />';
			}
			else if (name == 'twitterShare') {
				html += '<img src="/x/pics/social/' + name + '.png" alt="'+title+'" title="'+title+'" border="0" width="55" height="20" />';
			}
			else if (name == 'googlePlusShare') {
				html += '<img src="/x/pics/social/' + name + '.png" alt="'+title+'" title="'+title+'" border="0" width="32" height="20" />';
			}
			else {
				html += '<img src="/x/pics/social/' + name + '.gif" alt="'+title+'" title="'+title+'" border="0" width="16" height="16" />';
			}
			html += '</a></td>';
			if (orientation == 'vertical') { html += '</tr>'; }
		});
      
		if (orientation == 'horizontal') { html += '</tr>'; }
		html += '</table>';

		ed.execCommand('mceBeginUndoLevel');
		ed.execCommand('mceInsertContent', false, html);
		ed.execCommand('mceEndUndoLevel');

		tinyMCEPopup.close();
	}
}

SocialBookmarkDialog.preInit();
tinyMCEPopup.onInit.add(SocialBookmarkDialog.init, SocialBookmarkDialog);
