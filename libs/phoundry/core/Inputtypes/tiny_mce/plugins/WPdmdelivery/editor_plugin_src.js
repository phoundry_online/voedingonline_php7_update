/**
 * @author Web Power (Arjan Haverkamp)
 * @copyright Copyright � 2004-2011, Web Power BV, All rights reserved.
 */

(function() {
	// Load plugin specific language pack
	tinymce.PluginManager.requireLangPack('WPdmdelivery');

	tinymce.create('tinymce.plugins.WPDMdeliveryPlugin', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
			this.editor = ed;

			ed.addCommand('mceWPpreview', function() {
				ed.windowManager.open({
					file: url + '/preview.html',
					width: 480,
					height: 640,
					resizable: true,
					scrollbars: true,
					maximizable: true,
					inline: false
				},{
					base: ed.documentBaseURI.getURI(),
					url: url
				});
			});

			ed.addButton('WPpreview', {
				title: ed.getLang('preview.preview_desc'),
				image: url + '/img/preview.png',
				cmd: 'mceWPpreview'
			});

			ed.addCommand('mceWPsocialBookmarks', function() {
				ed.windowManager.open({
					file: url + '/social_bookmarks.html',
					width: 545,
					height:420,
					inline: true
				},{
					plugin_url: url
				});
			});

			ed.addButton('WPsocialBookmarks', {
				title : ed.getLang('dmdelivery.social_bookmarks'),
				image : url + '/img/social_bookmark.png',
				cmd   : 'mceWPsocialBookmarks'
			});

			ed.addCommand('mceWPsocialSharing', function() {
				ed.windowManager.open({
					file: url + '/social_sharing.html',
					width: 400,
					height:170,
					inline: true
				},{
					plugin_url: url
				});
			});

			ed.addButton('WPsocialSharing', {
				title : ed.getLang('dmdelivery.social_sharing'),
				image : url + '/img/social_sharing.png',
				cmd   : 'mceWPsocialSharing'
			});
		
			ed.addCommand('mceWPloadUrl', function() {
				ed.windowManager.open({
					file: url + '/load_url.html',
					width: 300,
					height:120,
					inline: true
				},{
					plugin_url: url
				});
			});

			ed.addButton('WPloadUrl', {
				title : ed.getLang('dmdelivery.load_url'),
				image : url + '/img/load_url.png',
				cmd   : 'mceWPloadUrl'
			});

			ed.addCommand('mceWPloadZip', function() {
				ed.windowManager.open({
					file: url + '/load_zip.html',
					width: 300,
					height:120,
					inline: true
				},{
					plugin_url: url
				});
			});

			ed.addButton('WPloadZip', {
				title : ed.getLang('dmdelivery.load_zip'),
				image : url + '/img/load_zip.png',
				cmd   : 'mceWPloadZip'
			});

			ed.addCommand('mceWPlinkTitles', function() {
				ed.windowManager.open({
					file: url + '/link_titles.html',
					width: 600,
					height:500,
					inline: true
				},{
					plugin_url: url
				});
			});

			ed.addButton('WPlinkTitles', {
				title : ed.getLang('dmdelivery.link_titles'),
				image : url + '/img/link_titles.png',
				cmd   : 'mceWPlinkTitles'
			});

			ed.addCommand('mceWPinlineStyles', function() {
				var html = ed.getContent();
				if (!/<style/i.test(html)) {
					alert(ed.getLang('dmdelivery.no_style_tags'));
					return;
				}
				ed.setProgressState(true);
				var json = tinymce.util.JSONRequest.sendRPC({
					url : url + '/css_inliner.php',
					params : {html:encodeURIComponent(html)},
					success : function(result) {
						ed.setContent(result['html']);
						ed.setProgressState(false);
						alert(ed.getLang('dmdelivery.inlining_done'));
					},
					error: function(e, x) {
						ed.setProgressState(false);
						alert(e);
					}
				});
			});

			ed.addButton('WPinlineStyles', {
				title : ed.getLang('dmdelivery.css_inliner'),
				image : url + '/img/css_inline.gif',
				cmd   : 'mceWPinlineStyles'
			});

		},

		/**
		 * Creates control instances based in the incomming name. This method is normally not
		 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
		 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
		 * method can be used to create those.
		 *
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {
			var t = this;
			switch (n) {
				case 'WPinsertField':
					var mlb = cm.createListBox('insertfieldmenu', {
						title: t.editor.getLang('dmdelivery.insert_field'),
						onselect: function(v) {
							if (v == '{$AcceptEmailBanner}') {
								v = '<img src="'+t.editor.theme.url+'/img/acceptemail_banner.gif" width="640" height="90" alt="AcceptEmail" />';
							}
							t.editor.execCommand('mceInsertContent',false,v);
							setTimeout(function(){mlb.selectByIndex(-1)}, 1000);
						}
					});

					var fields = t.editor.getParam('dmdelivery')['fields'];
					tinymce.each(fields, function(v, i) {
						mlb.add('{$'+v+'}', '{$'+v+'}');
					});

					// Return the new listbox instance
					return mlb;
			}
			return null;
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname : 'DMdelivery plugin',
				author : 'Web Power (Arjan Haverkamp)',
				authorurl : 'http://www.webpower.nl',
				infourl : 'http://www.webpower.nl',
				version : '2.2 (2011-05-13)'
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('WPdmdelivery', tinymce.plugins.WPDMdeliveryPlugin);
})();
