tinyMCE.addI18n('de.dmdelivery',{
	insert_field: 'Feld anlegen',
	load_url: 'Load URL',
	load_zip: 'Load ZIP file',
	link_titles: 'Link TITLEs',
	social_bookmarks: 'Social bookmarks',
	social_sharing: 'Social sharing',
	css_inliner: 'CSS inliner tool',
	no_style_tags: 'The HTML does not contain any <style> elements. Converting is not necessary.',
	inlining_done: 'The CSS has been converted to inline style tags.',
	social_tab_bookmark: 'Bookmarks',
	social_tab_social: 'Sharing',
	screen_too_small: 'Your screen is too small to show this resolution.\nI can only display'
});
