tinyMCE.addI18n('nl.dmdelivery',{
	insert_field: 'Voeg veld in',
	load_url: 'Laad URL',
	load_zip: 'Laad ZIP bestand',
	link_titles: 'Link TITLEs',
	social_bookmarks: 'Social bookmarks',
	social_sharing: 'Social sharing',
	css_inliner: 'CSS inliner tool',
	no_style_tags: 'De HTML bevat geen <style> elementen. Converteren is niet nodig.',
	inlining_done: 'De CSS is geconverteerd naar inline style tags.',
	social_tab_bookmark: 'Bookmarks',
	social_tab_social: 'Sharing',
	screen_too_small: 'Uw scherm is te klein om deze resolutie te kunnen tonen.\nIk kan slechts tonen'
});
