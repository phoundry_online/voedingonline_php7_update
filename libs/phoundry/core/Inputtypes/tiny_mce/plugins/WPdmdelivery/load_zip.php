<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once getenv('CONFIG_DIR') . '/PREFS.php';
	}

	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$RID = (int)$_GET['RID'];
	
	// Determine type (mailing, template or plugin):
	$sql = "SELECT name FROM phoundry_table WHERE id = {$TID}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$type = $db->FetchResult($cur,0,'name');

	$DMDcid  = $PHenv['DMDcid'];
	$orgName = $_FILES['zipFile']['name'];
	$phpName = $_FILES['zipFile']['tmp_name'];

	function arrayRegExp($arr, $regexp) {
		foreach($arr as $line) {
			if (!preg_match('/\.bak\.html$/', $line) && preg_match('/' . $regexp . '/i', $line)) {
				return $line;
			}
		}
		return false;
	}

	switch($type) {
		case 'mailing':
			$dir = "{$DMDprefs['mailingDir']}/{$DMDcid}/{$RID}";
			$url = "{$DMDprefs['mailingUrl']}/{$DMDcid}/{$RID}";
			break;
		case 'template':
			$dir = "{$DMDprefs['mailingDir']}/{$DMDcid}/tmpl/{$RID}";
			$url = "{$DMDprefs['mailingUrl']}/{$DMDcid}/tmpl/{$RID}";
			break;
		case 'plugin':
			$dir = "{$DMDprefs['mailingDir']}/{$DMDcid}/plugin/{$RID}";
			$url = "{$DMDprefs['mailingUrl']}/{$DMDcid}/plugin/{$RID}";
			break;
		case 'overall_plugin':
			$dir = "{$DMDprefs['mailingDir']}/overall/plugin/{$RID}";
			$url = "{$DMDprefs['mailingUrl']}/overall/plugin/{$RID}";
			break;
	}

	switch($_FILES['zipFile']['error']) {
		case UPLOAD_ERR_PARTIAL:
		case UPLOAD_ERR_NO_FILE:
			// No (or partial) file uploaded
			header('Location: load_zip.html?' . QS(0,'msg=' . word(1033)));
			exit;
		case UPLOAD_ERR_INI_SIZE:
			// File bigger than upload_max_filesize
			header('Location: load_zip.html?' . QS(0,'msg=' . word(1056)));
			exit;
	}

	// Create directory if it does not exist:
	if (!file_exists($dir)) {
		mkdir($dir, 0755, true);
	}

	// Move uploaded file:
	$zipName = "$dir/$orgName";
	move_uploaded_file($phpName, $zipName);
	chmod($zipName, 0644);

	$exec =  isset($PHprefs['paths']['unzip']) ? $PHprefs['paths']['unzip'] : 'unzip';
	$out  = trim(shell_exec("$exec -Z1 " . escapeshellarg($zipName)));
	$files = explode("\n", str_replace('\\', '/', $out));

	$baseDir = ''; $indexFile = null;
	$indexFiles = array('index\.html$', 'index\.htm$', 'default\.html$', 'default\.htm$', '\.html$', '\.htm$');
	foreach($indexFiles as $if) {
		$match = arrayRegExp($files, $if);
		if ($match !== false) {
			$indexFile = basename($match);
			$baseDir   = dirname($match);
			if (substr($baseDir, -1) == '/') {
				$baseDir = substr($baseDir, 0, -1);
			}
			break;
		}
	}

	if (is_null($indexFile)) {
		// ZIP does not contain a known index (.html) file:
		header('Location: load_zip.html?' . QS(0,'msg=' . word(1034)));
		exit;
	}

	// Unzip the ZIP file:
	$out = shell_exec("$exec -od " . escapeshellarg($dir) . ' ' . escapeshellarg($zipName));

	// Change permissions:
	foreach($files as $file) {
		@chmod($dir . '/' . $file, (substr($file,-1) == '/') ? 0755 : 0644);
	}

	// If ZIP-file contained directories, move the contents over to
	// the current working directory:
	if (!empty($baseDir)) {
		// Move files in subdirs to current directory ($dir):
		shell_exec('mv ' . escapeshellarg("{$dir}/{$baseDir}") . '/* ' . escapeshellarg($dir));
		// Remove top-level $baseDir recursively:
		$dirs = explode('/', $baseDir);
		shell_exec("rm -rf " . escapeshellarg("{$dir}/{$dirs[0]}"));
	}

	// Read index.html file into memory:
	$html = file_get_contents($dir . '/' . $indexFile);

	// If there's an index.txt file as well, read into memory:
	$plaintext = '';
	if (file_exists($dir . '/index.txt')) {
		$plaintext = file_get_contents($dir . '/index.txt');
	}

    // Try to find the charset of the HTML:
    $HTMLcharset = $PHprefs['charset'];
    if ( preg_match('/<meta\s+http-equiv[^;]+;\s*charset=([^">]+)/i', $html, $reg) ||
        preg_match('/<meta\s+charset=([^">]+)/i', $html) )
    {
        $HTMLcharset = strtolower($reg[1]);
    }
    if ($HTMLcharset != $PHprefs['charset'])
    {
        $html = preg_replace('/<meta\s+http-equiv="?content-type"?([^>]+)>/i', '', $html);
        $html = preg_replace('/<head>/i', "<head>\n" . '<meta http-equiv="content-type" content="text/html; charset=' . $PHprefs['charset'] .'" />', $html);
        $html = PH::iconv($HTMLcharset, "{$PHprefs['charset']}", $html);
        $plaintext = PH::iconv($HTMLcharset, "{$PHprefs['charset']}", $plaintext);
    }

	$html = DMD::fixUrlsInHtml($html, $url);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Upload ZIPfile</title>
	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
	<script type="text/javascript">

	window.onload = function() {
		var SF = document.forms[0], PF = parent.document.forms[0];

		tinyMCEPopup.editor.setContent(SF['html'].value);
		<?php if ($plaintext !== '') { ?>
		try {PF['plaintext_msg'].value = SF['plaintext'].value } catch (e) {};
		<?php } ?>
		setTimeout('tinyMCEPopup.close()', 10);
	}

	</script>
</head>
<body>
<form>
<input type="hidden" name="html" value="<?= PH::htmlspecialchars($html) ?>" />
<input type="hidden" name="plaintext" value="<?= PH::htmlspecialchars($plaintext) ?>" />
</form>
</body>
</html>
