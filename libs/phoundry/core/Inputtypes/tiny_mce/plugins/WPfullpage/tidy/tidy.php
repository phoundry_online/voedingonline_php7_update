<?php

function unicode_urldecode($url)
{
	preg_match_all('/%u([[:alnum:]]{4})/', $url, $a);

	foreach ($a[1] as $uniord)
	{
		$dec = hexdec($uniord);
		$utf = '';

		if ($dec < 128) {
			$utf = chr($dec); 
		}
		elseif ($dec < 2048) { 
			$utf = chr(192 + (($dec - ($dec % 64)) / 64)) . chr(128 + ($dec % 64)); 
		}
		else { 
			$utf = chr(224 + (($dec - ($dec % 4096)) / 4096)) . chr(128 + ((($dec % 4096) - ($dec % 64)) / 64)) . chr(128 + ($dec % 64)); 
		}

		$url = str_replace('%u'.$uniord, $utf, $url);
	}
	return urldecode($url);
}

function execProgram($command, $stdin, &$stdout, &$stderr) {
	$program = explode(' ', $command, 2);
	$program = $program[0];

	if (!file_exists($program) || !is_executable($program)) return false;

	$descriptorspec = array(
   	0 => array('pipe', 'r'), // stdin
   	1 => array('pipe', 'w'), // stdout
   	2 => array('pipe', 'w'), // stderr
	);

	$process = proc_open($command, $descriptorspec, $pipes);

	if (!is_resource($process)) {
		return false;
	}

	$txOff = 0; $txLen = strlen($stdin);
	$stdout = ''; $stdoutDone = false;
	$stderr = ''; $stderrDone = false;
	stream_set_blocking($pipes[0], false); // Make stdin/stdout/stderr non-blocking
	stream_set_blocking($pipes[1], false);
	stream_set_blocking($pipes[2], false);
	if ($txLen == 0) { fclose($pipes[0]); }
	while(true) {
		$rx = array(); // The program's stdout/stderr
		if (!$stdoutDone) { $rx[] = $pipes[1]; }
		if (!$stderrDone) { $rx[] = $pipes[2]; }
		$tx = array(); // The program's stdin
		if ($txOff < $txLen) { $tx[] = $pipes[0]; }
		stream_select($rx, $tx, $ex = null, null, null); // Block till r/w possible
		if (!empty($tx)) {
			$txRet = fwrite($pipes[0], substr($stdin, $txOff, 8192));
			if ($txRet !== false) { $txOff += $txRet; }
			if ($txOff >= $txLen) { fclose($pipes[0]); }
		}
		foreach ($rx as $r) {
			if ($r == $pipes[1]) {
				$stdout .= fread($pipes[1], 8192);
				if (feof($pipes[1])) { fclose($pipes[1]); $stdoutDone = true; }
			} else if ($r == $pipes[2]) {
				$stderr .= fread($pipes[2], 8192);
				if (feof($pipes[2])) { fclose($pipes[2]); $stderrDone = true; }
			}
		}
		if ($txOff >= $txLen && $stdoutDone && $stderrDone) break;
	}
	$exitCode = proc_close($process);

	return $exitCode;
}

header('Content-type: application/json; charset=utf-8'); // JSON Page; always utf-8

$args = json_decode(isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : implode("\r\n", file('php://input')));

$orgHTML = $html = trim(unicode_urldecode($args->params->html));

// Substitute (no)script/style blocks with placeholders:
preg_match_all('/<(noscript|script|style)([^>]+|)>([\s\S]*?)<\/\\1>/i', $html, $codeBlocks);
foreach(($codeBlocks = $codeBlocks[0]) as $i => $codeBlock) {
	$html = str_replace($codeBlock, '<!--[CODEBLOCK'.$i.']-->', $html);
}

$doctype = 'auto';
if (preg_match('/^<!doctype([^>]+)>/i', $html, $reg)) {
	// This page has a doctype:
	if (preg_match('/transitional/i', $reg[1])) {
		$doctype = 'transitional';
	}
	else if (preg_match('/strict/i', $reg[1])) {
		$doctype = 'strict';
	}
}

$indent = $args->params->indent ? '--indent auto --indent-spaces 1' : '';
$command = "./tidy -utf8 {$indent} --quiet true --wrap 0 --tidy-mark false --preserve-entities true --doctype {$doctype}";

$exitCode = execProgram($command, $html, $tidied, $stderr);

if ($exitCode === false) {
	// Cannot open process, revert to original HTML:
	$tidied = $orgHTML;
	$stderr = "Cannot proc_open $command";
	$exitCode = 2;
}

if ($exitCode > 1) {
	// Tidy generated errors, revert to original HTML:
	$tidied = $orgHTML;
}
else {
	// Put codeblocks back:
	foreach($codeBlocks as $idx=>$codeBlock) {
		$tidied = str_replace('<!--[CODEBLOCK'.$idx.']-->', $codeBlock, $tidied);
	}
}

print json_encode(array('result'=>array('html'=>trim($tidied), 'msgs'=>trim($stderr), 'exitCode'=>$exitCode)));

?>
