/**
 * @author Web Power (Arjan Haverkamp)
 * @copyright Copyright © 2004-2009, Web Power BV, All rights reserved.
 */

(function() {
	// Load plugin specific language pack
	tinymce.PluginManager.requireLangPack('WPphoundry');

	tinymce.create('tinymce.plugins.WPPhoundryPlugin', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
			//
			// Double-clicking images, links or rulers opens up their respective dialogs
			//
			ed.onDblClick.addToTop(function(ed, e) {
				if (e.shiftKey || (ed.plugins['WPeditable'] && !tinymce.activeRegion)) return;
				switch(e.target.nodeName.toUpperCase()) {
					case 'A':
						ed.execCommand(e.target.name?'mceInsertAnchor':'mceAdvLink',true,e.target);
						break;
					case 'IMG':
						ed.execCommand('mceAdvImage', true, false);
						break;
					case 'HR':
						ed.execCommand('mceAdvancedHr', true, false);
						break;
				}
				return tinymce.dom.Event.cancel(e);
			});

			ed.addCommand('mceWPcode', function() {
				ed.windowManager.open({
					file: url + '/source_editor.htm',
					width: 600,
					height:500,
					inline: true,
					resizable : true,
					maximizable : true
				},{
					plugin_url: url,
					indentCode: true
				})
			});

			ed.addButton('WPcode', {
				title : ed.getLang('phoundry.edit_html'),
				image : url + '/img/codemirror.gif',
				cmd   : 'mceWPcode'
			});

			ed.addCommand('mceWPsize', function() {
				ed.windowManager.open({
					url: url + '/size.htm',
					width: 300,
					height:200,
					inline: true
				},{
					plugin_url: url
				})
			});

			ed.addButton('WPsize', {
				title : ed.getLang('phoundry.stats'),
				image : url + '/img/stats.gif',
				cmd   : 'mceWPsize'
			});

			//
			// Firefox only:
			// There's a bug that style's for hyperlinks are not applied in Midas.
			// The workaround is to add !important to all CSS properties:
			//
			/*
			if (tinymce.isGecko) {
				ed.onSetContent.add(function(ed) {
					tinymce.each(ed.dom.select('a'), function(elm) {
						var css = elm.style.cssText, oldStyles, newStyles = [], s;
						if (css) {
							oldStyles = css.split(';');
							for (s = 0; s < oldStyles.length; s++) {
								if (oldStyles[s]) {
									newStyles.push(oldStyles[s] + (/\!\s*important/.test(oldStyles[s]) ? '' : ' !important'));
								}
							}
							elm.style.cssText = newStyles.join(';');
						}
					});
				});
			}
			*/
		},

		/**
		 * Creates control instances based in the incomming name. This method is normally not
		 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
		 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
		 * method can be used to create those.
		 *
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {
			return null;
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname : 'Phoundry plugin',
				author : 'Web Power (Arjan Haverkamp)',
				authorurl : 'http://www.webpower.nl',
				infourl : 'http://www.webpower.nl',
				version : '2.1 (2009-10-15)'
			};
		}

	});

	// Register plugin
	tinymce.PluginManager.add('WPphoundry', tinymce.plugins.WPPhoundryPlugin);
})();
