tinyMCEPopup.requireLangPack();

var SearchReplaceDialog = {
	lastPos: null,
	init : function(ed) {
		this.editor = ed;
		var f = document.forms[0];

		mcTabs.displayTab('replace_tab',  'replace_panel');

		f['replace_panel_searchstring'].value = tinyMCEPopup.getWindowArg('search_string');

		// Focus input field
		setTimeout(function(){f['replace_panel_searchstring'].focus();},0);

		this.lastSearch = '';
		this.codemirror = tinyMCEPopup.getWindowArg('codemirror');
	},

	// a='none', 'current' or 'all'
	searchNext : function(a) {
		var f = document.forms[0],
		    text = f['replace_panel_searchstring'].value,
		    rs = f['replace_panel_replacestring'].value,
          first = this.lastSearch == text, cursor;

		if (a == 'all')
		{
			var nrr = 0;
			cursor = this.codemirror.getSearchCursor(text, false);
			while (cursor.findNext()) {
				cursor.replace(rs);
				nrr++;
			}
			window.focus();
			if (nrr == 0) {
				alert(this.editor.getLang('phoundry_dlg.notfound'));
			}
			else {
				alert(nrr + ' ' + this.editor.getLang('phoundry_dlg.allreplaced'));
			}
			return;
		}

		if (a == 'current' &&  this.codemirror.getSelection() != '')
		{
			this.codemirror.replaceSelection(rs);
		}

		cursor = this.codemirror.getSearchCursor(text, this.lastPos);
		if (cursor.findNext()) {
			this.codemirror.setSelection(cursor.from(), cursor.to())
			//cursor.select();
			this.lastSearch = text;
			this.lastPos = cursor.to();
		}
		else {
			alert(this.editor.getLang('phoundry_dlg.notfound'));
			this.lastSearch = '';
		}
		window.focus();
	}
};

tinyMCEPopup.onInit.add(SearchReplaceDialog.init, SearchReplaceDialog);
