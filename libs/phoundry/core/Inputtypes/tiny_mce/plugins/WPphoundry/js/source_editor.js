// CAUTION: This Ajax function is only suitable for SYNCHRONOUS POST requests
var ajax = {
	x:function(){
		try{
			return new ActiveXObject('Msxml2.XMLHTTP');
		}
		catch(e) {
			try{
				return new ActiveXObject('Microsoft.XMLHTTP')
			}
			catch(e) {
				return new XMLHttpRequest()
			}
		}
	},
	
	send: function(u,f,a) {
		var x=ajax.x();
		x.open('POST',u,false /* non-async */);
		x.setRequestHeader('Content-type','application/x-www-form-urlencoded');
		x.send(a);
		f(x.responseText)
	}
};

var SourceCodeDialog = {
	searchPhrase : null,
	plugin_url: null,
	lastPos: null,

	preInit : function() {
		tinyMCEPopup.requireLangPack();
	},

   init : function(ed) {
		this.plugin_url = tinyMCEPopup.getWindowArg('plugin_url');
		ed.indentCode = tinyMCEPopup.getWindowArg('indentCode');
		this.editor = ed;
		this.cursorReplaced = false;
		var span = '<span class="Wp_CaReT" style="display:none">&#feff;</span>';
		try {// Safari barfs here sometimes...
			var doc = ed.dom.doc, selNode = ed.selection.getNode().nodeName;
			if (!/(BODY|HTML|TR|TBODY)/i.test(selNode)) {
				ed.selection.collapse(true);
				var r = ed.selection.getRng();
				if (r.insertNode) {
					if (r.createContextualFragment) {
						r.insertNode(r.createContextualFragment(span));
					}
					else { // IE9
						var frag = doc.createDocumentFragment(), temp = doc.createElement('span');
						frag.appendChild(temp);
						temp.outerHTML = span;
						r.insertNode(frag);
					}
				}
				else {
					r.pasteHTML(span);
				}
				this.cursorReplaced = true;
			}
		} catch(e) {};

		this.linenoEl = document.getElementById('lineno');
		this.charnoEl = document.getElementById('charno');

		var html = ed.getContent();
		ajax.send(this.plugin_url + '/cleanHTML.php', function(data) {
			eval('var json = '+data+';');
			SourceCodeDialog.content = json.html ? json.html : html;
		},'html='+encodeURIComponent(html));

		if (this.cursorReplaced) {
			this.content = this.content.replace(/<span\s+class="Wp_CaReT"([^>]*)>([^<]*)<\/span>/gm, String.fromCharCode(65279));
		}

		// Remove Wp_caret span element from tinyMCE:
		ed.dom.remove(ed.dom.select('.Wp_CaReT'));

		//document.getElementById('code').value = this.content;
		var CM = this.codeMirror = CodeMirror(document.body, {
			mode: 'application/x-httpd-php',
			value: this.content,
			lineNumbers: true,
			lineWrapping: true,
			indentUnit: 1,
			tabSize: 1,
			extraKeys: {'Tab':'insertTab'},
			matchBrackets: true,
			onCursorActivity: function() {
				var pos = CM.coordsChar(CM.cursorCoords(true)), scd = SourceCodeDialog;
				scd.linenoEl.value = pos.line+1;
				scd.charnoEl.value = pos.ch;
			}
		});
		setTimeout(function() {
			CM.refresh();
			CM.focus();
			if (SourceCodeDialog.cursorReplaced) {
				var cursor = CM.getSearchCursor(String.fromCharCode(65279), false);
    			if (cursor.findNext()) {
					CM.setCursor(cursor.to());
				}
			}
		}, 100);
	},

	saveContent : function() {
		var code = this.codeMirror.getValue();
		this.editor.setContent(code);
		setTimeout(function(){tinyMCEPopup.close()},200);
	},

	/*
	jump2line : function(line) {
		if (line && !isNaN(Number(line))) {
			var handle = this.codeMirror.nthLine(line);
			if (handle === false)
			{
				// z-index goes wrong without setTimeout...
				setTimeout(function(){tinyMCEPopup.alert(SourceCodeDialog.editor.getLang('phoundry_dlg.invalid_line')+'.')},0);
				return;
			}
			line = this.codeMirror.lineContent(handle);
			this.codeMirror.selectLines(handle, 0, handle, line.length);
		}
	},
	*/

	replace : function() {
		this.editor.windowManager.open({
			file :  this.plugin_url + '/searchreplace.htm',
			width : 420,
			height : 160,
			inline : !tinymce.isIE,
			close_previous : false
		}, {
			search_string : this.codeMirror.getSelection(),
			plugin_url: this.plugin_url,
			codemirror: this.codeMirror
		});
	},

	findNext : function() {
		if (!this.searchPhrase) return false;
		var cursor = this.codeMirror.getSearchCursor(this.searchPhrase, this.lastPos);
		var hasNext1 = cursor.findNext(), hasNext2 = false;
		if (!hasNext1) {
			// Re-search from top:
			cursor = this.codeMirror.getSearchCursor(this.searchPhrase);
			hasNext2 = cursor.findNext();
		}
		if (hasNext1 || hasNext2) { this.codeMirror.setSelection(cursor.from(), cursor.to())}
		document.getElementById('findStatus').innerHTML = (!hasNext1 && hasNext2) ? 'Continued from top' : '';
		if (!hasNext1 && !hasNext2) {
			alert(this.editor.getLang('phoundry_dlg.notfoundatall'));
		}
		this.lastPos = cursor.to();
	}
}

SourceCodeDialog.preInit();
tinyMCEPopup.onInit.add(SourceCodeDialog.init, SourceCodeDialog);
