<?php

$html = trim($_POST['html']);

if (!class_exists('Tidy')) {
	// No Tidy support available in PHP:
	$retval = array(
		'success' => true,
		'html' => $html,
		'error' => ''
	);

	header('Content-type: application/json');
	header('Expires: 0');
	print json_encode($retval);
	exit;
}

// Do we edit a complete HTML page (including <html> and </html> tags), or just a part?
$type = preg_match('/<html([^>]*)>/i', $html) ? 'html' : 'body';
if ($type == 'body') {
	// Tidy only handles complete pages.
	$html = '<html><head><title></title></head><body>' . $html . '</body></html>';
}

// Substitute (no)script/style blocks with placeholders:
preg_match_all('/<(noscript|script|style)([^>]+|)>([\s\S]*?)<\/\\1>/i', $html, $codeBlocks);
foreach(($codeBlocks = $codeBlocks[0]) as $i => $codeBlock) {
	$html = str_replace($codeBlock, '<!--[CODEBLOCK'.$i.']-->', $html);
}

// Does the HTML have a DOCTYPE?
$doctype = '';
if (preg_match('/<\!doctype([^>]+)>/i', $html, $reg)) {
	$doctype = $reg[0];
}

$config = array(
	'indent'              => true,
	'indent-spaces'       => 1,
	'output-xhtml'        => true,
	'wrap'                => 0,
	'new-blocklevel-tags' => 'article,aside,footer,header,hgroup,menu,nav,section,details,datagrid,video,audio',
	'new-inline-tags'     => 'mark,time,meter,progress',
	'new-empty-tags'      => 'command,source',
	'doctype'             => 'omit'
	//'preserve-entities' => true
);

// Tidy
$tidy = new tidy;
$tidy->parseString($html, $config, 'utf8');
$tidy->cleanRepair();

if ($type == 'html') {
	$tidied = $tidy->html();
}
else {
	$tidied = $tidy->body();
	$tidied = preg_replace('/<\/?body[^>]*>/i', '', $tidied);
}

// Re-insert DOCTYPE (if applicable):
if (!empty($doctype)) {
	$tidied = $doctype . "\n" . $tidied;
}

// Put codeblocks back:
foreach($codeBlocks as $idx=>$codeBlock) {
	$tidied = str_replace('<!--[CODEBLOCK'.$idx.']-->', $codeBlock, $tidied);
}

$retval = array(
	'success' => empty($tidy->errorBuffer),
	'html' => trim($tidied),
	'error' => $tidy->errorBuffer
);

header('Content-type: application/json');
header('Expires: 0');
print json_encode($retval);
