<?php
   $inc = @include_once('PREFS.php');
   if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
   }
   require_once "{$PHprefs['distDir']}/core/include/common.php";

	if (!isset($_GET['D'])) {
		error_log('D not defined in ' . __FILE__);
		exit();
	}
	$data = json_decode($_GET['D'], true);

	if (!array_key_exists('folder', $data)) {
		die('Permission denied!');
	}

	$folder = ltrim(PH::WPdecode($data['folder']), '/');

	$_SESSION['jsTree'] = array(
      'asciiOnly' => $PHprefs['charset'] != 'utf-8',
      'rootDir' => rtrim($PHprefs['uploadDir'], '/') . '/' .$folder,
      'rootUrl' => rtrim($PHprefs['uploadUrl'], '/') . '/' . $folder,
      'allowFiles' => array_key_exists('allowFiles', $data) ? $data['allowFiles'] : true,
      'allowFolders' => array_key_exists('allowFolders', $data) ? $data['allowFolders'] : true,
      'allowedWidth' => array_key_exists('allowedWidth', $data) ? $data['allowedWidth'] : null,
      'allowedHeight' => array_key_exists('allowedHeight', $data) ? $data['allowedHeight'] : null,
		'thumbnailWidth' => array_key_exists('thumbnailWidth', $data) ? $data['thumbnailWidth'] : null,
		'thumbnailHeight' => array_key_exists('thumbnailHeight', $data) ? $data['thumbnailHeight'] : null,
		'allowedSize' => array_key_exists('allowedSize', $data) ? $data['allowedSize'] : null,
		'selectExtensions' => array_key_exists('selectExtensions', $data) ? explode(',', $data['selectExtensions']) : array(),
		'uploadExtensions' => array_key_exists('uploadExtensions', $data) ? explode(',', $data['uploadExtensions']) : array(),
		'skipPattern' => array_key_exists('skipPattern', $data) ? $data['skipPattern'] : '/(^thumb_(.*)\.png$)|(^__edit__)/'
   );

	header("Location: {$PHprefs['url']}/core/jsTree/?mode=tiny_mce&lang={$PHSes->lang}&" . mt_rand(0,9999));
