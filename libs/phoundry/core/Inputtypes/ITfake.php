<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITfake extends Inputtype
{
	public function __construct(&$column)
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getHtml($page, $value = '', $readOnly = false, &$includes)
	{
		global $db;
		if(in_array($page, array('view', 'update', 'insert')) && !empty($value)) {
			$html  = '<fieldset class="field"><legend><b>'.
				htmlspecialchars($this->column->description).'</b></legend>';
			$html .= $value;
			$html .= "\n</fieldset>\n\n";
			
			return $html;
		}
		return '';
	}
	
	public function getValue($page, $value, $keyval = null)
	{
		global $db;
		$GLOBALS['RID'] = $keyval;
		
		if(in_array($page, array('search', 'view', 'update', 'insert'))) {
			return PH::evaluate($value);
		}
		return '';
	}

	public function getInputHtml($page, $value = '', &$includes)
	{
		return '';
	}
}
