<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITXmultipleselect extends Inputtype
{
	public $query, $help, $queryFields;

	public function __construct(&$column, $extra = null) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column, $extra);
		}
	}

	public function getValue($page, $keyValue, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				$selectedOptions = $this->column->datatype->getSelected($keyValue);
				$options = array();
				$GLOBALS['RECORD_ID'] = $keyValue;
				$sql = PH::evaluate($this->extra['query']);
				$cur = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				$y = 0;
				for ($x = 0; !$db->EndOfResult($cur); $x++) 
				{
					if (in_array($db->FetchResult($cur,$x,$this->extra['optionvalue']), $selectedOptions)) 
					{
						$option = '';
						foreach($this->extra['searchtext'] as $text) 
						{
							$option .= PH::langWord($db->FetchResult($cur,$x,$text)) . ' ';
						}
						$options[] = rtrim($option);
					}
				}
				return PH::htmlspecialchars(implode(', ', $options));

			default:
				return $keyValue;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;
		$alt = $this->column->getJScheckAltTag();
		$selected = $this->column->datatype->getSelected($value);
		$html = '<select multiple="multiple" size="' . $this->extra['size'] . '" name="' . $this->name . '[]" alt="' . $alt . '">';
		$sql = PH::evaluate($this->extra['query']);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$seld = in_array($db->FetchResult($cur,$x,$this->extra['optionvalue']), $selected) ? 'selected="selected"' : '';
			$html .= '<option ' . $seld . ' value="' . $db->FetchResult($cur,$x,$this->extra['optionvalue']) . '">';
			foreach($this->extra['optiontext'] as $text) {
				$html .= PH::htmlspecialchars(PH::langWord($db->FetchResult($cur,$x,$text))) . ' ';
			}
			$html .= '</option>';
		}
		$html .= '</select>';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $db, $PHSes, $PHprefs;

		$helpers = array('<a href="#" tabindex="1" onclick="selectAll(_D.forms[0][\'' . $this->name . '[]\']);return false">' . word(30) . '</a>'); 
		$mayAdd = false;

		if (empty($this->extra['addTable']))
			$mayAdd = false;
		elseif ($PHSes->groupId == -1)
			$mayAdd = true;
		else {
			// Make sure current user in current role may insert into $extra['addTable'];
			$sql = "SELECT rights FROM phoundry_group_table WHERE table_id = " . (int)$this->extra['addTable'] . " AND group_id = {$PHSes->groupId}";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur) && strpos($db->FetchResult($cur,0,'rights'), 'i') !== false)
				$mayAdd = true;
		}

		if ($mayAdd) {
			$url = $PHprefs['url'] . '/core/insert.php?' . QS(1,"TID={$this->extra['addTable']}&CID={$this->column->id}") . '&amp;noFrames&amp;callback=parent.newOption';
			$helpers[] = '<a href="' . $url . '" class="popupWin" title="' . ucfirst(word(6)) . '|600|400|0" onclick="callbackEl=_D.forms[0][\'' . $this->name . '[]\']">' . strtolower(word(6)) . '</a>';
		}
		return $helpers;
	}

	public function getExtra() 
	{
		global $db;
		
		$query = $queryFields = $addTable = null;
		$size = 6;
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
			if (isset($this->extra['addTable']))
				$addTable = $this->extra['addTable'];
			if (isset($this->extra['optionvalue'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
		}
		$adminInfo = array();
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the ' . substr(get_class($this),3) . ' (how many items are displayed at once)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query for the options in the ' . substr(get_class($this),3) . ' (<a href="#" onclick="makePopup(null,300,200,\'Select Query\',\'selectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);
		$adminInfo['queryFields'] = array(
			'name'     => 'ITqueryFields',
			'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
		);

		$tables = array();
		$sql = "SELECT id, name, description FROM phoundry_table WHERE show_in_menu NOT LIKE 'admin%' ORDER BY description";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$name = $db->FetchResult($cur,$x,'name');
			$tables[$db->FetchResult($cur,$x,'id')] = PH::langWord($db->FetchResult($cur,$x,'description')) . (!empty($name) ? ' (' . $name . ')' : '');
		}

		$adminInfo['addTable'] = array(
			'name'     => 'ITaddTable',
			'label'    => 'Add options table',
			'required' => false,
			'info'     => 'If you allow users to add options \'inline\', select the table to add options from.',
			'help'     => false,
			'input'    => array('type'=>'select', 'options'=>$tables, 'selected'=>$addTable)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
			$msg = 'Option value, option text and search text are not defined!';
			return false;
		}

		$struct = PH::javascript2php($_POST['ITqueryFields']);
		$extra = array(
			'query'     => $query,
			'addTable'  => (int)$_POST['ITaddTable'],
			'size'      => (int)$_POST['ITsize'],
			'optionvalue' => (string)$struct['optionvalue'],
			'optiontext' => $struct['optiontext'],
			'searchtext' => $struct['searchtext']
		);

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<h2>Regular SELECT query</h2>
<pre>
SELECT id, fields FROM table ORDER BY name
</pre>
<p>
The first field in the SELECT-part will be the VALUE of the OPTION. The other
fields will be showed as text to the user.<br />
</p>
<h2>Recursive SELECT query</h2>
<pre>
SELECT id,name,#parent# FROM category ORDER BY name
</pre>
<p>
This type of query is used when a table contains a reference to itself. This
is often seen in tables containing categories. The first field in the SELECT-part will be the VALUE of the OPTION. 
The last field, which has to be enclosed by
<tt>#</tt>, identifies the field that references to the primary key (id) in the
same table.
</p>
EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		//return str_replace(array("\r", "\n"), array('', '\\n'), $help);
		return $help;
	}
}
