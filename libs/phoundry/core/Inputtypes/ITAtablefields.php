<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITAtablefields extends Inputtype
{
	public  $value, $query, $queryFields;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;

		$alt = $this->column->getJScheckAltTag();
		$html = '<select name="' . $this->name . '" alt="' . $alt . '">';
		if (!$this->column->required)
			$html .= '<option value="">[' . word(57) . ']</option>';
		
		// What columns are available in the selected table?
		$filters = $this->column->getFilters();
		if (isset($filters['table_id'])) {
			$sql = "SELECT name FROM phoundry_table WHERE id = " . (int)$filters['table_id']['value'];
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
         $tableName = $db->FetchResult($cur,0,'name');
      }
		$db->ListTableFields($tableName, $DBcolnames)
			or trigger_error("Cannot list table fields for table $tableName: " . $db->Error(), E_USER_ERROR);
		foreach($DBcolnames as $colname) {
			$selected = ($colname == $value) ? ' selected="selected"' : '';
			$html .= '<option value="' . $colname . '"' . $selected . '>' . $colname . "</option>\n";
		}

		$html .= '</select>';
		return $html;
	}
}
