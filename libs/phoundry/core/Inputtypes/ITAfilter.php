<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITAfilter extends Inputtype
{
	public $value, $query, $queryFields;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}
	
	/**
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'search', 'insert', 'update' or 'preview'
	 */
	public function getValue($page, $value, $keyval = null)
	{
		global $db;
		$html = '';
		if ($page == 'update' || $page == 'copy')
			return $value;
		else
			return implode(' AND ', PH::getFilter($value));
	}
	
	public function getInputHtml($page, $parts = '', &$includes) 
	{
		global $db, $PHenv, $PHprefs;

		if ($parts === '') $parts = array();

		$html  = '<table class="sort-table" cellspacing="0">';
		$html .= '<tr><th></th><th>' . word(208) . '</th><th>' . word(209) . '</th><th>' . word(210) . '</th><th></th><th>' . word(208) . '</th><th>' . word(209) . '</th><th>' . word(210) . '</th><th></th></tr>';
		// What columns are available in the selected table?
		$filters = $this->column->getFilters();
		if (isset($filters['table_id'])) {
			$sql = "SELECT name FROM phoundry_table WHERE id = " . (int)$filters['table_id']['value'];
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$tableName = $db->FetchResult($cur,0,'name');
		}
		else {
			$tableName = $this->extra['table'];
		}

		// For DMdelivery:
		if (isset($PHprefs['product']) && $PHprefs['product'] == 'DMdelivery' && isset($PHenv['DMDcid'])) {
			$tableName = ($tableName == 'recipient_{$CAMPAIGN_ID}') ? $PHenv['rcptTable'] : str_replace('{$CAMPAIGN_ID}', $PHenv['DMDcid'], $tableName);
		}

		$db->ListTableFields($tableName, $DBcolnames)
			or trigger_error("Cannot list table fields for table $tableName: " . $db->Error(), E_USER_ERROR);
		$nrCols = count($DBcolnames);
		for ($x = 0; $x < $nrCols; $x++) {
			$html .= '<tr class="' . ($x & 1 ? 'even' : 'odd') . '">';
			$html .= '<td><b>(</b></td>';
			$html .= $this->getEntry($x . 'a', $DBcolnames, isset($parts[$x . 'a']) ? $parts[$x . 'a'] : array());
			$html .= '<td><b>OR</b></td>';
			$html .= $this->getEntry($x . 'b', $DBcolnames, isset($parts[$x . 'b']) ? $parts[$x . 'b'] : array());
			$html .= '<td nowrap="nowrap"><b>)';
			if ($x < $nrCols) {
				$html .= ' AND';
			}
			$html .= '</b></td>';
			$html .= "</tr>\n";
		}
		$html .= '</table>';

		return $html;
	}

	public function getEntry($prefix, $colNames, $parts) 
	{
		global $db;

		$sField = $sOperator = $sValue = '';
		if (count($parts)) {
			$sField    = $parts['fieldname'];
			$sOperator = $parts['operator'];
			$sValue    = $parts['value'];
		}
		$html = '';
		$html .= '<td><select name="' . $prefix . '_fieldName">';
		$html .= '<option value="">[select]</option>';
		foreach($colNames as $name) {
			$selected = ($name == $sField) ? ' selected="selected" ' : ' ';
			$html .= '<option' . $selected . 'value="' . $name . '">' . $name . '</option>';
		}
		$html .= '</select></td>';
		$html .=  '<td><select name="' . $prefix . '_operator">';
		$html .=  '<option value="">[select]</option>';
		$opts = array('<=','<','=','!=','>=','>','like','not like','in','not in');
		foreach($opts as $opt) {
			$selected = ($opt == $sOperator) ? ' selected="selected" ' : ' ';
			$html .= '<option' . $selected . 'value="' . PH::htmlspecialchars($opt) . '">' . PH::htmlspecialchars($opt) . '</option>';
		}
		$html .=  '</select></td>';
		$html .=  '<td><input type="text" name="' . $prefix . '_value" size="30" value="' . PH::htmlspecialchars($sValue) . '" /></td>';
		return $html;
	}

}
