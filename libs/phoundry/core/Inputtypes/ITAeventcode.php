<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

class ITAeventcode extends Inputtype
{
	public $size, $maxlength;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		$value = parent::getInputHtml($page, $value, $includes);
		$alt = $this->column->getJScheckAltTag();
		$html = '<div id="eventcodeDiv"></div>';
		$html .= '<textarea name="' . $this->name . '" alt="' . $alt . '" rows="' . $this->extra['rows'] . '" cols="' . $this->extra['cols'] . '">';
		if ($value !== '')
			$html .= PH::htmlspecialchars($value);
		$html .= '</textarea><br /><tt>}</tt>';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$helpers = array();
		if ($this->extra['preview']) {
			$url = $PHprefs['url'] . '/core/popups/preview.php?' . QS(1,"field={$this->name}&type=textarea");
	      $helpers[] = '<a href="' . $url . '" tabindex="1" class="popupWin" title="' . ucfirst(word(49)) . '|300|200|0">' . word(49) . '</a>';
		}
		return $helpers;
   }
	
	public function getExtra() 
	{
		// Structure of $extra:
		// $extra = array(
		//    'cols'=>20,
		//		'rows'=>10,
		//		'maxlength'=>50,
		//		'value'=>'my value',
		//    'preview'=>true|false
		// );

		$maxlength = isset($this->column->DBprops['length']) ? $this->column->DBprops['length'] : '';
		$cols      = 60;
		$rows      = 10;
		$preview   = false;
		if (count($this->extra)) {
			if (isset($this->extra['rows']))
				$rows = $this->extra['rows'];
			if (isset($this->extra['cols']))
				$cols = $this->extra['cols'];
			if (isset($this->extra['maxlength']))
				$maxlength = $this->extra['maxlength'];
			if (isset($this->extra['preview']))
				$preview = $this->extra['preview'] ? 1 : 0;
		}
		$adminInfo = array();
		$adminInfo['cols'] = array(
			'name'     => 'ITcols',
			'label'    => 'Columns',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the nr. of columns in the textarea',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$cols)
		);
		$adminInfo['rows'] = array(
			'name'     => 'ITrows',
			'label'    => 'Rows',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the nr. of rows in the textarea',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$rows)
		);
		$adminInfo['maxlength'] = array(
			'name'     => 'ITmaxlength',
			'label'    => 'Max. length',
			'required' => false,
			'syntax'   => 'integer',
			'info'     => 'Enter the maximum input-length of the textarea',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$maxlength)
		);
		$adminInfo['preview'] = array(
			'name'     => 'ITpreview',
			'label'    => 'HTML preview?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Does this textarea need a preview-link?',
			'input'    => array('type'=>'select', 'options'=>array(0=>'no',1=>'yes'), 'selected'=>$preview)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'cols'      => (int)$_POST['ITcols'],
			'rows'      => (int)$_POST['ITrows'],
			'maxlength' => $_POST['ITmaxlength'],
			'preview'   => ($_POST['ITpreview'] == 1) ? true : false
		);

		return $extra;
	}
}
