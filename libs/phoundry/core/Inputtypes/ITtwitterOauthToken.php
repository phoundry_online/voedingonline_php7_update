<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";
require_once "{$PHprefs['distDir']}/core/include/twitteroauth.php";

/**
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class ITtwitterOauthToken extends Inputtype
{

	/**
	 * Translate a database value into a human-readable format.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'search', 'insert', 'update' or 'view'
	 * @param $value(string) The database value
	 * @return A human-readable presentation of the database value
	 * @returns string
	 */
	public function getValue($page, $value, $keyval = null) 
	{
		$decoded = json_decode($value);
		switch($page) {
			case 'search':
			case 'view':
				return is_null($decoded) ? $value : $decoded->screen_name;
			default:
				return $value;
		}
	}

	public function getHtml($page, $value = '', $readOnly = false, &$includes)
	{
		global $DMDprefs;
		// No Twitter in China!
		if (isset($DMDprefs) && isset($DMDprefs['licenseOwner']) && $DMDprefs['licenseOwner'] == 'cn'){
			return '';
		}
		
		return parent::getHtml($page, $value, $readOnly, $includes);
	}

	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @param $includes(array) A reference to an array with this structure:
	 *                         'jsCode'=>array(),
	 *                         'jsFiles'=>array(),
	 *									'cssFiles'=>array()
	 *									Add entries to those arrays in order to have them
	 *									included in the <HEAD> of the edit-page.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db, $PHprefs;

		$url = $this->getAuthorizeUrl();
		$key = json_encode($this->extra['key']);
		$secret = json_encode($this->extra['secret']);
		$name = $this->name;
		
		$txtDel  = strtolower(word(8 /* delete */));
		$txtNone = '[' . strtolower(word(35 /* none */)) . ']';
		
		$success = escSquote(word(349 /* Authenticated successfully */));
		$includes['jsFiles'][strtolower(__CLASS__)] = $PHprefs['url'].
			"/core/csjs/jquery.json.min.js";
		
		$includes['jsCode'][] =<<<EOH
if (!window.ITtwitterOauthToken) {
	window.ITtwitterOauthToken = {};
}
window.ITtwitterOauthToken['{$name}'] = {
	success : function(token) {
		var parsed = $.parseJSON(token);
		parsed.application_key = ({$key});
		parsed.application_secret = ({$secret});
		$('#{$this->name}').val($.toJSON(parsed));
		$('#{$this->name}Scrn').html(parsed.screen_name + ': {$success}');
	}
};

$(document).ready(function() {
	$('#{$this->name}Remove').bind("click", function (event){
		event.preventDefault();
		$('#{$this->name}').val('');
		$('#{$this->name}Scrn').html('{$txtNone}');
		$(this).remove();
	});	
		
});

	

EOH;
		$updateHtml = '';
		$screenName = $txtNone;
		if (!empty($value)) {
			$json = json_decode($value);
			if (!is_null($json)) {

				// Verify that oauth token is still valid.
				//$updateHtml	= '<td><a id="' . $this->name . 'Remove" href="">[' . $txtDel . ']</a></td>';
				$vars			= json_decode($value);
				$conn			= new TwitterOAuth($vars->application_key, $vars->application_secret, $vars->oauth_token, $vars->oauth_token_secret);
				$parameters = array('user_id' => $vars->user_id);
				$res			= $conn->get('account/verify_credentials', $parameters);
				if (property_exists($res, 'error')) {
					$screenName = '[Authentication Revoked]';
					$value		= '';
				} else {
					$screenName = $json->screen_name;
				}
			}
		}

		$value = PH::htmlspecialchars($value);

		return <<<EOH
<input type="hidden" id="{$this->name}" name="{$this->name}" value="{$value}" />
<table><tr>
	<td><img class="ptr" border="0" src="{$PHprefs['url']}/core/pics/twitter_oauth.png" alt="Authenticate with Twitter" width="170" height="24" onclick="window.open('{$url}')" /></td>
	<td id="{$this->name}Scrn">{$screenName}</td>
</tr></table>
EOH;
	}
	
	public function getHelperLinks($page)
	{
		$helpers = array();
		$helpers[] = '<a href="#" tabindex="1" onclick="$(\'#' . $this->name . '\').val(\'\');$(\'#' . $this->name . 'Scrn\').html(\'[' . strtolower(word(35 /* none */)) . ']\');return false">' . word(161) . '</a>';
		return $helpers;
	}

	/**
	 * Returns admin info for building the GUI to configure this inputtype
	 * @return array
	 */
	public function getExtra()
	{
		$key = $secret = '';
		$updatable = 1;
		if (is_array($this->extra)) {
			if (isset($this->extra['key'])) {
				$key = (string) $this->extra['key'];
			}
			if (isset($this->extra['secret'])) {
				$secret = (string) $this->extra['secret'];
			}
			if (isset($this->extra['updatable'])) {
				$updatable = (int) $this->extra['updatable'];
			}
		}
		$adminInfo = array();
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['key'] = array(
			'name'		=> 'ITkey',
			'label'		=> 'Consumer key',
			'required'	=> true,
			'syntax'	=> 'text',
			'info'		=> 'Consumer key as given by https://twitter.com/apps/new',
			'input'		=> array('type'=>'text', 'size' => 50, 'value' => $key)
		);
		
		$adminInfo['secret'] = array(
			'name'     => 'ITsecret',
			'label'    => 'Consumer secret',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Consumer secret as given by https://twitter.com/apps/new',
			'input'    => array('type'=>'text', 'size'=>50, 'value'=>$secret)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';
		$extra = array(
			'key'     => trim($_POST['ITkey']),
			'secret'	=> trim($_POST['ITsecret']),
			'updatable' => $_POST['ITupdatable'] == 1
		);

		return $extra;
	}

	/**
	 * Tells if the field can be altered after insertion
	 * 
	 * @return bool
	 */
	private function _updatable()
	{
		if (isset($this->extra['updatable'])) {
			return $this->extra['updatable'];
		}
		return true;
	}
	
	private function getAuthorizeUrl()
	{
		global $PHprefs;
		return '/'.trim($PHprefs['url'], '/').
			'/core/Inputtypes/twitterOauthToken/authenticate.php?'.
			http_build_query(array('name' => $this->name,
			'key' => $this->extra['key'],
			'secret' => $this->extra['secret']));
	}
}
