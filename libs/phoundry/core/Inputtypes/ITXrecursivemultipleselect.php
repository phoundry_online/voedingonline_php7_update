<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";
require_once "{$PHprefs['distDir']}/core/include/sqlParser/Parser.php";
require_once "{$PHprefs['distDir']}/core/include/sqlParser/Compiler.php";

class ITXrecursivemultipleselect extends Inputtype
{
	public $query, $help, $queryFields;

	public function __construct(&$column) {
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}

		if (isset($this->extra['query']) && isset($this->extra['parent'])) {
			$this->initQuery();
		}
	}

	private function initQuery()
	{
		// Insert 'WHERE parent IS NULL' into query:
		$parser = new SQL_Parser();
		$sqlStruct = $parser->parse($this->extra['query']);

		$insWhere = array(
			'arg_1' => array('value'=>$this->extra['parent'], 'type'=>'ident'),
			'op' => 'is',
			'arg_2' => array('value'=>'', 'type'=>'null')
		);

		if (!isset($sqlStruct['where_clause']))
			$sqlStruct['where_clause'] = $insWhere;
		else {
			$sqlStruct['where_clause'] = array(
				'arg_1' => $insWhere,
				'op' => 'and',
				'arg_2' => $sqlStruct['where_clause']
			);
		}

		$compiler = new SQL_Compiler();
		$this->nullQuery = $compiler->compile($sqlStruct);

		$this->fromTable = $sqlStruct['table_names'][0];
	}

	public function getValue($page, $keyValue, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				$GLOBALS['RECORD_ID'] = $keyValue;
				$sql = PH::evaluate($this->extra['query']);
				$selectedOptions = $this->column->datatype->getSelected($keyValue);
				$options = array();
				$cur = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				$y = 0;
				for ($x = 0; !$db->EndOfResult($cur); $x++) {
					if (in_array($db->FetchResult($cur,$x,$this->extra['optionvalue']), $selectedOptions)) {
						$option = '';
						foreach($this->extra['searchtext'] as $text)
							$option .= PH::langWord($db->FetchResult($cur,$x,$text)) . ' ';
						$options[] = rtrim($option);
					}
				}
				return implode(', ', $options);

			default:
				return $keyValue;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;

		$selected = $this->column->datatype->getSelected($value);
		$alt = $this->column->getJScheckAltTag();
		$html = '<select multiple="multiple" size="' . $this->extra['size'] . '" name="' . $this->name . '[]" alt="' . $alt . '">';

		$html .= $this->getOptions($page, $selected);

		$html .= '</select>';
		return $html;
	}


	/**
	 * Return the OPTION's for a recursive SELECT.
	 *
	 * @author Arjan Haverkamp
	 * @param $selected(array) An array of selected OPTION's.
	 * @param $curid(string) The current value for the key.
	 * @param $depth(int) The current depth of the recursive SELECT.
	 * @return A string containing HTML-code for a recursive SELECT.
	 * @returns string
	 * @private
	 */
	public function getOptions($page, $selected, $curid = '', $depth = 0) 
	{
		global $db;

		$keyval = ($page == 'update') ? $_GET['RID'] : '';

		$html = '';
		$sql = $this->nullQuery;
		if ($curid !== '')
			$sql = str_replace('is null', '= ' . (int)$curid, $sql);
		$sql = PH::evaluate($sql);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$nextid = $db->FetchResult($cur,$x,$this->extra['optionvalue']);
			if (!($this->fromTable == $this->column->tableName && $keyval == $nextid)) {
				$html .= '<option value="' . $nextid . '"' . (in_array($nextid, $selected) ? ' selected="selected"' : '') . '>';
				for ($y = 0; $y < $depth; $y++) $html .= '-';
				foreach($this->extra['optiontext'] as $text) {
					$html .= PH::htmlspecialchars(PH::langWord($db->FetchResult($cur,$x,$text))) . ' ';
				}
				$html .= "</option>\n";
			}
			$html .= $this->getOptions($page, $selected, $nextid, $depth+1);
		}

		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $db, $PHSes, $PHprefs;

		$helpers = array('<a href="#" tabindex="1" onclick="selectAll(_D.forms[0][\'' . $this->name . '[]\']);return false">' . word(30) . '</a>'); 
		$mayAdd = false;

		if (empty($this->extra['addTable']))
			$mayAdd = false;
		elseif ($PHSes->groupId == -1)
			$mayAdd = true;
		else {
			// Make sure current user in current role may insert into $extra['addTable'];
			$sql = "SELECT rights FROM phoundry_group_table WHERE table_id = " . (int)$this->extra['addTable'] . " AND group_id = {$PHSes->groupId}";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur) && strpos($db->FetchResult($cur,0,'rights'), 'i') !== false)
				$mayAdd = true;
		}

		if ($mayAdd) {
			$url = $PHprefs['url'] . '/core/insert.php?' . QS(1,"TID={$this->extra['addTable']}&CID={$this->column->id}") . '&amp;noFrames&amp;callback=parent.newOption';
			$helpers[] = '<a href="' . $url . '" class="popupWin" title="' . ucfirst(word(6)) . '|600|400|0" onclick="callbackEl=_D.forms[0][\'' . $this->name . '[]\']">' . strtolower(word(6)) . '</a>';
		}
		return $helpers;
	}

	public function getExtra() 
	{
		global $db;
		
		$query = $queryFields = $addTable = null;
		$size = 6;
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
			if (isset($this->extra['addTable']))
				$addTable = $this->extra['addTable'];
			if (isset($this->extra['optionvalue'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'parent'      => $this->extra['parent'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
		}
		$adminInfo = array();
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the ' . substr(get_class($this),3) . ' (how many items are displayed at once)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query for the options in the ' . substr(get_class($this),3) . ' (<a href="#" onclick="makePopup(null,300,200,\'Select Query\',\'recursiveSelectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);
		$adminInfo['queryFields'] = array(
			'name'     => 'ITqueryFields',
			'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
		);

		$tables = array();
		$sql = "SELECT id, name, description FROM phoundry_table WHERE show_in_menu NOT LIKE 'admin%' ORDER BY description";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$name = $db->FetchResult($cur,$x,'name');
			$tables[$db->FetchResult($cur,$x,'id')] = PH::langWord($db->FetchResult($cur,$x,'description')) . (!empty($name) ? ' (' . $name . ')' : '');
		}

		$adminInfo['addTable'] = array(
			'name'     => 'ITaddTable',
			'label'    => 'Add options table',
			'required' => false,
			'info'     => 'If you allow users to add options \'inline\', select the table to add options from.',
			'help'     => false,
			'input'    => array('type'=>'select', 'options'=>$tables, 'selected'=>$addTable)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
			$msg = 'Option value, option text and search text are not defined!';
			return false;
		}

		$struct = PH::javascript2php($_POST['ITqueryFields']);
		$extra = array(
			'query'     => $query,
			'addTable'  => (int)$_POST['ITaddTable'],
			'size'      => (int)$_POST['ITsize'],
			'optionvalue' => (string)$struct['optionvalue'],
			'parent'      => trim($struct['parent']),
			'optiontext' => $struct['optiontext'],
			'searchtext' => $struct['searchtext']
		);

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<p>
You need to define a 'lookup' query, some SQL that generates a hierarchical
list of options. Make sure the <tt>SELECT</tt> clause of the query contains
both the record-id and the parent-id, and at least one other field to display
in the option tree as the option-text.
</p>
<pre>
SELECT id, name, parent_id FROM site_menu ORDER BY order_by
</pre>
<p>
Do not forget to click 'edit' in order to determine what fields in the
query defined here mean what!

EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		return $help;
	}
}
