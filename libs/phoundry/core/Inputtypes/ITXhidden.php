<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/Inputtypes/ITXmultipleselect.php";

class ITXhidden extends ITXmultipleselect
{
	public $query, $help, $queryFields;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column))
			parent::__construct($column);
	}

	// Same as IThidden->getHTML
	public function getHtml($page, $value = '', $readOnly = false, &$includes) 
	{
		if ($page == 'view' || $page == 'pdf') {
			if ($value === '' || is_null($value))
				$value = '&nbsp;';
			$html  = '<fieldset class="field"><legend><b>' . PH::htmlspecialchars($this->column->description) . '</b></legend>';
			$html .= $value;
			$html .= "\n</fieldset>\n\n";
			return $html;
		}
		else
			return $this->getInputHtml($page, PH::evaluate($value), $includes);
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;
		$alt = $this->column->getJScheckAltTag();
		$selected = $this->column->datatype->getSelected($value);

		$html = '';
		foreach($selected as $sel) {
			$sel = PH::evaluate($sel);
			/*
			if(preg_match('/^{\$.*}$/', $sel)) {
				$sel = PH::evaluate($sel);
			}
			*/
			$html .= '<input type="hidden" name="' . $this->name . '[]" alt="' . $alt . '" value="' . $sel . '" />';
		}
		return $html;
	}

	public function getExtra() 
	{
		global $db;

		$query = $queryFields = $addTable = null;
		if (count($this->extra)) {
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
			if (isset($this->extra['optionvalue'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
		}
		$adminInfo = array();
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query for the options in the ' . substr(get_class($this),3) . ' (<a href="#" onclick="makePopup(null,300,200,\'Select Query\',\'selectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);
		$adminInfo['queryFields'] = array(
			'name'     => 'ITqueryFields',
			'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
			$msg = 'Option value, option text and search text are not defined!';
			return false;
		}

		$struct = PH::javascript2php($_POST['ITqueryFields']);
		$extra = array(
			'query'     => $query,
			'optionvalue' => (string)$struct['optionvalue'],
			'optiontext' => $struct['optiontext'],
			'searchtext' => $struct['searchtext']
		);

		return $extra;
	}
}
