<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once "{$PHprefs['distDir']}/core/include/twitteroauth.php";

if (!isset($_GET['key'], $_GET['secret'], $_GET['name'])) {
	throw new Exception("Invalid call, missing key or secret.");
}

$name = $_GET['name'];
$key = $_GET['key'];
$secret = $_GET['secret'];

$callback = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http').
	'://'.$_SERVER['HTTP_HOST'].'/'.trim($PHprefs['url'], '/').
	'/core/Inputtypes/twitterOauthToken/?name='.$_GET['name'];

$conn = new TwitterOAuth($key, $secret);

$request = $conn->getRequestToken($callback);

if ($conn->http_code != 200) {
	throw new Exception("Could not connect to Twitter.");
}

if (!isset($_SESSION['twitterOauthToken'])) {
	$_SESSION['twitterOauthToken'] = array();
}

$_SESSION['twitterOauthToken'][$name] = array(
	'key' => $key,
	'secret' => $secret,
	'request_token' => $request
);
$url = $conn->getAuthorizeURL($request['oauth_token']);

PH::trigger_redirect($url);
