<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once "{$PHprefs['distDir']}/core/include/twitteroauth.php";

if (!isset($_GET['name'])) {
	PH::phoundryErrorPrinter("Malformed callback url", true);
}

$name = $_GET['name'];
if (!isset($_SESSION['twitterOauthToken'][$name]['request_token']['oauth_token'],
		$_SESSION['twitterOauthToken'][$name]['request_token']['oauth_token'])) {
	PH::phoundryErrorPrinter("Session expired", true);
}

$oauth_token = $_SESSION['twitterOauthToken'][$name]['request_token']['oauth_token'];
$oauth_token_secret = $_SESSION['twitterOauthToken'][$name]['request_token']['oauth_token_secret'];

/* If the oauth_token is old redirect to the connect page. */
if (isset($_REQUEST['oauth_token']) && $oauth_token !== $_REQUEST['oauth_token']) {
  unset($_SESSION['twitterOauthToken'][$name]);
  PH::phoundryErrorPrinter("Session expired.", true);
}

$conn = new TwitterOAuth($_SESSION['twitterOauthToken'][$name]['key'],
	$_SESSION['twitterOauthToken'][$name]['secret'], $oauth_token, $oauth_token_secret);

$access_token = $conn->getAccessToken($_REQUEST['oauth_verifier']);

if (!isset($access_token['oauth_token'], $access_token['oauth_token_secret'])) {
	PH::phoundryErrorPrinter("Twitter replied with  bad response (over capacity?). Please try again.", true);
}
unset($_SESSION['twitterOauthToken'][$name]['request_token']);
?>
<script type="text/javascript">
if (window.opener) {
	// Make it an object (instead of a string) -> double json_encode:
	window.opener.ITtwitterOauthToken[(<?=json_encode($name)?>)].success(<?php echo json_encode(json_encode($access_token)); ?>);
	window.close();
}
</script>
