<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/Inputtypes/ITXmultipleselect.php";

class ITXbigmultipleselect extends ITXmultipleselect
{
	public $query, $help, $queryFields;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;
		$alt = $this->column->getJScheckAltTag();
		$selected = $this->column->datatype->getSelected($value);
		$html = '<select multiple="multiple" size="' . $this->extra['size'] . '" name="' . $this->name . '[]" alt="' . $alt . '">';
		$sql = PH::evaluate($this->extra['query']);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			if (in_array($db->FetchResult($cur,$x,$this->extra['optionvalue']), $selected)) {
				$html .= '<option selected="selected" value="' . $db->FetchResult($cur,$x,$this->extra['optionvalue']) . '">';
				foreach($this->extra['optiontext'] as $text) {
					$html .= PH::htmlspecialchars(PH::langWord($db->FetchResult($cur,$x,$text))) . ' ';
				}
				$html .= '</option>';
			}
		}
		$html .= '</select>';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $db, $PHSes, $PHprefs;

		$url = $PHprefs['url'] . '/core/popups/bigITsearch.php?' . QS(1,"CID={$this->column->id}");
		$helpers = array('<a href="' . $url . '" tabindex="1" class="popupWin" title="' . ucfirst(word(10)) . '|400|240|0" onclick="callbackEl=_D.forms[0][\'' . $this->name . '[]\']">' . strtolower(word(10)) . '</a>'); 
		$mayAdd = false;

		if (empty($this->extra['addTable']))
			$mayAdd = false;
		elseif ($PHSes->groupId == -1)
			$mayAdd = true;
		else {
			// Make sure current user in current role may insert into $extra['addTable'];
			$sql = "SELECT rights FROM phoundry_group_table WHERE table_id = " . (int)$this->extra['addTable'] ." AND group_id = {$PHSes->groupId}";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur) && strpos($db->FetchResult($cur,0,'rights'), 'i') !== false)
				$mayAdd = true;
		}

		if ($mayAdd) {
			$url = $PHprefs['url'] . '/core/insert.php?' . QS(1,"TID={$this->extra['addTable']}&CID={$this->column->id}") . '&amp;noFrames&amp;callback=parent.newOption';
			$helpers[] = '<a href="' . $url . '" class="popupWin" title="' . ucfirst(word(6)) . '|600|400|0" onclick="callbackEl=_D.forms[0][\'' . $this->name . '[]\']">' . strtolower(word(6)) . '</a>';
		}
		return $helpers;
	}
}
