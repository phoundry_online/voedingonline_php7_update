<?php
global $PHprefs;
	
if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/Inputtypes/ITtext.php";

class ITcalendar extends ITtext
{
	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!isset($this->extra['size'])) {
			if (isset($column->DBprops['type'])) {
				$this->extra['size'] = $this->extra['maxlength'] = $column->DBprops['type'] == 'datetime' ? 16 : 10;
			}
			else {
				$this->extra['size'] = $this->extra['maxlength'] = 10;
			}
		}
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

   /**
    * Get a HTML-representation of this inputtype.
    *
    * @author Arjan Haverkamp
    * @param $page(string) The page (update, insert, copy) to get the fieldset for
    * @param $value(string) The value for the inputtype.
    * @return HTML-code representing this inputtype.
    * @returns string
    */
   public function getInputHtml($page, $value = '', &$includes) 
	{
		global $PHSes, $PHprefs;

		$includes['cssFiles']['datepicker'] = "{$PHprefs['url']}/layout/css/popupcal.css";
		$includes['jsFiles'][strtolower(get_class($this))] = "{$PHprefs['url']}/core/csjs/popupcal.js.php";

		$datePattern = word(251);
		if (strtolower(get_class($this->column->datatype)) == 'dtdate_iso') {
			$datePattern = 'YYYY-MM-DD';
		}
		elseif (strtolower(get_class($this->column->datatype)) == 'dtdatetime') {
			$datePattern .= ' hh:mm';
		}

		$weekday = '';
		if ($value === '' && $page == 'insert') {
			$value = $this->column->datatype->getDefaultValue();
		}
		if ($value !== '')
		{
			// Determine day:
			$d = (int)substr($value,strpos($datePattern, 'DD'),2);
			$m = (int)substr($value,strpos($datePattern, 'MM'),2);
			$y = (int)substr($value,strpos($datePattern, 'YYYY'),4);
			$weekday = (int)date('w', strtotime("{$y}-{$m}-{$d}"));
			$weekday = ($weekday == 0) ? word(262) : word(255+$weekday);
		}

		$html = '<span id="' . $this->name . '_weekday">' . $weekday . '</span>&nbsp;';
      if (isset($this->extra['updatable']) && $this->extra['updatable'] == false && $page == 'update') {
         $html .= '<input name="' . $this->name . '" type="hidden" value="' . $value . '" onchange="popUpCal.getWeekday(this)" /><b>' . $value . '</b>';
      }
      else {
         $alt = $this->column->getJScheckAltTag();
         $html .= '<input data-pattern="' . $datePattern . '" class="popupcal" name="' . $this->name . '" type="text" alt="' . $alt . '" size="' . $this->extra['size'] . '" onchange="popUpCal.getWeekday(this)" value="' . $value . '"';
         if ($this->maxlength != -1)
            $html .= ' maxlength="' . $this->maxlength . '"';
         $html .= ' />';
      }
      return $html;
	}

	public function getExtra() 
	{
		// Structure of $extra:
		// $extra = array(
		//    'size'=>20,
		//		'maxlength'=>50
		// );

		$maxlength = $size = 10;
		$updatable = true;
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['maxlength']))
				$maxlength = $this->extra['maxlength'];
			if (isset($this->extra['updatable']))
				$updatable = $this->extra['updatable'];
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['maxlength'] = array(
			'name'     => 'ITmaxlength',
			'label'    => 'Max. length',
			'required' => false,
			'syntax'   => 'integer',
			'info'     => 'Enter the maximum input-length of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$maxlength)
		);

		return $adminInfo;
	}
}
