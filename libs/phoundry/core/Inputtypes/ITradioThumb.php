<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

/**
 * 
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class ITradioThumb extends Inputtype
{
	/**
	 * 
	 * @param string $page
	 * @param mixedvar $value
	 */
	public function getValue($page, $value, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				$html = '';
				$sql = PH::evaluate($this->_recordQuery());
				if (preg_match('/^(SELECT|SHOW)/i', $sql)) {
					// Database query:
					$cur = $db->Query($sql) 
						or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
					for ($x = 0; !$db->EndOfResult($cur); $x++) {
						if ($db->FetchResult($cur,$x,$this->extra['optionvalue']) == $value) {
							foreach($this->extra['searchtext'] as $text)
								$html .= PH::langWord($db->FetchResult($cur,$x,$text)) . ' ';
							break;
						}
						$html = trim($html);
					}
				}
				else {
					// Pipe-separated option=value pairs:
					$pairs = explode('|', $sql);
					foreach($pairs as $pair) {
						$pair = explode('=', $pair, 2);
						if ($pair[0] == $value) {
							$html .= PH::langWord($pair[1]);
							break;
						}
					}
				}
				return $html;

			default:
				return $value;
		}
	}

	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @param $includes(array) A reference to an array with this structure:
	 *                         'jsCode'=>array(),
	 *                         'jsFiles'=>array(),
	 *									'cssFiles'=>array()
	 *									Add entries to those arrays in order to have them
	 *									included in the <HEAD> of the edit-page.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;

		// Get default value:
		$value = parent::getInputHtml($page, $value, $includes);

		if (!$this->_updatable() && $page == 'update') {
			return '<input name="'.$this->name.'" type="hidden" value="'.
				$value.'" /><b>'.$this->getValue('view', $value).'</b>';
		}
		
		$alt = $this->column->getJScheckAltTag();
		$html = '';
		$sql = PH::evaluate($this->_editQuery());
		// Database query:
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			
		$options = array();
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$title = array();
			foreach($this->extra['optiontext'] as $text) {
				$title[] = PH::langWord($db->FetchResult($cur,$x,$text)).' ';
			}
			
			$options[] = $this->_optionHtml(
				"{$this->name}_{$x}",
				$value,
				(!$x) ? $alt : "",
				$db->FetchResult($cur,$x,$this->extra['optionvalue']),
				implode("", $title)
			);
		}
		
		return implode("", $options);
	}

	/**
	 * Returns admin info for building the GUI to configure this inputtype
	 * @return array
	 */
	public function getExtra()
	{
		$query = null;
		$queryFields = null;
		$imgUrl = "";
		$updatable = true;
		if (is_array($this->extra) && count($this->extra)) {
			if (isset($this->extra['query'])) {
				$query = (string)$this->extra['query'];
			}
			if (isset($this->extra['optionvalue'], $this->extra['optiontext'], $this->extra['searchtext'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
			if (isset($this->extra['imgUrl'])) {
				$imgUrl = $this->extra['imgUrl']; 
			}
			if (isset($this->extra['updatable'])) {
				$updatable = (bool) $this->extra['updatable'];
			}
		}
		$adminInfo = array();
		$updatable = (int) $updatable;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['imgUrl'] = array(
			'name'		=> 'ITimgUrl',
			'label'		=> 'The url linking to the images',
			'required'	=> true,
			'syntax'	=> 'text',
			'info'		=> 'Enter the url to the images. {$option} is replaced by the option and {$title} by the options title',
			'input'		=> array('type'=>'text', 'size' => 50, 'value' => $imgUrl)
		);
		
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query for the different radio-buttons (<a href="#" onclick="makePopup(null,300,200,\'Select Query\',\'selectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);
		$adminInfo['queryFields'] = array(
			'name'     => 'ITqueryFields',
			//'required' => true,
			//'syntax'   => 'text',
			'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (preg_match('/^(SELECT|SHOW)/i', $query)) {
			if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
				$msg = 'Option value, option text and search text are not defined!';
				return false;
			}
		}

		$struct = PH::javascript2php($_POST['ITqueryFields']);
		$extra = array(
			'query'     => $query,
			'imgUrl'	=> trim($_POST['ITimgUrl']),
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false
		);
		if (isset($struct['optionvalue']))
			$extra['optionvalue'] = (string) $struct['optionvalue'];
		if (isset($struct['optiontext']))
			$extra['optiontext'] = $struct['optiontext'];
		if (isset($struct['searchtext']))
			$extra['searchtext'] = $struct['searchtext'];

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<h2>name=value pairs</h2>
<pre>
green=I like green|red=I like red|blue=I like blue
</pre>
<p>
In this case, the OPTIONs don't come from the database, but they are predefined.
The syntax is <tt>value=name</tt>, the OPTIONs are separated by a pipe. The
<tt>value</tt> is the VALUE for the OPTION, everything on the right of
the =-sign will be showed to the user.
</p>

<h2>Regular SELECT query</h2>
<pre>
SELECT id, fields FROM table
  [WHERE id = {\$RECORD_ID}] ORDER BY name
</pre>
<p>
The WHERE-part (between square brackets) is used for identifying the current
record (on the search-page and the update-page). The brackets are required!
The {\$RECORD_ID} variable will be filled with the correct value by Phoundry.
</p>
<h2>Variables</h2>
<table>
<tr>
	<th>Variable</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$USER_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$USER_NAME}</td>
	<td>The full name (field <tt>name</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="odd">
	<td>{\$GROUP_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_group</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$check}</td>
	<td>A checkmark (<img src="../../core/icons/tick.png" alt="Yes" width="16" height="16" />)</td>
</tr>
<tr class="odd">
	<td>{\$nocheck}</td>
	<td>A no-checkmark (<img src="../../core/icons/cross.png" alt="No" width="16" height="16" />)</td>
</tr>
<tr class="even">
	<td>{\$word(nr)}</td>
	<td>A word from the language file with index 'nr'.</td>
</tr>
</table>
<p>
Any other variable may be used as well. In that case, Phoundry will look in the
\$_REQUEST array: if a value for the variable is found there, it will be
substituted.
</p>
EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		return $help;
	}
	
	/**
	 * Tells if the field can be altered after insertion
	 * 
	 * @return bool
	 */
	private function _updatable()
	{
		if (isset($this->extra['updatable'])) {
			return $this->extra['updatable'];
		}
		return true;
	}
	
	/**
	 * Creates the thumb url
	 * 
	 * @param mixedvar $value
	 */
	private function _thumbUrl($value)
	{
		$url = "";
		if (isset($this->extra['imgUrl'])) {
			$url = $this->extra['imgUrl'];
			$url = PH::evaluate($url);
			$url = str_replace('{$option}', $value, $url);
		}
		return $url;
	}
	
	/**
	 * Query to fetch the records
	 * 
	 * @return string
	 */
	private function _recordQuery()
	{
		$recordQuery = "";
		if (isset($this->extra['query'])) {
			$query = $this->extra['query'];
			if (preg_match('/^(.*)\[[^\]]+\](.*)$/s', $query, $reg)) {
				// If the query contains [...] (optional WHERE part), split
				// the query into two variants: one with the WHERE part and
				// one without it.
				if (preg_match_all('/<\?(?:php)?(.+)\?>/Us', $query, $matches)) {
					foreach($matches[0] as $key => $match) {
						$query = str_replace($match, '#'.$key, $query);
					}
				}
				$recordQuery = str_replace(str_split("[]"), "", $query);
				if (isset($matches)) {
					foreach($matches[0] as $key => $match) {
						$recordQuery = str_replace('#'.$key, $match, $recordQuery);
					}
				}
			}
			else {
				$recordQuery = $query;
			}
		}
		return $recordQuery;
	}
	
	/**
	 * Query to fetch the available options for the edit view
	 * 
	 * @return string
	 */
	private function _editQuery()
	{
		$editQuery = "";
		if (isset($this->extra['query'])) {
			$query = $this->extra['query'];
			if (preg_match('/^(.*)\[[^\]]+\](.*)$/s', $query, $reg)) {
				// If the query contains [...] (optional WHERE part), split
				// the query into two variants: one with the WHERE part and
				// one without it.
				if (preg_match_all('/<\?(?:php)?(.+)\?>/Us', $query, $matches)) {
					foreach($matches[0] as $key => $match) {
						$query = str_replace($match, '#'.$key, $query);
					}
				}
				$editQuery = $reg[1] . $reg[2];
				if (isset($matches)) {
					foreach($matches[0] as $key => $match) {
						$editQuery = str_replace('#'.$key, $match, $editQuery);
					}
				}
			}
			else {
				$editQuery = $query;
			}
		}
		return $editQuery;
	}
	
	/**
	 * Generates the HTML for a option
	 * 
	 * @param string $id
	 * @param string $checked
	 * @param string $alt
	 * @param string $value
	 * @param string $title
	 * @return string the HTML
	 */
	private function _optionHtml($id, $checked, $alt, $value, $title)
	{
		$id = PH::htmlspecialchars($id);
		$checked = $checked == $value ? ' checked="checked"' : "";
		$alt = $alt ? ' alt="'.htmlspecialchars($alt).'"' : "";
		$value = PH::htmlspecialchars($value);
		$title = PH::htmlspecialchars($title);
		$thumb = PH::htmlspecialchars($this->_thumbUrl($value));
		
		return <<<EOH
	<div>
		<div>
			<label for="{$id}"><img src="{$thumb}" alt="{$title}" /></label>
		</div>
		<div>
			<input id="{$id}" name="{$this->name}" type="radio" {$checked} {$alt} value="{$value}" />
			<label for="{$id}">{$title}</label>
		</div>
	</div>
EOH;
	}
}
