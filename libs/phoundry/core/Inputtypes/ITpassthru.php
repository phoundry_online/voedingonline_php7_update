<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/Inputtypes/IThidden.php";

class ITpassthru extends IThidden
{
	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getExtra() 
	{
		$query = null;
		$queryFields = null;
		if (count($this->extra)) {
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
		}
		$adminInfo = array();
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Header-query',
			'required' => false,
			'syntax'   => 'text',
			'info'     => 'Enter the query that results in the page-header.',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'query'     => trim((string)$_POST['ITquery'])
		);

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<h2>SELECT query</h2>
With a passthru field, you often want to display the category or selection
in the page header. Define a query that results in the name of the category
or selection to do so.
<pre>
SELECT category_name FROM table WHERE id = {\$passthru_field}
</pre>
<p>
All fields in the SELECT-part will be displayed in the page header, separated
by spaces.
</p>
EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		return $help;
	}
}
