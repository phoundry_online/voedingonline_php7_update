<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITAeventevent extends Inputtype
{
	public $value, $query, $queryFields;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getValue($page, $value, $keyval = null)
	{
		return $value;
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;

		$alt = $this->column->getJScheckAltTag();

		$includes['jsCode'][] = "$(function(){setEventEvent(document.forms[0]['event'].value)});";
		$includes['jsCode'][] = "function setEventEvent(event) {
			event = event.replace(/\-/, '');
			if (\$.inArray(event, ['preinsert', 'preinsertForm']) != -1) {
				\$('#eventcodeDiv').html('<tt>function ' + event + '() {</tt>');
			}
			else if (\$.inArray(event, ['preinsert', 'postinsert', 'predelete', 'postdelete', 'preinsertForm']) != -1) {
				\$('#eventcodeDiv').html('<tt>function ' + event + '(\$keyVal) {</tt>');
			}
			else {
				\$('#eventcodeDiv').html('<tt>function ' + event + '(\$keyVal, \$oldKeyVal) {</tt>');
			}
		}";
		$html = '<select name="' . $this->name . '" alt="' . $alt . '" onchange="setEventEvent(this.value)">';
		if (!$this->column->required)
			$html .= '<option value="">[' . word(57) . ']</option>';
		// Pipe-separated option=value pairs:
		$opts = array('pre-insert', 'post-insert', 'pre-update', 'post-update', 'pre-copy', 'post-copy', 'pre-delete', 'post-delete', 'pre-insertForm', 'pre-updateForm');
		foreach($opts as $opt) {
			$selected = ($opt == $value) ? ' selected="selected"' : '';
			$html .= '<option value="' . $opt . '"' . $selected . '>' . PH::htmlspecialchars($opt) . '</option>';
		}
		$html .= '</select>';
		return $html;
	}
}
