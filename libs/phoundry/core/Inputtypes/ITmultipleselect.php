<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITmultipleselect extends Inputtype
{
	public $query, $help, $queryFields;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getValue($page, $value, $keyval = null)
	{
		global $db;

		$values = explode(',', $value);

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				$html = '';
				$sql = PH::evaluate($this->extra['query']);
				if (preg_match('/^(SELECT|SHOW)/i', $sql)) {
					// Database query:
					$cur = $db->Query($sql)
						 or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
					for ($x = 0; !$db->EndOfResult($cur); $x++) {
						if (in_array($db->FetchResult($cur,$x,$this->extra['optionvalue']), $values)) {
							if ($html != '') $html .= ', ';
							foreach($this->extra['searchtext'] as $text)
								$html .= PH::langWord($db->FetchResult($cur,$x,$text)) . ' ';
						}
						$html = trim($html);
					}
				}
				else {
					// Pipe-separated option=value pairs:
					$pairs = explode('|', $sql);
					foreach($pairs as $pair) {
						$pair = explode('=', $pair, 2);
						if (in_array($pair[0], $values)) {
							if ($html != '') $html .= ', ';
							$html .= PH::langWord($pair[1]);
						}
					}
				}
			return $html;
			default:
				return $value;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;
		$value = PH::htmlspecialchars(parent::getInputHtml($page, $value, $includes));
		$alt = $this->column->getJScheckAltTag();
		$selected = explode(',', $value);
		$html = '<select multiple="multiple" size="' . $this->extra['size'] . '" name="' . $this->name . '[]" alt="' . $alt . '">';
		$sql = PH::evaluate($this->extra['query']);

		if (preg_match('/^(SELECT|SHOW)/i', $sql)) {
			// Database query:
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++) {
				$seld = in_array($db->FetchResult($cur,$x,$this->extra['optionvalue']), $selected) ? 'selected="selected"' : '';
				$html .= '<option ' . $seld . ' value="' . PH::htmlspecialchars($db->FetchResult($cur,$x,$this->extra['optionvalue'])) . '">';
				foreach($this->extra['optiontext'] as $text)
					$html .= PH::htmlspecialchars(PH::langWord($db->FetchResult($cur,$x,$text))) . ' ';
				$html .= '</option>';
			}
		}
		else {
			// Pipe-separated option=value pairs:
			$pairs = explode('|', $sql);
			foreach($pairs as $pair) {
				$pair = explode('=', $pair, 2);
				$seld = in_array($pair[0], $selected) ? 'selected="selected"' : '';
				$html .= '<option ' . $seld . ' value="' . PH::htmlspecialchars($pair[0]) . '">'  . PH::htmlspecialchars(PH::langWord($pair[1])) . '</option>';
			}
		}
		$html .= '</select>';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $db, $PHSes, $PHprefs;

		$helpers = array('<a href="#" tabindex="1" onclick="selectAll(_D.forms[0][\'' . $this->name . '[]\']);return false">' . word(30) . '</a>'); 

		return $helpers;
	}

	public function getExtra() 
	{
		global $db;
		
		$query = $queryFields = null;
		$size = 6;
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
			if (isset($this->extra['optionvalue'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
		}
		$adminInfo = array();
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the ' . substr(get_class($this),2) . ' (how many items are displayed at once)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query for the options in the ' . substr(get_class($this),3) . ' (<a href="#" onclick="makePopup(null,300,200,\'Select Query\',\'selectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);
		$adminInfo['queryFields'] = array(
			'name'     => 'ITqueryFields',
			'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
		);

		$tables = array();
		$sql = "SELECT id, name, description FROM phoundry_table WHERE show_in_menu NOT LIKE 'admin%' ORDER BY description";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$name = $db->FetchResult($cur,$x,'name');
			$tables[$db->FetchResult($cur,$x,'id')] = PH::langWord($db->FetchResult($cur,$x,'description')) . (!empty($name) ? ' (' . $name . ')' : '');
		}

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (preg_match('/^(SELECT|SHOW)/i', $query)) {
			if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
				$msg = 'Option value, option text and search text are not defined!';
				return false;
			}
		}

		$struct = PH::javascript2php($_POST['ITqueryFields']);
		$extra = array(
			'query'     => $query,
			'size'      => (int)$_POST['ITsize'],
		);

		if (isset($struct['optionvalue']))
			$extra['optionvalue'] = (string)$struct['optionvalue'];
		if (isset($struct['optiontext']))
			$extra['optiontext'] = $struct['optiontext'];
		if (isset($struct['searchtext']))
			$extra['searchtext'] = $struct['searchtext'];

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<h2>name=value pairs</h2>
<pre>
green=I like green|red=I like red|blue=I like blue
</pre>
<p>
In this case, the OPTIONs don't come from the database, but they are predefined.
The syntax is <tt>value=name</tt>, the OPTIONs are separated by a pipe. The
<tt>value</tt> is the VALUE for the OPTION, everything on the right of
the =-sign will be showed to the user.
</p>

<h2>Regular SELECT query</h2>
<pre>
SELECT id, fields FROM table ORDER BY name
</pre>
<p>
The first field in the SELECT-part will be the VALUE of the OPTION. The other
fields will be showed as text to the user.<br />
</p>
<h2>Recursive SELECT query</h2>
<pre>
SELECT id,name,#parent# FROM category ORDER BY name
</pre>
<p>
This type of query is used when a table contains a reference to itself. This
is often seen in tables containing categories. The first field in the SELECT-part will be the VALUE of the OPTION. 
The last field, which has to be enclosed by
<tt>#</tt>, identifies the field that references to the primary key (id) in the
same table.
</p>
EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		//return str_replace(array("\r", "\n"), array('', '\\n'), $help);
		return $help;
	}
}
