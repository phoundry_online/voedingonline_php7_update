<?php
	/** 
	 *	This file has to be php-included inside a script tag, i.e.
	 *		<script type="text/javascript">
	 *			<? require('script/FilterConstants.js.php'); ? >
	 *		</script>
	 *	
	 * NOT: <script src="..."><script> !
	 *
	 *	Assumes: ../FilterConstants.php was loaded before
	*/
	if (!defined('FI_TYPE_NUM')) exit;
?>
		// START: script/FilterConstants.js.php

		var	
			close_img = '<?= $PHprefs['url'] ?>/layout/pics/popup_close.gif',
			TIME_TRIGGER = <?= $timeTrigger ? 'true' : 'false' ?>,
			URL_BASE = self.location.href.match(/^(.*)\/[^\/]*$/)[1],
			TYPE_NUM = <?=FI_TYPE_NUM?>,
			TYPE_STR = <?=FI_TYPE_STR?>,
			TYPE_DATE = <?=FI_TYPE_DATE?>,
	
			GROUP_OR = <?=FI_GROUP_OR?>,
			GROUP_AND = <?=FI_GROUP_AND?>,
			
			GROUP_NAMES = {<?=FI_GROUP_OR?>:'or', <?=FI_GROUP_AND?>:'and'},
			
			OP_EQUALS = <?=FI_OP_EQUALS ?>,
			OP_NULL = <?=FI_OP_NULL?>,
			OP_LESS = <?=FI_OP_LESS ?>,
			OP_GREATER = <?=FI_OP_GREATER ?>,
			OP_BETWEEN = <?=FI_OP_BETWEEN ?>,
			OP_PERIODIC = <?=FI_OP_PERIODIC ?>,
			OP_STARTS = <?=FI_OP_STARTS ?>,
			OP_ENDS = <?=FI_OP_ENDS ?>,
			OP_CONTAINS = <?=FI_OP_CONTAINS ?>,
			OP_FINDINSET = <?=FI_OP_FINDINSET ?>,
			OP_IN = <?=FI_OP_IN ?>,

			LI_CSS = {	<?=FI_GROUP_OR?>:{backgroundImage:'url('+URL_BASE+'/pics/filter/filter_or.gif)',paddingLeft:'22px'},
							<?=FI_GROUP_AND?>:{backgroundImage:'url('+URL_BASE+'/pics/filter/filter_and.gif)',paddingLeft:'30px'},
							first:{backgroundImage:'none',paddingLeft:0}},
			
			
			// @todo adjust not-images (differ in letter-spacing)
			NOT_IMG = {	<?=FI_GROUP_OR?>: URL_BASE+'/pics/filter/filter_not_or.gif',
							<?=FI_GROUP_AND?>: URL_BASE+'/pics/filter/filter_not_and.gif'},
			
			T_COLORS = {<?=FI_GROUP_OR?>:'<?=F_COLOR_OR?>', <?=FI_GROUP_AND?>:'<?=F_COLOR_AND?>'},

			OP_WORDING = {
				<?=FI_OP_EQUALS ?>: 'equals %1',
				<?=FI_OP_LESS ?>: 'smaller than %1',
				<?=FI_OP_GREATER ?>: 'greater than %1',
				<?=FI_OP_BETWEEN ?>: 'between %1 and %2',
				<?=FI_OP_PERIODIC ?>: '+ %1 year %2 %3 %4 = today',
				<?=FI_OP_STARTS ?>: 'starts with %1',
				<?=FI_OP_ENDS ?>: 'ends with %1',
				<?=FI_OP_CONTAINS ?>: 'contains %1',
				<?=FI_OP_FINDINSET ?>: 'find in database set %1',
				<?=FI_OP_NULL?>: 'is undefined (null)',
				<?=FI_OP_IN?>: 'has one of the following values %1'
			},

<?php	
			global $FI_VALS_PER_OP;
			$valsPerOp = array();
			foreach ($FI_VALS_PER_OP as $op => $valsNum) 
				$valsPerOp[] = sprintf('%s:%s', $op,$valsNum);
?> 
			VALS_PER_OP = {<?= join(',',$valsPerOp)?>},

			DEFAULT_ITEM_COL = 'id',

			DATE_REGEXP = <?=FI_DATE_REGEXP?>,
			DEFAULT_PADD_TIME = ' 00:00:00',

			// String constants.. 

			FW_LEGEND_FIELDNAME = '<?=word(219)?>',
			FW_LEGEND_YYYMMDD = '<?= word(439, '<b>YYYY-MM-DD</b>')?>';
			FW_LEGEND_CRITERIUM = '<?=word(220)?>';

		// END: script/FilterConstants.js.php
