
var MARKED_ITEMS_SELECTOR = '#rfilter u.marked4cb';

function FilterClipboard() {
	var me = this;
	this.buffer = [];
	
	// assign COPY functionality..
	this.copyBut = $('#copyBut').click( function() {
		me.buffer = me.getMarkedItems();
		me.update();
		return false;
	});

	// assign CUT functionality..
	this.cutBut = $('#cutBut').click( function() {
		var items = me.getMarkedItems(1);
		me.buffer = items.clones;
		$.each(items.origs, function() {
			this.parent.items = this.parent.items.kick(this);
		});
		updateFilterHTML();
		return false;
	});

	// assign DELETE functionality..
	this.delBut = $('#delBut').click( function() {
		var items = me.getMarkedItems(1);
		$.each(items.origs, function() {
			this.parent.items = this.parent.items.kick(this);
		});
		me.flush();
		updateFilterHTML();
		return false;
	});

	this.update();
}


/**
 * Updates the clipboard buttons.
 */
FilterClipboard.prototype.update = function() {
	var itemsMarked = $(MARKED_ITEMS_SELECTOR).length;
	this.copyBut.prop('disabled', !itemsMarked);
	this.cutBut.prop('disabled', !itemsMarked);
	this.delBut.prop('disabled', !itemsMarked);
}


/**
 * Returns an array of FilterItems clones of all marked items.
 * @param bool clonesAndOrigs if TRUE, the return value is an object {clones:[c1,..], origs:[o1,..]}
 */
FilterClipboard.prototype.getMarkedItems = function(clonesAndOrigs) {
	var clones = [], origs = [];
	$(MARKED_ITEMS_SELECTOR).parent('div').each( function() {
		clones.push( this.fItem.clone() );
		if (clonesAndOrigs) origs.push(this.fItem);
	});
	return (clonesAndOrigs)? {'clones':clones, 'origs':origs} : clones;
}


/**
 * Flushes the content of the clipboard and updates the buttons accordingly.
 */
FilterClipboard.prototype.flush = function() {
	this.buffer = [];
	this.update();
}

/**
 * Unmarks all currently selected items.
 */
FilterClipboard.prototype.unmarkAll = function() {
	$(MARKED_ITEMS_SELECTOR).removeClass('marked4cb');
	this.update();
}


FilterClipboard.prototype.getBufferCopy = function() {
	var newBuf = [];
	for (var i=0; i<this.buffer.length; i++) {
		newBuf.push( this.buffer[i].clone(1) );
	}
	return newBuf;
}
