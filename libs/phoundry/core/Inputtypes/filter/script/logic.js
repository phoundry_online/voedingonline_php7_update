
/**
 * The base FilterGroup-Object
 */
var	filterObj = null,
		selectedItems = [],
		undo = null,		// FilterUndo object
		justUpdatedItem = null;

// on dom load..
$(function() {
	filterObj = (RAW_FILTER_OBJ)? FilterGroup.getInstanceFromRaw(RAW_FILTER_OBJ) : new FilterGroup();
	undo = new FilterUndo();
	clipb = new FilterClipboard();
	updateFilterHTML();

	$(document.body).click( function() {
		clipb.unmarkAll();
	});
	
	$('input[type=button]').each( function() {
		this.on = function() {this.disabled=false;};
		this.off = function() {this.disabled=true;};
	});
	
});


// --------------------------------------------------------------------------------------------


/**
 * Returns the JSON representation of filterObj or an empty string if no items.
 */
function getFilterJSON() {
	return (filterObj.items.length)? $.toJSON(filterObj.clone()) : '';
}

// --------------------------------------------------------------------------------------------

/**
 * Creates the HTML for the filterObj.
 */
function updateFilterHTML(noUndo) {
	setParents();
	clipb.update();
	if (!noUndo) {
		undo.newState();
	}

	$('#rfilter').html('').append( filterObj.toJQuery() );
	justUpdatedItem = null;

	// find a #justUpdated item DIV and place an eyecatcher next to it..
	$('#justUpdated').each( function() {
		var	pos = $(this).find('img').eq(0).offset(),
				offsX = -10 - (($.browser.msie)?3:0),
				offsY = -12 - (($.browser.msie)?3:0),
				posCSS = {'left':pos['left']+offsX+'px', 'top':pos['top']+offsY+'px', position:'absolute'},
				EC_START_DELAY = 100,
				EC_FADEIN = 300,
				EC_STAY = 500,
				EC_FADEOUT = 400;

		setTimeout(function() {
			var jEyeCatcher = $('<img src="pics/filter/haken.gif" id="updEyeCatcher">');
			$('body').append( jEyeCatcher.css(posCSS)
				.fadeIn(EC_FADEIN, function() {
					setTimeout( function() {
						$(jEyeCatcher).fadeOut(EC_FADEOUT, function(a) {
							jEyeCatcher.remove();
						})
					}, EC_STAY);
				})
			); 
		}, EC_START_DELAY);
	});


	// add del functionality..
	$('img.fiDel').add('img.fgDel').click( function() {
		var	isItem = (this.className=='fiDel'),
				confMsg = (isItem)? 'Are you sure you want to remove this item?' : 'Are you sure you want to remove this group and all its items?',
				ancestorTag = (isItem)? 'div' : 'table';
		
		if (!confirm(confMsg)) {
			return false;
		}

		var	container = $(this).parents(ancestorTag)[0],
				itemOrGroup = container.fItem || container.fGroup,
				group = itemOrGroup.parent; 
		group.items = group.items.kick(itemOrGroup);
		updateFilterHTML();
		return false;
	});
	
	$('img.fgMerge').attr('title','merge with parent group').click( function() {
		var group = $(this).parents('table')[0].fGroup;
		group.parent.items = group.parent.items.kick(group).concat(group.items);
		updateFilterHTML();
		return false;
	});
}

// --------------------------------------------------------------------------------------------

/**
 * Kicks the given element off a array and returns the resulting array.
 */
Array.prototype.kick = function(e) {
	var newArr = [];
	for (var i=0; i<this.length; i++) {
		if (this[i]!=e) newArr.push(this[i]);
	}
	return newArr;
}

// --------------------------------------------------------------------------------------------


/**
 * Kicks the given element off a array and returns the resulting array.
 */
Array.prototype.replace = function(search, repl) {
	for (var i in this) {
		if (this[i]!=search) continue;
		this[i] = repl;
		// alert('replaced one');
		return;
	}
	// alert('nix replaced');
}

//function gE(id){return document.getElementById(id);}
