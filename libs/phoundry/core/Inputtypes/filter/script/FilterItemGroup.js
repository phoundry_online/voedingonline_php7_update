
// ------------ FilterGroup --------------

/**
 * Represents a group of type 'and' or 'or'
 */
function FilterGroup(groupType) {
	this.group = (typeof groupType=='undefined')? GROUP_AND : groupType;
	this.items = [];
}

// --------------------------------------------------------------------------------------------

/**
 * Returns a clone of the group.
 * Note: parent properties are NOT cloned!
 */
FilterGroup.prototype.clone = function() {
	return FilterGroup.getInstanceFromRaw(this, 1);
}


/**
 * Returns an instance of FilterGroup based on the given raw object.
 * @param oject rawObj anonymous filterGroup object without methods.
 * 
 */
FilterGroup.getInstanceFromRaw = function(rawObj, noParent) {
	var o = new FilterGroup(rawObj.group);
	if (rawObj.invert) o.invert = 1;
	if (!noParent && rawObj.parent) {
		o.parent = rawObj.parent;
	}
	$.each(rawObj.items, function() {
		o.addItem( (this.item)? FilterItem.getInstanceFromRaw(this, noParent) : FilterGroup.getInstanceFromRaw(this, noParent) );
	});
	return o;
}

// --------------------------------------------------------------------------------------------


/**
 * Converts this group into a single item.
 * Note: to be applied on Groups with only ONE item.
 */
FilterGroup.prototype.toItem = function() {
	if (this.items.length!=1) return null;
	return this.items[0];
}


// --------------------------------------------------------------------------------------------


/**
 * Adds a group or item to a group.
 */
FilterGroup.prototype.addItem = function(it) {
	switch (true) {
		case (it.item && it.isVoid):
			alert('Filter item "'+it.item+'" is void and will be skipped.');
			return;
		case (it.op && !OP_WORDING[it.op]):
			alert('Unknown operation code ('+it.op+') for "'+it.item+'" item.\nThis item will be skipped.');
			return;
		case (it.group && !it.items.length):
			//alert('empty groups are currently not skipped');
			//return;
	}
	this.items.push(it);
}


/**
 * Inverts the group or group type (boolean operator).
 * @param bool  typeOnly (optional) if TRUE, the grouptype is swapped (and<->or)
 */
FilterGroup.prototype.doInvert = function(typeOnly) {
	if (typeOnly) {
		this.group = (this.group==GROUP_AND)? GROUP_OR : GROUP_AND;
		return;
	}
	this.invert = !this.invert;
}


// --------------------------------------------------------------------------------------------


/**
 * Returns an HTML/DOM version of the group as JQuery object.
 */
FilterGroup.prototype.toJQuery = function() {
	var	group = this,
			groupType = this.group,
			bgColor = T_COLORS[groupType],
			delBut = this.parent ? '<img src="pics/filter/delete.gif" class="fgDel" title="remove group" />' : '',
			mergeBut = (this.parent && this.parent.group==groupType) ? '<img src="pics/filter/merge.gif" class="fgMerge" />' : '',
			jTable = $(	'<table cellspacing="0" class="'+GROUP_NAMES[groupType]+'">'+
							'<caption style="background:'+bgColor+'" align="bottom"></caption>'+
							'<tr><th fInverter="1">&nbsp;</th><td></td></tr>'+
							'</table>'),
			toggleBut = $('<img src="pics/filter/and_or.gif" title="Toggle group type AND/OR" />').click( function() {
				group.doInvert(1);
				updateFilterHTML();
				return false;
			}),
			invBut = $('<img src="pics/filter/not.gif" fInverter="1" />');
	
	jTable[0].fGroup = this;
	
	/* make arbitrary clicks flush the selection..
	if (!this.parent) jTable.click( function() {
		return false;
	});
	*/

	// add buttons to caption (toggle AND/OR)
	jTable.find('caption').append(toggleBut, invBut, mergeBut, delBut);
	
	var thCSS = (!this.invert)? 
					{	backgroundColor:bgColor} : 
					{	background: 'transparent url('+NOT_IMG[groupType]+') center center no-repeat',
						borderStyle: 'solid',
						borderColor: bgColor,
						borderWidth: '3px 3px 1px 3px',
						width: '30px'
					};

	jTable.find('th').css(thCSS);

	jTable.find('caption').find('*[fInverter]').attr('title','invert').click( function() {
		group.doInvert();
		updateFilterHTML();
		return false;
	});
		
	// put items into UL (as LI) (recurs.)
	var jUL = $('<ul></ul>');
	$.each(this.items, function(i,fItemOrGroup) {
		jUL.append(
			$('<li></li>').css(LI_CSS[i == 0 ? 'first' : groupType]).append(fItemOrGroup.toJQuery(fItemOrGroup.item ? groupType : null)) 
		);
	});

	// add 'new..' link
	jTable.find('td').append( jUL.append( $('<li>').css(LI_CSS[(this.items==0)?'first':groupType]).hover(
			function() {
				var className = (clipb.buffer.length)? 'a' : 'a[class="fadd"]';
				window.status=Math.random();
				$(this).find(className).css('display','inline');
				return false;
			}, function() {
				$(this).find('a').hide();
				return false;
			}
	)
	.append(
		$('<span></span>').attr('href','#').append('<span>...</span>')
			.append( $('<a class="fadd" style="display:none" href="javascript://">&lt;Add Item&gt;</a> &nbsp; ').bind('click', function(){
				var newItem = new FilterItem();
				newItem.parent = group;
				new FilterEditPopup(newItem);
				return false;
			}))
			.append( $('<a class="fadd" style="display:none" href="javascript://">&lt;Add Group&gt;</a> &nbsp; ').bind('click', function(){
				var newGroup = new FilterGroup( (groupType==GROUP_AND)? GROUP_OR : GROUP_AND );
				group.addItem(newGroup);
				updateFilterHTML();
				return false;
			}))
			.append( $('<a style="display:none" href="javascript://">&lt;Paste&gt;</a>').bind('click', function(){
				if (clipb.buffer.length) {
					group.items = group.items.concat( clipb.getBufferCopy() );
					updateFilterHTML();
				}
				return false;
			}))
		)
	));

	return jTable;
}


// --------------------------------------------------------------------------------------------


// ------------ FilterItem --------------

/**
 * Constructor for a filter item.
 */
function FilterItem(colName) {
	this.item = colName || DEFAULT_ITEM_COL;
	this.op = OP_EQUALS;
	try {
		this.type = FILTER_COLS[this.item].type;
		if (!colName) throw new Exception('bla'); // just make it set isVoid to indicate this one isnt member of a group yet
	} catch (e) {
		this.isVoid = true;
	}
}



// --------------------------------------------------------------------------------------------

/**
 * Returns a clone of the item.
 * Optional properties are purged if empty.
 * @see FilterItem.getInstanceFromRaw()
 */
FilterItem.prototype.clone = function(noParent) {
	return FilterItem.getInstanceFromRaw(this, noParent);
}


/**
 * Returns a FilterItem instance based on the given raw object.
 * Optional parameters are purged if empty.
 * @param rawObj anonymous item object without methods
 */
FilterItem.getInstanceFromRaw = function(rawObj, noParent) {
	var c = new FilterItem(rawObj.item), prop;
	c.type = rawObj.type;
	c.op = rawObj.op;
	
	// copy only vals that are needed by the operator..
	for (var i=0; i<VALS_PER_OP[c.op]; i++) {
		var prop = 'val'+(i+1);
		if (typeof rawObj[prop]=='undefined') continue;
		c[prop] = rawObj[prop];
	}

	// copy non-empty optional properties..
	var optProps = ['invert','isVoid'];
	if (!noParent) optProps.push('parent');
	for (var i in optProps) {
		prop = optProps[i];
		if (rawObj[prop]) c[prop] = rawObj[prop];
	}
	return c;
}


// --------------------------------------------------------------------------------------------

/**
 * Inverts the item.
 */
FilterItem.prototype.doInvert = function() {
	this.invert = !this.invert;
}


// --------------------------------------------------------------------------------------------

/**
 * Returns an HTML/DOM version of the item as JQuery object.
 * @param groupType to be passed by the invoking FilterGroup::toJQuery()
 */
FilterItem.prototype.toJQuery = function(groupType) {
	var	item = this,

	// string representation..
	opStr = OP_WORDING[this.op].replace(/%(\d)/g, function(m,i) {
		var val = (typeof item['val'+i]!='undefined')? item['val'+i] : '&lt;???&gt;';
		return (item.type==TYPE_STR)? '&quot;<b>'+val+'</b>&quot;' : '<b>'+val+'</b>';
	}),

	/* edit button..
	editBut = $('<img src="pics/filter/edit.gif" class="fiEdit" title="edit">').click( function(e) {
		$(this).parents('li').slice(0,1).attr('class','fmarked');
		new FilterEditPopup(item);
		return false;
	}),
	*/

	// invert button..
	notBut = $('<img src="pics/filter/not.gif" class="fiNot" title="invert" />').click( function() {
		item.doInvert();
		updateFilterHTML();
		return false;
	}),

	// del button.. (functionality added later in updateFilterHTML())
	delBut = $('<img src="pics/filter/delete.gif" class="fiDel" title="remove item" />'),

	jItem = $('<u title="click to mark">'+ this.item +'</u>').css('cursor','crosshair').click( function() {
		$(this).toggleClass('marked4cb');
		clipb.update();
		return false;
	}),
	jOpString = $('<i title="click to edit">'+ opStr + '</i>').click( function() {
		$(this).parents('li').slice(0,1).attr('class','fmarked');
		new FilterEditPopup(item);
		return false;
	}),
	jType = $('<span class="ftype">(type '+ this.type +')</span>'),

	// actual DIV containing string repres..
	jDIV = $('<div></div>').append(jItem, document.createTextNode(' '), jOpString, jType, document.createTextNode(' ['), notBut, delBut, document.createTextNode(']'));

	if (this===justUpdatedItem) {
		jDIV.attr('id', 'justUpdated');
	};

	// set inversion css..
	if (this.invert) {
		jDIV.css( {'backgroundImage':'url('+NOT_IMG[groupType]+')', 'paddingLeft':'30px'} );
	}

	jDIV[0].fItem = this;
	return jDIV;
}


// --------------------------------------------------------------------------------------------

/**
 * Adds a "parent" property to the o object (FilterItem or FilterGroup)
 * Use "setParents();" to set filterObj + descendents recursively (done in updateFilterHTML())
 * property must be removed again BEFORE json conversion (otherwise illegal recursion) (see removeParents())
 */
function setParents(obj, lastParent) {
	var	o = obj || filterObj,
			parent = (!lastParent)? null : lastParent;

	o.parent = parent;

	if (o.group) {
		for (var i=0; i<o.items.length; i++) {
			setParents(o.items[i], o);
		}
	}
}


// --------------------------------------------------------------------------------------------


/**
 * Removes the "parent" property from all objects.
 * (see setParents())
 */
function removeParents(obj) {
	var	o = obj || filterObj,
			newO = {};
	for (var prop in o) {
		if (prop=='parent') continue;
		if (prop=='items') {
			for (var j=0; j<o.items.length; j++) {
				o.items[j] = removeParents(o.items[j]);
			}
		}
		newO[prop] = o[prop];
	}
	return newO;
}
