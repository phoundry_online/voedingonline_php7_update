
var UNDO_STACK_SIZE = 20;

function FilterUndo() {
	this.undoStack = [];
	this.redoStack = [];
	var me = this;
	this.undoBut = $('#undoBut').click(function(){me.undo();})[0];
	this.redoBut = $('#redoBut').click(function(){me.redo();})[0];
	this.currentFilter = null;
	this.skipShortcuts = false;

	var keyElem = ($.browser.msie)? document : window;
	$(keyElem).keypress( function(e) {
		if (me.skipShortcuts) return true;
		var strgZ = e.ctrlKey && ( (e.charCode && e.charCode==122) || (e.keyCode && e.keyCode==26) );	// undo
		var strgY = e.ctrlKey && ( (e.charCode && e.charCode==121) || (e.keyCode && e.keyCode==25) );	// redo
		switch (true) {
			case strgZ: me.undo(); return false;
			case strgY: me.redo(); return false;
		}
	});
}


/**
 * Keeps track of the current filterObj by storing a copy inside the currentFilter property.
 * If currentFilter already filled with a previous state, that filterObj clone is added to the undo stack.
 * The oldest state is kicked as soon as the stack size reaches UNDO_STACK_SIZE.
 * @internal called from updateFilterHTML()
 */
FilterUndo.prototype.newState = function() {
	if (!this.currentFilter) {
		this.currentFilter = filterObj.clone();
		return;
	}
	
	// kick first element to prevent overflow..
	if (this.undoStack.length==UNDO_STACK_SIZE) {
		this.undoStack.shift();
	}

	this.undoStack.push(this.currentFilter);
	this.currentFilter = filterObj.clone();

	// statechange makes all redos void..
	if (this.redoStack.length) {
		this.redoStack = [];
		this.redoBut.off();
	}

	// switch undo button 
	if (this.undoStack.length==1) {
		this.undoBut.on();
	}
}


/**
 * Replaces the global filterObj with top element from the undo stack.
 * The HTML is updated, too.
 */
FilterUndo.prototype.undo = function() {
	if (!this.undoStack.length) {
		alert('The undo stack is empty!');
		return;
	}

	// kick first element of redostack to prevent overflow..
	if (this.redoStack.length==UNDO_STACK_SIZE) {
		this.redoStack.shift();
	}
	this.redoStack.push(this.currentFilter);
	if (this.redoStack.length==1) {
		this.redoBut.on();
	}
	
	filterObj = this.undoStack.pop();
	this.currentFilter = filterObj.clone();

	if (!this.undoStack.length) {
		this.undoBut.off();
	}
	updateFilterHTML(1);
}


/**
 * Replaces the global filterObj with top element from the undo stack.
 * The HTML is updated, too.
 */
FilterUndo.prototype.redo = function() {
	if (!this.redoStack.length) {
		alert('The redo stack is empty!');
		return;
	}

	// kick first element of undostack to prevent overflow..
	if (this.undoStack.length==UNDO_STACK_SIZE) {
		this.undoStack.shift();
	}
	this.undoStack.push(this.currentFilter);
	if (this.undoStack.length==1) {
		this.undoBut.on();
	}
	
	filterObj = this.redoStack.pop();
	this.currentFilter = filterObj.clone();

	if (!this.redoStack.length) {
		this.redoBut.off();
	}
	updateFilterHTML(1);
}