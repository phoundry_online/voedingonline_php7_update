<?php
	/** 
	 *	This file has to be php-included inside a script tag, i.e.
	 *		<script type="text/javascript">
	 *			<? require('script/RawFilterObj.js.php'); ..>
	 *		</script>
	 *	
	 * NOT: <script src="..."><script> !
	 *
	 *	Assumes: - var $json containing the json string currently in the database
	 *				- global $useUTF8 bool indicating if this page+database is utf8 (TRUE) or iso (FALSE)
	 * 
	 * In case of error, the JSON string is discarded and resetted for this page.
	*/
	if (!isset($json)) exit;

	// example json: {"group":1,"items":[{"item":"id","op":"1","type":1,"val1":543221},{"item":"title","op":"1","type":2,"val1":"lep"},{"group":2,"items":[{"item":"myname","op":"6","type":2,"val1":"LeP"},{"item":"myname","op":"7","type":2,"val1":"LeP"},{"item":"myname","op":"8","type":2,"val1":"LeP"},{"group":1,"items":[{"item":"title","op":"1","type":2,"val1":"abc"},{"item":"numfield","op":"1","type":1,"val1":122}]},{"item":"date","op":"5","type":3,"val1":"2005-12-31 05:40:26","val2":"2005-12-31 05:40:23"}]},{"item":"numfield","op":"5","type":1,"val1":21,"val2":44},{"item":"myname","op":"8","type":2,"val1":"bla"}]}; 

?>
// START: script/RawFilterObj.js.php
<?php
	$jsonStr = 'false';
	$decodeError = false;

	if ($json) {
		$converter = new FilterConverter($json, $useUTF8);
		if ($converter->error) $decodeError = true;
		else $jsonStr = $json;
	}

	if ($decodeError) { 
?>
	$(function() {
		alert('The saved filter is invalid and could not be loaded.');
		// error: <?=$converter->error?> 
		// json: <?=$json?> 
	});
<?php	} ?>
	var RAW_FILTER_OBJ = <?= $jsonStr ?>;

// END: script/RawFilterObj.js.php
