(function($) {
	var popup, modal, dragX, dragY, isFS=false, fsCSS, jqW = $(window),
        kill = function() {
            undo.skipShortcuts = false;
            if (popup) popup.remove();
            if (modal) modal.remove();
            modal = popup = null;
        };

	// the public plugin method
	$.fn.FilterPopup = function(settings, divFillCallback) {
		// setup configuration
		settings.delay  = settings.delay || 500;
		settings.width  = Number(settings.width) || 400;
		settings.height = Number(settings.height) || 300;
		settings.title  = settings.title || '';
		settings.event  = settings.event || {};
		settings.modal  = settings.modal || false;

		var popupMove = function(e){ popup.css({left:e.clientX-dragX+'px',top:e.clientY-dragY+'px'})};
		
		kill();

		// Size and position calculations:
		var
		ww = jqW.width(),
		wh = jqW.height(), 
		sl = self.pageXOffset || $.boxModel && document.documentElement.scrollLeft || document.body.scrollLeft,
		st = self.pageYOffset || $.boxModel && document.documentElement.scrollTop || document.body.scrollTop,
		w = settings.width,
		h = settings.height;

		// Negative width/height windows
		/*
		if (w < 0) w += ww;
		if (h < 0) h += (wh-25);
		*/

		var x = settings.event.clientX ? settings.event.clientX : (ww-w)/2,
		y = settings.event.clientY ? settings.event.clientY : (wh-h)/2;

		// Where to start: top/bottom, left/right
		if (x > ww/2) x -= w;
		if (y > wh/2) y -= h;

		// Make sure we stay within the viewport
		x = x < 0 ? 0 : x+w > ww ? (ww-w)/2 : x;
		y = y < 0 ? 0 : y+h > wh ? (wh-h)/2-25 : y-25;

		if (settings.modal) {
			$('body').append(modal = $('<div>').css({left:sl+'px',top:st+'px',width:jqW.width()+'px',height:jqW.height()+'px',position:'absolute',zIndex:998,background:'#000',opacity:0.6}))
		}

		var ph = h+73, pw = w;

		$(document.body).append(
			popup = $('<div class="popup inlineWindow normal" style="position:absolute;z-index:999;width:0;height:0;left:'+(sl+x)+'px;top:'+(st+y)+'px">')
			.animate({height:ph,width:pw}, settings.delay,
				function(){
						$(this)
						.html('<div id="mover" class="titlebar"><div style="-moz-user-select:none" unselectable="on"><div class="ptr close"></div>'+settings.title+'</div></div>' + '<div id="popupEL" style="width:'+w+'px;height:'+h+'px"><div id="fPopupContent">'+'<fieldset id="fSelDiv"><legend>(fieldname)</legend></fieldset><fieldset id="fValDiv"><legend>(criterium)</legend></fieldset><p id="fPopupButtons"></p>'+'</div></div>')
						.find('#mover')
						.dblclick(function(){
							if (isFS = !isFS) 
							{
								pCSS = [popup[0].style.cssText, $('#popupEL',popup)[0].style.cssText];
								popup.css({width:jqW.width()+'px',height:jqW.height()+'px',top:st+'px',left:sl+'px'}).find('#popupEL').css({width:'100%',height:(jqW.height()-25)+'px'});
							}
							else {
								popup[0].style.cssText = pCSS[0];
								$('#popupEL',popup)[0].style.cssText = pCSS[1];
							}
						})
						.mousedown(function(e){
							var $this = $(this), offset = $this.offset();
							dragX=e.clientX-offset['left'];
							dragY=e.clientY-offset['top'];
							$this.css({cursor:'move',zIndex:dragZ++});
							$(document).mousemove(popupMove);
						})
						.mouseup(function(){
							$(this).css('cursor','');
							$(document).unbind('mousemove',popupMove);
						})
						.find('.ptr').click(function(){kill()});

					divFillCallback($('#fSelDiv'), $('#fPopupButtons'));
				}
		)
	);

	$.fn.FilterPopup.kill = function() {
		kill();
	}
	
	return this;
};
})(jQuery);
