
/**
 *	Opens an item edit popup.
 * If isVoid, item (it) has to be added to its parent when OK is pressed.
 */
function FilterEditPopup(item, justCreated) {
	this.POPUP_SETTINGS = {width:300,height:410,title:'Edit filter', modal:true};	// popup settings
	this.origItem = item;

	var cItem = item.clone();	// cloned item

	cItem.item = item.item || DEFAULT_ITEM_COL;
	cItem.op = item.op || OP_EQUALS;

	this.cItem = cItem;
	var _popup = this;

	justUpdatedItem = null;

	// construct the popup..
	$.fn.FilterPopup(this.POPUP_SETTINGS, function(jSelDiv, jButDiv){
		// no undo/redo shortcuts while in this window..
		undo.skipShortcuts = true;

		// append buttons (ok/cancel)..
		var cancel = $('<input style="float:right" type="button" value="Cancel"/>').click( function() {
			$.fn.FilterPopup.kill();
			updateFilterHTML(1);
		});
		var ok = $('<input class="okBut" type="button" value="Ok"/>').click(function(){_popup.applyChanges();});
		jButDiv.append(cancel, ok);

		// create type select..
		var	select = $('<select>'),
				selIndex=-1, 
				i=0;
		
		// append type options..
		for (var prop in FILTER_COLS) {
			var	isCurr = (prop==_popup.cItem.item),
					opt = $('<option value="'+prop+'">'+prop+'</option>');
			if (isCurr) {
				selIndex = i;
				opt.css('fontWeight','bold');
			}
			select.append(opt);
			i++;
		}
		if (selIndex!=-1) select[0].selectedIndex = selIndex;

		// add change event handler..
		select.change( function(){
			var	it = _popup.cItem;
			it.item = this.value;
			it.type = FILTER_COLS[this.value].type;
			_popup.update();
		});

		jSelDiv.append(select);
		
		$('#fSelDiv legend').html(FW_LEGEND_FIELDNAME);
		$('#fValDiv legend').html(FW_LEGEND_CRITERIUM);

		// draw rest..
		_popup.update();
	});
}

/**
 * Redraws the contentDiv.
 */
FilterEditPopup.prototype.update = function() {
	var	jValDiv = $('#fValDiv'),
			col = FILTER_COLS[this.cItem.item];
	jValDiv.find('table').remove().end();
	var ops = [];
	switch(col.type) {
		case TYPE_NUM:		ops = [OP_EQUALS, OP_LESS, OP_GREATER, OP_BETWEEN, OP_IN, OP_FINDINSET];
								if (col.nullable) ops.push(OP_NULL);
								break;
		case TYPE_STR:		ops = [OP_EQUALS, OP_STARTS, OP_ENDS, OP_CONTAINS, OP_IN, OP_FINDINSET];
								if (col.nullable) ops.push(OP_NULL);
								break;
		case TYPE_DATE:	ops = [OP_EQUALS, OP_LESS, OP_GREATER, OP_BETWEEN, OP_PERIODIC];
								if (col.nullable) ops.push(OP_NULL);
								break;
		default: alert('Type '+col.type+' not yet implemented!');
	}
	jValDiv.append( $('<table width="100%" cellspacing="1">').append(this.getFilterRows(this.cItem, ops)) );
	this.focusOpInput();
}


/**
 * Sets the focus on the first inputfield belonging to the items op value.
 * @param bool sel if true, the input content will be selected
 */
FilterEditPopup.prototype.focusOpInput = function(sel) {
	var focussed = $('#fValDiv input[op='+this.cItem.op+'][className=fEnabled]');
	if (!focussed.length) return;
	focussed[0].focus();
	if (sel) focussed[0].select();
}


/** 
 * Returns a jquery TBODY containing TRs with all possible operators for the given item.
 * @param Array ops an array of operations OP_* constants
 */ 
FilterEditPopup.prototype.getFilterRows = function(item, ops) {
	var	trs = $('<tbody id="fPopupTBody">'),
			pop = this;
	if (item.type===TYPE_DATE) {
		trs.append('<tr><td></td><td>' + FW_LEGEND_YYYMMDD + '</td></tr>');
	}
	$.each(ops, function(i,op) {
		if (op == OP_PERIODIC && !TIME_TRIGGER) return true; // Continue if TRIGGERS are not allowed
		var	radioID = 'fop' + op,
				tr = $('<tr>'),
				th = $('<th>'),
				td = $('<td>'),
				valCount = 0,
				active = (op==item.op && item.type==pop.origItem.type);

		// create op selector (radio)..
		var radio = $('<input type="radio" name="fop" value="'+op+'" id="'+radioID+'" />');
		if (active) radio[0].checked = true;

		radio[0].clickFunc = function() {
			this.checked = true;
			var	inputs = $(trs).find('input[type=text],select'),
					PAT = '[op='+op+']';
			inputs.filter(PAT).attr('class','fEnabled').end().not(PAT).attr('class','fDisabled');
		};

		radio.click(radio[0].clickFunc);	// sorry for this.. otherwise IE refused to do radio.click() inside input[text] focus handlers

		th.append(radio);


		// create inputs for current op..
		var s = '';
		switch (op) {
			case OP_EQUALS:	
			case OP_LESS:		
			case OP_GREATER:	
			case OP_BETWEEN:	
			case OP_STARTS:	
			case OP_ENDS:		
			case OP_CONTAINS:
			case OP_FINDINSET:
			case OP_IN:	
			case OP_NULL:
				var txt = OP_WORDING[op];
				s = (txt.match(/%\d/)) ?
					txt.replace(/%(\d)/g, function(m,i) {
						var cName = active ? 'fEnabled' : 'fDisabled';
						return '<br /><input type="text" style="width:90%" class="'+cName+'" name="val'+op+'_'+i+'" op="'+op+'" prop="val'+(++valCount)+'" value="" /><br />';
					}) :
					'<label for="'+radioID+'">'+txt+'</label>';
				break;
			case OP_PERIODIC:
				var txt = OP_WORDING[op], cName = active ? 'fEnabled' : 'fDisabled';
				s = txt;
				s = s.replace(/%1/, '<input type="text" size="3" class="'+cName+'" name="val'+op+'_'+i+'" op="'+op+'" prop="val'+(++valCount)+'" value="" />');
				s = s.replace(/%2/, '<select class="'+cName+'" name="val'+op+'_'+i+'" op="'+op+'" prop="val'+(++valCount)+'"><option value="+">+</option><option value="-">-</option></select>');
				s = s.replace(/%3/, '<input type="text" size="3" class="'+cName+'" name="val'+op+'_'+i+'" op="'+op+'" prop="val'+(++valCount)+'" value="0" />');
				s = s.replace(/%4/, '<select class="'+cName+'" name="val'+op+'_'+i+'" op="'+op+'" prop="val'+(++valCount)+'"><option value="day">day</option><option value="week">week</option><option value="month">month</option></select><br />');
				break;
			// add code for additional ops HERE (e.g. enums)
			default:	alert('Unknown operation!');
		}

		td.append(s).find('input[prop],select[prop]').focus( function() {
			radio[0].clickFunc();
		}).each( function() {
			this.value = active ? (item[$(this).attr('prop')] || '') : this.value;
		}).end();

		trs.append( tr.append(th,td) );
	});
	return trs;
}

/**
 * Overwrites/adds the cItem in(to) the parent fgroup and updates the filter html.
 * HTML base:   
 *		<input type="radio" name="fop" value={op}> as operation selector
 *		<input type="input" name="fop{op}_{i}" op={op} prop={'val1'|'val2'|..}>
 */
FilterEditPopup.prototype.applyChanges = function() {
	var	it = this.cItem,
			col = FILTER_COLS[it.item],
			pop = this;
	
	var opRadio = $('input[name="fop"]').filter('*[checked]');
	if (!opRadio.length) {
		alert('Error: there is no operation selected.');
		return;
	}

	var	op = opRadio[0].value;
			valInputs = $('input[op='+op+'],select[op='+op+']').filter('*[prop]'),
			error = false;

	it.op = op;

	// iterate over inputs belonging to the items op
	valInputs.each( function() {
		var	val = $.trim(this.value),
				prop = $(this).attr('prop');
		switch(it.type) {
			case TYPE_NUM:		var valArray = (op==OP_IN)? val.split(',') : [val];
									
									// validation loop..
									for (var i=0; i<valArray.length; i++) {
										var	origVal = valArray[i],
												parsedVal = parseInt(valArray[i],10);
										switch (true) {
											case (isNaN(parsedVal) || typeof parsedVal!='number') :
													error = '"'+origVal+'" is an invalid number.';
													break;
											case (col.unsigned && parsedVal<0):
													error = 'Negative values are not allowed for this field.';
													break;
											case (col.unsigned && op==OP_LESS && parsedVal==0):
													error = 'Values smaller than 0 are not allowed for this field.';
													break;
										}
										if (error) break;
										valArray[i] = parsedVal;
									}
									if (error) break;

									if (valArray.length==1 && op==OP_IN) it.op=OP_EQUALS;		// convert 'in (A)' to 'equals A'
									var numval = valArray.join(',');
									
									if (op==OP_BETWEEN && prop=='val2') {
										if (numval==it.val1) it.op = OP_EQUALS;	// convert 'between A and A' to 'equals A' 
										 else if (parseInt(numval)<parseInt(it.val1)) {
											// smaller number first
											var t = it.val1;
											it.val1 = numval;
											numval = t;
										 }
									}
									it[prop] = numval;
									break;

			case TYPE_STR:		it[prop] = val;
									if (!val.length && (op==OP_CONTAINS || op==OP_STARTS || op==OP_ENDS)) {
										error = 'Empty strings are not allowed here.';
										break;
									}
									if (val.indexOf('\\')!=-1) {
										error = 'The \\ character is not allowed!';
										break;
									}
									break;

			case TYPE_DATE:	if (op != OP_PERIODIC && (!(DATE_REGEXP.test(val) || /^DATE_/.test(val)))) {
										error = '"'+val+'" is an invalid date.';
										break;
									}

									if (op == OP_PERIODIC && prop=='val1')  {
										if (!/^\d+$/.test(val) && val != 'any') {
											error = "Box1: Enter an integer or 'any'!";
											break;
										}
									}
									
									if (op == OP_PERIODIC && prop=='val3')  {
										if (!/^\d+$/.test(val)) {
											error = "Box3: Enter an integer!";
											break;
										}
									}

									if (op==OP_BETWEEN && prop=='val2') switch (true) {
										//case !(op==OP_BETWEEN && prop=='val2'): break;
										
										case (op==OP_BETWEEN && val==it.val1): 
													it.op = OP_EQUALS;	// convert 'between A and A' to 'equals A' 
													break;
										
										default:	// prop==val2 AND op=OP_BETWEEN
													var date2 = RegExp.$1,
														 time2 = RegExp.$2;

													// get date/time from val1..
													DATE_REGEXP.test(it.val1);	
													var date1 = RegExp.$1,
														 time1 = RegExp.$2;
													
													// make sure both contain (no) time..
													if (time1 && !time2) val += DEFAULT_PADD_TIME;
													 else if (time2 && !time1) it.val1 += DEFAULT_PADD_TIME;
													
													// compare dates..
													if (val==it.val1) {
														it.op = OP_EQUALS;	// convert 'between A and A' to 'equals A' 
														break;
													}

													var	flat1 = parseInt(it.val1.replace(/[-:\s]/g,''),10),
															flat2 = parseInt(val.replace(/[-:\s]/g,''),10);
													if (flat2 < flat1) {
														// smaller date first
														var t = it.val1;
														it.val1 = val;
														val = t;
													 }
									} // switch

									it[prop] = val;
									break;
			default:				error = 'Unknown type ('+it.type+')';
		} // switch

		if (error) return false;
	});

	if (error) {
		alert(error);
		this.focusOpInput(1);
		return;
	}

	if (it.isVoid) {
		// item was newly created
		it.isVoid = false;
		var clone = justUpdatedItem = it.clone();	// clone to remove superfluous isVoid property
		it.parent.addItem(clone);
	} else if (!it.isVoid) {
		justUpdatedItem = it;
		it.parent.items.replace(this.origItem, it);
	}
	$.fn.FilterPopup.kill();
	updateFilterHTML();
}
