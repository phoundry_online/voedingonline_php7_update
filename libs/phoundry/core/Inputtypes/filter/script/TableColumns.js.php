<?php

/**
 *    This file has to be php-included inside a script tag, i.e.
 *        <script type="text/javascript">
 *            <? require('script/TableColumns.js.php'); ..>
 *        </script>
 *
 * NOT: <script src="..."><script> !
 *
 *    Assumes: - a mysql connection established already
 *                - variable $targetTable containing the name of the table the filter is operating on
 *                - FilterConstants.js.php MUST have been loaded before
 */
if (!isset($targetTable)) {
    exit();
}

// patterns for sprintf
$JS_LINES = array(
    'skipped' => '// Skipped field "%s" (%s) -> %s',
    'def' => "'%s': {%s}",
    'int' => "type: TYPE_NUM",
    'string' => "type: TYPE_STR",
    'datetime' => "type: TYPE_DATE",
    'date' => "type: TYPE_DATE",
    'nullable' => "nullable: %s",
    'unsigned' => "unsigned: %s"
);

define('JS_LTAB', "\t\t\t");

$query = "SELECT * FROM {$targetTable} WHERE false";
$result = mysql_query($query);

$fields = mysql_num_fields($result);
$validLines = array();
$invalidLines = array();

// generate definition lines..

for ($i = 0; $i < $fields; $i++) {
    $meta = mysql_fetch_field($result, $i);
    $fieldname = mysql_field_name($result, $i);

    // When DMdelivery stores email addresses encrypted, you can't filter on it:
    if (isset($DMDprefs, $DMDprefs['encryptEmailAddresses']) && $fieldname == 'email') {
        continue;
    }

    switch (true) {
        case (!$meta):
            $invalidLines[] = sprintf($JS_LINES['skipped'], $fieldname, $i, 'No metainfo');
            continue 2;
        /*	removed because metainfo may consider TEXT fields as BLOB
        case ($meta->blob):
            $invalidLines[] = sprintf($JS_LINES['skipped'], $fieldname, $i, 'BLOB');
            continue 2;
        */
    }

    switch ($meta->type) {
        case 'blob':
            $meta->type = 'string'; // treat blobs as type "TEXT"
        case 'string':
        case 'int':
        case 'datetime':
        case 'date':
            $args = array($JS_LINES[$meta->type]);
            if ($meta->unsigned) {
                $args[] = sprintf($JS_LINES['unsigned'], '1');
            }
            // Arjan: Re-enabled this on 2009-02-16
            if (!$meta->not_null) {
                $args[] = sprintf($JS_LINES['nullable'], '1');
            }
            $validLines[] = sprintf($JS_LINES['def'], $fieldname, join(', ', $args));
            break;
        default:
            $invalidLines[] = sprintf($JS_LINES['skipped'], $fieldname, $i, 'Unsupported type ("' . $meta->type . '").');
    }
}
mysql_free_result($result);
?>

// START: script/TableColumns.js.php


/**
 * Object containing available filter items with their "filter-internal" datatypes.
 * Table: <?= $targetTable ?>.
 */
var FILTER_COLS = {
<?php
foreach ($validLines as $i => $line) {
    echo JS_LTAB . $line;
    echo ($i < count($validLines) - 1) ? ",\n" : "\n";
}
foreach ($invalidLines as $line) {
    echo JS_LTAB . $line . "\n";
}
?>
};

<?php

/* static example:

var FILTER_COLS = {
                'id': {
                    type: TYPE_NUM,
                    nullable: false,
                    unsigned: true
                },
                'numfield':	{
                    type: TYPE_NUM,
                    nullable: true,
                    unsigned: false
                },
                'myname': {
                        type: TYPE_STR,
                        nullable: false
                },
                'title': {
                    type: TYPE_STR,
                    nullable: true
                },
                'date': {
                    type: TYPE_DATE,
                    nullable: true
                }
        };
*/
?>

// END: script/TableColumns.js.php
