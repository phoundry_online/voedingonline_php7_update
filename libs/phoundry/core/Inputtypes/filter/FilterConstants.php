<?php
	// group types..
	define('FI_GROUP_AND', 1);
	define('FI_GROUP_OR', 2);
	$GLOBALS['VALID_GROUP_TYPES'] = array(FI_GROUP_AND, FI_GROUP_OR);

	// item types..
	define('FI_TYPE_NUM', 1);
	define('FI_TYPE_STR', 2);
	define('FI_TYPE_DATE', 3);
	$GLOBALS['VALID_ITEM_TYPES'] = array(FI_TYPE_NUM, FI_TYPE_STR, FI_TYPE_DATE);

	// operations..
	define('FI_OP_EQUALS', 1);
	define('FI_OP_NULL', 2);
	define('FI_OP_LESS', 3);
	define('FI_OP_GREATER', 4);
	define('FI_OP_BETWEEN', 5);
	define('FI_OP_STARTS', 6);
	define('FI_OP_ENDS', 7);
	define('FI_OP_CONTAINS', 8);
	define('FI_OP_IN', 9);
	define('FI_OP_FINDINSET', 10);
	define('FI_OP_PERIODIC', 11);
	
	// which operations are available on which item types..
	$GLOBALS['VALID_NUM_OPS'] = array(FI_OP_NULL,  FI_OP_EQUALS, FI_OP_LESS, FI_OP_GREATER, FI_OP_BETWEEN, FI_OP_FINDINSET, FI_OP_IN);
	$GLOBALS['VALID_DATE_OPS'] = array(FI_OP_NULL, FI_OP_EQUALS, FI_OP_LESS, FI_OP_GREATER, FI_OP_BETWEEN, FI_OP_PERIODIC);
	$GLOBALS['VALID_STR_OPS'] = array(FI_OP_NULL,  FI_OP_EQUALS, FI_OP_STARTS, FI_OP_ENDS, FI_OP_CONTAINS, FI_OP_FINDINSET, FI_OP_IN);

	// number of values for each operation..
	$GLOBALS['FI_VALS_PER_OP'] = array(
		FI_OP_EQUALS =>	1,
		FI_OP_LESS =>		1,
		FI_OP_GREATER =>	1,
		FI_OP_BETWEEN =>	2,
		FI_OP_PERIODIC =>	4,
		FI_OP_STARTS =>	1,
		FI_OP_ENDS =>		1,
		FI_OP_CONTAINS => 1,
		FI_OP_FINDINSET => 1,
		FI_OP_NULL =>		0,
		FI_OP_IN =>			1
	);

	// bgcolors for group types..
	define('F_COLOR_OR', '#48992F');
	define('F_COLOR_AND', '#F05011');

	// regexp for validating dates.. (iso with optional time)
	define('FI_DATE_REGEXP', '/^(\d{4}-(?:0[1-9]|1[012])-(?:0[1-9]|[12][0-9]|3[01]))( (?:[01][0-9]|[2][0-4]):[0-5][0-9]:[0-5][0-9])?$/');
?>
