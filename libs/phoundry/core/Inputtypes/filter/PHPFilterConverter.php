<?php

require_once('FilterConstants.php');

class PHPFilterConverter {

   private $json, // utf8/iso-encoded json string
           $obj, // json_decoded object (~ js FilterGroup/FilterItem objects)
           $valueAdaptor, // an adaptor used to get a actual named value
           $prefix;
      
   var $error = false, // error string on conversion errors, otherwise FALSE
           $where = ''; // sql-WHERE expression after successful conversion

   /**
    * Constructs a new FilterConverter and processes the given JSON string.
    * @param string $json UTF8-encoded json string
    * @param string $valueReferenceAdaptor an adaptor used to get a value (since were building expressions that can be evaluated).
    * @param string $prefix The prefix (alias) to use for each part.    
    */

   function __construct(&$json, $valueAdaptor,  $prefix = '') {
      if (!is_string($json) || $json == null) {
         $this->where = '1 == 1';
         return;
      }
      
      $this->json = $json;
      $this->prefix = $prefix;
      $this->valueAdaptor = $valueAdaptor;

      // json->obj conversion..
      $this->obj = json_decode($this->json);

      // switch off error messages (to avoid a thousand isset()'s)
      $savedErrRep = error_reporting(0);

      if (!is_object($this->obj)) {
         $this->error = 'Invalid JSON.';
      } else {
         $this->where = $this->toPHPWhere();
      }
      
      error_reporting($savedErrRep);
   }
   
   private function getValueReference($fieldName){
      return call_user_func($this->valueAdaptor, $fieldName);
   }

   /**
    * Converts the decoded object into a PHP WHERE string.
    */
   private function toPHPWhere() {
      try {
         $where = $this->groupToPHP($this->obj);
         if (!$where)
            $where = '1==1';
         return $where;
      } catch (Exception $e) {
         $this->error = $e->getMessage();
         return '';
      }
   }

   /**
    * Converts a group into PHP expression syntax.
    * Empty groups are skipped.
    * @return string the PHP for the group or NULL if group was empty.
    */
   private function groupToPHP(&$g) {
      global $VALID_GROUP_TYPES;
      if (!in_array($g->group, $VALID_GROUP_TYPES))
         throw new Exception('Group->type invalid (' . $g->group . ').');
      if (!is_array($g->items))
         throw new Exception('Group->items invalid.');
      
      $itemPHP = array();
      foreach ($g->items as $itemOrGroup) {
         $newPHP = ($itemOrGroup->item) ? $this->itemToPHP($itemOrGroup) : $this->groupToPHP($itemOrGroup);
         if ($newPHP)
            $itemPHP[] = $newPHP;
      }

      if (count($g->items) == 0)
         return null; // skip empty group

      $opStr = ($g->group == FI_GROUP_AND) ? ' && ' : ' || ';
      $notStr = ($g->invert) ? ' !' : '';

      $pattern = $notStr . '(%s)';

      return sprintf($pattern, join($opStr, $itemPHP));
   }

   /**
    * Converts an item into PHP expression syntax.
    * @return string PHP expession for the item.
    */
   private function itemToPHP(&$item) {
      global $VALID_ITEM_TYPES;
      if (!is_string($item->item) || strlen($item->item) == 0)
         throw new Exception('Invalid field name in item.');
      if (!in_array($item->type, $VALID_ITEM_TYPES))
         throw new Exception('Item.type invalid.');

      if ($item->op == FI_OP_NULL)
         return $this->getNullExpr($item);

      // add opt. prefix..
      $item->item = $this->prefix . $item->item;

      switch ($item->type) {
         case FI_TYPE_NUM: return '('.$this->convertNumItem($item).')';
         case FI_TYPE_STR: return '('.$this->convertStringItem($item).')';
         case FI_TYPE_DATE: return '('.$this->convertDateItem($item).')';
      }
   }

   /**
    * Converts a numeric item into PHP expression syntax.
    * @param object $item
    * @return string sql expression
    */
   private function convertNumItem(&$item) {
      global $VALID_NUM_OPS;
      if (!in_array($item->op, $VALID_NUM_OPS))
         throw new Exception('Operation invalid for numeric item.');

      $vals = $this->getValidatedValues($item, 'validateNumber');
      $itemReference = $this->getValueReference($item->item);
      
      switch ($item->op) {
         case FI_OP_EQUALS: 
            $pat = ($item->invert) ? '%s!=%s' : '%s=%s';
            return sprintf($pat, $itemReference, $vals[0]);
         case FI_OP_LESS:
            $pat = ($item->invert) ? '%s>=%s' : '%s<%s';
            return sprintf($pat, $itemReference, $vals[0]);
         case FI_OP_GREATER: 
            $pat = ($item->invert) ? '%s<=%s' : '%s>%s';
            return sprintf($pat, $itemReference, $vals[0]);
         case FI_OP_BETWEEN: 
            $pat = ($item->invert) ? '%s < %s && %s > %s' : '%s >= %s && %s <= %s';
            return sprintf($pat, $itemReference, $vals[0], $vals[1], $item->item);
         case FI_OP_IN: 
            $pat = ($item->invert) ? 'in_array(%s, array(%s)) == false' : 'in_array(%s, array(%s))';
            return sprintf($pat, $itemReference, $vals[0]);
         default: throw Exception(sprintf('Operation (%s) not handled in FilterConverter::convertNumItem().', $item->op));
      }
   }

   /**
    * Converts a numeric item into PHP expression syntax.
    * Empty strings for are skipped for all ops but EQUALS.
    * @param object $item
    * @return string php expression or NULL if string was invalid for operation (e.g. empty string for LIKE expression)
    */
   private function convertStringItem(&$item) {      
      global $VALID_STR_OPS;
      if (!in_array($item->op, $VALID_STR_OPS))
         throw new Exception('Operation invalid for string item.');

      $vals = &$this->getValidatedValues($item, 'validateString');
      $itemReference = $this->getValueReference($item->item);
      
      switch ($item->op) {
         case FI_OP_EQUALS:            
            $format = "%sstrcmp(%s, %s) == 0";
            $not = ($item->invert) ? '!' : '';
            return sprintf($format, $not, $itemReference, $vals[0]);
         case FI_OP_STARTS:
            if ($vals[0] == '')
               return null;
            $format = "%sstrncmp(%s, %s, strlen(%s))===0";
            $not = ($item->invert) ? '!' : '';
            $search = $vals[0];
            return sprintf($format, $not, $itemReference, $search, $search);
         case FI_OP_ENDS:
            if ($vals[0] == '')
               return null;
            $format = "%sstrcmp(substr(%s, strlen(%s) - strlen(%s)),%s)===0";
            $not = ($item->invert) ? '!' : '';
            $search = $vals[0];
            return sprintf($format, $not, $itemReference, $itemReference, $search, $search);
         case FI_OP_CONTAINS: 
            if ($vals[0] == '')
               return null;
            $format = "%sstrstr(%s, '%s')";
            $not = ($item->invert) ? '!' : '';
            return sprintf($format, $not, $itemReference, $vals[0]);
         case FI_OP_FINDINSET: 
            if ($vals[0] == '')
               return null;
            return sprintf("in_array(%s, array(%s))", $itemReference, $vals[0]);
         case FI_OP_IN: 
            $pat = ($item->invert) ? 'in_array(%s, array(%s))==false' : 'in_array(%s, array(%s))';
            return sprintf($pat, $itemReference, $vals[0]);
         default: throw Exception(sprintf('Operation (%s) not handled in FilterConverter::convertNumItem().', $item->op));
      }
   }

   /**
    * Converts a numeric item into SQL expression syntax.
    * @param object $item
    * @return string sql expression
    */
   private function convertDateItem(&$item) {
      global $VALID_DATE_OPS;
      if (!in_array($item->op, $VALID_DATE_OPS))
         throw new Exception('Operation invalid for date item.');

      $vals = $this->getValidatedValues($item, 'validateDate');
      $notStr = ($item->invert) ? '!' : '';
      $itemReference = $this->getValueReference($item->item);
     
      
      switch ($item->op) {
         case FI_OP_EQUALS:
            $pat = ($item->invert) ? "strtotime(%s)!==%s" : "strtotime(%s)===%s";
            return sprintf($pat, $itemReference, $vals[0]);
         case FI_OP_LESS:
            return sprintf($notStr . "strtotime(%s) < %s", $itemReference, $vals[0]);
         case FI_OP_GREATER:
            return sprintf($notStr . "strtotime(%s) > %s", $itemReference, $vals[0]);
         case FI_OP_BETWEEN:
            return sprintf($notStr . "(strtotime(%s) > %s && (strtotime(%s) < %s))", $itemReference, $vals[0]);
         case FI_OP_PERIODIC:
            $result = "";
            
            // $vals = array(0=>2, 1=>'-', 2=>5, 3=>'month');
            $years = strtoupper($vals[0]);
            $unitManipulator = $vals[1];
            $unitCount = $vals[2];
            $unit = strtoupper($vals[3]);
            
            // Will resolve to (-|+)(\d.+)
            $unitCount = "{$unitManipulator}{$unitCount}"; 
            
            $fixedDateOperand = "(strtotime('{$unitCount} {$unit}', strtotime({$itemReference})))";
           
            if ($years === 'ANY'){           
               $result = "(date('m-d', strtotime({$fixedDateOperand}))) === date('m-d')";
            }
            else{
               if ($years === '')
                  $years = 0;
               $result = "(date('Y-m-d', strtotime('+{$years} years', {$fixedDateOperand}))) === date('Y-m-d')";
            }
            return $notStr . $result;

         default: throw Exception(sprintf('Operation (%s) not handled in FilterConverter::convertNumItem().', $item->op));
      }
   }  

   /**
    * Checks if the given parameter is a number and returns it.
    * @param $num numeric variable to validate
    * @return the validated number or NULL if no number
    */
   private function validateNumber(&$num) {
      return (is_numeric($num)) ? (int) $num : null;
   }

   /**
    * Checks if the given parameter is a string.
    * The returned string escaped
    * @param string $str the var to validate (and escape)
    * @return the sql-escaped string or NULL if no string
    */
   private function validateString(&$str) {
      // return null or a version of the string where all system reserved characters and single quotes are escaped
      return (is_string($str)) ? "'". addcslashes($str, '\1..\31\39'). "'" : null;
   }

   /**
    * Checks if the given parameter is a valid date.
    * @param string $str the datestring
    * @return the date string itself or NULL if not valid
    */
   private function validateDate(&$str) {
      return (is_string($str) && (preg_match(FI_DATE_REGEXP, $str) || preg_match('/^DATE_/', $str))) ? strtotime($str) : null;
   }

   /**
    * Takes an item + a validation function(name) and iterates over all val{x} properties of the item.
    * The validation function must take one parameter and return its validated value back OR NULL if invalid.
    * @param object $item
    * @param string $fnName function name
    * @return array of validated values
    */
   private function getValidatedValues(&$item, $fnName) {
      global $FI_VALS_PER_OP;
      $vals = $splitVals = array();
      $op = $item->op;
      for ($i = 1; $i <= $FI_VALS_PER_OP[$item->op]; $i++) {
         $prop = 'val' . $i;
         if (!isset($item->$prop))
            throw new Exception(sprintf('Missing property (%s) in "%s" item.', $prop, $item->item));

         $propVal = $item->$prop;

         if ($op == FI_OP_PERIODIC) {
            if ($prop == 'val1') {
               // Check for integer or 'any':
               if (!preg_match('/^\d+$/', $propVal) && $propVal != 'any') {
                  throw new Exception(sprintf('Invalid value (%s) of property %s in "%s" item.', $propVal, $prop, $item->item));
               }
            } elseif ($prop == 'val3') { 
               // Check for integer:
               if (!preg_match('/^\d+$/', $propVal)) {
                  throw new Exception(sprintf('Invalid value (%s) of property %s in "%s" item.', $propVal, $prop, $item->item));
               }
            }
         }

         // convert value of current property into an array (i.e. contains multiple values in case of 'IN' operation)
         $splitVals = ($op == FI_OP_IN) ? explode(',', $propVal) : array($propVal);

         if ($op != FI_OP_PERIODIC) {
            foreach ($splitVals as $p => $valToCheck) {
               $checkedVal = $this->$fnName($valToCheck);
               if (is_null($checkedVal))
                  throw new Exception(sprintf('Invalid value (%s) of property %s in "%s" item.', $valToCheck, $prop, $item->item));
               else if (!preg_match('/^DATE_/', $checkedVal)) {                             
                  $checkedVal = time();
               }
               $splitVals[$p] = $checkedVal;
            }
         }

         $vals[] = ($op == FI_OP_IN) ? join(',', $splitVals) : $splitVals[0];
      }
      return $vals;
   }

   /**
    * Returns an IS_NULL(xxx) string where xxx is the database field name.
    * @param object $item
    * @return string sql expression
    */
   private function getNullExpr(&$item) {
      $notPrefix = ($item->invert) ? '!' : '';
      return sprintf($notPrefix . '(%s==null)', $this->getValueReference($item->item));
   }
}

?>
