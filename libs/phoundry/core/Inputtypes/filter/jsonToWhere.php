<?php
	header('Content-Type: text/html; charset=utf-8');
	require('include/FilterConverter.php');

	// ------------------------------------------------------------------------------
?>

<html><body style="font:14px monospace">

<?php
	// fetch json string..
	$json = (isset($_POST['json']))? $_POST['json'] : '[]';

	// get converter instance..
	$fc = new FilterConverter($json, $useUTF8);

	// check errror..
	if ($fc->error) {
		echo 'Error: '.$fc->error."\n\n";
		echo "JSON was: \n".$json;
		exit;
	}

	// output WHERE expression..
	echo $fc->where;

?>

</body></html>
