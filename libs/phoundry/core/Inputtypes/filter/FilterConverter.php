<?php
	/**
	 * Class to convert a JSON string into a MySQL WHERE Expression.
	 * Note: The JSON string is expected to be UTF-8 encoded.
	 * Usage:
	 *		1. make instance (passing json string to contructor)
	 *		2. check {@link error} property
	 *		3. retrieve SQL expression from the {@link where} property
	 *
	 * REQUIRES PHP v5.2+!
	 */

	require_once('FilterConstants.php');

	class FilterConverter {

		private	$json,			// utf8/iso-encoded json string
					$obj,				// json_decoded object (~ js FilterGroup/FilterItem objects)
					$useUTF8,		// flag if where-string is supposed to be utf8
					$prefix;

		var	$error = false,	// error string on conversion errors, otherwise FALSE
				$where = '';		// sql-WHERE expression after successful conversion

		/**
		 * Constructs a new FilterConverter and processes the given JSON string.
		 * @param string $json UTF8-encoded json string
		 * @param bool $useUTF8 indicates if the generated WHERE-string is supposed to be UTF8 (TRUE) or ISO (FALSE)
		 * @param string $prefix The prefix (alias) to use for each part.
		 */
		function __construct(&$json, $useUTF8, $prefix = '') {
			if (!is_string($json) || $json==null) {
				$this->where = 'TRUE';
				return;
			}

			$this->useUTF8 = $useUTF8;
			$this->json = ($useUTF8)? $json : utf8_encode($json);
			$this->prefix = $prefix;

			// json->obj conversion..
			$this->obj = json_decode($this->json);

			// switch off error messages (to avoid a thousand isset()'s)
			$savedErrRep  = error_reporting(0);

			if (!is_object($this->obj)) {
				$this->error = 'Invalid JSON.';
			} else {
				$this->where = $this->toSQLWhere();
			}

			error_reporting($savedErrRep);
		}


		/**
		 * Converts the decoded object into a MySQL WHERE string.
		 */
		private function toSQLWhere() {
			try {
				$where = $this->groupToSQL($this->obj);
				if (!$where) $where = 'TRUE';
				return $where;
			} catch (Exception $e) {
				$this->error = $e->getMessage();
				return '';
			}
		}


		/**
		 * Converts a group into SQL expression syntax.
		 * Empty groups are skipped.
		 *
		 * @param stdClass $g
		 *
		 * @throws Exception if checks fail
		 * @return string the SQL for the group or NULL if group was empty.
		 */
		private function groupToSQL(&$g) {
			global $VALID_GROUP_TYPES;
			if (! isset($g->invert)) $g->invert = false;
			if (!in_array($g->group, $VALID_GROUP_TYPES)) throw new Exception('Group->type invalid ('.$g->group.').');
			if (!is_array($g->items)) throw new Exception('Group->items invalid.');

			$itemSQL = array();
			foreach ($g->items as $itemOrGroup) {
				$newSQL = ($itemOrGroup->item)? $this->itemToSQL($itemOrGroup) : $this->groupToSQL($itemOrGroup);
				if ($newSQL) $itemSQL[] = $newSQL;
			}

			if (count($g->items)==0) return null;	// skip empty group

			$opStr = ($g->group==FI_GROUP_AND)? ' AND ' : ' OR ';
			$notStr = ($g->invert)? ' NOT ' : '';

			$pattern = (count($itemSQL)>1)? ($notStr.'(%s)') : ($notStr.'%s');

			return sprintf($pattern, join($opStr,$itemSQL));
		}


		/**
		 * Converts an item into SQL expression syntax.
		 *
		 * @param stdClass $item
		 *
		 * @throws Exception if checks fail
		 * @return string SQL expession for the item.
		 */
		private function itemToSQL(&$item) {
			global $VALID_ITEM_TYPES;
			if (! isset($item->invert)) $item->invert = false;
			if (!is_string($item->item) || strlen($item->item)==0) throw new Exception('Invalid field name in item.');
			if (!in_array($item->type, $VALID_ITEM_TYPES)) throw new Exception('Item.type invalid.');

			if ($item->op==FI_OP_NULL) return $this->getNullExpr($item);

			// add opt. prefix..
			$item->item = $this->prefix.$item->item;

			switch ($item->type) {
				case FI_TYPE_NUM:		return $this->convertNumItem($item);
				case FI_TYPE_STR:		return $this->convertStringItem($item);
				case FI_TYPE_DATE:	return $this->convertDateItem($item);
			}
			return '';
		}


		/**
		 * Converts a numeric item into SQL expression syntax.
		 *
		 * @param object $item
		 *
		 * @throws Exception if checks fail or operation is not handled here
		 * @return string sql expression
		 */
		private function convertNumItem(&$item) {
			global $VALID_NUM_OPS;
			if (!in_array($item->op, $VALID_NUM_OPS)) throw new Exception('Operation invalid for numeric item.');

			//unset($item->val1);
			//$item->val1 = '-4.445';
			$vals = $this->getValidatedValues($item, 'validateNumber');

			switch ($item->op) {
				case FI_OP_EQUALS:	$pat = ($item->invert)? '%s!=%s' : '%s=%s';
											return sprintf($pat, $item->item, $vals[0]);
				case FI_OP_LESS:		$pat = ($item->invert)? '%s>=%s' : '%s<%s';
											return sprintf($pat, $item->item, $vals[0]);
				case FI_OP_GREATER:	$pat = ($item->invert)? '%s<=%s' : '%s>%s';
											return sprintf($pat, $item->item, $vals[0]);
				case FI_OP_BETWEEN:	$pat = ($item->invert)? '%s NOT BETWEEN %s AND %s' : '%s BETWEEN %s AND %s';
											return sprintf($pat, $item->item, $vals[0], $vals[1]);
				case FI_OP_IN:			$pat = ($item->invert)? '%s NOT IN (%s)' : '%s IN (%s)';
											return sprintf($pat, $item->item, $vals[0]);
				default: throw new Exception(sprintf('Operation (%s) not handled in FilterConverter::convertNumItem().', $item->op));
			}
		}


		/**
		 * Converts a numeric item into SQL expression syntax.
		 * Empty strings for are skipped for all ops but EQUALS.
		 *
		 * @param object $item
		 *
		 * @throws Exception if checks fail or operation is not handled here
		 * @return string sql expression or NULL if string was invalid for operation (e.g. empty string for LIKE expression)
		 */
		private function convertStringItem(&$item) {
			global $VALID_STR_OPS;
			if (!in_array($item->op, $VALID_STR_OPS)) throw new Exception('Operation invalid for string item.');

			$vals = &$this->getValidatedValues($item, 'validateString');

			switch ($item->op) {
				case FI_OP_EQUALS:	$pat = ($item->invert)? "%s!='%s'" : "%s='%s'";
											return sprintf($pat, $item->item, $vals[0]);
				case FI_OP_STARTS:
				case FI_OP_ENDS:
				case FI_OP_CONTAINS:	if ($vals[0]=='') return null;
											$LIKE_PATTERNS = array (
												FI_OP_STARTS => "%s %sLIKE '%s%%'",
												FI_OP_ENDS => "%s %sLIKE '%%%s'",
												FI_OP_CONTAINS => "%s %sLIKE '%%%s%%'"
											);
											$not = ($item->invert)? 'NOT ' : '';
											return sprintf($LIKE_PATTERNS[$item->op], $item->item, $not, str_replace('%','%%', $vals[0]));
				case FI_OP_FINDINSET: if ($vals[0]=='') return null;
											return sprintf("FIND_IN_SET('%s', %s)", $vals[0], $item->item);
				case FI_OP_IN:			$pat = ($item->invert)? '%s NOT IN (%s)' : '%s IN (%s)';
											$splitVals = explode(',', $vals[0]);
											foreach ($splitVals as $i => $str) {
												$splitVals[$i] = sprintf("'%s'",trim($str));
											}
											return sprintf($pat, $item->item, join(',',$splitVals));
				default: throw new Exception(sprintf('Operation (%s) not handled in FilterConverter::convertNumItem().', $item->op));
			}
		}


		/**
		 * Converts a numeric item into SQL expression syntax.
		 *
		 * @param object $item
		 *
		 * @throws Exception if checks fail or operation is not handled here
		 * @return string sql expression
		 */
		private function convertDateItem(&$item) {
			global $VALID_DATE_OPS;
			if (!in_array($item->op, $VALID_DATE_OPS)) throw new Exception('Operation invalid for date item.');

			$vals = $this->getValidatedValues($item, 'validateDate');
			$notStr = ($item->invert)? 'NOT ' : '';

			switch ($item->op) {
				case FI_OP_EQUALS:
					if (preg_match('/^DATE_/', $vals[0])) {
						$pat = ($item->invert)? "%s!=%s" : "%s=%s";
					}
					else {
						$pat = ($item->invert)? "%s!='%s'" : "%s='%s'";
					}
					return sprintf($pat, $item->item, $vals[0]);
				case FI_OP_LESS:
					if (preg_match('/^DATE_/', $vals[0])) {
						return sprintf($notStr."%s<%s", $item->item, $vals[0]);
					}
					else {
						return sprintf($notStr."%s<'%s'", $item->item, $vals[0]);
					}
				case FI_OP_GREATER:
					if (preg_match('/^DATE_/', $vals[0])) {
						return sprintf($notStr."%s>%s", $item->item, $vals[0]);
					}
					else {
						return sprintf($notStr."%s>'%s'", $item->item, $vals[0]);
					}
				case FI_OP_BETWEEN:
					$pattern = ($item->invert)? "%s NOT BETWEEN %s AND %s" : "%s BETWEEN %s AND %s";
					// Add quotes for ISO dates (YYYY-MM-DD), leave them out for DATE_ functions:
					if (!preg_match('/^DATE_/', $vals[0])) { $vals[0] = "'" . $vals[0] . "'"; }
					if (!preg_match('/^DATE_/', $vals[1])) { $vals[1] = "'" . $vals[1] . "'"; }
					return sprintf($pattern, $item->item, $vals[0], $vals[1]);
				case FI_OP_PERIODIC:
					// $vals = array(0=>2, 1=>'-', 2=>5, 3=>'month');
					$unit = strtoupper($vals[3]);
					$pattern = '';

					if ($vals[0] == 'any') {
						// Any Birthday
						$compareTo = 'CURDATE()';
						if ($vals[2] != 0) {
							$compareTo  = ($vals[1] == '-') ? 'DATE_ADD' : 'DATE_SUB';
							$compareTo .= "(CURDATE(), INTERVAL {$vals[2]} {$unit})";
						}
						$pattern = "(DATE_FORMAT({$item->item}, 'md') = DATE_FORMAT({$compareTo}, 'md'))";
					}
					elseif (is_numeric($vals[0]) && $vals[0] != 0) {
						// Exact birthday:
						$compareTo = 'CURDATE()';
						if ($vals[2] != 0) {
							$compareTo  = ($vals[1] == '-') ? 'DATE_ADD' : 'DATE_SUB';
							$compareTo .= "(CURDATE(), INTERVAL {$vals[2]} {$unit})";
						}
						$pattern = "(DATE({$item->item}) = DATE_SUB({$compareTo}, INTERVAL {$vals[0]} YEAR))";
					}
					elseif (is_numeric($vals[0]) && $vals[0] == 0) {// Year == 0
						$fn = ($vals[1] == '-') ? 'DATE_ADD' : 'DATE_SUB';
						$pattern = "(DATE({$item->item}) = $fn(CURDATE(), INTERVAL {$vals[2]} {$unit}))";
					}

					return $notStr . $pattern;

				default: throw new Exception(sprintf('Operation (%s) not handled in FilterConverter::convertNumItem().', $item->op));
			}
		}


		/**
		 * Checks if the given parameter is a number and returns it.
		 * @param string $num numeric variable to validate
		 * @return int|null the validated number or NULL if no number
		 */
		private function validateNumber(&$num) {
			return (is_numeric($num)) ? (int)$num : null;
		}


		/**
		 * Checks if the given parameter is a string.
		 * The returned string is mysql_escaped [AND possibly utf8-decoded depending on $useUTF8].
		 * @param string $str the var to validate (and escape)
		 * @return string|null the sql-escaped string or NULL if no string
		 */
		private function validateString(&$str) {
			$temp = ($this->useUTF8) ? $str : utf8_decode($str);
			return (is_string($str)) ? escDBquote($temp) : null;
		}

		/**
		 * Checks if the given parameter is a valid date.
		 * @param string $str the datestring
		 * @return string|null the date string itself or NULL if not valid
		 */
		Private function validateDate(&$str) {
			return (is_string($str) && (preg_match(FI_DATE_REGEXP, $str) || preg_match('/^DATE_/', $str))) ? $str : null;
		}


		/**
		 * Takes an item + a validation function(name) and iterates over all val{x} properties of the item.
		 * The validation function must take one parameter and return its validated value back OR NULL if invalid.
		 *
		 * @param object $item
		 * @param string $fnName function name
		 *
		 * @throws Exception if checks fail
		 * @return array of validated values
		 */
		private function getValidatedValues(&$item, $fnName) {
			global $FI_VALS_PER_OP;
			$vals = $splitVals = array();
			$op = $item->op;
			for ($i=1; $i<=$FI_VALS_PER_OP[$item->op]; $i++) {
				$prop = 'val'.$i;
				if (!isset($item->$prop)) throw new Exception(sprintf('Missing property (%s) in "%s" item.',$prop,$item->item));

				$propVal = $item->$prop;

				/*
				// Emiel: Exception for scheduler management
				if(preg_match('/^DATE_/', $propVal)) {
					return array($propVal);
				}
				*/

				if ($op == FI_OP_PERIODIC) {
					if ($prop == 'val1') {
						// Check for integer or 'any':
						if (!preg_match('/^\d+$/', $propVal) && $propVal != 'any') {
							throw new Exception(sprintf('Invalid value (%s) of property %s in "%s" item.',$propVal, $prop, $item->item));
						}
					}
					elseif ($prop == 'val3') {
						// Check for integer:
						if (!preg_match('/^\d+$/', $propVal)) {
							throw new Exception(sprintf('Invalid value (%s) of property %s in "%s" item.',$propVal, $prop, $item->item));
						}
					}
				}

				// convert value of current property into an array (i.e. contains multiple values in case of 'IN' operation)
				$splitVals = ($op==FI_OP_IN)? explode(',', $propVal) : array($propVal);

				if ($op != FI_OP_PERIODIC) {
					foreach ($splitVals as $p => $valToCheck) {
						$checkedVal = $this->$fnName($valToCheck);
						if (is_null($checkedVal)) throw new Exception(sprintf('Invalid value (%s) of property %s in "%s" item.',$valToCheck, $prop, $item->item));
						$splitVals[$p] = $checkedVal;
					}
				}

				$vals[] = ($op==FI_OP_IN)? join(',', $splitVals) : $splitVals[0];
			}
			return $vals;
		}


		/**
		 * Returns an IS_NULL(xxx) string where xxx is the database field name.
		 * @param object $item
		 * @return string sql expression
		 */
		private function getNullExpr(&$item) {
			$notPrefix = ($item->invert)? 'NOT ' : '';
			return sprintf($notPrefix.'ISNULL(%s)', $item->item);
		}
	}
