<?php
	/**
	 * AJAX service for testing filter JSON string
	 * Expects: - json (string) (POST) js-utf8-encoded (like \u1234)
	 *				- useUTF8 (true|false) (POST)
	 *				- op parameter (GET)
	 */
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");	// no caching!

	require('common.php'); 
	require('FilterConverter.php'); 

	$op = (isset($_GET["op"]))? $_GET["op"] : "test";

	$useUTF8 = (isset($_POST["useUTF8"]))? $_POST["useUTF8"] : false;
	$targetTable = (isset($_GET["targetTable"]))? $_GET["targetTable"] : false;

	$jsonUTF8 = (isset($_POST['json']))? $_POST['json'] : '[]';		// fetch utf8-encoded json string

	switch ($op) {
		/**
		 * Returns a JSON object containing the result of a sql select.
		 * Assumes: parameter 'json' in $_POST
		 * {hits:<number>, where:<string> [,error: <string>, json:<string>]}
		 * json is only included in case of error
		 */
		case 'test':
			// get converter instance..
			$fc = new FilterConverter($jsonUTF8, $useUTF8);

			$hits = 0;
			$where = '';
			$error = ($targetTable)? false : 'targetTable not defined.';
			if ($fc->error) {
				$error = $fc->error;
			} else {
				$where = $fc->where;
				
				// echo $where."\n";
				
				if ($useUTF8) {
					//mysql_set_charset('UTF8');
					mysql_query('SET NAMES utf8');
				}
				$query = "SELECT COUNT(*) FROM {$targetTable} WHERE ".$where;
				$result = mysql_query($query);
				if ($error = mysql_error()) {
					$error = 'DB Error: '.$error;
				} else {
					$row = mysql_fetch_row($result);
					$hits = $row[0];
				}
			}
			$reply = array("hits" => $hits,
								"where" => $where);
			if ($error) {
				$reply["error"] = $error;
			}
			// $reply["json"] = $jsonUTF8;
			echo json_encode($reply);
			break;

		default: echo "'unknown operation.'";
	}

?>
