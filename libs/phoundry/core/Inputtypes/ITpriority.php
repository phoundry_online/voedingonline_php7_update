<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITpriority extends Inputtype
{
	public $size, $maxlength = -1;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
			if (!empty($this->extra['maxlength']))
				$this->maxlength = $this->extra['maxlength'];
		}
	}

	/**
	 * Translate a database value into a human-readable format.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'search', 'insert', 'update' or 'view'
	 * @param $value(string) The database value
	 * @return A human-readable presentation of the database value
	 * @returns string
	 */
	public function getValue($page, $value, $keyval = null)
	{
		global $PHprefs;
		switch($page) {
			case 'search':
				$html = '<span class="ITpriority" data-colname="' . PH::htmlspecialchars($this->name) . '">';
				$html .= '<span class="icon moveUp ptr"><img class="icon ptr moveUp" src="' . $PHprefs['url'] . '/core/icons/control_play_blue_up.png" alt="' . $this->name . '" title="Up" /></span>';
				$html .= '<span class="icon moveDown ptr"><img class="icon ptr moveDown" src="' . $PHprefs['url'] . '/core/icons/control_play_blue_down.png" alt="' . $this->name . '" title="Down" /></span>';
				$html .= '&nbsp;<span class="prio">' . $value . '</span>';
				$html .= '</span>';
				return $html;
			default:
				return $value;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		// Get default value:
		$value = PH::htmlspecialchars(parent::getInputHtml($page, $value, $includes));

		$alt = $this->column->getJScheckAltTag();
		$html = '<input name="' . $this->name . '" type="text" alt="' . $alt . '" size="6" value="' . $value . '" />';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$url = $PHprefs['url'] . '/core/popups/priority.php?' . QS(1,"CID={$this->column->id}");
		return array('<a href="#" onclick="makePopup(event,400,240,\'' . escSquote(ucfirst(word(116)) . ' ' . strtolower(PH::langWord($this->column->description))) . '\',\'' . $url . '&amp;prio=\'+_D.forms[0][\'' . $this->name . '\'].value);return false">' . strtolower(word(116)) . ' ' . strtolower(PH::langWord($this->column->description)) . '</a>');
	}

	public function getExtra() 
	{
		$order = 'desc';
		$query = null;
		if (count($this->extra)) {
			if (isset($this->extra['order']))
				$order = $this->extra['order'];
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
		}
		$adminInfo = array();

		$adminInfo['order'] = array(
			'name'     => 'ITorder',
			'label'    => 'Order',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'How to show the priorities list?',
			'input'    => array('type'=>'select', 'options'=>array('asc'=>'low - high', 'desc'=>'high - low'), 'selected'=>$order)
		);

		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Priority-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query to display the existing priorities.',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'order'		=> (string)$_POST['ITorder'],
			'query'     => trim((string)$_POST['ITquery'])
		);

		return $extra;
	}

	function getExtraHelp($what) {
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<h2>Priorities query</h2>
For sorting <b>high - low</b>:
<pre>
SELECT prio, headline FROM news ORDER BY prio DESC
</pre>
For sorting <b>low - high</b>:
<pre>
SELECT prio, headline FROM news ORDER BY prio ASC
</pre>
<p>
The first SELECT field is interpreted as the priority field, all
other SELECT fields will be shown in the popup-window that lets
the user choose the priority.
</p>

EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		return $help;
	}


}
