<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITyoutube extends Inputtype
{

	public $size, $maxlength = -1;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
			if (!empty($this->extra['maxlength'])) {
				$this->maxlength = $this->extra['maxlength'];
			}
		}
	}
	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $PHprefs;

		// Get default value:
		$value = PH::htmlspecialchars(parent::getInputHtml($page, $value, $includes));

		$includes['jsFiles'][] = $PHprefs['url'] . '/core/csjs/ITyoutube.js';
	
		if (isset($this->extra['updatable']) && $this->extra['updatable'] == false && $page == 'update') {
			$html = '<input name="' . $this->name . '" type="hidden" value="' . $value . '" /><b>' . $value . '</b>';
		}
		else {
			
			$alt = $this->column->getJScheckAltTag();
			$html = '<input name="' . $this->name . '" type="text" alt="' .$alt. '" size="80" value="' .$value. '"';
			if ($this->maxlength != -1) {
				$html .= ' maxlength="' . $this->maxlength . '"';
			}
			if (isset($this->extra['onlyID']) && $this->extra['onlyID'] == 1) {
				$html .= ' onblur="stripYouTubeID(this)"';
			}
			$html .= ' />';
		}
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;
		$url = $PHprefs['url'] . '/core/popups/youtube.php?' . QS(1);
		return array('<a href="#" onclick="yid=_D.forms[0][\'' . $this->name . '\'].value;if(yid)makePopup(event,425,344,\'Youtube\',\'' . $url . '&amp;yid=\'+yid);return false">' . strtolower(word(9)) . '</a>');
	}

	public function getExtra() 
	{
		
		// Structure of $extra:
		// $extra = array(
		//    'size'=>20,
		//		'maxlength'=>50
		// );

		$maxlength = isset($this->column->DBprops['length']) ? $this->column->DBprops['length'] : '';
		$size = ($maxlength == '') ? 60 : min(60, $maxlength);
		$updatable = true;
		$onlyID = true;
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['maxlength']))
				$maxlength = $this->extra['maxlength'];
			if (isset($this->extra['updatable']))
				$updatable = $this->extra['updatable'];
			if (isset($this->extra['onlyID']))
				$onlyID = $this->extra['onlyID'];
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['onlyID'] = array(
			'name'     => 'ITonlyID',
			'label'    => 'Only clip ID',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Strip and only store the unique YouTube clip ID',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$onlyID)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false,
			'onlyID'	=> ($_POST['ITonlyID'] == 1) ? true : false
		);

		return $extra;
	}

}
