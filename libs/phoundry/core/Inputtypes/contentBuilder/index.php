<?php
global $PHprefs;
require_once(getenv('CONFIG_DIR') . '/PREFS.php');
require_once dirname(dirname(__DIR__)).'/include/common.php';

PH::getHeader('text/html');

?><!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Content Builder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= $PHprefs['url'] ?>/core/csjs/contentbuilder/contentbuilder.css" rel="stylesheet" />

    <style>
        #rte-toolbar.rte-side {
            top: 110px;
        }

        #selectedFontDetailsDiv {
            margin-left: 15px;
            position: fixed;
            top: 10px;
            font-family: Roboto,sans-serif;
        }

        #selectedFontDetailsDiv p {
            font-size: 12px;
            line-height:22px;
        }

        #selectedFontDetailsDiv strong {
            font-weight: bold;
        }

        #contentarea {  margin: 90px auto; max-width: 1050px; width:100%; padding:0 35px; box-sizing:border-box; }
        @media all and (max-width: 1080px) {
            #contentarea { margin:0; }
        }

        #divTool {
            height: auto;
            bottom: 28px;
        }

        #footerFrame {
            bottom: 0;
            height: 28px;
            padding: 0 18px;
            background: #E2E2E2;
            position: fixed;
            left: 0;
            right: 0;
        }

        #footerFrame input[type="button"] {
            background: #ffffff none repeat scroll 0 0;
            border: 1px solid #6e6f71;
            color: #6f7072;
            height: 22px;
            vertical-align: middle;
            cursor: pointer;
            font-size: 11px;
            font-weight: bold;
            padding: 0 6px;
            width: 80px;
        }

        #footerFrame input[type="button"]:hover {
            background: #717d27 none repeat scroll 0 0;
            color: #FFFFFF;
        }
    </style>
</head>
<body>

<div id="selectedFontDetailsDiv"><p></p></div>
<div id="contentarea"></div>
<div id="footerFrame">
    <table id="footerButtons" style="width: 100%;" cellspacing="0" cellpadding="2"><tr><td>
        <input type="button" data-okbutton value="<?= PH::htmlspecialchars(word(82)) ?>" />
    </td><td align="right">
        <input type="button" data-cancelbutton value="<?= PH::htmlspecialchars(word(46 /* Cancel */)) ?>" />
    </td></tr></table>

</div>

<script src="<?= $PHprefs['url'] ?>/core/csjs/contentbuilder/jquery.min.js"></script>
<script src="<?= $PHprefs['url'] ?>/core/csjs/contentbuilder/jquery-ui.min.js"></script>
<script src="<?= $PHprefs['url'] ?>/core/csjs/contentbuilder/contentbuilder.js"></script>

<script>
(function($) {
    /**
     *
     * @param {{
     *   html: string, stylesheets: array<string>, style: string,
     *   snippetFile: string, contentBuilderUrl: string,
     *   fileselect: string, imageselect: string,
     *   colors: array<string>, containerClass: string
     * }} options
     * @param {function(?Error, string|boolean=)} cb
     */
    window.initContentBuilder = function (options, cb) {
        options = options || {};
        if (!options.hasOwnProperty('html')) {
            cb(new Error('Missing html'));
            return;
        }
        if (!options.hasOwnProperty('snippetFile')) {
            cb(new Error('Missing snippetFile'));
            return;
        }

        if ('stylesheets' in options) {
            $.each(options.stylesheets, function () {
                $('head').append($('<link rel="stylesheet" >').attr('href', this));
            });
        }
        if ('style' in options) {
            $('head').append($('<style>').html(options.style));
        }

        var $contentarea = $("#contentarea");
        $contentarea.contentbuilder({
            snippetFile: options.snippetFile,
            colors: options.colors,
            snippetOpen: true,
            toolbar: 'left',
            imageselect: options.imageselect.replace(/__CALLBACK__/g, 'parent.setUrl'),
            fileselect: options.fileselect.replace(/__CALLBACK__/g, 'parent.setUrl'),
            embedOriginalChecked: false,
            hideEmbedOriginal: true,
            imageEmbed: false
        });
        if ('containerClass' in options) {
            $contentarea.addClass(options.containerClass);
        }

        $contentarea.data('contentbuilder').loadHTML(options.html);

        $('[data-okbutton]').click(function(e) {
            e.preventDefault();
            var html = $contentarea.data('contentbuilder').html();
            cb(null, html);
        });
        $('[data-cancelbutton]').click(function(e) {
            e.preventDefault();

            cb(null, false);
        });
    };


    function setUrl(url) {
        // Set selected URL
        var inp = $('#active-input').val();
        $('#' + inp).val(url);
        killPopup();
    }

    function killPopup() {
        if ($('#md-fileselect').data('simplemodal')) $('#md-fileselect').hide();
        if ($('#md-imageselect').data('simplemodal')) $('#md-imageselect').hide();
        $('.md-overlay').remove();
    }

    $("#contentarea").on("mousedown", function () {
        $(this).one("mouseup", function () {
            $('#selectedFontDetailsDiv').html('<p><strong>SELECTED FONT-FAMILY:</strong> <br/ >' + getSelectedFontFamily() + '<br /><strong>SELECTED FONT-SIZE:</strong> <br/ >' + getSelectedFontSize() + '</p>');
        });
    });

    function getComputedStyleProperty(el, propName) {
        if (window.getComputedStyle) {
            return window.getComputedStyle(el, null)[propName];
        } else if (el.currentStyle) {
            return el.currentStyle[propName];
        }
    }

    function getSelectedElement() {
        var containerEl, sel;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.rangeCount) {
                containerEl = sel.getRangeAt(0).commonAncestorContainer;
                // Make sure we have an element rather than a text node
                if (containerEl.nodeType == 3) {
                    containerEl = containerEl.parentNode;
                }
            }
        } else if ( (sel = document.selection) && sel.type != "Control") {
            containerEl = sel.createRange().parentElement();
        }

        return containerEl;
    }

    function getSelectedFontFamily() {
        var containerEl = getSelectedElement()
        if (containerEl) {
            return getComputedStyleProperty(containerEl, "fontFamily");
        }
    }

    function getSelectedFontSize() {
        var containerEl = getSelectedElement()
        if (containerEl) {
            return getComputedStyleProperty(containerEl, "fontSize");
        }
    }

    window.setUrl = setUrl;
    window.killPopup = killPopup;
})(jQuery);
</script>
</body>
</html>
