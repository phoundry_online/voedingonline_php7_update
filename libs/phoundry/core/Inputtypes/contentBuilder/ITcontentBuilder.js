(function ($, srcDoc) {
    window.ITcontentBuilder = ITcontentBuilder;

    /**
     *
     * @param {{
     *   html: string, stylesheets: array<string>, style: string,
     *   snippetFile: string, contentBuilderUrl: string,
     *   fileselect: string, imageselect: string,
     *   colors: array<string>, containerClass: string
     * }} options
     * @constructor
     */
    function ITcontentBuilder(options) {
        this._options = options;
        this._state = {
            html: options.html
        };
    }

    ITcontentBuilder.prototype = {
        /**
         * Opens a content builder popup to edit the html
         *
         * @param {function(?Error, string=)} cb
         */
        openContentBuilder: function (cb) {
            var popup = $.inlineWindow({
                windowTitle: 'Content Builder',
                url: this._options.contentBuilderUrl,
                modal: true,
                maximize: false,
                maximizeButton: true,
                closeButton: false,
                statusBar: true,
                draggable: true,
                resizeable: true,
                width: -40,
                height: -40
            });

            var error = null;
            var self = this;

            popup.node.on('close.inlineWindow', function (e) {
                cb(error, self._state.html);
            });

            $(popup.content.find('iframe').get(0).contentWindow).one('load', function (e) {
                if (!this.initContentBuilder) {
                    popup.close();
                    console.error('Error while opening contentbuilder. Could not find initContentBuilder function on window');
                    return;
                }
                var options = $.extend(true, {}, self._options);
                options.html = self._state.html;
                this.initContentBuilder(options, function (err, html) {
                    error = err;
                    if (html !== false) {
                        self._state.html = html;
                    }
                    popup.close();
                });
            });
        },

        getPreviewHtml: function(html) {
            var $head = $('<head></head>');

            if ('stylesheets' in this._options) {
                $.each(this._options.stylesheets, function () {
                    $head.append($('<link rel="stylesheet" >').attr('href', this));
                });
            }
            if ('style' in this._options) {
                $head.append($('<style>').html(this._options.style));
            }
            if ('containerClass' in this._options) {
                html = '<div class="'+this._options.containerClass+'">' + html + '</div>';
            }

            return [
                '<!DOCTYPE HTML>', '<html>', '<head>',
                '<meta http-equiv="content-type" content="text/html; charset=utf-8" />',
                $head.html(),
                '</head>', '<body>',
                html,
                '</body>', '</html>'
            ].join('\n');
        }
    };

    $(function() {
        $('fieldset.field:has(div[data-contentbuilder])').each(function() {
            var fieldset = $(this);
            var options = fieldset.find('div[data-contentbuilder]').data('contentbuilder');
            var html = fieldset.find('input').val();
            options.html = html;
            var contentBuilder = new ITcontentBuilder(options);
            srcDoc.set(fieldset.find('iframe').get(0), contentBuilder.getPreviewHtml(html));

            fieldset.find('[data-contentbuilder-open]').click(function() {
                contentBuilder.openContentBuilder(
                    function(err, html) {
                        if (err) {
                            console.error(''+err, err);
                            return;
                        }
                        fieldset.find('input').val(html);
                        srcDoc.set(fieldset.find('iframe').get(0), contentBuilder.getPreviewHtml(html));
                    }
                );
            });
        });
    });
})(jQuery, srcDoc);