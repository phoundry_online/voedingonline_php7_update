<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";
require_once "{$PHprefs['distDir']}/core/include/sqlParser/Parser.php";
require_once "{$PHprefs['distDir']}/core/include/sqlParser/Compiler.php";

class ITrecursiveselect extends Inputtype
{
	public $value, $query, $queryFields, $nullQuery, $fromTable;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);

			if (isset($this->extra['query']) && isset($this->extra['parent'])) {
				$this->initQuery();
			}
		}
	}

	private function initQuery() 
	{
		// Insert 'WHERE parent IS NULL' into query:
		$parser = new SQL_Parser();
		$sqlStruct = $parser->parse(PH::evaluate($this->extra['query']));

		$insWhere = array(
			'arg_1' => array('value'=>$this->extra['parent'], 'type'=>'ident'),
			'op' => 'is',
			'arg_2' => array('value'=>'', 'type'=>'null')
		);

		if (!isset($sqlStruct['where_clause']))
			$sqlStruct['where_clause'] = $insWhere;
		else {
			$sqlStruct['where_clause'] = array(
				'arg_1' => $insWhere,
				'op' => 'and',
				'arg_2' => $sqlStruct['where_clause']
			);
		}

		$compiler = new SQL_Compiler();
		$this->nullQuery = $compiler->compile($sqlStruct);

		$this->fromTable = $sqlStruct['table_names'][0];
	}

	public function getValue($page, $value, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				$html = '';
				$sql = PH::evaluate($this->extra['query']);
				// Database query:
				$cur = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				for ($x = 0; !$db->EndOfResult($cur); $x++) {
					if ($db->FetchResult($cur,$x,$this->extra['optionvalue']) == $value) {
						foreach($this->extra['searchtext'] as $text)
							$html .= $db->FetchResult($cur,$x,$text) . ' ';
						break;
					}
					$html = trim($html);
				}
				return $html;

			default:
				return $value;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;

		$alt = $this->column->getJScheckAltTag();
		$html = '<select name="' . $this->name . '" alt="' . $alt . '">';
		if (!$this->column->required)
			$html .= '<option value="">[' . word(57) . ']</option>';

		$html .= $this->getOptions($page, $value);

		$html .= '</select>';
		return $html;
	}

	/**
	 * Return the OPTION's for a recursive SELECT.
	 *
	 * @author Arjan Haverkamp
	 * @param $selected(string) The value of the OPTION to make SELECTED.
	 * @param $curid(string) The current value for the key.
	 * @param $depth(int) The current depth of the recursive SELECT.
	 * @return A string containing HTML-code for a recursive SELECT.
	 * @returns string
	 * @private
	 */
	public function getOptions($page, $selected = -1, $curid = '', $depth = 0) 
	{
		global $db;

		$keyval = ($page == 'update') ? $_GET['RID'] : '';

		$html = '';
		$sql = $this->nullQuery;
		if ($curid !== '')
			$sql = str_replace('is null', "= '$curid'", $sql);
		$sql = PH::evaluate($sql);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$nextid = $db->FetchResult($cur,$x,$this->extra['optionvalue']);
			if (!($this->fromTable == $this->column->tableName && $keyval == $nextid)) {
				$html .= '<option value="' . $nextid . '"' . ($nextid == $selected ? ' selected="selected"' : '') . '>';
				for ($y = 0; $y < $depth; $y++) $html .= '-';
				foreach($this->extra['optiontext'] as $text)
					$html .= PH::htmlspecialchars($db->FetchResult($cur,$x,$text)) . ' ';
				$html .= "</option>\n";
			}
			$html .= $this->getOptions($page, $selected, $nextid, $depth+1);
		}

		return $html;
	}

	public function getExtra() 
	{
		$query = null;
		$queryFields = null;
		if (count($this->extra)) {
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
			if (isset($this->extra['optionvalue'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'parent'      => $this->extra['parent'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
		}
		$adminInfo = array();
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query for the options in the select box (<a href="#" onclick="makePopup(null,300,200,\'Select Query\',\'recursiveSelectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);
		$adminInfo['queryFields'] = array(
			'name'     => 'ITqueryFields',
			//'required' => true,
			//'syntax'   => 'text',
			'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (preg_match('/^(SELECT|SHOW)/i', $query)) {
			if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
				$msg = 'Option value, option text and search text are not defined!';
				return false;
			}
		}

		$struct = PH::javascript2php($_POST['ITqueryFields']);
		$extra = array(
			'query'     => $query
		);
		if (isset($struct['optionvalue']))
			$extra['optionvalue'] = (string)$struct['optionvalue'];
		if (isset($struct['parent']))
			$extra['parent'] = (string)$struct['parent'];
		if (isset($struct['optiontext']))
			$extra['optiontext'] = $struct['optiontext'];
		if (isset($struct['searchtext']))
			$extra['searchtext'] = $struct['searchtext'];

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<p>
You need to define a 'lookup' query, some SQL that generates a hierarchical
list of options. Make sure the <tt>SELECT</tt> clause of the query contains
both the record-id and the parent-id, and at least one other field to display
in the option tree as the option-text.
</p>
<pre>
SELECT id, name, parent_id FROM site_menu ORDER BY order_by
</pre>
<p>
Do not forget to click 'edit' in order to determine what fields in the
query defined here mean what!

EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		return $help;
	}
}
