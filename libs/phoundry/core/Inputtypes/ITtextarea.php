<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITtextarea extends Inputtype
{
	public $maxlength = -1;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column))
			parent::__construct($column);
			if (!empty($this->extra['maxlength'])) {
				$this->maxlength = $this->extra['maxlength'];
			}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		// Get default value:
		//$value = parent::getInputHtml($page, evaluate($value), $includes);
		if ($value === '' && $page == 'insert') {
			$value = $this->column->datatype->getDefaultValue();
		}

		if (isset($this->extra['updatable']) && $this->extra['updatable'] == false && $page == 'update') {
			return '<input name="' . $this->name . '" type="hidden" value="' . PH::htmlspecialchars($value) . '" />' . $value;
		}

		$alt = $this->column->getJScheckAltTag();
		$style = $cols = $rows = '';
		if (preg_match('/(px|%)$/', $this->extra['rows'])) {
			$style .= 'height:' . $this->extra['rows'] . ';';
		}
		else {
			$rows = $this->extra['rows'];
		}
		if (preg_match('/(px|%)$/', $this->extra['cols'])) {
			$style .= 'width:' . $this->extra['cols'] . ';';
		}
		else {
			$cols = $this->extra['cols'];
		}
		if ($this->column->datatype->extra['display'] == 'code') {
			$style .= 'font-family:Courier New,Courier';
		}
		$html = '<textarea id="' . $this->name . '" name="' . $this->name . '" alt="' . $alt . '"';
		if ($this->maxlength != -1)
			$html .= ' maxlength="' . $this->maxlength . '"';

		if (!empty($cols)) {
			$html .= ' cols="' . $cols . '"';
		}
		if (!empty($rows)) {
			$html .= ' rows="' . $rows . '"';
		}
		if (!empty($style)) {
			$html .= ' style="' . $style . '"';
		}
		if (($pattern = $this->column->datatype->getPattern()) !== '') {
			$html .= ' data-pattern="' . PH::htmlspecialchars($pattern) . '"';
		}

		if (isset($this->extra['counter']) && $this->extra['counter']) {
			$html .= ' onkeyup="showCharCount(this)"';
		}
		$html .= '>';
		if ($value !== '')
			$html .= PH::htmlspecialchars($value);
		$html .= '</textarea>';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$helpers = array();

		if ($this->extra['preview']) {
			$url = $PHprefs['url'] . '/core/popups/preview.php?' . QS(1,"field={$this->name}&type=textarea");
	      $helpers[] = '<a href="' . $url . '" tabindex="1" class="popupWin" title="' . ucfirst(word(49)) . '|300|200|0">' . word(49) . '</a>';
		}

		if (isset($this->extra['counter']) && $this->extra['counter']) {
			$helpers[] = '<span id="' . $this->name . '_cOuNtEr"></span>';
		}

		return $helpers;
   }
	
	public function getExtra() 
	{
		// Structure of $extra:
		// $extra = array(
		//    'cols'=>20,
		//		'rows'=>10,
		//		'maxlength'=>50,
		//		'value'=>'my value',
		//    'preview'=>true|false
		// );

		$maxlength = isset($this->column->DBprops['length']) ? $this->column->DBprops['length'] : '';
		$cols      = 60;
		$rows      = 10;
		$preview   = false;
		$counter   = false;
		$updatable = true;
		if (count($this->extra)) {
			if (isset($this->extra['rows']))
				$rows = $this->extra['rows'];
			if (isset($this->extra['cols']))
				$cols = $this->extra['cols'];
			if (isset($this->extra['maxlength']))
				$maxlength = $this->extra['maxlength'];
			if (isset($this->extra['preview']))
				$preview = $this->extra['preview'] ? 1 : 0;
			if (isset($this->extra['updatable']))
				$updatable = $this->extra['updatable'];
			if (isset($this->extra['counter']))
				$counter = $this->extra['counter'] ? 1 : 0;
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['cols'] = array(
			'name'     => 'ITcols',
			'label'    => 'Columns',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the nr. of columns in the textarea',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$cols)
		);
		$adminInfo['rows'] = array(
			'name'     => 'ITrows',
			'label'    => 'Rows',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the nr. of rows in the textarea',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$rows)
		);
		$adminInfo['maxlength'] = array(
			'name'     => 'ITmaxlength',
			'label'    => 'Max. length',
			'required' => false,
			'syntax'   => 'integer',
			'info'     => 'Enter the maximum input-length of the textarea',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$maxlength)
		);
		$adminInfo['preview'] = array(
			'name'     => 'ITpreview',
			'label'    => 'HTML preview?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Does this textarea need a preview-link?',
			'input'    => array('type'=>'select', 'options'=>array(0=>'no',1=>'yes'), 'selected'=>$preview)
		);
		$adminInfo['counter'] = array(
			'name'     => 'ITcounter',
			'label'    => 'Character counter?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Do you want to show a character counter?',
			'input'    => array('type'=>'select', 'options'=>array(0=>'no',1=>'yes'), 'selected'=>$counter)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false,
			'cols'      => trim($_POST['ITcols']),
			'rows'      => trim($_POST['ITrows']),
			'maxlength' => trim($_POST['ITmaxlength']),
			'preview'   => ($_POST['ITpreview'] == 1) ? true : false
		);

      if (isset($_POST['ITcounter'])) {
         $extra['counter'] = ($_POST['ITcounter'] == 1) ? true : false;
      }

		return $extra;
	}
}
