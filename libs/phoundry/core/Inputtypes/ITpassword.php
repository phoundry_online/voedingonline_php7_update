<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITpassword extends Inputtype
{
	public $size, $maxlength = -1, $value;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) 
		{
			parent::__construct($column);
			if (!empty($this->extra['maxlength'])) 
			{
				$this->maxlength = $this->extra['maxlength'];
				if (!empty($this->column->datatype->extra['encoding'])) 
				{
					if ($this->column->datatype->extra['encoding'] == 'md5') 
					{
						$this->maxlength += 4;
					}
				}
			}
		}
	}

	public function getValue($page, $value, $keyval = null)
	{
		switch($page) 
		{
			case 'view':
			case 'search':
			case 'export':
			case 'xml':
				return '********';
			case 'update':
				return '[Dummy|0|Password]'; // Dummy password, see also DTstring.php
			default:
				return $value;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		// Get default value:
		$value = parent::getInputHtml($page, $value, $includes);

		$alt = $this->column->getJScheckAltTag();
		$html = '<input' . (!isset($this->extra['showStrength']) || $this->extra['showStrength'] ? ' class="checkStrength"' : '') . ' name="' . $this->name . '" type="password" autocomplete="off" alt="' . $alt . '" size="' . $this->extra['size'] . '" value="' . PH::htmlspecialchars($value) . '"';
		if ($this->maxlength != -1) {
			$html .= ' maxlength="' . $this->maxlength . '"';
		}
		$html .= ' /><br /><b' . ($this->column->required ? ' class="req"' : '') . '>' . word(62) . '</b><br />';
		$html .= '<input name="' . $this->name . '_cNfRm_" type="password" autocomplete="off" alt="" size="' . $this->extra['size'] . '" value="' . PH::htmlspecialchars($value) . '"';
		if ($this->maxlength != -1) {
			$html .= ' maxlength="' . ($this->maxlength+6) . '"';
		}
		if (($pattern = $this->column->datatype->getPattern()) !== '') {
			$html .= ' data-pattern="' . PH::htmlspecialchars($pattern) . '"';
		}
		$html .= ' />';

		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$url = $PHprefs['url'] . '/core/popups/password.php?' . QS(1,"field={$this->name}&desc=" . urlencode(PH::langWord($this->column->description)));
		return array('<a href="' . $url . '" tabindex="1" class="popupWin" title="' . ucfirst(word(175)) . '|320|200|0">' . strtolower(word(175)) . '</a>');
	}

	public function getExtra() 
	{
		// Structure of $extra:
		// $extra = array(
		//    'size'=>20,
		//		'maxlength'=>50,
		//		'showStrength'=>1
		// );

		$maxlength = isset($this->column->DBprops['length']) ? $this->column->DBprops['length'] : '';
		$size = ($maxlength == '') ? 60 : min(60, $maxlength);
		$showStrength = true;
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['maxlength']))
				$maxlength = $this->extra['maxlength'];
			if (isset($this->extra['showStrength']))
				$showStrength = $this->extra['showStrength'] ? true : false;
		}
		$adminInfo = array();
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the password-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['maxlength'] = array(
			'name'     => 'ITmaxlength',
			'label'    => 'Max. length',
			'required' => false,
			'syntax'   => 'integer',
			'info'     => 'Enter the maximum input-length of the password-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$maxlength)
		);
		$adminInfo['showStrength'] = array(
			'name'     => 'ITshowStrength',
			'label'    => 'Show password strength?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Do you want to show a password strength indicator?',
			'input'    => array('type'=>'select', 'options'=>array(0=>'no',1=>'yes'), 'selected'=>$showStrength)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'size'      => (int)$_POST['ITsize'],
			'maxlength' => trim($_POST['ITmaxlength'])
		);

		if (isset($_POST['ITshowStrength'])) {
			$extra['showStrength'] = ($_POST['ITshowStrength'] == 1) ? true : false;
		}

		return $extra;
	}
}
