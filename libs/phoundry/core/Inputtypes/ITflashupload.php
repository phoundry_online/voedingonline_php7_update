<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITflashupload extends Inputtype
{
	public function __construct(&$column) {
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		// Get default value:
		$value = parent::getInputHtml($page, $value, $includes);
		return '<input readonly="readonly" name="' . $this->name . '" type="text" size="60" value="' . PH::htmlspecialchars($value) . '" />' . "\n";
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$helpers = array();
		$url = $PHprefs['url'] . '/core/popups/flashupload.php?' . QS(1,"name={$this->name}");
		$helpers[] = '<a href="#" tabindex="1" onclick="makePopup(event,600,415,\'' . escSquote(ucfirst(word(160))) . '\',\'' . $url . '&amp;file=\'+escape(_D.forms[0][\'' . $this->name . '\'].value));return false">browse</a>';

		return $helpers;
	}
}
