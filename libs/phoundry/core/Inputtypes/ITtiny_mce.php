<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITtiny_mce extends Inputtype
{
	public function __construct(&$column)
	{
		global $PHprefs, $PHSes, $PRODUCT;
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}

		$supportedLangs = array('nl'=>'nl', 'cn'=>'zh', 'de'=>'de', 'sv'=>'sv');
		$lang = isset($supportedLangs[$PHSes->lang]) ? $supportedLangs[$PHSes->lang] : 'en';

		$configPREFS = isset($PHprefs['tinyMCEconfig']) ? $PHprefs['tinyMCEconfig'] : '{}';
		$configPREFS = $this->parseConfigPrefsVariables($configPREFS);

		$spellLangs = array('English=en', 'Nederlands=nl', 'Deutsch=de', 'Espa\u00F1ol=es', 'Fran\u00E7ais=fr', 'Italiano=it', 'Norsk=no', 'Portugu\u00EAs=pt', 'Suomi=fi', 'Svenska=sv');
		foreach($spellLangs as $i=>$spellLang) {
			if (substr($spellLang, -2) == $PHSes->lang) {
				$spellLangs[$i] = '+' . $spellLang;
			}
		}
		$spellchecker_languages = implode(',', $spellLangs);

		$cid = isset($this->column->id) ? $this->column->id : 0;
        $this->tinyMCEconfig =<<<EOC
var cfg = {$configPREFS};

try {
    if (cfg.preInit) {
    	if (typeof cfg.preInit === 'string') {
			try { cfg.preInit = eval('(' + cfg.preInit + ')') } catch (err) { console.log(err) }        		
        }
        
        if (typeof cfg.preInit === 'function') {
        	cfg.preInit();
        }
    }
} catch(e) {
    console && console.log(e);
}

tinyMCE.init($.extend(true,{
	tiny_mceDir: '{$PHprefs['url']}/core/Inputtypes/tiny_mce',
	language : '{$lang}',
	mode : 'exact',
	elements : '{$this->name}',
	button_tile_map: true,
	table_inline_editing: true,
	relative_urls: false,
	convert_urls: false,
	doctype: '{\$doctype}',
	plugins : 'WPphoundry,advhr,advimage,advlink,contextmenu,directionality,fullscreen,inlinepopups,layer,media,nonbreaking,paste,preview,print,searchreplace,spellchecker,style,table',
	//paste_auto_cleanup_on_paste : false,
	spellchecker_languages : '{$spellchecker_languages}',
	theme : 'advanced',
	skin : 'phoundry',
	skin_variant: 'silver',
	theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect',
	theme_advanced_buttons2 : 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,|,cleanup,spellchecker,|,help,WPcode,preview,|,forecolor,backcolor',
	theme_advanced_buttons3 : 'tablecontrols,|,sub,sup,|,charmap,emotions,media,advhr,|,print,|,ltr,rtl,|,nonbreaking,|,visualaid,fullscreen,WPsize',
	theme_advanced_link_intern_dialog: '/phoundry/custom/brickwork/link_intern/',
	theme_advanced_toolbar_location : 'top',
	theme_advanced_toolbar_align : 'left',
	theme_advanced_statusbar_location : 'bottom',
	theme_advanced_resizing : true,
	file_browser_callback: 'PHfileBrowser',
	accessibility_focus: false,
	add_unload_trigger: false,
	entity_encoding: 'named',
	entities:'160,nbsp,38,amp,34,quot,162,cent,8364,euro,163,pound,165,yen,169,copy,174,reg,8482,trade,8240,permil,60,lt,62,gt,8804,le,8805,ge,176,deg,8722,minus',
	allow_html_in_named_anchor: true,
	content_css: '{$PHprefs['url']}/core/Inputtypes/tiny_mce/tiny_mce.css',
	class_filter: function(cls,rule) {
		if (/editable(-(text|html))?/.test(cls)) return false;
		return cls;
	},
	setup : function(ed) {
		ed.onKeyUp.add(function() { formIsDirty = true; });
	},
	phoundry: {
		media_browser : '{$PHprefs['url']}/core/popups/media_browser/',
		cid : {$cid},
		image: {
			folder: '{\$imageDir}',
			uploadExtensions: 'png,gif,jpg,jpeg',
			selectExtensions: 'png,gif,jpg,jpeg'
		},

		attachment: {
			folder: '{\$attachDir}'
		}
	}
},
{$configPREFS},
{\$localConfig}
));
EOC;

	}

	public function getInputHtml($page, $value = '', &$includes)
	{
		global $PHprefs;

		if ($value === '' && $page == 'insert') {
			$value = $this->column->datatype->getDefaultValue();
		}

		if (isset($this->extra['updatable']) && $this->extra['updatable'] == false && $page == 'update') {
			return '<input name="' . $this->name . '" type="hidden" value="' . PH::htmlspecialchars($value) . '" />' . $value;
		}

		$doctype = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'; // TinyMCE's default doctype
		//$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'; // TinyMCE's default doctype
		
		if ($value != null && isset($reg) && $reg != null && preg_match('/<\!doctype([^>]*)>/i', $value, $reg)) {
			$doctype = preg_replace("/\s+/", ' ', $reg[0]);
		}

		$mceVersion = filemtime($PHprefs['distDir'] . '/core/Inputtypes/tiny_mce/tiny_mce.js');

		$includes['jsFiles'][strtolower(get_class($this)).'_1'] = $PHprefs['url'] . '/core/Inputtypes/tiny_mce/tiny_mce_gzip.js?' . $mceVersion;

		$imageDir  = empty($this->extra['imageDir']) ? '' : PH::WPencode(PH::evaluate($this->extra['imageDir']));
		$attachDir = empty($this->extra['attachDir']) ? '' : PH::WPencode(PH::evaluate($this->extra['attachDir']));
		$html =<<<EOH
<script type="text/javascript">
tinyMCE_GZ.init({
	plugins : 'WPphoundry,WPdmdelivery,WPeditable,WPfullpage,advhr,advimage,advlink,contextmenu,directionality,fullscreen,inlinepopups,media,nonbreaking,paste,preview,searchreplace,spellchecker,style,table',
	themes : 'advanced',
	languages : 'en,nl,zh,de,sv',
	disk_cache : false,
	debug : false
});
</script>
EOH;

		$html .= '<script type="text/javascript">' . "\n";
		$html .= str_replace(array('{$doctype}', '{$imageDir}', '{$attachDir}', '{$localConfig}'), array($doctype, $imageDir, $attachDir, trim($this->extra['config'])), $this->tinyMCEconfig);
		$html .= "\n</script>\n";
		$html .= '<textarea id="' . $this->name . '" name="' . $this->name . '" cols="60" rows="10" style="width:' . $this->extra['width']  . ';height:' . $this->extra['height'] . '">';
		$html .= PH::htmlspecialchars($value);
		$html .= '</textarea>';

		return $html;
	}

	public function getExtra()
	{
		global $PHprefs;

		$width     = '100%';
		$height    = '400px';
		$updatable = true;
		$imageDir  = '';
		$attachDir = '';
		$config    = '{}';
		if (!empty($this->extra)) {
			if (isset($this->extra['width']))
				$width = $this->extra['width'];
			if (isset($this->extra['height']))
				$height = $this->extra['height'];
			if (isset($this->extra['updatable']))
				$updatable = $this->extra['updatable'];
			if (isset($this->extra['imageDir']))
				$imageDir = $this->extra['imageDir'];
			if (isset($this->extra['attachDir']))
				$attachDir = $this->extra['attachDir'];
			if (isset($this->extra['config']))
				$config = $this->extra['config'];
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['cols'] = array(
			'name'     => 'ITwidth',
			'label'    => 'Width',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Enter the width of the TinyMCE editor (CSS expression: 400px or 80%)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$width)
		);
		$adminInfo['rows'] = array(
			'name'     => 'ITheight',
			'label'    => 'Height',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Enter the height of the TinyMCE editor (CSS expression: 200px or 20%)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$height)
		);
		$adminInfo['imageDir'] = array(
			'name'     => 'ITimageDir',
			'label'    => 'Image directory',
			'required' => false,
			'syntax'   => 'string',
			'info'     => 'Enter the image directory, relative to ' . $PHprefs['uploadDir'] . '/',
			'help'     => true,
			'input'    => array('type'=>'text', 'size'=>60, 'maxlength'=>255, 'value'=>$imageDir)
		);
		$adminInfo['attachDir'] = array(
			'name'     => 'ITattachDir',
			'label'    => 'Attachment directory',
			'required' => false,
			'syntax'   => 'string',
			'info'     => 'Enter the attachment directory, relative to ' . $PHprefs['uploadDir'] . '/',
			'help'     => true,
			'input'    => array('type'=>'text', 'size'=>60, 'maxlength'=>255, 'value'=>$attachDir)
		);
		$adminInfo['config'] = array(
			'name'     => 'ITconfig',
			'label'    => 'Config',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Configure the TinyMCE editor (tinyMCE.init).',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>110, 'rows'=>15, 'value'=>$config)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg)
	{
		$msg = '';

		$extra = array(
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false,
			'width'     => trim($_POST['ITwidth']),
			'height'    => trim($_POST['ITheight']),
			'imageDir'  => trim($_POST['ITimageDir'], '/') . '/',
			'attachDir' => trim($_POST['ITattachDir'], '/') . '/',
			'config'    => trim($_POST['ITconfig'])
		);

		return $extra;
	}

	public function getExtraHelp($what)
	{
		global $PHprefs;

		switch($what) {
			case 'ITimageDir':
				$help =<<<EOH
<h6>Image directory</h6>
If you want to allow for insertion of images in the editor, use this property. Set it to the path to the
directory the &quot;Image Properties&quot; dialog should start browsing in. This directory is relative to the <b>\$PHprefs['uploadDir']</b> directory.<br />
You can use the variables {\$USER_ID} and {\$GROUP_ID} for personal directories.
EOH;
			break;
			case 'ITattachDir':
				$help =<<<EOH
<h6>Attachment directory</h6>
If you want to allow for linking to attachments in the editor, use this property. Set it to the path to the
directory the &quot;Attachment Properties&quot; dialog should start browsing in. This directory is relative to the <b>\$PHprefs['uploadDir']</b> directory.<br />
You can use the variables {\$USER_ID} and {\$GROUP_ID} for personal directories.
EOH;
			break;
			case 'ITconfig':
				$tinyMCEconfig = str_replace("\t", '   ', $this->tinyMCEconfig);
				$help =<<<EOH
<h6>TinyMCE configuration options</h6>
<p>
<a href="http://wiki.moxiecode.com/index.php/TinyMCE:Configuration" target="_blank">Click here</a> for TinyMCE configuration options.
</p>
<p>
Web Power introduced two new configuration options:
</p>
<ul>
<li>
	<tt>theme_advanced_link_intern_dialog (string)</tt><br />Contains a link to a dialog that allows users to select an internal link.
	<br />
	Example: <tt>theme_advanced_link_intern_dialog: '/phoundry/custom/link_intern.php'</tt>
</li>
<li><tt>theme_advanced_link_default_target (string)</tt><br />Configures a default target for all hyperlinks.
	<br />
	Example: <tt>theme_advanced_link_default_target: 'email'</tt>
</li>
</ul>
<h6>The WPphoundry plugin</h6>
<p>
This plugin introduces a few extra functionalities:
</p>
<ol>
<li><tt>&lt;script language=&quot;php&quot;&gt;</tt>-tags are preserved</li>
<li>A <tt>WPcode</tt> button, a source code viewer that uses <a href="http://marijn.haverbeke.nl/codemirror/" target="_blank">CodeMirror</a> to syntax-highlight the HTML source code. Use this button instead of the standard <tt>code</tt> button.</li>
<li>A <tt>WPsize</tt> button, clicking it shows a popup displaying document statistics: number of words, charachters and size in bytes.</li>
</ol>
</p>
<h6>The WPdmdelivery plugin</h6>
<p>
This plugin introduces a few DMdelivery specific functionalities:
</p>
<ol>
<li>A <tt>WPinsertField</tt> button, to insert recipient fields.</li>
<li>A <tt>WPloadUrl</tt> button, to load documents from a URL into the editor.</li>
<li>A <tt>WPloadZip</tt> button, to load ZIP documents into the editor.</li>
<li>A <tt>WPlinkTitles</tt> button, for entering <tt>TITLE</tt>'s for all hyperlinks.</li>
<li>A <tt>WPsocialBookmarks</tt> button, for inserting Social Bookmarks into the editor.</li>
</ol>
<h6>The WPeditable plugin</h6>
<p>
This plugin only allows editing of certain <i>editable regions</i>. An <i>editable region</i> is defined as a
<tt>&lt;div class=&quot;editable(-html)&quot;&gt;</tt> or <tt>&lt;div class=&quot;editable-text&quot;&gt;</tt>.
<br />
An <tt>editable(-html)</tt> div is editable by using the full WYSIWYG editor. An <tt>editable-text</tt> div only
allows for using the keyboard (including shortcuts like CTRL-B, CTRL-U etc).
<br />
All content outside these <tt>&lt;div&gt;</tt>'s is uneditable.
</p>

<h6>TinyMCE default configuration</h6>
<pre>
{$tinyMCEconfig}
</pre>

<h6>TinyMCE Phoundry-wide configuration</h6>
<p>This default TinyMCE configuration can be overwritten/appended to via PREFS.php:</p>
<pre>
\$PHprefs['tinyMCEconfig'] = "{external_link_list_url : 'lists/link_list.js'}"
</pre>

<h6>jsTree-image properties</h6>
<pre>
phoundry : image : {
   // What extensions allowed for upload? Default: all extensions
   uploadExtensions: 'png,gif,jpg,jpeg',
   // What extensions allowed to select? Default: all extensions
   selectExtensions: 'png,gif,jpg,jpeg',
   // Can files be uploaded? Default: true
   allowFiles: true,
   // Can folders be created? Default: true
   allowFolders: true,
   // Size/range of image-widths that can be selected. Default: no restriction
   allowedWidth: '100-200',
   // Size/range of image-heights that can be selected. Default: no restriction
   allowedHeight: 200,
   // Max. size of files to upload (in MB). Default: no limit
   allowedSize: 1,
   // Width of thumbnails to be created. Default: null (no thumbnails)
   thumbnailWidth: 100,
   // Height of thumbnails to be created. Default: null (no thumbnails)
   thumbnailHeight: 150
}
</pre>

<h6>jsTree-attachment properties</h6>

<pre>
phoundry : attachment : {
   // What extensions allowed for upload? Default: all extensions
   uploadExtensions: 'png,gif,jpg,jpeg',
   // What extensions allowed to select? Default: all extensions
   selectExtensions: 'png,gif,jpg,jpeg',
   // Can files be uploaded? Default: true
   allowFiles: true,
   // Can folders be created? Default: true
   allowFolders: true,
   // Max. size of files to upload (in MB). Default: no limit
   allowedSize: 1
}
</pre>
EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}

		return $help;
	}

	private function parseConfigPrefsVariables($configPREFS)
	{
		$decoded = json_decode($configPREFS, true);
		$matches = array();
		if (!empty($decoded['content_css'])) {
			$contentCss = (array) $decoded['content_css'];
			foreach ($contentCss as $key => $css) {
				$pattern = '/(.*){(.*)}(.*)/';
				$hits = preg_match($pattern, $css, $matches);
				if ($hits > 0 && !empty($matches[2]) && is_callable($matches[2])) {
					$decoded['content_css'][$key] = $matches[1] . call_user_func($matches[2]) . $matches[3];
				}
			}
		}

		return json_encode($decoded);
	}
}
