<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(23) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function setColor() {
	var url = 'colorpick.php?l=<?= $lang ?>&bg=<?= urlencode($_GET['bg']); ?>&color='+escape(_D.forms['phoundry'].color.value);
	winArgs['fn'] = function(wh,clr){setColor_(clr)};
	if (isMidas) {
		_W.open(url,'PEW2','width=390,height=180,left='+((_W.outerWidth-390)/2+_W.screenX)+',top='+((_W.outerHeight-180)/2+_W.screenY));
	}
	else {
		var h = 210;
		if(_W.navigator.userAgent.indexOf('SV1')!=-1)h+=20;
		showModalDialog(url, winArgs, 'dialogWidth:390px;dialogHeight:'+h+'px;scroll:0;status:no;help:0');
	}
}
function setColor_(color) {
	_D.forms['phoundry']['color'].value = color;
}

function init() {
	var f = _D.forms['phoundry'], arg;

	if (winArgs['el']) {
		EL = winArgs['el'];
		arg = EL.getAttribute('size');
		if (arg) f.size.value = arg;
		arg = EL.getAttribute('color');
		if (arg) f.color.value = arg;
		arg = EL.getAttribute('width');
		if (arg) f.width.value = arg;
		arg = EL.getAttribute('align');
		if (arg == '')
			f.align.selectedIndex = 0;
		else
			f.align.value = arg;
		if (EL.noShade)
			f.noShade.checked = true;
	}

	if (winArgs['editor'].feat&4) {
		if (winArgs['editor'].classes.length) {
			var classes = winArgs['editor'].classes;
			var html = '<select name="Fclass"><option value=""><?= PEword(219) ?></option>';
			for(var x = 0; x < classes.length; x++)
				html += '<option value="' + classes[x] + '">' + classes[x] + '</option>';
			html += '</select>';
			_D.getElementById('cssTD').innerHTML = html;
		}
		_D.getElementById('cssTR').style.visibility='visible';
	}

	if (winArgs['editor'].feat&4 && winArgs['editor'].classes.length && EL && EL.className == '')
		f.Fclass.selectedIndex = 0;
	else if (winArgs['editor'].feat&4 && EL)
		f.Fclass.value = EL.className;

	f.size.focus();
}

function submitMe() {
	var f = _D.forms['phoundry'], A = [], tag = '', arg;

   A['size'] = f.size.value;
   A['align'] = f.align.value;
   A['width'] = f.width.value;
   A['color'] = f.color.value;
   A['noShade'] = f.noShade.checked?'true':'';
   A['class'] = f.Fclass.value;

	if(!EL) {
		tag='<hr';
		tag+=A['width']==''?'':' width="'+A['width']+'"';
		tag+=A['size']==''?'':' size="'+A['size']+'"';
		tag+=A['align']==''?'':' align="'+A['align']+'"';
		tag+=A['color']==''?'':' color="'+A['color']+'" style="background-color:'+A['color']+'"';
		tag+=A['noShade']==''?'':' noShade';
		tag+=A['class']==''?'':' class="'+A['class']+'"';
		tag+= '>';
		winArgs['editor'].insertHTML(tag);
	} else {
		if (A['size'])
			EL.setAttribute('size', A['size']);
		else
			EL.removeAttribute('size',0);
		if (A['width'])
			EL.setAttribute('width', A['width']);
		else
			EL.removeAttribute('width',0);
		if (A['class'])
			EL.setAttribute(isMidas?'class':'className', A['class']);
		else
			EL.removeAttribute(isMidas?'class':'className',0);
		if (A['align'])
			EL.setAttribute('align', A['align']);
		else
			EL.removeAttribute('align',0);
		if (A['color']) {
			EL.setAttribute('color', A['color']);
			EL.style.backgroundColor = A['color'];
		}
		else
			EL.removeAttribute('color',0);
		EL.noShade = f.noShade.checked;
	}
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
</head>
<body onload="init()" oncontextmenu="return false">
<form name="phoundry">
<fieldset>
<legend><b><?= PEword(23) ?></b></legend>
<table width="230">
<tr>
	<td>Size:</td>
	<td><input class="txt" type="text" name="size" size="4" onblur="this.value=checkDigit(this.value,0)" /></td>
	<td><?= PEword(135) ?>:</td>
	<td><select name="align">
	<option value=""><?= PEword(219) ?></option>
	<option value="left"><?= PEword(220) ?></option>
	<option value="right"><?= PEword(221) ?></option>
	<option value="center"><?= PEword(226) ?></option>
	</select></td>
</tr>
<tr>
	<td><?= PEword(130) ?>:</td>
	<td><input class="txt" type="text" name="width" size="4" onblur="this.value=checkDigit(this.value,1)" /></td>
	<td><?= PEword(254) ?>:</td>
	<td><table cellspacing="0" cellpadding="0" border="0"><tr>
	<td><input class="txt" type="text" name="color" size="7" maxlength="7" /></td>
	<td><img src="pics/font_bgcolor.png" width="16" height="16" border="0" onclick="setColor()" style="cursor:pointer;cursor:hand" /></td>
	</tr></table>
	</td>
</tr>
<tr>
	<td></td>
	<td colspan="3"><input type="checkbox" id="noShade" /><label for="noShade"><?= PEword(255) ?></label></td>
</tr>
<tr id="cssTR" style="visibility:hidden">
	<td><?= PEword(137) ?>:</td>
	<td id="cssTD" colspan="3"><input class="txt" type="text" name="Fclass" size="20" style="width:180px" /></td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
