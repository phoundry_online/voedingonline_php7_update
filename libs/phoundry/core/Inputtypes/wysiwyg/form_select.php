<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(52) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function init() {
	var f = _D.forms['phoundry'];

	if (winArgs['editor'].feat&4) {
		if (winArgs['editor'].classes.length) {
			var classes = winArgs['editor'].classes;
			var html = '<select name="Fclass"><option value=""><?= PEword(219) ?></option>';
			for(var x = 0; x < classes.length; x++)
				html += '<option value="' + classes[x] + '">' + classes[x] + '</option>';
			html += '</select>';
			_D.getElementById('cssTD').innerHTML = html;
		}
		_D.getElementById('cssTR').style.visibility='visible';
	}

	if (winArgs['el']) {
		EL = winArgs['el'];
		f.Fname.value = EL.name;
		f.Fsize.value = f.example.size = (EL.size == 0) ? '' : EL.size;
		if (EL.multiple)
			f.Fmselect.checked = f.example.multiple = true;
		var opts = EL.options;
		for (var x = 0; x < opts.length; x++) {
			f.example.options[x]=new Option(opts[x].text,opts[x].value);
		}
		f.example.selectedIndex=-1;
	}

	if (winArgs['editor'].feat&4 && winArgs['editor'].classes.length && EL && EL.className == '')
		f.Fclass.selectedIndex = 0;
	else if (winArgs['editor'].feat&4 && EL)
		f.Fclass.value = EL.className;

	f.Fname.focus();
}

function setMultiple(on) {
	_D.forms['phoundry'].example.multiple = on;
}

function setSize(size) {
	_D.forms['phoundry'].example.size = size;
}

function doOption() {
	var f = _D.forms['phoundry'];
	if(f.FoptText.value == '' || f.FoptValue.value == '') {
		alert('<?= PEescSquote(PEword(193)) ?>');
		f.FoptText.focus();
		return;
	}
	var opt = new Option(f.FoptText.value, f.FoptValue.value);
	if (f.actBut.value == '<?= PEescSquote(PEword(191)) ?>')
		f.example.options[f.example.length] = opt;
	else
		f.example.options[f.example.selectedIndex] = opt;
	f.actBut.value = '<?= PEescSquote(PEword(191)) ?>';
	f.FoptText.value = f.FoptValue.value = '';
}

function delOption() {
	var f = _D.forms['phoundry'];
	if (f.example.selectedIndex<0) return;
	f.example.options[f.example.selectedIndex] = null;
	f.FoptText.value = f.FoptValue.value = '';
	f.actBut.value = '<?= PEescSquote(PEword(191)) ?>';
}

function setOption() {
	var f = _D.forms['phoundry'];
	if(!f.example.options.length)return;
	f.FoptText.value = f.example.options[f.example.selectedIndex].text;
	f.FoptValue.value = f.example.options[f.example.selectedIndex].value;
	f.actBut.value = '<?= PEescSquote(PEword(194)) ?>';
}

function submitMe() {
	var f = _D.forms['phoundry'], A = [], tag, x;

	if (f.Fname.value == '') { alert('<?= PEescSquote(PEword(185)) ?>'); f.Fname.focus(); return }

	A['name']  = f.Fname.value;
	A['class'] = f.Fclass.value;
	A['multiple'] = f.Fmselect.checked;
	A['size']  = f.Fsize.value;

	tag = '<select name="' + A['name'] + '" size="' + A['size'] + '"';
	tag+=A['class']==''?'':' class="'+A['class']+'"';
	if (A['multiple'])
		tag += ' multiple="multiple"';
	tag += '>\n';
	for (x = 0; x < f.example.options.length; x++)
		tag += '<option value="' + f.example.options[x].value + '">' + f.example.options[x].text + '</option>';
	tag += '\n</select>';

	if (!EL)
		winArgs['editor'].insertHTML(tag);
	else {
		if (!isMidas)
			EL.outerHTML = tag;
		else {
			while(EL.options.length)
				EL.options[0]=null;
			for (x = 0; x < f.example.options.length; x++)
				EL.options[x] = new Option(f.example.options[x].text,f.example.options[x].value);
			if (A['multiple'])
				EL.setAttribute('multiple', 'multiple');
			else
				EL.removeAttribute('multiple',0);
			if (A['name'])
				EL.setAttribute('name', A['name']);
			else
				EL.removeAttribute('name',0);
			if (A['size'])
				EL.setAttribute('size', A['size']);
			else
				EL.removeAttribute('size',0);
			if (A['class'])
				EL.setAttribute(isMidas?'class':'className', A['class']);
			else
				EL.removeAttribute(isMidas?'class':'className',0);
		}
	}
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
</head>
<body onload="init()" oncontextmenu="return false">
<form name="phoundry">
<fieldset>
<legend><b><?= PEword(52) ?></b></legend>
<table><tr><td valign="top">

<table>
<tr>
	<td><?= PEword(178) ?>:</td>
	<td><input class="txt" type="text" name="Fname" size="20" /></td>
</tr>
<tr>
	<td><?= PEword(186) ?>:</TD>
	<td><input class="txt" type="text" name="Fsize" size=4 onblur="this.value=checkDigit(this.value);setSize(this.value)" /></td>
</tr>
<tr>
	<td><label for="Fmselect"><?= PEword(189) ?>?</label></td>
	<td><input type="checkbox" id="Fmselect" value="1" onclick="setMultiple(this.checked)" /></td>
</tr>
<tr id="cssTR" STYLE="visibility:hidden">
	<td><?= PEword(137) ?>:</td>
	<td id="cssTD"><input class="txt" type="text" name="Fclass" size="20" /></td>
</tr>
<tr><td colspan="2"><hr noshade="noshade" size="1" /></td></tr>
<tr>
	<td><b><?= PEword(178) ?>:</b></td>
	<td><input class="txt" type="text" name="FoptText" /></td>
</tr>
<tr>
	<td><b><?= PEword(179) ?>:</b></td>
	<td><input class="txt" type="text" name="FoptValue" /></td>
</tr>
<tr>
	<td colspan="2" align="right">
	<input name="actBut" type="button" value="<?= PEword(191) ?>" onclick="doOption()" />
	<input type="button" value="<?= PEword(190) ?>" onclick="delOption()" />
	</td>
</tr>
</table>

</td><td valign="top">
<fieldset style="width:150px; height:140px">
<legend><b><?= PEword(192) ?></b></legend>
<center>
<select name="example" size="1" onchange="setOption()" /></select>
</center>
</fieldset>
</td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
