<?php

//
// jsTree redirector:
//

$inc = @include_once('PREFS.php');
if ($inc === false) {
   require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
// Needed for session-start:
require_once "{$PHprefs['distDir']}/core/include/common.php";

$dir = '';
if (isset($_GET['dir'])) {
	$dir = ltrim(str_replace('..', '', $_GET['dir']), '/');
}

$_SESSION['jsTree'] = array(
   'rootDir' => rtrim($PHprefs['uploadDir'], '/') . '/' . $dir,
   'rootUrl' => rtrim($PHprefs['uploadUrl'], '/') . '/' . $dir,
   'uploadExtensions' => array(),
   'selectExtensions' => array(),
   'allowFolders' => isset($_GET['allowFolders']) && $_GET['allowFolders'] == 'false' ? false : true,
   'allowFiles'   => isset($_GET['allowFiles']) && $_GET['allowFiles'] == 'false' ? false : true,
   'thumbnailWidth' => 0,
	'skipPattern' => '/(^thumb_(.*)\.png$)|(^__edit__)|(\.bak\.html$)/',
   'thumbnailHeight' => 0,
   'allowedWidth'     => null,
   'allowedHeight'    => null,
	'asciiOnly'			=> $PHprefs['charset'] != 'utf-8',
	'useFileFunction'	=> "parent.setFile('{\$path}')",
	'allowedSize'  => isset($_GET['maxSize']) ? (float)$_GET['maxSize'] : null
);

$args = '';
if (isset($_GET['lang'])) {
	$args .= '&lang=' . $_GET['lang'];
}
if (isset($_GET['mode'])) {
	$args .= '&mode=' . $_GET['mode'];
}
$args .= '&' . mt_rand(1,9999);

$url = '../../jsTree/?' . substr($args,1);
if (!preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) {
	header('Location: ../../jsTree/?' . substr($args,1));
	exit;
}

?>
<html>
<head>
<title>Explorer</title>
</head>
<body style="margin:0">
<iframe src="<?= $url ?>" width="100%" height="100%" frameborder="0"></iframe>
</body>
</html>
