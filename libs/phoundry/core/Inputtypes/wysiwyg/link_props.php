<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";

	$forDMdelivery = strpos($_SERVER['SCRIPT_FILENAME'], '/dmdelivery') !== false && $_GET['shift'] == 'false';
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(200) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

var re = new RegExp('^(http://|https://|mailto:|ftp://|news:|javascript:)', 'i'), anchors = [], deftgt = '', dummy;

function setColor() {
	var color, url;
	color = _D.phoundry['color'].value;
	url = 'colorpick.php?l=<?= $lang ?>&bg=<?= urlencode($_GET['bg']) ?>&color='+escape(color);
	winArgs['what'] = 'color';
	winArgs['fn'] = function(wh,clr){setColor_(wh,clr)};
	if (isMidas) {
		_W.open(url,'PEW2','width=390,height=210,left='+((_W.outerWidth-390)/2+_W.screenX)+',top='+((_W.outerHeight-180)/2+_W.screenY));
	}
	else {
		showModalDialog(url, winArgs, 'dialogWidth:390px;dialogHeight:210px;scroll:0;status:no;help:0;');
	}
}
function setColor_(what,color) {
	_D.phoundry[what].value = color;
	_D.getElementById(what+'Clr').style.backgroundColor = color;
}

function setPreview(what, value) {
	try {
		_D.getElementById(what+'Clr').style.backgroundColor = value;
	}
	catch(e) {
		_D.getElementById(what+'Clr').style.backgroundColor = 'menu';
	}
}

function init() 
{
	var f = _D.forms['phoundry'], re, url = '', color = '#000000'; 
	if (winArgs['editor'].feat&4) {
		if (winArgs['editor'].classes.length) {
			var classes = winArgs['editor'].classes;
			var html = '<select name="Fclass"><option value=""><?= PEword(219) ?></option>';
			for(var x = 0; x < classes.length; x++)
				html += '<option value="' + classes[x] + '">' + classes[x] + '</option>';
			html += '</select>';
			_D.getElementById('cssTD').innerHTML = html;
		}
		_D.getElementById('cssTR').style.visibility='visible';
	}

	if (winArgs['el'])
		EL = winArgs['el'];

	if (<?= $forDMdelivery ? 1 : 0 ?>) {
		if (winArgs['linkcolor']) color = winArgs['linkcolor'];
		if (EL && EL.style.textDecoration && !/underline/i.test(EL.style.textDecoration)) {
			f['underlined'].checked = false;
		}

		if (EL && /^<font([^>]+)>/i.test(EL.innerHTML)) {
			dummy=_D.createElement('div');
			dummy.style.visibility='hidden';
			dummy.innerHTML = winArgs['outerHTML'];
			dummy.id='example';
			_D.body.appendChild(dummy);
			var font_el = dummy.getElementsByTagName('font')[0];
			if (font_el.color) color = font_el.color;
		}
		f['color'].value = color;
		setPreview('color', color);
	}

	if (EL) {
		f.title.value = EL.title;
		re=new RegExp('<a.*href[^"]*=\s*("[^"]*"|\'[^\']*\'|[^\s>]*)','i');
		if(re.test(winArgs['outerHTML'])) {
			url=RegExp.$1.replace(/^\s+/,'').replace(/\s+$/,'').replace(/^'([^']+)'$/,'$1').replace(/^"([^"]+)"$/,'$1').replace(/%7B/g,'{').replace(/%7C/g,'|').replace(/%7D/g,'}');
		}
		if (url != '') {
			url = url.replace(/&amp;/g, '&');
			if (!isMidas && !winArgs['absoluteLinks']) {
				// Correct absolute links:
				var pathname = winArgs['loc'].pathname;
				pagename = winArgs['base']+pathname+winArgs['loc'].search;
				pagename=pagename.replace(/([\.\\\+\*\?\[\^\]\$\(\)\{\}\=\!\|\:])/g,'\\$1');
				re = new RegExp(pagename+'(.*)','gim');
				url = url.replace(re, '$1');

				dirname = winArgs['base']+pathname.substring(0,pathname.lastIndexOf('/')+1);
				dirname = dirname.replace(/([\.\\\+\*\?\[\^\]\$\(\)\{\}\=\!\|\:])/g,'\\$1');
				re = new RegExp(dirname+'(.*)','gim');
				url = url.replace(re, '$1');

				re=new RegExp(winArgs['base']+'\/','gim');
				url=url.replace(re,'/');
			}
			re = new RegExp('^(http://|https://|mailto:|ftp://|news:|javascript:)', 'i');
			var m = re.exec(url);
			if (!m) {
				f.protocol.selectedIndex = 0;
				f.url.value = url;
			}
			else {
				f.protocol.value = m[1];
				f.url.value = url.replace(re, '');
				if (m[1] == 'mailto:' || m[1] == 'news:')
					_D.getElementById('targetRow').style.visibility = 'visible';
			}
		}
	}

	// Set target:
	var tgts = {'_self':1,'_blank':2,'_top':3,'_parent':4};
	if (EL)
		var tgt = EL.target ? EL.target : '';
	else
		var tgt = '';
	if (isMidas) {
		deftgt = winArgs['deftgt'] ? winArgs['deftgt'] : '';
		if (tgt == '' && deftgt != '' && url == '') tgt = deftgt;
	}
	if (tgt == '')
		f.target.selectedIndex = 0;
	else if (tgts[tgt])
		f.target.selectedIndex = tgts[tgt];
	else {
		f.target.selectedIndex = 5;
		f.custom_target.value = tgt;
		_D.getElementById('targetEl').style.visibility = 'visible';
	}

	if (winArgs['anchors']) {
		anchors = winArgs['anchors'];
		for (x = 0; x < anchors.length; x++)
			f.anchor.options[f.anchor.options.length] = new Option('#'+anchors[x],x);
		if (x == 0)
			f.anchor.disabled = true;
	}

	if (f.url.value.charAt(0) == '#') {
		_D.getElementById('targetRow').style.visibility = 'visible';
		for (x = 0; x < anchors.length; x++) {
			if ('#'+anchors[x] == f.url.value) {
				f.anchor.selectedIndex = x+1;
				break;
			}
		}
	}

	if (winArgs['editor'].feat&4 && winArgs['editor'].classes.length && EL && EL.className == '')
		f.Fclass.selectedIndex = 0;
	else if (winArgs['editor'].feat&4 && EL)
		f.Fclass.value = EL.className;

	f.url.focus();
}

function setAnchor(id) {
	var f = _D.forms['phoundry'];
	if (id != '') {
		f.protocol.selectedIndex = 0;
		f.target.selectedIndex = 0;
		f.url.value = '#' + anchors[id];
		f.custom_target.value='';
		_D.getElementById('targetEl').style.visibility = 'hidden';
		_D.getElementById('targetRow').style.visibility = 'hidden';
	}
	else {
		if (f.url.value != '' && f.url.value.charAt(0) == '#') {
			f.url.value = '';
			f.protocol.selectedIndex = 1;
		}
		_D.getElementById('targetRow').style.visibility = 'visible';
	}
}

function checkProtocol(pr) {
	if (pr == 'mailto:' || pr == 'news:')
		_D.getElementById('targetRow').style.visibility = 'hidden';
	else
		_D.getElementById('targetRow').style.visibility = 'visible';
		
}

function checkTarget(tgt) {
	if (tgt == 'custom') {
		_D.getElementById('targetEl').style.visibility = 'visible';
		_D.phoundry['custom_target'].focus();
	}
	else
		_D.getElementById('targetEl').style.visibility = 'hidden';
}

function submitMe() {
	var f = _D.forms['phoundry'], A = [], tgt, tag_s = '', tag_e = '', color = f['color'] ? f['color'].value : null, underlined = f['underlined'] ? f['underlined'].checked : true;
	var protocol = f.protocol.options[f.protocol.selectedIndex].value;
	A['href'] = protocol + f.url.value.replace(/^\s+|\s+$/g, '');
	A['url'] = f.url.value.replace(/^\s+|\s+$/g, '');
	A['title'] = f.title.value.replace(/^\s+|\s+$/g, '');
	if (protocol == 'mailto:' || protocol == 'news:')
		tgt = '';
	else {
		tgt = f.target.options[f.target.selectedIndex].value;
		if (tgt == 'custom')
			tgt = f.custom_target.value;
	}	
	A['target'] = tgt.replace(/^\s+|\s+$/g, '');
	A['class'] = f.Fclass.value.replace(/^\s+|\s+$/g, '');

	if (!EL) {
		tag_s+='<a';
		if(A['href'])tag_s+=' href="'+A['href']+'"';
		if(A['title'])tag_s+=' title="'+A['title']+'"';
		if(A['target'])tag_s+=' target="'+A['target']+'"';
		if(A['class'])tag_s+=' class="'+A['class']+'"';
		if(!underlined)tag_s+=' style="text-decoration:none"';
		tag_s += '>';
		tag_e += '</a>';
		<?php if ($forDMdelivery) { ?>
		if (color) {
			tag_s += '<font color="'+color+'">';
			tag_e = '</font>'+tag_e;
		}
		<?php } ?>
		if (isMidas)
			winArgs['editor'].insertHTML(tag_s,tag_e);
		else
			winArgs['editor'].insertHTML(tag_s+A['href']+tag_e,'',0);
	} else {
		if (dummy) {
			var dummyFont = dummy.getElementsByTagName('font')[0];
			var dummyLink = dummy.getElementsByTagName('a')[0];
			if (f.url.value == '') {
				EL.outerHTML = dummyFont.innerHTML;
				_W.close();
			}
			if (A['href']){
				dummyLink.setAttribute('href', A['href']);
			}
			if (A['title'])
				dummyLink.setAttribute('title', A['title']);
			else
				dummyLink.removeAttribute('title',0);
			if (A['class'])
				dummyLink.setAttribute(isMidas?'class':'className', A['class']);
			else
				dummyLink.removeAttribute(isMidas?'class':'className',0);
			if (A['target'])
				dummyLink.setAttribute('target', A['target']);
			else
				dummyLink.removeAttribute('target',0);
			<?php if ($forDMdelivery) { ?>
			if (!underlined) {
				dummyLink.style.textDecoration = 'none';
			}
			else {
				dummyLink.style.textDecoration = 'underline';
			}
			<?php } ?>
			if (color)
				dummyFont.color = color;
			EL.outerHTML = dummy.innerHTML;
		}
		else {
			if(f.url.value == '') {
				try{doc.execCommand('unlink',false,null)}catch(e){};
				_W.close();
			}
			if (A['href'])
				EL.setAttribute('href', A['href']);
			if (A['title'])
				EL.setAttribute('title', A['title']);
			else
				EL.removeAttribute('title',0);
			if (A['class'])
				EL.setAttribute(isMidas?'class':'className', A['class']);
			else
				EL.removeAttribute(isMidas?'class':'className',0);
			if (A['target'])
				EL.setAttribute('target', A['target']);
			else
				EL.removeAttribute('target',0);
			<?php if ($forDMdelivery) { ?>
			if (!underlined) {
				EL.style.textDecoration = 'none';
			}
			else {
				EL.style.textDecoration = 'underline';
			}
			if (color) {
				tag_s += '<font color="'+color+'">';
				tag_e = '</font>'+tag_e;
			}
			EL.innerHTML = tag_s+EL.innerHTML+tag_e;
			<?php } ?>
		}
	}
	_W.close();
}

function closeMe() {
	if(winArgs['new']) {
		try{doc.execCommand('unlink',false,null)}catch(e){};
	}
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.clr {
	border: 1px solid black; padding: 0; margin: 0;
}

-->
</style>
</head>
<body onload="init()">

<form name="phoundry">
<fieldset>
<legend><b><?= PEword(200); ?></b></legend>
<table>
<tr>
	<td width="100">URL:</td>
	<td><select style="width:80px" NAME="protocol" onChange="checkProtocol(this.options[this.selectedIndex].value)">
	<option value=""><?= PEword(219) ?></option>
	<option selected="selected" value="http://">http://</option>
	<option value="https://">https://</option>
	<option value="mailto:">mailto:</option>
	<option value="ftp://">ftp://</option>
	<option value="news:">news:</option>
	<option value="javascript:">javascript:</option>
	</select></td>
 	<td><input class="txt" type="text" style="width:200px" name="url" /></td>
</tr>
<tr>
	<td colspan="3" align="right">
	<?= PEword(24); ?>: <select name="anchor" onchange="setAnchor(this.options[this.selectedIndex].value)" style="width:200px">
	<option value=""><?= PEword(219) ?></option>
	</select></td>
</tr>
<tr>
	<td><?= PEword(251) ?>:</td>
	<td colspan="2"><input class="txt" type="text" name="title" style="width:283px" /></td>
</tr>
<tr id="targetRow" style="visibility:visible" valign="top">
	<td><?= PEword(184); ?>:</td>
	<td><select style="width:80px" name="target" onchange="checkTarget(this.options[this.selectedIndex].value)">
	<option value=""><?= PEword(219) ?></option>
	<option value="_self">_self</option>
	<option value="_blank">_blank</option>
	<option value="_top">_top</option>
	<option value="_parent">_parent</option>
	<option value="custom"><b>custom:</b></option>
	</select></td>
	<td id="targetEl" style="visibility:hidden">
	<input class="txt" type="text" style="width:200px" name="custom_target" />
	<small><?= PEword(201); ?></small>
	</td>
</tr>

<?php if ($forDMdelivery) { ?>
<tr>
<td>Color:</td>
<td colspan="2">
<table cellspacing="0"><tr>
<td><input class="txt" type="text" name="color" style="width:80px" onblur="setPreview('color',this.value)" /></td>
<td id="colorClr" class="clr"><img src="pics/pixel.gif" alt="" width="20" height="5" /></td>
<td><img src="pics/font_bgcolor.png" alt="Color sample" width="16" height="16" border="0" onclick="setColor()" style="cursor:pointer;cursor:hand" /></td>
<td style="padding-left:10px"><input type="checkbox" name="underlined" checked="checked" id="underlined" /> <label for="underlined"><?= PEword(10) ?></label></td>
</tr></table>

</td>
</tr>
<?php } ?>

<tr id="cssTR" style="visibility:hidden">
	<td><?= PEword(137); ?>:</td>
	<td id="cssTD" colspan="3"><input class="txt" type="text" name="Fclass" size="20" style="width:284px" /></td>
</tr>

</table>
</fieldset>

<p align="right">
<input type="button" value="<?= PEword(110); ?>" onclick="submitMe()" /> 
<input type="button" value="<?= PEword(138); ?>" onclick="closeMe()" />
</p>

</form>
</body>
</html>
