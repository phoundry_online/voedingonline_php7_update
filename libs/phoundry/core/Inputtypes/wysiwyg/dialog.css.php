<?php
	header('Content-type: text/css');
?>

body, td, button, select, input, textarea {
	font-family: Tahoma,Futura,Verdana,Sans-serif;
	font-size: 11px;
}

body {
	background:<?= $_GET['bg'] ?>;
	border: 0;
	margin: 3px;
}

form {
	margin: 4px;
}

.txt, select {
	padding: 0;
	margin: 0;
	border: 1px inset window;
}

.ptr {
	cursor: pointer;
}

fieldset {
	-moz-border-radius: 6px;
}

p {
	margin: 6px;
}
