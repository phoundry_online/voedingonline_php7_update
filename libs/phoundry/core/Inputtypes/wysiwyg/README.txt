README.txt
==========

Copy the Phoundry Editor distribution directory to somewhere
in your Document Root.
Now point Internet Explorer to 'test.php' in this directory.
(Through a server, not by using File->Open!).
If the installation went okay, you should see the Phoundry
Editor in a basic FORM. (The image browser won't work!)
Make sure the 'test.php' page works. If it does, you can
start embedding it in your own FORM's.

Full and up-to-date documentation is available at 
http://editor.phoundry.com/docs/
You'll find installation instructions, requirements, examples, FAQs 
and much more there.
There's also an automated configuration-script at this site:
http://editor.phoundry.com/configomatic/

If you'd like to purchase a License for the Phoundry Editor, go to 
http://editor.phoundry.com/download/buy.php

Thank you and enjoy using the Phoundry Editor!

Web Power Internet Technology,
the developers of the Phoundry Editor.
info@editor.phoundry.com
http://editor.phoundry.com
http://www.webpower.nl
