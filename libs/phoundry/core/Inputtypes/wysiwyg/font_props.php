<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title>Font<?= str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function init() {
	var f = _D.forms['phoundry'], arg, x;
	if (winArgs['el']) 
		EL = winArgs['el'];
	if (winArgs['fontsize'])
		f['fontsize'].value = winArgs['fontsize'];
	if (winArgs['fontsizepx'])
		f['fontsizepx'].value = winArgs['fontsizepx'];
	if (winArgs['fontname'])
		f['fontname'].value = winArgs['fontname'].toLowerCase();
	if (winArgs['formatblock'])
		f['formatblock'].value = winArgs['formatblock'];
}

var fontChanged = false, sizeChanged = false, sizepxChanged = false, blockChanged = false;

function submitMe() {
	var f = _D.forms['phoundry'];
	if (sizeChanged)
		winArgs['document'].execCommand('fontsize',false,f['fontsize'].value);
	if (fontChanged)
		winArgs['document'].execCommand('fontname',false,f['fontname'].value);
	if (blockChanged) {
		var cmd = f['formatblock'].value;
		if (isMidas) {
			cmd = cmd.replace('Heading ', 'h');

		}
		winArgs['document'].execCommand('formatblock',false,cmd);
	}
	if (sizepxChanged && EL)
		EL.style.fontSize = f['fontsizepx'].value;
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
</head>
<body>
</body>
</html>
<body onload="init()" oncontextmenu="return false">
<form name="phoundry">
<fieldset>
<legend><b>Font</b></legend>
<table><tr>
<td>
	Font:<br />
	<select name="fontname" size="7" onchange="fontChanged=true">
	<option value="arial">Arial</option>
	<option value="comic sans ms">Comic Sans MS</option>
	<option value="courier">Courier</option>
	<option value="courier new">Courier New</option>
	<option value="helvetica">Helvetica</option>
	<option value="ms sans serif">MS Sans Serif</option>
	<option value="ms serif">MS Serif</option>
	<option value="script">Script</option>
	<option value="symbol">Symbol</option>
	<option value="tahoma">Tahoma</option>
	<option value="times new roman">Times New Roman</option>
	<option value="verdana">Verdana</option>
	<option value="trebuchet ms">Trebuchet MS</option>
	</select>
</td>
<td>
	Size:<br />
	<select name="fontsize" size="7" style="width:50px" onchange="sizeChanged=true">
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	</select>
</td>
<td>
	Size (px):<br />
	<select name="fontsizepx" size="7" style="width:60px" onchange="sizepxChanged=true">
	<option value="">[none]</option>
	<option value="8px">8px</option>
	<option value="9px">9px</option>
	<option value="10px">10px</option>
	<option value="11px">11px</option>
	<option value="12px">12px</option>
	<option value="13px">13px</option>
	<option value="14px">14px</option>
	<option value="15px">15px</option>
	<option value="16px">16px</option>
	<option value="17px">17px</option>
	<option value="18px">18px</option>
	<option value="19px">19px</option>
	<option value="20px">20px</option>
	<option value="21px">21px</option>
	<option value="22px">22px</option>
	<option value="23px">23px</option>
	<option value="24px">24px</option>
	</select>
</td>
<td>
	Block:<br />
	<select name="formatblock" size="7" onchange="blockChanged=true">
	<option value="Normal">Normal</option>
	<option value="Heading 1">Heading 1</option>
	<option value="Heading 2">Heading 2</option>
	<option value="Heading 3">Heading 3</option>
	<option value="Heading 4">Heading 4</option>
	<option value="Heading 5">Heading 5</option>
	<option value="Heading 6">Heading 6</option>
	<option value="Address">Address</option>
	<?php if (!$_isMidas) { ?>
	<option value="Numbered list">Numbered list</option>
	<option value="Bulleted list">Bulleted list</option>
	<?php } ?>
	</select>
</td>
</tr></table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
