<?php	
	require 'common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";

	// This script will not work for https:// URLS!
	// 'fopen' simply does not support it (yet?)

	$magic = get_magic_quotes_gpc();
	$url   = $_GET['url'];
	
	if ($url{0} == '/') {
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $url;
	}
	else if (!preg_match('/^https?:\/\//i', $url)) {
		$url = 'http://' . $url;
	}
	
	if ($magic) {
		$url  = stripslashes($url);
	}
	
	$html = slurpHTML($url, $errorMsg);
	if ($html === false) {
		// Some error
		print ">ERROR<: " . $errorMsg;
		exit;
	}
	elseif (preg_match('/<\s*frameset[^>]*>/i', $html)) {
		print ">ERROR<: " . PEword(240,$url);
		exit;
	}
	else {
		$editorCharset = isset($_GET['cs']) ? strtolower($_GET['cs']) : 'iso-8859-1';
		// Find charset:
		$HTMLcharset = 'iso-8859-1';
		if (preg_match('/<meta\s+http-equiv[^;]+;\s*charset=([^">]+)/i', $html, $reg)) {
	  		$HTMLcharset = strtolower(trim($reg[1]));
		}

		if ($editorCharset == 'utf-8' && $HTMLcharset == 'iso-8859-1') {
  			$html = preg_replace('/<meta\s+http-equiv="?content-type"?([^>]+)>/i', '', $html);
  			$html = preg_replace('/<head>/i', "<head>\n" . '<meta http-equiv="content-type" content="text/html; charset=utf-8" />', $html);
  			$html = utf8_encode($html);
		}
		elseif ($editorCharset == 'iso-8859-1' && $HTMLcharset == 'utf-8') {
  			$html = preg_replace('/<meta\s+http-equiv="?content-type"?([^>]+)>/i', '', $html);
  			$html = preg_replace('/<head>/i', "<head>\n" . '<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />', $html);
  			$html = utf8_decode($html);
		}
		elseif ($editorCharset == 'utf-8' && $HTMLcharset != 'utf-8') {
			// Editor is UTF-8, HTML is something 'weird' (gb2312, koi8-r)
  			$html = preg_replace('/<meta\s+http-equiv="?content-type"?([^>]+)>/i', '', $html);
  			$html = preg_replace('/<head>/i', "<head>\n" . '<meta http-equiv="content-type" content="text/html; charset=utf-8" />', $html);
			$html = iconv($HTMLcharset, 'utf-8//TRANSLIT', $html);
		}

		header("Content-type: text/html; charset={$editorCharset}");
		print $html;
	}

function fixUrlsInHtml($html, $base, $path = '/') {
	if (preg_match_all("/<\s*(a|area|body|form|img|table|td|link|object|embed|input)\s+[^>]+>/i", $html, $_matches, PREG_OFFSET_CAPTURE)) {
		$_posd = 0;
		$_chunks = array();
		foreach($_matches[0] as $_match) {
			$_posp  = $_match[1];
			$_pos   = $_match[1] - $_posd;
			$_tag   = $_oldTag = $_match[0];
			$_tagl  = strtolower($_tag);

			// Skip tags without 'href', 'action', 'src' or 'background' attribute:
			if (!preg_match('/(href|action|src|background|data)\s*=\s*(?:"([^"]+)"|([^\'">\s]+)|\'([^\']+)\')/i', $_tag, $_reg)){
				continue;
			}

			$_url  = $_reg[2] ? $_reg[2] : ($_reg[3] ? $_reg[3] : $_reg[4]);
			$_urll = strtolower($_url);

			// Continue when URL starts with #, mailto:, ftp: of javascript:
			if (strpos($_urll, '#') === 0 || strpos($_urll, 'mailto:') === 0 || strpos($_urll, 'javascript:') === 0 || strpos($_urll, 'ftp:') === 0 || strpos($_urll, 'itpc:') === 0 || substr($_urll, 0, 10) == '/x/plugin/') {
				continue;
			}

			if (substr($_urll, 0, 7) != 'http://' && substr($_urll, 0, 8) != 'https://' && substr($_urll,0,2) != '{$') {
				// Make HREF absolute:
				if ($_url{0} != '/') {
					$_url = $path . $_url;
				}
				$_url = $base . str_replace('//','/',$_url);
			}

			$_chunks[] = substr($html, 0, $_pos);
			$_chunks[] = str_replace($_reg[0], "{$_reg[1]}=\"{$_url}\"", $_tag); // New tag
			$_len      = strlen($_oldTag);
			$html      = substr($html, $_pos + $_len);
			$_posd     = $_posp + $_len;
		} // end foreach
		$_chunks[] = $html;
		$html = implode('', $_chunks);
	}

	// Substitute url() style tags:
	if (preg_match_all('/url\(([^\)]+)\)/i', $html, $reg)) {
		$i = 0;
		foreach($reg[1] as $match) {
			if (($match{0} == "'" && substr($match, -1) == "'") ||
				($match{0} == '"' && substr($match, -1) == '"')) {
				// Remove quotes (they shouldn't be there in the 1st place!):
				$match = substr($match, 1, strlen($match)-2);
			}
			if (preg_match('/^https?:\/\/|\/x\//', $match)) continue;

			$match = ($match{0} == '/') ? $base . $match : $base . $path . $match;
			$html = str_replace($reg[0][$i], 'url(' . $match . ')', $html);

			$i++;
		}
	}
	return $html;
}

/**
 * Fetch HTML from a given URL.
 *
 * @author Arjan Haverkamp
 *
 * @param $url(string) The URL to fetch
 * @param $error(string) Reference to error message
 * @return The HTML that was fetched, or false in case of an error
 * @returns mixed
 */
function slurpHTML($url, &$error) {
   //
   // Slurp the HTML using CURL:
   //
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)');
   curl_setopt($ch, CURLOPT_FAILONERROR, 1);
   @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
   curl_setopt($ch, CURLOPT_TIMEOUT, 30);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
   $html        = curl_exec($ch);
   $status      = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
   $curl_error  = curl_error($ch);
   curl_close($ch);

   // Make sure no CURL errors occured:
   if (!empty($curl_error)) {
      $error = "CURL error: {$curl_error} while connecting to {$url}";
      return false;
   }
   // Check for a valid HTTP status:
   if (!($status >= 200 && $status < 300)) {
      $error = "URL {$url} returned HTTP-status $status";
      return false;
   }

	// Add a 'meta content-type' tag to the HTML, if there is no such meta tag present yet.
	if (!preg_match('/<meta\s+http-equiv[^;]+;\s*charset=([^">]+)/i', $html, $reg)) {
		$html = preg_replace('/<head>/i', "<head>\n" . '<meta http-equiv="content-type" content="' . $contentType . '" />', $html);
	}

	//
	// Substitute paths in HTML:
	//
	$url_props = parse_url($url);

	$host = $url_props['scheme'] . '://' . $url_props['host'];
	$path = isset($url_props['path']) ? $url_props['path'] : '/';

	if (preg_match('/\.(php|php4|php3|phtml|cgi|cfml|pl|html?|jsp|asp|cfm)$/i', $path)) {
		$path = dirname($path);
	}
	if (substr($path, -1, 1) != '/'){
		$path .= '/';
	}

	// Replace MS-Word quotes:
	if (!is_utf8($html)) {
		$html = str_replace(array(chr(145),chr(146),chr(147),chr(148)), array("'","'",'"','"'), $html);
	}

	return fixUrlsInHtml($html, $host, $path);
}

/**
 * Check whether or not a string is an UTF-8 string.
 *
 * @author Arjan Haverkamp
 * @param $string(string) The string to check
 * @returns boolean
 */
function is_utf8($string) {
	return preg_match('%(?:
		[\xC2-\xDF][\x80-\xBF]					# non-overlong 2-byte
		|\xE0[\xA0-\xBF][\x80-\xBF]			# excluding overlongs
		|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}	# straight 3-byte
		|\xED[\x80-\x9F][\x80-\xBF]			# excluding surrogates
		|\xF0[\x90-\xBF][\x80-\xBF]{2}		# planes 1-3
		|[\xF1-\xF3][\x80-\xBF]{3}				# planes 4-15
		|\xF4[\x80-\x8F][\x80-\xBF]{2}		# plane 16
	)+%xs', $string);
}
?>
