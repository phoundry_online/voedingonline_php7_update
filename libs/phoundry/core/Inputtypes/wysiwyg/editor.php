<?php

$_file = @eval('return __FILE__;');
preg_match("'(.*?)\([0-9]+\)'si", $_file, $_reg);
require dirname($_reg[1]) . '/common.php';

$_js_args = 'bg=white';
if (isset($editorLang)) {
	$_js_args .= '&lang=' . $editorLang;
}

$editorLang = isset($editorLang) ? $editorLang : 'en';

print "<!-- Powered by the Phoundry Editor         (http://editor.phoundry.com) -->\n";
print "<!-- Programming by Arjan Haverkamp         (arjan-at-webpower.nl)       -->\n";
print '<!-- (c) ' . date('Y') . " by Web Power, The Netherlands (http://www.webpower.nl)     -->\n";
print <<<EOS
<style type="text/css">
<!--
.PEsep,.PEbutNot,.PEbutDown,.PEbut,.PEovr,.PErvo,.PEovr-sb,.PErvo-sb{background:url({$editorUrl}/pics/wysiwyg2.png) no-repeat}
.PEsep{width:2px;background-position:-400px -40px}
.PEbutNot{border:1px solid buttonface;filter:Gray() Alpha(Opacity=50);-moz-opacity:0.4}
.PEbutDown{background-color:gainsboro;border:1px inset window;cursor:pointer;cursor:hand}
.PEbut{border:1px solid buttonface}
.PEovr,.PEovr-sb{border:1px solid;border-color:buttonhighlight buttonshadow buttonshadow buttonhighlight}
.PErvo,.PErvo-sb{border:1px solid;border-color:buttonshadow buttonhighlight buttonhighlight buttonshadow}
.PEimg{width:20px;height:20px}
-->
</style>
EOS;

print '<script type="text/javascript" src="' . $editorUrl . '/editor.js.php?' . $_js_args . '"></script>' . "\n";

class PhoundryEditor {
var $name, $page, $width, $height, $dir, $value, $base, $useCSS, $css, $imgDir, $attachDir, $upload, $uploadMaxSize, $borders, $extensions, $classes, $tidy, $loadUrl, $disabled, $target, $showSize, $skipRegions, $absoluteLinks;

/**
* PhoundryEditor constructor.
* Initialises the Phoundry Editor.
*
* @author Arjan Haverkamp
* @param $name(string) The name of the Phoundry Editor. Use a name you would normally use in a &lt;TEXTAREA&gt;.
*/
function __construct($name) {
global $editorUrl;
if (substr($editorUrl,-1) == '/')
 $editorUrl = substr($editorUrl,0,-1);
$this->name   = $name;
$this->charset = 'utf-8';
$this->useCSS = false;
$this->page   = false;
$this->tables = true;
$this->forms  = false;
$this->dir    = $editorUrl;
$this->css    = '';
$this->imgDir = '';
$this->attachDir = '';
$this->width  = 600;
$this->height = 300;
$this->value  = '';
$this->upload = true;
$this->uploadMaxSize = 0;
$this->borders = false;
$this->tidy    = '';
$this->loadUrl = false;
$this->extensions = array();
$this->classes = array();
$this->disabled = array();
$this->base = '';
$this->target = '';
$this->showSize = false;
$this->skipRegions = false;
$this->absoluteLinks = false;
$this->strict = '';
}

/**
* Checks if the value passed is a positive integer.
* @author Arjan Haverkamp
* @param $value(mixed) The value to test.
* @return A boolean: true if the value passed is an integer, false otherwise.
* @returns boolean
* @private
*/
function isInt($value, $perc = false) {
if ($perc)
 return preg_match('/^[0-9]+%?$/', $value);
else
 return preg_match('/^[0-9]+$/', $value);
}

/**
* Checks if the value passed is a boolean.
* @author Arjan Haverkamp
* @param $value(mixed) The value to test.
* @return A boolean: true if the value passed is a boolean, false otherwise.
* @returns boolean
* @private
*/
function isBool($value) {
return ($value === 1 || $value === 0 || $value === true || $value === false);
}

/**
* Sets a feature for the Phoundry Editor.
* @author Arjan Haverkamp
* @param $name(string) The name of the feature to set.
* @param $value(mixed) The value of the feature to set.
*/
function set($name, $value) {

switch(strtolower($name)) {
 case 'charset':
	 // String
	 $this->charset = $value;
	 break;
 case 'width':
	 if (!$this->isInt($value,true))
		 die('Phoundry Editor: The <b>width</b> needs to be an integer or percentage!');
	 $this->width = $value;
	 break;
 case 'height':
	 if (!$this->isInt($value))
		 die('Phoundry Editor: The <b>height</b> needs to be an integer!');
	 $this->height = $value;
	 break;
 case 'page':
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>page</b> needs to be of type boolean!');
	 $this->page = $value;
	 break;
 case 'tables':
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>tables</b> needs to be of type boolean!');
	 $this->tables = $value;
	 break;
 case 'forms':
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>forms</b> needs to be of type boolean!');
	 $this->forms = $value;
	 break;
 case 'usecss':
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>useCSS</b> needs to be of type boolean!');
	 $this->useCSS = $value;
	 break;
 case 'upload':
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>upload</b> needs to be of type boolean!');
	 $this->upload = $value;
	 break;
 case 'uploadmaxsize':
	if (!is_float($value) && !is_int($value))
		die('Phoundry Editor: The <b>uploadMaxSize</b> needs to be a float!');
	$this->uploadMaxSize = $value;
	break;
 case 'tidy':
	 $value = strtolower($value);
	 if($value != 'html' && $value != 'xhtml')
		 die('Phoundry Editor: Property <b>tidy</b> needs to be <b>html</b> or <b>xhtml</b>!');
	 $this->tidy = $value;
	 break;
 case 'borders':
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>borders</b> needs to be of type boolean!');
	 $this->borders = $value;
	 break;
 case 'loadurl':
 case 'load_url':
	 // Load URL only available in registered version!
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>loadUrl</b> needs to be of type boolean!');
	 $this->loadUrl = $value;
	 break;
 case 'css':
	 // String:
	 $this->css = $value;
	 break;
 case 'imgdir':
	 // String:
	 $this->imgDir = $value;
	 if (substr($this->imgDir,-1) != '/')
		 $this->imgDir .= '/';
	 break;
 case 'attachdir':
	 // String
	 $this->attachDir = $value;
	 if (substr($this->attachDir,-1) != '/')
		 $this->attachDir .= '/';
	 break;
 case 'value':
	 // String:
	 $this->value = trim($value);
	 break;
 case 'extension':
	 // Array:
	 if (!is_array($value))
		 die('Phoundry Editor: Property <b>extensions</b> needs to be of type array!');
	 $this->extensions[] = $value;
	 break;
 case 'classes':
	 // Array:
	 if (!is_array($value))
		 die('Phoundry Editor: Property <b>classes</b> needs to be of type array!');
	 $this->classes = $value;
	 break;
 case 'disabled':
	 // Array:
	 if (!is_array($value))
		 die('Phoundry Editor: Property <b>disabled</b> needs to be of type array!');
	 $this->disabled = $value;
	 break;
 case 'base':
	 // String:
	 $this->base = $value;
	 break;
 case 'target':
	 // String:
	 $this->target = $value;
	 break;
 case 'showsize':
	 // Boolean:
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>showSize</b> needs to be of type boolean!');
	 $this->showSize = $value;
	 break;
 case 'skipregions':
	 // Boolean:
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>skipRegions</b> needs to be of type boolean!');
	 $this->skipRegions = $value;
	 break;
 case 'absolutelinks':
	 // Boolean:
	 if (!$this->isBool($value))
		 die('Phoundry Editor: Property <b>absoluteLinks</b> needs to be of type boolean!');
	 $this->absoluteLinks = $value;
	 break;
 case 'strict':
	// String:
 	$this->strict = $value;
	break;
 default:
	 die('Phoundry Editor: Unknown setting <b>' . $name . '</b>!');
}
}

/**
* Return the HTML to show the Phoundry Editor in your FORM.
* @author Arjan Haverkamp
* @return A string containing the HTML (XHTML) to use in your FORM.
* @returns string
*/
function html($alt = '')
{
global $_SERVER, $_browserOK, $editorLang, $PEWORDS, $_ua, $_isMidas;

$file = @eval('return __FILE__;');
preg_match("'(.*?)\([0-9]+\)'si", $file, $reg);
include dirname($reg[1]) . '/lang/' . $editorLang . '.php';

$pixel = $this->dir . '/pics/pixel.gif';
$name = $this->name;
$value = $this->value;
$html = '';
$hasRegions = !$this->skipRegions && preg_match('/<(\w+)\s+[^>]*?class\s*=\s*([\'"]?)(editable(-(html|text))?)\\2[^<>]*>/i', $this->value);

if (!$_browserOK) {
 $html .= PEword(3) . '<br />';
 $html .= '<textarea name="' . $name . '" cols="60" rows="10" wrap="physical" style="width:' . $this->width . ';height:' . $this->height . 'px;font:11px Courier New,Courier">' . PH::htmlspecialchars($value,ENT_COMPAT,$this->charset) . '</textarea>';
 $html .= '<br /><small><input type="button" onclick="PEpreview(\'' . $name . '\',this.form.' . $name . '.value)" value="' . PEword(4) . '" /></small>';
}
else {
 $html .= '<input type="hidden" id="' . $name . '" alt="' . $alt . '" name="' . $name . '" value="' . PH::htmlspecialchars($value,ENT_COMPAT,$this->charset) . '" />';

 $objH = ($this->forms) ? $this->height - 69 : $this->height - 46;	// Buttonbars
 $objH -= 34; // Footer

 $lineWord = PEword(246);
 $findWord = PEword(41);

 $advanced = isset($_COOKIE['PEadv']) && $_COOKIE['PEadv'] == 1;

 $html .= <<<EOM
<table cellspacing="0" cellpadding="3" style="background:buttonface" onmousedown="_curED=PEs['$name']" onmouseover="_curED=PEs['$name']" width="{$this->width}"><tr><td>
<div id="bttns_$name">
<table cellspacing="0" cellpadding="0" width="100%">
<tr style="background:#fff"><td><img src="{$pixel}" width="1" height="1" alt="" /></td></tr>
<tr><td><table cellspacing="0" cellpadding="0"><tr>
EOM;
if ($this->page && !in_array('pageprops',$this->disabled) && !$hasRegions)
$html .= '<td class="PEbut" style="background-position:0 0 0 0" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(247) . '" width="16" height="16" src="' . $pixel . '" onclick="_curED.pageProps()" /></td>';
if (!empty($this->imgDir) && !in_array('image',$this->disabled)) {
$html .= '<td class="PEbut" style="background-position:-20px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="m5017_' . $name . '"><img class="PEimg" alt="' . PEword(5) . '" src="' . $pixel . '" onclick="_curED.image()" /></td>';
$html .= '<td class="PEsep"></td>';
}
if (!empty($this->attachDir) && !in_array('attach',$this->disabled)) {
$html .= '<td class="PEbutNot" style="background-position:-40px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="attach_' . $name . '"><img class="PEimg" alt="' . PEword(216) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.attach()" /></td>';
$html .= '<td class="PEsep"></td>';
}
for ($x = 6; $x <= 54; $x++) {
eval('$w' . $x . ' = PEword(' . $x . ');');
}

if ($this->useCSS && count($this->classes)) {
$html .= '<td><select id="class_' . $name . '" style="font:Icon;width:90px" onChange="_curED.setClass(this.value)">';
$html .= '<option value="">' . PEword(137) . '</option>';
$html .= '<option value="0">' . PEword(219) . '</option>';
foreach($this->classes as $class)
$html .= '<option value="' . $class . '">' . $class . '</option>';
$html .= '</select></td>';
}
//else if (!$this->useCSS && !in_array('font', $this->disabled))
else if (!in_array('font', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-60px 0" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . $w6 . '" src="' . $pixel . '" onclick="_curED.font()" /></td>';

if (!in_array('bold', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-80px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="mbold_' . $name . '"><img class="PEimg" alt="' . PEword(8) . '" src="' . $pixel . '" onclick="_curED.cmd(\'bold\')" /></td>';
if (!in_array('italic', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-100px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="mitalic_' . $name . '"><img class="PEimg" alt="' . PEword(9) . '" src="' . $pixel . '" onclick="_curED.cmd(\'italic\')" /></td>';
if (!in_array('under', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-120px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="munderline_' . $name . '"><img class="PEimg" alt="' . PEword(10) . '" src="' . $pixel . '" onclick="_curED.cmd(\'underline\')" /></td>';
if (!in_array('strike', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-140px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="mstrikethrough_' . $name . '"><img class="PEimg" alt="' . PEword(272) . '" src="' . $pixel . '" onclick="_curED.cmd(\'strikethrough\')" /></td>';
if (!in_array('fgcolor', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-160px 0" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(11) . '" src="' . $pixel . '" onclick="_curED.color(\'fg\')" /></td>';
if (!in_array('bgcolor', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-180px 0" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(12) . '" src="' . $pixel . '" onclick="_curED.color(\'bg\')" /></td>';
$html .= '<td class="PEsep"></td>';
if (!in_array('subscript', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-200px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="msubscript_' . $name . '"><img class="PEimg" alt="' . PEword(13) . '" src="' . $pixel . '" onclick="_curED.sub(1)" /></td>';
if (!in_array('superscript', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-220px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="msuperscript_' . $name . '"><img class="PEimg" alt="' . PEword(14) . '" src="' . $pixel . '" onclick="_curED.sub(0)" /></td>';
if (!in_array('charmap', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-240px 0" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(15) . '" src="' . $pixel . '" onclick="_curED.charMap()" /></td>';
$html .= '<td class="PEsep"></td>';
if (!in_array('left', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-260px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="mjustifyleft_' . $name . '"><img class="PEimg" alt="' . PEword(16) . '" src="' . $pixel . '" onclick="_curED.cmd(\'justifyleft\')" /></td>';
if (!in_array('center', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-280px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="mjustifycenter_' . $name . '"><img class="PEimg" alt="' . PEword(17) . '" src="' . $pixel . '" onclick="_curED.cmd(\'justifycenter\')" /></td>';
if (!in_array('justify', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-300px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="mjustifyfull_' . $name . '"><img class="PEimg" alt="' . PEword(260) . '" src="' . $pixel . '" onclick="_curED.cmd(\'justifyfull\')" /></td>';
if (!in_array('right', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-320px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="mjustifyright_' . $name . '"><img class="PEimg" alt="' . PEword(18) . '" src="' . $pixel . '" onclick="_curED.cmd(\'justifyright\')" /></td>';
$html .= '<td class="PEsep"></td>';
if (!in_array('inindent', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-340px 0" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(19) . '" src="' . $pixel . '" onclick="_curED.cmd(\'indent\')" /></td>';
if (!in_array('deindent', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-360px 0" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(20) . '" src="' . $pixel . '" onclick="_curED.cmd(\'outdent\')" /></td>';
$html .= '<td class="PEsep"></td>';
if (!in_array('numlist', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-380px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="minsertorderedlist_' . $name . '"><img class="PEimg" alt="' . PEword(21) . '" src="' . $pixel . '" onclick="_curED.cmd(\'insertorderedlist\')" /></td>';
if (!in_array('bullist', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-400px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="minsertunorderedlist_' . $name . '"><img class="PEimg" alt="' . PEword(22) . '" src="' . $pixel . '" onclick="_curED.cmd(\'insertunorderedlist\')" /></td>';
$html .= '<td class="PEsep"></td>';
if (!in_array('hr', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-420px 0" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(23) . '" src="' . $pixel . '" onclick="_curED.hr()" /></td>';
if (!in_array('anchor', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-440px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="anchor_' . $name . '"><img class="PEimg" alt="' . PEword(24) . '" src="' . $pixel . '" onclick="_curED.link(\'anchor\')" /></td>';
if (!in_array('link', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-460px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="link_' . $name . '"><img class="PEimg" alt="' . PEword(25) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.link(\'link\',event.shiftKey)" /></td>';
if (!in_array('unlink', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-480px 0" onmouseover="_E(this)" onmouseout="_E(this)" id="unlink_' . $name . '"><img class="PEimg" alt="' . PEword(26) . '" src="' . $pixel . '" onclick="_curED.unlink()" /></td>';
$html .= '</tr></table></td></tr>';


$html .= '<tr style="background:#808080"><td><img src="' . $pixel . '" width="1" height="1" alt="" /></td></tr>';
$html .= '<tr style="background:#fff"><td><img src="' . $pixel . '" width="1" height="1" alt="" /></td></tr>';

$html .= '<tr><td><table cellspacing="0" cellpadding="0"><tr>';

if (!($_isMidas && $hasRegions)) {
	if (!in_array('find', $this->disabled)) {
		$html .= '<td class="PEbut" style="background-position:0 -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(41) . '..." src="' . $pixel . '" onclick="_curED.find()" /></td>';
	}
}

$html .= '<td class="PEsep"></td>';
if ($this->tables) {
if (!in_array('table', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-20px -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(27) . '" src="' . $pixel . '" onclick="_curED.table()" /></td>';
if (!in_array('insrow', $this->disabled)) {
	$html .= '<td class="PEbut" style="background-position:-40px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tinsrowtop_' . $name . '"><img class="PEimg" alt="' . PEword(276) . '" src="' . $pixel . '" onclick="_curED.cmd(\'insrow_top\')" /></td>';
	$html .= '<td class="PEbut" style="background-position:-60px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tinsrowbot_' . $name . '"><img class="PEimg" alt="' . PEword(277) . '" src="' . $pixel . '" onclick="_curED.cmd(\'insrow_bot\')" /></td>';
}
if (!in_array('delrow', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-80px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tdelrow_' . $name . '"><img class="PEimg" alt="' . PEword(29) . '" src="' . $pixel . '" onclick="_curED.cmd(\'delrow\')" /></td>';
if (!in_array('inscol', $this->disabled)) {
	$html .= '<td class="PEbut" style="background-position:-100px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tinscolleft_' . $name . '"><img class="PEimg" alt="' . PEword(278) . '" src="' . $pixel . '" onclick="_curED.cmd(\'inscol_left\')" /></td>';
	$html .= '<td class="PEbut" style="background-position:-120px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tinscolright_' . $name . '"><img class="PEimg" alt="' . PEword(279) . '" src="' . $pixel . '" onclick="_curED.cmd(\'inscol_right\')" /></td>';
}
if (!in_array('delcol', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-140px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tdelcol_' . $name . '"><img class="PEimg" alt="' . PEword(31) . '" src="' . $pixel . '" onclick="_curED.cmd(\'delcol\')" /></td>';
if (!in_array('inscell', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-160px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tinscell_' . $name . '"><img class="PEimg" alt="' . PEword(32) . '" src="' . $pixel . '" onclick="_curED.cmd(\'inscell\')" /></td>';
if (!in_array('delcell', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-180px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tdelcell_' . $name . '"><img class="PEimg" alt="' . PEword(33) . '" src="' . $pixel . '" onclick="_curED.cmd(\'delcell\')" /></td>';
if (!in_array('splitcell', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-200px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="Tsplitcell_' . $name . '"><img class="PEimg" alt="' . PEword(35) . '" src="' . $pixel . '" onclick="_curED.cmd(\'splitcell\')" /></td>';
$html .= '<td class="PEsep"></td>';
}
if (!in_array('cut', $this->disabled) && !$_isMidas)
$html .= '<td class="PEbut" style="background-position:-220px -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(38) . '" src="' . $pixel . '" onclick="_curED.cmd(\'cut\')" /></td>';
if (!in_array('copy', $this->disabled) && !$_isMidas)
$html .= '<td class="PEbut" style="background-position:-240px -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(39) . '" src="' . $pixel . '" onclick="_curED.cmd(\'copy\')" /></td>';
if (!in_array('paste', $this->disabled) && !$_isMidas)
$html .= '<td class="PEbut" style="background-position:-260px -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(40) . '" src="' . $pixel . '" onclick="_curED.cmd(\'paste\')" /></td>';
if (!$_isMidas) {
	$html .= '<td class="PEsep"></td>';
}

if (!in_array('spell', $this->disabled) && !$_isMidas && function_exists('pspell_check'))
$html .= '<td class="PEbut" style="background-position:-280px -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(228) . '" src="' . $pixel . '" onclick="_curED.spell()" /></td>';
if (!in_array('borders', $this->disabled) && !$_isMidas)
$html .= '<td class="PEbut" style="background-position:-300px -20px" onmouseover="_E(this)" onmouseout="_E(this)" id="borders_' . $name . '"><img class="PEimg" alt="' . PEword(43) . '" src="' . $pixel . '" onclick="_curED.showBorders()" /></td>';
$html .= '<td class="PEbut" style="background-position:-480px -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" src="' . $pixel . '" alt="Maximize" onclick="PEmaxi(\'PhEd_' . $name . '\')"></td>';
if (!in_array('help', $this->disabled))
$html .= '<td class="PEbut" style="background-position:-320px -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(45) . '" src="' . $pixel . '" onclick="_curED.help()" /></td>';
$html .= '<td class="PEsep"></td>';

if (!$_isMidas) {
	$checked = ($advanced) ? ' checked="checked" ' : ' ';
	$html .= '<td style="color:#1216bc"><i><label for="PhAv_' . $name . '">' . PEword(274) . ':</label></i></td><td><input type="checkbox" id="PhAv_' . $name . '"' . $checked . 'title="Advanced" onclick="_curED.toggleAdvanced(this.checked)" /></td>';
}

// Extensions:
foreach($this->extensions as $ext) {
	$class = (isset($ext['selection']) && $ext['selection']) ? 'PEbutNot' : 'PEbut';
	$func  = '_curED.' . $ext['jsfn'] . '()';
	$func  = ($class == 'PEbut') ? $func : 'if(this.className==\'PEovr\'){' . $func . '}';
	$html .= '<td class="' . $class . '" id="' . $ext['jsfn'] . '_' . $name . '" style="background:url(' . $ext['icon'] .')" onmouseover="_E(this)" onmouseout="_E(this)" onclick="' . $func . '"><img class="PEimg" alt="' . (isset($ext['alt']) ? $ext['alt'] : '') . '" src="' . $pixel . '" /></td>';
}

$html .= '</tr></table></td></tr>';

// Advanced

$html .= '<tr style="background:#808080"><td><img src="' . $pixel . '" width="1" height="1" alt="" /></td></tr>';
$html .= '<tr style="background:#fff"><td><img src="' . $pixel . '" width="1" height="1" alt="" /></td></tr>';

$html .= '<tr><td><table cellspacing="0" cellpadding="0"><tr>';

if($this->forms) {

if (!in_array('form', $this->disabled))
$html .= '<td class="PEbut" style="background-position:0 -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Fform_' . $name . '"><img class="PEimg" alt="' . PEword(46) . '" src="' . $pixel . '" onclick="_curED.form()" /></td>';
if (!in_array('textbox', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-20px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Ftext_' . $name . '"><img class="PEimg" alt="' . PEword(47) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'text\')" /></td>';
if (!in_array('textarea', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-40px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Ftextarea_' . $name . '"><img class="PEimg" alt="' . PEword(48) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'textarea\')" /></td>';
if (!in_array('password', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-60px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Fpassword_' . $name . '"><img class="PEimg" alt="' . PEword(242) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'password\')" /></td>';
if (!in_array('hidden', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-80px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Fhidden_' . $name . '"><img class="PEimg" alt="' . PEword(49) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'hidden\')" /></td>';
if (!in_array('radio', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-100px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Fradio_' . $name . '"><img class="PEimg" alt="' . PEword(50) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'radio\')" /></td>';
if (!in_array('checkbox', $this->disabled))
$html .= '<td class="PEbutNot"  style="background-position:-120px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Fcheckbox_' . $name . '"><img class="PEimg" alt="' . PEword(51) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'checkbox\')" /></td>';
if (!in_array('select', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-140px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Fselect_' . $name . '"><img class="PEimg" alt="' . PEword(52) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'select\')" /></td>';
if (!in_array('submit', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-160px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Fsubmit_' . $name . '"><img class="PEimg" alt="' . PEword(53) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'submit\')" /></td>';
if (!in_array('reset', $this->disabled))
$html .= '<td class="PEbutNot" style="background-position:-180px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="Freset_' . $name . '"><img class="PEimg" alt="' . PEword(54) . '" src="' . $pixel . '" onclick="if(this.parentNode.className!=\'PEbutNot\')_curED.formEl(\'reset\')" /></td>';
$html .= '<td class="PEsep"></td>';
}

$html .= '<td id="adv_on_' . $name . '" style="display:' . ($advanced||$_isMidas ? '' : 'none') . '"><table cellspacing="0" cellpadding="0"><tr>';

if (!($_isMidas && $hasRegions)) {
	if (!$hasRegions && !in_array('replace', $this->disabled)) {
		$html .= '<td class="PEbut" style="background-position:-200px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(55) . '" src="' . $pixel . '" onclick="_curED.sbsTxt()" /></td>';
	}

	if ($this->tidy) {
		$html .= '<td class="PEbut" style="background-position:-220px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" id="tidy_' . $name . '" alt="Tidy" src="' . $pixel . '" onclick="_curED.tidyMe(event,1)" /></td>';
	}

	if ($this->strict) {
		$html .= '<td class="PEbut" style="background-position:-240px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" id="strict_' . $name . '" alt="' . $this->strict . ' check" src="' . $pixel . '" onclick="_curED.strictHTML()" /></td>';
	}

	if ($this->page && !$hasRegions && $this->loadUrl && !in_array('loadurl', $this->disabled)) {
		$html .= '<td class="PEbut" style="background-position:-260px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="Load URL" src="' . $pixel . '" onclick="_curED.load()" /></td>';
	}

	if ($this->showSize) {
		$html .= '<td class="PEbut" style="background-position:-280px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" id="size_' . $name . '" alt="Document size (KB)" src="' . $pixel . '" onclick="_curED.setShowSize(null)" /></td>';
	}
}

if (!in_array('stats', $this->disabled) && !$_isMidas) {
	$html .= '<td class="PEbut" style="background-position:-300px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(202) . '" src="' . $pixel . '" onclick="_curED.stats()" /></td>';
}

if (!in_array('cleanup', $this->disabled) && !$hasRegions){
	$html .= '<td class="PEbut" style="background-position:-320px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="' . PEword(44) . '" src="' . $pixel . '" onclick="_curED.clean()" />';
	if (!$_isMidas) {
		$checked = isset($_COOKIE['PEcln']) && $_COOKIE['PEcln'] == 1 ? ' checked="checked" ' : ' ';
		$html .= '<input type="checkbox"' . $checked . 'id="PhAc_' . $name . '" title="' . PEword(270) . '" onclick="PEsetCookie(\'PEcln\',this.checked?1:0,365);_curED.doc.onkeydown=(this.checked&amp;&amp;_curED.advanced)?function(){_curED.keyDown(_curED.win.event)}:null" />';
	}
	$html .= '</td>';
}

$html .= '</tr></table></td>';

$html .= '<td id="adv_off_' . $name . '" style="display:' . ($advanced&&!$_isMidas ? 'none' : '') . '"><table cellspacing="0" cellpadding="0"><tr>';

if (!in_array('undo', $this->disabled))
	$html .= '<td class="PEbut" style="background-position:-360px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="mundo_' . $name . '"><img class="PEimg" alt="' . PEword(36) . '" src="' . $pixel . '" onclick="_curED.cmd(\'undo\')" /></td>';
if (!in_array('redo', $this->disabled))
	$html .= '<td class="PEbut" style="background-position:-380px -40px" onmouseover="_E(this)" onmouseout="_E(this)" id="mredo_' . $name . '"><img class="PEimg" alt="' . PEword(37) . '" src="' . $pixel . '" onclick="_curED.cmd(\'redo\')" /></td>';
$html .= '</tr></table></td>';

$html .= '</tr></table></td></tr>';

// End advanced

$html .= '</table></div>';
$html .= '<div><iframe class="wysiwyg" id="PhEd_' . $name . '" frameborder="0" src="' . $this->dir . '/blank.html" style="width:100%;height:' . $objH . 'px;border:1px solid threedshadow;margin-bottom:2px"></iframe></div>';
			$html .= '<table cellspacing="0" cellpadding="0" width="100%"><tr>';

			$html .= '<td id="mode0_' . $name . '" class="PErvo-sb" style="background-position:-340px -20px"><img src="' . $pixel . '" alt="" width="41" height="18" onclick="_curED.setMode(0)" /></td>';
			if (!$hasRegions) {
				$html .= '<td id="mode1_' . $name . '" class="PEovr-sb" style="background-position:-380px -20px"><img src="' . $pixel . '" alt="" width="41" height="18" onclick="_curED.setMode(1)" /></td>';
			}
			if (!in_array('preview', $this->disabled))
				$html .= '<td id="mode2_' . $name . '" class="PEovr-sb" style="background-position:-420px -20px"><img src="' . $pixel . '" alt="" width="41" height="18" onclick="_curED.setMode(2)" /></td>';

			$html .= '<td width="100%"' . (!in_array('statusbar', $this->disabled) ? ' id="status_' . $name . '"' : '') . ' style="font:Icon;color:#000;border:1px solid;border-color:buttonshadow buttonhighlight buttonhighlight buttonshadow;cursor:default">&nbsp;</td>';
			if ($this->showSize)
				$html .= '<td nowrap="nowrap" id="PhSz_' . $name . '" style="font:Icon;color:#000">&nbsp;</td>';
			$html .= '<td><div id="PhCv_' . $name . '" style="height:1px;width:1px;overflow:hidden" contenteditable="true" onpaste="setTimeout(\'_curED.pasteMS()\',100)"></div></td>';
			$html .= "</tr></table>\n";
			$html .= "</td></tr></table>\n";
		}

		/**
		 * Bit pattern used here:
		 * 	TABLES = 1;
		 *		FORMS = 2;
		 * 	USECSS = 4;
		 *		UPLOAD = 8;
		 *    PAGE = 16;
		 *		BORDERS = 32;
		 *    LOADURL = 128;
		 */
		$feat = 0;
		if($this->tables)  $feat += 1;
		if($this->forms)   $feat += 2;
		if($this->useCSS)  $feat += 4;
		if($this->upload)  $feat += 8;
		if($this->page)    $feat += 16;
		if($this->borders) $feat += 32;
		if($this->showSize) $feat += 64;
		if($this->loadUrl) $feat += 128;
		if($this->skipRegions) $feat += 256;
		if($this->absoluteLinks) $feat += 512;
		$imgDir = PEescSquote($this->imgDir);
		$attachDir = PEescSquote($this->attachDir);
		if ($this->useCSS && count($this->classes))
			$classes = '[\'' . implode("','", $this->classes) . '\']';
		else
			$classes = '[]';

		$sels = array();
		foreach($this->extensions as $ext) {
			if (isset($ext['selection']) && $ext['selection'])
				$sels[] = "'" . $ext['jsfn'] . "'";
		}
		$sels = '[' . implode(',', $sels) . ']';

		$html .= "<script type=\"text/javascript\">\n";

		if ($this->base === '')
			$html .= "PEaddEditor('$name','{$this->css}','$imgDir','$attachDir',{$this->uploadMaxSize},'{$this->tidy}','{$this->strict}',$classes,$feat,$sels,'{$this->target}');\n";
		else
			$html .= "PEaddEditor('$name','{$this->css}','$imgDir','$attachDir',{$this->uploadMaxSize},'{$this->tidy}','{$this->strict}',$classes,$feat,$sels,'{$this->target}','{$this->base}');\n";

		$html .= "</script>\n";

		return $html;
	}
}
?>
