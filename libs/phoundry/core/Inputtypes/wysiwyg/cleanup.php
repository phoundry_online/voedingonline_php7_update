<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title>Clean up HTML <?= str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function submitMe() {
	var f = _D.forms['phoundry'], txt = winArgs['editor'].doc.body.innerHTML;
	//txt=txt.replace(/<\\?xml[^>]*>/g,'').replace(/<\/?[a-z]+:[^>]*>/ig,'').replace(/(<[^>]+) style=\"[^\"]*\"([^>]*>)/ig,'$1 $2').replace(/<span[^>]*><\/span[^>]*>/ig,'').replace(/<span><span>/ig,'<span>').replace(/<\/span><\/span>/ig,'</span>');
	txt=txt.replace(/<o:p>&nbsp;<\/o:p>/g,'');
	txt=txt.replace(/<o\:p><\/o\:p>/g,'');
	txt=txt.replace(/<st1:[^>]*>/g,''); 
	txt=txt.replace(/mso-[^\";]*/g,'');
	txt=txt.replace(/<\\?xml[^>]*>/g,'')
	txt=txt.replace(/<\\?\?xml[^>]*>/ig,'');
	txt=txt.replace(/<\/?\w+:[^>]*>/ig,'')
	txt=txt.replace(/(<[^>]+) style=\"[^\"]*\"([^>]*>)/ig,'$1 $2');
	txt=txt.replace(/<(\w[^>]*) class=([^ |>]*)([^>]*)/gi,"<$1$3");
	txt=txt.replace(/<(\w[^>]*) style="([^"]*)"([^>]*)/gi,"<$1$3");
	txt=txt.replace(/<(\w[^>]*) lang=([^ |>]*)([^>]*)/gi,"<$1$3");
	txt=txt.replace(/<span[^>]*><\/span[^>]*>/ig,'');
	txt=txt.replace(/<span><span>/ig,'<span>');
	txt=txt.replace(/<\/span><\/span>/ig,'</span>');

	if(!winArgs['editor'].classes.length)
		txt=txt.replace(/(<[^>]+) class=[^ |^>]*([^>]*>)/ig,'$1 $2');
	if (f.span.checked)
		txt=txt.replace(/<\/?span[^>]*>/ig,'');
	if (f.font.checked)
		txt=txt.replace(/<\/?font[^>]*>/ig,'');
	winArgs['editor'].initWYSIWYG(txt);
//	winArgs['editor'].doc.body.innerHTML = txt;
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

tt {
	font: 11px Courier New;
}

.txt,select { width:200px }

-->
</style>
</head>
<body oncontextmenu="return false">
<form name="phoundry">
<fieldset>
<legend><b>Clean up HTML</b></legend>
<table>
<tr><td><?= PEword(116) ?></td></tr>
<tr><td>
	<label for="span"><?= PEword(1) ?></label>
	<input type="checkbox" id="span" value="1" />
</td></tr>
<tr><td>
	<label for="font"><?= PEword(2) ?></label>
	<input type="checkbox" id="font" value="1" />
</td></tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" oncLick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
