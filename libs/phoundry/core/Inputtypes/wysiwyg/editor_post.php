<?php

function PEexecProgram($command, $stdin, &$stdout, &$stderr) {
	$descriptorspec = array(
   	0 => array('pipe', 'r'), // stdin
   	1 => array('pipe', 'w'), // stdout
   	2 => array('pipe', 'w'), // stderr
	);

	$process = proc_open($command, $descriptorspec, $pipes);

	if (!is_resource($process)) {
		return false;
	}

	if (is_resource($process)) {
		$txOff = 0; $txLen = strlen($stdin);
		$stdout = ''; $stdoutDone = false;
		$stderr = ''; $stderrDone = false;
		stream_set_blocking($pipes[0], false); // Make stdin/stdout/stderr non-blocking
		stream_set_blocking($pipes[1], false);
		stream_set_blocking($pipes[2], false);
		if ($txLen == 0) { fclose($pipes[0]); }
		while(true) {
			$rx = array(); // The program's stdout/stderr
			if (!$stdoutDone) { $rx[] = $pipes[1]; }
			if (!$stderrDone) { $rx[] = $pipes[2]; }
			$tx = array(); // The program's stdin
			if ($txOff < $txLen) { $tx[] = $pipes[0]; }
			stream_select($rx, $tx, $ex = null, null, null); // Block till r/w possible
			if (!empty($tx)) {
				$txRet = fwrite($pipes[0], substr($stdin, $txOff, 8192));
				if ($txRet !== false) { $txOff += $txRet; }
				if ($txOff >= $txLen) { fclose($pipes[0]); }
			}
			foreach ($rx as $r) {
				if ($r == $pipes[1]) {
					$stdout .= fread($pipes[1], 8192);
					if (feof($pipes[1])) { fclose($pipes[1]); $stdoutDone = true; }
				} else if ($r == $pipes[2]) {
					$stderr .= fread($pipes[2], 8192);
					if (feof($pipes[2])) { fclose($pipes[2]); $stderrDone = true; }
				}
			}
			if ($txOff >= $txLen && $stdoutDone && $stderrDone) break;
		}
		$exitCode = proc_close($process);
	}

	return $exitCode;
}

function PEtidyUp() {
	// We don't tidy content that was GET'ed!!!
	if ($_SERVER['REQUEST_METHOD'] == 'GET' || count($_POST) == 0 || !isset($_GET['_pe_eds'])) {
		return;
	}

	$PEeds = explode('|', $_GET['_pe_eds']);
	unset($_GET['_pe_eds']);

	foreach($PEeds as $ed) {
		$feat  = explode('.', $ed);
		$name  = $feat[0];
		$feat  = $feat[1];
	
		// Page = 1, html = 2, xhtml = 4, utf-8 = 8
		$page = ($feat & 1);
		if     ($feat & 2) $what = 'html';
		elseif ($feat & 4) $what = 'xhtml';
		else               continue; // If $what is empty, tidy is not used -> continue!
		
		$charset = 'latin1';
		if ($feat & 8) {
			$charset = 'utf8';
		}

		$html = $orgHtml = $_POST[$name];
		$html = preg_replace("'<\s*SCRIPT\s+LANGUAGE\s*=\s*\"?php\"?\s*>(.*?)</SCRIPT>'si", "<?php$1?>", $html);
		$html = preg_replace('/<\/?o:p>/i', '', $html);

		$file = @eval('return __FILE__;');
		preg_match("'(.*?)\([0-9]+\)'si", $file, $_reg);
		$cwd = dirname($_reg[1]);

		$command  = "{$cwd}/tidy/tidy -{$charset} --add-xml-decl false --quiet true --tidy-mark false --wrap-php false --preserve-entities true"; 
		if ($what == 'xhtml') {
			$command .= ' -asxhtml';
		}
		if (!$page) {
			$command .= ' --show-body-only true --bare true --wrap 120';
		}
		else {
			$command .= ' --wrap 400';
		}

		$exitCode = PEexecProgram($command, $html, $tidied, $stderr);
		if ($exitCode === false) {
			// Cannot open process, revert to original HTML:
			die("Phoundry Editor: Cannot execute $cmd!\n");
		}

		if ($exitCode > 1) {
			$tidied = $orgHtml;
		}
		else {
			$tidied = preg_replace("'<\?php(.*?)\?>'si", '<script language="php">$1</script>', $tidied);
		}
		$tidied = trim($tidied);
		$_POST[$name] = trim($tidied);
	}
}

PEtidyUp();
?>
