<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(273) . str_repeat('&nbsp; ', 70) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function init() {
	var f = _D.forms['phoundry'], arg;
	if (winArgs['el']) {
		EL = winArgs['el'];
		f['html'].value = winArgs['html'].replace(/%7B/gi, '{').replace(/%7D/gi, '}');
	}
	f['html'].focus();
}

function submitMe() {
	var f = _D.forms['phoundry'], A = [];
	EL.innerHTML = f['html'].value;
	winArgs['editor'].updMenu();
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
</head>
<body onload="init()">
<form name="phoundry">
<fieldset>
<legend><b><?= PEword(273) ?></b></legend>
<textarea name="html" style="width:560px;height:300px;font:11px Courier New,Courier"></textarea>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
