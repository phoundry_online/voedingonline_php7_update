<?php

/********** REALLY IMPORTANT SETTINGS! **********/
$PE_DEBUG = false;
$PE_VERSION = '6.7'; // March 18, 2009
// Last modified: March 18, 2009
/********** /REALLY IMPORTANT SETTINGS! **********/

$_ua = &$_SERVER['HTTP_USER_AGENT'];
$_ieVer = 0;
if (preg_match('/MSIE ([0-9\.]+)/', $_ua, $_reg)) {
	$_ieVer = (float)$_reg[1];
}
$_browserOK = ($_ieVer >= 5.5 && preg_match('/Win/', $_ua) && !preg_match('/Opera/', $_ua)) || 
              (preg_match('/Gecko/', $_ua, $reg));

$_isMidas = strpos($_ua, 'Gecko') !== false;

/**
 * Get a word from the language-file.
 * Has a variable number of arguments, one is required: the id of the word to
 * get. The other arguments represent values that should be replaced in the
 * word.
 * Example: word[24] = 'This is example #1 and #2'.
 * A call to PEword(24,'one','two') would result in 'This is example one and two'.
 *
 * @author Arjan Haverkamp
 * @return The desired word(s).
 */
function PEword() {
	global $PEWORDS;
	$ss = $PEWORDS[func_get_arg(0)];
	$na = func_num_args();
	for ($x = 1; $x < $na; $x++) {
		$tmp = func_get_arg($x);
		$ss = preg_replace('/#' . $x . '/', $tmp, $ss);
	}
	return $ss;
}

/**
 * Escape all single quotes in string $str with a backslash.
 *
 * @author Arjan Haverkamp
 * @param $str(string)	The string to escape.
 * @return The escaped string.
 */
function PEescSquote($str) {
	return str_replace("'", "\\'", $str);
}

/**
 * Encode a string using the Web Power encoding.
 *
 * @author Arjan Haverkamp
 * @param $instr(string) The string to decode.
 * @return A string containing the encoded value of $instr.
 * @returns string
 */
function PEencode($str, $what) {
	if ($what == 'license')
		$str .= chr(255) . crc32('P' . $str . 'E');
	else
		$str .= chr(255) . crc32('u' . $str . 'l');
	
	$ret = '';
	for ($x = 0; $x < strlen($str); $x++)
		$ret .= bin2hex($str{$x});
	return strrev($ret);
}

/**
 * Decode a string using the Web Power encoding.
 *
 * @author Arjan Haverkamp
 * @param $instr(string) The string to decode.
 * @return A string containing the decoded value of $instr.
 * @returns string
 */
function PEdecode($instr,$what) {
	$instr = strrev($instr);
	$orig = '';
	if (!strlen($instr) || strlen($instr) % 2 != 0)
		return false;
	for ($i = 0; $i < strlen($instr); $i += 2)
		$orig .= chr(hexdec($instr{$i}) * 16 + hexdec($instr{$i+1}));
	$tmp = explode(chr(255), $orig);
	if ($what == 'license') {
		if (!isset($tmp[1]) || crc32('P' . $tmp[0] . 'E') != $tmp[1])
			return false;
	}
	else {
		if (!isset($tmp[1]) || crc32('u' . $tmp[0] . 'l') != $tmp[1])
			return false;
	}
	return $tmp[0];
}

/**
 * Recursively remove a directory and all files/dirs in it.
 *
 * @author Arjan Haverkamp
 * @param $dirName(string)	The path of the directory to remove.
 */
function PERmDirR($dirName) { 
	if (is_dir($dirName)) {
		$d = opendir($dirName); 
		while($entry = readdir($d)) { 
			if ($entry != '.' && $entry != '..' && $entry != '') { 
				if (is_dir($dirName.'/'.$entry)) { 
					PERmDirR($dirName.'/'.$entry); 
				} else { 
					unlink($dirName.'/'.$entry); 
				} 
			} 
		} 
		closedir($d); 
		rmdir($dirName);
	}
}

?>
