<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(48) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function init() {
	var f = _D.forms['phoundry'];

	if (winArgs['editor'].feat&4) {
		if (winArgs['editor'].classes.length) {
			var classes = winArgs['editor'].classes;
			var html = '<select name="Fclass"><option value=""><?= PEword(219) ?></option>';
			for(var x = 0; x < classes.length; x++)
				html += '<option value="' + classes[x] + '">' + classes[x] + '</option>';
			html += '</select>';
			_D.getElementById('cssTD').innerHTML = html;
		}
		_D.getElementById('cssTR').style.visibility='visible';
	}

	if (winArgs['el']) {
		EL = winArgs['el'];
		f.Fname.value = EL.name;
		f.Fvalue.value = EL.value;
		f.Fcols.value = EL.cols;
		f.Frows.value = EL.rows;
		f.Fwrap.value = EL.wrap;
	}

	if (winArgs['editor'].feat&4 && winArgs['editor'].classes.length && EL && EL.className == '')
		f.Fclass.selectedIndex = 0;
	else if (winArgs['editor'].feat&4 && EL)
		f.Fclass.value = EL.className;

	f.Fname.focus();
}

function submitMe() {
	var f = _D.forms['phoundry'], A=[], tag;
	if (f.Fname.value == '') { alert('<?= PEescSquote(PEword(185)) ?>'); f.Fname.focus(); return }
	A['name'] = f.Fname.value;
	A['value'] = f.Fvalue.value;
	A['rows'] = f.Frows.value;
	A['cols'] = f.Fcols.value;
	A['wrap'] = f.Fwrap.value;
	A['class'] = f.Fclass.value;
	if (!EL) {
		tag='<textarea name="'+A['name']+'"';
		tag+=A['cols']==''?'':' cols='+A['cols'];
		tag+=A['rows']==''?'':' rows='+A['rows'];
		tag+=A['wrap']==''?'':' wrap='+A['wrap'];
		tag+=A['class']==''?'':' class="'+A['class']+'"';
		tag+='>'+A['value']+'</textarea>';
		winArgs['editor'].insertHTML(tag);
	} else {
		if (A['name'])
			EL.setAttribute('name', A['name']);
		else
			EL.removeAttribute('name',0);
		if (A['rows'])
			EL.setAttribute('rows', A['rows']);
		else
			EL.removeAttribute('rows',0);
		if (A['cols'])
			EL.setAttribute('cols', A['cols']);
		else
			EL.removeAttribute('cols',0);
		if (A['wrap'])
			EL.setAttribute('wrap', A['wrap']);
		else
			EL.removeAttribute('wrap',0);
		if (A['class'])
			EL.setAttribute(isMidas?'class':'className', A['class']);
		else
			EL.removeAttribute(isMidas?'class':'className',0);
		if (A['value'])
			EL.setAttribute('value', A['value']);
		else
			EL.removeAttribute('value',0);

	}
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
</head>
<body onload="init()" oncontextmenu="return false">
<form name="phoundry">
<fieldset>
<legend><b><?= PEword(48) ?></b></legend>
<table>
<tr>
	<td><?= PEword(178) ?>:</td>
	<td colspan="3"><input class="txt" type="text" name="Fname" size="20" style="width:200px" /></td>
</tr>
<tr>
	<td><?= PEword(165) ?>:</td>
	<td><input class="txt" type="text" name="Fcols" size="4" onblur="this.value=checkDigit(this.value,0)" /></td>
	<td><?= PEword(164) ?>:</td>
	<td><input class="txt" type="text" name="Frows" size="4" onblur="this.value=checkDigit(this.value,0)" /></td>
</tr>
<tr>
	<td><?= PEword(188) ?>:</td>
	<td colspan="3"><select name="Fwrap">
	<option value="off">off</option>
	<option value="soft">soft</option>
	<option value="hard">hard</option>
	</select></td>
</tr>
<tr id="cssTR" style="visibility:hidden">
	<td><?= PEword(137) ?>:</td>
	<td id="cssTD" colspan="3"><input class="txt" type="text" name="Fclass" size="20" style="width:200px" /></td>
</tr>
<tr>
	<td><?= PEword(179) ?>:</td>
	<td colspan="3">
	<textarea class="txt" name="Fvalue" style="width:200px; height:50px"></textarea>
	</td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
