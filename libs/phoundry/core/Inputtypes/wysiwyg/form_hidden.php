<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(49) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function init() {
	var f = _D.forms['phoundry'];

	if (winArgs['el']) {
		EL = winArgs['el'];
		f.Fname.value = EL.name;
		f.Fvalue.value = EL.value;
	}
	
	f.Fname.focus();	
}

function submitMe() {
	var f = _D.forms['phoundry'], A = [], tag;
	if (f.Fname.value == '') { alert('<?= PEescSquote(PEword(185)) ?>'); f.Fname.focus(); return }
	var A = [];
	A['name'] = f.Fname.value;
	A['value'] = f.Fvalue.value;
	if (!EL) {
		tag='<input type="hidden" name="'+A['name']+'"';
		tag+=A['value']==''?'':' value="'+A['value']+'"';
		tag+='>';
		winArgs['editor'].insertHTML(tag);
	}
	else {
		if (A['name'])
			EL.setAttribute('name', A['name']);
		else
			EL.removeAttribute('name',0);
		if (A['value'])
			EL.setAttribute('value', A['value']);
		else
			EL.removeAttribute('value',0);
	}
	_W.close();
}

//][>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
</head>
<body onload="init()" oncontextmenu="return false">
<form name="phoundry">
<fieldset>
<legend><b><?= PEword(49) ?></b></legend>
<table>
<tr>
	<td><?= PEword(178) ?>:</td>
	<td><input class="txt" type="text" name="Fname" size="20" style="width:200px" /></td>
</tr>
<tr>
	<td><?= PEword(179) ?>:</td>
	<td><input class="txt" type="text" name="Fvalue" size="20" style="width:200px" /></td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
