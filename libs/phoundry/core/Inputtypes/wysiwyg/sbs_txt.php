<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(55) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function submitMe() {
	var f = _D.forms['phoundry'], html = winArgs['editor'].mode==0 ? winArgs['editor'].doc.body.innerHTML : winArgs['editor'].ed.value;
	var regexp = f.REregexp.checked, src = f.REsrc.value, dst = f.REdst.value;
	if (!regexp)
		src=src.replace(/([\.\\\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g,"\\$1");
	m=f.REcase.checked?'g':'gi';
	if (winArgs['editor'].mode==0) {
		re=new RegExp('(?![^<]+>)'+src+'(?![^<]+>)',m);
		winArgs['editor'].doc.body.innerHTML=html.replace(re,dst);
	}
	else {
		re=new RegExp(src,m);
		winArgs['editor'].ed.value=html.replace(re,dst)
	}
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.txt,select { width:200px }

-->
</style>
</head>
<body onload="_D.forms['phoundry'].REsrc.focus()" oncontextmenu="return false">
<form name="phoundry">
<fieldset>
<legend><b><?= PEword(55) ?></b></legend>
<table>
<tr>
	<td><?= PEword(195) ?>:</td>
	<td colspan="3"><input class="txt" type="text" name="REsrc" size="20" /></td>
</tr>
<tr>
	<td><?= PEword(196) ?>:</td>
	<td colspan="3"><input class="txt" type="text" name="REdst" size="20" /></td>
</tr>
<tr>
	<td><label for="REcase"><?= PEword(197) ?>:</label></td><td><input type="checkbox" id="REcase" value="1" /></td>
	<td align="right"><label for="REregexp"><?= PEword(198) ?>:</label></td><td><input type="checkbox" id="REregexp" value="1" /></td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" oncLick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
