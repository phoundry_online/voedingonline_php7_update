<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(41) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;
var obj, selection, openerDoc, matches = [], searching=false, wysiwyg=true;

function findTextWYSIWYG(obj, query, ignoreCase) {
	matches = [];
	if (ignoreCase)query = query.toLowerCase();
	var tw = openerDoc.createTreeWalker(obj, NodeFilter.SHOW_TEXT, {acceptNode: function(node) {return NodeFilter['FILTER_'+(RegExp(query, (ignoreCase ? 'i' : '')).test(node.nodeValue)?'ACCEPT':'REJECT')]}},true),offsets=[],totalMatches,trueOffsetDiff,range=openerDoc.createRange();
	offsets[-1] = query.length * -1;
	while (tw.nextNode()) {
		totalMatches = tw.currentNode.nodeValue.split(RegExp(query,(ignoreCase?'i':''))).length-1;
		for (var i = 0; i < totalMatches; i++) {
			trueOffsetDiff = offsets[offsets.length-1]+query.length;
			offsets[offsets.length] = tw.currentNode.nodeValue.substr(trueOffsetDiff)[ignoreCase ? 'toLowerCase' : 'toString']().indexOf(query) + trueOffsetDiff;
			range.selectNode(tw.currentNode);
			range.setStart(tw.currentNode,offsets[offsets.length-1]);
			range.setEnd(tw.currentNode,range.startOffset+query.length);
			matches[matches.length]=range.cloneRange()
		}
		offsets.length=0
	}
	return(tw.currentNode != obj)
}

function hiliteTextWYSIWYG() {
	if (matches.length>0) {
		selection.removeAllRanges();
		selection.addRange(matches.shift());
		return true
	}
	return false
}

function findTextPlain(input, search, ignoreCase) {
	matches = [];
	var txt = input.value, re = new RegExp(search,(ignoreCase?'i':'')), match = re.exec(txt), idx = 0, end;
	while(match) {
		end   = match.index + match[0].length;
		matches.push((match.index+idx)+','+(end+idx));
		txt = txt.substr(end,txt.length);
		match = re.exec(txt);
		idx += end;
	}
}

function hiliteTextPlain(input) {
	var match = matches.shift().split(',');
	input.focus();
	input.setSelectionRange(match[0], match[1]);
}

function init() {
	openerDoc = _W.opener.document;
	obj = winArgs['obj']; 
	if (winArgs['sel']) {
		wysiwyg = true;
		selection = winArgs['sel'];
	}
	else
		wysiwyg = false;
	_D.forms[0].word.focus();
}

function findNext() {
	if (searching == false) {
		var cases = _D.forms[0].cases.checked == true, word = _D.forms[0].word.value;
		if (word == '') return;
		if (wysiwyg)
			findTextWYSIWYG(obj,word,!cases);
		else
			findTextPlain(obj,word,!cases);
		searching = true;
	}
	if (matches.length==0){
		alert('Finished searching the document!');
		searching = false;
	}
	else {
		if (wysiwyg)
			hiliteTextWYSIWYG();
		else
			hiliteTextPlain(obj);
	}
}

function checkInput(el,event) {
	if (event.keyCode==27)_W.close();
	var but = _D.getElementById('nextBut');
	but.disabled = (el.value == '') ? true : false;
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.txt { width:180px }
.but { width:70px; margin:2px}

-->
</style>
</head>
<body onload="init()">
<fieldset>
<legend><b><?= PEword(41) ?></b></legend>
<form onsubmit="findNext();return false">

<table cellspacing="0" cellpadding="0" border="0"><tr>
<td valign="top">

<table>
<tr>
	<td nowrap="nowrap">Find what:</td>
	<td><input class="txt" type="text" name="word" size="20" onkeyup="checkInput(this,event)" /></td>
</tr>
</table>

<table cellspacing="0" cellpadding="0" border="0">
<tr><td valign="top">

<table>
<tr>
	<td><input id="cases" type="checkbox" /></td><td><label for="cases">Match case</label></td>
</tr>
</table>

</td><td>&nbsp;</td>
</tr></table>

</td><td>&nbsp;</td>

</td><td valign="top">
	<input id="nextBut" type="button" disabled="disabled" value="Find next" onclick="findNext()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</td>
</tr></table>

</form>
</fieldset>

</body>
</html>
