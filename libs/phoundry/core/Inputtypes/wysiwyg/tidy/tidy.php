<?php

function execProgram($command, $stdin, &$stdout, &$stderr) {
	$descriptorspec = array(
   	0 => array('pipe', 'r'), // stdin
   	1 => array('pipe', 'w'), // stdout
   	2 => array('pipe', 'w'), // stderr
	);

	$process = proc_open($command, $descriptorspec, $pipes);

	if (!is_resource($process)) {
		return false;
	}

	if (is_resource($process)) {
		$txOff = 0; $txLen = strlen($stdin);
		$stdout = ''; $stdoutDone = false;
		$stderr = ''; $stderrDone = false;
		stream_set_blocking($pipes[0], false); // Make stdin/stdout/stderr non-blocking
		stream_set_blocking($pipes[1], false);
		stream_set_blocking($pipes[2], false);
		if ($txLen == 0) { fclose($pipes[0]); }
		while(true) {
			$rx = array(); // The program's stdout/stderr
			if (!$stdoutDone) { $rx[] = $pipes[1]; }
			if (!$stderrDone) { $rx[] = $pipes[2]; }
			$tx = array(); // The program's stdin
			if ($txOff < $txLen) { $tx[] = $pipes[0]; }
			stream_select($rx, $tx, $ex = null, null, null); // Block till r/w possible
			if (!empty($tx)) {
				$txRet = fwrite($pipes[0], substr($stdin, $txOff, 8192));
				if ($txRet !== false) { $txOff += $txRet; }
				if ($txOff >= $txLen) { fclose($pipes[0]); }
			}
			foreach ($rx as $r) {
				if ($r == $pipes[1]) {
					$stdout .= fread($pipes[1], 8192);
					if (feof($pipes[1])) { fclose($pipes[1]); $stdoutDone = true; }
				} else if ($r == $pipes[2]) {
					$stderr .= fread($pipes[2], 8192);
					if (feof($pipes[2])) { fclose($pipes[2]); $stderrDone = true; }
				}
			}
			if ($txOff >= $txLen && $stdoutDone && $stderrDone) break;
		}
		$exitCode = proc_close($process);
	}//end

	return $exitCode;
}


$charset = strtolower($_POST['charset']) == 'utf-8' ? 'utf8' : 'latin1';

$html = $orgHtml = $_POST['html'];
$html = preg_replace('/<\/?o:p>/i', '', $html);

$command  = "./tidy -{$charset} --add-xml-decl false --quiet true --tidy-mark false --wrap-php false --input-encoding utf8 --preserve-entities true";
$command .= (isset($_POST['indent']) && $_POST['indent'] == 'true') ? ' --indent auto --indent-spaces 1' : '';
if ($_POST['page'] == 0) {
	$command .= ' --show-body-only true --bare true --wrap 120';
}
else {
	$command .= ' --wrap 400';
}

$exitCode = execProgram($command, $html, $tidied, $stderr);
if ($exitCode === false) {
	// Cannot open process, revert to original HTML:
	$stderr = "Cannot proc_open $command";
	$exitCode = 2;
}

if ($exitCode > 1) {
	$tidied = $orgHtml;
	if ($charset == 'latin1') {
		$tidied = utf8_decode($tidied);
	}
}
else {
	$tidied = preg_replace("'<\?php(.*?)\?>'si", '<script language="php">$1</script>', $tidied);
}
$tidied = trim($tidied);

if ($_POST['page'] == 0) {
	// Remove some tidy messages
	$errs_old = explode("\n", $stderr);
	$errs_new = array();
	foreach($errs_old as $err) {
		if (preg_match('/(missing <!DOCTYPE> declaration|inserting implicit <body>|inserting missing \'title\' element)/', $err)) continue;
		$errs_new[] = trim($err);
	}
	$stderr = implode("\n", $errs_new);
}

header('Content-type: text/plain; charset=' . $_POST['charset']);
print $tidied;
print '+-_*^-TiDY-^*_-+';
print PH::htmlspecialchars(trim($stderr));

exit(0);

?>
