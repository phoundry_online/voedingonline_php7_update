<?php
	require './common.php';

	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME']))))) . '/PREFS.php');
	}

	$request_uri = $_SERVER['PHP_SELF'];
	if (!empty($_SERVER['QUERY_STRING'])) {
		$request_uri .= '?' . $_SERVER['QUERY_STRING'];
	}

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$lang = isset($_POST['lang']) ? $_POST['lang'] : 'en';
	}
	else {
		$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	}
	require "lang/$lang.php";

	$title = PEword(228);

	if (isset($_GET['step']) && $_GET['step'] == 1) {

		$url = str_replace('step=1','step=2',$request_uri);
		print <<<EOM
<html>
<head>
<title>$title</title>
</head>
<frameset rows="100%,*" frameborder="0" border="0" framespacing="0">
<frame name="mcontent" src="$url" noresize="noresize" scrolling="no" />
<frame name="hidden" src="blank.html" />
</frameset>
</html>
EOM;
	exit();
	}
	elseif (isset($_GET['step']) && $_GET['step'] == 2) {
		print <<<EOM
<html>
<head>
<title>$title</title>
<script type="text/javascript">
//<![CDATA[

function init() {
	document.forms[0].input.value = parent.window.dialogArguments['editor'].doc.body.innerHTML;
	setTimeout('document.forms[0].submit()', 100);
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg={$_GET['bg']}" type="text/css" />
</head>
<body onload="init()" oncontextmenu="return false">
<fieldset>
<legend><b>{$title}</b></legend>
<table width="100%"><tr>
<td width="100%" height="130" align="center" valign="middle"><img src="pics/busy.gif" alt="Busy..." width="16" height="16" /></td>
</tr></table>
<form action="spell.php" method="post" style="visibility:hidden">
<input type="hidden" name="input" />
<input type="hidden" name="lang" value="{$lang}" />
<input type="hidden" name="bg" value="{$_GET['bg']}" />
</form>
</fieldset>
</body>
</html>
EOM;
		exit;
	}

	$pspell_cfg = pspell_config_create($PHprefs['languages'][0]); 
	pspell_config_ignore($pspell_cfg, 3); 
	pspell_config_mode($pspell_cfg, PSPELL_NORMAL);
	pspell_config_personal($pspell_cfg, "{$PHprefs['uploadDir']}/{$PHprefs['DBname']}.pws"); 
	$pspell_handle = pspell_new_config($pspell_cfg); 

	if (isset($_GET['learn'])) {
		$word = $_GET['learn'];
		if (get_magic_quotes_gpc()) {
			$word = stripslashes($word);
		}

		pspell_add_to_personal($pspell_handle, $word);
		pspell_save_wordlist($pspell_handle);

		header('Content-type: image/gif');
		echo('GIF89a  �  ������!�    ,       D ;');
		exit;
	}

	$html = $_POST['input'];
	if (get_magic_quotes_gpc()) {
		$html = stripslashes($html);
	}

	// Remove SCRIPT and SELECT's altogether:
	$html = preg_replace("'<script[^>]*?>.*?</script>'si", '', $html);
	$html = preg_replace("'<select[^>]*?>.*?</select>'si", '', $html);

	// Remove inline tags:
	$inline = array('acronym', 'strong', 'strike', 'small', 'abbr', 'cite', 'code', 'font', 'samp', 'span', 'big', 'dfn', 'kbd', 'sub', 'sup', 'var', 'del', 'ins', 'em', 'tt', 'a', 'b', 'i', 'q', 's', 'u');
	foreach ($inline as $tag) {
		$regexp = "'<{$tag}(?:(?:\s*\/?\s*>)|(?:\s+[^>]+>))(.*?)</{$tag}>'si";
		$html = preg_replace($regexp, '$1', $html);
	}

	// Remove HTML tags & entities:
	$search = array("'<[\/\!]*?[^<>]*?>'si",	// HTML tags
						 "'&(quot|#34);'i",
						 "'&(amp|#38);'i",
						 "'&(lt|#60);'i",
						 "'&(gt|#62);'i",
						 "'&(nbsp|#160);'i",
						 "'&(iexcl|#161);'i",
						 "'&(cent|#162);'i",
						 "'&(pound|#163);'i",
						 "'&(copy|#169);'i",
						 "'&(middot|copy);'i",
						 "'&#(\d+);'e"); 
	$replace = array(' ',
						  '"',
						  '&',
						  '<',
						  '>',
						  ' ',
						  chr(161),
						  chr(162),
						  chr(163),
						  chr(169),
						  '',
						  "chr(\\1)");

	$html = preg_replace($search, $replace, $html);

	// Remove spaces.
	$html = preg_replace('/\s+/', ' ', $html);

	// Replace single quotes with their octal counterpart.
	$html = str_replace("'", "\\047", $html);

	// Do not use escapeshellcommand!!!
?>
<!--
   Copyright (c) <?= strftime('%Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
   Programming by Arjan Haverkamp (arjan-at-webpower.nl)
-->
<html>
<head>
<title><?= PEword(228) ?></title>
<script type="text/javascript">
//<![CDATA[

function Spell(word, suggestions) {
	this.word = word;
	this.suggestions = suggestions;
}

var spell = [], curWordIdx = 0, curWord = '';

<?php
	$count = 0;

	$words = preg_split('/\W+/', $html);
	foreach($words as $word) {
		if (!pspell_check($pspell_handle, $word)) {
			// Misspelled word:
			print 'spell[' . $count++ . "] = new Spell('" . str_replace("'", "\\'", $word) . "', [";

			$suggestions = pspell_suggest($pspell_handle, $word);

			$x = 0;
			foreach($suggestions as $suggestion) {
				if ($x++ > 0) print ',';
				print "'" . str_replace("'", "\\'", $suggestion) . "'";
			}
			print "])\n";
		}
	}
?>

function checkIt(moveNext) {
	var entry, lwr, html, len, x, sugg = document.getElementById('suggestionEl');
	if(moveNext){
		range.moveStart('character',1);
		range.moveEnd('textedit');
	}
	while(curWordIdx < spellLen) {
		entry = spell[curWordIdx];
		if (entry == null)
			curWordIdx++;
		else break;
	}

	if (curWordIdx == spellLen) {
		// Checking done.
		parent.close();
		return;
	}

	curWord = entry.word;
	lwr = curWord;

	f.word.value = curWord;

	if (range.findText(curWord)==true)
		range.select();

	html = '<select name="suggestion" size="5" style="width:200px" onchange="f.changeto.value=this.options[this.selectedIndex].value">';
	len  = entry.suggestions.length;
	for (x = 0; x < len; x++)
		html += '<option value="' + entry.suggestions[x] + '">' + entry.suggestions[x] + '</option>';
	html += '</select>';
	
	if (len > 0)
		f.changeto.value = entry.suggestions[0];
	else
		f.changeto.value = '';

	sugg.innerHTML = html;
}

function removeWord(idx) {
	var entry = spell[idx];
	if (entry == null) return;
	var word = entry.word;
	for (var x = idx; x < spellLen; x++) {
		entry = spell[x];
		if (entry == null) continue;
		if (entry.word == word)
			spell[x] = null;
	}
}

function change(all) {
	if (f.changeto.value == '') {
		alert('<?= PEescSquote(PEword(237)) ?>');
		f.changeto.focus();
		return;
	}
	var lwr = curWord;
	if (all) {
		var re = new RegExp('\\b'+curWord+'\\b','g');
		editor.DOM.body.innerHTML = editor.DOM.body.innerHTML.replace(re,f.changeto.value);
		removeWord(curWordIdx);
	}
	else {
		range.text = f.changeto.value;
	}
	curWordIdx++;
	checkIt(1);
}

function ignore(all) {
	if (all)
		removeWord(curWordIdx);
	curWordIdx++;
	checkIt(1);
}

function learn() {
	if (confirm('<?= PEescSquote(PEword(238)) ?>'.replace(/#1/,curWord))) {
		var im = document.images['learnImg'];
		im.onload = learn2;
		im.src = 'spell.php?learn='+escape(curWord);
	}
}

function learn2() {
	removeWord(curWordIdx);
	checkIt(1);
}

function keydown() {
	if (event.keyCode == 67)
		alert(speller);
}

var editor, range, f, spellLen;
function init() {
	f  = document.forms['spellForm'];
	spellLen = spell.length;
	editor = parent.window.dialogArguments['editor'];
	range  = parent.window.dialogArguments['range'];
	document.onkeydown = keydown;
	checkIt(0);
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_POST['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.txt { width:200px }
.but { width:60px }

-->
</style>
</head>

<body onload="init()">
<fieldset>
<legend><b><?= PEword(228) ?></b></legend>

<form name="spellForm">
<table>
<tr>
	<td><?= PEword(229) ?>:</td>
	<td><input class="txt" type="text" name="word" style="background:menu;font-weight:bold" readonly="readonly" /></td>
</tr>
<tr>
	<td><?= PEword(230) ?>:</td>
	<td><input class="txt" type="text" name="changeto"></td>
</tr>
<tr valign="top">
	<td><?= PEword(231) ?>:</td>
	<td id="suggestionEl">&nbsp;</td>
	<td>
	<table>
	<tr>
		<td><input type="button" value="<?= PEword(232) ?>" onclick="ignore(0)" /></td>
		<td><input type="button" value="<?= PEword(233) ?>" onclick="ignore(1)" /></td>
	</tr>
	<tr>
		<td><input type="button" value="<?= PEword(234) ?>" onclick="change(0)" /></td>
		<td><input type="button" value="<?= PEword(235) ?>" onclick="change(1)" /></td>
	</tr>
	<tr>
		<td><input type="button" value="<?= PEword(236) ?>" onclick="learn()" /></td>
		<td><input type="button" value="<?= PEword(138) ?>" onclick="parent.close()" /></td>
	</tr>
	</table>
	</td>
</tr>
</table>
</form>
</fieldset>

<img name="learnImg" src="pics/pixel.gif" width="1" height="1" />

</body>
</html>
