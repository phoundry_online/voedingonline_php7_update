<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title>Phoundry Editor: <?= PEword(45) ?></title>
<style type="text/css">
<!--

body, table, td {
	font-family: Tahoma,Futura,Verdana,Sans-serif;
	font-size: 11px;
}

body {
	background: #fff;
	margin: 6px;
}

img {
	background-color: menu; 
	border-bottom: buttonShadow solid 1px; 
	border-left: buttonHighlight solid 1px; 
	border-right: buttonShadow solid 1px; 
	border-top:  buttonHighlight solid 1px;
}

.defimg {
	background: none;
	border: 0;
}

fieldset {
	-moz-border-radius: 6px;
}

button {font-family:Arial, Helvetica; font-size:8pt; width:100px}

td div {
	background:url(pics/wysiwyg2.png) no-repeat;
	width:20px;
	height:20px;
	/*
	background-color: menu; 
	border-bottom: buttonShadow solid 1px; 
	border-left: buttonHighlight solid 1px; 
	border-right: buttonShadow solid 1px; 
	border-top:  buttonHighlight solid 1px;
	*/
}

-->
</style>
</head>
<body>

<fieldset>
<legend><b>
<?php
	print 'Phoundry Editor ' . $PE_VERSION . ' ' . PEword(45);
?>
</b></legend>

<table border="0" cellspacing="2" cellpadding="0" width="100%">
<tr><td colspan="4"><b><?= PEword(56) ?></b></td></tr>
<tr>

<?php
	if ($_GET['p'] != 0) {
		print '<td><div style="background-position:0 0 0 0 0"></div></td><td width="50%">' . PEword(252) . '</td>';
	}
	if ($_GET['u'] != 0) {
		print '<td><div style="background-position:-20px 0"></div></td><td width="50%">' . PEword(58) . '</td>';
	}
?>
</tr>
<tr>
	<td><div style="background-position:-40px 0"></div></td><td><?= PEword(99) ?></td>
	<td><div style="background-position:-60px 0"></div></div></td><td width="50%"><?= PEword(59) ?></td>
</tr>
<tr>
	<td><div style="background-position:-80px 0"></div></td><td><?= PEword(61) ?></td>
	<td><div style="background-position:-100px 0"></div></td><td><?= PEword(62) ?></td>
</tr>
<tr>
	<td><div style="background-position:-120px 0"></div></td><td><?= PEword(63) ?></td>
	<td><div style="background-position:-160px 0"></div></td><td><?= PEword(64) ?></td>
</tr>
<tr>
	<td><div style="background-position:-180px 0"></div></td><td><?= PEword(65) ?></td>
	<td><div style="background-position:-200px 0"></div></td><td><?= PEword(66) ?></td>
</tr>
<tr>
	<td><div style="background-position:-220px 0"></div></td><td><?= PEword(67) ?></td>
	<td><div style="background-position:-240px 0"></div></td><td><?= PEword(68) ?></td>
</tr>
<tr>
	<td><div style="background-position:-260px 0"></div></td><td><?= PEword(69) ?></td>
	<td><div style="background-position:-280px 0"></div></td><td><?= PEword(70) ?></td>
</tr>
<tr>
	<td><div style="background-position:-320px 0"></div></td><td><?= PEword(71) ?></td>
	<td><div style="background-position:-340px 0"></div></td><td><?= PEword(72) ?></td>
</tr>
<tr>
	<td><div style="background-position:-360px 0"></div></td><td><?= PEword(73) ?></td>
	<td><div style="background-position:-380px 0"></div></td><td><?= PEword(74) ?></td>
</tr>
<tr>
	<td><div style="background-position:-400px 0"></div></td><td><?= PEword(75) ?></td>
	<td><div style="background-position:-420px 0"></div></td><td><?= PEword(76) ?></td>
</tr>
<tr>
	<td><div style="background-position:-440px 0"></div></td><td><?= PEword(77) ?></td>
	<td><div style="background-position:-460px 0"></div></td><td><?= PEword(78) ?></td>
</tr>
<tr>
	<td><div style="background-position:-480px 0"></div></td><td><?= PEword(79) ?></td>
	<td><div style="background-position:-280px -40px"></div></td><td><?= PEword(263) ?></td>
</tr>

<tr>
	<td><div style="background-position:0 -20px"></div></td><td><?= PEword(94) ?></td>
	<td><div style="background-position:-220px -40px"></div></td><td><?= PEword(245) ?></td>
</tr>
<tr>
	<td><div style="background-position:-200px -40px"></div></td><td><?= PEword(100) ?></td>
	<td><div style="background-position:-260px -40px"></div></td><td><?= PEword(264) ?></td>
</tr>


<?php if ($_GET['t'] != 0) { ?>
<tr>
	<td><div style="background-position:-20px -20px"></div></td><td><?= PEword(80) ?></td>
	<td><div style="background-position:-40px -20px"></div></td><td><?= PEword(81) ?></td>
</tr>
<tr>
	<td><div style="background-position:-80px -20px"></div></td><td><?= PEword(82) ?></td>
	<td><div style="background-position:-100px -20px"></div></td><td><?= PEword(83) ?></td>
</tr>
<tr>
	<td><div style="background-position:-140px -20px"></div></td><td><?= PEword(84) ?></td>
	<td><div style="background-position:-160px -20px"></div></td><td><?= PEword(85) ?></td>
</tr>
<tr>
	<td><div style="background-position:-180px -20px"></div></td><td><?= PEword(86) ?></td>
	<!--<td><img height="16" src="pics/table_cell_split.png" width="16" /></td><td><?= PEword(87) ?></td>-->
</tr>
<tr>
	<td><div style="background-position:-200px -20px"></div></td><td><?= PEword(88) ?></td>
</tr>
<?php } ?>

<?php if ($_isMidas) { ?>
<tr>
	<td><div style="background-position:-360px -40px"></div></td><td><?= PEword(89) ?></td>
	<td><div style="background-position:-380px -40px"></div></td><td><?= PEword(90) ?></td>
</tr>
<?php } else { ?>
<tr>
	<td><div style="background-position:-220px -20px"></div></td><td><?= PEword(91) ?></td>
	<td><div style="background-position:-240px -20px"></div></td><td><?= PEword(92) ?></td>
</tr>
<tr>
	<td><div style="background-position:-260px -20px"></div></td><td><?= PEword(93) ?></td>
</tr>
<?php } ?>
<tr>
	<td><div style="background-position:-320px -40px"></div></td><td><?= PEword(97) ?></td>
	<?php if (!$_isMidas) { ?>
	<td><div style="background-position:-280px -20px"></div></td><td><?= PEword(239) ?></td>
	<? } ?>
</tr>
<tr>
	<?php if (!$_isMidas) { ?>
	<td><div style="background-position:-300px -20px"></div></td><td><?= PEword(43) ?></td>
	<? } ?>
	<td><div style="background-position:-300px -40px"></div></td><td><?= PEword(42) ?></td>
</tr>
<tr>
	<td><div style="background-position:-320px -20px"></div></td><td><?= PEword(98) ?></td>
</tr>
<?php if ($_GET['f'] != 0) { ?>
<tr>
	<td><div style="background-position:0 -40px"></div></td><td><?= PEword(101) ?></td>
	<td><div style="background-position:-20px -40px"></div></td><td><?= PEword(102) ?></td>
</tr>
<tr>
	<td><div style="background-position:-60px -40px"></div></td><td><?= PEword(243) ?></td>
	<td><div style="background-position:-40px -40px"></div></td><td><?= PEword(103) ?></td>
</tr>
<tr>
	<td><div style="background-position:-80px -40px"></div></td><td><?= PEword(104) ?></td>
	<td><div style="background-position:-100px -40px"></div></td><td><?= PEword(105) ?></td>
</tr>
<tr>
	<td><div style="background-position:-120px -40px"></div></td><td><?= PEword(106) ?></td>
	<td><div style="background-position:-140px -40px"></div></td><td><?= PEword(107) ?></td>
</tr>
<tr>
	<td><div style="background-position:-160px -40px"></div></td><td><?= PEword(108) ?></td>
	<td><div style="background-position:-180px -40px"></div></td><td><?= PEword(109) ?></td>
</tr>
<?php } ?>
</table>
</fieldset>

<table width="100%"><tr>
<td><img src="pics/webpower.gif" width="125" height="18" border="0" alt="www.webpower.nl" class="defimg" /></td>
<td align="right">&copy; copyright 2000-<?= date('Y') ?> by Web Power</td>
</tr></table>

</body>
</html>
