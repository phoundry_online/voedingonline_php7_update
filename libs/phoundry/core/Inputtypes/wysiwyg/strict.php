<?php

require 'common.php';

$html = trim($_POST['html']);

if (get_magic_quotes_gpc()) {
	$html = stripslashes($html);
}
$html = preg_replace('/<\/?o:p>/i', '', $html);

$doctype = $_POST['doctype'];
$charset = strtolower($_POST['charset']);
$page = $_POST['page'] == 1;
if (!$page) {
	$html = '<html><head><title>23c</title></head><body>' . $html . '</body></html>';
}

$urlstring = 'charset=' . $charset . '&doctype=' . urlencode($doctype) . '&fragment=' . rawurlencode($html) . '&output=soap12';

$ch = curl_init('http://validator.w3.org/check');
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $urlstring);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$xml = curl_exec($ch);

curl_close($ch);

$parser = xml_parser_create();
xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
xml_parse_into_struct($parser, $xml, $values, $tags);
xml_parser_free($parser);

print "{$doctype} validation:<br />\n";
print '<ul style="margin-left:16px;padding-left:0">';

$inErrors = false;
$inError = false;
$curError = '';
foreach($values as $idx=>$data) {
	if ($data['tag'] == 'm:errors' && $data['type'] == 'open') {
		$inErrors = true;
	}
	elseif ($data['tag'] == 'm:errors' && $data['type'] == 'close') {
		$inErrors = false;
		$inError = false;
	}
	elseif ($data['tag'] == 'm:errorcount' && $data['type'] == 'complete') {
		$nrErrors = $data['value'];
		print "$nrErrors errors\n";
	}
	elseif ($inErrors && $data['tag'] == 'm:error' && $data['type'] == 'open') {
		$inError = true;
		$curError = '';
	}
	elseif ($inErrors && $data['tag'] == 'm:error' && $data['type'] == 'complete') {
		$inError = false;
	}
	elseif ($inError && $data['tag'] == 'm:line' && $data['type'] == 'complete') {
		$lineno = (int)$data['value'] - 1;
		if ($_POST['mode'] == 1) {
			$curError .= '<a href="javascript:G(' . $lineno . ')">Line: ' . $lineno . '</a>, ';
		}
		else
			$curError .= 'Line: ' . $lineno . ', ';
	}
	elseif ($inError && $data['tag'] == 'm:col' && $data['type'] == 'complete') {
		$curError .= 'Col: ' . $data['value'] . ' ';
	}
	elseif ($inError && $data['tag'] == 'm:message' && $data['type'] == 'complete') {
		$curError .= PH::htmlspecialchars($data['value']);
		if (strpos($curError, 'no document element') === false) {
			print '<li>' . $curError . "</li>\n";
		}
	}

}
print "</ul>\n";

exit(0);

?>
