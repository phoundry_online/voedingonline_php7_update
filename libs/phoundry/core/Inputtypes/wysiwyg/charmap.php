<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(15) . str_repeat('&nbsp; ', 50) ?></title>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.s
{
	font-family: Symbol;
	font-size: 12px;
	background-color: menu; 
	border-bottom: buttonshadow solid 1px; 
	border-left: buttonhighlight solid 1px; 
	border-right: buttonshadow solid 1px; 
	border-top:  buttonhighlight solid 1px;
	width:16px; height:16px; cursor:pointer; cursor:hand;
}

.a
{
	font-family: Arial;
	font-size: 12pt;
	background-color: menu; 
	border-bottom: buttonshadow solid 1px; 
	border-left: buttonhighlight solid 1px; 
	border-right: buttonshadow solid 1px; 
	border-top:  buttonhighlight solid 1px;
	width:16px; height:16px; cursor:pointer; cursor:hand;
}

-->
</style>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function insChar(s, c) {
	var symbol;
	if (s)
		symbol = '<font face="Symbol">&#' + c + ';</font>';
	else
		symbol = '&#' + c + ';';
	winArgs['editor'].insertHTML(symbol);
	_W.close();
}

function insNBSP() {
	winArgs['editor'].insertHTML('&nbsp;');
	_W.close();
}

//]]>
</script>
</head>
<body oncontextmenu="return false">
<fieldset>
<legend><b><?= PEword(15) ?></b></legend>
<center>
<br />
<table cellspacing="0" cellpadding="1" border="0">
<tr>
<td class="a" align="center" onclick="insNBSP()" title="&amp;nbsp;">&nbsp;</td>
<td class="a" align="center" onclick="insChar(0,128)">&#128;</td><td class="a" align="center" onclick="insChar(0,131)">&#131;</td>
<td class="a" align="center" onclick="insChar(0,137)">&#137;</td><td class="a" align="center" onclick="insChar(0,138)">&#138;</td>
<td class="a" align="center" onclick="insChar(0,140)">&#140;</td><td class="a" align="center" onclick="insChar(0,142)">&#142;</td>
<td class="a" align="center" onclick="insChar(0,150)">&#150;</td>
<td class="a" align="center" onclick="insChar(0,153)">&#153;</td><td class="a" align="center" onclick="insChar(0,154)">&#154;</td>
<td class="a" align="center" onclick="insChar(0,156)">&#156;</td><td class="a" align="center" onclick="insChar(0,158)">&#158;</td>
<td class="a" align="center" onclick="insChar(0,159)">&#159;</td><td class="a" align="center" onclick="insChar(0,162)">&#162;</td>
<td class="a" align="center" onclick="insChar(0,163)">&#163;</td><td class="a" align="center" onclick="insChar(0,164)">&#164;</td>
<td class="a" align="center" onclick="insChar(0,165)">&#165;</td><td class="a" align="center" onclick="insChar(0,166)">&#166;</td>
<td class="a" align="center" onclick="insChar(0,167)">&#167;</td><td class="a" align="center" onclick="insChar(0,168)">&#168;</td>
</tr>
<tr>
<td class="a" align="center" onclick="insChar(0,169)">&#169;</td>
<td class="a" align="center" onclick="insChar(0,170)">&#170;</td>
<td class="a" align="center" onclick="insChar(0,171)">&#171;</td><td class="a" align="center" onclick="insChar(0,172)">&#172;</td>
<td class="a" align="center" onclick="insChar(0,173)">&#173;</td><td class="a" align="center" onclick="insChar(0,174)">&#174;</td>
<td class="a" align="center" onclick="insChar(0,175)">&#175;</td><td class="a" align="center" onclick="insChar(0,176)">&#176;</td>
<td class="a" align="center" onclick="insChar(0,177)">&#177;</td><td class="a" align="center" onclick="insChar(0,178)">&#178;</td>
<td class="a" align="center" onclick="insChar(0,179)">&#179;</td><td class="a" align="center" onclick="insChar(0,180)">&#180;</td>
<td class="a" align="center" onclick="insChar(0,181)">&#181;</td><td class="a" align="center" onclick="insChar(0,182)">&#182;</td>
<td class="a" align="center" onclick="insChar(0,183)">&#183;</td><td class="a" align="center" onclick="insChar(0,184)">&#184;</td>
<td class="a" align="center" onclick="insChar(0,185)">&#185;</td><td class="a" align="center" onclick="insChar(0,186)">&#186;</td>
<td class="a" align="center" onclick="insChar(0,187)">&#187;</td><td class="a" align="center" onclick="insChar(0,188)">&#188;</td>
</tr>
<tr>
<td class="a" align="center" onclick="insChar(0,189)">&#189;</td>
<td class="a" align="center" onclick="insChar(0,190)">&#190;</td>
<td class="a" align="center" onclick="insChar(0,191)">&#191;</td><td class="a" align="center" onclick="insChar(0,192)">&#192;</td>
<td class="a" align="center" onclick="insChar(0,193)">&#193;</td><td class="a" align="center" onclick="insChar(0,194)">&#194;</td>
<td class="a" align="center" onclick="insChar(0,195)">&#195;</td><td class="a" align="center" onclick="insChar(0,196)">&#196;</td>
<td class="a" align="center" onclick="insChar(0,197)">&#197;</td><td class="a" align="center" onclick="insChar(0,198)">&#198;</td>
<td class="a" align="center" onclick="insChar(0,199)">&#199;</td><td class="a" align="center" onclick="insChar(0,200)">&#200;</td>
<td class="a" align="center" onclick="insChar(0,201)">&#201;</td><td class="a" align="center" onclick="insChar(0,202)">&#202;</td>
<td class="a" align="center" onclick="insChar(0,203)">&#203;</td><td class="a" align="center" onclick="insChar(0,204)">&#204;</td>
<td class="a" align="center" onclick="insChar(0,205)">&#205;</td><td class="a" align="center" onclick="insChar(0,206)">&#206;</td>
<td class="a" align="center" onclick="insChar(0,207)">&#207;</td><td class="a" align="center" onclick="insChar(0,208)">&#208;</td>
</tr>
<tr>
<td class="a" align="center" onclick="insChar(0,209)">&#209;</td>
<td class="a" align="center" onclick="insChar(0,210)">&#210;</td>
<td class="a" align="center" onclick="insChar(0,211)">&#211;</td><td class="a" align="center" onclick="insChar(0,212)">&#212;</td>
<td class="a" align="center" onclick="insChar(0,213)">&#213;</td><td class="a" align="center" onclick="insChar(0,214)">&#214;</td>
<td class="a" align="center" onclick="insChar(0,215)">&#215;</td><td class="a" align="center" onclick="insChar(0,216)">&#216;</td>
<td class="a" align="center" onclick="insChar(0,217)">&#217;</td><td class="a" align="center" onclick="insChar(0,218)">&#218;</td>
<td class="a" align="center" onclick="insChar(0,219)">&#219;</td><td class="a" align="center" onclick="insChar(0,220)">&#220;</td>
<td class="a" align="center" onclick="insChar(0,221)">&#221;</td><td class="a" align="center" onclick="insChar(0,222)">&#222;</td>
<td class="a" align="center" onclick="insChar(0,223)">&#223;</td><td class="a" align="center" onclick="insChar(0,224)">&#224;</td>
<td class="a" align="center" onclick="insChar(0,225)">&#225;</td><td class="a" align="center" onclick="insChar(0,226)">&#226;</td>
<td class="a" align="center" onclick="insChar(0,227)">&#227;</td><td class="a" align="center" onclick="insChar(0,228)">&#228;</td>
</tr>
<tr>
<td class="a" align="center" onclick="insChar(0,229)">&#229;</td>
<td class="a" align="center" onclick="insChar(0,230)">&#230;</td>
<td class="a" align="center" onclick="insChar(0,231)">&#231;</td><td class="a" align="center" onclick="insChar(0,232)">&#232;</td>
<td class="a" align="center" onclick="insChar(0,233)">&#233;</td><td class="a" align="center" onclick="insChar(0,234)">&#234;</td>
<td class="a" align="center" onclick="insChar(0,235)">&#235;</td><td class="a" align="center" onclick="insChar(0,236)">&#236;</td>
<td class="a" align="center" onclick="insChar(0,237)">&#237;</td><td class="a" align="center" onclick="insChar(0,238)">&#238;</td>
<td class="a" align="center" onclick="insChar(0,239)">&#239;</td><td class="a" align="center" onclick="insChar(0,240)">&#240;</td>
<td class="a" align="center" onclick="insChar(0,241)">&#241;</td><td class="a" align="center" onclick="insChar(0,242)">&#242;</td>
<td class="a" align="center" onclick="insChar(0,243)">&#243;</td><td class="a" align="center" onclick="insChar(0,244)">&#244;</td>
<td class="a" align="center" onclick="insChar(0,245)">&#245;</td><td class="a" align="center" onclick="insChar(0,246)">&#246;</td>
<td class="a" align="center" onclick="insChar(0,247)">&#247;</td><td class="a" align="center" onclick="insChar(0,248)">&#248;</td>
</tr>
<tr>
<td class="a" align="center" onclick="insChar(0,249)">&#249;</td>
<td class="a" align="center" onclick="insChar(0,250)">&#250;</td>
<td class="a" align="center" onclick="insChar(0,251)">&#251;</td><td class="a" align="center" onclick="insChar(0,252)">&#252;</td>
<td class="a" align="center" onclick="insChar(0,253)">&#253;</td><td class="a" align="center" onclick="insChar(0,254)">&#254;</td>
<!-- NEW -->
<td class="a" align="center" onclick="insChar(0,913)">&#913;</td><td class="a" align="center" onclick="insChar(0,945)">&#945;</td>
<td class="a" align="center" onclick="insChar(0,914)">&#914;</td><td class="a" align="center" onclick="insChar(0,946)">&#946;</td>
<td class="a" align="center" onclick="insChar(0,915)">&#915;</td><td class="a" align="center" onclick="insChar(0,947)">&#947;</td>
<td class="a" align="center" onclick="insChar(0,916)">&#916;</td><td class="a" align="center" onclick="insChar(0,948)">&#948;</td>
<td class="a" align="center" onclick="insChar(0,917)">&#917;</td><td class="a" align="center" onclick="insChar(0,949)">&#949;</td>
<td class="a" align="center" onclick="insChar(0,918)">&#918;</td><td class="a" align="center" onclick="insChar(0,950)">&#950;</td>
<td class="a" align="center" onclick="insChar(0,919)">&#919;</td><td class="a" align="center" onclick="insChar(0,951)">&#951;</td>
</tr>
<tr>

<td class="a" align="center" onclick="insChar(0,8743)">&#8743;</td><td class="a" align="center" onclick="insChar(0,8736)">&#8736;</td>
<td class="a" align="center" onclick="insChar(0,9827)">&#9827;</td><td class="a" align="center" onclick="insChar(0,9828)">&#9828;</td>
<td class="a" align="center" onclick="insChar(0,9829)">&#9829;</td><td class="a" align="center" onclick="insChar(0,9830)">&#9830;</td>

<td class="a" align="center" onclick="insChar(0,8656)">&#8656;</td><td class="a" align="center" onclick="insChar(0,8592)">&#8592;</td>
<td class="a" align="center" onclick="insChar(0,8657)">&#8658;</td><td class="a" align="center" onclick="insChar(0,8593)">&#8593;</td>
<td class="a" align="center" onclick="insChar(0,8658)">&#8658;</td><td class="a" align="center" onclick="insChar(0,8594)">&#8594;</td>
<td class="a" align="center" onclick="insChar(0,8659)">&#8659;</td><td class="a" align="center" onclick="insChar(0,8595)">&#8595;</td>


</tr>
</table>

</center>
</fieldset>
<p align="right">
<button type="button" onclick="window.close()"><?= PEword(151) ?></button>
</p>

</body>
</html>
