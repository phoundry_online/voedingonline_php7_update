<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(41) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var D, W=window,range, ta;

function init() {
	D = document;
	ta = W.dialogArguments;
	range = ta.createTextRange();
	D.phoundry['word'].focus();
}

function findNext() {
	var word = D.phoundry.word.value;
	if (word == '') return;
	var whole = D.phoundry.whole.checked == true;
	var cases = D.phoundry.cases.checked == true;
	var flags = 0;
	if (whole) flags += 2;
	if (cases) flags += 4;
	if (range.findText(word,0,flags)==true){
		range.select();
		range.moveStart('character',1);
		range.moveEnd('textedit');
	}
	else {
		alert('Finished searching the document!');
		range = ta.createTextRange();
	}
	D.phoundry['word'].focus();
}

function checkInput(el,event) {
	if (event.keyCode==27)W.close();
	var but = D.getElementById('nextBut');
	but.disabled = (el.value == '') ? true : false;
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.txt { width:180px }
.but { width:70px; margin:2px}

-->
</style>
</head>
<body onload="init()">
<form name="phoundry" onsubmit="findNext();return false">
<fieldset>
<legend><b><?= PEword(41) ?></b></legend>

<table cellspacing="0" cellpadding="0" border="0"><tr>
<td valign="top">

<table>
<tr>
	<td nowrap="nowrap">Find what:</td>
	<td><input class="txt" type="text" name="word" size="20" onkeyup="checkInput(this,event)" /></td>
</tr>
</table>

<table cellspacing="0" cellpadding="0" border="0">
<tr><td valign="top">

<table>
<tr>
	<td><input id="whole" type="checkbox" /></td><td><label for="whole">Match whole word only</label></td>
</tr>
<tr>
	<td><input id="cases" type="checkbox" /></td><td><label for="cases">Match case</label></td>
</tr>
</table>

</td><td>&nbsp;</td>
</tr></table>

</td><td>&nbsp;</td>

</td><td valign="top">
	<input id="nextBut" type="button" disabled="disabled" value="Find next" onclick="findNext()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="W.close()" />
</td>
</tr></table>

</fieldset>

</form>
</body>
</html>
