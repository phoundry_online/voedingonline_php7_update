<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
	$nt = $_GET['nt'];
	$title = ($nt) ? PEword(159) : PEword(161);
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(158) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

<?php if ($_isMidas) { ?>

function PEset(el,arr){
	for(var x in arr){
		if(x=='class'){
			if(arr[x])el.className=arr[x];
			else el.removeAttribute(x+'Name',0)
		}
		else{
			if(arr[x])el.setAttribute(x,arr[x],0);
			else el.removeAttribute(x,0)
		}
	}
}

<?php } else { ?>

function PEset(el,arr){
	for(var x in arr){
		if(x=='class'){
			if(arr[x])el.className=arr[x];
			else el.removeAttribute(x+'Name',0)
		}
		else{
			if(arr[x]){try{el.attributes[x].nodeValue=arr[x]}catch(e){el.setAttribute(x,arr[x],0)}}
			else el.removeAttribute(x,0)
		}
	}
}

<?php } ?>

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function setColor(what) {
	var color, url;
	color = _D.forms[what+'Form']['bgcolor'].value;
	url = 'colorpick.php?l=<?= $lang ?>&bg=<?= urlencode($_GET['bg']) ?>&color='+escape(color);
	winArgs['what'] = what;
	winArgs['fn'] = function(wh,clr){setColor_(wh,clr)};
	if (isMidas) {
		_W.open(url,'PEW2','width=390,height=210,left='+((_W.outerWidth-390)/2+_W.screenX)+',top='+((_W.outerHeight-180)/2+_W.screenY));
	}
	else {
		showModalDialog(url, winArgs, 'dialogWidth:390px;dialogHeight:210px;scroll:0;status:no;help:0;');
	}
}
function setColor_(what,color) {
	_D.forms[what+'Form']['bgcolor'].value = color;
}

function setBackground(what) {
	var url = 'file.php?mode=explorer&lang=<?= $lang ?>&dir='+escape(winArgs['imgDir']);
	if (!winArgs['upload']) {
		url += '&allowFolders=false&allowFiles=false';
	}
	winArgs['callback'] = function(attrs){setBackground_(attrs['url'],what)};
	if (isMidas) {
		_W.open(url,'PEW2','width=600,height=460,left='+((_W.outerWidth-525)/2+_W.screenX)+',top='+((_W.outerHeight-415)/2+_W.screenY));
	}
	else {
		A = showModalDialog(url,winArgs,'dialogWidth:600px;dialogHeight:460px;help:0;status:0;scroll:0');
	}
}

function setBackground_(src,what){
	_D.forms[what+'Form']['bgimage'].value = src;
}

function init() {
	var CF = _D.forms['cellForm'], RF = _D.forms['rowForm'], TF = _D.forms['tblForm'];
	var Cargs = winArgs['CA'], Rargs = winArgs['RA'], Targs = winArgs['TA'];
	var elem;
	for (elem in Cargs) {
		switch(elem) {
			case 'width':
				CF.width.value = Cargs['width'];
				break;
			case 'height':
				CF.height.value = Cargs['height'];
				break;
			case 'align':
				if (Cargs['align'] == '')
					CF.align.selectedIndex = 0;
				else
					CF.align.value = Cargs['align'];
				break;
			case 'vAlign':
				if (Cargs['vAlign'] == '')
					CF.valign.selectedIndex = 0;
				else
					CF.valign.value = Cargs['vAlign'];
				break;
			case 'bgColor':
				CF.bgcolor.value = Cargs['bgColor'];
				break;
			case 'background':
				CF.bgimage.value = Cargs['background'];
				break;
			case 'noWrap':
				CF.nowrap.checked = Cargs['noWrap'];
				break;
		}
	}
	
	for (elem in Rargs) {
		switch(elem) {
			case 'align':
				if (Rargs['align'] == '')
					RF.align.selectedIndex = 0;
				else
					RF.align.value = Rargs['align'];
				break;
			case 'vAlign':
				if (Rargs['vAlign'] == '')
					RF.valign.selectedIndex = 0;
				else
					RF.valign.value = Rargs['vAlign'];
				break;
			case 'bgColor':
				RF.bgcolor.value = Rargs['bgColor'];
				break;
		}
	}
	
	if (winArgs['editor'].feat&4) {
		if (winArgs['editor'].classes.length) {
			var classes=winArgs['editor'].classes;
			var html = '<select name="Fclass"><option value=""><?= PEword(219) ?></option>';
			for(var x = 0; x < classes.length; x++)
				html += '<option value="' + classes[x] + '">' + classes[x] + '</option>';
			html += '</select>';
			_D.getElementById('TcssTD').innerHTML = html;
			_D.getElementById('TcssTR').style.visibility = 'inherit';
			<?php if(!$nt) { ?>
			_D.getElementById('RcssTD').innerHTML = html;
			_D.getElementById('RcssTR').style.visibility = 'inherit';
			_D.getElementById('CcssTD').innerHTML = html;
			_D.getElementById('CcssTR').style.visibility = 'inherit';
			<?php } ?>
		}
		if (Targs['class']) {
			if (classes && Targs['class'] == '')
				TF.Fclass.selectedIndex = 0;
			else
				TF.Fclass.value = Targs['class'];
		}
		if (Rargs['class']) {
			if (classes && Rargs['class'] == '')
				RF.Fclass.selectedIndex = 0;
			else
				RF.Fclass.value = Rargs['class'];
		}
		if (Cargs['class']) {
			if (classes && Cargs['class'] == '')
				CF.Fclass.selectedIndex = 0;
			else
				CF.Fclass.value = Cargs['class'];
		}
	}

	for (elem in Targs) {
		switch(elem) {
			case 'cellPadding':
				TF.padding.value = Targs['cellPadding'];
				break;
			case 'cellSpacing':
				TF.spacing.value = Targs['cellSpacing'];
				break;
			case 'border':
				TF.border.value = Targs['border'];
				break;
			case 'bgColor':
				TF.bgcolor.value = Targs['bgColor'];
				break;
			case 'background':
				TF.bgimage.value = Targs['background'];
				break;
			case 'width':
				TF.width.value = Targs['width'];
				break;
			case 'height':
				TF.height.value = Targs['height'];
				break;
			case 'rows':
				TF.rows.value = Targs['rows'];
				break;
			case 'columns':
				TF.columns.value = Targs['columns'];
				break;
			case 'align':
				if (Targs['align'])
					TF.align.value = Targs['align'];
				break;
			case 'caption':
				TF.caption.value = Targs['caption'];
				break;
			case 'summary':
				TF.summary.value = Targs['summary'];
				break;
			case 'cap_pos':
				if (Targs['cap_pos'] == 'bottom')
					TF.cap_pos[1].checked = true;
				else
					TF.cap_pos[0].checked = true;
			case 'nt':
				if(Targs['nt']==1)
					TF.rows.disabled = TF.columns.disabled = false;
		
		}
	}
	var start = winArgs['start'];
	if (start == 'TD')
		editIt('cell');
	else if (start == 'TR')
		editIt('row');
	else if (<?= $nt ? '0' : '1' ?>)
		editIt('table');
}

function editIt(what) {
	var cb = _D.forms['whatForm'].what;
	if (what == 'cell') {
		_D.getElementById('rowDiv').style.visibility = 'hidden';
		_D.getElementById('tblDiv').style.visibility = 'hidden';
		_D.getElementById('cellDiv').style.visibility = 'visible';
		_D.getElementById('legendTitle').innerHTML = '<b><?= PEescSquote(PEword(161)) ?></b>';
		cb[0].checked = true;
	}
	else if (what == 'row') {
		_D.getElementById('cellDiv').style.visibility = 'hidden';
		_D.getElementById('tblDiv').style.visibility = 'hidden';
		_D.getElementById('rowDiv').style.visibility = 'visible';
		_D.getElementById('legendTitle').innerHTML = '<b><?= PEescSquote(PEword(160)) ?></b>';
		cb[1].checked = true;
	}
	else if (what == 'table') {
		_D.getElementById('cellDiv').style.visibility = 'hidden';
		_D.getElementById('rowDiv').style.visibility = 'hidden';
		_D.getElementById('tblDiv').style.visibility = 'visible';
		_D.getElementById('legendTitle').innerHTML = '<b><?= PEescSquote(PEword(159)) ?></b>';
		cb[2].checked = true;
	}
}

function submitMe() {
	var Targs = [],TF = _D.forms['tblForm'], elTBL = winArgs['elTBL'], c ,r;
	var rows = TF.rows.value, columns = TF.columns.value, caption = TF.caption.value, cap_pos = (TF.cap_pos[0].checked) ? 'top' : 'bottom';

	if (<?= $nt ?> && (TF.columns.value == '' || TF.rows.value ==  '')) {
		alert('<?= PEescSquote(PEword(214)) ?>');
		return;
	}
	Targs['class'] = TF.Fclass.value;
	Targs['cellPadding'] = TF.padding.value;
	Targs['cellSpacing'] = TF.spacing.value;
	Targs['border'] = TF.border.value;
	Targs['bgColor'] = TF.bgcolor.value;
	Targs['background'] = TF.bgimage.value;
	Targs['width'] = TF.width.value;
	Targs['height'] = TF.height.value;
	Targs['align'] = TF.align.value;
	Targs['summary'] = TF.summary.value;
	<?php if (!$nt) { ?>
	var Rargs = [],RF = _D.forms['rowForm'];
	Rargs['class'] = RF.Fclass.value;
	Rargs['align'] = RF.align.value;
	Rargs['vAlign'] = RF.valign.value;
	Rargs['bgColor'] = RF.bgcolor.value;
	var Cargs = [],CF = _D.forms['cellForm'];
	Cargs['class'] = CF.Fclass.value;
	Cargs['width'] = CF.width.value;
	Cargs['height'] = CF.height.value;
	Cargs['align'] = CF.align.value;
	Cargs['vAlign'] = CF.valign.value;
	Cargs['bgColor'] = CF.bgcolor.value;
	Cargs['background'] = CF.bgimage.value;
	Cargs['noWrap'] = CF.nowrap.checked;
	<?php } ?>

	if (elTBL) {
		PEset(elTBL,Targs);
		if (winArgs['elTR'])
			PEset(winArgs['elTR'],Rargs);
		if (winArgs['elTD'])
			PEset(winArgs['elTD'],Cargs);
	}
	else {
		tag = '<table';
		if (Targs['cellPadding'])	tag += ' cellpadding="' + Targs['cellPadding'] + '"';
		if (Targs['cellSpacing'])	tag += ' cellspacing="' + Targs['cellSpacing'] + '"';
		if (Targs['border'])			tag += ' border="' + Targs['border'] + '"';
		if (Targs['class'])			tag += ' class="' + Targs['class'] + '"';
		if (Targs['bgColor'])		tag += ' bgcolor="' + Targs['bgColor'] + '"';
		if (Targs['background'])	tag += ' background="' + Targs['background'] + '"';
		if (Targs['width'])			tag += ' width="' + Targs['width'] + '"';
		if (Targs['height'])			tag += ' height="' + Targs['height'] + '"';
		if (Targs['summary'])		tag += ' summary="' + Targs['summary'] + '"';
		if (Targs['align']) 			tag += ' align="' + Targs['align'] + '"';
		tag += '>';
		if (caption) {
			tag += '<caption align="'+cap_pos+'">'+caption+'</caption>';
		}
		for (r = 0; r < rows; r++) {
			tag += '<tr>';
			for (c = 0; c < columns; c++)
				tag += '<td valign="top">' + (isMidas?'<br />':'') + '</td>';
			tag += '</tr>'
		}
		tag += '</table>';
		winArgs['editor'].insertHTML(tag)
	}

	if (!isMidas && elTBL) {
		if(caption){
			P=elTBL.caption?elTBL.caption:elTBL.createCaption();
			P.innerHTML=caption;
			P.align=cap_pos
		}
		//else if(elTBL.caption)
		//	elTBL.caption.removeNode();
	}
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.txt,select { width:70px }
form {margin:0}

-->
</style>
</head>
<body onload="init()" oncontextmenu="return false">
<?php if (!$nt) { ?>

<div id="cellDiv" style="position:absolute;left:6px;top:18px;width:308px">
<form name="cellForm">
<table width="100%">
<tr>
	<td width="55"><?= PEword(135) ?>:</td>
	<td><select name="align">
	<option value=""><?= PEword(219) ?></option>
	<option value="left"><?= PEword(220) ?></option>
	<option value="right"><?= PEword(221) ?></option>
	<option value="center"><?= PEword(226) ?></option></select></td>
	<td width="55"><?= PEword(162) ?>:</td>
	<td><select name="valign">
	<option value=""><?= PEword(219) ?></option>
	<option value="top"><?= PEword(222) ?></option>
	<option value="middle"><?= PEword(223) ?></option>
	<option value="baseline"><?= PEword(227) ?></option>
	<option value="bottom"><?= PEword(224) ?></option>
	</select></td>  
</tr>
<tr>
	<td><?= PEword(130) ?>:</td>
	<td><input class="txt" type="text" name="width" size="4" onblur="this.value=checkDigit(this.value,1)" /></td>
	<td><?= PEword(131) ?>:</td>
	<td><input class="txt" type="text" name="height" size="4" onblur="this.value=checkDigit(this.value,1)" /></td>
</tr>
<tr>
	<td><?= PEword(12) ?>:</td>
	<td><table cellspacing="0" cellpadding="0" border="0"><tr>
	<td><input class="txt" type="text" name="bgcolor" size="7" maxlength="7" /></td>
	<td><img src="pics/font_bgcolor.png" width="16" height="16" alt="" onclick="setColor('cell')" style="cursor:pointer;cursor:hand" /></td>
	</tr></table>
	</td>
</tr>
<tr>
	<td><?= PEword(257) ?>:</td>
	<td colspan="3"><table cellspacing="0" cellpadding="0" border="0"><tr>
	<td><input class="txt" type="text" name="bgimage" size="40" style="width:214px" /></td>
	<td><img src="pics/picture.png" width="16" height="16" onclick="setBackground('cell')" style="cursor:pointer;cursor:hand" /></td>
	</tr></table>
	</td>
</tr>
<tr>
	<td></td>
	<td colspan="3"><input type="checkbox" id="nowrap" name="nowrap" /><Label for="nowrap"><?= PEword(163) ?></label></td>
</tr>
<tr id="CcssTR" style="visibility:hidden">
	<td><?= PEword(137) ?>:</td>
	<td id="CcssTD" colspan="3"><input class="txt" type="text" name="Fclass" size="20" style="width:234px" /></td>
</tr>
</table>
</form>
</div>

<div id="rowDiv" style="position:absolute;left:6px;top:18px;width:308px;visibility:hidden">
<form name="rowForm">
<table width="100%">
<tr>
	<td width="55"><?= PEword(135) ?>:</td>
	<td><select name="align">
	<option value=""><?= PEword(219) ?></option>
	<option value="left"><?= PEword(220) ?></option>
	<option value="right"><?= PEword(221) ?></option>
	<option value="center"><?= PEword(226) ?></option>
	</select></td>  
	<td width="55"><?= PEword(162) ?>:</td>
	<td><select name="valign">
	<option value=""><?= PEword(219) ?></option>
	<option value="top"><?= PEword(222) ?></option>
	<option value="middle"><?= PEword(223) ?></option>
	<option value="baseline"><?= PEword(227) ?></option>
	<option value="bottom"><?= PEword(224) ?></option>
	</select></td>  
</tr>
<tr>
	<td><?= PEword(12) ?>:</td>
	<td><table cellspacing="0" cellpadding="0" border="0"><tr>
	<td><input class="txt" type="text" name="bgcolor" size="7" maxlength="7"></td>
	<td><img src="pics/font_bgcolor.png" width="16" height="16" alt="" onclick="setColor('row')" style="cursor:pointer;cursor:hand"></td>
	</tr></table>
	</td>
</tr>
<tr id="RcssTR" style="visibility:hidden">
	<td><?= PEword(137) ?>:</td>
	<td id="RcssTD" colspan="3"><input class="txt" type="text" name="Fclass" size="20" style="width:234px" /></TD>
</tr>
</table>
</form>
</div>

<?php } ?>

<div id="tblDiv" style="position:absolute;left:6px;top:18px;width:308px;visibility:<?php if($nt) print 'visible'; else print 'hidden' ?>">
<form name="tblForm">
<table width="100%">
<tr>
	<td><?= PEword(164) /* Rows */ ?>:</td>
	<td><input class="txt" type="text" name="rows" size="3" maxlength="3" onblur="this.value=checkDigit(this.value,0)" disabled="disabled"></td>
	<td><?= PEword(165) /* Cols */ ?>:</td>
	<td><input class="txt" type="text" name="columns" size="3" maxlength="3" onblur="this.value=checkDigit(this.value,0)" disabled="disabled" /></td>
</tr>
<tr>
	<td><?= PEword(166) /* Padding */ ?>:</td>
	<td><input class="txt" type="text" name="padding" size="3" maxlength="3" onblur="this.value=checkDigit(this.value,0)" /></td>
	<td><?= PEword(167) /* Spacing */ ?>:</td>
	<td><input class="txt" type="text" name="spacing" size="3" maxlength="3" onblur="this.value=checkDigit(this.value,0)" /></td>
</tr>
<tr>
	<td><?= PEword(132) /* Border */ ?>:</td>
	<td><input class="txt" type="text" name="border" size="3" maxlength="3" onblur="this.value=checkDigit(this.value,0)"></td>
	<TD><?= PEword(12) /* Background color */ ?>:</TD>
	<td><table cellspacing="0" cellpadding="0" border="0"><tr>
	<td><input class="txt" type="text" name="bgcolor" size="7" maxlength="7" /></td>
	<td><img src="pics/font_bgcolor.png" width="16" height="16" alt="" onclick="setColor('tbl')" style="cursor:pointer;cursor:hand" /></td>
	</tr></table></td>
</tr>
<tr>
	<td><?= PEword(130) /* Width */ ?>:</td>
	<td><input class="txt" type="text" name="width" size="4" maxlength="4" onblur="this.value=checkDigit(this.value,1)" /></td>
	<td><?= PEword(131) /* Height */ ?>:</td>
	<td><input class="txt" type="text" name="height" size="4" maxlength="4" onblur="this.value=checkDigit(this.value,1)" /></td>
</tr>
<tr>
	<td>Align:</td>
	<td colspan="3"><select name="align">
	<option value=""><?= PEword(219) ?></option>
	<option value="left"><?= PEword(220) ?></option>
	<option value="center"><?= PEword(226) ?></option>
	<option value="right"><?= PEword(221) ?></option>
	</select></td>
</tr>
<tr>
	<td><?= PEword(168) /* Caption */ ?>:</td>
	<!--<td colspan="2"><input class="txt" type="text" name="caption" style="width:140px" /></td>-->
	<td colspan="2"><textarea class="txt" name="caption" style="width:140px" rows="2"></textarea></td>
	<td nowrap="nowrap">
	<input type="radio" id="capTop" name="cap_pos" value="top" checked="checked" /><label for="capTop"><?= PEword(203) ?></label>
	<input type="radio" id="capBot" name="cap_pos" value="bottom" /><label for="capBot"><?= PEword(204) ?></label>
	</td>
</tr>
<tr>
	<td><?= PEword(210) /* Summary */ ?>:</TD>
	<td colspan="3"><input class="txt" type="text" name="summary" style="width:216px" /></td>
</tr>
<tr>
	<td><?= PEword(257) /* Background image */ ?>:</td>
	<td colspan="3">
	<table cellspacing="0" cellpadding="0" border="0"><tr>
	<td><input class="txt" type="text" name="bgimage" style="width:216px" /></td>
	<td><img src="pics/picture.png" width="16" height="16" alt="" onclick="setBackground('tbl')" style="cursor:pointer;cursor:hand" /></td>
	</tr></table></td>
</tr>
<tr id="TcssTR" style="visibility:hidden">
	<td><?= PEword(137) ?>:</td>
	<td id="TcssTD" colspan="3"><input class="txt" type="text" name="Fclass" size="20" style="width:226px" /></td>
</tr>
</table>
</form>
</div>

<fieldset>
<legend id="legendTitle"><b><?= $title ?></b></legend>
<form name="whatForm">
<table width="100%">
<tr><td><img src="pics/pixel.gif" width="1" height="255" alt="" /></td></tr>
</table>
</fieldset>

<br />

<table width="100%">
<tr>
<?php if (!$nt) { ?>
<td width="55%" nowrap="nowrap">
<input type="radio" id="whatCell" name="what" value="cell" checked="checked" onclick="editIt('cell')" /> <label for="whatCell"><?= PEword(169) ?></label>
<input type="radio" id="whatRow" name="what" value="row" onclick="editIt('row')" /> <label for="whatRow"><?= PEword(170) ?></label>
<input type="radio" id="whatTbl" name="what" value="table" onclick="editIt('table')" /> <label for="whatTbl"><?= PEword(171) ?></label>
</td>
<?php } ?>
<td align="right" width="45%" nowrap="nowrap">
<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" /> 
<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</td></tr>
</table>

</form>

</body>
</html>
