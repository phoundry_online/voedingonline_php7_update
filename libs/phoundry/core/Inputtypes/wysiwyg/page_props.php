<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(247) . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

var DOM;

function init() {
	var f = _D.phoundry;
	DOM = winArgs['dom'];
	f.title.value = DOM.title;
	f.bgColor.value = DOM.body.bgColor;
	f.bgimage.value = DOM.body.background;
	_D.getElementById('bgColorClr').style.backgroundColor = DOM.body.bgColor;
	f.text.value    = DOM.body.text;
	_D.getElementById('textClr').style.backgroundColor = DOM.body.text;
	f.link.value    = DOM.body.link;
	_D.getElementById('linkClr').style.backgroundColor = DOM.body.link;
	f.vLink.value   = DOM.body.vLink;
	_D.getElementById('vLinkClr').style.backgroundColor = DOM.body.vLink;
	f.aLink.value   = DOM.body.aLink;
	_D.getElementById('aLinkClr').style.backgroundColor = DOM.body.aLink;
	f.title.focus();
}

function setColor(what) {
	var A=[], color, url;
	color = _D.phoundry[what].value;
	url = 'colorpick.php?l=<?= $lang ?>&bg=<?= urlencode($_GET['bg']) ?>&color='+escape(color);
	A['what'] = what;
	A['fn'] = function(wh,clr){setColor_(wh,clr)};
	if (isMidas) {
		winArgs = A;
		_W.open(url,'PEW2','width=390,height=210,left='+((_W.outerWidth-390)/2+_W.screenX)+',top='+((_W.outerHeight-180)/2+_W.screenY));
	}
	else {
		showModalDialog(url, A, 'dialogWidth:390px;dialogHeight:210px;scroll:0;status:no;help:0;');
	}
}
function setColor_(what,color) {
	_D.phoundry[what].value = color;
	_D.getElementById(what+'Clr').style.backgroundColor = color;
}


function setBackground() {
	var url = 'file.php?mode=explorer&lang=<?= $lang ?>&dir='+escape(winArgs['imgDir']);
	if (!winArgs['upload']) {
		url += '&allowFolders=false&allowFiles=false';
	}
	winArgs['callback'] = function(attrs){setBackground_(attrs['url'])};
	if (isMidas) {
		_W.open(url,'PEW2','width=600,height=460,left='+((_W.outerWidth-525)/2+_W.screenX)+',top='+((_W.outerHeight-415)/2+_W.screenY));
	}
	else {
		A = showModalDialog(url,winArgs,'dialogWidth:600px;dialogHeight:460px;help:0;status:0;scroll:0');
	}
}

function setBackground_(src){
	_D.phoundry['bgimage'].value = src;
}

function setPreview(what, value) {
	try {
		_D.getElementById(what+'Clr').style.backgroundColor = value;
	}
	catch(e) {
		_D.getElementById(what+'Clr').style.backgroundColor = 'menu';
	}
}

function submitMe() {
	var f = _D.phoundry;

	if (f.text.value=='')
		DOM.body.removeAttribute('text',0);
	else
		DOM.body.setAttribute('text',f.text.value);

	if (f.link.value=='')
		DOM.body.removeAttribute('link',0);
	else
		DOM.body.setAttribute('link',f.link.value);

	if (f.vLink.value=='')
		DOM.body.removeAttribute('vLink',0);
	else
		DOM.body.setAttribute('vLink',f.vLink.value);

	if (f.aLink.value=='')
		DOM.body.removeAttribute('aLink',0);
	else
		DOM.body.setAttribute('aLink',f.aLink.value);

	if (f.bgColor.value=='')
		DOM.body.removeAttribute('bgColor',0);
	else
		DOM.body.setAttribute('bgColor',f.bgColor.value);

	if (f.bgimage.value=='')
		DOM.body.removeAttribute('background',0);
	else
		DOM.body.setAttribute('background', f.bgimage.value);

	DOM.title = f.title.value;
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.txt { width:60px }

.clr {
	border: 1px solid black; padding: 0; margin: 0;
}

-->
</style>
</head>
<body onload="init()" oncontextmenu="return false">
<form name="phoundry">
<fieldset>
<legend><b><?= PEword(247) ?></b></legend>
<table>
<tr>
	<td><?= PEword(251) ?>:</td>
	<td colspan="3"><input class="txt" type="text" name="title" size="20" style="width:160px"></td>
</tr>
<tr>
	<td><?= PEword(11) ?>:</td>
	<td><input class="txt" type="text" name="text" size="7" onblur="setPreview('text',this.value)" />
	<td id="textClr" class="clr"><img src="pics/pixel.gif" alt="" width="20" height="5" /></td>
	<td><img src="pics/font_bgcolor.png" title="Color sample" width="16" height="16" border="0" onclick="setColor('text')" style="cursor:pointer" /></td>
</tr>
<tr>
	<td><?= PEword(248) ?>:</td>
	<td><input class="txt" type="text" name="link" size="7" onblur="setPreview('link',this.value)" />
	<td id="linkClr" class="clr"><img src="pics/pixel.gif" alt="" width="20" height="5" /></td>
	<td><img src="pics/font_bgcolor.png" title="Color sample" width="16" height="16" border="0" onclick="setColor('link')" style="cursor:pointer" /></td>
</tr>
<tr>
	<td><?= PEword(249) ?>:</td>
	<td><input class="txt" type="text" name="vLink" size="7" onblur="setPreview('vLink',this.value)" />
	<td id="vLinkClr" class="clr"><img src="pics/pixel.gif" alt="" width="20" height="5" /></td>
	<td><img src="pics/font_bgcolor.png" title="Color sample" width="16" height="16" border="0" onclick="setColor('vLink')" style="cursor:pointer" /></td>
</tr>
<tr>
	<td><?= PEword(250) ?>:</td>
	<td><input class="txt" type="text" name="aLink" size="7" onblur="setPreview('aLink',this.value)" />
	<td id="aLinkClr" class="clr"><img src="pics/pixel.gif" alt="" width="20" height="5" /></td>
	<td><img src="pics/font_bgcolor.png" title="Color sample" width="16" height="16" border="0" onclick="setColor('aLink')" style="cursor:pointer" /></td>
</tr>
<tr>
	<td><?= PEword(12) ?>:</td>
	<td><input class="txt" type="text" name="bgColor" size="7" onblur="setPreview('bgColor',this.value)" />
	<td id="bgColorClr" class="clr"><img src="pics/pixel.gif" alt="" width="20" height="5" /></td>
	<td><img src="pics/font_bgcolor.png" title="Color sample" width="16" height="16" onclick="setColor('bgColor')" style="cursor:pointer" /></td>
</tr>
<tr>
	<td><?= PEword(257) ?>:</td>
	<td colspan="2"><input class="txt" type="text" name="bgimage" style="width:140px" /></td>
	<td><img src="pics/picture.png" width="16" height="16" onclick="setBackground()" style="cursor:pointer" /></td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
