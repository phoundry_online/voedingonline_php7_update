<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(199)  . str_repeat('&nbsp; ', 50) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function init() 
{
	var f = _D.forms['phoundry'];

	if (winArgs['editor'].feat&4) {
		if (winArgs['editor'].classes.length) {
			var classes = winArgs['editor'].classes;
			var html = '<select name="Fclass"><option value=""><?= PEword(219) ?></option>';
			for(var x = 0; x < classes.length; x++)
				html += '<option value="' + classes[x] + '">' + classes[x] + '</option>';
			html += '</select>';
			_D.getElementById('cssTD').innerHTML = html;
		}
		_D.getElementById('cssTR').style.visibility='visible';
	}

	if (winArgs['el']) {
		EL = winArgs['el'];
		f.name.value = EL.name;
	}

	if (winArgs['editor'].feat&4 && winArgs['editor'].classes.length && EL && EL.className == '')
		f.Fclass.selectedIndex = 0;
	else if (winArgs['editor'].feat&4 && EL)
		f.Fclass.value = EL.className;

	f.name.focus();
}

function submitMe() {
	var f = _D.forms['phoundry'], A = [], tag;
	A['name'] = f.name.value;
	if (!/^[a-z0-9_\-]+$/i.test(A['name'])) {
		alert('<?= PEescSquote(PEword(271)) ?>');
		return;
	}
	A['class'] = f.Fclass.value;
	if (!EL) {
		tag='<a';
		if(A['name'])tag+=' name="'+A['name']+'"';
		if(A['class'])tag+=' class="'+A['class']+'"';
		tag += '>';
		winArgs['editor'].insertHTML(tag,'</a>');
	}
	else {
		if(A['name']=='') {
			try{winArgs['document'].execCommand('unlink',false,null)}catch(e){};
		}
		else {
			if (isMidas) {
				EL.setAttribute('name', A['name']);
				EL.setAttribute('id', A['name']);
				EL.removeAttribute('href',0);
				if (A['class'])
					EL.setAttribute('class', A['class']);
				else
					EL.removeAttribute('class',0);
			}
			else {
				// MSIE refuses to set the 'name' attribute in an A tag...
				// We need to hack around it, using outerHTML.
				var oh = '<a name="'+A['name']+'" id="'+A['name']+'"';
				if (A['class']) oh += ' class="'+A['class']+'"';
				oh += '>' + EL.innerHTML + '</a>';
				EL.outerHTML = oh;
			}
		}
	}
	_W.close();
}

function closeMe() {
	if(winArgs['new']) {
		try{winArgs['document'].execCommand('unlink',false,null)}catch(e){};
	}
	_W.close();
}

//]]>
</script>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
</head>
<body onload="init()" oncontextmenu="return false">
<form name="phoundry" onsubmit="return false">
<fieldset>
<legend><b><?= PEword(199) ?></b></legend>
<table>
<tr>
	<td><?= PEword(178) ?>:</td>
	<td><input class="txt" type="text" style="width:200px" name="name" /></td>
</tr>
<tr id="cssTR" style="visibility:hidden">
	<td><?= PEword(137); ?>:</td>
	<td id="cssTD"><input class="txt" type="text" name="Fclass" size="20" style="width:200px" /></td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitMe()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="closeMe()" />
</p>

</form>
</body>
</html>
