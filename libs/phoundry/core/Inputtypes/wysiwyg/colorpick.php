<?php
	require './common.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require "lang/$lang.php";
?>
<!-- 
    Copyright (c) <?= date('Y') ?> by Web Power, The Netherlands (http://www.webpower.nl)
    Programming by Arjan Haverkamp (arjan-at-webpower.nl)       
-->
<html>
<head>
<title><?= PEword(172) . str_repeat('&nbsp; ', 50) ?></title>
<link rel="stylesheet" href="dialog.css.php?bg=<?= urlencode($_GET['bg']) ?>" type="text/css" />
<style type="text/css">
<!--

.cslider {
	position: relative;
	-moz-user-focus: normal;
	-moz-user-select:	none;
	cursor: default;
}

.horizontal {
	width: 200px;
	height: 27px;
}

.cslider input {
	display:	none;
}

.cslider .handle {
	position: absolute;	
	overflow: hidden;
	-moz-user-select: none;
	cursor: default;
	margin-top: 2px;
	width: 11px;
	height: 14px;
}

.cslider .red {
	position: absolute;
	overflow: hidden;
	height: 6px;
	background-image: url(pics/red.gif);
}

.cslider .blue {
	position: absolute;
	overflow: hidden;
	height: 6px;
	background-image: url(pics/blue.gif);
}

.cslider .green {
	position: absolute;
	overflow: hidden;
	height: 6px;
	background-image: url(pics/green.gif);
}

.cslider {
	width: auto;
	height: 20px;
	margin: 0;
}

.color-picker input {
	width: 30px;
	border: 1px inset window;
	padding: 0;
	margin: 0;
}
.color-picker td,
.color-picker input, .but {
	font:	Icon;
}

#color-result {
	width: 29px;
	border: 1px inset window;
}

.colorBox {
	width: 8px;
	height: 8px;
	cursor: pointer;
	cursor: hand;
}

-->
</style>

<script type="text/javascript">
//<![CDATA[

///////////////////////////////////////////////////////////////////////
//     This slidebar script was designed by Erik Arvidsson for WebFX //
//                                                                   //
//     For more info and examples see: http://webfx.eae.net          //
//     or send mail to erik@eae.net                                  //
//                                                                   //
//     Feel free to use this code as lomg as this disclaimer is      //
//     intact.                                                       //
///////////////////////////////////////////////////////////////////////

var _W=window, _D=document, isMidas = <?= $_isMidas ? 1 : 0 ?>, EL;
var winArgs = _W.dialogArguments ? _W.dialogArguments : _W.opener.winArgs;

function Range() {
	this._value = 0;
	this._minimum = 0;
	this._maximum = 100;
	this._extent = 0;
	this._isChanging = false;
}

Range.prototype.setValue = function(value) {
	value = parseInt(value);
	if (isNaN(value)) return;
	if (this._value != value) {
		if (value + this._extent > this._maximum)
			this._value = this._maximum - this._extent;
		else if (value < this._minimum)
			this._value = this._minimum;
		else
			this._value = value;
		if (!this._isChanging && typeof this.onchange == 'function')
			 this.onchange();
	}
};

Range.prototype.getValue = function() {
	return this._value;
};

Range.prototype.setExtent = function(extent) {
	if (this._extent != extent) {
		if (extent < 0)
			this._extent = 0;
		else if (this._value + extent > this._maximum)
			this._extent = this._maximum - this._value;
		else
			this._extent = extent;
		if (!this._isChanging && typeof this.onchange == 'function')
			this.onchange();
	}
};

Range.prototype.setMaximum = function(maximum) {
	if (this._maximum != maximum) {
		var oldIsChanging = this._isChanging;
		this._isChanging = true;

		this._maximum = maximum;		
		
		if (maximum < this._value)
			this.setValue(maximum - this._extent);
		if (maximum < this._minimum) {
			this._extent = 0;
			this.setValue(this._maximum);
		}		
		if (maximum < this._minimum + this._extent)
			this._extent = this._maximum - this._minimum;
		if (maximum < this._value + this._extent)
			this._extent = this._maximum - this._value;
		
		this._isChanging = oldIsChanging;
		if (!this._isChanging && typeof this.onchange == 'function')
			this.onchange();
	}
};

Slider.isSupported = typeof _D.createElement != 'undefined' &&
	typeof _D.documentElement != 'undefined' &&
	typeof _D.documentElement.offsetWidth == 'number';


function Slider(oElement, oInput, oColor) {

	this._color = oColor;
	this._range = new Range();
	this._range.setExtent(0);


	this._blockIncrement = 10;
	this._unitIncrement = 1;
		
	if (Slider.isSupported && oElement) {
		
		this.document = oElement.ownerDocument || oElement.document;
		
		this.element = oElement;
		this.element.slider = this;
		this.element.unselectable = 'on';

		this.element.className = 'horizontal ' + this.classNameTag + ' ' + this.element.className;

		this.line = this.document.createElement('DIV');
		this.line.className = oColor;
		this.line.unselectable = 'on';
		this.line.appendChild(this.document.createElement('DIV'));
		this.element.appendChild(this.line);
		
		this.handle = this.document.createElement('DIV');
		this.handle.className = 'handle';
		this.handle.unselectable = 'on';
		this.handle.innerHTML = '<img src="pics/arrow.gif" alt="" width="11" height="14" />';
		this.element.appendChild(this.handle);
	}
	
	this.input = oInput;

	var oThis = this;
	this._range.onchange = function () {
		oThis.recalculate();
		if (typeof oThis.onchange == 'function')
			oThis.onchange();
	};	
	
	if (Slider.isSupported && oElement) {
		this.element.onfocus		= Slider.eventHandlers.onfocus;
		this.element.onblur			= Slider.eventHandlers.onblur;
		this.element.onmousedown	= Slider.eventHandlers.onmousedown;
		this.element.onmouseover	= Slider.eventHandlers.onmouseover;
		this.element.onmouseout		= Slider.eventHandlers.onmouseout;
		this.element.onkeydown		= Slider.eventHandlers.onkeydown;
		this.element.onkeypress		= Slider.eventHandlers.onkeypress;
		this.handle.onselectstart	=
		this.element.onselectstart	= function () { return false; };
	}
	else {
		this.input.onchange = function (e) {
			oThis.setValue(oThis.input.value);
		};
	}
	this._range.setMaximum(255);
	this.input.value = 255;
}

Slider.eventHandlers = {
	getEvent:	function (e, el) {
		if (!e) {
			if (el)
				e = el.document.parentWindow.event;
			else
				e = _W.event;
		}
		if (!e.srcElement) {
			var el = e.target;
			while (el != null && el.nodeType != 1)
				el = el.parentNode;
			e.srcElement = el;
		}
		if (typeof e.offsetX == 'undefined') {
			e.offsetX = e.layerX;
			e.offsetY = e.layerY;
		}
		
		return e;
	},
	
	getDocument:	function (e) {
		if (e.target)
			return e.target.ownerDocument;
		return e.srcElement.document;
	},
	
	getSlider:	function (e) {
		var el = e.target || e.srcElement;
		while (el != null && el.slider == null)	{
			el = el.parentNode;
		}
		if (el)		
			return el.slider;
		return null;
	},
	
	getLine:	function (e) {
		var el = e.target || e.srcElement;
		while (el != null && el.className != 'line')	{
			el = el.parentNode;
		}			
		return el;
	},
	
	getHandle:	function (e) {
		var el = e.target || e.srcElement;
		var re = /handle/;
		while (el != null && !re.test(el.className))	{
			el = el.parentNode;
		}			
		return el;
	},
	
	onfocus:	function (e) {
		var s = this.slider;
		s._focused = true;
	},
	
	onblur:	function (e) {
		var s = this.slider
		s._focused = false;
	},
	
	onmouseover:	function (e) {
		e = Slider.eventHandlers.getEvent(e, this);
		var s = this.slider;
		if (e.srcElement == s.handle)
			s.handle.className = 'handle hover';
	},
	
	onmouseout:	function (e) {
		e = Slider.eventHandlers.getEvent(e, this);
		var s = this.slider;
		if (e.srcElement == s.handle && !s._focused)
			s.handle.className = 'handle';
	},

	onmousedown:	function (e) {
		e = Slider.eventHandlers.getEvent(e, this);
		var s = this.slider;
		if (s.element.focus)
			s.element.focus();
		
		Slider._currentInstance = s;
		var doc = s.document;
		
		if (doc.addEventListener) {
			doc.addEventListener('mousemove', Slider.eventHandlers.onmousemove, true);
			doc.addEventListener('mouseup', Slider.eventHandlers.onmouseup, true);
		}
		else if (doc.attachEvent) {
			doc.attachEvent('onmousemove', Slider.eventHandlers.onmousemove);
			doc.attachEvent('onmouseup', Slider.eventHandlers.onmouseup);
			doc.attachEvent('onlosecapture', Slider.eventHandlers.onmouseup);
			s.element.setCapture();
		}
		
		if (Slider.eventHandlers.getHandle(e)) {	// start drag
			Slider._sliderDragData = {
				screenX:	e.screenX,
				screenY:	e.screenY,
				dx:			e.screenX - s.handle.offsetLeft,
				dy:			e.screenY - s.handle.offsetTop,
				startValue:	s.getValue(),
				slider:		s
			};
		}
		else {
			var lineEl = Slider.eventHandlers.getLine(e);
			s._mouseX = e.offsetX + (lineEl ? s.line.offsetLeft : 0);
			s._mouseY = e.offsetY + (lineEl ? s.line.offsetTop : 0);
			s._increasing = null;
		}
	},
	
	onmousemove:	function (e) {
		e = Slider.eventHandlers.getEvent(e, this);
		
		if (Slider._sliderDragData) {	// drag
			var s = Slider._sliderDragData.slider;

			var boundSize = 255;
			var size, pos, reset;
			size = s.element.offsetWidth - s.handle.offsetWidth;
			pos = e.screenX - Slider._sliderDragData.dx;
			reset = Math.abs(e.screenY - Slider._sliderDragData.screenY) > 100;
			s.setValue(reset ? Slider._sliderDragData.startValue : boundSize * pos / size);
			return false;
		}
		else {
			var s = Slider._currentInstance;
			if (s != null) {
				var lineEl = Slider.eventHandlers.getLine(e);
				s._mouseX = e.offsetX + (lineEl ? s.line.offsetLeft : 0);
				s._mouseY = e.offsetY + (lineEl ? s.line.offsetTop : 0);
			}
		}
		
	},
	
	onmouseup:	function (e) {
		e = Slider.eventHandlers.getEvent(e, this);
		var s = Slider._currentInstance;
		var doc = s.document;
		if (doc.removeEventListener) {
			doc.removeEventListener('mousemove', Slider.eventHandlers.onmousemove, true);
			doc.removeEventListener('mouseup', Slider.eventHandlers.onmouseup, true);
		}
		else if (doc.detachEvent) {
			doc.detachEvent('onmousemove', Slider.eventHandlers.onmousemove);
			doc.detachEvent('onmouseup', Slider.eventHandlers.onmouseup);
			doc.detachEvent('onlosecapture', Slider.eventHandlers.onmouseup);
			s.element.releaseCapture();
		}

		updateColors();

		if (Slider._sliderDragData)
			Slider._sliderDragData = null;
		else
			s._increasing = null;
		Slider._currentInstance = null;
	},
	
	onkeydown:	function (e) {
		e = Slider.eventHandlers.getEvent(e, this);
		var s = this.slider;
		var kc = e.keyCode;
		switch (kc) {
			case 33:	// page up
				s.setValue(s.getValue() + s.getBlockIncrement());
				break;
			case 34:	// page down
				s.setValue(s.getValue() - s.getBlockIncrement());
				break;
			case 35:	// end
				s.setValue(255);
				break;
			case 36:	// home
				s.setValue(0);
				break;
			case 39:	// right
				s.setValue(s.getValue() + s.getUnitIncrement());
				break;
			
			case 37:	// left
				s.setValue(s.getValue() - s.getUnitIncrement());
				break;
		}
		
		if (kc >= 33 && kc <= 40) {
			updateColors();
			return false;
		}
	},
	
	onkeypress:	function (e) {
		e = Slider.eventHandlers.getEvent(e, this);
		var kc = e.keyCode;
		if (kc >= 33 && kc <= 40) {
			return false;
		}
	}
};

Slider.prototype.classNameTag = 'cslider',
	
Slider.prototype.setValue = function (v) {
	this._range.setValue(v);
	this.input.value = this.getValue();
};

Slider.prototype.getValue = function () {
	return this._range.getValue();
};

Slider.prototype.getUnitIncrement = function () {
	return this._unitIncrement;
};

Slider.prototype.getBlockIncrement = function () {
	return this._blockIncrement;
};

Slider.prototype.recalculate = function() {
	if (!Slider.isSupported || !this.element) return;
	
	var w = this.element.offsetWidth;
	var h = this.element.offsetHeight;
	var hw = this.handle.offsetWidth;
	var hh = this.handle.offsetHeight;
	var lw = this.line.offsetWidth;
	var lh = this.line.offsetHeight;
	this.handle.style.left = (w - hw) * this.getValue() / 255 + 'px';
	this.handle.style.top = (h - hh) / 2 + 'px';
	this.line.style.top = (h - lh) / 2 + 'px';
	this.line.style.left = hw / 2 + 'px';
	this.line.style.width = Math.max(0, w - hw - 2)+ 'px';
	this.line.firstChild.style.width = Math.max(0, w - hw - 4)+ 'px';
};

var r,g,b,Red=0,Green=0,Blue=0;

function init() {
	r = new Slider(_D.getElementById('red-slider'), _D.getElementById('red-slider-input'), 'red');
	g = new Slider(_D.getElementById('green-slider'), _D.getElementById('green-slider-input'), 'green');
	b = new Slider(_D.getElementById('blue-slider'), _D.getElementById('blue-slider-input'), 'blue');

	var ri = _D.getElementById('red-input');
	ri.onchange = function () {
		r.setValue(parseInt(this.value));updateColors();
	}

	var gi = _D.getElementById('green-input');
	gi.onchange = function () {
		g.setValue(parseInt(this.value));updateColors();
	}

	var bi = _D.getElementById('blue-input');
	bi.onchange = function () {
		b.setValue(parseInt(this.value));updateColors();
	}

	r.onchange = g.onchange = b.onchange = function () {
		var cr = _D.getElementById('color-result');
		cr.style.backgroundColor = 'rgb(' + r.getValue() + ',' + g.getValue() + ',' + b.getValue() + ')';
		ri.value = r.getValue();
		gi.value = g.getValue();
		bi.value = b.getValue();
	}
	<?php
		if (!empty($_GET['color']) && preg_match('/^#[ABCDEF0123456789]{6}$/i', $_GET['color']))
			print "updateHex('{$_GET['color']}');\n";
		else
			print "updateHex('#808080');\n";
	?>
	_D.images['HSVimg'].onmousedown = calculateRGB;
	_D.getElementById('colorTbl').style.visibility = 'visible';
}

function setRgb(nRed, nGreen, nBlue) {
	Red = nRed, Green = nGreen, Blue = nBlue;
	r.setValue(Red); g.setValue(Green); b.setValue(Blue);
	_D.forms[0].hexValue.value = rgb2hex(Red,Green,Blue);	
}

function rgb2hex(r,g,b) {
	var rHex = r.toString(16).toUpperCase();
	var gHex = g.toString(16).toUpperCase();
	var bHex = b.toString(16).toUpperCase();
	if (rHex.length < 2) rHex = '0' + rHex;
	if (gHex.length < 2) gHex = '0' + gHex;
	if (bHex.length < 2) bHex = '0' + bHex;
	return '#' + rHex + gHex + bHex;
}

function updateHex(value) {
	var Red = parseInt(value.substring(1,3), 16);
	var Green = parseInt(value.substring(3,5), 16);
	var Blue = parseInt(value.substring(5,7), 16);
	r.setValue(Red);
	g.setValue(Green);
	b.setValue(Blue);
	_D.forms[0].hexValue.value = rgb2hex(Red,Green,Blue);
}

function updateColors() {
	Red = r.getValue();
	Green = g.getValue();
	Blue = b.getValue();
	_D.forms[0].hexValue.value = rgb2hex(Red,Green,Blue);
}

function hsv2rgb(hsv) {
	var rgb = new Object();
	var i, f, p, q, t;

	if (hsv.s == 0) {
		rgb.r = rgb.g = rgb.b = hsv.v;
		return rgb;
	}
	hsv.h /= 60;
	i = Math.floor( hsv.h );
	f = hsv.h - i;
	p = hsv.v * (1 - hsv.s);
	q = hsv.v * (1 - hsv.s * f);
	t = hsv.v * (1 - hsv.s * ( 1 - f ));
	switch(i) {
		case 0:
			rgb.r = hsv.v;
			rgb.g = t;
			rgb.b = p;
			break;
		case 1:
			rgb.r = q;
			rgb.g = hsv.v;
			rgb.b = p;
			break;
		case 2:
			rgb.r = p;
			rgb.g = hsv.v;
			rgb.b = t;
			break;
		case 3:
			rgb.r = p;
			rgb.g = q;
			rgb.b = hsv.v;
			break;
		case 4:
			rgb.r = t;
			rgb.g = p;
			rgb.b = hsv.v;
			break;
		default:
			rgb.r = hsv.v;
			rgb.g = p;
			rgb.b = q;
			break;
	}
	rgb.r = Math.round(rgb.r*255);
	rgb.g = Math.round(rgb.g*255);
	rgb.b = Math.round(rgb.b*255);
	if (rgb.r > 255) rgb.r = 255;
	if (rgb.r < 0)   rgb.r = 0;
	if (rgb.g > 255) rgb.g = 255;
	if (rgb.g < 0)   rgb.g = 0;
	if (rgb.b > 255) rgb.b = 255;
	if (rgb.b < 0)   rgb.b = 0;
	return rgb;
}

function calculateRGB(e) {
	var x,y,h,hsv,el = _D.images['HSVimg'];
	if (isMidas) {
		x = e.pageX - el.x;
		y = e.pageY - el.y;
	}
	else {
		x = event.offsetX;
		y = event.offsetY;
	}

	hsv = new Object();
	h = el.height;
	hsv.h = 360 * x / el.width;
	if (y > h/2) {
		hsv.s = 1.0;
		hsv.v = 2 * (h - y) / h;
	}
	else {
		hsv.v = 1.0;
		hsv.s = y / (h/2);
	}
	
	var rgb = hsv2rgb(hsv);
	setRgb(rgb.r,rgb.g,rgb.b);
}

function submitColor() {
	if (winArgs['fn'])
		winArgs['fn'](winArgs['what'],_D.forms[0].hexValue.value);
	else
		winArgs['document'].execCommand(winArgs['what']=='fg'?'forecolor':isMidas?'hilitecolor':'backcolor',0,_D.forms[0].hexValue.value);
	_W.close();
}

//]]>
</script>
</head>
<body onload="init()" oncontextmenu="return false">
<form style="margin:0">
<fieldset>
<legend><b><?= PEword(172) ?></b></legend>
<table id="colorTbl" style="visibility:hidden" class="color-picker" cellspacing="2" cellpadding="0" border="0">
<tr>
	<td></td>
	<td><img src="pics/pixel.gif" width="153" height="1" alt="" /></td>
	<td></td>
</tr>
<tr>
	<td align="right">R</td>
	<td>
		<div class="slider" id="red-slider" tabIndex="1">
			<input class="slider-input" id="red-slider-input" />
		</div>
	</td>
	<td><input id="red-input" maxlength="3" tabIndex="2" /></td>
	<td>&nbsp;</td>
	<td rowspan="5" valign="top" align="center">
	<table cellpadding="0" cellspacing="0" border="0">
	<script type="text/javascript">
	<!--
		clr = ['00','33','66','99','CC','FF'];
		for(k = 0; k < 6; k++){
			for(j = 0; j < 6;) {
				_D.write('<tr>\n');
				for(m = 0; m < 3; m++) {
					for(i = 0; i < 6; i++)
						_D.write('<td class="colorBox" bgcolor="#'+clr[k]+clr[j+m]+clr[i]+'" onclick="updateHex(\'#'+clr[k]+clr[j+m]+clr[i]+'\')"></td>');
				}
				j+=3;
				_D.write('</tr>\n');
			}
		}
	//-->
	</script>
	</table>
	<?= PEword(176) ?>
	</td>
</tr>
<tr>
	<td align="right">G</td>
	<td>
		<div class="slider" id="green-slider" tabIndex="3">
			<input class="slider-input" id="green-slider-input" />
		</div>
	</td>
	<td><input id="green-input" maxlength="3" tabIndex="4" /></td>
</tr>
<tr>
	<td align="right">B</td>
	<td>
		<div class="slider" id="blue-slider" tabIndex="5">
			<input class="slider-input" id="blue-slider-input" />
		</div>
	</td>
	<td><input id="blue-input" maxlength="3" tabIndex="6" /></td>
</tr>
<tr>
	<td>HEX</td>
	<td align="right"><input type="text" name="hexValue" style="width:142px; margin-right:7px" onchange="updateHex(this.value)" /></td>
	<td id="color-result" rowspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" align="right">
	<img name="HSVimg" src="pics/colors.jpg" width="164" height="20" alt="<?= PEword(177) ?>" style="border:1px inset window; cursor:pointer; cursor:hand; margin-right:7px" />
	</td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= PEword(110) ?>" onclick="submitColor()" />
	<input type="button" value="<?= PEword(138) ?>" onclick="_W.close()" />
</p>

</form>
</body>
</html>
