<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITcheckbox extends Inputtype
{
	public $value;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
			if (count($this->extra)) {
				$this->value = $this->extra['value'];
			}
		}
	}

	public function getValue($page, $value, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				if ($value == $this->value) {
					return $this->value;
				}
				else {
					return $this->column->datatype->getDefaultValue();
				}
			default:
				return $value;
		}
	}
	
	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @param $includes(array) A reference to an array with this structure:
	 *                         'jsCode'=>array(),
	 *                         'jsFiles'=>array(),
	 *									'cssFiles'=>array()
	 *									Add entries to those arrays in order to have them
	 *									included in the <HEAD> of the edit-page.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;

		// Get default value:
		$value = parent::getInputHtml($page, $value, $includes);

		$alt = $this->column->getJScheckAltTag();
		$checked = ($value == $this->value) ? ' checked="checked" ' : ' ';
		$html = '<input type="checkbox"' . $checked . 'name="' . $this->name . '" alt="' . $alt . '" value="' . PH::htmlspecialchars($this->value) . '" />';

		return $html;
	}

	public function getExtra()
	{
		$value = null;
		if (count($this->extra)) {
			if (isset($this->extra['value']))
				$value = $this->extra['value'];
		}
		$adminInfo = array();
		$adminInfo['value'] = array(
			'name'     => 'ITvalue',
			'label'    => 'Value',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the value to store in the DB when the checkbox is checked. A NULL is stored when the checkbox is not checked.',
			'help'     => false,
			'input'    => array('type'=>'text', 'size'=>60, 'value'=>$value)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array('value'=>trim($_POST['ITvalue']));
		
		return $extra;
	}

}
