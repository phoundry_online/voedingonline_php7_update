<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITbigselect extends Inputtype
{
	public $value, $query, $help, $queryFields;
	public $updatable = true, $togglable = false;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);

			if (!empty($this->extra)) {
				if (isset($this->extra['query'])) {
					$query = $this->recordQuery = $this->editQuery = $this->extra['query'];
      	
					// Substitute php code injection with #number construction
					if (preg_match_all('/<\?(?:php)?(.+)\?>/Us', $query, $matches)) {
						foreach($matches[0] as $key=>$match) {
							$query = str_replace($match, '#'.$key, $query);
						}
					}
      
					if (preg_match('/^(.*)\[[^\]]+\](.*)$/s', $query, $reg)) {
						// If the query contains [...] (optional WHERE part), split
						// the query into two variants: one with the WHERE part and
						// one without it.
            
						$this->recordQuery = str_replace(array('[',']'), array('',''), $query);
						$this->editQuery   = $reg[1] . $reg[2];
						if (isset($matches)) {
							// Substitute #number construction with original php code injection
							foreach($matches[0] as $key=>$match) {
								$this->recordQuery = str_replace('#'.$key, $match, $this->recordQuery);
								$this->editQuery   = str_replace('#'.$key, $match, $this->editQuery);
							}
						}
					}
				}

				if (isset($this->extra['updatable'])) {
					$this->updatable = $this->extra['updatable'];
				}
				if (isset($this->extra['togglable'])) {
					$this->togglable = $this->extra['togglable'];
				}
			}
		}
	}

	public function getValue($page, $value, $keyval = null)
	{
		global $db;

		switch($page) {
			case 'view':
			case 'search':
			case 'export':
				$html = '';
				$sql = PH::evaluate($this->recordQuery);
				// Database query:
				$cur = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				for ($x = 0; !$db->EndOfResult($cur); $x++) {
					if ($db->FetchResult($cur,$x,$this->extra['optionvalue']) == $value) {
						foreach($this->extra['searchtext'] as $text)
							$html .= $db->FetchResult($cur,$x,$text) . ' ';
						break;
					}
					$html = trim($html);
				}
				if ($page == 'search' && $this->updatable && $this->togglable) {
					return '<a href="#" class="swapSelect cid' . $this->column->id . '">' . $html . '</a>';
				}
				return $html;
			default:
				return $value;
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;

		$alt = $this->column->getJScheckAltTag();

		$sql = PH::evaluate($this->editQuery);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$val = '';
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			if ($db->FetchResult($cur,$x,$this->extra['optionvalue']) == $value) {
				foreach($this->extra['optiontext'] as $text) {
					$val .= $db->FetchResult($cur,$x,$text) . ' ';
				}
				$val = trim($val);
				break;
			}
		}

		if (!$this->updatable && $page == 'update') {
			$html = '<input type="hidden" name="' . $this->name . '" value="' . $value . '" />';
		}
		else {
			$html = '<input type="hidden" name="' . $this->name . '" alt="' . $alt . '" value="' . $value . '" />';
			$html .= '<input type="text" disabled="disabled" name="dsp_' . $this->name . '" size="' . $this->extra['size'] . '" value="' . PH::htmlspecialchars($val) . '" />';
		}

		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $db, $PHSes, $PHprefs;

		$helpers = array(); $mayAdd = false;

		$url =  $PHprefs['url'] . '/core/popups/bigITsearch.php?' . QS(1,"CID={$this->column->id}");
		$helpers[] = '<a href="' . $url . '" tabindex="1" class="popupWin" title="' . ucfirst(word(10)) . '|400|240|0">' . strtolower(word(10)) . '</a>';
		if (!$this->column->required) {
			$helpers[] = '<a href="#" tabindex="1" onclick="_D.forms[0][\'' . $this->name . '\'].value=_D.forms[0][\'dsp_' . $this->name . '\'].value=\'\';return false">' . word(161) . '</a>';
		}
		return $helpers;
	}

	public function getExtra() 
	{
		global $db;

		$query = $queryFields = null;
		$updatable = true;
		$togglable = false;

		$size = 60;
		if (count($this->extra)) {
			if (isset($this->extra['size']))
				$size = $this->extra['size'];
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
			if (isset($this->extra['optionvalue'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
			if (isset($this->extra['updatable'])) {
				$updatable = $this->extra['updatable'];
			}
			if (isset($this->extra['togglable'])) {
				$togglable = $this->extra['togglable'];
			}
		}
		$adminInfo = array();
		$adminInfo['size'] = array(
			'name'     => 'ITsize',
			'label'    => 'Size',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the size of the text-box',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$size)
		);
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['togglable'] = array(
			'name'     => 'ITtogglable',
			'label'    => 'Togglable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be toggled in the record overview?',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$togglable)
		);
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query for the options in the select box (<a href="#" onclick="makePopup(null,300,200,\'Select Query\',\'selectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);
		$adminInfo['queryFields'] = array(
			'name'     => 'ITqueryFields',
			//'required' => true,
			//'syntax'   => 'text',
			'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
		);

		$tables = array();
		$sql = "SELECT id, name, description FROM phoundry_table WHERE show_in_menu NOT LIKE 'admin%' ORDER BY description";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$name = $db->FetchResult($cur,$x,'name');
			$tables[$db->FetchResult($cur,$x,'id')] = PH::langWord($db->FetchResult($cur,$x,'description')) . (!empty($name) ? ' (' . $name . ')' : '');
		}

		return $adminInfo;
	}

	public function setExtra(&$msg) {
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (preg_match('/^(SELECT|SHOW)/i', $query)) {
			if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
				$msg = 'Option value, option text and search text are not defined!';
				return false;
			}
		}

		$struct = PH::javascript2php($_POST['ITqueryFields']);
		$extra = array(
			'size'      => (int)$_POST['ITsize'],
			'query'     => $query,
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false,
			'togglable' => ($_POST['ITtogglable'] == 1) ? true : false
		);
		if (isset($struct['optionvalue']))
			$extra['optionvalue'] = (string)$struct['optionvalue'];
		if (isset($struct['optiontext']))
			$extra['optiontext'] = $struct['optiontext'];
		if (isset($struct['searchtext']))
			$extra['searchtext'] = $struct['searchtext'];


		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<h2>Regular SELECT query</h2>
<pre>
SELECT id, fields FROM table
  [WHERE id = {\$RECORD_ID}] ORDER BY name
</pre>
<p>
The WHERE-part (between square brackets) is used for identifying the current
record (on the search-page and the update-page). The brackets are required!
The {\$RECORD_ID} variable will be filled with the correct value by Phoundry.
</p>
<h2>Recursive SELECT query</h2>
<pre>
SELECT id,name,#parent# FROM category
  [WHERE id = {\$RECORD_ID}] ORDER BY name
</pre>
<p>
This type of query is used when a table contains a reference to itself. This
is often seen in tables containing categories. The first field in the SELECT-part will be the VALUE of the OPTION. 
The last field, which has to be enclosed by
<tt>#</tt>, identifies the field that references to the primary key (id) in the
same table.
</p>
<h2>Variables</h2>
<table>
<tr>
	<th>Variable</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$USER_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$USER_NAME}</td>
	<td>The full name (field <tt>name</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="odd">
	<td>{\$GROUP_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_group</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$word(nr)}</td>
	<td>A word from the language file with index 'nr'.</td>
</tr>
</table>
<p>
Any other variable may be used as well. In that case, Phoundry will look in the
\$_REQUEST array: if a value for the variable is found there, it will be
substituted.
</p>
EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		return $help;
	}
}
