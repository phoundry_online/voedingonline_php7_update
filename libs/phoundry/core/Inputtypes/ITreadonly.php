<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITreadonly extends Inputtype
{
	public $size, $maxlength, $value;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		// Get default value:

		$value = parent::getInputHtml($page, $value, $includes);
		$display = isset($this->column->datatype->extra['display']) ? $this->column->datatype->extra['display'] : 'literal';
		if ($display == 'html')
			$show = $value;
		elseif ($display == 'code')
			$show = '<pre>' . PH::htmlspecialchars($value) . '</pre>';
		else
			$show = PH::htmlspecialchars($value);

		return '<input name="' . $this->name . '" type="hidden" value="' . PH::htmlspecialchars($value) . '" />' . "\n$show\n";
	}
}
