<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class IThidden extends Inputtype
{
	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getHtml($page, $value = '', $readOnly = false, &$includes) 
	{
		if ($page == 'view' || $page == 'pdf') {
			if ($value === '' || is_null($value)) {
				$value = '&nbsp;';
			}
			return parent::getHtml($page, $value, $readOnly, $includes);
		}
		else
			return $this->getInputHtml($page, PH::evaluate($value), $includes);
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		// Get default value:
		$value = parent::getInputHtml($page, $value, $includes);
		return '<input name="' . $this->name . '" type="hidden" value="' . PH::htmlspecialchars($value) . '" />' . "\n";
	}
}
