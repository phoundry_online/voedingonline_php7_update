<?php

require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITwysiwyg extends Inputtype
{
	public $size, $maxlength;

	public function __construct(&$column) {
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $PHprefs, $PHSes;

		//$value = parent::getInputHtml($page, $value, $includes);
		if ($value === '' && $page == 'insert') {
			$value = $this->column->datatype->getDefaultValue();
		}

		if (isset($this->extra['updatable']) && $this->extra['updatable'] == false && $page == 'update') {
			return '<input name="' . $this->name . '" type="hidden" value="' . PH::htmlspecialchars($value) . '" />' . $value;
		}

		$alt = $this->column->getJScheckAltTag();
		if (file_exists($PHprefs['instDir'] . '/custom/Inputtypes/ITwysiwyg.js.php'))
			$includes['jsFiles'][strtolower(get_class($this))] = $PHprefs['url'] . '/custom/Inputtypes/ITwysiwyg.js.php?' . QS(1);

		$editor = new PhoundryEditor($this->name);
		$editor->set('width', $this->extra['width']);
		$editor->set('height', $this->extra['height']);
		if (isset($this->extra['settings'])) {
			eval($this->extra['settings']);
			foreach($settings as $settingName=>$settingValue) {
				if ($settingName == 'extensions')
					foreach($settingValue as $extension)
						$editor->set('extension', $extension);
				else {
					if ($settingName == 'imgDir' || $settingName == 'attachDir') {
						// Substitute $USER_ID and $GROUP_ID:
						$settingValue = str_replace(array('{$USER_ID}','{$GROUP_ID}'), array($PHSes->userId, $PHSes->groupId), $settingValue);
						$res = $this->column->makedirs($PHprefs['uploadDir'] . $settingValue)
							or trigger_error('Cannot create directory ' . $PHprefs['uploadDir'] . $settingValue . '!', E_USER_ERROR);
					}
					$editor->set($settingName, $settingValue);
				}
			}
			if (!isset($settings['charset']))
				$editor->set('charset', $PHprefs['charset']);
		}
		if ($value !== '')
			$editor->set('value', $value);
		return $editor->html($alt);
	}

	public function getHelperLinks($page) 
	{
		global $PHprefs;

		$helpers = array();
		$url = $PHprefs['url'] . '/core/popups/preview.php?' . QS(1,"field={$this->name}&type=wysiwyg");
      $helpers[] = '<a href="' . $url . '" tabindex="1" class="popupWin" title="' . ucfirst(word(49)) . '|500|400|0">' . word(49) . '</a>';
		return $helpers;
   }

	public function getExtra() 
	{
		$maxlength = isset($this->column->DBprops['length']) ? $this->column->DBprops['length'] : '';
		$width     = '95%';
		$height    = 400;
		$updatable = true;
		$settings  =<<<EOS
\$settings = array(
   'page'      => false,
   'tables'    => true,
   'forms'     => false,
   'useCSS'    => false,
   'css'       => '',
   'upload'    => true,
   'tidy'      => 'xhtml',
   'borders'   => false,
   'loadUrl'   => false,
   'classes'   => array(),
   'imgDir'    => '/IMAGES',
   'attachDir' => '/FILES',
   'target'    => '',
   'showSize'  => false
);
EOS;
		if (count($this->extra)) {
			if (isset($this->extra['width']))
				$width = $this->extra['width'];
			if (isset($this->extra['height']))
				$height = $this->extra['height'];
			if (isset($this->extra['maxlength']))
				$maxlength = $this->extra['maxlength'];
			if (isset($this->extra['updatable']))
				$updatable = $this->extra['updatable'];
			if (isset($this->extra['settings']))
				$settings = $this->extra['settings'];
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['cols'] = array(
			'name'     => 'ITwidth',
			'label'    => 'Width',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Enter the width of the WYSIWYG editor (in pixels or as a percentage)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$width)
		);
		$adminInfo['rows'] = array(
			'name'     => 'ITheight',
			'label'    => 'Height',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Enter the height of the WYSIWYG editor (in pixels)',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$height)
		);
		$adminInfo['maxlength'] = array(
			'name'     => 'ITmaxlength',
			'label'    => 'Max. length',
			'required' => false,
			'syntax'   => 'integer',
			'info'     => 'Enter the maximum input-length of the WYSIWYG editor',
			'input'    => array('type'=>'text', 'size'=>20, 'maxlength'=>10, 'value'=>$maxlength)
		);
		$adminInfo['settings'] = array(
			'name'     => 'ITsettings',
			'label'    => 'Settings',
			'required' => true,
			'syntax'   => 'string',
			'info'     => 'Configure the WYSIWYG editor.',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>110, 'rows'=>15, 'value'=>$settings)
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false,
			'width'     => $_POST['ITwidth'],
			'height'    => $_POST['ITheight'],
			'maxlength' => $_POST['ITmaxlength'],
			'settings'  => $_POST['ITsettings']
		);

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		global $PHprefs;

		switch($what) {
			case 'ITsettings':
				$help =<<<EOH
<h6>'charset' => 'iso-8859-1'</h6>
Define the characterset of the document in the editor. If omitted, the editor
will use the same characterset as the document it is in (\$PHprefs['charset']).

<h6>'page' => [true|false]</h6>
<tt>false</tt> if you want to edit part of a HTML-page,
<tt>true</tt> if you want to edit a complete HTML-page, including
&lt;HTML&gt; and &lt;/HTML&gt; tags.

<h6>'tables' => [true|false]</h6>
Whether or not the buttons for operations on HTML-tables are present
in the buttonbar.

<h6>'forms' => [true|false]</h6>
Whether or not the buttons for operations on HTML-forms are present
in the buttonbar.

<h6>'borders' => [true|false]</h6>
Whether or not to show table and image borders at initialization.

<h6>'tidy' => [html|xhtml]</h6>
If you want the code generated by the editor to be <a href="http://tidy.sourceforge.net" target="_blank">tidied</a>,
use this setting. Set it to <tt>html</tt> for HTML code, use <tt>xhtml</tt> to generate valid XHTML.

<h6>'useCSS' => [true|false]</h6>
Tell the editor to use 'CLASS'-attributes in the dialog-windows for Links, Images, Tables, FORM elements etc.
Caution: When 'useCSS' is enabled, the Font Properties button is unavailable.

<h6>'css' => '/css/default.css'</h6>
Optionally set a stylesheet the editor should use.
(Per default no stylesheet is used).

<h6>'classes' => array('class1', 'class2', ...)</h6>
If you want to limit the user in the CSS-classes he can enter, you can provide the editor with a set of classes
to choose from. Instead of a TEXT-input box, a pulldown list will be shown in the dialog windows. If any classes
are defined, a Class-pulldown menu will appear in the editor itself as well.<br />
Caution: In order to use this feature, useCSS needs to be enabled as well!

<h6>'imgDir' => '/IMAGES'</h6>
If you want to allow for insertion of images in the editor, use this property. Set it to the path to the
directory the &quot;Image Properties&quot; dialog should start browsing in. This directory is relative to the <b>\$PHprefs['uploadDir']</b> directory.<br />
You can use the variables {\$USER_ID} and {\$GROUP_ID} for personal directories.

<h6>'attachDir' => '/FILES'</h6>
If you want to allow for attaching document in the editor, use this property. Set it to the path to the
directory the &quot;Attachment Properties&quot; dialog should start browsing in. This directory is relative to the <b>\$PHprefs['uploadDir']</b> directory.<br />
You can use the variables {\$USER_ID} and {\$GROUP_ID} for personal directories.

<h6>'upload' => [true|false]</h6>
Per default, the image- and attachment dialogs windows allow for uploading of new document.
Set this to <tt>false</tt>, if you don't allow uploads.

<h6>'target' => '_blank'</h6>
Set the default target for hyperlinks. Per default, no target will be set for hyperlinks.

<h6>'showSize' => [true|false]</h6>
Whether or not to show a document-size-indicator. It shows the 'weight' of the document, as you type.

<h6>'disabled' => array('preview','spell')</h6>
If you want to disable certain features in the editor, you can use this setting. Fill the array with
the names of the buttons to disable. Look <a href="http://editor.phoundry.com/docs/gui_manual.php" target="_blank">here</a>
for the names to use.

<h6><pre>
'extensions' => array(
   array('jsfn'=>'BoldItalic',
         'icon'=>'/pics/bold_italic.gif',
         'alt'=>'Bold and Italics',
         'selection'=>true)
);
</pre></h6>
If this editor needs custom functionality, add an extension.

EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		//return str_replace(array("\r", "\n"), array('', '\\n'), $help);
		return $help;
	}

}

?>
