<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/Inputtypes/ITXmultipleselect.php";

class ITXduallist extends ITXmultipleselect
{
	public $query, $help, $queryFields;

	public function __construct(&$column) {
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);
		}
	}

	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;
		$options_sel = $options_all = array();
		$alt = $this->column->getJScheckAltTag();
		$selected = $this->column->datatype->getSelected($value);
		$sql = PH::evaluate($this->extra['query']);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) 
		{
			$value = $db->FetchResult($cur,$x,$this->extra['optionvalue']);
			$text = '';
			foreach($this->extra['optiontext'] as $t)
			{
				$text .= PH::langWord($db->FetchResult($cur,$x,$t)) . ' ';
			}
			$text = trim($text);
			if (in_array($value, $selected)) 
			{
				$options_sel[] = array($value, $text);
			}
			else
			{
				$options_all[] = array($value, $text);
			}
		}
		$html  = '<input type="hidden" alt="' . $alt . '" name="' . $this->name . '" value="' . implode('|', $selected) . '" />';
		$html .= '<table cellspacing="1" cellpadding="1" class="ITXduallist">';
		$html .= '<tr><th><b>' . word(51) . ':</b></th><td></td><th><b>' . word(50) . ':</b></th></tr>';
		$html .= '<tr>';
		$html .= '<td><select multiple="multiple" size="' . $this->extra['size'] . '" name="all_' . $this->name . '" style="min-width:160px">';
		foreach($options_all as $valuetext) {
			$html .= '<option value="' . PH::htmlspecialchars($valuetext[0]) . '">' . PH::htmlspecialchars(PH::langWord($valuetext[1])) . '</option>';
		}
		$html .= '</select></td>';
		$html .= '<td><input class="moveRight" type="button" onclick="moveTo(\'' . $this->name . '\',\'right\')" value="&gt;&gt;" /><br /><input class="moveLeft" type="button" onclick="moveTo(\'' . $this->name . '\',\'left\')" value="&lt;&lt;" /></td>';
		$html .= '<td><select multiple="multiple" size="' . $this->extra['size'] . '" name="dsp_' . $this->name . '" style="min-width:160px">';
		foreach($options_sel as $valuetext) {
			$html .= '<option selected="selected" value="' . PH::htmlspecialchars($valuetext[0]) . '">' . PH::htmlspecialchars(PH::langWord($valuetext[1])) . '</option>';
		}
		$html .= '</select></td>';
		$html .= '</tr><tr>';
		$html .= '<td align="center"><a href="#" onclick="selectAll(_D.forms[0][\'all_' . $this->name . '\']);return false">' . word(30) . '</a></td>';
		$html .= '<td></td>';
		$html .= '<td align="center"><a href="#" onclick="selectAll(_D.forms[0][\'dsp_' . $this->name . '\']);return false">' . word(30) . '</a></td>';
		$html .= '</tr></table>';
		return $html;
	}

	public function getHelperLinks($page) 
	{
		global $db, $PHSes, $PHprefs;

		$helpers = array();
		$mayAdd = false;

		if (empty($this->extra['addTable']))
			$mayAdd = false;
		elseif ($PHSes->groupId == -1)
			$mayAdd = true;
		else {
			// Make sure current user in current role may insert into $extra['addTable'];
			$sql = "SELECT rights FROM phoundry_group_table WHERE table_id = " . (int)$this->extra['addTable'] . " AND group_id = {$PHSes->groupId}";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur) && strpos($db->FetchResult($cur,0,'rights'), 'i') !== false)
				$mayAdd = true;
		}

		if ($mayAdd) {
			 $url = $PHprefs['url'] . '/core/insert.php?' . QS(1,"TID={$this->extra['addTable']}&CID={$this->column->id}") . '&amp;noFrames&amp;callback=parent.newOption';
			$helpers[] = '<a href="' . $url . '" class="popupWin" title="' . ucfirst(word(6)) . '|600|400|0" onclick="callbackEl=_D.forms[0][\'all_' . $this->name . '\']">' . strtolower(word(6)) . '</a>';
		}
		return $helpers;
	}

}
