<?php
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITXtags extends Inputtype
{
	
	public function getValue($page, $value, $keyvalue = null)
	{
		switch($page) {
			case 'search':
			case 'view':
			case 'export':
				$options = $this->column->datatype->getSelected($value);
				return implode(', ', $options);

			default:
				return $value;
		}
	}
	
	/**
	 * Stolen from IThidden->getHTML
	 */
	public function getHtml($page, $value = '', $readOnly = false, &$includes)
	{
		if ($page == 'view' || $page == 'pdf') {
			if ($value === '' || is_null($value))
				$value = '&nbsp;';
			$html  = '<fieldset class="field"><legend><b>' . PH::htmlspecialchars($this->column->description) . '</b></legend>';
			$html .= $value;
			$html .= "\n</fieldset>\n\n";
			return $html;
		}
		else
			return $this->getInputHtml($page, PH::evaluate($value), $includes);
	}
	
	public function getInputHtml($page, $value = '', &$includes)
	{
		global $db, $PHprefs;
		$selected = $this->column->datatype->getSelected($value);

		$html  = '<fieldset class="field"><legend><b>' . PH::htmlspecialchars($this->column->description) . '</b></legend>';
		$html .= '<input class="wide tags_input" type="text" name="' . $this->name. '" ';
		$html .= 'value="' . ($selected ? implode(", ",$selected) : '') . '" size="60" />'."\n";
		$html .= "\n</fieldset>\n\n";

		return $html;

	}	
	
}