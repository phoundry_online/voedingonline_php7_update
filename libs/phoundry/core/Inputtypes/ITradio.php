<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/Inputtype.php";

class ITradio extends Inputtype
{
	public $value, $query, $help, $queryFields, $recordQuery, $editQuery;
	public $updatable = true, $togglable = false;

	public function __construct(&$column) 
	{
		// Call parent constructor:
		if (!is_null($column)) {
			parent::__construct($column);

			if (!empty($this->extra)) {
				if (isset($this->extra['query'])) {
					$query = $this->recordQuery = $this->editQuery = $this->extra['query'];

					// Substitute php code injection with #number construction
					if (preg_match_all('/<\?(?:php)?(.+)\?>/Us', $query, $matches)) {
						foreach($matches[0] as $key=>$match) {
							$query = str_replace($match, '#'.$key, $query);
						}
					}

					if (preg_match('/^(.*)\[[^\]]+\](.*)$/s', $query, $reg)) {
						// If the query contains [...] (optional WHERE part), split
						// the query into two variants: one with the WHERE part and
						// one without it.

						$this->recordQuery = str_replace(array('[',']'), array('',''), $query);
						$this->editQuery   = $reg[1] . $reg[2];
						if (isset($matches)) {
							// Substitute #number construction with original php code injection
							foreach($matches[0] as $key=>$match) {
								$this->recordQuery = str_replace('#'.$key, $match, $this->recordQuery);
								$this->editQuery   = str_replace('#'.$key, $match, $this->editQuery);
							}
						}
					}
				}
				
				if (isset($this->extra['updatable'])) {
					$this->updatable = $this->extra['updatable'];
				}

				if (isset($this->extra['togglable'])) {
					$this->togglable = $this->extra['togglable'];
				}
			}
		}
	}

    public function getValue($page, $value, $keyval = null)
    {
        global $db;

        switch($page) {
            case 'export':
                $html = '';
                $sql = PH::evaluate($this->recordQuery);
                if (preg_match('/^(SELECT|SHOW)/i', $sql)) {
                    // Database query:
                    $cur = $db->Query($sql)
                    or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
                    for ($x = 0; !$db->EndOfResult($cur); $x++) {
                        if ($db->FetchResult($cur,$x,$this->extra['optionvalue']) == $value) {
                            foreach($this->extra['searchtext'] as $text)
                                $html .= PH::langWord($db->FetchResult($cur,$x,$text)) . ' ';
                            break;
                        }
                        $html = trim($html);
                    }
                }
                else {
                    // Pipe-separated option=value pairs:
                    $pairs = explode('|', $this->recordQuery);
                    foreach($pairs as $pair) {
                        $pair = explode('=', $pair, 2);
                        if ($pair[0] == $value) {
                            if (strpos($pair[1], 'check}') !== false) {
                                $html .= $pair[0];
                            }
                            else {
                                $html .= PH::langWord($pair[1]);
                            }
                            break;
                        }
                    }
                }
                return $html;

            case 'view':
            case 'search':
                $html = '';
                $sql = PH::evaluate($this->recordQuery);
                if (preg_match('/^(SELECT|SHOW)/i', $sql)) {
                    // Database query:
                    $cur = $db->Query($sql)
                    or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
                    for ($x = 0; !$db->EndOfResult($cur); $x++) {
                        if ($db->FetchResult($cur,$x,$this->extra['optionvalue']) == $value) {
                            foreach($this->extra['searchtext'] as $text)
                                $html .= PH::langWord($db->FetchResult($cur,$x,$text)) . ' ';
                            break;
                        }
                        $html = trim($html);
                    }
                }
                else {
                    // Pipe-separated option=value pairs:
                    $pairs = explode('|', $sql);
                    foreach($pairs as $pair) {
                        $pair = explode('=', $pair, 2);
                        if ($pair[0] == $value) {
                            if (count($pairs) == 2 && $page == 'search' && $this->updatable && $this->togglable) {
                                $html .= '<a href="#" class="swapRadio cid' . $this->column->id . '">' . $pair[1] . '</a>';
                            }
                            else {
                                $html .= PH::langWord($pair[1]);
                            }
                            break;
                        }
                    }
                }
                return $html;

            default:
                return $value;
        }
    }

	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @param $includes(array) A reference to an array with this structure:
	 *                         'jsCode'=>array(),
	 *                         'jsFiles'=>array(),
	 *									'cssFiles'=>array()
	 *									Add entries to those arrays in order to have them
	 *									included in the <HEAD> of the edit-page.
	 * @return HTML-code representing this inputtype.
	 * @returns string
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		global $db;

		// Get default value:
		$value = parent::getInputHtml($page, $value, $includes);

		if (!$this->updatable && $page == 'update') {
			$html = '<input name="' . $this->name . '" type="hidden" value="' . $value . '" /><b>' . $this->getValue('view', $value) . '</b>';
		}
		else {
			$alt = $this->column->getJScheckAltTag();
			$html = '';
			$sql = PH::evaluate($this->editQuery);
			if (preg_match('/^(SELECT|SHOW)/i', $sql)) {
				// Database query:
				$cur = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				for ($x = 0; !$db->EndOfResult($cur); $x++) {
					$id = "{$this->name}_{$x}";
					$checked = ($db->FetchResult($cur,$x,$this->extra['optionvalue']) == $value) ? ' checked="checked"' : '';
					$html .= '<input id="' . $id . '" ';
					if ($x == 0)
						$html .= 'alt="' . $alt . '" ';
					$html .= 'type="radio" name="' . $this->name .'" value="' . $db->FetchResult($cur,$x,$this->extra['optionvalue']) . '"' . $checked . '/><label for="' . $id . '">';
					foreach($this->extra['optiontext'] as $text) {
						$html .= PH::htmlspecialchars(PH::langWord($db->FetchResult($cur,$x,$text))) . ' ';
					}
					$html .= '</label>';
				}
			}
			else {
				// Pipe-separated option=value pairs:
				$pairs = explode('|', $sql);
				$x = 0;
				foreach($pairs as $pair) {
					$id = "{$this->name}_{$x}";
					$pair = explode('=', $pair, 2);
					$checked = ($pair[0] == $value) ? ' checked="checked"' : '';
					$html .= '<input id="' . $id . '" ';
					if ($x == 0)
						$html .= 'alt="' . $alt . '" ';
					$html .= 'type="radio" name="' . $this->name . '" value="' . PH::htmlspecialchars($pair[0]) . '"' . $checked . ' />' . '<label for="' . $id . '">' . PH::langWord($pair[1]) . '</label> ';
					$x++;
				}
			}
		}
		return $html;
	}

	public function getExtra()
	{
		$query = null;
		$queryFields = null;
		$updatable = true;
		$togglable = false;
		if (count($this->extra)) {
			if (isset($this->extra['query']))
				$query = $this->extra['query'];
			if (isset($this->extra['optionvalue'])) {
				$queryFields = array(
					'optionvalue' => $this->extra['optionvalue'],
					'optiontext'  => $this->extra['optiontext'],
					'searchtext'  => $this->extra['searchtext']
				);
			}
			if (isset($this->extra['updatable'])) {
				$updatable = $this->extra['updatable'];
			}
			if (isset($this->extra['togglable'])) {
				$togglable = $this->extra['togglable'];
			}
		}
		$adminInfo = array();
		$updatable = $updatable ? 1 : 0;
		$adminInfo['updatable'] = array(
			'name'     => 'ITupdatable',
			'label'    => 'Updatable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be updated as well? (Insertion is always allowed)',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$updatable)
		);
		$adminInfo['togglable'] = array(
			'name'     => 'ITtogglable',
			'label'    => 'Togglable?',
			'required' => true,
			'syntax'   => 'integer',
			'info'     => 'Can this field be toggled in the record overview?',
			'input'    => array('type'=>'select', 'options'=>array(1=>'yes',0=>'no'), 'selected'=>$togglable)
		);
		$adminInfo['query'] = array(
			'name'     => 'ITquery',
			'label'    => 'Options-query',
			'required' => true,
			'syntax'   => 'text',
			'info'     => 'Enter the query for the different radio-buttons (<a href="#" onclick="makePopup(null,300,200,\'Select Query\',\'selectQuery.php?sql=\'+escape(_D.forms[0][\'ITquery\'].value));return false">edit</a>)',
			'help'     => true,
			'input'    => array('type'=>'textarea', 'cols'=>80, 'rows'=>4, 'value'=>$query)
		);
		$adminInfo['queryFields'] = array(
			'name'     => 'ITqueryFields',
			//'required' => true,
			//'syntax'   => 'text',
			'input'    => array('type'=>'hidden', 'value'=>PH::php2javascript($queryFields))
		);

		return $adminInfo;
	}

	public function setExtra(&$msg) 
	{
		$msg = '';

		$query = trim($_POST['ITquery']);
		if (preg_match('/^(SELECT|SHOW)/i', $query)) {
			if (empty($_POST['ITqueryFields']) || $_POST['ITqueryFields'] == '{}') {
				$msg = 'Option value, option text and search text are not defined!';
				return false;
			}
		}

		$struct = PH::javascript2php($_POST['ITqueryFields']);
		$extra = array(
			'query'     => $query,
			'updatable' => ($_POST['ITupdatable'] == 1) ? true : false,
			'togglable' => ($_POST['ITtogglable'] == 1) ? true : false
		);
		if (isset($struct['optionvalue']))
			$extra['optionvalue'] = (string)$struct['optionvalue'];
		if (isset($struct['optiontext']))
			$extra['optiontext'] = $struct['optiontext'];
		if (isset($struct['searchtext']))
			$extra['searchtext'] = $struct['searchtext'];

		return $extra;
	}

	public function getExtraHelp($what) 
	{
		switch($what) {
			case 'ITquery':
				$help =<<<EOH
<h2>name=value pairs</h2>
<pre>
green=I like green|red=I like red|blue=I like blue
</pre>
<p>
In this case, the OPTIONs don't come from the database, but they are predefined.
The syntax is <tt>value=name</tt>, the OPTIONs are separated by a pipe. The
<tt>value</tt> is the VALUE for the OPTION, everything on the right of
the =-sign will be showed to the user.
</p>

<h2>Regular SELECT query</h2>
<pre>
SELECT id, fields FROM table
  [WHERE id = {\$RECORD_ID}] ORDER BY name
</pre>
<p>
The WHERE-part (between square brackets) is used for identifying the current
record (on the search-page and the update-page). The brackets are required!
The {\$RECORD_ID} variable will be filled with the correct value by Phoundry.
</p>
<h2>Variables</h2>
<table>
<tr>
	<th>Variable</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td>{\$USER_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$USER_NAME}</td>
	<td>The full name (field <tt>name</tt> in table <tt>phoundry_user</tt>) of the current user.</td>
</tr>
<tr class="odd">
	<td>{\$GROUP_ID}</td>
	<td>The database-id (field <tt>id</tt> in table <tt>phoundry_group</tt>) of the current user.</td>
</tr>
<tr class="even">
	<td>{\$check}</td>
	<td>A checkmark (<img src="../../core/icons/tick.png" alt="Yes" width="16" height="16" />)</td>
</tr>
<tr class="odd">
	<td>{\$nocheck}</td>
	<td>A no-checkmark (<img src="../../core/icons/cross.png" alt="No" width="16" height="16" />)</td>
</tr>
<tr class="even">
	<td>{\$word(nr)}</td>
	<td>A word from the language file with index 'nr'.</td>
</tr>
</table>
<p>
Any other variable may be used as well. In that case, Phoundry will look in the
\$_REQUEST array: if a value for the variable is found there, it will be
substituted.
</p>
EOH;
			break;
		default:
			$help = 'No help found for ' . $what . '!';
			break;
		}
		return $help;
	}
}
