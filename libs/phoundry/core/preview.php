<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'view');
	$RID = $_GET['RID'];

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);
	
	// If versioning is enabled load the corresponding version
	if (!empty($phoundry->extra['versioning'])) {
		$service = $phoundry->getVersioningService();
		if (!isset($_GET['_versioning']['parent_id'])) {
			$version = $service->getLatestVersion(PH::getTableStringIdById($_GET['TID']), $RID);
			if ($version) {
				$_REQUEST['_versioning']['parent_id'] = $version->id;
				$_GET['_versioning']['parent_id'] = $version->id;
				$phoundry->loadVersion($version);
			}
		}
		else {
			$version = $service->getVersion($_GET['_versioning']['parent_id']);
			if ($version && $version->record_id === $RID) {
				$phoundry->loadVersion($version);
			}
		}
	}
	
	$pageTitle = $phoundry->getPageHeader('view', word(9));

	// Assemble page contents:
	$page = $phoundry->viewPage($RID);

	PH::getHeader('text/html');
?>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="<?= preg_replace('/_\d+$/', '', $phoundry->name) ?>.view" />
<title><?= $pageTitle['title'] ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">
//<![CDATA[

var TID = <?= $TID ?>,
	RID = '<?= PH::htmlspecialchars($_GET['RID']) ?>';

function exportData(what) {
	switch(what) {
		case 'pdf':
			makePopup(null,300,170,'<?= escSquote(word(74)) ?>', '<?= $PHprefs['url'] ?>/core/popups/export_pdf.php?<?= QS(0) ?>');
			break;
	}
}

function doRecord(action, url) {
	url = url + '?<?= QS(0) ?>';
	if (action == 'delete') {
		if (confirm('<?= escSquote(word(140 /* Do you want to delete this record? */)) ?>')) {
			$.post(url, {'RID':RID}, function(json){if(!json.success){alertError(json.response);}else{_D.location='<?= $phoundry->rUrl . '?' . QS(1,'RID') ?>';}});
		}
	}
	else if (action == 'edit') {
		var lock = hasLock(RID);
		if (lock == false || confirm(lock)) {
			_D.location=url;
		}
	}
	else {
		_D.location=url;
	}
}

<?if (!empty($phoundry->extra['versioning'])):?>
jQuery(function($){
	$("#versioning").bind("change", function(){
		window.location = $(this).val();
	});

	var versioning_popup;
	$(".version_message").bind("click", function(e) {
		if (versioning_popup) {
			versioning_popup.close();
			versioning_popup = null;
		}
		else {
			versioning_popup = $.inlineWindow({
				event: e,
				content: this.title
			});
		}
	});
});
<?endif?>
//]]>
</script>
</head>
<body class="frames">
<form>
<?php
	$showFields = array();
	foreach($phoundry->cols as $col) {
		if (isset($col->rights['view'])) {
			$showFields[] = $col->name;
		}
	}
?>
<input type="hidden" name="PHordr" value="<?= $phoundry->key ?> ASC" />
<input type="hidden" name="PHshow" value="<?= implode(',', $showFields) ?>" />
<input type="hidden" name="_id[]" value="<?= PH::htmlspecialchars($RID) ?>" />
</form>
<div id="headerFrame" class="headerFrame">
<?= getHeader($pageTitle['title'], $pageTitle['extra'], $pageTitle['filters']); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
	<?php if (isset($phoundry->rights['update'])) { ?>
	<!-- Edit button -->
	<input type="button" value="<?= word(7) ?>" onclick="doRecord('edit','<?= $phoundry->uUrl ?>')" />
	<? } ?>
	
	<?php if (isset($phoundry->rights['delete'])) { ?>
	<!-- Delete button -->
	<input type="button" value="<?= word(8) ?>" onclick="doRecord('delete','<?= $phoundry->dUrl ?>')" />
	<? } ?>
	
	<?php if (!empty($phoundry->extra['versioning'])) {?>
	<!-- Trail button -->
	<input type="button" value="<?=ucfirst(word(364 /* versies */))?>" onclick="doRecord('trail','<?=$PHprefs['url']?>/core/versioning/trail.php')" />
	<?php } ?>

	<!-- Print button -->
	<input type="button" value="<?= word(164) ?>" onclick="_W.print()" />

	<?php if (isset($phoundry->rights['export']) && isset($PHprefs['paths']['htmldoc'])) { ?>
	<!-- Export PDF button -->
	<input type="button" value="<?= word(74) ?>" onclick="exportData('pdf')" />
	<? } ?>

</td>
<td align="right">
	<?if (isset($_GET['_versioning']['parent_id']) && isset($service)):
		$ver = $service->getVersion($_GET['_versioning']['parent_id']);
		$live = $service->getLiveVersion($ver->table_id, $ver->record_id);
		$versions = $service->getVersionsSelect($ver->trail_id);
	?>
	<span title="<?=htmlspecialchars($ver->message)?>" class="version_message ptr" style="display: inline-block;">
		<?=htmlspecialchars(PH::abbr($ver->message,100))?>
	</span>
	<select id="versioning">
		<?foreach ($versions as $version):
		$classes = array();
		if ($version['user_id'] == $PHSes->userId) {
			$classes[] = 'versioning-own';
		}
		if ($version['id'] == $live->id) {
			$classes[] = 'versioning-published';
		}
		$selected = ($version['id'] == $_GET['_versioning']['parent_id']) ? ' selected="selected"' : '';
		?>
		<option value="<?=htmlspecialchars($phoundry->vUrl)?>?<?=QS(true, "_versioning[parent_id]=".$version['id'])?>" class="<?=htmlspecialchars(implode(" ", $classes))?>" <?=$selected?>>
			<?=$version['revision']?> <?=$version['name']?> <?=$version['date']?> (<?=$version['status']?>)
		</option>
		<?endforeach?>
	</select>
	<?endif?>
	<?php
		$shownRIDs = isset($PHSes->shownRIDs[$TID]) ? $PHSes->shownRIDs[$TID] : array();
		if (($key = array_search($RID, $shownRIDs)) !== false) {
			if(isset($shownRIDs[$key-1])) {
				print '<input type="button" value="&lt;&lt;" onclick="_D.location=\'' . $phoundry->vUrl . '?' . QS(1,'RID=' . urlencode($shownRIDs[$key-1])) . '\'" />';
			}
			else {
				print '<input type="button" value="&lt;&lt;" disabled="disabled" />';
			}
			if (isset($shownRIDs[$key+1])) {
				print '<input type="button" value="&gt;&gt;" onclick="_D.location=\'' . $phoundry->vUrl . '?' . QS(1,'RID=' . urlencode($shownRIDs[$key+1])) . '\'" />';
			}
			else {
				print '<input type="button" value="&gt;&gt;" disabled="disabled" />';
			}
		}
	?>
	<?php include $PHprefs['distDir'] .'/layout/elements/manual_button.php' ?>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<?= $page['html'] ?>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<?php include 'cancelBut.php';?>
	</td>
</tr></table>
</div>

</body>
</html>
