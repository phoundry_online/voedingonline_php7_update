<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	checkAccess();

	// Generate URL-parameters for manual viewing:
	$url = $_GET['url'];
	$ts = time();
	$ip = PH::getRemoteAddr();

	$qs = array();
	$qs[] = 'lang=' . $PHSes->lang;
	$qs[] = 'ts=' . $ts;
	$qs[] = 'ip=' . $ip;
	$qs[] = 'Z='  . md5('Man' . $ts . $ip . 'uaL');

	$sep = (strpos($url, '?') === false) ? '?' : '&';

	print $url . $sep . implode('&', $qs);
