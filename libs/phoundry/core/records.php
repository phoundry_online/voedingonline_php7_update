<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Is the current table a shortcut?
	$isShortcut = PH::isShortcut($TID, $PHSes->userId);

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);

	// Has the record-limit been exceeded?
	$allowInsert = $phoundry->insertAllowed('insert');
	$allowCopy   = $phoundry->insertAllowed('copy');

	$request = $phoundry->get2struct($_GET);
	
	$_ands = array();
	if (!empty($_GET['PHlimitRIDs']) && !empty($PHSes->limitRIDs[$_GET['PHlimitRIDs']])) {
		$rids = $PHSes->limitRIDs[$_GET['PHlimitRIDs']];
		if ($phoundry->keyType == 'integer') {
			$_set = $rids;
		} else {
			$_set = array();
			foreach($rids as $_key) {
				$_set[] = "'" . escDBquote($_key) . "'";
			}
		}
		$_ands[] = "{$phoundry->key} IN (" . implode(',',$_set) . ')';
	}
	
	$query = $phoundry->makeQuery($request, $_ands);
	$pageTitle = $phoundry->getPageHeader('records');
	$jsCode = $phoundry->getJScode('search');
	$page = $phoundry->recordsPage($request, $query, $nrPages);
	$notes = PH::getNotes($TID, $PHSes->userId);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="<?= preg_replace('/_\d+$/', '', $phoundry->name) ?>.records" />
<title><?= $pageTitle['title'] ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">

var RID = null;

<?php if (isset($_GET['_reloadMenu'])) { ?>
try{parent.frames['PHmenu'].location.reload()} catch(e) {}
<?php } ?>

// Start javascript code for columns:
<?= $jsCode; ?>
// End javascript code for columns

function setRow(row) {
	var row_id = row.id.replace(/^row/, ''), idx = row_id.indexOf('_');
	rowIndex = parseInt(row_id.substring(0,idx),10);
	RID = row_id.substring(idx+1,row_id.length);
	window.status = RID;
	if (activeRow)
		activeRow.elem.className = activeRow.className;
	activeRow = {elem:row, className:row.className};
	row.className = 'active';
	setButtons(true);
}

function editRow(row) {
	<?php if (isset($phoundry->rights['update'])) { ?>
	if (!$('#editBut').prop('disabled'))
		doRecord('edit','<?= $phoundry->uUrl ?>');
	<?php } ?>
}

function setButtons(state) {
	var buts = ['view','edit','trail','del'<?= $allowCopy ? ", 'copy'" : '' ?>];
	$.each(buts, function(x){$('#'+buts[x]+'But').prop('disabled', !state)});
}

var activeRow = null, rowIndex = -1;

function doRecord(action, url, shiftKey) 
{
	var F = $('form')[0], x, qs = '<?= QS(0) ?>'.replace(/([\?&])PHfltr_(\d+)=([^&$]+)/g,''), cbs = F['_id[]'], delIds = [];
	if (shiftKey) qs += '&shift=1';

	if (cbs)
	{
		if (cbs.length) 
		{
			for (x = 0; x < cbs.length; x++) 
			{
				if (cbs[x].checked) { delIds.push(cbs[x].value); }
			}
		}
		else if (cbs.checked) 
		{
			delIds[0] = cbs.value;
		}
	}

	if (!RID && (action == 'copy' || action == 'view' || action == 'edit' || action == 'trail' || (action == 'delete' && delIds.length == 0)))
	{
		return;
	}

	if (action == 'delete') 
	{
		if ((activeRow == null && delIds.length) || (activeRow && delIds.length && confirm('<?= escSquote(str_replace("\n",'\n',word(181))) ?>'))) 
		{// Delete all checked records:
			if (delIds.length > 1000)
			{
				alert('<?= escSquote(word(440, '1000' /* can't delete more than 1000 records at once */)) ?>');
				return;
			}
			if (confirm('<?= escSquote(word(121)) ?>'))
			 {// Remove checked records?
				var lock = hasLock(delIds.join('|'));
				if (lock == false || confirm(lock))
				{// POST, not GET
					//$.post('<?= $phoundry->daUrl ?>?'+qs, {'RID':delIds.join('|')}, function(json){if(!json.success){alertError(json.response);}else{_D.location.reload(true);}});
                    $.post('<?= $phoundry->daUrl ?>?'+qs, {'RID':delIds.join('|')}, function(){_D.location.reload(true);});
				}
			}
			return;
		}
	}

	if (RID != null && action != 'insert' && action != 'delete') { qs += '&RID=' + RID; }
	for (x = 0; x < F.elements.length; x++) 
	{
		el = F.elements[x];
		if (/^PHfltr_/.test(el.name) && el.value != '') {
			qs += '&' + el.name + '=' + escape(el.value);
		}
	}
	var sep = url.indexOf('?') == -1 ? '?' : '&';
	url = url + sep + qs;

	if (action == 'delete' || action == 'edit') 
	{
		if (action == 'edit' || confirm('<?= escSquote(word(140)) ?>')) 	
		{
			var lock = hasLock(RID);
			if (lock == false || confirm(lock)) 
			{
				if (action == 'delete')
				{// POST, not GET
					//$.post(url, {'RID':RID}, function(json){if(!json.success){console.log(json);alertError(json.response);}else{_D.location.reload(true);}});
                    $.post(url, {'RID':RID}, function(){_D.location.reload(true);});
				}
				else 
				{
					_D.location = url; 
				}
			}
		}
	}
	else 
	{
		if (shiftKey) 
		{
			window.open(url,'Preview','scrollbars=no,status=no');
		}
		else 
		{
			_D.location = url;
		}
	}
}

$(document).keydown(function(evt){
	var kc = evt.keyCode, tbl = $('#recordTBL')[0], nrr = tbl.rows.length;
	if (evt.target.tagName=='SELECT') return;
	switch(kc) {
		case 38: // up
			rowIndex--;
			if (rowIndex < 0) rowIndex = nrr-2;
			break;
		case 40: // down
			rowIndex++;
			if (rowIndex > nrr-2) rowIndex = 0;
			break;
		<?php if (isset($phoundry->rights['view'])) { ?>
		case 32: // spacebar
			if (!$('#viewBut').prop('disabled'))
				doRecord('view','<?= $phoundry->vUrl ?>');
			return false;
		<?php } ?>
		<?php if (isset($phoundry->rights['update'])) { ?>
		case 13: // enter
			if (!$('#editBut').prop('disabled'))
				doRecord('edit','<?= $phoundry->uUrl ?>');
			return false;
		<?php } ?>
		<?php if (isset($phoundry->rights['delete'])) { ?>
		case 46: // delete
			if (!$('#delBut').prop('disabled'))
				doRecord('delete','<?= $phoundry->dUrl ?>');
			return false;
		<?php } ?>
		<?php if ($allowInsert) { ?>
		case 107: case 187: // +
			if (!$('#addBut').prop('disabled'))
				doRecord('insert', '<?= $phoundry->iUrl ?>');
			return false;
		<?php } ?>
		default:
			return;
	}
	if (nrr > 1 && (kc == 38 || kc == 40)) {
		setRow(tbl.rows[rowIndex+1]);
		tbl.rows[rowIndex].scrollIntoView();
		return false;
	}
});

$(function(){
	try {parent.frames['PHmenu'].setGroupSelect(true)} catch(e) {}
	restoreScrollPos();
	setTimeout('initNotes()',3000);

	$('.maxedOut:disabled').each(function() {
		var el = $(this), offset = el.offset();
		$('#headerFrame').append('<div style="position:absolute;top:'+offset.top+'px;left:'+offset.left+'px;width:'+el.outerWidth()+'px;height:'+el.outerHeight()+'px" title="' + el.attr('title') + '"></div>');
	});
});

$(window).unload(storeScrollPos);

</script>
</head>
<body class="frames">
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="get" onsubmit="return false">
<?php
	foreach($_GET as $name=>$value) {
		if ($name[0] == '_' || substr($name,0,2) == 'PH' || $name == 'RID' || $value === '')
			continue;
		print '<input type="hidden" name="' . $name . '" value="' . PH::htmlspecialchars($value) . '" />' . "\n";
	}
?>
<div id="headerFrame" class="headerFrame">
<?= getHeader($pageTitle['title'], $pageTitle['extra'], $pageTitle['filters']); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
	<?php if (isset($phoundry->rights['insert'])) { ?>
	<!-- Insert button -->
	<?php
		if ($allowInsert)
			print '<input type="button" id="addBut" value="' . word(6) . '" onclick="doRecord(\'insert\',\'' . $phoundry->iUrl . '\',event.shiftKey)" />';
		else
			print '<input type="button" id="addBut" class="maxedOut" value="' . word(6) . '" disabled="disabled" title="' . ($phoundry->name == 'phoundry_group' ? word(185) : word(33, $phoundry->maxRecords, $phoundry->description)) . '" />';
	?>
	<img id="div-lt1" src="<?= $PHprefs['url'] ?>/core/pics/div.gif" alt="|" class="div" />
	<?php } ?>

	<?php if (!empty($phoundry->vUrl) && isset($phoundry->rights['view'])) { ?>
	<!-- Preview button -->
	<input type="button" disabled="disabled" id="viewBut" value="<?= word(9) ?>" onclick="doRecord('view','<?= $phoundry->vUrl ?>',event.shiftKey)" />
	<?php } ?>

	<?php if (isset($phoundry->rights['copy'])) { ?>
	<!-- Copy button -->
	<?php
		if ($allowCopy)
			print '<input type="button" disabled="disabled" id="copyBut" value="' . word(109) . '" onclick="doRecord(\'copy\',\'' .  $phoundry->cUrl . '\',event.shiftKey)" />';
		else
			print '<input type="button" disabled="disabled" id="copyBut" class="maxedOut" value="' . word(109) . '" title="' . word(33, $phoundry->maxRecords, $phoundry->description) . '" />';
	?>
	<?php } ?>

	<?php if (isset($phoundry->rights['update'])) { ?>
	<!-- Edit button -->
	<input type="button" disabled="disabled" id="editBut" value="<?= word(7) ?>" onclick="doRecord('edit','<?= $phoundry->uUrl ?>',event.shiftKey)" />
	<?php } ?>

	<?php if (isset($phoundry->rights['delete'])) { ?>
	<!-- Delete button -->
	<input type="button" disabled="disabled" id="delBut" value="<?= word(8) ?>" onclick="doRecord('delete','<?= $phoundry->dUrl ?>',event.shiftKey)" />
	<?php } ?>
	
	<?php if (!empty($phoundry->extra['versioning'])) {?>
	<!-- Trail button -->
	<input type="button" disabled="disabled" id="trailBut" value="<?=ucfirst(word(364 /* versies */))?>" onclick="doRecord('trail','<?=$PHprefs['url']?>/core/versioning/trail.php',event.shiftKey)" />
	<?php } ?>
	
	<img id="div-lt2" src="<?= $PHprefs['url'] ?>/core/pics/div.gif" alt="|" class="div" />
	<span class="icon ptr shortcut<?if ($isShortcut):?> shortcut-del<?endif?>" onclick="shortCut(this, <?= $TID ?>)" title="<?=word(19)?>"></span>
	<?php if ($PHprefs['nrNotes'] > 0 && $notes['count'] < $PHprefs['nrNotes']) { ?>
	<span class="icon ptr popupWin add-note" title="<?= word(196) ?>|340|330|0|<?= $PHprefs['url'] ?>/core/popups/editNote.php?<?= QS(1) ?>" title="<?= word(196) ?>"></span>
	<?php } ?>

	<?php if (isset($phoundry->extra['timetable'])) { ?>
	<a href="<?= $PHprefs['url'] ?>/core/timeTable/?<?= QS(1) ?>" class="popupWin" title="<?= word(289 /* Timetable */) ?>|-50|-50"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/calendar.png" alt="Timetable" /></a>
	<?php } ?>
	
	<?php if (!empty($phoundry->extra['versioning']) && !empty($PHprefs['enableVersioningWorkflow'])) { 
		$service = $phoundry->getVersioningService();
		$orphans = $service->getOrphanVersionIds($phoundry->id);
	?>
	<a class="icon versioning-orphan-count" href="<?=$PHprefs['url']?>/core/versioning/versions.php?<?=QS(true, 'TID='.PH::getTableIdByStringId("phoundry_versioning").'&table_id='.$phoundry->id)?>" title="Nieuwe versies">
		<img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/timeline_marker.png" alt="Nieuwe versies" />
		<span><?=count($orphans)?></span>
	</a>
	<?if ($PHSes->isAdmin || PH::getAccessRights($TID, array('b'))):
		$on = $phoundry->versioningNotifyStatus();
	?>
	<a class="icon versioning-notify<?=$on?'-off':''?>" href="<?=$PHprefs['url']?>/core/versioning/notifications.php?<?=QS(true)?>" title="<?=$on?'Versie notificaties uitzetten':'Versie notificaties aanzetten'?>">
		<img src="<?=$PHprefs['url']?>/core/icons/<?=$on?'bell_delete':'bell_add'?>.png" alt="<?=$on?'Versie notificaties uitzetten':'Versie notificaties aanzetten'?>" class="icon" />
	</a>
	<?endif?>
	<?php } ?>

</td>
<td align="right" id="navButtons">
	<input type="button" id="zoekBut" value="<?= word(10) ?>" class="popupWin" title="<?= escSquote(word(10)) ?>|340|325|0|<?= $PHprefs['url'] ?>/core/popups/search.php?<?= QS(1) ?>" />
	<img id="div-rt1" src="<?= $PHprefs['url'] ?>/core/pics/div.gif" alt="|" class="div" />
	<input type="button" id="prevBut" onclick="var F=this.form;F['PHpage'].selectedIndex=F['PHpage'].selectedIndex-1;F.submit()" value="&lt;&lt;" />
	<select name="PHpage" onchange="this.form.submit()">
	<?php
		$PHpage = isset($_GET['PHpage']) ? (int)$_GET['PHpage'] : 0;
		$PHpages = array_unique(array_merge(range(0,9), range($PHpage-10, $PHpage+10), range($nrPages-10, $nrPages-1)));
		foreach($PHpages as $p) {
			if ($p < 0 || $p >= $nrPages) continue;
			$selected = ($PHpage == $p) ? ' selected="selected" ' : ' ';
			print '<option' . $selected . 'value="' . $p . '">' . ($p+1) . '</option>';
		}
		$selected = $PHpage == -1 ? ' selected="selected" ' : ' ';
	?>
	<option<?= $selected ?>value="-1"><?= word(163) ?></option>
	</select>
	<input type="button" id="nextBut" onclick="var F=this.form;F['PHpage'].selectedIndex=F['PHpage'].selectedIndex+1;F.submit()" value="&gt;&gt;" />
	<?php include $PHprefs['distDir'] .'/layout/elements/manual_button.php' ?>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?php
	if (!$allowInsert && $phoundry->name == 'phoundry_group') {
		print '<fieldset><legend class="caution"><img class="icon" src="' . $PHprefs['url'] . '/core/icons/exclamation.png" alt="" align="left" />' . word(187) . '</legend><b>' . word(185) . '</b></fieldset><br />';
	}
?>
<?= $page ?>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td width="50%">&nbsp;<b id="recordIndex"></b></td>
	<td align="right" width="50%">
	<?php
		if (isset($phoundry->rights['export'])) {
	?>
	<select id="exportSel" onchange="exportData(this.value);this.selectedIndex=0">
		<option value=""><?= word(141) ?></option>
		<option value="csv">CSV</option>
		<?php
			if (!empty($PHprefs['paths']['htmldoc'])) {
				print '<option value="pdf">PDF</option>';
			}
		?>
	</select>
	&nbsp;
	<?php
		}
		if (isset($phoundry->rights['import'])) {
	?>
	<select onchange="importData(this.value);this.selectedIndex=0">
		<option value=""><?= word(142) ?></option>
		<option value="csv">CSV</option>
	</select>
	<?php } ?>
	<img id="div-rb1" src="<?= $PHprefs['url'] ?>/core/pics/div.gif" alt="|" class="div" />
	<?php include 'cancelBut.php'?>
	</td>
</tr></table>
</div>

</form>

<?php if ($notes['count'] > 0) { ?>
<?= $notes['html'] ?>
<?php } ?>

<?php if (!empty($_GET['PHsrch'])) { ?>
	<div id="blDiv" style="background:url(<?= $PHprefs['customUrls']['pics'] ?>/balloon_<?= $PHSes->lang ?>.png) top left no-repeat">
	<div id="blDiv1"><img src="<?= $PHprefs['url'] ?>/core/pics/pixel.gif" width="88" height="16" border="0" usemap="#balloonMap" /></div>
	<div id="blDiv2"><?= PH::htmlspecialchars($_GET['PHsrch']) ?></div>
	</div>
	<map name="balloonMap">
	<area shape="rect" coords="74,0,88,14" href="#" onclick="$('#blDiv').hide()" />
	<area shape="rect" coords="2,0,55,16" href="#" onclick="document.location=document.location.href.replace(/(&|\?)PHsrch=[^&]+/, '')" />
	</map>
<?php } ?>
</body>
</html>
