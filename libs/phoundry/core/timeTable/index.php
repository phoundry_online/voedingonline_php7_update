<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	require_once 'ttManager.class.php';
	require_once 'ttItem.class.php';
	require_once 'ttRow.class.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'view');

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);

	$TT        = $phoundry->extra['timetable'];
	$dateFormat = str_replace(array('DD','MM','YYYY'),array('d','m','Y'),word(251)); // Date format

	$ts = time();
	if (!empty($_GET['date'])) {
		if (is_numeric($_GET['date'])) {
			$ts = (int)$_GET['date'];
		}
		else {
			$ts = mktime(0,0,0,substr($_GET['date'], strpos($dateFormat,'m'), 2),substr($_GET['date'], strpos($dateFormat,'d'), 2),substr($_GET['date'], strpos($dateFormat,'Y'), 4));
		}
	}

	$ttM = new ttManager($TT['fromField'], $TT['tillField'], $TT['showFields'], $phoundry->name, $phoundry->key, $TT['timespan'], date('Y-m-d 00:00:00', $ts), $TT['where']);
	$ttM->scheduleItems();
	
	// size in pixels for every hour
	$hourSize = 10;

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>TimeTable</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popupcal.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<style type="text/css">

	table.legend tr td {
		background:url(<?= $PHprefs['customUrls']['pics'] ?>/bg_black.gif) repeat-x;	
		border-bottom: 2px solid #ffd045;
	}

	table.legend tr td div {
		border-right:1px solid #fff;
		color: #fff;
		text-align: center;
		height:20px;
	}

	table.timetable, table.legend {
		margin-bottom: 1px;
	}

	table.timetable tr {
		background: #ddd;
	}
	
	table.timetable tr td.none {
		background: #fff;
	}

	table.timetable div.outer { 
		overflow: hidden;
	}

	table.timetable div.inner {
		border-right:1px solid #fff;
		height:50px;
		padding:2px;
	}

	div#over {
		display: none;
		background: url(bg.png) no-repeat;
		position: absolute; 
		width: 243px;
		height: 195px;
		padding:28px 0 0 24px;
	}

	div#over div {
		overflow-y: scroll;
		width: 210px;
		height: 152px
	}

	#recordTBL {
		border-right: 1px solid #9c9c9c;
		border-bottom: 1px solid #9c9c9c;
		border-left: 1px dotted #cfcfcf;
		border-top: 1px dotted #cfcfcf;
	}

</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>&noFrames"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/popupcal.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">
//<![CDATA[

var balloon = false;

$(function() {
	var w, c;
	$('div#over').bind('click', function() {
		$(this).animate({height:'toggle', width:'toggle'});
		balloon = false;
	});
	
	$('table.timetable td:not(.none)').each(function() {
		w = this.style.width; c = $(this).attr('bgcolor');
		$(this)
		.removeAttr('style')
		.html('<div class="outer" style="width:'+w+'"><img src="<?= $PHprefs['url'] ?>/core/pics/pixel.gif" height="1" width="'+w+'" /><div class="inner" style="cursor:pointer;filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='+c+',endColorStr=#eeeeee,gradientType=0)">'+($(this).html())+'</div></div>')
		.bind('click', function(e) {
			var $this = $(this), pos = $this.position();
			$('#over')
			.css({left:$this.width()/2+pos['left'],top:$this.height()/2+pos['top']})
			.find('div').html($('.inner',this).html().replace(/<img[^>]+>/i,''));
			if (!balloon) {
				$('#over').animate({height:'toggle', width:'toggle'});
				balloon = true;
			}
		})
	});

	$('table.timetable td.none').each(function() {
		w = this.style.width;
		$(this)
		.removeAttr('style')
		.html('<div class="outer" style="width:'+w+'"><img src="<?= $PHprefs['url'] ?>/core/pics/pixel.gif" height="1" width="'+w+'" /><div class="inner">'+($(this).html())+'</div></div>')
	});

	// Remove loading overlay:
	$('#loading').remove();
});

//]]>
</script>
</head>
<body>
<form action="#" onsubmit="return false">
<div id="loading" style="width:100%;height:100%;position:absolute;top:0;left:0;background:#fff"></div>
<fieldset style="padding:5px">
<button type="button" onclick="_D.location='?<?= QS(1,'date=' . ($ts - ($TT['timespan']*24*60*60))) ?>';return false">&lt;&lt; &lt;&lt;</button>
<button type="button" onclick="_D.location='?<?= QS(1,'date=' . ($ts - (24*60*60))) ?>';return false">&lt;&lt;</button>
<input dateformat="<?= word(251) ?>" class="popupcal" name="date" type="text" size="10" maxlength="10" value="<?= date($dateFormat, $ts) ?>" onchange="_D.location='?<?= QS(1,'date') ?>&date='+this.value" />
<button type="button" onclick="_D.location='?<?= QS(1,'date=' . ($ts + (24*60*60))) ?>';return false">&gt;&gt;</button>
<button type="button" onclick="_D.location='?<?= QS(1,'date=' . ($ts + ($TT['timespan']*24*60*60))) ?>';return false">&gt;&gt; &gt;&gt;</button>
</fieldset><br />
<table align="center" cellspacing="0" cellpadding="1" id="recordTBL"><tr><td>
<?php
	$colors = array(
		'#cc3300',
		'#ff9933',
		'#ffcc00',
		'#336633',
		'#ffcc99',
		'#99cccc',
		'#0066cc',
		'#cccccc',
		'#99ccff'
	);

	// printen van datum
	print '<table cellspacing="0" cellpadding="0" class="legend"><tr>' . "\n";
	for($x=0;$x < $TT['timespan'];$x++) {
		$datum = date($dateFormat, $ts+($x*24*60*60));
		print '<td style="width:'.(round(24*$hourSize)).'px"><img src="' . $PHprefs['url'] . '/core/pics/pixel.gif" height="1" width="'.(round(24*$hourSize)).'" /><div>'.$datum.'</div></td>';
	}
	print '</tr></table>' . "\n";

	foreach($ttM->getRows() as $key => $row) {
		print '<table cellspacing="0" cellpadding="0" class="timetable"><tr>' . "\n";;
		foreach($row->getItems() as $item) {
			if($item instanceof ttFiller) {
				print '<td class="none" style="width:'.(round($item->width*$hourSize)).'px">&nbsp</td> ' . "\n";
			}
			else
			{
				$color = array_pop($colors);
				array_unshift($colors, $color);
				print '<td bgcolor="'.$color.'" style="width:'.(round($item->width*$hourSize)).'px">';
					print '<a href="' . $phoundry->uUrl . '?' . QS(1, 'RID='.$item->id) . '" target="_parent" onclick="event.cancelBubble=true"><img class="icon" src="' . $PHprefs['url'] . '/core/icons/page_edit.png" align="right" alt="Edit record" /></a>';
					print PH::htmlspecialchars($item->title) . '<br />';
					$start = date($dateFormat . ' H:i', $item->startDate);
					$end   = date($dateFormat . ' H:i', $item->endDate);
					if (preg_match('/00:00$/', $start) && preg_match('/00:00$/', $end)) {
						$start = preg_replace('/\s+00:00$/','',$start);
						$end   = preg_replace('/\s+00:00$/','',$end);
					}
					print $start . ' - ' . $end;
					if(!empty($item->extra))
					{
						print '<br />';
						print nl2br(htmlspecialchars(implode("\n", $item->extra)));
					}
				print '</td> ' . "\n";
			}
		}
		print '</tr></table>' . "\n";
	}
?>
</td></tr></table>

<div id="over"><div></div></div>

<hr size="1" noshade="noshade" />
<div align="right">
<button type="button" onclick="parent.killPopup()"><?= word(48 /* Close */) ?></button>
</div>

</form>
</body>
</html>
