<?php
/**
 * TimeTableItem
 *
 * Class with all information needed by the timetable
 *
 * @author Mark Veldhuizen
 * @date 10-4-2008
 */
class ttRow {
	private $items = array();
	private $days = 0;

	/**
	 * Construct
	 * @acces public
	 */
	public function __construct($days, $items = array()) {
		$this->days = $days;
		foreach($items as $item) {
			if($item instanceof ttItem) {
				$this->items[] = $item;
			}
		}
	}

	public function addItem($item, $begin) {
		// needs to be of type ttItem
		if($item instanceof ttItem) {
			// does the new item fit in this row
			foreach($this->items as $rItem) {
				if($rItem->interferes($item->startDate, $item->endDate)) {
					return FALSE;
				}
			}
			// still here so it fits
			
			// get enddate of last item			
			//$begin = strtotime(date("Y-m-d 00:00:00"));
			/*if(count($this->items) > 0) {
				$begin = end($this->items)->endDate;
			}*/

			// set left and width of item
			$item->setLeftWidth($this->days, $begin);
			
			// add to row
			$this->items[] = $item;
			return TRUE;
		}
	}

	public function getItems() {
		return $this->items;
	}

	public function fillSpaces($begin) {
		$newArray = array();
		$size = count($this->items);
		for($x = 0; $x < $size; $x++) {
			//prefiller
			if($x==0 && ($this->items[$x]->startDate > $begin)) {
				//insert new item at beginning
				$tmpItem = new ttFiller (0,
							$begin,
							$this->items[$x]->startDate,
							"",
							array()
					);
				$tmpItem->setLeftWidth($this->days, $begin);
				$newArray[] = $tmpItem;
			}
			//normal items
			if($x > 0) {
				if(($this->items[$x]->startDate-60) > $this->items[$x-1]->endDate) {
					$tmpItem = new ttFiller(0,
							$this->items[$x-1]->endDate,
							$this->items[$x]->startDate,
							"",
							array()
					);
					$tmpItem->setLeftWidth($this->days, $begin);
					$newArray[] = $tmpItem;
				}
			}
			$newArray[] = $this->items[$x];

		}
		//postfiller niet nodig

		//replace arrays
		$this->items = $newArray;
	}
}
?>
