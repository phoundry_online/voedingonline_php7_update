<?php
/**
 * TimeTableItem
 *
 * Class with all information needed by the timetable
 *
 * @author Mark Veldhuizen
 * @date 10-4-2008
 */
class ttItem {
	public $id;
	public $startDate;
	public $endDate;
	public $title;
	public $width;
	public $left;
	
	/**
	 * Array with extra information. Uses the key as title in value as text
	 */
	public $extra;

	/**
	 * Construct
	 * @acces public
	 */
	public function __construct($id, $start, $end, $title, $extra = array()) {
		$this->id = $id;
		$this->startDate = $start;
		$this->endDate = $end;
		$this->title = $title;
		$this->extra = $extra;
	}

	/**
	 * this function checks if this item interferes with another item
	 */
	public function interferes($start, $end) {
		if(	($start <= $this->startDate && $end > $this->startDate) ||
			($start > $this->startDate && ($start < $this->endDate || $this->endDate == 0)) ||
			($start <= $this->endDate && $end == 0) ||
			($end == 0 && $this->endDate == 0)
		) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function setLeftWidth($days, $begin){
		$this->left = ($this->startDate-$begin)/3600;
		
		//maximum van een item bepalen
		$maxEndDate = $begin+($days*86400);
		
		if($this->endDate > $maxEndDate || $this->endDate < 1) {
			$this->width = ($maxEndDate-$this->startDate)/3600;
		} elseif($this->endDate < $this->startDate){
			$this->width = ($maxEndDate-$this->startDate)/3600;
		} else {
			$this->width = ($this->endDate-$this->startDate)/3600;
		}
		if($this->left < 0) {
			$this->width += $this->left;
			$this->left = 0;
		}
		if($this->width > $days*24) {
			$this->width = $days*24-$this->left;
		}
	}
}

class ttFiller extends ttItem {

}
?>
