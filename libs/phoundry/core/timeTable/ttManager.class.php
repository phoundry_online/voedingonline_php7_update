<?php
/**
* TimeTableManager
*
* Class that gets and arranges the items in correct order
*
* @author Mark Veldhuizen
* @date 10-4-2008
*/
class ttManager {
	public $startField;
	public $endField;
	public $titleField;
	public $startDate;
	
	/*
	 * Array with extra fields
	 */
	public $extraFields;

	/*
	 * Array with all selected items
	 */
	private $items = array();

	/*
	 * Array with the rows containing items
	 */
	private $rows = array();

	/**
	* Construct
	* @acces public
	*/
	public function __construct($start, $end, $fields, $tableName, $keyName, $days, $startDate, $where = '') {
		$this->startField = $start;
		$this->endField = $end;
		$this->tableName = $tableName;
		$this->keyName = $keyName;
		$this->days = $days;
		$this->startDate = $startDate;
		$this->where = $where;

		$this->titleField = array_shift($fields);
		$this->extraFields = $fields;
	}

	private function getItems() {
		global $db;

		if(count($this->items) == 0) {
			$begin = $this->startDate; //date("Y-m-d 00:00:00");
			$sql = sprintf('SELECT ' . $this->keyName . ',
					%1$s,
					UNIX_TIMESTAMP(%2$s) AS %2$s,
					UNIX_TIMESTAMP(%3$s) AS %3$s
					%4$s
					FROM %5$s
					WHERE ((
						%2$s > "%6$s"
						AND %2$s < ADDDATE("%6$s", %7$d)
					) OR (
						"%6$s" BETWEEN %2$s AND %3$s
					))
					%8$s
					ORDER BY %2$s ASC',
					$this->titleField,
					$this->startField,
					$this->endField,
					((count($this->extraFields) > 0) ? ','.implode(",", $this->extraFields) : ''),
					$this->tableName,
					$begin,
					$this->days,
					(($this->where != '') ? 'AND ' . $this->where : '')
				);

			//die($sql);
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++)
			{
				$extra = array();
				foreach($this->extraFields as $field) {
					$field = trim($field);
					$extra[] = $db->FetchResult($cur,$x,"{$field}");
				}
				$tmpItem = new ttItem (
							$db->FetchResult($cur,$x,$this->keyName),
							$db->FetchResult($cur,$x,$this->startField),
							$db->FetchResult($cur,$x,$this->endField),
							$db->FetchResult($cur,$x,$this->titleField),
							$extra
				);
				$this->items[] = $tmpItem;
			}
		}
		return $this->items;
	}

	public function scheduleItems() {
		$items = $this->getItems();
		$begin = strtotime($this->startDate);
		// add all items to the rows.
		foreach($items as $item) {
			foreach($this->rows as $row) {
				if($row->addItem($item, $begin)){
					// item fits in row: so break
					continue(2);
				}
				// doesn't fit: continue with next row
			}
			// doesn't fit in any row, add new row
			$tmpRow = new ttRow($this->days);
			$tmpRow->addItem($item, $begin);
			$this->rows[] = $tmpRow;
			unset($tmpRow);
		}
		// all items added, rows are set, ready for take-off
		foreach($this->rows as $row) {
			$row->fillSpaces($begin);
		}
	}

	public function getRows(){
		return $this->rows;
	}

	public function getNrRows() {
		return count($this->rows);
	}
}
?>
