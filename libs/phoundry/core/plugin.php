<?php

	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (!isset($_GET['PID']) && !isset($_POST['PID'])) {
		trigger_error('PID missing!', E_USER_ERROR);
	}
	$PID = isset($_GET['PID']) ? (int)$_GET['PID'] : (int)$_POST['PID'];

	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$sql = "SELECT code FROM phoundry_plugin WHERE id = {$PID}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if ($db->EndOfResult($cur)) {
		trigger_error('Plugin ' . $PID . ' does not exist!', E_USER_ERROR);
	}
	eval(' ?>' . $db->FetchResult($cur,0,'code') . '<?php ');
