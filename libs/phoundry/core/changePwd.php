<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess();

	if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		$errors = array();

		// Check if cur_pwd is valid for this user.
		$sql = "SELECT username, password, name FROM phoundry_user WHERE id = " . (int)$PHSes->userId;
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$db->FetchResultAssoc($cur, $user, 0);

		if (sha1($PHSes->userId . PH::$pwdSalt . md5($_POST['cur_pwd'])) != $user['password']) {
			$errors[] = word(65);
		}

		// Check if the new passwords aren't empty.
		if (trim($_POST['new_pwd']) == '' || trim($_POST['new_pwd_cNfRm_']) == '') {
			$errors[] = word(66);
		}
		// Check if the new passwords are equal.
		if ($_POST['new_pwd'] != $_POST['new_pwd_cNfRm_']) {
			$errors[] = word(67);
		}

		// Check password strength:
		$errors = array_merge($errors, PH::checkPasswordStrength($_POST['new_pwd'], word(71) . ' ' . word(72)));
	
		// New password must differ from old one:
		$newSha1Password = sha1($PHSes->userId . PH::$pwdSalt . md5($_POST['new_pwd']));
		if ($newSha1Password == $user['password']) {
			$errors[] = word(293 /* New password must differ from old */);
		}

		if (!empty($errors)) 
		{
			$errMsg = '<ul>';
			foreach($errors as $error)
				$errMsg .= '<li>' . $error . '</li>';
			$errMsg .= '</ul>';
			PH::phoundryErrorPrinter($errMsg, true);
		}
		else {
			$sql = "UPDATE phoundry_user SET password = '" . escDBquote($newSha1Password) . "', password_set = '" . date('Y-m-d H-i-s') . "' WHERE id = " . (int)$PHSes->userId;
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	
			$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
			$body = word(125, $user['name'], $PRODUCT['name'], $protocol . $_SERVER['HTTP_HOST'] . $PHprefs['url'], PH::getRemoteAddr());

			PH::sendHTMLemail($PHSes->email, word(126, $PRODUCT['name']), nl2br($body));
	
			if (isset($_GET['DMDwizard'])) {
				print '<html><head><script>parent.killPopup()</script></head><body></body></html>';
			}
			else {
				header('Location: ' . $PHprefs['customUrls']['home'] . '?_msg=69'); // Your password has been changed
			}
			exit;
		}
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(31) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">

$(function() {
	try {parent.frames['PHmenu'].setGroupSelect(false)} catch(e) {}
	document.forms[0]['cur_pwd'].focus();
	$(':submit').prop('disabled',false);
});

</script>
</head>
<body class="frames">
<form action="<?= $_SERVER['PHP_SELF'] . '?' . QS(1) ?>" method="post" onsubmit="return checkForm(this)">

<div id="headerFrame" class="headerFrame">
<?= getHeader(word(31)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b><?= word(32) ?></b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
	<?php
		if (isset($_GET['expire'])) {
			print '<div class="info">' . word(78, $PRODUCT['name']) . '</div>';
		}
	?>

	<fieldset class="field"><legend><b class="req"><?= word(70) . ' ' . word(72) ?></b></legend>
	<input type="password" name="cur_pwd" alt="1|string|24" size="40" maxlength="24" />
	</fieldset>
	
	<fieldset class="field"><legend><b class="req"><?= word(71) . ' ' . word(72) ?></b></legend>
	<input class="checkStrength" type="password" name="new_pwd" alt="1|string|24" size="40" maxlength="24" />
	</fieldset>

	<fieldset class="field"><legend><b class="req"><?= word(62) . ' ' . word(71) . ' ' . word(72) ?></b></legend>
	<input type="password" name="new_pwd_cNfRm_" size="40" maxlength="24" />
	</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td><input type="submit" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>" /></td>
	<td align="right">
	<?php if (isset($_GET['DMDwizard'])) { ?>
	<input type="button" value="<?= PH::htmlspecialchars(word(46)) ?>" onclick="parent.killPopup()" /></td>
	<?php } ?>
	</td>
</tr></table>
</div>

</form>
</body>
</html>
