<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: application/json; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'delete');

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);

    $postedRid = isset($_POST['RID']) ? $_POST['RID'] : null;
    if (!$postedRid) {
        $postedRid = isset($_GET['RID']) ? $_GET['RID'] : null;
    }
	
	// Go ahead, delete the record from the database:
	$phoundry->deleteRecord($postedRid);

	if (isset($_GET['callback'])) 
	{
        if ($_SERVER['HTTPS'] == 'on')
            $_GET['callback'] = str_replace('http:', 'https:', $_GET['callback']);

        $phoundry->finishEdit(
			'delete',
			isset($_GET['callback']) ? $_GET['callback'] : null,
			false,
			$postedRid
		);
	}
	
	exit(json_encode(array('success'=>true)));
