<fieldset><legend><b>Search help</b> (<a href="#" onclick="$('#helpDiv,#searchDiv').toggle();return false">close</a>)</legend>
<p>
Within this overview page you can restrict the number of records shown, by 
entering a search term.
</p>

<h2>Search for strings</h2>
<p>
When one or more columns contain alphanumeric characters (strings), then you
can select records by entering (a part of) the name of the record(s) you're
looking for.
Searching is case-insensitive, which means that upper- and lowercase letters can
be used interchangeably. A search for <tt>george</tt>, <tt>George</tt> and
<tt>gEoRgE</tt> will all return the same results.
</p>
<b>Wildcards</b>
<p>
You can also search for records using wildcards (* and ?). A '?' represents 
any single character, a '*' represents any number of any characters. For 
example:
</p>
<table cellspacing="0" cellpadding="3">
<tr><th>search term</th>
<th>matches</th></tr>
<tr><td>a?c</td><td>abc,acc,a1c,...</td></tr>
<tr><td>ab*</td><td>abc,abcc,abcde,...</td></tr>
<tr><td>?b*</td><td>abc,xbaaa,bbbbb,...</td></tr>
</table>

<h2>Search for numbers</h2>
<p>
When one or more columns contain numbers, you can search for specific records
by entering the number you're looking for. Wildcards can <i>not</i> be used
here!
</p>
<h2>Search for dates</h2>
<p>
When one of more of the columns contain date(-time) fields, you can also search
for specific records matching a date.
</p>
<table cellspacing="0" cellpadding="3">
<tr>
	<th>search term</th>
	<th>matches</th>
</tr>
<?php
	$fdate = str_replace(array('DD','MM','YYYY'), array('12','05','1973'), isset($g_dateFormat) ? $g_dateFormat : ' YYYY-MM-DD');
	$tdate = str_replace(array('DD','MM','YYYY'), array('11','12','2003'), isset($g_dateFormat) ? $g_dateFormat : ' YYYY-MM-DD')
?>
<tr><td>date(<?= $fdate ?>)</td><td>The exact date May, 12 1973</td></tr>
<tr><td>date(<?= $fdate ?>,*)</td><td>All dates from May, 12 1973</td></tr>
<tr><td>date(*,<?= $fdate ?>)</td><td>All dates till May, 12 1973</td></tr>
<tr><td>date(<?= $fdate ?>,<?= $tdate ?>)</td><td>All dates between May 12, 1973 and Decemer 11, 2003</td></tr>
</table>

