<fieldset><legend><b>Zoekhulp</b> (<a href="#" onclick="$('#helpDiv,#searchDiv').toggle();return false">sluiten</a>)</legend>
<p>
Binnen de overzichtspagina kunt u het aantal getoonde records beperken, door een
zoekterm op te geven. 
</p>

<h2>Zoeken op strings</h2>
<p>
Als &eacute;&eacute;n of meer velden bestaan uit alphanumerieke karakters (strings), dan
kunt u records selecteren door de waarde van het record dat u
zoekt in te tikken.<br />
Zoeken is case-insensitive, wat inhoudt dat hoofd- en kleine letters door
elkaar gebruikt kunnen worden.
</p>
<b>Wildcards</b>
<p>
U kunt ook zoeken met behulp van zogenaamde wildcards (* en ?). Een '?' 
representeert een enkel willekeurig karakter, een '*' representeert een reeks
willekeurige karakters. Bijvoorbeeld:
</p>
<table cellspacing="0" cellpadding="3">
<tr>
	<th>zoekstring</th>
	<th>matcht met</th>
</tr>
<tr><td>a?c</td><td>abc,acc,a1c,...</td></tr>
<tr><td>ab*</td><td>abc,abcc,abcde,...</td></tr>
<tr><td>?b*</td><td>abc,xbaaa,bbbbb,...</td></tr>
</table>

<h2>Zoeken op nummers</h2>
<p>
Als &eacute;&eacute;n of meerdere kolommen nummers bevatten, kunt u hierop zoeken door
het nummer dat u zoekt in te geven. Wildcards kunnen hier <i>niet</i> worden
gebruikt.
</p>
<h2>Zoeken op data</h2>
<p>
Als &eacute;&eacute;n of meer van de kolommen die u op het scherm ziet datum(-tijd)velden 
bevat, kunt u ook zoeken op deze velden. 
</p>
<table cellspacing="0" cellpadding="3">
<tr>
	<th>zoekstring</th>
	<th>matcht met</th>
</tr>
<?php
	$fdate = str_replace(array('DD','MM','YYYY'), array('12','05','1973'), isset($g_dateFormat) ? $g_dateFormat : ' YYYY-MM-DD');
	$tdate = str_replace(array('DD','MM','YYYY'), array('11','12','2003'), isset($g_dateFormat) ? $g_dateFormat : ' YYYY-MM-DD')
?>
<tr><td>date(<?= $fdate ?>)</td><td>De exacte datum 12 mei 1973</td></tr>
<tr><td>date(<?= $fdate ?>,*)</td><td>Alle data vanaf 12 mei 1973</td></tr>
<tr><td>date(*,<?= $fdate ?>)</td><td>Alle data t/m 12 mei 1973</td></tr>
<tr><td>date(<?= $fdate ?>,<?= $tdate ?>)</td><td>Alle data tussen 12 mei 1973 en 11 december 2003</td></tr>
</table>
</fieldset>
