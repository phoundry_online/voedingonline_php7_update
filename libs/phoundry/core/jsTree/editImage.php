<?php
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require 'jsTree.class.php';

	header('Expires: 0');
	header("Content-type: text/html; charset=utf-8");
	if (!isset($_SESSION['jsTree'])) {
		die("Variable \$_SESSION['jsTree'] is not set!");
	}
	$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';

	$orgWidth = (int)$_GET['w'];
	$orgHeight = (int)$_GET['h'];
	$orgPath = str_replace('..','.',$_GET['path']);

	// Make a working copy:
	$editUrl =   $treePREFS['rootUrl'] . dirname($orgPath) . '/__edit__' . basename($orgPath);
	$editPath =  $treePREFS['rootDir'] . dirname($orgPath) . '/__edit__' . basename($orgPath);

	copy($treePREFS['rootDir'] . $orgPath, $editPath);

	$_SESSION['jsTree']['editPath'] = $editPath;
?>

<script type="text/javascript">

var imgEdit = {
	cropVals: null,
	keepAspect: false,

	checkDigit:function(nr, allowPercent) {
		if (allowPercent)
			return nr.replace(/[^0-9%]/g, '');
		else
			return nr.replace(/[^0-9]/g, '');
	},

	checkAspect:function(what, value) {
		if (imgEdit.keepAspect) {
   		if (what == 'width') {
      		$('#editHeight').val(value == '' ? '' : Math.round(<?= $orgHeight ?> / (<?= $orgWidth ?>/value)));
   		}
   		else if (what == 'height') {
      		$('#editWidth').val(value == '' ? '' : Math.round(<?= $orgWidth ?> / (<?= $orgHeight ?>/value)));
   		}
		}
	},

	saveAs:function() {
		makePopup(300,130,'<?= treeWord(46 /* Save as */) ?>','jsTree.php?lang=<?= $lang ?>&filename=<?= escSquote(basename($orgPath)) ?>&task=save_as');
	},

	action:function(action, data) {
		data = data || {};
		data['action'] = action;

		$("#editDiv button,input").prop('disabled',true);

		$.ajax({	url:'processImage.php',
					data:data,
					dataType:'json',
					error:function(request,status,errorThrown) {
						alert('An error occured!');
					},
		success:function(data) {
			$("#editDiv button,input").prop('disabled',false);

			if (data['status'] == 'ok') {
				$('#content').html('<img id="editImage" src="<?= PH::htmlspecialchars(PH::urlencodeURL($editUrl)) ?>?_='+new Date().getTime()+'" width="'+data['width']+'" height="'+data['height']+'" />');
				$('#editWidth').val(data['width']);
				$('#editHeight').val(data['height']);
				//$('#editImage').attr({width:data['width'], height:data['height'], src:'<?= PH::htmlspecialchars(PH::urlencodeURL($editUrl)) ?>?'+Math.random()});

				if (action == 'save') {
					// Update preview image, close edit-image-window:
					viewFile('<?= $orgPath ?>', data['width']+'x'+data['height']+' pixels');
					$('#editDiv').fadeOut(500,function(){$(this).remove()});
				}
				else if (action == 'save_as') {
					// Reload tree:
					$('#treeDiv').jstree('refresh');
					$('#editDiv').fadeOut(500,function(){$(this).remove()});
				}
			}
			else {
				alert(data['message']);
			}
			$('#cropToolbar,#resizeToolbar').fadeOut();
		}});
	},

	endCrop:function(c) {
		$('#cropWidth').val(c.w);
		$('#cropHeight').val(c.h);
		if (c.h == 0 || c.w == 0) {
			imgEdit.cropVals = null; // Nothing selected
		}
		else {
			imgEdit.cropVals = c;
		}
	},

	startCrop:function(keepAspect) {
		imgEdit.keepAspect = keepAspect;
		$('#cropToolbar').show();
		$('#topToolbar button').prop('disabled',true);

		if (keepAspect) {
			// Retain aspect ratio
			var img = $('#editImage')[0];
			$('#editImage').Jcrop({
				aspectRatio: img.width/img.height,
				onSelect:function(c) { imgEdit.endCrop(c); },
				onChange:function(c) { imgEdit.endCrop(c); }
			});
		}
		else {
			$('#editImage').Jcrop({
				onSelect:function(c) { imgEdit.endCrop(c); },
				onChange:function(c) { imgEdit.endCrop(c); }
			});
		}
	},

	stopCrop:function() {
		if (imgEdit.cropVals == null) {
			alert('<?= treeWord(54 /* Select an area to crop */) ?>');return;
		}
		var data = {action:'crop',x:imgEdit.cropVals.x,y:imgEdit.cropVals.y,width:imgEdit.cropVals.w,height:imgEdit.cropVals.h};

		imgEdit.cropVals = null;
		$.ajax({url:'processImage.php', data:data, dataType:'json', success:function(data) {
			$("#editDiv button").prop('disabled',false);

			if (data['status'] == 'ok') {
				$('#content').html('<img id="editImage" src="<?= PH::htmlspecialchars(PH::urlencodeURL($editUrl)) ?>?_='+new Date().getTime()+'" width="'+data['width']+'" height="'+data['height']+'" />');
				$('#cropWidth').val(data['width']);
				$('#cropHeight').val(data['height']);
			}
			else {
				alert(data['message']);
			}
			imgEdit.cancelCrop();
		}});
	},

	cancelCrop:function() {
		imgEdit.cropVals = null;
		var img = $('#editImage')[0];
		$('#content').html('<img id="editImage" src="<?= PH::htmlspecialchars(PH::urlencodeURL($editUrl)) ?>?_='+new Date().getTime()+'" width="'+img.width+'" height="'+img.height+'" />');
		$('#topToolbar button').prop('disabled',false);
		$('#cropToolbar').fadeOut();
	},

	startResize:function(keepAspect) {
		imgEdit.keepAspect = keepAspect;
		$('#resizeToolbar').show();
		$('#topToolbar button').prop('disabled',true);
	},

	stopResize:function() {
		imgEdit.action('resize',{width:$('#editWidth').val(),height:$('#editHeight').val()});
		$('#topToolbar button').prop('disabled',false);
		//imgEdit.cancelResize();
	},

	cancelResize:function() {
		$('#topToolbar button').prop('disabled',false);
		$('#resizeToolbar').fadeOut();
	}
}

</script>

<!-- Header -->
<div id="header">

<div class="toolbar" id="topToolbar">
	<table cellspacing="0" cellpadding="0"><tr>
	<td class="space">
		<button class="but" type="button" title="<?= treeWord(39 /* Undo/Reset */) ?>" onclick="imgEdit.action('reset')"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/arrow_undo.png" /></button>
	</td>
	<td class="space">
		<button class="but" type="button" title="<?= treeWord(41 /* Rotate counter clockwise */) ?>" onclick="imgEdit.action('rotate_ccw')"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/arrow_rotate_anticlockwise.png" /></button><button class="but" type="button" title="<?= treeWord(40 /* Rotate clockwise */) ?>" onclick="imgEdit.action('rotate_cw')"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/arrow_rotate_clockwise.png" /></button>
	</td>
	<td class="space">
		<button class="but" type="button" title="<?= treeWord(42 /* Horizontal flip */) ?>" onclick="imgEdit.action('flip_hor')"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/shape_flip_horizontal.png" /></button><button class="but" type="button" title="<?= treeWord(43 /* Vertical flip */) ?>" onclick="imgEdit.action('flip_vert')"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/shape_flip_vertical.png" /></button>
	</td>
	<td class="space">
		<button class="but" type="button" title="<?= treeWord(44 /* Crop */) ?>" onclick="imgEdit.startCrop(false)"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/img_crop.png" /></button>
		<button class="but" type="button" title="<?= treeWord(44 /* Crop */) . ' (' . treeWord(56 /* Keep aspect */) . ')' ?>" onclick="imgEdit.startCrop(true)"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/img_crop.png" /><img class="icon" src="pics/lock.png" /></button>
	</td>
	<td class="space">
		<button class="but" type="button" title="<?= treeWord(45 /* Resize */) ?>" onclick="imgEdit.startResize(false)"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/img_resize.png" /></button>
		<button class="but" type="button" title="<?= treeWord(45 /* Resize */) . ' (' . treeWord(56 /* Keep aspect */) . ')' ?>" onclick="imgEdit.startResize(true)"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/img_resize.png" /><img class="icon" src="pics/lock.png" /></button>
	</td>
	</tr></table>
</div>

<div class="toolbar" id="bottomToolbar" style="position:absolute;top:28px;width:100%">
<button class="but" type="button" style="float:right" onclick="$('#editDiv').fadeOut(500,function(){$(this).remove()})"><?= treeWord(26 /* Cancel */) ?></button>
<button class="but" type="button" style="float:right" onclick="imgEdit.saveAs()"><?= treeWord(46 /* Save as... */) ?></button>
<button class="but okBut" type="button" style="float:right" onclick="imgEdit.action('save')"><?= treeWord(47 /* Save */) ?></button>
<?php if (function_exists('imagefilter')) { ?>
<button class="but" type="button" onclick="imgEdit.action('grayscale')"><?= treeWord(48 /* Grayscale */) ?></button>
<button class="but" type="button" onclick="imgEdit.action('sepia')"><?= treeWord(49 /* Sepia */) ?></button>
<button class="but" type="button" onclick="imgEdit.action('invert')"><?= treeWord(50 /* Invert */) ?></button>
<button class="but" type="button" onclick="imgEdit.action('brighten')"><?= treeWord(51 /* Brighten */) ?></button>
<button class="but" type="button" onclick="imgEdit.action('darken')"><?= treeWord(52 /* Darken */) ?></button>
<?php } ?>
</div>

<div class="toolbar" id="resizeToolbar" style="position:absolute;top:28px;width:100%;display:none">
	<button class="but" type="button" style="float:right" onclick="imgEdit.cancelResize()"><?= treeWord(26 /* Cancel */) ?></button>
	<table cellspacing="0" cellpadding="0" style="margin-left:3px"><tr>
		<td>
			<input type="text" id="editWidth" value="<?= $orgWidth ?>" style="width:24px" size="2" maxlength="4" onchange="this.value=imgEdit.checkDigit(this.value,false);imgEdit.checkAspect('width',this.value)" />x<input type="text" id="editHeight" value="<?= $orgHeight ?>" style="width:24px" size="2" maxlength="4" onchange="this.value=imgEdit.checkDigit(this.value,false);imgEdit.checkAspect('height',this.value)" />px
		</td>
		<td style="padding-left:5px">
		<select onchange="if(this.value!=''){var tmp=this.value.split('x');$('#editWidth').val(tmp[0]);$('#editHeight').val(tmp[1])}">
		<option value="">[<?= treeWord(53 /* select size */) ?>]</option>
		<option value="1024x768">1024x768</option>
		<option value="768x1024">768x1024</option>
		<option value="800x600">800x600</option>
		<option value="600x800">600x800</option>
		<option value="400x300">400x300</option>
		<option value="300x400">300x400</option>
		<option value="320x240">320x240</option>
		<option value="340x320">240x320</option>
		</select>
		</td>
		<td>
		<button class="but" type="button" onclick="imgEdit.stopResize()"><?= treeWord(25 /* Ok */) ?></button>
		</td>
	</tr></table>
</div>

<div class="toolbar" id="cropToolbar" style="position:absolute;top:28px;width:100%;display:none">
	<button class="but" type="button" style="float:right" onclick="imgEdit.cancelCrop()"><?= treeWord(26 /* Cancel */) ?></button>
	<table cellspacing="0" cellpadding="0" style="margin-left:3px"><tr>
		<td>
			<input type="text" id="cropWidth" style="width:24px" size="2" readonly="readonly" />x<input type="text" id="cropHeight" style="width:24px" size="2" readonly="readonly" />px
		</td>
		<td><button class="but" type="button" onclick="imgEdit.stopCrop()"><?= treeWord(55 /* crop */) ?></button></td>
	</tr></table>
</div>

</div>
<!-- /Header -->

<!-- Content -->
<div id="content" style="background:url(pics/bg.gif)"><img name="editImage" id="editImage" src="<?= PH::htmlspecialchars(PH::urlencodeURL($editUrl)) . '?' . time() ?>" width="<?= $orgWidth ?>" height="<?= $orgHeight ?>" /></div>
<!-- /Content -->
