<?php

$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";
checkAccess();

/*
$_SESSION['jsTree'] = array(
	'rootDir' => '/project/docs/htdocs/tree/uploaded',
	'rootUrl' => '/tree/uploaded',
	'uploadExtensions' => array('gif','jpg','jpeg','png'),
   'selectExtensions' => array('gif')
);
*/

if (isset($_GET['shared'])) {
	if ($_GET['shared'] == 'true') {
		$_SESSION['jsTree']['shared'] = true;
	}
	else {
		unset($_SESSION['jsTree']['shared']);
	}
	unset($_GET['shared']); // Prevent bubbling
}

$treePREFS = array(
	'showTabs' => true,
	'asciiOnly' => false,
	'skipPattern' => '/(^thumb_(.*)\.png$)|(^__edit__)/',
	'dateFormat'  => 'd-m-Y H:i:s',
	'rootDir'     => '/some/browsable/dir',
	'rootUrl'     => '/some/browsable/url',
	'uploadExtensions' => array(),
	'selectExtensions' => array(),
	'allowFiles'   => true,
	'allowFolders' => true,
	'useFileFunction' => "alert('{\$path}')",
	'thumbnailWidth' => null,
	'thumbnailHeight' => null,
	'allowedWidth' => null,
	'allowedHeight' => null,
	'allowedSize' => null // in MegaBytes
);

if (isset($_SESSION['jsTree'])) {
	foreach($_SESSION['jsTree'] as $name=>$value) {
		if (array_key_exists($name, $treePREFS)) {
			$treePREFS[$name] = $value;
		}
	}
}

if (isset($_SESSION['jsTree']['shared'])) {
	$treePREFS['rootDir'] = $PHprefs['uploadDir'] . '/SHARED';
	$treePREFS['rootUrl'] = $PHprefs['uploadUrl'] . '/SHARED';
	$treePREFS['allowFiles'] = false;
	$treePREFS['allowFolders'] = false;
}

$treeWORDS = array(
	'nl' => array(
		0   => 'Nee',
		1   => 'Ja',
		2   => 'Bestand',
		3   => 'Map',
		4   => 'Nieuwe map',
		5   => 'Verwijder map',
		6   => 'Upload bestand',
		7   => 'Verwijder bestand',
		8   => 'Selecteer een map!',
		9   => 'Gebruik bestand',
		10  => 'Hernoem bestand',
		11  => 'Hernoem map',
		12  => 'De root map kan niet worden verwijderd!',
		13  => 'Bekijk',
		14  => 'Ongeldige extensie! Gebruik #1.',
		15  => 'Verplaatsen mislukt!',
		16  => 'Weet u zeker dat u #1 wilt verwijderen?',
		17  => 'Bestand kon niet worden verwijderd!',
		18  => '(alle submappen en bestanden in deze map worden ook verwijderd)',
		19  => 'Map kon niet worden verwijderd!',
		20  => 'De root map kan niet worden hernoemd!',
		21  => 'Help',
		22  => 'Ongeldige bestandsnaam!',
		23  => '<table id="keyboard">
					<tr><td><span>up</span> <span>down</span> <span>left</span> <span>right</span></td><td>navigeer door boom</td></tr>
					<tr><td><span>enter</span></td><td>selecteer bestand/map</td></tr>
					<tr><td><span>del</span></td><td>verwijder bestand/map</td></tr>
					<tr><td><span>n</span></td><td>upload bestand(en)</td></tr>
					<tr><td><span>N</span></td><td>maak nieuwe map</td></tr>
					<tr><td><span>f2</span></td><td>hernoem bestand/map</td></tr>
					<tr><td><span>drag &amp; drop</span></td><td>verplaats bestand/map</td></tr>
				</table>',
		24  => 'Kies bestand(en)',
		25  => 'Ok',
		26  => 'Annuleer',
		27  => 'Bestand of map kon niet worden hernoemd!',
		28  => 'Terug',
		29  => 'Map #1 kon niet worden aangemaakt!',
		30  => 'Deze afbeelding is #1 pixels breed, toegestaan is #2.',
		31  => 'Deze afbeelding is #1 pixels hoog, toegestaan is #2.',
		32  => 'Bestand #1 is #2, het maximum is #3.',
		33  => 'geen',
		34  => 'Afbeelding is niet beschikbaar op URL!',
		35  => 'Voer een ALT tag in.',
		36  => 'Laad URL',
		37  => 'Afbeelding eigenschappen',
		38  => 'Bewerk afbeelding',
		39  => 'Ongedaan maken/Reset',
		40  => 'Roteer linksom',
		41  => 'Roteer rechtsom',
		42  => 'Spiegel horizontaal',
		43  => 'Spiegel verticaal',
		44  => 'Bijsnijden',
		45  => 'Verkleinen/Vergroten',
		46  => 'Opslaan als...',
		47  => 'Opslaan',
		48  => 'Zwart/wit',
		49  => 'Sepia',
		50  => 'Negatief',
		51  => 'Lichter',
		52  => 'Donkerder',
		53  => 'kies grootte',
		54  => 'Selecteer een deel van de afbeelding.',
		55  => 'Bijsnijden',
		56  => 'behoud aspect-ratio',
		57  => 'Lokaal',
		58  => 'Gedeeld',
		59  => 'Verkenner',
		60	=> 'Er bestaat al een bestand #1 in deze map, als u doorgaat wordt dit bestand overschreven',
		61	=> 'Er bestaan nog links naar het te verwijderen bestand. Druk op OK om door te gaan en op annuleren om te kijken waar er wordt gelinkt.',
	),
	'en' => array(
		0   => 'No',
		1   => 'Yes',
		2   => 'File',
		3   => 'Folder',
		4   => 'New folder',
		5   => 'Delete folder',
		6   => 'Upload file',
		7   => 'Delete file',
		8   => 'Select a folder!',
		9   => 'Use file',
		10  => 'Rename file',
		11  => 'Rename folder',
		12  => 'The root folder cannot be deleted!',
		13  => 'Preview',
		14  => 'Invalid extension! Please use #1.',
		15  => 'Moving failed!',
		16  => 'Are you sure you want to delete #1?',
		17  => 'File could not be deleted!',
		18  => '(all subfolders and images in them will be deleted as well)',
		19  => 'Folder could not be deleted!',
		20  => 'The root folder cannot be renamed!',
		21  => 'Help',
		22  => 'Invalid file or folder name!',
		23  => '<table id="keyboard">
					<tr><td><span>up</span> <span>down</span> <span>left</span> <span>right</span></td><td>navigate tree</td></tr>
					<tr><td><span>enter</span></td><td>select file/folder</td></tr>
					<tr><td><span>del</span></td><td>delete file/folder</td></tr>
					<tr><td><span>n</span></td><td>upload file(s)</td></tr>
					<tr><td><span>N</span></td><td>create new folder</td></tr>
					<tr><td><span>f2</span></td><td>rename file/folder</td></tr>
					<tr><td><span>drag &amp; drop</span></td><td>move file/folder</td></tr>
				</table>',
		24  => 'Select file(s)',
		25  => 'Ok',
		26  => 'Cancel',
		27  => 'File or folder could not be renamed!',
		28  => 'Back',
		29  => 'Folder #1 could not be created!',
		30  => 'This image is #1 pixels wide, allowed is #2.',
		31  => 'This image is #1 pixels tall, allowed is #2.',
		32  => 'File #1 is #2, the maximum is #3.',
		33  => 'none',
		34  => 'Image could not be found on URL!',
		35  => 'Enter an ALT tag.',
		36  => 'Load URL',
		37  => 'Image properties',
		38  => 'Edit image',
		39  => 'Undo/Reset',
		40  => 'Rotate clockwise',
		41  => 'Rotate counter clockwise',
		42  => 'Horizontal flip',
		43  => 'Vertical flip',
		44  => 'Crop',
		45  => 'Resize',
		46  => 'Save as...',
		47  => 'Save',
		48  => 'Grayscale',
		49  => 'Sepia',
		50  => 'Invert',
		51  => 'Brighten',
		52  => 'Darken',
		53  => 'select size',
		54  => 'Select an area to crop.',
		55  => 'Crop',
		56  => 'keep aspect ratio',
		57  => 'Local',
		58  => 'Shared',
		59  => 'Explorer',
		60	=> 'File #1 already exists in this folder, it will be overwritten if you proceed', // TODO: translate
		61	=> 'There are still links pointing to the file that is about to be deleted. Press OK to continue or cancel to view and edit the links.',
	),
	'de' => array(
		0	=>	'Nein',
		1	=> 'Ja',
		2	=> 'Datei',
		3	=> 'Ordner',
		4	=> 'Neuer Ordner',
		5	=> 'Ordner löschen',
		6	=> 'Datei hochladen',
		7	=> 'Datei löschen',
		8	=> 'Einen Ordner wählen',
		9	=> 'Datei benutzen',
		10	=> 'Datei umbenennen',
		11 => 'Ordner umbenennen',
		12 => 'Der Root-Ordner kann nicht gelöscht werden!',
		13	=> 'Vorschau',
		14 => 'Falsche Endung! Bitte benutzen Sie #1.',
		15 => 'Verschieben fehlgeschlagen!',
		16	=> 'Sind Sie sich sicher, dass Sie #1 löschen möchten?',
		17	=> 'Datei kann nicht gelöscht werden!',
		18 => '(alle Unterordner und Bilder werden ebenfalls gelöscht)',
		19 => 'Ordner kann nicht gelöscht werden!',
		20 => 'Der Root-Ordner kann nicht umbenannt werden!',
		21	=> 'Hilfe',
		22	=> 'Falscher Datei-oder Ordnername!',
		23	=> '<table id="keyboard">
		       <tr><td><span>up</span> <span>down</span> <span>left</span> <span>right</span></td><td>Baumstruktur</td></tr>
				 <tr><td><span>enter</span></td><td>wählen sie die Datei/den Ordner</td></tr>
				 <tr><td><span>del</span></td><td>Datei/Ordner löschen</td></tr>
				 <tr><td><span>n</span></td><td>Datei(en) hochladen</td></tr>
				 <tr><td><span>N</span></td><td>neuen Ordner erstellen</td></tr>
				 <tr><td><span>f2</span></td><td>Datei umbenennen/Ordner</td></tr>
				 <tr><td><span>drag &amp; drop</span></td><td>Datei verschieben/Ordner</td></tr>
				 </table>',
		24	=> 'Datei(en) wählen',
		25	=> 'Ok',
		26 => 'Abbrechen',
		27	=> 'Datei oder Ordner konnten nicht umbenannt werden!',
		28	=> 'Zurück',
		29	=> 'Ordner #1 kann nicht erstellt werden!',
		30	=> 'Das Bild ist #1 Pixel breit, erlaubt sind #2.',
		31	=> 'Das Bild ist #1 Pixel hoch, erlaubt sind #2.',
		32	=> 'Datei #1 ist #2, das Maximum sind #3.',
		33	=> 'nichts',
		34	=> 'Die Bilder konnten unter der URL nicht gefunden werden!',
		35	=> 'ALT-Tag eingeben.',
		36	=> 'URL laden',
		37 => 'Bild Eigenschaften',
		38	=> 'Bild editieren',
		39	=> 'Rückgängig/Reset',
		40	=> 'Im Uhrzeigersinn drehen',
		41	=> 'Gegen den Uhrzeigersinn drehen',
		42	=> 'Horizontal spiegeln',
		43	=> 'Vertikal spiegeln',
		44	=> 'Bild beschneiden',
		45	=> 'Größe anpassen',
		46	=> 'Speichern als...',
		47	=> 'Speichern',
		48	=> 'Graustufen',
		49	=> 'Sepia',
		50	=> 'Invert',
		51	=> 'Heller',
		52	=> 'Dunkler',
		53	=> 'Größe wählen',
		54	=> 'Wählen Sie einen Bereich zum beschneiden.',
		55	=> 'Beschneiden',
		56	=> 'Bildformat beibehalten',
		57	=> 'Lokal',
		58	=> 'Geteilt',
		59	=> 'Explorer',
		// TODO naar duits vertalen
		60	=> 'File #1 already exists in this folder it will be overwritten if you proceed!',
		61	=> 'There are still links pointing to the file that is about to be deleted. Press OK to continue or cancel to view and edit the links.',
	),
	'sv' => array(
		0   => 'Nej',
		1   => 'Ja',
		2   => 'Fil',
		3   => 'Mapp',
		4   => 'Ny mapp',
		5   => 'Radera mapp',
		6   => 'Ladda upp fil',
		7   => 'Radera fil',
		8   => 'Välj mapp',
		9   => 'Använd fil',
		10  => 'Namnge fil',
		11  => 'Namnge mapp',
		12  => 'Rotmappen kan inte tas bort',
		13  => 'Förhandsvisa',
		14  => 'Ogiltig koppling! Använd #1.',
		15  => 'Flytt misslyckades',
		16  => 'Är du säker på att du vill ta bort #1?',
		17  => 'Filen kunde inte raderas!',
		18  => '(alla undermappar och bilder i dem kommer också att raderas)',
		19  => 'Mappen kunde inte tas bort!',
		20  => 'Rotmappen kan inte namnges!',
		21  => 'Hjälp',
		22  => 'Ogiltig fil- eller mappnamn!',
		23  => '<table id="keyboard">
					<tr><td><span>upp</span> <span>ner</span> <span>vänster</span> <span>höger</span></td><td>navigera träd</td></tr>
					<tr><td><span>enter</span></td><td>välj fil/mapp</td></tr>
					<tr><td><span>del</span></td><td>radera fil/mapp</td></tr>
					<tr><td><span>n</span></td><td>ladda upp fil(er)</td></tr>
					<tr><td><span>N</span></td><td>skapa ny mapp</td></tr>
					<tr><td><span>f2</span></td><td>byt namn på fil/mapp</td></tr>
					<tr><td><span>dra &amp; släpp</span></td><td>flytta fil/mapp</td></tr>
				</table>',
		24  => 'Välj fil(er)',
		25  => 'Ok',
		26  => 'Avbryt',
		27  => 'Filen eller mappens namn kan inte ändras!',
		28  => 'Tillbaka',
		29  => 'Mapp #1 kunde inte skapas!',
		30  => 'Den här bilden är #1 bildpunkter och tillåtet är #2.',
		31  => 'Den här bilden är #1 bildpunkter och tillåtet är #2.',
		32  => 'Fil #1 är #2, maximalt #3.',
		33  => 'ingen',
		34  => 'Bild kunde inte hittas via URL!',
		35  => 'Skriv en ALT tag.',
		36  => 'Ladda URL',
		37  => 'Bildegenskaper',
		38  => 'Redigera bild',
		39  => 'Ångra/Nollställ',
		40  => 'Rotera medurs',
		41  => 'Rotera moturs',
		42  => 'Spegelvänd (Horisontellt)',
		43  => 'Spegelvänd (Vertikalt)',
		44  => 'Beskär',
		45  => 'Ändra storlek',
		46  => 'Spara som...',
		47  => 'Spara',
		48  => 'Gråskala',
		49  => 'Sepia',
		50  => 'Invertera',
		51  => 'Ljusare',
		52  => 'Mörkare',
		53  => 'Välj storlek',
		54  => 'Välj område som ska beskäras.',
		55  => 'Beskär',
		56  => 'behåll bildförhållande',
		57  => 'Lokal',
		58  => 'Delad',
		59  => 'Utforskaren',
		// TODO
		60	=> 'File #1 already exists in this folder, it will be overwritten if you proceed!',
		61	=> 'There are still links pointing to the file that is about to be deleted. Press OK to continue or cancel to view and edit the links.',
	),
	'cn' => array(
		0	=> '否',
		1	=> '是',
		2   => '文件',
		3   => '文件夹',
		4   => '新文件夹',
		5   => '删除文件夹',
		6   => '上传文件',
		7   => '删除文件',
		8   => '选择一个文件夹！',
		9   => '使用文件',
		10  => '重命名文件',
		11  => '重命名文件夹',
		12  => '根目录文件夹不能被删除！',
		13  => '预览',
		14  => '无效的扩展名格式！请使用#1。',
		15  => '移动行为失败！',
		16  => '你确定你要删除#1?',
		17  => '文件不能被删除！',
		18  => '(所有目录以下的文件夹以及图像也将被删除)',
		19  => '文件夹不能被删除！',
		20  => '根目录文件夹不能被重命名！',
		21  => '帮助',
		22  => '无效的文件或文件夹名称！',
		23  => '<table id="keyboard">
				  <tr><td><span>up</span> <span>down</span> <span>left</span> <span>right</span></td><td>导航树状栏</td></tr>
				  <tr><td><span>enter</span></td><td>选择文件/文件夹</td></tr>
				  <tr><td><span>del</span></td><td>删除文件/文件夹</td></tr>
				  <tr><td><span>n</span></td><td>上传单个或多个文件</td></tr>
				  <tr><td><span>N</span></td><td>生成新的文件夹</td></tr>
				  <tr><td><span>f2</span></td><td>重命名文件/文件夹</td></tr>
				  <tr><td><span>drag &amp; drop</span></td><td>移动文件/文件夹</td></tr>
				  </table>',
		24  => '选择单个或多个文件',
		25  => 'Ok',
		26  => '取消',
		27  => '文件或文件夹不能被重命名！',
		28  => '后退',
		29  => '文件夹#1不能被生成！',
		30  => '这张图片的像素宽度为#1，系统允许的最大值为#2。',
		31  => '这张图片的像素高度#1，系统允许的最大值为#2。',
		32  => '文件#1为#2，最大值是#3。',
		33  => 'none',
		34  => '图片未在链接中找到！',
		35  => '输入一个ALT标签',
		36  => '载入一个URL链接',
		37  => '图像属性',
		38  => '编辑图像',
		39  => '撤销/重置',
		40  => '顺时针旋转',
		41  => '逆时针旋转',
		42  => '平行翻转',
		43  => '垂直翻转',
		44  => '修剪',
		45  => '调整',
		46  => '另存为...',
		47  => '保存',
		48  => '灰度',
		49  => '墨汁感',
		50  => '反转',
		51  => '加亮',
		52  => '加暗',
		53  => '选择大小',
		54  => '选择一个区域来修剪。',
		55  => '修剪',
		56  => '保持宽高比',
		57  => '本地',
		58  => '共享的',
		59  => '浏览器',
		// TODO naar chinees vertalen
		60	=> 'File #1 already exists in this folder, it will be overwritten if you proceed!',
		61	=> 'There are still links pointing to the file that is about to be deleted. Press OK to continue or cancel to view and edit the links.',
	)
);

function treeWord() {
	// The $coreWORDS and $customWORDS are utf-8 encoded!
	global $treeWORDS, $lang;

	if (!array_key_exists($lang, $treeWORDS)) {
		$lang = 'en';
	}

	$wordNR = func_get_arg(0);
	$ss = $treeWORDS[$lang][$wordNR];
	$na = func_num_args();
	for ($x = 1; $x < $na; $x++) {
		$tmp = func_get_arg($x);
		$ss = str_replace('#' . $x, $tmp, $ss);
	}
	return $ss;
}

class jsTree {

var $rootDir = '/some/dir/';
var $rootUrl = '/some/url/';
var $skipPattern = '';
var $dateFormat = 'Y-m-d H:i:s';
var $thumbnailWidth = null;
var $thumbnailHeight = null;
var $allowedSize = null;
var $asciiOnly = false;

var $imgExts = array('gif','jpg','jpeg','png','bmp','swf');
var $knownExts = array('csv', 'doc', 'gif', 'html', 'htm', 'jpeg', 'jpg', 'pdf', 'png', 'ppt', 'swf', 'txt', 'xls', 'zip', 'xlsx', 'docx', 'pptx');

function __construct($props) {
	foreach($props as $name=>$value) {
		$this->$name = $value;
	}
	if (!is_null($this->allowedSize)) {
		$this->allowedSize *= 1048576; // Megabytes to bytes
	}
}

/**
 * Recursively delete a directory.
 */
function RmDirR($dirName) {
	if (is_dir($dirName) && $d = @opendir($dirName)) {
		while($entry = readdir($d)) {
			if ($entry != '.' && $entry != '..' && $entry != '') {
				if (is_dir($dirName.'/'.$entry)) {
					$this->RmDirR($dirName.'/'.$entry);
				} else {
					unlink($dirName.'/'.$entry);
				}
			}
		}
		closedir($d);
	}
	@rmdir($dirName);
}

/*
 * Base 64 functions that generate strings that are safe to use
 * as DOM/jQuery element id's.
 * The default +, / and = characters cannot be used in DOM ids.
 * That's why we replace them with *, _ and - respectively.
 */
function base64_encode($str) {
	return strtr(base64_encode($str), '+/=', '*_-');
}

function base64_decode($str) {
	return base64_decode(strtr($str, '*_-', '+/='));
}

function removeAccents($string) {
	$string = strtr(utf8_decode($string),
		"\xA1\xAA\xBA\xBF\xC0\xC1\xC2\xC3\xC5\xC7\xC8\xC9\xCA\xCB\xCC\xCD\xCE\xCF\xD0\xD1\xD2\xD3\xD4\xD5\xD8\xD9\xDA\xDB\xDD\xE0\xE1\xE2\xE3\xE5\xE7\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF0\xF1\xF2\xF3\xF4\xF5\xF8\xF9\xFA\xFB\xFD\xFF",
		'!ao?AAAAACEEEEIIIIDNOOOOOUUUYaaaaaceeeeiiiidnooooouuuyy');
	return strtr($string, array("\xC4"=>'Ae', "\xC6"=>'AE', "\xD6"=>'Oe', "\xDC"=>'Ue', "\xDE"=>'TH', "\xDF"=>'ss', "\xE4"=>'ae', "\xE6"=>'ae', "\xF6"=>'oe', "\xFC"=>'ue', "\xFE"=>'th'));
}

function human_file_size($size) {
	if($size == 0) {
		return '0 Bytes';
	}
	$filesizename = array(' Bytes', ' KB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB');
	return round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i];
}

/**
 * Check if $dir is a branch of $this->rootDir.
 * If not, someone is trying to hack!
 */
function checkDir($dir, $or_root = false) {
    $dir = str_replace('//', '/', $dir);
    if (!file_exists($this->rootDir)) return false;
    if (!file_exists($dir)) return false;
	$rootdir = realpath($this->rootDir);
	$realdir = realpath($dir);
	return strpos($realdir, $rootdir) === 0 && ($or_root || strlen($realdir) > strlen($rootdir));
}

function convertZIPfilenames($path)
{// Files in ZIP archives use the CP437 character set (weird!!!)
	if (is_dir($path)) {
		$dh = opendir($path);
		while(false !== ($file = readdir($dh))) {
			if ($file == '.' || $file == '..') continue;
			if (is_dir("$path/$file")) {
				$this->convertZIPfilenames("$path/$file");
			}
			if ($this->asciiOnly) {
				rename("$path/$file", $path . '/' . $this->removeAccents(iconv('cp437', 'utf-8//TRANSLIT', $file)));
			}
			else {
				rename("$path/$file", $path . '/' . iconv('cp437', 'utf-8//TRANSLIT', $file));
			}
			if (preg_match('/\.(asp|phtml|php|php3|php4|pl|cgi|cfml|cfm|sh)$/i', $file)) {
				rename("$path/$file", $path . '/' . preg_replace('/\.(asp|phtml|php|php3|php4|pl|cgi|cfml|cfm|sh)$/i',".\\1.txt",$file));
			}
		}
		closedir($dh);
	}
}

function getXMLtree($myDir = null) {
	global $PHprefs, $PHSes;

	if (!is_null($myDir)) {
		$myDir = $this->base64_decode($myDir);
	}

	$dir = realpath($this->rootDir . (is_null($myDir) ? '' : $myDir));
	$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";

	if (!$this->checkDir($dir, true) || $dir === false) {
		return $xml . '<root><item rel="folder"><content><name>[ -access denied- ]</name></content></item></root>';
	}

	$files = $dirs = array();

	if (is_dir($dir) && $dh = opendir($dir)) {
		while (($file = readdir($dh)) !== false) {
            $path = preg_replace('|\/+|', '/', $dir . '/'. $file);

			// Remove "old" (1 hour) image-edit files:
			if (preg_match('/^__edit__/', $file)) {
				if (time() - filemtime($path) > 3600) {
					unlink($path);
                    continue;
				}
			}

			if ($file === '.' || $file === '..' || ($this->skipPattern && preg_match($this->skipPattern, $file))) {
                continue;
            }

			/*
			 * Hide folders/files whose name start with a '.' character for regular users.
			 * SU-users do see these hidden folders.
			 */
			if ($file[0] === '.' && $PHSes->userId != 1) {
				continue;
			}

			$id = preg_replace('|^' . rtrim($this->rootDir, '/') . '|', '', $path);

			if (is_dir($dir.'/'.$file) && is_executable($dir.'/'.$file)) {
				$dirs[$file] = array(
					'mtime' => date($this->dateFormat, filemtime($path)),
					'id' => $id
				);
			}
			elseif(is_file($dir.'/'.$file) && is_readable($dir.'/'.$file)) {
				$ext = strtolower(substr($file, strrpos($file,'.')+1));

				$files[$file] = array(
					'id' => $id,
					'mtime' => date($this->dateFormat, filemtime($path)),
					'size'  => $this->human_file_size(filesize($path))
				);

				if (in_array($ext, $this->imgExts)) {
					if ($imgSz = @getimagesize($path)) {
						$files[$file]['width'] = $imgSz[0];
						$files[$file]['height'] = $imgSz[1];
					}
				}
			}
		}
		closedir($dh);
	}

	uksort($files,'strnatcasecmp'); uksort($dirs,'strnatcasecmp');

	if (is_null($myDir)) {
		$xml .= '<root>';
		$hasChildren = count($dirs) + count($files) > 0;
		$xml .= '<item rel="folder" id="' . $this->base64_encode('/') . '" ' . ($hasChildren ? 'hasChildren="true"' : '') . ' state="' . ($hasChildren ? 'open' : 'closed') . '" title=""><content><name>/</name></content>';
	}
	else {
		$xml .= '<root id="' . $this->base64_encode($myDir) . '">';
	}

	foreach($dirs as $name=>$info) {
		// id of a directory has a trailing slash, which makes it easy to see
		// whether an item is a directory or file.
		$title = $info['mtime'];

		// We need to determine if this folder has childnodes:
		$hasChildren = false;
		if (is_dir($dir . '/' . $name)) {
			$dh = opendir($dir . '/' . $name);
			while (($file = readdir($dh)) !== false) {
				if ($file[0] == '.') {
					continue;
				}

				$hasChildren = true;
				break;
			}
			closedir($dh);
		}

		$xml .= '<item rel="folder" id="' . $this->base64_encode($info['id'] .'/') . '" title="' . $title . '" ' . ($hasChildren ? 'hasChildren="true"' : '') . '><content><name>' . PH::htmlspecialchars($name) . '</name></content></item>';
	}

	foreach($files as $name=>$info) {
		$title = $info['mtime'] . '. ' . $info['size'];
		if (isset($info['height'])) {
			$title = $info['width'] . 'x' . $info['height'] . " pixels. " . $title;
		}

		$ext = strtolower(substr($name, strrpos($name,'.')+1));
		$im = $PHprefs['url'] . '/core/icons/ext_' . (in_array($ext, $this->knownExts) ? $ext : 'other') . '.png';

		$xml .= '<item rel="file" id="' . $this->base64_encode($info['id']) . '" title="' . $title . '"><content><name icon="' . $im . '">' . PH::htmlspecialchars($name) . '</name></content></item>';
	}

	if (is_null($myDir)) {
		$xml .= '</item>';
	}
	$xml .= '</root>';
	return $xml;
}

function deleteFile($myDir) {
	$myDir = $this->base64_decode($myDir);
	$base = basename($myDir);
	$file = $this->rootDir . $myDir;
	if (!$this->checkDir($file)) {
		return false;
	}

	$thumb = dirname($file) . '/thumb_' . preg_replace('/\.(gif|png|jpg|jpeg)$/i', '.png', $base);

	@unlink($thumb);

	if (!is_writable(dirname($file))) return false; // This file cannot be deleted

	if (is_file($file)) {
		unlink($file);
	}
	else {
		$this->RmDirR($file);
	}

	return true;
}

function moveFile($from, $to) {
	$from = $this->base64_decode($from);
	$to   = $this->base64_decode($to);


	$isDir = substr($from,-1) == '/';

	$fromThumb = dirname($from) . '/thumb_' . preg_replace('/\.(gif|png|jpg|jpeg)$/i', '.png', basename($from));

	$src = $this->rootDir . $from;
	$dst = $this->rootDir . $to . basename($from);

	$srcThumb = $this->rootDir . $fromThumb;
	$dstThumb = $this->rootDir . $to . basename($fromThumb);

	if (!$this->checkDir($src) || !$this->checkDir($this->rootDir . $to)) {
		return false;
	}

	@rename($srcThumb, $dstThumb);
	$res = @rename($src, $dst);
	return (!$res) ? false : $this->base64_encode($to . basename($from) . ($isDir ? '/' : ''));
}

function renameFile($id, $newName) {
	$newName = preg_replace('/\.(asp|phtml|php|php3|php4|pl|cgi|cfml|cfm|sh)$/i',".\\1.txt",$newName);

	$id  = $this->base64_decode($id);
	$src = $this->rootDir . $id;

	$isDir = substr($id,-1) == '/';

	if (!$this->checkDir($src)) {
		return false;
	}

	if ($this->asciiOnly) {
		$newName = $this->removeAccents($newName);
	}

	$thumb = dirname($id) . '/thumb_' . preg_replace('/\.(gif|png|jpg|jpeg)$/i', '.png', basename($id));

	$path = str_replace('//', '/', dirname($id) . '/' . $newName);
	$pathThumb = str_replace('//', '/', dirname($thumb) . '/thumb_' . preg_replace('/\.(gif|png|jpg|jpeg)$/i', '.png', $newName));

	@rename($this->rootDir . $thumb, $this->rootDir . $pathThumb);

	$res = @rename($src, $this->rootDir . $path);
	return (!$res) ? false : array('path'=>$this->base64_encode($path . ($isDir ? '/' : '')), 'name'=>$newName);
}

function addDir($id, $newName) {
	$id = $this->base64_decode($id);
	if (!$this->checkDir($this->rootDir . $id, true)) {
		return false;
	}

	if ($this->asciiOnly) {
		$newName = $this->removeAccents($newName);
	}

	$path = str_replace('//', '/', $id . '/' . $newName);

	$res = @mkdir($this->rootDir . $path, 0755);
	return (!$res) ? false : array('path'=>$this->base64_encode($path . '/'), 'name'=>$newName);
}

function downloadURLs($id, $urls, &$errors) {
	$errors = array();
	$id = $this->base64_decode($id);
	$saveDir = $this->rootDir . $id;
	if (!$this->checkDir($saveDir, true)) {
		$errors[] = "Invalid directory {$saveDir}.";
		return false;
	}
	foreach($urls as $url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$data = curl_exec($ch);
		$status      = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		$curl_error  = curl_error($ch);
		curl_close($ch);
		if (!empty($curl_error)) {
			$errors[] = "Error fetching {$url}: {$curl_error}.\n";
			continue;
		}
		$fp = fopen("{$saveDir}/" . basename($url), 'w');
		fwrite($fp, $data);
		fclose($fp);
		chmod("{$saveDir}/" . basename($url), 0644);
	}
	return true;
}

function addFiles($id, $files, $unzip, &$errors) {
	$errors = array();
	$id = $this->base64_decode($id);
	$saveDir = $this->rootDir . $id;
	if (!$this->checkDir($saveDir, true)) {
		$errors[] = "Invalid directory {$saveDir}.";
		return false;
	}

	$nrFiles = count($files['name']);
	for($f = 0; $f < $nrFiles; $f++) {
		if (empty($files['name'][$f])) continue;

		if (!is_null($this->allowedSize) && $files['size'][$f] > $this->allowedSize) {
			$errors[] = treeWord(32, $files['name'][$f], $this->human_file_size($files['size'][$f]),  $this->human_file_size($this->allowedSize)) . "\n";
			continue;
		}

		$orgName = $files['name'][$f];
		$phpName = $files['tmp_name'][$f];

		// For security reasons, we don't allow uploads with extensions .php etc:
		$orgName = preg_replace('/\.(asp|phtml|php|php3|php4|pl|cgi|cfml|cfm|sh)$/i',".\\1.txt",$orgName);

		if ($this->asciiOnly) {
			$orgName = $this->removeAccents($orgName);
		}

		if (!@move_uploaded_file($phpName, "{$saveDir}/{$orgName}")) {
			$errors[] = "Could not save file {$orgName}.";
			return false;
		}

		chmod("{$saveDir}/{$orgName}", 0644);

		// Do we need to unzip?
		if (in_array($f, $unzip)) {
			shell_exec('unzip -od ' . escapeshellarg($saveDir) . ' ' . escapeshellarg($saveDir . '/' . $orgName));
			unlink("$saveDir/$orgName");
			$this->convertZIPfilenames($saveDir);
		}

		// Create thumbnail, if so requested:
		if (preg_match('/\.(gif|png|jpg|jpeg)$/i', $orgName, $reg) &&
			function_exists('imagepng') &&
			($this->thumbnailWidth || $this->thumbnailHeight)) {
			$ext = $reg[1];
			require_once 'Thumbnail.php';
			$thumb = new Thumbnail("{$saveDir}/{$orgName}");
			if ($this->thumbnailWidth && $this->thumbnailHeight) {
				$thumb->sizeBoth($this->thumbnailWidth, $this->thumbnailHeight);
			}
			elseif ($this->thumbnailWidth) {
				$thumb->sizeWidth($this->thumbnailWidth);
			}
			elseif ($this->thumbnailHeight) {
				$thumb->sizeHeight($this->thumbnailHeight);
			}
			$thumbName = 'thumb_' . preg_replace('/' . $ext . '$/', 'png', $orgName);
			$thumb->save("$saveDir/$thumbName", 'PNG');
			chmod("$saveDir/$thumbName", 0644);
		}
	}
	return true;
}

} // End class
?>
