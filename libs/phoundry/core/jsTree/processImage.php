<?php
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require 'jsTree.class.php';

	header('Expires: 0');
	header('Content-type: application/json; charset=utf-8');
	if (!isset($_SESSION['jsTree'])) {
		die("Variable \$_SESSION['jsTree'] is not set!");
	}
	$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';

	$img = $_SESSION['jsTree']['editPath'];
	preg_match('/\.([^\.]+)$/', $img, $reg);
	$ext = strtolower($reg[1]);
	if ($ext == 'jpg') $ext = 'jpeg';

	$infn  = 'imagecreatefrom' . $ext;

	if (!function_exists($infn)) {
		die('{status:"error",message:"Function \"' . $infn . '\" is not available!"}');
	}

	function outputImage($image, $file, $ext) {
		$outfn = 'image' . $ext;
		if ($ext == 'jpeg') {
			$result = $outfn($image, $file, 100);
		}
		else {
			$result = $outfn($image, $file);
		}
		return $result;
	}

	switch($_GET['action']) {
		case 'reset':
			copy(str_replace('/__edit__', '/', $img), $img);
			break;
		case 'save':
			rename($img, str_replace('/__edit__', '/', $img));
			$img = str_replace('/__edit__', '/', $img);
			break;
		case 'save_as':
			$newName = str_replace(array('..','/'), array('',''), $_GET['filename']);
			if (preg_match('/\.(asp|phtml|php|php3|php4|pl|cgi|cfml|cfm|sh)$/i', $newName)) {
				$newName = $newName .= '.txt';
			}
			$newName = dirname($img) . '/' . $newName;
			rename($img, $newName);
			$img = $newName;
			break;
		case 'resize':
			$width = $_GET['width'];
			if (!is_numeric($width) || $width < 1 || $width > 2000) {
				echo json_encode(array('status'=>'error', 'message'=>'Invalid width!'));
				exit;
			}
			$height = $_GET['height'];
			if (!is_numeric($height) || $height < 1 || $height > 2000) {
				echo json_encode(array('status'=>'error', 'message'=>'Invalid height!'));
				exit;
			}
			$in = $infn($img);
			list($in_w, $in_h) = getimagesize($img);
			$out = imagecreatetruecolor($width, $height);
			imagecopyresampled($out, $in, 0, 0, 0, 0, $width, $height, $in_w, $in_h);
			outputImage($out, $img, $ext);
			imagedestroy($in);
			imagedestroy($out);
			break;
		case 'crop':
			$x = $_GET['x'];
			$y = $_GET['y'];
			$w = $_GET['width'];
			$h = $_GET['height'];
			if (!is_numeric($x) || !is_numeric($y) || !is_numeric($w) || !is_numeric($h) || $x < 0 || $y < 0 || $w < 1 || $h < 1) {
				echo json_encode(array('status'=>'error', 'message'=>'Invalid crop arguments!'));
				exit;
			}
			$in = $infn($img);
			$out = imagecreatetruecolor($w, $h);
			imagecopyresampled($out, $in, 0, 0, $x, $y, $w, $h, $w, $h);
			outputImage($out, $img, $ext);
			imagedestroy($in);
			imagedestroy($out);
			break;
		case 'flip_hor':
		case 'flip_vert':
			$in = $infn($img);
			$x = imagesx($in);
			$y = imagesy($in);
			$out = imagecreatetruecolor($x, $y);
			if ($_GET['action'] == 'flip_hor') {
				imagecopyresampled($out, $in, 0, 0, $x-1, 0, $x, $y, 0-$x, $y);
			}
			else {
				imagecopyresampled($out, $in, 0, 0, 0, $y-1, $x, $y, $x, 0-$y);
			}
			outputImage($out, $img, $ext);
			imagedestroy($in);
			imagedestroy($out);
			break;
		case 'rotate_cw':
		case 'rotate_ccw':
			$angle = $_GET['action'] == 'rotate_ccw' ? 90 : 270;
			$in = $infn($img);
			$x = imagesx($in);
			$y = imagesy($in);
			$max = max($x, $y);
			$square = imagecreatetruecolor($max, $max);
			imagecopy($square, $in, 0, 0, 0, 0, $x, $y);
			$square = imagerotate($square, $angle, 0);
			$out = imagecreatetruecolor($y, $x);
			if ($angle == 270) {
				imagecopy($out, $square, 0, 0, $max - $y, 0, $y, $x);
			}
			else {
				imagecopy($out, $square, 0, 0, 0, $max - $x, $y, $x);
			}
			imagedestroy($square);
			outputImage($out, $img, $ext);
			imagedestroy($in);
			imagedestroy($out);
			break;
		case 'sepia':
			$in = $infn($img);
			imagefilter($in, IMG_FILTER_GRAYSCALE);
			//imagefilter($in, IMG_FILTER_COLORIZE, 100, 50, 0);
			imagefilter($in, IMG_FILTER_COLORIZE, 90, 60, 30);
			outputImage($in, $img, $ext);
			imagedestroy($in);
			break;
		case 'invert':
			$in = $infn($img);
			imagefilter($in, IMG_FILTER_NEGATE);
			outputImage($in, $img, $ext);
			imagedestroy($in);
			break;
		case 'grayscale':
			$in = $infn($img);
			imagefilter($in, IMG_FILTER_GRAYSCALE);
			outputImage($in, $img, $ext);
			imagedestroy($in);
			break;
		case 'brighten':
			$in = $infn($img);
			imagefilter($in, IMG_FILTER_BRIGHTNESS, 10);
			outputImage($in, $img, $ext);
			imagedestroy($in);
			break;
		case 'darken':
			$in = $infn($img);
			imagefilter($in, IMG_FILTER_BRIGHTNESS, -10);
			outputImage($in, $img, $ext);
			imagedestroy($in);
			break;
		default:
			echo json_encode(array('status'=>'error', 'message'=>"Invalid action {$_GET['action']}!"));
			exit;
	}

	list($w, $h) = getimagesize($img);
	echo json_encode(array('status'=>'ok', 'width'=>$w, 'height'=>$h));
