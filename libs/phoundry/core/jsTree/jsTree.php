<?php
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require 'jsTree.class.php';

	$jsTree = new jsTree($treePREFS);
	$task = isset($_GET['task']) ? $_GET['task'] : 'listDir';

	function _escSquote($str) {
		return str_replace("'", "\\'", $str);
	}

	function human_file_size($size) {
		if($size == 0) {
			return '0 Bytes';
		}
		$filesizename = array(' Bytes', ' KB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB');
		return round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i];
	}

	if ($task == 'listDir') {
		header('Content-type: ' . (stristr($_SERVER['HTTP_ACCEPT'],'application/xhtml+xml') ? 'application/xhtml+xml' : 'text/xml'));
		header('Cache-control: no-cache');
		header('Pragma: no-cache');
		header('Expires: 0');
		print $jsTree->getXMLtree(!empty($_GET['id']) ? $_GET['id'] : null);
		exit;
	}

	if ($task == 'delete' && !empty($_GET['id'])) {
		header('Content-type: application/json');
		print json_encode($jsTree->deleteFile($_GET['id']));
		exit;
	}

	if ($task == 'move' && !empty($_GET['id']) && !empty($_GET['dst'])) {
		header('Content-type: application/json');
		print json_encode($jsTree->moveFile($_GET['id'], $_GET['dst']));
		exit;
	}

	if ($task == 'rename' && !empty($_GET['id']) && !empty($_GET['newName'])) {
		header('Content-type: application/json');
		print json_encode($jsTree->renameFile($_GET['id'], $_GET['newName']));
		exit;
	}

	if ($task == 'addDir' && !empty($_GET['id']) && !empty($_GET['newName'])) {
		header('Content-type: application/json');
		print json_encode($jsTree->addDir($_GET['id'], $_GET['newName']));
		exit;
	}

	header("Content-type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Popup</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/jstree.css" type="text/css" />
<style type="text/css">

body {
	background: #fff;
	margin: 5px;
}

form {
	margin: 0;
	padding: 0;
}

body, input, th, td, div, button {
	font-family: Tahoma,Futura,Helvetica,Sans-serif;
	font-size: 11px;
}

th {
	font-weight: bold;
}

fieldset {
	padding: 5px;
	margin-bottom: 4px;
	border: 1px solid #cfcfcf;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
}

.but {
	width: 70px;
}

img {
	border: 0;
}

.icon {
	width: 16px;
	height: 16px;
}

.ptr {
	cursor: pointer;
}

b.error {
	color: red;
}

#keyboard span {
	border: 2px outset silver;
	background: #f5f5f5;
	padding: 0 1px;
	font-weight: bold;
	line-height:20px;
}

</style>
<script src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
</head>
<body>
<?php if ($task == 'help') { ?>

<fieldset><legend><?= treeWord(21 /* Help */) ?></legend>
<table id="keyboard"><?= treeWord(23 /* Keyboard help */) ?></table>
</fieldset>
<p>
<input type="button" class="but" value="<?= treeWord(25 /* Cancel */) ?>" onclick="parent.killPopup()" />
</p>

<?php
	}
	elseif ($task == 'save_as') {
?>
<form onsubmit="return false">
<fieldset><legend>Filename</legend>
<input type="text" name="filename" style="width:100%" value="<?= PH::htmlspecialchars($_GET['filename']) ?>" />
<p>
<input type="button" style="float:right" class="but" value="<?= treeWord(26 /* Cancel */) ?>" onclick="parent.killPopup()" />
<input id="okBut" type="button" class="but" value="<?= treeWord(25 /* Ok */) ?>" onclick="parent.imgEdit.action('save_as',{filename:this.form['filename'].value});parent.killPopup()" />
</p>
</form>
<?php
	}
	elseif ($task == 'addFile') {
		// Whats' the Max upload size?
		$systemLimit = (int)preg_replace('/[^\d]/', '', ini_get('upload_max_filesize')) * 1048576;
		$localLimit  = $jsTree->allowedSize;
		if (is_null($localLimit)) {
			$limit = $systemLimit;
		}
		elseif ($localLimit < $systemLimit) {
			$limit = $localLimit;
		}
		else {
			$limit = $systemLimit;
		}
?>
<script type="text/javascript">

var row = '<tr><td><img src="pics/link.png" class="icon ptr" onclick="toggleURLupload({$idx})" /></td><td width="90%"><input id="fileInput{$idx}" type="file" name="file[]" style="width:100%" onchange="checkExtension(this)" /><div class="note" style="color:red"></div></td><td valign="top" align="center"><input type="checkbox" name="unzip[]" title="Unzip?" value="{$idx}" style="display:none" /></td></tr>';

function checkExtension(el) {
	var tr = $(el).parents('tr').eq(0);
	var allowed = '<?= implode('|', $treePREFS['uploadExtensions']) ?>';
	var re = new RegExp(allowed+'$', 'i');
	if (/[#\?\*]/.test(el.value)) {
		$('.note', tr).html('<?= treeWord(22 /* Invalid filename */) ?>');
		return;
	}
	if (/\.zip$/.test(el.value) &&
	    (allowed.length == 0 || re.test(el.value))
		) {
		$("input[name='unzip[]']", tr).show();
	}
	else {
		$("input[name='unzip[]']", tr).hide();
	}

	var msg = [];

	if (allowed.length > 0 && !re.test(el.value)) {
		msg.push('<?= treeWord(14 /* Invalid extension */) ?>'.replace('#1', allowed.replace(/\|/g, ', ')));
	}

	if(checkFileDuplicate(el.value)) {
		msg.push('<?= treeWord(60) ?>'.replace('#1', el.value));
	}
	$('.note', tr).html(msg.join("\n"));
}

function checkURL(el) {
	if (!/^https?:\/\//.test(el.value)) {
		alert('Enter a valid URL!');
	}
}

function checkFileDuplicate(filename) {
	var folder = parent.jQuery("#"+(<?=json_encode($_GET['id'])?>)),
		 files = folder.find(">ul>li[rel='file'] a").map(function(){
		return $.trim($(this).text());
	});

	return -1 !== $.inArray(filename, files);
}

var nrRows = 0;

function addFileRow() {
	$('#filesTBL').append(row.replace(/\{\$idx\}/g, nrRows++));
}

function toggleURLupload(idx) {
	var type = $('#fileInput'+idx)[0].type, html;
	if (type == 'text') {
		html = '<input id="fileInput'+idx+'" type="file" name="file[]" style="width:100%" onchange="checkExtension(this)" />';
	}
	else {
		html = '<input id="fileInput'+idx+'" type="text" name="url[]" style="width:100%" value="http://" onchange="checkURL(this)" />';
	}
	$('#fileInput'+idx).replaceWith(html);
	if (type == 'file')
	setTimeout(function(){$('#fileInput'+idx).focus()},100);
}

$(function() {
	addFileRow();
});

</script>
<form action="<?= $_SERVER['PHP_SELF'] ?>?lang=<?= $lang ?>&amp;task=addFileRes" method="post" enctype="multipart/form-data" onsubmit="$('#okBut').prop('disabled',true)">
<input type="hidden" name="id" value="<?= PH::htmlspecialchars($_GET['id']) ?>" />
<input type="hidden" name="CSRF" value="830393630353739333d2ff078607e25656274537a6" />
<fieldset><legend><?= treeWord(24 /* Select file */) ?> (Max <?= human_file_size($limit) ?>) <a href="#" onclick="addFileRow();return false"><img src="pics/file_add.png" alt="Add another file" class="icon" /></a></legend>
	<table id="filesTBL" width="100%" cellspacing="1">
	<tr><th style="border-bottom:1px solid #000">URL</th><th style="border-bottom:1px solid #000"><?= treeWord(2 /* File */) ?></th><th style="border-bottom:1px solid #000"><img src="pics/compress.png" alt="Unzip" title="Unzip" class="icon" /></th></tr>
	</table>
</fieldset>
<p>
<input type="button" style="float:right" class="but" value="<?= treeWord(26 /* Cancel */) ?>" onclick="parent.killPopup()" />
<input id="okBut" type="submit" class="but" value="<?= treeWord(25 /* Ok */) ?>" />
</p>
</form>

<?php
	}
	else if ($task == 'addFileRes') {
		if (!empty($_FILES['file'])) {
			$res1 = $jsTree->addFiles($_POST['id'], $_FILES['file'], isset($_POST['unzip']) ? $_POST['unzip'] : array(), $errors1);
		}
		else {
			$res1 = true; $errors1 = array();
		}
		if (!empty($_POST['url'])) {
			$res2 = $jsTree->downloadURLs($_POST['id'], $_POST['url'], $errors2);
		}
		else {
			$res2 = true; $errors2 = array();
		}
		$errors = $errors1 + $errors2;
		if ($res1 && $res2) {
			if (!empty($errors)) {
				print '<fieldset><b class="error">' . implode('<br />', $errors) . '</b></fieldset><p align="right"><button type="button" onclick="parent.killPopup()">' . treeWord(28 /* Back */) . '</button></p>';
			}
			else {
				print '<script type="text/javascript">' . "\n";
				print "parent.$('#treeDiv').jstree('refresh',$('#{$_POST['id']}'));\n";
				print "parent.killPopup();\n";
				print "</script>\n";
			}
		}
		else {
			print '<fieldset><b class="error">' . implode('<br />', $errors) . '</b></fieldset><p align="right"><button type="button" onclick="parent.killPopup()">' . treeWord(28 /* Back */) . '</button></p>';
		}
	}
?>
</body>
</html>
