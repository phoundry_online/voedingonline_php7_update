<?php
	require 'jsTree.class.php';

	if (!isset($_SESSION['jsTree'])) {
		die("Variable \$_SESSION['jsTree'] is not set!");
	}

	$file = str_replace('..','',rawurldecode($_SERVER['QUERY_STRING']));

	if (substr($file, 0, strlen($_SESSION['jsTree']['rootUrl'])) != $_SESSION['jsTree']['rootUrl']) {
		die("Illegal request.");
	};
	$file = $_SESSION['jsTree']['rootDir'] . preg_replace('/^' . preg_quote($_SESSION['jsTree']['rootUrl'], '/') . '/', '', $file);

	if (!file_exists($file)) {
		die("File {$file} does not exist.");
	}

	header('Content-type: ' . getContentType($file));
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Disposition: attachment; filename=" . preg_replace('/_+/', '_', preg_replace('/\W/', '_', basename($file))));
	$fp = fopen($file, 'r');
	fpassthru($fp);

function getContentType($filename) {

   $mimeTypes = array(
      'doc' => 'application/msword',
      'zip' => 'application/zip',
      'xls' => 'application/vnd.ms-excel',
      'ppt' => 'application/vnd.ms-powerpoint',
      'gif' => 'image/gif',
      'jpg' => 'image/jpeg',
      'jpeg' => 'image/jpeg',
      'bmp' => 'image/bmp',
      'png' => 'image/png',
      'tif' => 'image/tiff',
      'tiff' => 'image/tiff',
      'pdf' => 'application/pdf',
      'hqx' => 'application/mac-binhex40',
      'swf' => 'application/x-shockwave-flash',
      'sit' => 'application/x-stuffit',
      'mp3' => 'audio/mpeg',
      'html' => 'text/html',
      'htm' => 'text/html',
      'txt' => 'text/plain',
      'xml' => 'text/xml',
      'eml' => 'message/rfc822',
      'csv' => 'text/csv'
   );

   $extension = strtolower(substr($filename, strrpos($filename, '.')+1));
   return isset($mimeTypes[$extension]) ? $mimeTypes[$extension] : 'application/octet-stream';
}
