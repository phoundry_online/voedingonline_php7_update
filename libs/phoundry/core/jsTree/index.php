<?php
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	require 'jsTree.class.php';

	if (!isset($_SESSION['jsTree'])) {
		die("Variable \$_SESSION['jsTree'] is not set!");
	}
	$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
	$MODE = isset($_GET['mode']) ? $_GET['mode'] : 'explorer';

	$isShared = isset($_SESSION['jsTree']['shared']);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?= treeWord(59 /* Explorer */) ?></title>

<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="jquery.jstree.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.hotkeys.min.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.cookie.pack.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.inlineWindow.min.js"></script>
<script type="text/javascript" src="base64.js"></script>
<script type="text/javascript" src="jquery.Jcrop.pack.js"></script>

<!-- INITIALIZE -->
<script type="text/javascript">

var winArgs = [], curPath;
if (window.dialogArguments) {
	winArgs = window.dialogArguments;
}
else if (window.opener && window.opener.winArgs) {
	winArgs = window.opener.winArgs;
}
else if (window.parent && window.parent.winArgs) {
	winArgs = window.parent.winArgs;
}

$(function()
{
	$.ajaxSetup({
		cache: false
	});

	// Size treeDiv:
	$(window)
    .bind('resize', function(){
        var $this = $(this);
        var height = $this.height();
        if (height == 0) { height = document.body.offsetHeight; }
        var width = $this.width();
        if (width == 0) { width = document.body.offsetWidth; }
        $('#treeDiv').css({height:(height-<?= $MODE=='image'?53:27?>)+'px',width:(width-200)+'px'});
        $('#img_props').css({height:(height-<?= $MODE=='image'?53:27?>-10)+'px'});
    })
	.trigger('resize');

	var dir = '<?= strtr(base64_encode($treePREFS['rootUrl']), '+/=', '*_-') ?>';

	// Set the Ajax Spinner
	$('#throbber').
		ajaxStart(function(){ $(this).show(); }).
		ajaxStop(function(){ $(this).fadeOut(); });


	$('#treeDiv').jstree({
		core: {
			animation: 0
		},
		themes: {
			theme: 'phoundry',
			dots: true,
			icons: true
		},
		cookies: {
			save_opened: dir+'_open',
			save_selected: false
		},
		ui: {
			'initially_select': ['#Lw--'] // Root element (/)
		},
		xml_data: {
			xsl: 'nest',
			ajax: {
				url: 'jsTree.php',
				data: function(n) {
					return {
						id:n.attr ? n.attr('id') : 0,
						_rnd:new Date().getTime()
					}
				}
			}
		},
		crrm: {
			move: {
				check_move: function(m) {
					if (
						m.np.attr('rel') !== 'folder' || // You can't drop on a non-folder
						m.p == 'before' ||	// Moving 'before' does not make sense in dirs
						m.p == 'after'			// Moving 'after' does not make sense in dirs
					) { return false; }
					return true;
				}
			}
		},
		hotkeys : {
			'del' : function() { delFile(); },
			'f2' : function() { renameFile(); },
			'shift+n': function() { addFolder(); },
			'n': function() { addFile(); },
			'h': function() { makePopup(360,260,'<?= treeWord(21 /* Help */) ?>','jsTree.php?lang=<?= $lang ?>&task=help'); }
		},
		plugins: [
			'themes', 'xml_data', 'ui', 'dnd', 'crrm', 'hotkeys', 'cookies'
		]
	})
	.bind('create.jstree', function(e, treeData) {
		var newName = treeData.rslt.name;
		$.ajax({url:'jsTree.php', data:{lang:'<?= $lang ?>',task:'addDir', id:treeData.rslt.parent.attr('id'), newName:newName}, dataType:'json', success:function(data) {
			if (data === false) {
				// Rollback:
				$.jstree.rollback(treeData.rlbk);
				alert('<?= treeWord(29 /* Folder could not be created */) ?>'.replace(/#1/, newName));
			}
			else {
				// Update node id:
				$(treeData.rslt.obj).attr('id', data.path);
			}
		}});
	})
	.bind('click.jstree', function(e) {
		//var node = $.jstree._reference('#treeDiv')._get_node(null,true);
		var allowed = '<?= implode('|', $treePREFS['selectExtensions']) ?>',
			 node = $.jstree._focused().get_selected();

		if (!node) return;

		var type = node.attr('rel'),
			 path = Base64.decode(node.attr('id') || '');
		curPath = path;

		viewFile(path, node.attr('title'));

		$('.forFile').toggle(type !== 'folder');
		$('.forDir').toggle(type === 'folder');

		if (allowed.length > 0) {
			var re = new RegExp(allowed+'$', 'i');
			if (!re.test(path)) {
				$('#useFile').hide();
			}
		}
	})
	.bind('dblclick.jstree', function(e) {
		var node = $.jstree._focused().get_selected();
		if (!node) return;

		var type = node.attr('rel'),
			 path = Base64.decode(node.attr('id') || '');
		curPath = path;
		if (type != 'folder') {
			useFile();
		}
		else {
			//$('#treeDiv').jstree('toggle_node', node);
			$(this).jstree('toggle_node', node);
		}
	})
	.bind('delete_node.jstree', function(e, treeData) {
		var node = treeData.rslt.obj,
			 type = node.attr('rel'),
			 id = node.attr('id');

		$.ajax({url:'jsTree.php', data:{lang:'<?= $lang ?>',task:'delete', id:id}, dataType:'json', success:function(data) {
			if (data === false) {
				// Rollback:
				$.jstree.rollback(treeData.rlbk);
				alert(type == 'folder' ? '<?= treeWord(19 /* Folder could not be deleted */) ?>' : '<?= treeWord(17 /* File could not be deleted */) ?>');
			}
			else {
				curPath = null;
				$.jstree._focused().deselect_all();
				$('.forFile, .forDir').toggle(false);
			}
		}});
	})
	.bind('rename.jstree', function(e, treeData) {
		var node = treeData.rslt.obj,
			 type = node.attr('rel'),
			 id = node.attr('id'),
			 newName = treeData.rslt.new_name;

		if (/[#\/\?\*\:\\]/.test(newName)) {// Invalid filename
			$.jstree.rollback(treeData.rlbk);
			alert('<?= treeWord(22 /* Invalid filename */) ?>');
			return false;
		}
		$.ajax({url:'jsTree.php', data:{lang:'<?= $lang ?>',task:'rename', id:id, newName:newName}, dataType:'json', success:function(data)
		{
			if (data === false) {
				// Rollback:
				$.jstree.rollback(treeData.rlbk);
				alert('<?= treeWord(27 /* Renaming failed */) ?>');
			}
			else {
				// Update node id:
				$(treeData.rslt.obj).attr('id', data.path);
				$.jstree._focused().set_text(node, data.name);
			}
		}});
	})
	.bind('move_node.jstree', function(e, treeData) {
		var src = treeData.rslt.o.attr('id'),
			 dst = treeData.rslt.np.attr('id'),
			 $this = $(this);
		//alert('from:'+Base64.decode(src)+' to:'+Base64.decode(dst));
		$.ajax({url:'jsTree.php', data:{lang:'<?= $lang ?>', task:'move', id:src, dst:dst}, dataType:'json', success:function(data) {
			if (data === false) {
				// Rollback:
				$.jstree.rollback(treeData.rlbk);
				alert('<?= treeWord(15 /* Moving failed */) ?>');
			}
			else {
				// Refresh branch we moved to:
				$this.jstree('refresh',treeData.rslt.np);
			}
		}});
	});

	setTimeout(function(){
		window.focus();
	}, 100);	// Focus window, in order for keyboard to work
});

function viewFile(src, title)
{
	var wh, w, h, factor, html, orgSrc = src.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');

	// Reset preview screen
	$('#preview').html('').data('download-link', null).data('edit-image', null);
	$('.forImage').prop('disabled', true);

	if (!/^https?:\/\//i.test(src)) {
		src = '<?= $treePREFS['rootUrl'] ?>'+encodeURI(src);
	}

	if (title && (wh = title.match(/^(\d+)x(\d+) pixels/))) {
		w = ww = www = parseInt(wh[1],10);
		h = hh = hhh = parseInt(wh[2],10);
		if (w > 170 || h > 170) {
			factor = Math.max(w,h)/170;
			ww = Math.floor(w/factor);
			hh = Math.floor(h/factor);
		}
		if (/\.swf$/i.test(src)) {
			$('#preview').html('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" width="'+ww+'" height="'+hh+'"><param name="movie" value="' +src+'" /><param name="quality" value="high" /><embed src="'+src+'" quality="high" width="'+ww+'" height="'+hh+'" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed></object>').
				data('download-link', 'download.php?'+src);
		}
		else {
			// Determine size of popup, needs to be constrained within window size (465*288)
			if (w > 465 || h > 288) {
				factor = Math.max(w/465,h/288);
				www = Math.floor(w/factor)+20;
				hhh = Math.floor(h/factor)+30;
			}
			else {
				www = w+20; hhh = h+30;
			}
			html = '<img src="'+src+'?'+Math.random()+'" class="ptr" style="visibility:hidden" border="0" alt="'+title+'" onload="sizeThumb(this)" onclick="makePopup('+www+','+hhh+',\'<?= treeWord(13 /* Preview */) ?>\',this.src)" />';
			if (!/^https?:\/\//i.test(src) && <?= $isShared ? 'false' : 'true' ?>) {
				$('#preview').data('edit-image', {width : w, height : h, src : orgSrc});
				$('.forImage').prop('disabled', false);
			}
			$('#preview').html(html).data('download-link', 'download.php?'+src);
		}
	}
	else if (src != '' && !/\/$/.test(src)) {
		html = '<a href="download.php?'+src+'" class="downloadLink">' + basename(unescape(src)) + '</a>';
		$('#preview').html(html).data('download-link', 'download.php?'+src);
	}
}

function sizeThumb(img)
{
	var w = img.width, h = img.height, factor;
	if (w > 170 || h > 170) {
		factor = Math.max(w,h)/170;
		ww = Math.floor(w/factor);
		hh = Math.floor(h/factor);
	}
	img.width = ww; img.height = hh;
	$(img).css('visibility','visible');
}

function editImage()
{
	var args = $('#preview').data('edit-image'), src;
	if (args) {
		src = args.src
			   .replace(/&quot;/g, '"')
			   .replace(/&lt;/g, '<')
				.replace(/&gt;/g, '>')
				.replace(/&amp;/g, '&');
		src = encodeURI(src).replace(/\+/g, '%2B');
		$('<div id="editDiv" style="position:absolute;top:0;left:0;width:100%;height:100%;background:#fff"></div>').
			appendTo('body').
			load('editImage.php?lang=<?=$lang?>&w='+args.width+'&h='+args.height+'&path='+src);
	}
}

function downloadFile()
{
	var link = $('#preview').data('download-link');
	if (link) {
		window.location = link;
	}
}

function addFolder()
{
	<?php if ($treePREFS['allowFolders']) { ?>
	$('#treeDiv').jstree('create',null,0,{attr:{rel:'folder'},state:'closed',data:'New folder'});
	<?php } ?>
}

function addFile()
{
	<?php if ($treePREFS['allowFiles']) { ?>
	var node = $.jstree._focused().get_selected();
	makePopup(360, 260, '<?= treeWord(6 /* Upload file */) ?>', 'jsTree.php?lang=<?= $lang ?>&task=addFile&id='+escape(node.attr('id'))+'&_rnd='+Math.random());
	<?php } ?>
}

function delFile()
{
	var node = $.jstree._focused().get_selected();
	if (!node[0]) return;

	var type = node.attr('rel'),
		path = Base64.decode(node.attr('id') || ''),
   		msg = (type == 'folder') ? '\n<?= treeWord(18 /* Subfolders are deleted too */) ?>' : '';

	if (<?= $treePREFS['allowFolders'] ? 'true' : 'false' ?> && type == 'folder' ||
	    <?= $treePREFS['allowFiles']   ? 'true' : 'false' ?> && type != 'folder')
	{
	   	if (path == '/') {
	      	alert('<?= treeWord(12 /* You cannot delete the root */) ?>');
	      	return;
	   	}

		if (!confirm('<?= treeWord(16 /* Are you sure */) ?>'.replace('#1', path)+msg)) {
			return;
		}

<?php if (!empty($PHprefs['enableUploadBrowserDeleteCheck'])):?>
		$.ajax({
			url : '<?=$PHprefs['url']?>/core/check_urls/ajax_check_url.php',
			dataType : 'json',
			data : {
				link : gatherLinks(node),
				protocol : <?=json_encode($protocol)?>,
				domain : <?=json_encode($_SERVER['HTTP_HOST'])?>
			},
			success : function(data) {
				if (data.found_urls && data.found_urls > 0) {
					if (!confirm(<?=json_encode(treeWord(61))?>)) {
						window.parent.urlCheckInitialLoad = data;
						checkUrl();
					}
				} else {
					$('#treeDiv').jstree('delete_node',null);
				}
			}
		});
<?php else: ?>
		$('#treeDiv').jstree('delete_node',null);
<?php endif; ?>
	}
}

function checkUrl()
{
	var nodes = $.jstree._focused().get_selected();
	if (!nodes[0]) return;

	var url = '<?=$PHprefs['url']?>/core/popups/urlCheck.php?<?=http_build_query(
	array(
		'_protocol' => $protocol,
		'_domain' => $_SERVER['HTTP_HOST']
	)
)?>';

	$.each(gatherLinks(nodes), function(i, link) {
		url = url + '&_link[]='+link;
	});

	window.parent.makePopup(
		null,
		-20,
		-20,
		'<?= escSquote(word(522)) ?>',
		url,
		null,
		true
	);
	return;
}

function gatherLinks(nodes)
{
	var dir = (<?=json_encode($treePREFS['rootUrl'])?>),
		links = [];
	nodes.each(function() {
		var node = $(this),
			type = node.attr('rel'),
			path = Base64.decode(node.attr('id') || '');
		if (type === 'file') {
			links.push(dir+path)
		} else if (type === 'folder') {
			links = links.concat(gatherLinks(node.find('> ul > li')));
		}
	});
	return links;
}

function renameFile()
{
	var node = $.jstree._focused().get_selected(),
		 type = node.attr('rel'),
		 path = Base64.decode(node.attr('id') || '');

	if (<?= $treePREFS['allowFolders'] ? 'true' : 'false' ?> && type == 'folder'  ||
	    <?= $treePREFS['allowFiles']   ? 'true' : 'false' ?> && type != 'folder')
	{
		if (path == '/') {
			alert('<?= treeWord(20 /* You cannot rename the root */) ?>');
			return false;
		}

		$('#treeDiv').jstree('rename');
	}
}

/*
function dirname(path) {
	return path.substring(0,path.lastIndexOf('/'));
}
*/

function basename(path)
{
	return path.substring(path.lastIndexOf('/')+1);
}

function makePopup(width, height, title, url) {
	while(killPopup());

	var popup = $.fn.inlineWindow({
		windowTitle : title,
		url : url,
		width : width,
		height : height,
		content : null,
		event : null,
		modal : false
	});

	if (!window.popup) {
		window.popup = popup;
	} else {
		if ("close" in window.popup) {
			window.popup = [window.popup];
		}
		window.popup.push(popup);
	}
	return popup;
}

function killPopup() {
	if (!window.popup) {
		return false;
	}
	var popup;
	if ("close" in window.popup) {
		popup = window.popup;
		window.popup = null;
	} else {
		popup = window.popup.pop();
	}
	popup.close();
	window.focus();
	return true;
}

function setPaneColor(color)
{
	$('#preview').css({background:color});
}

function allowedImageSize(what, size) {
	var allowed = (what == 'width') ? <?= isset($_SESSION['jsTree']['allowedWidth']) ? "'" . $_SESSION['jsTree']['allowedWidth'] . "'" : 'null' ?> : <?= isset($_SESSION['jsTree']['allowedHeight']) ? "'" . $_SESSION['jsTree']['allowedHeight'] . "'" : 'null' ?>, range, msg = (what == 'width') ? '<?= escSquote(treeWord(30)) ?>' : '<?= escSquote(treeWord(31)) ?>';

	if (!allowed) {
		// No restrictions on size:
		return '';
	}

	if (allowed.indexOf('-') != -1) {
		range = allowed.split('-');
		if (size < range[0] || size > range[1]) {
			// Image width is not in allowed range.
			return msg.replace('#1', size).replace('#2', range[0] + '-' + range[1]);
		}
	}
	else if (/\d+/.test(allowed)) {
		if (size != allowed) {
			return msg.replace('#1', size).replace('#2', allowed);
		}
	}
	return '';
}

function useFile()
{
	var title = $('#preview img').attr('alt'), wh, check;
	if (!title) title = '';

	if (wh = title.match(/^(\d+)x(\d+) pixels/)) {
		check = allowedImageSize('width', parseInt(wh[1],10));
		if (check != '') {
			alert(check); return;
		}
		check = allowedImageSize('height', parseInt(wh[2],10));
		if (check != '') {
			alert(check); return;
		}
	}

	<?php if ($MODE == 'media_browser') { ?>

	window.parent.setUrl('<?= $treePREFS['rootUrl'] ?>'+curPath);

	<?php } else if ($MODE == 'explorer') { ?>

	if (winArgs && winArgs['callback']) {
		winArgs['callback']({url:'<?= $treePREFS['rootUrl'] ?>'+curPath});
	}
	else {
		var fn = '<?= str_replace("'", "\\'", $treePREFS['useFileFunction']) ?>'.replace('{$path}', '<?= $treePREFS['rootUrl'] ?>'+curPath.replace(/'/g, "\\'"));
		try { eval(fn) } catch(e) { eval('parent.'+fn); };
	}

	<?php } else if ($MODE == 'image') { ?>

	// Show properties dialogue:
	if (/\.(png|gif|jpg|jpeg)$/i.test(curPath)) {
		var readableSrc = basename(curPath);
		$('#img_src').text(readableSrc.length > 35 ? readableSrc.substr(0,35)+'...' : readableSrc);
		$('#img_width,#img_orgWidth').val(wh[1]);
		$('#img_height,#img_orgHeight').val(wh[2]);
		$('#img_props').fadeIn();
		$('#buttons,#loadDiv').fadeOut();
	}

	<?php } else if ($MODE == 'attachment') { ?>

	var retval = {url:'<?= $treePREFS['rootUrl'] ?>' + curPath}, target = $('#attachment_target').val();
	if (target) { retval['target'] = target; }
	if (winArgs['callback']) {
		winArgs['callback'](retval);
	}
	window.close();

	<?php } ?>
}

/*** For images & swf ***/

function checkDigit(nr, allowPercent)
{
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function checkAspect(elem, what, value)
{
	if (!$('#'+elem+'_keepAspect')[0].checked) return;
	if (what == 'width') {
		$('#'+elem+'_height').val(value == '' ? '' : Math.round($('#'+elem+'_orgHeight').val() / ($('#'+elem+'_orgWidth').val()/value)));
	}
	else if (what == 'height') {
		$('#'+elem+'_width').val(value == '' ? '' : Math.round($('#'+elem+'_orgWidth').val() / ($('#'+elem+'_orgHeight').val()/value)));
	}
}

function loadUrl(el, url)
{
	if (!/^https?:\/\//i.test(url)) return;
	el.disabled = true;
	var img = new Image();
	img.onerror = function(){el.disabled = false; alert('<?= escSquote(treeWord(34 /* Loading failed */)) ?>')};
	img.onload  = function(){
		curPath = url;
		el.disabled = false;
		img.title = img.alt = img.width + 'x' + img.height + ' pixels';
		viewFile(url, img.title);
		$('#useFile').show();
	};
	img.src = url;
}

function setImgProps()
{
	var val, attrib, attribs = ['src','align','alt','title','width','height','border','vspace','hspace','class'], retval = {};

	if ($.trim($('#img_alt').val()) == '') {
		alert('<?= escSquote(treeWord(35 /* Enter an ALT tag */)) ?>');
		$('#img_alt').focus();
		return false;
	}

	for (attrib in attribs) {
		attrib = attribs[attrib];
		if (attrib == 'src') {
			val = /^https?:\/\//i.test(curPath) ? curPath : '<?= $treePREFS['rootUrl'] ?>' + curPath;
		}
		else if (attrib == 'title') {
			val = $('#img_alt').val();
		}
		else {
			val = $('#img_'+attrib).val();
		}

		if ($.trim(val) !== '') {
			retval[attrib] = val;
		}
	}
	if (winArgs['callback']) {
		winArgs['callback'](retval);
	}

	window.close();
}

</script>
<!-- Styles -->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="jquery.Jcrop.css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/jstree.css" type="text/css" />

<!--[if lte IE 6]>
<style type="text/css">

* html #content {
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	height: 100%;
	width: 100%;
	overflow: auto;
	border-top: 56px solid #fff;
	z-index: -1;
}

</style>
<![endif]-->

</head>
<body style="margin:0">
<?php if ($treePREFS['showTabs']) { ?>
<ul class="stabs">
<?php
	// VNU does not want shared images in Wizard
	if (!isset($DMDprefs) || $DMDprefs['identifier'] != 'vnu-wizard') {
		if ($isShared) {
			print '<li><a href="?' . QS(1,'shared=false') . '">' . treeWord(57 /* Local */) . '</li><li class="current"><a href="#" onclick="return false">' . treeWord(58 /* Shared */) . '</a></li>';
		}
		else {
			print '<li class="current"><a href="#" onclick="return false">' . treeWord(57 /* Local */) . '</a></li><li><a href="?' . QS(1,'shared=true') . '">' . treeWord(58 /* Shared */) . '</a></li>';
		}
	}
?>
</ul>
<?php } ?>

<div id="treeDiv"></div>

<?php if ($MODE == 'image') { ?>
<div id="loadDiv">
	<form style="margin:0;padding:0">
		<input type="text" id="remoteUrl" style="width:315px" /><button type="button" style="width:75px" onclick="loadUrl(this,$('#remoteUrl').val());return false"><?= treeWord(36 /* Load URL */) ?></button>
	</form>
</div>
<?php } ?>

<div id="preview"></div>

<div id="buttons">
	<div class="icon-buttons">
<?php if ($treePREFS['allowFolders']) { ?>
	<button type="button" class="forDir delete" onclick="delFile()" title="<?= treeWord(5 /* Delete folder */) ?>"><img class="icon" src="pics/folder_delete.png" alt="<?= treeWord(5 /* Delete folder */) ?>" /></button>
	<button type="button" class="forDir rename" onclick="renameFile()" title="<?= treeWord(11 /* Rename folder */) ?>"><img class="icon" src="pics/textfield_rename.png" alt="<?= treeWord(11 /* Rename folder */) ?>" /></button>
	<button type="button" class="forDir add" onclick="addFolder()" title="<?= treeWord(4 /* Add folder */) ?>"><img class="icon" src="pics/folder_add.png" alt="<?= treeWord(4 /* Add folder */) ?>" /></button>
	<?php if ((!isset($PHprefs['product']) || $PHprefs['product'] == 'Phoundry')) { ?>
	<button type="button" class="forDir checkurl" onclick="checkUrl()" title="<?=word(522)?>"><img class="icon" src="<?=$PHprefs['url']?>/core/icons/page_find.png" alt="<?= word(522) ?>" /></button>
	<?php } ?>
<?php } ?>
<?php if ($treePREFS['allowFiles']) { ?>
	<button type="button" class="forDir add-file" onclick="addFile()" title="<?= treeWord(6 /* Add file */) ?>"><img class="icon" src="pics/file_add.png" alt="<?= treeWord(6 /* Add file */) ?>" /></button>
	<button type="button" class="forFile delete" style="display: none;" onclick="delFile()" title="<?= treeWord(7 /* Delete file */) ?>"><img class="icon" src="pics/file_delete.png" alt="<?= treeWord(7 /* Delete file */) ?>" /></button>
	<button type="button" class="forFile rename" style="display: none;" onclick="renameFile()" title="<?= treeWord(10 /* Rename file */) ?>"><img class="icon" src="pics/textfield_rename.png" alt="<?= treeWord(10 /* Rename file */) ?>" /></button>
	<button type="button" class="forFile download" style="display: none;" onclick="downloadFile()" title="Download"><img class="icon" src="pics/disk.png" alt="Download" /></button>
	<button type="button" class="forFile forImage edit" style="display: none;" onclick="editImage()" title="<?= treeWord(38 /* edit image */) ?>"><img class="icon" src="pics/picture_edit.png" alt="<?= treeWord(38 /* edit image */) ?>" /></button>
	<?php if ((!isset($PHprefs['product']) || $PHprefs['product'] == 'Phoundry')) { ?>
	<button type="button" class="forFile checkurl" onclick="checkUrl()" title="<?=word(522)?>"><img class="icon" src="<?=$PHprefs['url']?>/core/icons/page_find.png" alt="<?= word(522) ?>" /></button>
	<?php } ?>
<?php } ?>
	</div>
	<?php if ($MODE == 'attachment') { ?>
	<table>
	<tr>
		<td align="right">Target:</td>
		<td align="right"><select id="attachment_target">
		<option value="">[<?= treeWord(33 /* none */) ?>]</option>
		<option value="_self">_self</option>
		<option value="_parent">_parent</option>
		<option value="_top">_top</option>
		<option value="_blank">_blank</option>
		</select></td>
	</tr>
	</table>
	<?php } ?>
	<button type="button" id="useFile" class="forFile okBut" style="width:176px;display:none;" onclick="useFile()"><?= treeWord(9 /* Use file */) ?></button>
</div>


<div id="throbber"><img src="<?= $PHprefs['url'] ?>/core/icons/throbber_large.gif" width="48" height="48" alt="Wait..." /></div>
<div id="help"><img class="icon ptr" src="<?= $PHprefs['url'] ?>/core/icons/help.png" alt="<?= treeWord(21 /* Help */) ?>" title="<?= treeWord(21 /* Help */) ?>" onclick="makePopup(360,260,'<?= treeWord(21 /* Help */) ?>','jsTree.php?lang=<?= $lang ?>&task=help')" /></div>

<?php if ($MODE == 'image') { ?>
<div id="img_props">
<form onsubmit="return false">
<input type="hidden" id="img_orgWidth" />
<input type="hidden" id="img_orgHeight" />
<h4><?= treeWord(37 /* Image properties */) ?></h4>
<table>
	<tr><th>Src:</th><td colspan="2" id="img_src"></td></tr>
	<tr><th>Align:</th>
		 <td><select id="img_align" onchange="$('#img_sample').attr('align',this.value)">
		 	<option value="">[<?= treeWord(33 /* none */) ?>]</option>
			<option value="left">left</option>
			<option value="right">right</option>
			<option value="top">top</option>
			<option value="middle">middle</option>
			<option value="bottom">bottom</option>
			<option value="absmiddle">absmiddle</option>
			<option value="baseline">baseline</option>
			<option value="texttop">texttop</option>
		</select></td>
		<td rowspan="6" style="border:1px solid silver;width:152px" valign="top">
		<img src="pics/sample.gif" alt="" width="40" height="40" id="img_sample" />Lorem ipsum dolor sit amet, morbi non quam quis libero. Donec malesuada interdum nisi.
		</td>
	</tr>
	<tr><th>ALT:</th><td><input type="text" id="img_alt" class="ipt" /></td></tr>
	<tr><th>Size:</th><td><input type="text" id="img_width" maxlength="4" style="width:28px" onchange="this.value=checkDigit(this.value,false);checkAspect('img','width',this.value)" /> x <input type="text" id="img_height" maxlength="4" style="width:28px" onchange="this.value=checkDigit(this.value,false);checkAspect('img','height',this.value)" /> px <input type="checkbox" id="img_keepAspect" checked="checked"><img class="icon" src="pics/lock.png" alt="Keep aspect" /></td></tr>
	<tr><th>Border:</th><td><input type="text" class="ipt" id="img_border" maxlength="2" value="0" onchange="this.value=checkDigit(this.value,false)" /></td></tr>
	<tr><th>Vspace:</th><td><input type="text" class="ipt" id="img_vspace" maxlength="2" onchange="this.value=checkDigit(this.value,false)" /></td></tr>
	<tr><th>Hspace:</th><td><input type="text" class="ipt" id="img_hspace" maxlength="2" onchange="this.value=checkDigit(this.value,false)" /></td></tr>
	<tr id="img_cssTR" style="visibility:hidden">
	   <th>Class:</th>
		<td></td>
	</tr>
	<tr><td></td><td><button type="button" style="width:60px" onclick="setImgProps()"><?= treeWord(25 /* Ok */) ?></button>&nbsp;<button type="button" onclick="$('#img_props').fadeOut();$('#buttons,#loadDiv').fadeIn()"><?= treeWord(26 /* Cancel */) ?></button></td></tr>
</table>
</form>
</div>
<?php } ?>
</body>
</html>
