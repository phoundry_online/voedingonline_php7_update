<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
// Ajax responses should always be in UTF8
$PHprefs['charset'] = 'utf8';

/* @var $PHprefs array */
require_once "{$PHprefs['distDir']}/core/include/common.php";
/* @var $db metabase_database_class */
/* @var $PHSes PHSes */


// Make sure the current user in the current role has access to this page:
checkAccess(null, '');

$errors = array();
if (empty($_GET['link'])) {
	$errors[] = "Missing Link argument";
} else {
	$link = (array) $_GET['link'];
}

if (empty($_GET['protocol'])) {
	$errors[] = 'Missing protocol argument';
} else {
	$protocol = $_GET['protocol'];
}

if (empty($_GET['domain'])) {
	$errors[] = 'Missing domain argument';
} else {
	$domain = $_GET['domain'];
}

$in = !empty($_GET['in']) ? $_GET['in'] : null;

if ($errors) {
	echo json_encode(array('errors' => $errors));
} else {
	require_once $PHprefs['distDir'].'/core/include/UrlCheck.php';
	
	$checker = new UrlCheck(
		$db,
		$protocol,
		$domain,
		$PHprefs['uploadUrl']
	);
	$link = $checker->prepareLinks($link);
	$res = $checker->searchLink($link, $in);
	echo json_encode(
		array(
			'links' => $link,
			'results' => $res,
			'found_urls' => count($res)
		)
	);
}
