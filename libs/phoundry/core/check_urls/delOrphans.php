<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();

	checkAccess($TID);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(17) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../csjs/global.js.php?<?= QS(1) ?>"></script>
</head>
<body class="frames">
<form action="delOrphansRes.php" method="post" onsubmit="return confirm('<?= escSquote(word(178)) ?>')">
<input type="hidden" name="outFile" value="<?= $_GET['outFile'] ?>" />
<div id="headerFrame" class="headerFrame">
<?= getHeader(word(17)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<?php
	$tmpDir = $PHprefs['tmpDir'];
	if ($fp = @fopen("$tmpDir/{$_GET['outFile']}", 'r')) {
		print '<fieldset><legend><b>' . word(143) . '</b></legend>';
		print '<div class="miniinfo">' . word(177) . '</div>';

		while(!feof($fp)) {
			$orphan = trim(fgets($fp, 2048));
			if (empty($orphan)) continue;
			print '<input type="checkbox" name="o[]" value="' . PH::htmlspecialchars($orphan) . '" checked="checked" /><a href="' . $PHprefs['uploadUrl'] . $orphan . '" class="popupWin" title="View|300|300|0">' . $orphan . "</a><br />\n";
		}
		fclose($fp);
		print '</fieldset>';
	}
	else {
		print '<p>' . word(120) . '</p>';
	}
?>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>" />
	</td>
	<td align="right">
	<input type="button" value="<?= PH::htmlspecialchars(word(46)) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
