#!/usr/bin/php -q
<?php
	if (isset($_SERVER['REMOTE_ADDR'])) exit; // This is a commandline script!
	$incDir = '';
	$email = '';
	$checks = array();
	$protocol = 'http';
	$localhost = '';
	$domain = '';
	$timeout = 10;
	$orphans = false;
	$checkCounter = 0;
	$errorCounter = 0;
	$startTime = time();
	$lang = 'en';
	$username = $password = '';
	$search = '';
	$tmpDir = '/tmp';
	$TID = 0;
	$GID = 0;
	$site_identifier = '';

	// Parse command line arguments:
	for ($x = 1; $x < $_SERVER['argc']; $x++) {
		$arg = $_SERVER['argv'][$x];
		if (strpos($arg,'=') !== false)
			list($name,$value) = explode('=', $arg);
		else
			$name = $arg;
		switch($name) {
			case '-localhost':
				$localhost = $value;
				break;
			case '-dir':
				$incDir = $value;
				break;
			case '-tid':
				$TID = $value;
				break;
			case '-email':
				$email = $value;
				break;
			case '-proto':
				$protocol = $value;
				break;
			case '-domain':
				$domain = $value;
				break;
			case '-checks':
				$checks = explode('|', $value);
				break;
			case '-lang':
				$lang = $value;
				break;
			case '-orphans':
				$orphans = ($value == 1) ? true : false;
				break;
			case '-gid':
				$GID = $value;
				break;
			case '-user':
				$username = $value;
				break;
			case '-pass':
				$password = $value;
				break;
			case '-search':
				$search = $value;
				break;
			case '-tmp':
				$tmpDir = realpath($value);
				break;
			case '-site_identifier':
				$site_identifier = $value;
				break;
		}
	}

	require_once $incDir . '/PREFS.php';
	require_once "{$PHprefs['distDir']}/core/include/common_LC.php";
	include_once "{$PHprefs['distDir']}/core/lang/{$lang}/words.php";

	$db = LC::DBconnect();

	function isUploadedUrl($url) {
		global $protocol, $domain, $PHprefs;

		if (strpos($url, $protocol . '://' . $domain . $PHprefs['uploadUrl']) !== false) {
			return true;
		}
		return false;
	}

	function debug($msg) {
		print $msg;
	}

	function errorMessage($fieldName, $fieldDesc, $url, $TID, $RID, $errMsg) {
		global $PHprefs, $protocol, $domain, $errorCounter, $site_identifier;
		$errorCounter++;

		$phoundryURL =  $protocol . '://' . $domain . $PHprefs['url'] . '/core/update.php?TID=' . $TID . '&RID=' . $RID;
		if (!empty($site_identifier)) {
			$phoundryURL .= '&site_identifier=' . urlencode($site_identifier);
		}
		$phoundryURL .= '#' . $fieldName;

		$msg  = '<dl>';
		$msg .= '<dt><a href="' . $phoundryURL . '">' . $fieldDesc . '</a>: <a href="' . $url . '" target="mail">' . (strlen($url) > 120 ? substr($url,0,120) . '...' : $url) . '</a></dt>';
		$msg .= '<dd>' . $errMsg . '</dd>';
		$msg .= "</dl>\n";
		return $msg;
	}

	function urlencodeURL2($url) {
		$parts = array('scheme'=>'', 'host'=>'', 'port'=>'', 'user'=>'', 'pass'=>'', 'path'=>'','query'=>'', 'fragment'=>'');
		if (preg_match('|^https?://$|i', $url)) 
			return $url;
		$parts = array_merge($parts, parse_url($url));
		if (!empty($parts['scheme']))
			$parts['scheme'] .= '://';
		if (!empty($parts['port']))
			$parts['port'] = ':' . $parts['port'];
		if (!empty($parts['query']))
			$parts['query'] = '?' . $parts['query'];
		if (!empty($parts['fragment']))
			$parts['fragment'] = '#' . $parts['fragment'];
		$dirs = explode('/', $parts['path']);
		for ($i = 0; $i < count($dirs); $i++) {
			// Only rawurlencode if it isn't already.
			if (strpos($dirs[$i], '%') === false)
				$dirs[$i] = rawurlencode($dirs[$i]);
		}
		$res = $parts['scheme'] . $parts['host'] . $parts['port'] . implode('/', $dirs) . $parts['query'] . $parts['fragment'];
		return $res;
	}

	function doCurlRequest($url, $timeout = 10, $username = '', $password = '', $noBody = true) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)');
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		if (!empty($username) || !empty($password)) {
			curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
		}

		if ($noBody) {
			curl_setopt($ch, CURLOPT_NOBODY,1);
		}
		curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$error = curl_error($ch);
		curl_close($ch);

		if ($status != 0 && $status != 404 && !($status >= 200 && $status < 300) && $noBody) {
			return doCurlRequest($url, $timeout, $username, $password, false);
		}

		return array($status, $error);
	}

	$URLcache = array();
	function checkUrl($url, $timeout = 5, &$msg) {
		global $checkCounter, $URLcache, $username, $password;
		$checkCounter++;

		$url = urlencodeURL2($url);

		if (isset($URLcache[$url])) {
			print " (cached) ";
			$check = $URLcache[$url];
			$msg = $check[1];
			return $check[0];
		}

   	$http_status = array(
			100=>'Continue',
			101=>'Switching protocols',
			200=>'Successful',
			201=>'Created',
			202=>'Accepted',
			203=>'Non-Authorative Information',
			204=>'No content',
			205=>'Reset content',
			206=>'Partial Content',
			300=>'Multiple Choices',
			301=>'Moved Permanently',
			302=>'Moved Temporarily',
			303=>'See Other',
			304=>'Not Modified',
			305=>'Use Proxy',
			400=>'Bad Request',
			401=>'Unauthorized',
			402=>'Payment Required',
			403=>'Forbidden',
			404=>'Not Found',
			405=>'Method Not Allowed',
			406=>'Not Acceptable',
			407=>'Proxy Authentication Required',
			408=>'Request Timeout',
			409=>'Conflict',
			410=>'Gone',
			411=>'Length Required',
			412=>'Precondition Failed',
			413=>'Request Entity Too Long',
			414=>'Request-URI Too Long',
			415=>'Unsupported Media Type',
			500=>'Internal Server Error',
			501=>'Not Implemented',
			502=>'Bad Gateway',
			503=>'Service Unavailable',
			504=>'Gateway Timeout',
			505=>'HTTP Version Not Supported'
		);

		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)');
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_NOBODY,1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		if (!empty($username) || !empty($password)) {
			curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
		}
		curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$error = curl_error($ch);
		curl_close($ch);
		

		list($status, $error) = doCurlRequest($url, $timeout, $username, $password);
		if (empty($error)) {
			$msg = $status . ': ' . (array_key_exists($status, $http_status) ? $http_status[$status] : 'Invalid HTTP status code');
			$URLcache[$url] = array($status >= 200 && $status < 300, $msg);
			return ($status >= 200 && $status < 300);
		}
		else {
			$msg = $error;
			$URLcache[$url] = array(false, $msg);
			return false;
		}
	}

	// Create lockfile:
	$lockFile = $tmpDir . '/PHCL' . $domain;
	print "LOCKFILE: $lockFile\n";
	if ($fp = fopen($lockFile,'w')) {
		//file_put_contents("/var/www/voedingonline/tmp/locker.txt", "is locked");
		print "LOCKED!\n";
		fwrite($fp, time() . "\n");
		fclose($fp);
	}

	$tables = array();
	$body = '';

	foreach($checks as $check) {
		$tmp = explode(':', $check);
		$tables[$tmp[0]] = explode(',', $tmp[1]);
	}

	$allURLs = array();

	foreach($tables as $TID=>$CIDS) {
		$tableErrors = $tableChecks = 0;

		$TID = (int)$TID;

		//
		// Determine TABLE properties:
		//
		$sql = "SELECT name, description FROM phoundry_table WHERE id = $TID";
		$cur = $db->Query($sql)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql\n");
		$tableName = $db->FetchResult($cur,0,'name');
		$tableDesc = LC::langWord($db->FetchResult($cur,0,'description'), $lang);
		// Determine primary key field:
		$db->GetTableIndexDefinition($tableName, 'PRIMARY', $index)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "\n");
		$tableKey = array_keys($index['FIELDS']);
		$tableKey = $tableKey[0];

		//
		// Any system filters on this table?
		//
		$ands = array();
		$sql = "SELECT wherepart FROM phoundry_filter WHERE table_id = $TID AND type = 'system'";
		$cur = $db->Query($sql)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql\n");
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$filter = $db->FetchResult($cur,$x,'wherepart');
			if (preg_match('|\{\$([a-z][\w]+)\}|i', $filter)) continue;
			$ands = array_merge($ands, LC::getFilter(unserialize($filter)));
		}

		$body .= '<span class="legend">' . $tableDesc . '</span><div class="fieldset">';

		//
		// Determine COLUMN properties:
		//
		$colProps = array();
		$sql = "SELECT * FROM phoundry_column WHERE id IN (" . escDBquote(implode(',', $CIDS)) . ")";
		$cur = $db->Query($sql)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql\n");
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$colProps[$db->FetchResult($cur,$x,'name')] = 
				array('id'=>$db->FetchResult($cur,$x,'id'),
				      'desc'=>LC::langWord($db->FetchResult($cur,$x,'description'), $lang),
				      'datatype'=>$db->FetchResult($cur,$x,'datatype'));
		}

		$sql = "SELECT $tableKey";
		foreach($colProps as $name=>$props)
			$sql .= ', ' . $name;
		$sql .= " FROM $tableName";
		if (count($ands) > 0) {
			$sql .= ' WHERE ' . implode(' AND ', $ands);
		}
		debug("SQL: $sql\n");
		$cur = $db->Query($sql)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql\n");
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$nrf = $db->NumberOfColumns($cur);
			$RID = $db->FetchResult($cur,$x,0);

			for ($y = 1; $y < $nrf; $y++) {
				$fieldName = $db->FetchColumnName($cur,$y);
				$fieldDesc = $colProps[$fieldName]['desc'];
				$columnDT = $colProps[$fieldName]['datatype'];
				if ($columnDT == 'DTurl') {
					$url = $db->FetchResult($cur,$x,$fieldName);
					if (!$url)
						continue;
					if ($url[0] == '/')
						$url = $protocol . '://' . $domain . $url;

					if (!empty($search) && strpos($url, $search) === false) {
						continue;
					}

					debug("Checking $url");

					if (isUploadedUrl($url))
						$allURLs[] = $url;
	
					$tableChecks++;
					$res = checkUrl($url, $timeout, $msg);
					if (!$res) {
						$tableErrors++;
						debug(" ... ERR: $msg\n");
						$body .= errorMessage($fieldName, $fieldDesc, $url, $TID, $RID, $msg);
					}
					else
						debug("... OK: $msg\n");
				}
				elseif ($columnDT == 'DTfile') {
					$url = $db->FetchResult($cur,$x,$fieldName);
					if (!$url)
						continue;
					$url = $PHprefs['uploadUrl'] . $url;
					if ($url[0] == '/')
						$url = $protocol . '://' . $domain . $url;

					if (!empty($search) && strpos($url, $search) === false) {
						continue;
					}

					debug("Checking $url");

					if (isUploadedUrl($url))
						$allURLs[] = $url;

					$tableChecks++;
					$res = checkUrl($url, $timeout, $msg);
					if (!$res) {
						$tableErrors++;
						debug(" ... ERR: $msg\n");
						$body .= errorMessage($fieldName, $fieldDesc, $url, $TID, $RID, $msg);
					}
					else
						debug("... OK: $msg\n");
				}
				else {
					// Fetch HREF's and SRC's:

					$dom = new DOMDocument('1.0', $PHprefs['charset']);
					$dbFetch = $db->FetchResult($cur, $x, $fieldName);
					if ($dbFetch) {
						@$dom->loadHTML($db->FetchResult($cur,$x,$fieldName));
						$xpath = new DOMXPath($dom);
						$tags = $xpath->query('//img[@src]|//a[@href]|//form[@action]');
						foreach($tags as $tag) {
							$attribs = $tag->attributes;
							$url = $attribs->getNamedItem('src');
							if (!$url) { $url = $attribs->getNamedItem('href'); }
							if (!$url) { $url = $attribs->getNamedItem('action'); }
							if (!$url) continue;
							$url = $url->nodeValue;

							// Skip URL's that start with #, ., mailto:, javascript: or https://
							if (empty($url) || preg_match('/^(?:#|\.|mailto:|mms:|javascript:|https:\/\/)/i', $url)) {
								continue;
							}
							if ($url[0] == '/') {
								$url = $protocol . '://' . $domain . $url;
							}
							if (!empty($search) && strpos($url, $search) === false) {
								continue;
							}

							debug("Checking $url");

							if (isUploadedUrl($url)) {
								$allURLs[] = $url;
							}

							$tableChecks++;
							$res = checkUrl($url, $timeout, $msg);
							if (!$res) {
								$tableErrors++;
								debug(" ... ERR: $msg\n");
								$body .= errorMessage($fieldName, $fieldDesc, $url, $TID, $RID, $msg);
							}
							else
								debug("... OK: $msg\n");
						}
					}
				}
			}
		}
		$body .= '<b>' . word(117, $tableChecks, $tableErrors) . "</b></div>\n\n";
	}

	//
	// Start checking for orphaned files in the upload directory:
	//
	
	if ($orphans) {
		// Read all files in the upload-dir:
		$DirectoriesToScan  = array($PHprefs['uploadDir']);
		$DirectoriesScanned = array();
		while (count($DirectoriesToScan) > 0) {
			foreach ($DirectoriesToScan as $DirectoryKey => $startingdir) {
				if (is_dir($startingdir) && $dir = @opendir($startingdir)) {
					while (($file = readdir($dir)) !== false) {
						// Skip system files, hidden files and thumbnails.
						if ($file[0] != '.' && !preg_match('/^thumb_/', $file)) {
							$RealPathName = $startingdir.'/'.$file;
							if (@is_dir($RealPathName)) {
								if (!in_array($RealPathName, $DirectoriesScanned) && !in_array($RealPathName, $DirectoriesToScan)) {
									$DirectoriesToScan[] = $RealPathName;
								}
							} 
							elseif (@is_file($RealPathName)) {
								$FilesInDir[] = $RealPathName;
							}
						}
					}
					closedir($dir);
				}
				$DirectoriesScanned[] = $startingdir;
				unset($DirectoriesToScan[$DirectoryKey]);
			}
		}

		$URLsInDir = array();
		foreach($allURLs as $url) {
			// Translate URL to DIR:
			$URLsInDir[] = str_replace($protocol . '://' . $domain . $PHprefs['uploadUrl'], $PHprefs['uploadDir'], rawurldecode($url));
		}

		$orphaned = array_diff($FilesInDir, $URLsInDir);
		sort($orphaned);

		$body .= '<span class="legend">' . word(34) . '</span><div class="fieldset">';
		$body .= '<form action="' . $protocol . '://' . $domain . $PHprefs['url'] . '/core/check_urls/delOrphans.php" method="get">';
		$body .= '<input type="hidden" name="TID" value="' . $TID . '" />';
		
		if (count($orphaned) > 0) {
			$outFile = 'PHCU' . $domain . uniqid('');

			$body .= '<input type="hidden" name="outFile" value="' . $outFile . '" /><ul>';

			$fp = fopen("$tmpDir/$outFile", 'w');
			foreach($orphaned as $orphan) {
				$orphanAbs = str_replace($PHprefs['uploadDir'], $protocol . '://' . $domain . $PHprefs['uploadUrl'], $orphan);
				$orphanRel = str_replace($PHprefs['uploadDir'], '', $orphan);
				debug("ORPHAN: $orphan\n");
				$body .= '<li><a href="' . urlencodeURL2($orphanAbs) . '" target="mail">' . $orphanRel . "</a></li>\n";
				fwrite($fp, "$orphanRel\n");
			}
			fclose($fp);
			$body .= '</ul><input type="submit" value="' . word(119) . '" />';
		}
		else
			$body .= '<b>' . word(35) . '!</b>';
		$body .= '</form></div>';
	}

	//
	// End checking for orphaned files in the upload directory.
	//

	$body  = '<span class="legend">' . word(58) . '</span><div class="fieldset"><b>' .
	         word(117, $checkCounter, $errorCounter) . '<br />' .
	         word(118, (time() - $startTime)) . 
				"</b></div>\n\n" . 
				$body;

	// Compose mail message with results:
	//print $body;

	// Send mail:
	LC::sendHTMLemail($email, 'Phoundry link checker', $body);

	// Remove lockfile:
	@unlink($lockFile);
?>
