<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(17) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader(word(17)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<?php
	$tmpDir = $PHprefs['tmpDir'];

	// Remove old (> 1 hr) import files that might be wandering around...
	if (is_dir($tmpDir)) {
		$handle = opendir($tmpDir);
		while(($file = readdir($handle)) !== false) {
			if (preg_match("/^PHCU{$_SERVER['HTTP_HOST']}/", $file)) {
				if (time() - 3600 > filemtime("$tmpDir/$file"))
					@unlink("$tmpDir/$file");
			}
		}
		closedir($handle);

		foreach($_POST['o'] as $delFile) {
			print 'Deleting ' . $delFile . '... ';
			$delFile = str_replace('..', '', $delFile);
			$delFile = $PHprefs['uploadDir'] . $delFile;
			if (@unlink($delFile))
				print 'Succes.';
			else
				print '<b class="req">Failed!</b>';
			print '<br />';


			$outFile = str_replace(array('..', '/'), array('',''), $_POST['outFile']);
			@unlink("$tmpDir/$outFile");
		}
	}
?>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="button" class="okBut" value="<?= PH::htmlspecialchars(word(82)) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
	<td align="right">
	<input type="button" value="<?= PH::htmlspecialchars(word(46)) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
</tr></table>
</div>

</body>
</html>
