<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, '');

	if (!function_exists('curl_init'))
		trigger_error('This PHP-configuration is not build with CURL support (--with-curl). The URL-checker will not work without it.', E_USER_ERROR);

	 // See if there's a lockfile:
	$lockFile = $PHprefs['tmpDir'] . '/PHCLwww.voedingonline.nl'; // . $_SERVER['HTTP_HOST'];

	if (file_exists($lockFile)) {
		PH::phoundryErrorPrinter(word(122),true);
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= word(11) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../../core/csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">
//<![CDATA[

function checkDTemail(desc, value) {
	value = value.replace(/ /g, '');
	var errmsg = '';
	emails = value.split(',');
	for (var x = 0; x < emails.length; x++)
		if (!/^([-!#\$&%'*+.\/0-9=?A-Z^_`{|}~])+@((\w([-!#\$%'*+\/0-9=?A-Z^_`{|}~]*\.)+[A-Z]{2,6})|(\[([0-9]{1,3}\.){3}[0-9]{1,3}\]))$/i.test(emails[x]))
			errmsg += '<?= escSquote(word(36)) ?>'.replace(/#1/, desc);
	return errmsg;
}

function setOrphans(F, state) {
	var el, x;
	for (x = 0; x < F.elements.length; x++) {
		el = F.elements[x];
		if (el.type == 'checkbox') {
			if (state)
				el.checked = true;
			el.disabled = state;
		}
	}
}

function startURLcheck() {
	var url, F = _D.forms[0], orphans = (F['orphans'] && F['orphans'][1].checked) ? 1 : 0;
	if (!checkForm(F))
		return;

	var args = {'TID':<?= $TID ?>, 'orphans':orphans, 'email':F['email'].value, 'username':F['username'].value, 'password':F['password'].value};
	<?php if (isset($_GET['site_identifier'])) { ?>
		args['site_identifier'] = '<?= $_GET['site_identifier'] ?>';
	<?php } ?>
	for (var x = 0; x < F.elements.length; x++) {
		if (F.elements[x].type == 'checkbox' && F.elements[x].checked) {
			//alert(F.elements[x].name+':'+F.elements[x].value);
			if (args[F.elements[x].name]) {
				args[F.elements[x].name] += ',' + F.elements[x].value;
			}
			else {
				args[F.elements[x].name] = F.elements[x].value;
			}
		}
	}

	$.post('check.php', args, function(msg) {
		alert(msg);
		if (!/^ERROR/.test(msg)) {
			_D.location = '<?= $PHprefs['customUrls']['home'] ?>';
		}
	});
}

function checkFormExtra(F) {
// Make sure at least one checkbox is checked.
	var el;
	for (var x = 0; x < F.elements.length; x++) {
		el = F.elements[x];
		if (el.type == 'checkbox') {
			if (el.checked)
				return true;
		}
	}
	alert(words[20]);
	return false;
}

//]]>
</script>
</head>
<body class="frames">
<form>

<div id="headerFrame" class="headerFrame">
<?= getHeader(word(11)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b><?= word(32) ?></b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<fieldset>
<legend><b><?= word(88) ?></b></legend>
<?= word(162) ?>
</fieldset>

<fieldset>
<legend><b class="req"><?= word(98) ?></b></legend>
<div class="miniinfo"><?= word(180) ?></div>
<input type="text" name="email" alt="1|email|120" size="60" value="<?= $PHSes->email ?>" />
</fieldset>

<?php
	$fullAccess = false;

	// What tables does the current user have access to?
	$userTables = array(-1);
	$sql = "SELECT table_id FROM phoundry_group_table WHERE group_id = {$PHSes->groupId}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++)
		$userTables[] = (int)$db->FetchResult($cur,$x,'table_id');

	// Does current user have access to all available tables?
	$sql = "SELECT COUNT(*) FROM phoundry_table WHERE is_plugin = 0 AND name NOT LIKE 'phoundry_%' AND id NOT IN (" . escDBquote(implode(',', $userTables)) . ")";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if ($db->FetchResult($cur,0,0) == 0) {
		// User has access to all tables, make sure there's access to all columns:
		$fullAccess = true;
		foreach($userTables as $tableId) {
			$sql = "SELECT pgc.rights FROM phoundry_group_column pgc, phoundry_column pc WHERE pc.table_id = $tableId AND pc.id = pgc.column_id AND pgc.group_id = {$PHSes->groupId}";
			$tmp = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if ($db->EndOfResult($tmp))
				continue;

			for ($y = 0; !$db->EndOfResult($tmp); $y++) {
				if (strpos($db->FetchResult($tmp,$y,'rights'), 'v') === false) {
					$fullAccess = false;
					break;
				}
			}
		}
	}
	else {
		// User does not have access to all tables:
		$fullAccess = false;
	}
	
	if ($PHSes->isAdmin && $PHSes->groupId == -1)
		$fullAccess = true;
	if ($fullAccess) {
?>
<fieldset>
<legend><b class="req"><?= word(16) ?></b></legend>
<div class="miniinfo"><?= word(179) ?></div>
<input type="radio" name="orphans" value="0" checked="checked" onclick="setOrphans(this.form,false)" /><?= word(0) ?>
<input type="radio" name="orphans" value="1" onclick="setOrphans(this.form,true)" /><?= word(1) ?>
</fieldset>
<?php } ?>

<fieldset><legend><b>Authorization</b></legend>
<table>
<tr><td>Username:</td><td><input type="text" name="username" size="40" alt="0|string" /></td></tr>
<tr><td>Password:</td><td><input type="text" name="password" size="40" alt="0|string" /></td></tr>
</table>
</fieldset>

<?php
	$tables = array();
	if ($PHSes->isAdmin && $PHSes->groupId == -1)
		$sql = "SELECT pt.id, pt.name, pt.description FROM phoundry_table pt WHERE pt.is_plugin = 0 ORDER BY pt.order_by";
	else
		$sql = "SELECT pt.id, pt.name, pt.description FROM phoundry_table pt, phoundry_group_table pgt WHERE pt.is_plugin = 0 AND pgt.group_id = {$PHSes->groupId} AND pt.id = pgt.table_id ORDER BY pt.order_by";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$name = $db->FetchResult($cur,$x,'name');
		if (preg_match('/\{\$([^\}]+)\}/', $name)) { continue; }
		$tables[(int)$db->FetchResult($cur,$x,'id')] = array('name'=>$name, 'desc'=>PH::langWord($db->FetchResult($cur,$x,'description')));
	}

	foreach($tables as $id=>$table) {
		$sql = "SELECT COUNT(*) AS cnt FROM {$table['name']}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$cnt = $db->FetchResult($cur, 0, 'cnt');
		if (!$cnt) {
			continue;
		}
			
		$sql = "SELECT id, name, description FROM phoundry_column WHERE table_id = $id AND (datatype IN ('DTfile','DTurl') OR (datatype = 'DTstring' AND datatype_extra LIKE '%\"display\";s:4:\"html\"%')) ORDER BY order_by";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur))
			continue;

		print '<fieldset><legend><b>' . $table['desc'] . '</b> (<a href="#" onclick="selectAll(_D.forms[0][\'check_' . $id . '[]\']);return false">' . word(30) . '</a>)</legend>';
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$colId = (int)$db->FetchResult($cur,$x,'id');
			// Make sure current user has view-rights on this column:
			$sql = "SELECT rights FROM phoundry_group_column WHERE group_id = {$PHSes->groupId} AND column_id = $colId";
			$tmp = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if ($db->EndOfResult($tmp) || strpos($db->FetchResult($tmp,0,'rights'), 'v') !== false) {
				print '<input type="checkbox" name="check_' . $id . '[]" value="' . $colId . '" />';
				print PH::langWord($db->FetchResult($cur,$x,'description')) . "<br />\n";
			}
		}
		print "</fieldset>\n\n";
	}	
?>
</div>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="button" value="Check" onclick="startURLcheck()" />
	</td>
	<td align="right">
	<input type="button" value="<?= word(46) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
