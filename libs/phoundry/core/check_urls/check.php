<?php
$PHprefs = array();
$inc = @include_once('PREFS.php');
if ($inc === false) {
    require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";

$ARR = ($_SERVER['REQUEST_METHOD'] == 'POST') ? $_POST : $_GET;

// Make sure the current user in the current role has access to this page:
$TID = getTID();
checkAccess($TID);

// See if there's a lockfile:
$lockFile = $PHprefs['tmpDir'] . '/PHCL' . $_SERVER['HTTP_HOST'];
if (file_exists($lockFile)) {
    die('ERROR: ' . word(122));
}

$checks = array();
foreach ($ARR as $name => $value) {
    if (preg_match('/^check_([0-9]+)/', $name, $reg)) {
        $checks[$reg[1]] = $value;
    }
}

$str = '';
foreach ($checks as $tid => $cids) {
    if ($str != '') {
        $str .= '|';
    }
    $str .= $tid . ':' . implode(',', $cids);
}

$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
$domain = $_SERVER['HTTP_HOST'];
$site_identifier = '';
if (isset($ARR['site_identifier'])) {
    $sql = "SELECT protocol, domain FROM brickwork_site WHERE identifier = '" . escDBquote(
        $ARR['site_identifier']
    ) . "'";
    $cur = $db->Query($sql)
        or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
    $protocol = explode(',', $db->FetchResult($cur, 0, 'protocol'));
    $protocol = $protocol[0];
    $domain = $db->FetchResult($cur, 0, 'domain');
    $site_identifier = $ARR['site_identifier'];
}

$exec = isset($PHprefs['paths']['php']) ? $PHprefs['paths']['php'] : 'php';
$cmd = sprintf(
    '%s %s/doCheck.php -dir=%s -tmp=%s -tid=%s -email=%s -localhost=%s -proto=%s -domain=%s -gid=%s -orphans=%s -checks=%s -lang=%s -site_identifier=%s',
    $exec,
    dirname($_SERVER['SCRIPT_FILENAME']),
    escapeshellarg(realpath(getenv('CONFIG_DIR'))),
    escapeshellarg(realpath($PHprefs['tmpDir'])),
    escapeshellarg($ARR['TID']),
    escapeshellarg($ARR['email']),
    escapeshellarg($_SERVER['HTTP_HOST']),
    escapeshellarg('https'),
    escapeshellarg($domain),
    escapeshellarg($PHSes->groupId),
    escapeshellarg($ARR['orphans']),
    escapeshellarg($str),
    escapeshellarg($PHSes->lang),
    escapeshellarg($site_identifier)
);

if (!empty($_POST['username']) || !empty($_POST['password'])) {
    $cmd .= ' -user=' . escapeshellarg($ARR['username']) . ' -pass=' . escapeshellarg($ARR['password']);
} else {
    $cmd .= ' --user=' . escapeshellarg('linkChecker') . ' -pass=' . escapeshellarg('Cafvask8#');
}

error_log($cmd, 3, TMP_DIR.'/linkChecker.log');

shell_exec($cmd . ' >> '.TMP_DIR.'linkChecker.log 2>&1 &');
print word(123, $ARR['email']);
