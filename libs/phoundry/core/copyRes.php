<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this specific page:
	checkAccess($TID, 'copy');
	
	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);
	
	// Go ahead, insert the data into the database:
	$phoundry->insertRecord('copy');
