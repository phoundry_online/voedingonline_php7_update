<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	if (!file_exists($PHprefs['uploadDir'] . '/SHARED')) {
		mkdir($PHprefs['uploadDir'] . '/SHARED', 0755);
	}

	$_SESSION['jsTree'] = array(
		'showTabs' => false,
		'asciiOnly' => $PHprefs['charset'] != 'utf-8',
   	'rootDir' => $PHprefs['uploadDir'] . '/SHARED',
   	'rootUrl' => $PHprefs['uploadUrl'] . '/SHARED',
		'width' => null,
		'height' => null,
		'uploadExtensions' => array(),
		'selectExtensions' => array(),
		'allowFiles'   => true,
		'allowFolders' => true,
		'thumbnailWidth' => 0,
		'thumbnailHeight' => 0,
		'allowedWidth' => null,
		'allowedHeight' => null,
		'allowedSize' => null // In Megabytes (float)
	);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="shared_files" />
<title><?= word(312 /* Shared files */) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader(word(312 /* Shared files */)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame" style="padding:0;overflow:hidden">
<!-- Content -->
<iframe src="../../core/jsTree/?lang=<?= $PHSes->lang . '&amp;' . mt_rand(0,9999) ?>" frameborder="0" style="width:100%;height:100%"></iframe>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'"><?= word(46 /* Cancel */) ?></button>
	</td>
</tr></table>
</div>

</body>
</html>
