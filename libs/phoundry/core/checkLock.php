<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header('Content-type: application/json');

	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$RID = $_POST['RID'];

	// Instantiate Phoundry object:
	$phoundry = new Phoundry($TID, false);

	// Delete locks older than 4 hours:
	$sql = "DELETE FROM phoundry_lock WHERE NOW() - 0 > since";
	$cur = $db->Query($sql)
		or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql");

	$lockedSince = '';
	$lockedBy = '';
	$lockMsg = '';

	$RID = explode('|', $RID);
	if (count($RID) > 1) 
	{
		// Check for multiple locks (deleteAll).
		$checkOne = false;
		$sql = "SELECT l.since, pu.name FROM phoundry_lock l, phoundry_user pu WHERE l.table_id = $TID AND l.keyval IN (" . $phoundry->convertKeyval($RID) . ") AND l.user_id = pu.id";
	}
	else {
		// Check for a single lock.
		$checkOne = true;
		$sql = "SELECT l.since, pu.name FROM phoundry_lock l, phoundry_user pu WHERE l.table_id = $TID AND l.keyval = " . $phoundry->convertKeyval($RID[0]) . " AND l.user_id = pu.id";
	}

	$cur = $db->Query($sql)
		or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql");

	if ($db->EndOfResult($cur)) 
	{
		print json_encode(false);
	}
	else 
	{
		$lockedBy    = $db->FetchResult($cur,0,'name');
		$since       = $db->FetchDatetimeResult($cur,0,'since');
		$lockedSince = $g_dateFormat;
		$lockedSince = str_replace('DD',   substr($since, 8, 2), $lockedSince);
		$lockedSince = str_replace('MM',   substr($since, 5, 2), $lockedSince);
		$lockedSince = str_replace('YYYY', substr($since, 0, 4), $lockedSince);
		$lockedSince .= ' ' . substr($since, 11, 2) . ':' . substr($since, 14, 2);

		if ($checkOne) 
		{
		 	print json_encode(word(64, $lockedBy, $lockedSince));
		}
		else 
		{
			print json_encode(word(13));
		}
	}
