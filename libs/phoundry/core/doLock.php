<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	header("Content-type: text/plain; charset={$PHprefs['charset']}");

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction'])) 
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	if (!isset($_GET['RID'])) exit;

	$RID = $_GET['RID'];
	// $lock == 1 to lock a record, false to unlock it:
	$lock = isset($_GET['lock']) ? $_GET['lock'] : 0;

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);
	
	$phoundry = new Phoundry($TID, false);
	if ($lock) 
	{
		$phoundry->lockRecord($RID);
		print "RECORD {$RID} locked!";
	}
	else 
	{
		$phoundry->unlockRecord($RID, false);
		print "RECORD {$RID} unlocked!";
	}
