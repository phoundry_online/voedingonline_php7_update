<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$eTID = isset($_REQUEST['eTID']) ? (int)$_REQUEST['eTID'] : '';
	$eGID = isset($_REQUEST['eGID']) ? (int)$_REQUEST['eGID'] : '';

	$sql = "SELECT name FROM phoundry_group WHERE id = $eGID";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$uname = $db->FetchResult($cur,0,'name');

	$sql = "SELECT name, description FROM phoundry_table WHERE id = $eTID";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$tname = $db->FetchResult($cur,0,'name');
	$tdesc = PH::langWord($db->FetchResult($cur,0,'description'));

	$phoundry = new Phoundry($eTID);
	$rights = array('v'=>word(9), 'i'=>word(6), 'u'=>word(7));

	// Count rights:
	$sql = "SELECT COUNT(column_id) FROM phoundry_group_column gc, phoundry_column c WHERE c.table_id = $eTID AND c.id = gc.column_id AND gc.group_id = $eGID";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$nrRights = $db->FetchResult($cur,0,0);

	list($CSRFname, $CSRFtoken) = PH::startCSRF();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title><?= word(47) ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<style type="text/css">

div#info ul {
	padding-left: 0;
	padding-top: 0;
	margin-top: 0;
	margin-bottom: 0;
}

</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript">

var _D = document;

function allRights()
{
	var F = _D.forms[0];
	var len = F.elements.length;
	for (var x = 0; x < len; x++) {
		if (F.elements[x].name.charAt(0) == 'c')
			F.elements[x].checked = true;
	}
}

function init() {
	CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');
	<?php if (isset($_GET['_done'])) { ?>
	parent.killPopup();
	alert('<?= escSquote(word(83)) ?>');
	<?php } ?>
}

</script>
</head>
<body onload="init()">
<form name="phoundry" action="columnRes.php" method="post">
<input type="hidden" name="TID" value="<?= $TID ?>" />
<input type="hidden" name="eGID" value="<?= $eGID ?>" />
<input type="hidden" name="eTID" value="<?= $eTID ?>" />

<h2><?=word(52) . " $uname, " . word(59) . " $tdesc"?></h2>
<table class="view-table" width="100%">
<tr>
<th><?= word(75) ?></th>
<th colspan="<?= count($rights) ?>"><?= word(76) ?></th>
</tr>

<?php
	$teller = $nrCheckBoxes = 0;

	$row = 0;
	foreach($phoundry->cols as $col) {
		if ($col->name == $phoundry->key || strtolower(get_class($col->datatype)) == 'dtfake')
			continue;
		$sql = "SELECT rights FROM phoundry_group_column WHERE column_id = $col->id AND group_id = $eGID";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur))
			$DBrights = $db->FetchResult($cur,0,'rights');
		else
			$DBrights = '';

		print '<tr class="' . ($row & 1 ? 'even' : 'odd') . '">';
		print '<td>' . $col->description . '</td>';

		foreach($rights as $abbr=>$right) {
				$nrCheckBoxes++;
			if ($abbr == 'i' && $col->required) {
				// A notnull-column always has to be inserted!
				print '<td bgcolor="#ff6666">';
				print '<input type="hidden" name="c' . $col->id . '_' . $abbr . '" value="on" />';
				print '<input type="checkbox" checked="checked" disabled="disabled" /> ' . word(6);
				print '</td>';
			}
			else {
				if (strpos($DBrights, $abbr) !== false || $nrRights == 0)
					print '<td><input type="checkbox" name="c' . $col->id . '_' . $abbr . '" checked="checked" /> ' . $right . '</td>';
				else
					print '<td><input type="checkbox" name="c' . $col->id . '_' . $abbr . '" /> ' . $right . '</td>';
			}
		}
		print "</tr>\n";
		$row++;
	}

?>
</table>
<input type="hidden" name="nrCheckBoxes" value="<?= $nrCheckBoxes ?>" />
<br />
<table width="100%"><tr>
<td>
	<input type="button" value="<?= word(73) ?>" onclick="allRights()" />
</td>
<td align="right">
	<input class="okBut" type="submit" value="Ok" />
	<input type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</td>
</tr></table>
</form>

<div id="info" class="miniinfo">
<?= word(81) ?>
</div>

</body>
</html>
