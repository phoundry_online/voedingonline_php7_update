<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	$GID = isset($_REQUEST['GID']) ? (int)$_REQUEST['GID'] : '';

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$sql = 'SELECT id, name FROM phoundry_group WHERE id > 0 ORDER BY name';
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
   $groups = array();
   for ($x = 0; !$db->EndOfResult($cur); $x++) {
      if ($x == 0 && $GID == '')
         $GID = $db->FetchResult($cur,$x,'id');
	      $groups[$db->FetchResult($cur,$x,'id')] = $db->FetchResult($cur,$x,'name');
   }

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<meta name="pageID" id="pageID" content="rolerights" />
<title>Phoundry</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../csjs/global.js.php"></script>
<script type="text/javascript">

function init() {
	<?php if (isset($_GET['_done'])) { ?>
	alert('<?= escSquote(word(83)) ?>');
	<?php } ?>
}

</script>
</head>
<body class="frames" onload="init()">
<form action="tableRes.php" method="post">
<input type="hidden" name="TID" value="<?= $TID ?>" />
<input type="hidden" name="GID" value="<?= $GID ?>" />
<div id="headerFrame" class="headerFrame">
<?php
	if ($GID == '')
		print getHeader(word(55));
	else
		print getHeader(word(55), word(52) . ' ' . $groups[$GID]);
?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
	<?= word(52) ?>:
	<select onchange="_D.location='index.php?<?= QS(1) ?>&GID='+this.options[this.selectedIndex].value">
   <?php
		foreach($groups as $id=>$name) {
			$selected = $id == $GID ? 'selected="selected"' : '';
			print '<option ' . $selected . ' value="' . $id . '">' . $name . "</option>\n";
      }
   ?>
   </select>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?php
	// This page only makes sense when there are some groups defined:
	if (count($groups) == 0) {
		print '<p>' . word(54) . '</p>';
	}
	else {
		$rights = array(
			'p'=>word(9), // Preview
			'i'=>word(6), // Insert
			'u'=>word(7), // Update
			'd'=>word(8), // Delete
			'm'=>word(142), // Import
			'x'=>word(141), // Export
		);
		if (!empty($PHprefs['enableVersioning']))
			$rights['b'] = word(351);
		if ($PHprefs['useCopy'])
			$rights['c'] = word(109);
		if ($PHprefs['useNotify'])
			$rights['n'] = word(12);
		print '<table class="sort-table" cellspacing="0">';
		print '<tr>';
		print '<th>' . word(59) . '</th>';
		print '<th>'.implode('</th><th>', $rights).'</th>';
		print '<th></th>';
		print '</tr>';

		$sql = "SELECT rt.description AS tname, rt.id, rt.extra, gt.rights FROM phoundry_table rt, phoundry_group_table gt WHERE gt.group_id = $GID AND gt.table_id = rt.id ORDER BY rt.description";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$idx = 0;
		for ($row = 0; !$db->EndOfResult($cur); $row++) {
			$extra = $db->FetchResult($cur,$row,'extra');
			$extra = empty($extra) ? array() : unserialize($extra);
			if (isset($extra['rolerights_global']) && $extra['rolerights_global'] == false) {
				foreach($rights as $abbr=>$right) {
					print '<input type="hidden" name="t' . $db->FetchResult($cur,$row,'id') . '_' . $abbr . '" value="1" />';
				}
				continue;
			}

			print '<tr class="' . ($idx & 1 ? 'even' : 'odd') . '">';
			print '<td align="center">' . PH::langWord($db->FetchResult($cur,$row,'tname')) . '</td>';
			foreach($rights as $abbr=>$right) {
				$tableId = (int)$db->FetchResult($cur,$row,'id');
				if (strpos($db->FetchResult($cur,$row,'rights'), $abbr) !== false)
					print '<td align="center"><input type="checkbox" name="t' . $tableId . '_' . $abbr . '" checked="checked" /></td>';
				else
					print '<td align="center"><input type="checkbox" name="t' . $tableId . '_' . $abbr . '" /></td>';
			}
			$sql = "SELECT COUNT(*) FROM phoundry_column WHERE table_id = $tableId";
			$tmp = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$disabled = !((!isset($extra['rolerights_detail']) || $extra['rolerights_detail'] == true) && $db->FetchResult($tmp,0,0) > 0);	
			print '<td><input class="popupWin" type="button" value="' . word(23) . '" title="' . word(76) . '|440|400|1|column.php?' . QS(1,'eGID=' . $GID . '&eTID=' . $tableId . '&_done') . '" '.($disabled ? 'disabled="disabled" ' : "").'/></td>';
			print "</tr>\n";

			if($idx !== 0 && $idx % 10 === 0) {
				print '<tr>';
				print '<th>' . word(59) . '</th>';
				print '<th>'.implode('</th><th>', $rights).'</th>';
				print '<th></th>';
				print '</tr>';
			}
			$idx++;
		}
		print '</table>';
	}
?>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input class="okBut" type="submit" value="Ok" />
	</td>
	<td align="right">
	<input type="button" value="<?= word(46) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
