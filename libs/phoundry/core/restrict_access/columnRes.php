<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$eTID = isset($_REQUEST['eTID']) ? (int)$_REQUEST['eTID'] : '';
	$eGID = isset($_REQUEST['eGID']) ? (int)$_REQUEST['eGID'] : '';

	$rights = array('v'=>word(9), 'i'=>word(6), 'u'=>word(7));
	
	$phoundry = new Phoundry($eTID);

	
	$checked = 0;
	$colIds = array();
	foreach($phoundry->cols as $col) {
		if ($col->name == $phoundry->key)
			continue;

		$colIds[] = $col->id;
		$colRights = '';
		foreach($rights as $abbr=>$right) {
			$var = 'c' . $col->id . '_' . $abbr;
			if (isset($_POST[$var])) {
				$colRights .= $abbr;
				$checked++;
			}
		}
		
		// Do we need an INSERT or an UPDATE?
		$sql = "SELECT rights FROM phoundry_group_column WHERE column_id = $col->id AND group_id = $eGID";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur))
			$sql = "INSERT INTO phoundry_group_column VALUES ($eGID, $col->id, '$colRights')";
		else
			$sql = "UPDATE phoundry_group_column SET rights = '$colRights' WHERE group_id = $eGID AND column_id = $col->id";
		$res = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}
	if ($checked == $_POST['nrCheckBoxes']) {
		// User has all rights:
		$db->Query("DELETE FROM phoundry_group_column WHERE group_id = $eGID AND column_id IN (" . implode(',', $colIds) . ")");
	}

	header('Status: 302 moved');
	header('Location: column.php?TID=' . $TID . '&eGID=' . $eGID . '&eTID=' . $eTID . '&_done=true');
