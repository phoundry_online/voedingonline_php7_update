<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	$GID = isset($_REQUEST['GID']) ? (int)$_REQUEST['GID'] : '';

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$rights = array(
		'i'=>word(6),
		'u'=>word(7),
		'd'=>word(8),
		'b'=>word(351), // Publish
		'p'=>word(9),
		'c'=>word(109),
		'n'=>word(12),
		'm'=>word(142),
		'x'=>word(141)
	);

	$sql = "SELECT pt.id FROM phoundry_table pt, phoundry_group_table pgt WHERE pgt.group_id = $GID AND pgt.table_id = pt.id ORDER BY pt.name";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	
	for ($row = 0; !$db->EndOfResult($cur); $row++) {
		$tableRights = '';
		foreach($rights as $abbr=>$right) {
			$var = 't' . $db->FetchResult($cur,$row,'id') . '_' . $abbr;
			if (isset($_POST[$var]))
				$tableRights .= $abbr;
		}
		if (!$PHprefs['useNotify'])
			$tableRights .= 'n';
		if (!$PHprefs['useCopy'])
			$tableRights .= 'c';
	
		$sql = "UPDATE phoundry_group_table SET rights = '$tableRights' WHERE group_id = $GID AND table_id = " . (int)$db->FetchResult($cur,$row,'id');
		$res = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}
	header('Status: 302 moved');
	header('Location: index.php?TID=' . $TID . '&GID=' . $GID . '&_done=true');
