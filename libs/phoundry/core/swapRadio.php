<?php
   $inc = @include_once('PREFS.php');
   if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
   }
	header("Content-type: text/html; charset={$PHprefs['charset']}");
   require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
	
	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Instantiate Phoundry opbject:
	$phoundry = new Phoundry($TID);

	$CID = (int)$_GET['CID'];
	$RID = $_GET['RID'];

	$sql = "SELECT name, inputtype_extra FROM phoundry_column WHERE id = {$CID}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$extra = unserialize($db->FetchResult($cur,0,'inputtype_extra'));
	$colName = $db->FetchResult($cur,0,'name');

	$sql = "SELECT {$colName} FROM {$phoundry->name} WHERE {$phoundry->key} = '" . escDBquote($RID) . "'";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$value = $db->FetchResult($cur,0,0);

	$pairs = explode('|', $extra['query']);
	$pair0 = explode('=', $pairs[0], 2);
	$pair1 = explode('=', $pairs[1], 2);

	$newVal = $newTxt = null;

	if ($pair0[0] == $value) {
		// Toggle to pair1
		$newVal = $pair1[0];
		$newTxt = PH::evaluate($pair1[1]);
	}
	if ($pair1[0] == $value) {
		// Toggle to pair0
		$newVal = $pair0[0];
		$newTxt = PH::evaluate($pair0[1]);
	}

	if (!is_null($newVal)) {
		$phoundry->doEvent('pre-update', $RID, $RID);
		$sql = "UPDATE {$phoundry->name} SET {$colName} = '" . escDBquote($newVal) . "' WHERE {$phoundry->key} = '" . escDBquote($RID) . "'";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$phoundry->doEvent('post-update', $RID, $RID);
		print $newTxt;
	}
?>
