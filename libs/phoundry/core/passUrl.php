<?php
	if (!isset($_GET['url'])) {
		error_log('url not defined in ' . __FILE__);
		exit();
	}
	$url = $_GET['url'];
	if (!preg_match('|^https?://|i', $url)) {
		// Make sure they don't use this to read system files!
		$url = 'http://' . $url;
	}
	if (function_exists('curl_init')) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		print curl_exec($ch);
	}
	else {
		if ($fp = fopen($url, 'r')) {
			fpassthru($fp);
		}
	}
