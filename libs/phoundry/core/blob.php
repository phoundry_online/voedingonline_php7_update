<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$RID = (int)$_GET['RID'];
	$CID = (int)$_GET['CID'];

	$sql = "SELECT t.name as tname, c.name as cname FROM phoundry_table t, phoundry_column c WHERE t.id = {$TID} AND c.id = {$CID} AND c.table_id = t.id";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($cur)) {
		$cname = $db->FetchResult($cur,0,'cname');
		$tname = $db->FetchResult($cur,0,'tname');
		
		$db->GetTableIndexDefinition($tname, 'PRIMARY', $index)
			or trigger_error("Cannot get index for table {$tname}: " . $db->Error(), E_USER_ERROR);
		$key = array_keys($index['FIELDS']);
		$key = $key[0];

		$sql = "SELECT " . escDBquote($cname) . ", " . escDBquote($cname) . "_blob FROM " . escDBquote($tname) . " WHERE " . escDBquote($key) . " = {$RID}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		header('Content-type: ' . PH::getContentType($db->FetchResult($cur,0,$cname)));
		print $db->FetchResult($cur,0,$cname.'_blob');
	}
