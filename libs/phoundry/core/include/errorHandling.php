<?php

if (isset($_SERVER['REMOTE_ADDR'])) {
	$vendorDir = dirname(dirname(__DIR__)) . '/vendor';
	if (!class_exists('WebPower\Phoundry\ErrorHandling\ErrorHandler', true) &&  !is_dir($vendorDir)) {
		error_log('Vendor dir is missing');
		exit(1);
	}
	require_once $vendorDir . '/autoload.php';

    $errorHandler = new \WebPower\Phoundry\ErrorHandling\ErrorHandler();

    if (!empty($PHprefs['errorRcpts'])) {
        $errorHandler->addObserver(
            new \WebPower\Phoundry\ErrorHandling\MailerErrorObserver(
                $PHprefs['errorRcpts'],
                $_SERVER,
                "errormailer@".php_uname('n'),
                php_uname('n')
            )
        );
    }

    /**
     * Stack on the Raven > Sentry errorhandler when exists and not in TESTMODE
     */
	if (false && class_exists('Raven_Client') && defined('TESTMODE') && !TESTMODE) {
        if (isset($PHprefs['raven']['dsn'])) {
            $raven_dsn = $PHprefs['raven']['dsn'];
        } else {
            $raven_dsn = 'https://61818a428c4f4da8beb1c3a44f8e865b:1b1a624d2b944215873a83aed7e6cece@logsentry.phoundry.nl/5';
        }
        if (isset($PHprefs['raven']['error_level'])) {
            $error_level = $PHprefs['raven']['error_level'];
        } else {
            $error_level = E_ERROR | E_USER_ERROR;
        }
		$errorHandler->addObserver(new \WebPower\Phoundry\ErrorHandling\LogSentry($raven_dsn, $error_level));
	}

	if (!empty($PHprefs['showErrors']) || (getenv('APP_MODE') === 'development' && !isset($PHprefs['showErrors']))) {
		$errorHandler->addObserver(
			new \WebPower\Phoundry\ErrorHandling\HtmlPageErrorObserver($PHprefs['charset'])
		);
		$errorHandler->setDebug(true);
	} else {
		$errorHandler->addObserver(
			new \WebPower\Phoundry\ErrorHandling\SupportErrorObserver(
				$_GET,
				$_POST,
				$_SERVER,
				$PHprefs['url'] . '/core/support.php?type=bug'
			)
		);
	}
	$errorHandler->register();
}
