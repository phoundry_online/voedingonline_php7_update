<?php
/**  Memcached session handler native in PHP
  *
  *  For distributed session management in PHP applications.
  *
  *  @author: Roalt Zijlstra
  *  @version: 0.9
  *
  *  0.9 Converted into MemCacheSession class,
  *  0.8 Fixed Locking
  *  0.4 Fixed logfile handling
  */


/*
if( function_exists("memcache_connect") && !class_exists("MemCacheSession") ) {
        // In case not all webservers have memcache support, we need to check first
        // if it exists... If it exists then we can use the memecache session handler
        include_once '/project/MODULES/tmp/memcache_session.class.php';

        $MemcacheSession = new MemCacheSession('10.168.10.2',11211);

         session_set_save_handler(
                array($MemcacheSession, 'Open'),
                array($MemcacheSession, 'Close'),
                array($MemcacheSession, 'Read'),
                array($MemcacheSession, 'Write'),
                array($MemcacheSession, 'Destroy'),
                array($MemcacheSession, 'GC')
        );

}
*/



class MemCacheSession {
	private $_logging = true;
	private $_log = array();
	private $_log_dir = '';
	private $_memcache_obj = NULL;
	private $_name = '';

	private $_server = 'localhost';
	private $_port = 11211;
	private $_lock_timeout = 30;
	private $_lock_usleep = 100000;
	private $_lock_wait_count = 3000;
	private $_unique_instance = '';

	public function __construct($server = 'localhost', $port = 11211, $name = ''){
		global $PHprefs;
		
		// set default log_dir
		$this->_log_dir = $PHprefs['tmpDir'];

		// set unique script call identifier for session locking
		$this->_unique_instance = uniqid();
		$this->_server	= $server;
		$this->_port	= $port;
		$this->_logging = FALSE;
		$this->_lock_wait_count = (($this->_lock_timeout * 1000)/ $this->_lock_usleep );
	}

	public function __set($key, $value){
		switch($key){
			case 'logging':
				$this->_logging = (bool)$value;
			break;
			default:
				throw new AttributeNotFoundException(sprintf('Attribute %s not found', $name));
		}
	}

	public function Open($void1, $void2){
		// set session name
		$this->_name = empty($name) ? session_name() : $name;

		// log what we are doing
		$this->_log[] = "Script name: ".$_SERVER['SCRIPT_NAME']."\nopen {$this->_name} unique: {$this->_unique_instance}";

		// set memcache object and connect to memcache server
	        $this->_memcache_obj = memcache_connect($this->_server, $this->_port) or die("MemCacheSession: Could not connect to server {$this->_server}");

	    return TRUE;
	}

	public function Close(){
		global $PHprefs;

		$this->_log[] = "close {$this->_name} unique: {$this->_unique_instance}";

		// If the session is closed, we remove the lock key value.
		$my_sid = session_id();
	    	@memcache_delete($this->_memcache_obj,"lock_{$this->_name}_{$my_sid}");

		// close memcache object
	        @memcache_close($this->_memcache_obj);
		// Log errors here instead of in the destruct... 
		if($this->_logging == TRUE){
			error_log(implode("\n", $this->_log)."\n", 3, $PHprefs['tmpDir'].'/error.log');
		}
    
		return TRUE;
	}

	public function Read($id){
		global $PHprefs;

		// Create a lock key value in the memcache database
		$lock = memcache_add( $this->_memcache_obj, "lock_{$this->_name}_{$id}", $this->_unique_instance, false, $this->_lock_timeout);
		// If we could not set the lock key then we retry until we can.
		if( $lock === false ) {
			if(memcache_get($this->_memcache_obj, "lock_{$this->_name}_{$id}") == $this->_unique_instance){
				$lock = true;
			} else {
				$this->_log[] = "locked, waiting for lock_{$this->_name}_{$id}!";
			}
		}

		$waits = 0;
		while( $lock === false) {
			usleep( $this->_lock_usleep );
			if($waits > $this->_lock_wait_count ){
				$this->_log[] = "Too many wait loops!";
				// Log errors here instead of in the destruct... 
				if($this->_logging == TRUE){
					error_log(implode("\n", $this->_log)."\n", 3, $PHprefs['tmpDir'].'/error.log');
				}

				die('Too many connections');

			}
			$lock = memcache_add( $this->_memcache_obj, "lock_{$this->_name}_{$id}", $this->_unique_instance, false, $this->_lock_timeout);
			$waits++;
		}

		// log what we are doing
		$this->_log[] = "read after $waits waits: $id";

		$value = @memcache_get($this->_memcache_obj,"{$this->_name}_{$id}");
		//$this->_log[] = "read: $value";

		return (string)$value;
	}


	public function Write($id, $data){
		$this->_log[] = "write $id";
		if( $this->_memcache_obj->connection === NULL  ){
	   		$this->_memcache_obj = memcache_connect($this->_server, $this->_port) or die("MemCacheSession: Could not connect to server {$this->_server}");
			   //	$this->Open(NULL, NULL);
		}

		$return = @memcache_set($this->_memcache_obj,"{$this->_name}_{$id}", $data, 0,3600);
		$this->_log[] = "write after set $id";
		return $return;
	}


	public function Destroy($id){
		$this->_log[] = "destroy $id";
		$res = @memcache_delete($this->_memcache_obj,"{$this->_name}_{$id}");
		return TRUE;

	}

	public function GC($maxlifetime){
		return TRUE;
	}
}
?>
