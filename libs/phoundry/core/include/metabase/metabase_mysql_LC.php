<?php
if(!defined("METABASE_MYSQL_INCLUDED_LC"))
{
    define("METABASE_MYSQL_INCLUDED_LC",1);

    /*
     * metabase_mysql.php
     *
     * @(#) $Header: /home/mlemos/cvsroot/metabase/metabase_mysql.php,v 1.78 2005/11/21 20:51:57 mlemos Exp $
     *
     */

    class metabase_mysql_class_LC extends metabase_database_class
    {
        var $connection=0;
        var $connected_host;
        var $connected_user;
        var $connected_password;
        var $connected_port;
        var $opened_persistent="";
        var $decimal_factor=1.0;
        var $emulate_decimal=0;
        var $highest_fetched_row=array();
        var $columns=array();
        var $fixed_float=0;
        var $escape_quotes="\\";
        var $sequence_prefix="_sequence_";
        var $dummy_primary_key="dummy_primary_key";
        var $manager_class_name="metabase_manager_mysql_class";
        var $manager_include="manager_mysql.php";
        var $manager_included_constant="METABASE_MANAGER_MYSQL_INCLUDED";
        var $default_table_type="";
        var $select_queries=array(
            "select"=>"",
            "show"=>"",
            "check"=>"",
            "repair"=>"",
            "analyze"=>"",
            "optimize"=>"",
            "explain"=>"",
            ### ARJAN ###
            "desc"=>"",
            "(select"=>""
            ### /ARJAN ###
        );

        function Connect()
        {
            $port=(isset($this->options["Port"]) ? $this->options["Port"] : "");
            $forceNewConnection=(isset($this->options["forceNewConnection"]) && $this->options["forceNewConnection"] == true ? true : false);

            if($this->connection)
            {
                if(
                    !strcmp($this->connected_host,$this->host)
                    && !strcmp($this->connected_user,$this->user)
                    && !strcmp($this->connected_password,$this->password)
                    && !strcmp($this->connected_port,$port)
                    && $this->opened_persistent==$this->persistent
                ) {
                    return(1);
                }
                mysqli_Close($this->connection);
                $this->connection = 0;
                $this->affected_rows = -1;
            }
            $this->fixed_float=30;

            ### ARJAN ###
            if ($this->persistent) {
                $this->connection = @mysqli_connect($this->host.(!strcmp($port,'') ? '' : ':'.$port),$this->user,$this->password, isset($this->options['ClientFlags']) ? $this->options['ClientFlags'] : 0);
            }
            else {
                $this->connection = @mysqli_connect($this->host.(!strcmp($port,'') ? '' : ':'.$port),$this->user,$this->password,$forceNewConnection,isset($this->options['ClientFlags']) ? $this->options['ClientFlags'] : 0);
            }
            ### /ARJAN ###

            if (!$this->connection) {
                return($this->SetError("Connect",isset($php_errormsg) ? $php_errormsg : "Could not connect to MySQL server"));
            }
            if(isset($this->options["FixedFloat"])) {
                $this->fixed_float=$this->options["FixedFloat"];
            }
            else
            {
                if(($result=mysqli_query($this->connection, "SELECT VERSION()")))
                {
                    $version=explode(".",$this->mysqli_result($result,0,0));
                    $major=intval($version[0]);
                    $minor=intval($version[1]);
                    $revision=intval($version[2]);
                    if($major>3
                        || ($major==3
                            && $minor>=23
                            && ($minor>23
                                || $revision>=6)))
                        $this->fixed_float=0;
                    mysqli_free_result($result);
                }
            }

            ### ARJAN ###
            $this->SetCharset($this->charset);
            ### /ARJAN ###

            if(isset($this->supported["Transactions"])
                && !$this->auto_commit)
            {
                if(!mysqli_query($this->connection, "SET AUTOCOMMIT=0"))
                {
                    mysqli_Close($this->connection);
                    $this->connection=0;
                    $this->affected_rows=-1;
                    return(0);
                }
                $this->RegisterTransactionShutdown(0);
            }
            $this->connected_host=$this->host;
            $this->connected_user=$this->user;
            $this->connected_password=$this->password;
            $this->connected_port=$port;
            $this->opened_persistent=$this->persistent;
            return(1);
        }

        ### ARJAN ###
        function SetCharset($charset) {
            $charset = strtolower($charset);
            $mapping = array(
                'latin1'		 => 'latin1',
                'utf8'		 => 'utf8',
                'iso-8859-1' => 'latin1',
                'utf-8'      => 'utf8',
                'gb2312'     => 'gb2312',
                'gbk'        => 'gbk',
                'koi8-r'     => 'koi8r'
            );

            $charset = isset($mapping[$charset]) ? $mapping[$charset] : 'latin1';

            if (function_exists('mysqli_set_charset')) {
                mysqli_set_charset($this->connection, $charset);
            }
            else {
                mysqli_query($this->connection, "SET NAMES {$charset}");
            }
        }
        ### /ARJAN ###

        function Close()
        {
            if($this->connection!=0)
            {
                if(isset($this->supported["Transactions"])
                    && !$this->auto_commit)
                    $this->AutoCommitTransactions(1);
                mysqli_Close($this->connection);
                $this->connection=0;
                $this->affected_rows=-1;
            }
        }

        function Query($query)
        {
            $this->Debug("Query: $query");
            $first=$this->first_selected_row;
            $limit=$this->selected_row_limit;
            $this->first_selected_row=$this->selected_row_limit=0;
            if(!strcmp($this->database_name,""))
                return($this->SetError("Query","it was not specified a valid database name to select"));
            if(!$this->Connect())
                return(0);

            ### ARJAN ###
            if (preg_match('/([^\s]+)/', $query, $reg))
                $query_string = strtolower($reg[1]);
            ### /ARJAN ###

            if(($select=isset($this->select_queries[$query_string]))
                && $limit>0)
                $query.=" LIMIT $first,$limit";

            if($this->selected_database !== $this->database_name) {
                if(mysqli_select_db($this->connection, $this->database_name)) {
                    $this->selected_database = $this->database_name;
                }
                else {
                    return $this->SetError("Query",mysqli_error($this->connection));
                }
            }

            if (isset($this->options['profilerFirebug']) && $this->options['profilerFirebug']) {
                $_sTime = microtime();
                $result = mysqli_query($this->connection, $query);
                $_tTime = microtime() - $_sTime;
                fb($query, $_tTime);
            } else {
                $result = mysqli_query($this->connection,$query);
            }

            if($result)
            {
                if($select)
                {
                    # ARJAN: using is_ functions instead of gettype:
                    if (!is_bool($result)) {
                        // Query succeeded:
                        $this->highest_fetched_row[metabase_unique_id_for_resource($result)]=-1;
                    }
					elseif ($result === true) {
                        // Query succeeded, but nothing can be fetched.
                    }
                    else {
                        $error=mysqli_error($this->connection);
                        return($this->SetError("Query","this select query did not return valid result set value: ".$query.(strlen($error) ? " (".$error.")" : "")));
                    }
                    /*
                    switch(GetType($result))
                    {
                        case "resource":
                        case "integer":
                            $this->highest_fetched_row[$result]=-1;
                            break;
                        default:
                            $error=mysql_error($this->connection);
                            return($this->SetError("Query","this select query did not return valid result set value: ".$query.(strlen($error) ? " (".$error.")" : "")));
                    }
                    */
                }
                else
                    $this->affected_rows=mysqli_affected_rows($this->connection);
            }
            else
                return($this->SetError("Query",mysqli_error($this->connection)));
            return($result);
        }

        function Replace($table,&$fields)
        {
            $count=count($fields);
            for($keys=0,$query=$values="",Reset($fields),$field=0;$field<$count;Next($fields),$field++)
            {
                $name=Key($fields);
                if($field>0)
                {
                    $query.=",";
                    $values.=",";
                }
                $query.=$name;
                if(isset($fields[$name]["Null"])
                    && $fields[$name]["Null"])
                    $value="NULL";
                else
                {
                    if(!isset($fields[$name]["Value"]))
                        return($this->SetError("Replace","it was not specified a value for the $name field"));
                    switch(isset($fields[$name]["Type"]) ? $fields[$name]["Type"] : "text")
                    {
                        case "text":
                            $value=$this->GetTextFieldValue($fields[$name]["Value"]);
                            break;
                        case "boolean":
                            $value=$this->GetBooleanFieldValue($fields[$name]["Value"]);
                            break;
                        case "integer":
                            $value=strval($fields[$name]["Value"]);
                            break;
                        case "decimal":
                            $value=$this->GetDecimalFieldValue($fields[$name]["Value"]);
                            break;
                        case "float":
                            $value=$this->GetFloatFieldValue($fields[$name]["Value"]);
                            break;
                        case "date":
                            $value=$this->GetDateFieldValue($fields[$name]["Value"]);
                            break;
                        case "time":
                            $value=$this->GetTimeFieldValue($fields[$name]["Value"]);
                            break;
                        case "timestamp":
                            $value=$this->GetTimestampFieldValue($fields[$name]["Value"]);
                            break;
                        default:
                            return($this->SetError("Replace","it was not specified a supported type for the $name field"));
                    }
                }
                $values.=$value;
                if(isset($fields[$name]["Key"])
                    && $fields[$name]["Key"])
                {
                    if($value=="NULL")
                        return($this->SetError("Replace","key values may not be NULL"));
                    $keys++;
                }
            }
            if($keys==0)
                return($this->SetError("Replace","it were not specified which fields are keys"));
            return($this->Query("REPLACE INTO $table ($query) VALUES($values)"));
        }

        function EndOfResult($result)
        {
            if(!isset($this->highest_fetched_row[metabase_unique_id_for_resource($result)]))
            {
                $this->SetError("End of result","attempted to check the end of an unknown result");
                return(-1);
            }
            return($this->highest_fetched_row[metabase_unique_id_for_resource($result)]>=$this->NumberOfRows($result)-1);
        }

        function mysqli_result($result, $iRow, $field = 0)
        {
            if(!mysqli_data_seek($result, $iRow))
                return false;
            if(!($row = mysqli_fetch_array($result)))
                return false;
            if(!array_key_exists($field, $row))
                return false;
            return $row[$field];
        }

        function mysqli_result2($result,$row,$field=0) {
            if ($result===false) return false;
            if ($row>=mysqli_num_rows($result)) return false;
            if (is_string($field) && !(strpos($field,".")===false)) {
                $t_field=explode(".",$field);
                $field=-1;
                $t_fields=mysqli_fetch_fields($result);
                for ($id=0;$id<mysqli_num_fields($result);$id++) {
                    if ($t_fields[$id]->table==$t_field[0] && $t_fields[$id]->name==$t_field[1]) {
                        $field=$id;
                        break;
                    }
                }
                if ($field==-1) return false;
            }
            mysqli_data_seek($result,$row);
            $line=mysqli_fetch_array($result);
            return isset($line[$field])?$line[$field]:false;
        }

        function FetchResultForInputField($result,$row,$field)
        {
            $this->highest_fetched_row[metabase_unique_id_for_resource($result)]=max($this->highest_fetched_row[metabase_unique_id_for_resource($result)],$row);
            return($this->mysqli_result2($result,$row,$field));
        }

        function FetchResult($result,$row,$field)
        {
            $this->highest_fetched_row[metabase_unique_id_for_resource($result)]=max($this->highest_fetched_row[metabase_unique_id_for_resource($result)],$row);
            return($this->mysqli_result($result,$row,$field));
        }

        ### ARJAN ###
        function FetchColumnName($result,$field_index)
        {
            return(mysqli_fetch_field_direct($result,$field_index)->name);
        }

        function FetchResultAssoc($result,&$array,$row, $skipConvert = false)
        {
            if(!mysqli_data_seek($result,$row)
                || !($array=mysqli_fetch_assoc($result)))
                return($this->SetError("Fetch result assoc",mysqli_error($this->connection)));
            $this->highest_fetched_row[metabase_unique_id_for_resource($result)]=max($this->highest_fetched_row[metabase_unique_id_for_resource($result)], $row);
            return ($skipConvert) ? $result : $this->ConvertResultRow($result,$array);
        }
        ### /ARJAN ###

        ### ARJAN ADDED: skipConvert ###
        function FetchResultArray($result,&$array,$row, $skipConvert = false)
        {
            if(!mysqli_data_seek($result,$row)
                || !($array=mysqli_fetch_row($result)))
                return($this->SetError("Fetch result array",mysqli_error($this->connection)));
            $this->highest_fetched_row[metabase_unique_id_for_resource($result)]=max($this->highest_fetched_row[metabase_unique_id_for_resource($result)],$row);
            return ($skipConvert) ? $result : $this->ConvertResultRow($result,$array);
        }

        function FetchCLOBResult($result,$row,$field)
        {
            return($this->FetchLOBResult($result,$row,$field));
        }

        function FetchBLOBResult($result,$row,$field)
        {
            return($this->FetchLOBResult($result,$row,$field));
        }

        function ConvertResult(&$value,$type)
        {
            switch($type)
            {
                case METABASE_TYPE_BOOLEAN:
                    $value=(strcmp($value,"Y") ? 0 : 1);
                    return(1);
                case METABASE_TYPE_DECIMAL:
                    if($this->emulate_decimal)
                        $value=sprintf("%.".$this->decimal_places."f",doubleval($value)/$this->decimal_factor);
                    return(1);
                case METABASE_TYPE_FLOAT:
                    $value=doubleval($value);
                    return(1);
                case METABASE_TYPE_DATE:
                case METABASE_TYPE_TIME:
                case METABASE_TYPE_TIMESTAMP:
                    return(1);
                default:
                    return($this->BaseConvertResult($value,$type));
            }
        }

        function NumberOfRows($result)
        {
            return(mysqli_num_rows($result));
        }

        function FreeResult($result)
        {
            unset($this->highest_fetched_row[metabase_unique_id_for_resource($result)]);
            unset($this->columns[metabase_unique_id_for_resource($result)]);
            unset($this->result_types[metabase_unique_id_for_resource($result)]);
            mysqli_free_result($result);
        }

        function GetCLOBFieldTypeDeclaration($name,&$field)
        {
            if(isset($field["length"]))
            {
                $length=$field["length"];
                if($length<=255)
                    $type="TINYTEXT";
                else
                {
                    if($length<=65535)
                        $type="TEXT";
                    else
                    {
                        if($length<=16777215)
                            $type="MEDIUMTEXT";
                        else
                            $type="LONGTEXT";
                    }
                }
            }
            else
                $type="LONGTEXT";
            return("$name $type".(isset($field["notnull"]) ? " NOT NULL" : ""));
        }

        function GetBLOBFieldTypeDeclaration($name,&$field)
        {
            if(isset($field["length"]))
            {
                $length=$field["length"];
                if($length<=255)
                    $type="TINYBLOB";
                else
                {
                    if($length<=65535)
                        $type="BLOB";
                    else
                    {
                        if($length<=16777215)
                            $type="MEDIUMBLOB";
                        else
                            $type="LONGBLOB";
                    }
                }
            }
            else
                $type="LONGBLOB";
            return("$name $type".(isset($field["notnull"]) ? " NOT NULL" : ""));
        }

        function GetIntegerFieldTypeDeclaration($name,&$field)
        {
            return("$name ".(isset($field["unsigned"]) ? "INT UNSIGNED" : "INT").(isset($field["autoincrement"]) ? " AUTO_INCREMENT" : (isset($field["default"]) ? " DEFAULT ".$field["default"] : "")).(isset($field["notnull"]) ? " NOT NULL" : ""));
        }

        function GetDateFieldTypeDeclaration($name,&$field)
        {
            return($name." DATE".(isset($field["default"]) ? " DEFAULT '".$field["default"]."'" : "").(isset($field["notnull"]) ? " NOT NULL" : ""));
        }

        # ARJAN #
        function GetDatetimeFieldTypeDeclaration($name,&$field)
        {
            return($name." DATETIME".(isset($field["default"]) ? " DEFAULT '".$field["default"]."'" : "").(isset($field["notnull"]) ? " NOT NULL" : ""));
        }
        # /ARJAN #

        function GetTimestampFieldTypeDeclaration($name,&$field)
        {
            return($name." DATETIME".(isset($field["default"]) ? " DEFAULT '".$field["default"]."'" : "").(isset($field["notnull"]) ? " NOT NULL" : ""));
        }

        function GetTimeFieldTypeDeclaration($name,&$field)
        {
            return($name." TIME".(isset($field["default"]) ? " DEFAULT '".$field["default"]."'" : "").(isset($field["notnull"]) ? " NOT NULL" : ""));
        }

        function GetFloatFieldTypeDeclaration($name,&$field)
        {
            if(isset($this->options["FixedFloat"]))
                $this->fixed_float=$this->options["FixedFloat"];
            else
            {
                if($this->connection==0)
                    $this->Connect();
            }
            return("$name DOUBLE".($this->fixed_float ? "(".($this->fixed_float+2).",".$this->fixed_float.")" : "").(isset($field["default"]) ? " DEFAULT ".$this->GetFloatFieldValue($field["default"]) : "").(isset($field["notnull"]) ? " NOT NULL" : ""));
        }

        function GetDecimalFieldTypeDeclaration($name,&$field)
        {
            return($name.($this->emulate_decimal ? " BIGINT" : " DECIMAL(30,".$this->decimal_places.")").(isset($field["default"]) ? " DEFAULT ".$this->GetDecimalFieldValue($field["default"]) : "").(isset($field["notnull"]) ? " NOT NULL" : ""));
        }

        function GetCLOBFieldValue($prepared_query,$parameter,$clob,&$value)
        {
            for($value="'";!MetabaseEndOfLOB($clob);)
            {
                if(MetabaseReadLOB($clob,$data,$this->lob_buffer_length)<0)
                {
                    $value="";
                    return($this->SetError("Get CLOB field value",MetabaseLOBError($clob)));
                }
                $this->EscapeText($data);
                $value.=$data;
            }
            $value.="'";
            return(1);
        }

        function FreeCLOBValue($prepared_query,$clob,&$value,$success)
        {
            Unset($value);
        }

        function GetBLOBFieldValue($prepared_query,$parameter,$blob,&$value)
        {
            for($value="'";!MetabaseEndOfLOB($blob);)
            {
                if(!MetabaseReadLOB($blob,$data,$this->lob_buffer_length))
                {
                    $value="";
                    return($this->SetError("Get BLOB field value",MetabaseLOBError($blob)));
                }
                $value.=AddSlashes($data);
            }
            $value.="'";
            return(1);
        }

        function FreeBLOBValue($prepared_query,$blob,&$value,$success)
        {
            Unset($value);
        }

        function GetFloatFieldValue($value)
        {
            return(!strcmp($value,"NULL") ? "NULL" : "$value");
        }

        function GetDecimalFieldValue($value)
        {
            return(!strcmp($value,"NULL") ? "NULL" : strval($this->emulate_decimal ? round(doubleval($value)*$this->decimal_factor) : $value));
        }

        function GetColumnNames($result,&$column_names)
        {
            $result_value=metabase_unique_id_for_resource($result);
            if(!isset($this->highest_fetched_row[$result_value]))
                return($this->SetError("Get column names","it was specified an inexisting result set"));
            if(!isset($this->columns[$result_value]))
            {
                $this->columns[$result_value]=array();
                $columns=mysqli_num_fields($result);
                for($column=0;$column<$columns;$column++) {
                    $this->columns[$result_value][strtolower(mysqli_fetch_field_direct($result,$column)->name)]=$column;}
            }
            $column_names=$this->columns[$result_value];
            return(1);
        }

        function NumberOfColumns($result)
        {
            if(!isset($this->highest_fetched_row[metabase_unique_id_for_resource($result)]))
            {
                $this->SetError("Get column names","it was specified an inexisting result set");
                return(-1);
            }
            return(mysqli_num_fields($result));
        }

        ### ARJAN ###
        function GetLastInsertId($tableName = '', $keyName = '') {
            if($result=$this->Query('SELECT LAST_INSERT_ID()')) {
                return (int)$this->FetchResult($result,0,0);
            }
            return false;
        }
        ### /ARJAN ###

        function GetSequenceNextValue($name,&$value)
        {
            $sequence_name=$this->sequence_prefix.$name;
            if(!$this->Query("INSERT INTO $sequence_name (sequence) VALUES (NULL)"))
                return(0);
            $value=intval(mysqli_insert_id($this->connection));
            if(!$this->Query("DELETE FROM $sequence_name WHERE sequence<$value"))
                $this->warning="could delete previous sequence table values";
            return(1);
        }

        function GetNextKey($table,&$key)
        {
            $key="NULL";
            return(1);
        }

        function GetInsertedKey($table,&$value)
        {
            $value=intval(mysqli_insert_id($this->connection));
            return(1);
        }

        function AutoCommitTransactions($auto_commit)
        {
            $this->Debug("AutoCommit: ".($auto_commit ? "On" : "Off"));
            if(!isset($this->supported["Transactions"]))
                return($this->SetError("Auto-commit transactions","transactions are not in use"));
            if(((!$this->auto_commit)==(!$auto_commit)))
                return(1);
            if($this->connection)
            {
                if($auto_commit)
                {
                    if(!$this->Query("COMMIT")
                        || !$this->Query("SET AUTOCOMMIT=1"))
                        return(0);
                }
                else
                {
                    if(!$this->Query("SET AUTOCOMMIT=0"))
                        return(0);
                }
            }
            $this->auto_commit=$auto_commit;
            return($this->RegisterTransactionShutdown($auto_commit));
        }

        function CommitTransaction()
        {
            $this->Debug("Commit Transaction");
            if(!isset($this->supported["Transactions"]))
                return($this->SetError("Commit transaction","transactions are not in use"));
            if($this->auto_commit)
                return($this->SetError("Commit transaction","transaction changes are being auto commited"));
            return($this->Query("COMMIT"));
        }

        function RollbackTransaction()
        {
            $this->Debug("Rollback Transaction");
            if(!isset($this->supported["Transactions"]))
                return($this->SetError("Rollback transaction","transactions are not in use"));
            if($this->auto_commit)
                return($this->SetError("Rollback transaction","transactions can not be rolled back when changes are auto commited"));
            return($this->Query("ROLLBACK"));
        }

        function Setup()
        {
            $this->supported["Sequences"]=
            $this->supported["Indexes"]=
            $this->supported["AffectedRows"]=
            $this->supported["SummaryFunctions"]=
            $this->supported["OrderByText"]=
            $this->supported["GetSequenceCurrentValue"]=
            $this->supported["SelectRowRanges"]=
            $this->supported["LOBs"]=
            $this->supported["Replace"]=
            $this->supported["AutoIncrement"]=
            $this->supported["PrimaryKey"]=
            $this->supported["OmitInsertKey"]=
                1;
            if(isset($this->options["UseTransactions"])
                && $this->options["UseTransactions"])
            {
                $this->supported["Transactions"]=1;
                $this->default_table_type="BDB";
            }
            else
                $this->default_table_type="";
            if(isset($this->options["DefaultTableType"]))
            {
                switch($this->default_table_type=strtoupper($this->options["DefaultTableType"]))
                {
                    case "BERKELEYDB":
                        $this->default_table_type="BDB";
                    case "BDB":
                    case "INNODB":
                    case "GEMINI":
                        break;
                    case "HEAP":
                    case "ISAM":
                    case "MERGE":
                    case "MRG_MYISAM":
                    case "MYISAM":
                        if(isset($this->supported["Transactions"]))
                            return($this->options["DefaultTableType"]." is not a transaction-safe default table type");
                        break;
                    default:
                        return($this->options["DefaultTableType"]." is not a supported default table type");
                }
            }
            if(isset($this->options["EmulateDecimal"])
                && $this->options["EmulateDecimal"])
            {
                $this->emulate_decimal=1;
                $this->decimal_factor=pow(10.0,$this->decimal_places);
            }
            return("");
        }
    };

}
?>
