<?php
global $PHprefs;

/**
 * THIS FILE IS DEPRECATED!!!!! (2009-08-01)
 * IT IS ONLY HERE FOR BACKWARDS COMPATIBILITY
 * DO NOT USE THE 'DBconnect' FUNCTION ANYMORE, USE 'PH::DBconnect'.
 */

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require "{$PHprefs['distDir']}/core/include/metabase/metabase_database.php";
require "{$PHprefs['distDir']}/core/include/metabase/metabase_interface.php";

/**
 * Establish a permanent database-connection. Relies on the following
 * $PHprefs variables:
 * DBserver, DBpasswd, DBname, DBport and DBtype
 *
 * @author Arjan Haverkamp
 * @return A connection to the specified database.
 */
function DBconnect($asObject = false)
{
	global $PHprefs;

	$options = isset($PHprefs['DBoptions']) ? $PHprefs['DBoptions'] : array();
	if ($asObject) {
		$error = MetaBaseSetupDatabaseObject(array(
			'Type' =>		$PHprefs['DBtype'],
			'Host' =>		$PHprefs['DBserver'],
			'User' =>		$PHprefs['DBuserid'],
			'Password' =>	$PHprefs['DBpasswd'],
			'Port' =>		$PHprefs['DBport'],
			'Persistent'	=> isset($PHprefs['DBpersistent']) ? $PHprefs['DBpersistent'] : true,
			'IncludePath' =>	"{$PHprefs['distDir']}/core/include/metabase/",
			'Options' => $options), $db);
		if ($error != '')
		{
			print "Cannot connect to database on '{$PHprefs['DBserver']}', user '{$PHprefs['DBuserid']}': $error!<br />\n";
			exit;
		}
		$db->SetDatabase($PHprefs['DBname']);
		//$db->Connect();
		return $db;
	}
	else {
		$error = MetaBaseSetupDatabase(array(
			'Type' =>		$PHprefs['DBtype'],
			'Host' =>		$PHprefs['DBserver'],
			'User' =>		$PHprefs['DBuserid'],
			'Password' =>	$PHprefs['DBpasswd'],
			'Port' =>		$PHprefs['DBport'],
			'Persistent'	=> isset($PHprefs['DBpersistent']) ? $PHprefs['DBpersistent'] : true,
			'IncludePath' =>	"{$PHprefs['distDir']}/core/include/metabase/",
			'Options' => $options), $db);
		if ($error != '')
		{
			print "Cannot connect to database on '{$PHprefs['DBserver']}', user '{$PHprefs['DBuserid']}': $error!<br />\n";
			exit;
		}
		$error = MetabaseSetDatabase($db, $PHprefs['DBname']);
		if ($error != '')
		{
			print "Cannot select database '{$PHprefs['DBname']}'!<br />\n";
			exit;
		}
		return $db;
	}
}
