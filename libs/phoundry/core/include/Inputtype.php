<?php

class Inputtype
{
	/**
	 * @var Column
	 */
	public $column;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var array|null
	 */
	public $extra = array();

	public function __construct($column, $extra = null)
	{
		global $db;

		$this->column = $column;
		$this->name = $this->column->name;

		if (is_null($extra)) {
			$sql = "SELECT inputtype_extra FROM phoundry_column WHERE id = {$column->id}";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur) && !$db->ResultIsNull($cur,0,'inputtype_extra')) {
				$this->extra = unserialize($db->FetchResult($cur,0,'inputtype_extra'));
			}
		}
		else {
			$this->extra = $extra;
		}
	}

	/**
	 * Translate a database value into a human-readable format.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'search', 'insert', 'update', 'export' or 'view'
	 * @param $value(string) The database value
	 * @return string A human-readable presentation of the database value
	 */
	public function getValue($page, $value, $keyval = null) 
	{
		return $value;
	}

	/**
	 * Create a FIELDSET containing a representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (pdf, view, update, insert, copy) to get the fieldset for
	 * @param $value(string) The value for the inputtype.
	 * @param $readOnly(bool) Whether this inputtype is read-only (non-editable)
	 * @param $includes(array) A reference to an array with this structure:
	 *                         'jsCode'=>array(),
	 *                         'jsFiles'=>array(),
	 *									'cssFiles'=>array()
	 *									Add entries to those arrays in order to have them
	 *									included in the <HEAD> of the edit-page.
	 * @return string HTML-code for a FIELDSET containing the inputtype.
	 */
	public function getHtml($page, $value = '', $readOnly = false, &$includes) 
	{
		global $PHprefs;

		if ($readOnly || $page == 'view') 
		{
			$GLOBALS['RECORD_ID'] = $value;
			if ($value === '' || is_null($value)) 
			{
				$value = '&nbsp;';
			}
			$html  = '<div class="fieldset-wrapper"><fieldset class="field"><legend><b>' . PH::htmlspecialchars($this->column->description) . '</b></legend>';
			$html .= ($readOnly && $page == 'update') ? $this->getValue('view',$value) : $value;
			$html .= "\n</fieldset></div>\n\n";
			return $html;
		}
		switch ($page) 
		{
			case 'insert':
			case 'update':
			case 'copy':
				$html  = '<div class="fieldset-wrapper'.($this->column->required ? ' fieldset-wrapper-req' : '').'"><fieldset class="field"><legend><b' . ($this->column->required ? ' class="req"' : '') . '>' . PH::htmlspecialchars($this->column->description) . '</b>';
				$helpers = array_merge($this->getHelperLinks($page), $this->column->datatype->getHelperLinks($page));
				if (count($helpers)) 
				{
					$html .= ' (' . implode(', ', $helpers) . ')';
				}
				$html .= "</legend>\n";
				if (!empty($this->column->info)) 
				{
					$html .= '<div class="miniinfo">' . $this->column->info . '</div>';
				}
				$html .= $this->getInputHtml($page, $value, $includes);
				$explanation = $this->column->datatype->getExplanation();
				if (!empty($explanation)) 
				{
					$html .= ' (' . $explanation . ')';
				}
				$html .= "\n</fieldset></div>\n\n";
				break;
			default:
				return $value;
		}
		return $html;
	}

	/**
	 * Get a HTML-representation of this inputtype.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get the inputtype for
	 * @param $value(string) The value for the inputtype.
	 * @param $includes(array) A reference to an array with this structure:
	 *                         'jsCode'=>array(),
	 *                         'jsFiles'=>array(),
	 *									'cssFiles'=>array()
	 *									Add entries to those arrays in order to have them
	 *									included in the <HEAD> of the edit-page.
	 * @return string HTML-code representing this inputtype.
	 */
	public function getInputHtml($page, $value = '', &$includes) 
	{
		if ($page == 'copy' && !empty($this->column->datatype->extra['encoding'])) 
		{
			// Do not copy encoded fields (so we don't reveal sha1/md5 encoded strings):
			$value = '';
		}

		if ($value === '' && $page == 'insert') 
		{
			$value = $this->column->datatype->getDefaultValue();
		}
		return PH::evaluate($value);
	}
	
	/**
	 * Get hyperlinks for helper-GUIs in FIELDSETs.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page (update, insert, copy) to get helpers for.
	 * @return array of helper-links.
	 */
	public function getHelperLinks($page) 
	{
		return array();
	}

	/**
	 * Retrieve Javascript code for this inputtype (used for fake columns).
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Currently, only 'overview' is known.
	 * @returns string of Javascript code
	 */
	public function getJScode($page) 
	{
		return '';
	}

	/**
	 * Retreive info about this inputtype, for use in the Phoundry Admin.
	 *
	 * @author Arjan Haverkamp
	 * @return array with info about this inputtype.
	 */
	public function getExtra() 
	{
		return array();
	}

	/**
	 * Store info for this inputtype, throught the Phoundry Admin.
	 *
	 * @author Arjan Haverkamp
	 * @param $msg(string) Contains an errormessage in case something is wrong.
	 * @return bool|array false in case of an error, a datastructure (array) otherwise.
	 */
	public function setExtra(&$msg) 
	{
		return array();
	}

	public function getExtraHelp($what) 
	{
		return '';
	}
}
