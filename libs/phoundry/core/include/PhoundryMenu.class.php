<?php

class PhoundryMenuItem {
	public $name, $table_id, $depth, $url, $id;
	
	public function __construct($name, $table_id, $depth, $url = '', $id = null) 
	{
		$this->name = $name;
		$this->table_id = $table_id;
		$this->depth = $depth;
		$this->url = $url;
		$this->id = $id;
	}
}

class PhoundryMenu {

public $lang;

/**
 * Constructor 
 */
public function __construct($lang) 
{
	global $PHprefs;

	$this->lang = $lang;
}

public function word($word) 
{
	if (PH::is_serialized($word) &&($tmp = @unserialize($word)) !== false) {
		return isset($tmp[$this->lang]) ? $tmp[$this->lang] : current($tmp);
	}
	return $word;
}


/**
 * Read the Phoundry-Menu into a datastructure.
 *
 * @author Arjan Haverkamp
 * @param $what(string) The menu to fetch: 'service', 'admin', 'admin_brickwork', 'tab0'...'tabn'
 * @param $groupId(int) The group-id the user is in.
 * @param $userId(int) The user-id of the user.
 * @param $isAdmin(bool) Whether the current user is an admin user.
 * @param $gtID(int) In case you want to get tableIDs greater than or equal to a specific value.
 * @return An array of PhoundryMenuItems, representing the Phoundry menu.
 * @returns array
 * @public
 */
public function readMenu($what, $userId, $groupId, $isAdmin = false, $gtID = 0) 
{
	global $PHprefs, $db;
	$menu = array();

	$tabID = null;
	if (preg_match('/^tab(\d+)/', $what, $reg)) {
		$tabID = $reg[1];
	}
	
	switch($what) {
		case 'service':
		case 'admin_brickwork':
			if ($isAdmin && $groupId == -1)
				$sql = "SELECT id, description FROM phoundry_table WHERE show_in_menu = '" . escDBquote($what) . "' ORDER BY order_by";
			else {
				// Determine group(s) this user belongs to:
				$sql = "SELECT pt.id, pt.description FROM phoundry_table pt, phoundry_group_table gt WHERE pt.show_in_menu = '" . escDBquote($what) . "' AND gt.group_id = " . (int)$groupId . " AND pt.id = gt.table_id ORDER BY pt.order_by";
			}
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++) {
				$TID = $db->FetchResult($cur,$x,'id');
				$menu[] = new PhoundryMenuItem($this->word($db->FetchResult($cur,$x,'description')), $TID, 1, $this->getPhoundryMenuUrl($TID));
			}
			break;
		case 'admin':
			$menu[] = new PhoundryMenuItem('Tables', 1, 1, $PHprefs['url'] . '/admin/table/');
			$menu[] = new PhoundryMenuItem('Columns', 1, 1, $PHprefs['url'] . '/admin/column/');
			$menu[] = new PhoundryMenuItem('Plugins', 1, 1, $PHprefs['url'] . '/admin/plugin/');
			$menu[] = new PhoundryMenuItem('Phoundry Menu', 1, 1, $PHprefs['url'] . '/admin/menu/');
			/*
			 *Translation interface disabled due to rearranging words files
			 *
			 *$menu[] = new PhoundryMenuItem('Interface translations', 1, 1, $PHprefs['url'] . '/admin/translate/');
			 */
			$menu[] = new PhoundryMenuItem('Search and Replace', 1, 1, $PHprefs['url'] . '/admin/search_replace/');
			$menu[] = new PhoundryMenuItem('Phoundry Sync', 1, 1, $PHprefs['url'] . '/admin/sync/');
			$menu[] = new PhoundryMenuItem('Upload browser', 1, 1, $PHprefs['url'] . '/admin/uploadBrowser/');
			$menu[] = new PhoundryMenuItem('Database Interface', 1, 1, $PHprefs['url'] . '/admin/DBinterface/');
			$menu[] = new PhoundryMenuItem('Database Export', 1, 1, $PHprefs['url'] . '/admin/DBexport/');
			$menu[] = new PhoundryMenuItem('Database Import', 1, 1, $PHprefs['url'] . '/admin/DBimport/');
			$menu[] = new PhoundryMenuItem('Database Sync', 1, 1, $PHprefs['url'] . '/admin/DBsync/');
			$menu[] = new PhoundryMenuItem('Preferences', 1, 1, $PHprefs['url']  . '/admin/prefs/');
			if ($isAdmin && $userId == 1) {
				$menu[] = new PhoundryMenuItem('String Encoder', 1, 1, $PHprefs['url'] . '/admin/encode/');
			}
			$menu[] = new PhoundryMenuItem('Mail test', 1, 1, $PHprefs['url'] . '/admin/mailtest/');
			$menu[] = new PhoundryMenuItem('License & PHPinfo', 1, 1, $PHprefs['url'] . '/admin/info/');
			$sql = "SELECT id, description FROM phoundry_table WHERE show_in_menu = 'admin' ORDER BY order_by";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++) {
				$menu[] = new PhoundryMenuItem($this->word($db->FetchResult($cur,$x,'description')), 1, 1, $this->getPhoundryMenuUrl($db->FetchResult($cur,$x,'id')));
			}
			break;
		default:
			// One of the tabs:
			// Figure out what tables user has access to:
			$sql = ($isAdmin && $groupId == -1) ? 
						"SELECT id FROM phoundry_table ORDER BY order_by" :
						"SELECT table_id FROM phoundry_group_table WHERE group_id = " . (int)$groupId;
			$TIDs = array(-1=>true);
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++) {
				$TIDs[$db->FetchResult($cur,$x,0)] = true;
			}
			
			// Get configured menu:
			$menu = $this->getPhoundryMenu(
			 			"SELECT id, parent, name, table_id
						 FROM phoundry_menu WHERE
						 tab_id = {$tabID}
						 AND (table_id IS NULL OR (table_id >= {$gtID} AND table_id IN (" . implode(',', array_keys($TIDs)) . ")))
						 AND parent IS NULL
						 ORDER BY id");
			$miscTIDs = array(-1=>true);
			foreach($menu as $item) {
				if ($item->table_id && array_key_exists($item->table_id, $TIDs)) {
					$miscTIDs[$item->table_id] = $item->table_id;
				}
			}

			// Now fetch all menu-items that are not in the 'phoundry_menu' table:
			if ($isAdmin && $groupId == -1) {
				$sql = "SELECT pt.id, pt.description FROM phoundry_table pt WHERE pt.show_in_menu = 'tab{$tabID}' AND pt.id >= {$gtID} AND pt.id NOT IN (" . implode(',', $miscTIDs) . ") ORDER BY pt.order_by";
			}
			else {
				$sql = "SELECT pt.id, pt.description FROM phoundry_group_table gt, phoundry_table pt WHERE pt.show_in_menu = 'tab{$tabID}' AND pt.id >= {$gtID} AND gt.group_id = " . (int)$groupId . " AND gt.table_id NOT IN (" . implode(',', $miscTIDs) . ') AND gt.table_id = pt.id ORDER BY pt.order_by';
			}
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++) {
				if ($x == 0) { $menu[] = new PhoundryMenuItem(word(249 /* Misc */), null, 1, ''); }
				$TID = $db->FetchResult($cur,$x,'id');
				$menu[] = new PhoundryMenuItem($this->word($db->FetchResult($cur,$x,'description')), $TID, 2, $this->getPhoundryMenuUrl($TID));
			}

			// Remove all empty folders...
			if (!($isAdmin && $groupId == -1) && !empty($menu)) {
				$menu = $this->removeEmptyFolders($menu);
			}

			break;
	}
	return $menu;
}

/**
 * Remove empty Folders from a Menu-tree.
 *
 * @author Arjan Haverkamp
 * @param $menu(array) An array of PhoundryMenuItems
 * @return An array of PhoundryMenuItems, with empty folders removed.
 * @returns array
 * @private
 */
public function removeEmptyFolders($menu) 
{
	if (empty($menu)) return $menu;
	$menuCount = count($menu) - 1;
	$curDepth = $menu[$menuCount]->depth;
	$toRemove = array();
		
	for ($x = $menuCount; $x >= 0; $x--) {
		if (!$menu[$x]->table_id) {
			// Folder
			if ($menu[$x]->depth >= $curDepth)
				$toRemove[] = $x;
		}
		$curDepth = $menu[$x]->depth;
	}
	
	foreach($toRemove as $rm) {
		unset($menu[$rm]);
	}
	
	$menu = array_values($menu);

	return empty($toRemove) ? $menu : $this->removeEmptyFolders($menu);
}

/**
 * Generate a URL for a specific table-id.
 *
 * @author Arjan Haverkamp
 * @param $table_id(int) The id of the table (column 'id' in DB-table 'phoundry_table') to generate the URL for.
 * @return A string containing the URL for the specified table.
 * @returns string
 * @private
 */
public function getPhoundryMenuUrl($TID) 
{
	global $db, $PHprefs;
	
	if (!$TID) return '';
	
	$sql = "SELECT extra FROM phoundry_table WHERE id = " . (int)$TID;
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$extra = $db->FetchResult($cur,0,'extra');
	$extra = empty($extra) ? array() : unserialize($extra);

	if (isset($extra['sUrl']) || isset($extra['rUrl'])) {
		$url = isset($extra['sUrl']) ? $extra['sUrl'] : $extra['rUrl'];
		if (preg_match('/^javascript:/i', $url))
			return $url;
		if ($url[0] != '/')
			$url = $PHprefs['url'] . '/' . $url;
		$sep = (strpos($url, '?') === false) ? '?' : '&';

		return $this->addMenuVars($url . (strpos($url, 'TID=') !== false ? '' : $sep . 'TID=' . $TID));
	}
	else
		return $this->addMenuVars($PHprefs['url'] . "/core/records.php?TID=$TID");
}

/**
 * Add custom variables to a URL.
 *
 * @author Arjan Haverkamp
 * @param $url(string) An existing URL, to which the passthru vars will be appended.
 * @return The new URL with the Passthru vars appended.
 * @returns string
 */
public function addMenuVars($url) 
{
	global $PHprefs;

	if (!isset($PHprefs['menuVars'])) {
		return $url;
	}

	$sep = (strpos($url, '?') === false) ? '?' : '&';
	foreach($PHprefs['menuVars'] as $name=>$src) {
		eval('$source = &' . $src . ';');
		if (isset($source[$name]) && strpos($url, "{$name}=") === false) {
			$url .= $sep . $name . '=' . urlencode($source[$name]);
			$sep = '&';
		}
	}
	return $url;
}

/**
 * Recursively read the menu-items that a user configured himself (using the 'Phoundry Menu') feature.
 *
 * @author Arjan Haverkamp
 * @param $query(string) The query to use.
 * @param $curid(int) The id of the current menu (used for recursion).
 * @param $depth(int) The depth of the current menu (used for recursion).
 * @return An array of PhoundryMenuItems.
 * @private
 */
public function getPhoundryMenu($query, $curid = '', $depth = 1) 
{
	global $db;

	$menu = array();
	
	$myQuery = $query;
	if ($curid != '') {
		$myQuery = str_replace('parent IS NULL', "parent = " . (int)$curid, $myQuery);
	}

	$cur = $db->Query($myQuery)
		or trigger_error("Query $myQuery failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$id = $db->FetchResult($cur,$x,'id');
		$table_id = $db->FetchResult($cur,$x,'table_id');
		$menu[] = new PhoundryMenuItem(
			$db->FetchResult($cur,$x,'name'),
			$table_id,
			$depth,
			$this->getPhoundryMenuUrl($table_id),
			$id
		);
								
		$menu = array_merge($menu, $this->getPhoundryMenu($query, $id, $depth+1));
	}
	return $menu;
}

public function getXMLmenuData($menuStruct) 
{
	$curDepth = 1;
	$str = '';
	$cnt = 0;
	foreach($menuStruct as $item) {
		if ($item->depth != $curDepth) {
			$diff = $item->depth - $curDepth;
			if ($diff < 0) {
				$str .= str_repeat('</item>', abs($diff));
			}
			$curDepth = $item->depth;
		}

		$name = $this->word($item->name);
		if ($item->table_id) {
			$str .= '<item text="' . PH::htmlspecialchars($name) . '" id="i' . $cnt . '_' . $item->table_id . '" />' . "\n";
		}
		else {
			$str .= '<item text="' . PH::htmlspecialchars($name) . '" id="f' . $cnt . '" im0="folderClosed.gif">' . "\n";
			$curDepth++;
		}
		$cnt++;
	}

	while ($curDepth-- > 1) {
		$str .= '</item>';
	}
	
	return '<tree id="0"><item id="froot" text="/" open="1" im0="folderOpen.gif">' . $str . '</item></tree>';
}

public function getMenuTree($menus, $idx) 
{
	$curDepth = $startDepth = $menus[$idx]->depth;
	$str = ''; $cnt = count($menus);
	for ($x = $idx; $x < $cnt; $x++) {
		$item = $menus[$x];
		if ($item->depth < $startDepth) break;
		if ($item->depth > $startDepth) continue;
		$name = $this->word($item->name);

		if ($item->table_id) {
			// Item:
			$str .= '   <li><a href="' . PH::htmlspecialchars($item->url) . '">' . PH::htmlspecialchars($name) . "</a></li>\n";
		}
		else {
			// Folder:
			$str .= '   <li><span>' . PH::htmlspecialchars($name) . '</span>';
			if (isset($menus[$x+1]) && $menus[$x+1]->depth > $curDepth) {
				$str .= "<ul>\n" . $this->getMenuTree($menus, $x+1) . "</ul>\n";
			}
			$str .= "</li>\n";
		}
	}

	return $str;
}

/**
 * Determine a user's shortcuts.
 *
 * @author Arjan Haverkamp
 * @param $userId(int) The userid of the user to get the shortcuts for
 * @param $DMDcid(int) The current campaign-id, if applicable (DMdelivery). null otherwise.
 * @return An associative array of shortcuts (key = name, value = url)
 * @retirn array
 * @public
 */
public function getShortcuts($userId, $DMDcid = null) 
{
	global $db;

	$shortcuts = array();
	$sql = "SELECT t.id, t.description FROM phoundry_table t, phoundry_user_shortcut s WHERE s.user_id = " . (int)$userId . " AND s.table_id = t.id";
	if (is_null($DMDcid)) {
		$sql .= " AND t.id > 50";
	}
	$sql .= " ORDER BY t.order_by";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$shortcuts[$this->word($db->FetchResult($cur,$x,'description'))] = $this->getPhoundryMenuUrl($db->FetchResult($cur,$x,'id'));
	}
	return $shortcuts;
}

} // end class

?>
