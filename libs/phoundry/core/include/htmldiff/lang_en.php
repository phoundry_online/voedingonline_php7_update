<?php
return array(
	'diff-movedto' => 'moved to $1', 
	'diff-styleadded' => '$1 style added', 
	'diff-added' => '$1 added',
	'diff-changedto' => 'changed to $1', 
	'diff-movedoutof' => 'moved out of $1', 
	'diff-styleremoved' => '$1 style removed', 
	'diff-removed' => '$1 removed', 
	'diff-changedfrom' => 'changed from $1', 
	'diff-src' => 'source',
	'diff-withdestination' => 'with destination $1', 
	'diff-with' => '&#32;with $1 $2',
	'diff-with-additional' => '$1 $2',  # only translate this message to other languages if you have to change it 	 
	'diff-with-final' => '&#32;and $1 $2', 
	'diff-width' => 'width',
	'diff-height' => 'height', 
	'diff-p' => "a <b>paragraph</b>", 
	'diff-blockquote' => "a <b>quote</b>", 
	'diff-h1' => "a <b>heading (level 1)</b>", 
	'diff-h2' => "a <b>heading (level 2)</b>", 
	'diff-h3' => "a <b>heading (level 3)</b>", 
	'diff-h4' => "a <b>heading (level 4)</b>", 
	'diff-h5' => "a <b>heading (level 5)</b>", 
	'diff-pre' => "a <b>preformatted block</b>", 
	'diff-div' => "a <b>division</b>", 
	'diff-ul' => "an <b>unordered list</b>", 
	'diff-ol' => "an <b>ordered list</b>", 
	'diff-li' => "a <b>list item</b>", 
	'diff-table' => "a <b>table</b>", 
	'diff-tbody' => "a <b>table's content</b>", 
	'diff-tr' => "a <b>row</b>", 'diff-td' => "a <b>cell</b>", 
	'diff-th' => "a <b>header</b>", 
	'diff-br' => "a <b>break</b>", 
	'diff-hr' => "a <b>horizontal rule</b>", 
	'diff-code' => "a <b>computer code block</b>", 
	'diff-dl' => "a <b>definition list</b>", 
	'diff-dt' => "a <b>definition term</b>", 
	'diff-dd' => "a <b>definition</b>", 
	'diff-input' => "an <b>input</b>", 
	'diff-form' => "a <b>form</b>", 
	'diff-img' => "an <b>image</b>", 
	'diff-span' => "a <b>span</b>", 
	'diff-a' => "a <b>link</b>", 'diff-i' => "<b>italics</b>", 
	'diff-b' => "<b>bold</b>", 'diff-strong' => "<b>strong</b>", 
	'diff-em' => "<b>emphasis</b>", 'diff-font' => "<b>font</b>", 
	'diff-big' => "<b>big</b>", 'diff-del' => "<b>deleted</b>", 
	'diff-tt' => "<b>fixed width</b>", 
	'diff-sub' => "<b>subscript</b>", 
	'diff-sup' => "<b>superscript</b>", 
	'diff-strike' => "<b>strikethrough</b>",
	
	'semicolon-separator' => ';&#32;',
	'comma-separator' => ',&#32;',
	'colon-separator' => ':&#32;',
	'autocomment-prefix' => '-&#32;',
	'pipe-separator' => '&#32;|&#32;',
	'word-separator' => '&#32;',
	'ellipsis' => '...',
	'percent' => '$1%',
	'parentheses' => '($1)', 
);