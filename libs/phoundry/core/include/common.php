<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

require_once 'errorHandling.php';

// This makes sure that any shell_exec spawns in phoundry also use the APP_MODE
putenv('APP_MODE='.getenv('APP_MODE'));

define('IN_PHOUNDRY', true);
require_once 'PHSes.class.php';
require_once 'PH.class.php';
if (isset($DMDprefs['sharedDir']) && is_file($DMDprefs['sharedDir'] . '/include/DMDcommon.php')) {
    include_once $DMDprefs['sharedDir'] . '/include/DMDcommon.php';
}

if (!isset($PHprefs['baseDir'])) {
	$PHprefs['baseDir'] = dirname($PHprefs['instDir']);
}
if (!isset($PHprefs['tmpDir'])) {
	$PHprefs['tmpDir'] = $PHprefs['baseDir'] . '/tmp';
}

// Default custom urls, can be overwritten through PREFS['customUrls']
$_customUrls = array(
	'pics'    => $PHprefs['url'] . '/layout/pics',
	'css'     => $PHprefs['url'] . '/layout/css',
	'login'   => $PHprefs['url'] . '/layout/login.php',
	'home'    => $PHprefs['url'] . '/layout/home.php',
	'menu'    => $PHprefs['url'] . '/layout/menu.php',
	'topmenu' => $PHprefs['url'] . '/layout/topmenu.php',
	'emailTmpl' => $PHprefs['distDir'] . '/layout/emailTmpl.html'
);

// overwrite custom urls if defined in the prefs
if (isset($PHprefs['customUrls']))
{
	$_customUrls = array_merge($_customUrls, $PHprefs['customUrls']);
}

$PHprefs['customUrls'] = $_customUrls;

// Rechtbreien van oude cssUrl
$PHprefs['cssUrl'] = $PHprefs['customUrls']['css'];

// Set locale, for CSV im/exports, escapeshellarg etc.
setlocale(LC_ALL, $PHprefs['charset'] == 'iso-8859-1' ? 'en_US' : 'en_US.utf8');
if (function_exists('mb_internal_encoding'))
{
	mb_internal_encoding($PHprefs['charset']);
}

if (isset($PHprefs['timezonePHP']))
{
	date_default_timezone_set($PHprefs['timezonePHP'])
		or die("Could not set timezone to {$PHprefs['timezonePHP']}");
}

if (!function_exists('getHeader'))
{// Can be overwritten in PREFS.php
	function getHeader($mainTitle, $subTitle = '', $filters = '')
	{
		global $db, $PHprefs, $DMDprefs;

		$menuclosed = '';
		if (preg_match('/(&|\?)closeMenu/', $_SERVER['REQUEST_URI'])) {
			$menuclosed = ' menu-closed';
		}
		$head = '<div class="menu-handle'.$menuclosed.'"></div>';
		$head .= "<div class=\"header\">\n";
		$head .= '<div id="pageTitle">' . $mainTitle . "</div>\n";
		if ($subTitle !== '') {
			$head .= '<div id="subTitle">' . $subTitle . "</div>\n";
		}
		if ($filters !== '') {
			$head .= '<div class="filters">' . $filters . "</div>\n";
		}
		$isReseller = isset($PHprefs['product']) && strtolower($PHprefs['product']) == 'dmdelivery' && !empty($DMDprefs['resellerLogo']);
		if (!$isReseller) {
			$head .= '<div class="header-logo"></div>';
		}
		$head .= "</div>\n";


		if (isset($PHprefs['product']) && $PHprefs['product'] == 'DMdelivery')
		{
			// Google Analytics:
			$head .=<<<EOJ
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12182631-58']);
  _gaq.push(['_setDomainName', '.dmdelivery.com']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_gat._anonymizeIp']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
EOJ;
		}
		return $head;
	}
}

// Check for POST's exceeding ini-limit post_max_size:
if (isset($_SERVER) && isset($_SERVER['CONTENT_LENGTH']))
{
	$_postMaxSize = ini_get('post_max_size');
	$_mul = substr($_postMaxSize, -1);
	$_mul = ($_mul == 'M' ? 1048576 : ($_mul == 'K' ? 1024 : ($_mul == 'G' ? 1073741824 : 1)));
	if ($_SERVER['CONTENT_LENGTH'] > $_mul*(int)$_postMaxSize && $_postMaxSize) {
		PH::phoundryErrorPrinter('POST request too large!', true);
	}
}

// Check for CSRF:
function checkCSRF()
{
	global $PHprefs;

	if (isset($PHprefs['useCSRF']) && $PHprefs['useCSRF'] === false)
	{
		return;
	}

	$csrfFound = false;
	// Find 'CSSRF*' argument in $_POST or Apache request headers:
	$ARR = $_POST;

	foreach(apache_request_headers() as $name=>$value)
	{
		if (preg_match('/^csrf(\d+)$/i', $name))
		{
			$ARR[$name] = $value; break;
		}
	}

	foreach($ARR as $name=>$val)
	{
		if (preg_match('/^csrf(\d+)$/i', $name))
		{
			$csrfFound = true;
			if (isset($_SESSION[$name]) && $_SESSION[$name] != $val)
			{// CSRF problem
				die('Invalid request (CSRF)!');
			}
			break;
		}
	}
	if (!$csrfFound)
	{
        trigger_error('Invalid request (no CSRF)', E_USER_WARNING);
		die('Invalid request (no CSRF)!');
	}
	if (isset($name, $_SESSION, $_SESSION[$name])) { unset($_SESSION[$name]); }
}

// Always connect to the database.
// Create global Metabase database object $db:
$db = PH::DBconnect();

if (isset($_SERVER['REMOTE_ADDR']))
{
	// Check for a valid product-key.
	if (!PH::validProductKey($PHprefs['productKey']))
	{
		trigger_error('Invalid product key!', E_USER_ERROR);
	}
	else
	{
		$decKey = explode('|', PH::WPdecode($PHprefs['productKey']));
		$PHprefs['license'] = $decKey[1];
	}

	if (!isset($dontStartSession) || $dontStartSession == false)
	{
		if (isset($PHprefs['sessionHandler']))
		{
			$parts = parse_url($PHprefs['sessionHandler']);
			switch($parts['scheme'])
			{
				case 'memcache':
					require_once 'MemCacheSession.class.php';
					$MemcacheSession = new MemCacheSession($parts['host'], $parts['port']);
					session_set_save_handler(
						array($MemcacheSession, 'Open'),
						array($MemcacheSession, 'Close'),
						array($MemcacheSession, 'Read'),
						array($MemcacheSession, 'Write'),
						array($MemcacheSession, 'Destroy'),
						array($MemcacheSession, 'GC')
					);
					break;
				case 'mysql':
					require_once 'DatabaseSession.class.php';
					$DatabaseSession = new DatabaseSession($db->connection);
					session_set_save_handler(
						array($DatabaseSession, 'phpSessionOpen'),
						array($DatabaseSession, 'phpSessionClose'),
						array($DatabaseSession, 'phpSessionRead'),
						array($DatabaseSession, 'phpSessionWrite'),
						array($DatabaseSession, 'phpSessionDestroy'),
						array($DatabaseSession, 'phpSessionGarbageCollect')
					);
					break;
				default:
					trigger_error("Session handler {$PHprefs['sessionHandler']} is not supported!", E_USER_ERROR);
			}
		}

		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
		{
			// Set secure session cookie, when on HTTPS:
			ini_set('session.cookie_secure',true);
		}
		ini_set('session.cookie_httponly',true);
		session_name('PHSes' . md5($_SERVER['HTTP_HOST']));
		session_start();

		// CSRF check.
		// You can 'disable' CSRF checks, by adding a hidden form field named 'CSRF' to the form.
		// The value of the field should be the basename of the script it's POST-ing to, WPencoded.
		// If your form POSTs to a script named /dir/index.php, and you want to circumvent CSRF check,
		// then add the following field to the form:
		// <input type="hidden" name="CSRF" value="83439303837343331313ff078607e2875646e696" />
		// The value in this example is the WP-encoded basename (index.php) of /dir/index.php.
		if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
			 (!isset($_POST['CSRF']) || $_POST['CSRF'] != PH::WPencode(basename($_SERVER['PHP_SELF'])))) {
			checkCSRF();
		}
	}

	if (isset($_SESSION['PHSes']))
	{
		// Is there a request for a change of group?
		if (isset($_GET['setGroupId']))
		{
			$_SESSION['PHSes']->setGroupId($_GET['setGroupId']);
		}
	}
	else
	{
		$PHSes = new PHSes();
		$PHSes->lang = isset($GLOBALS['userLang']) ? $GLOBALS['userLang'] : $PHprefs['languages'][0];
		$_SESSION['PHSes'] = $PHSes;
	}

	$PHSes = &$_SESSION['PHSes'];
	if (!$PHSes->lang)
	{
		$PHSes->lang = $PHprefs['languages'][0];
	}
	PH::includeLangFiles($PHSes->lang);
}
else
{
	PH::includeLangFiles(isset($GLOBALS['userLang']) ? $GLOBALS['userLang'] : 'en');
}
