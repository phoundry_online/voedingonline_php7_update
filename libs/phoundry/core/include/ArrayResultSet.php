<?php
class ArrayResultSet extends ResultSet
{
    public function __construct(array $source = array())
    {
        $source = array_values($source);
        $this->_count = count($source);
        $this->_current_index = 0;
        parent::__construct($source);
    }

    public function current()
    {
        if (array_key_exists($this->_current_index, $this->_source)) {
            return $this->_source[$this->_current_index];
        }
        return false;
    }
}