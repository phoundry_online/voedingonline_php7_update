<?php

/**
 * Mime mail composer class. Can handle: text and html bodies, embedded html
 * images and attachments.
 * Documentation and examples of this class are avaible here:
 * http://pear.php.net/manual/
 *
 * @notes This class is based on HTML Mime Mail class from
 *   Richard Heyes <richard@phpguru.org> which was based also
 *   in the mime_mail.class by Tobias Ratschiller <tobias@dnet.it> and
 *   Sascha Schumann <sascha@schumann.cx>.
 *
 * @author Richard Heyes <richard.heyes@heyes-computing.net>
 * @author Tomas V.V.Cox <cox@idecnet.com>
 * @package Mail
 * @access public
 */

require_once dirname(__FILE__) . '/DMDmimepart.php';

class DMDmime
{
    /**
     * Contains the plain text part of the email
     * @var string
     */
    private $_txtbody;
    /**
     * Contains the html part of the email
     * @var string
     */
    private $_htmlbody;

    private $_icalbody;
    /**
     * contains the mime encoded text
     * @var string
     */
    private $_mime;
    /**
     * contains the multipart content
     * @var string
     */
    private $_multipart;
    /**
     * list of the attached images
     * @var array
     */
    private $_html_images = array();

    /**
     * list of the attachements
     * @var array
     */
    private $_parts = array();
    /**
     * Build parameters
     * @var array
     */
    private $_build_params = array();
    /**
     * Headers for the mail
     * @var array
     */
    private $_headers = array();

    private $_errorMsg = '';

    private $_iconvPrefs = array(
        'scheme' => 'B', // Q = quoted-printable, B = base64
        'input-charset' => 'utf-8',
        'output-charset' => 'utf-8',
        'line-length' => 76
    );

    /*
    * Constructor function
    *
    * @access public
    */
    public function __construct($crlf = "\r\n")
    {
        if (!defined('MAIL_MIME_CRLF')) {
            define('MAIL_MIME_CRLF', $crlf);
        }

        $this->_build_params = array(
            'text_encoding' => '7bit',
            'html_encoding' => 'quoted-printable',
            '7bit_wrap' => 998,
            'html_charset' => 'ISO-8859-1',
            'text_charset' => 'ISO-8859-1',
            'head_charset' => 'ISO-8859-1'
        );
    }

    /*
     * Accessor function to set the body text. Body text is used if
     * it's not an html mail being sent or else is used to fill the
     * text/plain part that emails clients who don't support
     * html should show.
     *
     * @param string $data Either a string or the file name with the
     *        contents
     * @param bool $isfile If true the first param should be trated
     *        as a file name, else as a string (default)
     * @param bool If true the text or file is appended to the
     *        existing body, else the old body is overwritten
     * @return bool true on success or false on error
     * @access public
     */
    public function setTXTBody($data, $isfile = false, $append = false)
    {
        if (!$isfile) {
            if (!$append) {
                $this->_txtbody = $data;
            } else {
                $this->_txtbody .= $data;
            }
        } else {
            $cont = $this->_file2str($data, $error);
            if ($cont === false) {
                $this->_errorMsg = $error;
                return false;
            }
            if (!$append) {
                $this->_txtbody = $cont;
            } else {
                $this->_txtbody .= $cont;
            }
        }
        return true;
    }

    private function _file2str($file_name, &$error)
    {
        if (!is_readable($file_name)) {
            $error = 'File is not readable ' . $file_name;
            return false;
        }
        if (!$fd = fopen($file_name, 'rb')) {
            $error = 'Could not open ' . $file_name;
            return false;
        }
        $cont = fread($fd, filesize($file_name));
        fclose($fd);
        return $cont;
    }

    /*
     * Adds a html part to the mail
     *
     * @param string $data Either a string or the file name with the
     *        contents
     * @param bool $isfile If true the first param should be trated
     *        as a file name, else as a string (default)
     * @return bool true on success, false on failure
     * @access public
     */

    public function setIcalBody($ical)
    {
        $this->_icalbody = $ical;
        return true;
    }

    /*
     * Adds an image to the list of embedded images.
     *
     * @param string $file The image file name OR image data itself
     * @param string $c_type The content type
     * @param string $name The filename of the image. Only use if $file is the image data
     * @param bool $isfilename Whether $file is a filename or not. Defaults to true
     * @return bool true on success, false on failure
     * @access public
     */

    public function setHTMLBody($data, $isfile = false)
    {
        if (!$isfile) {
            $this->_htmlbody = $data;
        } else {
            $cont = $this->_file2str($data, $error);
            if ($cont === false) {
                $this->_errorMsg = $error;
                return false;
            }
            $this->_htmlbody = $cont;
        }
        return true;
    }

    /*
     * Adds a file to the list of attachments.
     *
     * @param string $file The file name of the file to attach OR the file data itself
     * @param string $c_type The content type
     * @param string $name The filename of the attachment. Only use if $file is the file data
     * @param bool $isFilename Whether $file is a filename or not. Defaults to true
     * @return bool true on success, false on failure
     * @access public
     */

    public function addHTMLImage($file, $c_type = 'application/octet-stream', $name = '', $isfilename = true)
    {
        $filedata = ($isfilename === true) ? $this->_file2str($file, $error) : $file;
        $filename = ($isfilename === true) ? basename($file) : basename($name);
        if ($filedata === false) {
            $this->_errorMsg = $error;
            return false;
        }
        $this->_html_images[] = array(
            'body' => $filedata,
            'path' => $name,
            'name' => $filename,
            'c_type' => $c_type,
            'cid' => 'DMD' . str_replace(array('.', ' '), '_', microtime())
        );
        return true;
    }

    /*
    * Returns the contents of the given file name as string
    * @param string $file_name
    * @return string (the file contents) on succes, false otherwise.
    * @access private
    */

    public function addAttachment($file, $c_type = 'application/octet-stream', $name = '', $isfilename = true, $encoding = 'base64')
    {
        $filedata = ($isfilename === true) ? $this->_file2str($file, $error) : $file;
        if ($isfilename === true) {
            // Force the name the user supplied, otherwise use $file
            $filename = (!empty($name)) ? $name : $file;
        } else {
            $filename = $name;
        }
        if (empty($filename)) {
            $this->_errorMsg = 'The supplied filename for the attachment can\'t be empty';
            return false;
        }
        $filename = basename($filename);
        if ($filedata === false) {
            return false;
        }

        $this->_parts[] = array(
            'body' => $filedata,
            'name' => $filename,
            'c_type' => $c_type,
            'encoding' => $encoding
        );
        return true;
    }

    /*
     * Adds a text subpart to the mimePart object and
     * returns it during the build process.
     *
     * @param mixed    The object to add the part to, or
     *                 null if a new object is to be created.
     * @param string   The text to add.
     * @return object  The text mimePart object
     * @access private
     */

    public function get($build_params = null)
    {
	    if (!is_null($build_params)) {
		    foreach($build_params as $key => $value) {
		    	$this->_build_params[$key] = $value;
		    }
        }

        if (!empty($this->_html_images) && isset($this->_htmlbody)) {
            foreach ($this->_html_images as $value) {
                $this->_htmlbody = str_replace($value['path'], 'cid:' . $value['cid'], $this->_htmlbody);
            }
        }

        $null = null;
        $attachments = !empty($this->_parts) ? TRUE : FALSE;
        $html_images = !empty($this->_html_images) ? TRUE : FALSE;
        $html = !empty($this->_htmlbody) ? TRUE : FALSE;
        $text = (!$html && !empty($this->_txtbody)) ? TRUE : FALSE;

        switch (TRUE) {
            case $text && !$attachments:
                if ($this->_icalbody) {
                    $message = $this->_addAlternativePart($null);
                    $this->_addTextPart($message, $this->_txtbody);
                    $this->_addIcalPart($message);
                } else {
                    $message = $this->_addTextPart($null, $this->_txtbody);
                }
                break;

            case !$text && !$html && $attachments:
                $message = $this->_addMixedPart();

                $cnt = count($this->_parts);
                for ($i = 0; $i < $cnt; $i++) {
                    $this->_addAttachmentPart($message, $this->_parts[$i]);
                }
                break;

            case $text && $attachments:
                $message = $this->_addMixedPart();
                $this->_addTextPart($message, $this->_txtbody);

                $cnt = count($this->_parts);
                for ($i = 0; $i < $cnt; $i++) {
                    $this->_addAttachmentPart($message, $this->_parts[$i]);
                }
                if ($this->_icalbody) {
                    $this->_addIcalPart($message);
                }
                break;

            case $html && !$attachments && !$html_images:
                if (isset($this->_txtbody)) {
                    $message = $this->_addAlternativePart($null);
                    $this->_addTextPart($message, $this->_txtbody);
                    $this->_addHtmlPart($message);
                } else {
                    $message = $this->_addHtmlPart($null);
                }
                if ($this->_icalbody) {
                    $this->_addIcalPart($message);
                }
                break;

            case $html && !$attachments && $html_images:
                if (isset($this->_txtbody)) {
                    $message = $this->_addAlternativePart($null);
                    $this->_addTextPart($message, $this->_txtbody);
                    $related = $this->_addRelatedPart($message);
                } else {
                    $message = $this->_addRelatedPart($null);
                    $related = $message;
                }
                if ($this->_icalbody) {
                    $this->_addIcalPart($message);
                }
                $this->_addHtmlPart($related);
                $cnt = count($this->_html_images);
                for ($i = 0; $i < $cnt; $i++) {
                    $this->_addHtmlImagePart($related, $this->_html_images[$i]);
                }
                break;

            case $html && $attachments && !$html_images:
                $message = $this->_addMixedPart();
                if (isset($this->_txtbody)) {
                    $alt = $this->_addAlternativePart($message);
                    $this->_addTextPart($alt, $this->_txtbody);
                    $this->_addHtmlPart($alt);
                } else {
                    $this->_addHtmlPart($message);
                }
                if ($this->_icalbody) {
                    $this->_addIcalPart($message);
                }
                $cnt = count($this->_parts);
                for ($i = 0; $i < $cnt; $i++) {
                    $this->_addAttachmentPart($message, $this->_parts[$i]);
                }
                break;

            case $html && $attachments && $html_images:
                $message = $this->_addMixedPart();
                if (isset($this->_txtbody)) {
                    $alt = $this->_addAlternativePart($message);
                    $this->_addTextPart($alt, $this->_txtbody);
                    $rel = $this->_addRelatedPart($alt);
                } else {
                    $rel = $this->_addRelatedPart($message);
                }
                $this->_addHtmlPart($rel);
                $cnt = count($this->_html_images);
                for ($i = 0; $i < $cnt; $i++) {
                    $this->_addHtmlImagePart($rel, $this->_html_images[$i]);
                }
                $cnt = count($this->_parts);
                for ($i = 0; $i < $cnt; $i++) {
                    $this->_addAttachmentPart($message, $this->_parts[$i]);
                }
                if ($this->_icalbody) {
                    $this->_addIcalPart($message);
                }
                break;
        }

        if (isset($message)) {
            $output = $message->encode();
            $this->_headers = array_merge($this->_headers, $output['headers']);

            return $output['body'];

        } else {
            return FALSE;
        }
    }

    /*
     * Adds a html subpart to the mimePart object and
     * returns it during the build process.
     *
     * @param mixed    The object to add the part to, or
     *                 null if a new object is to be created.
     * @return object  The html mimePart object
     * @access private
     */

    private function _addAlternativePart(&$obj)
    {
        $params = array('content_type' => 'multipart/alternative');
        if (is_object($obj)) {
            return $obj->addSubpart('', $params);
        } else {
            return new DMDmimepart('', $params);
        }
    }

    private function _addTextPart(&$obj, $text)
    {
        $params = array(
            'content_type' => 'text/plain',
            'encoding' => $this->_build_params['text_encoding'],
            'charset' => $this->_build_params['text_charset']
        );
        if (is_object($obj)) {
            return $obj->addSubpart($text, $params);
        } else {
            return new DMDmimepart($text, $params);
        }
    }

    /*
     * Creates a new mimePart object, using multipart/mixed as
     * the initial content-type and returns it during the
     * build process.
     *
     * @return object  The multipart/mixed mimePart object
     * @access private
     */

    private function _addIcalPart(&$obj)
    {
        $params = array(
            'content_type' => 'text/calendar; method=REQUEST; name="invite.ics"',
            'encoding' => '8bit',
            'charset' => 'utf-8'
        );
        if (is_object($obj)) {
            return $obj->addSubpart($this->_icalbody, $params);
        } else {
            return new DMDmimepart($this->_icalbody, $params);
        }
    }

    /*
     * Adds a multipart/alternative part to a mimePart
     * object, (or creates one), and returns it  during
     * the build process.
     *
     * @param mixed    The object to add the part to, or
     *                 null if a new object is to be created.
     * @return object  The multipart/mixed mimePart object
     * @access private
     */

    private function _addMixedPart()
    {
        $params = array('content_type' => 'multipart/mixed');
        return new DMDmimepart('', $params);
    }

    /*
     * Adds a multipart/related part to a mimePart
     * object, (or creates one), and returns it  during
     * the build process.
     *
     * @param mixed    The object to add the part to, or
     *                 null if a new object is to be created.
     * @return object  The multipart/mixed mimePart object
     * @access private
     */

    private function _addAttachmentPart(&$obj, $value)
    {
        $params = array(
            'content_type' => $value['c_type'],
            'encoding' => $value['encoding'],
            'disposition' => 'attachment',
            'dfilename' => $value['name']
        );
        $obj->addSubpart($value['body'], $params);
    }

    /*
     * Adds an html image subpart to a mimePart object
     * and returns it during the build process.
     *
     * @param  object  The mimePart to add the image to
     * @param  array   The image information
     * @return object  The image mimePart object
     * @access private
     */

    private function _addHtmlPart(&$obj)
    {
        $params = array(
            'content_type' => 'text/html',
            'encoding' => $this->_build_params['html_encoding'],
            'charset' => $this->_build_params['html_charset']
        );
        if (is_object($obj)) {
            return $obj->addSubpart($this->_htmlbody, $params);
        } else {
            return new DMDmimepart($this->_htmlbody, $params);
        }
    }

    /*
     * Adds an attachment subpart to a mimePart object
     * and returns it during the build process.
     *
     * @param  object  The mimePart to add the image to
     * @param  array   The attachment information
     * @return object  The image mimePart object
     * @access private
     */

    private function _addRelatedPart(&$obj)
    {
        $params = array('content_type' => 'multipart/related');
        if (is_object($obj)) {
            return $obj->addSubpart('', $params);
        } else {
            return new DMDmimepart('', $params);
        }
    }

    /*
     * Builds the multipart message from the list ($this->_parts) and
     * returns the mime content.
     *
     * @param  array  Build parameters that change the way the email
     *                is built. Should be associative. Can contain:
     *                text_encoding  -  What encoding to use for plain text
     *                                  Default is 7bit
     *                html_encoding  -  What encoding to use for html
     *                                  Default is quoted-printable
     *                7bit_wrap      -  Number of characters before text is
     *                                  wrapped in 7bit encoding
     *                                  Default is 998
     *                html_charset   -  The character set to use for html.
     *                                  Default is iso-8859-1
     *                text_charset   -  The character set to use for text.
     *                                  Default is iso-8859-1
     *                head_charset   -  The character set to use for headers.
     *                                  Default is iso-8859-1
     * @return string The mime content
     * @access public
     */

    private function _addHtmlImagePart(&$obj, $value)
    {
        $params = array(
            'content_type' => $value['c_type'],
            'encoding' => 'base64',
            'disposition' => 'inline',
            'dfilename' => $value['name'],
            'cid' => $value['cid']
        );
        $obj->addSubpart($value['body'], $params);
    }

    /*
     * Returns an array with the headers needed to prepend to the email
     * (MIME-Version and Content-Type). Format of argument is:
     * $array['header-name'] = 'header-value';
     *
     * @param  array $xtra_headers Assoc array with any extra headers. Optional.
     * @return array Assoc array with the mime headers
     * @access public
     */

    /**
     * Get the text version of the headers
     * (usefull if you want to use the PHP mail() function)
     *
     * @param  array $xtra_headers Assoc array with any extra headers. Optional.
     * @return string Plain text headers
     * @access public
     */
    public function txtHeaders($xtra_headers = null)
    {
        $headers = $this->headers($xtra_headers);
        $ret = '';
        foreach ($headers as $key => $val) {
            $ret .= "$key: $val" . MAIL_MIME_CRLF;
        }
        return $ret;
    }

    public function headers($headers = array())
    {
        // Content-Type header should already be present,
        // So just add mime version header
        $this->_headers = array('MIME-Version' => '1.0') + $headers + $this->_headers;
        return $this->_encodeHeaders($this->_headers);
    }

    /**
     * Encodes a header as per RFC2047
     * Only encodes 'from', 'reply-to', 'subject' and 'errors-to' headers.
     *
     * @param  string $input The header data to encode
     * @return string         Encoded data
     * @access private
     */
    private function _encodeHeaders($input)
    {
        $prefs = $this->_iconvPrefs;
        $prefs['output-charset'] = $this->_build_params['head_charset'];

        foreach ($input as $hdr_name => $hdr_value) {
            if (!in_array(strtolower($hdr_name), array('from', 'reply-to', 'subject', 'errors-to'))) continue;

            $suffix = '';
            if (preg_match('/<[^>]+>$/', $hdr_value, $reg)) {
                $suffix = $reg[0];
                $hdr_value = trim(str_replace($suffix, '', $hdr_value));
            }

            if ($hdr_value && preg_match('/[\x80-\xFF]/', $hdr_value)) {
                $encoded = @iconv_mime_encode('', $hdr_value, $prefs);
                if ($encoded === false) {
                    // Fallback to utf-8:
                    $prefs['output-charset'] = 'utf-8';
                    $encoded = iconv_mime_encode('', $hdr_value, $prefs);
                }
                $hdr_value = substr($encoded, 2);
            }

            if ($suffix) {
                $hdr_value = '"' . str_replace('"', '\\"', $hdr_value) . '" ' . $suffix;
            }
            $input[$hdr_name] = trim($hdr_value);
        }
        return $input;
    }

    /**
     * Sets the Subject header
     *
     * @param  string $subject String to set the subject to
     * access  public
     */
    public function setSubject($subject)
    {
        $this->_headers['Subject'] = $subject;
    }

    /**
     * Set an email to the From (the sender) header
     *
     * @param string $email The email direction to add
     * @access public
     */
    public function setFrom($email)
    {
        $this->_headers['From'] = $email;
    }

    /**
     * Add an email to the Cc (carbon copy) header
     * (multiple calls to this method is allowed)
     *
     * @param string $email The email direction to add
     * @access public
     */
    public function addCc($email)
    {
        if (isset($this->_headers['Cc'])) {
            $this->_headers['Cc'] .= ", $email";
        } else {
            $this->_headers['Cc'] = $email;
        }
    }

    /**
     * Add an email to the Bcc (blank carbon copy) header
     * (multiple calls to this method is allowed)
     *
     * @param string $email The email direction to add
     * @access public
     */
    public function addBcc($email)
    {
        if (isset($this->_headers['Bcc'])) {
            $this->_headers['Bcc'] .= ", $email";
        } else {
            $this->_headers['Bcc'] = $email;
        }
    }

    /**
     * Return this class' last error message.
     *
     * @return string    The last error message.
     * @access public
     */
    public function getLastError()
    {
        return $this->_errorMsg;
    }

} // End of class
