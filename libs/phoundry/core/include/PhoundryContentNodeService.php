<?php
require_once "PhoundryContentNode.php";

class PhoundryContentNodeService
{
	/**
	 * @var DB_Adapter
	 */
	private $db;
	
	/**
	 * @var string
	 */
	private $cache_file;
	
	/**
	 * @var array
	 */
	private $cache;
	
	/**
	 * @var false
	 */
	private $cache_dirty;
	
	public function __construct(DB_Adapter $db, $cache_file = null)
	{
		global $PHprefs;
		$this->db = $db;
		$this->cache_file = $cache_file === null ?
			"{$PHprefs['tmpDir']}/cache/PhoundryContentNodeService.cache" :
			$cache_file;
		$cache_dir = dirname($this->cache_file);
		
		if (!is_dir($cache_dir) && !mkdir($cache_dir, 0777, true)) {
			throw new Exception("Directory containing cache file does not ".
				"exist nor could it be created");
		}
		
		$this->cache = $this->readCacheFile($this->cache_file);
		$this->cache_dirty = false;
	}
	
	public function __destruct()
	{
		if ($this->cache_dirty) {
			$this->writeCacheFile($this->cache_file, $this->cache);
		}
	}
	
	/**
	 * Gets a ContentNode by its id
	 * 
	 * @param int $id
	 * @return PhoundryContentNode|bool
	 */
	public function getById($id)
	{
		if (!isset($this->cache[$id])) {
			$row = $this->db->iQuery(sprintf("
				SELECT
					string_id, rid
				FROM
					phoundry_content_node
				WHERE
					id = %d
				LIMIT 1",
				$id
			))->first();
			
			if ($row) {
				$this->cacheNode(new PhoundryContentNode($id,
					$row->string_id, $row->rid));
			}
			else {
				$this->cache[$id] = false;
			}
		}
		return $this->cache[$id];
	}
	
	/**
	 * Gets a ContentNode by string id and record id
	 * @throws Exception
	 * @param string $string_id
	 * @param int $record_id
	 * @param bool $create autocreate if not exists
	 * @return PhoundryContentNode
	 */
	public function getByStringIdAndRecordId($string_id, $record_id, $create = true)
	{
		if (!isset($this->cache[$string_id."_".$record_id])) {
			$row = $this->db->iQuery(sprintf("
				SELECT
					id
				FROM
					phoundry_content_node
				WHERE
					string_id = %s
				AND
					rid = %d
				LIMIT 1",
				$this->db->quote($string_id, true),
				$record_id
			))->first();
			
			if ($row) {
				$node = new PhoundryContentNode($row->id, $string_id, $record_id);
				$this->cacheNode($node);
				return $node;
			}
			
		}
		else {
			$content_id = $this->cache[$string_id."_".$record_id];
			return $this->cache[$content_id];
		}
		
		if ($create) {
			$node = new PhoundryContentNode(null, $string_id, $record_id);
			$this->add($node);
			return $node;
		}
		throw new Exception("Content node not found");
	}

	/**
	 * Store a new Content Node
	 * @param PhoundryContentNode $node
	 * @return int the newly generated id for the contentnode
	 */
	public function add(PhoundryContentNode $node)
	{
		$res = $this->db->iQuery(sprintf("
			INSERT INTO phoundry_content_node (string_id, rid)
			VALUES (%s, %d)",
			$this->db->quote($node->string_id, true),
			$node->rid
		));
		
		$node->id = $res->last_insert_id;
		
		$this->cacheNode($node);
		return $node->id;
	}
		
	/**
	 * Update an existing collection
	 * @param PhoundryContentNode $node
	 * @return int the id of the collection
	 */
	public function update(PhoundryContentNode $node)
	{
		$this->db->iQuery(sprintf("
			UPDATE phoundry_content_node
			SET
				string_id = %s,
				rid = %d
			WHERE id = %d",
			$this->db->quote($node->string_id, true),
			$node->rid,
			$node->id
		));
		
		$this->cacheNode($node);
		return $node->id;
	}
	
	/**
	 * Deletes a Content Node
	 * @param PhoundryContentNode $node
	 */
	public function delete(PhoundryContentNode $node)
	{
		$this->db->iQuery(sprintf("
			DELETE FROM phoundry_content_node
			WHERE id = %d",
			$node->id
		));
		
		unset($this->cache[$node->string_id."_".$node->rid]);
		unset($this->cache[$node->id]);
		$this->cache_dirty = true;
	}
	
	private function cacheNode(PhoundryContentNode $node)
	{
		$this->cache[$node->string_id."_".$node->rid] = $node->id;
		$this->cache[$node->id] = $node;
		$this->cache_dirty = true;
	}
	
	/**
	 * Read the cachefile
	 * 
	 * @param string $file
	 * @return array
	 */
	private function readCacheFile($file)
	{
        if (file_exists($file)) {
            $cache = include $file;
            if (is_array($cache)) {
                return $cache;
            }
        }
        return array();
	}
	
	/**
	 * Write the cache to the designated file
	 * 
	 * @param string $file
	 * @param array $cache
	 */
	private function writeCacheFile($file, array $cache)
	{
		file_put_contents($file, "<"."?php return (".
			var_export($cache, true).");");
	}
}