<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}
require_once "{$PHprefs['distDir']}/core/include/common.php";

// Backwards compatibility with 'popup=close' GET argument:
if (isset($_GET['popup']) && !isset($_GET['callback'])) {
	$_GET['callback'] = 'void';
}

// Backwards compatibility: _editNext used to be called '_insertAgain'
if (isset($_POST['_insertAgain'])) {
	$_POST['_editNext'] = $_POST['_insertAgain'];
	unset($_POST['_insertAgain']);
}

/**
 ** The Phoundry object, the main class.
 ** @author Arjan Haverkamp
 **/

class Phoundry 
{
	/** @var int phoundry table id */
	public $id;
	/** @var string phoundry table string_id */
	public $string_id;
	/** @var string the actual mysql table name */
	public $name;
	/** @var string The name of the primary key for this table */
	public $key;
	/** @var Column[] */
	public $cols = array();

	/** @var string Friendly name */
	public $description;
	/** @var string */
	public $info;
	/** @var int */
	public $maxRecords;
	/** @var array[] list of arrays that hold the row values from phoundry_filter table */
	public $filters;
	/** @var array key value pairs */
	public $extra;
	/** @var array Events for this table coming from phoundry_event */
	public $events;


	/**
	 * The rights the currently selected group in the session has on this table
	 *
	 * If the key exists in this array the group has the rights. So rights can
	 * be checked for the current group like this
	 * if (isset($phoundry->rights['update'])) {}
	 *
	 * @var int[]
	 */
	public $rights;
	/**
	 * SQL that can be used in the ORDER BY clause ex: 'id DESC'
	 *
	 * @var string
	 */
	public $recordOrder;

	/** @var string records page url */
	public $rUrl;
	/** @var string insert page url */
	public $iUrl;
	/** @var string update page url */
	public $uUrl;
	/** @var string delete page url */
	public $dUrl;
	/** @var string delete multiple url */
	public $daUrl;
	/** @var string preview page url */
	public $vUrl;
	/** @var string copy page url */
	public $cUrl;

	/**
	 * Phoundry constructor.
	 * Initialises all DB-columns that are editable through Phoundry.
	 *
	 * @author Arjan Haverkamp
	 * @param $TID(int) The id of the table to initialise (field 'id' in table 'phoundry_table').
	 * @param $initColumns(bool) Whether or not to initialise the columns for this table.
	 */
	function __construct($TID = null, $initColumns = true) {
		if ($TID === null) {
			trigger_error('Creating a Phoundry object without specifying a table_id is deprecated', E_USER_DEPRECATED);
			return;
		}

		$this->changeTable($TID, $initColumns);
	}

	function changeTable($TID, $initColumns = true) {
		global $db, $PHSes, $PHenv, $PHprefs;

		if ($this->id !== null) {
			trigger_error('Changing table after Phoundry instantiation is deprecated.' .
				' Instead just construct a new Phoundry object', E_USER_DEPRECATED);
		}

		$this->id = (int)$TID;
		$sql = "SELECT * FROM phoundry_table WHERE id = {$this->id}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$db->FetchResultAssoc($cur,$row,0);
		$db->FreeResult($cur);

		$this->cols  = array();
		$this->string_id = isset($row['string_id']) ? $row['string_id'] : '';
		$this->name  = isset($row['name']) ? $row['name'] : '';

		// For DMdelivery:
		if (isset($PHenv['DMDcid'])) {
			$this->name = (($this->name == 'recipient_{$CAMPAIGN_ID}') || ($this->name == 'recipient_CAMPAIGN_ID')) ? $PHenv['rcptTable'] : strtr($this->name, array('{$CAMPAIGN_ID}' => $PHenv['DMDcid'], 'CAMPAIGN_ID' => $PHenv['DMDcid']));
			$this->DMDcampaignRelated = ($TID >= 8 && $TID <= 27);
		}
		else {
			$this->name = PH::evaluate($this->name);
			$this->DMDcampaignRelated = false;
		}
		
		$this->description = PH::langWord($row['description']);
		$this->maxRecords  = isset($row['max_records']) ? $row['max_records'] : -1;
		$this->filters = array();
		$sql = "SELECT * FROM phoundry_filter WHERE table_id = {$this->id} AND (group_id IS NULL OR group_id = {$PHSes->groupId}) ORDER BY id";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$db->FetchResultAssoc($cur,$filter,$x);
			
			if (PH::is_serialized($filter['name']) && ($tmp = @unserialize($filter['name'])) !== false) {
				// Multilang filter name
				$filter['name'] = isset($tmp[$PHSes->lang]) ? $tmp[$PHSes->lang] : current($tmp);
			}

			$this->filters[] = $filter;
		}
		$db->FreeResult($cur);

		$this->events = array();
		$sql = "SELECT event, code FROM phoundry_event WHERE table_id = {$this->id} ORDER BY id";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$event = $db->FetchResult($cur,$x,'event');
			if(!isset($this->events[$event])) $this->events[$event] = array();
			$this->events[$event][] = $db->FetchResult($cur,$x,'code');
		}
		$db->FreeResult($cur);

		$this->info  = isset($row['info']) ? PH::evaluate(PH::langWord($row['info'])) : '';
		$this->extra = empty($row['extra']) ? array() : unserialize($row['extra']);

		$this->rUrl = $PHprefs['url'] . '/core/records.php';
		$this->iUrl = $PHprefs['url'] . '/core/insert.php';
		$this->uUrl = $PHprefs['url'] . '/core/update.php';
		$this->dUrl = $PHprefs['url'] . '/core/delete.php';
		$this->daUrl = $PHprefs['url'] . '/core/deleteAll.php';
		$this->vUrl = $PHprefs['url'] . '/core/preview.php';
		$this->cUrl = $PHprefs['useCopy'] ? $PHprefs['url'] . '/core/copy.php' : '';

		// Are there custom urls defined for this table?

		if (isset($this->extra['rUrl']))
			$this->rUrl = $PHprefs['url'] . '/' . $this->extra['rUrl']; // Record overview URL
		if (isset($this->extra['iUrl']))
			$this->iUrl = $PHprefs['url'] . '/' . $this->extra['iUrl']; // Insert URL
		if (isset($this->extra['uUrl']))
			$this->uUrl = $PHprefs['url'] . '/' . $this->extra['uUrl']; // Update URL
		if (isset($this->extra['dUrl'])) {
			$this->dUrl = $this->daUrl = $PHprefs['url'] . '/' . $this->extra['dUrl']; // Delete URLs
		}
		if (isset($this->extra['vUrl'])) {
			$this->vUrl = $PHprefs['url'] . '/' . $this->extra['vUrl']; // View URL
		}
		if (isset($this->extra['cUrl'])) {
			$this->cUrl = $PHprefs['url'] . '/' . $this->extra['cUrl']; // Copy URL
		}

		$plugin = $row['is_plugin'];
		if ($plugin == 0 && $this->name != 'phoundry_group_table' && $this->name != 'phoundry_group_column') {
			$db->GetTableIndexDefinition($this->name, 'PRIMARY', $index)
				or trigger_error("Cannot get index for table {$this->name}: " . $db->Error(), E_USER_ERROR);
			$this->key = array_keys($index['FIELDS']);
			$this->key = $this->key[0];
		}
		else {
			$this->key = 'id'; // Dangerous default value, but anyways...
		}

		// What is the key-type (integer, string)?
		$db->GetTableFieldDefinition($this->name, $this->key, $keyInfo);
		
		$this->keyType = !is_null($keyInfo) && !is_null($keyInfo[0]) ? $keyInfo[0]['type'] : null;

		//$this->recordOrder = !empty($row['record_order']) ? $row['record_order'] : "{$this->key} DESC";
		$this->recordOrder = !($row['record_order']) ? null : $row['record_order'];

		// Initialise columns & references.
		if ($initColumns) {
			$this->initColumns();
		}

		// Sort columns according to the 'order_by'-field.
		if (!empty($this->cols)) 
		{
			ksort($this->cols);
		}

		$this->rights = $this->getRights($PHSes->groupId, $this->id);
	}

	/**
	 * Determine the rights that a group has on this table.
	 * view, insert, update, delete, copy, notify, import, export
	 *
	 * @author Arjan Haverkamp
	 * @param $groupId(int) The group-id to get the table-rights for.
	 * @param $tableId(int) The table-id to get the table-rights for.
	 * @return An array with table rights.
	 * @returns array
	 */
	function checkRights($user = null, $role = null) {
		global $db;
		if (null === $role) { return 'No role number has been definied!'; }
		if (null === $user) { return 'No user has been definied!'; }
		if (is_numeric($role)) {
			$sql = "
				SELECT phoundry_user_group.group_id, phoundry_user.id, phoundry_user.admin 
				FROM phoundry_user_group 
				LEFT JOIN phoundry_user 
					ON phoundry_user.id=phoundry_user_group.user_id 
				WHERE phoundry_user.username = '" . escDBquote($user) . "' 
				AND phoundry_user_group.group_id = '" . escDBquote($role) . "'";
		} else {
			$sql = "
				SELECT phoundry_user_group.group_id, phoundry_user.id, phoundry_user.admin
				FROM phoundry_user_group
				LEFT JOIN phoundry_user 
					ON phoundry_user.id=phoundry_user_group.user_id
				LEFT JOIN phoundry_group 
					ON phoundry_group.id=phoundry_user_group.group_id
				WHERE phoundry_user.username = '" . escDBquote($user) . "' 
				AND phoundry_group.name = '" . escDBquote($role) . "'";
		}
		$cur = $db->Query($sql);
		if ($cur && !$db->EndOfResult($cur)) {
			$PHSes->groupId = $db->FetchResult($cur,0,'group_id');
		}
		else {
			return 'User is not a member in the given role!';
		}
		return;
	}

	/**
	 * Determine the rights that a group has on this table.
	 * view, insert, update, delete, copy, notify, import, export
	 *
	 * @author Arjan Haverkamp
	 * @param $groupId(int) The group-id to get the table-rights for.
	 * @param $tableId(int) The table-id to get the table-rights for.
	 * @return An array with table rights.
	 * @returns array
	 */
	function getRights($groupId, $tableId) {
		global $db, $PHprefs;

		$groupId = (int)$groupId;
		$tableId = (int)$tableId;

		// A group has all rights per default, unless restricted.
		$rights = array('view'=>1, 'insert'=>1, 'update'=>1, 'delete'=>1, 'import'=>1, 'export'=>1);
		if ($PHprefs['useNotify'])
			$rights['notify'] = 1;
		if ($PHprefs['useCopy'])
			$rights['copy'] = 1;

		$sql = "SELECT rights FROM phoundry_group_table WHERE group_id = $groupId AND table_id = $tableId";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur)) {
			// Group has restricted rights.
			$rights = array();
			$DBrights = $db->FetchResult($cur,0,'rights');
			if (strpos($DBrights, 'p') !== false)
				$rights['view'] = 1;
			if (strpos($DBrights, 'i') !== false)
				$rights['insert'] = 1;
			if (strpos($DBrights, 'u') !== false)
				$rights['update'] = 1;
			if (strpos($DBrights, 'd') !== false)
				$rights['delete'] = 1;
			if (strpos($DBrights, 'm') !== false)
				$rights['import'] = 1;
			if (strpos($DBrights, 'x') !== false)
				$rights['export'] = 1;
			if (strpos($DBrights, 'n') !== false && $PHprefs['useNotify'])
				$rights['notify'] = 1;
			if (strpos($DBrights, 'c') !== false && $PHprefs['useCopy'])
				$rights['copy'] = 1;
		}
		$db->FreeResult($cur);
		return $rights;
	}

	/**
	 * Initialise all columns and cross-references in the current table.
	 *
	 * @author Arjan Haverkamp
	 * @returns void
	 * @private
	 */
	function initColumns() 
	{
		global $db, $PHSes, $PHprefs;

		$sql = "SELECT id, order_by, name, searchable FROM phoundry_column WHERE table_id = {$this->id} ORDER BY order_by";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$visibles = 0;
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			if (in_array((int)$db->FetchResult($cur,$x,'searchable'), array(1,3,5))) {
				$visibles++;
			}
			$this->cols[(int)$db->FetchResult($cur,$x,'order_by')] =
				new Column($db->FetchResult($cur,$x,'id'), $db->FetchResult($cur,$x,'name'), $this->name, $PHSes->groupId);
		}
		if ($visibles == 0) {
			PH::phoundryErrorPrinter('There are no visible fields for this table!', true);
		}
		$db->FreeResult($cur);
	}

	function initColumn($cid)
	{
		global $db, $PHSes, $PHprefs;

		$cid = (int)$cid;
		$sql = "SELECT id, order_by, name, searchable FROM phoundry_column WHERE id = {$cid}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur)) {
			$retVal = new Column($db->FetchResult($cur,0,'id'), $db->FetchResult($cur,0,'name'), $this->name, $PHSes->groupId);
		}
		else {
			$retVal = false;
		}
		$db->FreeResult($cur);
		return $retVal;
	}

	function convertKeyval($keyVal) 
	{
		if (is_array($keyVal)) 
		{
			if ($this->keyType == 'integer')
				return implode(',', $keyVal);
			else 
			{
				$keyVals = array();
				foreach($keyVal as $kv) 
				{
					$keyVals[] = escDBquote($kv);
				}
				return "'" . implode("','", $keyVals) . "'";
			}
		}
		if ($this->keyType == 'integer') 
		{
			return (int)$keyVal;
		}
		else {
			return "'" . escDBquote($keyVal) . "'";
		}
	}

	/**
	 * Generate a page-title for the page currently viewed.
	 * Each page has a title, sometimes there's a subtitle as well.
	 * This function returns an associative array:
	 * ['title'] contains the main page title.
	 * ['extra'] contains the sub page title (or an empty string)
	 *
	 * @author Arjan Haverkamp
	 * @param $h_title(string) Some extra text to append to the page title.
	 * @return An associative array with page-title info.
	 * @returns array
	 * @public
	 */
	function getPageHeader($page, $h_title = '') {
		global $db, $g_dateFormat, $PHprefs;

		$title = $this->description;
		if ($h_title != '') {
			$title .= ' - ' . $h_title;
		}

		$extra = '';
		foreach ($this->cols as $col) {
			if (strtolower(get_class($col->inputtype)) == 'itpassthru' && !empty($col->inputtype->extra['query'])) {
				$sql = PH::evaluate($col->inputtype->extra['query']);
				$cur = $db->Query($sql);
				// The query above might fail, f.e. when the edit-page is called
				// directly through the LinkChecker e-mail. Passthru vars are not
				// available then. In that case, do not print the extra header.
				if ($cur && $db->NumberOfRows($cur) == 1) {
					if ($extra != '') {
						$extra .= ' - ';
					}
					$nrf = $db->NumberOfColumns($cur);
					for ($i = 0; $i < $nrf; $i++) {
						$extra .= $db->FetchResult($cur,0,$i) . ' ';
					}
				}
			}
		}

		// User Filters:
		$script = ''; $htmls = array();

		foreach($this->filters as $filter) {
			$isSystemFilter = false;
			if ($filter['type'] != 'user') {
				continue;
			}
			$fName  = 'PHfltr_' . $filter['id'];
			$fValue = (isset($_REQUEST[$fName]) && $_REQUEST[$fName] !== '') ? $_REQUEST[$fName] : '';

			if ($page != 'records' && $fValue === '') {
				continue;
			}
			
			if ($filter['query'] == '{$DATE}') {
				if ($fValue !== '') {
					$fValue = str_replace(array('YYYY','MM','DD'), explode('-',$fValue), $g_dateFormat);
				}
				if ($page == 'records') {
                    $fHtml = '<input type="text" size="10" maxlength="10" value="' . $fValue . '" title="' . word(252) . '" onchange="applyFilter(' . $filter['id'] . ',this.value,true,'. $filter['string_id'] . ')" />';
				}
				else {
					$fHtml .= $fValue;
				}
				$htmls[] = '<td>' . $filter['name'] . ':<br />' . $fHtml . '</td>';
				continue;
			}
			
			$options = array();
			if (stripos($filter['query'], 'SELECT') !== false) {
				$sql = PH::evaluate($filter['query']);
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				$nrf = $db->NumberOfColumns($cur);
				for ($x = 0; !$db->EndOfResult($cur); $x++) {
					$text = '';
					for ($y = 1; $y < $nrf; $y++) {
               	$text .= PH::langWord($db->FetchResult($cur,$x,$y)) . ' ';
					}
					if ($db->ResultIsNull($cur,$x,0)) {
						$options['NULL'] = trim($text);
					}
					else {
						$options[$db->FetchResult($cur,$x,0)] = trim($text);
					}
				}
				$db->FreeResult($cur);
			}
			else {
				$_query = PH::evaluate($filter['query']);
				if (empty($_query)) {
					$isSystemFilter = true;
				}
				else {
					$pairs = explode('|', $_query);
					foreach($pairs as $pair) {
						$pair = explode('=', $pair, 2);
						$options[$pair[0]] = $pair[1];
					}
				}
			}

			$fHtml = '<td>' . $filter['name'] . ':<br />';
			if (empty($options)) {
				$fHtml .= '<b style="color:red">[' . word(35 /* none */) . ']</b>';
			}
			else {
				if ($page == 'records') {
					$fHtml .= '<select onchange="applyFilter(' . $filter['id'] . ',this.value)">';
					if ($filter['required'] == 0) {
						$fHtml .= '<option value="">[' . word(163 /* all */) . ']</option>';
					}
				}
				$x = 0;
				foreach($options as $value=>$text) {
					$sel = isset($_REQUEST[$fName]) && $_REQUEST[$fName] !== '' && $_REQUEST[$fName] == $value;

					if ($x == 0 && $filter['required'] > 0 && !isset($_REQUEST[$fName])) {
						// A required filter, where no selection was made: we need to
						// apply the first filter in the list. 
						$_REQUEST[$fName] = $value;
						$script = "\n<script type=\"text/javascript\">\n//<![CDATA[\n$(function(){_D.forms[0]['{$fName}'].value='" . escSquote($value) . "'})\n//]]>\n</script>\n";
					}

					if ($page == 'records') {
						if (function_exists('mb_strlen')) {
							$abbr = mb_strlen($text, $PHprefs['charset']) <= 60 ? $text : mb_substr($text, 0, 60, $PHprefs['charset']) . '...';
						}
						else {
							$abbr = strlen($text) <= 60 ? $text : substr($text, 0, 60) . '...';
						}
						$fHtml .= '<option value="' . $value . '"' . ($sel ? ' selected="selected"' : '') . '>' . PH::htmlspecialchars($abbr) . '</option>';
					}
					else {
						if ($sel) {
							$fHtml .= $text;
							break;
						}
					}
				}
				if ($page == 'records') {
					$fHtml .= '</select>';
				}
				$x++;
			}
			if (!$isSystemFilter) {
				$htmls[] = $fHtml . $script . '</td>';
			}
		} // End filters

		return array(
			'title'		=> $title,
			'extra'		=> $extra,
			'filters'	=> empty($htmls) ? '' : '<table class="filter-table"><tr>' . implode('',$htmls) . '</tr></table>'
		);
	}

	/**
	 * Check whether the 'max_records' limit for this table has been reached.
	 *
	 * @author Arjan Haverkamp
	 * @param $action(string) Either 'insert' or 'copy'.
	 * @param $page(string) Either 'records' or 'insert'.
	 * @return A boolean: true if more records may be inserted, false if not.
	 * @returns boolean
	 * @public
	 */
	function insertAllowed($action = 'insert', $page = 'records') {
		global $db, $PHprefs;

		if ($this->name == 'phoundry_group' && $action == 'insert' && $PHprefs['useCopy']) {
			// If current table is 'phoundry_group', then we do something extra:
			// If a group with name 'Superuser' exists, then insertion is not
			// allowed, as it is better practice to copy this group and thus retain
			// access-rights. Copying in this case is allowed, inserting is not.
			$sql = "SELECT id FROM phoundry_group WHERE name = 'Superuser'";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur)) {
				$db->FreeResult($cur);
				return false;
			}
		}

		if ($page == 'insert' && $action == 'insert') {
			if (isset($this->extra['ok_and_ins_again']) && $this->extra['ok_and_ins_again'] == false) {
				return false;
			}
		}

		// -1 means: unlimited.
		if ($this->maxRecords == -1) {
			return true;
		}

		// Has the record-limit been exceeded?
		$ands = array();
		foreach($this->filters as $filter) {
			if ($filter['type'] == 'system') {
				$ands = array_merge($ands, PH::getFilter(unserialize($filter['wherepart'])));
			}
		}

		static $counts = array(); // Cache
		$sql = "SELECT count(*) FROM {$this->name}";
		if (!empty($ands)) {
			$sql .= ' WHERE ' . implode(' AND ', $ands);
		}
		if (isset($counts[$sql])) {
			// Use cached results if available.
			$nr = $counts[$sql];
		}
		else {
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$nr = (int)$db->FetchResult($cur,0,0);
			$db->FreeResult($cur);
			$counts[$sql] = $nr;
		}

		$max = (int)$this->maxRecords;
		if ($page == 'insert') { $max -= 1; }

		return $nr < $max;
	}

	/**
	 * Determine the records the current user has notifications on.
	 *
	 * @author Arjan Haverkamp
	 * @param $TID(int) The table-id to determine the rights for. If empty, returns the rights for the current table.
	 * @return An array with notificications.
	 * @returns array
	 * @private
	 */
	function determineNotifications($TID = NULL) {
		global $db, $PHSes, $PHenv;

		if ($TID === NULL) $TID = $this->id;
		$notifications = array();

		$TID = (int)$TID;

		$sql = "SELECT keyval FROM phoundry_notify WHERE table_id = $TID AND user_id = {$PHSes->userId}";
		if ($this->DMDcampaignRelated) {
			$sql .= " AND campaign_id = {$PHenv['DMDcid']}";
		}
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$notifications[] = $db->FetchResult($cur,$x,'keyval');
		}
		$db->FreeResult($cur);
		return $notifications;
	}

	/**
	 * Lock a record in the current table in the database.
	 *
	 * @author Arjan Haverkamp
	 * @param $keyVal(string) The value of the primary key field for the record to lock.
	 * @returns void
	 * @private
	 */
	function lockRecord($keyVal) 
	{
		global $db, $PHSes;
		$sql = "INSERT INTO phoundry_lock VALUES ({$this->id}, '" . escDBquote($keyVal) . "', $PHSes->userId, '" . MetabaseNow() . "')";
		$res = @$db->Query($sql);	// Ignore SQL errors: the lock might already be there!
//			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}

	/**
	 * Unlock a record in the current table in the database.
	 *
	 * @author Arjan Haverkamp
	 * @param $keyVal(string) The value of the primary key field for the record to unlock.
	 * @param $allUsers(bool) If true, all locks on this record are removed. If false, only the records locked by the current user are removed.
	 * @returns void
	 * @private
	 */
	function unlockRecord($keyVal, $allUsers = false)
	{
		global $db, $PHSes;
		$sql = "DELETE FROM phoundry_lock WHERE table_id = {$this->id} AND keyval = " . $this->convertKeyval($keyVal);
		if (!$allUsers) $sql .= ' AND user_id = ' . $PHSes->userId;
		$res = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}

	/**
	 * Send notifications (by e-mail) to all subscribers on records in the current table.
	 * 
	 * @author Arjan Haverkamp
	 * @param $keyVal(mixed) The value for the primary key of the record to send notifications for (string or int).
	 * @param $action(string) The action just performed ('insert', 'update' or 'delete').
	 * @param $diffs(array) The names of the fields that were actually changed (only needed when $action == 'update').
	 * @returns void
	 * @private
	 */
	function notifyMessage($keyVal, $action, $diffs = array()) 
	{
		global $PHprefs, $db, $PHSes, $PRODUCT, $PHenv;

		if ($action == 'update' && null !== $diffs && empty($diffs)) {
			// No changes: no notifications.
			return;
		}

		// Determine users that need to get a nofication:
		$sql = "SELECT u.id, u.name, u.e_mail FROM phoundry_notify n, phoundry_user u WHERE n.table_id = {$this->id} AND (n.keyval = " . $this->convertKeyval($keyVal) . " OR n.keyval = 'any') AND n.user_id = u.id";
		if ($this->DMDcampaignRelated) {
			$sql .= " AND n.campaign_id = {$PHenv['DMDcid']}";
		}
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		$nrRecipients = $db->NumberOfRows($cur);
		if ($nrRecipients == 0) {
			// No recipients: no notifications.
			return;
		}

		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$uurl = $protocol . $_SERVER['HTTP_HOST'] . $PHprefs['url'] . '/core/update.php?' . QS(0,'RID=' . $keyVal);

		if     ($action == 'delete') $what = word(4, $PHSes->userName);
		elseif ($action == 'update') $what = word(3, $PHSes->userName, $uurl);
		elseif ($action == 'insert' || $action == 'copy') $what = word(5, $PHSes->userName, $uurl);
		$what .= '<br /><small>Client: ' . (isset($_SERVER['REMOTE_HOST']) ? $_SERVER['REMOTE_HOST'] : '') . ' (' . PH::getRemoteAddr() . ')</small>';

		$plainTxt = strip_tags(preg_replace('/<br( \/)?>/i', "\n", $what));

		for ($x = 0; $x < $nrRecipients; $x++) {
			$recipient = $db->FetchResult($cur,$x,'e_mail');
			$body = $this->viewPage($keyVal, $db->FetchResult($cur,$x,'id'), $PHSes->userId == 1, true, $diffs);
			if ($body === false) {
				continue;
			}

			// Outlook 2007 cannot display fieldsets/legends...
			// We transform them to <span class="legend">[legend]</span><div class="fieldset">[fieldset]</div>
			$mailHTML = '';

			// DOM parsers don't like ISO, let's convert to utf-8:
			if ($PHprefs['charset'] != 'utf-8') {
				$body['html'] = PH::iconv($PHprefs['charset'], 'utf-8', $body['html']);
			}

			$dom = new DOMDocument('1.0', 'utf-8');
			@$dom->loadHTML('<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8" /></head><body>' . $body['html'] . '</body></html>');
			$xpath = new DOMXPath($dom);
			foreach($xpath->query('//fieldset[@class=\'field\']') as $fieldset) {
				$legend = $xpath->query('legend[1]', $fieldset)->item(0);
				$mailHTML .= '<span class="legend">' . $legend->textContent . '</span><div class="fieldset">' . preg_replace('/^' . preg_quote(trim(PH::DOMouterHTML($legend, 'utf-8')), '/') . '/', '', trim(PH::DOMinnerHTML($fieldset, 'utf-8'))) . "</div>\n";
			}

			PH::sendHTMLemail($recipient, $PRODUCT['name'] . ' ' . word(12 /* Notification */) . ' ' . $this->description, $what . '<hr />' . $mailHTML, array(), null, 'utf-8');
		}
		$db->FreeResult($cur);
	}

	function search($keyword, $searchFields, $prefix = '') {
		global $db, $PHprefs, $g_dateFormat;
		
		$wheres = array();
		$where  = '';
		$keyword = escDBquote($keyword);
		$datep = str_replace(array('DD','MM','YYYY'), array('[0-9]{2}','[0-9]{2}','[0-9]{4}'), $g_dateFormat);

		foreach ($this->cols as $col) {
			$i = array_search($col->name, $searchFields);
			if (false !== $i && method_exists($col->datatype, "customSearch")) {
				array_splice($searchFields, $i, 1);
				$custom_where = $col->datatype->customSearch($keyword,
					$prefix, $this->id, $this->key);
				if (false !== $custom_where) {
					$wheres[] = $custom_where;
				}
			}
		}
		
		if (preg_match("/^date\((\*|$datep)(,(\*|$datep))*\)$/", $keyword, $regs)) {
			if (isset($regs[3]) && $regs[3] != '*') {
				$td = substr($regs[3], strpos($g_dateFormat, 'DD'), 2);
				$tm = substr($regs[3], strpos($g_dateFormat, 'MM'), 2);
				$ty = substr($regs[3], strpos($g_dateFormat, 'YYYY'), 4);
			}
			if ($regs[1] != '*') {
				$fd = substr($regs[1], strpos($g_dateFormat, 'DD'), 2);
				$fm = substr($regs[1], strpos($g_dateFormat, 'MM'), 2);
				$fy = substr($regs[1], strpos($g_dateFormat, 'YYYY'), 4);
			}
			if (!isset($regs[3]))
				$where .= '{$SEARCHFIELD} = ' . $db->GetDateFieldValue("$fy-$fm-$fd");
			elseif ($regs[1] != '*' && $regs[3] != '*')
				$where .= '{$SEARCHFIELD} >= ' . $db->GetDateFieldValue("$fy-$fm-$fd") . ' AND {$SEARCHFIELD} <= ' . $db->GetDateFieldValue("$ty-$tm-$td");
			elseif ($regs[1] == '*' && $regs[3] != '*')
				$where .= '{$SEARCHFIELD} <= ' . $db->GetDateFieldValue("$ty-$tm-$td");
			elseif ($regs[1] != '*' && $regs[3] == '*')
				$where .= '{$SEARCHFIELD} >= ' . $db->GetDateFieldValue("$fy-$fm-$fd");

			// Only apply this WHERE-part on columns with datatype date/datetime:
			$tmp = $searchFields; $searchFields = array();
			foreach($this->cols as $col) {
				if (!isset($col->DBprops['type'])) continue;
				$dt = $col->DBprops['type'];
				if (in_array($col->name, $tmp) && ($dt == 'date' || $dt == 'datetime'))
					$searchFields[] = $col->name;
			}
		}
		else {
			if (preg_match('/[\*\?]/', $keyword))
				$keyword = str_replace(array('*', '?'), array('%', '_'), $keyword);
			/* TOO SLOW!!!
			else
				$keyword = '%' . $keyword . '%';
			*/
			if ($PHprefs['DBtype'] == 'oci' || $PHprefs['DBtype'] == 'pgsql')
				$where = 'LOWER({$SEARCHFIELD}) LIKE \'' . strtolower($keyword) . '\'';
			else
				$where = '{$SEARCHFIELD} LIKE \'' . $keyword . '\'';
		}
		foreach($searchFields as $searchField)
            $wheres[] = strtr($where, array('{$SEARCHFIELD}' => $prefix . $searchField, 'SEARCHFIELD' => $prefix . $searchField));
		return $wheres;
	}

	/**
	 * Return info (help-text) for the current table.
	 * This info will be contained within a fieldset. Returns empty string
	 * if no info is available.
	 *
	 * @author Arjan Haverkamp
	 * @return A string representing this table's info, wrapped in a fieldset.
	 * @returns string
	 */
	function getInfo() {
		if ($this->info === '') { return ''; }
		return '<div id="pageInfo" class="miniinfo" style="margin-bottom:8px">' . $this->info . '</div>';
	}

	/**
	 * Translate the parameters in $ENV (usually _GET or _POST) into a PHP datastructure.
	 * 
	 * @author Arjan Haverkamp
	 * @param $ENV(array) An array that contains variables that determine certain recordset
	 *                    features.
	 * @return An array with the following structure:
	 *         'page' => The current page-number, -1 for all records
	 *         'srch' => The keyword that was searched for (or empty)
	 *         'ordr' => The record order (fieldname ASC|DESC)
	 *         'show' => The fields to display to the user
	 *         's_in' => The fields to search in for 'srch'.
	 *         'filters' => The filters applied.
	 * @returns array
	 */
	public function get2struct($ENV)
	{
		global $db;

		$show = $s_in = $filters = array();
		$showENV = empty($ENV['PHshow']) ? array() : explode(',', $ENV['PHshow']);
		$s_inENV = empty($ENV['PHs_in']) ? array() : explode(',', $ENV['PHs_in']);
		foreach ($this->cols as $col) 
		{
			if (strtolower(get_class($col->datatype)) == 'dtfake' && 
			    !empty($col->datatype->html_code))
			{
				$show[] = $col->name;
			}

			if (isset($col->rights['view']) && strtolower(get_class($col->datatype)) != 'dtfake') 
			{
				if (!empty($s_inENV)) 
				{
					if (in_array($col->name, $s_inENV)) 
					{
						$s_in[] = $col->name;
					}
				} 
				else 
				{
					if (in_array($col->searchable, array(0,1)) && !$col->datatype->isXref)
					{
						$s_in[] = $col->name;
					}
				}

				if (((count($showENV) == 0 &&
						in_array($col->searchable, array(1,3,5))) ||
						in_array($col->name, $showENV))) 
				{
					$show[] = $col->name;
				}
			}
		}

		// User Filters:
		foreach ($this->filters as $filter) 
		{
			if ($filter['type'] == 'user') 
			{
				$fName = 'PHfltr_' . $filter['id'];
				$filters[$filter['id']] = (isset($ENV[$fName]) && $ENV[$fName] !== '') ? $ENV[$fName] : '';
			}
		}

		$struct = array();
		$struct['page'] = !empty($ENV['PHpage']) ? $ENV['PHpage'] : 0;
		$struct['srch'] = !empty($ENV['PHsrch']) ? $ENV['PHsrch'] : '';

		$struct['ordr'] = (null === $this->recordOrder) ? null : array($this->recordOrder);
		if (!empty($ENV['PHordr']) && preg_match('/\w+\s+(asc|desc)$/i', $ENV['PHordr']))
		{
			$struct['ordr'] = array($ENV['PHordr']);
		}

		$struct['show'] = $show;
		$struct['s_in'] = $s_in;
		$struct['filters'] = $filters;

		return $struct;
	}

	/**
	 * Create two SQL query's: 1 for determining the total number of records in a recordset,
	 * and one for displaying the recordset.
	 *
	 * @author Arjan Haverkamp
	 * @param $struct(array) An array generated by the get2struct function.
	 * @param $ands(array) An array of conditions to use in the WHERE part.
	 * @return An array containing two strings:
	 *         [0] = Query for generating the recordset
	 *         [1] = Query for determining number of rows in recordset
	 * @returns array
	 */
	function makeQuery($struct, $_ands = array()) 
	{
		global $db, $PHSes;
		$selectFields = $_ors = array();

		// Filters:
		foreach($this->filters as $filter) {
			if ($filter['type'] == 'system') {
				$_ands = array_merge($_ands, PH::getFilter(unserialize($filter['wherepart'])));
			}
			elseif ($filter['type'] == 'user' && !empty($struct['filters'])) 
			{
				$fValue = $struct['filters'][$filter['id']];
				if ($fValue === '' && $filter['required'] > 0) {
					//if (preg_match('/^SELECT/i', $filter['query'])) {
					if (stripos($filter['query'], 'SELECT') !== false) {
						$_sql = PH::evaluate($filter['query']);
						$cur = $db->Query($_sql)
							or trigger_error("Query $_sql failed: " . $db->Error(), E_USER_ERROR);
						$fValue = ($db->EndOfResult($cur,0,0)) ? '9653922000715115233' : $db->FetchResult($cur,0,0); // The 9653922000715115233 is there to ensure we do *not* match a DB record!
						$db->FreeResult($cur);
					}
					else {
						$options = explode('|', PH::evaluate($filter['query']));
						$fValue = explode('=', $options[0], 2);
						$fValue = $fValue[0];
					}
				}
				if ($fValue !== '') {
                    $_ands[] = strtr(PH::evaluate($filter['wherepart']), array('{$FILTER_VALUE}' => $fValue, 'FILTER_VALUE' => $fValue));
				}
				/* New: 2008-09-15 */
				elseif (null === $filter['query']) {
					$_ands[] = PH::evaluate($filter['wherepart']);
				}
				/* /New: 2008-09-15 */
			}
		}

		// If there's an entry for $PHSes->limitRIDs[$TID], then use the values
		// in there to limit the resultset.
		if (isset($PHSes->limitRIDs[$this->id]) && !empty($PHSes->limitRIDs[$this->id])) {
			if ($this->keyType == 'integer') {
				$_set = $PHSes->limitRIDs[$this->id];
			}
			else {
				$_set = array();
				foreach($PHSes->limitRIDs[$this->id] as $_key) {
					$_set[] = "'" . escDBquote($_key) . "'";
				}
			}
			$_ands[] = "{$this->key} IN (" . implode(',',$_set) . ')';
		}

		if (!empty($struct['where'])) {
			$_ands = array_merge($_ands, $struct['where']);
		}
		else {
			// Was a search-term used?
			if ($struct['srch'] !== '' && count($struct['s_in']))
				$_ors = array_merge($_ors, $this->search($struct['srch'], $struct['s_in']));
		}

		// If $struct['show'][0] contains a '*', translate it to 'all fields':
		if (!empty($struct['show'][0]) && $struct['show'][0] == '*') {
			$struct['show'] = array();
			foreach($this->cols as $col) {
				if (isset($col->rights['view']))
					$struct['show'][] = $col->name;
			}
			if (!in_array($this->key, $struct['show']))
				array_unshift($struct['show'], $this->key);
		}

		// Determine fields for the SELECT-part
		foreach($this->cols as $col) {
			if (in_array($col->name, $struct['show'])) {
				if (!is_null($sf = $col->getSQLpart('view')))
					$selectFields[] = $sf;
			}
		}
		if (!in_array($this->key, $selectFields))
			array_unshift($selectFields, $this->key);

		// Create query:
		$_sql = "SELECT #fields# FROM {$this->name}";
		if (count($_ors) > 0 || count($_ands) > 0)
			$_sql .= ' WHERE ';
		$_sql .= implode(' AND ', $_ands);
		if (count($_ors) > 0) {
			if (count($_ands) > 0)
				$_sql .= ' AND ';
			$_sql .= '(' . implode(' OR ', $_ors) . ')';
		}

		$_countSQL  = strtr($_sql, array('#fields#' => 'count(*)', 'fields' => 'count(*)'));
		$_recordSQL = strtr($_sql, array('#fields#' => implode(',', $selectFields), 'fields' => implode(',', $selectFields)));
		if (null !== $struct['ordr']) 
		{
			for($_x = 0; $_x < count($struct['ordr']); $_x++) 
			{
				$_recordSQL .= ($_x == 0) ? ' ORDER BY ' : ', ';
				$_recordSQL .= escDBquote($struct['ordr'][$_x]);
			}
		}

		return array(
			'recordSQL' => $_recordSQL,
			'countSQL'  => $_countSQL,
			'ands'      => $_ands,
			'ors'       => $_ors
		);
	}

	/**
	 * Generate HTML for the records-page.
	 *
	 * @author Arjan Haverkamp
	 *
	 * @return string A (pretty long) HTML-string that contains the search-page content.
	 */
	function recordsPage($struct, $sql, &$nrPages) {
		global $db, $PHSes, $PHprefs;

		if (isset($this->rights['notify'])) {
			$notifications = $this->determineNotifications();
		}

		$html = array();
		$html[] = $this->getInfo();

		// Filters:
		foreach($struct['filters'] as $id=>$value) {
			$html[] = '<input type="hidden" name="PHfltr_'.$id.'" value="'.htmlspecialchars($value).'" />';
		}

		// Record order:
		if ($struct['ordr'] !== null) {
			$html[] = '<input type="hidden" name="PHordr" value="'.htmlspecialchars($struct['ordr'][0]).'" />';
		}
		
		// What fields will we show?
		$html[] = '<input type="hidden" name="PHshow" value="'.implode(',', $struct['show']).'" />';

		// What fields will we search?
		$html[] = '<input type="hidden" name="PHs_in" value="'.implode(',', $struct['s_in']).'" />';

		// Was a search-term used?
		$html[] = '<input type="hidden" name="PHsrch" value="'.htmlspecialchars($struct['srch']).'" />';

		// Draw HTML table:
		$html[] = '<table id="recordTBL" class="sort-table" cellspacing="0" data-tid="'.$this->id.'">';
		$html[] = '<tbody>';
		$html[] = '<tr>';
		if (isset($this->rights['delete'])) {
			$html[] = '   <th><input type="checkbox" onclick="setAllTrash(this.checked)" /></th>';
		}
		if (isset($this->rights['notify'])) {
			$notifyImg = (in_array('any', $notifications)) ? 'email-all-active-icon' : 'email-all-icon';
			$html[] = '   <th><span class="icon ptr '.$notifyImg.'" title="' . word(12) .'"></span></th>' . "\n";
		}
		if (!empty($this->extra['versioning'])) {
			$html[] = '   <th>Rev</th>';
		}

		// Draw column headers:
		foreach($this->cols as $col) 
		{
			if (!in_array($col->name, $struct['show'])) {
				continue;
			}

			$sort = $col->name;
			if ($struct['ordr'] !== null && preg_match("/^{$col->name} (asc|desc)/", $struct['ordr'][0], $reg)) {
				$sort .= ' ' . ($reg[1] == 'asc' ? 'desc' : 'asc');
			} else {
				$sort .= ' asc';
			}

			if ($col->datatype->isXref || strtolower(get_class($col->datatype)) == 'dtfake') {
				$html[] = '   <th nowrap="nowrap">';
			}
			else {
				$html[] = '   <th id="' . $col->name . 'Column" class="sort' . (isset($reg[1]) ? ' sortHeader' . ucfirst($reg[1]) : '') . '" nowrap="nowrap" onclick="var F=_D.forms[0];F[\'PHpage\'].value=F[\'PHpage\'].value==-1?-1:0;F[\'PHordr\'].value=\'' . $sort . '\';F.submit()" title="' . word(213, $col->description, word(strpos($sort, 'desc') === false ? 214 : 215)) . '">';
			}
			$html[] = PH::htmlspecialchars($col->description) . "</th>";
		}
		$html[] = "</tr>";

		// Determine total number of rows:
		$cur = $db->Query($sql['countSQL'])
			or trigger_error("Query {$sql['countSQL']} failed: " . $db->Error(), E_USER_ERROR);
		$nrRows = $db->FetchResult($cur,0,0);
		$db->FreeResult($cur);

		// Limit query:
		$first = 0;
		if ($struct['page'] != -1) 
		{
			$first = $struct['page'] * $PHprefs['recordsPerPage'];
			$res = $db->SetSelectedRowRange($first, $PHprefs['recordsPerPage'])
				or trigger_error('Cannot set row range: ' . $db->Error(), E_USER_ERROR);
		}

		$hilite = empty($_GET['_hilite']) ? -1 : $_GET['_hilite'];
		$row = 0; $recordIDs = array();

		// Execute query:
		$cur = $db->Query($sql['recordSQL'])
			or trigger_error("Query {$sql['recordSQL']} failed: " . $db->Error(), E_USER_ERROR);

		for ($len = $db->NumberOfRows($cur), $row = 0; $row < $len; $row++) {
			$recordIDs[] = @$db->FetchResult($cur,$row,0);
		}
			
		if (!empty($this->extra['versioning'])) {
			$statuses = $this->getVersioningService()->
				getStatusForRecords($recordIDs, $this->id);
		}
		else {
			$statuses = array();
		}
		
		for ($len = $db->NumberOfRows($cur), $row = 0; $row < $len; $row++) {
			$recordID = @$db->FetchResult($cur,$row,0);

			$style = ($hilite == $recordID) ? ' style="font-weight:bold" ' : ' ';
			
			$status = isset($statuses[$recordID]) ? ' status-'.$statuses[$recordID]['status'] : '';
			$html[] = '<tr id="row'.$row.'_'.$recordID.'" class="'.($row & 1 ? 'even' : 'odd').$status.'"'.$style.'onclick="setRow(this)" ondblclick="editRow(this)" data-rid="'.$recordID.'">';
			if (isset($this->rights['delete'])) {
				$html[] = '   <td><input type="checkbox" name="_id[]" value="' . $recordID . '" onclick="setDel(event,this)" /></td>';
			}
			
			if (isset($this->rights['notify'])) {
				$html[] = '   <td><span class="icon ptr email-'.(in_array($recordID, $notifications) ? 'active' : 'inactive').'-icon" title="' . word(12) . '"></span></td>' . "\n";
			}
			
			if (!empty($this->extra['versioning'])) {
				if (isset($statuses[$recordID])) {
					$html[] = '   <td title="'.htmlspecialchars($statuses[$recordID]['status']).'">'.$statuses[$recordID]['revision'].'</td>';
				}
				else {
					$html[] = '   <td></td>';
				}
			}
			foreach($this->cols as $col) {
				if (!in_array($col->name, $struct['show'])) {
					continue;
				}
				if (strtolower(get_class($col->datatype)) != 'dtfake' && !$col->datatype->isXref) {
					$GLOBALS['RECORD_ID'] = null === $db->FetchResult($cur,$row,$col->name) ? 'NULL' : $db->FetchResult($cur,$row,$col->name);
				}
				$html[] = '<td' . (strtolower(get_class($col->inputtype)) == 'itradio' ? ' align="center"' : '') . '>'.
					$col->getValue('search',$recordID,$cur,$row,$PHprefs['recordStrlen']).
					"</td>";
			}
			$html[] = "</tr>";
		}
		$db->FreeResult($cur);
		$html[] = '</tbody>';
		$html[] = "</table>";
		$html[] = '<span id="_RIDs" style="display:none">' . implode('|', $recordIDs) . '</span>';

		$last = $first + $row;

		// Javascript for setting result-index in footer:
		$nrPages = ceil($nrRows/$PHprefs['recordsPerPage']);
		$msg = ($nrRows == 0) ? word(25) : word(26) . ' ' . ($first+1) . " - $last " . word(27) . " $nrRows";
		$html[] = "<script type=\"text/javascript\">\n//<![CDATA[";
		$html[] = "$(function(){setResults({$nrRows},{$nrPages},{$struct['page']},'" . escSquote($msg) . "')});";
		$html[] = "//]]>\n</script>";

		$PHSes->shownRIDs[$this->id] = $recordIDs;

		return implode("\n", $html);
	}

	function recordsXML($struct, $sql) {
		global $db, $PHprefs;

		print '<?xml version="1.0" encoding="' . $PHprefs['charset'] . '"?>' . "\n";
		print "<recordset>\n";

		// Execute query:
		$cur = $db->Query($sql['recordSQL'])
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> {$sql['recordSQL']}");
		$row = 0;
		while(!$db->EndOfResult($cur)) {
			print "<record>\n";
			$recordID = @$db->FetchResult($cur,$row,0);

			foreach($this->cols as $col) {
				if (!in_array($col->name, $struct['show']))
					continue;
				print "<{$col->name}>" . $col->getValue('xml',$recordID,$cur,$row) . "</{$col->name}>\n";
			}
			print "</record>\n";
			$row++;
		}
		$db->FreeResult($cur);
		print "</recordset>\n";
	}

	/**
	 * Generate HTML for the insert-page.
	 *
	 * @author Arjan Haverkamp
	 * @return A (pretty long) HTML-string that contains the insert-page content.
	 * @returns string
	 * @public
	 */
	function insertPage() 
	{
		return $this->editPage('insert');
	}

	/**
	 * Generate HTML for the update-page.
	 *
	 * @author Arjan Haverkamp
	 * @param $keyVal(mixed) The value for the primary key of the record to update.
	 * @return A (pretty long) HTML-string that contains the update-page content.
	 * @returns string
	 * @public
	 */
	function updatePage($keyVal)
	{
		return $this->editPage('update', $keyVal);
	}

	/**
	 * Generate HTML for the copy-record-page.
	 *
	 * @author Arjan Haverkamp
	 * @param $keyVal(mixed) The value for the primary key of the record to copy.
	 * @return A (pretty long) HTML-string that contains the copy-page content.
	 * @returns string
	 * @public
	 */
	function copyPage($keyVal)
	{
		return $this->editPage('copy', $keyVal);
	}

	/**
	 * Get javascript functions for sytactically checking input.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page ('insert', 'update' or 'copy') to generate HTML for.
	 * @return A string of Javascript syntax-check functions.
	 * @returns string
	 */
	function getJSchecks($page) {
		$checks = array();
		foreach($this->cols as $col) {
			if ($col->name != $this->key && isset($col->rights[$page])) {
				// Only one JS function for every datatype (prevent duplicate functions):
				$checks[strtolower(get_class($col->datatype))] = $col->checkSyntaxJS($page);
			}
		}
		return implode("\n", $checks);
	}


	/**
	 * Retrieve Javascript code.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Currently, only 'search' is known.
	 * @returns A string of Javascript code
	 * @returns string
	 */
	function getJScode($page) {
		$JScode = array();
		foreach($this->cols as $col) {
			$code = $col->getJScode($page);
			if (trim($code) != '') {
				$JScode[] = $col->getJScode($page);
			}
		}
		return PH::evaluate(implode("\n", $JScode));
	}

	/**
	 * Generate HTML for an edit-page.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page ('insert', 'update' or 'copy') to generate HTML for.
	 * @param $keyVal(mixed) The value for the primary key of the record to update. Omit when inserting!
	 * @return A (pretty long) HTML-string that contains the edit-page content.
	 * @rerurns string
	 * @private
	 */
	function editPage($page, $keyVal = NULL)
	{
		global $db, $PHprefs;

		$html = $this->getInfo();

		if ($page == 'update' && $PHprefs['useLocks']) 
		{
			if ($this->keyType == 'integer')
				$keyVal = (int)$keyVal;
			$this->unlockRecord($keyVal, true);
			$this->lockRecord($keyVal);
		}
		elseif ($page == 'insert') 
		{
			$keyVal = -1;
		}

		$pageProps = array('jsFiles'=>array(), 'jsCode'=>array(), 'cssFiles'=>array());

		if ($page == 'update' || $page == 'copy') 
		{
			$selectFields = array();
			foreach($this->cols as $col) 
			{
				if (!is_null($sf = $col->getSQLpart($page))) 
				{
					$selectFields[] = $sf;
				}
			}
			if (!in_array($this->key, $selectFields)) 
			{
				array_unshift($selectFields, $this->key);
			}

			$sql = 'SELECT ' . implode(',', $selectFields) . " FROM {$this->name} WHERE {$this->key} = " . $this->convertKeyval($keyVal);
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if ($db->NumberOfRows($cur) == 0) 
			{
				$pageProps['html'] = '<b>' . word(131) /* This record has been deleted */ . '</b>';
				return $pageProps;
			}
			$db->FetchResultAssoc($cur,$row,0);
		}
		
		$cols_html = array('<div class="fieldsets-wrapper-fields">');
		foreach($this->cols as $col) 
		{
			if (isset($cur) && !isset($col->rights[$page])) 
			{
				if ($page == 'update' && isset($col->rights['view']));
				else continue;
			}

			$filtered = false;
			$dbval = isset($cur) ? $col->getValue($page,$keyVal,$cur,0) : '';
			foreach($this->filters as $filter) 
			{
				if ($filter['type'] != 'user') 
				{
					continue;
				}
				$fName = 'PHfltr_' . $filter['id'];
				$fValue = !empty($_REQUEST[$fName]) ? $_REQUEST[$fName] : '';
				if ($filter['matchfield'] == $col->name && $fValue !== '') 
				{
					if ($filter['required'] == 1) 
					{
						$filtered = true;
					}
					if ($page == 'insert' && $filter['required'] != 2) 
					{
						$dbval = $fValue;
					}
					break;
				}
			}
			$cols_html[] = $col->getHtml($page, $dbval, $filtered, $pageProps);
		}
		$cols_html[] = '</div>';
		
		if (isset($cur)) { $db->FreeResult($cur); }
		$html .= implode("\n", $cols_html);
		$pageProps['html'] = $html;
		return $pageProps;
	}

	/**
	 * Generate HTML for the view-record page.
	 * (Also used for Notification e-mails)
	 *
	 * @author Arjan Haverkamp
	 * @param $keyVal(mixed) The value for the primary key of the record to view.
	 * @param $userID(int) The id of the user to show this page for.
	 *                     If omitted, will use the user-id of the current user.
	 * @param $isAdminUser(bool) Whether or not $userId is an admin user,
	 *                           who may see all columns.
	 * @param $absUrls(bool) Whether or not URL's in the view-page should be
	 *                       made absolute.
	 * @return A (pretty long) HTML-string that contains the view-page content.
	 * @returns string
	 */
	function viewPage($keyVal = null, $userID = null, $isAdminUser = false, $absUrls = false, $diffs = null)
	{
		global $db, $PHSes;

		$html = '';
		$regexp = '((?:src|background)\s*=)([\s"\']*)(?!#|\/\/|http|"|\')(?:\/*)(.*?)([\s>"\'])';
		$selectFields = $showFields = array();

		if (null !== $userID && $isAdminUser) {
			foreach($this->cols as $col) {
				$showFields[] = $col->name;
			}
		}
		elseif (null === $userID) {
			foreach($this->cols as $col) {
				if (isset($col->rights['view'])) {
					$showFields[] = $col->name;
				}
			}
		}
		else {
			// Determine columns with 'view' rights for user $userID:
			$sql = "SELECT pc.name, pgc.rights 
			FROM 
				phoundry_user pu
			INNER JOIN 
				phoundry_group_table pgt
			INNER JOIN 
				phoundry_table pt
				ON pt.id = pgt.table_id
			INNER JOIN 
				phoundry_user_group pug
			INNER JOIN 
				phoundry_group pg
				ON pg.id = pug.group_id
			INNER JOIN
				phoundry_column pc 
				ON pc.table_id = pgt.table_id
			LEFT JOIN phoundry_group_column pgc 
				ON pgc.column_id = pc.id 
				AND pgc.group_id = pg.id 
			WHERE pu.id = pug.user_id 
			AND pgt.group_id = pg.id 
			AND pc.table_id = pgt.table_id 
			AND (pgc.rights LIKE '%v%' OR pgc.rights IS NULL) 
			AND pu.id ={$userID} 
			AND pt.id = {$this->id}";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++) {
				$showFields[] = $db->FetchResult($cur,$x,'name');
			}
			$db->FreeResult($cur);
		}

		//
		// If a field has changed ($diffs), but the current user has no
		// 'view' rights for that field, then we do not need to send a
		// notification e-mail.
		//
		if (null !== $diffs && count($diffs) > 0) {
			if (count(array_intersect(array_keys($diffs), $showFields)) == 0) {
				// Do not send confirmation e-mail...
				return false;
			}
		}

		foreach($this->cols as $col) {
			if (!is_null($sf = $col->getSQLpart('view', is_null($userID)))) {
				$selectFields[] = $sf;
			}
		}
		if (!in_array($this->key, $selectFields)) {
			array_unshift($selectFields, $this->key);
		}

		$pageProps = array('jsFiles'=>array(), 'jsCode'=>array(), 'cssFiles'=>array());

		$sql = 'SELECT ' . implode(',', $selectFields) . " FROM {$this->name} WHERE {$this->key} = " . $this->convertKeyval($keyVal);
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->NumberOfRows($cur) == 0) {
			$html .= '<b>' . word(131) /* This record has been deleted! */ . '</b>';
			$pageProps['html'] = $html;
			return $pageProps;
		}

		$html .= '<div class="fieldsets-wrapper-fields">';
		foreach($this->cols as $col) {
			if (!in_array($col->name, $showFields)) {
				continue;
			}
			if (strtolower(get_class($col->datatype)) != 'dtfake' && !$col->datatype->isXref) {
				$GLOBALS['RECORD_ID'] = null === $db->FetchResult($cur,0,$col->name) ? 'NULL' : $db->FetchResult($cur,0,$col->name);
			}
			$dbval = $col->getValue('view',$keyVal,$cur,0);
			// Translate paths in WYSIWYG/Radio fields to absolute URL's, in order
			// to make them work in e-mail messages as well.
			$cls = strtolower(get_class($col->inputtype));
			if ($absUrls && ($cls == 'itradio' || $cls == 'itsoap_api' || $cls == 'ittiny_mce')) {
				$dbval = preg_replace("/$regexp/i", "$1$2http://{$_SERVER['HTTP_HOST']}/$3$4", $dbval);
			}

			if (isset($diffs[$col->name])) {
				//$val = '<fieldset class="diff"><legend>' . word(243 /* Changes */) . '</legend><div style="background:#ff7">' . $diffs[$col->name]['diff'] . '</div></fieldset><fieldset class="diff"><legend>' . word(244 /* Old */) . '</legend><div style="background:#faa">' . $diffs[$col->name]['old'] . '</div></fieldset><fieldset class="diff"><legend>' . word(71 /* New */) . '</legend><div style="background:#afa">' . $dbval . '</div></fieldset>';
				$val = '<div class="diffs"><span class="legend">' . word(243 /* Changes */) . '</span><div class="fieldset" style="background:#ff7">' . $diffs[$col->name]['diff'] . '</div><span class="legend">' . word(244 /* Old */) . '</span><div class="fieldset" style="background:#faa">' . $diffs[$col->name]['old'] . '</div><span class="legend">' . word(71 /* New */) . '</span><div class="fieldset" style="background:#afa">' . $dbval . '</div></div>';
			}
			else {
				$val = $dbval;
			}

			$html .= $col->getHtml('view', $val, false, $pageProps);
		}
		$db->FreeResult($cur);
		$html .= '</div>';
		
		$pageProps['html'] = $html;
		return $pageProps;
	}

	/**
	 * Generate HTML suitable for converting to PDF, using HTMLdoc.
	 * (http://www.htmldoc.org)
	 *
	 * @author Arjan Haverkamp
	 * @param $id(array) An array of record-ids to export.
	 * @param $fields(array) Array of fieldnames to export.
	 * @param $orderBy(string) The order to show the records in.
	 * @param $pagePerRecord(bool) Whether or not to start each record on a new page.
	 * @return A HTML-string representing the preview-page suitable for converting to PDF.
	 * @returns string
	 */
	function viewPDFpage($keyVals = array(), $fields = array(), $orderBy, $pagePerRecord = true) {
		global $db;

		$html = '';

		$selectFields = array();
		foreach($this->cols as $col) {
			if (!is_null($sf = $col->getSQLpart('view')))
				$selectFields[] = $sf;
		}
		if (!in_array($this->key, $selectFields))
			array_unshift($selectFields, $this->key);

		$sql = sprintf("
			SELECT %s
			FROM %s
			WHERE %s IN (%s)
			%s",
			implode(',', $selectFields),
			$this->name,
			$this->key,
			$this->convertKeyval($keyVals),
			$orderBy ? 'ORDER BY ' . escDBquote($orderBy) : ''
		);
		
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($row = 0; !$db->EndOfResult($cur); $row++) {
			$keyVal = $db->FetchResult($cur,$row,$this->key);

			foreach($this->cols as $col) {
				if (!(isset($col->rights['view']) && in_array($col->name, $fields))) {
					continue;
				}
				if (strtolower(get_class($col->datatype)) != 'dtfake' && !$col->datatype->isXref) {
					$GLOBALS['RECORD_ID'] = null === $db->FetchResult($cur,$row,$col->name) ? 'NULL' : $db->FetchResult($cur,$row,$col->name);
				}
				$dbval = $col->getValue('view',$keyVal,$cur,$row);
				if ($dbval === '' || null === $dbval) {
					$dbval = '&nbsp;';
				}
				$html .= '<table width="100%" cellspacing="0" cellpadding="0">';
				$html .= '<tr bgcolor="#dbdbdb" height="16"><td>&nbsp;<font color="#0046d5"><b>' . $col->description . '</b></font></td></tr>';
				$html .= '<tr bgcolor="#000000"><td></td></tr>';
				$html .= '<tr height="4"><td></td></tr>';
				$html .= '</table>';
				$html .= $dbval;
				$html .= '<br><br>';
			}
			if ($pagePerRecord)
				$html .= '<!-- PAGE BREAK -->';
			else
				$html .= '<hr>';
		}
		$db->FreeResult($cur);
		return $html;
	}


	/**
	 * Validate for valid input.
	 * If input is not valid, shows errorpage. Does not return anything.
	 * 
	 * @author Arjan Haverkamp
	 * @param $page(string) The page ('insert', 'copy' or 'update') to validate for.
	 * @returns void
	 * @private
	 */
	function validate($page, $keyVal = null)
	{
		global $db, $PHprefs, $PHSes;
		
		$msgs = array();
		foreach($this->cols as $col) 
		{
			if (isset($col->rights[$page]) && $col->name != $this->key) 
			{
				$col->checkSyntaxPHP($page, $_POST, $msgs);
			}
		}
		if (count($msgs) > 0) 
		{
			$msg = '<ul>';
			foreach($msgs as $err) 
			{
				$msg .= '<li>' . $err . '</li>';
			}
			$msg .= '</ul>';

			if ($PHprefs['useLocks'] && $page == 'update') {
				PH::phoundryErrorPrinter($msg, true, QS(0));
			}
			else {
				PH::phoundryErrorPrinter($msg, true);
			}
		}
	}
	
	/**
	 * Execute an event.
	 *
	 * @author Arjan Haverkamp
	 * @param $event(string) The name of the event.
	 * @param $keyVal(mixed) The id of the record the event is executed on.
	 * @param $oldKeyVal(mixed) The old id of the record the event is executed on (useful for updates)
	 * @return true if the event didn't cause errors, a string otherwise.
	 * @returns mixed
	 */
	function doEvent($event, $keyVal = null, $oldKeyVal = null) {
		global $db, $PHSes, $PHprefs, $DMDprefs, $PHenv;

		if (isset($this->events[$event]))
		{
			foreach($this->events[$event] as $code)
			{
				$retval = eval($code);
				if (!empty($retval)) {
					return $retval;
				}
			}
		}
		return true;
	}

	/**
	 * Finish editing (insert, update, copy) a record. 
	 * Either redirects to appropriate page, or closes popup.
	 * 
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert', 'update' or 'copy'
	 * @param $callback(string) JS-callback function name
	 * @param $editNext(bool) Whether or not to immediately edit the next record
	 * @param $newKey(string) Key value of the newly created record or the deleted record (yeah damn fuzzy!)
	 * @param $oldKey(string) Key value of the modified record (only when $page == 'update')
	 */
	function finishEdit($page, $callback = null, $editNext = false, $newKey, $oldKey = null) {
		global $PHSes, $db;

		if (null !== $callback) {
			if ($page !== 'delete') {
				$sql = "SELECT * FROM {$this->name} WHERE {$this->key} = '" . escDBquote($newKey) . "'";
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				$db->FetchResultAssoc($cur,$row,0);
				$db->FreeResult($cur);
			} else {
				$row = array();
			}
			$this->popupClose($callback, $newKey, $row);
			exit();
		}

		// Determine URL to redirect to:
		if ($page == 'update' && $editNext) {
			$sep = strpos($this->uUrl, '?') === false ? '?' : '&';
			$url = $this->uUrl . $sep . QS(0,'RID=' . rawurlencode($PHSes->shownRIDs[$this->id][(int)array_search($oldKey, $PHSes->shownRIDs[$this->id])+1]));
		}
		elseif ($page == 'insert' && $editNext) {
 			$sep = strpos($this->iUrl, '?') === false ? '?' : '&';
			$url = $this->iUrl . $sep . QS(0);
		}
		else {// Go back to record overview
			$sep = strpos($this->rUrl, '?') === false ? '?' : '&';
			$url = $this->rUrl . $sep . QS(0,"RID&_hilite={$newKey}");
			if (isset($this->extra['reloadMenu']) && strpos($this->extra['reloadMenu'], 'update') !== false) {
				$url .= '&_reloadMenu=1';
			}
		}

		header('Status: 302 moved');
		header('Location: ' . $url);
		exit();
	}
	
	/**
	 * Insert a new record in the database. Send notifications.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) The page ('insert' or 'copy') to insert a record for.
	 * @returns void
	 * @public
	 */
	function insertRecord($page, $redirectAfter = true)
	{
		global $db, $fields, $fieldName, $PHSes, $PHprefs;

		// Trim all POST fields:
		PH::sanitizeInput($_POST);

		// Validate all input:
		$this->validate($page);

		// Does this table contain an auto_increment (identity) field?
		$hasAutoInc = false;
		foreach($this->cols as $col) {
			if (isset($col->DBprops['autoincrement'])) {
				$hasAutoInc = true; break;
			}
		}

		if ($hasAutoInc)
			$newKey = -1;
		else {
			if ($this->keyType == 'integer') {
				$newKey = isset($_POST[$this->key]) ? $_POST[$this->key] : PH::determineNewKeyVal($this->name, $this->key);
				$_POST[$this->key] = $newKey;
			}
			else {
				$newKey = $_POST[$this->key];
			}
		}

		if (!empty($this->extra['versioning']) &&
				!$this->isLiveVersion($_POST)) {
			$this->createVersion($_GET, $_POST);
            if ($_SERVER['HTTPS'] == 'on' && isset($_GET['callback']))
                $_GET['callback'] = str_replace('http:', 'https:', $_GET['callback']);
			if ($redirectAfter) {
				$this->finishEdit($page,
					isset($_GET['callback']) ? $_GET['callback'] : null,
					isset($_POST['_editNext']) && $_POST['_editNext'] == 1,
					$newKey);
			}
			return $newKey;
		}
		
		if ($page == 'copy')
			$eventRes = $this->doEvent('pre-' . $page, $newKey, $_GET['RID']);
		else
			$eventRes = $this->doEvent('pre-' . $page, $newKey);
		if ($eventRes !== true)
			PH::phoundryErrorPrinter($eventRes, true);

		$fields = $extras = array();
		foreach($this->cols as $col) {
			$col->getSQLvalue($page, $_POST, $fields, $extras);
		}

		$pre = $post = array();
		$x = 0;
		foreach($fields as $name=>$value) {
			$pre[] = $name;
			$post[] = $value;
		}
		$sql = "INSERT INTO {$this->name} (" . implode(',',$pre) . ') VALUES (' . implode(',',$post) . ')';
		$cur = $db->Query($sql);
		if (!$cur) {
			$error = $db->Error();
			if (strpos($error, 'Duplicate entry') === false || $PHprefs['DBtype'] != 'mysql') {
				// This is a real SQL error:
				trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				exit;
			}
			else {
				// Duplicate entry error:
				if (preg_match('/for key (.+)/i', $error, $reg)) {
					// Older MySQL versions say: for key 2
					// Newer MySQL versions say: for key 'key_name'
					$key = $reg[1];
					if (preg_match("/^'([^']+)'$/", $key, $reg)) { $key = $reg[1]; };
					$data = $this->getDuplicateData($key, $fields);
					$errMsg = '<ul>';
					foreach($data as $name=>$value) {
						$errMsg .= "<li>{$name} = " . stripslashes($value) . "</li>";
					}
					$errMsg .= '</ul>';
					PH::phoundryErrorPrinter(word(202,$errMsg), true, QS(0));
				}
				else {
					trigger_error("Query $sql failed: " . $db->Error() . ". In addition, the key-index could not be determined.", E_USER_ERROR);
				}
			}
		}
		PH::logPhoundry('QUERY', $sql, $cur ? 'OK' : 'ERROR');

		// Determine record-id:
		if ($hasAutoInc) {
			$newKey = $db->GetLastInsertId($this->name, $this->key);
		}
		
		// Record is succesfully saved so create the version for it
		if (!empty($this->extra['versioning'])) {
			$this->createVersion($_GET, $_POST, $newKey);
		}

		foreach($extras as $sql) {
			$sql = strtr($sql, array('{$RECORD_ID}' => $newKey, 'RECORD_ID' => $newKey));
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			PH::logPhoundry('QUERY', $sql, $cur ? 'OK' : 'ERROR');
		}

		foreach($this->cols as $col) {
			if (method_exists($col->datatype, 'customSave')) {
				$col->datatype->customSave($page, $_POST, $fields, $newKey);
			}
		}

		if ($page == 'copy')
			$eventRes = $this->doEvent('post-' . $page, $newKey, $_GET['RID']);
		else
			$eventRes = $this->doEvent('post-' . $page, $newKey);
		if ($eventRes !== true)
			PH::phoundryErrorPrinter($eventRes, true);

		$this->notifyMessage($newKey, 'insert');

        if ($_SERVER['HTTPS'] == 'on' && isset($_GET['callback']))
            $_GET['callback'] = str_replace('http:', 'https:', $_GET['callback']);

        if ($redirectAfter) {
			$this->finishEdit($page, isset($_GET['callback']) ? $_GET['callback'] : null, isset($_POST['_editNext']) && $_POST['_editNext'] == 1, $newKey);
		}
		else {
			return $newKey;
		}
	}

	/**
	 * Find properties of the record that causes a unique key violation
	 * (duplicate entry). For MySQL only!
	 *
	 * @author Arjan Haverkamp
	 * @param $key(int/string) The key-index or name that was triggered
	 * @param $values(array) An associative array of values that caused the
	 *                       violation
	 * @return An array of duplicate data
	 * @returns array
	 */
	function getDuplicateData($key, $values) {
		global $db;

		$colDescs = array();
		foreach($this->cols as $col) {
			$colDescs[$col->name] = $col->description;
		}
		if (is_numeric($key)) {
			$db->ListTableIndexes($this->name, $indexes);
			$db->GetTableIndexDefinition($this->name, $indexes[$key-1], $indexDef);
		}
		else {
			$db->GetTableIndexDefinition($this->name, $key, $indexDef);
		}
		$data = array();
		foreach($indexDef['FIELDS'] as $name=>$props) {
			$data[$colDescs[$name]] = $values[$name];
		}
		return $data;
	}

	function _getRowValues($key) {
		global $db;

		$selectFields = $row = array();
		foreach($this->cols as $col) {
			if (!is_null($sf = $col->getSQLpart('view'))) {
				$selectFields[] = $sf;
			}
		}
		if (!in_array($this->key, $selectFields)) {
			array_unshift($selectFields, $this->key);
		}

		// Determine old values:
		$sql = "SELECT " . implode(',', $selectFields) . " FROM {$this->name} WHERE {$this->key} = " . $this->convertKeyval($key);
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		foreach($this->cols as $col) {
//			if ($col->datatype->isXref) { continue; } // Wilbert: Arjan removed this on 2011-03-15
			if (!is_null($sf = $col->getSQLpart('view'))) {
				$_val = @$db->FetchResult($cur,0,$col->name);
				if ($_val === false) continue; // For Xrefs
				$GLOBALS['RECORD_ID'] = (null === $_val) ? 'NULL' : $_val;
				$row[$col->name] = $col->getValue('view',$key,$cur,0);
			}
		}
		$db->FreeResult($cur);
		return $row;
	}


	/**
	 * Update an existing record in the database. Send notifications.
	 *
	 * @author Arjan Haverkamp
	 * @returns void
	 * @public
	 */
	function updateRecord($redirectAfter = true)
	{
		global $db, $PHprefs, $PHSes;

		// Trim all POST fields:
		PH::sanitizeInput($_POST);

		$oldKey = $_GET['RID'];
		$newKey = isset($_POST[$this->key]) ? $_POST[$this->key] : $oldKey;

		if ($this->keyType == 'integer') 
		{
			$oldKey = (int)$oldKey;
			$newKey = (int)$newKey;
		}

		// Validate all input:
		$this->validate('update', $oldKey);

		if (false === $this->createVersion($_GET, $_POST, $oldKey)) 
		{
            if ($_SERVER['HTTPS'] == 'on' && isset($_GET['callback']))
                $_GET['callback'] = str_replace('http:', 'https:', $_GET['callback']);

            if ($redirectAfter)
			{
				$this->finishEdit('update',
					isset($_GET['callback']) ? $_GET['callback'] : null,
					isset($_POST['_editNext']) && $_POST['_editNext'] == 1,
					$newKey, $oldKey);
			}
			return;
		}

		$eventRes = $this->doEvent('pre-update', $newKey, $oldKey);
		if ($eventRes !== true) 
		{
			PH::phoundryErrorPrinter($eventRes, true);
		}

		$fields = $extras = $oldRow = $newRow = $skipDiffs = array();
		foreach($this->cols as $col) 
		{
			$col->getSQLvalue('update', $_POST, $fields, $extras, $oldKey);
			if (isset($col->datatype->extra['default']) &&
				 preg_match('/^{\$MOD_/', $col->datatype->extra['default'])) 
			{
				$skipDiffs[$col->name] = true;
			}
		}

		if (count($fields) > 0) 
		{
			// If the table only contains one field, and a cross reference,
			// the $fields array is empty. Don't UPDATE the record itself,
			// continue below with updating the cross reference table(s).

			// Determine old row values:
			$oldRow = $this->_getRowValues($oldKey);

			// Update record in DB:
			$sql = "UPDATE {$this->name} SET ";
			$x = 0;
			foreach($fields as $name=>$value) 
			{
				if ($x++ > 0) $sql .= ', ';
				$sql .= "$name = $value";
			}
			$sql .= " WHERE {$this->key} = " . $this->convertKeyval($oldKey);

			$cur = $db->Query($sql);
			if (!$cur) 
			{
				$error = $db->Error();
				if (strpos($error, 'Duplicate entry') === false || $PHprefs['DBtype'] != 'mysql') 
				{
					// This is a real SQL error:
					trigger_error("Query {$sql} failed: {$error}", E_USER_ERROR);
					exit;
				}
				else 
				{
					// Duplicate entry error:
					if (preg_match('/for key (.+)/i', $error, $reg)) {
						// Older MySQL versions say: for key 2
						// Newer MySQL versions say: for key 'key_name'
						$key = $reg[1];
						if (preg_match("/^'([^']+)'$/", $key, $reg)) { $key = $reg[1]; };
						$data = $this->getDuplicateData($key, $fields);
						$errMsg = '<ul>';
						foreach($data as $name=>$value) {
							$errMsg .= "<li>{$name} = " . stripslashes($value) . "</li>";
						}
						$errMsg .= '</ul>';
						PH::phoundryErrorPrinter(word(202,$errMsg), true, QS(0));
					}
					else 
					{
						trigger_error("Query $sql failed: " . $db->Error() . ". In addition, the key-index could not be determined.", E_USER_ERROR);
					}
				}
			}
			PH::logPhoundry('QUERY', $sql, $cur ? 'OK' : 'ERROR');
		}

		// Update extras (cross references etc):
		foreach($extras as $sql) 
		{
			$sql = strtr($sql, array('{$RECORD_ID}' => $newKey, 'RECORD_ID' => $newKey));
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			PH::logPhoundry('QUERY', $sql, $cur ? 'OK' : 'ERROR');
		}

		foreach($this->cols as $col) 
		{
			if (method_exists($col->datatype, 'customSave')) {
				$col->datatype->customSave('update', $_POST, $fields, $newKey);
			}
		}

		if (isset($this->rights['notify']) && $newKey != $oldKey) 
		{
			// No need for DMdelivery exception here: keys cannot be changed there.
			$sql = "UPDATE phoundry_notify SET keyval = " . $this->convertKeyval($newKey) . " WHERE table_id = {$this->id} AND keyval = " . $this->convertKeyval($oldKey);
			@$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}

		if ($PHprefs['useLocks']) 
		{
			$this->unlockRecord($oldKey, false);
		}
	
		$eventRes = $this->doEvent('post-update', $newKey, $oldKey);
		if ($eventRes !== true) 
		{
			PH::phoundryErrorPrinter($eventRes, true, QS(0));
		}

		// Determine new row values:
		$newRow = $this->_getRowValues($newKey);

		if (!empty($newRow)) 
		{
			// Determine diffs:
			require_once 'Text_Diff-1.1.1/Diff.php';
			require_once 'Text_Diff-1.1.1/Diff/Renderer.php';
			require_once 'Text_Diff-1.1.1/Diff/Renderer/inline.php';
	
			$diffs = array();
			foreach($oldRow as $name=>$value) 
			{
				if ($oldRow[$name] != $newRow[$name] && !isset($skipDiffs[$name])) 
				{
					$diff = new Text_Diff('auto', array(explode("\n", $oldRow[$name] ? $oldRow[$name] : ''), explode("\n", $newRow[$name] ? $oldRow[$name] : '')));
					$renderer = new Text_Diff_Renderer_inline();
					$diffs[$name] = array('diff'=>$renderer->render($diff), 'old'=>$oldRow[$name], 'new'=>$oldRow[$name]);
				}
			}
			$this->notifyMessage($newKey, 'update', $diffs);
		}

        if ($_SERVER['HTTPS'] == 'on' && isset($_GET['callback']))
            $_GET['callback'] = str_replace('http:', 'https:', $_GET['callback']);

        if ($redirectAfter)
		{
			$this->finishEdit('update', isset($_GET['callback']) ? $_GET['callback'] : null, isset($_POST['_editNext']) && $_POST['_editNext'] == 1, $newKey, $oldKey);
		}
	}

	function popupClose($callback, $key, $record = null) {
		global $db, $PHprefs;

		$text = '';
		if (isset($_GET['CID'])) {
			require_once "{$PHprefs['distDir']}/core/include/sqlParser/Parser.php";
			require_once "{$PHprefs['distDir']}/core/include/sqlParser/Compiler.php";
			$parser = new SQL_Parser();
			$compiler = new SQL_Compiler();

			// Create text to show as new option:
			$sql = "SELECT inputtype_extra FROM phoundry_column WHERE id = " . (int)$_GET['CID'];
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$extra = unserialize($db->FetchResult($cur,0,'inputtype_extra'));
			if (preg_match('/^(.*)\[(.*)\](.*)$/', $extra['query'], $reg)) {
				$extra['query'] = PH::evaluate($reg[1] . $reg[3]);
			}
			$db->FreeResult($cur);
			$sqlStruct = $parser->parse($extra['query']);

			$insWhere = array(
				'arg_1' => array('value'=>$extra['optionvalue'],'type'=>'ident'),
				'op' => '=',
				'arg_2' => array('value'=>$key,'type'=>'ident')
			);
			$sqlStruct['where_clause'] =
				isset($sqlStruct['where_clause']) ? 
				array(
					'arg_1' => $insWhere,
					'op' => 'and',
					'arg_2' => $sqlStruct['where_clause']
				) : $insWhere;
			$sql = $compiler->compile($sqlStruct);
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur)) {
				foreach($extra['optiontext'] as $field) {
					if ($text != '') $text .= ' ';
					$text .= PH::htmlspecialchars($db->FetchResult($cur,0,$field));
				}
			}
			$db->FreeResult($cur);
		}

		if ($callback && ($callback[0] === '/' ||
				stripos($callback, 'http://') === 0 ||
				stripos($callback, 'https://') === 0)) {
			$query = @parse_url($callback, PHP_URL_QUERY);
			$hash = @parse_url($callback, PHP_URL_FRAGMENT);
			
			$query_arr = array();
			if ($query) {
				parse_str($query, $query_arr);
			}
			$query_arr['phoundry'] = array(
				'key' => $key
			);
			
			$i = strpos($callback, '?');
			if ($i !== false) {
				$callback = substr($callback, 0, $i);
			}
			$callback .= '?' . http_build_query($query_arr);
			if ($hash) {
				$callback .= '#'.$hash;
			}
			PH::trigger_redirect($callback);
		} else {

			if ($PHprefs['charset'] == 'iso-8859-1') {
				$record = array_walk_recursive($record, function (&$item, $key) {
					if (PH::is_utf8($item) === false) {
						$item = utf8_encode($item);
					}
				});
			}

			$fn = sprintf("%s(%s,%s,'%s');", $callback, $key, json_encode($record), escSquote($text));

			print '<html><head><title></title>
   <script type="text/javascript">
   //<![CDATA[' . "\n{$fn}\n" .
	'if(window.opener){ window.close() }
	try { parent.killPopup(); } catch(e) {}
	//]]>
	</script>
	</head>
	<body></body>
	</html>
';
		}
	}
	
	/**
	 * Delete an existing record from the database. Send notifications.
	 *
	 * @author Arjan Haverkamp
	 * @param $keyVal(mixed) The value for the primary key of the record to delete.
	 * @returns void
	 * @public
	 */
	function deleteRecord($keyVal)
	{
		global $db, $PHprefs, $PHenv;
		
		if ($this->keyType == 'integer') {
			$keyVal = (int)$keyVal;
		}

		$eventRes = $this->doEvent('pre-delete', $keyVal);
		if ($eventRes !== true) {
			exit(json_encode(array('success'=>false, 'response'=>$eventRes)));
		}

		// We need to notify now, before record actually gets deleted...
		$this->notifyMessage($keyVal, 'delete');

		// Delete notifications for this record now:
		$sql = "DELETE FROM phoundry_notify WHERE table_id = {$this->id} AND keyval = " . $this->convertKeyval($keyVal);
		if ($this->DMDcampaignRelated) {
			$sql .= " AND campaign_id = {$PHenv['DMDcid']}";
		}
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		// Delete everything that is associated with the record to be deleted:
		foreach($this->cols as $col)
			$col->deleteStuff($this->key,$keyVal);

		// Delete any locks that might hang around:
		if ($PHprefs['useLocks'])
			$this->unlockRecord($keyVal, true);

		// Actually delete the record:
		$sql = "DELETE FROM {$this->name} WHERE {$this->key} = " . $this->convertKeyval($keyVal);
		$res = @$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		PH::logPhoundry('QUERY', $sql, $res ? 'OK' : 'ERROR');

		// Delete any versioning related stuff
		if (!empty($this->extra['versioning'])) {
			$this->removeVersions($keyVal);
		}

		$eventRes = $this->doEvent('post-delete', $keyVal);
		if ($eventRes !== true) {
			exit(json_encode(array('success'=>false, 'response'=>$eventRes)));
		}
	}

	/**
	 * Delete some (1 or more) records from the database.
	 *
	 * @author Arjan Haverkamp
	 * @param $keyVals(array) An array containing the keyvals to delete.
	 * @returns void
	 * @public
	 */
	function deleteAll($keyVals) 
	{
		foreach($keyVals as $delId) {
			$this->deleteRecord($delId);
		}
	}

	/**
	 * Get the versioning service
	 * @return Versioning_Service|bool
	 */
	public function getVersioningService()
	{
		global $db, $PHprefs, $PHSes;
		require_once "{$PHprefs['distDir']}/core/include/DB/MetaBase.php";
		require_once "{$PHprefs['distDir']}/core/include/Versioning/Service.php";
		return new Versioning_Service(new DB_MetaBase($db));
	}

	private function isLiveVersion(array $post)
	{
		global $PHSes;
		return isset($post['_versioning']['state']) &&
			$post['_versioning']['state'] == 'publish' &&
			($PHSes->isAdmin || PH::getAccessRights($this->id, array('b')));
	}
	
	/**
	 * Load the values of a version into the $_REQUEST array so datatypes will
	 * use those values instead of the database values
	 * @param Versioning_Version $version
	 */
	public function loadVersion(Versioning_Version $version)
	{
		$data = $version->data;
		
		foreach($this->cols as $col) {
			$name = $col->datatype->name;
			if (isset($data[$name])) {
				$_POST['_'.$name] =
				$_REQUEST['_'.$name] = $data[$name];
			}
			else {
				$_POST['_'.$name] =
				$_REQUEST['_'.$name] = "";
			}
		}
	}
	
	/**
	 * Create a version record if enabled
	 * @param array $get the $_GET array
	 * @param array $post the $_POST array
	 * @param mixedvar $rid the record id
	 * @return bool if the save should continue (true) or be aborted (false)
	 */
	public function createVersion(array $get, array $post, $rid = null)
	{
		global $db, $PHprefs, $PHSes;
		if (empty($this->extra['versioning'])) {
			return true;
		}

		$service = $this->getVersioningService();
		
		$data = array();
		foreach($this->cols as $col) {
			if (array_key_exists($col->datatype->name, $post)) {
				$data[$col->datatype->name] = $post[$col->datatype->name];
			}
		}
		
		$status = null;
		if ($this->isLiveVersion($post)) {
			$status = 'published';
		}
		else if (isset($post['_versioning']['state']) && $post['_versioning']['state'] == 'pending') {
			$status = 'pending';
		}
		else {
			$status = 'draft';
		}
		
		$parent_id = null;
		if (isset($post['_versioning']['parent_id'])) {
			$parent_id = $post['_versioning']['parent_id'];
		}
		else if ($rid && $parent = $service->getLiveVersion($this->id, $rid)) {
			$parent_id = $parent->id;
		}
		
		$message = isset($post['_versioning']['message']) ? 
			trim($post['_versioning']['message']) : "";
		
		$version = new Versioning_Version();
		$version->table_id = $this->id;
		$version->record_id = $rid;
		$version->parent_id = $parent_id;
		if (isset($get['site_identifier'])) {
			$version->site_identifier = $get['site_identifier'];
		}
		$version->user_id = $PHSes->userId;
		$version->user_name = $PHSes->userName;
		$version->date = new DB_Expression('NOW()');
		$version->status = $status;
		$version->message = $message;
		$version->data = $data;
		
		$service->storeVersion($version, $this->key);

		if ($status === 'pending') {
			// send notification mail to subscribers
			$this->versioningNotifySend($version);
		}
		
		return $status === 'published';
	}
	
	public function removeVersions($record_id)
	{
		$service = $this->getVersioningService();
		$service->deleteVersions($this->id, $record_id);
	}
	
	/**
	 * Get the versioning notification status for the given user
	 * 
	 * @return bool
	 */
	public function versioningNotifyStatus()
	{
		global $db, $PHprefs, $PHSes;
		require_once "{$PHprefs['distDir']}/core/include/DB/MetaBase.php";
		require_once "{$PHprefs['distDir']}/core/include/Versioning/NotifyService.php";
		$service = new Versioning_NotifyService(new DB_MetaBase($db));
		
		return $service->getStatus($this->id, $PHSes->userId);
	}
	
	private function versioningNotifySend(Versioning_Version $version)
	{
		global $db, $PHprefs, $PHSes, $PRODUCT;
		require_once "{$PHprefs['distDir']}/core/include/DB/MetaBase.php";
		require_once "{$PHprefs['distDir']}/core/include/Versioning/NotifyService.php";
		$service = new Versioning_NotifyService(new DB_MetaBase($db));
		
		$users = $service->getNotifyUsers($this->id);
		if (!$users) {
			// No recipients: no notifications.
			return;
		}
		
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$uurl = $protocol.$_SERVER['HTTP_HOST'].$PHprefs['url'].
			'/core/versioning/trail.php?'.QS(0,'_versioning[parent_id]='.$version->id);
		
		$phoundry = new Phoundry($this->id);
		
		$what = word(3, $PHSes->userName, $uurl);
		$what .= '<br /><small>Client: ' . (isset($_SERVER['REMOTE_HOST']) ? $_SERVER['REMOTE_HOST'] : '') . ' (' . PH::getRemoteAddr() . ')</small>';

		$plainTxt = strip_tags(preg_replace('/<br( \/)?>/i', "\n", $what));

		foreach ($users as $user) {
			$recipient = $user['e_mail'];
			$phoundry->loadVersion($version);
			$body = $phoundry->viewPage((int) $version->record_id,
				$PHSes->userId, $PHSes->isAdmin, true);
			
			if ($body === false) {
				continue;
			}

			// Outlook 2007 cannot display fieldsets/legends...
			// We transform them to <span class="legend">[legend]</span><div class="fieldset">[fieldset]</div>

			$mailHTML = '';
			$dom = new DOMDocument('1.0', $PHprefs['charset']);
			@$dom->loadHTML($body['html']);
			$xpath = new DOMXPath($dom);
			foreach($xpath->query('//fieldset[@class=\'field\']') as $fieldset) {
				$legend = $xpath->query('legend[1]', $fieldset)->item(0);
				$mailHTML .= '<span class="legend">' . $legend->textContent . '</span><div class="fieldset">' . preg_replace('/^' . preg_quote(trim(PH::DOMouterHTML($legend)), '/') . '/', '', trim(PH::DOMinnerHTML($fieldset))) . "</div>\n";
			}

			PH::sendHTMLemail($recipient, 'Notification', $what . '<hr />' . $mailHTML);
		}
	}
}
