<?php
class PhoundryLogin
{
	public $cookie = array();
	public $loginMsg;

	private $protocol;
	private $authServer;
	private $useSafeLogin;
	private $userData = array();

	/**
	 * @param bool $useSafeLogin default set to the phoundry setting safe_login
	 */
	public function __construct($useSafeLogin = null)
	{
		global $PHprefs, $db;

		$this->protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';

		if (null === $useSafeLogin) {
			$this->useSafeLogin = (PH::getSetting('safe_login') == 1);
		} else {
			$this->useSafeLogin = $useSafeLogin;
		}
		
		$this->authServer = isset($PHprefs['authServerUrl']) ? $PHprefs['authServerUrl'] : 'https://phoundry.cherry-marketing.nl';

		if (isset($_COOKIE['PHprefs'])) {
			$this->cookie = PH::is_serialized(stripslashes($_COOKIE['PHprefs'])) ? @unserialize(stripslashes($_COOKIE['PHprefs'])) : stripslashes($_COOKIE['PHprefs']);
			if (!is_array($this->cookie)) {
				$this->cookie = array();
			}
		}

		// Delete expired Phoundry logins:
		$cur = $db->Query("DELETE FROM phoundry_login WHERE max_login_date < UNIX_TIMESTAMP(NOW())");
		if (!$cur) {
			// Create table if it didn't exist:
			$charset = $PHprefs['charset'] == 'iso-8859-1' ? 'latin1' : 'utf8';
			$sql = "CREATE TABLE phoundry_login (user_id integer NOT NULL, ip_address varchar(40) NOT NULL, max_login_date integer NOT NULL, password varchar(40) NOT NULL, type varchar(20), UNIQUE KEY idx_uid_ip (user_id,ip_address))  ENGINE=MyISAM DEFAULT CHARSET={$charset};";
			$db->Query($sql) or trigger_error(
				"Query $sql failed: " . $db->Error(), E_USER_ERROR
			);
		}
	}

	public function getBrowserSignature()
	{
		$signature = '';
		foreach(array('REMOTE_ADDR', 'HTTP_USER_AGENT', 'HTTP_ACCEPT_ENCODING', 'HTTP_ACCEPT_CHARSET') as $part) {
            if ($part === 'HTTP_ACCEPT_ENCODING') { $_SERVER[$part] = str_replace(', sdch', '', $_SERVER[$part]); }
			$signature .= isset($_SERVER[$part]) ? $_SERVER[$part] : 'nada';
		}
		return sha1('&%%$G@' . $signature . ')(&3jhnw');
	}

	public function getGroups($userID, $isAdmin = false)
	{
		global $db;

		// Determine group(s) this user is member of:
		$groups = array();
		if ($isAdmin || $userID == 1) {
			// This user has access to *all* Phoundry interfaces:
			$sql = "SELECT pg.id, pg.name, COUNT(pgt.table_id) AS nr_tables
				  FROM phoundry_group pg, phoundry_table pt, phoundry_group_table pgt 
				  WHERE pgt.group_id = pg.id
				  AND pgt.table_id = pt.id
				  AND (pt.show_in_menu LIKE 'tab%') 
				  GROUP BY pg.id, pg.name
				  ORDER BY nr_tables DESC, pg.name ASC";
		} else {
			// This user has limited access to Phoundry interfaces:
			$sql = "SELECT pg.id, pg.name, COUNT(pgt.table_id) AS nr_tables
				  FROM phoundry_group pg, phoundry_user_group pug, phoundry_table pt, phoundry_group_table pgt
				  WHERE pug.user_id = {$userID} AND pug.group_id = pg.id
				  AND pgt.group_id = pg.id
				  AND pgt.table_id = pt.id
				  GROUP BY pg.id, pg.name
				  ORDER BY nr_tables DESC, pg.name ASC";
		}
		$cur = $db->Query($sql) or trigger_error(
			"Query $sql failed: " . $db->Error(), E_USER_ERROR
		);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$groups[$db->FetchResult($cur,$x,'id')] = $db->FetchResult($cur,$x,'name');
		}
		if ($isAdmin) {
			$groups[-1] = '-- Admin --';
		}

		return $groups;
	}

	/**
	 * Is the current user logged in already?
	 * @return bool
	 */
	public function isLoggedIn()
	{
		global $PHSes;
		return isset($PHSes) && $PHSes->userId !== '';
	}

	public function logout()
	{
		unset($_SESSION['PHSes']);
		session_destroy();
	}

	public function checkLogin($username, $password, $signature)
	{
		global $PHprefs, $db, $PRODUCT;

		// Make sure browser signature hasn't changed:
		if ($signature !== $this->getBrowserSignature()) {
			return false;
		}

		$enableSUlogin = isset($PHprefs['enableSUlogin']) && $PHprefs['enableSUlogin'] == false ? false : true;

		// Check username & password:
		if ($enableSUlogin && preg_match('/^su\./', $username)) {
			$this->checkSUlogin1($username, $password);
			// Login procedure continues after redirect to auth.webpower.nl.
			return false;
		} else {
			$ok = $this->checkLocalLogin($username, $password);
		}

		if (!$ok) {
			return false;
		}

		// Every user needs to be member of at least one group:
		if (empty($this->userData['groups'])) {
			$this->loginMsg = "You are not a member of any group, contact {$PRODUCT['name']} Administrator!";
			return false;
		}

		// SU users have always direct access
		// Admin users ("webpower") only have direct access from trusted IP's
		// Regular users have direct access when $this->useSafeLogin == false
		$loginType = 'direct';
		if ($this->useSafeLogin && false) {
			// This is a non-admin login:
			$sql = "UPDATE phoundry_login SET max_login_date = " . (time() + 7776000) . " WHERE user_id = " . (int)$this->userData['id'] . " AND ip_address = '" . PH::getRemoteAddr() . "' AND type = 'multi'";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$nrAffected = 0;
			$db->AffectedRows($nrAffected);
			$loginType = ($nrAffected == 0) ? 'email' : 'direct';
		}

		if ($loginType == 'direct') {
			// Normal login:
			$this->setLastLogin($this->userData['id']);
			return true;
		}

		//
		// Safe login, via email:
		//
		$pwd = uniqid($this->userData['id'].'_');
		$sql = "REPLACE INTO phoundry_login VALUES (" . (int)$this->userData['id'] . ", '" . escDBquote(PH::getRemoteAddr()) . "', " . (time()+10800 /* 3 hours */) . ", '" . md5($pwd) . "', '')";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		$loginUrl = $this->protocol . $_SERVER['HTTP_HOST'] . $PHprefs['url'];
		$login1Url = $loginUrl . '/?login1=' . urlencode($pwd);
		$login2Url = $loginUrl . '/?login2=' . urlencode($pwd);

		$HTMLbody = word(203, $PRODUCT['name'], '<a href="' . PH::htmlspecialchars($login1Url) . '">' . PH::htmlspecialchars($login1Url) . '</a>', '<a href="' . PH::htmlspecialchars($login2Url) . '">' . PH::htmlspecialchars($login2Url) . '</a>', date('H:i', time()+10800 /* 3 hours */), PH::getRemoteAddr());

		PH::sendHTMLemail($this->userData['e_mail'], "{$PRODUCT['name']} login", nl2br($HTMLbody));

		$this->loginMsg = word(189, $PRODUCT['name'], $this->userData['e_mail']); // You need to confirm your login
		return 'email_confirm';
	}

	private function checkLocalLogin($username, $password)
	{
		global $db, $PHprefs;
		$ip = PH::getRemoteAddr(); $now = time();

		// Delete expired Phoundry login attempts (> 3 months):
		$cur = $db->Query("DELETE FROM phoundry_login_attempt WHERE last_attempt < " . (time() - 86400 * 90));
		if (!$cur) {
			// Create table if it didn't exist:
			$charset = $PHprefs['charset'] == 'iso-8859-1' ? 'latin1' : 'utf8';
			$sql = "CREATE TABLE `phoundry_login_attempt` (`ip_address` varchar(40) not null, `nr_attempts` integer not null, `last_attempt` integer unsigned not null, PRIMARY KEY (`ip_address`)) ENGINE=MyISAM DEFAULT CHARSET={$charset}";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}

		$sql = "DELETE FROM phoundry_login_attempt WHERE last_attempt < " . (time() - 86400 * 90);
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		// Make sure we're not suffering from DDOS logins:
		$sql = "SELECT * FROM phoundry_login_attempt WHERE ip_address = '" . escDBquote($ip) . "'";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur)) {
			$attempt = array();
			$db->FetchResultAssoc($cur, $attempt, 0);
			$failedLoginWait = isset($PHprefs['maxFailedLoginWait']) ? $PHprefs['maxFailedLoginWait'] : 300; // Default 300 secs (5 mins)
			$wait = min($failedLoginWait, pow(2, $attempt['nr_attempts'])); // Max $failedLoginWait seconds
			if ($attempt['last_attempt'] + $wait > $now) {
				// User needs to wait:
				$sql = "UPDATE phoundry_login_attempt SET nr_attempts = nr_attempts+1, last_attempt = {$now} WHERE ip_address = '" . escDBquote($ip) . "'";
				$db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				$this->loginMsg = word(68, date('H-i-s', $now+$wait));
				return false;
			}
		}

		$sql = "SELECT * FROM phoundry_user WHERE username = '" . escDBquote($username) . "' AND password = SHA1(CONCAT(id, '" . PH::$pwdSalt . "', '" . md5($password) . "')) AND username != 'webpower' LIMIT 1";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			$this->loginMsg = word(514); // Invalid login (incorrect username or password)!

			$sql = "INSERT INTO phoundry_login_attempt (ip_address, nr_attempts, last_attempt) VALUES ('" . escDBquote($ip) . "', 1, {$now}) ON DUPLICATE KEY UPDATE nr_attempts = nr_attempts+1, last_attempt = {$now}";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

			return false;
		}

		// Valid login:
		$db->FetchResultAssoc($cur, $this->userData, 0);
		$this->userData['groups'] = $this->getGroups($this->userData['id'], false);

		$sql = "DELETE FROM phoundry_login_attempt WHERE ip_address = '" . escDBquote($ip) . "'";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		return true;
	}

	private function checkSUlogin1($username, $password)
	{
		global $PHprefs;

		$_SESSION['su'] = array(
			'remote_addr' => $_SERVER['REMOTE_ADDR'],
			'username' => $username,
			'password' => $password,
			'http_user_agent' => $_SERVER['HTTP_USER_AGENT'],
		);

		$_host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
		$_host .= '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		$token = PH::WPencode(json_encode(array(
			'callback_url' => $_host,
			'username' => $username,
			'password' => $password,
			'charset' => $PHprefs['charset']
		)));

		$args = array('su' => $token);
		if (!empty($_REQUEST['_page'])) {
			$args['_page'] = $_REQUEST['_page'];
		}

		header('Location: ' . $this->authServer . '/su/client.php?' . http_build_query($args));
	}

	public function checkSUlogin2($token)
	{
		global $PHprefs, $DMDprefs, $db;
		if (!function_exists('curl_init')) {
			$this->loginMsg = "CURL not available, unable to check for valid SU login!";
			return false;
		}

		$token = PH::WPdecode($token);
		if (false === $token) {// Cannot decode token
			$this->loginMsg = word(514) . ' (code su1)'; // Invalid login
			unset($_SESSION['su']);
			return false;
		}
		if (null === ($token = @json_decode($token, true))) {
			$this->loginMsg = word(514) . ' (code su2)'; // Invalid login
			return false;
		}
		// $token already contains 'certificate_serial' and 'callback_url'
		$token['username'] = $_SESSION['su']['username'];
		$token['password'] = $_SESSION['su']['password'];
		$token['uname']    = php_uname('n');
		$token['env']      = isset($PHprefs['suEnv']) ? $PHprefs['suEnv'] : array();
		$token['charset']  = $PHprefs['charset'];
		$token['product']  = isset($PHprefs['product']) ? strtolower($PHprefs['product']) : 'phoundry';
		$token['version']  = isset($PHprefs['version']) ? strtolower($PHprefs['version']) : 'unknown';
		$token['client']   = isset($PHprefs['clientName']) ? $PHprefs['clientName'] : '';
		$token['domain']   = $_SERVER['HTTP_HOST'];
		$token['owner']    = isset($DMDprefs) && isset($DMDprefs['licenseOwner']) ? $DMDprefs['licenseOwner'] : 'nl';
		$token = PH::WPencode(json_encode($token));
		$ch = curl_init($this->authServer . '/su/server.php?su=' . urlencode($token));
		curl_setopt($ch, CURLOPT_TIMEOUT, 40);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(trim(curl_exec($ch)), true);
		if (null === $response) {
			$this->loginMsg = word(514) . '<br />Invalid curl response.'; // Invalid login
			unset($_SESSION['su']);
			return false;
		}
		if (!$response['success']) {
			$this->loginMsg = word(514) . '<br />' . $response['response']; // Invalid login
			unset($_SESSION['su']);
			return false;
		}

		// Login okay, make sure we have a 'Superuser' group (while all SU users are hooked up to this group):
		$sql = "SELECT id FROM phoundry_group WHERE name = 'Superuser'";
		$cur = $db->Query($sql)
			or	trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			$this->loginMsg = "There is no 'Superuser' group!";
			unset($_SESSION['su']);
			return false;
		}

		$sql = "SELECT id, name, e_mail, username, last_login_date FROM phoundry_user WHERE username = 'webpower' LIMIT 1";
		$cur = $db->Query($sql)
			or	trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			// There is no webpower user:
			$this->loginMsg = "There is no 'webpower' user!";
			unset($_SESSION['su']);
			return false;
		}

		// This is a valid SU login:
		$db->FetchResultAssoc($cur, $this->userData, 0);
		$this->userData = array_merge($this->userData, $response['data']);
		$this->userData['groups'] = $this->getGroups($this->userData['id'], $this->userData['is_admin']);
		unset($_SESSION['su']);
		return true;
	}

	public function checkOTPlogin($username, $password, $otp_login_code)
	{
		global $PHprefs, $db;
		if (!function_exists('curl_init')) {
			$this->loginMsg = "CURL not available, unable to check for valid SU login!";
			return false;
		}

		$token = PH::WPencode(json_encode(array(
			'username' => $username,
			'password' => $password,
			'domain' => $_SERVER['HTTP_HOST'],
			'otp_login_code' => $otp_login_code,
			'charset' => $PHprefs['charset'],
			'uname'   => php_uname('n'),
			'env'     => isset($PHprefs['suEnv']) ? $PHprefs['suEnv'] : array()
		)));

		$ch = curl_init($this->authServer . '/su/otp.php?su=' . urlencode($token));
		curl_setopt($ch, CURLOPT_TIMEOUT, 40);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(trim(curl_exec($ch)), true);

		if (null === $response || !is_array($response)) {
			$this->loginMsg = word(514) . ' (code su5)'; // Invalid login
			unset($_SESSION['su']);
			return false;
		}
		if (false === $response) {
			$this->loginMsg = word(514) . ' (code su6)'; // Invalid login
			unset($_SESSION['su']);
			return false;
		}

		// Login okay, make sure we have a 'Superuser' group (while all SU users are hooked up to this group):
		$sql = "SELECT id FROM phoundry_group WHERE name = 'Superuser'";
		$cur = $db->Query($sql)
		or	trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			$this->loginMsg = "There is no 'Superuser' group!";
			unset($_SESSION['su']);
			return false;
		}

		$sql = "SELECT id, name, e_mail, username, last_login_date FROM phoundry_user WHERE username = 'webpower' LIMIT 1";
		$cur = $db->Query($sql)
		or	trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			// There is no webpower user:
			$this->loginMsg = "There is no 'webpower' user!";
			unset($_SESSION['su']);
			return false;
		}

		// This is a valid SU login:
		$db->FetchResultAssoc($cur, $this->userData, 0);
		$this->userData = array_merge($this->userData, $response);
		$this->userData['groups'] = $this->getGroups($this->userData['id'], $this->userData['is_admin']);
		unset($_SESSION['su']);
		return true;
	}

	public function checkSMSlogin($username, $password, $sms_login_code)
	{
		global $PHprefs, $db;
		if (!function_exists('curl_init')) {
			$this->loginMsg = "CURL not available, unable to check for valid SU login!";
			return false;
		}

		$token = PH::WPencode(json_encode(array(
			'username' => $username,
			'password' => $password,
			'domain' => $_SERVER['HTTP_HOST'],
			'sms_login_code' => $sms_login_code,
			'charset' => $PHprefs['charset'],
			'uname'   => php_uname('n'),
			'env'     => isset($PHprefs['suEnv']) ? $PHprefs['suEnv'] : array()
		)));

		$ch = curl_init($this->authServer . '/su/sms.php?su=' . urlencode($token));
		curl_setopt($ch, CURLOPT_TIMEOUT, 40);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(trim(curl_exec($ch)), true);

		if (null === $response || !is_array($response)) {
			$this->loginMsg = word(514) . ' (code su5)'; // Invalid login
			unset($_SESSION['su']);
			return false;
		}
		if (false === $response) {
			$this->loginMsg = word(514) . ' (code su6)'; // Invalid login
			unset($_SESSION['su']);
			return false;
		}

		// Login okay, make sure we have a 'Superuser' group (while all SU users are hooked up to this group):
		$sql = "SELECT id FROM phoundry_group WHERE name = 'Superuser'";
		$cur = $db->Query($sql)
			or	trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			$this->loginMsg = "There is no 'Superuser' group!";
			unset($_SESSION['su']);
			return false;
		}

		$sql = "SELECT id, name, e_mail, username, last_login_date FROM phoundry_user WHERE username = 'webpower' LIMIT 1";
		$cur = $db->Query($sql)
			or	trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			// There is no webpower user:
			$this->loginMsg = "There is no 'webpower' user!";
			unset($_SESSION['su']);
			return false;
		}

		// This is a valid SU login:
		$db->FetchResultAssoc($cur, $this->userData, 0);
		$this->userData = array_merge($this->userData, $response);
		$this->userData['groups'] = $this->getGroups($this->userData['id'], $this->userData['is_admin']);
		unset($_SESSION['su']);
		return true;
	}

	/**
	 * @param string $login
	 * @param string $kind Either 'once' or 'multi'
	 * @return bool
	 */
	public function checkDelayedLogin($login, $kind = 'once')
	{
		global $PHprefs, $PHSes, $db;

		//
		// Delayed login (user clicked login-link in e-mail):
		//
		$tmp = explode('_', $login);

		$sql = "SELECT pu.* FROM phoundry_login pl, phoundry_user pu WHERE pl.user_id = " . (int)$tmp[0] . " AND pl.password = '" . md5($login) . "' AND pl.ip_address = '" . PH::getRemoteAddr() . "' AND pl.user_id = pu.id AND pl.type = ''";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			$this->loginMsg = word(515); //Invalid login -  Login link error
			return false;
		}

		// Succesful delayed login:
		$db->FetchResultAssoc($cur, $this->userData, 0);
		$this->userData['groups'] = $this->getGroups($this->userData['id'], false);

		if ($kind == 'multi') {
			// 7776000 = 90 days
			$sql = "UPDATE phoundry_login SET type = 'multi', max_login_date = " . (time() + 7776000) . " WHERE user_id = " . (int)$tmp[0] . " AND ip_address = '" . escDBquote(PH::getRemoteAddr()) . "' AND password = '" . md5($login) . "'";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}
		elseif ($kind == 'once') {
			$sql = "DELETE FROM phoundry_login WHERE user_id = " . (int)$tmp[0] . " AND ip_address = '" . escDBquote(PH::getRemoteAddr()) . "' AND password = '" . md5($login) . "'";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}

		$this->setLastLogin($this->userData['id']);

		return true;
	}

	public function getLoginToken($username, $password)
	{
		global $db;

		$fn = preg_match('/^su\./', $username) ? 'checkSUlogin1' : 'checkLocalLogin';
		$loginOK = $this->$fn($username, $password);
		if (!$loginOK) {
			return '';
		}
		$token = uniqid('');
		$sql = "UPDATE phoundry_user SET login_token = '{$token}' WHERE id = {$this->userData['id']} LIMIT 1";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		return $token;
	}

	public function checkTokenLogin($token)
	{
		global $db;

		$sql = "SELECT * FROM phoundry_user WHERE login_token = '" . escDBquote($token) . "' AND login_token IS NOT NULL AND login_token != '' LIMIT 1";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur)) {
			$this->loginMsg = word(516); //Invalid login (invalid token)!
			return false;
		}

		$db->FetchResultAssoc($cur, $this->userData, 0);
		$this->userData['groups'] = $this->getGroups($this->userData['id'], false);

		// Invalidate token:
		$sql = "UPDATE phoundry_user SET login_token = '' WHERE id = " . (int)$this->userData['id'];
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		return true;
	}

	private function setLastLogin($userID)
	{
		global $db;
		$sql = "UPDATE phoundry_user SET last_login_date = NOW(), last_login_ip = '" . escDBquote(PH::getRemoteAddr()) . "' WHERE id = {$userID}";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}

	public function startPhoundrySession($lang = null)
	{
		global $PHprefs, $DMDprefs;

		$PHSes = new PHSes();
		$PHSes->lang      = !is_null($lang) ? $lang : (isset($this->cookie['lang']) ? $this->cookie['lang'] : $PHprefs['languages'][0]);
		$PHSes->groupId   = null;
		$PHSes->loginName = $this->userData['username'];
		$PHSes->userId    = (int)$this->userData['id'];
		$PHSes->userName  = $this->userData['name'];
		$PHSes->email     = $this->userData['e_mail'];
		$PHSes->lastLoginDate = $this->userData['last_login_date'];
		if (isset($this->userData['kind'])) {
			$PHSes->kind = $this->userData['kind'];
		}
		if (isset($this->userData['is_admin'])) {
			// For SU login:
			$PHSes->isAdmin = $this->userData['is_admin'];
		} else {
			// For regular login:
			$PHSes->isAdmin = false;
		}

		reset($this->userData['groups']);
		$PHSes->groups = $this->userData['groups'];
		$PHSes->groupId = key($this->userData['groups']); // Set active group to first group in list
		$_SESSION['PHSes'] = $PHSes;
		if (isset($DMDprefs)) {
			$_SESSION['NRrecipient'] = $_SESSION['NRrecipient_egroup'] = 0;
		}

		PH::logPhoundry('LOGIN', 'Successful login', 'OK');

		$_className = isset($DMDprefs) ? 'DMD' . str_replace('-','_',$DMDprefs['identifier']) : 'x______x';
		$_fnName = 'postLogin';
        if (class_exists($_className) && method_exists($_className, $_fnName)) {
            call_user_func(array($_className, $_fnName), $this->userData['id']);
        }
        elseif (function_exists($_fnName)) { $_fnName($this->userData['id']); }

        // For DMdelivery:
        if (class_exists('DMD') && method_exists('DMD', 'postLogin')) {
            DMD::postLogin($this->userData['id']);
        }
	}

	public function needsPasswordChange($userID)
	{
		global $db;

		$passwordResetInterval = PH::getSetting('password_reset_interval');
		if ($userID == 1 || $passwordResetInterval == 0) { return false; }

		// Does the user have to change his password?
		$sql = "SELECT UNIX_TIMESTAMP(password_set) FROM phoundry_user WHERE id = " . (int)$userID;
		$cur = $db->Query($sql) or trigger_error(
			"Query $sql failed: " . $db->Error(), E_USER_ERROR
		);
		return time() - (int)$db->FetchResult($cur,0,0) > $passwordResetInterval * 86400;
	}

	public function getLoginPage($message = '', $onload = '')
	{
		global $PHSes, $PHprefs, $DMDprefs, $PRODUCT;
		session_regenerate_id(true);


		if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			$tmp = array($PHprefs['languages'][0]);
		} else {
			$tmp = explode('-', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
		}

		$pageVars = array(
			'username' => isset($this->cookie['username']) ? $this->cookie['username'] : '',
			'lang'    => isset($this->cookie['lang']) ? $this->cookie['lang'] : ((in_array($tmp[0], $PHprefs['languages'])) ? $tmp[0] : $PHprefs['languages'][0] ),
			'product' => $PRODUCT['name'],
			'version' => $PRODUCT['version'],
			'title'   => isset($PHprefs['title']) ? $PHprefs['title'] : $PRODUCT['name'],
			'favicon' => $PHprefs['customUrls']['pics'] . '/favicon.ico',
			'langs'   => array('nl'=>'Nederlands', 'en'=>'English', 'de'=>'Deutsch', 'sv'=>'Svenska', 'cn'=>'中文', 'ja'=>'日本語', 'es'=>'Español'),
			'message' => $message,
			'signature' => $this->getBrowserSignature(),
			'onload' => $onload
		);

		/*** HACK to translate loginUrl to loginFile ***/
		if ($PHprefs['customUrls']['login'] == $PHprefs['url'] . '/layout/login.php') {
			$loginFile = $PHprefs['distDir'] . '/layout/login.php';
		} elseif (strpos($PHprefs['customUrls']['login'], 'custom') == false) {
			$loginFile = preg_replace("|{$PHprefs['url']}/?$|", '', str_replace('/index.php','',$_SERVER['SCRIPT_FILENAME'])) . $PHprefs['customUrls']['login'];
		} else {
			$loginFile = preg_replace("|{$PHprefs['url']}/?$|", '', $PHprefs['instDir']) . $PHprefs['customUrls']['login'];
		}
		/*** /HACK to translate loginUrl to loginFile ***/
		ob_start();
		include $loginFile;
		return ob_get_clean();
	}

	public function getLoginMessage()
	{
		return $this->loginMsg;
	}

	public function setCookieVar($name, $value)
	{
		if (null === $value) {
			unset($this->cookie[$name]);
		} else {
			$this->cookie[$name] = $value;
		}
		$_COOKIE['PHprefs'] = serialize($this->cookie);
		setcookie('PHprefs', $_COOKIE['PHprefs'], (time()+(3600*24*365)), '/', preg_replace('/:([0-9]+)$/', '', $_SERVER['HTTP_HOST']), $this->protocol == 'https://' ? 1 : 0, true);
	}

	/**
	 * Define Phoundry Frameset.
	 *
	 * @author Arjan Haverkamp
	 * @param string $url to show in the content frame.
	 * @return string representing the frameset.
	 */
	public function getFrameset($url)
	{
		global $PHprefs, $DMDprefs, $PRODUCT, $PHSes;

		ob_start();
		include $PHprefs['distDir'] . '/layout/frameset.php';
		return ob_get_clean();
	}
}
