<?php
/*
Sample :
$thumb=new thumbnail('./source.jpg');	// Original filename
$thumb->size_width(100);					// set width for thumbnail, or
$thumb->size_height(300);					// set height for thumbnail, or
$thumb->size_auto(200);						// set the biggest width or height for thumbnail
$thumb->jpeg_quality(75);					// [OPTIONAL] set quality for jpeg only (0 - 100) (worst - best), default = 75
$thumb->show();								// show the thumbnail
$thumb->save("./huhu.jpg");				// save thumbnail to file
----------------------------------------------
Note :
- GD must be Enabled
- Autodetect file extension (.jpg/jpeg, .png, .gif, .wbmp)
  but some server can't generate .gif / .wbmp file types
- If your GD does not support 'ImageCreateTrueColor' function,
  change one line from 'ImageCreateTrueColor' to 'ImageCreate'
  (the position in 'show' and 'save' function)
*/


class Thumbnail
{
	public $format, $src, $dest, $width, $height, $widthThumb, $heightThumb;
	public $quality = 75;

	public function __construct($imgFile)
	{
		// Determine source image format:
		if (preg_match('/\.(gif|jpg|jpeg|png|wbmp)$/i', $imgFile, $reg)) {
			$this->format = strtoupper($reg[1]);
			if ($this->format == 'JPG')
				$this->format = 'JPEG';
			$fn = 'ImageCreateFrom' . $this->format;
			$this->src = $fn($imgFile);
			$this->width = imagesx($this->src);
			$this->height = imagesy($this->src);
		}
		else {
			die("Files with extension {$reg[1]} cannot be thumbnailed!");
		}
	}

	public function sizeBoth($width, $height) {
		$this->widthThumb  = $width;
		$this->heightThumb = $height;
	}

	public function sizeHeight($size=100)
	{
		$this->heightThumb = $size;
    	$this->widthThumb  = ($this->heightThumb/$this->height) * $this->width;
	}

	public function sizeWidth($size=100)
	{
		$this->widthThumb  = $size;
		$this->heightThumb = ($this->widthThumb/$this->width) * $this->height;
	}

	public function sizeAuto($size=100)
	{
		//size
		if ($this->width >= $this->height) {
			$this->widthThumb = $size;
    		$this->heightThumb = ($this->widthThumb/$this->width) * $this->height;
		} else {
	    	$this->heightThumb = $size;
    		$this->widthThumb  = ($this->heightThumb/$this->height) * $this->width;
 		}
	}

	public function jpegQuality($quality=75)
	{
		$this->quality = $quality;
	}

	public function show()
	{
		//show thumb
		header('Content-Type: image/'.$this->img['format']);

		$this->dest = ImageCreateTrueColor($this->widthThumb,$this->heightThumb);
		imagecopyresampled($this->dest, $this->src, 0, 0, 0, 0, $this->widthThumb, $this->heightThumb, $this->width, $this->height);

		if ($this->format == 'JPEG')
			imageJPEG($this->dest, '', $this->quality);
		else {
			$fn = 'image' . $this->format;
			$fn($this->dest);
		}
	}

	public function save($save='', $format = '')
	{
		//save thumb
		if (empty($save)) $save=strtolower('./thumb.'.$this->format);
		$this->dest = ImageCreateTrueColor($this->widthThumb, $this->heightThumb);
  		imagecopyresampled($this->dest, $this->src, 0, 0, 0, 0, $this->widthThumb, $this->heightThumb, $this->width, $this->height);

		if (empty($format))
			$format = $this->format;

		if ($format == 'JPEG'||$format == 'JPG')
			imageJPEG($this->dest, $save, $this->quality);
		else {
			$fn = 'image' . $format;
			$fn($this->dest, $save);
		}
	}
}
?>
