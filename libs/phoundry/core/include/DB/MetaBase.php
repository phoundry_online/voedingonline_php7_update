<?php
if (!class_exists('DB_Mysql')) {
	require_once 'Mysql.php';
}
if (!class_exists('DB_Exception')) {
	require_once 'Exception.php';
}

class DB_MetaBase Extends DB_Mysql
{
	public function __construct($metabase)
	{
		$resource = $metabase->connection;
		if (null !== $resource && !is_resource($resource)) {
			throw new DB_Exception("Metabase given has no valid resource");
		}
		$this->_resource = $resource;
		parent::__construct();
	}
	
	public function connect($host, $user, $pass, $db, $port = 3306)
	{
		throw new DB_Exception("MetaBase should already have connected");
	}
	
	public function disconnect()
	{
		throw new DB_Exception("Use MetaBase to disconnect");
	}

	public function query($sql)
	{
		throw new DB_Exception("DB_MetaBase does not implement the decrepated query method");
	}
	
	/**
	 * Returns the database name of the currently selected database
	 * 
	 * @return string|void
	 */
	public function getDbName()
	{
		global $PHprefs;
		return isset($PHprefs['DBname']) ? $PHprefs['DBname'] : null;
	}
	
	/**
	 * Returns the details for this connection
	 * 
	 * @return array
	 */
	public function getConnectionDetails()
	{
		global $PHprefs;
		return array(
			'host' => $PHprefs['DBserver'],
			'user' => $PHprefs['DBuserid'],
			'pass' => $PHprefs['DBpasswd'],
			'db' => $PHprefs['DBname'],
			'port' => $PHprefs['DBport']
		);
	}
}
