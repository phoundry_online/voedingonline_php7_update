<?php
class DB_Exception extends Exception {
	const BAD_TABLE = 1146;
	const UNKNOWN_COLUMN = 1054;
}