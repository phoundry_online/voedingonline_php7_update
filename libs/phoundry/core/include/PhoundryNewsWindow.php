<?php

class PhoundryNewsWindow {

static public function getHTML() {
	global $PHSes, $PHprefs, $PRODUCT;

	$url = 'http://portal.webpower.nl/productNews/?product=phoundry&l=' . $PHSes->lang . '&uname=' . urlencode(php_uname('n')) . '&version=' . urlencode($PRODUCT['version']);
	if (function_exists('curl_init')) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$xml = curl_exec($ch);
	}
	else {
		$xml = @file_get_contents($url);
	}

	$parser = xml_parser_create($PHprefs['charset']); // This only specifies output encoding!!!
	xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
	xml_parse_into_struct($parser,$xml,$vals,$index);
	xml_parser_free($parser);

	$html = $version = '';

	$html .=<<<EOH
<script type="text/javascript">
//<![CDATA[

var scrollCount, scrollTimer, oldHeadline = 0, curHeadline=0;

$(function(){
	scrollCount = $('div.headline').length;
	$('div.headline:eq('+curHeadline+')').css('top','3px');
	if (scrollCount < 2) return;
	scrollTimer = setInterval(scroller,5000); //time in milliseconds
	$('#dn .winContent').hover(function() {
		clearInterval(scrollTimer);
	}, function() {
		scrollTimer = setInterval(scroller,5000); //time in milliseconds
		scroller();
	});
});

function scroller() {
	curHeadline = (oldHeadline + 1) % scrollCount;
	$('div.headline:eq('+oldHeadline+')').animate({top:-105},'slow',function(){
		$(this).css('top','110px');
	});
	$('div.headline:eq('+curHeadline+')').show().animate({top:3},'slow');  
	oldHeadline = curHeadline;
}

//]]>
</script>
EOH;

	$msgs = array();
	foreach($vals as $idx=>$info) {
		if ($info['tag'] == 'item' && $info['type'] == 'open' && isset($info['attributes']) && isset($info['attributes']['version'])) {
			$version = $info['attributes']['version'];
		}
		if ($info['tag'] == 'title' && $info['type'] == 'complete') {
			$title = $info['value'];
		}
		elseif ($info['tag'] == 'date' && $info['type'] == 'complete') {
			$date = substr($info['value'],0,10);
		}
		elseif ($info['tag'] == 'description' && $info['type'] == 'complete') {
			$desc = $info['value'];
			$msg = '<div class="headline">';
			if (!empty($version)) {
				$sep = strpos($PHprefs['manualUrl'], '?') === false ? '?' : '&';
				$msg .= '<a href="' . $PHprefs['manualUrl'] . $sep . 'changelog#v'  . $version . '" onclick="openCommunity(this.href);return false">';
			}
			$msg .= '<b>' . PH::htmlspecialchars($title) . '</b>';
			if (!empty($version)) {
				$msg .= '</a>';
			}
			$msg .= ' - ' . $date;
			$msg .= '<br />';
			$msg .= $desc . '</div>';
			$msgs[] = $msg;
			$version = $title = $date = $desc = '';
		}
	}

	if (empty($msgs)) {
		return '<p align="center" style="margin-top:65px">' . word(248 /* No messages */) . '</p>';
	}
	else {
		return '<div style="padding:3px">' . implode('<hr noshade="noshade" size="1" />', $msgs) . '</div>';
	}
} 

} // End class

?>
