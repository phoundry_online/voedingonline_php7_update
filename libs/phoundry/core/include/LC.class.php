<?php

// Define version info:
require_once dirname(__FILE__) . '/../../VERSION';

if (!function_exists('escSquote')) {
    /**
     * Escape all single quotes in string $str with a backslash.
     *
     * @author Arjan Haverkamp
     * @param $str(string)	The string to escape.
     * @return string The escaped string.
     */
    function escSquote($str)
    {
        return str_replace("'", "\\'", $str);
    }
}

if (!function_exists('escDBquote')) {
    /**
     * Escape all single quotes in string $str for use in database queries.
     * @param $str(string)	The string to escape.
     * @return string the escaped string
     */
    function escDBquote($str)
    {
        global $db;
        return mysqli_real_escape_string($db->connection, $str);
    }
}

/**
 * Get a word from the language-file.
 * Has a variable number of arguments, one is required: the id of the word to
 * get. The other arguments represent values that should be replaced in the
 * word.
 * Example: word[24] = 'This is example #1 and #2'.
 * A call to word(24,'one','two') would result in 'This is example one and two'.
 * This function uses two word-arrays: customWORDS and coreWORDS, customWORDS
 * has a higher precedence (core-words can be overwritten by custom-words).
 *
 * @author Arjan Haverkamp
 * @return string
 */
function word()
{
	// The $coreWORDS and $customWORDS are utf-8 encoded!
	global $coreWORDS, $customWORDS, $PHprefs;

	$wordNR = func_get_arg(0);
	$ss = isset($customWORDS[$wordNR]) ? $customWORDS[$wordNR] : $coreWORDS[$wordNR];
	$na = func_num_args();
	for ($x = 1; $x < $na; $x++) {
		$tmp = func_get_arg($x);
		$ss = str_replace('#' . $x, $tmp, $ss);
	}
	return $PHprefs['charset'] == 'iso-8859-1' && LC::is_utf8($ss) ? utf8_decode($ss) : $ss;
}

/** Backwards compatibility functions (moved to PH.class.php) **/
function getTID()
{
	return LC::getTID();
}

function QS($encode = true, $extra = null)
{
	return LC::QS($encode, $extra);
}

function evaluate($str)
{
	return LC::evaluate($str);
}

function getRemoteAddr()
{
	return LC::getRemoteAddr();
}

function WPencode($str, $editor = false)
{
	return LC::WPencode($str, $editor);
}

function WPdecode($instr)
{
	return LC::WPdecode($instr);
}

function checkAccess($TID = null, $right = '')
{
	LC::checkAccess($TID, $right);
}

function RmDirR($dirName)
{
	LC::RmDirR($dirName);
}

function getNotes($tableId, $userId)
{
	return LC::getNotes($tableId, $userId);
}

function langWord($word, $lang = null)
{
	return LC::langWord($word, $lang);
}
/** /Backwards compatibility functions **/

abstract class LC
{

static public $pwdSalt = '*&|^%$$^*%(U(&*^$!BFFjnnfidu+O}=P:":L:KJN';

static public function sanitizeInputItem(&$item, $key)
{
	$item = trim($item);
}

static public function sanitizeInput(&$source)
{
	array_walk_recursive($source, array('LC', 'sanitizeInputItem'));
}

/**
 * Output a Content-type header.
 * When type == 'text/html', output a comment tag to force IE <= 7 into
 * quirks mode. Too bad we still need old IE support, this hack sucks big time.
 * @param string $type
 * @param null $charset
 */
static public function getHeader($type, $charset = null)
{
	global $PHprefs, $DMDprefs;

	if (null === $charset) { $charset = $PHprefs['charset']; }

	header("Content-type: {$type}; charset={$charset}");
	if (isset($DMDprefs) && $type == 'text/html' && preg_match('/compatible; MSIE ([^;]+);/', $_SERVER['HTTP_USER_AGENT'], $reg)) {
		$version = (float)$reg[1];
		if ($version < 7) {
			print "<!-- Ancient IE Quirks mode -->\n";
		}
	}
}

static public function includeLangFiles($lang)
{
	global $PHprefs, $coreWORDS, $customWORDS;
	global $g_locale, $g_dateFormat, $g_csvFieldSep;

	// Include Phoundry's core words:
	@include "{$PHprefs['distDir']}/core/lang/{$lang}/words.php";

	// You can define your own language directories too:
	if (isset($PHprefs['langDirs'])) {
		foreach($PHprefs['langDirs'] as $langDir) {
			if (file_exists("{$langDir}/{$lang}/words.php")) {
				include "{$langDir}/{$lang}/words.php";
			}
		}
	}

	// Finally, include custom lang file, which can overwrite 'core' words
	// with custom ones.
	$file = $PHprefs['instDir'] . '/custom/lang/' . $lang . '/words.php';
	if (file_exists($file)) {
		include $file;
	}

	// For backwards compatibility:
	$g_locale      = $coreWORDS[250];
	$g_dateFormat  = $coreWORDS[251];
	$g_csvFieldSep = ';';

	// In China, the CSV field separator is always a comma, independent of user language:
	global $DMDprefs;
	if (isset($DMDprefs) && isset($DMDprefs['licenseOwner'])) {
		switch ($DMDprefs['licenseOwner']) {
			case 'cn': $g_csvFieldSep = ','; break;
			//case 'nl': case 'se': $g_csvFieldSep = ';'; break;
			default: $g_csvFieldSep = ';'; break;
		}
	}
}

/**
 * Establish a permanent database-connection. Relies on the following
 * $PHprefs variables:
 * DBserver, DBpasswd, DBname, DBport and DBtype
 *
 * @author Arjan Haverkamp
 * @return metabase_mysql_class_LC A connection to the specified database.
 */
static public function DBconnect()
{
	global $PHprefs;

	require_once "{$PHprefs['distDir']}/core/include/metabase/metabase_database_LC.php";

	$options = isset($PHprefs['DBoptions']) ? $PHprefs['DBoptions'] : array();

	$error = MetabaseSetupDatabaseObject(array(
		'Type' =>		$PHprefs['DBtype'],
		'Host' =>		$PHprefs['DBserver'],
		'User' =>		$PHprefs['DBuserid'],
		'Password' =>	$PHprefs['DBpasswd'],
		'Port' =>		$PHprefs['DBport'],
		'Persistent'	=> isset($PHprefs['DBpersistent']) ? $PHprefs['DBpersistent'] : false,
		'IncludePath' =>	"{$PHprefs['distDir']}/core/include/metabase/",
		'Charset'	=> $PHprefs['charset'],
		'Options' => $options), $db);
	if ($error != '')
	{
		die("Cannot connect to database on '{$PHprefs['DBserver']}', user '{$PHprefs['DBuserid']}': $error!<br />\n");
	}
    /** @var $db metabase_mysql_class_LC */
	$db->SetDatabase($PHprefs['DBname']);
	$db->Connect();

	// Set database connection timezone:
	if ($PHprefs['DBtype'] == 'mysql' && isset($PHprefs['timezonePHP'])) {
		if (!isset($PHprefs['timezoneMySQL'])) {
			$PHprefs['timezoneMySQL'] = $PHprefs['timezonePHP'];
		}
		$sql = "SET session time_zone = '" . mysqli_real_escape_string($db->connection, $PHprefs['timezoneMySQL']) . "'";
		$db->Query($sql) or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}

	// Set up profilerFirebug if enabled
	if (isset($options['profilerFirebug']) && $options['profilerFirebug']) {
		require_once "{$PHprefs['distDir']}/core/include/FirePHPCore/fb.php";
	}

	// Dummy query, in order to 'initialize' the actual DB connection.
	// If we don't do this, we cannot use bare 'mysql_query' on the connection initialized here.
	$db->Query('SELECT 1') or trigger_error("Query SELECT 1 failed: " . $db->Error(), E_USER_ERROR);

	return $db;
}

/**
 * Retrieve 'TID' from REQUEST or QUERY_STRING.
 * TID stands for tableID, the 'id' field in the 'phoundry_table' table.
 */
static public function getTID()
{
	if (isset($_REQUEST['TID']))
		return (int)$_REQUEST['TID'];
	elseif (isset($_REQUEST['QUERY_STRING'])) {
		parse_str($_REQUEST['QUERY_STRING'], $qs);
		if (isset($qs['TID']))
			return (int)$qs['TID'];
	}
	return '';
}

/**
 * Get QUERY string.
 * @param bool $encode
 * @param null|string $extra
 * @return string
 */
static public function QS($encode = true, $extra = null)
{
	$QSpage = array();

	if (!empty($_GET)) {
		// Parse current query-string into QSpage array:
		foreach($_GET as $name=>$value) {
			if ($name[0] == '_') continue;
			$QSpage[strip_tags($name)] = strip_tags($value);
		}
	}

	$TIDchange = false;

	if (null !== $extra) {
		// Parse extra parameters into QSextra array:
		parse_str($extra, $QSextra);
		foreach($QSextra as $name=>$value) {
			if ($name == 'TID' && isset($QSpage['TID']) && $QSpage['TID'] != $value) {
				$TIDchange = true;
			}
			if ($value === '') {
				unset($QSpage[$name]);
			}
			else {
				$QSpage[$name] = $value;
			}
		}
	}

	// If the 'TID' parameter is changed, we need to reset the LC* vars:
	if ($TIDchange) {
		unset($QSpage['PHsrch'], $QSpage['PHshow'], $QSpage['PHs_in'], $QSpage['PHordr'], $QSpage['PHfltr']);
	}

	$qs = http_build_query($QSpage);
	return $encode ? LC::htmlspecialchars($qs) : $qs;
}

static public function iconv($srcCharset, $dstCharset, $str)
{
	if ($srcCharset == $dstCharset) { return $str; }
	if (function_exists('mb_convert_encoding') && $srcCharset != 'mac')
	{
		return mb_convert_encoding($str, $dstCharset, $srcCharset);
	}
	return iconv($srcCharset, "{$dstCharset}//TRANSLIT", $str);
}

/**
 * Check whether the current user is logged in.
 * Redirects to login-page if not.
 * Also: check if the current user has the right to access a DB-table.
 * Redirects to Phoundry-homepage if not.
 *
 * @author Arjan Haverkamp
 * @param int $TID The table-id to check access for.
 *        Use 'admin' for admin access.
 * @param string $right The right to check on table $TID (insert,update,delete,preview,notify,import,export,copy).
 * @param string $url The page to redirect to when access is denied. By default this is the Phoundry homepage.
 */
static public function checkAccess($TID = null, $right = '', $url = null)
{
	global $db, $PHSes, $PHprefs;

	if (null === $url) {
		$url = $PHprefs['customUrls']['home'];
	}
	$sep = strpos($url, '?') === false ? '?' : '&';
	$rights = array('insert'=>'i', 'update'=>'u', 'delete'=>'d', 'view'=>'p', 'notify'=>'n', 'import'=>'m', 'export'=>'x', 'copy'=>'c');

	/* Does not work when web-pages contain a direct link to Phoundry!
	if (!empty($_SERVER['HTTP_REFERER']) && !preg_match('/'.$_SERVER['HTTP_HOST'].'/', $_SERVER['HTTP_REFERER'])) {
		die('Referrer error!');
	}
	*/

	if (!isset($_SESSION['PHSes']) || $PHSes->userId === '')
	{
		// User is not logged in at all!
		header('Status: 302 moved');
		header("Location: {$PHprefs['url']}/?_page=" . urlencode($_SERVER['REQUEST_URI']));
		exit();
	}

	if ($TID === 'admin' && !$PHSes->isAdmin)
	{
		// Administrator only!
		header('Status: 302 moved');
		header("Location: {$url}{$sep}_msg=195"); // You have no access to this page
		exit();
	}

	if (!$TID && $right !== '')
	{
		header('Status: 302 moved');
		header("Location: {$url}{$sep}_msg=195"); // You have no access to this page
		exit();
	}

	if (null !== $TID && $TID !== 'admin' && !$PHSes->isAdmin)
	{
		$sql = "SELECT rights FROM phoundry_group_table WHERE group_id = " . (int)$PHSes->groupId . " AND table_id = " . (int)$TID;
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur)) {
			if ($right == '' || strpos($db->FetchResult($cur,0,'rights'), $rights[$right]) !== false) {
				$db->FreeResult($cur);
				return;
			}
		}
		$db->FreeResult($cur);
		header('Status: 302 moved');
		header("Location: {$url}{$sep}_msg=195"); // You have no access to this page
		exit();
	}
}

static public function getSetting($name)
{
	global $db;
	$sql = "SELECT value FROM phoundry_setting WHERE name = '" . escDBquote($name) . "' LIMIT 1";
	$cur = $db->Query($sql) or
		trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$retVal = $db->EndOfResult($cur) ? false : trim($db->FetchResult($cur,0,'value'));
	$db->FreeResult($cur);

	return $retVal;
}

static public function checkPasswordStrength($password, $desc)
{
	$password = trim($password);
	$errors = array();
	// Password must be at least 8 chars long
	// Password must contain at least one capital letter
	// Password must contain at least one number
	// Password must contain at least one punctuation character
	if (strlen($password) < 8) {
		$errors[] = word(452, $desc, '8'); // At least 8 chars
	}
	if (!preg_match('/[A-Z]/', $password)) {
		$errors[] = word(453, $desc); // At least one capital
	}
	if (!preg_match('/\d/', $password)) {
		$errors[] = word(454, $desc); // At least one number
	}
	if (!preg_match('/[\~\`\!\@\#\$\%\^\&\*\(\)\-\_\=\+\{\}\[\]\\\|\.\,\s\'\"\;\:\<\>\?\/]/', $password)) {
		$errors[] = word(455, $desc); // At least one special char
	}
	return $errors;
}

/**
 * Check for specific rights on a table for the current user in the session
 *
 * @param int $TID table id
 * @param array $rights array containing the rights to check <ul>
 * <li>i = insert
 * <li>u = update
 * <li>d = delete
 * <li>p = view
 * <li>c = copy
 * <li>n = notify
 * <li>x = export
 * <li>m = import
 * <li>b = publish</ul>
 * @return array the rights that were actually found
 */
static public function getAccessRights($TID, $rights)
{
	global $db, $PHSes;
	/* Removed 2010-05-27: Should return actual role rights?
	if($PHSes->isAdmin) {
		return $rights;
	}
	*/

	if (!$PHSes || !$PHSes->groupId) {
		return array();
	}
	$sql = sprintf("
		SELECT rights
		FROM phoundry_group_table
		WHERE group_id = %d
		AND table_id = %d",
		$PHSes->groupId,
		$TID
	);

	$filter = array();
	$cur = $db->Query($sql) or
		trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if(!$db->EndOfResult($cur)) {
		$row = str_split($db->FetchResult($cur, 0, 'rights'));
		$filter = array_merge($filter, $row);
	}
	$db->FreeResult($cur);

	// compare the wanted rights against the found rights
	return array_intersect($rights, $filter);
}

static public function evaluate($_str)
{
	global $db, $PHSes, $PHenv, $PHprefs, $DMDprefs, $RID, $TID;

	if ($_str === '') return '';

	if (preg_match('/(<\?php|<\?)/', $_str)) {
		ob_start();
		eval(' ?>' . $_str . '<?php ');
		$_str = ob_get_clean();
	}

	// Substitute variables starting with PU. with phoundry user fields:
	if (isset($PHSes) && preg_match('`\{\$PU\.([A-z][\w]+)\}|\{\$USER_ID\}|\{\$GROUP_ID\}|\{\$USER_NAME\}|\{\$USER_EMAIL\}`', $_str)) {
		// Backwards compatibility:
		$_str = str_replace(array('{$USER_ID}', '{$USER_NAME}', '{$USER_EMAIL}'), array('{$PU.id}', '{$PU.username}', '{$PU.e_mail}'), $_str);
		// Substitute {$GROUP_ID} with the group-id of the current user:
		$_str = str_replace('{$GROUP_ID}', $PHSes->groupId, $_str);

		$_sql = "SELECT * FROM phoundry_user WHERE id = {$PHSes->userId}";
		$_cur = $db->Query($_sql)
			or trigger_error("Query $_sql failed: " . $db->Error(), E_USER_ERROR);
		$db->FetchResultAssoc($_cur, $_fields, 0, true);

		$_str = preg_replace_callback(
			'`\{\$PU\.([A-z][\w]+)\}`',
			function($matches) use ($_fields) {
				return (array_key_exists($matches[1], $_fields) ? $_fields[(string)$matches[1]] : $matches[0]);
			},
			$_str
		);

		unset($_sql, $_cur, $_fields);
	}

	// Substitute {$CAMPAIGN_ID} with the currently selected campaign-id
	// (DMdelivery only)
	if (isset($PHenv['DMDcid'])) {
		$_str = str_replace('{$CAMPAIGN_ID}', $PHenv['DMDcid'], $_str);
	}

	// When there's a variable $RECORD_ID, it is only available in $GLOBALS:
	if (isset($GLOBALS['RECORD_ID'])) {
		$_str = str_replace('{$RECORD_ID}', $GLOBALS['RECORD_ID'], $_str);
	}

	// Substitute $word(x,...) vars:
	$_str = preg_replace_callback('/\{\$word\(([^\)]+)\)\}/i', function($matches) { return word($matches[1]); }, $_str);

	// Substitute {$nocheck} and {$check} with images:
	$_path = '';
	if (isset($_SERVER) && isset($_SERVER['REQUEST_URI']) && isset($_SERVER['HTTP_HOST'])) {
		// Use absolute URL's if at all possible, so that DMD notification emails show them properly
		$_path = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
	}

	$_str = str_replace('{$check}', '<span class="icon check-icon"></span>', $_str);
	$_str = str_replace('{$nocheck}', '<span class="icon nocheck-icon"></span>', $_str);

	// Substitute vars with SESSION vars:
	// This can be used in combination with the postLogin-function to filter
	// for specific records.
	$_str = preg_replace_callback('/\{\$([a-z_][\w]+)\}/i', function($matches) { return isset($_SESSION[$matches[1]]) ? $_SESSION[$matches[1]] : (isset($_GET[$matches[1]]) ? $_GET[$matches[1]] : (isset($_POST[$matches[1]]) ? $_POST[$matches[1]] : $matches[1])); }, $_str);
	// Substitute vars with REQUEST vars:
	$_str = preg_replace_callback('/\{\$([a-z_][\w]+)\}/i', function($matches) { return isset($_REQUEST[$matches[1]]) ? $_REQUEST[$matches[1]] : $matches[1]; }, $_str);

	return $_str;
}

/**
 * Retrieve real IP address of client, proxy safe.
 */
static public function getRemoteAddr()
{
	if (getenv('HTTP_X_FORWARDED_FOR') !== false) {
		$ip = getenv('HTTP_X_FORWARDED_FOR');
		if (strpos($ip, ',') !== false) {
			$ip = explode(',', $ip);
			$ip = trim($ip[0]);
		}
		return $ip;
	}
	if (getenv('HTTP_X_FORWARDED') !== false) {
		return getenv('HTTP_X_FORWARDED');
	}
	if (getenv('HTTP_FORWARDED_FOR') !== false) {
		return getenv('HTTP_FORWARDED_FOR');
	}
	if (getenv('HTTP_FORWARDED') !== false) {
		return getenv('HTTP_FORWARDED');
	}
	if (isset($_SERVER['REMOTE_ADDR'])) {
		return $_SERVER['REMOTE_ADDR'];
	}
	return false;
}

/**
 * Does IP address $ip fall into range $ranges?
 *
 * @author Arjan Haverkamp
 * @param string $ip The ip-address to check access for.
 * @param array $ranges An array of IP-ranges the $ip should be in.
 * @return bool
 */
static public function checkIPaccess($ip, $ranges)
{
	foreach($ranges as $range) {
		if (preg_match('/^' . preg_quote(trim($range)) . '/', trim($ip))) {
			return true;
		}
	}
	return false;
}

/**
 * Check for a valid Phoundry product-key.
 * The product-key is encoded using the Web Power encoding.
 * It's plain format is: domains|license|#users
 * Example: webpower.nl,webpower.com|S|20
 * 	An Silver license for 20 users, may be accessed on host *.webpower.nl
 * 	and *.webpower.com.
 * Example: www.phoundry.com|B|5
 * 	A Bronze license for 5 users, may be accessed on host www.phoundry.com
 *
 * @author Arjan Haverkamp
 * @param string $h_prodKey The product-key to validate.
 * @return bool true if product key is valid, false if not.
 */
static public function validProductKey($h_prodKey)
{
	$server = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : preg_replace('/(:[0-9]+)?$/', '', $_SERVER['HTTP_HOST']);

	$decoded = LC::WPdecode($h_prodKey);
	if ($decoded === false) return false;
	$decKey = explode('|', $decoded);
	if (strpos('BSG', $decKey[1]) !== false && preg_match('/^[0-9]+$/', $decKey[2])) {
		foreach (explode(',', $decKey[0]) as $PHdomain)
			if (strncmp(strrev(strtolower($PHdomain)), strrev(strtolower($server)), strlen($PHdomain)) === 0)
			return true;
	}
	return false;
}

/**
 * Include a class.
 * This function is used for including datatypes, inputtypes and cross-reference types.
 *
 * @author Arjan Haverkamp
 * @param string $class The file- & class-name of the class to include.
 * @param string $dir The directory where to search for the class.
 */
static public function includeClass($class, $dir)
{
	global $PHprefs;

	if (!class_exists($class) && file_exists("{$PHprefs['distDir']}/core/include/../$dir/$class.php")) {
		include "{$PHprefs['distDir']}/core/include/../$dir/$class.php";
	}
	elseif (!class_exists($class) && file_exists("{$PHprefs['distDir']}/dmdelivery/$dir/$class.php")) {
		include "{$PHprefs['distDir']}/dmdelivery/$dir/$class.php";
	}
	elseif (!class_exists($class) && file_exists("{$PHprefs['instDir']}/custom/$dir/$class.php")) {
		include "{$PHprefs['instDir']}/custom/$dir/$class.php";
	}
	elseif (!class_exists($class))
		trigger_error("Class <b>$class</b> could not be found in directory $dir!", E_USER_ERROR);
}

static public function ubbreplace($var)
{
	// Strip any HTML tags
	$var = strip_tags($var);

	// Bold, underline, and italic UBB tags
	$var = preg_replace("/\[b\](.+)\[\/b\]/", "<b>\\1</b>", $var);
	$var = preg_replace("/\[u\](.+)\[\/u\]/", "<u>\\1</u>", $var);
	$var = preg_replace("/\[i\](.+)\[\/i\]/", "<i>\\1</i>", $var);

	// UBB Url tags
	$var = preg_replace("/\[url=(.+)\](.+)\[\/url\]/", "<a href=\"\\1\" target=\"note\">\\2</a>", $var);

	// Email Addresses
	$var = preg_replace("/(([a-zA-Z0-9_-]+)@([a-zA-Z0-9_-]+)([\.a-zA-Z0-9_-]+))/", "<a href=\"mailto:\\2@\\3\\4\">\\1</a>", $var);

	return $var;
}

/**
 * Check if a user created a shortcut to the given interface
 *
 * @author Christiaan Baartse
 * @param int $tid
 * @param int $user_id
 * @return bool
 */
static public function isShortcut($tid, $user_id = null)
{
	global $PHSes, $db;

	if(null === $user_id) $user_id = $PHSes->userId;

	$sql = sprintf("
		SELECT COUNT(*) as cnt
		FROM phoundry_user_shortcut
		WHERE user_id = %d AND table_id = %d
		",
		$user_id,
		$tid
	);
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	$retVal = (int)$db->FetchResult($cur,0,0) > 0;
	$db->FreeResult($cur);
	return $retVal;
}

/**
 * Get a table TID by a table's string id
 *
 * @param string $string_id
 * @return int|bool either the id or false if not found
 */
static public function getTableIdByStringId($string_id)
{
	global $db;

	$sql = sprintf("
		SELECT id
		FROM phoundry_table
		WHERE string_id = '%s'",
		mysql_real_escape_string($string_id)
	);

	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	$retVal = !$db->EndOfResult($cur) ? $db->FetchResult($cur,0,0) : false;
	$db->FreeResult($cur);

	return $retVal;
}

/**
 * Get a table string id by a table's id
 *
 * @param int $tid
 * @return string|bool either the string_id or false if not found
 */
static public function getTableStringIdById($tid)
{
	global $db;

	$sql = sprintf("
		SELECT string_id
		FROM phoundry_table
		WHERE id = %d",
		$tid
	);

	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	$retVal = !$db->EndOfResult($cur) ? $db->FetchResult($cur,0,0) : false;
	$db->FreeResult($cur);
	return $retVal;
}

/**
 * Get HTML representing the Notes for a table.
 *
 * @author Arjan Haverkamp
 * @param int $tableId The table-id of the table to get the notes for.
 * @param int $userId The user-id of the user that views the notes.
 * @return array
 *         'count': The number of notes
 *         'html': String containing HTML for the Notes.
 */
static public function getNotes($tableId, $userId)
{
	global $PHprefs, $PHenv, $db;

	if (!isset($PHprefs['nrNotes']) || $PHprefs['nrNotes'] < 1)
	{
		return array('count'=>0, 'html'=>'');
	}

	// Delete expired notes:
	$sql = "DELETE FROM phoundry_note WHERE till_date < NOW()";
	$db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	$notes = array();

	/*
	 For DMDdelivery, the notes are campaign dependent. The areas 'brand' (8),
	 'campaign' (9) and 'forward addresses' (11) are campaign-independent, so
	 this is when we fall back to the default behaviour.
	*/
	if (isset($PHenv['DMDcid']) && !in_array($tableId, array(8,9,11))) {
		$sql = "SELECT pn.*, pu.name AS username FROM phoundry_note pn, phoundry_user pu WHERE pn.campaign_id = {$PHenv['DMDcid']} AND pn.table_id = " . (int)$tableId . " AND pn.user_id = pu.id ORDER BY pn.id";
	}
	else {
		$sql = "SELECT pn.*, pu.name AS username FROM phoundry_note pn, phoundry_user pu WHERE pn.table_id = " . (int)$tableId . " AND pn.user_id = pu.id ORDER BY pn.id";
	}
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$db->FetchResultAssoc($cur,$note,$x);
		$notes[$db->FetchResult($cur,$x,'id')] = $note;
	}
	$db->FreeResult($cur);

	$html = '';
	$x = 0;
	foreach($notes as $id=>$note)
	{
		$html .= '<dl class="note">';
		$html .= '<dt>' . LC::htmlspecialchars($note['title']) . '</dt><dd>';
		$html .= nl2br(LC::ubbreplace(strip_tags($note['contents'])));
		$html .= '<br />';
		if ($userId == $note['user_id'])
		{
			$html .= '<span class="ptr icon edit-note" onclick="editNote(' . $id . ');return false"></span>';
			$html .= ' ';
			$html .= '<span class="ptr icon del-note" onclick="delNote(this,' . $id . ');return false"></span>';
			$html .= ' | ';
		}
		$html .= '<strong>' . $note['username'] . '</strong>';
		$html .= '</dd>';
		$html .= "</dl>\n";
		$x++;
	}

    if ($html) {
        $html = '<div id="notesPane">
    <div id="notesGrippie"></div>
    <div class="notes">
        ' . $html . '
    </div>
</div>';
    }
	return array('count'=>count($notes), 'html'=>$html);
}


/**
 * Append a message to the Phoundry logfile ($PHprefs['logFile']).
 *
 * @author Arjan Haverkamp
 * @param string $type Type of message. Either 'LOGIN', 'QUERY' or 'NOTICE'
 * @param string $message The message to log.
 * @param string $status Optional status (ex: 'OK' or 'ERROR')
 */
static public function logPhoundry($type, $message, $status = null)
{
	global $PHprefs, $db, $PHSes;

	if (!isset($PHprefs['logFile']) || empty($PHprefs['logFile'])) {
		return;
	}

	$logmsg = ''; $log = false;
	if (!file_exists($PHprefs['logFile'])) {
		$log = fopen($PHprefs['logFile'], 'w');
		if (!$log)
			trigger_error(word(2, $PHprefs['logFile']), E_USER_ERROR);
		else
			$logmsg = "DATE/TIME\tUSER\tIP_ADDRESS\tTYPE\tMESSAGE\nSTATUS\n";
	}
	else
		$log = fopen($PHprefs['logFile'], 'a');
	if ($log) {
		$logmsg .= date('Y-m-d H:i:s');
		$logmsg .= "\t" . (isset($PHSes) ? $PHSes->userName : '???');
		$logmsg .= "\t" . LC::getRemoteAddr();
		$logmsg .= "\t" . $type;
		$logmsg .= "\t" . str_replace(array("\n", "\t", "\r"), array(' ', ' ', ''), $message);
		if (null !== $status) {
			$logmsg .= "\t" . $status;
		}
		$logmsg .= "\n";
		fwrite($log, $logmsg);
		fclose($log);
	}
}

/**
 * Determine the content-type for a file.
 *
 * @author Arjan Haverkamp
 * @param $filename(string) The filename to get the mimetype for.
 * @return string If known, the content-type for the file, 'application/octet-stream'
 *         if unknown.
 */
static public function getContentType($filename)
{

	$mimeTypes = array(
		'doc' => 'application/msword',
		'zip' => 'application/zip',
		'xls' => 'application/vnd.ms-excel',
		'ppt' => 'application/vnd.ms-powerpoint',
		'gif' => 'image/gif',
		'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'bmp' => 'image/bmp',
		'png' => 'image/png',
		'tif' => 'image/tiff',
		'tiff' => 'image/tiff',
		'pdf' => 'application/pdf',
		'hqx' => 'application/mac-binhex40',
		'swf' => 'application/x-shockwave-flash',
		'sit' => 'application/x-stuffit',
		'mp3' => 'audio/mpeg',
		'html' => 'text/html',
		'htm' => 'text/html',
		'txt' => 'text/plain',
		'xml' => 'text/xml',
		'eml' => 'message/rfc822',
		'csv' => 'text/csv'
	);

	$extension = strtolower(substr($filename, strrpos($filename, '.')+1));
	return isset($mimeTypes[$extension]) ? $mimeTypes[$extension] : 'application/octet-stream';
}

/*
 * Send an email, in a Phoundry template
 * @author Arjan Haverkamp
 * @param array $rcpts Array of recipient email addresses
 * @param string $subject Subject of email
 * @param string $body Body of e-mail (HTML)
 * @param string[] $attachments Files (path(s)) to attach to message
 * @param string $from Sender's email address
 * @param string $charset Message's character set (iso-8859-1 or utf-8)
 * @param string $css Stylesheet to include for proper message rendering
 * @param array $headers Array of additional mail headers, f.e: array('Reply-To'=>'user@example.com')
 */
static public function sendHTMLemail($rcpts, $subject, $body, $attachments = array(), $from = null, $charset = null, $css = null, $headers = array())
{
	global $PHprefs, $DMDprefs, $PRODUCT;

	$charset = null === $charset ? $PHprefs['charset'] : $charset;

	if (!is_array($rcpts)) $rcpts = array($rcpts);
	if (!class_exists('DMDsmtp')) {
		require_once "{$PHprefs['distDir']}/core/include/DMDsmtp.php";
	}
	if (!class_exists('DMDmime')) {
		require_once "{$PHprefs['distDir']}/core/include/DMDmime.php";
	}

	if (null === $from) { $from = $PHprefs['SMTPfrom']; }
	$mime = new DMDmime();

	$tmpl = isset($PHprefs['customUrls']['emailTmpl']) ?
	        $PHprefs['customUrls']['emailTmpl'] :
			  $PHprefs['distDir'] . '/layout/emailTmpl.html';
	$html = file_get_contents($tmpl);
	$html = str_replace(array('{$charset}','{$title}','{$content}'), array($charset, $subject, $body), $html);
	if ($css) {
		$html = str_replace('</style>', "\n{$css}\n</style>", $html);
	}

	// Create plaintext message:
	$ptxt = preg_replace(
		array('~<(/dd|/div|/fieldset|/p|/li|/tr|hr ?/|br ?/)>~i',
				'~<(/td|/th)>~i',
				'~<(/dt)>~i'),
		array("\n",
				' ',
				': '
		),
		$body
	);
    $ptxt = preg_replace_callback(
        '~<h([1-6])([^>]*)>(.*)</h\\1>~i',
        function($matches) {
            return strtoupper($matches[3]) . "\n";
        },
        $ptxt
    );
	$ptxt = trim(preg_replace('~ +~s',' ',str_replace(array('&lt;','&gt;','&quot;','&amp;'), array('<', '>', '"', '&'), strip_tags($ptxt))));

	$mime->setTXTBody($ptxt);
	$mime->setHTMLBody($html);
	if ($attachments) {
		if (is_string($attachments)) {
			$attachments = array($attachments);
		}
		foreach($attachments as $attachment) {
			$mime->addAttachment($attachment, LC::getContentType($attachment), basename($attachment), true, 'base64');
		}
	}
	$mimeBody = $mime->get(array(
		'text_encoding' => 'quoted-printable',
		'html_encoding' => 'quoted-printable',
		'head_charset'  => $charset,
		'text_charset'  => $charset,
		'html_charset'  => $charset
	));

	$smtp = new DMDsmtp();

	$SMTPserver = isset($DMDprefs['pluginSMTPserver']) ? $DMDprefs['pluginSMTPserver'] : (is_array($PHprefs['SMTPserver']) ? $PHprefs['SMTPserver'][0] : $PHprefs['SMTPserver']);
	$SMTPport   = isset($DMDprefs['pluginSMTPport']) ? $DMDprefs['pluginSMTPport'] : (isset($PHprefs['SMTPport']) ? $PHprefs['SMTPport'] : 25);
	$SMTPhelo   = isset($PHprefs['SMTPhelo']) ? $PHprefs['SMTPhelo'] : php_uname('n');
	$smtp->connect(array('host'=>$SMTPserver, 'port'=>$SMTPport, 'helo'=>$SMTPhelo));

	$hdrs = array(
		'X-Mailer'  => $PRODUCT['name'] . ' ' . $PRODUCT['version'],
		'From'      => $from,
		'Subject'   => $subject
	);

	// Add custom headers:
	foreach($headers as $name=>$value) {
		$hdrs[$name] = trim($value);
	}

	foreach($rcpts as $rcpt) {
		$rcpt = trim($rcpt);
		$hdrs['To'] = $rcpt;
		//$smtp->send($PHprefs['SMTPfrom'], $rcpt, $mime->headers($hdrs), $mimeBody);
		$smtp->send($from, $rcpt, $mime->headers($hdrs), $mimeBody);
	}
}

/**
 * Parse an SQL statement.
 *
 * @author Niek Kerkemeijer/Arjan Haverkamp
 * @param $h_query(string) The SQL-query to parse
 * @param $h_what(string) The part of the query to return. Possible options:
 *								'select':	return the SELECT part
 *								'from':		return the FROM part
 *								'where':		return the WHERE part
 *								'order_by':	return the ORDER BY part
 *								'group_by':	return the GROUP BY part
 * @return string containing the desired SQL-part.
 */
public static function parseSQL($h_query, $h_what)
{
	switch ($h_what) {
		case 'select':
			$regexp = '/SELECT(.*?)FROM/Uis';
			break;
		case 'from':
			$regexp = '/FROM(.*)(LIMIT|\[?WHERE|ORDER BY|GROUP BY|$)/Uis';
			break;
		case 'where':
			$regexp = '/WHERE(.*)(LIMIT|ORDER BY|GROUP BY|$)/Uis';
			break;
		case 'order_by':
			$regexp = '/ORDER BY(.*)(LIMIT|$)/Uis';
			break;
		case 'group_by':
			$regexp = '/GROUP BY(.*)(ORDER BY|LIMIT|$)/Uis';
			break;
	}

	if (preg_match_all($regexp, $h_query, $l_regs, PREG_SET_ORDER))
		return trim($l_regs[0][1]);
	else
		return '';
}

/**
 * Output the results of an SQL query as CSV.
 * (Uses direct mysql functions for ultimate performance)
 *
 * @author Arjan Haverkamp
 * @param string $sql The query to execute
 * @param bool $showHeader Whether or not to show fieldnames on first line
 * @param string $fieldSep The field separator
 * @param string $textDelim The text delimiter
 * @param string $charset The characterset to save the CSV in. By default, the CSV will
 *                         be generated in the $PHprefs['charset']
 */
static public function CSVexport($sql, $showHeader = true, $fieldSep = ';', $textDelim = '"', $charset = null, $fp = null)
{
	global $db, $PHprefs;

	$convert = null !== $charset && $charset != $PHprefs['charset'];
	if (null === $fp) {
		$fp = fopen('php://output', 'w');
	}

	$cur = mysql_query($sql)
		or trigger_error("Query $sql failed: " . mysql_error(), E_USER_ERROR);

	$nrf = mysql_num_fields($cur);
	$header = '';
	if ($showHeader) {
		for ($x = 0; $x < $nrf; $x++) {
			if ($x > 0) { $header .= $fieldSep; }
			$header .= mysql_field_name($cur, $x);
		}
		$header .= "\r\n";
	}

	fwrite($fp, ($convert) ? LC::iconv($PHprefs['charset'], $charset, $header) : $header);

	$chunk = '';
	$y = 0;
	while($row = mysql_fetch_row($cur)) {
		for ($x = 0; $x < $nrf; $x++) {
			if ($x > 0) { $chunk .= $fieldSep; }
			$cnt = str_replace(array("\r", "\n", $textDelim), array('', ' ', $textDelim . $textDelim), $row[$x]);
			//if (strpos($cnt, $textDelim) || strpos($cnt, $fieldSep))
				$chunk .= $textDelim . $cnt . $textDelim;
			//else
			//	$chunk .= $cnt;
		}
		$chunk .= "\r\n";
		$y++;

		if ($y == 1000) {
			// Output chunk:
			fwrite($fp, ($convert) ? LC::iconv($PHprefs['charset'], $charset, $chunk) : $chunk);
			$y = 0; $chunk = '';
		}
	}
	mysql_free_result($cur);

	if (strlen($chunk)) {
		fwrite($fp, ($convert) ? LC::iconv($PHprefs['charset'], $charset, $chunk) : $chunk);
	}
	flush();
	fclose($fp);
}

/**
 * Escape a string, so that it becomes XML ISO-8859-1 compatible.
 *
 * @author Arjan Haverkamp
 * @param $str(string) The string to escape
 * @return string The escaped string
 */
static public function XMLentities($str, $charset = 'iso-8859-1')
{
	switch($charset) {
		case 'iso-8859-1':
			return str_replace('�', '&#8364;',
                preg_replace_callback(
                    '/([\x81-\x95]|[\x99-\xFF])/',
                    function($m) {
                        return "&#" . ord($m[1]) . ";";
                    },
                    LC::htmlspecialchars($str, ENT_COMPAT, 'ISO-8859-1')
                )
            );
		default:
			return LC::htmlspecialchars($str, ENT_COMPAT, strtoupper($charset));
	}
}

static public function getFilter($filters, $prefix = '')
{
	global $PHSes, $PHprefs;

	$as = $bs = $keys = $ands = array();

	foreach($filters as $idx=>$filter) {
		$value = $filter['value'];
		$fieldName = $prefix . $filter['fieldname'];
		$operator = $filter['operator'];
		$where = $fieldName . ' ';
		if ($value == 'NULL' && $operator == '=')
			$operator = 'IS';
		elseif ($value == 'NULL' && $operator == '!=')
			$operator = 'IS NOT';
		$where .= $operator . ' ';
		if ($value == 'NULL')
			$where .= 'NULL';
		else {
			if ($operator == 'like' || $operator == 'not like') {
				$value = str_replace(array('*','?'), array('%','_'), $value);
			}

			if ($operator == 'in' || $operator == 'not in') {
				if (preg_match('/^\(?select/i', $value)) {
					$where .= LC::evaluate($value);
				}
				else {
					$isAllNum = true;
					$vs = explode(',', $value);
					foreach($vs as $v) {
						if (!is_numeric($v)) {
							$isAllNum = false; break;
						}
					}
					if (!$isAllNum && !preg_match('/^\{\$/', $value)) {
						$value = '';
						foreach($vs as $v) {
							if ($value != '') $value .= ', ';
							$value .= "'" . escDBquote($v) . "'";
						}
					}
					$where .= '(' . LC::evaluate($value) . ')';
				}
			}
			elseif($value == 'NOW()' && $PHprefs['DBtype'] == 'mysql') {
				$where .= "NOW()";
			}
			elseif (strpos($value, '<?php') === false) {
				$where .= "'" . escDBquote(LC::evaluate($value)) . "'";
			}
			else {
				$where .= LC::evaluate($value);
			}
		}
		preg_match('/^([0-9]+)([ab])$/', $idx, $reg);
		if ($reg[2] == 'a')
			$as[$reg[1]] = $where;
		else
			$bs[$reg[1]] = $where;
		$keys[] = $reg[1];
	}
	$keys = array_unique($keys);
	foreach ($keys as $x) {
		$where = '(';
		if (isset($as[$x])) {
			$where .= $as[$x];
			if (isset($bs[$x]))
				$where .= ' OR ';
		}
		if (isset($bs[$x]))
			$where .= $bs[$x];
		$where .= ')';
		$ands[] = $where;
	}
	return $ands;
}

/**
 * Convert a PHP datastructure to a Javascript datastructure.
 *
 * @author Arjan Haverkamp
 * @param $struct(mixed) The PHP-structure to convert.
 * @return string A Javascript-compatible string.
 */
static public function php2javascript($struct)
{
	if (function_exists('json_encode') && version_compare(PHP_VERSION, '5.2.3') === 1) {
		//return json_encode($struct);
		//return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));
		return preg_replace_callback("/\\\\u([a-f0-9]{4})/", function($matches) { return iconv('UCS-4LE','UTF-8',pack('V', hexdec('U'. $matches[1]))); }, json_encode($struct));
	}

	// PHP < 5.3.3 has a bug with encoding empty array keys... See here:
	// http://bugs.php.net/bug.php?id=41504

	if (null === $struct) return 'null';
	if ($struct === false) return 'false';
	if ($struct === true) return 'true';
	if (is_scalar($struct))
	{
		if (is_float($struct))
 		{
			// Always use "." for floats.
			return floatval(str_replace(',', '.', strval($struct)));
		}

		if (is_string($struct))
		{
			static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
			return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $struct) . '"';
		}
		else
			return $struct;
	}
	$isList = true;
	for ($i = 0, reset($struct); $i < count($struct); $i++, next($struct))
	{
		if (key($struct) !== $i)
		{
			$isList = false;
			break;
		}
	}
	$result = array();
	if ($isList)
	{
		foreach ($struct as $v) $result[] = LC::php2javascript($v);
		return '[' . join(',', $result) . ']';
	}
	else
	{
		foreach ($struct as $k => $v) $result[] = LC::php2javascript((string)$k).':'.LC::php2javascript($v);
		return '{' . join(',', $result) . '}';
	}
}

/**
 * Convert a JS datastructure to a PHP datastructure.
 * 'json_decode' is very "sensitive", and does not parse single/double quotes correctly.
 * Hence this more robust solution.
 *
 * @author Arjan Haverkamp
 * @param $str(string) A javascript object-as-sting ({a:1,b:true,c:[1,2]})
 * @return mixed A PHP datastructure
 */
static public function javascript2php($str)
{
	$json_slice = 1;
	$json_in_str = 2;
	$json_in_arr = 3;
	$json_in_obj = 4;
	$json_in_cmt = 5;

	switch (strtolower($str)) {
		case 'true':
			return true;

		case 'false':
			return false;

		case 'null':
			return null;

		default:
			$m = array();
			if (is_numeric($str)) {
				return ((float)$str == (integer)$str) ? (integer)$str : (float)$str;
			} elseif (preg_match('/^("|\').*(\1)$/s', $str, $m) && $m[1] == $m[2]) {
				// STRINGS RETURNED IN UTF-8 FORMAT
				$delim = substr($str, 0, 1);
				$chrs = substr($str, 1, -1);
				$utf8 = '';
				$strlen_chrs = strlen($chrs);

				for ($c = 0; $c < $strlen_chrs; ++$c) {

					$substr_chrs_c_2 = substr($chrs, $c, 2);
					$ord_chrs_c = ord($chrs[$c]);

					switch (true) {
						case $substr_chrs_c_2 == '\b':
							$utf8 .= chr(0x08);
							++$c;
							break;
						case $substr_chrs_c_2 == '\t':
							$utf8 .= chr(0x09);
							++$c;
							break;
						case $substr_chrs_c_2 == '\n':
							$utf8 .= chr(0x0A);
							++$c;
							break;
						case $substr_chrs_c_2 == '\f':
							$utf8 .= chr(0x0C);
							++$c;
							break;
						case $substr_chrs_c_2 == '\r':
							$utf8 .= chr(0x0D);
							++$c;
							break;
						case $substr_chrs_c_2 == '\\"':
						case $substr_chrs_c_2 == '\\\'':
						case $substr_chrs_c_2 == '\\\\':
						case $substr_chrs_c_2 == '\\/':
							if (($delim == '"' && $substr_chrs_c_2 != '\\\'') || ($delim == "'" && $substr_chrs_c_2 != '\\"')) {
								$utf8 .= $chrs[++$c];
							}
							break;
						case ($ord_chrs_c >= 0x20) && ($ord_chrs_c <= 0x7F):
							$utf8 .= $chrs[$c];
							break;
						case ($ord_chrs_c & 0xE0) == 0xC0:
							$utf8 .= substr($chrs, $c, 2);
							++$c;
							break;
						case ($ord_chrs_c & 0xF0) == 0xE0:
							$utf8 .= substr($chrs, $c, 3);
							$c += 2;
							break;
						case ($ord_chrs_c & 0xF8) == 0xF0:
							$utf8 .= substr($chrs, $c, 4);
							$c += 3;
							break;
						case ($ord_chrs_c & 0xFC) == 0xF8:
							$utf8 .= substr($chrs, $c, 5);
							$c += 4;
							break;
						case ($ord_chrs_c & 0xFE) == 0xFC:
							$utf8 .= substr($chrs, $c, 6);
							$c += 5;
						break;
					}
				}
				return $utf8;
			} elseif (preg_match('/^\[.*\]$/s', $str) || preg_match('/^\{.*\}$/s', $str)) {
				if ($str[0] == '[') {
					$stk = array($json_in_arr);
					$arr = array();
				} else {
					$stk = array($json_in_obj);
					$obj = array();
				}

				array_push($stk, array('what'	=> $json_slice, 'where' => 0, 'delim' => false));

				$chrs = substr($str, 1, -1);

				if ($chrs == '') {
					if (reset($stk) == $json_in_arr) {
						return $arr;
					} else {
						return $obj;
					}
				}

				$strlen_chrs = strlen($chrs);

				for ($c = 0; $c <= $strlen_chrs; ++$c) {

					$top = end($stk);
					$substr_chrs_c_2 = substr($chrs, $c, 2);

					if (($c == $strlen_chrs) || (($chrs[$c] == ',') && ($top['what'] == $json_slice))) {
						$slice = substr($chrs, $top['where'], ($c - $top['where']));
						array_push($stk, array('what' => $json_slice, 'where' => ($c + 1), 'delim' => false));
						if (reset($stk) == $json_in_arr) {
							array_push($arr, LC::javascript2php($slice));
						} elseif (reset($stk) == $json_in_obj) {
							$parts = array();
							if (preg_match('/^\s*(["\'].*[^\\\]["\'])\s*:\s*(\S.*),?$/Uis', $slice, $parts)) {
								$key = LC::javascript2php($parts[1]);
								$val = LC::javascript2php($parts[2]);
								$obj[$key] = $val;
							} elseif (preg_match('/^\s*(\w+)\s*:\s*(\S.*),?$/Uis', $slice, $parts)) {
								$key = $parts[1];
								$val = LC::javascript2php($parts[2]);
								$obj[$key] = $val;
							}
						}
					} elseif ((($chrs[$c] == '"') || ($chrs[$c] == "'")) && ($top['what'] != $json_in_str)) {
						array_push($stk, array('what' => $json_in_str, 'where' => $c, 'delim' => $chrs[$c]));
					} elseif (($chrs[$c] == $top['delim']) && ($top['what'] == $json_in_str) && ((strlen(substr($chrs, 0, $c)) - strlen(rtrim(substr($chrs, 0, $c), '\\'))) % 2 != 1)) {
						array_pop($stk);
					} elseif (($chrs[$c] == '[') && in_array($top['what'], array($json_slice, $json_in_arr, $json_in_obj))) {
						array_push($stk, array('what' => $json_in_arr, 'where' => $c, 'delim' => false));
					} elseif (($chrs[$c] == ']') && ($top['what'] == $json_in_arr)) {
						array_pop($stk);
					} elseif (($chrs[$c] == '{') && in_array($top['what'], array($json_slice, $json_in_arr, $json_in_obj))) {
						array_push($stk, array('what' => $json_in_obj, 'where' => $c, 'delim' => false));
					} elseif (($chrs[$c] == '}') && ($top['what'] == $json_in_obj)) {
						array_pop($stk);
					} elseif (($substr_chrs_c_2 == '/*') && in_array($top['what'], array($json_slice, $json_in_arr, $json_in_obj))) {
						array_push($stk, array('what' => $json_in_cmt, 'where' => $c, 'delim' => false));
						$c++;
					} elseif (($substr_chrs_c_2 == '*/') && ($top['what'] == $json_in_cmt)) {
						array_pop($stk);
						$c++;
						for ($i = $top['where']; $i <= $c; ++$i)
							$chrs = substr_replace($chrs, ' ', $i, 1);
					}
				}

				if (reset($stk) == $json_in_arr) {
					return $arr;
				} elseif (reset($stk) == $json_in_obj) {
					return $obj;
				}
			}
	}
}

static public function utf8rawurlencode($str)
{
	global $PHprefs;
	if ($PHprefs['charset'] == 'utf-8') {
		return rawurlencode($str);
	}
	else {
		return rawurlencode(utf8_encode($str));
	}
}

static public function urlencodeURL($path)
{
	return implode('/', array_map(array('LC','utf8rawurlencode'), explode('/', $path)));
}

/**
 * Recursively remove a directory and all files/dirs in it.
 *
 * @author Arjan Haverkamp
 * @param string $dirName	The path of the directory to remove.
 */
static public function RmDirR($dirName)
{
	if (is_dir($dirName) && $d = @opendir($dirName)) {
		while($entry = readdir($d)) {
			if ($entry != '.' && $entry != '..' && $entry != '') {
				if (is_dir($dirName.'/'.$entry)) {
					LC::RmDirR($dirName.'/'.$entry);
				} else {
					@unlink($dirName.'/'.$entry);
				}
			}
		}
		closedir($d);
	}
	@rmdir($dirName);
}

/**
 * Determine the first 'free' id in table $h_table, column $h_column, in a
 * database-independent manner.
 *
 * @author Arjan Haverkamp
 * @param string $h_table The table to determine the id for.
 * @param string $h_column The column to determine the id for.
 * @return int The first free value (integer) to use as a key-value.
 */
static public function determineNewKeyVal($h_table, $h_column)
{
	global $db;
	// Determine new key-value (DB-independent):
	$newKey = 1;
	$sql = "SELECT MAX(" . escDBquote($h_column) . ")+1 AS new_key FROM " . escDBquote($h_table);
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->ResultIsNull($cur,0,'new_key')) {
		$newKey = $db->FetchResult($cur, 0, 'new_key');
	}
	if ($newKey == '') { $newKey = 1; }
	$db->FreeResult($cur);
	return $newKey;
}

/*
static public function setUserPassword($userID)
{
	global $db;
	$sql = "SELECT password FROM phoundry_user WHERE id = {$userID} LIMIT 1";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if ($db->EndOfResult($cur)) { return; }
	$newPassword = sha1($userID . self::$pwdSalt . $db->FetchResult($cur,0,'password'));
	$sql = "UPDATE phoundry_user SET password = '{$newPassword}' WHERE id = " . (int)$userID;
	$db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
}
*/

/**
 * Print an error message.
 *
 * @author Arjan Haverkamp
 * @param string $msg The error-message to print.
 * @param bool $exitAfter true, if you want to exit after, false if not.
 * @param string $qs The query-string - used for managing record locks.
 */
static public function phoundryErrorPrinter($msg, $exitAfter = false, $qs = null)
{
	global $PHprefs, $PHSes;

	$head = word(127 /* Error */);
	if (null === $head) { $head = 'Error'; }
	$back = word(45 /* Back */);
	if (null === $back) { $back = 'Back'; }

	if (headers_sent()) {
		print '<div class="error" style="position:absolute;top:100px;left:10px;z-index:400">' . $msg . '</div>';
	}
	else {
		print '<html><head>';
		print '<meta http-equiv="content-type" content="text/html; charset=' . $PHprefs['charset'] . '" />';
		print '<title>Error</title>';
		print '<link rel="stylesheet" href="' . $PHprefs['customUrls']['css'] . '/phoundry.css" type="text/css" />';
		print '<!--[if IE]><link rel="stylesheet" href="' . $PHprefs['customUrls']['css'] . '/msie.css" type="text/css" media="screen" /><![endif]-->';
		if ($PHprefs['useLocks'] && null !== $qs) {
			print "<script type=\"text/javascript\">\n//<![CDATA[\n";
			print "top.doLock(true,'{$qs}');\n";
			print "window.onunload=function(){top.doLock(false,'{$qs}')}\n";
			print "//]]>\n</script>\n";
		}
		print '</head><body class="frames">' .
		'<div id="headerFrame" class="headerFrame">' .
		getHeader($head) .
		'<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr><td>&nbsp;</td></tr></table>' .
		'</div>' .
		'<div id="contentFrame" class="contentFrame">' .
		'<div class="error">' . $msg . '</div>' .
		'</div>' .
		'<div id="footerFrame" class="footerFrame">' .
		'<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>' .
		'<td></td>' .
		'<td align="right"><button type="button" onclick="if(parent.popup){parent.killPopup()}else{history.back()}">' . $back . '</button></td>' .
		'</tr></table>' .
		'</div>' .
		'</body></html>';
	}
	if ($exitAfter) {
		exit(99);
	}
}

static public function filter2where($json, $prefix = '')
{
	global $PHprefs;
	require_once(dirname(__FILE__) . '/../Inputtypes/filter/FilterConverter.php');
	$converter = new FilterConverter($json, $PHprefs['charset']=='utf-8', $prefix);
	return (!$converter->error)? $converter->where : false;
}

static public function filter2wherePHP($json, $valueReferenceAdaptor, $prefix = '')
{
        global $PHprefs;
	require_once(dirname(__FILE__) . '/../Inputtypes/filter/PHPFilterConverter.php');
	$converter = new PHPFilterConverter($json, $valueReferenceAdaptor, $prefix);
	return (!$converter->error)? $converter->where : false;
}

/**
 * Retrieve a word or sentence in the user's language.
 * $word is a serialized array of format:
 * $word = array('nl'=>'Dit is een zin', 'en'=>'This is a sentence')
 * It will return the right word/sentence for the current user's language
 *
 * @author Arjan Haverkamp
 * @param string|int $word Either a regular string or a serialized array
 * @return string The desired word or sentence.
 */
static public function langWord($word, $lang = null)
{
	global $PHSes, $PHprefs;

	if (null === $lang) { $lang = $PHSes->lang; }

	if (is_int($word) && class_exists('DMD')) {
		$ss = DMD::$langWORDS[isset(DMD::$langWORDS[$lang]) ? $lang : 'en'];
		$ss = isset($ss[$word]) ? $ss[$word] : $word;
		if (null !== $ss) {
			return $PHprefs['charset'] == 'iso-8859-1' && LC::is_utf8($ss) ? utf8_decode($ss) : $ss;
		}
	}
	return (LC::is_serialized($word) && ($tmp = @unserialize($word)) !== false) ? (isset($tmp[$lang]) ? $tmp[$lang] : current($tmp)) : $word;
}

static public function is_serialized($data, $strict = true)
    {
        // if it isn't a string, it isn't serialized.
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace = strpos($data, '}');
            // Either ; or } must exist.
            if (false === $semicolon && false === $brace)
                return false;
            // But neither must be in the first X characters.
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            // or else fall through
            case 'a' :
            case 'O' :
                return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }



/**
 * CRC function that is compatible with both 32 and 64 bit processors
 * (PHP's built-in crc32 function yields different results on 32 & 64 bit processors)
 * @param string $num
 * @return int
 */
static public function real_crc32($num)
{
	$crc = crc32($num);
	if($crc & 0x80000000){
		$crc ^= 0xffffffff;
		$crc += 1;
		$crc = -$crc;
	}
	return $crc;
}

/**
 * Encode a string using the Web Power encoding.
 *
 * @author Arjan Haverkamp
 * @param string $str The string to decode.
 * @param bool $editor
 * @return string A string containing the encoded value of $instr.
 */
static public function WPencode($str, $editor = false)
{
	if ($editor) {
		$str .= chr(255) . LC::real_crc32('u' . $str . 'l');
	}
	else {
		$str .= chr(255) . LC::real_crc32('w' . $str . 'p');
	}
	$ret = '';
	for ($x = 0; $x < strlen($str); $x++) {
		$ret .= bin2hex($str[$x]);
	}
	return strrev($ret);
}

/**
 * Decode a string using the Web Power encoding.
 *
 * @author Arjan Haverkamp
 * @param $instr(string) The string to decode.
 * @return string containing the decoded value of $instr.
 */
static public function WPdecode($instr)
{
	$instr = strrev($instr);
	$orig = '';
	if (!strlen($instr) || strlen($instr) % 2 != 0) {
		return false;
	}
	for ($i = 0; $i < strlen($instr); $i += 2) {
		$orig .= chr(hexdec($instr[$i]) * 16 + hexdec($instr[$i+1]));
	}
	$tmp = explode(chr(255), $orig);
	if (!isset($tmp[1]) || LC::real_crc32('w' . $tmp[0] . 'p') != $tmp[1]) {
		return false;
	}
	return $tmp[0];
}

/**
 * Custom Phoundry error handler.
 *
 * @author Arjan Haverkamp
 * @param int $errno The error number.
 * @param string $errstr The error message.
 * @param string $errfile The name of the file the error occured in.
 * @param int $errline The linenumber within the file the error occured in.
 * @return bool
 */
static public function phoundryErrorHandler($errno, $errstr, $errfile, $errline, $vars)
{
	global $PHprefs, $PHSes;

	static $hasError = false;

	if ((int)PHP_VERSION >= 5 && $errno == E_STRICT) {
		// This happens when PHP5 is used and PHP4-functionality is
		// encountered: we need to continue as if nothing happened.
		return true;
	}

	// Prevent errors triggered by the '@'-construct:
	if (error_reporting() == 0) return;

	$errortype = array(
		E_WARNING => 'Warning',
		E_NOTICE => 'Notice',
		E_USER_ERROR => 'User Error',
		E_USER_WARNING => 'User Warning',
		E_USER_NOTICE => 'User Notice',
		E_STRICT => 'Runtime Notice',
		E_DEPRECATED => 'Deprecated'
	);

	// Log errors to PHP error log:
	$errorID = uniqid('');
	$chunks = str_split("{$errortype[$errno]}|{$errfile} ($errline)|{$errstr}", 486);
	foreach($chunks as $chunk) {
		error_log($errorID . '|' . $chunk);
	}

	if ($errno == E_NOTICE || $errno == E_USER_NOTICE)
	{// Don't bother users with NOTICEs, only log them.
		return false;
	}

	// Create HTML error message, store in session:
	$error = array();
	if (isset($_SERVER['REQUEST_URI'])) {
		$error['URL'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}
	$error['Date/Time'] = date('Y-m-d H:i:s');
	$error['Error type'] = $errortype[$errno];
	$error['Error message'] = $errstr;
	$error['Error file'] = $errfile;
	$error['Error line'] = $errline;
	$error['_GET array'] = print_r($_GET, true);
	$error['_POST array'] = print_r($_POST, true);
	$error['_SERVER array'] = print_r($_SERVER, true);
	$backtrace = array_slice(debug_backtrace(), 1, 10);
	$error['Backtrace'] = print_r($backtrace, true);
   
   // If debug is on, fallback to PHP error handler
   if (!empty($PHprefs['debug'])) {
      return false;
   }
   
	if (!$hasError) {
		// Often, chained errors occur (one error causes another one). We only need to be informed about the first one.
		$hasError = true;
		$_SESSION['error'] = $error;
		print '<div style="position:absolute;z-index:9999;background:#eee;border:2px solid red;text-align:left;font:10pt Arial,Helvetica;margin:2px;padding:7px;-moz-opacity:0.85; filter:alpha(opacity=85);-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px">';
		print '<b style="font-size:11pt">We are sorry to inform you that an error has occured.</b><div style="border:1px dotted #666;padding:3px;margin:2px 0">What should you do?<ul><li>Please try what you were doing again;</li><li>Error occurs again? <a href="' . $PHprefs['url'] . '/core/support.php?type=bug" target="PHcontent" style="color:#0046d5;font-weight:bold">Click here</a> to send the error message to Web Power so they can have a look at it.</li></ul>Our apologies for the inconvenience!</div><small onclick="this.parentNode.style.display=\'none\'" style="cursor:pointer">[click to close box]</small>';
		print '</div>';
	}

	return true; // Don't left PHP default error handler kick in
}

/**
 * Check whether or not a string is an UTF-8 string.
 *
 * @author Arjan Haverkamp
 * @param string $string The string to check
 * @return boolean
 */
static public function is_utf8($string)
{
	return preg_match('%(?:
		[\xC2-\xDF][\x80-\xBF]					# non-overlong 2-byte
		|\xE0[\xA0-\xBF][\x80-\xBF]			# excluding overlongs
		|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}	# straight 3-byte
		|\xED[\x80-\x9F][\x80-\xBF]			# excluding surrogates
		|\xF0[\x90-\xBF][\x80-\xBF]{2}		# planes 1-3
		|[\xF1-\xF3][\x80-\xBF]{3}				# planes 4-15
		|\xF4[\x80-\x8F][\x80-\xBF]{2}		# plane 16
	)+%xs', $string);
}

static public function abbr($string, $len, $abbr = '...')
{
	global $PHprefs;
	$cs = $PHprefs['charset'];
	if (mb_strlen($string, $cs) <= $len) {
		return $string;
	}

	return mb_substr($string, 0, $len - mb_strlen($abbr, $cs), $cs).$abbr;
}

/**
 * Generate a random password.
 *
 * @author Arjan Haverkamp
 * @param int $len The length of the password to generate
 * @returns string
 */
static public function generatePassword($len = 10)
{
	$special = '~`!@#$%^&*()-_=+{}[]\|.,\'";:<>?/';
	$letters = 'abcdefghijklmnopqrstuvwxyz';
	$caps    = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$numbers = '1234567890';
	$password = $caps[mt_rand(0,strlen($caps)-1)];
	for ($x = 0; $x < $len - 2; $x++) {
		$password .= $letters[mt_rand(0,strlen($letters)-1)];
	}
	$password .= $numbers[mt_rand(0,strlen($numbers)-1)];
	$password .= $special[mt_rand(0,strlen($special)-1)];
	return $password;
}

/**
 * Convert a date in arbitrary 'YYYY-MM-DD' format to an ISO date format (YYYY-MM-DD).
 *
 * Example: toISOdate('DD-MM-YYYY', '12-05-1973') -> '1973-05-12'
 * Example: toISOdate('YYYY-DD-MM', '1973-12-05') -> '1973-05-12'
 * Example: toISOdate('DD-MM-YYYY hh:mm', '12-05-1973 09:12') -> '1973-05-12 09:12'
 *
 * @param string $inFormat
 * @param string $date
 * @return string
 */
static public function toISOdate($inFormat, $date) {
	$y = 1970; $m = $d = 1; $h = $i = $s = 0; $hasTime = false;

	if (strpos($inFormat, 'DD') !== false) {
		$d = (int)substr($date,strpos($inFormat, 'DD'),2);
	}
	if (strpos($inFormat, 'MM') !== false) {
		$m = (int)substr($date,strpos($inFormat, 'MM'),2);
	}
	if (strpos($inFormat, 'YYYY') !== false) {
		$y = (int)substr($date,strpos($inFormat, 'YYYY'),4);
	}
	if (strpos($inFormat, 'hh') !== false) {
		$hasTime = true;
		$h = (int)substr($date,strpos($inFormat, 'hh'),2);
	}
	if (strpos($inFormat, 'mm') !== false) {
		$i = (int)substr($date,strpos($inFormat, 'mm'),2);
	}
	/*
	if (strpos($inFormat, 'ss') !== false) {
		$s = (int)substr($date,strpos($inFormat, 'ss'),2);
	}
	*/

	$val = sprintf('%04d-%02d-%02d', $y, $m, $d);
	if ($hasTime) {
		$val .= sprintf(' %02d:%02d:%02d', $h, $i, $s);
	}

	return $val;
}

/**
 * Convert a date in ISO format (YYYY-MM-DD) to an arbitrary date format.
 *
 * Example: fromISOdate('DD-MM-YYYY', '1973-05-12') -> '12-05-1973'
 * Example: fromISOdate('YYYY-DD-MM', '1973-05-12') -> '1973-12-05'
 * Example: fromISOdate('DD-MM-YYYY hh:mm', '1973-05-12 09:12') -> '12-05-1973 09:12'
 * @param string $outFormat
 * @param string $date
 * @return string
 */
static public function fromISOdate($outFormat, $date)
{
	$val = $outFormat;
	if (preg_match('/^(\d{4})-(\d{2})-(\d{2})( (\d{2}):(\d{2})(:(\d{2}))?)?/', $date, $reg)) {
		$val = str_replace(array('YYYY','MM','DD'), array($reg[1],$reg[2],$reg[3]), $val);
		if (strpos($outFormat, 'hh') !== false) {
			$val = str_replace('hh', $reg[5], $val);
		}
		if (strpos($outFormat, 'mm') !== false) {
			$val = str_replace('mm', $reg[6], $val);
		}
		if (strpos($outFormat, 'ss') !== false) {
			$val = str_replace('ss', $reg[8], $val);
		}
	}
	return $val;
}

static public function DOMinnerHTML($node, $charset = null)
{
	if (null === $charset) {
		$charset = $GLOBALS['PHprefs']['charset'];
	}
   $doc = new DOMDocument('1.0', $charset);
   foreach ($node->childNodes as $child) {
      $doc->appendChild($doc->importNode($child, true));
   }
   return $doc->saveHTML();
}

static public function DOMouterHTML($node, $charset = null)
{
	if (null === $charset) {
		$charset = $GLOBALS['PHprefs']['charset'];
	}
   $doc = new DOMDocument('1.0', $charset);
   $doc->appendChild($doc->importNode($node, true));
   return $doc->saveHTML();
}

/**
 * Trigger redirect
 *
 * Redirect to url in a correct way and stop script execution directly after redirect headers.
 * @author Jørgen Teunis <jorgen.teunis@webpower.nl>
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @param string $url
 * @param int $status 301=Moved Permanently, 302=Found, 307=Temporary redirect
 */
static public function trigger_redirect($url, $status = 302)
{
	$status = (int) $status;
	switch($status) {
		case 301: // echt verplaatst
			$msg = 'Moved Permanently';
		    break;
		case 302: // eerste url MOET in de toekomst nog steeds gebruikt worden
			$msg = 'Found';
		    break;
		case 303: // see other, niet ondersteunt voor 1.1
			$msg = 'See other';
		    break;
		case 307:
			$msg = 'Temporary Redirect';
            break;
		default:
			throw new InvalidArgumentException('Status should be either '.
				'301, 302, 303 or 307. '.$status.' given.');
	}
	header('HTTP/1.1 '.$status.' '.$msg, true, $status);
	header('URI: '.$url);
	header('Location: '.$url);
	print $msg.":\n".$url."\n";
	exit;
}

static public function startCSRF()
{
	$name = 'csrf' . mt_rand(0,mt_getrandmax());
	$token = uniqid('csrf');
	if (isset($_SESSION)) { $_SESSION[$name] = $token; }
	return array($name, $token);
}

/* PHP 5.4 compatible function */
static public function htmlspecialchars($value, $flags = null, $encoding = null)
{
	global $PHprefs;

	/* check if PHP version is less then 5.4 */
	$php54 = version_compare(sprintf('%s.%s', PHP_MAJOR_VERSION, PHP_MINOR_VERSION), '5.4') < 0;

	if (is_null($flags)) {
		/* Fix 5.3: Use of undefined constant ENT_HTML401 - assumed 'ENT_HTML401' */
		if ($php54) {
			$flags = ENT_COMPAT|ENT_SUBSTITUTE;
		} else {
			$flags = ENT_COMPAT|ENT_HTML401;
		}
	}

	if (is_null($encoding)) {
		/* This logic will go off when the version is less then 5.4 */
		if ($php54) {
			/* From version 5.4 the default was ISO-8859-1 */
			$encoding = 'ISO-8859-1';
		} elseif (self::is_utf8($value)) {
			$encoding = 'UTF-8';
		} elseif (isset($PHprefs['charset'])) {
			$encoding = $PHprefs['charset'];
		} else {
			$encoding = null;
		}
	}

	return htmlspecialchars($value, $flags, $encoding);
}
} // End class LC
