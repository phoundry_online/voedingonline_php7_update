<?php

/**
 *
 *  Raw mime encoding class
 *
 * What is it?
 *   This class enables you to manipulate and build
 *   a mime email from the ground up.
 *
 * Why use this instead of mime.php?
 *   mime.php is a userfriendly api to this class for
 *   people who aren't interested in the internals of
 *   mime mail. This class however allows full control
 *   over the email.
 *
 * Eg.
 *
 * // Since multipart/mixed has no real body, (the body is
 * // the subpart), we set the body argument to blank.
 *
 * $params['content_type'] = 'multipart/mixed';
 * $email = new Mail_mimePart('', $params);
 *
 * // Here we add a text part to the multipart we have
 * // already. Assume $body contains plain text.
 *
 * $params['content_type'] = 'text/plain';
 * $params['encoding']     = '7bit';
 * $text = $email->addSubPart($body, $params);
 *
 * // Now add an attachment. Assume $attach is
 * the contents of the attachment
 *
 * $params['content_type'] = 'application/zip';
 * $params['encoding']     = 'base64';
 * $params['disposition']  = 'attachment';
 * $params['dfilename']    = 'example.zip';
 * $attach = $email->addSubPart($body, $params);
 *
 * // Now build the email. Note that the encode
 * // function returns an associative array containing two
 * // elements, body and headers. You will need to add extra
 * // headers, (eg. Mime-Version) before sending.
 *
 * $email = $message->encode();
 * $email['headers'][] = 'Mime-Version: 1.0';
 *
 *
 * Further examples are available at http://www.phpguru.org
 *
 * TODO:
 *  - Set encode() to return the $obj->encoded if encode()
 *    has already been run. Unless a flag is passed to specifically
 *    re-build the message.
 *
 * @author  Richard Heyes <richard@phpguru.org>
 * @version $Revision: 1.10 $
 * @package Mail
 */
class DMDmimepart
{

    /**
     * The encoding type of this part
     * @var string
     */
    private $_encoding;

    /**
     * An array of subparts
     * @var array
     */
    private $_subparts;

    /**
     * Headers for this part
     * @var array
     */
    private $_headers;

    /**
     * The body of this part (not encoded)
     * @var string
     */
    private $_body;

    /**
     * Constructor.
     *
     * Sets up the object.
     *
     * @param $body - The body of the mime part if any.
     * @param $params - An associative array of parameters:
     *                  content_type - The content type for this part eg multipart/mixed
     *                  encoding     - The encoding to use, 7bit, 8bit, base64, or quoted-printable
     *                  cid          - Content ID to apply
     *                  disposition  - Content disposition, inline or attachment
     *                  dfilename    - Optional filename parameter for content disposition
     *                  description  - Content description
     *                  charset      - Character set to use
     * @access public
     */
    public function __construct($body = '', $params = array())
    {
        if (!defined('MAIL_MIMEPART_CRLF')) {
            define('MAIL_MIMEPART_CRLF', defined('MAIL_MIME_CRLF') ? MAIL_MIME_CRLF : "\r\n");
        }

        foreach ($params as $key => $value) {
            switch ($key) {
                case 'content_type':
                    $headers['Content-Type'] = $value . (isset($charset) ? '; charset="' . $charset . '"' : '');
                    break;
                case 'encoding':
                    //if (!isset($params['encoded'])) {
                    $this->_encoding = $value;
                    //}
                    $headers['Content-Transfer-Encoding'] = $value;
                    break;
                case 'cid':
                    $headers['Content-ID'] = '<' . $value . '>';
                    break;
                case 'disposition':
                    $headers['Content-Disposition'] = $value . (isset($dfilename) ? '; filename="' . $dfilename . '"' : '');
                    break;
                case 'dfilename':
                    if (isset($headers['Content-Disposition'])) {
                        $headers['Content-Disposition'] .= '; filename="' . $value . '"';
                    } else {
                        $dfilename = $value;
                    }
                    break;
                case 'description':
                    $headers['Content-Description'] = $value;
                    break;
                case 'charset':
                    if (isset($headers['Content-Type'])) {
                        $headers['Content-Type'] .= '; charset="' . $value . '"';
                    } else {
                        $charset = $value;
                    }
                    break;
            }
        }

        // Default content-type
        if (!isset($headers['Content-Type'])) {
            $headers['Content-Type'] = 'text/plain';
        }

        //Default encoding
        if (!isset($this->_encoding)) {
            $this->_encoding = '7bit';
        }

        // Assign stuff to member variables
        $this->_headers = $headers;
        $this->_body = $body;
    }

    /**
     * encode()
     *
     * Encodes and returns the email. Also stores
     * it in the encoded member variable
     *
     * @return An associative array containing two elements,
     *         body and headers. The headers element is itself
     *         an indexed array.
     * @access public
     */
    public function encode()
    {
        if (!empty($this->_subparts)) {
            $boundary = 'D' . str_replace(array('.', ','), '', microtime(true)) . mt_rand(999, 999999);
            $this->_headers['Content-Type'] .= '; boundary="' . $boundary . '"';

            // Add body parts to $subparts
            for ($i = 0; $i < count($this->_subparts); $i++) {
                $headers = array();
                $tmp = $this->_subparts[$i]->encode();
                foreach ($tmp['headers'] as $key => $value) {
                    $headers[] = $key . ': ' . $value;
                }
                $subparts[] = implode(MAIL_MIMEPART_CRLF, $headers) . MAIL_MIMEPART_CRLF . MAIL_MIMEPART_CRLF . $tmp['body'];
            }

            $encoded['body'] = '--' . $boundary . MAIL_MIMEPART_CRLF . implode('--' . $boundary . MAIL_MIMEPART_CRLF, $subparts) . '--' . $boundary . '--' . MAIL_MIMEPART_CRLF;
        } else {
            $encoded['body'] = $this->_getEncodedData($this->_body, $this->_encoding) . MAIL_MIMEPART_CRLF;
        }

        // Add headers to $encoded
        $encoded['headers'] = $this->_headers;
        return $encoded;
    }

    /**
     * _getEncodedData()
     *
     * Returns encoded data based upon encoding passed to it
     *
     * @param $data     The data to encode.
     * @param $encoding The encoding type to use, 7bit, base64,
     *                  or quoted-printable.
     * @access private
     */
    private function _getEncodedData($data, $encoding)
    {
        switch ($encoding) {
            case '8bit':
            case '7bit':
                return $data;
                break;
            case 'quoted-printable':
                return $this->_quotedPrintableEncode($data);
                break;

            case 'base64':
                return rtrim(chunk_split(base64_encode($data), 76, MAIL_MIMEPART_CRLF));
                break;

            default:
                return $data;
        }
    }

    /**
     * quotedPrintableEncode()
     *
     * Encodes data to quoted-printable standard.
     *
     * @param $text    The text to encode
     * @param $line_max Optional max line length. Should
     *                  not be more than 76 chars
     *
     * @access private
     */
    private function _quotedPrintableEncode($text, $line_max = 76)
    {
        if (function_exists('imap_8bit')) {
            return imap_8bit($text);
        }

        // split text into lines
        $lines = explode(chr(13) . chr(10), $text);

        for ($i = 0; $i < count($lines); $i++) {
            $line =& $lines[$i];
            if (strlen($line) == 0) continue; // do nothing, if empty

            // imap_8bit encodes x09 everywhere, not only at lineends,
            // for EBCDIC safeness encode !"#$@[\]^`{|}~,
            // for complete safeness encode every character :)
            // $line = preg_replace('/[^\x20\x21-\x3C\x3E-\x7E]/e', 'sprintf("=%02X", ord("$0"));', $line);
            $line = preg_replace_callback('/[^\x20\x21-\x3C\x3E-\x7E]/', function ($matches) {
                return sprintf("=%02X", ord($matches[0]));
            }, $line);

            // encode x09,x20 at lineends
            $len = strlen($line);
            $lastchar = ord($line[$len - 1]);

            if (($lastchar == 0x09) || ($lastchar == 0x20)) {
                $line[$len - 1] = '=';
                $line .= ($lastchar == 0x09) ? '09' : '20';
            }
            // although IMHO not requested by RFC2045, why not do it safer :)
            // and why not encode any x20 around chr(10) or chr(13)
            $line = str_replace(' =0D', '=20=0D', $line);

            // finally split into softlines no longer than 76 chars,
            // for even more safeness one could encode x09,x20
            // at the very first character of the line
            // and after soft linebreaks, as well,
            // but this wouldn't be caught by such an easy RegExp
            preg_match_all('/.{1,73}([^=]{0,2})?/', $line, $match);
            $line = implode('=' . chr(13) . chr(10), $match[0]); // add soft crlf's
        }

        // join lines into text
        return implode(chr(13) . chr(10), $lines);
    }

    /**
     * addSubPart()
     *
     * Adds a subpart to current mime part and returns
     * a reference to it
     *
     * @param $body   The body of the subpart, if any.
     * @param $params The parameters for the subpart, same
     *                as the $params argument for constructor.
     * @return A reference to the part you just added. It is
     *         crucial if using multipart/* in your subparts that
     *         you use = in your script when calling this function,
     *         otherwise you will not be able to add further subparts.
     * @access public
     */
    public function addSubPart($body, $params)
    {
        $this->_subparts[] = new DMDmimepart($body, $params);
        return $this->_subparts[count($this->_subparts) - 1];
    }

} // End of class
