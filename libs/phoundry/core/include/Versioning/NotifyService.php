<?php
class Versioning_NotifyService
{
	/**
	 * @var DB_Adapter
	 */
	private $db;
	
	public function __construct(DB_Adapter $db)
	{
		$this->db = $db;
	}
	
	/**
	 * Get the notify status for a table and a user
	 * @param int $table_id can be zero to get the overall status
	 * @param int $user_id
	 * @return bool
	 */
	public function getStatus($table_id, $user_id)
	{
		$res = $this->db->iQuery(sprintf('
			SELECT user_id
			FROM phoundry_versioning_notify
			WHERE user_id = %d AND table_id = %d
			LIMIT 1',
			$user_id,
			$table_id
		));
		
		return count($res) === 1;
	}
	
	/**
	 * Enable or disable notifications for the given user and table
	 * 
	 * @param int $table_id zero to set the overall status
	 * @param int $user_id
	 * @param bool $status
	 */
	public function setStatus($table_id, $user_id, $status)
	{
		if ($status) {
			$this->db->iQuery(sprintf('
				INSERT INTO phoundry_versioning_notify
				(table_id, user_id) VALUES (%d, %d)',
				$table_id, $user_id
			));
		}
		else {
			$this->db->iQuery(sprintf('
				DELETE FROM phoundry_versioning_notify
				WHERE user_id = %d AND table_id = %d
				LIMIT 1',
				$user_id,
				$table_id
			));
		}
	}
	
	/**
	 * Send a notification for the given table
	 * 
	 * @param Versioning_Version $version
	 * @return array
	 */
	public function getNotifyUsers($table_id)
	{
		// send the notification
		return $this->db->iQuery(sprintf('
			SELECT DISTINCT u.id, u.name, u.e_mail
			FROM phoundry_versioning_notify AS n
			INNER JOIN phoundry_user AS u
			ON u.id = n.user_id
			WHERE table_id = %d',
			$table_id
		))->getAsArray();
	}
}