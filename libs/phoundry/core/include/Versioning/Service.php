<?php
require_once 'Version.php';

/**
 * Versioning service for the versioning of records in phoundry
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class Versioning_Service
{
	private $db;
	
	public function __construct(DB_Adapter $db)
	{
		$this->db = $db;
	}
	
	/**
	 * Store a Versioning Version to the database
	 * @param Versioning_Version $version
	 */
	public function storeVersion(Versioning_Version $version, $key)
	{
		if ($version->record_id) {
			$version->data[$key] = $version->record_id;
		}
		
		// If the parent version has no record id but we do, update the parent
		if ($version->status === 'published' && $version->parent_id) {
			$parent = $this->getVersion($version->parent_id);
			if (!$parent->record_id) {
				$this->setRecordIdForTrail($parent->trail_id, $version->record_id, $key);
			}
		}

		// Determine the trail id
		if ($version->parent_id &&
				$parent = $this->getVersion($version->parent_id)) {
			$version->trail_id = $parent->trail_id; 
		}
		else {
			$version->trail_id = $this->getNewTrailId();
		}
		
		// Determine revision id
		$previous = $this->db->iQuery(sprintf("
			SELECT revision
			FROM phoundry_versioning
			WHERE trail_id = %d
			ORDER BY revision DESC
			LIMIT 1",
			$version->trail_id
		))->first();
		
		if ($previous) {
			$version->revision = $previous->revision + 1;
		}
		else {
			$version->revision = 1;
		}
		
		$ids = $this->db->insertRows(array(array(
			'table_id' => $version->table_id,
			'trail_id' => $version->trail_id,
			'record_id' => $version->record_id,
			'parent_id' => $version->parent_id,
			'site_identifier' => $version->site_identifier,
			'user_id' => $version->user_id,
			'user_name' => $version->user_name,
			'revision' => $version->revision,
			'date' => $version->date,
			'status' => $version->status,
			'message' => $version->message,
			'data' => $version->encodeData()
		)), 'phoundry_versioning');
		
		if ($ids) {
			$version->id = $ids[0];
		}
	}
	
	/**
	 * Delete all versions for a specific record
	 * 
	 * @param int $table_id
	 * @param int $record_id
	 */
	public function deleteVersions($table_id, $record_id)
	{
		$this->db->iQuery(sprintf('
			DELETE FROM phoundry_versioning
			WHERE table_id = %d
			AND record_id = %d',
			$table_id,
			$record_id
		));
	}
	
	/**
	 * Get a version by its id
	 * @param int|array $id
	 * @return Traversable|Versioning_Version|bool
	 */
	public function getVersion($id)
	{
		if (!$id) {
			return false;
		}
		
		return $this->getVersions((array) $id)->first();
	}
	
	/**
	 * Get a version by its id
	 * @param int|array $id
	 * @return DB_ResultSet|bool
	 */
	public function getVersions(array $id)
	{
		if (!$id) {
			return false;
		}
		
		return $this->db->iQuery(sprintf("
			SELECT
				id,
				trail_id,
				table_id,
				record_id,
				parent_id,
				user_id,
				user_name,
				revision,
				date,
				status,
				message,
				data
			FROM phoundry_versioning
			WHERE id IN (%s)",
			implode(", ", array_map("intval", $id))
		))->setReturnClass('Versioning_Version');
	}
	
	/**
	 * Get the current live version for a node
	 * @param int $table_id
	 * @param int $record_id
	 * @return Versioning_Version|bool
	 */
	public function getLiveVersion($table_id, $record_id)
	{
		return $this->db->iQuery(sprintf("
			SELECT
				id,
				trail_id,
				table_id,
				record_id,
				parent_id,
				user_id,
				user_name,
				revision,
				date,
				status,
				message,
				data
			FROM phoundry_versioning
			WHERE table_id = %d
			AND record_id = %d
			AND status = 'published'
			ORDER BY date DESC, id DESC
			LIMIT 1",
			$table_id,
			$record_id
		))->setReturnClass('Versioning_Version')->first();
	}
	
	/**
	 * Get the latest version for a record
	 * @param int $table_id
	 * @param int $record_id
	 * @return Versioning_Version|bool
	 */
	public function getLatestVersion($table_id, $record_id)
	{
		return $this->db->iQuery(sprintf("
			SELECT
				id,
				trail_id,
				table_id,
				record_id,
				parent_id,
				user_id,
				user_name,
				revision,
				date,
				status,
				message,
				data
			FROM phoundry_versioning
			WHERE table_id = %d
			AND record_id = %d
			ORDER BY date DESC, id DESC
			LIMIT 1",
			$table_id,
			$record_id
		))->setReturnClass('Versioning_Version')->first();
	}
	
	/**
	 * Get a resultset containing arrays to create a version select
	 * @param int $trail_id
	 * @return array
	 */
	public function getTrail($trail_id)
	{
		return $this->db->iQuery(sprintf("
			SELECT
				id,
				trail_id,
				table_id,
				record_id,
				parent_id,
				user_id,
				user_name,
				revision,
				date,
				status,
				message,
				data
			FROM phoundry_versioning 
			WHERE trail_id = %d
			ORDER BY revision DESC",
			$trail_id
		))->setReturnClass('Versioning_Version');
	}
	
	/**
	 * Get the ids of versions that are currently ready to be published
	 * 
	 * @param int $table_id
	 * @return array
	 */
	public function getReadyTrails($table_id = null, $where = array())
	{
		$sql = '';
		if ($where) {
			$sql .= 'AND';
			foreach ($where as $key => $value) {
				$sql .= ' ver.'.$key.' '.(is_null($value) ? 'IS' : '=').
					' '. $this->db->quoteValue($value);
			}
		}
		
		// TODO optimalizeren
		$res = $this->db->iQuery(sprintf("
			SELECT subq.id
			FROM (
				SELECT MAX(v.id) AS id
				FROM phoundry_versioning AS v
				%s
				GROUP BY trail_id
				
			) AS subq
			INNER JOIN phoundry_versioning AS ver
			ON subq.id = ver.id
			WHERE ver.status = 'pending'
			%s",
			$table_id ? "WHERE v.table_id = ".(int) $table_id : "",
			$sql
		));
		
		return $res->getSelect('id');
	}
	
	/**
	 * Get the ids of the latest versions for trails without record_ids
	 * @param int $table_id
	 * @return array
	 */
	public function getOrphanVersionIds($table_id, $where = array())
	{
		// TODO optimalizeren
		return $this->db->iQuery(sprintf("
			SELECT subq.id
			FROM (
				SELECT MAX(v.id) AS id
				FROM phoundry_versioning AS v
				WHERE v.table_id = %d
				GROUP BY trail_id
				
			) AS subq
			INNER JOIN phoundry_versioning AS ver
			ON subq.id = ver.id
			WHERE ver.record_id IS null",
			$table_id
		))->getSelect('id');
	}
	
	/**
	 * Get the statusses for a set of ids
	 * @param array $record_ids
	 * @param int $table_id
	 * @return array
	 */
	public function getStatusForRecords(array $record_ids, $table_id)
	{
		if (!$record_ids || !$table_id) {
			return array();
		}
		
		$record_ids = array_map(array($this->db, 'quoteValue'), $record_ids);
		
		// TODO check if a subselect in the SELECT is faster
		$res = $this->db->iQuery(sprintf("
			SELECT subq.record_id, subq.status, subq.revision
			FROM (
				SELECT v.record_id, v.status, v.revision
				FROM phoundry_versioning AS v
				WHERE v.table_id = %d
				AND v.record_id IN (%s)
				ORDER BY v.date DESC, v.id DESC
			) AS subq
			GROUP BY subq.record_id",
			$table_id,
			implode(', ', $record_ids)
		));
		
		$results = array();
		foreach ($res as $row) {
			$results[$row->record_id] = array(
				'status' => $row->status,
				'revision' => $row->revision
			);
		}
		return $results;
	}
	
	/**
	 * Decode the datapart of the versions
	 * 
	 * @param string $data
	 * @return array
	 */
	private function decodeData($data)
	{
		$data = gzuncompress($data);
		return json_decode($data, true);
	}
	
	/**
	 * Encode the datapart of the versions 
	 * 
	 * @param array $data
	 * @return string
	 */
	private function encodeData(array $data)
	{
		$data = json_encode($data);
		return gzcompress($data);
	}
	
	/**
	 * Get a new Trail id
	 * @return int
	 */
	private function getNewTrailId()
	{
		$row = $this->db->iQuery("
			SELECT trail_id
			FROM phoundry_versioning
			ORDER BY trail_id DESC
			LIMIT 1"
		)->first();
		
		return (int) ($row ? $row->trail_id + 1 : 1);
	}
	
	private function setRecordIdForTrail($trail_id, $record_id, $key)
	{
		// Update the compressed data
		$res = $this->db->iQuery(sprintf("
			SELECT id, data FROM phoundry_versioning WHERE trail_id = %d",
			$trail_id));
		
		foreach ($res as $row) {
			$data = $this->decodeData($row->data);
			$data[$key] = $record_id;
			
			$this->db->iQuery(sprintf("
				UPDATE phoundry_versioning
				SET record_id = %d, data = %s
				WHERE id = %d",
				$record_id,
				$this->db->quoteValue($this->encodeData($data)),
				$row->id
			));
		}
	}
}
