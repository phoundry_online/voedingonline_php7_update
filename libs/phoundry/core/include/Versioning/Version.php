<?php
class Versioning_Version
{
	public $id;
	public $trail_id;
	public $table_id;
	public $record_id;
	public $parent_id;
	public $site_identifier;
	public $user_id;
	public $user_name;
	public $revision;
	public $date;
	public $status;
	public $message;
	public $data;
	
	public function __construct()
	{
		// Object was created by mysql_fetch_object
		if (is_string($this->data)) {
			$this->data = $this->decodeData($this->data);
		}
	}
	
	/**
	 * Decode the datapart of the versions
	 * 
	 * @param string $data
	 * @return array
	 */
	private function decodeData($data)
	{
		if (!is_string($data)) {
			throw new Exception("Data should be a valid gzipped string");
		}
		return json_decode(gzuncompress($data), true);
	}
	
	/**
	 * Encode the datapart of the versions 
	 * 
	 * @param array $data
	 * @return string
	 */
	public function encodeData()
	{
		return gzcompress(json_encode($this->data));
	}
	
	public function getStatusString()
	{
		$statusses = array(
			'draft' => word(361),
			'pending' => word(362),
			'published' => word(363)
		);
		return isset($statusses[$this->status]) ?
			$statusses[$this->status] : 'unknown';
	}
	
	public function __toString()
	{
		$statusses = array(
			'draft' => word(361),
			'pending' => word(362),
			'published' => word(363)
		);
		return '(' . $this->revision . ') ' .
			date('H-i, m F Y', strtotime($this->date)) .
			' ' . $this->user_name .
			' (' . $this->getStatusString() . ')';
	}
}
