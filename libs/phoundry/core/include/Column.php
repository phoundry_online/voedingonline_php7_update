<?php

class Column 
{
	public $name, $tableName, $DBprops = array(), $id = -1, $description = '', $required, $info = '', $inputtype = '', $datatype = '', $searchable = 0, $rights = array();
	
	public function __construct($id = -1, $DBcolName = '', $DBtableName = '', $groupId = -1, $required = 0, $datatype = '', $inputtype = '') 
	{
		global $db, $PHenv, $PHprefs;

		$this->id = (int)$id;

		if ($this->id != -1) {
			$sql = "SELECT c.*, t.name as table_name FROM phoundry_column c, phoundry_table t WHERE c.id = {$this->id} AND c.table_id = t.id";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$this->name         = $db->FetchResult($cur,0,'name');
			$this->tableName    = $db->FetchResult($cur,0,'table_name');

			// For DMdelivery:
			if (isset($PHprefs['product']) && $PHprefs['product'] == 'DMdelivery' && isset($PHenv['DMDcid'])) {
				$this->tableName = ($this->tableName == 'recipient_{$CAMPAIGN_ID}') ? $PHenv['rcptTable'] : str_replace('{$CAMPAIGN_ID}', $PHenv['DMDcid'], $this->tableName);
			}

			$this->description  = PH::langWord($db->FetchResult($cur,0,'description'));
			$this->required     = $db->FetchResult($cur,0,'required') == 1;
			$this->searchable   = (int)$db->FetchResult($cur,0,'searchable');
			$this->info         = PH::evaluate(PH::langWord($db->FetchResult($cur,0,'info')));

			$datatype           = ($datatype === '')  ? $db->FetchResult($cur,0,'datatype')  : $datatype;
			$inputtype          = ($inputtype === '') ? $db->FetchResult($cur,0,'inputtype') : $inputtype;

			// Determine properties of this column:
			if (!preg_match('/^(DTX|DTfake)/', $datatype)) {
				$db->GetTableFieldDefinition($this->tableName, $this->name, $DBprops)
					or trigger_error("Cannot get index {$this->name} for table {$this->tableName}: " . $db->Error());
		   	$this->DBprops = $DBprops[0];
			}
		}
		else {
			$this->name         = $DBcolName;
			$this->tableName    = $DBtableName;
			$this->description  = $DBcolName;
			$this->required     = $required;
			$this->searchable   = 0;
			$this->info         = '';
			if ($DBcolName != '' && $DBtableName != '') {
				// Determine properties of this column:
				$db->GetTableFieldDefinition($this->tableName, $this->name, $DBprops)
					or trigger_error("Cannot get index {$this->name} for table {$this->tableName}: " . $db->Error());
		   	    $this->DBprops = $DBprops[0];
			}

		}

		if (!empty($datatype)) {
			PH::includeClass($datatype, 'Datatypes');
			$this->datatype  = new $datatype($this);
		}

		if (!empty($inputtype)) {
			PH::includeClass($inputtype, 'Inputtypes');
			$this->inputtype = new $inputtype($this);
		}

		$this->rights = $this->getRights($groupId);
	}

	/**
	 * Determine the rights that a group has on this column.
	 * Rights are: view, insert, copy and update.
	 *
	 * @author Arjan Haverkamp
	 * @param $groupId(int) The group-id to get the column-rights for.
	 * @return An array with column rights.
	 * @returns array
	 */
	public function getRights($groupId) 
	{
		global $db;

		$rights = array('view'=>1, 'search'=>1, 'insert'=>1, 'copy'=>1, 'update'=>1);
		// A group has all rights per default, unless restricted.
		$sql = "SELECT rights FROM phoundry_group_column WHERE group_id = " . (int)$groupId . " AND column_id = {$this->id}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur)) {
			// Group has restricted rights.
			$rights = array();
			$DBrights = $db->FetchResult($cur,0,'rights');
			if (strpos($DBrights, 'v') !== false)
				$rights['view'] = $rights['search'] = 1;
			if (strpos($DBrights, 'i') !== false)
				$rights['insert'] = $rights['copy'] = 1;
			if (strpos($DBrights, 'u') !== false)
				$rights['update'] = 1;
		}
		return $rights;
	}

	/**
	 * Get the value of this column, as stored in the database.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'search', 'export, 'insert', 'update' or 'preview'
	 * @param $keyval(string) The key-value for the record this column is in.
	 * @param $cur(object) The database cursor object.
	 * @param $row(int) The row-number of the record this column is in.
	 * @param $strlength(int) The maximum string-length to return (-1 for no limit).
	 * @return A string representing the value.
	 * @returns string
	 */
	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		if ($page == 'insert') {
			$value = isset($this->rights[$page]) ? $this->datatype->getValue($page, $keyval, $cur, $row, $strlength) : null;
		}
		else {
			$value = $this->datatype->getValue($page, $keyval, $cur, $row, $strlength);
		}
	
		return $this->inputtype->getValue($page, $value, $keyval);
	}

	/*
	 * Retrieve the SELECT-part for this column (SELECT what FROM ....).
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'search', 'insert', 'update' or 'preview'
	 * @param $obeyRights(boolean) Whether or not to obey the column-rights.
	 * @return A string suitable for use in the SELECT clause of a SQL query.
	 * @returns string
	 */
	public function getSQLpart($page, $obeyRights = true) 
	{
		// If we're on the update page, we might still need to put this column in
		// the SELECT-list, if the user has view-rights, that is.
		if (!$obeyRights || isset($this->rights[$page]) || ($page == 'update' && isset($this->rights['view']))) {
			return $this->datatype->getSQLpart($page);
		}
		else {
			return null;
		}
	}

	/**
	 * Get the value to store in the database, using $source as a datasource.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $fields(array) A reference to an associative array that will be filled by this
	 *                       function. The index contains the column name, the value is the 
	 *                       (translated) value, which has to be available in $source.
	 * @param $extras(array) A reference to an array that will be filled by this function.
	 *                       May contain extra SQL-statements that need to be executed after 
	 *                       the INSERT or UPDATE statement is executed against the database.
	 * @param $oldKey(string) The (old) record-id being UPDATEd (omit if $page == 'insert')
	 * @returns void
	 */
	public function getSQLvalue($page, &$source, &$fields, &$extras, $oldKey = -1) 
	{ 
		if (isset($this->rights[$page]) && !isset($this->DBprops['autoincrement'])) {
			$this->datatype->getSQLvalue($page, $source, $fields, $extras, $oldKey);
		}
	}

	/**
	 * Delete stuff that is associated with this column.
	 * For normal columns, this doesn't do anything. For files and cross references,
	 * the associated records and files can be deleted.
	 *
	 * @author Arjan Haverkamp
	 * @param $keyName(string) The key-name for the record this column is in.
	 * @param $keyVal(string) The key-value for the record this column is in.
	 * @returns void
	 */
	public function deleteStuff($keyName, $keyVal) 
	{
		$this->datatype->deleteStuff($keyName, $keyVal);
	}

	/**
	 * Get HTML-code for editing this column.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert', 'copy' or 'update'
	 * @param $value(string) The database value for this column.
	 * @param $filtered(bool) Whether this Column's value was determined
	 *                        by a filter or not.
	 * @param $includes(array) A reference to an array with this structure:
	 *                         'jsCode'=>array(),
	 *                         'jsFiles'=>array(),
	 *									'cssFiles'=>array()
	 *									Add entries to those arrays in order to have them
	 *									included in the <HEAD> of the edit-page.
	 * @return A string of HTML code, representing an editable column.
	 * @returns string
	 */
	public function getHtml($page, $value = '', $filtered = false, &$includes) 
	{
		if ($filtered) 
		{
			return '<input name="' . $this->name . '" type="hidden" value="' . PH::htmlspecialchars($value) . '" />' . "\n";
		}
		if (isset($this->rights[$page])) 
		{
			return $this->inputtype->getHtml($page, $value, false, $includes);
		}
		else 
		{
			// On the update page, we need to display the field as read-only,
			// if the user has 'view' rights on this column.
			if ($page == 'update' && isset($this->rights['view'])) 
			{
				return $this->inputtype->getHtml($page, $value, true, $includes); // Read-only
			}
		}
		return '';
	}

	/**
	 * Get ALT-tag for use in inputtypes, so we can check the value for 
	 * this column using Javascript. Format of the ALT-tag is:
	 *   required|datatype(|maxlength)
	 * required: 1 or 0
	 * datatype: The name of the datatype (all lowercase)
	 * maxlength: maximum number of characters the value may contain
	 *
	 * @author Arjan Haverkamp
	 * @returns A string containing an ALT-tag.
	 * @returns string
	 */
	public function getJScheckAltTag() 
	{
		$alt = $this->required ? '1' : '0';
		$alt .= '|' . substr(strtolower(get_class($this->datatype)),2);
		if (!empty($this->inputtype->extra['maxlength'])) {
			$alt .= '|' . $this->inputtype->extra['maxlength'];
		}
		return $alt;
	}

	/**
	 * Retrieve Javascript code to syntactically check this column.
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Either 'insert' or 'update'
	 * @returns A string of Javascript code, representing this column's syntax check
	 * @returns string
	 */
	public function checkSyntaxJS($page) 
	{
		return $this->datatype->checkSyntaxJS($page);
	}

	/**
	 * Retrieve Javascript code for this column (used for fake columns).
	 *
	 * @author Arjan Haverkamp
	 * @param $page(string) Currently, only 'search' is known.
	 * @returns A string of Javascript code
	 * @returns string
	 */
	public function getJScode($page) 
	{
		return $this->datatype->getJScode($page) . "\n" . $this->inputtype->getJScode($page);
	}

	/**
	 * Check this Column's syntax.
	 *
	 * @param $page(string) Either 'insert' or 'update'.
	 * @param $source(array) An associative array that contains the 'raw' value.
	 *                       Most commonly, $_POST or $_GET are used here.
	 * @param $errors(array) A reference to the array that will contain the error
	 *                       messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
		$value = ''; $hasError = false;

		// Check required:
		//if ($this->required) 
		if ($this->required && !($page == 'update' && isset($this->datatype->extra['encoding'])))
		{
			if ($this->inputtype instanceof ITradio && array_key_exists($this->name, $source) && $source[$this->name] == '') {
				$source[$this->name] = 0;
			}


			$reqError = $this->datatype->checkRequired($source);
			if ($reqError === true) 
			{
				$errors[] = word(43, $this->description); // Required field not entered
				$hasError = true;
			}
		}

		if (!$hasError) 
		{
			$value = isset($source[$this->name]) ? $source[$this->name] : (isset($_FILES[$this->name]['name']) ? $_FILES[$this->name]['name'] : '');
			if ($this->inputtype instanceof ITradio && $value == '') {
				$value = 0;
				$source[$this->name] = 0;
			}

			if (is_array($value)) { $value = implode(',', $value); }

			// Check maxlength: (skip if encoding was applied)
			if (!empty($this->inputtype->extra['maxlength'])) 
			{
				if (strlen((string)$value) > $this->inputtype->extra['maxlength'] && !isset($this->datatype->extra['encoding'])) 
				{
					// Field #1 contains #2 characters, max is #3
					$errors[] = word(44, $this->description, strlen((string)$value), $this->inputtype->extra['maxlength']);
				}
			}
			else 
			{
				if (isset($this->DBprops['length']) && strlen((string)$value) > $this->DBprops['length'] && !isset($this->datatype->extra['encoding'])) 
				{
					// Field #1 contains #2 characters, max is #3
					$errors[] = word(44, $this->description, strlen((string)$value), $this->DBprops['length']);
				}
			}
			
			// Check syntax:
			if ($value !== '') 
			{
				$this->datatype->checkSyntaxPHP($page, $source, $errors);
			}
		}
	}

	/**
	 * Some inputtypes or datatypes would like to know what filter-value
	 * was chosen, so they can customize the shown HTML accordingly.
	 */
	public function getFilters() 
	{
		global $db;

		$filters = $filterIDs = array();
		foreach($_REQUEST as $name=>$value) {
			if (preg_match('/PHfltr_([0-9]+)$/', $name, $reg)) {
				$filterIDs[$reg[1]] = $value;
			}
		}

		if (empty($filterIDs)) {
			return array();
		}

		$sql = 'SELECT * FROM phoundry_filter WHERE type = \'user\' AND id IN (' . implode(',', array_keys($filterIDs)) . ')';
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$fValue = $filterIDs[$db->FetchResult($cur,$x,'id')];
			$filters[$db->FetchResult($cur,$x,'matchfield')] = array(
				'name'		=> $db->FetchResult($cur,$x,'name'),
				'value'		=> $fValue,
				'group_id'	=> $db->FetchResult($cur,$x,'group_id'),
				'matchfield' => $db->FetchResult($cur,$x,'matchfield'),
				'query'		=> $db->FetchResult($cur,$x,'query'),
				'wherepart' => strtr($db->FetchResult($cur,$x,'wherepart'), array('{$FILTER_VALUE}' => $fValue, 'FILTER_VALUE' => $fValue))
			);
		}
		return $filters;
	}

	/**
 	 * Recursively make directories.
 	 *
 	 * @author Arjan Haverkamp
 	 * @param $makedir(string) The full path of the directory to create.
	 * @return true on succes, false otherwise.
	 * @returns boolean
 	 */
	public function makedirs($makedir) 
	{
		global $PHprefs;

		$makedir = str_replace('\\', '/', $makedir);
		
		if (is_dir($makedir))
			return true;

		$makedir = preg_replace('|^' . preg_quote(str_replace('\\','/',$PHprefs['uploadDir'])) . '|', '', $makedir);
		if (substr($makedir, -1) == '/') {
			$makedir = substr($makedir, 0, strlen($makedir)-1);
		}
		$dirs = explode('/', $makedir);
	
	 	$dir = '';
	 	for ($x = 1; $x < count($dirs); $x++) {
			if (preg_match('`\{\$([A-z][\w]+)(?:\|([^\}]+))?}`', $dirs[$x])) {
				// This is a variable directory. Do not create it!
				return true;
			}
	 		$dir .= '/' . $dirs[$x];
			if (!is_dir($PHprefs['uploadDir'] . $dir)) {
				if (!@mkdir($PHprefs['uploadDir'] . $dir, 0775)) 
					return false;
			}
		}
		return true;
	}

} // End class

?>
