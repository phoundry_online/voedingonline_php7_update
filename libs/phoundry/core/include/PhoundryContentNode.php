<?php
class PhoundryContentNode
{
	public $id;
	public $string_id;
	public $rid;
	
	public function __construct($id = null, $string_id = null, $rid = null)
	{
		if (null !== $id) {
			$this->id = $id;
		}
		if (null !== $string_id) {
			$this->string_id = $string_id;
		}
		if (null !== $rid) {
			$this->rid = $rid;
		}
	}
	
	/**
	 * Allows this object to be var_exported
	 * @param array $a
	 * @return PhoundryContentNode
	 */
	public static function __set_state($a)
	{
		return new self($a['id'], $a['string_id'], $a['rid']);
	}
}
