<?php
class PhoundryMenuSync
{
	private $db;

	public function __construct(DB_Adapter $db)
	{
		$this->db = $db;
	}

	public function export()
	{
		return $this->db->iQuery(
			sprintf(
				'SELECT
					m.id,
					m.tab_id,
					m.name,
					t.string_id,
					m.parent
				FROM phoundry_menu AS m
				LEFT JOIN phoundry_table AS t
				ON m.table_id = t.id'
			)
		)->getAsArray();
	}

	public function import($importData)
	{
		$stringIds = array();
		foreach ($importData as $menu) {
			$stringIds[] = $menu['string_id'];
		}

		$stringIds = array_map(array($this->db, 'quoteValue'), $stringIds);
		if (!$stringIds) {
			return;
		}

		$res = $this->db->iQuery(
			sprintf(
				'SELECT id, string_id FROM phoundry_table WHERE string_id IN (%s)',
				implode(', ', $stringIds)
			)
		);

		$stringIds = $res->getSelect('string_id', 'id');

		$values = array();
		foreach ($importData as $menu) {
			$stringId = $menu['string_id'];
			unset($menu['string_id']);
			if (array_key_exists($stringId, $stringIds)) {
				$menu['table_id'] = $stringIds[$stringId];
			} else {
				$menu['table_id'] = null;
			}
			$values[] = $menu;
		}

		$this->db->iQuery('TRUNCATE phoundry_menu;');

		$this->db->insertRows($values, 'phoundry_menu');
	}
}