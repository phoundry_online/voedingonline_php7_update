<?php
/**
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2011 Web Power BV, http://www.webpower.nl
 */
class UrlCheck
{
	/**
	 * @var metabase_database_class
	 */
	private $db;
	
	private $protocol;
	private $domain;
	private $upload_url;
	
	/**
	 * @param metabase_database_class $db Metabase instatie
	 * @param string $protocol
	 * @param string $domain
	 * @param string $upload_url
	 */
	public function __construct(metabase_database_class $db, 
		$protocol, $domain, $upload_url)
	{
		$this->db = $db;
		$this->protocol = $protocol;
		$this->domain = $domain;
		$this->upload_url = $upload_url;
	}
	
	/**
	 * Get the tables that will be searched in
	 * @param array $in
	 */
	public function getTables(array $in = null)
	{
		$tables = array();
		if ($in) {
			$in_sql = 'AND pt.id IN (' . implode(', ', $in) . ')';
		} else {
			$in_sql = '';
		}

		$sql = "SELECT pt.id, pt.name, pt.description
			FROM phoundry_table pt
			WHERE pt.is_plugin = 0
			{$in_sql}
			ORDER BY pt.order_by";
		$cur = $this->db->Query($sql) or trigger_error(
			"Query $sql failed: " . $this->db->Error(), E_USER_ERROR
		);
		for ($x = 0; !$this->db->EndOfResult($cur); $x++) {
			$id = (int) $this->db->FetchResult($cur, $x, 'id');
			$tables[$id] = array(
				'id' => $id,
				'name' => $this->db->FetchResult($cur, $x, 'name'), 
				'desc' => PH::langWord(
					$this->db->FetchResult($cur, $x, 'description')
				)
			);
		}
		
		foreach ($tables as $id => $table) {
			$sql = "SELECT id, name, description, datatype
				FROM phoundry_column
				WHERE table_id = $id
				AND (
					datatype IN ('DTfile','DTurl','DTsiteStructure')
					OR (
						datatype = 'DTstring'
						AND datatype_extra LIKE '%\"display\";s:4:\"html\"%'
					)
				)
				ORDER BY order_by";
			$cur = $this->db->Query($sql) or trigger_error(
				"Query $sql failed: " . $this->db->Error(), E_USER_ERROR
			);

			$columns = array();
			for ($x = 0; !$this->db->EndOfResult($cur); $x++) {
			 	$colName = $this->db->FetchResult($cur, $x, 'name');
				$columns[$colName] = array(
					'id' => (int) $this->db->FetchResult($cur, $x, 'id'), 
					'name' => $colName, 
					'desc' => PH::langWord(
						$this->db->FetchResult($cur, $x, 'description')
					),
					'datatype' => $this->db->FetchResult(
						$cur, $x, 'datatype'
					)
				);
			}
			if ($columns) {
				$tables[$id]['columns'] = $columns;
			} else {
				unset($tables[$id]);
			}
		}
		return $tables;
	}

	/**
	 * Searches the database for the given links
	 * If an array of table ids is given only those tables will be searched
	 * @param array $links List of urls to check
	 * @param array $in optional list of table_ids
	 * @return array 
	 */
	public function searchLink(array $links, array $in = null)
	{
		$tables = $this->getTables($in);
		$result = array();
		
		foreach ($tables as $table) {
			$sql = $this->buildSelectQuery($table);
			$cur = $this->db->Query($sql) or trigger_error(
				"Query $sql failed: " . $this->db->Error(), E_USER_ERROR
			);
			for ($x = 0; !$this->db->EndOfResult($cur); $x++) {
				$nrf = $this->db->NumberOfColumns($cur);
				$RID = $this->db->FetchResult($cur, $x, 0);
	
				foreach ($table['columns'] as $fieldName => $column) {
					$fieldDesc = $table['columns'][$fieldName]['desc'];
					$columnDT = $table['columns'][$fieldName]['datatype'];
					$value = $this->db->FetchResult($cur, $x, $fieldName);
					if ($columnDT == 'DTurl') {
						$found = $this->checkUrl($value, $links);
					} else if ($columnDT == 'DTfile') {
						$found = $this->checkFile($value, $links);
					} else if ($columnDT == 'DTstring') {
						$found = $this->checkHtml($value, $links);
					} else if ($columnDT == 'DTsiteStructure') {
						$found = $this->checkSiteStructure($value, $links);
					} else {
						$found = false;
					}
					
					if ($found) {
						if (!isset($result[$found])) {
							$result[$found] = array();
						}
						$result[$found][] = array(
							'table' => $table,
							'record' => array(
								'id' => $RID,
								$fieldName => $value,
							),
							'column' => $fieldName
						);
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * Makes relative urls absolute, and absolute urls relative
	 * @param array $links
	 * @return array
	 */
	public function prepareLinks(array $links)
	{
		$domain = $this->protocol.'://'.$this->domain;
		
		foreach ($links as $link) {
			$link = $this->cleanUrl($link);
			if ($link[0] === '/') {
				$links[] = $domain.$link;
			} else if (stripos($link, $domain) === 0) {
				$links[] = substr($link, strlen($domain));
			}
		}
		return array_unique($links);
	}
	
	private function cleanUrl($url)
	{
		// replace all double slashes by single slashes exept when preceded by :
		return preg_replace('/(?<!\:)\/\/+/', '/', $url);
	}
	
	/**
	 * While searching the tables take the filters into account
	 * @param int $table_id
	 * @return array Where parts that should be joined by AND
	 */
	private function getFilterWhereParts($table_id)
	{
		$ands = array();
		$sql = "SELECT wherepart
			FROM phoundry_filter
			WHERE table_id = {$table_id}
			AND type = 'system'";
		$cur = $this->db->Query($sql) or trigger_error(
			"Query $sql failed: " . $this->db->Error(), E_USER_ERROR
		);
		for ($x = 0; !$this->db->EndOfResult($cur); $x++) {
			$filter = $this->db->FetchResult($cur, $x, 'wherepart');
			if (preg_match('|\{\$([a-z][\w]+)\}|i', $filter)) continue;
			$ands = array_merge($ands, PH::getFilter(unserialize($filter)));
		}
		return $ands;
	}
	
	private function buildSelectQuery(array $table)
	{
		$index = array();
		$this->db->GetTableIndexDefinition($table['name'], 'PRIMARY', $index) or trigger_error(
			"Query failed: " . $this->db->Error(), E_USER_ERROR
		);
		$key = array_keys($index['FIELDS']);
		$key = $key[0];
		
		$sql = "SELECT * FROM ".$table['name'];
		
		$ands = $this->getFilterWhereParts($table['id']);
		if ($ands) {
			$sql .= ' WHERE ' . implode(' AND ', $ands);
		}
		return $sql;
	}
	
	private function checkUrl($url, array $links)
	{
		if (!$url) {
			return false;
		}
		
		$url = $this->cleanUrl($url);
		
		if (in_array($url, $links)) {
			return $url;
		}
		
		if ($url[0] == '/') {
			$full_url = $this->protocol.'://'.$this->domain.$url;
			if (in_array($full_url, $links)) {
				return $url;
			}
		}
		
		return false;
	}
	
	private function checkFile($url, array $links)
	{
		if (!$url) {
			return  false;
		}
		$url = $this->upload_url.$url;
		return $this->checkUrl($url, $links);
	}
	
	private function checkHtml($html, array $links)
	{
		$dom = new DOMDocument();
		@$dom->loadHTML($html);
		$xpath = new DOMXPath($dom);
		$tags = $xpath->query('//img[@src]|//a[@href]|//form[@action]');
		/* @var $tag DOMElement */
		foreach ($tags as $tag) {
			switch ($tag->tagName) {
				case 'img':
					$attr = 'src';
					break;
				case 'a':
					$attr = 'href';
					break;
				case 'form':
					$attr = 'action';
					break;
			}
			$url = $tag->getAttribute($attr);

			if (empty($url)) {
				continue;
			}
			
			// Skip URL's that start with #, ., mailto:, javascript: or https://
			if (preg_match('/^(?:#|\.|mailto:|mms:|javascript:\/\/)/i', $url)) {
				continue;
			}
			
			if ($this->checkUrl($url, $links)) {
				return $url;
			}
		}
		
		return false;
	}
	
	private function checkSiteStructure($site_structure_id, array $links)
	{
		require_once INCLUDE_DIR.'/functions/autoloader.include.php';
		
		/* @var $structure Model_Site_Structure */
		$structure = ActiveRecord::factory('Model_Site_Structure', $site_structure_id);

		if ($structure->isLoaded()) {
			/* @var $page Model_Page */
			foreach ($structure->pages as $page) {
				$link = $page->getFullPath();
				if ($this->checkUrl($link, $links)) {
					return $link;
				}
			}
			/* @var $url Model_Url */
			foreach ($structure->urls as $url) {
				if ($this->checkUrl($url['link'], $links)) {
					return $url['link'];
				}
			}
			
			$link = SiteStructure::getFullPathByStructureId($site_structure_id, $structure['site_identifier']);
			if ($this->checkUrl($link, $links)) {
				return $link;
			}
			if ($this->checkUrl($structure->site->getUrl().$link, $links)) {
				return $link;
			}
		}
		
		return false;
	}
}