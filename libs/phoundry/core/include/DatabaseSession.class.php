<?php

/**
 * Database managed session
 * When using this class all sessions will be stored in the database.
 * This is the recommened session handler for all websites that run
 * on more than one machine and has little session data.
 * 
 * @author		Roalt Zijlstra, Arjan Haverkamp
 * @version		2.0 (2009-01-31)
 * @package		Phoundry
 * @copyright	Web Power (http://www.webpower.nl)
 */
class DatabaseSession
{
	public $DBlink;
	
	/** Constructor
	 *
	 * A valid mysql DB link needs to be passed to this class. If not sessions will not work right.
	 */
	public function __construct($DBlink) 
	{
		$this->DBlink = $DBlink;
	}

	public function __destruct() 
	{
		session_write_close();
	}
	
	/** Session Open
	 *
	 * For MySQL connectivity we assume the Database link to be passed with the constructor 
	 * and do nothing here.
	 */
	public function phpSessionOpen($savePath, $sessionName)
	{
		return true;
	}

	/** Session Close
	 *
	 * The database should be closed elsewhere as we share the database connection for mysql Sessions.
	 */
	public function phpSessionClose()
	{
		return true;
	}
	
	public function phpSessionRead($sessionId)
	{
		$maxlifetime = (int) ini_get('session.gc_maxlifetime');
		if ($maxlifetime === 0) {
			$maxlifetime = 1440;
		}
		
		$sql = sprintf(
			"SELECT data
			FROM phoundry_session
			WHERE session_id = '%s'
			AND last_access > DATE_SUB(NOW(), INTERVAL %d SECOND)",
			mysql_real_escape_string($sessionId, $this->DBlink),
			$maxlifetime
		);
		$cur = mysql_query($sql, $this->DBlink);
		
		if (!$cur && mysql_errno($this->DBlink) == 1146) { // 1146 = table doesn't exist
			// No, there is no such table: create it:
			$sql = 'CREATE TABLE phoundry_session (
							`session_id` char(64) not null,
							`data` mediumblob not null,
							`last_access` datetime not null,
							`ip` char(32) not null,
							`created_on` datetime not null,
							PRIMARY KEY (`session_id`),
							KEY last_access (`last_access`)
						)';
			mysql_query($sql, $this->DBlink)
				or trigger_error("Session Table Create Error: Query $sql failed: " . mysql_error($this->DBlink), E_USER_ERROR);
			// Table successfully created, but no session data available:
			return '';
		}
		else if(!$cur) {
			// Other (real) database error
			trigger_error("Session Read Error: Query $sql failed: " . mysql_error($this->DBlink), E_USER_ERROR);
		}
		
		if (mysql_num_rows($cur) == 0) {
			return '';
		}

		$row = mysql_fetch_assoc($cur);
		return $row['data'];
	}
	
	public function phpSessionWrite($sessionId, $sessionData)
	{
		$sql = sprintf("INSERT INTO phoundry_session (session_id, data, last_access, ip, created_on) VALUES ('%s', '%s', NOW(), '%s', NOW()) ON DUPLICATE KEY UPDATE data = '%s', last_access = NOW()" ,
						mysql_real_escape_string($sessionId, $this->DBlink),
						mysql_real_escape_string($sessionData, $this->DBlink),
						mysql_real_escape_string($_SERVER['REMOTE_ADDR'], $this->DBlink),
						mysql_real_escape_string($sessionData, $this->DBlink));
		mysql_query($sql, $this->DBlink)
			or trigger_error("Session Write Error: Query $sql failed: " . mysql_error($this->DBlink), E_USER_ERROR);

		return true;
	}
	
	public function phpSessionDestroy($sessionId)
	{
		$sql = "DELETE FROM phoundry_session WHERE session_id = '" . mysql_real_escape_string($sessionId, $this->DBlink) . "'";
		mysql_query($sql, $this->DBlink)
			or trigger_error("Session Destroy Error: Query $sql failed: " . mysql_error($this->DBlink), E_USER_ERROR);

		return true;
	}
	
	public function phpSessionGarbageCollect($maxLifeTime)
	{
		$sql = 'DELETE FROM phoundry_session WHERE last_access < DATE_SUB(NOW(), INTERVAL ' . (int)$maxLifeTime . ' SECOND)';
		mysql_query($sql, $this->DBlink)
			or trigger_error("Session Destroy Error: Query $sql failed: " . mysql_error($this->DBlink), E_USER_ERROR);

		return true;
	}
}

?>
