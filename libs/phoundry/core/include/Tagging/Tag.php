<?php
class Tagging_Tag
{
	public $id;
	public $name;
	
	public function __construct($id = null, $name = null)
	{
		if (null !== $id) {
			$this->id = $id;
		}
		if (null !== $name) {
			$this->name = $name;
		}
	}
	
	public function __toString()
	{
		return (string) $this->name;
	}
	
	/**
	 * Allows this object to be var_exported
	 * @param array $a
	 * @return Tagging_Tag
	 */
	public static function __set_state($a)
	{
		return new self($a['id'], $a['name']);
	}
}