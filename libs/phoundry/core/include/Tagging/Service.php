<?php
require_once 'Exception.php';
require_once "{$PHprefs['distDir']}/core/include/PhoundryContentNode.php";
require_once 'Tag.php';

/**
 * Tagging service allows working with the Tagging functionality
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class Tagging_Service
{
	/**
	 * @var DB_Adapter
	 */
	private $db;
	
	/**
	 * Creates the Taggingservice with the required dependencies
	 * 
	 * @param DB_Adapter $db
	 */
	public function __construct(DB_Adapter $db)
	{
		$this->db = $db;
	}
	
	/**
	 * Search for a tag by its name
	 * 
	 * @throws Tagging_Exception
	 * @param string $name
	 * @return Tagging_Tag
	 */
	public function getTagByName($name)
	{
		$row = $this->getTagsByNames(array($name))->first();
		
		if (false === $row) {
			throw new Tagging_Exception("Tag not found");
		}
		return new Tagging_Tag($row->id, $name);
	}
	
	/**
	 * Get an array of tags by their names
	 * 
	 * @throws Tagging_Exception
	 * @param array $names
	 * @return Traversable containing Tagging_Tag objects
	 */
	public function getTagsByNames(array $names)
	{
		if (!$names) {
			throw new Tagging_Exception("No names given");
		}
		
		return $this->db->iQuery(sprintf("
			SELECT
				id, name
			FROM
				phoundry_tag
			WHERE
				name IN(%s)",
			implode(", ", array_map(array($this->db, "quoteValue"), $names))
		))->setReturnClass("Tagging_Tag");
	}
	
	/**
	 * Get a interator containing Tagging_Tag objects
	 * 
	 * @param PhoundryContentNode|int $collection id or collection object
	 * @return Traversable containing Tagging_Tag objects
	 */
	public function getTagsForContentNode($node)
	{
		$node_id = $node instanceof PhoundryContentNode ?
			$node->id : (int) $node;
		
		return $this->db->iQuery(sprintf("
			SELECT
				id, name
			FROM
				phoundry_tag AS t
			INNER JOIN
				phoundry_tag_content_node AS tct
			ON
				t.id = tct.tid
			AND
				tct.cid = %d",
			$node_id
		))->setReturnClass("Tagging_Tag");
	}

	/**
	 * Get a iterator containing PhoundryContentNode objects
	 * 
	 * @param Tagging_Tag|int $tag either a tag or a tag id
	 * @return Traversable containing PhoundryContentNode objects
	 */
	public function getContentNodesForTags($tags)
	{
		$tag_ids = array();
		foreach ($tags as $tag) {
			$tag_ids[] = $tag instanceof Tagging_Tag ? $tag->id : (int) $tag;
		}
		
		if (!$tag_ids) {
			throw new Tagging_Exception("No tags given");
		}
		
		return $this->db->iQuery(sprintf("
			SELECT DISTINCT
				id, string_id, rid
			FROM
				phoundry_content_node AS c
			INNER JOIN
				phoundry_tag_content_node AS tct
			ON
				c.id = tct.cid
			AND
				tct.tid IN(%s)",
			implode(", ", $tag_ids)
		))->setReturnClass("PhoundryContentNode");
	}
	
	/**
	 * Get all tags
	 * @return Traversable containing Tagging_Tag opbjects
	 */
	public function getAllTags()
	{
		return $this->db->iQuery("
			SELECT
				id, name
			FROM
				phoundry_tag
		")->setReturnClass("Tagging_Tag");
	}
	
	/**
	 * Search for tags with a name like the given name
	 * @param string $name
	 * @return Tagging_Tag[]
	 */
	public function getTagsLikeName($name)
	{
		return $this->db->iQuery(sprintf("
			SELECT
				id, name
			FROM
				phoundry_tag
			WHERE
				name LIKE '%s'",
			$this->db->quote($name)
		))->setReturnClass("Tagging_Tag");
	}

	/**
	 * @param string $name
	 * @param string $string_id
	 * @return PhoundryContentNode[]
	 */
	public function getContentNodesWithTagsLikeName($name, $string_id = null)
	{
		return $this->db->iQuery(sprintf("
			SELECT
				cn.id, cn.string_id, cn.rid
			FROM
				phoundry_content_node AS cn
			INNER JOIN
				phoundry_tag_content_node AS tcn
			ON
				cn.id = tcn.cid
			INNER JOIN
				phoundry_tag AS t
			ON
				t.id = tcn.tid
			AND
				t.name LIKE '%s'
			%s",
			$this->db->quote($name),
			null !== $string_id ?
				"AND cn.string_id = '{$this->db->quote($string_id)}'" : ''
		))->setReturnClass("PhoundryContentNode");
	}
	
	/**
	 * Stores the tag if it is a new tag the id will be set with the new id
	 * @param Tagging_Tag $tag
	 * @return void
	 */
	public function storeTag(Tagging_Tag $tag)
	{
		if (null === $tag->id) {
			$res = $this->db->iQuery(sprintf("
				INSERT INTO phoundry_tag (name)
				VALUES (%s)",
				$this->db->quote($tag->name, true)
			));
			$tag->id = $res->last_insert_id;
		}
		else {
			$this->db->iQuery(sprintf("
				UPDATE phoundry_tag
				SET name = %s
				WHERE id = %d",
				$this->db->quote($tag->name, true),
				$tag->id
			));			
		}
	}
	
	/**
	 * Delete the given tag and all its relations
	 * @param Tagging_Tag $tag
	 */
	public function deleteTag(Tagging_Tag $tag)
	{
		// Delete all relations to collections
		$this->db->iQuery(sprintf("
			DELETE FROM phoundry_tag_content_node
			WHERE tid = %d",
			$tag->id
		));
		
		// Delete the tag itself
		$this->db->iQuery(sprintf("
			DELETE FROM phoundry_tag
			WHERE id = %d",
			$tag->id
		));
	}
	
	/**
	 * Set the tags for a given content node
	 * 
	 * @param array $tags
	 * @param Tagging_Collection $collection
	 * @return void
	 */
	public function setTagsForContentNode(array $tags,
		PhoundryContentNode $node)
	{
		$tag_ids = array();
		foreach ($tags as $tag) {
			$tag_ids[] = $tag->id;
		}
		
		$this->db->iQuery(sprintf("
			DELETE FROM phoundry_tag_content_node
			WHERE cid = %d",
			$node->id
		));
		
		if ($tag_ids) {
			$this->db->iQuery(sprintf("
				INSERT INTO phoundry_tag_content_node (cid, tid) VALUES
				({$node->id}, %s)",
				implode("), ({$node->id}, ", $tag_ids)
			));
		}
	}
}