<?php
/**
 ** The Phoundry Session object.
 **
 ** @author Arjan Haverkamp
 **/
class PHSes {
	public $userId, $userName, $loginName, $email, $groups, $groupId, $isAdmin;
	public $lang, $shownRIDs, $limitRIDs, $customVars, $kind, $challenge;

	/**
	 * Session constructor.
	 * Instantiates an empty PHSes object.
	 */
	public function __construct() {
		$this->userId = '';
		$this->userName = '';
		$this->loginName = '';
		$this->email = '';
		$this->groups = array();
		$this->groupId = -1;
		$this->isAdmin = false;
		$this->lang = '';
		$this->limitRIDs = array();
		$this->shownRIDs = array();
		$this->customVars = array();
		$this->challenge = '';
		$this->kind = 'phoundry';
		$this->lastLoginDate = null;
	}

	/**
	 * Change the group-id for the current user.
	 * The group-id will only be changed, if the user actually belongs to the new group.
	 *
	 * @author Arjan Haverkamp
	 * @param $groupId(int) The user's new group-id.
	 * @return A boolean: true, if the user has indeed access to this group, false otherwise.
	 * @returns boolean
	 * @public
	 */
	public function setGroupId($groupId) {
		if (isset($this->groups[(int)$groupId])) {
			$this->groupId = (int)$groupId;
			$this->shownRIDs = array();
			return true;
		}
		return false;
	}
}
?>
