<?php
/**
 * Abstract class that is not abstract but still should be extended for use
 * 
 * @abstract
 * @author Arjan Haverkamp <arjan.haverkamp@webpower.nl>
 * @copyright 2011 Web Power BV, http://www.webpower.nl
 */
class Datatype
{
	/**
	 * @var Column
	 */
	public $column;
	
	/**
	 * @var string column name as in the database
	 * @see Column#name
	 */
	public $name;
	
	/**
	 * @var int length of column in database
	 * @see Column#DBprops
	 */
	public $maxlength;
	
	/**
	 * @var bool if the column is configured as required in phoundry
	 * @see Column#required
	 */
	public $required;
	
	/**
	 * @var string value copied from $extra['default'] if it exists
	 * @see Datatype#extra['default']
	 */
	public $default;
	
	/**
	 * @var array Options as configured for this datatype
	 */
	public $extra;
	
	/**
	 * @var bool should this datatype be treated as a Xref
	 */
	public $isXref = false;
	
	/**
	 * List of inputtypes that can be used in conjunction with this datatype.
	 * The compatible and incompatible inputtypes will be split into two
	 * seperate select boxes on the column admin page.
	 * @var array 
	 */
	public $compatibleInputtypes = array();

	/**
	 * Initializes the datatype fetching its options from the database
	 * 
	 * @param Column $column the phoundry column this datatype is configured for
	 * @param array $extra if given the extra properties won't be
	 * 					   fetched from the database but use this array instead
	 */
	public function __construct(Column $column, array $extra = null)
	{
		global $db;

		$this->column = $column;
		$this->name = $this->column->name;
		$this->maxlength = isset($column->DBprops['length']) ?
			$column->DBprops['length'] : -1;
		
		$this->required = $column->required;

		if (!is_null($extra)) {
			$this->extra = $extra;
		} else {
			$sql = "SELECT datatype_extra
				FROM phoundry_column
				WHERE id = {$column->id}
			";
			
			$cur = $db->Query($sql) or trigger_error(
				"Query $sql failed: " . $db->Error(), E_USER_ERROR
			);
				
			if (!$db->EndOfResult($cur)) {
				$datatype_extra = $db->FetchResult($cur, 0, 'datatype_extra');
				$this->extra = unserialize($datatype_extra);
			}
			
			if (isset($this->extra['default'])) {
				$this->default = $this->extra['default'];
			} else {
				$this->default = '';
			}
		}
	}

	/**
	 * @see Datatype#compatibleInputtypes
	 * @return array Returns $this->compatibleInputtypes
	 */
	public function getCompatibleInputtypes() 
	{
		return $this->compatibleInputtypes;
	}

	/**
	 * @todo
	 * @return string
	 */
	public function getExplanation() 
	{
		return '';
	}

	/**
	 * @todo
	 * @return string
	 */
	public function getDefaultValue() 
	{
		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}
		return $this->default;
	}

	/**
	 * @todo
	 * @param string $page
	 * @return string
	 */
	public function getSQLpart($page) 
	{
		return $this->name;
	}

	/**
	 * @param string $page Either 'search', 'insert', 'update', 'view' or 'xml'
	 * @param $keyval
	 * @param $cur
	 * @param int $row
	 * @param int $strlength
	 */
	public function getValue($page, $keyval, $cur, $row, $strlength = -1) 
	{
		if (isset($_REQUEST['_' . $this->name])) {
			return $_REQUEST['_' . $this->name];
		}
		
		global $db, $PHprefs;
		$value = $db->FetchResult($cur, $row, $this->name);
		if ($page == 'search' && $strlength != -1) {
			if (function_exists('mb_strlen')) {
				if ($value != null && mb_strlen($value) > $strlength) {
					$value = mb_substr($value, 0, $strlength) . '...';
				}
			} else {
				if ($value != null && strlen($value) > $strlength) {
					$value = substr($value, 0, $strlength) . '...';
				}
			}
		}
		return $value;
	}

	/**
	 * Updates the $fields array with values that should be update/inserted
	 * 
	 * @param string $page the page type for which to fetch
	 * @param array $source mostly directly referring to $_POST
	 * @param array $fields
	 * @param array $extras can contain extra sql statements eg for cleanup
	 * @param int $oldKey
	 */
	public function getSQLvalue($page, &$source, &$fields, &$extras,
		$oldKey = -1)
	{
		if ($page == 'update' && !isset($source[$this->name])) {
			$fields[$this->name] = $this->name; return;
		}

		$value = isset($source[$this->name]) ? $source[$this->name] : '';
		if ($value === '' && !$this->column->required) {
			$fields[$this->name] = isset($this->column->DBprops['default']) ?
				"'" . escSquote($this->column->DBprops['default']) . "'" :
				'NULL';
			
		} else {
			$fields[$this->name] = "'" . trim(escDBquote($value)) . "'";
		}
	}

	/**
	 * Delete stuff that is associated with this datatypes.
	 * For normal datatypes, this doesn't do anything.
	 * For files and cross references the associated records and files
	 * can be deleted.
	 *
	 * @param string $keyName The key-name for the record this column is in.
	 * @param string $keyVal The key-value for the record this column is in.
	 */
	public function deleteStuff($keyName, $keyVal)
	{
		return;
	}

	/**
	 * Get hyperlinks for helper-GUIs in FIELDSETs.
	 *
	 * @author Arjan Haverkamp
	 * @param string $page The page (update, insert, copy) to get helpers for.
	 * @return array of helper-links.
	 */
	public function getHelperLinks($page, $key = null)
	{
		return array();
	}

	public function checkRequired($source) 
	{
		if (!isset($source[$this->name]) || $source[$this->name] === '') 
		{
			return true;
		}
		return false;
	}

	/**
	 * Check this Datatype's syntax.
	 *
	 * @param string $page Either 'insert' or 'update'.
	 * @param array $source An associative array that contains the 'raw' value.
	 *                      Most commonly, $_POST or $_GET are used here.
	 * @param array $errors A reference to the array that will contain the error
	 *                      messages.
	 */
	public function checkSyntaxPHP($page, &$source, &$errors) 
	{
	}

	/**
	 * Retrieve Javascript code to syntactically check this datatype.
	 *
	 * @param string $page Either 'insert' or 'update'
	 * @return string Javascript code, representing this column's syntax check
	 */
	public function checkSyntaxJS($page) 
	{
		return '';
	}

	/**
	 * @return string
	 */
	public function getPattern() 
	{
		return '';
	}

	/**
	 * Retrieve Javascript code for this datatype (used for fake columns).
	 *
	 * @param string $page Currently, only 'overview' is known.
	 * @return string A string of Javascript code
	 */
	public function getJScode($page) 
	{
		return '';
	}

	/**
	 * Retreive info about this datatype, for use in the Phoundry Admin.
	 *
	 * @return array An array with info about this datatype.
	 */
	public function getExtra() 
	{
		$default = isset($this->column->DBprops['default']) ?
			$this->column->DBprops['default'] : '';
		
		if (isset($this->extra['default'])) {
			$default = $this->extra['default'];
		}
		$adminInfo = array();
		$adminInfo['default'] = array(
			'name' => 'DTdefault',
			'label' => 'Default value',
			'required' => false,
			'syntax' => 'text',
			'input' => array(
				'type' => 'textarea',
				'cols' => 60,
				'rows' => 10,
				'value' => $default
			)
		);

		return $adminInfo;
	}


	/**
	 * Store info for this datatype, through the Phoundry Admin.
	 *
	 * @param string $msg Contains an errormessage in case something is wrong.
	 * @return false in case of an error, a datastructure (array) otherwise.
	 * @returns bool|array
	 */
	public function setExtra(&$msg) 
	{
		$msg = '';

		$extra = array(
			'default' => trim($_POST['DTdefault'])
		);

		return $extra;
	}
}
