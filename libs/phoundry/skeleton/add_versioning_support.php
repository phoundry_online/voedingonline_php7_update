<?php
class Script_Add_Versioning_Support
{
	/**
	 * @var resource
	 */
	protected $_db;
	
	public function __construct($db) {
		if(!is_resource($db)) {
			throw new Exception("db should be a resource\n");
		}
		$this->_db = $db;
	}
	
	public function updateAll() 
	{
		global $PHprefs;

		$charset = $PHprefs['charset'] == 'iso-8859-1' ? 'latin1' : 'utf8';
		$this->_query(
			"CREATE TABLE `phoundry_versioning` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `trail_id` int(10) unsigned NOT NULL,
  `table_id` int(10) unsigned NOT NULL,
  `record_id` int(10) unsigned default NULL,
  `parent_id` int(10) unsigned default NULL,
  `site_identifier` varchar(20) NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_name` varchar(80) NOT NULL default '',
  `revision` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('published','pending','draft') NOT NULL,
  `message` text,
  `data` blob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `trail_index` (`trail_id`),
  KEY `record_index` (`table_id`,`record_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;"
		);
		
		$this->_query(
			"CREATE TABLE `phoundry_versioning_notify` (
  `table_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`table_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;"
		);

		print "ALL DONE!\n\n";
	}
	
	protected function _query($sql)
	{
		if($res = mysql_query($sql, $this->_db)) {
			return $res;
		}
		
		throw new Exception('Error '.mysql_error($this->_db).' in query '.
			$sql, (int) mysql_errno($this->_db));
	}
	
	protected function _quote($var)
	{
		return "'".mysql_real_escape_string($var, $this->_db)."'";
	}
}

if (__FILE__ === realpath($_SERVER['SCRIPT_FILENAME'])) {
	if(!isset($argv[1]) && !isset($_GET['prefs'])) {
		die("No Prefs given\n");
	} else {
		$prefsfile = isset($argv[1]) ? $argv[1] : $_GET['prefs'];
	}
	
	if (!is_file($prefsfile)) {
		die("Prefs file not found\n");
	}
	
	require $prefsfile;
	require_once $PHprefs['distDir'].'/core/include/PH.class.php';
	
	if(isset($PHprefs['DBtype']) && $PHprefs['DBtype'] != 'mysql') {
		trigger_error("no support for db type ".$PHprefs['DBtype'] . "\n", E_USER_ERROR);
	}
	
	$db = mysql_connect($PHprefs['DBserver'], $PHprefs['DBuserid'], $PHprefs['DBpasswd']);
	mysql_select_db($PHprefs['DBname'], $db);
	
	if ($PHprefs['charset'] == 'utf-8') {
		mysql_query('SET NAMES utf8', $db);
	}
	else {
		mysql_query('SET NAMES latin1', $db);
	}
	
	$script = new Script_Add_Versioning_Support($db);
	$script->updateAll();
}