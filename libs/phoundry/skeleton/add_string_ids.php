<?php
if(!isset($argv[1])) {
	die('No Prefs given');
}
require $argv[1];

if(isset($PHprefs['DBtype']) && $PHprefs['DBtype'] != 'mysql') {
	trigger_error("no support for db type ".$PHprefs['DBtype'], E_USER_ERROR);
}

require_once $PHprefs['distDir'].'/core/include/PH.class.php';

class Script_Add_String_Ids
{
	/**
	 * @var resource
	 */
	protected $_db;
	
	public function __construct($db) {
		if(!is_resource($db)) {
			throw new Exception("db should be a resource");
		}
		$this->_db = $db;
	}
	
	public function updateAll() {
		$this->alterTables();
		$this->addPhoundryColumns();
		
		$res = $this->_query("
			SELECT
				id,
				name,
				description
			FROM
				phoundry_table
			WHERE
				string_id IS NULL");
		
		while($r = mysql_fetch_assoc($res)) {
			$this->updateTable($r['id'], $r['name'], $r['description']);
		}
		
		$res = $this->_query("
			SELECT
				id,
				name,
				description,
				table_id
			FROM
				phoundry_column
			WHERE
				string_id IS NULL");
		
		while($r = mysql_fetch_assoc($res)) {
			$this->updateColumn($r['id'], $r['name'], $r['description'], $r['table_id']);
		}
		
		$res = $this->_query("
			SELECT
				id,
				event,
				table_id
			FROM
				phoundry_event
			WHERE
				string_id IS NULL");
		
		while($r = mysql_fetch_assoc($res)) {
			$this->updateEvent($r['id'], $r['event'], $r['table_id']);
		}
		
		$res = $this->_query("
			SELECT
				id,
				name,
				table_id
			FROM
				phoundry_filter
			WHERE
				string_id IS NULL");
		
		while($r = mysql_fetch_assoc($res)) {
			$this->updateFilter($r['id'], $r['name'], $r['table_id']);
		}
	}
	
	public function alterTables() {
		$this->_query("
			ALTER TABLE phoundry_table
			ADD COLUMN `string_id` VARCHAR(80) AFTER id,
			ADD	UNIQUE `string_id` (`string_id`)
		");
		$this->_query("
			ALTER TABLE phoundry_column
			ADD COLUMN `string_id` VARCHAR(80) AFTER id,
			ADD UNIQUE `string_id` (`string_id`, `table_id`)
		");
		$this->_query("
			ALTER TABLE phoundry_event
			ADD COLUMN `string_id` VARCHAR(80) AFTER id,
			ADD UNIQUE `string_id` (`string_id`, `table_id`)
		");
		$this->_query("
			ALTER TABLE phoundry_filter
			ADD COLUMN `string_id` VARCHAR(80) AFTER id,
			ADD UNIQUE `string_id` (`string_id`, `table_id`)
		");
	}
	
	public function addPhoundryColumns()
	{
		$this->_query("
		INSERT INTO `phoundry_column` VALUES (NULL,'string_id',6,15,'string_id','String ID','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'');
		");
		$this->_query("
		INSERT INTO `phoundry_column` VALUES (NULL,'string_id',4,15,'string_id','String ID','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'');
		");
		$this->_query("
		INSERT INTO `phoundry_column` VALUES (NULL,'string_id',5,15,'string_id','String ID','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'');
		");
	}

	public function updateTable($id, $name, $desc) {
		// Decide String_id
		if($name) {
			$string_id = $name;
		}
		else if($desc) {
			$string_id = PH::langWord($desc, 'en');
			if(!$string_id) {
				$string_id = PH::langWord($desc, 'nl');
			}
		}
		
		$string_id = $this->_toUri(strtolower($string_id));
		$string_id = str_replace('-', '_', $string_id);
	
		$s_id = $string_id;
		$i = 1;
		
		
		$sql = "SELECT string_id FROM phoundry_table WHERE string_id = ";
		$res = $this->_query($sql.$this->_quote($s_id));
		while(false !== mysql_fetch_assoc($res)) {
			$s_id = $string_id.'_'.$i;
			$i++;
			$res = $this->_query($sql.$this->_quote($s_id));
		}
	
		return false !== $this->_query("UPDATE phoundry_table SET string_id = ".$this->_quote($s_id).
			" WHERE id = ".$id);
	}
	
	public function updateColumn($id, $name, $desc, $table_id) {
		if($name) {
			$string_id = $name;
		}
		else if($desc) {
			$string_id = PH::langWord($desc, 'en');
			if(!$string_id) {
				$string_id = PH::langWord($desc, 'nl');
			}
		}
		$string_id = $this->_toUri(strtolower($string_id));
		$string_id = str_replace('-', '_', $string_id);
	
		$s_id = $string_id;
		$i = 1;
		
		$sql = "SELECT string_id FROM phoundry_column WHERE table_id = ".
			((int) $table_id) ." AND string_id = ";
		$res = $this->_query($sql.$this->_quote($s_id));
		
		while(false !== mysql_fetch_assoc($res)) {
			$s_id = $string_id."_".$i;
			$i++;
			$res = $this->_query($sql.$this->_quote($s_id));
		}
	
		return false !== $this->_query("UPDATE phoundry_column SET string_id = ".$this->_quote($s_id).
			" WHERE id = ".(int) $id);
	}
	
	public function updateEvent($id, $event, $table_id) {
			$string_id = $event;
			$string_id = $this->_toUri(strtolower($string_id));
			$string_id = str_replace('-', '_', $string_id);
		
			$s_id = $string_id;
			$i = 1;
			
			$sql = "SELECT string_id FROM phoundry_event WHERE table_id = ".
				((int) $table_id) . " AND string_id = ";
			$res = $this->_query($sql.$this->_quote($s_id));	
			
			while(false !== mysql_fetch_assoc($res)) {
				$s_id = $string_id."_".($i++);
				$res = $this->_query($sql.$this->_quote($s_id));	
			}
		
			return false !== $this->_query("UPDATE phoundry_event SET ".
				"string_id = ".$this->_quote($s_id)." WHERE id = ".$id);
	}
	
	public function updateFilter($id, $name, $table_id) {
		$string_id = $name;
		$string_id = $this->_toUri(strtolower($string_id));
		$string_id = str_replace('-', '_', $string_id);
	
		$s_id = $string_id;
		$i = 1;
		
		$sql = "SELECT string_id FROM phoundry_filter WHERE table_id = ".
				((int) $table_id) . " AND string_id = ";
		$res = $this->_query($sql.$this->_quote($s_id));
		
		while(false !== mysql_fetch_assoc($res)) {
			$s_id = $string_id."_".($i++);
			$res = $this->_query($sql.$this->_quote($s_id));	
		}
	
		return false !== $this->_query("UPDATE phoundry_filter SET ".
			"string_id = ".$this->_quote($s_id)." WHERE id = ".$id);
	}
	
	protected function _query($sql)
	{
		if($res = mysql_query($sql, $this->_db)) {
			return $res;
		}
		
		throw new Exception('Error '.mysql_error($this->_db).' in query '.
			$sql, (int) mysql_errno($this->_db));
	}
	
	protected function _quote($var)
	{
		return "'".mysql_real_escape_string($var, $this->_db)."'";
	}
	
	protected function _removeAccents($string) {
		$encoding = $this->_isUtf8($string) ? 'UTF-8' : 'ISO-8859-1';
		$string = htmlentities($string, ENT_COMPAT, $encoding);
		
		$string = str_replace(
			array('&Agrave;','&Aacute;','&Acirc;','&Auml;', '&Atilde;', '&Aring;', '&AElig;'),
			'A',
			$string
		);

		$string = str_replace(
			array('&agrave;','&aacute;','&acirc;','&auml;','&aelig;', '&atilde;', '&aring;'),
			'a',
			$string
		);
		
		$string = str_replace(
			array('&Egrave;','&Eacute;','&Ecirc;','&Euml;'),
			'E',
			$string
		);
		
		$string = str_replace(
			array('&egrave;','&eacute;','&ecirc;','&euml;'),
			'e',
			$string
		);
		
		$string = str_replace(
			array('&Igrave;','&Iacute;','&Icirc;','&Iuml;'),
			'I',
			$string
		);
		
		$string = str_replace(
			array('&igrave;','&iacute;','&icirc;','&iuml;'),
			'i',
			$string
		);
		
		$string = str_replace(
			array('&Ograve;','&Oacute;','&Ocirc;','&Ouml;','&Oslash;', '&Otilde;'),
			'O',
			$string
		);
		
		$string = str_replace(
			array('&ograve;','&oacute;','&ocirc;','&ouml;','&oslash;', '&otilde;'),
			'o',
			$string
		);
		
		$string = str_replace(
			array('&Ugrave;','&Uacute;','&Ucirc;','&Uuml;'),
			'U',
			$string
		);
		
		$string = str_replace(
			array('&ugrave;','&uacute;','&ucirc;','&uuml;'),
			'u',
			$string
		);
		
		$string = str_replace(
			array('&Ccedil;'),
			'C',
			$string
		);
		
		$string = str_replace(
			array('&ccedil;', '&cent;'),
			'c',
			$string
		);
		
		$string = str_replace(
			array('&ETH;'),
			'D',
			$string
		);
		
		$string = str_replace(
			array('&eth;'),
			'd',
			$string
		);
		
		$string = str_replace(
			array('&Ntilde;'),
			'N',
			$string
		);
		
		$string = str_replace(
			array('&ntilde;'),
			'n',
			$string
		);
		
		$string = str_replace(
			array('&Yacute;','&Yuml;'),
			'Y',
			$string
		);
		
		$string = str_replace(
			array('&yacute;','&yuml;', '&yen;'),
			'y',
			$string
		);
		
		$string = str_replace(
			array('&times;'),
			'x',
			$string
		);
		
		$string = str_replace(
			array('&szlig;'),
			'ss',
			$string
		);
		
		return html_entity_decode($string, ENT_COMPAT, $encoding);
	}
	
	protected function _toUri($str) {
		return trim(
			preg_replace(
				array('/[^a-z0-9\-\_\/]/i', '/\/+/', '/-+/'),
				array('-', '/', '-'),
				$this->_removeAccents($str)
			),
			'-/'
		);
	}
	
	protected function _isUtf8($string) {
	    return utf8_encode(utf8_decode($string)) == $string;
	}
}

$db = mysql_connect($PHprefs['DBserver'], $PHprefs['DBuserid'], $PHprefs['DBpasswd']);
mysql_select_db($PHprefs['DBname'], $db);

if ($PHprefs['charset'] == 'utf-8') {
	mysql_query('SET NAMES utf8', $db);
}
else {
	mysql_query('SET NAMES latin1', $db);
}

$script = new Script_Add_String_Ids($db);
$script->updateAll();
