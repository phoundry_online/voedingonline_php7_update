CREATE TABLE `phoundry_versioning` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `trail_id` int(10) unsigned NOT NULL,
  `table_id` int(10) unsigned NOT NULL,
  `record_id` int(10) unsigned default NULL,
  `parent_id` int(10) unsigned default NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_name` varchar(80) NOT NULL default '',
  `revision` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('published','pending','draft') NOT NULL,
  `message` text,
  `data` blob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `trail_index` (`trail_id`),
  KEY `record_index` (`table_id`,`record_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `phoundry_versioning_notify` (
  `table_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`table_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

