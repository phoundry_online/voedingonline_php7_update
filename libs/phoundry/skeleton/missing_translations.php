<?php
$langs = array();

/* @var $dir DirectoryIterator */
foreach (new DirectoryIterator(dirname(__FILE__).'/../core/lang') as $dir) {
	if ($dir->isDir() && !$dir->isDot() && substr($dir, 0, 1) !== '.') {
		$coreWORDS = array();
		require $dir->getRealPath() . '/words.php';
		$langs[$dir->__toString()] = $coreWORDS;
		unset($coreWORDS);
	}
}

$keys = array();
foreach ($langs as $lang) {
	$keys = array_merge($keys, array_keys($lang));
}
$keys = array_unique($keys);

$missing = array();
foreach ($langs as $name => $lang) {
	$diff = array_diff($keys, array_keys($lang));
	if ($diff) {
		$missing[$name] = $diff;
	}
}

if ($missing) {
	echo 'Missing translations.'."\n";
	foreach ($missing as $lang => $codes) {
		echo $lang.': '.implode(', ',$codes) . "\n";
	}
	exit(1);
}

exit(0);
