<?php
if(!isset($argv[1])) {
	die("No Prefs given\n");
}
require $argv[1];

if(isset($PHprefs['DBtype']) && $PHprefs['DBtype'] != 'mysql') {
	trigger_error("no support for db type ".$PHprefs['DBtype'] . "\n", E_USER_ERROR);
}

require_once $PHprefs['distDir'].'/core/include/PH.class.php';

class upgradeTo59
{
	/**
	 * @var resource
	 */
	protected $_db;
	
	public function __construct($db) {
		if(!is_resource($db)) {
			throw new Exception("db should be a resource\n");
		}
		$this->_db = $db;
	}
	
	public function updateAll() 
	{
		global $PHprefs;

		$charset = $PHprefs['charset'] == 'iso-8859-1' ? 'latin1' : 'utf8';

		$sql = "ALTER TABLE phoundry_user MODIFY password varchar(80) not null";
		$res = $this->_query($sql);
		$sql = "ALTER TABLE phoundry_user DROP admin";
		$res = $this->_query($sql);
		$sql = "UPDATE phoundry_user SET password = '' WHERE id = 1";
		$res = $this->_query($sql);
		$sql = "ALTER TABLE phoundry_login MODIFY ip_address varchar(40) NOT NULL";
		$res = $this->_query($sql);

		// Update phoundry_user.password's:
		$sql = "SELECT id, password FROM phoundry_user WHERE LENGTH(password) = 32";
		$cur = $this->_query($sql);
		while($row = mysql_fetch_assoc($cur)) {
			$newPassword = sha1($row['id'] . '*&|^%$$^*%(U(&*^$!BFFjnnfidu+O}=P:":L:KJN' . $row['password']);
			$sql = "UPDATE phoundry_user SET password = '{$newPassword}' WHERE id = {$row['id']}";
			$res = $this->_query($sql);
		}

		// What is the ID of the phoundry_user table?
		$sql = "SELECT id FROM phoundry_table WHERE name = 'phoundry_user'";
		$cur = $this->_query($sql);
		if ($row = mysql_fetch_assoc($cur)) {
			$TID = $row['id'];
		}
		else {
			trigger_error("phoundry_user table not found!\n", E_USER_ERROR);
		}
		// What is the ID fo the phoundry_password column?
		$sql = "SELECT id FROM phoundry_column WHERE name = 'password' AND table_id = {$TID}";
		$cur = $this->_query($sql);
		if ($row = mysql_fetch_assoc($cur)) {
			$CID = $row['id'];
		}
		else {
			trigger_error("phoundry_user.password column not found!\n", E_USER_ERROR);
		}
		$sql = "DELETE FROM phoundry_column WHERE id = {$CID}";
		$res = $this->_query($sql);

		$sql =<<<EOS
INSERT INTO `phoundry_column` VALUES ({$CID},'password',1,14,'password','a:2:{s:2:\"nl\";s:10:\"Wachtwoord\";s:2:\"en\";s:8:\"Password\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:6:\"wpsha1\";}','ITpassword','a:3:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:12:\"showStrength\";b:1;}',1,4,'')
EOS;
		$res = $this->_query($sql);

		$sql = "DELETE FROM phoundry_filter WHERE id = 1";
		$res = $this->_query($sql);

		$sql =<<<EOS
INSERT INTO `phoundry_filter` VALUES (1,'hide_webpower_user',1,'system',0,NULL,'Hide webpower user',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:8:\"username\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:8:\"webpower\";}}')
EOS;
		$res = $this->_query($sql);

		$sql = "DELETE FROM phoundry_column WHERE id = 16";
		$res = $this->_query($sql);
		$sql =<<<EOS
INSERT INTO `phoundry_column` VALUES (16,'phoundry_user_group',2,40,'phoundry_user_group','a:2:{s:2:\"nl\";s:10:\"Gebruikers\";s:2:\"en\";s:5:\"Users\";}','DTXref','a:2:{s:7:\"src_col\";s:8:\"group_id\";s:7:\"dst_col\";s:7:\"user_id\";}','ITXduallist','a:5:{s:5:\"query\";s:61:\"SELECT id, name FROM phoundry_user WHERE id > 1 ORDER BY name\";s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'');
EOS;
		$res = $this->_query($sql);


		$sql = "CREATE TABLE `phoundry_login_attempt` (`ip_address` varchar(40) not null, `nr_attempts` integer not null, `last_attempt` integer unsigned not null, PRIMARY KEY (`ip_address`)) ENGINE=MyISAM DEFAULT CHARSET={$charset}";
		$res = $this->_query($sql);

		$sql = "CREATE TABLE `phoundry_setting` (`id` int unsigned NOT NULL auto_increment, `name` varchar(40) not null, `type` varchar(40) not null, `value` varchar(255) not null, `history` text not null default '', PRIMARY KEY  (`id`), UNIQUE KEY `unq_name` (`name`)) ENGINE=MyISAM DEFAULT CHARSET={$charset}"; 
		$res = $this->_query($sql);

		$value = isset($PHprefs['useSafeLogin']) && $PHprefs['useSafeLogin'] == true ? 1 : 0;
		$sql = "INSERT INTO `phoundry_setting` (name, type, value) VALUES ('safe_login', 'security', {$value})";
		$res = $this->_query($sql);
		
		$sql = "INSERT INTO `phoundry_setting` (name, type, value) VALUES ('password_reset_interval', 'security', 0)";
		$res = $this->_query($sql);

		// Lookup first available table-id:
		$newID = 1;
		while(true) {
			$sql = "SELECT id FROM phoundry_table WHERE id = $newID";
			$cur = $this->_query($sql);
			if ($row = mysql_fetch_assoc($cur)) {
				$newID++; continue;
			}
			break;
		}
		$sql =<<<EOS
INSERT INTO `phoundry_table` VALUES ({$newID},'security_center',100035,'','a:2:{s:2:\"nl\";s:24:\"Beveiligingsinstellingen\";s:2:\"en\";s:17:\"Security settings\";}',NULL,'','',1,'service','a:3:{s:4:\"sUrl\";s:20:\"core/securityCenter/\";s:17:\"rolerights_global\";b:0;s:17:\"rolerights_detail\";b:0;}')
EOS;
		$res = $this->_query($sql);

		$sql = "INSERT INTO `phoundry_group_table` VALUES (1,{$newID},'iudpnmx')";
		$res = $this->_query($sql);

		print "ALL DONE!\n\n";
		print "Please modify this license's PREFS.php file, and remove the following settings:\n";
		print "pwdExpire, useSafeLogin\n";
	}
	
	protected function _query($sql)
	{
		if($res = mysql_query($sql, $this->_db)) {
			return $res;
		}
		
		throw new Exception('Error '.mysql_error($this->_db).' in query '.
			$sql, (int) mysql_errno($this->_db));
	}
	
	protected function _quote($var)
	{
		return "'".mysql_real_escape_string($var, $this->_db)."'";
	}
}

$db = mysql_connect($PHprefs['DBserver'], $PHprefs['DBuserid'], $PHprefs['DBpasswd']);
mysql_select_db($PHprefs['DBname'], $db);

if ($PHprefs['charset'] == 'utf-8') {
	mysql_query('SET NAMES utf8', $db);
}
else {
	mysql_query('SET NAMES latin1', $db);
}

$script = new upgradeTo59($db);
$script->updateAll();
