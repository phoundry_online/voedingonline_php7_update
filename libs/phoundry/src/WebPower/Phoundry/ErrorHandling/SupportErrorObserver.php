<?php
namespace WebPower\Phoundry\ErrorHandling;

class SupportErrorObserver implements ErrorObserver
{
	private $hasError = false;
	private $get;
	private $post;
	private $server;
	private $supportUrl;

	function __construct($get, $post, $server, $supportUrl)
	{
		$this->get = $get;
		$this->post = $post;
		$this->server = $server;
		$this->supportUrl = $supportUrl;
	}

	public function notify(Error $error, $source, array $backtrace)
	{
		// Log errors to PHP error log:
		$errorID = uniqid('');
		$chunks = str_split("{$error->getType()}|{$error->errfile} ($error->errline)|{$error->errstr}", 486);
		foreach ($chunks as $chunk) {
			error_log($errorID . '|' . $chunk);
		}

		if ($error->errno == E_NOTICE || $error->errno == E_USER_NOTICE) { // Don't bother users with NOTICEs, only log them.
			return;
		}

		// Create HTML error message, store in session:
		$this->createHtmlMessage($error, $backtrace);

		return;
	}

	/**
	 * @param Error $errObj
	 * @param array $backtrace
	 */
	private function createHtmlMessage(Error $errObj, array $backtrace)
	{
		$error = array();
		if (isset($this->server['REQUEST_URI'])) {
			$error['URL'] = (isset($this->server['HTTPS']) && $this->server['HTTPS'] == 'on' ? 'https://' : 'http://') . $this->server['HTTP_HOST'] . $this->server['REQUEST_URI'];
		}
		$error['Date/Time'] = date('Y-m-d H:i:s');
		$error['Error type'] = $errObj->getType();
		$error['Error message'] = $errObj->errstr;
		$error['Error file'] = $errObj->errfile;
		$error['Error line'] = $errObj->errline;
		$error['_GET array'] = print_r($this->get, true);
		$error['_POST array'] = print_r($this->post, true);
		$error['_SERVER array'] = print_r($this->server, true);
		$error['Backtrace'] = print_r($backtrace, true);

		if (!$this->hasError) {
			// Often, chained errors occur (one error causes another one). We only need to be informed about the first one.
			$this->hasError = true;
			$_SESSION['error'] = $error;
			print '<div style="position:absolute;z-index:9999;background:#eee;border:2px solid red;text-align:left;font:10pt Arial,Helvetica;margin:2px;padding:7px;-moz-opacity:0.85; filter:alpha(opacity=85);-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px">';
			print '<b style="font-size:11pt">We are sorry to inform you that an error has occured.</b><div style="border:1px dotted #666;padding:3px;margin:2px 0">What should you do?<ul><li>Please try what you were doing again;</li><li>Error occurs again? <a href="' . \PH::htmlspecialchars($this->supportUrl) . '" target="PHcontent" style="color:#0046d5;font-weight:bold">Click here</a> to send the error message to Phoundry so they can have a look at it.</li></ul>Our apologies for the inconvenience!</div><small onclick="this.parentNode.style.display=\'none\'" style="cursor:pointer">[click to close box]</small>';
			print '</div>';
		}
	}
}
