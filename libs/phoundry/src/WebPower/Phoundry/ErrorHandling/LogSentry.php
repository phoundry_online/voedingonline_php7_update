<?php

namespace WebPower\Phoundry\ErrorHandling;

/**
 * Error observer which sends an sentry log to the central server
 *
 * @author Martijn van Maurik <martijn.van.maurik@phoundry.nl>
 * @version 0.1
 */
class LogSentry implements ErrorObserver
{
    /**
     * @var \Raven_Client
     */
    private $client;

    /**
     * @var
     */
    private $error_level;

    /**
     * @param string $raven_dsn
     * @param $error_level
     */
    public function __construct($raven_dsn, $error_level)
    {
        $this->client = new \Raven_Client($raven_dsn);

        $this->error_level = $error_level;
    }

    /**
     * This method gets called by the ErrorHandler for each error that occurs
     *
     * @param \ErrorVoedingonline $error
     * @param string $source
     * @param array $backtrace
     */
    public function notify(\ErrorVoedingonline $error, $source, array $backtrace)
    {
        if (($this->error_level & $error->errno) === $error->errno) {
            global $PRODUCT;

            $data['tags']['release'] = $PRODUCT['version'];
            $data['message'] = $error->errstr;
            $data['level'] = $this->client->translateSeverity($error->errno);
            $data['tags']['php_version'] = phpversion();
            $this->client->capture($data, $backtrace);
        }
    }
}