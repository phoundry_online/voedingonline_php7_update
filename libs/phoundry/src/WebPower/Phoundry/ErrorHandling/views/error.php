<!doctype html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Error</title>
	<style type="text/css">
		body {
			font-family: Arial, Courier New, sans-serif;
			font-size: 10pt;
			margin: 0 auto;
			width: 940px;
			position: relative;
		}

		.line {
			font-weight: bold;
		}

		#source_btn,
		#backtrace div.file {
			cursor: pointer;
		}

		#source,
		.source {
			display: none;
			padding: 3px;
		}

		#source_btn {
			border: 1px solid #c00;
			background-color: #ee7;
			padding: 3px;
		}

		#source_btn span#message {
			color: #c00;
			font-weight: bold;
		}

		span.file:hover {
			color: #005;
			text-decoration: underline;
		}

		h1 {
			font-family: Arial, sans-serif;
			font-size: 10pt;
			font-weight: bold;
			background-color: #c00;
			color: #fff;
			padding: 3px;
			margin: 0;
			text-transform: uppercase;
		}

		ol {
			margin-top: 0;
			border-left: 1px solid #c00;
			border-right: 1px solid #c00;
			border-bottom: 1px solid #c00;
		}

		#source {
			border-left: 1px solid #c00;
		}

		ol div.file {
			margin-left: -40px;
			padding-left: 40px;
			background-color: #ee7;
		}

		ol .source {
			background-color: #fff;
			padding: 3px;
		}
		ol li {
			margin-bottom: 10px;
		}
	</style>
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script type="text/javascript">
		jQuery(function ($) {
			$('#source_btn').click(function() {
				$('#source').toggle();
			});

			$('div.file').click(function() {
				$(this).next('.source').toggle();
			});
		});
	</script>
</head>
<body>
<div id="source_btn">
	Error in file <span class="file"><?=$file?></span>
	on line <span class="line"><?=$line?></span>:<br/>
	<span id="code"><?=$code?></span>:
	<span id="message"><?=$message?></span>
</div>
<div id="source"><?=$source?></div>
<h1>Backtrace</h1>
<ol id="backtrace" style="">
<?php
foreach ($backtrace as $bt):
$args = array();
if (isset($bt['args']) && is_array($bt['args'])) {
	foreach ($bt['args'] as $arg) {
		$args[] = nl2br(htmlspecialchars(print_r($arg, true)));
	}
}
?>
	<li>
		<?if (isset($bt['file'])): ?>
			<div class="file"><span class="file"><?=$bt['file']?></span>
				<span class="line"><?=$bt['line']?></span></div>
			<div class="source"><?=$this->getSource($bt['file'], $bt['line'])?></div>
		<? endif?>
		<div>
			<?if (isset($bt['class'])): ?>
				<?= $bt['class'] ?>
				<?= $bt['type'] ?>
			<? endif?>
			<?=$bt['function']?>(<?=implode(', ', $args)?>)
		</div>
	</li>
<?php endforeach; ?>
</ol>
</body>
</html>