<?php
namespace WebPower\Phoundry\ErrorHandling;

class HtmlPageErrorObserver implements ErrorObserver
{
	private $template;
	private $charset;
	private $debug;

	public static function getDefaultView()
	{
		return __DIR__ . '/views/error.php';
	}

	public function __construct($charset, $debug = false)
	{
		$this->setTemplate(self::getDefaultView());
		$this->charset = $charset;
		$this->debug = $debug;
	}

	public function setTemplate($template)
	{
		if (!is_file($template)) {
			throw new \InvalidArgumentException();
		}
		$this->template = $template;
		return $this;
	}

	public function notify(\ErrorVoedingonline $error, $source, array $backtrace)
	{
		$source = (string) $source;

		if(!$this->debug) {
			$source = '';
			$backtrace = array();
		}

		if(!headers_sent()) {
			header('HTTP/1.1 500 Internal Server Error', true, 500);
			header('Content-Type: text/html; charset=' . strtoupper($this->charset));
		}

		$msg = $this->_includeArgs(
			array(
				'code' => $error->errno,
				'message' => $error->errstr,
				'file' => $error->errfile,
				'line' => $error->errline,
				'backtrace' => $backtrace,
				'source' => $source,
			)
		);

		echo $msg;

		if($this->debug) {
			exit(1);
		}
	}

	/**
	 * Used to include template files with global vars
	 *
	 * @param array $args vars which will be in the scope of the template
	 * @return string
	 */
	private function _includeArgs($args)
	{
		extract($args, EXTR_SKIP);
		ob_start();
		include($this->template);
		return ob_get_clean();
	}

	/**
	 * Get a syntax highlighted source code snippet
	 *
	 * @param string $file
	 * @param int $file_line
	 * @param int $topoffset
	 * @param int $botoffset
	 * @return string
	 */
	public function getSource($file, $file_line, $topoffset = 5, $botoffset = 5)
	{
		if(file_exists($file) && $file = file($file)) {
			$startrow = max(0, $file_line -$topoffset);
			$buffer = '<?php';
			for($line = $startrow; $line < $file_line + $botoffset; $line++) {
				if(isset($file[$line])) {
					$row = $line + 1;
					$buffer .= "\n/*{$row}*/ ".trim($file[$line], "\n");
				}
			}
			$buffer .= "\n?>";
			return highlight_string($buffer, 1);
		}

		return 'No source found';
	}
}