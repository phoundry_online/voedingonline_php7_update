<?php
namespace WebPower\Phoundry\ErrorHandling;

interface ErrorObserver
{
	public function notify(Error $error, $source, array $backtrace);
}
