<?php
namespace WebPower\Phoundry\ErrorHandling;

class Error
{
    public $errno;
    public $errstr;
    public $errfile;
    public $errline;
    public $backtrace;

    public function __construct($errno, $errstr, $errfile = '', $errline = '', array $backtrace = array())
    {
        $this->errno = $errno;
        $this->errstr = $errstr;
        $this->errfile = $errfile;
        $this->errline = $errline;
        $this->backtrace = $backtrace;
    }

    public function getType()
    {
        switch ($this->errno) {
            case 1: // E_ERROR
                $message = 'Run-time Error';
                break;

            case 2: // E_WARNING
                $message = 'Warning';
                break;

            case 4: // E_PARSE
                $message = 'Parse Error';
                break;

            case 8: // E_NOTICE
                $message = 'Notice';
                break;

            case 16: // E_CORE_ERROR
                $message = 'Core Error';
                break;

            case 32: // E_CORE_WARNING
                $message = 'Core Warning';
                break;

            case 64: // E_COMPILE_ERROR
                $message = 'Zend Compile Error';
                break;

            case 128: // E_COMPILE_WARNING
                $message = 'Compile-time Warnings';
                break;

            case 256: // E_USER_ERROR
                $message = 'User Error';
                break;

            case 512: // E_USER_WARNING
                $message = 'User Warning';
                break;

            case 1024; // E_USER_NOTICE
                $message = 'User Notice';
                break;

            case 2048: // E_STRICT
                $message = 'Strict Notice';
                break;

            case 4096: // E_RECOVERABLE_ERROR
                $message = 'Catchable Fatal Error';
                break;

            case 8192: // E_DEPRECATED
                $message = 'Deprecated Warning';
                break;

            case 16384: // E_USER_DEPRECATED
                $message = 'User Deprecated Warning';
                break;

            default:
                $message = sprintf('Unknown number: %s', $this->errno);
                break;
        }

        return $message;
    }

    /**
     * Test if an object is of type error
     *
     * @param $e object
     * @return Boolean
     */
    public static function isError($e)
    {
        return $e instanceof self;
    }
}
