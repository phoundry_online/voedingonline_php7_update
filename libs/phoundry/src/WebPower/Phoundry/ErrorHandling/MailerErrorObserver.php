<?php
namespace WebPower\Phoundry\ErrorHandling;

use InvalidArgumentException;

class MailerErrorObserver implements ErrorObserver
{
	/** @var array */
	private $recipientList;
	/** @var array */
	private $server;
	/** @var string */
	private $fromEmail;
	/** @var string */
	private $fromName;

	private $subject;
	private $headers;
	private $message;

	private $mimeBoundary;

	public function __construct(array $recipientList, array $server, $fromEmail = null, $fromName = null)
	{
		foreach ($recipientList as $recipient) {
			if (!filter_var($recipient, FILTER_VALIDATE_EMAIL)) {
				throw new InvalidArgumentException("Recipients contains a ".
					"invalid emailadress");
			}
		}

		$this->recipientList = $recipientList;
		$this->server = $server;
		$this->fromEmail = $fromEmail ? $fromEmail : 'errormailer@webpower.nl';
		$this->fromName = $fromName ? $fromName : 'Error Mailer';
	}


	public function notify(\ErrorVoedingonline $error, $source, array $backtrace)
	{
		$this->prepareMail($error, $source, $backtrace);
		foreach ($this->recipientList as $recipient) {
			mail($recipient, $this->subject, $this->message, $this->headers);
		}
	}

	/**
	 * @param \ErrorVoedingonline $error
	 * @param $source
	 * @param array $backtrace
	 */
	private function prepareMail(\ErrorVoedingonline $error, $source, array $backtrace)
	{
		$url = $this->getUrl();

		$referer = isset($this->server['HTTP_REFERER']) ?
			'Referer: ' . $this->server['HTTP_REFERER'] . "<br>\n" : '';

		$this->subject = "Error Mailer: " . $error->errstr;

		$this->message = sprintf(
			"%d: %s in file: %s at line: %d<br>
			URL: %s<br>
			UA: %s<br>
			%s%s",
			$error->errno,
			$error->errstr,
			$error->errfile,
			$error->errline,
			$url,
			isset($this->server['HTTP_USER_AGENT']) ? $this->server['HTTP_USER_AGENT'] : '',
			$referer,
			$source
		);

		$this->headers = "From: {$this->fromName} <{$this->fromEmail}>";

		$semi_rand = md5(time());
		$this->mimeBoundary = "==Multipart_Boundary_x{$semi_rand}x";
		$this->headers .= "\nMIME-Version: 1.0\n" .
			"Content-Type: multipart/mixed;\n" .
			" boundary=\"{$this->mimeBoundary}\"";

		$this->message = "--{$this->mimeBoundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" .
			"Content-Transfer-Encoding: 7bit\n\n" . $this->message . "\n\n";

		$this->addAttachment('backtrace.txt', print_r($backtrace, true));
	}

	/**
	 * @return string
	 */
	private function getUrl()
	{
		if (isset($this->server['HTTP_HOST'])) {
			$url = (isset($this->server['HTTPS']) ? 'https://' : 'http://') .
				$this->server['HTTP_HOST'] . $this->server['REQUEST_URI'];
			return $url;
		} else {
			$url = 'Probably Commandline';
			return $url;
		}
	}

	private function addAttachment($filename, $data)
	{
		$data = chunk_split(base64_encode($data));
		$this->message .= "--{$this->mimeBoundary}\n";
		$this->message .= "Content-Type: text/plain; name=\"{$filename}\"\n";
		$this->message .= "Content-Description: \"{$filename}\"\n";
		$this->message .= "Content-Disposition: attachment;\n filename=\"{$filename}\"; size=".strlen($data).";\n";
		$this->message .= "Content-Transfer-Encoding: base64\n\n{$data}\n\n";
	}
}