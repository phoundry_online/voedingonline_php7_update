<?php
namespace WebPower\Phoundry\ErrorHandling;

class ErrorHandler
{
	/** @var ErrorObserver[] */
	private $observerList;
	private $debug;

	function __construct()
	{
		$this->debug = false;
		$this->observerList = array();
	}

	public function addObserver(ErrorObserver $observer)
	{
		$this->observerList[] = $observer;
	}

	public function setDebug($state)
	{
		$this->debug = $state;
	}

	public function register()
	{
		ini_set('display_errors', 0);
		set_error_handler(array($this, 'handleError'));
		set_exception_handler(array($this, 'handleException'));
		register_shutdown_function(array($this, 'shutdown'));
	}

	public function handleError($errno, $errstr, $errfile, $errline)
	{
		// This happens when PHP5 is used and PHP4-functionality is
		// encountered: we need to continue as if nothing happened.
		if((int) PHP_VERSION >= 5 && $errno == E_STRICT) {
			return false;
		}
		// error_reporting value of 0 means the @ construct is used
		if(error_reporting() === 0) {
			return false;
		}

		$backtrace = array_slice(debug_backtrace(), 1);
		$this->notify(new Error($errno, $errstr, $errfile, $errline, $backtrace));

		// We never want the normal error handler to handle errors
		// @see http://php.net/set_error_handler
		return true;
	}

	/**
	 * @param \Throwable|\Exception $exception
	 */
	public function handleException($exception)
	{
		$error = new Error(
			E_USER_ERROR,
			$exception->getMessage(),
			$exception->getFile(),
			$exception->getLine(),
			$exception->getTrace()
		);
		$this->notify($error);
	}

	public function shutdown()
	{
		$err = error_get_last();
		if (is_array($err) && in_array($err['type'], array(E_ERROR, E_COMPILE_ERROR))) {
			if (preg_match('/allowed memory size of \d+ bytes exhausted/i', $err['message'])) {
				$limit = (int) ini_get('memory_limit');
				ini_set('memory_limit', ($limit + 4) .'M');
			}
			$this->handleError($err['type'], $err['message'], $err['file'], $err['line']);
		}
	}

	private function notify(Error $error)
	{
		$source = $this->highlightSource($error->errfile, $error->errline);
		$backtrace = $this->prettifyBacktrace($error->backtrace);

		foreach($this->observerList as $observer) {
			$observer->notify($error, $source, $backtrace);
		}

		if($error->errno == E_USER_ERROR || $this->debug) {
			exit(1);
		}
	}

	private function highlightSource($error_file, $error_line)
	{
		$source = '';
		if(file_exists($error_file)) {
			if($file = file($error_file)) {
				$startrow = max(0, $error_line -5);
				$buffer = '<?php';
				for($line = $startrow; $line < $error_line + 5; $line++) {
					if(isset($file[$line])) {
						$row = $line + 1;
						$buffer .= "\n/*{$row}*/ ".trim($file[$line], "\n");
					}
				}
				$source = highlight_string($buffer, true);
			} else {
				$source = 'No source found';
			}
		}
		return $source;
	}

	private function prettifyBacktrace($error_backtrace)
	{
		$backtrace = array();
		foreach ($error_backtrace as $er) {
			$bt = array();

			foreach($er as $k => $v) {
				if($k == 'args' && is_array($v)) {
					foreach($er['args'] as $argument) {
						if(is_object($argument)) {
							$bt[$k][] = get_class($argument).'()';
						}
						else if(is_array($argument)) {
							$aTmp = array();
							foreach($argument as $key => $val) {
								if(strstr(strtolower($key), 'passw')) {
									$aTmp[$key] = 'XXXXXXXXXXXXXXXX';
								}
								else if(is_array($val)) {
									$aTmp[$key] = 'Array( ... )';
								}
								else if(is_object($val)) {
									$aTmp[$key] = get_class($val) . '()';
								}
								else {
									$aTmp[$key] = var_export($val, true);
								}
							}
							$bt[$k][] = $aTmp;
						}
						else {
							$bt[$k][] = var_export($argument, true);
						}

					}
				}
				else {
					$bt[$k] = $v;
				}
			}

			$backtrace[] = $bt;
		}
		return $backtrace;
	}
}
