<?php


namespace WebPower\Phoundry\ErrorHandling;


class LogErrorObserver implements ErrorObserver
{
    /**
     * @var string The file where logs are being written to
     */
    protected $_logfile;

    /**
     * Creates a new ErrorLogger and validates and creates the logfile
     * @param string $file path to logfile
     */
    public function __construct($file = null)
    {
        if (is_file($file)) {
            $this->_logfile = $file;
        }
    }

    /**
     * (non-PHPdoc)
     * @see classes/ErrorObserver#notify()
     * @param \ErrorVoedingonline $obj
     * @param $source
     * @param array $backtrace
     */
    public function notify(\ErrorVoedingonline $obj, $source, array $backtrace)
    {
        $msg = sprintf("%d: %s in file: %s at line: %d",
            $obj->errno,
            $obj->errstr,
            basename($obj->errfile),
            $obj->errline
        );

        if($this->_logfile) {
            error_log('['.date ("D d M Y H:i:s").'] [error] '.$msg."\n", 3,
                $this->_logfile);
        }
        else {
            error_log($msg);
        }
    }
}

