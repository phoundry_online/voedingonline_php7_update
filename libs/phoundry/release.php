#!/usr/bin/php
<?php
$sourceDir = __DIR__;
require $sourceDir . '/VERSION';
if (!isset($PRODUCT, $PRODUCT['version'], $PRODUCT['name'])) {
    echo 'VERSION file is missing or broken in root'.PHP_EOL;
    exit(1);
}
$encodeDir = $sourceDir .'.enc';
$destinationDir = dirname($sourceDir) . '/' . strtolower($PRODUCT['name']) . $PRODUCT['version'] . '.enc';
$archive = dirname($sourceDir) . '/' . basename($destinationDir) . '.tar.gz';
if (is_dir($encodeDir)) {
    echo $encodeDir . ' already exists' . PHP_EOL;
    exit(1);
}
if (is_dir($destinationDir)) {
    echo $destinationDir . ' already exists' . PHP_EOL;
    exit(1);
}
if (is_file($archive)) {
    echo $archive . ' already exists' . PHP_EOL;
    exit(1);
}
if (!is_writeable(dirname($archive))) {
    echo dirname($archive) . ' is not writable' . PHP_EOL;
    exit(1);
}

chdir($sourceDir);
echo shell_exec('./encode.sh');
if (!is_dir($encodeDir)) {
    echo 'Encode command should\'ve generated ' . $encodeDir . ' but it does not exist' . PHP_EOL;
    exit(1);
}

rename($encodeDir, $destinationDir);
if (!is_dir($destinationDir)) {
    echo 'Encoded sources should be in ' . $destinationDir . ' but it does not exist' . PHP_EOL;
    exit(1);
}

chdir(dirname($destinationDir));
echo shell_exec('tar -zcf '.escapeshellarg($archive). ' '.escapeshellarg(basename($destinationDir)));
if (!is_file($archive)) {
    echo 'Archive should be at ' . $archive . ' but it is not.'.PHP_EOL.
        'Encoded sources are at ' . $destinationDir . PHP_EOL;
    exit(1);
}
echo shell_exec('rm -Rf ' . escapeshellarg($destinationDir));
echo 'Succesfully generated ' . $archive . PHP_EOL;
exit(0);
