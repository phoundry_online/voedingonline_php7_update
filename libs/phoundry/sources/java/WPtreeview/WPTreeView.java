// Decompiled by Jad v1.5.7f. Copyright 2000 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/SiliconValley/Bridge/8617/jad.html
// Decompiler options: packimports(3) 

import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;
import java.util.Vector;
import netscape.javascript.JSObject;

public class WPTreeView extends Applet
    implements Runnable
{

    public WPTreeView()
    {
        jm40 = false;
        jm41 = new Image[100];
        jm42 = 0;
        jm45 = 0;
        jm55 = -1;
        jm71 = -1;
        jm72 = -1;
        jm78 = new Image[3];
        jm79 = new Graphics[3];
        jm80 = new int[28];
        jm84 = new Color[30];
        jm85 = new Font[30];
        jm86 = 0;
        jm87 = 0;
        jm88 = -1;
        jm90 = false;
        jm91 = new MediaTracker(this);
        jm92 = false;
        jm100 = new float[2];
        jm101 = new int[2];
        jm102 = new int[2];
        jm103 = new int[2];
        jm104 = new boolean[2];
        jm105 = new int[2];
        jm106 = new int[2];
        jm107 = new float[2];
        jm108 = new float[2];
        jm109 = new float[2];
        jm110 = new float[2];
        jm111 = new int[2];
        jm112 = new int[2];
        jm113 = new int[2];
        jm114 = new float[2];
        jm115 = new boolean[2];
        jm116 = new boolean[2];
        jm117 = new boolean[2];
        jm118 = new boolean[2];
        jm122 = 1;
        jm123 = "";
        jm125 = "";
        jm126 = null;
        jm127 = "";
    }

    public void init()
    {
        jm115[0] = true;
        jm115[1] = true;
        jm116[0] = true;
        jm116[1] = true;
        for(int i = 0; i < 28; i++)
            jm80[i] = Integer.parseInt(getParameter(jm81[i]));

        jm128 = size().height;
        jm129 = size().width;
        jm89 = getParameter("FunctionNames");
        jm43 = new WPTreeNode[jm80[0]];
        jm53 = new int[jm80[0]];
        jm54 = new Vector(jm80[0]);
        setLayout(null);
        jm25();
        jm77 = createImage(jm129, 2 * jm128);
        jm82 = jm77.getGraphics();
        jm26(jm85, "TreeFont", 2);
        jm26(jm84, "TreeColor", 3);
        if(jm80[24] == 1)
            try
            {
                jm91.waitForAll();
            }
            catch(InterruptedException interruptedexception) { }
        try
        {
            jm91.waitForID(0);
        }
        catch(InterruptedException interruptedexception1) { }
        CreateBackground(0, jm80[22]);
        SetDescriptionFont(jm80[27]);
        GetTreeNodes();
        jm83 = getGraphics();
        jm76 = new Thread(this);
    }

    public void start()
    {
        jm80[23] = Integer.parseInt(getParameter("BlockStartPaint"));
        TreeResize(jm80[9], jm80[10]);
        jm21();
        if(jm76 == null)
            jm76 = new Thread(this);
        jm46 = true;
        jm76.start();

		String selNode = getParameter("NodeSelected");
		if (selNode != null)
		{
			jm87 = Integer.parseInt(selNode);
			Refresh();
		}
    }

    public synchronized void TreeResize(int i, int j)
    {
        if(i < 80)
            i = 80;
        if(j < 80)
            j = 80;
        jm80[9] = i;
        jm80[10] = j;
        float f = jm100[1];
        float f1 = jm100[0];
        jm30(0, jm80[10] - 17, jm80[9] - 17, 17, 1, 0);
        jm30(jm80[9] - 17, 0, 17, jm80[10] - 17, 0, 1);
        jm18();
        jm17();
        if(jm104[0])
            ScrollTo(f1, 0);
        if(jm104[1])
            ScrollTo(f, 1);
        paint(jm83);
    }

    public int GetTreeWidth()
    {
        return jm80[9];
    }

    public int GetTreeHeight()
    {
        return jm80[10];
    }

    public void SetGeneral(String s, int i)
    {
        for(int j = 0; j < 28; j++)
            if(s.compareTo(jm81[j]) == 0)
            {
                jm80[j] = i;
                return;
            }

    }

    public int GetGeneral(String s)
    {
        for(int i = 0; i < 28; i++)
            if(s.compareTo(jm81[i]) == 0)
                return jm80[i];

        return -1;
    }

    public void DisableLinks(boolean flag)
    {
        jm92 = flag;
    }

    public void SetTreeFont(int i, String s)
    {
        jm85[i] = null;
        jm85[i] = jm27(s);
    }

    public String GetTreeFont(int i)
    {
        return jm85[i].toString();
    }

    public void SetDescriptionFont(int i)
    {
        boolean flag = false;
        jm121 = jm85[i];
        jm78[0] = createImage(50, 50);
        jm79[0] = jm78[0].getGraphics();
        jm79[0].setFont(jm121);
        jm51 = jm79[0].getFontMetrics();
        jm94 = jm51.getMaxAscent() - jm51.getMaxDescent();
        for(int j = 0; j < 3; j++)
        {
            jm78[j] = null;
            jm79[j] = null;
            jm78[j] = createImage(jm129 - 2, (jm94 + jm94 / 2) * (j + 2) + 1);
            jm79[j] = jm78[j].getGraphics();
            jm79[j].setFont(jm121);
        }

    }

    public void CreateBackground(int i, int j)
    {
        int k = 0;
        int l = jm128;
        jm80[22] = j;
        if(jm41[i] == null || j > 1)
            return;
        int i1 = jm41[i].getHeight(this);
        int j1 = jm41[i].getWidth(this);
        jm82.setColor(jm84[jm80[14]]);
        jm82.fillRect(0, jm128, jm129, jm128);
        for(; l < 2 * jm128; l += i1)
        {
            while(k < jm129) 
            {
                jm82.drawImage(jm41[i], k, l, this);
                k += j1;
                if(j == 0)
                    return;
            }
            k = 0;
        }

    }

    public void run()
    {
        int i = 50;
        while(jm46) 
        {
            if(jm47)
            {
                jm21();
                jm47 = false;
            }
            if(jm125.compareTo("") != 0)
            {
                jm127 = "";
                jm126 = jm7(jm125, jm48, jm49);
                jm127 = "Ready";
                jm125 = "";
            }
            while(jm40) 
            {
                if(jm62 + jm69 / jm80[12] >= jm66 || jm69 / jm80[12] < 0 || jm70 > jm80[9] - 17 || jm69 > jm80[10] - 17)
                {
                    jm87 = -1;
                } else
                {
                    jm87 = jm53[jm69 / jm80[12]];
                    if(jm70 + jm60 >= (jm43[jm87].iLevel - 1) * jm80[13] && !jm43[jm87].bExpanded && jm70 + jm60 <= jm43[jm87].iLevel * jm80[13] && jm43[jm87].iType < 2 && GetNumberChildren(jm43[jm87]) > 0 && (jm43[jm87].bytState & 0x8) != 8)
                        jm8(jm87, true, jm43[jm87].iPosition);
                    if(jm70 + jm60 >= jm43[jm87].iLevel * jm80[13] && jm70 + jm60 <= (jm43[jm87].iLevel + 1) * jm80[13] + jm43[jm87].jm4 + 2 && jm69 >= 0)
                        jm87 = jm53[jm69 / jm80[12]];
                    else
                        jm87 = -1;
                }
                if(jm69 < jm80[12] || jm69 > jm80[10] - jm80[12] || jm70 < 0 || jm70 > jm80[9])
                {
                    if(jm69 < 0)
                    {
                        ScrollTo(jm100[1] - 1.0F, 1);
                        i = 50 + jm69;
                    }
                    if(jm69 > jm80[10] && jm104[1])
                    {
                        ScrollTo(jm100[1] + 1.0F, 1);
                        i = 50 - (jm69 - jm80[10]);
                    }
                    if(jm70 < 0)
                    {
                        ScrollTo(jm100[0] - 5F, 0);
                        i = 50 + jm70;
                    }
                    if(jm70 > jm80[9] && jm104[0])
                    {
                        ScrollTo(jm100[0] + 5F, 0);
                        i = 50 - (jm70 - jm80[9]);
                    }
                    if(i < 5)
                        i = 5;
                    try
                    {
                        Thread.sleep(i);
                    }
                    catch(InterruptedException interruptedexception) { }
                }
                paint(jm83);
            }
            try
            {
                WPTreeView _tmp = this;
                Thread.sleep(250L);
            }
            catch(InterruptedException interruptedexception1) { }
            if(jm47)
            {
                jm21();
                jm47 = false;
            }
            jm71 = jm69;
            jm72 = jm70;
            while(jm97) 
            {
                jm40 = false;
                if(System.currentTimeMillis() - jm130 < 500L)
                    break;
                jm36(jm70, jm69, 0);
                jm36(jm70, jm69, 1);
                try
                {
                    WPTreeView _tmp1 = this;
                    Thread.sleep(jm80[26]);
                }
                catch(InterruptedException interruptedexception2) { }
            }
            try
            {
                WPTreeView _tmp2 = this;
                Thread.sleep(250L);
            }
            catch(InterruptedException interruptedexception3) { }
            if(jm90 && !jm75)
            {
                jm57 = new StringTokenizer(jm22(jm43[jm63].sDescript), "~", false);
                int j = jm57.countTokens();
                if(j > 3)
                    j = 3;
                jm64 = jm64 = (jm94 + 3) * 3;
					 /*
                while(jm90 && jm71 == jm69 && jm72 == jm70 && jm43[jm63].sDescript.compareTo(" ") != 0) 
                {
                    if(jm69 < (jm80[10] - 15) / 2)
                        DrawDescription((jm69 / jm80[12] + 1) * jm80[12] + 8, jm43[jm63].sDescript, jm43[jm63].iDescriptTextColor, jm43[jm63].iDescriptBackColor);
                    else
                        DrawDescription((jm69 / jm80[12]) * jm80[12] - (jm94 + jm94 / 2) * (j + 1) - 8, jm43[jm63].sDescript, jm43[jm63].iDescriptTextColor, jm43[jm63].iDescriptBackColor);
                    try
                    {
                        WPTreeView _tmp3 = this;
                        Thread.sleep(jm80[2]);
                    }
                    catch(InterruptedException interruptedexception4) { }
                }
					 */
            }
        }
    }

    public void DrawDescription(int i, String s, int j, int k)
    {
        int i1 = 0;
        StringTokenizer stringtokenizer = new StringTokenizer(jm22(s), "~", false);
        int l = stringtokenizer.countTokens();
        int j1 = l;
        if(j1 <= 3)
        {
            jm64 = 0;
        } else
        {
            j1 = 3;
            jm64 = jm64 - 1;
        }
        i1 = jm94 / 2;
        jm79[j1 - 1].setColor(jm84[k]);
        jm79[j1 - 1].fillRect(0, 0, jm80[9] - 3, (jm94 + i1) * (j1 + 1) + 1);
        jm79[j1 - 1].setColor(jm84[j]);
        jm79[j1 - 1].drawRect(0, 0, jm80[9] - 3, (jm94 + i1) * (j1 + 1));
        for(jm68 = 1; jm68 <= l; jm68++)
            jm79[j1 - 1].drawString(stringtokenizer.nextToken(), 3, (jm94 + i1) * jm68 + jm64 + i1);

        if((jm94 + i1) * l + jm64 < 0)
            jm64 = (jm94 + i1) * 3;
        jm83.drawImage(jm78[j1 - 1], 2, i, this);
    }

    public int GetTreeColor(int i)
    {
        return jm84[i].getRGB();
    }

    public void SetTreeColor(int i, int j)
    {
        jm84[i] = null;
        jm84[i] = new Color(j);
    }

    public void GetFile(String s, int i, boolean flag)
    {
        jm48 = i;
        jm49 = flag;
        jm126 = null;
        jm127 = "Ready";
        jm125 = s;
    }

    public void DisableDescriptions(boolean flag)
    {
        jm75 = flag;
    }

    public byte[] GetFileContents()
    {
        return jm126;
    }

    public String FileReady()
    {
        return jm127;
    }

    private byte[] jm7(String s, int i, boolean flag)
    {
        Object obj = null;
        Object obj1 = null;
        try
        {
            URL url = new URL(getCodeBase(), s);
            URLConnection urlconnection = url.openConnection();
            urlconnection.setUseCaches(flag);
            urlconnection.connect();
            return jm28(i, urlconnection.getInputStream());
        }
        catch(Exception exception)
        {
            jm127 = "Error - " + exception.toString();
        }
        return null;
    }

    public boolean keyDown(Event event, int i)
    {
        jm20(jm89 + "_KeyDown", i, event.x, event.y, event.modifiers, event.when);
        return true;
    }

    public boolean keyUp(Event event, int i)
    {
        jm20(jm89 + "_KeyUp", i, event.x, event.y, event.modifiers, event.when);
        return true;
    }

    private void jm8(int i, boolean flag, int j)
    {
        boolean flag1 = false;
        boolean flag2 = false;
        Object obj = null;
        if(i != -1)
        {
            jm90 = false;
            int k1 = jm43[i].iParent;
            if(jm43[i].iType > 1)
            {
                SetupNode(jm43[i]);
                paint(jm83);
                return;
            }
            if(flag)
            {
                jm43[i].bExpanded = true;
                SetupNode(jm43[i]);
                int k = jm43[i].iNumberExposableNodes;
                jm11(i, k);
                int l1 = i + 1;
                for(int i1 = 0; i1 < k; i1++)
                {
                    l1 = jm10(l1, j + i1);
                    if(jm80[1] != 0 && i1 <= jm80[10] / jm80[12])
                    {
                        jm19(jm80[1]);
                        paint(jm83);
                    }
                }

            } else
            {
                int l = jm43[i].iNumberExposableNodes;
                jm11(i, -1 * l);
                for(int j1 = 0; j1 < l; j1++)
                {
                    WPTreeNode WPTreeNode = (WPTreeNode)jm54.elementAt(j + 1);
                    jm54.removeElementAt(j + 1);
                    if(jm80[1] != 0 && j1 <= jm80[3])
                    {
                        jm19(jm80[1]);
                        paint(jm83);
                    }
                }

                jm43[i].bExpanded = false;
                if(!CheckInVisibleTree(jm87))
                    jm87 = i;
                SetupNode(jm43[i]);
            }
            jm88 = -1;
        }
        jm66 = jm54.size();
        jm9();
        jm21();
    }

    private void jm9()
    {
        for(int i = 0; i < jm54.size(); i++)
        {
            WPTreeNode WPTreeNode = (WPTreeNode)jm54.elementAt(i);
            WPTreeNode.iPosition = i;
        }

    }

    private int jm10(int i, int j)
    {
        if(jm43[i].iType < 2 && jm43[i].bExpanded)
        {
            jm54.insertElementAt(jm43[i], j + 1);
            i++;
        } else
        if(jm43[i].iType < 2 && !jm43[i].bExpanded)
        {
            jm54.insertElementAt(jm43[i], j + 1);
            for(; jm43[i].iType == 1 && jm43[i].iParent != -1; i = jm43[i].iParent);
            i = jm43[i].iNextNode;
        } else
        if(jm43[i].iType > 1)
        {
            jm54.insertElementAt(jm43[i], j + 1);
            i++;
        }
        return i;
    }

    private void jm11(int i, int j)
    {
        int l = jm43[i].iParent;
        for(int k = 0; k < jm43[i].iLevel; k++)
        {
            jm43[l].iNumberExposableNodes = jm43[l].iNumberExposableNodes + j;
            if(l != -1 && !jm43[l].bExpanded)
                return;
            l = jm43[l].iParent;
        }

    }

    public boolean CheckInVisibleTree(int i)
    {
        for(int j = 0; j < jm54.size(); j++)
        {
            WPTreeNode WPTreeNode = (WPTreeNode)jm54.elementAt(j);
            if(WPTreeNode.iIndex == i)
                return true;
        }

        return false;
    }

    public void SelectNode(int i, boolean flag)
    {
        if(CheckInVisibleTree(i))
        {
            if(flag)
                jm87 = i;
            else
                jm87 = -1;
            Refresh();
        }
    }

    public WPTreeNode GetNewNode()
    {
        return new WPTreeNode();
    }

    public WPTreeNode GetNode(int i)
    {
        if(i > -1 && i < jm65)
            return jm43[i];
        else
            return null;
    }

    public int GetNumberNodes()
    {
        return jm65;
    }

    public void SetNumberNodes(int i)
    {
        jm65 = i;
    }

    public int GetSelectedIndex()
    {
        return jm87;
    }

    public void AddNodeToList(int i, WPTreeNode WPTreeNode)
    {
        jm44[i] = WPTreeNode;
    }

    public void SetListSize(int i)
    {
        jm45 = i;
        jm44 = null;
        jm44 = new WPTreeNode[i];
    }

    public void AddNodeList(int i)
    {
        jm12(i, jm45, jm44);
    }

    private void jm12(int i, int j, WPTreeNode aWPTreeNode[])
    {
        jm90 = false;
        jm55 = -1;
        System.arraycopy(jm43, i + 1, jm43, i + j + 1, jm65 - i - 1);
        jm65 = jm65 + j;
        for(int k = 0; k < j; k++)
        {
            jm43[i + 1 + k] = null;
            jm43[i + 1 + k] = aWPTreeNode[k];
        }

        for(int l = 0; l < jm65; l++)
        {
            jm43[l].iIndex = l;
            if(l <= i || l > i + j)
            {
                if(jm43[l].iParent > i && jm43[l].iParent != -1)
                    jm43[l].iParent = jm43[l].iParent + j;
                if(jm43[l].iNextNode > i && jm43[l].iNextNode != -1)
                    jm43[l].iNextNode = jm43[l].iNextNode + j;
            }
        }

        InitNodes(i, j);
        jm9();
        jm66 = jm54.size();
    }

    public WPTreeNode GetPrevious(WPTreeNode WPTreeNode)
    {
        int i = WPTreeNode.iIndex - 1;
        for(WPTreeNode WPTreeNode1 = GetNode(i); WPTreeNode1 != null; WPTreeNode1 = GetNode(i))
        {
            if(WPTreeNode.iLevel == WPTreeNode1.iLevel)
                return WPTreeNode1;
            if(WPTreeNode1.iLevel < WPTreeNode.iLevel)
                return null;
            i--;
        }

        return null;
    }

    public void RemoveNodes(int i)
    {
        int i1 = jm43[i].iNumberExposableNodes;
        int j1 = GetNumberChildren(jm43[i]);
        int k1 = i;
        jm90 = false;
        jm55 = -1;
        if(jm43[k1].iType < 2 && j1 == 0 || jm43[k1].iType > 1)
        {
            jm11(k1, -1);
            j1 = 1;
        } else
        if(!jm43[k1].bExpanded && j1 > 0)
        {
            i++;
        } else
        {
            i++;
            jm11(k1, -1 * i1);
        }
        jm43[k1].iNumberExposableNodes = 0;
        if(jm43[i].iNextNode == -1)
        {
            WPTreeNode WPTreeNode = GetPrevious(jm43[i]);
            if(WPTreeNode != null)
            {
                WPTreeNode.iNextNode = -1;
                jm23(WPTreeNode);
                SetupNode(WPTreeNode);
                for(int j = WPTreeNode.iIndex; j < i; j++)
                    jm15(WPTreeNode.iLevel, j, false);

            }
        }
        for(int k = 0; k < jm54.size(); k++)
        {
            WPTreeNode WPTreeNode1 = (WPTreeNode)jm54.elementAt(k);
            if(WPTreeNode1.iIndex >= i && WPTreeNode1.iIndex < i + j1)
            {
                jm54.removeElementAt(k);
                k--;
            }
        }

        System.arraycopy(jm43, i + j1, jm43, i, jm80[0] - i - j1);
        jm65 = jm65 - j1;
        for(int l = 0; l < jm65; l++)
        {
            jm43[l].iIndex = l;
            if(jm43[l].iParent > i && jm43[l].iParent != -1)
                jm43[l].iParent = jm43[l].iParent - j1;
            if(jm43[l].iNextNode > i && jm43[l].iNextNode != -1)
                jm43[l].iNextNode = jm43[l].iNextNode - j1;
        }

        jm9();
        jm66 = jm54.size();
    }

    private void jm13(int i, int j, int k)
    {
        for(int l = 0; l < i; l++)
        {
            jm43[l].iIndex = l;
            if(jm43[l].iParent > j && jm43[l].iParent != -1)
                jm43[l].iParent = jm43[l].iParent - k;
            if(jm43[l].iNextNode > j && jm43[l].iNextNode != -1)
                jm43[l].iNextNode = jm43[l].iNextNode - k;
        }

    }

    public boolean AllParentsVisible(int i)
    {
        boolean flag = true;
        for(int j = jm43[i].iParent; j != -1; j = jm43[j].iParent)
            flag &= jm43[j].bExpanded;

        return flag;
    }

    public int GetURLIndex(String s)
    {
        for(int i = 0; i < jm65; i++)
            if(jm43[i].sURL.compareTo(s) == 0)
                return i;

        return -1;
    }

    public void ExposeNode(int i, boolean flag)
    {
        if(i != 0)
        {
            if(flag)
                jm14(i, flag);
            if(jm43[i].bExpanded != flag && flag)
                jm8(i, flag, jm43[i].iPosition);
            else
            if(jm43[i].bExpanded != flag && !flag)
                if(AllParentsVisible(i))
                {
                    jm8(i, flag, jm43[i].iPosition);
                } else
                {
                    jm11(i, -1 * jm43[i].iNumberExposableNodes);
                    jm43[i].bExpanded = false;
                }
        }
    }

    private void jm14(int i, boolean flag)
    {
        if(jm43[i].iParent != 0)
        {
            jm14(jm43[i].iParent, flag);
            if(jm43[jm43[i].iParent].bExpanded != flag)
            {
                jm8(jm43[i].iParent, flag, jm43[jm43[i].iParent].iPosition);
                ScrollTo(jm43[jm43[i].iParent].iPosition, 1);
                ScrollTo(jm43[jm43[i].iParent].iLevel * jm80[13], 0);
            }
        }
    }

    private void jm15(int i, int j, boolean flag)
    {
        int k = i / 8;
        int l = i % 8;
        int i1 = 1;
        if(flag)
            jm43[j].jm3[k] = (byte)(jm43[j].jm3[k] | i1 << l);
        else
            jm43[j].jm3[k] = (byte)(jm43[j].jm3[k] & ~(i1 << l));
    }

    private boolean jm16(int i, int j)
    {
        int k = i / 8;
        int l = i % 8;
        int i1 = 1;
        return (jm43[j].jm3[k] & i1 << l) != 0;
    }

    public void SetupNode(WPTreeNode WPTreeNode)
    {
        int i = 0;
        boolean flag = false;
        int k = 0;
        i = WPTreeNode.iIndex;
        k = i;
        for(int j = 1; j <= WPTreeNode.iLevel; j++)
        {
            if(jm43[jm43[k].iParent].iType != 1)
                jm15(WPTreeNode.iLevel - j, WPTreeNode.iIndex, true);
            else
                jm15(WPTreeNode.iLevel - j, WPTreeNode.iIndex, false);
            k = jm43[k].iParent;
        }

        WPTreeNode.jm2 = (WPTreeNode.iLevel - 1) * jm80[13];
        if(WPTreeNode.iType <= 1)
        {
            if(WPTreeNode.bExpanded && (WPTreeNode.iNumberExposableNodes > 0 || jm80[7] == 0))
            {
                if(WPTreeNode.iType == 1)
                    WPTreeNode.jm1 = 2;
                else
                    WPTreeNode.jm1 = 5;
            } else
            if(WPTreeNode.iNumberExposableNodes > 0 || jm80[7] == 0)
            {
                if(WPTreeNode.iType == 1)
                    WPTreeNode.jm1 = 3;
                else
                    WPTreeNode.jm1 = 6;
            } else
            if(WPTreeNode.iType == 1)
                WPTreeNode.jm1 = 1;
            else
                WPTreeNode.jm1 = 4;
            if(WPTreeNode.bExpanded)
                WPTreeNode.jm0 = WPTreeNode.iImageNumberOpen;
            else
                WPTreeNode.jm0 = WPTreeNode.iImageNumberClosed;
        }
        if(WPTreeNode.iType >= 2)
        {
            if(WPTreeNode.iType == 3)
                WPTreeNode.jm1 = 1;
            else
                WPTreeNode.jm1 = 4;
            if(jm87 == i)
                WPTreeNode.jm0 = WPTreeNode.iImageNumberClosed;
            else
                WPTreeNode.jm0 = WPTreeNode.iImageNumberOpen;
        }
        jm82.setFont(jm85[WPTreeNode.iFont]);
        WPTreeNode.jm4 = jm82.getFontMetrics().stringWidth(WPTreeNode.sName);
        WPTreeNode.jm5 = (WPTreeNode.iLevel + 1) * jm80[13] + WPTreeNode.jm4;
    }

    public synchronized void paint(Graphics g)
    {
        boolean flag = false;
        int k = 0;
        boolean flag1 = false;
        boolean flag2 = false;
        boolean flag3 = false;
        Object obj = null;
        setBackground(jm84[jm80[14]]);
        jm60 = (int)Math.floor(jm100[0]);
        jm62 = (int)Math.ceil(jm100[1]);
        jm82.setClip(0, 0, jm129, 2 * jm128);
        jm82.setColor(jm84[jm80[14]]);
        if(jm80[22] > 1)
            jm82.fillRect(0, 0, jm80[9], jm80[10]);
        else
            jm82.copyArea(0, jm128, jm129, jm128, 0, -jm128);
        jm66 = jm54.size();
        if(jm66 <= 0 || jm80[23] == 1)
        {
            g.drawImage(jm77, 0, 0, this);
            return;
        }
        if(jm62 < 0)
            jm62 = 0;
        jm73 = jm62;
        if(jm66 > jm62 + jm80[10] / jm80[12] + 1)
            jm86 = jm62 + jm80[10] / jm80[12] + 1;
        else
            jm86 = jm66;
        jm82.setClip(0, 0, jm80[9], jm80[10]);
        if(jm80[25] < 3)
            if(jm104[0] && !jm104[1])
                jm82.setClip(0, 0, jm80[9], jm80[10] - 17);
            else
            if(jm104[1] && !jm104[0])
                jm82.setClip(0, 0, jm80[9] - 17, jm80[10]);
            else
            if(jm104[0] && jm104[1])
                jm82.setClip(0, 0, jm80[9] - 17, jm80[10] - 17);
        int k1 = jm60 / jm80[13];
        int l1 = k1 + jm80[9] / jm80[13];
        for(int i1 = jm73; i1 < jm86; i1++)
        {
            WPTreeNode WPTreeNode = (WPTreeNode)jm54.elementAt(i1);
            int l = WPTreeNode.iIndex;
            int i2;
            if(jm43[l].iLevel < l1)
                i2 = jm43[l].iLevel;
            else
                i2 = l1;
            for(int j1 = k1; j1 <= i2; j1++)
                if(jm16(j1, l))
                {
                    int i = -jm60 + (j1 - 1) * jm80[13];
                    jm82.drawImage(jm41[7], i, k, this);
                }

            int j = -jm60 + jm43[l].jm2;
            jm82.drawImage(jm41[jm43[l].jm1], j, k, this);
            if(jm43[l].iType >= 2)
                if(jm87 == l)
                    jm43[l].jm0 = jm43[l].iImageNumberClosed;
                else
                    jm43[l].jm0 = jm43[l].iImageNumberOpen;
            j += jm80[13];
            if(j < jm80[9])
            {
                jm82.drawImage(jm41[jm43[l].jm0], j, k, this);
                if(jm43[l].iImageOverlay != -1)
                    jm82.drawImage(jm41[jm43[l].iImageOverlay], j, k, this);
                if(l == jm55 && jm43[l].iImageMouseOver != -1)
                    jm82.drawImage(jm41[jm43[l].iImageMouseOver], j, k, this);
            }
            j += jm80[13];
            if(j < jm80[9])
            {
                jm82.setFont(jm85[jm43[l].iFont]);
                FontMetrics fontmetrics = jm82.getFontMetrics();
                jm4 = jm43[l].jm4;
                if(jm87 == l || (jm43[l].bytState & 0x1) == 1)
                {
                    jm82.setColor(jm84[jm80[15]]);
                    jm82.fillRect(j + 2, k + 1, jm4 + 4, jm80[12] - 2);
                    if(jm80[16] != -1)
                        jm82.setColor(jm84[jm80[16]]);
                    else
                        jm82.setColor(jm84[jm43[l].iColor]);
                    jm82.drawString(jm43[l].sName, j + 4, k + (jm80[12] - fontmetrics.getHeight()) / 2 + fontmetrics.getAscent());
                }
                if(jm87 != l)
                {
                    if(l == jm55 || (jm43[l].bytState & 0x2) == 2)
                    {
                        if(jm80[17] != -1)
                            jm82.setColor(jm84[jm80[17]]);
                        else
                            jm82.setColor(jm84[jm43[l].iColor]);
                        if(jm40 && jm52 != null || (jm43[l].bytState & 0x2) == 2)
                            jm82.drawRect(j + 1, k + 1, jm4 + 4, jm80[12] - 2);
                    } else
                    {
                        jm82.setColor(jm84[jm43[l].iColor]);
                    }
                    jm82.drawString(jm43[l].sName, j + 4, k + (jm80[12] - fontmetrics.getHeight()) / 2 + fontmetrics.getAscent());
                    jm82.setColor(getBackground());
                }
            }
            jm53[i1 - jm73] = jm43[l].iIndex;
            j = -1 * jm60;
            k += jm80[12];
        }

        if(jm52 != null && jm40 || jm124)
        {
            jm82.setFont(jm85[jm52.iFont]);
            FontMetrics fontmetrics1 = jm82.getFontMetrics();
            jm82.drawImage(jm41[jm122], jm70, jm69, this);
            if(jm52.iImageOverlay != -1)
                jm82.drawImage(jm41[jm52.iImageOverlay], jm70, jm69, this);
            if(jm80[17] != -1)
                jm82.setColor(jm84[jm80[17]]);
            else
                jm82.setColor(jm84[jm52.iColor]);
            jm82.drawString(jm123, jm70 + 4 + jm80[13], jm69 + (jm80[12] - fontmetrics1.getHeight()) / 2 + fontmetrics1.getAscent());
        }
        jm82.setClip(0, 0, jm80[9], jm80[10]);
        if(jm104[0])
            jm37(jm82, 0);
        if(jm104[1])
            jm37(jm82, 1);
        if((jm104[0] || jm104[1]) && jm80[25] < 3)
        {
            jm82.setColor(jm84[jm80[18]]);
            jm82.fillRect(jm80[9] - 17, jm80[10] - 17, 16, 16);
            jm82.setColor(jm84[jm80[19]]);
            jm82.drawRect(jm80[9] - 17, jm80[10] - 17, 16, 16);
        }
        g.drawImage(jm77, 0, 0, this);
    }

    public synchronized boolean mouseExit(Event event, int i, int j)
    {
        if(jm40)
            return true;
        jm34();
        jm35();
        jm55 = -1;
        try
        {
            paint(jm83);
        }
        catch(Exception exception) { }
        jm20(jm89 + "_MouseExit", -1, i, j, event.modifiers, event.when);
        jm90 = false;
        return true;
    }

    public void SetMouseImage(String s, int i, boolean flag)
    {
        jm123 = s;
        jm122 = i;
        jm124 = flag;
    }

    public synchronized boolean mouseDrag(Event event, int i, int j)
    {
        jm90 = false;
        if(jm120 && !jm40)
        {
            jm55 = -1;
            jm31(i, j);
            return true;
        }
        if(event.modifiers != 4 || jm80[11] < 1)
        {
            if(jm80[4] == 0)
                return true;
            if(jm87 != -1 && !jm40 && jm55 == jm87)
            {
                if((jm43[jm55].bytState & 0x4) == 4)
                    return true;
                jm52 = jm43[jm87];
                jm122 = jm52.iImageNumberOpen;
                jm123 = jm52.sName;
            }
            if(jm52 != null)
            {
                if(!jm40)
                    jm20(jm89 + "_MouseStartDrag", jm43[jm87].iIndex, i, j, event.modifiers, event.when);
                jm40 = true;
                jm70 = i;
                jm69 = j;
            }
        }
        if(j < jm80[10] - 18 && i < jm80[9] - 18 || j > jm80[10] || i > jm80[9] || j < 0 || i < 0 || i > 17 && i < jm80[9] - 34 || j > 17 && j < jm80[10] - 34 || j > jm80[10] - 18 && i > jm80[9] - 18)
        {
            jm34();
            paint(jm83);
        }
        return true;
    }

    private void jm17()
    {
        if(jm66 >= jm80[10] / jm80[12])
        {
            float f = jm100[1];
            if(!jm104[1])
                f = 0.0F;
            jm104[1] = true;
            jm29(f, jm80[10] / jm80[12], (jm66 + 2) - jm80[10] / jm80[12], 1);
        } else
        if(jm104[1])
        {
            jm73 = 0;
            jm100[1] = 0.0F;
            jm32(1);
            jm62 = 0;
            jm104[1] = false;
        }
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    private void jm18()
    {
        jm24();
        if(jm67 + 34 > jm80[9])
        {
            float f = jm100[0];
            if(!jm104[0])
                f = 0.0F;
            jm29(f, jm80[9], (jm67 - jm80[9]) + 34, 0);
            jm104[0] = true;
        } else
        if(jm104[0])
        {
            jm60 = 0;
            jm61 = 0;
            jm104[0] = false;
            jm100[0] = 0.0F;
            jm32(0);
        }
    }

    public synchronized boolean mouseEnter(Event event, int i, int j)
    {
        if(jm40)
        {
            return true;
        } else
        {
            paint(jm83);
            jm20(jm89 + "_MouseEnter", -1, i, j, event.modifiers, event.when);
            jm90 = false;
            return true;
        }
    }

    public synchronized boolean mouseMove(Event event, int i, int j)
    {
        int k = 0;
        if(jm120 || jm40)
            return true;
        k = j / jm80[12];
        jm55 = -1;
        jm90 = false;
        jm69 = j;
        jm70 = i;
        jm72 = -1;
        if(jm62 + k >= jm66 || k < 0 || i > jm80[9] - 17 && jm104[1] || j > jm80[10] - 17 && jm104[0] && jm80[25] != 3)
        {
            jm90 = false;
            paint(jm83);
            jm20(jm89 + "_MouseMove", -1, i, j, event.modifiers, event.when);
            return true;
        }
        if(i + jm60 >= jm43[jm53[k]].iLevel * jm80[13] && i + jm60 <= (jm43[jm53[k]].iLevel + 1) * jm80[13] + jm43[jm53[k]].jm4 + 2)
        {
            jm90 = true;
            jm55 = jm53[k];
            paint(jm83);
            jm20(jm89 + "_MouseMove", jm43[jm55].iIndex, i, j, event.modifiers, event.when);
            jm63 = jm53[k];
        } else
        {
            jm20(jm89 + "_MouseMove", -1, i, j, event.modifiers, event.when);
            if(!jm40)
                paint(jm83);
        }
        return true;
    }

    private void jm19(int i)
    {
        try
        {
            Thread.sleep(i);
        }
        catch(InterruptedException interruptedexception) { }
    }

    public synchronized boolean mouseUp(Event event, int i, int j)
    {
        int k = jm87;
        jm34();
        jm35();
        if(jm65 == 0)
            return true;
        if(jm87 == -1)
            jm87 = jm55;
        jm55 = -1;
        jm120 = false;
        jm97 = false;
        jm40 = false;
        paint(jm83);
        int l;
        if(jm87 == -1)
            l = -1;
        else
            l = jm43[jm87].iIndex;
        if(jm52 != null)
        {
            jm52 = null;
            jm20(jm89 + "_MouseEndDrag", l, i, j, event.modifiers, event.when);
        }
        return true;
    }

    public synchronized boolean mouseDown(Event event, int i, int j)
    {
        int k = -1;
        jm90 = false;
        jm130 = event.when;
        long l = 0L;
        if(i >= jm80[9] - 17 && i <= jm80[9] && jm104[1] && jm80[25] != 3)
        {
            jm36(i, j, 1);
            jm20(jm89 + "_MouseDown", -1, i, j, event.modifiers, event.when);
            return true;
        }
        if(j >= jm80[10] - 17 && j <= jm80[10] && jm104[0] && jm80[25] != 3)
        {
            jm36(i, j, 0);
            jm20(jm89 + "_MouseDown", -1, i, j, event.modifiers, event.when);
            return true;
        }
        k = j / jm80[12];
        if(jm62 + k >= jm66 || k < 0)
        {
            jm20(jm89 + "_MouseDown", -1, i, j, event.modifiers, event.when);
            return true;
        }
        int i1 = (jm43[jm53[k]].iLevel - 1) * jm80[13];
        if(i + jm60 >= i1 && i + jm60 <= i1 + 2 * jm80[13] + jm43[jm53[k]].jm4 + 2)
        {
            jm55 = jm53[k];
            if(i + jm60 > i1 + jm80[13])
                jm87 = jm53[k];
            else
                jm55 = -1;
            jm88 = jm53[k];
            long l1 = jm130 - jm56;
            if(l1 < 0L)
                l1 = -l1;
            if((long)jm80[6] > l1 || jm43[jm87].iType > 1 || jm80[5] == 0 || i + jm60 <= i1 + jm80[13])
            {
                jm56 = 0L;
                if(jm43[jm87].iType < 2 && (event.modifiers != 4 || jm80[11] < 1))
                    if(!jm43[jm88].bExpanded)
                        jm20(jm89 + "_Expand", jm88, i, j, event.modifiers, event.when);
                    else
                        jm20(jm89 + "_Collapse", jm88, i, j, event.modifiers, event.when);
                if((event.modifiers != 4 || jm80[11] < 1) && (jm43[jm88].bytState & 0x8) != 8)
                    jm8(jm88, !jm43[jm88].bExpanded, jm62 + k);
            } else
            {
                jm56 = jm130;
                paint(jm83);
            }
            String s = jm43[jm87].sURL;
            if(s.compareTo(" ") != 0 && !jm92 && i + jm60 > i1 + jm80[13] && (event.modifiers != 4 || jm80[11] < 1) && (jm43[jm88].bytState & 0x16) != 22)
            {
                try
                {
                    jm96 = null;
                    if(s.startsWith(".") || s.indexOf("//.") != -1)
                        jm96 = new URL(getCodeBase(), jm43[jm87].sURL);
                    else
                        jm96 = new URL(jm43[jm87].sURL);
                }
                catch(Exception exception) { }
                getAppletContext().showDocument(jm96, jm43[jm87].sTarget);
            }
            jm20(jm89 + "_MouseDown", jm43[jm87].iIndex, i, j, event.modifiers, event.when);
            return true;
        } else
        {
            jm20(jm89 + "_MouseDown", -1, i, j, event.modifiers, event.when);
            return true;
        }
    }

    private synchronized void jm20(String s, int i, int j, int k, int l, long l1)
    {
        if(jm89.compareTo("") == 0 || jm89 == null)
            return;
        try
        {
            JSObject.getWindow(this).eval("javascript:" + s + "(" + i + "," + j + "," + k + "," + l + "," + l1 + ")");
        }
        catch(Exception exception) { }
    }

    public void Refresh()
    {
        jm47 = true;
    }

    public void RefreshNow()
    {
        jm21();
    }

    private void jm21()
    {
        jm18();
        jm17();
        paint(jm83);
    }

    private String jm22(String s)
    {
        String s1 = "";
        String s3 = "";
        String s4 = "";
        String s5 = "";
        boolean flag = false;
        int k = 0;
        StringTokenizer stringtokenizer = new StringTokenizer(s, " ", false);
        k = stringtokenizer.countTokens();
        for(int j = 0; j < k; j++)
        {
            String s6;
            for(s6 = stringtokenizer.nextToken(); jm51.stringWidth(s6) >= jm80[9] - 12;)
            {
                s6 = s4 + s6;
                for(int i = s6.length(); i > 0; i--)
                {
                    String s2 = s6.substring(0, i);
                    if(jm51.stringWidth(s2 + "-") < jm80[9] - 12)
                    {
                        s3 = s3 + s2 + "-~";
                        s6 = s6.substring(i, s6.length());
                        i = 0;
                        s4 = "";
                    }
                }

            }

            if(jm51.stringWidth(s4 + s6) >= jm80[9] - 12)
            {
                s3 = s3 + s4 + "~";
                s4 = s6 + " ";
            } else
            {
                s4 = s4 + s6 + " ";
            }
        }

        s3 = s3 + s4;
        return s3;
    }

    public void GetTreeNodes()
    {
        jm65 = jm26(jm43, "Node", 0);
        if(jm65 > 0)
            InitTree(jm65);
    }

    public void stop()
    {
        jm80[23] = 1;
        jm46 = false;
        paint(jm83);
        try
        {
            jm76.stop();
        }
        catch(Exception exception) { }
        jm76 = null;
        System.gc();
    }

    public WPTreeNode ParseNodeInfo(String s)
    {
        WPTreeNode WPTreeNode = new WPTreeNode();
        jm95 = new StringTokenizer(s, ",");
        WPTreeNode.sName = jm95.nextToken();
        if(jm95.nextToken().compareTo("true") == 0)
            WPTreeNode.bExpanded = true;
        else
            WPTreeNode.bExpanded = false;
        WPTreeNode.iImageNumberOpen = Integer.parseInt(jm95.nextToken());
        WPTreeNode.iImageNumberClosed = Integer.parseInt(jm95.nextToken());
        WPTreeNode.iType = Integer.parseInt(jm95.nextToken());
        WPTreeNode.iLevel = Integer.parseInt(jm95.nextToken());
        WPTreeNode.sDescript = jm95.nextToken();
        WPTreeNode.sURL = jm95.nextToken();
        WPTreeNode.sTarget = jm95.nextToken();
        WPTreeNode.iImageOverlay = Integer.parseInt(jm95.nextToken());
        WPTreeNode.iImageMouseOver = Integer.parseInt(jm95.nextToken());
        WPTreeNode.sTag = jm95.nextToken();
        WPTreeNode.iColor = Integer.parseInt(jm95.nextToken());
        WPTreeNode.iFont = Integer.parseInt(jm95.nextToken());
        WPTreeNode.iDescriptTextColor = Integer.parseInt(jm95.nextToken());
        WPTreeNode.iDescriptBackColor = Integer.parseInt(jm95.nextToken());
        WPTreeNode.bytState = Byte.parseByte(jm95.nextToken());
        return WPTreeNode;
    }

    public void InitTree(int i)
    {
        jm54.addElement(jm43[0]);
        SetupNode(jm43[0]);
        jm43[0].iIndex = 0;
        if(i > 1)
            InitNodes(0, i - 1);
        jm43[0].iParent = -1;
    }

    public int GetNumberChildren(WPTreeNode WPTreeNode)
    {
        int i = 1;
        if(WPTreeNode.iType > 1)
            return 0;
        int j = GetNumberNodes();
        WPTreeNode WPTreeNode1 = GetNode(WPTreeNode.iIndex + 1);
        if(WPTreeNode1 == null)
            return 0;
        while(WPTreeNode.iLevel < WPTreeNode1.iLevel && WPTreeNode.iIndex + i < j) 
        {
            i++;
            WPTreeNode1 = GetNode(WPTreeNode.iIndex + i);
            if(WPTreeNode1 == null)
                return i - 1;
        }
        return i - 1;
    }

    public void InitNodes(int i, int j)
    {
        boolean flag = false;
        int k1 = 0;
        int ai[] = new int[50];
        int j2 = 0;
        if(jm43[i].iLevel - 1 >= 0 && jm43[i].iType > 1)
            ai[jm43[i].iLevel - 1] = jm43[i].iParent;
        else
            ai[jm43[i].iLevel] = i;
        for(int k = i + 1; k < i + j + 1; k++)
        {
            jm43[k].iIndex = k;
            int i1 = ai[jm43[k].iLevel];
            ai[jm43[k].iLevel] = jm43[k].iIndex;
            jm43[k].iNextNode = -1;
            if(jm43[k].iLevel == jm43[k - 1].iLevel)
                jm43[k - 1].iNextNode = k;
            else
            if(jm43[k].iLevel < jm43[k - 1].iLevel)
                jm43[i1].iNextNode = k;
            else
                jm43[i1].iNextNode = -1;
            if(jm43[k].iLevel == 0)
            {
                jm43[k].iParent = -1;
            } else
            {
                int j1 = ai[jm43[k].iLevel - 1];
                jm43[k].iParent = j1;
            }
            int l1 = k;
            boolean flag1;
            if(jm43[k].iLevel != 0)
                flag1 = jm43[jm43[l1].iParent].bExpanded;
            else
                flag1 = true;
            for(int i2 = 0; i2 < jm43[k].iLevel; i2++)
            {
                if(flag1)
                {
                    jm43[jm43[l1].iParent].iNumberExposableNodes++;
                } else
                {
                    jm43[jm43[l1].iParent].iNumberExposableNodes++;
                    i2 = jm43[k].iLevel;
                }
                if(jm43[jm43[l1].iParent].iParent != -1)
                {
                    l1 = jm43[l1].iParent;
                    flag1 = flag1 && jm43[jm43[l1].iParent].bExpanded;
                }
            }

            if(flag1)
            {
                jm54.insertElementAt(jm43[k], jm43[i].iPosition + j2 + 1);
                j2++;
            }
            jm43[k].iPosition = jm43[i].iPosition + j2;
        }

        if(jm43[i].iType < 2)
            k1 = GetNumberChildren(jm43[i]);
        else
        if(jm43[i].iParent != -1)
            k1 = GetNumberChildren(jm43[jm43[i].iParent]);
        if(k1 - j > 0)
            jm43[ai[jm43[i + 1].iLevel]].iNextNode = i + j + 1;
        if(jm43[i].iType > 1)
        {
            jm43[i].iNextNode = i + 1;
            if(i == (jm43[i].iParent + k1) - j)
                jm43[i + 1].iNextNode = -1;
        }
        jm23(jm43[i]);
        jm23(jm43[i + 1]);
        for(int l = i + 1; l < i + j + 1; l++)
            SetupNode(jm43[l]);

        SetupNode(jm43[i]);
        SetupNode(jm43[i + 1]);
        jm66 = jm54.size();
    }

    private void jm23(WPTreeNode WPTreeNode)
    {
        if(WPTreeNode.iNextNode == -1)
        {
            if(WPTreeNode.iType > 1)
                WPTreeNode.iType = 3;
            else
                WPTreeNode.iType = 1;
        } else
        if(WPTreeNode.iType > 1)
            WPTreeNode.iType = 2;
        else
            WPTreeNode.iType = 0;
    }

    private void jm24()
    {
        Object obj = null;
        jm67 = 0;
        for(int i = 0; i < jm54.size(); i++)
        {
            WPTreeNode WPTreeNode = (WPTreeNode)jm54.elementAt(i);
            if(jm67 < WPTreeNode.jm5)
                jm67 = WPTreeNode.jm5;
        }

    }

    public void GetTreeImage(String s, int i, byte abyte0[])
    {
        try
        {
            jm41[i].flush();
        }
        catch(Exception exception) { }
        jm41[i] = null;
        if(abyte0 != null)
            jm41[i] = Toolkit.getDefaultToolkit().createImage(abyte0);
        else
            jm41[i] = getImage(getCodeBase(), s);
    }

    private void jm25()
    {
        jm42 = jm26(jm41, "TreeImage", 1);
    }

    public void destroy()
    {
        boolean flag = false;
        jm54.removeAllElements();
        jm54 = null;
        jm43 = null;
        super.destroy();
        for(int i = 0; i < jm42; i++)
        {
            try
            {
                jm41[i].flush();
            }
            catch(Exception exception) { }
            jm41[i] = null;
        }

        for(int j = 0; j < 3; j++)
        {
            try
            {
                jm78[j].flush();
            }
            catch(Exception exception1) { }
            jm78[j] = null;
        }

        jm83.dispose();
        jm77.flush();
        jm91 = null;
        System.gc();
    }

	 /*
	  * Parse PARAM tag
	  */
    private int jm26(Object aobj[], String s, int i)
    {
        int j = 0;
        Integer integer = new Integer(j);
        Object obj = null;
        String s1 = "";
        Object obj1 = null;
        for(String s2 = getParameter(s + integer.toString()); s2 != null; s2 = getParameter(s + Integer.toString(j)))
        {
            switch(i)
            {
            default:
                break;

            case 0: // '\0'
                aobj[j] = ParseNodeInfo(s2);
                break;

            case 1: // '\001'
                if(s2.compareTo("") == 0)
                    break;
					 /*
                byte abyte0[] = null;
                abyte0 = GetFileFromJar(s2);
                if(abyte0 != null)
                    aobj[j] = Toolkit.getDefaultToolkit().createImage(abyte0);
                else
					 */
                   aobj[j] = getImage(getCodeBase(), s2);
                jm91.addImage((Image)aobj[j], j);
                break;

            case 2: // '\002'
                aobj[j] = jm27(s2);
                break;

            case 3: // '\003'
                aobj[j] = new Color(Integer.parseInt(s2));
                break;
            }
            j++;
        }

        return j;
    }

    public void ReplaceNode(WPTreeNode WPTreeNode, int i)
    {
        jm43[i] = null;
        jm43[i] = WPTreeNode;
    }

    private Font jm27(String s)
    {
        int i = 0;
        StringTokenizer stringtokenizer = new StringTokenizer(s, ",", false);
        String s1 = stringtokenizer.nextToken();
        String s2 = stringtokenizer.nextToken();
        String s3 = stringtokenizer.nextToken();
        String s4 = stringtokenizer.nextToken();
        if(s3.compareTo("true") == 0)
            i = 1;
        if(s4.compareTo("true") == 0)
            i += 2;
        return new Font(s1, i, Integer.parseInt(s2));
    }

	 /*
    public byte[] GetFileFromJar(String s)
    {
        try
        {
            return jm28(30000, getClass().getResourceAsStream(s));
        }
        catch(Exception exception)
        {
            return null;
        }
    }
	 */

    private byte[] jm28(int i, InputStream inputstream)
    {
        boolean flag = false;
        int k = 0;
        long l = System.currentTimeMillis();
        Object obj = null;
        try
        {
            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
            for(; k != -1 && System.currentTimeMillis() - l < (long)i; jm19(30))
            {
                int j = inputstream.available();
                byte abyte0[] = new byte[j];
                k = inputstream.read(abyte0, 0, j);
                if(k != -1)
                    bytearrayoutputstream.write(abyte0, 0, k);
            }

            byte abyte1[] = bytearrayoutputstream.toByteArray();
            inputstream.close();
            bytearrayoutputstream.close();
            if(System.currentTimeMillis() - l == (long)i)
            {
                jm127 = "Error - Timeout";
                return null;
            } else
            {
                return abyte1;
            }
        }
        catch(Exception exception)
        {
            jm127 = exception.toString();
        }
        return null;
    }

    public String GetTextFromBytes(byte abyte0[])
    {
        String s = new String(abyte0);
        return String.valueOf(s);
    }

    private void jm29(float f, float f1, int i, int j)
    {
        jm100[j] = f;
        jm114[j] = (float)i + f1;
        jm113[j] = jm112[j];
        jm107[j] = f1;
        jm108[j] = (f1 / jm114[j]) * (float)jm113[j];
        if(jm108[j] < 10F)
        {
            jm113[j] = (jm112[j] - 10) + (int)jm108[j];
            jm108[j] = 10F;
        }
        jm109[j] = (jm100[j] / jm114[j]) * (float)jm113[j] + 17F;
        jm32(j);
    }

    public void ScrollTo(float f, int i)
    {
        if(jm104[i])
        {
            jm100[i] = (int)f;
            jm109[i] = (jm100[i] / jm114[i]) * (float)jm113[i] + 17F;
            jm32(i);
        }
    }

    private void jm30(int i, int j, int k, int l, int i1, int j1)
    {
        jm105[j1] = i;
        jm106[j1] = j;
        jm101[j1] = k;
        jm102[j1] = l;
        jm103[j1] = i1;
        if(jm103[j1] == 0)
        {
            jm112[j1] = l - 34;
            jm111[j1] = jm102[j1] - 17;
        } else
        {
            jm112[j1] = k - 34;
            jm111[j1] = jm101[j1] - 17;
        }
        jm109[j1] = 17F;
    }

    private void jm31(int i, int j)
    {
        if(jm80[25] > 1)
            return;
        if(jm103[jm99] == 1)
            j = i;
        jm109[jm99] = (jm110[jm99] + (float)j) - (float)jm119;
        jm100[jm99] = (float)Math.floor(((jm109[jm99] - 17F) / (float)jm113[jm99]) * jm114[jm99]);
        jm32(jm99);
        paint(jm83);
    }

    private void jm32(int i)
    {
        if(jm100[i] < 0.0F || jm109[i] <= 17F)
        {
            jm100[i] = 0.0F;
            jm109[i] = 17F;
        } else
        if(jm109[i] > (float)(jm111[i] - (int)jm108[i] - 1))
        {
            if(jm100[i] > jm114[i] - jm107[i])
                jm100[i] = jm114[i] - jm107[i];
            jm109[i] = jm111[i] - (int)jm108[i] - 1;
        }
    }

    private void jm33(int i, int j)
    {
        if(i < 17)
        {
            jm115[j] = false;
            jm97 = true;
        }
        if(i > 17 && (float)i < jm109[j] + jm108[j] && jm80[25] < 2)
        {
            jm97 = true;
            jm117[j] = false;
        }
        if(j == 0)
        {
            if(i > jm101[j] - 17 && i < jm101[j] && j == 0)
            {
                jm116[j] = false;
                jm97 = true;
            }
            if((float)i > jm109[j] + jm108[j] && i < jm80[9] - 34 && j == 0 && jm80[25] < 2)
            {
                jm97 = true;
                jm118[j] = false;
            }
            if(!jm115[0])
                ScrollTo(jm100[0] - 5F, 0);
            if(!jm116[0])
                ScrollTo(jm100[0] + 5F, 0);
            if(!jm117[0])
                ScrollTo(jm100[0] - jm107[0] * 5F, 0);
            if(!jm118[0])
                ScrollTo(jm100[0] + jm107[0] * 5F, 0);
        } else
        {
            if(i > jm102[j] - 17 && i < jm102[j])
            {
                jm116[j] = false;
                jm97 = true;
            }
            if((float)i > jm109[j] + jm108[j] && i < jm80[10] - 34 && jm80[25] < 2)
            {
                jm97 = true;
                jm118[j] = false;
            }
            if(!jm115[1])
                ScrollTo(jm100[1] - 1.0F, 1);
            if(!jm116[1])
                ScrollTo(jm100[1] + 1.0F, 1);
            if(!jm117[1])
                ScrollTo(jm100[1] - (float)((jm80[10] - 34) / jm80[12]), 1);
            if(!jm118[1])
                ScrollTo(jm100[1] + (float)((jm80[10] - 34) / jm80[12]), 1);
        }
    }

    private void jm34()
    {
        jm115[0] = true;
        jm115[1] = true;
        jm116[0] = true;
        jm116[1] = true;
        jm97 = false;
    }

    private void jm35()
    {
        jm117[0] = true;
        jm117[1] = true;
        jm118[0] = true;
        jm118[1] = true;
        jm98 = false;
    }

    private void jm36(int i, int j, int k)
    {
        jm99 = k;
        if(jm80[25] == 3)
            return;
        if(jm103[k] == 1)
            j = i;
        jm119 = j;
        jm110[k] = jm109[k];
        if((float)j >= jm109[k] && (float)j <= jm109[k] + jm108[k])
        {
            jm97 = false;
            jm120 = true;
            paint(jm83);
            return;
        } else
        {
            jm33(j, k);
            paint(jm83);
            return;
        }
    }

    private void jm37(Graphics g, int i)
    {
        int j = jm105[i];
        int k = jm106[i];
        if(jm103[i] == 0 && jm80[25] != 3)
        {
            if(jm80[8] != 0 && jm80[25] == 0)
            {
                g.setColor(jm84[jm80[21]]);
                g.fillRect((j + 8) - jm80[8], k + 17, (jm101[i] - 17) + 2 * jm80[8], jm102[i] - 34);
                g.setColor(jm84[jm80[19]]);
                g.drawRect((j + 8) - jm80[8], k + 17, (jm101[i] - 17) + 2 * jm80[8], jm102[i] - 34);
            }
            if(jm80[25] < 2)
                jm39(j + 1, k + (int)jm109[i] + 1, jm101[i] - 3, (int)jm108[i] - 1, g, true);
            g.setColor(jm84[jm80[19]]);
            jm39(j + 1, k + 1, jm101[i] - 3, 15, g, jm115[i]);
            g.setColor(jm84[jm80[20]]);
            jm38(j + 8, 7 + k, 5 + j, 10 + k, 11 + j, 10 + k, g);
            g.setColor(jm84[jm80[19]]);
            jm39(j + 1, (k + jm80[10]) - 33, jm101[i] - 3, 15, g, jm116[i]);
            g.setColor(jm84[jm80[20]]);
            jm38(j + 8, (k + jm80[10]) - 17 - 7, j + 5, (k + jm80[10]) - 17 - 10, j + 11, (k + jm80[10]) - 17 - 10, g);
        } else
        if(jm80[25] != 3)
        {
            if(jm80[8] != 0 && jm80[25] == 0)
            {
                g.setColor(jm84[jm80[21]]);
                g.fillRect(j + 17, (k + 8) - jm80[8], jm101[i] - 34, (jm102[i] - 17) + 2 * jm80[8]);
                g.setColor(jm84[jm80[19]]);
                g.drawRect(j + 17, (k + 8) - jm80[8], jm101[i] - 34, (jm102[i] - 17) + 2 * jm80[8]);
            }
            if(jm80[25] < 2)
                jm39(j + (int)jm109[i] + 1, k + 1, (int)jm108[i] - 1, jm102[i] - 3, g, true);
            g.setColor(jm84[jm80[19]]);
            jm39(j + 1, k + 1, 15, jm102[i] - 3, g, jm115[i]);
            g.setColor(jm84[jm80[20]]);
            jm38(7 + j, 8 + k, 10 + j, 5 + k, 10 + j, 11 + k, g);
            g.setColor(jm84[jm80[19]]);
            jm39((j + jm80[9]) - 33, k + 1, 15, jm102[i] - 3, g, jm116[i]);
            g.setColor(jm84[jm80[20]]);
            jm38((j + jm80[9]) - 15 - 9, k + 8, (j + jm80[9]) - 15 - 12, k + 5, (j + jm80[9]) - 15 - 12, k + 11, g);
        }
    }

    private void jm38(int i, int j, int k, int l, int i1, int j1, Graphics g)
    {
        int ai[] = new int[4];
        int ai1[] = new int[4];
        ai[0] = i;
        ai1[0] = j;
        ai[1] = k;
        ai1[1] = l;
        ai[2] = i1;
        ai1[2] = j1;
        ai[3] = i;
        ai1[3] = j;
        g.fillPolygon(ai, ai1, 4);
        g.drawPolygon(ai, ai1, 4);
    }

    private void jm39(int i, int j, int k, int l, Graphics g, boolean flag)
    {
        g.setColor(jm84[jm80[18]]);
        g.fillRect(i, j, k, l);
        g.draw3DRect(i, j, k, l, flag);
        g.draw3DRect(i + 1, j + 1, k - 2, l - 2, flag);
        g.setColor(jm84[jm80[19]]);
        g.drawRect(i - 1, j - 1, k + 2, l + 2);
    }

    boolean jm40;
    Image jm41[];
    int jm42;
    WPTreeNode jm43[];
    WPTreeNode jm44[];
    int jm45;
    boolean jm46;
    boolean jm47;
    int jm48;
    boolean jm49;
    FontMetrics jm50;
    FontMetrics jm51;
    WPTreeNode jm52;
    int jm53[];
    Vector jm54;
    int jm55;
    long jm56;
    StringTokenizer jm57;
    int jm58;
    int jm59;
    int jm60;
    int jm61;
    int jm62;
    int jm63;
    int jm64;
    int jm4;
    int jm65;
    int jm66;
    int jm67;
    int jm68;
    int jm69;
    int jm70;
    int jm71;
    int jm72;
    int jm73;
    int jm74;
    boolean jm75;
    Thread jm76;
    Image jm77;
    Image jm78[];
    Graphics jm79[];
    int jm80[];
    String jm81[] = {
        "NumberNodes", "TreeDelay", "ScrollDelay", "MaximumCollapse", "DragDrop", "DoubleClick", "ClickInterval", "ShowFoldersEmpty", "InnerBarWidth", "InitWidth", 
        "InitHeight", "BlockRightButton", "ImageHeights", "ImageWidths", "BackColor", "HighlightColor", "HighlightTextColor", "MouseOverColor", "ScrollbarColor", "ScrollbarOutlineColor", 
        "ArrowColor", "InnerBarColor", "Tile", "BlockStartPaint", "ImageWait", "SetScrollbar", "ScrollbarDelay", "DescrFont"
    };
    Graphics jm82;
    Graphics jm83;
    Color jm84[];
    Font jm85[];
    int jm86;
    int jm87;
    int jm88;
    String jm89;
    boolean jm90;
    MediaTracker jm91;
    boolean jm92;
    int jm93;
    int jm94;
    StringTokenizer jm95;
    URL jm96;
    boolean jm97;
    boolean jm98;
    int jm99;
    float jm100[];
    int jm101[];
    int jm102[];
    int jm103[];
    boolean jm104[];
    int jm105[];
    int jm106[];
    float jm107[];
    float jm108[];
    float jm109[];
    float jm110[];
    int jm111[];
    int jm112[];
    int jm113[];
    float jm114[];
    boolean jm115[];
    boolean jm116[];
    boolean jm117[];
    boolean jm118[];
    int jm119;
    boolean jm120;
    Font jm121;
    int jm122;
    String jm123;
    boolean jm124;
    String jm125;
    byte jm126[];
    String jm127;
    int jm128;
    int jm129;
    long jm130;
}
