<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();

	checkAccess($TID);

	if ($DOCSprefs['trashAge'] != 0) {
		// Cleanup expired trash docs:

		// Fetch all files in trash, last modified since trashAge or before
		$sql = "SELECT id FROM {$DOCSprefs['table']} WHERE kind = 'file' AND UNIX_TIMESTAMP(modified_date) < " . mktime(0, 0, 0, date('m'), date('d') - $DOCSprefs['trashAge'], date('Y')) . " AND folder_id = " . (int)$DOCSprefs['trashFolder'];
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$file_path = $DOCSprefs['saveDir'] . '/' . $db->FetchResult($cur,$x,'id');
			
			if (is_file($file_path)) {
				$unlinked = @unlink($file_path);
			} else {
				$unlinked = true;
			}

			if ($unlinked) {
				$sql = "DELETE FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$db->FetchResult($cur,$x,'id');
				$req = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			}
		}
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Phoundry</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<style type="text/css">
<!--

div.contextEl {
	position: absolute;
	background: #fff;
	border: 1px solid black;
	z-index: 11;
	filter:progid:DXImageTransform.Microsoft.Shadow(color=#777777,Direction=135,Strength=2)
}

ul.contextEl {
	margin: 0;
	padding: 0;
	list-style: none;
	width: 100px;
}

ul.contexEl li {
	position: relative;
}

ul.contextEl li a:link, ul.contextEl li a:visited {
	display: block;
	text-decoration: none;
	color: #0046d5;
	padding: 2px;
}

ul.contextEl li a:hover {
	background: #ccc;
	color: #000;
	text-decoration: none;
}

/* DTREE CSS */

.DT {
	font: 11px Futura;
	font: Icon;
	color: #666;
	white-space: nowrap;
}
.DT img {
	border: 0px;
	vertical-align: middle;
}
.DT a:link, .DT a:visited, .DT a:active {
	color: #333;
	text-decoration: none;
}
.DT a.N, .DT a.NS {
	white-space: nowrap;
	padding-left: 2px;
}
.DT a.N:hover, .DT a.NS:hover {
	text-decoration: underline;
}
.DT a.NS:link, .DT a.NS:visited, .DT a.NS:active {
	background-color: #0c2e82;
	color: #fff;
}
.DT .clip {
	overflow: hidden;
}

-->
</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript" src="js/dtree.js"></script>
<script type="text/javascript">
//<![CDATA[

var contextEl = null, contextTmr = null;

/******************************* File functions ******************************/

function fileSelectL(e, node_id) {
	makePopup(e, 400, 400, 'Bekijk bestand', 'fileView.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

function fileSelectR(e, node_id, rights) {
	showContext(e, 'file', node_id, rights);
}

function fileAdd(e, node_id) {
	makePopup(e, 400, 400, 'Upload bestand', 'fileAdd.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

function fileDel(e, node_id, perm) {
	if (confirm('Zeker weten?')) {
		var file = perm ? 'fileDel' : 'fileTrash';
		_D.location = file + '.php?<?= QS(0, '_msg') ?>&node_id='+node_id;
	}
}

function fileProps(e, node_id) {
	makePopup(e, 400, 400, 'Eigenschappen bestand', 'fileProps.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

function fileMove(e, node_id) {
	makePopup(e, 400, 400, 'Verplaats bestand', 'fileMove.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

function fileUpgrade(e, node_id) {
	makePopup(e, 400, 400, 'Nieuwe versie bestand', 'fileUpgrade.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

/****************************** /File functions ******************************/

/****************************** Folder functions *****************************/

function rootSelectR(e, node_id) {
	showContext(e, 'root', node_id);
}

function fldrSelectL(e, node_id) {
	makePopup(e, 400, 300, 'Bekijk map', 'fldrView.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

function fldrSelectR(e, node_id, rights) {
	showContext(e, 'folder', node_id, rights);
}

function fldrAdd(e, node_id) {
	makePopup(e, 400, 320, 'Nieuwe map', 'fldrAdd.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

function fldrDel(e, node_id) {
	if (confirm('Zeker weten?')) {
		_D.location = 'fldrDel.php?<?= QS(0, '_msg') ?>&node_id='+node_id;
	}
}

function fldrProps(e, node_id) {
	makePopup(e, 400, 320, 'Eigenschappen map', 'fldrProps.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

function fldrMove(e, node_id) {
	makePopup(e, 400, 400, 'Verplaats map', 'fldrMove.php?<?= QS(0, '_msg') ?>&node_id='+node_id);
}

/***************************** /Folder functions *****************************/

function showDetails(node_id, Z, isFolder) {
	var el = gE('details');
	if (!el) return;

	var f = '';

	if (isFolder){
		f = '&f=1';
	}

	$.ajax({type:'GET', url:'fileInfo.php?node_id=' + escape(node_id) + '&Z=' + Z + f, dataType:'html', success:function(msg) {
			if (msg) {
				wH(el, response);
				SE(el);
			}
			else {
				HE(el);
			}
		}
	});
}

function showContext(e, kind, node_id, rights) {
	hideContext();
	var hasMenu = false;

	var top = e.clientY, left = e.clientX, html = '';
	if (kind == 'folder') {
		html = '<ul class="contextEl">';
		
		if (rights.indexOf('w') != -1) {
			html += '<li><a href="#" onclick="fileAdd(event,'+node_id+');return false">Upload bestand</a></li>';
			html += '<li><a href="#" onclick="fldrAdd(event,'+node_id+');return false">Nieuwe map</a></li>';
			hasMenu = true;
		}
		
		if (rights.indexOf('d') != -1) {
			html += '<li><a href="#" onclick="fldrDel(event,'+node_id+');return false">Verwijder</a></li>';
			hasMenu = true;
		}
		
		if (rights.indexOf('w') != -1) {
			html += '<li><a href="#" onclick="fldrProps(event,'+node_id+');return false">Wijzig</a></li>';
			hasMenu = true;
		}
		
		if (rights.indexOf('m') != -1) {
			html += '<li><a href="#" onclick="fldrMove(event,'+node_id+');return false">Verplaats</a></li>';
			hasMenu = true;
		}

		if (rights.indexOf('z') != -1) {
			html += '<li><a href="#" onclick="fileDel(event,\'all\',true);return false">Verwijder alle eigen bestanden</a></li>';
			hasMenu = true;
		}
		
		html += '<li><a href="#" onclick="makePopup(event, 400, 300, \'Bekijk map\', \'fldrView.php?<?= QS(0, '_msg') ?>&node_id='+node_id +'\');return false">Eigenschappen</a></li>';

		html += '</ul>';
	}
	else if (kind == 'file') {
		html = '<ul class="contextEl">';
		
		if (rights.indexOf('r') != -1) {
			html += '<li><a href="#" onclick="fileUpgrade(event,'+node_id+');return false">Nieuwe versie</a></li>';
		}

		if (rights.indexOf('t') != -1) {
			html += '<li><a href="#" onclick="fileDel(event,'+node_id+', true);return false">Verwijder</a></li>';
		} else if (rights.indexOf('d') != -1) {
			html += '<li><a href="#" onclick="fileDel(event,'+node_id+', false);return false">Verwijder</a></li>';
		}
		
		if (rights.indexOf('w') != -1) {
			html += '<li><a href="#" onclick="fileProps(event,'+node_id+');return false">Wijzig</a></li>';
		}
		
		if (rights.indexOf('m') != -1) {
			html += '<li><a href="#" onclick="fileMove(event,'+node_id+');return false">Verplaats</a></li>';
		}

		html += '<li><a href="#" onclick="makePopup(event, 400, 400, \'Bekijk bestand\', \'fileView.php?<?= QS(0, '_msg') ?>&node_id='+node_id +'\');return false">Eigenschappen</a></li>';

		html += '</ul>';

	}
	else if (kind == 'root') {
		html = '<ul class="contextEl"><li><a href="#" onclick="fileAdd(event,'+node_id+');return false">Upload bestand</a></li><li><a href="#" onclick="fldrAdd(event,'+node_id+');return false">Nieuwe map</a></li></ul>';
	}
	else {
		return;
	}

	if (is.ie) {
		sl = (is.ieBox) ? _D.body.scrollLeft : _D.documentElement.scrollLeft;
		st = (is.ieBox) ? _D.body.scrollTop  : _D.documentElement.scrollTop;
	}
	else {
		sl = _W.pageXOffset;
		st = _W.pageYOffset;
	}
	contextEl = _D.createElement('div');
	contextEl.className = 'contextEl';
	contextEl.style.left = left+'px';
	contextEl.style.top  = top+'px';
	contextEl.innerHTML = html;
	contextEl.onmouseout = function(){contextTmr=setTimeout('hideContext()',1000)};
	contextEl.onmouseover = function(){clearTimeout(contextTmr)};
	_D.body.appendChild(contextEl);
}

function hideContext() {
	d = gE('details');
	if (d) HE(d);
	clearTimeout(contextTmr);
	_D.oncontextmenu = null;
	if (contextEl) {
		_D.body.removeChild(contextEl);
		contextEl = null;
	}
}

function init() {
	<?php if (isset($_GET['_msg'])) { ?>
	alert('<?= escSquote($_GET['_msg']) ?>');
	<?php } ?>
}

/******************************* Drag & Drop ********************************/

/*
function startDrag() {
   srcObj = _W.event.srcElement;
   var dragData = _W.event.dataTransfer;
   dragData.effectAllowed = 'linkMove';
   dragData.dropEffect = 'move';
}

function overDrag() {
   _W.event.returnValue = false;
}

function drop(dstObj) {
   _W.event.returnValue = false;
}
*/

//]]>
</script>
</head>
<body class="frames" onload="init()" onclick="hideContext()">
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="get">
<?php
	foreach($_GET as $name=>$value) {
		if ($name{0} == '_' || substr($name,0,2) == 'PH' || $name == 'RID' || $value === '')
			continue;
		print '<input type="hidden" name="' . $name . '" value="' . PH::htmlspecialchars($value) . '" />' . "\n";
	}
?>
<input type="hidden" name="PHsrch" value="<?= isset($_GET['PHsrch']) ? PH::htmlspecialchars($_GET['PHsrch']) : '' ?>" />
<input type="hidden" name="PHext" value="<?= isset($_GET['PHext']) ? PH::htmlspecialchars($_GET['PHext']) : '*' ?>" />
<input type="hidden" name="PHinFilename" value="<?= isset($_GET['PHinFilename']) ? PH::htmlspecialchars($_GET['PHinFilename']) : '' ?>" />
<input type="hidden" name="PHinDescr" value="<?= isset($_GET['PHinDescr']) ? PH::htmlspecialchars($_GET['PHinDescr']) : '*' ?>" />
<input type="hidden" name="PHauth" value="<?= isset($_GET['PHauth']) ? PH::htmlspecialchars($_GET['PHauth']) : '' ?>" />
<input type="hidden" name="PHalteredSince" value="<?= isset($_GET['PHalteredSince']) ? PH::htmlspecialchars($_GET['PHalteredSince']) : '' ?>" />

<input type="hidden" name="node_id" />
<div id="headerFrame" class="headerFrame">
<?= getHeader('Documenten beheer', ($DOCSprefs['allowMail'] ? $DOCSprefs['email'] : '')); ?>
<div style="position:absolute;top:100px;right:14px;background-color:#ffc;border:1px solid #000;display:none;padding:2px;width:300px;z-index:2" id="details"></div>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td><input type="button" onclick="Tree.closeAll()" value="Close all" />
<td><input type="button" onclick="Tree.openAll()" value="Open all" />
<td>Toon:</td>
<td><select onchange="document.location='?<?= QS(1,'PHdate') ?>&PHdate=' + this.options[this.options.selectedIndex].value">
	<option value="0">Alles</option>
	<?php
		$fd = isset ($_GET['PHdate']) ? $_GET['PHdate'] : mktime(0, 0, 0, date("m") - 3, date ("d"), date ("Y"));
		
		$dateFilters = array (
										'Vandaag' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
										'Laatste week' => mktime(0, 0, 0, date('m'), date ('d') - 7, date ('Y')),
										'Laatste maand' => mktime(0, 0, 0, date('m') - 1, date ('d'), date ('Y')),
										'Laatste 3 maanden' => mktime(0, 0, 0, date('m') - 3, date ('d'), date ('Y')),
										'Laatste 6 maanden' => mktime(0, 0, 0, date('m') - 6, date ('d'), date ('Y')),
										'Laatste jaar' => mktime(0, 0, 0, date('m'), date ('d'), date ('Y') - 1)
									);

		foreach ($dateFilters as $desc => $timestamp) {
			print '<option value="' . $timestamp . '" ' . ($timestamp == $fd ? 'selected="selected"' : '') . '>' . $desc . '</option>';
		}
	?>
</select></td>
<td>Extensie:</td>
<td>

<select onchange="document.location='?<?= QS(1,'PHext') ?>&PHext=' + this.options[this.options.selectedIndex].value">
	<option value="*">*</option>
<?php
	$gf = isset ($_GET['PHext']) ? $_GET['PHext'] : '*';
	$sql = "SELECT DISTINCT SUBSTRING_INDEX(name, '.', -1) AS ext FROM docs_file WHERE kind = 'file' ORDER BY ext";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$cur_ext = strtolower($db->FetchResult($cur, $x, 'ext'));
		$cur_opt = '*.' . $cur_ext;
		print '<option value="' . $cur_ext . '" ' . ($cur_ext == $gf ? 'selected="selected"' : '') . '>' . $cur_opt . '</option>';
	}
?>
<td width="100%" align="right">
<input type="button" id="vwBut" value="Voorwaarden" onclick="makePopup(null,340,260,'Voorwaarden','voorwaarden.php?<?= QS(1) ?>');" />
<input type="button" id="zoekBut" value="Zoek" onclick="makePopup(null,340,340,'Zoek','search.php?<?= QS(1) ?>');" />
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<fieldset>
<script type="text/javascript">
//<![CDATA[

var imgpath = './pics/tree';
var Tree = new dTree('Tree');
Tree.config.inOrder = true;
Tree.add(0,-1,'<b>root</b>','#','','','rootSelectR(event,0)','root','','','',1);
<?php
	$tree = getTree(false);
	foreach($tree as $item) {
		$read = strpos($item['rights'],'r') !== false || $item['parent'] == $DOCSprefs['inboxFolder'];
			
		if ($item['kind'] == 'folder') {
			$isOpen = ((empty ($_GET['PHsrch']) && (empty ($_GET['PHext']) || $_GET['PHext'] == '*')) || strpos($item['rights'], 'r') === false) ? 0 : 1;

			if ($DOCSprefs['trashFolder'] == $item['id']) {
				$item['rights'] = 'z';
			}

			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . escSquote($item['append']) . "', '#', 'showDetails({$item['id']}," . ($read ? crc32('WP' . $item['id']) : 0) . ",true)', \"fldrSelectL(event,{$item['id']})\", \"fldrSelectR(event,{$item['id']},'{$item['rights']}')\", '" . ($read ? "door {$item['author']} ({$item['date']})" : "") . "', '', '{$item['img_n']}', '{$item['img_o']}', {$isOpen});\n";
		}
		else {
			if ($DOCSprefs['trashFolder'] == $item['parent'] && strpos('d', $item['rights']) !== false) {
				$item['rights'].= 't';
			}

			$leftClick = $read ? "document.location='fileDownload.php?" . QS(0, "node_id={$item['id']}&Z=" . crc32('WP' . $item['id'])) . "'" : '';

			// , \"fileSelectL(event,{$item['id']})\"
			print "Tree.add({$item['id']}, {$item['parent']}, '" . (!$read ? '<em style="color:#888">' : '') . escSquote($item['text']) . (!$read ? '</em>' : '') . escSquote($item['append']) . "', '#', 'showDetails({$item['id']}," . ($read ? crc32('WP' . $item['id']) : 0) . ")', \"{$leftClick}\", \"fileSelectR(event,{$item['id']},'{$item['rights']}')\", '" . ($read ? "{$item['version']}, door {$item['author']} ({$item['date']}) - {$item['size']}" : "") . "', '', '{$item['img_n']}', '{$item['img_o']}', 1);\n";
		}
	}
?>

_D.write(Tree);

//]]>
</script>
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<?php
		$used = diskSpace('used');
		$perc  = $used/$DOCSprefs['diskSpace']*100;
		if ($perc > 100) { $perc = 100; }
		print '<img src="pics/bar' . round($perc/10) . '.gif" width="132" height="12" align="left" title="' . number_format($used,2) . ' van ' . $DOCSprefs['diskSpace'] . ' MB" /> ' . number_format($perc,2) . '%';
	?>
	</td>
	<td align="right">
	<input type="button" value="Annuleer" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
</tr></table>
</div>


<?php if (!empty($_GET['PHsrch'])) { ?>
	<div id="blDiv" style="position:absolute;right:0;top:0;padding:4px;z-index:2">
	<table cellspacing="0" cellpadding="0">
	<tr><td><img src="<?= $PHprefs['url'] ?>/core/pics/ballon_top_<?= $PHSes->lang ?>.gif" alt="" width="158" height="15" border="0" usemap="#balloonMap" /></td></tr>
	<tr><td background="<?= $PHprefs['url'] ?>/core/pics/ballon_mid.gif" style="height:24px;padding:0 6px 0 6px">&nbsp;<?= PH::htmlspecialchars($_GET['PHsrch']) ?></td></tr>
	<tr><td><img src="<?= $PHprefs['url'] ?>/core/pics/ballon_bot.gif" alt="" width="158" height="24" /></td></tr>
	</table>
	</div>
	<map name="balloonMap">
	<area shape="rect" coords="138,3,151,16" href="#" onclick="$('#blDiv').hide()" />
	<area shape="rect" coords="73,5,115,13" href="#" onclick="_D.location=_D.location.href.replace(/(&|\?)PHsrch=[^&]+/, '').replace(/(&|\?)PHext=[^&]+/, '')" />
	</map>
<?php } ?>

</form>
</body>
</html>
