<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	/*
	 * Skip access check, while this script can be called from the
	 * website (no Phoundry Session available) as well.
	 *
	 
	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	*/

	if (crc32('WP' . $_GET['node_id']) != $_GET['Z']) {
		die('No hacking!');
	}
	
	// Fetch file info:
	$sql = "SELECT id, name, description, version FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($cur)) {
		$file_id = $db->FetchResult($cur,0,'id');
		$file_name = $db->FetchResult($cur,0,'name');
		$extension = strtolower(substr($file_name, strrpos($file_name, '.')+1));

		$fopen  = in_array($extension,$DOCSprefs['nozipExts']) ? '' : 'compress.zlib://';
		$fopen .= $DOCSprefs['saveDir'] . '/';
		$fopen .= $file_id;

		if ($fp = @fopen($fopen, 'rb')) {
			$name = $db->FetchResult($cur,0,'name');
			$ext = strtolower(substr($name, strrpos($name, '.')+1));
			$disposition = in_array($ext, $DOCSprefs['inlineExts']) ? 'inline' : 'attachment';
			header('Content-Type: ' . getContentType($name));
			header('Content-Disposition: ' . $disposition . '; filename=' . $name);
			header('Content-Transfer-Encoding: binary');

			fpassthru($fp);
			exit();
		}
		else {
			die('Not found (on disk)!');
		}
	}
	else {
		die('Not found (in DB)!');
	}
?>
