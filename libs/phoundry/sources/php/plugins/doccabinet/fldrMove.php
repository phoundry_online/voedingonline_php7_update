<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		// Move folder:
		$dest_id = (int)$_POST['dest_id'];
		if ($dest_id == 0) $dest_id = 'NULL';
		$sql = "UPDATE {$DOCSprefs['table']} SET folder_id = {$dest_id}, history = CONCAT(IFNULL(history,''),'\nMoved by {$PHSes->userName} at " . date('Y-m-d H:i:s') . ".') WHERE kind = 'folder' AND id = " . (int)$_GET['node_id'];
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		print '<script type="text/javascript">' . "\n";
		print "//<![CDATA[\n";
		print "parent.location.reload();\n";
		print "//]]>\n";
		print "</script>\n";
		exit();
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Verplaats map</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="css/dtree.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript" src="js/dtree.js"></script>
<script type="text/javascript">
//<![CDATA[

function fldrSelect(e, node_id) {
	_D.forms[0]['dest_id'].value = node_id;
}

function init() {
	<?php if (isset($_GET['_msg'])) { ?>
	alert('<?= escSquote($_GET['_msg']) ?>');
	<?php } ?>
}

//]]>
</script>
</head>
<body onload="init()">
<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="dest_id" alt="1|integer" />
<fieldset><legend><b class="req">Map</b></legend>
<script type="text/javascript">
//<![CDATA[

var imgpath = '<?= $PHprefs['url'] ?>/core/pics/tree';
var Tree = new dTree('Tree');
Tree.config.inOrder = true;
Tree.add(0,-1,'<b>root</b>','#','fldrSelect(event,0)','','','root','','','',true);
<?php
	$tree = getTree(true, '', 'w');
	foreach($tree as $item) {
		if ($item['id'] == (int)$_GET['node_id']) {
			// A folder cannot be moved to itself!
			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . "', '', '', '', '', '', '', '{$item['img_n']}', '{$item['img_o']}', 1);\n";
		}
		else {
			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . "', '#', 'fldrSelect(event,{$item['id']})', '', '', '', '', '{$item['img_n']}', '{$item['img_o']}', 1);\n";
		}
	}
?>

_D.write(Tree);

//]]>
</script>

</fieldset>


<p align="right">
<input type="submit" value="<?= word(82) ?>" />
<input type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</p>
</form>
</body>
</html>
