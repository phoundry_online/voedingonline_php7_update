<?php

/**
 * Determine the content-type for a file.
 *
 * @author Arjan Haverkamp
 * @param $filename(string) The filename to get the mimetype for.
 * @return If known, the content-type for the file, 'application/octet-stream'
 *         if unknown.
 * @returns string
 */
function getContentType($filename) {

	$mimeTypes = array(
		'doc' => 'application/msword',
		'zip' => 'application/zip',
		'xls' => 'application/vnd.ms-excel',
		'ppt' => 'application/vnd.ms-powerpoint',
		'gif' => 'image/gif',
		'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'bmp' => 'image/bmp',
		'png' => 'image/png',
		'tif' => 'image/tiff',
		'tiff' => 'image/tiff',
		'pdf' => 'application/pdf',
		'hqx' => 'application/mac-binhex40',
		'swf' => 'application/x-shockwave-flash',
		'sit' => 'application/x-stuffit',
		'mp3' => 'audio/mpeg',
		'html' => 'text/html',
		'htm' => 'text/html',
		'txt' => 'text/plain',
		'xml' => 'text/xml',
		'eml' => 'message/rfc822'
	);

	$extension = strtolower(substr($filename, strrpos($filename, '.')+1));
	return isset($mimeTypes[$extension]) ? $mimeTypes[$extension] : 'application/octet-stream';
}

function userid2username($userId) {
	global $db;

	$sql = "SELECT name FROM phoundry_user WHERE id = " . (int)$userId;
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($cur)) {
		return $db->FetchResult($cur,0,'name');
	}
	return '';
}

function getPath($value) {
	global $db, $DOCSprefs;

	$parents = array();
	do {
		$sql = "SELECT id, name, folder_id FROM {$DOCSprefs['table']} WHERE kind = 'folder' AND id = " . (int)$value;
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur)) {
			$value = $db->FetchResult($cur,0,'folder_id');
			$parents[] = $db->FetchResult($cur,0,'name');
		}
		else
			$value = null;
	} while (!is_null($value));
	return '/' . implode('/', array_reverse($parents));
}

function getTree($foldersOnly = false, $curid = '', $reqRights = '') {
	global $db, $DOCSprefs, $PHSes, $g_dateFormat;

	$df = str_replace(array('DD','MM','YYYY'), array('d','m','Y'), $g_dateFormat);
	$type = array('b', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	
	static $knownExtImgs = array('zip','xls','txt','swf','ppt','png','pdf','jpg','html','gif','doc','csv','bmp');
	$menu = array();

	$closedFolders = array();

	$ta = $DOCSprefs['trashAge'] > 0;
	if ($ta) {
		$taDate = mktime(0, 0, 0, date("m"), date("d") - $DOCSprefs['trashAge'], date("Y"));
	}

	// Directories:
	$sql = "SELECT d.id AS id, d.name, folder_id, version, created_by, UNIX_TIMESTAMP(modified_date) AS md, rights, public, u.name as username FROM {$DOCSprefs['table']} d LEFT JOIN phoundry_user u ON u.id = d.created_by LEFT JOIN {$DOCSprefs['tableRights']} r ON d.id = r.file_id AND group_id = " . (int)$PHSes->groupId . " WHERE kind = 'folder' AND is_last_version = 1 AND folder_id is null AND (d.created_by = u.id OR d.created_by = 0) ORDER BY name";
	if ($curid != '') {
		$sql = str_replace('is null', "= $curid", $sql);
	}
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$nextid = $db->FetchResult($cur,$x,'id');
		$parent = $db->FetchResult($cur,$x,'folder_id');
		$name   = $db->FetchResult($cur,$x,'name');
		$cre_by = $db->FetchResult($cur,$x,'created_by') == $PHSes->userId;
		$rights = $cre_by ? 'rwdm' : $db->FetchResult($cur,$x,'rights');
		$img_n  = 'folder.gif';
		$img_o  = 'folderopen.gif';
		if ($name == '#Trash') {
			$name = 'Trash';
			$img_n = $img_o = 'trash.gif';
			$trashId = $nextid;
		}
		elseif ($name == '#Inbox') {
			$name = 'Inbox';
			$img_n = $img_o = 'inbox.gif';
		}

		if (hasRights ($reqRights, $rights) && ($DOCSprefs['inboxFolder'] != $nextid || ($DOCSprefs['inboxFolder'] == $nextid && $DOCSprefs['allowMail']))) {

			if ($db->FetchResult($cur,$x,'public') == 1) {
				$append = " <em>(<span style='color:red'>publiek</span>)</em>";
			} else {
				$append = '';
			}

			$author = $db->FetchResult($cur,$x,'username');
			if (is_null($author)) $author = "Phoundry";

			$menu[] = array('id' => $nextid,
								 'text' => $name,
								 'author' => $author,
								 'date' => date($df, $db->FetchResult($cur,$x,'md')),
								 'append' => $append,
								 'kind' => 'folder',
								 'parent' => is_null($parent) ? 0 : $parent,
								 'img_n' => $img_n,
								 'img_o' => $img_o,
								 'rights' => $rights
								);

			if (strpos ($rights, 'r') !== false || $nextid == $DOCSprefs['inboxFolder'] || $nextid == $DOCSprefs['trashFolder']) {
				$menu = array_merge($menu, getTree($foldersOnly, $nextid));
			}
		}
	}

	// Files:
	if (!$foldersOnly) {
		$and = '';

		// Find-query executed
		if (isset ($_GET['PHsrch'])) {

			if ($_GET['PHinDescr'] == '1' && $_GET['PHinFilename'] == '1') {
				$and .= "AND (MATCH(description, contents) AGAINST (\"" . escDBquote($_GET['PHsrch']) . "\" IN BOOLEAN MODE) OR d.name LIKE \"" . escDBquote(convToLike($_GET['PHsrch'])) . "\") ";
			} else if ($_GET['PHinDescr'] == '1') {
				$and .= "AND MATCH(description, contents) AGAINST (\"" . escDBquote($_GET['PHsrch']) . "\" IN BOOLEAN MODE) ";
			} else if ($_GET['PHinFilename'] == '1') {
				$and .= "AND d.name LIKE \"%" . escDBquote(convToLike ($_GET['PHsrch'])) . "%\" ";
			}

			if (isset($_GET['PHauth']) && !empty($_GET['PHauth'])) {
				$and.= "AND u.name LIKE \"%" . escDBquote($_GET['PHauth']) . "%\" ";
			}

			if (isset ($_GET['PHalteredSince']) && !empty ($_GET['PHalteredSince'])) {
				$date = explode('-', $_GET['PHalteredSince']);
				$since = mktime(0, 0, 0, (int)$date[1], (int)$date[0], (int)$date[2]);
				$and.= "AND UNIX_TIMESTAMP(modified_date) >= " . $since . " ";
			}
		}
		
		if (isset($_GET['PHext']) && $_GET['PHext'] != '*') {
			$and.= "AND d.name LIKE '%." . escDBquote($_GET['PHext']) . "' ";
		}

		if (isset ($_GET['PHdate'])) {
			$and.= "AND UNIX_TIMESTAMP(modified_date) >= " . (int)$_GET['PHdate'] . " ";
		}

		if (count ($closedFolders) > 0) {
			$and.= "AND parent_id NOT IN(" . implode (',', $closedFolders) . ") ";
		}
		//$sql = "SELECT d.id AS id, d.name, folder_id, version, size, created_by, UNIX_TIMESTAMP(modified_date) AS md, rights, public, u.name as username FROM {$DOCSprefs['table']} d, phoundry_user u LEFT JOIN {$DOCSprefs['tableRights']} r ON d.id = r.file_id AND group_id = '{$PHSes->groupId}' WHERE kind = 'file' AND is_last_version = 1 AND folder_id is null AND d.created_by = u.id {$and} ORDER BY name";

		$sql = "SELECT d.id, d.name, d.folder_id, d.version, d.size, d.created_by, UNIX_TIMESTAMP(d.modified_date) AS md, r.rights, d.public, u.name as username 
		FROM {$DOCSprefs['table']} d
		INNER JOIN phoundry_user u ON d.created_by = u.id
		LEFT JOIN {$DOCSprefs['tableRights']} r ON d.id = r.file_id AND r.group_id = " . (int)$PHSes->groupId . "
		WHERE d.kind = 'file' 
		AND d.is_last_version = 1 
		AND d.folder_id is null
		AND d.created_by = u.id {$and} ORDER BY name";
		if ($curid != '') {
			$sql = str_replace('is null', "= $curid", $sql);
		}

		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$parent = $db->FetchResult($cur,$x,'folder_id');
			$name   = $db->FetchResult($cur,$x,'name');
			$cre_by = $db->FetchResult($cur,$x,'created_by') == $PHSes->userId;
			$rights = $cre_by ? 'rwdm' : $db->FetchResult($cur,$x,'rights');
			$ext = strtolower(substr($name, strrpos($name, '.')+1));
			$icon = in_array($ext, $knownExtImgs) ? $ext.'.gif' : '';

			if ($db->FetchResult($cur,$x,'public') == 1) {
				$append = " <em>(<span style='color:red'>publiek</span>)</em>";
			} else {
				$append = '';
			}

			$filesize = $db->FetchResult($cur,$x,'size');
			for ($i = 0; $filesize > 1024; $i++) {
				$filesize /= 1024;
			}

			$menu[] = array('id' => $db->FetchResult($cur,$x,'id'),
								 'text' => $db->FetchResult($cur,$x,'name'),
								 'author' => $db->FetchResult($cur,$x,'username'),
								 'date' => date($df, $db->FetchResult($cur,$x,'md')),
								 'size' => round ($filesize, 2) . $type[$i],
								 'append' =>  ($ta && $parent == $DOCSprefs['trashFolder'] ? ' <em>(<span style=\'color:red\'>' . date('d-m-Y', $db->FetchResult($cur,$x,'md') + (86400 * $DOCSprefs['trashAge'])) . '</span>)</em>' : $append),
								 'kind' => 'file',
								 'version' => 'v'.sprintf('%01.1f', $db->FetchResult($cur,$x,'version')),
								 'parent' => is_null($parent) ? 0 : $parent,
								 'img_n' => $icon,
								 'img_o' => $icon,
								 'rights' => $rights
								);
		}
	}

	return $menu;
}

function getGroups() {
	global $db;

	$groups = array();
	$sql = "SELECT id, name FROM phoundry_group, phoundry_group_table WHERE id = group_id AND table_id = " . escDBquote($_GET['TID']) . " ORDER BY name";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$groups[$db->FetchResult($cur,$x,'id')] = $db->FetchResult($cur,$x,'name');
	}
	return $groups;
}

function hasRights ($req, $rights) {
	for ($i = 0; $i < strlen ($req); ++$i) {
		if (strpos ($rights, $req{$i}) === false) {
			return false;
		}
	}

	return true;
}

function grabContents($extension) {
	global $DOCSprefs;

	$contents = '';
		
	if (isset($DOCSprefs['parsers'][$extension])) {
		$parser = $DOCSprefs['parsers'][$extension];
		if (!empty($parser)) {
			$contents = escDBquote(shell_exec(str_replace("#1", $_FILES['file']['tmp_name'], $parser) . ' | /usr/bin/head -c ' . $DOCSprefs['contentBlock']));
		}
	}

	return $contents;
}

function convToLike($string) {
	return str_replace(array('*','+','-','"'), array('%','','',''), $string);
}

function getWebTree($curid = '') {
	global $db, $DOCSprefs, $PHSes;

	static $knownExtImgs = array('zip','xls','txt','swf','ppt','png','pdf','jpg','html','gif','doc','csv','bmp');
	$menu = array();

	$closedFolders = array();

	// Directories:
	$sql = "SELECT d.id AS id, name, folder_id, version, created_by FROM {$DOCSprefs['table']} d WHERE kind = 'folder' AND is_last_version = 1 AND folder_id is null AND public = 1 ORDER BY name";
	if ($curid != '')
			$sql = str_replace('is null', "= $curid", $sql);
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$nextid = $db->FetchResult($cur,$x,'id');
		$parent = $db->FetchResult($cur,$x,'folder_id');
		$name   = $db->FetchResult($cur,$x,'name');
		$img_n  = 'folder.gif';
		$img_o  = 'folderopen.gif';

		if ($nextid != $DOCSprefs['trashFolder'] && $nextid != $DOCSprefs['inboxFolder']) {
			$menu[] = array('id' => $nextid,
								 'text' => $name,
								 'kind' => 'folder',
								 'parent' => is_null($parent) ? 0 : $parent,
								 'img_n' => $img_n,
								 'img_o' => $img_o
								);
			$menu = array_merge($menu, getWebTree($nextid));
		}
	}

	// Files:
	$sql = "SELECT d.id AS id, name, folder_id, version, created_by FROM {$DOCSprefs['table']} d WHERE kind = 'file' AND is_last_version = 1 AND folder_id is null AND public = 1 ORDER BY name";
	if ($curid != '')
			$sql = str_replace('is null', "= $curid", $sql);
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$parent = $db->FetchResult($cur,$x,'folder_id');
		$name   = $db->FetchResult($cur,$x,'name');
		$ext = strtolower(substr($name, strrpos($name, '.')+1));
		$icon = in_array($ext, $knownExtImgs) ? $ext.'.gif' : '';
		$menu[] = array('id' => $db->FetchResult($cur,$x,'id'),
							 'text' => $db->FetchResult($cur,$x,'name'),
							 'kind' => 'file',
							 'version' => 'v'.sprintf('%01.1f', $db->FetchResult($cur,$x,'version')),
							 'parent' => is_null($parent) ? 0 : $parent,
							 'img_n' => $icon,
							 'img_o' => $icon
							);
	}

	return $menu;
}

function diskSpace($what = 'free') {
	global $DOCSprefs;

	$used = (int)trim(shell_exec("du -b \"{$DOCSprefs['saveDir']}\""))/1024/1024;
	$free = $DOCSprefs['diskSpace'] - $used;

	return $free;
}

?>
