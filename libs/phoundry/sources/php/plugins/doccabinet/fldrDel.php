<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Make sure folder is empty:
	$folder_id = (int)$_GET['node_id'];
	$sql = "SELECT COUNT(*) FROM {$DOCSprefs['table']} WHERE folder_id = {$folder_id}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if ($db->FetchResult($cur,0,0) > 0) {
		// There are still files/folders in this folder!
		header('Location: index.php?' . QS(0,'node_id&_msg=Deze+map+is+niet+leeg%21'));
		exit();
	}
	
	$sql = "DELETE FROM {$DOCSprefs['table']} WHERE kind = 'folder' AND id = {$folder_id}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	header('Location: index.php?' . QS(0,'node_id'));
	exit();
?>
