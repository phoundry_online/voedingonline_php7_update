Installatie documentenkabinet
=============================

- Database acties:

CREATE TABLE `docs_file` (
`id` int(11) NOT NULL auto_increment, 
`kind` enum('file','folder') NOT NULL default 'file', 
`folder_id` int(11) default NULL, 
`parent_id` int(11) default NULL, 
`name` varchar(80) NOT NULL default '', 
`description` text,
`contents` text, 
`size` bigint(20) unsigned default NULL, 
`version` float NOT NULL default '0', 
`is_last_version` tinyint(4) NOT NULL default '0', 
`created_by` int(11) NOT NULL default '0', 
`created_date` datetime NOT NULL default '0000-00-00 00:00:00', 
`modified_by` int(11) NOT NULL default '0', 
`modified_date` datetime NOT NULL default '0000-00-00 00:00:00', 
`public` tinyint NOT NULL default '0',
`history` text,
PRIMARY KEY (`id`), FULLTEXT KEY `ft_index` (`description`,`contents`) 
);

INSERT INTO `docs_file` VALUES (1,'folder',NULL,NULL,'#Trash','Trash can',NULL,0,1,1,0,NOW(),0,NOW(),0,NULL);
INSERT INTO `docs_file` VALUES (2,'folder',NULL,NULL,'#Inbox','Inbox',NULL,0,1,1,0,NOW(),0,NOW(),0,NULL);

CREATE TABLE `docs_file_rights` (
`id` int(11) NOT NULL auto_increment,
`file_id` int(11) NOT NULL default '0',
`group_id` int(11) NOT NULL default '0',
`rights` varchar(40) NOT NULL default '',
PRIMARY KEY (`id`) 
);

Wijzig nu de settings in de DOCSprefs.php file.

- Phoundry -> Admin -> Plugins -> Add
Description: Documentenkabinet
Show in menu: Phoundry
Start URL: custom/docs/
Global rights applicable? No
Detailed rights applicable? No

- "Link to doc"-button in WYSIWYG editors:
Phoundry -> Columns -> Choose table -> Edit Column</script>

'extensions' => array(
   array('jsfn'=>'docLink', 'icon'=>'../custom/Inputtypes/link_doc.gif', 'alt'=>'Link naar document', 'selection'=>true)
)

Copy the following files to phoundry/custom/Inputtypes from the docs/software
directory:
	ITwysiwyg.js.php
	link_doc.gif
	link_doc.php

File-size upload opmerking:
Ik zie posts van mensen die upload_max_filesize op 600 meg hebben staan, dus
erg grote files kun je wel uploaden.
Ze merken wel op dat je je post_max_size dan ook moet verhogen naar bv 601M. 
max_execution_time en max_input_time zul je ook zwaar moeten ophogen.
