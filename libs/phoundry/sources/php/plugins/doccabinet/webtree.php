<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();

	checkAccess($TID);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Phoundry</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript" src="js/dtree.js"></script>
</head>
<body  onload="Tree.openAll()">
<input type="hidden" name="node_id" />
<fieldset>
<script type="text/javascript">
//<![CDATA[

var imgpath = '<?= $PHprefs['url'] ?>/core/pics/tree';
var Tree = new dTree('Tree');
Tree.config.inOrder = true;
Tree.add(0,-1,'<b>root</b>','#','','','root','','','',1);
<?php
	$tree = getWebTree();
	foreach($tree as $item) {
		if ($item['kind'] == 'folder') {
			$isOpen = ($item['text'] == 'Trash' || $item['text'] == 'Inbox') ? 0 : 1;

			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . "', '#', \"\", \"\", '', '', '{$item['img_n']}', '{$item['img_o']}', $isOpen);\n";
		}
		else {
			if ($DOCSprefs['trashFolder'] == $item['parent']) {
				$item['rights'].= 't';
			}
			$leftClick = "alert('Ja dit moet nog!')";
			// , \"fileSelectL(event,{$item['id']})\"
			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . "', '#', \"{$leftClick}\", \"\", '" . escDBquote($item['text']) . " ({$item['version']})', '', '{$item['img_n']}', '{$item['img_o']}', 1);\n";
		}
	}
?>

_D.write(Tree);

//]]>
</script>
</fieldset>

<p align="right">
<button type="button" onclick="parent.killPopup()"><?= word(82) ?></button>
</p>

</form>
</body>
</html>
