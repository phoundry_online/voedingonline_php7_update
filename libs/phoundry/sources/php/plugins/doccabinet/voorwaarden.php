<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();

	checkAccess($TID);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Search</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
</head>
<body>
<fieldset>
<legend><b>Voorwaarden</b></legend>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec ultricies, libero in ultrices lacinia, wisi eros tincidunt est, vel rhoncus ante diam et sem. Duis metus. Duis sed augue in tortor dictum pulvinar. Morbi eget enim. Nam sit amet metus. Fusce cursus, neque sit amet ullamcorper blandit, dui magna molestie justo, non faucibus mauris orci sit amet odio. Nullam nisl dolor, placerat vel, ultrices vel, egestas in, est. Donec mollis justo a quam. Nunc ornare wisi sit amet quam. Duis eget erat. Duis tempor nulla. Nullam sem lectus, euismod at, sodales et, vestibulum at, leo. Duis venenatis quam a arcu.</p>

<p>Cras sed urna. Sed vel metus. Nullam ac erat sit amet justo dignissim tristique. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Cras accumsan tortor in odio. Suspendisse tempor, justo at tempus hendrerit, arcu tellus tristique libero, eget varius elit dui et wisi. Quisque quam. Curabitur eleifend pulvinar nunc. Duis aliquam vehicula erat. Cras tempor dolor tincidunt turpis. In eleifend leo. Donec libero. Curabitur mauris. Fusce et nibh vel ligula mollis feugiat. Aliquam odio turpis, facilisis a, sollicitudin et, malesuada at, quam.</p>

<p>Nullam gravida enim nec dolor. Cras ut erat quis ipsum porta suscipit. Quisque dolor pede, tristique in, tincidunt at, mattis sit amet, nunc. Aenean tempor massa vitae lectus. Suspendisse placerat consequat nibh. In eros. Donec ante enim, condimentum non, lacinia nec, sollicitudin nec, massa. Integer scelerisque, mauris laoreet lacinia posuere, enim mi mattis orci, tempor vulputate tellus leo vehicula turpis. Integer ut lectus. Nulla arcu ipsum, dictum nec, rutrum vel, vestibulum ut, lectus. Donec diam purus, commodo ut, lobortis eu, sagittis ut, mi.</p>

<p>Nulla malesuada, eros ac dictum ultrices, metus felis interdum nibh, sit amet adipiscing metus velit quis ipsum. Cras imperdiet. Proin erat erat, iaculis at, commodo quis, lacinia eu, libero. Integer eros sapien, fermentum pellentesque, adipiscing eget, ultricies eu, metus. Quisque feugiat dignissim elit. Nam wisi mauris, nonummy nec, pharetra auctor, luctus nec, ligula. Integer dui turpis, eleifend in, lobortis eu, commodo ut, nunc. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque viverra semper risus. Nullam tincidunt. Maecenas placerat, metus quis varius pharetra, nibh tellus iaculis risus, sed tincidunt lorem mi eu sem. Morbi accumsan, dolor eget viverra condimentum, ante augue nonummy orci, eget molestie massa ligula eget risus. Suspendisse potenti. Etiam nonummy.</p>

<p>Quisque suscipit sem quis quam. Donec enim. Vivamus fermentum quam in libero. Donec feugiat nunc ut nibh. Fusce id mi et elit convallis fermentum. Donec nisl. Pellentesque auctor imperdiet eros. Aliquam faucibus massa nec nibh. Duis dignissim. Aliquam fermentum euismod purus. Maecenas elementum cursus tortor. Praesent ultrices velit vel risus. Etiam ut purus ac dui lacinia accumsan. Cras tincidunt. Quisque egestas, augue ut bibendum dictum, neque sem tristique dolor, sed mattis ipsum felis nec lectus. Quisque et sem. In sapien. Donec eget neque.</p>
</fieldset>

<p align="right">
<input type="button" value="<?= word(82) ?>" onclick="parent.killPopup()" />
</p>
</body>
</html>
