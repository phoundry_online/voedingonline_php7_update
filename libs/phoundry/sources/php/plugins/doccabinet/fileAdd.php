<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$free = diskSpace('free');

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		// Save file:
		if ($_FILES['file']['error'] != 0) {
			header('Location: ' . $_SERVER['PHP_SELF'] . '?' . QS(0, '_msg=Upload+mislukt%21'));
			exit();
		}

		if ($_FILES['file']['size'] / 1024 / 1024 > $free) {
			header('Location: ' . $_SERVER['PHP_SELF'] . '?' . QS(0, '_msg=Upload+mislukt%2C+er+is+geen+schijfruimte+meer+vrij%21'));
			exit();
		}

		$extension = strtolower(substr($_FILES['file']['name'], strrpos($_FILES['file']['name'], '.')+1));
		$contents = grabContents($extension);

		$folder_id = (int)$_GET['node_id']; if ($folder_id == 0) $folder_id = 'NULL';
		$sql = "INSERT INTO {$DOCSprefs['table']} VALUES (NULL, 'file', {$folder_id}, NULL, '" . escDBquote($_FILES['file']['name']) . "', '" . escDBquote($_POST['description']) . "', '{$contents}', " . $_FILES['file']['size'] . ", " . $_POST['version'] . ", 1, {$PHSes->userId}, NOW(), {$PHSes->userId}, NOW(), " . (isset($_POST['public']) ? '1' : '0') . ", '')";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$file_id = $db->getLastInsertId($DOCSprefs['table'], 'id');

		// Possible rights (Read, Write, Delete, Move)
		$rights = array ('r', 'w', 'd', 'm');

		// Fetch groups with access rights to docs
		$groups = getGroups();
		foreach ($groups as $id => $name) {
			$file_rights = "";

			// Create string with rights
			foreach ($rights as $right) {
				$file_rights.= isset ($_POST['g' . $id . '_' . $right]) ? $right : '';
			}

			// Apply rights to user groups
			$sql = "INSERT INTO {$DOCSprefs['tableRights']} VALUES (NULL, {$file_id}, {$id}, '{$file_rights}')";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}
		
		$file_path = $DOCSprefs['saveDir'] . '/' . $file_id;

		
		if (!in_array($extension, $DOCSprefs['nozipExts'])) {
			$outFile = fopen('compress.zlib://' . $file_path, 'wb')
				or trigger_error('Can\'t open ZIP file for writing!', E_USER_ERROR);
			$inFile  = @fopen($_FILES['file']['tmp_name'], 'rb')
				or trigger_error('Can\'t open uploaded file fro reading!', E_USER_ERROR);
			if ($inFile !== false && $outFile !== false) {
				while(!feof($inFile)) {
					fwrite($outFile, fread($inFile, 4096));
				}
				fclose($outFile);
				fclose($inFile);
			}
		}
		else {
			move_uploaded_file($_FILES['file']['tmp_name'], $file_path);
		}
		chmod($file_path, 0644);

		print '<script type="text/javascript">' . "\n";
		print "//<![CDATA[\n";
		print "parent.location.reload();\n";
		print "//]]>\n";
		print "</script>\n";
		exit();
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Upload bestand</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript">
//<![CDATA[

function init() {
	<?php if (isset($_GET['_msg'])) { ?>
	alert('<?= escSquote($_GET['_msg']) ?>');
	<?php } ?>
}

//]]>
</script>
</head>
<body onload="init()">
<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= QS(1) ?>" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this)">
<?php
do {
	if ($free <= 0) {
		print '<fieldset><legend><strong class="req">Fout</strong></legend>Er is geen vrije schijfruimte meer beschikbaar!</fieldset>';
		break;
	}
?>

<fieldset><legend><b class="req">Map</b></legend>
<?= getPath((int)$_GET['node_id']); ?>
</fieldset>

<fieldset><legend><b class="req">Bestand</b></legend>
Maximale bestandsgrootte: 
<?php
	$ini_size = str_replace('M', 'MB', ini_get('upload_max_filesize')); 
	print number_format(min($ini_size, $free),2) . 'MB';
?><br />
<input type="file" name="file" size="42" />
<?php 
	if ($free < $DOCSprefs['diskFullWarning']) {
		print '<br /><strong class="req">Let op!</strong> Er is nog maar ' . number_format($free,2) . 'MB van de ' . $DOCSprefs['diskSpace'] . 'MB beschikbaar.';
	}
?>
</fieldset>

<fieldset><legend><b class="req">Versie</b></legend>
<input type="text" name="version" size="10" maxlength="10" value="1.0" alt="1|float" />
</fieldset>

<fieldset><legend><b>Omschrijving</b></legend>
<textarea name="description" cols="60" rows="10"></textarea>
</fieldset>

<fieldset><legend><b>Publiek toegankelijk</b></legend>
<input type="checkbox" name="public" value="1" /> Is het bestand publiek toegankelijk?
</fieldset>

<fieldset><legend><b>Rechten</b></legend>
<table class="sort-table" cellspacing="0">
<tr>
	<th>Rol</th>
	<th>Lees</th>
	<th>Schrijf</th>
	<th>Verwijder</th>
	<th>Verplaats</th>
</tr>
<?php
	// Rechten van map waar file in wordt geplaatst overnemen
	$sql = "SELECT g.id AS id, g.name AS name, r.rights AS rights FROM phoundry_group g, phoundry_group_table t LEFT JOIN {$DOCSprefs['tableRights']} r ON r.group_id = t.group_id AND r.file_id = {$_GET['node_id']} WHERE g.id = t.group_id AND t.table_id = " . escDBquote($_GET['TID']) . " ORDER BY name";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$x = 0;
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$rights = $db->FetchResult($cur,$x,'rights');
		$id = $db->FetchResult($cur,$x,'id');

		print '<tr class="' . ($x % 2 > 0 ? 'odd' : 'even') . '">';
		print '<td>' . $db->FetchResult($cur,$x,'name') . '</td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_r" ' . (strpos($rights, 'r') !== false ? 'checked="checked"' : '') . ' /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_w" ' . (strpos($rights, 'w') !== false ? 'checked="checked"' : '') . ' /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_d" ' . (strpos($rights, 'd') !== false ? 'checked="checked"' : '') . ' /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_m" ' . (strpos($rights, 'm') !== false ? 'checked="checked"' : '') . ' /></td>';
		print "</tr>\n";
	}
?>
</table>
</fieldset>

<?php
// End of do {}
} while(0); 
?>

<p align="right">
<?= ($free > 0 ? '<input type="submit" value="' . word(82) . '" />' : '') ?>
<input type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</p>
</form>
</body>
</html>
