<?php

$DOCSprefs = array(
'table'		=> 'docs_file',
'tableRights'	=> 'docs_file_rights',

// Email address that may be used for uploading files
'email'			=> 'doccabinet@office.webpower.nl',
// Allow users to mail files?
'allowMail'		=> true,
// Where to save the documents:
'saveDir'		=>	realpath('/project/phoundry.nl/doccabinet'),
// Total diskspace than can be used in 'saveDir' (in MB):
'diskSpace'	=>	1,
// How many days till an item is removed from Trash?
'trashAge' => 14,
// Which files to show inline:
'inlineExts'	=> array('gif','jpg','jpeg','png'),
//'inlineExts'	=> array(),
// Which extensions not to ZIP:
'nozipExts'		=> array('zip','jpg','jpeg'),
// ID's of special folders
'trashFolder'	=> 1,
'inboxFolder'	=> 2,
// Max length of the content column
'contentBlock' => 64000,
// Shell commands for parsing specified files
'parsers' => array(
	'doc' => '/usr/local/bin/catdoc -w #1',
	'ppt' => '/usr/local/bin/catppt #1',
	'pdf' => '/usr/local/bin/pdftotext #1 -',
	'html' => '/usr/local/scripts/strip_tags.php < #1',
	'htm' => '/usr/local/scripts/strip_tags.php < #1',
	'txt' => '/usr/bin/cat #1',
	'csv' => '/usr/bin/cat #1',
	'xls' => '/usr/local/bin/xls2csv -q0 -c" " #1'
),
// Display warning when uploading a file and free disk space is lower than following number (in MB)
'diskFullWarning' => 3
);

require 'docs.inc.php';

?>
