<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		include '../../PREFS.php';
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once '../doccabinet/DOCSprefs.php';
	checkAccess();
?>
<html>
<head>
<title>Link document</title>
<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/core/Inputtypes/wysiwyg/dialog.css.php?bg=buttonface" type="text/css" />
<link rel="stylesheet" href="../doccabinet/css/dtree.css" type="text/css" />
<script type="text/javascript" src="../doccabinet/js/dtree.js"></script>
<script type="text/javascript">
//<![CDATA[

var _W = window, _D = document;

function setFile(node_id, Z) {
	var url = '<?= $PHprefs['url'] ?>/custom/doccabinet/fileDownload.php?node_id='+node_id+'&Z='+escape(Z);
	_W.returnValue = url;
	_W.close();
}

//]]>
</script>
</head>
<body>
<fieldset>
<legend><b>Link document</b></legend>

<script type="text/javascript">
//<![CDATA[

var imgpath = '<?= $PHprefs['url'] ?>/core/pics/tree';
var Tree = new dTree('Tree');
Tree.config.inOrder = true;
Tree.config.indentClick = false;
Tree.add(0,-1,'<b>root</b>','#','','','','root','','','',1);


<?php
	$tree = getWebTree();
	foreach($tree as $item) {
		if ($item['kind'] == 'folder') {
			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . "', '#', '', '', '', '', '', '{$item['img_n']}', '{$item['img_o']}', 1);\n";
		}
		else {
			if ($DOCSprefs['trashFolder'] == $item['parent']) {
				$item['rights'].= 't';
			}
			$leftClick = "setFile({$item['id']},'" . crc32('WP' . $item['id']) . "')";
			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . "', '#', \"{$leftClick}\", '', '', '" . escDBquote($item['text']) . " ({$item['version']})', '', '{$item['img_n']}', '{$item['img_o']}', 1);\n";
		}
	}
?>

_D.write(Tree);

//]]>
</script>
</fieldset>

<p align="right">
	<input type="button" value="Annuleer" onclick="_W.close()" />
</p>
</form>
</body>
</html>
