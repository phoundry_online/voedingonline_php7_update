<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();

	checkAccess($TID);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Search</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript">
//<![CDATA[

function submitForm(F) {
	var Fparent = parent._D.forms[0];
	if (F['PHsrch'].value == '')
	{
		alert('Voer een zoekterm in!');
		F['PHsrch'].focus();
		return false;
	}

	if (!F['PHinFilename'].checked && !F['PHinDescr'].checked)
	{
		alert('Vink minstens 1 onderdeel waar in gezocht moet worden aan!');
		return false;
	}

	if (F['PHalteredSince'].value != '') {
		if (!F['PHalteredSince'].value.match(/^\d{2}-\d{2}-\d{4}$/)) {
			alert('Onjuiste datum ingevoerd!');
			F['PHalteredSince'].focus();
			return false;
		}

		var dTmp = F['PHalteredSince'].value.split('-');
		if (!checkDTdate(dTmp[0],dTmp[1],dTmp[2])) {
			alert('Onjuiste datum ingevoerd!');
			F['PHalteredSince'].focus();
			return false;
		}
	}
	Fparent['PHsrch'].value = F['PHsrch'].value;
	Fparent['PHext'].value = F['PHext'].value;
	Fparent['PHinFilename'].value = F['PHinFilename'].checked ? F['PHinFilename'].value : 0;
	Fparent['PHinDescr'].value = F['PHinDescr'].checked ? F['PHinDescr'].value : 0;
	Fparent['PHauth'].value = F['PHauth'].value;
	Fparent['PHalteredSince'].value = F['PHalteredSince'].value;
	Fparent.submit();
	return false;
}

function init() {
	 var Fself = _D.forms[0], Fparent = parent._D.forms[0];
	
	 Fself['PHsrch'].value = Fparent['PHsrch'].value;
	 Fself['PHext'].value = Fparent['PHext'].value;
	 Fself['PHinFilename'].checked = Fself['PHinFilename'].checked || Fparent['PHinFilename'].value == '1';
	 Fself['PHinDescr'].checked = Fself['PHinDescr'].checked || Fparent['PHinDescr'].value == '1';
	 Fself['PHauth'].value = Fparent['PHauth'].value;
	 Fself['PHalteredSince'].value = Fparent['PHalteredSince'].value;
	 Fself['PHsrch'].focus();
}

function checkDTdate(D,M,Y) {
	M--;
	var tmp = new Date(Y,M,D);
	if ((tmp.getFullYear()==Y) && (tmp.getMonth()==M) && (tmp.getDate()==D))
		// Datum is okee!
		return true;
	else
		return false;
}

//]]>
</script>
</head>
<body onload="init()">
<form action="index.php" method="post" onsubmit="return submitForm(this)">
<div id="zoekDiv">
<fieldset>
<legend><b class="req"><?= word(96) ?></b> (<a href="#" onclick="el=_D.forms[0]['PHsrch'];el.value='';el.focus();return false"><?= word(161) ?></a>, <a href="#" onclick="sE(gE('helpDiv'));hE(gE('zoekDiv'));return false">help</a>)</legend>
<input type="text" name="PHsrch" id="PHsrch" size="20" style="width:100%" />
</fieldset>

<fieldset>
<legend><b>Bestandstype</b></legend>
<select name="PHext">
<option value="*">*</option>
<?php
	$sql = "SELECT DISTINCT SUBSTRING_INDEX(name, '.', -1) AS ext FROM docs_file WHERE kind = 'file' ORDER BY ext";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		print '<option value="' . $db->FetchResult($cur,$x,'ext') . '">*.' . $db->FetchResult($cur,$x,'ext') . '</option>';
	}
?>
</select>
</fieldset>

<fieldset>
<legend><b>Zoeken in</b></legend>
<input type="checkbox" name="PHinFilename" value="1" <?= (!isset ($_GET['PHinFilename']) ? 'checked="checked"' : '') ?> /> Bestandsnaam<br />
<input type="checkbox" name="PHinDescr" value="1" <?= (!isset ($_GET['PHinDescr']) ? 'checked="checked"' : '') ?> /> Omschrijving en inhoud<br />
</fieldset>

<fieldset>
<legend><b>Auteur</b></legend>
<input type="text" name="PHauth" id="PHauth" size="20" style="width:100%" />
</fieldset>

<fieldset>
<legend><b>Gewijzigd sinds</b></legend>
<input type="text" name="PHalteredSince" size="10" maxlength="10" /> (DD-MM-JJJJ)
</fieldset>
<p align="right">
<input type="submit" value="<?= word(10) ?>" />
<input type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</p>
</div>
</form>

<div id="helpDiv" style="background:#fff;position:absolute;top:6px;left:6px;width:312px;visibility:hidden;padding-bottom:5px">
<fieldset><legend><b>Zoekhulp</b> (<a href="#" onclick="sE(gE('zoekDiv'));hE(gE('helpDiv'));return false">sluit</a>)</legend>
<p>
Binnen de boomstructuur kunt u het aantal getoonde bestanden beperken, door een zoekterm op te geven.
</p>
<h2>Zoeken op bestandsnaam</h2>
<p>
U kunt op bestanden zoeken door een bestandsnaam uit de omschrijving in te voeren in het zoek-vel en het <em>Bestandsnaam</em> vakje aan te vinken.
</p>
<h2>Zoeken op omschrijving en inhoud</h2>
<p>
U kunt ook op bestanden zoeken door een woord uit de omschrijving in te voeren in het zoek-veld en het <em>Omschrijving en inhoud</em> vakje aan te vinken. Bij enkele bestandstypen, zoals .doc, .ppt, .txt, .pdf en .html wordt ook de inhoud van het bestand doorzocht. Standaard zal er gezocht worden naar het exacte woord dat is ingevoerd, niet op deelstring. 

<?php
	$notForFilename = '<strong class="req">*</strong>';
?>

<table cellspacing="0" cellpadding="3">
<tr>
	<th>zoekstring</th>
	<th>geeft terug</th>
</tr>
<tr valign="top">
	<td><strong>computer</strong></td>
	<td>Waar het woord <em>computer</em> in de omschrijving of content voorkomt.</td>
</tr>
<tr valign="top">
	<td><strong>computer*</strong></td>
	<td>Waar woorden beginnend met <em>computer</em> in voorkomen, zoals <em>computerapparatuur</em> en <em>computeronderdelen</em>.</td>
</tr>
<tr valign="top">
	<td><strong>+computer*<br/>-printer</strong></td>
	<td>Waar woorden beginnend met <em>computer</em> in voorkomen, maar niet het woord <em>printer</em> in voorkomt. <?= $notForFilename ?></td>
</tr>
<tr valign="top">
	<td><strong>+computer +printer</strong></td>
	<td>Waar zowel het woord <em>computer</em> als het woord <em>printer</em> in voorkomt. <?= $notForFilename ?></td>
</tr>
<tr valign="top">
	<td><strong>"Deze computer heeft geen printer"</strong></td>
	<td>Waar de exacte zin <em>Deze computer heeft geen printer</em> in voorkomt. <?= $notForFilename ?></td>
</tr>
</table>
</p>

<p><?= $notForFilename ?> Dit geldt niet voor het zoeken op bestandsnamen.</p>
</div>

</body>
</html>
