<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$df = str_replace(array('DD','MM','YYYY'), array('d','m','Y'), $g_dateFormat);


	/*
	 * Skip access check, while this script can be called from the
	 * website (no Phoundry Session available) as well.
	 *
	 
	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	*/

	if (crc32('WP' . $_GET['node_id']) != $_GET['Z']) {
		die();
	}

	$type = array('b', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

	$kind = isset($_GET['f']) ? 'folder' : 'file';

	// Fetch file info:
	$sql = "SELECT f.id, f.name, f.description, f.version, f.size, UNIX_TIMESTAMP(f.created_date) AS crea_date, UNIX_TIMESTAMP(f.modified_date) AS mod_date, u.name as username FROM {$DOCSprefs['table']} f, phoundry_user u WHERE f.kind = '{$kind}' AND f.id = " . (int)$_GET['node_id'] . " AND f.created_by = u.id";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($cur)) {
		$filesize = $db->FetchResult($cur,0,'size');
		for ($i = 0; $filesize > 1024; $i++) {
			$filesize /= 1024;
		}

		print 'Naam: ' . $db->FetchResult($cur,0,'name') . '<br />';
		
		if ($kind == 'file') {
			print 'Versie: v' . sprintf('%01.1f', $db->FetchResult($cur,0,'version')) . '<br />';
		}

		print 'Auteur: ' . $db->FetchResult($cur,0,'username') . '<br />';

		if ($kind == 'file') {
			print 'Grootte: ' . round ($filesize, 2) . $type[$i] . '<br />';
		}

		print 'Aangemaakt: ' . date($df, $db->FetchResult($cur,0,'crea_date')) . '<br />';
		print 'Laatst gewijzigd: ' . date($df, $db->FetchResult($cur,0,'mod_date')) . '<br />';
		print 'Omschrijving: ' . PH::htmlspecialchars($db->FetchResult($cur,0,'description')) . '<br />';
	}
	else {
		die();
	}
?>
