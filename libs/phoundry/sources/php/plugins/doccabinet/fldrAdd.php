<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		// Save folder:

		$folder_id = (int)$_GET['node_id']; if ($folder_id == 0) $folder_id = 'NULL';
		$sql = "INSERT INTO {$DOCSprefs['table']} (kind, folder_id, name, description, is_last_version, created_by, created_date, modified_by, modified_date, public) VALUES ('folder', {$folder_id}, '" . escDBquote($_POST['fldr']) . "', '" . escDBquote($_POST['description']) . "', 1, {$PHSes->userId}, NOW(), {$PHSes->userId}, NOW(), " . (isset($_POST['public']) ? '1' : '0') . ")";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$fldr_id = $db->getLastInsertId($DOCSprefs['table'], 'id');

		// Possible rights (Read, Write, Delete, Move)
		$rights = array ('r', 'w', 'd', 'm');

		// Fetch groups with access rights to docs
		$groups = getGroups();
		foreach ($groups as $id => $name) {
			$file_rights = "";

			// Create string with rights
			foreach ($rights as $right) {
				$file_rights.= isset ($_POST['g' . $id . '_' . $right]) ? $right : '';
			}

			// Apply rights to user groups
			$sql = "INSERT INTO {$DOCSprefs['tableRights']} VALUES (NULL, {$fldr_id}, {$id}, '{$file_rights}')";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}

		print '<script type="text/javascript">' . "\n";
		print "//<![CDATA[\n";
		print "parent.location.reload();\n";
		print "//]]>\n";
		print "</script>\n";
		exit();
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Nieuwe map</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript">
//<![CDATA[

function init() {
	_D.forms[0]['fldr'].focus();
	<?php if (isset($_GET['_msg'])) { ?>
	alert('<?= escSquote($_GET['_msg']) ?>');
	<?php } ?>
}

//]]>
</script>
</head>
<body onload="init()">
<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">

<fieldset><legend><b class="req">Map</b></legend>
<?= getPath((int)$_GET['node_id']); ?>
</fieldset>

<fieldset><legend><b class="req">Naam map</b></legend>
<input type="text" name="fldr" size="60" maxlength="80" alt="1|string|80" />
</fieldset>

<fieldset><legend><b>Omschrijving</b></legend>
<textarea name="description" cols="60" rows="10"></textarea>
</fieldset>

<fieldset><legend><b>Publiek toegankelijk</b></legend>
<input type="checkbox" name="public" value="1" /> Is de map publiek toegankelijk?
</fieldset>

<fieldset><legend><b>Rechten</b></legend>
<table class="sort-table" cellspacing="0">
<tr>
	<th>Rol</th>
	<th>Lees</th>
	<th>Schrijf</th>
	<th>Verwijder</th>
	<th>Verplaats</th>
</tr>
<?php
	$groups = getGroups();
	$x = 0;
	foreach($groups as $id=>$name) {
		print '<tr class="' . (($x++ & 1) ? 'odd' : 'even') . '">';
		print '<td>' . $name . '</td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_r" checked="checked" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_w" checked="checked" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_d" checked="checked" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_m" checked="checked" /></td>';
		print "</tr>\n";
	}
?>
</table>
</fieldset>

<p align="right">
<input type="submit" value="<?= word(82) ?>" />
<input type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</p>
</form>
</body>
</html>
