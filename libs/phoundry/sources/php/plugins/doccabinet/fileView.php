<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Fetch file info:
	$sql = "SELECT id, folder_id, parent_id, size, name, description, version, created_by, unix_timestamp(created_date) as created_date, modified_by, unix_timestamp(modified_date) as modified_date, public, history FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$db->FetchResultAssoc($cur,$row,0);

	// true if current user owns file
	$isOwner = $row['created_by'] == $PHSes->userId;

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Bekijk bestand</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript">
//<![CDATA[
var hisOpen = false;

function history() {
	var hLink = gE('historyLink');
	var hBody = gE('history');

	hisOpen = !hisOpen;
	
	hLink.innerHTML = hisOpen ? "&laquo; Sluiten" : "Historie &raquo;";
	hBody.style.display = hisOpen ? "block" : "none";

}
//]]>
</script>
</head>
<body>

<fieldset><legend><b>Map</b></legend>
<?php
	$path = getPath((int)$row['folder_id']);
	if ($row['folder_id'] == $DOCSprefs['trashFolder'] || $row['folder_id'] == $DOCSprefs['inboxFolder']) {
		$path = '/' . substr ($path, 2);
	}

	print $path;
	$downloadURL = 'fileDownload.php?' . QS(1, 'Z=' . crc32('WP' . $_GET['node_id']));
?>
</fieldset>

<?php
do { // BEGIN do {} (closes at end of file)
	// Check whether user has permission to view this file
	if (!$isOwner && $row['folder_id'] != $DOCSprefs['inboxFolder']) {
		$sql = "SELECT rights FROM {$DOCSprefs['tableRights']} WHERE group_id = '{$PHSes->groupId}' AND file_id = '" . escDBquote($_GET['node_id']) . "'";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur) || strpos($db->FetchResult($cur,0,'rights'),'r') === false) {
			print '<fieldset><legend><b>Geen toegang</b></legend>';
			print 'U hebt geen toegang tot dit bestand';
			print '</fieldset>';
			break;
		}
	}
?>

<fieldset><legend><b>Bestand</b></legend>
<a href="<?= $downloadURL ?>" title="Download"><img src="pics/download.png" width="16" height="16" alt="Download" border="0" /> <?= PH::htmlspecialchars($row['name']) ?></a>
</fieldset>

<!--
<fieldset><legend><b>Download URL</b></legend>
http://<?= $_SERVER['SERVER_NAME'] ?><?= dirname($_SERVER['PHP_SELF']) ?>/<?= $downloadURL ?>
</fieldset>
-->

<fieldset><legend><b>Grootte</b></legend>
<?php
	$type = array('bytes', 'kilobytes', 'megabytes', 'gigabytes', 'terabytes', 'petabytes', 'exabytes', 'zettabytes', 'yottabytes');
	$filesize = $row['size'];
	for ($i = 0; $filesize > 1024; $i++) {
		$filesize /= 1024;
	}
	print round ($filesize, 2)." $type[$i]";
?>
</fieldset>

<fieldset><legend><b>Versie</b></legend>
<?php
	// Determine available verions:
	if (is_null($row['parent_id'])) {
		$sql = "SELECT id, name, version FROM docs_file WHERE (parent_id = {$row['id']} OR id = {$row['id']}) ORDER BY version DESC";
	}
	else {
		$sql = "SELECT id, name, version FROM docs_file WHERE (parent_id = {$row['parent_id']} OR id = {$row['parent_id']}) ORDER BY version DESC";
	}
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$nrRows = $db->NumberOfRows($cur);

	for ($x = 0; $x < $nrRows; $x++) {
		$file_id = $db->FetchResult($cur,$x,'id');
		$version = 'v'.sprintf('%01.1f', $db->FetchResult($cur,$x,'version'));
		if ($file_id == $_GET['node_id']) {
			// Skip HREF:
			print '<b><u>' . $version . '</u></b>';
		}
		else {
			print '<a href="' . $_SERVER['PHP_SELF'] ?>?<?= QS(1,'node_id=' . $db->FetchResult($cur,$x,'id')) . '">' . $version . '</a>';
		}
		if ($x < $nrRows -1) print ', ';
	}
?>
</fieldset>

<fieldset><legend><b>Omschrijving</b></legend>
<?= PH::htmlspecialchars($row['description']) ?>&nbsp;
</fieldset>

<fieldset><legend><b>Laatste wijziging</b></legend>
<?= date('d-m-Y H:i', $row['modified_date']) ?>, door <?= userid2username($row['modified_by']) ?>
</fieldset>

<fieldset><legend><b>Creatie</b></legend>
<?= date('d-m-Y H:i', $row['created_date']) ?>, door <?= userid2username($row['created_by']) ?>
</fieldset>

<fieldset><legend><b>Publiek toegankelijk</b></legend>
Dit bestand is <?= $row['public'] ? "" : "niet" ?> publiek toegankelijk.
</fieldset>

<fieldset><legend><b>Rechten</b></legend>
<table class="sort-table" cellspacing="0">
<tr>
	<th>Rol</th>
	<th>Lees</th>
	<th>Schrijf</th>
	<th>Verwijder</th>
	<th>Verplaats</th>
</tr>
<?php
	$sql = "SELECT g.id AS id, g.name AS name, r.rights AS rights FROM phoundry_group g, phoundry_group_table t LEFT JOIN {$DOCSprefs['tableRights']} r ON r.group_id = t.group_id AND r.file_id = {$_GET['node_id']} WHERE g.id = t.group_id AND t.table_id = " . escDBquote($_GET['TID']) . " ORDER BY name";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$x = 0;
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$rights = $db->FetchResult($cur,$x,'rights');
		$id = $db->FetchResult($cur,$x,'id');

		print '<tr class="' . ($x % 2 > 0 ? 'odd' : 'even') . '">';
		print '<td>' . $db->FetchResult($cur,$x,'name') . '</td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_r" ' . (strpos($rights, 'r') !== false ? 'checked="checked"' : '') . ' disabled="disabled" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_w" ' . (strpos($rights, 'w') !== false ? 'checked="checked"' : '') . ' disabled="disabled" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_d" ' . (strpos($rights, 'd') !== false ? 'checked="checked"' : '') . ' disabled="disabled" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_m" ' . (strpos($rights, 'm') !== false ? 'checked="checked"' : '') . ' disabled="disabled" /></td>';
		print "</tr>\n";
	}
?>
</table>
</fieldset>

<br/>
<a name="his"></a>
<strong><a href="#his" onclick="history()" id="historyLink">Historie &raquo;</a></strong>
<fieldset id="history" style="display:none;">
<legend>Historie</legend>
<pre><?=trim($row['history'])?></pre>
</fieldset>

<?php
} while (0); // END do {}
?>

<p align="right">
<button type="button" onclick="parent.killPopup()"><?= word(82) ?></button>
</p>
</body>
</html>
