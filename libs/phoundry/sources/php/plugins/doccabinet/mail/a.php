#!/usr/local/bin/php
<?php

chdir(dirname($argv[0]));

// Required files
require_once realpath('../../../PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once "{$PHprefs['instDir']}/custom/docs/DOCSprefs.php";
require_once "{$PHprefs['instDir']}/custom/docs/mail/mimeDecode.php";

// Read entire message into memory:
$msg = '';
while($chunk = fread(STDIN, 16000)) {
   $msg .= $chunk;
}

// Parameters for mime decoder
$params['include_bodies'] = true;
$params['decode_bodies']  = true;
$params['decode_headers'] = true;
$params['input'] = $msg;

// Decode mime mail
$structure = Mail_mimeDecode::decode($params);

print_r ($structure);

?>
