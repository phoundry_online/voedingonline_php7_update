#!/usr/local/bin/php
<?php

mail('jurriaan@webpower.nl','process.php','process.php');

chdir(dirname($argv[0]));

// Required files
require_once realpath('../../../PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once "{$PHprefs['instDir']}/custom/docs/DOCSprefs.php";

if (!$DOCSprefs['allowMail']) {
	// Exit if mail-upload has been disabled
	exit;
}

require_once "{$PHprefs['instDir']}/custom/docs/mail/mimeDecode.php";

// Read entire message into memory:
$msg = '';
while($chunk = fread(STDIN, 16000)) {
   $msg .= $chunk;
}

mail('jurriaan@webpower.nl','reply',$msg);
exit;

// Parameters for mime decoder
$params['include_bodies'] = true;
$params['decode_bodies']  = true;
$params['decode_headers'] = true;
$params['input'] = $msg;

// Decode mime mail
$structure = Mail_mimeDecode::decode($params);

$saved = array();
$notsaved = array();

$to = $structure->headers['from'];

foreach ($structure->parts as $part) {
	// Diskspace
	$used = (int)trim(shell_exec("du -b " . escapeshellarg($DOCSprefs['saveDir'])))/1024/1024;
	$free = $DOCSprefs['diskSpace'] - $used;
	$perc  = $used/$DOCSprefs['diskSpace']*100;

	// Only save if attachment
	if (isset ($part->disposition) && ($part->disposition == 'inline' || $part->disposition == 'attachment')) {
		// Do not save if not enough space left
		if (strlen($part->body) / 1024 / 1024 > $free) {
			$notsaved[] = $part->d_parameters['filename'];
		} else {
			$extension = strtolower(substr($part->d_parameters['filename'], strrpos($part->d_parameters['filename'], '.')+1));
			$contents = '';
			
			if (isset($DOCSprefs['parsers'][$extension])) {
				$parser = $DOCSprefs['parsers'][$extension];
				if (!empty ($parser)) {
					$contents = grabContents ($extension);
				}
			}

			$sql = "INSERT INTO {$DOCSprefs['table']} VALUES (NULL, 'file', {$DOCSprefs['inboxFolder']}, NULL, '" . escDBquote($part->d_parameters['filename']) . "', '" . escDBquote($to) . "', '{$contents}', " . strlen($part->body) . ", 1.0, 1, 1, NOW(), 1, NOW(), 0, 'Uploaded by " . escDBquote($to) . " on " . date('Y-m-d H:i:s') . ".')";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$file_id = $db->getLastInsertId($DOCSprefs['table'], 'id');

			$file_path = $DOCSprefs['saveDir'] . '/' . $file_id;

			if (!in_array($extension, $DOCSprefs['nozipExts'])) {
				$outFile = fopen('compress.zlib://' . $file_path, 'wb')
					or trigger_error('Can\'t open ZIP file for writing!', E_USER_ERROR);
				if ($outFile !== false) {
					fwrite($outFile, $part->body, strlen ($part->body));
					fclose($outFile);
				}
			} else {
				$fp = fopen ($file_path, 'w')
					or trigger_error('Can\'t open file for writing!', E_USER_ERROR);
				fwrite ($fp, $part->body);
				fclose ($fp);
			}

			$saved[] = $part->d_parameters['filename'];
		}
	}
}

// Reply to sender
$name = strpos ($to, '<') !== false ? substr ($to, 0, strpos ($to, '<') - 1) : $to;
$msg = "Beste {$name},\n";
$msg.= "\n";

if (count($saved) > 0) {
	$msg.= "de volgende bestanden zijn toegevoegd aan het documentencabinet:\n";
	foreach ($saved as $key => $value) {
		$msg.= ($key + 1) . ") " . $value . "\n";
	}
	$msg.= "\n";
}

if (count($notsaved) > 0) {
	$msg.= "de volgende bestanden zijn NIET toegevoegd aan het documentencabinet, omdat er geen ruimte meer beschikbaar was:\n";
	foreach ($notsaved as $key => $value) {
		$msg.= ($key + 1) . ") " . $value . "\n";
	}
	$msg.= "\n";
}

$msg.= "\n";
$msg.= "\n";
$msg.= "Dit is een automatisch gegenereerd bericht van Phoundry.";

mail ($to, "Resultaten E-mail naar Phoundry", $msg);

?>
