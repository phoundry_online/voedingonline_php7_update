<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Fetch file info:
	$sql = "SELECT created_by FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$db->FetchResultAssoc($cur,$row,0);

	// true if current user owns file
	$isOwner = $row['created_by'] == $PHSes->userId;

	if (!$isOwner) {
		$sql = "SELECT rights FROM {$DOCSprefs['tableRights']} WHERE group_id = '{$PHSes->groupId}' AND file_id = '" . escDBquote($_GET['node_id']) . "'";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur) || strpos($db->FetchResult($cur,0,'rights'),'d') === false) {
			header ('Location: index.php?' . QS(0, 'node_id') . '&_msg=' . urlencode ("U hebt geen rechten om dit bestand te verwijderen!"));
			exit();
		}
	}

	// What's the id of the Trash folder?
	$sql = "SELECT id FROM docs_file WHERE kind = 'folder' AND name = '#Trash'";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$trash_id = $db->FetchResult($cur,0,'id');

	$sql = "UPDATE docs_file SET folder_id = {$trash_id}, history = CONCAT(IFNULL(history,''),'\nTrashed by {$PHSes->userName} at " . date('Y-m-d H:i:s') . ".') WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	/*	
	$sql = "SELECT id, folder_id, name, description, version, created_by, unix_timestamp(created_date) as created_date, modified_by, unix_timestamp(modified_date) as modified_date FROM {$DOCSprefs['file']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	
	$file_path = $DOCSprefs['saveDir'] . '/' . $db->FetchResult($cur,0,'id');
	@unlink($file_path);

	$sql = "DELETE FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);\
	*/

	header('Location: index.php?' . QS(0,'node_id'));
	exit();
?>
