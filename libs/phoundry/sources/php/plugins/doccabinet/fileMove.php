<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		// Move file:
		$dest_id = (int)$_POST['dest_id'];
		if ($dest_id == 0) $dest_id = 'NULL';
		$sql = "UPDATE {$DOCSprefs['table']} SET folder_id = {$dest_id}, history = CONCAT(IFNULL(history,''),'\nMoved by {$PHSes->userName} at " . date('Y-m-d H:i:s') . ".') WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		// Update previous versions as well:
		$sql = "SELECT parent_id FROM {$DOCSprefs['table']} WHERE id = " . (int)$_GET['node_id'];
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$parent_id = $db->FetchResult($cur,0,'parent_id');
		if (!is_null($parent_id)) {
			$sql = "UPDATE {$DOCSprefs['table']} SET folder_id = {$dest_id}, history = CONCAT(IFNULL(history,''),'\nMoved by {$PHSes->userName} at " . date('Y-m-d H:i:s') . ".') WHERE kind = 'file' AND parent_id = {$parent_id}";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$sql = "UPDATE {$DOCSprefs['table']} SET folder_id = {$dest_id}, history = CONCAT(IFNULL(history,''),'\nMoved by {$PHSes->userName} at " . date('Y-m-d H:i:s') . ".') WHERE kind = 'file' AND id = {$parent_id}";
			$db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}

		print '<script type="text/javascript">' . "\n";
		print "//<![CDATA[\n";
		print "parent.location.reload();\n";
		print "//]]>\n";
		print "</script>\n";
		exit();
	}

	// Determine folder id:
	$sql = "SELECT folder_id FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
	$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$folder_id = $db->FetchResult($cur,0,'folder_id');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Verplaats bestand</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="css/dtree.css" type="text/css" />
</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript" src="js/dtree.js"></script>
<script type="text/javascript">
//<![CDATA[

function fldrSelect(e, node_id) {
	_D.forms[0]['dest_id'].value = node_id;
}

function init() {
	<?php if (isset($_GET['_msg'])) { ?>
	alert('<?= escSquote($_GET['_msg']) ?>');
	<?php } ?>
}

//]]>
</script>
</head>
<body onload="init()">

<?php

// Fetch file info:
$sql = "SELECT created_by FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
$cur = $db->Query($sql)
	or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
$db->FetchResultAssoc($cur,$row,0);

// true if current user owns file
$isOwner = $row['created_by'] == $PHSes->userId;

do { // BEGIN do {} (closes at end of file)
	if (!$isOwner) {
		$sql = "SELECT rights FROM {$DOCSprefs['tableRights']} WHERE group_id = '{$PHSes->groupId}' AND file_id = '" . escDBquote($_GET['node_id']) . "'";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur) || strpos($db->FetchResult($cur,0,'rights'),'m') === false) {
			print '<fieldset><legend><b>Geen toegang</b></legend>';
			print 'U hebt geen rechten om dit bestand te verplaatsen.';
			print '</fieldset>';
			break;
		}
	}
?>

<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="dest_id" alt="1|integer" />
<fieldset><legend><b class="req">Map</b></legend>
<script type="text/javascript">
//<![CDATA[

var imgpath = './pics/tree';
var Tree = new dTree('Tree');
Tree.config.inOrder = true;

<?php
	if (is_null($folder_id)) {
		print "Tree.add(0,-1,'<b>root</b>','','','','','root','','','',true);\n";
	}
	else {
		print "Tree.add(0,-1,'<b>root</b>','#','fldrSelect(event,0)','','','root','','','',true);\n";
	}

	$tree = getTree(true, '', 'w');
	foreach($tree as $item) {
		if ($item['id'] == $folder_id) {
			// A folder cannot be moved to itself!
			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . "', '', '', '', '', '', '', '{$item['img_n']}', '{$item['img_o']}', 1);\n";
		}
		else {
			print "Tree.add({$item['id']}, {$item['parent']}, '" . escSquote($item['text']) . "', '#', 'fldrSelect(event,{$item['id']})', '', '','', '', '{$item['img_n']}', '{$item['img_o']}', 1);\n";
		}
	}
?>

_D.write(Tree);

//]]>
</script>

</fieldset>


<p align="right">
<input type="submit" value="<?= word(82) ?>" />
<input type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</p>
</form>

<?php
} while (0); // END do {}
?>

</body>
</html>
