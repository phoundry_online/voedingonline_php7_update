<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	$file_id = (int)$_GET['node_id'];

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		// Save file properties:

		$file = $_POST['file_name'] . $_POST['file_ext'];
		$sql = "UPDATE {$DOCSprefs['table']} SET name = '" . escDBquote($file) . "', description = '" . escDBquote($_POST['description']) . "', modified_by = {$PHSes->userId}, modified_date = NOW(), public = " . (int)$_POST['public'] . ", history = CONCAT(IFNULL(history,''),'\nAltered by {$PHSes->userName} at " . date('Y-m-d H:i:s') . ".') WHERE kind = 'file' AND id = {$file_id}";
		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		// Update user rights, insert where necessary :

		// Possible rights (Read, Write, Delete, Move)
		$rights = array ('r', 'w', 'd', 'm');

		// Fetch groups with access rights to docs
		$groups = getGroups();
		foreach ($groups as $id => $name) {
			$file_rights = "";

			// Create string with rights
			foreach ($rights as $right) {
				$file_rights.= isset ($_POST['g' . $id . '_' . $right]) ? $right : '';
			}

			// Apply rights to user groups
			$sql = "SELECT id FROM {$DOCSprefs['tableRights']} WHERE file_id = {$file_id} AND group_id = {$id}";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			
			if ($db->EndOfResult($cur)) { // New rights -> insert
				$sql = "INSERT INTO {$DOCSprefs['tableRights']} VALUES (NULL, {$file_id}, {$id}, '{$file_rights}')";
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			} else { // Exisiting rights -> update
				$sql = "UPDATE {$DOCSprefs['tableRights']} SET rights = '{$file_rights}' WHERE id = " . $db->FetchResult($cur,0,'id');
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			}
		}

		print '<script type="text/javascript">' . "\n";
		print "//<![CDATA[\n";
		print "parent.location.reload();\n";
		print "//]]>\n";
		print "</script>\n";
		exit();
	}

	$sql = "SELECT id, folder_id, created_by, name, description, public FROM {$DOCSprefs['table']} WHERE kind='file' AND id = {$file_id}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$db->FetchResultAssoc($cur,$row,0);

	// true if current user owns file
	$isOwner = $row['created_by'] == $PHSes->userId;

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Eigenschappen bestand</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript">
//<![CDATA[

function init() {
	try
	{
		_D.forms[0]['file_name'].focus();
	}
	catch (e)
	{
	}
	<?php if (isset($_GET['_msg'])) { ?>
	alert('<?= escSquote($_GET['_msg']) ?>');
	<?php } ?>
}

//]]>
</script>
</head>
<body onload="init()">
<?php
do { // BEGIN do {} (closes at end of file)
	// Check whether user has permission to view this file
	if (!$isOwner) {
		$sql = "SELECT rights FROM {$DOCSprefs['tableRights']} WHERE group_id = '{$PHSes->groupId}' AND file_id = {$file_id}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur) || strpos($db->FetchResult($cur,0,'rights'),'r') === false) {
			print '<fieldset><legend><b>Geen toegang</b></legend>';
			print 'U hebt geen toegang tot dit bestand';
			print '</fieldset>';
			break;
		}
	}
?>
<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">

<fieldset><legend><b class="req">Map</b></legend>
<?= getPath($row['folder_id']); ?>
</fieldset>

<fieldset><legend><b class="req">Naam bestand</b></legend>
<?php
	if (preg_match('/(.*)\.(.*)$/', $row['name'], $reg)) {
		$file_name = $reg[1];
		$file_ext  = '.' . $reg[2];
	}
	else {
		$file_name = $row['name'];
		$file_ext  = '';
	}
?>

<input type="text" name="file_name" size="50" maxlength="80" alt="1|string|80" value="<?= PH::htmlspecialchars($file_name) ?>" /><?= $file_ext ?>
<input type="hidden" name="file_ext" value="<?= $file_ext ?>" />
</fieldset>

<fieldset><legend><b>Omschrijving</b></legend>
<textarea name="description" cols="60" rows="10"><?= PH::htmlspecialchars($row['description']) ?></textarea>
</fieldset>

<fieldset><legend><b>Publiek toegankelijk</b></legend>
<input type="checkbox" name="public" value="1" <?= $row['public'] ? 'checked="checked"' : '' ?> /> Is het bestand publiek toegankelijk?
</fieldset>

<fieldset><legend><b>Rechten</b></legend>
<table class="sort-table" cellspacing="0">
<tr>
	<th>Rol</th>
	<th>Lees</th>
	<th>Schrijf</th>
	<th>Verwijder</th>
	<th>Verplaats</th>
</tr>
<?php
	$sql = "SELECT g.id AS id, g.name AS name, r.rights AS rights FROM phoundry_group g, phoundry_group_table t LEFT JOIN {$DOCSprefs['tableRights']} r ON r.group_id = t.group_id AND r.file_id = {$file_id} WHERE g.id = t.group_id AND t.table_id = " . escDBquote($_GET['TID']) . " ORDER BY name";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$x = 0;
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$rights = $db->FetchResult($cur,$x,'rights');
		$id = $db->FetchResult($cur,$x,'id');

		print '<tr class="' . ($x % 2 > 0 ? 'odd' : 'even') . '">';
		print '<td>' . $db->FetchResult($cur,$x,'name') . '</td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_r" ' . (strpos($rights, 'r') !== false ? 'checked="checked"' : '') . ' /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_w" ' . (strpos($rights, 'w') !== false ? 'checked="checked"' : '') . ' /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_d" ' . (strpos($rights, 'd') !== false ? 'checked="checked"' : '') . ' /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_m" ' . (strpos($rights, 'm') !== false ? 'checked="checked"' : '') . ' /></td>';
		print "</tr>\n";
	}
?>
</table>
</fieldset>

<p align="right">
<input type="submit" value="<?= word(82) ?>" />
<input type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</p>
</form>
<?php
} while (0); // END do {}
?>
</body>
</html>
