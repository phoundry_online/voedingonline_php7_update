<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	if ($_GET['node_id'] == 'all') {
		$sql = "SELECT id FROM {$DOCSprefs['table']} WHERE kind = 'file' AND created_by = " . (int)$PHSes->userId . " AND folder_id = " . (int)$DOCSprefs['trashFolder'];
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$file_path = $DOCSprefs['saveDir'] . '/' . $db->FetchResult($cur,$x,'id');
			
			if (is_file ($file_path)) {
				$unlinked = @unlink($file_path);
			} else {
				$unlinked = true;
			}

			if ($unlinked) {
				$sql = "DELETE FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$db->FetchResult($cur,$x,'id');
				$req = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			}
		}

		header('Location: index.php?' . QS(0,'node_id'));
		exit();
	} else {
		// Fetch file info:
		$sql = "SELECT id, created_by FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$db->FetchResultAssoc($cur,$row,0);
		
		// true if current user owns file
		$isOwner = $row['created_by'] == $PHSes->userId;

		if (!$isOwner) {
			header ('Location: index.php?' . QS(0, 'node_id') . '&_msg=' . urlencode ("Dit bestand kan enkel door de eigenaar worden verwijderd!"));
			exit();
		}

		$file_path = $DOCSprefs['saveDir'] . '/' . $db->FetchResult($cur,0,'id');

		if (is_file ($file_path)) {
			$unlinked = @unlink($file_path);
		} else {
			$unlinked = true;
		}

		if ($unlinked) {
			$sql = "DELETE FROM {$DOCSprefs['table']} WHERE kind = 'file' AND id = " . (int)$_GET['node_id'];
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

			header('Location: index.php?' . QS(0,'node_id'));
			exit();
		}

		header('Location: index.php?' . QS(0,'node_id') . '&_msg=Verwijderen+mislukt%21');
		exit();
	}
?>
