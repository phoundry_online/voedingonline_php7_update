<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'DOCSprefs.php';

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID);

	// Fetch folder info:
	$sql = "SELECT id, folder_id, name, description, created_by, unix_timestamp(created_date) as created_date, modified_by, unix_timestamp(modified_date) as modified_date, public FROM {$DOCSprefs['table']} WHERE kind = 'folder' AND id = " . (int)$_GET['node_id'];
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$db->FetchResultAssoc($cur,$row,0);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Bekijk map</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?noFrames"></script>
<script type="text/javascript">
//<![CDATA[

//]]>
</script>
</head>
<body>

<fieldset><legend><b>Map</b></legend>
<?= getPath((int)$row['folder_id']); ?>
</fieldset>

<fieldset><legend><b>Naam map</b></legend>
<?php
	if ($row['id'] == $DOCSprefs['trashFolder'] || $row['id'] == $DOCSprefs['inboxFolder']) {
		print substr ($row['name'], 1);
	} else {
		print PH::htmlspecialchars($row['name']);
	}
?>
</fieldset>

<fieldset><legend><b>Omschrijving</b></legend>
<?= PH::htmlspecialchars($row['description']) ?>&nbsp;
</fieldset>

<fieldset><legend><b>Laatste wijziging</b></legend>
<?= date('d-m-Y H:i', $row['modified_date']) ?>, door <?= userid2username($row['modified_by']) ?>
</fieldset>

<fieldset><legend><b>Creatie</b></legend>
<?= date('d-m-Y H:i', $row['created_date']) ?>, door <?= userid2username($row['created_by']) ?>
</fieldset>

<fieldset><legend><b>Publiek toegankelijk</b></legend>
Deze map is <?= $row['public'] ? "" : "niet" ?> publiek toegankelijk.
</fieldset>

<fieldset><legend><b>Rechten</b></legend>
<table class="sort-table" cellspacing="0">
<tr>
	<th>Rol</th>
	<th>Lees</th>
	<th>Schrijf</th>
	<th>Verwijder</th>
	<th>Verplaats</th>
</tr>
<?php
	$sql = "SELECT g.id AS id, g.name AS name, r.rights AS rights FROM phoundry_group g, phoundry_group_table t LEFT JOIN {$DOCSprefs['tableRights']} r ON r.group_id = t.group_id AND r.file_id = {$_GET['node_id']} WHERE g.id = t.group_id AND t.table_id = " . escDBquote($_GET['TID']) . " ORDER BY name";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$x = 0;
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$rights = $db->FetchResult($cur,$x,'rights');
		$id = $db->FetchResult($cur,$x,'id');

		print '<tr class="' . ($x % 2 > 0 ? 'odd' : 'even') . '">';
		print '<td>' . $db->FetchResult($cur,$x,'name') . '</td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_r" ' . (strpos($rights, 'r') !== false ? 'checked="checked"' : '') . ' disabled="disabled" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_w" ' . (strpos($rights, 'w') !== false ? 'checked="checked"' : '') . ' disabled="disabled" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_d" ' . (strpos($rights, 'd') !== false ? 'checked="checked"' : '') . ' disabled="disabled" /></td>';
		print '<td align="center"><input type="checkbox" name="g' . $id . '_m" ' . (strpos($rights, 'm') !== false ? 'checked="checked"' : '') . ' disabled="disabled" /></td>';
		print "</tr>\n";
	}
?>
</table>
</fieldset>

<p align="right">
<button type="button" onclick="parent.killPopup()"><?= word(82) ?></button>
</p>
</body>
</html>
