<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, '');

	if (!function_exists('curl_init'))
		trigger_error('This PHP-configuration is not build with CURL support (--with-curl). The URL-checker will not work without it.', E_USER_ERROR);

	 // See if there's a lockfile:
	$lockFile = realpath(ini_get('session.save_path')) . '/PHCL' . $_SERVER['HTTP_HOST'];
	if (file_exists($lockFile))
		trigger_error(word(122), E_USER_ERROR);

	// This script only works on Linux/Unix:
	$windows  = stristr(php_uname(), 'windows') !== false;
	if ($windows)
		trigger_error('This tool does not work on Windows servers!', E_USER_ERROR);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Zoek url's</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../../core/csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">
//<![CDATA[

function checkFormExtra(F) {
// Make sure at least one checkbox is checked.
	var el;
	for (var x = 0; x < F.elements.length; x++) {
		el = F.elements[x];
		if (el.type == 'checkbox') {
			if (el.checked)
				return true;
		}
	}
	alert(words[20]);
	return false;
}

//]]>
</script>
</head>
<body class="frames">
<form action="search.php?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">

<div id="headerFrame" class="headerFrame">
<?= getHeader('Zoek url\'s'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b><?= word(32) ?></b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<fieldset>
<legend><b class="req">Zoekterm</b></legend>
<input type="text" name="search" size="40" alt="1|string" />
</fieldset>

<h2>Zoek in...</h2>

<?php
	$tables = array();
	if ($PHSes->isAdmin && $PHSes->groupId == -1)
	$sql = "SELECT pt.id, pt.name, pt.description FROM phoundry_table pt WHERE pt.is_plugin = 0 ORDER BY pt.order_by";
	else
		$sql = "SELECT pt.id, pt.name, pt.description FROM phoundry_table pt, phoundry_group_table pgt WHERE pt.is_plugin = 0 AND pgt.group_id = {$PHSes->groupId} AND pt.id = pgt.table_id ORDER BY pt.order_by";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$tables[$db->FetchResult($cur,$x,'id')] = array('name'=>$db->FetchResult($cur,$x,'name'), 'desc'=>langWord($db->FetchResult($cur,$x,'description')));
	}

	foreach($tables as $id=>$table) {
		$sql = "SELECT id, name, description FROM phoundry_column WHERE table_id = $id AND (datatype IN ('DTfile','DTurl') OR (datatype = 'DTstring' AND datatype_extra LIKE '%\"display\";s:4:\"html\"%')) ORDER BY order_by";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur))
			continue;

		print '<fieldset><legend><b>' . $table['desc'] . '</b> (<a href="#" onclick="selectAll(_D.forms[0][\'check_' . $id . '[]\']);return false">' . word(30) . '</a>)</legend>';
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$colId = $db->FetchResult($cur,$x,'id');
			// Make sure current user has view-rights on this column:
			$sql = "SELECT rights FROM phoundry_group_column WHERE group_id = {$PHSes->groupId} AND column_id = $colId";
			$tmp = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if ($db->EndOfResult($tmp) || strpos($db->FetchResult($tmp,0,'rights'), 'v') !== false) {
				print '<input type="checkbox" name="check_' . $id . '[]" value="' . $colId . '" />';
				print langWord($db->FetchResult($cur,$x,'description')) . "<br />\n";
			}
		}
		print "</fieldset>\n\n";
	}	
?>
</div>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Check" />
	</td>
	<td align="right">
	<input type="button" value="<?= word(46) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
