<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	checkAccess($TID);

	set_time_limit(0);

	$search = $_POST['search'];
	$tables = array();
	$host = $_SERVER['HTTP_HOST'];
	$lang = $PHSes->lang;

	foreach($_POST as $name=>$value) {
		if (preg_match('/check_(\d+)/', $name, $reg)) {
			$tables[$reg[1]] = $value;
		}
	}

	function urlMessage($fieldName, $fieldDesc, $url, $TID, $RID) {
		global $PHprefs;
		return '<li><a href="' . $url . '" target="mail">' . (strlen($url) > 120 ? substr($url,0,120) . '...' : $url) . '</a><br />Phoundry: <a href="' . $PHprefs['url'] . '/core/update.php?TID=' . $TID . '&RID=' . $RID . '#' . $fieldName . '">' . $fieldDesc . '</a></li>';
	}

	function debug($msg) {
		//print $msg;
	}


	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Zoek url's</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[


//]]>
</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Zoek url\'s'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<?php

	foreach($tables as $TID=>$CIDS) {
		$tableErrors = $tableChecks = 0;

		//
		// Determine TABLE properties:
		//
		$sql = "SELECT name, description FROM phoundry_table WHERE id = $TID";
		$cur = $db->Query($sql)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql\n");
		$tableName = $db->FetchResult($cur,0,'name');
		$tableDesc = langWord($db->FetchResult($cur,0,'description'), $lang);
		// Determine primary key field:
		$db->GetTableIndexDefinition($tableName, 'PRIMARY', $index)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "\n");
		$tableKey = array_keys($index['FIELDS']);
		$tableKey = $tableKey[0];

		//
		// Any system filters on this table?
		//
		$ands = array();
		$sql = "SELECT wherepart FROM phoundry_filter WHERE table_id = $TID AND type = 'system'";
		$cur = $db->Query($sql)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql\n");
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$filter = $db->FetchResult($cur,$x,'wherepart');
			if (preg_match('|\{\$([a-z][\w]+)\}|i', $filter)) continue;
			$ands = array_merge($ands, PH::getFilter(unserialize($filter)));
		}

		print '<fieldset><legend><b>' . $tableDesc . '</b></legend><ul>';

		//
		// Determine COLUMN properties:
		//
		$colProps = array();
		$sql = "SELECT * FROM phoundry_column WHERE id IN (" . implode(',', $CIDS) . ")";
		$cur = $db->Query($sql)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql\n");
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$colProps[$db->FetchResult($cur,$x,'name')] = 
				array('id'=>$db->FetchResult($cur,$x,'id'),
				      'desc'=>langWord($db->FetchResult($cur,$x,'description'), $lang),
				      'datatype'=>$db->FetchResult($cur,$x,'datatype'));
		}

		$sql = "SELECT $tableKey";
		foreach($colProps as $name=>$props)
			$sql .= ', ' . $name;
		$sql .= " FROM $tableName";
		if (count($ands) > 0) {
			$sql .= ' WHERE ' . implode(' AND ', $ands);
		}
		debug("SQL: $sql\n");
		$cur = $db->Query($sql)
			or die('File ' . __FILE__ . ', line ' . __LINE__ . ': ' . $db->Error() . "<br /><b>Query:</b> $sql\n");
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$nrf = $db->NumberOfColumns($cur);
			$RID = $db->FetchResult($cur,$x,0);

			for ($y = 1; $y < $nrf; $y++) {
				$fieldName = $db->FetchColumnName($cur,$y);
				$fieldDesc = $colProps[$fieldName]['desc'];
				$columnDT = $colProps[$fieldName]['datatype'];
				if ($columnDT == 'DTurl') {
					$url = $db->FetchResult($cur,$x,$fieldName);
					if (!$url)
						continue;
					if ($url{0} == '/')
						$url = 'http://' . $host . $url;

					if (!empty($search) && strpos($url, $search) === false) {
						continue;
					}

					debug("Found $url");
					print urlMessage($fieldName, $fieldDesc, $url, $TID, $RID);

					$tableChecks++;
				}
				elseif ($columnDT == 'DTfile') {
					$url = $db->FetchResult($cur,$x,$fieldName);
					if (!$url)
						continue;
					$url = $PHprefs['uploadUrl'] . $url;
					if ($url{0} == '/')
						$url = 'http://' . $host . $url;

					if (!empty($search) && strpos($url, $search) === false) {
						continue;
					}

					debug("Found $url");

					$tableChecks++;
					print urlMessage($fieldName, $fieldDesc, $url, $TID, $RID);
				}
				else {
					// Fetch HREF's and SRC's:
					$dom = new DOMDocument('1.0', $PHprefs['charset']);
					@$dom->loadHTML($db->FetchResult($cur,$x,$fieldName));
					$xpath = new DOMXPath($dom);
					$tags = $xpath->query('//img[@src]|//a[@href]|//form[@action]');
					foreach($tags as $tag) {
						$attribs = $tag->attributes;
						$url = $attribs->getNamedItem('src');
						if (!$url) { $src = $attribs->getNamedItem('href'); }
						if (!$url) { $src = $attribs->getNamedItem('action'); }
						if (!$url) continue;
						$url = $url->nodeValue;

						// Skip URL's that start with #, ., mailto:, javascript: or https://
						if (empty($url) || preg_match('/^(?:#|\.|mailto:|javascript:|https:\/\/)/i', $url))
							continue;
						if ($url{0} == '/')
							$url = 'http://' . $host . $url;

						if (!empty($search) && strpos($url, $search) === false) {
							continue;
						}

						debug("Found $url");

						$tableChecks++;
						print urlMessage($fieldName, $fieldDesc, $url, $TID, $RID);
					}
				}
			}
		}
		print '</ul><b>' . $tableChecks . " hyperlinks gevonden.</b></fieldset>\n\n";
	}
?>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<input type="button" value="Terug" onclick="history.back()" />
	</td>
</tr></table>
</div>

</body>
</html>
