--
-- Table structure for table `forum`
--

CREATE TABLE forum (
  id int(11) NOT NULL auto_increment,
  name varchar(80) NOT NULL default '',
  description varchar(255) default NULL,
  prio float NOT NULL default '0',
  use_preview tinyint(4) NOT NULL default '0',
  use_moderation tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `forum_post`
--

CREATE TABLE forum_post (
  id int(11) NOT NULL auto_increment,
  forum_id int(11) NOT NULL default '0',
  parent_id int(11) default NULL,
  author varchar(80) NOT NULL default '',
  subject varchar(255) NOT NULL default '',
  body text NOT NULL,
  email varchar(255) default NULL,
  post_host varchar(80) NOT NULL default '',
  post_date datetime NOT NULL default '0000-00-00 00:00:00',
  approved tinyint(4) NOT NULL default '0',
  is_sticky tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (id)
) TYPE=MyISAM;
