Web Power Forum Module
======================

This forum-module can be integrated into any website that is based on
Phoundry and Metabase. It requires MySQL 4.0 to run!
Phoundry's PREFS.php file should contain the following:

	$WEBprefs = array(
   	// How many posts per page:
   	'forumPerPage'    => 10
	// Whether or not to use captcha to prevent spamming:
	'forumUseCaptcha' => true
	);

Installation
------------

Run 'tables.sql' in your MySQL4 database.
Just copy the entire 'forum' tree into the 'htdocs' dir of your website.
Adjust 'forum.css' if you need different fonts or colors.

Arjan Haverkamp
January 5, 2005
