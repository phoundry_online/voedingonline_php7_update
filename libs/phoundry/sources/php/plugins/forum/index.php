<?php
	require_once 'forum.inc.php';

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<link rel="stylesheet" href="forum.css" type="text/css" />
<title>Forum</title>
</head>
<body>

<table class="forumIndex" cellpadding="3" cellspacing="1">
<tr>
	<th width="85%">Forum</th>
	<th width="5%" nowrap="nowrap"># Berichten</th>
	<th width="10%" nowrap="nowrap">Laatste bericht</th>
</tr>
<?php
	$sql = "SELECT id, name, description FROM forum ORDER BY prio DESC";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$forumID = $db->FetchResult($cur,$x,'id');
		print '<tr class="' . ($x & 1 ? 'even' : 'odd') . '">';
		print '<td><a href="topics.php?fid=' . $forumID . '"><b style="font-size:12pt">' . PH::htmlspecialchars($db->FetchResult($cur,$x,'name')) . '</b></a><br />' . PH::htmlspecialchars($db->FetchResult($cur,$x,'description')) . '</td>';

		$props = getForumProps($forumID);

		// How many posts:
		print '<td align="center">' . $props['posts'] . '</td>';
		
		// Last post:
		print '<td nowrap="nowrap">' . $props['last_post'] . '</td>';

		print "</tr>\n";
	}
	if ($x == 0) {
		print '<tr><td colspan="3"><b>Momenteel geen actieve forums!</b></td></tr>';
	}
?>
</table>

</body>
</html>

