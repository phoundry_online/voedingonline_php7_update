<?php
	require_once 'forum.inc.php';

	$fid = (int)$_GET['fid'];
	$pid = (int)$_GET['pid'];
	$forumProps = getForumProps($fid);

	$defVals = array(
		'subject'	=> '',
		'body'		=> '',
		'author'		=> '',
		'email'		=> ''
	);
		
	// Do we have a cookie?
	$prefs = isset($_COOKIE['PHforumPrefs']) ? unserialize(stripslashes($_COOKIE['PHforumPrefs'])) : array();
	if (isset($prefs['name'])) {
		$defVals['author'] = $prefs['name'];
	}
	if (isset($prefs['email'])) {
		$defVals['email'] = $prefs['email'];
	}

	$sql = "SELECT author, email, subject FROM forum_post WHERE id = {$pid}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$email = $db->FetchResult($cur,0,'email');
	$author = $db->FetchResult($cur,0,'author');
	$defVals['subject'] = 'Re: ' . $db->FetchResult($cur,0,'subject');

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (!isset($_POST['email'])) exit;

		require dirname($_SERVER['DOCUMENT_ROOT']) . '/include/DMDsmtp.php';
		require dirname($_SERVER['DOCUMENT_ROOT']) . '/include/DMDmime.php';

		// Store cookie with name and e-mail address:
		$prefs = array('name'=>$_POST['author'], 'email'=>$_POST['email']);
		$https = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on');
		$_COOKIE['PHforumPrefs'] = serialize($prefs);
      setcookie('PHforumPrefs', $_COOKIE['PHforumPrefs'], (time()+(3600*24*365)), '/', preg_replace('/:([0-9]+)$/', '', $_SERVER['HTTP_HOST']), $https ? 1 : 0);

		$body = nl2br(_ubbreplace($_POST['body']));
		$html = '<html><head><title>' . $_POST['subject'] . '</title></head><body><div style="font: 11pt Arial, Helvetica, Sans-serif">' . $body . '<hr noshade="noshade" size="1" /><small>Dit bericht werd verzonden via het forum op ' . $_SERVER['HTTP_HOST'] . ', vanaf IP adres ' . $_SERVER['REMOTE_ADDR'] . '.</small></body></html>';

		$smtp = new DMDsmtp();
		$port = isset($PHprefs['SMTPport']) ? $PHprefs['SMTPport'] : 25;
		$helo = isset($PHprefs['SMTPhelo']) ? $PHprefs['SMTPhelo'] : php_uname('n');
		$smtp->connect(array('host'=>$PHprefs['SMTPserver'], 'port'=>$port, 'helo'=>$helo));
		$mime = new DMDmime();
		$mime->setHTMLBody($html);

		$eml = $mime->get(array(
			'text_encoding'   => '7bit',
			'html_encoding'   => 'quoted-printable',
			'head_charset'    => 'iso-8859-1',
			'text_charset'    => 'iso-8859-1',
			'html_charset'    => 'iso-8859-1'
		));

		$hdrs = array(
			'X-Mailer'        => 'forum@' . $_SERVER['HTTP_HOST'],
			'From'            => '"' . $_POST['author'] . '" <' . $_POST['email'] . '>',
			'To'              => $_POST['email'],
			'Subject'         => $_POST['subject']
		);

		$hdrs = $mime->headers($hdrs);

		$res = $smtp->send($_POST['email'], $email, $hdrs, $eml);
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<link rel="stylesheet" href="forum.css" type="text/css" />
<title>Forum - <?= PH::htmlspecialchars($forumProps['name']) ?> - E-mail <?= PH::htmlspecialchars($author) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W = window, _D = document;

function trim(str) {
	return str.replace(/^\s+/,'').replace(/\s+$/,'');
}

function checkEmail(value) {
	return /^([a-z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-z0-9\-]+\.)+))([a-z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
}

function checkForm(F) {
	var msg = '';
	if (trim(F['author'].value) == '') {
		msg += "Verplicht veld 'Uw naam' is niet ingevuld!\n";
	}
	if (trim(F['subject'].value) == '') {
		msg += "Verplicht veld 'Onderwerp' is niet ingevuld!\n";
	}
	if (trim(F['body'].value) == '') {
		msg += "Verplicht veld 'Bericht' is niet ingevuld!\n";
	}
	if (!checkEmail(F['email'].value)) {
		msg += "Voer een geldig e-mail adres in!\n";
	}
	if (msg == '') return true;
	alert(msg);
	return false;
}

function det_replace(type, text)
{
	var val = '';
	switch (type)
	{
		case 'plain':
			break;
		case 'bold':
			text = '[b]'+text+'[/b]';
			break;
		case 'italic':
			text = '[i]'+text+'[/i]';
			break;
		case 'underline':
			text = '[u]'+text+'[/u]';
			break;
		case 'url':
			if (/^(http:\/\/)/i.test(text))
			{
				val = prompt('Omschrijving hyperlink:', text);
				if (val !== null && val != '') text = '[url='+text+']'+val+'[/url]';
			}
			else
			{
				val = prompt('URL hyperlink:','http:\/\/');
				if (val !== null && val != 'http:\/\/')
				{
					if (text == '') text = '[url]'+val+'[/url]';
					else text = '[url='+val+']'+text+'[/url]';
				}
			}
			break;
		case 'hr':
			text += '[hr]';
			break;
	}
	return text;
}

function storeCursor(el)
{
	if (typeof el.createTextRange != 'undefined')
		el.cursorPos = _D.selection.createRange().duplicate();
}

function editCmd(type, text)
{
	var target = _D.getElementById('bodyTA');

	if (target)
	{
		target.focus();
		storeCursor(target);
		if (typeof target.cursorPos != 'undefined')
		{
			var cursorPos = target.cursorPos;
			if (type != 'plain') text = cursorPos.text;
			cursorPos.text = det_replace(type, text);
		}
		else if (typeof target.selectionStart != 'undefined')
		{
			var scrollTop = target.scrollTop;

			var sStart = target.selectionStart;
			var sEnd = target.selectionEnd;
			if (type != 'plain') text = target.value.substring(sStart, sEnd);
			text = det_replace(type, text);
			target.value = target.value.substr(0, sStart) + text + target.value.substr(sEnd);
			var nStart = sStart == sEnd ? sStart + text.length : sStart;
			var nEnd = sStart + text.length;
			target.setSelectionRange(nStart, nEnd);

			target.scrollTop = scrollTop;
		}
		else
		{
			if (type != 'plain') text = '';
			target.value += det_replace(type, text);
		}
		target.focus();
		storeCursor(target);
	}
}

//]]>
</script>
</head>
<body>

<div class="forumHeader">
	<div style="float:right">&nbsp;</div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a>
	<?php if (isset($tid)) { ?>
	&bull; <a href="read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><?= PH::htmlspecialchars($topicProps['subject']) ?></a>
	<?php } ?>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	</div>
</div>

<h2><a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a> &raquo; E-mail <?= PH::htmlspecialchars($author) ?></h2>

<div align="center">

<?php if ($_SERVER['REQUEST_METHOD'] == 'POST') { ?>

<fieldset style="width:70%">
<?php if ($res) { ?>

<legend><b>Bericht verzonden</b></legend>
Het volgende bericht is verzonden aan <?= PH::htmlspecialchars($author) ?>:
<hr noshade="noshade" size="1" />
<p>
<?= $body ?>
</p>

<?php } else { ?>

<legend><b class="req">Bericht niet verzonden!</b></legend>
Om technische redenen kon uw bericht helaas niet worden verzonden.<br />
Probeer het later nog eens.
<?php } ?>

</fieldset>

<br />

<?php } else { ?>

<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= $_SERVER['QUERY_STRING'] ?>" method="post" onsubmit="return checkForm(this)">

<fieldset style="width:70%">
<legend><b>Ontvanger</b></legend>
<b><?= PH::htmlspecialchars($author) ?></b>
</fieldset>

<fieldset style="width:70%">
<legend><b class="req">Uw naam</b></legend>
<input type="text" name="author" size="60" maxlength="255" value="<?= PH::htmlspecialchars($defVals['author']) ?>" style="width:100%" />
</fieldset>

<fieldset style="width:70%">
<legend><b class="req">Uw e-mail adres</b></legend>
<small>Uw e-mail adres wordt niet op deze website getoond!</small><br />
<input type="text" name="email" size="60" maxlength="255" value="<?= PH::htmlspecialchars($defVals['email']) ?>" style="width:100%" />
</fieldset>

<fieldset style="width:70%">
<legend><b class="req">Onderwerp</b></legend>
<input type="text" name="subject" size="60" maxlength="255" value="<?= PH::htmlspecialchars($defVals['subject']) ?>" style="width:100%" />
</fieldset>

<fieldset style="width:70%">
<legend><b class="req">Bericht</b></legend>
<div>
<img class="forumBut" src="pics/bold.gif" alt="Bold" onclick="editCmd('bold')" />
<img class="forumBut" src="pics/italic.gif" alt="Italic" onclick="editCmd('italic')" />
<img class="forumBut" src="pics/under.gif" alt="Underline" onclick="editCmd('underline')" />
<img class="forumBut" src="pics/link.gif" alt="Hyperlink" onclick="editCmd('url')" />
<img class="forumBut" src="pics/hr.gif" alt="Horizontal Ruler" onclick="editCmd('hr')" />
</div>
<textarea id="bodyTA" name="body" cols="60" rows="20" style="width:100%"><?= PH::htmlspecialchars($defVals['body']) ?></textarea>
</fieldset>

<p style="width:70%" align="right">
<input type="submit" value="Verstuur" />
</p>

</div>

</form>

<?php } ?>


<div class="forumFooter">
	<div style="float:right">&nbsp;</div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a>
	<?php if (isset($tid)) { ?>
	&bull; <a href="read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><?= PH::htmlspecialchars($topicProps['subject']) ?></a>
	<?php } ?>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	</div>
</div>

</body>
</html>


