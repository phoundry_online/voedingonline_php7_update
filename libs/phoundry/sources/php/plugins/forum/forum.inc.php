<?php

require_once(getenv('CONFIG_DIR') . '/PREFS.php');
require_once($PHprefs['distDir'] . '/core/include/metabase/connect.php');

$dateFormat = 'd-m-Y';
$timeFormat = 'H:i';

// Always connect to the database.
$db = DBconnect(true);

if (get_magic_quotes_gpc()) {
	foreach(array('_GET','_POST','_COOKIE') as $super) {
		foreach($GLOBALS[$super] as $k=>$v) {
			$GLOBALS[$super][$k] = _stripslashes_r($v);
		}
	}
}

function _stripslashes_r($str)
{
	if (is_array($str)) {
		foreach ($str as $k=>$v) {
			$str[$k] = _stripslashes_r($v);
		}
		return $str;
	} else {
		return stripslashes($str);
	}
}

function getForumProps($id) {
	global $db, $dateFormat, $timeFormat;

	$props = array('id'=>$id);

	$sql = "SELECT name, description, use_preview, use_moderation FROM forum WHERE id = $id";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$props['name'] = $db->FetchResult($cur,0,'name');
	$props['description'] = $db->FetchResult($cur,0,'description');
	$props['use_moderation'] = $db->FetchResult($cur,0,'use_moderation');
	$props['use_preview'] = $db->FetchResult($cur,0,'use_preview') == 1;

	// How many posts:
	$sql = "SELECT COUNT(*) FROM forum_post WHERE forum_id = $id";
	if ($props['use_moderation']) {
		$sql .= " AND approved = 1";
	}
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$props['posts'] = $db->FetchResult($cur,0,0);
		
	// Last post:
	$sql = "SELECT MAX(UNIX_TIMESTAMP(post_date)) FROM forum_post WHERE forum_id = $id";
	if ($props['use_moderation']) {
		$sql .= " AND approved = 1";
	}
	$tmp = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	if (!$db->ResultIsNull($tmp,0,0)) {
		$props['last_post'] = date("$dateFormat $timeFormat", $db->FetchResult($tmp,0,0));
	}
	else {
		$props['last_post'] = '';
	}
	return $props;
}

function getForumAuthor($author, $email = null) {
	$txt = '';
	if ($email) {
		$txt = '<a href="mailto:';
		$len = strlen($email);
		for ($x = 0; $x < $len; $x++) {
			$txt .= '&#' . ord($email{$x}) . ';';
		}
		$txt .= '">' . PH::htmlspecialchars($author) . '</a>';
		return $txt;
	}
	else {
		return PH::htmlspecialchars($author);
	}
}

function _escDBquote($str) {
	return mysql_real_escape_string($str);
}

function _ubbreplace($var)
{
	$var = preg_replace("/\[code\]([^\\[]*)\[\/code\]/se", '"[div style=\"border:1px solid #000;padding:5px;margin:5px;font:10pt Courier\"]" . PH::htmlspecialchars(trim("\\1")) . "[/div]"', $var);

   // Strip any HTML tags
   $var = strip_tags($var);

	$var = preg_replace('/\[div([^\]]+)\]/', "<div\\1>", $var);
	$var = str_replace('[/div]', '</div>', $var);

   // Bold, underline, and italic UBB tags
   $var = preg_replace("/\[b\]([^\\[]*)\[\/b\]/s", "<b>\\1</b>", $var);
   $var = preg_replace("/\[u\]([^\\[]*)\[\/u\]/s", "<u>\\1</u>", $var);
   $var = preg_replace("/\[i\]([^\\[]*)\[\/i\]/s", "<i>\\1</i>", $var);
	$var = str_replace('[hr]', '<hr noshade="noshade" size="1" />', $var);

   // UBB Url tags
   $var = preg_replace("/\[url=([^\]]+)\]([^\\[]*)\[\/url\]/s", "<a href=\"\\1\" target=\"ext_url\">\\2</a>", $var);
	$var = preg_replace("/\[url\]([^\\[]*)\[\/url\]/s", "<a href=\"\\1\" target=\"ext_url\">\\1</a>", $var);

	// Quotes:
	$var = str_replace(array('[quote]', '[/quote]'), array('<div class="forumQuote"><img src="pics/quote_open.gif" width="39" height="32" alt="quote" align="left" />', '</div>'), $var);

   // E-mail Addresses
   $var = preg_replace("/(([a-zA-Z0-9_-]+)@([a-zA-Z0-9_-]+)([\.a-zA-Z0-9_-]+))/", "<a href=\"mailto:\\2@\\3\\4\">\\1</a>", $var);

   return $var;
}

function getNavigation($totalResults) {
	global $WEBprefs;
	
	$curPage = isset($_GET['page']) ? (int)$_GET['page'] : 0;
	$perPage = $WEBprefs['forumPerPage'];
	$nrPages =  $totalResults/$perPage;
	$offset = $perPage * $curPage;

	$html = '<form action="' . $_SERVER['PHP_SELF'] . '" method="get">';
	foreach (array('fid', 'tid', 'pid') as $x) {
		if (isset($_GET[$x])) {
			$html .= '<input type="hidden" name="' . $x . '" value="' . PH::htmlspecialchars($_GET[$x]) . '" />';
		}
	}
	$html .= '<input type="button" onclick="var F=_D.forms[0];F[\'page\'].value=F[\'page\'].selectedIndex-1;F.submit()" ' .  ($offset == 0 ? 'disabled="disabled" ' : '') . ' value="&lt;&lt;" />';
	
	$html .= ' <select' . (($nrPages <= 0) ? ' disabled="disabled" ' : ' ') . 'name="page" onchange="this.form.submit()">';
	for($i = 0; $i < $nrPages; $i++) {
		$html .= '<option value="' . $i . '" ' . ($i == $curPage ? 'selected="selected"' : '') . '>' . ($i+1) . '</option>';
	}
	$html .= '</select> ';
	
	$html .= '<input type="button" onclick="var F=_D.forms[0];F[\'page\'].value=F[\'page\'].selectedIndex+1;F.submit()" ' . (($curPage+1)*$perPage >= $totalResults ? 'disabled="disabled" ' : '') . ' value="&gt;&gt;" />';
	$html .= '</form>';

	return $html;
}


function getMessage($pid, $subject, $author, $email, $post_date, $body) {
	global $fid, $tid, $dateFormat, $timeFormat;

	$html  = '<div class="forumPost">';
	$html .= '<div class="forumPostProps"><table cellspacing="0" cellpadding="0" width="99%"><tr>';
	$html .= '<td width="49%" class="forumPostSubject"><a name="post' . $pid . '">' . PH::htmlspecialchars($subject) . '</a></td>';
	$html .= '<td width="49%" align="right">';
	if ($email) {
		$html .= '<a href="mail.php?fid=' . $fid . '&amp;pid=' . $pid . '">';
	}
	$html .= $author;
	if ($email) {
		$html .= '</a>';
	}
	$html .= ', ' . date("$dateFormat $timeFormat", $post_date);
	$html .= '</td>';
	$html .= '<td width="2%" align="right" style="padding-left:10px">';
	
	if (!is_null($pid)) {
		$html .= '<a href="post.php?fid=' . $fid . '&amp;tid=' . $tid . '&amp;pid=' . $pid . '&amp;quote=1"><img src="pics/quote.gif" alt="Quote" width="26" height="24" border="0" /></a>';
	}
	
	$html .= '</td>';
	$html .= '</tr></table></div>';
	
	$body = nl2br(_ubbreplace($body));
	$html .= '<p>' . $body . '</p>';
	$html .= "</div>\n";
	$html .= "<br />\n";
	return $html;
}

?>
