<?php
	require_once 'forum.inc.php';

	$fid = (int)$_GET['fid'];
	$forumProps = getForumProps($fid);

	$defVals = array(
		'subject'	=> '',
		'body'		=> '',
		'author'		=> '',
		'email'		=> ''
	);
		
	// Do we have a cookie?
	$prefs = isset($_COOKIE['PHforumPrefs']) ? unserialize(stripslashes($_COOKIE['PHforumPrefs'])) : array();
	if (isset($prefs['name'])) {
		$defVals['author'] = $prefs['name'];
	}
	if (isset($prefs['email'])) {
		$defVals['email'] = $prefs['email'];
	}

	if (isset($_GET['tid'])) { 
		// This is a reply/quote in an existing topic:
		$tid = (int)$_GET['tid'];
		$sql = "SELECT id, author, subject, email, post_host, UNIX_TIMESTAMP(post_date) as post_date FROM forum_post WHERE id = $tid AND approved = 1";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$db->FetchResultAssoc($cur,$topicProps,0);
		$title = 'Reactie op ' . $topicProps['subject'];
		$defVals['subject'] = 'Re: ' . $topicProps['subject'];
		if (isset($_GET['pid']) && isset($_GET['quote']) && $_GET['quote'] == 1) {
			$pid = (int)$_GET['pid'];
			$sql = "SELECT body FROM forum_post WHERE id = $pid AND approved = 1";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur)) {
				$defVals['body'] = '[quote]' . $db->FetchResult($cur,0,'body') . '[/quote]';
			}
		}
	}
	else {
		// This is a new topic:
		$title = 'Nieuw onderwerp';
	}

	if (isset($_POST['body'])) {
		$defVals['body'] = $_POST['body'];
	}
	if (isset($_POST['subject'])) {
		$defVals['subject'] = $_POST['subject'];
	}
	if (isset($_POST['author'])) {
		$defVals['author'] = $_POST['author'];
	}
	if (isset($_POST['email'])) {
		$defVals['email'] = $_POST['email'];
	}

	if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['preview'] == 0) {
		if (!isset($_POST['js_check']) || $_POST['js_check'] != 'checked') {
			die('Ongeldig request!');
		}

		if ($WEBprefs['forumUseCaptcha']) {
			session_name('ForumCaptcha');
			session_start();
			if (strtolower($_SESSION['captcha']) != strtolower($_POST['captcha'])) {
				die('Ongeldig request!');
			}
			unset($_SESSION['captcha']);
		}

		// Store cookie with name and e-mail address:
		$prefs = array('name'=>$_POST['author'], 'email'=>$_POST['email']);
		$https = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on');
		$_COOKIE['PHforumPrefs'] = serialize($prefs);
      setcookie('PHforumPrefs', $_COOKIE['PHforumPrefs'], (time()+(3600*24*365)), '/', preg_replace('/:([0-9]+)$/', '', $_SERVER['HTTP_HOST']), $https ? 1 : 0);
	
		$approved    = ($forumProps['use_moderation']) ? 0 : 1;
		$tid = isset($_GET['tid']) ? (int)$_GET['tid'] : 'NULL';
		$sql = "INSERT INTO forum_post VALUES (NULL, {$fid}, {$tid}, '" . _escDBquote($_POST['author']) . "', '" . _escDBquote($_POST['subject']) . "', '" . _escDBquote($_POST['body']) . "', '" . _escDBquote($_POST['email']) . "', '" . _escDBquote($_SERVER['REMOTE_ADDR']) . "', NOW(),  $approved, 0)";

		$db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		if (isset($_GET['tid'])) {
			$url = 'read.php?fid=' . $fid . '&tid=' . $tid;
		}
		else {
			$url = 'topics.php?fid=' . $fid;
		}
		if ($forumProps['use_moderation']) {
			$url .= '&mod';
		}
		header("Location: $url");
		exit();
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<link rel="stylesheet" href="forum.css" type="text/css" />
<title>Forum - <?= PH::htmlspecialchars($forumProps['name']) ?> - <?= PH::htmlspecialchars($title) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W = window, _D = document;

function trim(str) {
	return str.replace(/^\s+/,'').replace(/\s+$/,'');
}

function checkEmail(value) {
	return /^([a-z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-z0-9\-]+\.)+))([a-z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
}

function checkForm(F) {
	var msg = '';
	F['js_check'].value = 'checked';
	if (trim(F['author'].value) == '') {
		msg += "Verplicht veld 'Uw naam' is niet ingevuld!\n";
	}
	if (trim(F['subject'].value) == '') {
		msg += "Verplicht veld 'Onderwerp' is niet ingevuld!\n";
	}
	if (trim(F['body'].value) == '') {
		msg += "Verplicht veld 'Bericht' is niet ingevuld!\n";
	}
	if (trim(F['email'].value) != '' && !checkEmail(F['email'].value)) {
		msg += "Voer een geldig e-mail adres in!\n";
	}
	<?php if ($WEBprefs['forumUseCaptcha']) { ?>
	if (trim(F['captcha'].value) == '' || F['captcha'].value.length != 6) {
		msg += "Type de bevestigingscode over!\n";
	}
	<?php } ?>
	if (msg == '') return true;
	alert(msg);
	return false;
}

function det_replace(type, text)
{
	var val = '';
	switch (type)
	{
		case 'plain':
			break;
		case 'bold':
			text = '[b]'+text+'[/b]';
			break;
		case 'italic':
			text = '[i]'+text+'[/i]';
			break;
		case 'underline':
			text = '[u]'+text+'[/u]';
			break;
		case 'url':
			if (/^(http:\/\/)/i.test(text))
			{
				val = prompt('Omschrijving hyperlink:', text);
				if (val !== null && val != '') text = '[url='+text+']'+val+'[/url]';
			}
			else
			{
				val = prompt('URL hyperlink:','http:\/\/');
				if (val !== null && val != 'http:\/\/')
				{
					if (text == '') text = '[url]'+val+'[/url]';
					else text = '[url='+val+']'+text+'[/url]';
				}
			}
			break;
		case 'hr':
			text += '[hr]';
			break;
	}
	return text;
}

function storeCursor(el)
{
	if (typeof el.createTextRange != 'undefined')
		el.cursorPos = _D.selection.createRange().duplicate();
}

function editCmd(type, text)
{
	var target = _D.getElementById('bodyTA');

	if (target)
	{
		target.focus();
		storeCursor(target);
		if (typeof target.cursorPos != 'undefined')
		{
			var cursorPos = target.cursorPos;
			if (type != 'plain') text = cursorPos.text;
			cursorPos.text = det_replace(type, text);
		}
		else if (typeof target.selectionStart != 'undefined')
		{
			var scrollTop = target.scrollTop;

			var sStart = target.selectionStart;
			var sEnd = target.selectionEnd;
			if (type != 'plain') text = target.value.substring(sStart, sEnd);
			text = det_replace(type, text);
			target.value = target.value.substr(0, sStart) + text + target.value.substr(sEnd);
			var nStart = sStart == sEnd ? sStart + text.length : sStart;
			var nEnd = sStart + text.length;
			target.setSelectionRange(nStart, nEnd);

			target.scrollTop = scrollTop;
		}
		else
		{
			if (type != 'plain') text = '';
			target.value += det_replace(type, text);
		}
		target.focus();
		storeCursor(target);
	}
}

//]]>
</script>
</head>
<body>

<div class="forumHeader">
	<div style="float:right">&nbsp;</div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a>
	<?php if (isset($tid)) { ?>
	&bull; <a href="read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><?= PH::htmlspecialchars($topicProps['subject']) ?></a>
	<?php } ?>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	</div>
</div>

<h2><a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a> &raquo; <?= PH::htmlspecialchars($title) ?></h2>

<?php
	if (isset($_POST['preview']) && $_POST['preview'] == 1) {
		print getMessage(null, $_POST['subject'], $_POST['author'], $_POST['email'], time(), $_POST['body']);
	}
?>

<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= $_SERVER['QUERY_STRING'] ?>" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="js_check" />
<input type="hidden" name="preview" value="0" />

<div align="center">

<fieldset style="width:70%">
<legend><b class="req">Uw naam</b></legend>
<input type="text" name="author" size="60" maxlength="255" value="<?= PH::htmlspecialchars($defVals['author']) ?>" style="width:100%" />
</fieldset>

<fieldset style="width:70%">
<legend><b>Uw e-mail adres</b></legend>
<small>Uw e-mail adres wordt niet op deze website getoond!</small><br />
<input type="text" name="email" size="60" maxlength="255" value="<?= PH::htmlspecialchars($defVals['email']) ?>" style="width:100%" />
</fieldset>

<fieldset style="width:70%">
<legend><b class="req">Onderwerp</b></legend>
<input type="text" name="subject" size="60" maxlength="255" value="<?= PH::htmlspecialchars($defVals['subject']) ?>" style="width:100%" />
</fieldset>

<fieldset style="width:70%">
<legend><b class="req">Bericht</b></legend>
<div>
<img class="forumBut" src="pics/bold.gif" alt="Bold" onclick="editCmd('bold')" />
<img class="forumBut" src="pics/italic.gif" alt="Italic" onclick="editCmd('italic')" />
<img class="forumBut" src="pics/under.gif" alt="Underline" onclick="editCmd('underline')" />
<img class="forumBut" src="pics/link.gif" alt="Hyperlink" onclick="editCmd('url')" />
<img class="forumBut" src="pics/hr.gif" alt="Horizontal Ruler" onclick="editCmd('hr')" />
</div>
<textarea id="bodyTA" name="body" cols="60" rows="20" style="width:100%"><?= PH::htmlspecialchars($defVals['body']) ?></textarea>
</fieldset>

<?php if ($WEBprefs['forumUseCaptcha']) { ?>
<fieldset style="width:70%">
<legend><b class="req">Bevestigingscode</b></legend>
<small>Type de bevestigingscode over.</small>
<table><tr>
<td><input type="text" name="captcha" size="10" maxlength="6" /></td>
<td><img src="captcha/captcha.php" alt="" /></td>
</tr></table>
</fieldset>
<?php } ?>

<p style="width:70%" align="right">
<?php if ($forumProps['use_preview']) { ?>
<input type="submit" value="Preview" onclick="this.form['preview'].value=1" />
<?php } ?>
<input type="submit" value="Post" />
</p>

</div>

</form>

<div class="forumFooter">
	<div style="float:right">&nbsp;</div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a>
	<?php if (isset($tid)) { ?>
	&bull; <a href="read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><?= PH::htmlspecialchars($topicProps['subject']) ?></a>
	<?php } ?>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	</div>
</div>

</body>
</html>


