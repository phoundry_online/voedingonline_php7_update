<?php	
	require_once '../forum.inc.php';

	$fid = (int)$_GET['fid'];
	$tid = (int)$_GET['tid'];
	$forumProps = getForumProps($fid);

	function XMLentities($str) {
		return preg_replace('/([\x7F-\xFF])/e', '"&#" . ord("\\1") . ";"', PH::htmlspecialchars($str)); 
	}

	$sql = "SELECT id, author, subject, body, email, post_host, UNIX_TIMESTAMP(post_date) as post_date FROM forum_post WHERE id = $tid AND approved = 1";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	header('Content-type: text/xml');

	print '<?xml version="1.0" encoding="iso-8859-1"?>' . "\n";
?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
<channel>
<title>Forum Title</title>
<copyright>Copyright <?= date('Y') ?></copyright>
<pubDate><?= date('D, d M Y H:i:s GMT', time()-3600) ?></pubDate>
<lastBuildDate><?= date('D, d M Y H:i:s GMT', time()-3600) ?></lastBuildDate>
<docs>http://blogs.law.harvard.edu/tech/rss</docs>
<link>http://<?= $_SERVER['HTTP_HOST'] ?>/forum/read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?></link>
<description>Forum <?= XMLentities($forumProps['name']) ?>, Topic <?= XMLentities($db->FetchResult($cur,0,'subject')) ?></description>
<item>
	<title><?= XMLentities($db->FetchResult($cur,0,'subject')) ?></title>
	<link>http://<?= $_SERVER['HTTP_HOST'] ?>/forum/read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>&amp;pid=<?=  $db->FetchResult($cur,0,'id') ?></link>
	<description><?= XMLentities(strip_tags($db->FetchResult($cur,0,'body'))) ?></description>
	<content:encoded>
	<![CDATA[
	<?= nl2br(_ubbreplace($db->FetchResult($cur,0,'body'))) ?>
	]]>
	</content:encoded>
	<author><?= XMLentities($db->FetchResult($cur,0,'author')) ?></author>
	<pubDate><?= date('D, d M Y H:i:s GMT', $db->FetchResult($cur,0,'post_date')-3600) ?></pubDate>
</item>
<?php
	$sql = "SELECT id, author, subject, body, email, post_host, UNIX_TIMESTAMP(post_date) as post_date FROM forum_post WHERE parent_id = $tid AND approved = 1 ORDER BY id";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		print "\t<item>\n";
		print "\t<title>" . XMLentities($db->FetchResult($cur,$x,'subject')) . "</title>\n";
		print "\t<link>http://{$_SERVER['HTTP_HOST']}/forum/read.php?fid={$fid}&amp;tid={$tid}&amp;pid=" . $db->FetchResult($cur,$x,'id') . "</link>\n";
		print "\t<description>" . XMLentities(strip_tags($db->FetchResult($cur,$x,'body'))) . "</description>\n";
		print "\t<content:encoded>\n\t<![CDATA[\n\t" . nl2br(_ubbreplace($db->FetchResult($cur,$x,'body'))) . "\n\t]]>\n\t</content:encoded>\n";
		print "\t<author>" . XMLentities($db->FetchResult($cur,$x,'author')) . "</author>\n";
		print "\t<pubDate>" . date('D, d M Y H:i:s GMT', $db->FetchResult($cur,$x,'post_date')-3600) . "</pubDate>\n";
		print "\t</item>\n";
	}
?>
</channel>
</rss>
