<?php
	require_once 'forum.inc.php';

	$fid = (int)$_GET['fid'];
	$forumProps = getForumProps($fid);

	$curPage = isset($_GET['page']) ? (int)$_GET['page'] : 0;
	$perPage = $WEBprefs['forumPerPage'];
	$offset = $perPage * $curPage;
	$totalResults = 0;

	$content = '';

	$sql = "SELECT SQL_CALC_FOUND_ROWS id, is_sticky, author, email, subject FROM forum_post WHERE forum_id = {$fid} AND parent_id IS NULL AND approved = 1 ORDER BY is_sticky DESC, post_date DESC LIMIT $offset, $perPage";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	$sql = "SELECT found_rows()";
	$tmp = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($tmp)) {
		$totalResults = $db->FetchResult($tmp,0,0);
	}

	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$tid = $db->FetchResult($cur,$x,'id');
		$isSticky = $db->FetchResult($cur,$x,'is_sticky') == 1;
		$content .= '<tr class="' . ($x & 1 ? 'even' : 'odd') . '">';
		$content .= '<td' . ($isSticky ? ' style="font-weight:bold"' : '') . '>';
		if ($isSticky) {
			$content .= '<b>Sticky: </b>';
		}
		$content .= '<a href="read.php?fid=' . $fid . '&amp;tid=' . $tid . '">' . PH::htmlspecialchars($db->FetchResult($cur,$x,'subject')) . '</a></td>';

		// How many posts?
		$sql = "SELECT COUNT(*) FROM forum_post WHERE parent_id = {$tid}";
		$tmp = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$content .= '<td align="center">' . $db->FetchResult($tmp,0,0) . '</td>';

		// Started by:
		$email = $db->FetchResult($cur,$x,'email');
		$content .= '<td>';

		if ($email) {
			$content .= '<a href="mail.php?fid=' . $fid . '&amp;pid=' . $tid . '">';
		}
		$content .= $db->FetchResult($cur,$x,'author');
		if ($email) {
			$content .= '</a>';
		}

		$content .= '</td>';
		//. getForumAuthor($db->FetchResult($cur,$x,'author'), $db->FetchResult($cur,$x,'email')) . '</td>';

		// Last post:
		$sql = "SELECT MAX(UNIX_TIMESTAMP(post_date)) FROM forum_post WHERE parent_id = {$tid}";
		$tmp = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$content .= '<td>';
		if (!$db->ResultIsNull($tmp,0,0)) {
			$content .= date("$dateFormat $timeFormat", $db->FetchResult($tmp,0,0));
		}
		$content .= '</td>';

		$content .= "</tr>\n";
	}

	$navigation = getNavigation($totalResults);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<link rel="stylesheet" href="forum.css" type="text/css" />
<title>Forum - <?= PH::htmlspecialchars($forumProps['name']) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W = window, _D = document;

<?php if (strpos($_SERVER['QUERY_STRING'], 'mod') !== false) { ?>
window.onload=function(){
	alert('Uw bericht wordt na goedkeuring door de redactie geplaatst.');
}
<?php } ?>

//]]>
</script>
</head>
<body>
<div class="forumHeader">
	<div style="float:right"><?= $navigation ?></div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	&bull; <a href="post.php?fid=<?= $fid ?>">Nieuw onderwerp</a>
	</div>
</div>

<h2><?= PH::htmlspecialchars($forumProps['name']) ?></h2>

<table class="forumIndex" cellpadding="3" cellspacing="1">
<tr>
	<th width="85%">Onderwerp</th>
	<th width="5%" nowrap="nowrap"># Antwoorden</th>
	<th width="5%" nowrap="nowrap">Gestart door</th>
	<th width="5%" nowrap="nowrap">Laatste antwoord</th>
</tr>
<?= $content ?>
</table>

<br />

<div class="forumFooter">
	<div style="float:right"><?= $navigation ?></div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	&bull; <a href="post.php?fid=<?= $fid ?>">Nieuw onderwerp</a>
	</div>
</div>

</body>
</html>
