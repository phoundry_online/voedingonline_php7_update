<?php	
	require_once 'forum.inc.php';

	$fid = (int)$_GET['fid'];
	$forumProps = getForumProps($fid);

function build_search_terms($search, $match) {
	$terms=array();

	// if this is an exact phrase match
	if($match == 3) {
		$terms[]=$search;
	}
	// not exact phrase, break up the terms
	else {
		if (strpos($search, '"') !== false) {
			// first pull out all the double quoted strings
			if(strpos($search, '"') !== false){
				$search_string = $search;
				while(ereg('-*"[^"]*"', $search_string, $match)){
					$terms[]=trim(str_replace('"', '', $match[0]));
					$search_string = substr(strstr($search_string, $match[0]), strlen($match[0]));
				}
			}
			$search = ereg_replace('-*"[^"]*"', '', $search);
		}

		// pull out the rest words in the string
		$regular_terms = explode(' ', $search);

		// merge them all together and return
		while (list($key, $val) = each($regular_terms)) {
			if($val!='') {
				$terms[] = trim($val);
			}
		}
	}
	return $terms;
}

function build_terms_clause($terms, $date, $fields, $match){
	static $where_clause;
	$termArray = array();

	if(empty($where_clause)){
		if($date!=0){
			$cutoff=date('Y-m-d', mktime(0,0,0,date('m'),date('d')-$date));
			$where_clause .= " fp.post_date >= '$cutoff' AND ";
		}

		while (list ($junk, $term) = each ($terms)) {
			$cmpfunc = 'LIKE';
			if(substr($term, 0, 1)== '-'){
				$term = substr($term, 1);
				$cmpfunc = 'NOT LIKE';
			}
			reset($fields);
			unset($likeArray);
			while (list ($key, $val) = each($fields)) {
				$likeArray[]=" fp.$val $cmpfunc '%$term%' ";
			}
			$termArray[] = ' (' . implode($likeArray, ' OR ') . ') ';
		}

		$cmptype = 'AND';
		if($match != 1) $cmptype = 'OR';
		$where_clause .= ' (' . implode($termArray, " $cmptype ") . ') ';
	}

	return $where_clause;
}

	$defVals = array(
		'search' => ''
	);

	if (isset($_GET['search'])) {
		$defVals['search'] = $_GET['search'];
		$terms = build_search_terms($defVals['search'], $_GET['match']);
		$sql = "
			SELECT DISTINCT 
				fp.id, 
				fp.subject, 
				fp.author, 
				fp.email, 
				IF(fp.parent_id IS NULL, fp.id, fp.parent_id) parent_id,
				UNIX_TIMESTAMP(fp.post_date) post_date,
				IF(ft.subject IS NULL, fp.subject, ft.subject) topic
			FROM
					forum_post fp
			LEFT JOIN
					forum_post ft
			ON
					fp.parent_id = ft.id
			WHERE
					fp.forum_id = {$fid} AND
					fp.approved = 1 AND
					" . build_terms_clause($terms, $_GET['date'], $_GET['in'], $_GET['match']) . " AND
					(fp.parent_id = ft.id OR fp.parent_id IS NULL)
			ORDER BY
				fp.post_date DESC
			LIMIT 101
		";
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<link rel="stylesheet" href="forum.css" type="text/css" />
<title>Forum - Zoek</title>
<script type="text/javascript">
//<![CDATA[

var _W = window, _D = document;

function trim(str) {
	return str.replace(/^\s+/,'').replace(/\s+$/,'');
}

function checkForm(F) {
	var msg = '';
	if (trim(F['search'].value) == '') {
		msg += 'Voer een zoekterm in!\n';
	}
	var len = F['in[]'].length, has_in = false;
	for (var i = 0; i < len; i++) {
		if (F['in[]'][i].checked) {
			has_in = true; break;
		}
	}
	if (!has_in)
		msg += "Selecteer tenminste ��n 'Zoek in' veld!\n";
	if (msg == '') return true;
	alert(msg);
	return false;
}

window.onload = function() {
	_D.forms[0]['search'].focus();
}

//]]>
</script>
</head>
<body>

<div class="forumHeader">
	<div style="float:right">&nbsp;</div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a>
	<?php if (isset($tid)) { ?>
	&bull; <a href="read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><?= PH::htmlspecialchars($topicProps['subject']) ?></a>
	<?php } ?>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	</div>
</div>

<h2><a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a> &raquo; Zoeken</h2>


<form action="<?= $_SERVER['PHP_SELF'] ?>" method="get" onsubmit="return checkForm(this)">
<input type="hidden" name="fid" value="<?= $fid ?>" />

<div align="center">

<fieldset style="width:70%">
<legend><b class="req">Zoekterm</b></legend>
<input type="text" name="search" size="60" maxlength="255" value="<?= PH::htmlspecialchars($defVals['search']) ?>" style="width:100%" />
</fieldset>

<fieldset style="width:70%">
<legend><b class="req">Zoek in</b></legend>
<?php
	$opts = array('author' => 'Auteur', 'subject'=>'Onderwerp', 'body'=>'Bericht');
	foreach($opts as $value=>$text) {
		if (isset($_GET['in'])) {
			$checked = in_array($value, $_GET['in']) ? ' checked="checked" ' : ' ';
		}
		else {
			$checked = in_array($value, array('subject','body')) ? ' checked="checked" ' : ' ';
		}
				print '<input' . $checked . 'type="checkbox" name="in[]" value="' . $value . '" />' . $text . '<br />';
	}
?>
</fieldset>

<fieldset style="width:70%">
<legend><b>Match</b></legend>
<select name="match">
<?php
	$opts = array(1=>'Alle woorden', 2=>'�en van de woorden', 3=>'Exacte zinsnede');
	foreach($opts as $value=>$text) {
		$selected = (isset($_GET['match']) && $_GET['match'] == $value) ? ' selected="selected" ' : ' ';
		print '<option' . $selected . 'value="' . $value . '">' . $text . '</option>';
	}
?>
</select>
</fieldset>

<fieldset style="width:70%">
<legend><b>Periode</b></legend>
<select name="date">
<?php
	$opts = array(30=>'Laatste 30 dagen', 60=>'Laatste 60 dagen', 90=>'Laatste 90 dagen', 180=>'Laatste 180 dagen', 0=>'[Alles]');
	foreach($opts as $value=>$text) {
		$selected = (isset($_GET['date']) && $_GET['date'] == $value) ? ' selected="selected" ' : ' ';
		print '<option' . $selected . 'value="' . $value . '">' . $text . '</option>';
	}
?>
</select>
</fieldset>

<p style="width:70%" align="right">
<input type="button" value="Annuleer" onclick="history.back()" />
<input type="submit" value="Zoek" />
</p>

</div>

</form>

<?php if (isset($_GET['search'])) { ?>

<table class="forumIndex" cellpadding="3" cellspacing="1">
<tr>
	<th>Discussie</th>
	<th>Onderwerp</th>
	<th>Auteur</th>
	<th>Datum</th>
</tr>
<?php
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	for ($x = 0; !$db->EndOfResult($cur) && $x < 100; $x++) {
		print '<tr class="' . ($x & 1 ? 'even' : 'odd') . '">';

		print '<td>' . PH::htmlspecialchars($db->FetchResult($cur,$x,'topic')) . '</td>';

		print '<td><a href="read.php?fid=' . $fid . '&amp;tid=' . $db->FetchResult($cur,$x,'parent_id') . '&amp;pid=' . $db->FetchResult($cur,$x,'id') . '"><b>' . PH::htmlspecialchars($db->FetchResult($cur,$x,'subject')) . '</b></a></td>';
		
		print '<td>' . getForumAuthor($db->FetchResult($cur,$x,'author'), $db->FetchResult($cur,$x,'email')) . '</td>';
		print '<td>' . date("$dateFormat $timeFormat", $db->FetchResult($cur,$x,'post_date')) . '</td>';
		print "</tr>\n";
	}

	$resTxt = '<b>' . $x . ' resultaten gevonden.</b>';
	if ($x == 0)
		$resTxt = '<b>Geen resultaten.</b>';
	elseif ($x == 1) 
		$resTxt = '<b>1 resultaat gevonden.</b>';
	if (!$db->EndOfResult($cur)) {
		$resTxt .= '<br />Er zijn meer dan 100 resultaten. Hier worden slechts de eerste 100 getoond. Verfijn uw zoekopdracht.';
	}
	?>
</table>
<?= $resTxt ?><br /><br />
<?php } ?>

<div class="forumFooter">
	<div style="float:right">&nbsp;</div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a>
	<?php if (isset($tid)) { ?>
	&bull; <a href="read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><?= PH::htmlspecialchars($topicProps['subject']) ?></a>
	<?php } ?>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	</div>
</div>

</body>
</html>
