<?php
	require_once 'forum.inc.php';
	
	$fid = (int)$_GET['fid'];
	$tid = (int)$_GET['tid'];
	$forumProps = getForumProps($fid);

	$curPage = isset($_GET['page']) ? (int)$_GET['page'] : 0;
	$perPage = $WEBprefs['forumPerPage'];
	$offset = $perPage * $curPage;
	$totalResults = 0;

	$sql = "SELECT id, author, subject, body, email, post_host, UNIX_TIMESTAMP(post_date) as post_date FROM forum_post WHERE id = $tid AND approved = 1";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$db->FetchResultAssoc($cur,$topicProps,0);

	$content = '';
	if ($curPage == 0) {
		$content .= getMessage($tid, $topicProps['subject'], $topicProps['author'], $topicProps['email'], $topicProps['post_date'], $topicProps['body']);
	}

	if (isset($_GET['pid'])) {
		$sql = "SELECT SQL_CALC_FOUND_ROWS id, author, subject, body, email, post_host, UNIX_TIMESTAMP(post_date) as post_date FROM forum_post WHERE parent_id = $tid AND id = " . (int)$_GET['pid'] . " AND approved = 1 ORDER BY id LIMIT $offset, $perPage";
	}
	else {
		$sql = "SELECT SQL_CALC_FOUND_ROWS id, author, subject, body, email, post_host, UNIX_TIMESTAMP(post_date) as post_date FROM forum_post WHERE parent_id = $tid AND approved = 1 ORDER BY id LIMIT $offset, $perPage";
	}
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	$sql = "SELECT found_rows()";
	$tmp = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($tmp)) {
		$totalResults = $db->FetchResult($tmp,0,0);
	}

	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$db->FetchResultAssoc($cur,$postProps,$x);
		$content .= getMessage($postProps['id'], $postProps['subject'], $postProps['author'], $postProps['email'], $postProps['post_date'], $postProps['body']);
	}

	if (isset($_GET['pid'])) {
		$content.= "<p><b>Deze pagina toont slechts ��n reactie uit de discussie. Klik <a href=\"{$_SERVER['PHP_SELF']}?fid={$fid}&tid={$tid}\">hier</a> voor de gehele discussie.</b></p>";
	}

	$navigation = getNavigation($totalResults);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<link rel="stylesheet" href="forum.css" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS for <?= PH::htmlspecialchars($topicProps['subject']) ?>" href="<?= str_replace('read.php', 'rss/',htmlspecialchars($_SERVER['REQUEST_URI'])) ?>" />
<title>Forum - <?= PH::htmlspecialchars($forumProps['name']) ?> - <?= PH::htmlspecialchars($topicProps['subject']) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W = window, _D = document;

<?php if (strpos($_SERVER['QUERY_STRING'], 'mod') !== false) { ?>
window.onload=function(){
	alert('Uw bericht wordt na goedkeuring door de redactie geplaatst.');
}
<?php } ?>

//]]>
</script>
</head>
<body>

<div class="forumHeader">
	<div style="float:right"><?= $navigation ?></div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	&bull; <a href="post.php?fid=<?= $fid ?>">Nieuw onderwerp</a>
	&bull; <a href="post.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>&amp;quote=0">Reageer</a>
	&bull; <a href="rss/?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><img src="pics/rss.gif" alt="RSS 2.0" width="80" height="15" border="0" style="vertical-align:middle" /></a>	</div>
</div>

<h2><a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a> &raquo; <a href="read.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><?= PH::htmlspecialchars($topicProps['subject']) ?></a></h2>

<?= $content ?>

<div class="forumFooter">
	<div style="float:right"><?= $navigation ?></div>
	<div>
	<a href="index.php">Forum index</a>
	&bull; <a href="topics.php?fid=<?= $fid ?>"><?= PH::htmlspecialchars($forumProps['name']) ?></a>
	&bull; <a href="search.php?fid=<?= $fid ?>">Zoek...</a>
	&bull; <a href="post.php?fid=<?= $fid ?>">Nieuw onderwerp</a>
	&bull; <a href="post.php?fid=<?= $fid ?>&amp;tid=<?= $tid ?>&amp;quote=0">Reageer</a>
	&bull; <a href="rss/?fid=<?= $fid ?>&amp;tid=<?= $tid ?>"><img src="pics/rss.gif" alt="RSS 2.0" width="80" height="15" border="0" style="vertical=align:middke" /></a>	</div>
</div>

</body>
</html>

