<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
?>
<html>
<head>
<title><?php echo word(420 /*Textarea eigenschappen*/) ?></title>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="jquery.h5.js"></script>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function submitMe() {
    	
    var formResult = $('form').h5Validation('validate', {formInvalidMessage: '<?php echo word(429 /*Form invalid*/) ?>'});
	if (! formResult) {
	    var msg = $('form').h5Validation('getInvalidMessages') ;
		alert(msg);
		return false;
	}
	
	var x, arg, el, element = {'type':'textarea'}, parts = [], attrs = [], args = ['label', 'required', 'info', 'name', 'cols', 'rows', 'value'];
	for(x = 0; x < args.length; x++) {
		arg = args[x];
		el = $('#F'+arg);
		if (el[0].type == 'checkbox') {
			val = el[0].checked;
		}
		else {
			val = el.val();
		}
		element[arg] = val;
	}

	parts.push('<textarea');
	if (element['name'])      { parts.push('name="'+element['name']+'"'); }
	if (element['cols'])      { parts.push('cols="'+element['cols']+'"'); }
	if (element['rows'])      { parts.push('rows="'+element['rows']+'"'); }
	if (element['maxlength']) { parts.push('maxlength="'+element['maxlength']+'"'); }
	if (element['required'])  { parts.push('required="required"'); }
	parts.push('>');
	if (element['value'])  { parts.push(element['value']); }
	parts.push('</textarea>');
	parent.setElement(parts.join(' ') ,element);
	
	parent.killPopup();
}

$(function() {
	var x, el, element = $.extend({label:'Label', required:false, info:'', name:'', cols:'', rows:'', maxlength:'', value:''}, parent.curJSON);
	
	for (x in element) {
		el = $('#F'+x);
		if (el.length == 0) continue;
		if (el[0].type == 'checkbox') {
			el.prop('checked', element[x]);
		}
		else {
			el.val(element[x]);
		}
	}

	// Focus first input element:
	$(":input[type!='button']:visible:enabled:first").focus();
});

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body>
<form name="phoundry">
<fieldset>
<legend><b><?php echo word(394 /*General Properties*/) ?></b></legend>
<table>
<tr>
	<td><?php echo word(396 /*Label*/) ?>:</td>
	<td><input class="txt" type="text" id="Flabel" size="40" required="required" /></td>
</tr>
<tr>
	<td><?php echo word(397 /*Required ?*/) ?></td>
	<td><input type="checkbox" id="Frequired" value="1" /></td>
</tr>
<tr>
	<td><?php echo word(398 /*Info*/) ?></td>
	<td><input type="text" id="Finfo" size="40" maxlength="255" /></td>
</tr>
</table>
</fieldset>
<br/>
<fieldset>
<legend><b><?php echo word(420 /*Textarea eigenschappen*/) ?></b></legend>

<table>
<tr>
	<td valign="top"><?php echo word(388 /*Name*/) ?>:</td>
	<td colspan="3">
	<input class="txt" type="text" name="Fname" id="Fname" title="<?php echo word(431 /*Name required*/) ?>" required="required" size="30" /></td>
	</td>
</tr>
<tr>
	<td><?php echo word(421 /*Columns*/) ?>:</td>
	<td><input class="txt" type="text" name="Fcols" id="Fcols" size="4" 
	title="<?php echo word(432 /*Column field only digits*/) ?>" pattern="^\d+$" value="50" /></td>
	<td><?php echo word(422 /*Rows*/) ?>:</td>
	<td><input class="txt" type="text" name="Frows" id="Frows" size="4" 
	title="<?php echo word(432 /*Row field only digits*/) ?>" pattern="^\d+$" value="8" /></td>
</tr>
<tr>
	<td><?php echo word(423 /*Wrap*/) ?>:</td>
	<td colspan="3"><select name="Fwrap" id="Fwrap">
	<option value="off"><?php echo word(424 /*Off*/) ?></option>
	<option value="soft"><?php echo word(425 /*Soft*/) ?></option>
	<option value="hard"><?php echo word(426 /*Hard*/) ?></option>
	</select></td>
</tr>
<tr>
	<td valign="top"><?php echo word(402 /*Default value*/) ?>:</td>
	<td colspan="3">
	<textarea class="txt" name="Fvalue" id="Fvalue" style="width:200px; height:50px"></textarea>
	</td>
</tr>
</table>
</fieldset>

<p>
	<input style="float:right" type="button" value="<?php echo word(413 /*Cancel*/) ?>" onclick="parent.killPopup()" />
	<input type="button" value="<?php echo word(412 /*Ok*/) ?>" onclick="submitMe()" />
</p>
</form>
</body>
</html>
