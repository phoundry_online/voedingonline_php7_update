<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
?>
<html>
<head>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="jquery.h5.js"></script>
<script type="text/javascript" src="jquery.json-2.2.min.js"></script>
<title><?php echo word(417 /*Radio Properties*/) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function setMultiple(on) {
	_D.forms['phoundry'].example.multiple = on;
}

function doOption() {
	var f = _D.forms['phoundry'];
	if(f.FoptText.value == '' || f.FoptValue.value == '') {
		alert('Enter both a name and a value!');
		f.FoptText.focus();
		return;
	}
	var opt = new Option(f.FoptText.value, f.FoptValue.value);
	if (f.actBut.value == '<?php echo word(409 /*Add*/) ?>')
		f.example.options[f.example.length] = opt;
	else
		f.example.options[f.example.selectedIndex] = opt;
	f.actBut.value = '<?php echo word(409 /*Add*/) ?>';
	f.FoptText.value = f.FoptValue.value = '';
}

function delOption() {
	var f = _D.forms['phoundry'];
	if (f.example.selectedIndex<0) return;
	f.example.options[f.example.selectedIndex] = null;
	f.FoptText.value = f.FoptValue.value = '';
	f.actBut.value = '<?php echo word(409 /*Add*/) ?>';
}

function setOption() {
	var f = _D.forms['phoundry'];
	if(!f.example.options.length)return;
	f.FoptText.value = f.example.options[f.example.selectedIndex].text;
	f.FoptValue.value = f.example.options[f.example.selectedIndex].value;
	f.actBut.value = '<? echo word(411 /*Change*/) ?>';
}

function submitMe() {
    
	var formResult = $('form').h5Validation('validate', {formInvalidMessage: '<?php echo word(429 /*Form invalid*/) ?>'});	
	if (! formResult) {
	    var msg = $('form').h5Validation('getInvalidMessages') ;
		alert(msg);
		return false;
	}
	
	var x, arg, el, element = {'type':'radio'}, html = '', parts = [], optionValue = new Object(), attrs = [], args = ['label', 'info', 'required', 'example', 'name'];
	for(x = 0; x < args.length; x++) {
		arg = args[x];
		el = $('#F'+arg);
		if (el[0].type == 'checkbox') {
			val = el[0].checked;
		}
		else {
			val = el.val();
		}
		element[arg] = val;
	}
	
	var exampleElement = $('#Fexample option');
	
	exampleElement.each(function(index, value) {
		parts.push('<input type="radio"');
		parts.push('name=""');
		parts.push('value="' + $(this).val() + '"');
		parts.push('/>&nbsp;' + $(this).text() + '<br/>');
		
		var newElement = parts.join('');
		html           = newElement;
		var option = $(this);
		optionValue[option.val()]  = option.text();
	});
	
	//Json encode example options
	element.example = $.toJSON(optionValue);
	
	parent.setElement(html , element);
	parts = [];
	
	parent.killPopup();
}

$(function() {
	var x, el, element = $.extend({label:'Label', required:false, info:'', name:'', example:''}, parent.curJSON);
	for (x in element) {
		el = $('#F'+x);
		if (el.length == 0) continue;
		if (el[0].type == 'checkbox') {
			el.prop('checked', element[x]);
		}
		else {
			el.val(element[x]);
		}
	}
    
	//Fill example select
	var optionValues   = $.parseJSON(element.example);
	if (! $.isEmptyObject(optionValues)) {
	    $.each(optionValues, function(index, value) {
	        $('#Fexample').append('<option value="' + index + '">' + value + '</option>');
	    });
	}
	
	// Focus first input element:
	$(":input[type!='button']:visible:enabled:first").focus();
});

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body>
<form name="phoundry">
<fieldset>
<legend><b><?php echo word(394 /*General Properties*/) ?></b></legend>
<table>
<tr>
	<td><?php echo word(396 /*Label*/) ?>:</td>
	<td><input class="txt" type="text" id="Flabel" size="40" title="<?php echo word(430 /*Label  required*/) ?>" required="required" /></td>
</tr>
<tr>
	<td><?php echo word(397 /*Required ?*/) ?></td>
	<td><input type="checkbox" id="Frequired" value="1" /></td>
</tr>
<tr>
	<td><?php echo word(398 /*Info*/) ?></td>
	<td><input type="text" id="Finfo" size="40" maxlength="255" /></td>
</tr>
</table>
</fieldset>

<br/>
<fieldset>
<legend><b><?php echo word(417 /*Radio Properties*/) ?></b></legend>
<table>
<tr >
	<td><b><?php echo word(388 /*Name*/) ?>:</b></td>
	<td><input class="txt" type="text" name="Fname" id="Fname" title="<?php echo word(431 /*Name required*/) ?>" required="required" size="40" /></td>
</tr>
</table>
<table><tr><td valign="top">

<table>
<tr>
	<td><b><?php echo word(406 /*Value*/) ?>:</b></td>
	<td><input class="txt" type="text" name="FoptValue" /></td>
</tr>
<tr>
	<td><b><?php echo word(407 /*Text*/) ?>:</b></td>
	<td><input class="txt" type="text" name="FoptText" /></td>
</tr>
<tr>
	<td colspan="2" align="right">
	<input name="actBut" type="button" value="<?php echo word(409 /*Add*/) ?>" onclick="doOption()" />
	<input type="button" value="<?php echo word(410 /*Delete*/) ?>" onclick="delOption()" />
	</td>
</tr>
</table>

</td><td valign="top">
<fieldset style="width:150px; height:140px">
<legend><b><?php echo word(408 /*Options*/) ?></b></legend>
<center>
<select name="example" size="1" id="Fexample" onchange="setOption()" /></select>
</center>
</fieldset>
</td>
</tr>
</table>

</fieldset>

<p>
	<input style="float:right" type="button" value="<?php echo word(413 /*Cancel*/) ?>" onclick="parent.killPopup()" />
	<input type="button" value="<?php echo word(412 /*Ok*/) ?>" onclick="submitMe()" />
</p>
</form>
</body>
</html>
