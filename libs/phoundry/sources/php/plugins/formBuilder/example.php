<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";
$TID = getTID();

if (isset($PHprefs['PHenvFunction'])) {
	eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
}

// Make sure the current user in the current role has access to this page:
checkAccess($TID, 'view');

require_once('FormBuilder.php');

$recordId = (int)$_GET['RID'];

$sql = "SELECT name, description, elements FROM form WHERE id = {$recordId}";
$cur = $db->Query($sql)
	or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

if (!$db->EndOfResult($cur)) {
    $db->FetchResultAssoc($cur, $form, 0);
    $formBuilder = new FormBuilder($form);
	$formBuilder->displayTemplate = '<table style="font-size:12px" border=0>{CONTAINER}</table>';
	$formBuilder->elementTemplate = '<tr><td>{LABEL}</td><td>{ELEMENT}</td></tr>';
}

if ('POST' == $_SERVER['REQUEST_METHOD']) {
	$formBuilder->setSaveToDb(true)
	            ->setEmailRecipient('ir_ir@hotmail.com')
				->setValues($_POST)
				//->setDisplayDefaults(true)
				->setDisplayAnswers(true)
				->process();
}

PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Form</title>
<script type="text/javascript" src="fv_english.js"></script>
<script type="text/javascript" src="fv_engine.js"></script>
<script type="text/javascript">
//<![CDATA[

//]]>
</script>
<style type="text/css">

</style>
</head>
<body>
<form action="" method="post" alt="fv" onsubmit="return _formCheck(this)">
    <?php $formBuilder->render(); ?>
</form>
</body>
</html>
