<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	$TID = getTID();

	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'view');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Phoundry Form Builder</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Form builder', word(9)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame" style="padding:0px;overflow:hidden">
<!-- Content -->

<iframe src="example.php?<?= QS(1) ?>" frameborder="0" style="width:100%;height:100%"></iframe>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['url'] ?>/core/records.php?<?= QS(1,'RID') ?>'"><?= word(46) ?></button>
	</td>
</tr></table>
</div>

</body>
</html>
