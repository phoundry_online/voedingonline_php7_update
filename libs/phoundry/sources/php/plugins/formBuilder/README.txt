CREATE TABLE form (
	id int(11) NOT NULL auto_increment,
	name varchar(80) NOT NULL default '',
	description text,
	elements text NOT NULL,
	PRIMARY KEY (id)
);

Phoundry -> Admin -> Tables -> form -> Add
Description: Form builder
Insert record URL: custom/formBuilder/edit.php
Update record URL: custom/formBuilder/edit.php
View record URL: custom/formBuilder/preview.php

Phoundry -> Admin -> Columns -> form
id -> integer -> hidden
name -> string -> text
description -> string -> textarea
elements: don't add to Phoundry!

