<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
?>
<html>
<head>
<title>Paragraph properties</title>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="jquery.h5.js"></script>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function submitMe() {
    
	var formResult = $('form').h5Validation('validate', {formInvalidMessage: '<?php echo word(429 /*Form invalid*/) ?>'});
	if (! formResult) {
	    var msg = $('form').h5Validation('getInvalidMessages') ;
		alert(msg);
		return false;
	}
	
	var x, arg, el, element = {'type':'paragraph'}, parts = [], attrs = [], args = ['value', 'label'];
	for(x = 0; x < args.length; x++) {
		arg = args[x];
		el = $('#F'+arg);
		if (el[0].type == 'checkbox') {
			val = el[0].checked;
		}
		else {
			val = el.val();
		}
		element[arg] = val;
	}

	parent.setElement('<p>' + element.value + '</p>',element);
	
	parent.killPopup();
}

$(function() {
	var x, el, element = $.extend({label:'Plain text', value:''}, parent.curJSON);
	
	for (x in element) {
		el = $('#F'+x);
		if (el.length == 0) continue;
		if (el[0].type == 'checkbox') {
			el.prop('checked', element[x]);
		}
		else {
			el.val(element[x]);
		}
	}

	// Focus first input element:
	$(":input[type!='button']:visible:enabled:first").focus();
});

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body>
<form name="phoundry">
<input type="hidden" name="Flabel" id="Flabel" />
<fieldset>
<legend><b><?php echo word(416 /*Paragraph Properties*/) ?></b></legend>

<table>
<tr>
	<td valign="top"><?php echo word(407 /*Text*/) ?>:</td>
	<td colspan="3">
	<textarea class="txt" name="Fvalue" id="Fvalue" style="width:300px; height:150px"></textarea>
	</td>
</tr>
</table>
</fieldset>

<p>
	<input style="float:right" type="button" value="<?php echo word(413 /*Cancel*/) ?>" onclick="parent.killPopup()" />
	<input type="button" value="<?php echo word(412 /*Ok*/) ?>" onclick="submitMe()" />
</p>
</form>
</body>
</html>
