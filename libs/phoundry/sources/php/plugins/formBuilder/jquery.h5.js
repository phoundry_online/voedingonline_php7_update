(function( $ ){
	
	var invalidElements   = [];
	
	var invalidMessages   = [];
	
	var options = {
	    'errorClass'          : 'error',
		'requiredMessage'     : 'This field is required.',
		'patternMessage'      : 'Field does not match the expected pattern.',
		'formInvalidMessage'  : 'Form is incomplete'
	}
	
	/**
	  *  Validator methods internally accessed by plugin.
	  *
	  */
	var validators = {

		validateRequired : function (element) {
		    var value = element.attr('value');
			if ('' == value) {
			    helpers.setNotValid(element);
			} else {
			    helpers.setValid(element);
			}
		},
		
		validatePattern   : function (element) {
		    var pattern = element.attr('pattern');
			var value   = element.attr('value');
			
			//If field is required fail on invalid pattern.
			if ('' == value && !element.attr('required')) {
			    return;
			} else {
				var regex = new RegExp(pattern);

				if (! regex.test(value)) {
					helpers.setNotValid(element);
				} else {
				    helpers.setValid(element);
				}
			}			
		},

        validateMaxlength : function (element) {
            var value = element.attr('value');
			var max   = element.attr('maxlength');
			if (value.length > max) {
			    helpers.setNotValid(element);
			} else {
			    helpers.setValid(element);
			}
        }        
		
	};
	
	/**
	  * Helper methods internally accessed by plugin.
	  *
	  */
	var helpers = {
		
		setNotValid          : function (element) {
		    element.addClass(options.errorClass);
			
			//Save invalid element.
			helpers.saveInvalid(element);
			
			//Save invalid element message.
			helpers.saveInvalidMessage(element);
			
		},
		
		setValid             : function (element) {
		    element.removeClass(options.errorClass);
		},
		
		saveInvalid          : function (element) {
		    invalidElements.push(element);
		},
		
		saveInvalidMessage   : function (element) {
		    var title    = element.attr('title');
		    if (-1 != title && '' != title) {
			    var message = title;
			} else {
			    //Try and get the message from the DOM
			    var message = options.formInvalidMessage;
			}
			
			invalidMessages.push(message);
		}
	};
	
	/**
	  * Methods accessible publicly.
	  *
	  */
    var methods = {
         init     : function( options ) { },
         validate : function( ) {   
		     
			 //Reset invalidElements
			 invalidElements = [];
			 invalidMessages = [];
			 
			 //Get elements
			 var inputs = $(':input', this);
			 $.each(inputs, function(index, value) {
			     
				 //Check for required elements
                 if ($(this).attr('required')) {
                     validators.validateRequired($(this));   
                 }
                 
				 //Check for regex pattern
                 if ($(this).attr('pattern')) {
                     validators.validatePattern($(this));   
                 }                 
				 
				 //Check for maxlength
				 var maxL = $(this).attr('maxlength');
				 if (-1 != maxL && typeof(maxL) != 'undefined') {
					 validators.validateMaxlength($(this));
				 }
				 
			 });
             
			 if (! invalidElements.length) {
			     return true;
			 } else {
			     return false;
			 } 
		 },
		 
		 getInvalidElements : function () {
		     return $.unique(invalidElements);
		 },
		 
		 getInvalidMessages : function () {
		     return $.unique(invalidMessages);;
		 },
		 
		 getFormattedMessage: function () {
		     var msg = '';
		     $.each(messages.getInvalidMessages(), function (index, value){
	             msg = msg + "\n" + value;
	         });
		 }		 
		 
    };
  
    $.fn.h5Validation = function( method, myOptions ) {    
         
		 $.extend(options, myOptions);
		 
		 // Method calling logic
         if ( methods[method] ) {
             return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
         } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
         } else {
             $.error( 'Method ' +  method + ' does not exist on jQuery.h5Validation' );
         }
    };
	
})( jQuery );
