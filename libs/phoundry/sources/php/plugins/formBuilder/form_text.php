<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
?>
<html>
<head>
<title><?php echo word(395 /*Text Field Properties*/) ?></title>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="jquery.h5.js"></script>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, element;

function submitMe() {
    
	var formResult = $('form').h5Validation('validate', {formInvalidMessage: '<?php echo word(429 /*Form invalid*/) ?>'});	
	if (! formResult) {
	    var msg = $('form').h5Validation('getInvalidMessages') ;
		alert(msg);
		return false;
	}
	
	var x, arg, el, element = {'type':'text'}, parts = [], attrs = [], args = ['label', 'required', 'info', 'name', 'size', 'maxlength', 'value', 'pattern'];
	for(x = 0; x < args.length; x++) {
		arg = args[x];
		el = $('#F'+arg);
		if (el[0].type == 'checkbox') {
			val = el[0].checked;
		}
		else {
			val = el.val();
		}
		element[arg] = val;
	}

	parts.push('<input type="text"');
	if (element['name'])      { parts.push('name="'+element['name']+'"'); }
	if (element['size'])      { parts.push('size="'+element['size']+'"'); }
	if (element['maxlength']) { parts.push('maxlength="'+element['maxlength']+'"'); }
	if (element['required'])  { parts.push('required="required"'); }
	if (element['value'])     { parts.push('value="'+element['value']+'"'); }
	if (element['pattern'])   { parts.push('pattern="'+element['pattern']+'"'); }
	parts.push('/>');
	parent.setElement(parts.join(' ') ,element);
	
	parent.killPopup();
}

$(function() {
	var x, el, element = $.extend({label:'Label', required:false, info:'', name:'', size:40, maxlength:'', value:'', pattern:''}, parent.curJSON);
	
	for (x in element) {
		el = $('#F'+x);
		if (el.length == 0) continue;
		if (el[0].type == 'checkbox') {
			el.prop('checked', element[x]);
		}
		else {
			el.val(element[x]);
		}
	}

	// Focus first input element:
	$(":input[type!='button']:visible:enabled:first").focus();
});

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body>
<form id="myForm" action="#" onsubmit="return submitMe()">
<fieldset>
<legend><b><?php echo word(394 /*General Properties*/) ?></b></legend>
<table>
<tr>
	<td><?php echo word(396 /*Label*/) ?>:</td>
	<td><input class="txt" type="text" id="Flabel" title="<?php echo word(430 /*Label required*/) ?>" size="40" required="required" /></td>
</tr>
<tr>
	<td><?php echo word(397 /*Required ?*/) ?></td>
	<td><input type="checkbox" id="Frequired" value="1" /></td>
</tr>
<tr>
	<td><?php echo word(398 /*Info ?*/) ?></td>
	<td><input type="text" id="Finfo" size="40" maxlength="255" /></td>
</tr>
</table>
</fieldset>

<fieldset>
<legend><b><?php echo word(395 /*Text Field Properties*/) ?></b></legend>
<table>
<tr>
	<td><?php echo word(388 /*Name*/) ?>:</td>
	<td><input class="txt" type="text" id="Fname" size="40" title="<?php echo word(431 /*Name required*/) ?>" required="required" pattern="^\w[\w\d]+$")/></td>
</tr>
<tr>
	<td><?php echo word(400 /*Size*/) ?>:</td>
	<td><input class="txt" type="text" id="Fsize" required="required" title="<?php echo word(427 /*Required + Numbers field required*/) ?>" size="4" pattern="^\d+$" /></td>
</tr>
<tr>
	<td><?php echo word(401 /*Max length*/) ?>:</td>
	<td><input class="txt" type="text" id="Fmaxlength" size="4" title="<?php echo word(428 /*Max length only numbers*/) ?>" pattern="^\d+$" /></td>
</tr>
<tr>
	<td><?php echo word(402 /*Default value*/) ?>:</td>
	<td><input class="txt" type="text" id="Fvalue" size="40" /></td>
</tr>
<tr>
	<td><?php echo word(404 /*Syntax*/) ?>:</td>
	<td><select id="Fpattern">
	<option value=""><?php echo word(403 /*Anything*/) ?></option>
	<option value="(^[a-z][a-z0-9]+$)">Indentifier</option>
	<option value="(^d{5}$)|(^d{5}-d{4}$)">American zipcode</option>
	<option value="(^\d{4} ?[a-z]{2}$)">Dutch zipcode</option>
	</select>
	</td>
</tr>
</table>
</fieldset>
<p>
	<input style="float:right" type="button" value="<?php echo word(413 /*Cancel*/) ?>" onclick="parent.killPopup()" />
	<input type="submit" value="<?php echo word(412 /*Ok*/) ?>" />
</p>
</form>
</body>
</html>
