<?php
/**
 * FormBuilder
 *
 * @Author Ilan Rivers
 * 
 */
class FormBuilder
{	
	/**
	 * Form action
	 *
	 */
	public $formAction = '';
	
	/**
	 * Template for the element
	 *
	 */
	public $elementTemplate = '
	<div>{INFO}</div>
	<div>{LABEL}</div>
	<div>{ELEMENT}</div>	
	';
	
	/**
	 * Display to wrap elements in.
	 *
	 */
	public $displayTemplate = '<div id="formContainer">{CONTAINER}</div>'; 
	
	/**
	 * Element spacer
	 *
	 */
	public $elementSpacer = '<div><!----></div>'; 
	
	/**
	 * Array of form elements
	 * 
	 */
	protected $_form = null;
	
	/**
	 * Email Recipient
	 *
	 */
	protected $_emailRecipient = '';
	
	/** 
	 * Set Send Email
	 *
	 */
	protected $_sendEmail = false;
	
	/**
	 * Set Save to Db
	 *
	 */
	protected $_saveToDb = false;
	
	/**
	 * Values
	 * 
	 */
	protected $_values = null;
	
	/**
	 * Display Answers
	 *
	 */
	protected $_displayAnswers = true;
	
	/**
	 * Display Defaults
	 *
	 */
	protected $_displayDefaults = false;
	
	/**
	 *  
	 *
	 */
    public function __construct(Array $form = array())
	{
	    $this->_form = $form;
	}
	
	/**
	 * Set the form
	 *
	 */
	public function setForm(array $form)
	{
	    $this->_form = $form;
		return $this;
	}
	
	/**
	 * Set Send Email
	 *
	 * @param boolean $sendEmail 
	 * @return FormBuilder
	 */
	public function setSendEmail($sendEmail)
	{
	    $this->_sendEmail = $sendEmail;
		return $this;
	}
	
	/**
	 * Set Save To Db
	 *
	 * @return FormBuilder
	 */
	public function setSaveToDb($saveToDb)
	{
	    $this->_saveToDb = $saveToDb;
		return $this;
	}
	
	/**
	 * Set Email Recipient
	 *
	 */
	public function setEmailRecipient($email)
	{
	    $this->_emailRecipient = $email;
		return $this;
	}
	
	/**
	 *  Set values for form
	 *
	 */
	public function setValues($values)
    {
        $this->_values = $values;
		return $this;
    }	
	
	/**
	 *  Set display answers
	 *
	 */
	public function setDisplayAnswers($displayAnswers)
    {
        $this->_displayAnswers = $displayAnswers;
		return $this;
    }
	
	/**
	 *  Set display defaults
	 *
	 */
	public function setDisplayDefaults($displayDefaults)
    {
        $this->_displayDefaults = $displayDefaults;
		return $this;
    }
	
	/**
	 * Process action to do.
	 *
	 * Get method to save the form information e.g Send Email or Save to DB.
	 *
	 */
	public function process()
	{		
	    if (!$this->_sendEmail && !$this->_saveToDb) {
		    throw new Exception('You must specify a save method to process the form! 
			- Use the sendEmail() or saveToDb() methods to specify the process type.');
		}
		
		if (empty($this->_values)) {
		    throw new Exception('You have not set any values for the form 
			- Use the setValues() method to set values for the form.');
		}
		
		if ($this->_sendEmail) {
		    $emailBody = $this->setDisplayAnswers(true)->render(true);
			PH::sendHTMLemail(array($this->_emailRecipient), 'Email Subject', $emailBody);
		} else if ($this->_saveToDb) {
		    global $db;
			$sql = 'INSERT INTO formPost (data) VALUES("' . escDBquote(serialize($this->_values)) . '")';
			$db->Query($sql);
		}
	}
	
	/**
	 * Render the form
	 *
	 * @param boolean $boolean
	 * @return string
	 */
	public function render($returnOutput = false)
	{
	    if (null === $this->_form) {
		    throw new Exception('You must set the form row with "setForm()" of pass the 
			    array of form elements to the constructor');
		}
		
		$elements = $this->_getFormElements();
		
		$html = '';
		foreach ($elements['elements'] as $element) {
		    
			$renderedElement = $this->_buildElement($element);
			
			if ($this->_displayAnswers) {
			    $newValue = '';
			    if (isset($element['name']) && isset($this->_values[$element['name']])) {
				    if (is_array($this->_values[$element['name']])) {
					    foreach ($this->_values[$element['name']] as $v) {
						    $newValue .= $v . '<br/>';
						}
					} else {
					    $newValue = $this->_values[$element['name']];
					}
				} else {
				    $newValue = $renderedElement;
				}

                $renderedElement = $newValue;				
			}
			
			$label = (isset($element['label'])) ? PH::htmlspecialchars($element['label']) : '';
			$info  = (isset($element['info']))  ? PH::htmlspecialchars($element['info'])   : '';
			
			$html .= str_replace(
			    array('{ELEMENT}', '{LABEL}', '{INFO}'),
				array($renderedElement, $label, $info),
				$this->elementTemplate
			);
			
			//Add spacer
			$html .= $this->elementSpacer;
		}
		
		//Wrap elements in container.
		$html = str_replace('{CONTAINER}', $html, $this->displayTemplate);
		
		if ($returnOutput) {
		    return $html;
		} else {
		    echo $html;
		}
	}
	
	/**
	 * Get form elements
	 *
	 */
	protected function _getFormElements()
	{
	    $elements = unserialize($this->_form['elements']);
		return $elements;
	}
	
	/**
	 * Build element
	 *
	 * @param $formElement array
	 * @param $value string
	 *
	 * @returns string
	 */
	protected function _buildElement(array $formElement, $defaults = null)
	{
	    $excludes = array('label', 'info', 'example'); 
	    $element  = '';		
	    
		if (isset($formElement['required']) && $formElement['required']) {
		    $formElement['required'] = 'required';
		} else {
		    unset($formElement['required']);
		}
		
		switch ($formElement['type']) {
		    
			case 'password' :
			case 'hidden'   :
			case 'text'     :
			    //Check if defaults should be set.
			    if ($this->_displayDefaults && isset($this->_values[$formElement['name']])) {
			        $formElement['value'] = $this->_values[$formElement['name']];
				}
			
                $element .= '<input type="' . $formElement['type'] . '"';
				foreach ($formElement as $key => $value) {
				    if (in_array($key, $excludes)) {
					    continue;
					}
				    $element .= ' ' . $key . '="' . $value . '"';
				}
				$element .= '/>';
				
			break;
			
			case 'radio':
			case 'checkbox':
			    
			    $options = json_decode($formElement['example']);
			    foreach ($options as $optionValue => $optionText) {
				    //Check if defaults should be set.
			        if ($this->_displayDefaults && isset($this->_values[$formElement['name']])) {
					    unset($formElement['checked']);
						if (is_array($this->_values[$formElement['name']]) && 
						    in_array($optionValue, $this->_values[$formElement['name']])) {
						    $formElement['checked'] = 'checked';
						} else if ($optionValue === $this->_values[$formElement['name']]) {
						    $formElement['checked'] = 'checked';
						}
				    }
				    $childElement    = '<input type="' . $formElement['type'] . '"';
				    foreach ($formElement as $key => $value) {
					    if (in_array($key, $excludes)) {
					        continue;
					    }
					    if ('name' == $key && 'checkbox' == $formElement['type']) {
						    $value = $value . '[]';
						}
					    $childElement .= ' ' . $key . '="' . $value . '"';
					}
					$childElement .= ' value="' . $optionValue . '"';
					$childElement .= '/>&nbsp;' . $optionText . '<br/>' ;
					$element      .= $childElement;
				}				
				
			break;
			
			case 'select':			
			    
                $options = json_decode($formElement['example']);
				$element = '<select';
				
				foreach ($formElement as $key => $value) {
				    if (in_array($key, $excludes)) {
				        continue;
				    }
				    $element .= ' ' . $key . '="' . $value . '"';
				}
					
			    foreach ($options as $optionValue => $optionText) {
				    $selected = '';
					
				    //Check if defaults should be set.
			        if ($this->_displayDefaults && isset($this->_values[$formElement['name']])) {
						if ($this->_values[$formElement['name']] == $optionValue) {
						    $selected = ' selected ';
						}
				    }
				    $element .= '<option ' . $selected . ' value="' . $optionValue . '">' . $optionText . '</option>';
				}
                
				$element .= '</select>';				
				
			break;
			
			case 'textarea':		

				$element = '<textarea';
				
				foreach ($formElement as $key => $value) {
				    if (in_array($key, $excludes) || 'value' == $key || 'type' == $key) {
				        continue;
				    }
				    $element .= ' ' . $key . '="' . $value . '"';
				}
                $element .= '>';
				//Check if defaults should be set.
			    if ($this->_displayDefaults && isset($this->_values[$formElement['name']])) {
				    $formElement['value'] = $this->_values[$formElement['name']];
				}
				$element .= $formElement['value'];
				$element .= '</textarea>';		
				
			break;
			
			case 'paragraph':
			    
				$element = '<p>' . nl2br($formElement['value']) . '</p>';
				
			break;
			
			default :
			    
				
				
			break;
		}
		
		return $element;
	}
	
	/**
	 * Render element
	 *
	 */
	public function renderElement(array $element)
	{
	    echo $this->_buildElement($element);
	}
}