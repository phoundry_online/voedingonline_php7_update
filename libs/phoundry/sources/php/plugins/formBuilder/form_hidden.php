<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
?>
<html>
<head>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="jquery.h5.js"></script>
<title><?php echo word(415 /*Verborgen veld eigenschappen*/) ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function submitMe() {
    
	var formResult = $('form').h5Validation('validate', {formInvalidMessage: '<?php echo word(429 /*Form invalid*/) ?>'});	
	if (! formResult) {
	    var msg = $('form').h5Validation('getInvalidMessages') ;
		alert(msg);
		return false;
	}
	
	var x, arg, el, element = {'type':'hidden'}, parts = [], attrs = [], args = ['label', 'value', 'name'];
	for(x = 0; x < args.length; x++) {
		arg = args[x];
		el = $('#F'+arg);
		
		if (el[0].type == 'checkbox') {
			val = el[0].checked;
		}
		else {
			val = el.val();
		}
		element[arg] = val;
	}

	parts.push('<input type="text"');
	if (element['name'])      { parts.push('name="'+element['name']+'"'); }
	parts.push('/>');
	parent.setElement(parts.join(' ') ,element);
	parent.killPopup();
}

$(function() {
	var x, el, element = $.extend({label:'Label', value:'', name:''}, parent.curJSON);
	for (x in element) {
		el = $('#F'+x);
		if (el.length == 0) continue;
		if (el[0].type == 'checkbox') {
			el.prop('checked', element[x]);
		}
		else {
			el.val(element[x]);
		}
	}

	// Focus first input element:
	$(":input[type!='button']:visible:enabled:first").focus();	
});

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body>
<form name="phoundry">
<fieldset>
<legend><b><?php echo word(394 /*General Properties*/) ?></b></legend>
<table>
<tr>
	<td><?php echo word(396 /*Label*/) ?>:</td>
	<td><input class="txt" type="text" id="Flabel" size="40" required="required" /></td>
</tr>
</table>
</fieldset>
<fieldset>
<legend><b><?php echo word(414 /*Hidden field Properties*/) ?></b></legend>
<table>
<tr>
	<td><?php echo word(388 /*Name*/) ?>:</td>
	<td><input class="txt" type="text" name="Fname" id="Fname" title="<?php echo word(431 /*Name required*/) ?>" required="required" size="40" /></td>
</tr>
<tr>
	<td><?php echo word(406 /*Value*/) ?>:</td>
	<td><input class="txt" type="text" name="Fvalue" id="Fvalue" required="required" size="40" /></td>
</tr>
</table>
</fieldset>
<p>
	<input style="float:right" type="button" value="<?php echo word(413 /*Cancel*/) ?>" onclick="parent.killPopup()" />
	<input type="button" value="<?php echo word(412 /*Ok*/) ?>" onclick="submitMe()" />
</p>
</form>
</body>
</html>
