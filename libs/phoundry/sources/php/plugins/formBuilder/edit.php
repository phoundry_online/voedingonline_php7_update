<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	$TID = getTID();

	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	if (isset($_GET['RID'])) {
		$RID = (int)$_GET['RID'];
		$page = 'update';
	}
	else {
		$page = 'insert';
	}

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, $page);

	$form_name = $form_desc = ''; $form_els = array('elements'=>array());

	if ($page == 'update') {
		$RID = (int)$_GET['RID'];
		$sql = "SELECT name, description, elements FROM form WHERE id = {$RID}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur)) {
			$form_name = $db->FetchResult($cur,0,'name');
			$form_desc = $db->FetchResult($cur,0,'description');
			$form_els = unserialize($db->FetchResult($cur,0,'elements'));
		}
	}

	$input_types = array(
		'text'		=> 'Text',
		'password'	=> 'Password',
		'hidden'    => 'Hidden',
		'select'	=> 'Select',
		'radio'		=> 'Radio',
		'checkbox'	=> 'Checkbox',
		'textarea'	=> 'Textarea',
		'paragraph' => 'Plain text'
		//'submit'	=> 'Submit button',
		//'reset'		=> 'Reset button'
	);

	$data_types = array(
		'integer'	=> 'Integer',
		'email'		=> 'Email',
		'date'		=> 'Date (DD-MM-YYYY)',
		'zipcode'	=> 'Zipcode'
	);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Phoundry Form Builder</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<style type="text/css">

.label {
	font-weight: bold;
}

.editButtons {
	display: inline;
	float: right;
	display: none;
}

ul#formDiv {
	list-style-type: none; margin: 0; padding: 0; width: 80%;
}
ul#formDiv li {
	margin: 0; padding: 0;
}

</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript" src="jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="jquery.json-2.2.min.js"></script>
<script type="text/javascript">
//<![CDATA[

function PH::htmlspecialchars(str) {
	if (typeof(str) == 'string') {
		str = str.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#039;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	}
	return str;
}

var curRow = null, curJSON = null;

function delField(el) {
	if (confirm('Zeker weten?')) {
		$(el).parents('li:first').remove();
	}
}

function editField(el) 
{
	curRow = $(el).parents('.formElement:first');
	curJSON = $.parseJSON($("input[name='elements[]']", curRow).val());
	var fieldType = curRow.data('fieldtype');
	makePopup(null,400,400,'Field type '+fieldType,'form_'+fieldType+'.php');
}

function setElement(tag, element) {
	if (!curJSON) {
		alert('Error!'); return;
	}
	// Store JSON in this element's hidden input field:
	$("input[name='elements[]']", curRow).val($.toJSON(element));

	$('.field', curRow).html(tag);
	$('.label', curRow).html(element['label']+(element['required'] ? '*' : ''));
	if (element['info']) {
		$('.miniinfo', curRow).remove();
		$('.field', curRow).prepend('<div class="miniinfo">' + element['info'] + '</div>');
	}
}

function addField(type) 
{
	var input;

	switch(type) {
		case 'text':
			input = '<input type="text" size="40" readonly="readonly" />';
			break;
		case 'password':
			input = '<input type="password" />';
			break;
		case 'textarea':
			input = '<textarea cols="40" rows="5"></textarea>';
			break;
		case 'radio':
			input = '<input type="radio" /> <input type="radio" />';
			break;
		case 'select':
			input = '<select><option>[Select]</option></select>';
			break;
		case 'checkbox':
			input = '<input type="checkbox" />';
			break;
		case 'hidden':
			input = '<input type="hidden" />';
			break;
		case 'submit':
			input = '<input type="submit" value="Submit" />';
			break;
		case 'reset':
			input = '<input type="reset" value="Reset" />';
			break;
	}

	$('#formDiv').append(
		'<li><fieldset class="formElement" data-fieldtype="'+type+'"><legend><span class="label">Label</span></legend>\
		 <input type="hidden" name="elements[]" value="{}" />\
		 <div class="editButtons">\
	    <img class="icon ptr" src="<?= $PHprefs['url'] ?>/core/icons/page_edit.png" onclick="editField(this)" />\
		 <img class="icon ptr" src="<?= $PHprefs['url'] ?>/core/icons/bin.png" onclick="delField(this)" />\
		 </div>\
		 <div class="field">' + input + '</div>\
		 </fieldset></li>'
	);

	initDrag();
}

function checkForm(F) {
	if (F['form_name'].value == '') {
		alert('Enter a name for this form!');
		F['form_name'].focus();
		return false;
	}
	/*
	var tbl = gE('formTbl'), tr, name, text, inputEls;
	if (tbl.rows.length < 2) {
		alert('Define at least one form field!');
		return false;
	}
	for (var x = 1; x < tbl.rows.length; x++) {
		tr = tbl.rows[x];
		// Label and name are required:
		inputEls = tr.getElementsByTagName('input');
		name = inputEls[2];
		text = inputEls[1];
		if (name.value == '') {
			alert('Enter a name for field ' + x + '!');
			name.focus();
			return false;
		}
		if (text.value == '') {
			alert('Enter a label for field ' + x + '!');
			text.focus();
			return false;
		}
		reqd = inputEls[3];
		if (!reqd.checked) {
			reqd.parentElement.innerHTML = '<input type="hidden" name="required[]" value="0" />';
		}
	}
	*/
	return true;
}

function initDrag() 
{
	$('.formElement').hover(
		function(){$('.editButtons', this).show()},
		function(){$('.editButtons', this).hide()}
	);
	$('#formDiv').sortable({
		cursor: 'ns-resize',
		axis: 'y'
	});
}

$(function() {
	initDrag();
});

//]]>
</script>
</head>
<body class="frames">
<form action="editRes.php?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Form builder', word($page == 'update' ? 7 : 6)); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<fieldset>
<legend><b class="req"><?php echo word(388 /* Name */) ?></b></legend>
<input type="text" name="form_name" size="60" maxlength="80" value="<?= PH::htmlspecialchars($form_name) ?>" />
</fieldset>

<fieldset>
<legend><b><?php echo word(389 /* Description */) ?></b></legend>
<textarea name="form_desc" cols="60" rows="10"><?= PH::htmlspecialchars($form_desc) ?></textarea>
</fieldset>

<fieldset>
<legend><b class="req"><?php echo word(390 /* Form Fields */) ?></b>
<select onchange="if(this.selectedIndex!=0){addField(this.value);this.selectedIndex=0}">
   <option value=""><?php echo word(392 /* Choose field */) ?></option>
   <?php foreach($input_types as $value=>$name) { print '<option value="' . $value . '">' . $name . '</option>'; } ?>
   </select>, <span style="color:blue; cursor:pointer;" onclick="makePopup(null,650,500,'<?php echo word(393 /*Example*/) ?>', 'example.php?<?= QS(1) ?>');" href=""><?php echo word(393 /*Example*/) ?></span></legend>

<ul id="formDiv">
<?php for ($x = 0; $x < count($form_els['elements']); $x++) { 
	$element = $form_els['elements'][$x];
	$parts   = array();
	$input   = '';
	
	switch($element['type']) {
	    case 'password':
	    case 'hidden':
		case 'text':
			$parts[] = '<input type="text"';
			if (isset($element['name']) && $element['name']) $parts[] = 'name="' . PH::htmlspecialchars($element['name']) . '"';
			if (isset($element['size']) && $element['size']) $parts[] = 'size="' . (int)$element['size'] . '"';
			if (isset($element['value']) && $element['value']) $parts[] = 'value="' . PH::htmlspecialchars($element['value']) . '"';
			if (isset($element['required']) && $element['required']) $parts[] = 'required="required"';
			if (isset($element['pattern']) && $element['pattern']) $parts[] = 'pattern="' . PH::htmlspecialchars($element['pattern']) . '"';
			if (isset($element['maxlength']) && $element['maxlength']) $parts[] = 'maxlength="' . (int)$element['maxlength'] . '"';
			$parts[] = '/>';
			$input = implode(' ', $parts);
		break;
		
		case 'radio':
		    
			$html    = array();
			$options = json_decode($element['example']);
			foreach ($options as $value => $text) {
			    $radio   = array();
			    $radio[] = '<input type="radio"';
			    if (isset($element['name']) && $element['name']) $radio[] = 'name="' . PH::htmlspecialchars($element['name']) . '"';
			    if (isset($element['value']) && $element['value']) $radio[] = 'value="' . PH::htmlspecialchars($value) . '"';
				$radio[] = '/>&nbsp;' . $text . '<br/>';
				$html[]  = implode(' ', $radio);
			}
			$input = implode('', $html);  
			
		break;
		
		case 'select' :
		
			$select   = array('<select');
			if (isset($element['name']) && $element['name']) $select[] = 'name="' . PH::htmlspecialchars($element['name']) . '"';
			if (isset($element['size']) && $element['size']) $select[] = 'size="' . PH::htmlspecialchars($element['size']) . '"';
			if (isset($element['mselect']) && $element['mselect']) $select[] = 'multiple ';
			$select[] = '>';
			$options  = json_decode($element['example']);
			foreach ($options as $value => $text) {
			    $select[] = '<option value="' . $value . '">' . $text . '</option>';
			}
			
			$select[] = '</select>';
			$input    = implode(' ', $select);
      
		break;
		
		case 'textarea':
		    
			$html   = array();
			$html[] = '<textarea';
			if (isset($element['name']) && $element['name']) $html[] = 'name="' . PH::htmlspecialchars($element['name']) . '"';
			if (isset($element['cols']) && $element['cols']) $html[] = 'cols="' . PH::htmlspecialchars($element['cols']) . '"';
			if (isset($element['rows']) && $element['rows']) $html[] = 'rows="' . PH::htmlspecialchars($element['rows']) . '"';
			if (isset($element['required']) && $element['required']) $html[] = 'required="required"';
			$html[] = '>';
			$html[] = $element['value'];
			$html[] = '</textarea>';
			
			$input  = implode(' ', $html);
			
		break;
        
        case 'checkbox':
            
			$html    = array();
			$options = json_decode($element['example']);
			foreach ($options as $value => $text) {
			    $checkbox   = array();
			    $checkbox[] = '<input type="checkbox"';
			    if (isset($element['name']) && $element['name']) $checkbox[] = 'name="' . PH::htmlspecialchars($element['name']) . '[]"';
			    if (isset($element['value']) && $element['value']) $checkbox[] = 'value="' . PH::htmlspecialchars($value) . '"';
				$checkbox[] = '/>&nbsp;' . $text . '<br/>';
				$html[]     = implode(' ', $checkbox);
			}
			$input = implode('', $html);
			
        break;

        case 'paragraph':
            
			$input = '<p>' . nl2br($element['value']) . '</p>';
			
        break;        
		
	}
?>
	<li><fieldset class="formElement" data-fieldtype="<?= $element['type'] ?>"><legend>
	<span class="label"><?php echo PH::htmlspecialchars($element['label']) ?>
	<?php echo (isset($element['required']) && $element['required']) ? '*' : '' ?></span></legend>
	<input type="hidden" name="elements[]" value="<?= PH::htmlspecialchars(json_encode($element)) ?>" />
	<div class="editButtons">
		<img class="icon ptr" src="<?= $PHprefs['url'] ?>/core/icons/page_edit.png" onclick="editField(this)" />
		<img class="icon ptr" src="<?= $PHprefs['url'] ?>/core/icons/bin.png" onclick="delField(this)" />
	</div>
	<?php if (isset($element['info']) && $element['info']) { print '<div class="miniinfo">' . PH::htmlspecialchars($element['info']) . '</div>'; } ?>
	<div class="field"><?= $input ?></div>
	</fieldset></li>
<?php } ?>
</ul>

</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" class="okBut" value="<?= word(82 /* Ok */) ?>" />
	</td>
	<td align="right">
	<input type="button" value="<?= word(46 /* Cancel */) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
