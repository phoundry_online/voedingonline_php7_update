<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	if (isset($_GET['RID'])) {
		$RID = (int)$_GET['RID'];
		$page = 'update';
	}
	else {
		$page = 'insert';
	}

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, $page);

	$form_name = escDBquote($_POST['form_name']);
	$form_desc = escDBquote($_POST['form_desc']);
	unset($_POST['form_name'], $_POST['form_desc']);

	$elements = array();
	foreach($_POST['elements'] as $element) {
		$elements[] = json_decode($element, true);
	}
	$_POST['elements'] = $elements;

	//echo '<pre>' . print_r($_POST, 1);exit;

	if ($page == 'update') {
		$sql = "UPDATE form SET name = '{$form_name}', description = '{$form_desc}', elements = '" . escDBquote(serialize($_POST)) . "' WHERE id = {$RID}";
	}
	else {
		$sql = "INSERT INTO form (name, description, elements) VALUES ('{$form_name}', '{$form_desc}', '" . escDBquote(serialize($_POST)) . "')";
	}
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	header('Location: ' . $PHprefs['url'] . '/core/records.php?' . QS(0, 'RID'));
?>
