<?php
	require_once getenv('CONFIG_DIR') . '/PREFS.php';
	require_once INCLUDE_DIR . 'common.php';

	class MenuItem {
		var $name, $depth, $url;
		
		function __construct($h_name, $h_depth, $h_url = '') {
			$this->name = $h_name;
			$this->depth = $h_depth;
			$this->url = $h_url;
		}
	}
	
	/**
	 * Recursively read the menu-items that a user configured himself (using the 'Phoundry Menu') feature.
	 *
	 * @author Arjan Haverkamp
	 * @param $query(string) The query to use.
	 * @param $curid(int) The id of the current menu (used for recursion).
	 * @param $depth(int) The depth of the current menu (used for recursion).
	 * @return An array of PhoundryMenuItems.
	 * @private
	 */
	function getMenu($query, $curid = '', $depth = 1) {
		global $db;

		$menu = array();
		
		$myQuery = $query;
		if ($curid != '')
			$myQuery = str_replace('parent_id IS NULL', "parent_id = $curid", $myQuery);

		$cur = mysql_query($myQuery)
			or trigger_error("Query $myQuery failed: " . mysql_error(), E_USER_ERROR);
		while($row = mysql_fetch_assoc($cur)) {
			$id = $row['id'];
			$menu[] = new MenuItem(
								$row['name'],
								$depth,
								'http://www.msn.com');
									
			$menu = array_merge($menu, getMenu($query, $id, $depth+1));
		}
		return $menu;
	}
	
	/*
	 * Translate a menustructure (array of PhoundryMenuItems) to a string useable in the Menu Tree.
	 * 
	 * @author Arjan Haverkamp
	 * @param $menuStruct(array) An array of PhoundryMenuItems representing a menu.
	 * @return A string containing Javascript commands
	 * @returns string
	 * @private
	 */
	function getJSmenuData($root, $menuStruct) {
		$idx = $root + 1; $str = '';
		$depths = array(0=>$root);
		foreach($menuStruct as $item) 
		{
			$depths[$item->depth] = $idx;
			$parent = $depths[$item->depth-1];
			$name = escSquote($item->name);
			$str .= "p.add($idx,$parent,'$name','{$item->url}');\n";
			$idx++;
		}
		return $str;
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title></title>
<style type="text/css">
<!--

/* DTREE CSS */

.DT {
	font-family: Tahoma,Futura,Helvetica,Sans-serif;
	font-size: 11px;
   color: #666;
   white-space: nowrap;
}
.DT img {
   border: 0px;
   vertical-align: middle;
}
.DT a:link, .DT a:visited, .DT a:active {
   color: #333;
   text-decoration: none;
}
.DT a.N, .DT a.NS {
   white-space: nowrap;
   padding-left: 2px;
}
.DT a.N:hover, .DT a.NS:hover {
   text-decoration: underline;
}
.DT a.NS:link, .DT a.NS:visited, .DT a.NS:active {
   background-color: #0c2e82;
   color: #fff;
}
.DT .clip {
   overflow: hidden;
}


-->
</style>
<script type="text/javascript" src="dtree.js"></script>
<script type="text/javascript">
//<![CDATA[

var imgpath = 'pics';
var _D = document;


//]]>
</script>

</head>
<body>
<h1>Menu!</h1>


<script type="text/javascript">
//<![CDATA[

p = new dTree('p');
p.config.closeSameLevel = true;
//p.config.target = 'PHcontent';
p.add(0,-1,'<b>Crossmedia menu</b>');

// Phoundry Menu
<?php
	$menu = getMenu("SELECT id, parent_id, name FROM menu WHERE parent_id IS NULL ORDER BY order_by");
	print getJSmenuData(0, $menu); 
?>

_D.write(p);

//]]>
</script>

</body>
</html>
