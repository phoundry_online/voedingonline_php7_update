//  1 = TABLES;
//  2 = FORMS;
//  4 = USECSS;
//  8 = UPLOAD;
// 16 = PAGE;
// 32 = BORDERS;
// 64 = SHOW SIZE;
// 128 = LOAD_URL;
// 256 = SKIP_REGIONS;
// PhAc_ : Phoundry AutoClean Microsoft Pasted content (INPUT checkbox)
// PhEd_ : Contenteditable (IFRAME)
// PhSz_ : Document Size indicator (TD)
// PhCv_ : Div for cleaning up Microsoft Pasted content (DIV, contenteditable = true)

var _curRegion=null,_popup,bkmrk,_POSS,_POSR,_edH=0,winArgs=[];

function PEmaxi(id) {
	var h = _D.documentElement.scrollHeight, el = _D.getElementById(id);
	if (_edH){
		el.style.height=_edH+'px';
		_edH=0;
	}
	else{
		_edH = parseInt(el.style.height,10);
		el.style.height=(h-220)+'px';
	}
}

try {
	XmlHttp=new ActiveXObject('Msxml2.XMLHTTP')
} catch(e) {
	try {
		XmlHttp=new ActiveXObject('Microsoft.XMLHTTP')
	} catch(e) {
		XmlHttp=false
	}
}

function PEinit(){
	for(var x in PEs){
		if(_curED==null)_curED=PEs[x];
		PEs[x].init()
	}
	if(_form)
		_form.attachEvent('onsubmit',PEsubmit);
	else if(PEs.length>0)
		alert('Phoundry Editor(s) could not be initialised!')
}
if(typeof noPEinit=='undefined')
	_W.attachEvent('onload',PEinit);

function PEparentTag(S,name,topElem){
	var R=S.createRange(),curEl=(S.type=='None'||S.type=='Text')?R.parentElement():R.commonParentElement();
	while(curEl&&(topElem?curEl!=topElem:curEl.tagName!='BODY')){
		if (curEl.tagName.toUpperCase()==name)
			return curEl;
		curEl=curEl.parentElement
	}
	return null
}

function PEset(el,arr){
	for(var x in arr){
		if(x=='class'){
			if(arr[x])el.className=arr[x];
			else el.removeAttribute(x+'Name',0)
		}
		else{
			if(arr[x]){try{el.attributes[x].nodeValue=arr[x]}catch(e){el.setAttribute(x,arr[x],0)}}
			else el.removeAttribute(x,0)
		}
	}
}

function PEargs(el,i){
	var ret=[],x;
	for(x=0;x<i.length;x++)
		ret[i[x]]=(i[x]=='class')?el.className:eval('el.'+i[x]);
	return ret
}

function PEgetClass(S,elem){
	var R=S.createRange(),el=(S.type=='None'||S.type=='Text')?R.parentElement():R.commonParentElement();
	while(el&&!el.className&&el.tagName!='BODY')
		el=el.parentElement;
	if(elem)
		return el.tagName=='BODY'?null:el;
	else
		return el.className?el.className:''
}

function PEsubmit() {
	var xh='',x,sum,pe;
	for(x in PEs) {
		pe=PEs[x];
		if(pe.mode!=2)
			_form[x].value=pe.mode==0?pe.src(true):pe.ed.value;
		if(pe.tidy){
			if(xh!='')xh+='|';
			sum=0;
			if(pe.feat&16)sum++;
			if(pe.tidy=='html')sum+=2;
			if(pe.tidy=='xhtml')sum+=4;
			if(pe.charset=='utf-8')sum+=8;
			xh+=x+'.'+sum
		}
	}
	if(xh)_form.action+=(_form.action.indexOf('?')==-1?'?':'&')+'_pe_eds='+escape(xh);
	return true
};

// Prototype functions

PhoundryEditor.prototype={

init:function(){
	var hd=_gE(this.name),parent=_gE('PhEd_'+this.name).parentNode,el,html,tbl;
	if(hd.form==null){
		alert('PhoundryEditor '+this.name+' is not contained in a FORM!');return
	}
	_form=hd.form;
	this.advanced = _gE('PhAv_'+this.name).checked;
	this.iframeTag = parent.innerHTML;
	this.buttonBar = _gE('bttns_'+this.name);
	this.statusBar = this.advanced?_gE('status_'+this.name):false;
	tbl = this.buttonBar.childNodes[0];
	this.buttonBar.style.height = tbl.offsetHeight+'px';
	this.buttonBar.style.width  = tbl.offsetWidth+'px';

	// Create layer/table:
	this.imgSrc=this.buttonBar.getElementsByTagName('img')[1].src;
	this.imgSrc=this.imgSrc.substring(0,this.imgSrc.lastIndexOf('/'));
	var pixel = this.imgSrc+'/pixel.gif';
	this.wysiwygButtons=this.buttonBar.innerHTML;

	this.sourceButtons = '<table cellspacing="0" cellpadding="0"><tr><td class="PEbut" style="background-position:-420px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="Jump to line..." src="'+pixel+'" onclick="_curED.line()" /></td><td class="PEbut" style="background-position:0 -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="'+PEW[41]+'..." src="'+pixel+'" onclick="_curED.find()" /></td><td class="PEbut" style="background-position:-200px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="'+PEW[55]+'" src="'+pixel+'" onclick="_curED.sbsTxt()" /></td>';
	if(_gE('tidy_'+this.name))
		this.sourceButtons += '<td class="PEbut" style="background-position:-220px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="Tidy" src="'+pixel+'" onclick="_curED.tidyMe(event,1)" /></td>';
	if(_gE('strict_'+this.name))
		this.sourceButtons += '<td class="PEbut" style="background-position:-240px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="'+this.strict+' check" src="'+pixel+'" onclick="_curED.strictHTML()" /></td>';
	if(_gE('size_'+this.name))
		this.sourceButtons += '<td class="PEbut" style="background-position:-280px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="Document size (KB)"  src="'+pixel+'" onclick="_curED.setShowSize(null)" /></td>';
	this.sourceButtons += '</tr></table>';

	this.initWYSIWYG(hd.value,0);
	if(this.feat&32)this.showBorders();
},

initWYSIWYG:function(html,focus) {
	var self=this,obj=_gE('PhEd_'+this.name),parent=obj.parentNode,R,i,j,els,cs,res,nonEditable={'TABLE':1,'COL':1,'COLGROUP':1,'TBODY':1,'TD':1,'TFOOT':1,'TH':1,'THEAD':1,'TR':1};
	this.charset=_D.charset.toLowerCase();
	cs = '<meta http-equiv="Content-Type" content="text/html; charset='+this.charset+'" />';
	parent.innerHTML=this.iframeTag; // This solves a javascript-error when hitting the back-button...
	this.regions=[];
	this.ed=_gE('PhEd_'+this.name);
	this.win=this.ed.contentWindow;
	this.doc=this.win.document;
	html=this.feat&16?(html?html:'<html><head>'+cs+'</head><body></body></html>'):'<html><head>'+cs+'</head><body'+(PEforDMD?' link="'+_D.body.link+'"':'')+'>'+html+'</body></html>';
	with(this.doc){open('text/html','replace');write(html);close()};

	res = html.match(/<meta\s+http\-equiv\s*=\s*"?content\-type"?.*content\s*=\s*"?([^"]+)"?>/i);
	if (res&&res[1]) {
		res=res[1].match(/charset\s*=\s*([a-z0-9\-]+)/i);
		if(res&&res[1]) this.charset=res[1].toLowerCase();
	}

	this.doc.onmouseup=this.doc.onkeyup=function(){formIsDirty=true;self.updMenu(self.win.event)};
	this.doc.oncontextmenu=function(){return false};
	var av = _gE('PhAc_'+this.name);
	if(this.advanced&&av&&av.checked)
		this.doc.onkeydown=function(){self.keyDown(self.win.event)};

	this.frmEls = []; this.tblEls = [];
	for(x=8;x>=0;x--){
		el=_gE('F'+_fes[x]+'_'+this.name);
		if(el)this.frmEls.push(el)
	}
	for(x=8;x>=0;x--){
		el=_gE('T'+_tes[x]+'_'+this.name);
		if(el)this.tblEls.push(el)
	}
	this.anchorBut=_gE('anchor_'+this.name);
	this.linkBut=_gE('link_'+this.name);
	this.unlinkBut=_gE('unlink_'+this.name);
	this.attachBut=_gE('attach_'+this.name);

	if (!(this.feat&256)) {
		// Are there any div/span-class-editable's?
		els = this.doc.body.getElementsByTagName('*');
		for(i=0,j=els.length;i<j;i++){
			if(!nonEditable[els[i].tagName]&&/^editable/.test(els[i].className))
				this.regions.push(els[i])
		}

		for(i in this.regions) {
			obj=this.regions[i];
			obj.runtimeStyle.borderWidth = '1px';
			obj.runtimeStyle.borderStyle = 'dotted';
			obj.runtimeStyle.borderColor = obj.className == 'editable-text' ? 'green' : 'red';
			obj.onfocus=function(){_curRegion=this};
			if (obj.className!='editable-text'){
				obj.oncontextmenu=function(){self.ctxtMenu(self.win.event);return false};
				obj.ondblclick=function(){self.dblclick(self.win.event);return false}
			}
			obj.contentEditable=true
		}
	}
	if (this.regions.length==0) {
		obj=this.doc;
		if(this.css)obj.createStyleSheet(this.css);
		obj.oncontextmenu=function(){self.ctxtMenu(self.win.event);return false};
		obj.ondblclick=function(){self.dblclick(self.win.event);return false};
		obj.body.contentEditable=true;
	}
	else
		_curRegion=this.regions[0];

	if(focus){
		R=this.doc.selection.createRange();
		R.expand('textedit');R.select();R.collapse();R.select();
		if(R.findText('##_CURSORPOSITION_##')){
			R.text='';R.select()
		}
		else if(this.doc.body.innerHTML.indexOf('##_CURSORPOSITION_##')!=-1)
			this.doc.body.innerHTML=this.doc.body.innerHTML.replace('##_CURSORPOSITION_##','');
		this.updMenu()
	}
},

toggleAdvanced:function(state){
	if (state) {
		alert(PEW[275]);
		_gE('adv_on_'+this.name).style.display='';
		_gE('adv_off_'+this.name).style.display='none';
		this.statusBar = _gE('status_'+this.name);
	}
	else {
		_gE('adv_on_'+this.name).style.display='none';
		_gE('adv_off_'+this.name).style.display='block';
		this.statusBar.innerHTML = '&nbsp;';
		this.statusBar = false;
		if(this.size)this.setShowSize(false);
	}
	PEsetCookie('PEadv',state?1:0,365);
	this.advanced = state;
},

saveCursor:function(){
	if(_curRegion)_curRegion.focus();else this.win.focus();
	_POSS=this.doc.selection;_POSR=_POSS.createRange()
},

keyDown:function(evt){
	if(evt.keyCode==86&&evt.ctrlKey){ // CTRL-V
		var S=this.doc.selection;
		if(S.type!='Control'&&confirm(PEW[269]))this.pasteMSprepare();
		this.doc.execCommand('paste');
		evt.returnValue=false
	}
},

pasteMSprepare:function(){
	var el=_gE('PhCv_'+this.name),S=this.doc.selection,R=S.createRange();
	el.innerHTML='';
	// Remove selection, it won't work otherwise...
	if(S.type=='Text'){R.collapse();R.select()}
	bkmrk=R.getBookmark();
	el.setActive()
},

pasteMS:function(){
	var el=_gE('PhCv_'+this.name),R=this.doc.body.createTextRange(),txt=el.innerHTML,els;
	R.moveToBookmark(bkmrk);R.select();
	txt=txt.replace(/<\\?xml[^>]*>/g,'').replace(/<\\?\?xml[^>]*>/ig,'').replace(/<\/?\w+:[^>]*>/ig,'').replace(/(<[^>]+) style=\"[^\"]*\"([^>]*>)/ig,'$1 $2').replace(/<(\w[^>]*) class=([^ |>]*)([^>]*)/gi,"<$1$3").replace(/<(\w[^>]*) style="([^"]*)"([^>]*)/gi,"<$1$3").replace(/<(\w[^>]*) lang=([^ |>]*)([^>]*)/gi,"<$1$3");
	el.innerHTML=txt;
	els=el.getElementsByTagName('SPAN');
	while(els.length){
		els[0].outerHTML=els[0].innerHTML;
		els=el.getElementsByTagName('SPAN')
	}
	els=el.getElementsByTagName('FONT');
	while(els.length){
		els[0].outerHTML=els[0].innerHTML;
		els=el.getElementsByTagName('FONT')
	}
	R.pasteHTML(el.innerHTML)
},

ctxtMenuItem:function(txt,elem){
	var el=_popup.document.createElement('div');
	with(el.style){width='100%';height='17px';padding='2px 0 2px 28px';cursor='default'}
	if(txt){
		el.onmouseover=function(){this.style.background='#b2b4bf'};
		el.onmouseout=function(){this.style.background='none'};
		if(!elem) {
			if(this.doc.queryCommandValue('formatblock')==txt)txt='<b>'+txt+'</b>';
			el.onclick=(txt=='Edit source')?function(){_curED.editRegionHTML();_popup.hide()}:function(){_curED.doc.execCommand('formatblock',false,this.innerText);_popup.hide()};
		}
		else {
			el.el=elem;
			el.onclick=function(){_curED.quickEdit(this.el);_popup.hide()}
		}
		el.innerHTML=txt
	}
	else {
		el.style.margin=el.style.padding=0;
		el.innerHTML='<hr size="1" style="color:#8c8a85;margin-left:24px"/>'
	}
	return el
},

ctxtMenu:function(evt){
	if(!this.advanced)return;
	var tags={'A':'Hyperlink','IMG':'Image','FORM':'Form','TD':'Cell','TR':'Row','TABLE':'Table','HR':'Ruler'},cmds=['Normal','Heading 1','Heading 2','Heading 3','Heading 4','Heading 5','Heading 6','Address','Numbered List','Bulleted List'],pb,x,nrc=0,nrt=0,ancestors=this.getAncestors(_curRegion);
	_popup=this.win.createPopup();
	pb=_popup.document.body;
	if(this.regions.length>0)cmds.push('Edit source');
	pb.oncontextmenu=function(){return false};
	with(pb.style){background='#fff url('+this.imgSrc+'/ctxt_bg.gif) repeat-y';border='1px solid #8c8a85';font='11px Tahoma'}
	for(x in ancestors){
		if(tags[ancestors[x].tagName]){
			pb.appendChild(this.ctxtMenuItem(tags[ancestors[x].tagName]+' properties',ancestors[x]));
			nrt++
		}
	}
	if(nrt)pb.appendChild(this.ctxtMenuItem());
	for(x in cmds){
		pb.appendChild(this.ctxtMenuItem(cmds[x]));
		nrc++
	}
	nrc+=(nrt)?nrt+1:0;
	_popup.show(evt.clientX+2,evt.clientY-4,130,nrc*17+2,this.doc.body)
},

setMode:function(mode){
	if(this.mode==mode)return;
	var self=this,obj=_gE('PhEd_'+this.name),parent=obj.parentNode,w=obj.style.width,h=obj.style.height,html,el;

	if (mode==0){
		// WYSIWYG
		if(this.mode==2)
			html=_form[this.name].value;
		else {
			obj.focus();
			R=_D.selection.createRange();
			R.collapse(false);
			R.text='##_CURSORPOSITION_##';
			R=/(<[^>]*?)(##_CURSORPOSITION_##)([^<]*?>)/;
			if(obj.value.search(R)!=0)
				obj.value=obj.value.replace(R,'$1$3$2');
			html=obj.value
		}
		// parent.innerHTML is set in initWYSIWYG!
		this.buttonBar.innerHTML=this.wysiwygButtons;
		this.initWYSIWYG(html,1)
	}else if(mode==1){
		// HTML source
		this.saveCursor();
		if(!_POSR.parentElement||_POSR.parentElement().tagName!='FORM'){
			if(_POSS.type=='Control')
				_POSR.item(0).insertAdjacentText('afterEnd','##_CURSORPOSITION_##');
			else {
				html=_POSR.htmlText.replace(/^\s+|\s+$/g, '');
				if(!/^<FORM/.test(html)||!/<\/FORM>$/.test(html)){
					_POSR.collapse(false);
					_POSR.pasteHTML('##_CURSORPOSITION_##')
				}
			}
		}
		html=(this.mode==0)?this.src(true):_form[this.name].value;
		if (this.tidy) html = this.tidyHTML(html)['html'];

		if(this.mode==0&&(el=_gE('borders_'+this.name))){el.className='PEbut';this.borders=false}
		parent.innerHTML='<textarea id="PhEd_'+this.name+'" style="width:'+w+';height:'+h+';border:1px solid threedshadow;font:11px Courier New,Courier;margin-top:-1px;margin-bottom:3px" onkeyup="PEs[\''+this.name+'\'].showSize()"></textarea>';
		_form[this.name].value=obj.value=_gE('PhEd_'+this.name).value=html;
		this.ed=_gE('PhEd_'+this.name);
		this.buttonBar.innerHTML=this.sourceButtons;
		if(this.statusBar)this.statusBar.innerHTML='&nbsp;';
		setTimeout(function(){var R=self.ed.createTextRange();R.collapse();if(R.findText('##_CURSORPOSITION_##')){R.text='';R.select()}},100);
	}else if (mode==2) {
		// HTML preview
		html=(this.mode==0)?this.src(true):obj.value;
		_form[this.name].value=obj.value=html;
		this.buttonBar.innerHTML='';
		if(this.statusBar)this.statusBar.innerHTML='&nbsp;';
		parent.innerHTML=this.iframeTag;
		if (!(this.feat&16)){
			var css=this.css?'<link rel="stylesheet" type="text/css" href="'+this.css+'" />':'';
			html='<html><head><title>'+PEW[4]+'</title>'+css+'<base href="'+this.base+'"/></head><body'+(PEforDMD?' link="'+_D.body.link+'"':'')+'>'+html+'</body></html>';
		}
		this.ed=_gE('PhEd_'+this.name);
		this.win=this.ed.contentWindow;
		this.doc=this.win.document;
		with(this.doc){open('text/html','replace');write(html);close()}
	}
	this.mode=mode;
	_gE('mode0_'+this.name).className=(mode==0)?'PErvo-sb':'PEovr-sb';
	try{_gE('mode1_'+this.name).className=(mode==1)?'PErvo-sb':'PEovr-sb'}catch(e){};
	try{_gE('mode2_'+this.name).className=(mode==2)?'PErvo-sb':'PEovr-sb'}catch(e){};
	//this.showSize()
},

editRegionHTML:function(){
	this.saveCursor();_POSR.select();
	var A=[];
	A['el']=this.doc.activeElement;
	A['html']=this.src(false,this.doc.activeElement);
	this.mdl('source.php',A,600,400)
},

insertHTML:function(s,e,ns){
	if(!e)e='';
	if(ns&&_POSS.type=='None')return;
	if(_POSS.type!='Control'){
		_POSR.pasteHTML(s+(_POSS.type=='None'?'':_POSR.htmlText)+e);
		_POSR.select();_POSR.moveEnd('character',1);_POSR.moveStart('character',1)
	}
	else
		_POSR.item(0).outerHTML=s+(_POSR.item(0).outerHTML)+e
},

src:function(removeRegions, region){
	var el,els,re,html,s,w,h,i,j,pathname,dirname,pagename;

	els = region ? region.getElementsByTagName('*') : this.doc.getElementsByTagName('*');

	for(i=0,j=els.length;i<j;i++){
		el=els[i];
		if(el.tagName=='TABLE'||el.tagName=='IMG'){
			// Fix IMG/TABLE widths/heights:
			s=el.getAttribute('style',0);
			w=s.width.replace(/px/,'');
			h=s.height.replace(/px/,'');
			if(w){el.setAttribute('width',w,0);s.removeAttribute('width',0)}
			if(h){el.setAttribute('height',h,0);s.removeAttribute('height',0)}
		}
		// Remove contenteditable attributes:
		if ((this.feat&16)&&removeRegions&&els[i].getAttribute('contentEditable')!==null)
			els[i].removeAttribute('contentEditable')
	}

	if(region)
		html=region.innerHTML;
	else{
		if(this.feat&16){
			html=this.doc.documentElement.outerHTML;
			html=(/<title>/i.test(html))?html.replace(/(<title>)[^<]*(<\/title>)/i,'$1'+this.doc.title+'$2'):html.replace(/(<head>)/i,'$1<title>'+this.doc.title+'</title>')
		}
		else
			html=this.doc.body.innerHTML
	}
	// Sometimes the 'removeAttribute' doesn't work, here's a hack:
	html=html.replace(/contenteditable\s*=\s*"?true"?/ig,'',html);

	/* This fucks up [script language="php"]-tags!
	if(!this.tidy)
		html=html.replace(/(?![^<]+>)\"(?![^<]+>)/g, '&quot;');
	*/
	re = new RegExp('<(t[d|r])([^<>]*?)([ \t\n\r]+)valign\s*=\s*"?center"?', 'gi');
	html=html.replace(re, '<$1$2$3valign="middle"');
	re = new RegExp('<(t[d|r])([^<>]*?)([ \t\n\r]+)align\s*=\s*"?middle"?', 'gi');
	html=html.replace(re,'<$1$2$3align="center"');

	if (!(this.feat&512)) {
		//
		// Make absolute links & images to current domain relative:
		//

		// Links to anchors:
		pathname=_D.location.pathname;
		pagename=this.base+pathname;
		pagename=pagename.replace(/([\.\\\+\*\?\[\^\]\$\(\)\{\}\=\!\|\:])/g,'\\$1').replace(/&/,'&(?:amp;)?',pagename);
		re=new RegExp('(<[^<>]*)(href)(\s*=\s*["\']?)'+pagename+'[^#"\'/>\s]*(#[^"\'/>\s]*)([^>]*)','gim');
		html=html.replace(re,'$1$2$3$4$5');
	
		// Relative links:
		dirname=this.base+pathname.substring(0,pathname.lastIndexOf('/')+1);
		dirname=dirname.replace(/([\.\\\+\*\?\[\^\]\$\(\)\{\}\=\!\|\:])/g,'\\$1');
		re=new RegExp('(<[^<>]*)(src|href|action)(\s*=\s*["\']?)'+dirname,'gim');
		html=html.replace(re,'$1$2$3');

		// Remainder absolute links:
		re=new RegExp('(<[^<>]*)(src|href|action)(\s*=\s*["\']?)'+this.base+'\/','gim');
   	html=html.replace(re,'$1$2$3/');
	}

	if(html.toLowerCase()=='<p>&nbsp;</p>')html='';
	return html.replace(/%7B/g,'{').replace(/%7C/g,'|').replace(/%7D/g, '}');
},

tidyHTML:function(html, indent){
	// encodeURIComponent translates to UTF8!!!
	var params='html='+encodeURIComponent(html)+'&charset='+this.charset+'&page='+(this.feat&16)+'&tidy='+this.tidy;
	if(indent)params+='&indent=true';

	XmlHttp.open('POST',PEdir+'/tidy/tidy.php',false);
	XmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	XmlHttp.send(params);
	var result = XmlHttp.responseText.split('+-_*^-TiDY-^*_-+');
	return {'html':result[0], 'msgs':result[1]};
},

strictHTML:function(){
	// encodeURIComponent translates to UTF8!!!
	var html=(this.mode==0)?this.src(true):this.ed.value;
	var tidy = this.tidyHTML(html, false);
	var params='html='+encodeURIComponent(tidy['html'])+'&charset='+this.charset+'&doctype='+escape(this.strict)+'&mode='+this.mode+'&page='+(this.feat&16?1:0);
	var docHTML = '<html><head><meta http-equiv="content-type" content="text/html; charset='+this.charset+'"><script>function G(L){window.opener._curED.line(L)}</script><title>W3C messages</title></head><body style="border:0;font:Icon;background:menu" onload="self.focus()">#BODY#</body></html>';
	var log=_W.open('', 'w3c', 'width=500,height=220,scrollbars=yes,left='+(_W.screenLeft+_D.documentElement.offsetWidth/2-190)+',top='+(_W.screenTop+_D.documentElement.offsetHeight/2-110));
	with(log.document){open();write(docHTML.replace('#BODY#','loading...'));close()}
	XmlHttp.open('POST',PEdir+'/strict.php',false);
	XmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	XmlHttp.send(params);
	with(log.document){open();write(docHTML.replace('#BODY#',XmlHttp.responseText + '<center><button onclick="window.close()" style="font:Icon;width:80px">Ok</button></center>'));close()}
	if(this.mode==0)
		this.initWYSIWYG(tidy['html']);
	else
		this.ed.value=tidy['html'];
},

tidyMe:function(evt,report){
	var html=(evt!=null&&this.mode==0)?this.src(true):this.ed.value;
	var tidy = this.tidyHTML(html, evt.shiftKey);

	if(this.mode==0)
		this.initWYSIWYG(tidy['html']);
	else
		this.ed.value=tidy['html'];

	if (report) {
		var msgs = tidy['msgs'];
		html = '<html><head><meta http-equiv="content-type" content="text/html; charset=' + this.charset + '"><title>Tidy messages</title>';
		html += "<script>function G(L){window.opener._curED.line(L)}</script>\n";
		html += '</head><body style="border:0;font:Icon;background:menu" onload="self.focus()">';
		if (msgs == '')
			html += '<p>No errors or warnings.</p>';
		else {
			html += '<ul style="margin-left:16px">';
			msgs = msgs.split("\n");
			for (var m = 0; m < msgs.length; m++) {
				msg = msgs[m];
				if (this.mode == 1)
					msg = msg.replace(/(line ([0-9]+))/, "<a href=\"javascript:G($2)\">$1</a>");
				html += '<li>' + msg + '</li>';
			}
		}
		html += '</ul><center><button onclick="window.close()" style="font:Icon;width:80px">Ok</button></center></body></html>';
		var log=_W.open('', 'tidy', 'width=380,height=220,scrollbars=yes,left='+(_W.screenLeft+_D.documentElement.offsetWidth/2-190)+',top='+(_W.screenTop+_D.documentElement.offsetHeight/2-110));
		with(log.document){open();write(html);close()}
	}
},

mdl:function(u,A,w,h,s){
	if(!s)s=0;
	if(A){A['loc']=_D.location;A['editor']=this;A['document']=this.win.document;if(this.classes.length)A['classes']=this.classes}
	u+=(u.indexOf('?')==-1)?'?':'&';u+='lang='+PElang+'&bg='+escape(PEbg)+'&'+(Math.random()+'').substr(8);
	if(u.charAt(0)!='/')u=PEdir+'/'+u;
	// On SP2, the dialogHeight needs to be adjusted, while 'status:0' does not
	// work anymore for untrusted sites.
	if(_W.navigator.userAgent.indexOf('SV1')!=-1)h+=20;
	winArgs = A;
	return showModalDialog(u,A,'dialogWidth:'+w+'px;dialogHeight:'+h+'px;help:0;status:0;scroll:'+s)
},

find:function() {
	var obj=this.mode==0?this.doc.body:this.ed;
	showModelessDialog(PEdir+'/find.php?bg='+escape(PEbg),obj,'dialogWidth:354px;dialogHeight:160px;help:0;status:0;scroll:0')
},

cmd:function(cmd){
	var curEl,A=[],td,el;
	this.saveCursor();
	switch(cmd) {
		case 'insrow_top':
		case 'insrow_bot':
			var row = PEparentTag(_POSS,'TR'), tbl = PEparentTag(_POSS,'TABLE'), td;
			if (row) {
				tr = tbl.insertRow(cmd=='insrow_top'?row.rowIndex:row.rowIndex+1);
				for (x = 0; x < row.cells.length; x++)
					tr.insertCell(x).setAttribute('valign','top',0);
			}
			break;
		case 'delrow':
			var row = PEparentTag(_POSS,'TR'), tbl = PEparentTag(_POSS,'TABLE');
			if(row&&tbl)
				tbl.deleteRow(row.rowIndex);
			break;
		case 'inscell':	// insert cell
			var cell = PEparentTag(_POSS,'TD'),row = PEparentTag(_POSS,'TR');
			if(cell&&row)
				row.insertCell(cell.cellIndex).setAttribute('valign','top',0);
			break;
		case 'splitcell':	// split cell
			var cell = PEparentTag(_POSS,'TD'),row = PEparentTag(_POSS,'TR'),attrs;
			if(cell&&row){
				attrs = cell.attributes;
				td = row.insertCell(cell.cellIndex+1);
				for (x = 0; x < attrs.length; x++) {
					if(attrs[x].specified)
						td.setAttribute(attrs[x].name, attrs[x].value)
				}
			}
			break;
		case 'delcell':	// delete cell
			var cell = PEparentTag(_POSS,'TD'),row = PEparentTag(_POSS,'TR');
			if(cell&&row)
				row.deleteCell(cell.cellIndex);
			break;
		case 'inscol_left':
		case 'inscol_right':
			var tbl = PEparentTag(_POSS,'TABLE'), cell = PEparentTag(_POSS,'TD');
			if (cell){
				idx = cell.cellIndex;
				for (x = 0; x < tbl.rows.length; x++)
					tbl.rows[x].insertCell(cmd=='inscol_left'?idx:idx+1).setAttribute('valign','top',0)
			}
			break;
		case 'delcol':	// delete column
			var tbl = PEparentTag(_POSS,'TABLE'), cell = PEparentTag(_POSS,'TD');
			if (cell) {
				idx = cell.cellIndex;
				for (x = 0; x < tbl.rows.length; x++)
					if(tbl.rows[x].cells.length>idx)		
						tbl.rows[x].deleteCell(idx)
			}
			break;
		case 'paste': 				
			if((el=_gE('PhAc_'+this.name))&&this.advanced&&_POSS.type!='Control'&&el.checked&&confirm(PEW[269]))this.pasteMSprepare();
			this.doc.execCommand(cmd);
			break;
		default:
			if (this.doc.queryCommandSupported(cmd))
				this.doc.execCommand(cmd)
	}
	_POSR.select(); // Restore cursor
	if(typeof noPEinit=='undefined')
		this.updMenu();
},

preview:function(popup){
	var html;
	if(this.mode==0)html=this.src(true);
	if(this.mode==1)html=this.ed.value;
	if(this.mode==2)html=_form[this.name].value;
	if (!(this.feat&16)){
		var css=this.css?'<link rel="stylesheet" type="text/css" href="' + this.css + '" />':'';
		html='<html><head><title>'+PEW[4]+'</title>'+css+'<base href="'+this.base+'"/></head><body'+(PEforDMD?' link="'+_D.body.link+'"':'')+'>'+html+'</body></html>';
	}
	if(popup){
		PEopenWin('');
		with(PEnewWin.document){open();write(html);close()}
	}
	else
		return html
},

help:function(){
	var url = PEdir+'/help.php?l='+PElang+'&t='+(this.feat&1)+'&f='+(this.feat&2)+'&p='+(this.feat&16)+'&u='+(this.imgDir==''?0:1);
	showModelessDialog(url,null,'dialogWidth:600px;dialogHeight:400px;help:0;status:0;scroll:1')
},

sbsTxt:function(){
	this.mdl('sbs_txt.php',[],300,175)
},

clean:function(){
	this.mdl('cleanup.php',[],300,200)
},

stats:function(){
	var im=this.doc.images,is=0,ds,x,m,txt=this.src(false);
	ds=txt.length;
	for (x=0;x<im.length;x++)
		is+=im[x].fileSize-0;
	m=PEW[111]+': '+ds+' bytes\n'+PEW[112]+': '+is+' bytes\n'+PEW[113]+': '+(is+ds)+' bytes\n';
	if(ds)
		m+=PEW[114]+': '+this.doc.body.innerText.split(/\s/).length+' '+PEW[115];
	alert(m)
},

showBorders:function(){
	var state=this.borders=!this.borders,tbls=this.doc.getElementsByTagName('TABLE'),cells,x,y,css,imgs=this.doc.getElementsByTagName('IMG');
	try{_gE('borders_'+this.name).className=state?'PEbutDown':'PEbut'}catch(e){};
	for (x=0;x<tbls.length;x++){
		cells = tbls[x].getElementsByTagName('TD');
		if (state && (!tbls[x].border||tbls[x].border==0)) {
			css='border:1px solid #bcbcbc';
			tbls[x].runtimeStyle.cssText=css
		}
		else if (!state) {
			css='';
			tbls[x].runtimeStyle.cssText=css
		}
		for (y=0;y<cells.length;y++)
			cells[y].runtimeStyle.cssText=css
	}
	for (x=0;x<imgs.length;x++){
		if (state && !imgs[x].border)
			imgs[x].runtimeStyle.cssText='border:1px solid #bcbcbc';
		else if (!state)
			imgs[x].runtimeStyle.cssText=''
	}
},

color:function(what){
	this.saveCursor();
	var A=[];A['what']=what;
	this.mdl('colorpick.php',A,390,210)
},

font:function(){
	this.saveCursor();
	var A=[],opts={'fontsize':0,'fontname':0,'formatblock':0},x,el;
	for (x in opts)
		A[x] = this.doc.queryCommandValue(x);
	el = PEparentTag(_POSS,'FONT');
	if (el) A['el'] = el;
	if (el && el.style && el.style.fontSize)
		A['fontsizepx'] = el.style.fontSize;
	this.mdl('font_props.php',A,365,200)
},

pageProps:function(){
	var A={dom:this.doc, imgDir:this.imgDir, upload:this.feat&8};
	this.mdl('page_props.php',A,300,290)
},

charMap:function(){
	this.saveCursor();
	this.mdl('charmap.php',[],425,350)
},

sub:function(sub){
	this.saveCursor();
	this.insertHTML(sub?'<sub>':'<sup>',sub?'</sub>':'</sup>')
},

table:function(click,el){
	this.saveCursor();
	var el,RA=[],CA=[],TA=[],R,C,P,A=[],elTD,elTR,elTBL;
	elTD=(el&&el.tagName=='TD')?el:PEparentTag(_POSS,'TD',_curRegion);
	elTR=(el&&el.tagName=='TR')?el:PEparentTag(_POSS,'TR',_curRegion);
	elTBL=(el&&el.tagName=='TABLE')?el:PEparentTag(_POSS,'TABLE',_curRegion);
	if(click)
		nt=0;
	else
		nt=(elTBL) ? confirm(PEW[261]) ? 1 : 0 : 1;
	if(!nt) {
		if(elTBL)A['elTBL'] = elTBL;
		if(elTD)A['elTD'] = elTD;
		if(elTR)A['elTR'] = elTR
	}
	if(nt&&_POSS.type!='Control'){
		TA['rows']=TA['columns']=3;
		TA['cellPadding']=TA['cellSpacing']=2;
		TA['border']=TA['nt']=1
	}else{
		if(!elTBL||elTBL.tagName!='TABLE')return;
		TA=PEargs(elTBL,['cellPadding','cellSpacing','width','height','border','bgColor','background','class']);
		if(elTBL.summary)TA['summary']=elTBL.summary;
		if(elTBL.caption){TA['caption']=elTBL.caption.innerHTML;TA['cap_pos']=elTBL.caption.align}
		TA['align']=elTBL.align;
		TA['rows']=elTBL.rows.length;
		TA['columns']=Math.round(elTBL.cells.length/elTBL.rows.length);
		R=PEparentTag(_POSS,'TR');
		if(R)RA=PEargs(R,['align','vAlign','bgColor','class']);
		C=PEparentTag(_POSS,'TD');
		if(C)CA=PEargs(C,['width','height','align','vAlign','noWrap','bgColor','background','class'])
	}
	A['imgDir']=this.imgDir;A['upload']=this.feat&8;
	A['TA'] = TA;
	A['RA'] = RA;
	A['CA'] = CA;
	A['start'] = el?el.tagName:'TABLE';
	this.mdl('table_props.php?nt='+nt,A,360,344)
},

unlink:function(){
	this.saveCursor();
	this.cmd('unlink')
},

_tempLink:function(){
	// Cursor is already saved in callee's.
	var el=PEparentTag(_POSS,'A'),ms=0,me=0;
	if (_POSS.type=='Control') {
		// Image selected
		if(_POSR.item(0).tagName!='IMG')return false
	}
	else {
		txt=_POSR.htmlText;
		// Remove spaces around selected text:
		txt=_POSR.text;
		if (txt != '') {
			while(/^\s/.test(txt)) {
				txt=txt.substr(1);
				ms++
			}
			while(/\s$/.test(txt)) {
				txt=txt.substring(0,txt.length-1);
				me--
			}
			_POSR.moveStart('character',ms);
			_POSR.moveEnd('character',me);
			_POSR.select()
		}
		txt=_POSR.htmlText;
		if (!(/^<A/i.test(txt) && /<\/A>$/i.test(txt)) && /<\/?A/i.test(txt)) {
			alert(PEW[267]);return false
		}
	}
	if(!el) {
		// Create default, empty link.
		this.doc.execCommand('CreateLink',false,'http://');
		el=PEparentTag(_POSS,'A')
	}
	return el
},

link:function(what, shift){
	this.saveCursor();
	var as,A=[],url,el;
	if (!PEparentTag(_POSS,'A'))
		A['new']=true;
	el=this._tempLink();
	if(what=='link'&&A['new']&&this.tgt)el.target=this.tgt;
	if(!el)return;
	url=this.doc.body.createTextRange();
	url.moveToElementText(el);
	url.select();
	A['el']=el;
	A['base']=this.base;
	A['outerHTML']=el.outerHTML;
	A['anchors']=[];
	A['absoluteLinks']=(this.feat&512)?true:false;
	as=this.doc.anchors;
	for(x=0;x<as.length;x++)
		A['anchors'][x]=as[x].name;
	A['linkcolor']=this.doc.body.link;
	this.mdl(what+'_props.php?shift='+shift,A,what=='link'?365:276,what=='link'?250:140)
},

image:function(){
	this.saveCursor();
	var self=this,el,A={replace:1,base:this.base,callback:function(args){self.imageFinish(args)}};
	A['el']=el=_POSS.type=='Control'?_POSR.item(0):PEparentTag(_POSS,'IMG');
	if(el) {
		A['replace'] = confirm(PEW[282] + '\n' + PEW[283])?1:0;
	}
	var url = 'file.php?mode=image&dir='+escape(this.imgDir);
	if (this.uploadMaxSize) {
		url += '&maxSize='+this.uploadMaxSize;
	}
	if (!(this.feat&8)) {
		url += '&allowFolders=false&allowFiles=false';
	}
	this.mdl(url,A,600,460)
},

imageFinish:function(attrs){
	var attr,allow=['src','align','alt','title','width','height','border','vspace','hspace','className'],x,tag='';
	if (winArgs['el']) {// Modify existing image
		// Remove existing attribs:
		for(x = 0; x < allow.length; x++) {
			winArgs['el'].removeAttribute(allow[x]);
		}
		// Add new attribs:
		for(attr in attrs) {
			winArgs['el'].setAttribute(attr == 'class' ? 'className' : attr, attrs[attr]);
		}
	}
	else {// New image
		for(attr in attrs) {
			if (tag != '') tag += ' ';
			tag += attr + '="' + attrs[attr] + '"';
		}
		this.insertHTML('<img ' + tag + ' />');
	}
},

attach:function(){
	this.saveCursor();
	var self=this, A={callback:function(args){self.attachFinish(args)}}, newLink = !PEparentTag(_POSS,'A');
	A['el']=this._tempLink();
	if(!A['el'])return;
	var url = 'file.php?mode=attachment&dir='+escape(this.attachDir);
	if (this.uploadMaxSize) {
		url += '&maxSize='+this.uploadMaxSize;
	}
	if (!(this.feat&8)) {
		url += '&allowFolders=false&allowFiles=false';
	}
	this.mdl(url,A,600,425);
	if (newLink && /^https?:\/\/$/.test(A['el'].href)) {
		this.doc.execCommand('unlink',false,null);
	}
},

attachFinish:function(attrs){
	winArgs['el'].setAttribute('href', attrs['url']);
	if (attrs['target']) {
		winArgs['el'].setAttribute('target', attrs['target']);
	}
},

hr:function(){
	this.saveCursor();
	var A=[];
	A['el']=_POSS.type=='Control'?_POSR.item(0):PEparentTag(_POSS,'HR');
	this.mdl('hr_props.php',A,264,185)
},

form:function(){
	this.saveCursor();
	var el,A=[];
	if(!(el=PEparentTag(_POSS,'FORM')))
		el=_POSR.parentElement();
	if(el.tagName=='FORM')A['el']=el;
	this.mdl('form_props.php',A,290,210)
},

formEl:function(what){
	this.saveCursor();
	var el,A=[],w=300,h=185;
	if(what=='select'){w=430;h=285}
	if(what=='textarea')h=240;
	if(what=='submit'||what=='reset')h=160;
	A['el']=el=_POSS.type=='Control'?_POSR.item(0):PEparentTag(_POSS,'INPUT');
	if(el!=null){
		if(what=='select'&&el.tagName!='SELECT')return;
		if(what!='select'&&el.type.toLowerCase()!=what)return
	}
	this.mdl('form_'+what+'.php',A,w,h)
},

setClass:function(css){
	this.saveCursor();
	var classEl=PEgetClass(_POSS,1);
	if(classEl){
		if(css==0){
			classEl.removeAttribute('className',0);
			if (classEl.tagName=='SPAN'){
				el=PEparentTag(_POSS,'SPAN');
				el.outerHTML=el.innerHTML
			}
		}
		else
			classEl.className=css
	}
	else if(css!=0){
		if(_POSS.type=='Control')
			_POSR.commonParentElement().className=css;
		else if(_POSS.type=='Text')
			this.insertHTML('<span class="'+css+'">','</span>',1)
	}
},

line:function(n){
	var r,i=0;
	if(!n)n=prompt(PEW[246],'');
	if (n&&/^[0-9]+$/.test(n)){
		r=this.ed.createTextRange();
		while(i++<n&&r.findText('\r')){
			r.collapse(0);
			r.moveEnd('textedit')
		}
		r.collapse(1);
		while(r.moveStart('word',-1)&&(!/^\r/.test(r.text)));
		if(/^\r/.test(r.text))r.moveStart('word');
		r.select()
	}
},

getAncestors:function(topElem){
	var S=this.doc.selection,A=[],R,curEl;
	try{R=S.createRange()}catch(e){return A};
	curEl=(S.type=='None'||S.type=='Text')?R.parentElement():R.commonParentElement();
	while(curEl&&(topElem?curEl!=topElem:curEl.tagName!='BODY')){
		A.push(curEl);
		curEl=curEl.parentElement
	}
	if(!topElem)A.push(this.doc.body);
	return A
},

updMenu:function(evt){
	var S=this.doc.selection,R,el,inForm,inTable,inA,inImg,x,ancestors,txt,a,cn,alt='',menuVisible,cmds,cmd;
	try{R=S.createRange()}catch(e){return};
	if(evt&&(evt.ctrlKey||evt.altKey))
		return;

	try{
		var ae=this.doc.activeElement;
		menuVisible=ae.isContentEditable&&ae.className!='editable-text';
	}
	catch(e) {
		menuVisible=true
	}
	if(menuVisible!=this.menuVisible){
		this.menuVisible=menuVisible;
		this.buttonBar.style.visibility=(menuVisible)?'visible':'hidden';
		if(!menuVisible&&this.statusBar)this.statusBar.innerHTML='&nbsp;'
	}
	if(!menuVisible)return;

	if(this.classes.length)
		_gE('class_'+this.name).value=PEgetClass(S,0);

	// Togglable buttons:
	for(x = 0; x < _cmds.length; x++) {
		try{
		_gE('m'+_cmds[x]+'_'+this.name).className = this.doc.queryCommandState(_cmds[x]) ? 'PEbutDown' : 'PEbut';
		} catch(e){};
	}

	if (!this.advanced) {
		try {
			_gE('mundo_'+this.name).className = this.doc.queryCommandEnabled('undo') ? 'PEbut' : 'PEbutNot';
			_gE('mredo_'+this.name).className = this.doc.queryCommandEnabled('redo') ? 'PEbut' : 'PEbutNot';
		} catch(e) {};
	}

	// Statusbar:
	if(this.statusBar)this.statusBar.innerHTML='&nbsp;';
	ancestors=this.getAncestors(_curRegion);
	for (x=ancestors.length;--x>=0;){
		el=ancestors[x];
		if(!el||el.tagName=='TBODY')continue;
		txt=el.tagName.toLowerCase();
		if(txt=='form')inForm=1;
		else if(txt=='table')inTable=1;
		else if(txt=='a')inA=1;
		else if(txt=='img')inImg=1;
		if(this.statusBar){
			a=_D.createElement('a');
			a.href='#';
			a.el=el;
			a.style.color='#000';
			a.onclick=function(){
				_curED.quickEdit(this.el);
				return false
			};
			a.title='';
			if(el.id)a.title+='#'+el.id;
			if(el.className)a.title+='.'+el.className;
			a.appendChild(_D.createTextNode(txt));
			this.statusBar.appendChild(a);
			if(x!=0)
				this.statusBar.appendChild(_D.createTextNode(' '+String.fromCharCode(0xbb)+' '))
		}
	}
	if(this.statusBar&&ancestors.length){
		a=_D.createElement('a');
		a.href='#';
		a.el=el;
		a.onclick=function(){
			_curED.delTag(this.el);
			return false
		};
		a.appendChild(_D.createTextNode('[del]'));
		this.statusBar.appendChild(_D.createTextNode(' '));
		this.statusBar.appendChild(a)
	}

	if(this.feat&1){
		for(x in this.tblEls)
			this.tblEls[x].className=inTable?'PEbut':'PEbutNot'
	}
	if(this.feat&2){
		for(x in this.frmEls)
			this.frmEls[x].className=inForm?'PEbut':'PEbutNot'
	}

	var sel=inImg||(S.type!='Control'&&R.htmlText.length>0);
	// Link/Anchor/Unlink buttons:
	cn=sel||inA?'PEbut':'PEbutNot';
	try{
		this.anchorBut.className=cn;
		this.linkBut.className=cn;
		this.unlinkBut.className=inA?'PEbut':'PEbutNot';
		this.attachBut.className=cn
	}catch(e){};

	// Extra buttons that need to have a selection:
	cn=sel?'PEbut':'PEbutNot';
	l=this.sels.length;
	for(x=0;x<l;x++){
		el=_gE(this.sels[x]+'_'+this.name);
		if(el)el.className=cn
	}
	this.showSize();
},

quickEdit:function(el){
	if(el.tagName!='BODY'&&el.tagName!='A'){
		var R=this.doc.body.createTextRange();
		R.moveToElementText(el);
		R.select()
	}
	switch(el.tagName) {
		case 'A':
			this.link(el.name?'anchor':'link',false); break;
		case 'IMG':
			this.image(); break;
		case 'FORM':
			this.form(); break;
		case 'TD': case 'TR': case 'TABLE':
			this.table(1,el); break;
		case 'BODY':
			if(this.feat&16) this.pageProps(); break;
		case 'SELECT':
			this.formEl('select'); break;
		case 'TEXTAREA':
			this.formEl('textarea'); break;
		case 'INPUT':
			this.formEl(el.type); break;
		case 'HR':
			this.hr(); break;
		case 'BODY':
			if((this.feat&16)&&this.regions.length==0)this.pageProps();
			break;
	}
	this.updMenu()
},

delTag:function(el){
	this.saveCursor();
	var tn=el.tagName,lis,i;
	if (/^editable/.test(el.className))return;
	if(tn!='BODY'&&tn!='TD'&&tn!='TR'&&tn!='HR'&&tn!='UL'&&tn!='OL'&&tn!='TABLE'&&tn!='IMG')
		el.outerHTML=el.innerHTML;
	else if(tn=='TABLE'||tn=='IMG'||tn=='HR')
		el.parentElement.removeChild(el);
	else if(tn=='OL'||tn=='UL') {
		lis=el.getElementsByTagName('LI');
		for(i=0;i<lis.length;i++)
			lis[i].outerHTML=lis[i].innerHTML+'<br />';
		el.outerHTML=el.innerHTML
	}
	try{_POSR.select()}catch(e){}; // Restore cursor
	this.updMenu()
},

setShowSize:function(show){
	this.size = (show == null) ? !this.size : show;
	if(!this.size)
		_gE('PhSz_'+this.name).innerHTML='';
	this.showSize()
},

showSize:function(){
	if(!this.advanced) return;
	if(this.size&&this.mode<2){
		var txt=this.mode==0?(this.feat&16?this.doc.documentElement.outerHTML:this.doc.body.innerHTML):this.ed.value;
		if(!txt)return;
		var len=txt.length;
		_gE('PhSz_'+this.name).innerHTML=len>1023?Math.round((len/1024)*100)/100+' KB':len+' bytes'
	}
},

//// Licensed version only ////

getHTML:function(){
	return this.mode==1?this.ed.value:this.src(false).replace(/\s+contenteditable="?true"?/gi, '')
},

setHTML:function(html){
	if(this.mode!=0)
		return false;
	this.initWYSIWYG(html,1);
	return true
},

setCSS:function(css){
	if(this.mode!=0||this.feat&16||css=='')
		return false;
	this.css=css;
	this.doc.createStyleSheet(this.css);
	return true
},

setClasses:function(cls){
	var el,l,x;
	if(!(this.feat&4)||this.classes.length==0)return false;
	this.classes=cls;
	el=_gE('class_'+name);
	l=el.options.length;
	for(x=2;x<l;x++)
		el.options[2]=null;
	l=cls.length;
	for(x=0;x<l;x++)
		el.options[2+x]=new Option(cls[x],cls[x]);
	return true
},

load:function(url){
	if(!(this.feat&16)){
		alert("PEload can only be called when editing a complete page (setting 'page' has value 'true')!");
		return
	}
	if(!url)
		url=prompt(PEW[211],'http://');
	if(url){
		url = PEdir+'/load_url.php?l='+PElang+'&cs='+this.charset+'&url='+escape(url);
		XmlHttp.open('GET',url,false);
		XmlHttp.send(null);
		var result = XmlHttp.responseText;
		if (/^>ERROR</.test(result)){
			alert(result); return
		}
		if(this.mode==0)
			this.initWYSIWYG(result,1);
		else
			this.ed.value=result
	}
},

spell:function(){
	var A=[];
	_rng=this.doc.body.createTextRange();
	A['range']=_rng;
	this.mdl('spell.php?step=1',A,435,200)
},

dblclick:function(event){
	this.saveCursor();

	if(event.shiftKey) {
		this.editRegionHTML();
		return;
	}

	var ok=(_POSS.type=='None'||_POSS.type=='Text'),curEl=ok?_POSR.parentElement():_POSR.item(0),href=PEparentTag(_POSS,'A');
	if(!(_POSS.type=='Control'||(_POSS.type=='Text'&&href)))return;
	if(curEl.tagName=='OBJECT'||curEl.tagName=='IFRAME')return;
		
	if(curEl.tagName=='IMG'){
		curEl=curEl.parentElement;	
		if(this.imgDir!='')
			this.image();
		return
	}

	if (this.doc.queryCommandSupported('createlink') && href){
		this.link(href.name?'anchor':'link',false);
		return
	}

	switch(curEl.tagName){
		case 'INPUT':
			if(!(this.feat&2))break;
			this.formEl(curEl.type);break;
			break;
		case 'SELECT':
			if(!(this.feat&2))break;
			this.formEl('select');break;
		case 'TEXTAREA':
			if(!(this.feat&2))break;
			this.formEl('textarea');break;
		case 'TABLE':
			if(!(this.feat&1))break;
			this.table(1,curEl);break;
		case 'HR':
			this.hr();break
	}
},
// Especially for VNU:
replaceImage:function() {
      this.saveCursor();
      var el,A=[],re,r=1;
      A['el']=el=_POSS.type=='Control'?_POSR.item(0):PEparentTag(_POSS,'IMG');
      A['imgDir']=this.imgDir;
      this.mdl('vnu_img_replace.php?maxSize='+this.uploadMaxSize,A,300,100);
   }
}
// End prototype functions
