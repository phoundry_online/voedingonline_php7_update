var winArgs=[],_curWin,_edH=0;

try {
	XmlHttp = new XMLHttpRequest();
} catch(e) {
	XmlHttp=false
}

function PEmaxi(id) {
	var h = _D.documentElement.scrollHeight, el = _D.getElementById(id);
	if (_edH){
		el.style.height=_edH+'px';
		_edH=0;
	}
	else{
		_edH = parseInt(el.style.height,10);
		el.style.height=(h-220)+'px';
	}
}

function PEinit(){
	for(var x in PEs){
		if(_curED==null)_curED=PEs[x];
		PEs[x].init()
	}
	if(_form)
		_form.addEventListener('submit',PEsubmit,true);
	else if(PEs.length>0)
		alert('Phoundry Editor(s) could not be initialised!')
}
if(typeof noPEinit=='undefined')
	_W.addEventListener('DOMContentLoaded',PEinit,true);

function PEparentTag(R,name){
	var curEl=R.commonAncestorContainer;
	if(curEl.nodeType == 3)
		curEl=curEl.parentNode;
	while(curEl&&curEl.tagName!='BODY'){
		if (curEl.tagName.toLowerCase()==name.toLowerCase())
			return curEl;
		curEl = curEl.parentNode
	}
	return null
}

function PEset(el,arr){
	for(var x in arr){
		if(x=='class'){
			if(arr[x])el.className=arr[x];
			else el.removeAttribute(x+'Name',0)
		}
		else{
			if(arr[x])el.setAttribute(x,arr[x],0);
			else el.removeAttribute(x,0)
		}
	}
}

function PEsetCSS(name,css) {
	alert(PEW[258])
}

function PEargs(el,i){
	var ret=[],x;
	for(x=0;x<i.length;x++)
		ret[i[x]]=(i[x]=='class')?el.className:el.getAttribute(i[x]);
	return ret
}

function PEgetClass(R,elem){
	var el=R.startContainer.childNodes[R.startOffset];
	if(!el)el=R.startContainer;
	if(el.nodeType==3)
		el=el.parentNode;
	while(el&&!el.className&&el.tagName!='BODY')
		el=el.parentNode;
	if (elem)
		return el.tagName=='BODY'?null:el;
	else
		return el.className?el.className:''
}

function PEsubmit() {
	var xh='',x,sum,pe;
	for(x in PEs) {
		pe=PEs[x];
		if(pe.mode!=2)
			_form[x].value=pe.mode==0?pe.src():pe.ed.value;
		if(pe.tidy){
			if(xh!='')xh+='|';
			sum=0;
			if(pe.feat&16)sum++;
			if(pe.tidy=='html')sum+=2;
			if(pe.tidy=='xhtml')sum+=4;
			if(pe.charset.toLowerCase()=='utf-8')sum+=8;
			xh+=x+'.'+sum
		}
	}
	if(xh)_form.action+=(_form.action.indexOf('?')==-1?'?':'&')+'_pe_eds='+escape(xh);
	return true
}

function PEdec2hex(dec) {
	var hc='0123456789abcdef',a=dec%16,b=(dec-a)/16;
	return ''+hc.charAt(b)+hc.charAt(a)
}

function PEsubstColor(m,r,g,b){
	return '#' + PEdec2hex(r) + PEdec2hex(g) + PEdec2hex(b)
}

// Prototype functions

PhoundryEditor.prototype={

saveCursor:function(){}, // For MSIE compatibility

getAncestors:function(isRoot){
	var R=_curWin.getSelection().getRangeAt(0),A=[];
	var curEl=(R.collapsed)?R.commonAncestorContainer:R.startContainer.childNodes[R.startOffset];
	if(curEl&&curEl.nodeType == 3)
		curEl=curEl.parentNode;
	while(curEl&&curEl.tagName!='BODY'){
		A.push(curEl);
		curEl=curEl.parentNode
	}
	if(isRoot)A.push(this.doc.body);
	return A
},


// Retrieves the HTML code from the given node.
node2html:function(root, outputRoot){
	var html='',closed,i,a,value,name;
	switch (root.nodeType) {
		case 1:
		case 11:
			if (outputRoot) {
				closed=(!(root.hasChildNodes()||(root=='script'||root=='style'||root=='div'||root=='span')));
				html='<'+root.tagName.toLowerCase();
				for(i=0;i<root.attributes.length;++i) {
					a=root.attributes.item(i);
					if(!a.specified)continue;
					name=a.name.toLowerCase();
					if(name.substr(0,4)=='_moz')continue;
					value=(name!='style')?a.value:root.style.cssText.toLowerCase();
					if(value.substr(0,4)=='_moz')continue;
					html+=' '+name+'="'+value+'"'
				}
				html+=closed?' />':'>'
			}
			for (i=root.firstChild;i;i=i.nextSibling)
				html+=this.node2html(i,true);
			if (outputRoot&&!closed)
				html+='</'+root.tagName.toLowerCase()+'>';
			break;
		case 3:
			html=root.data.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/\"/g,'&quot;');
			break;
		 case 8:
			html='<!--'+root.data+'-->';
			break
	}
	return html
},

pasteHTML:function(html){
	var sel=_curWin.getSelection(),R=sel.getRangeAt(0);
	R.deleteContents();
	var container=R.startContainer,pos=R.startOffset,afterNode;
	if (container.nodeType==3) {
		var textNode = container;
		container = textNode.parentNode;
		var text = textNode.nodeValue;
		var textBefore = text.substr(0,pos);
		var textAfter = text.substr(pos);
		var beforeNode = this.doc.createTextNode(textBefore);
		afterNode = this.doc.createTextNode(textAfter);
		container.insertBefore(afterNode, textNode);
		container.insertBefore(beforeNode, afterNode);
		container.removeChild(textNode)
	} else
		afterNode = container.childNodes[pos];
   var fragment = R.createContextualFragment(html);
  	while (fragment.firstChild)
		container.insertBefore(fragment.firstChild, afterNode)
},

init:function(){
	var hd=_gE(this.name),parent=_gE('PhEd_'+this.name).parentNode,tbl;
	if(hd.form==null){
		alert('PhoundryEditor '+this.name+' is not contained in a FORM!');return
	}
	_form=hd.form;
	this.iframeTag = parent.innerHTML;
	this.buttonBar = _gE('bttns_'+this.name);
	this.statusBar = _gE('status_'+this.name);
	tbl = this.buttonBar.childNodes[1];
	this.buttonBar.style.height = tbl.clientHeight+'px';
	this.buttonBar.style.width  = tbl.clientWidth+'px';

	// Create layer/table:
	this.imgSrc=this.buttonBar.getElementsByTagName('img')[1].src;
	this.imgSrc=this.imgSrc.substring(0,this.imgSrc.lastIndexOf('/'));
	var pixel = this.imgSrc+'/pixel.gif';
	this.wysiwygButtons = this.buttonBar.innerHTML;

	this.sourceButtons = '<table cellspacing="0" cellpadding="0"><tr><td class="PEbut" style="background-position:0 -20px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="'+PEW[41]+'..." src="'+pixel+'" onclick="_curED.find()" /></td><td class="PEbut" style="background-position:-200px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="'+PEW[55]+'" src="'+pixel+'" onclick="_curED.sbsTxt()" /></td>';
	if(_gE('tidy_'+this.name))
		this.sourceButtons += '<td class="PEbut" style="background-position:-220px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="Tidy" src="'+pixel+'" onclick="_curED.tidyMe(event,1)" /></td>';
	if(_gE('strict_'+this.name))
		this.sourceButtons += '<td class="PEbut" style="background-position:-240px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="'+this.strict+' check" src="'+pixel+'" onclick="_curED.strictHTML()" /></td>';
	if(_gE('size_'+this.name))
		this.sourceButtons += '<td class="PEbut" style="background-position:-280px -40px" onmouseover="_E(this)" onmouseout="_E(this)"><img class="PEimg" alt="Document size (KB)"  src="'+pixel+'" onclick="_curED.setShowSize(null)" /></td>';
	this.sourceButtons += '</tr></table>';

	var btns=this.buttonBar.getElementsByTagName('img');
	for (b in btns) {
		if (btns[b].alt)btns[b].title=btns[b].alt
	}

	this.initWYSIWYG(hd.value)
},

initWYSIWYG:function(html,focus){
	var self=this,i,j,x,css,el,style,id=0,nonEditable={'TABLE':1,'COL':1,'COLGROUP':1,'TBODY':1,'TD':1,'TFOOT':1,'TH':1,'THEAD':1,'TR':1},cssRegExp=new RegExp('^(background|border|color|font|line|margin|padding|textAlign)'),cs='<meta http-equiv="Content-type" content="text/html; charset='+_D.characterSet+'" />',parent=_gE('PhEd_'+this.name).parentNode;
	this.charset=_D.characterSet;
	cs = '<meta http-equiv="Content-Type" content="text/html; charset='+this.charset+'" />';
	html=this.feat&16?(html?html:'<html><head>'+cs+'</head><body bgcolor="white"></body></html>'):'<html><head>'+cs+'</head><body bgcolor="white"'+(PEforDMD?' link="'+_D.body.link+'"':'')+'>'+html+'</body></html>';
	parent.innerHTML=this.iframeTag; // This solves weird javascript-errors when reloading the page

	this.regions=[];
	this.ed=_gE('PhEd_'+this.name);
	this.win=_curWin=this.ed.contentWindow;
	this.doc=this.win.document;
	this.win.addEventListener('load',function(){self.initIframes()},true);
	with(this.doc){open();write(html);close()}
	
	res = html.match(/<meta\s+http\-equiv\s*=\s*"?content\-type"?.*content\s*=\s*"?([^"]+)"?>/i);
	if (res&&res[1]) {
		res=res[1].match(/charset\s*=\s*([a-z0-9\-]+)/i);
		if(res&&res[1]) this.charset=res[1];
	}

	this.win.HTMLElement.prototype.outerHTML getter=function()
	{
		var E = '|IMG|BR|INPUT|META|LINK|PARAM|HR',tn=this.tagName.toLowerCase(),A=this.attributes,str='<'+tn,i;
		for(i=0;i<A.length;i++)
			str+=' '+A[i].name+'="'+A[i].value+'"';
		if(E.indexOf('|'+this.tagName)!=-1)
			return str+'>';
		return str+'>'+this.innerHTML.replace(/%7B/g,'{').replace(/%7C/g,'|').replace(/%7D/g,'}')+'</'+tn+'>'
	};
	this.win.HTMLElement.prototype.outerHTML setter=function(str)
	{
		var r = this.ownerDocument.createRange();
		r.setStartBefore(this);
		var df = r.createContextualFragment(str);
		this.parentNode.replaceChild(df,this);
		return str
	};

	this.frmEls = []; this.tblEls = [];
	for(x=8;x>=0;x--){
		el=_gE('F'+_fes[x]+'_'+this.name);
		if(el)this.frmEls.push(el)
	}
	for(x=8;x>=0;x--){
		el=_gE('T'+_tes[x]+'_'+this.name);
		if(el)this.tblEls.push(el)
	}
	this.anchorBut=_gE('anchor_'+this.name);
	this.linkBut=_gE('link_'+this.name);
	this.unlinkBut=_gE('unlink_'+this.name);
	this.attachBut=_gE('attach_'+this.name);

	if (!(this.feat&256)) {
		// Are there any div/span-class-editable's?
		els = this.doc.getElementsByTagName('*');
		for(i=0,j=els.length;i<j;i++){
			if(!nonEditable[els[i].tagName]&&/^editable/.test(els[i].className)) {
				css = [];
				style = this.win.getComputedStyle(els[i],'');
				for(x in style) {
					if (x && style[x] && style[x]!='none' && cssRegExp.test(x)) {
						if (x == 'fontWeight' && style[x] == '401')css[x] = 'bold';else css[x] = style[x]
					}
				}
				this.regions.push({'element':els[i], 'id':'region'+id++, 'width':els[i].offsetWidth, 'height':els[i].offsetHeight, 'html':els[i].innerHTML, 'css':css, 'isBlockElement':getComputedStyle(els[i],'')['display'] == 'block', 'frame':null})
			}
		}

		// This needs a second loop: we're changing the DOM with innerHTML!
		for (i in this.regions) {
			el=this.regions[i].element;
			el.style.borderWidth='1px';
			el.style.borderStyle='dotted';
			el.style.borderColor=el.className=='editable-text'?'green':'red';
			el.innerHTML='<iframe id="region'+i+'" src="'+PEdir+'/blank.html" width="'+(this.regions[i].isBlockElement?'100%':this.regions[i].width)+'" height="'+this.regions[i].height+'" frameborder="0" style="overflow:hidden"></iframe>';
		}
	}

	this.win.addEventListener('contextmenu',function(event){event.preventDefault();return false},true);
	if(this.regions.length==0){
		if(this.css) {
			var link = this.doc.createElement('link'), head = this.doc.getElementsByTagName('head')[0];
			link.href = this.css;
			link.type = 'text/css';
			link.rel = 'stylesheet';
			if (head) head.appendChild(link);
		}
		this.win.name='rootWin';
		this.win.addEventListener('focus',function(){_curWin=this},true);
		this.win.addEventListener('keypress',function(event){formIsDirty=true;self.keyPress(event)},true);
		this.win.addEventListener('mouseup',function(event){self.updMenu(event)},true);
		this.win.addEventListener('dblclick',function(event){self.dblclick(event)},true);
		this.doc.designMode='on';
		this.doc.execCommand('styleWithCSS',false,false);
		if(focus){this.win.focus();setTimeout(function(){self.updMenu()},100)}
	}
},

initIframes:function(){
	var i, j, region, win, self = this;
	for (i in this.regions){
		region=this.regions[i];
		region.frame=this.doc.getElementById(region.id);
		if(!region.frame)continue;
		win = region.frame.contentWindow;
		win.region=region;

		win.HTMLElement.prototype.outerHTML getter=function()
		{
			var E = '|IMG|BR|INPUT|META|LINK|PARAM|HR',tn=this.tagName.toLowerCase(),A=this.attributes,str='<'+tn,i;
			for(i=0;i<A.length;i++)
				str+=' '+A[i].name+'="'+A[i].value+'"';
			if(E.indexOf('|'+this.tagName)!=-1)
				return str+'>';
			return str+'>'+this.innerHTML.replace(/%7B/g,'{').replace(/%7C/g,'|').replace(/%7D/g,'}')+'</'+tn+'>'
		};

		win.HTMLElement.prototype.outerHTML setter=function(str)
		{
			var r = this.ownerDocument.createRange();
			r.setStartBefore(this);
			var df = r.createContextualFragment(str);
			this.parentNode.replaceChild(df, this);
			return str
		};

		// Set styles:
		for (j in this.regions[i].css)
			win.document.body.style[j]=this.regions[i].css[j];
		win.document.body.style.marginTop=0;
		if(!region.isBlockElement)
			win.document.body.style.whiteSpace='nowrap';

		win.addEventListener('contextmenu',function(event){event.stopPropagation();return false},true);
		win.addEventListener('focus',function(){_curWin=this},true);
		win.addEventListener('mouseup',function(event){self.updMenu(event)},true);
		win.addEventListener('keypress',function(event){formIsDirty=true;self.keyPress(event)},true);
		
		if(region.element.className!='editable-text')
			win.document.addEventListener('dblclick',function(event){self.dblclick(event)},true);

		win.document.body.innerHTML=region.html;
		region.frame.height = win.document.body.offsetHeight;
		win.document.designMode='on';
		win.document.execCommand('styleWithCSS',false,false)
	}
},

setMode:function(mode){
	if(this.mode==mode)return;
	var self=this,obj=_gE('PhEd_'+this.name),parent=obj.parentNode,w=obj.style.width,h=obj.style.height,html;

	if (mode==0){
		// WYSIWYG
		html=(this.mode==2)?_form[this.name].value:obj.value;
		// parent.innerHTML is set in initWYSIWYG!
		this.buttonBar.innerHTML=this.wysiwygButtons;
		this.initWYSIWYG(html,1);
	}else if(mode==1){
		// HTML source
		html=(this.mode==0)?this.src():_form[this.name].value;
		if (this.tidy) html = this.tidyHTML(html)['html'];
		parent.innerHTML='<textarea id="PhEd_'+this.name+'" style="width:'+w+';height:'+h+';border:1px solid threedshadow;font:11px Courier New,Courier;margin-top:-1px;margin-bottom:3px" onkeyup="PEs[\''+this.name+'\'].showSize()"></textarea>';
		_form[this.name].value=obj.value=_gE('PhEd_'+this.name).value=html;
		this.ed=_gE('PhEd_'+this.name);
		this.buttonBar.innerHTML=this.sourceButtons;
		if(this.statusBar)this.statusBar.innerHTML='&nbsp;';
		this.ed.focus();
	}else if (mode==2) {
		// HTML preview
		html=(this.mode==0)?this.src():obj.value;
		_form[this.name].value=obj.value=html;
		this.buttonBar.innerHTML='';
		if(this.statusBar)this.statusBar.innerHTML='&nbsp;';
		parent.innerHTML=this.iframeTag;
		if (!(this.feat&16)){
			var css=this.css?'<link rel="stylesheet" type="text/css" href="'+this.css+'" />':'';
			html='<html><head><title>'+PEW[4]+'</title>'+css+'<base href="'+this.base+'"/></head><body bgcolor="white"'+(PEforDMD?' link="'+_D.body.link+'"':'')+'>'+html+'</body></html>';
		}
		this.ed=_gE('PhEd_'+this.name);
		this.win=this.ed.contentWindow;
		this.doc=this.win.document;
		with(this.doc){open('text/html','replace');write(html);close()}
	}
	this.mode=mode;
	_gE('mode0_'+this.name).className=(mode==0)?'PErvo-sb':'PEovr-sb';
	try{_gE('mode1_'+this.name).className=(mode==1)?'PErvo-sb':'PEovr-sb'}catch(e){}
	try{_gE('mode2_'+this.name).className=(mode==2)?'PErvo-sb':'PEovr-sb'}catch(e){}
	this.showSize()
},

editRegionHTML:function(){
	var A=[];
	A['el']=_curWin.document.body;
	A['html']=_curWin.document.body.innerHTML;
	this.mdl('source.php',A,600,400)
},

insertHTML:function(s,e,ns){
	if(!e)e='';
	var R=_curWin.getSelection().getRangeAt(0),el;
	if (ns&&R.collapsed) return; // No selection!
	el=R.startContainer.childNodes[R.startOffset];
	if(el)
		this.pasteHTML(s+el.outerHTML+e);
	else
		this.pasteHTML(s+R.toString()+e);
	this.updMenu();
	_curWin.focus()
},

src:function(){
	var txt,html,s,i,a,F=[];

	for(i in this.regions) {
		region = this.regions[i];
		region.element.style.removeProperty('border');
		if(region.element.style.cssText=='')
			region.element.removeAttribute('style');
		if(!region.frame) continue;
		region.element.innerHTML = region.frame.contentWindow.document.body.innerHTML
	}

	if(this.feat&16){
		txt=this.doc.documentElement.outerHTML;
		txt=(/<title>/i.test(txt))?txt.replace(/(<title>)[^<]*(<\/title>)/i,'$1'+this.doc.title+'$2'):txt.replace(/(<head>)/i,'$1<title>'+this.doc.title+'</title>')
	}
	else
		txt = this.doc.body.innerHTML;

	/* This fucks up [script language="php"]-tags!
	if(!this.tidy)
		txt=txt.replace(/(?![^<]+>)\"(?![^<]+>)/g,'&quot;');
	*/
	txt=txt.replace(/%7B/g,'{').replace(/%7C/g,'|').replace(/%7D/g,'}');
	if(txt.length < 65536) txt=txt.replace(/(?![^>]+<)rgb\(([0-9]+),\s*([0-9]+),\s*([0-9]+)\)(?![^>]+<)/g,PEsubstColor);

	if (txt.replace(/\s+$/,'').toLowerCase()=='<br>')txt='';
	return txt
},

tidyHTML:function(html, indent){
	// encodeURIComponent translates to UTF8!!!
	var params='html='+encodeURIComponent(html)+'&charset='+this.charset+'&page='+(this.feat&16)+'&tidy='+this.tidy;
	if(indent)params+='&indent=true';

	XmlHttp.open('POST',PEdir+'/tidy/tidy.php',false);
	XmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	XmlHttp.send(params);
	var result = XmlHttp.responseText.split('+-_*^-TiDY-^*_-+');
	return {'html':result[0], 'msgs':result[1]};
},


strictHTML:function(){
	// encodeURIComponent translates to UTF8!!!
	var html=(this.mode==0)?this.src(true):this.ed.value;
	var tidy = this.tidyHTML(html, false);
	var params='html='+encodeURIComponent(tidy['html'])+'&charset='+this.charset+'&doctype='+escape(this.strict)+'&mode='+this.mode+'&page='+(this.feat&16?1:0);
	var docHTML = '<html><head><meta http-equiv="content-type" content="text/html; charset='+this.charset+'"><script>function G(L){}</script><title>W3C messages</title></head><body style="border:0;font:Icon;background:menu" onload="self.focus()">#BODY#</body></html>';
	var log=_W.open('', 'w3c', 'width=500,height=220,scrollbars=yes,left='+(_W.screenLeft+_D.documentElement.offsetWidth/2-190)+',top='+(_W.screenTop+_D.documentElement.offsetHeight/2-110));
	with(log.document){open();writeln(docHTML.replace('#BODY#','loading...'));close()}
	XmlHttp.open('POST',PEdir+'/strict.php',false);
	XmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	XmlHttp.send(params);
	with(log.document){open();write(docHTML.replace('#BODY#',XmlHttp.responseText + '<center><button onclick="window.close()" style="font:Icon;width:80px">Ok</button></center>'));close()}
	if(this.mode==0)
		this.initWYSIWYG(tidy['html']);
	else
		this.ed.value=tidy['html'];
},

tidyMe:function(evt,report){
	var html=(evt!=null&&this.mode==0)?this.src(true):this.ed.value;
	var tidy = this.tidyHTML(html, evt.shiftKey);

	if(this.mode==0)
		this.initWYSIWYG(tidy['html']);
	else
		this.ed.value=tidy['html'];

	if (report) {
		var msgs = tidy['msgs'];
		html = '<html><head><meta http-equiv="content-type" content="text/html; charset=' + this.charset + '"><title>Tidy messages</title>';
		html += "<script>function G(L){window.opener._curED.line(L)}</script>\n";
		html += '</head><body style="border:0;font:Icon;background:menu" onload="self.focus()">';
		if (msgs == '')
			html += '<p>No errors or warnings.</p>';
		else {
			html += '<ul style="padding-left:12px">';
			msgs = msgs.split("\n");
			for (var m = 0; m < msgs.length; m++)
				html += '<li>' + msgs[m] + '</li>';
		}
		html += '</ul><center><button onclick="window.close()" style="font:Icon;width:80px">Ok</button></center></body></html>';
		var log=_W.open('', 'tidy', 'width=380,height=220,scrollbars=yes,left='+((_W.outerWidth-380)/2+_W.screenX)+',top='+((_W.outerHeight-220)/2+_W.screenY));
		with(log.document){open();write(html);close()}
	}
},

mdl:function(u,A,w,h,s){
	s=(!s)?'no':'yes';
	if(A){A['loc']=_D.location.href;A['editor']=this;A['document']=_curWin.document}
	u+=(u.indexOf('?')==-1)?'?':'&';u+='lang='+PElang+'&bg='+escape(PEbg)+'&'+(Math.random()+'').substr(8);
	winArgs = A;
	if(u.charAt(0)!='/')u=PEdir+'/'+u;
	_W.open(u,'PEW','modal=yes,resizable=yes,scrollbars='+s+',width='+w+',height='+h+',left='+((_W.outerWidth-w)/2+_W.screenX)+',top='+((_W.outerHeight-h)/2+_W.screenY))
},

find:function(){
	var A=[];
	A['obj']=this.mode==0?this.doc.body:this.ed;
	if(this.mode==0)
		A['sel']=_curWin.getSelection();
	this.mdl('find-moz.php',A,360,110)
},

cmd:function(nr){
	var x,e,A=[],td,R=_curWin.getSelection().getRangeAt(0);
	switch(nr) {
		case 'insrow_top':
		case 'insrow_bot':
			var row = PEparentTag(R,'TR'), tbl = PEparentTag(R,'TABLE'), td;
			if (row) {
				tr = tbl.insertRow(nr=='insrow_top'?row.rowIndex:row.rowIndex+1);
				for (x = 0; x < row.cells.length; x++) {
					td=tr.insertCell(x);
					td.setAttribute('valign','top',0);
					td.innerHTML='<br />';
				}
			}
			break;
		case 'delrow':
			var row = PEparentTag(R,'TR'), tbl = PEparentTag(R,'TABLE');
			if(row&&tbl)
				tbl.deleteRow(row.rowIndex);
			break;
		case 'inscell':
			var cell = PEparentTag(R,'TD'),row = PEparentTag(R,'TR');
			if(cell&&row){
				td = row.insertCell(cell.cellIndex);
				td.setAttribute('valign','top',0);
				td.innerHTML='<br />'
			}
			break;
		case 'splitcell':
			var cell = PEparentTag(R,'TD'),row = PEparentTag(R,'TR'),attrs;
			if(cell&&row){
				attrs = cell.attributes;
				td = row.insertCell(cell.cellIndex+1);
				for (x = 0; x < attrs.length; x++)
					td.setAttribute(attrs[x].name, attrs[x].value);
				td.innerHTML='<br />'
			}
			break;
		case 'delcell':
			var cell = PEparentTag(R,'TD'),row = PEparentTag(R,'TR');
			if(cell&&row)
				row.deleteCell(cell.cellIndex);
			break;
		case 'inscol_left':
		case 'inscol_right':
			var tbl = PEparentTag(R,'TABLE'), cell = PEparentTag(R,'TD'), idx = cell.cellIndex;
			for (x = 0; x < tbl.rows.length; x++){
				td = tbl.rows[x].insertCell(nr=='inscol_left'?idx:idx+1);
				td.setAttribute('valign','top',0);
				td.innerHTML='<br />'
			}
			break;
		case 'delcol':
			var tbl = PEparentTag(R,'TABLE'), cell = PEparentTag(R,'TD'), idx = cell.cellIndex;
			for (x = 0; x < tbl.rows.length; x++)
				if(tbl.rows[x].cells.length>idx)		
					tbl.rows[x].deleteCell(idx);
			break;
		default:
			_curWin.document.execCommand(nr,false,null);
	}
	//this.win.focus(); (Don't use, popup windows disappear to back on Mac!)
	this.updMenu()
},

preview:function(popup){
	var html;
	if(this.mode==0)html=this.src();
	if(this.mode==1)html=this.ed.value;
	if(this.mode==2)html=_form[this.name].value;
	if (!(this.feat&16)){
		var css=this.css?'<link rel="stylesheet" type="text/css" href="'+this.css+'" />':'';
		html='<html><head><title>'+PEW[4]+'</title>'+css+'<base href="'+this.base+'"></head><body bgcolor="white"'+(PEforDMD?' link="'+_D.body.link+'"':'')+'>'+html+'</body></html>';
	}
	if(popup){
		PEopenWin('');
		with(PEnewWin.document){open();write(html);close()}
	}
	else
		return html
},

help:function(){
	PEopenWin(PEdir+'/help.php?l='+PElang+'&t='+(this.feat&1)+'&f='+(this.feat&2)+'&p='+(this.feat&16)+'&u='+(this.imgDir==''?0:1))
},

sbsTxt:function(){
	this.mdl('sbs_txt.php',[],310,165)
},

clean:function(){
	this.mdl('cleanup.php',[],300,185)
},

color:function(what){
	var A=[];A['what']=what;
	this.mdl('colorpick.php',A,390,190)
},

font:function(){
	var A=[],opts={'fontsize':0,'fontname':0,'formatblock':0},x,el,R=_curWin.getSelection().getRangeAt(0);
	for (x in opts)
		A[x] = _curWin.document.queryCommandValue(x);
	var el = PEparentTag(R,'FONT');
	if (el) A['el'] = el;
	if (el && el.style && el.style.fontSize)
		A['fontsizepx'] = el.style.fontSize;
	this.mdl('font_props.php',A,400,200)
},

pageProps:function(){
	var A={dom:this.doc, imgDir:this.imgDir, upload:this.feat&8};
	this.mdl('page_props.php',A,300,290)
},

charMap:function(){
	this.mdl('charmap.php',[],425,335)
},

sub:function(sub){
	_curWin.document.execCommand(sub?'SubScript':'SuperScript',false,null);
	this.updMenu()
},

table:function(click,el){
	var el,R,RA=[],CA=[],TA=[],R,C,P,A=[],elTD,elTR,elTBL;
	R = _curWin.getSelection().getRangeAt(0);
	elTD=(el&&el.tagName=='TD')?el:PEparentTag(R,'TD');
	elTR=(el&&el.tagName=='TR')?el:PEparentTag(R,'TR');
	elTBL=(el&&el.tagName=='TABLE')?el:PEparentTag(R,'TABLE');
	if(click)
		nt=0;
	else
		nt=(elTBL) ? confirm(PEW[261]) ? 1 : 0 : 1;
	if(!nt) {
		if(elTBL)A['elTBL'] = elTBL;
		if(elTD)A['elTD'] = elTD;
		if(elTR)A['elTR'] = elTR
	}
	if(nt){
		TA['rows']=TA['columns']=3;
		TA['cellPadding']=TA['cellSpacing']=2;
		TA['border']=TA['nt']=1
	}else{
		if(!elTBL)return;
		TA=PEargs(elTBL,['cellPadding','cellSpacing','width','height','border','bgColor','background','class']);
		if(elTBL.summary)TA['summary']=elTBL.summary;
		if(elTBL.caption){TA['caption']=elTBL.caption.innerHTML;TA['cap_pos']=elTBL.caption.align}
		TA['align']=elTBL.align;
		TA['rows']=elTBL.rows.length;
		TA['columns'] = '';
		if (elTR)
			RA=PEargs(elTR,['align','vAlign','bgColor','class']);
		if (elTD)
			CA=PEargs(elTD,['width','height','align','vAlign','noWrap','bgColor','background','class'])
	}
	A['imgDir']=this.imgDir;A['upload']=this.feat&8;
	A['TA'] = TA;
	A['RA'] = RA;
	A['CA'] = CA;
	A['start'] = el?el.tagName:'TABLE';
	A=this.mdl('table_props.php?nt='+nt,A,380,345)
},

unlink:function(){
	var R=_curWin.getSelection().getRangeAt(0),el=PEparentTag(R,'A');
	if(el)this.delTag(el)
},

link:function(what,shift){
	var el,A=[],R=_curWin.getSelection().getRangeAt(0),hasSel=true;
	if(R.collapsed) hasSel=false;
	el = R.startContainer.childNodes[R.startOffset];
	if(!el||el.tagName!='A')
		el=R.commonAncestorContainer;
	if(el.nodeType == 3)
		el=el.parentNode;
	while(el && el.tagName != 'A')
		el=el.parentNode;
	if (!el && !hasSel) return;	
	if(el && el.tagName == 'A'){
		A['el']=el;
		R=this.doc.createRange();
		R.selectNodeContents(el);
		_curWin.getSelection().addRange(R)
	}
	else {
		if (/<\/?A/i.test(this.node2html(R.cloneContents()))) {
			alert(PEW[267]);return
		}
	}
	A['anchors']=[];
	A['absoluteLinks']=(this.feat&512)?true:false;
	A['outerHTML']=(el)?el.outerHTML:'';
	as=this.doc.anchors;
	for(x=0;x<as.length;x++)
		A['anchors'][x]=as[x].name;
	A['linkcolor']=this.doc.body.link;
	A['deftgt']=this.tgt;
	this.mdl(what+'_props.php?shift='+shift,A,what=='link'?360:280,what=='link'?215:115)
},

image:function(){
	var self=this,el,A={replace:1,base:this.base,callback:function(args){self.imageFinish(args)}},R;
	R = _curWin.getSelection().getRangeAt(0);
	el = R.collapsed?PEparentTag(R,'IMG'):R.startContainer.childNodes[R.startOffset];
	if(el&&el.nodeType==1&&el.tagName=='IMG'){
		A['el']=el;
		A['replace'] = confirm(PEW[282] + '\n' + PEW[283])?1:0;
	}
	var url = 'file.php?mode=image&dir='+escape(this.imgDir);
	if (this.uploadMaxSize) {
		url += '&maxSize='+this.uploadMaxSize;
	}
	if (!(this.feat&8)) {
		url += '&allowFolders=false&allowFiles=false';
	}
	this.mdl(url,A,600,435)
},

imageFinish:function(attrs){
	var attr,allow=['src','align','alt','title','width','height','border','vspace','hspace','class'],x,tag='';
	if (winArgs['el']) {// Modify existing image
		// Remove existing attribs:
		for(x = 0; x < allow.length; x++) {
			winArgs['el'].removeAttribute(allow[x]);
		}
		// Add new attribs:
		for(attr in attrs) {
			winArgs['el'].setAttribute(attr, attrs[attr]);
		}
	}
	else {// New image
		for(attr in attrs) {
			if (tag != '') tag += ' ';
			tag += attr + '="' + attrs[attr] + '"';
		}
		this.insertHTML('<img ' + tag + ' />');
	}
},

attach:function(){
	var self=this,A={callback:function(args){self.attachFinish(args)}}, url = 'file.php?mode=attachment&dir='+escape(this.attachDir);
	if (this.uploadMaxSize) {
		url += '&maxSize='+this.uploadMaxSize;
	}
	if (!(this.feat&8)) {
		url += '&allowFolders=false&allowFiles=false';
	}
	this.mdl(url,A,600,415);
},

attachFinish:function(attrs){
	var tag = 'href="' + attrs['url'] + '"';
	if (attrs['target']) {
		tag += ' target="' + attrs['target'] + '"';
	}
	this.insertHTML('<a ' + tag + '>', '</a>', 1);
	return;


	var attr,tag = '';
	for(attr in attrs) {
		if (attr == 'url') attr = 'href';
		if (tag != '') tag += ' ';
		tag += attr + '="' + attrs[attr] + '"';
	}
	alert(tag);
	this.insertHTML('<a ' + tag + '>', '</a>', 1);
},

hr:function(){
	var el,A=[],R;
	R = _curWin.getSelection().getRangeAt(0);
	el = R.collapsed?PEparentTag(R,'HR'):R.startContainer.childNodes[R.startOffset];
	if(el&&el.nodeType==1&&el.tagName=='HR')
		A['el']=el;
	this.mdl('hr_props.php',A,290,155)
},

form:function(){
	var el,A=[],R;
	R = _curWin.getSelection().getRangeAt(0);
	var form = PEparentTag(R,'FORM');
	if (form)
		A['el']=form;
	this.mdl('form_props.php',A,290,180)
},

formEl:function(what){
	var el,A=[],w=300,h=155,R,ok='|INPUT|SELECT|TEXTAREA';
	if(what=='select'){w=430;h=260}
	if(what=='textarea')h=215;
	if(what=='submit'||what=='reset')h=135;
	R = _curWin.getSelection().getRangeAt(0);
	if (R.collapsed) {
		el=PEparentTag(R,'INPUT');
		if(!el)el=PEparentTag(R,'TEXTAREA');
		if(!el)el=PEparentTag(R,'SELECT');
	}
	else {
		el=R.startContainer;
		if(ok.indexOf(el.tagName)==-1)
			el=R.startContainer.childNodes[R.startOffset]
	}
	if(el&&el.nodeType==1&&ok.indexOf(el.tagName!=-1)) {
		if(what=='select'&&el.tagName!='SELECT')return;
		if(what!='select'&&el.type.toLowerCase()!=what)return;
		A['el']=el
	}
	this.mdl('form_'+what+'.php',A,w,h)
},

setClass:function(css){
	var R=_curWin.getSelection().getRangeAt(0),classEl=PEgetClass(R,1),selEl;
	selEl=R.startContainer.childNodes[R.startOffset];
	if(classEl){
		if(css==0){
			classEl.removeAttribute('class',0);
			/*
			if (classEl.tagName=='SPAN'){
				el=PEparentTag(R,'span');
				//el.outerHTML=el.innerHTML
			}
			*/
		}
		else
			classEl.className=css
	}
	else if(css!=0){
		if(selEl)
			selEl.className=css;
		else
			this.insertHTML('<span class="'+css+'">','</span>',1)
	}
},

updMenu:function(){
	var R=_curWin.getSelection().getRangeAt(0),el,inForm,inTable,inA,inImg,x,ancestors,txt,a,cn,menuVisible;
	if(this.regions.length>0){
		_curWin.region.frame.height=parseInt(_curWin.document.body.offsetHeight,10);
		if(!_curWin.region.isBlockElement)
			_curWin.region.frame.width=parseInt(_curWin.document.body.scrollWidth,10);

		menuVisible = _curWin.region.element.className!='editable-text';
		if(menuVisible!=this.menuVisible){
			this.menuVisible=menuVisible;
			this.buttonBar.style.visibility=(menuVisible)?'visible':'hidden';
			if(!menuVisible&&this.statusBar)this.statusBar.innerHTML='&nbsp;';
		}
		if(!menuVisible)return;
	}

	if(this.classes.length)
		_gE('class_'+this.name).value=PEgetClass(R,0);

	// Togglable buttons:
	_cmds=['bold','italic','underline','strikethrough','superscript','subscript','justifyleft','justifycenter','justifyright','justifyfull','insertorderedlist','insertunorderedlist'];
	for(x = 0; x < _cmds.length; x++) {
		try{
		_gE('m'+_cmds[x]+'_'+this.name).className = this.doc.queryCommandState(_cmds[x]) ? 'PEbutDown' : 'PEbut';
		} catch(e){};
	}

	try {
		_gE('mundo_'+this.name).className = this.doc.queryCommandEnabled('undo') ? 'PEbut' : 'PEbutNot';
		_gE('mredo_'+this.name).className = this.doc.queryCommandEnabled('redo') ? 'PEbut' : 'PEbutNot';
	} catch(e) {};

	// Statusbar:
	if(this.statusBar)this.statusBar.innerHTML='&nbsp;';
	ancestors=this.getAncestors(_curWin.name=='rootWin');
	for (x=ancestors.length;--x>=0;){
		el=ancestors[x];
		if(!el||el.tagName=='TBODY')continue;
		txt=el.tagName.toLowerCase();
		if(txt=='form')inForm=1;
		if(txt=='table')inTable=1;
		if(txt=='a')inA=1;
		if(txt=='img')inImg=1;
		if(this.statusBar){
			a=_D.createElement('a');
			a.href='#';
			a.el=el;
			a.style.color='#000';
			a.onclick=function(){
				_curED.quickEdit(this.el);
				return false
			};
			a.title='';
			if(el.id)a.title+='#'+el.id;
			if(el.className)a.title+='.'+el.className;
			a.appendChild(_D.createTextNode(txt));
			this.statusBar.appendChild(a);
			if(x!=0)
				this.statusBar.appendChild(_D.createTextNode(' '+String.fromCharCode(0xbb)+' '))
		}
	}
	if(this.statusBar&&ancestors.length){
		a=_D.createElement('a');
		a.href='#';
		a.el=el;
		a.onclick=function(){
			_curED.delTag(this.el);
			return false
		};
		a.appendChild(_D.createTextNode('[del]'));
		this.statusBar.appendChild(_D.createTextNode(' '));
		this.statusBar.appendChild(a);
	}

	if(this.feat&1){
		for(x in this.tblEls)
			this.tblEls[x].className=inTable?'PEbut':'PEbutNot'
	}
	if(this.feat&2){
		for(x in this.frmEls)
			this.frmEls[x].className=inForm?'PEbut':'PEbutNot'
	}

	var sel=inImg||(!R.collapsed&&R.toString()!='');
	// Link/Anchor/Unlink buttons:
	cn=sel||inA?'PEbut':'PEbutNot';
	try{
		this.anchorBut.className=cn;
		this.linkBut.className=cn;
		this.unlinkBut.className=inA?'PEbut':'PEbutNot';
		this.attachBut.className=cn
	}catch(e){}
	
	// Extra buttons that need to have a selection:
	cn=R.collapsed?'PEbutNot':'PEbut';
	l=this.sels.length;
	for(x=0;x<l;x++){
		el=_gE(this.sels[x]+'_'+this.name);
		if(el)el.className=cn
	}
	this.showSize()
},

quickEdit:function(el){
	if(el.tagName!='BODY'){
		var S=_curWin.getSelection(),R=this.doc.createRange();
		R.selectNodeContents(el);
		S.removeAllRanges();
		S.addRange(R)
	}
	switch(el.tagName) {
		case 'A':
			this.link(el.name?'anchor':'link',false); break;
		case 'IMG':
			this.image(); break;
		case 'FORM':
			this.form(); break;
		case 'TD': case 'TR': case 'TABLE':
			this.table(1,el); break;
		case 'SELECT':
			this.formEl('select'); break;
		case 'TEXTAREA':
			this.formEl('textarea'); break;
		case 'INPUT':
			this.formEl(el.type); break;
		case 'HR':
			this.hr(); break;
		case 'BODY':
			if((this.feat&16)&&this.regions.length==0)this.pageProps();
			break;
	}
	_curWin.focus();
	this.updMenu()
},

delTag:function(el){
	var tn=el.tagName,lis,i;
	if(tn!='BODY'&&tn!='TD'&&tn!='TR'&&tn!='HR'&&tn!='UL'&&tn!='OL'&&tn!='TABLE'&&tn!='IMG')
		el.outerHTML=el.innerHTML;
	else if(tn=='TABLE'||tn=='IMG'||tn=='HR')
		el.parentNode.removeChild(el);
	else if(tn=='OL'||tn=='UL') {
		lis=el.getElementsByTagName('LI');
		for(i=0;i<lis.length;i++)
			lis[i].outerHTML=lis[i].innerHTML+'<br />';
		el.outerHTML=el.innerHTML
	}
	_curWin.focus();
	this.updMenu()
},

setShowSize:function(){
	this.size=!this.size;
	if(!this.size)
		_gE('PhSz_'+this.name).innerHTML='';
	if(this.mode==0)_curWin.focus();else this.ed.focus();
	this.showSize()
},

showSize:function(){
	if(this.size&&this.mode<2){
		var txt=this.mode==0?this.src():this.ed.value,len=txt.length;
		_gE('PhSz_'+this.name).innerHTML=len>1023?Math.round((len/1024)*100)/100+' KB':len+' bytes'
	}
},

keyPress:function(evt){
	var el,key=String.fromCharCode(evt.charCode).toLowerCase(),cmd=null,value=null,self=this;
	if (evt.ctrlKey){
		switch(key) {
			case 'b':cmd='bold'; break;
			case 'i':cmd='italic'; break;
			case 'u':cmd='underline'; break;
			case 's':cmd='strikethrough';	break;
			case 'l':cmd='justifyleft'; break;
			case 'e':cmd='justifycenter'; break;
			case 'r':cmd='justifyright'; break;
			case 'j':cmd='justifyfull'; break;
			case 'z':cmd='undo';break;
			case 'y':cmd='redo';break;
			case '1':case '2': case '3': case '4': case '5': case '6':cmd='formatblock';value='h'+key; break;
		}
		if (cmd) {
			_curWin.document.execCommand(cmd,false,value);
			evt.preventDefault()
		}
	}
	setTimeout(function(){self.updMenu()},50)
},

//// Licensed version only ////

getHTML:function(){
	if(this.regions.length){
		alert('getHTML not available with editable regions!');
		return ''
	}
	return this.mode==1?this.ed.value:this.src()
},

setHTML:function(html){
	if(this.mode!=0)
		return false;
	this.initWYSIWYG(html,1);
	return true
},

setClasses:function(cls){
	var el,l,x;
	if(!(this.feat&4)||this.classes.length==0)return false;
	this.classes=cls;
	el=_gE('class_'+this.name);
	l=el.options.length;
	for(x=2;x<l;x++)
		el.options[2]=null;
	l=cls.length;
	for(x=0;x<l;x++)
		el.options[2+x]=new Option(cls[x],cls[x]);
	return true
},

load:function(url){
	if(!(this.feat&16)){
		alert("PEload can only be called when editing a complete page (setting 'page' has value 'true')!");
		return
	}
	if(!url)
		url=prompt(PEW[211],'http://');
	if(url){
		url = PEdir+'/load_url.php?l='+PElang+'&cs='+this.charset+'&url='+escape(url);
		XmlHttp.open('GET',url,false);
		XmlHttp.send(null);
		var result = XmlHttp.responseText;
		if (/^>ERROR</.test(result)){
			alert(result); return
		}
		if(this.mode==0)
			this.initWYSIWYG(result,1);
		else
			this.ed.value=result
	}
},

dblclick:function(evt){
	var curEl,R,re;

	if(evt.shiftKey) {
		this.editRegionHTML();
		return;
	}

	R = _curWin.getSelection().getRangeAt(0);
	if (R.collapsed) return;
	curEl = R.startContainer.childNodes[R.startOffset];
	if(!curEl||curEl.nodeType!=1)return;
	if(curEl.tagName=='IMG'){
		curEl=curEl.parentElement;	
		if(this.imgDir!='')
			this.image();
		return
	}
	switch(curEl.tagName){
		case 'A':
			if(curEl){
				this.link(curEl.name?'anchor':'link',false);
				return
			}
			break;
		case 'INPUT':
			if(!(this.feat&2))break;
			this.formEl(curEl.type);break;
			break;
		case 'SELECT':
			if(!(this.feat&2))break;
			this.formEl('select');break;
		case 'TEXTAREA':
			if(!(this.feat&2))break;
			this.formEl('textarea');break;
		case 'TABLE':
			if(!(this.feat&1))break;
			this.table(1);break;
		case 'HR':
			this.hr();break;
	}
},

// Especially for VNU:
replaceImage:function() {
      var el,A=[],R,re,r=1;
      R = _curWin.getSelection().getRangeAt(0);
      el = R.collapsed?PEparentTag(R,'IMG'):R.startContainer.childNodes[R.startOffset];
      if(el&&el.nodeType==1&&el.tagName=='IMG'){
         A['el']=el;
      }
      A['win'] = _curWin;
      re=new RegExp('^'+this.base);
      if(el)A['src']=el.src.replace(re,'');
      A['imgDir']=this.imgDir;
      this.mdl('vnu_img_replace.php?maxSize='+this.uploadMaxSize,A,300,100)
   }
}
// End prototype functions
