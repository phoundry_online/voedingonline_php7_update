keyDown:function(evt){
   if(evt.keyCode==13&&!evt.shiftKey){
      this.insertHTML('<br>');
      evt.returnValue=false
   }
   else if(evt.keyCode==13&&evt.shiftKey){
      var R=this.doc.selection.createRange(),el=R.parentElement();
      while(el.tagName!='BODY'&&el.tagName!='LI')
         el=el.parentElement;
      R.pasteHTML(el.tagName=='LI'?'</li><li>':'<p>'+R.htmlText+'</p>');R.select();
      evt.returnValue=false
   }
   else if(evt.keyCode==86&&evt.ctrlKey){ // CTRL-V
      if(_gE('PhAc_'+this.name).checked&&confirm(PEW[269]))this.pasteMSprepare();
      this.doc.execCommand('paste');
      evt.returnValue=false
   }
},
