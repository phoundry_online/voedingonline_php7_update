<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Link intern</title>
	<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/tiny_mce/tiny_mce_popup.js"></script>
	<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/tiny_mce/utils/mctabs.js"></script>
	<script type="text/javascript">

	function init() {
		tinyMCEPopup.resizeToInnerSize();
	}

	function setInternLink(url) {
		var input = tinyMCEPopup.getWindowArg('input');
		input.value = url;
		tinyMCEPopup.close();
	}

	tinyMCEPopup.onInit.add(init);

	</script>
</head>
<body style="display:none">
<form onsubmit="setInternLink(this['url'].value);return false" action="#">
	<div class="tabs">
		<ul>
			<li id="link1_tab" class="current"><span><a href="javascript:mcTabs.displayTab('link1_tab','link1_panel');" onmousedown="return false;">Link intern</a></span></li>
			<!--
			<li id="link2_tab"><span><a href="javascript:mcTabs.displayTab('link2_tab','link2_panel');" onmousedown="return false;">Link intern</a></span></li>
			-->
		</ul>
	</div>

	<div class="panel_wrapper">
		<div id="link1_panel" class="panel current" style="height:220px">
		<fieldset>
		<legend>Kies interne link</legend>
		<select name="url" style="width:100%">
		<option value="http://www.webpower.nl">webpower.nl</option>
		<option value="http://www.dmdelivery.nl">dmdelivery.nl</option>
		<option value="http://www.phoundry.nl">phoundry.nl</option>
		</select>
		</div>

		<!--
		<div id="link2_panel" class="panel" style="height:220px">
		<fieldset>
		<legend>Kies interne link</legend>
		<select name="url" style="width:100%">
		<option value="http://www.webpower.nl">webpower.nl</option>
		<option value="http://www.dmdelivery.nl">dmdelivery.nl</option>
		<option value="http://www.phoundry.nl">phoundry.nl</option>
		</select>
		</div>
		-->

	</div>

	<div class="mceActionPanel">
		<div style="float: left">
			<input type="submit" id="insert" name="insert" value="{#insert}" />
		</div>

		<div style="float: right">
			<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" />
		</div>
	</div>
</form>
</body>
</html>
