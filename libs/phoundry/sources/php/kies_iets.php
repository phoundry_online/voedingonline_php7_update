<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	checkAccess($TID);

	$title = 'Kies categorie';

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title><?= $title ?></title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[


//]]>
</script>
</head>
<body class="frames">
<form action="<?= $PHprefs['url'] ?>/core/records.php">
<input type="hidden" name="TID" value="<?= $TID ?>" />
<div id="headerFrame" class="headerFrame">
<?= getHeader($title); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<select name="cat_id">
<?php
	$sql = "SELECT id, name FROM product_cat ORDER BY name";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		print '<option value="' . $db->FetchResult($cur,$x,'id') . '">' . $db->FetchResult($cur,$x,'name') . '</option>';
	}
?>
</select>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" class="okBut" value="<?= word(82) ?>" />
	</td>
	<td align="right">
	<input type="button" value="<?= word(46) ?>" onclick="history.back()" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
