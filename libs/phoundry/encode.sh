#!/bin/bash

#############################
## Configuration variables ##
#############################
SRC_DIR=`pwd -P`
DST_DIR=`echo $SRC_DIR.enc`
#ZEND_ENC=/usr/local/Zend/ZendGuard-5_0_1/bin/zendenc5

### NEW, for PHP 5.3:
ZEND_ENC=/usr/local/Zend/ZendGuard-5_5_0/plugins/com.zend.guard.core.resources.linux.x86_5.5.0/resources/bin/zendenc53
PATH=$PATH:/usr/local/Zend/ZendGuard-5_5_0/plugins/com.zend.guard.core.resources.linux.x86_5.5.0/resources

echo
echo "+------------------------------+"
echo "|    Phoundry Zend-encoder     |"
echo "+----- (c)2005 Web Power ------+"
echo

# Does DST_DIR exist? If not, create it:
if [ ! -d $DST_DIR ]
then
	echo "Making destination directory $DST_DIR ..."
	mkdir $DST_DIR
fi

if [ ! -d $DST_DIR ]
then
	echo "ERROR: Directory $DST_DIR could not be created!";
	exit
fi

echo "Cleaning up destination directory $DST_DIR ..."
rm -rf $DST_DIR/*

echo "Copying source files ..."
(cd $SRC_DIR ; tar --exclude=encode.sh --exclude=encode_ZendGuard5.5.0.sh --exclude=.svn --exclude=.git --exclude='*.old' --exclude='*.ok' --exclude='*.bak' --exclude=sources --exclude=.buildpath --exclude=.project --exclude=.settings -cpf - .) | ( cd $DST_DIR ; tar xpf -)

echo "Compiling source files ..."
echo
cd $DST_DIR

$ZEND_ENC --silent --recursive --delete-source --include-ext php --ignore "$DST_DIR/custom" --ignore "$DST_DIR/INSTALL.php" --ignore "$DST_DIR/core/include/PhoundryLogin.php" .

echo
echo "Done!"
