#!/usr/local/bin/php -q
<?php

if ( !isset($_ENV['SHELL']) || isset($_SERVER['HTTP_HOST']) ) {
	die("This script can only be called from the command line\n");
}


// INSTALL.php --install-dir=/home/peter.eussen/apache_chroot/project/bdu.kranten.fase3/phoundry --unattended --upload-dir=/project/bdu.kranten.fase3/uploaded --charset=iso-8859-1 --db-server=192.168.2.7 --db-schema=peterdb --db-user=peter --db-password=test123 --logfile=/project/bdu.kranten.fase3/tmp/phoundry.log --no-html-doc --server-name=dev.local --create-dir –quiet

	$cmdLineOptions = array(
							'unattended'			=> "Runs installer in unattended mode. This eliminates all interactive questions.\n{i}You should configure the installer using the command line arguments to really use\n{i}this method.",
							'quiet'					=> "Do not print any information",
							'install-dir'			=> "The path on the filesystem where Phoundry should be installed",
							'phoundry-url'			=> "URL path to the Phoundry directory (should start with a /)",
							'upload-dir'			=> "Path on the filesystem to the upload directory (should be absolute)",
							'upload-url'			=> "URL path to the upload directory (should start with a /)",
							'config-dir'			=> "Location where you want the PREFS.php file to be created",
							'charset'				=> "Character set to use (either iso-8859-1 or utf-8)",
							'db-server'				=> "Hostname of your database server",
							'db-schema'				=> "Database schema you want to use",
							'db-user'				=> "Database username you want to use to connect",
							'db-password'			=> "The password that should be used when connecting to the database",
							'db-port'				=> "The port your database server is using",
							'create-tables'			=> "Automatically create the Phoundry tables",
							'search-string-length'	=> "The maximum string length of search results",
							'language'				=> "The default language to use. Use the 2 letter country code",
							'logfile'				=> "The path and name of the logfile",
							'password-expire'		=> "The number of days a user password is valid (0 for no expiration)",
							'smtp-server'			=> "The servername of your smtp server, typically this will be localhost",
							'smtp-from'				=> "The default FROM e-mail adres that can be used by Phoundry",
							'html-doc-path'			=> "The path to HTMLDoc, if no HTML doc is available use the --no-html-doc option",
							'no-html-doc'			=> "Do not use HTMLDoc",
							'use-record-lock'		=> "Use record locking",
							'use-record-copy'		=> "Use the record copy function",
							'use-email-notification'=> "Use email notification when a record has been changed",
							'max-table-notes'		=> "The maximum amount of notes you can attach to a table",
							'license'				=> "The type of license you want to install. This can be:\n{i}B - Bronze\n{i}S - Silver\n{i}G - Gold",
							'max-users'				=> "Maximum amount of users that can be created in the Phoundry system",
							'server-name'			=> "The hostname where Phoundry is installed, and will be accessed from",
							'create-dir'			=> "Automatically create the target directory if it does not exist"
						 );
						 
	$cmdLinePrefs = array(
							'unattended'	=> false,
							'quiet'			=> false,
							'no-html-doc'	=> false
						 );
	
	function yes( $sValue )
	{
		return ( strtolower($sValue) == 'y' || $sValue === true);
	}
	
	function printUsage()
	{
		global $cmdLineOptions;
		
		$b = array_keys($cmdLineOptions);
		$m = 0;
		
		foreach($b as $i ) $m = max($m,strlen($i));

		print("Usage: INSTALL.php <--option=value>\n\n");
		print("This script can be used to install Phoundry. You have the following command-line options you can use: \n");
		
		foreach( $cmdLineOptions as $opt => $desc )
			print( str_pad('--' . $opt,$m + 3) . str_replace("{i}",str_repeat(' ', $m+3),$desc) . "\n" );
			
		print "\n\n";
	}
	
	function parsePref($sLine)
	{
		$aLine = explode('=',$sLine);
		
		// Als alleen de naam, dan de value op TRUE zetten
		if (count($aLine) == 1)
			$aLine[1] = true;
		else
			$aLine[1] = trim($aLine[1],"\"\r\t\n ");
		$aLine[0] = trim(ltrim($aLine[0],'-'));
		return $aLine;
	}
	
	if (isset($_SERVER['argv']) )
	{
		array_shift($_SERVER['argv']);
		
		while ( $option = array_shift($_SERVER['argv']) )
		{
			list($key,$value) = parsePref($option);
			
			if ( in_array($key, Array('h','?','help') ) )
			{
				printUsage();
				exit();
			}
			
			if ( !in_array($key,array_keys($cmdLineOptions)) )
				die("Invalid option $key");
			else
			{
				// Option checking
				switch ($key ){
				case 'upload-dir':
				case 'phoundry-url':
					if ( substr($value,0,1) != '/' )
						$value = '/' . $value;
					 
					$cmdLinePrefs[ $key ] = $value;
					break;
				case 'language':
					if ( strlen($value) != 2)
						die("Invalid country code: $value\n");
						
					$cmdLinePrefs[ $key ] = $value;
					break;
				case 'license':
					$value = strtolower($value);
					
					if ( !in_array($value,Array('s','g','b')) )
						die("Invalid license: $value\n");
						
					$cmdLinePrefs[ $key ] = $value;
					break;
				case 'db-port':
				case 'max-table-notes':
				case 'max-users':
					$value = (int) $value;
					$cmdLinePrefs[ $key ] = $value;
					break;
				default:
					$cmdLinePrefs[ $key ] = $value;
				}
			}
		}
	}
	
	function defaultstdin( $sOption, &$sValue, $sConfigOption )
	{
		global $cmdLinePrefs;
		
		if ( array_key_exists( $sConfigOption, $cmdLinePrefs ) && !is_null($cmdLinePrefs[$sConfigOption]) )
		{
			$sValue = $cmdLinePrefs[$sConfigOption];
			return $sValue;
		}	
		
		if ( $cmdLinePrefs['unattended'] && $sValue !== '' && !is_null($sValue))
			return $sValue;
			
		stdin($sOption,$sValue);
		
		$cmdLinePrefs[ $sConfigOption ] = $sValue;
		return $sValue;
	}
	
	function printVerbose( $sMsg )
	{
		global $cmdLinePrefs;
		
		if ( !$cmdLinePrefs['quiet'] )
			return print $sMsg;
		return 0;
	}
	
	set_time_limit(0);

	$win = stristr(php_uname(), 'windows') !== false;

	function WPencode($str) {
		$str .= chr(255) . crc32('w' . $str . 'p');
		$ret = '';
		for ($x = 0; $x < strlen($str); $x++)
			$ret .= bin2hex($str{$x});
		return strrev($ret);
	}

	function stdin($prompt, &$var) {
		global $win;

		$in = '';
		$pr = $prompt;
		if ($var !== '')
			$pr .= " ($var)";
		$pr .= ': ';
		print wordwrap($pr, 79);
		while(($char = fgetc(STDIN)) != "\n") {
			if(ord($char) != 13)
				$in .= $char;
			//print "O: " . ord($char) . "\n";
		}
	
		if ($in !== '')
			$var = $in;

		if ($var === ' ') print "*NONE*\n\n";
		else             print " -> $var\n\n";
	}

	function checkDir($dir) {
		global $cmdLinePrefs;
		
		if (!@is_dir($dir)) {
			
			if (array_key_exists('create-dir', $cmdLinePrefs) && yes($cmdLinePrefs['create-dir'] ) )
				$cdir = 'y';
			else
			{ 
				print "Warning: directory $dir does not exist!\n";
				print "Do you want it to be created? [y|n] ";
				$cdir = '';
				while(($char = fgetc(STDIN)) != "\n") {
					if (ord($char) != 13)
						$cdir .= $char;
				}
			}
			
			if ($cdir == 'y' || $cdir == 'Y')
				mkdir($dir, 0755);
			else {
				print "Exiting!\n";
				exit(1);
			}
		}
	}

if ($win)
	$fromDir = '.';
else {
	$fromDir = getenv('PWD');
	if (empty($fromDir)) $fromDir = '/project/MODULES/phoundry';
}

if ( ini_get('session.save_path') == '')
	ini_set('session.save_path', (is_dir('/tmp') ? '/tmp' : './') );
	
printVerbose( "Phoundry Installer\n" );
printVerbose( "==================\n\n" );
printVerbose( "Installing from $fromDir\n\n" );

if ($win)
	printVerbose( "ATTENTION: when entering paths, use slashes (/) instead of backslashes(\)!\n\n" );

$installDir = '';
defaultstdin('Enter directory where Phoundry should be installed', $installDir ,'install-dir');
if (substr($installDir, -1) == '/')
	$installDir = substr($installDir, 0, strlen($installDir)-1);

$phoundryUrl = '/phoundry/';
defaultstdin('Enter absolute Phoundry URL (start with /)', $phoundryUrl, 'phoundry-url');

$configDir = dirname($installDir) . '/config';
defaultstdin("Enter the directory where the PREFS.php should be saved (start with /)", $configDir, 'config-dir');
if (substr($configDir, -1) == '/')
	$configDir = substr($configDir, 0, strlen($configDir)-1);

$charset = 'iso-8859-1';
defaultstdin("Enter the characterset to user (iso-8859-1 or utf-8)", $charset, 'charset');

$DBserver = ($win) ? trim(shell_exec('hostname')) : 'localhost';
defaultstdin("Enter mysql server", $DBserver,'db-server');

$DBname = '';
defaultstdin("Enter mysql DB-name", $DBname, 'db-schema');

$DBuserid = '';
defaultstdin("Enter mysql username", $DBuserid, 'db-user');

$DBpasswd = '';
defaultstdin("Enter mysql password", $DBpasswd, 'db-password');

$DBport = 3306;
defaultstdin("Enter mysql port", $DBport, 'db-port');

$uploadDir = dirname($installDir) . '/uploaded';
defaultstdin("Enter global (absolute) upload directory (start with /)", $uploadDir, 'upload-dir');
if (substr($uploadDir, -1) == '/')
	$uploadDir = substr($uploadDir, 0, strlen($uploadDir)-1);

$uploadUrl = '/uploaded';
defaultstdin("Enter global (absolute) upload URL (start with /)", $uploadUrl, 'upload-url');
if (substr($uploadUrl, -1) == '/')
	$uploadUrl = substr($uploadUrl, 0, strlen($uploadUrl)-1);

$recordStrlen = 90;
defaultstdin('Enter maximum (string)length of search results', $recordStrlen, 'search-string-length');

$language = 'nl';
defaultstdin('Enter language to use ([nl|en|de])', $language, 'language');

$logFile = "/var/log/www/$DBuserid/phoundry.log";
defaultstdin('Enter logfile path (enter space if not needed)', $logFile, 'logfile');
if ($logFile == ' ') $logFile = '';

$g_SMTPserver = 'localhost';
defaultstdin('Enter the name of your SMTP server', $g_SMTPserver, 'smtp-server');

$g_SMTPfrom = 'phoundry@' . (($win) ? trim(shell_exec('hostname')) : trim(shell_exec('hostname -d')));
defaultstdin('Enter the SMTP-from (Return-Path) address', $g_SMTPfrom, 'smtp-from');

$htmldocPath = '';
if (!$win)
	$htmldocPath = trim(shell_exec('which htmldoc 2>/dev/null'));
if ( !$cmdLinePrefs['no-html-doc'] )
	defaultstdin('Enter path to HTMLdoc (leave empty if unavailable)', $htmldocPath, 'html-doc-path');

$useLocks = 'n';
defaultstdin('Do you want to use record-locking? [y|n]', $useLocks, 'use-record-lock');

$useNotify = ($win) ? 'n' : 'y';
defaultstdin('Do you want to use email-notification? [y|n]', $useNotify, 'use-email-notification');

$useCopy = ($win) ? 'n' : 'y';
defaultstdin('Do you want to use record-copy? [y|n]', $useCopy ,'use-record-copy');

$nrNotes = 3;
defaultstdin('How many notes per table?', $nrNotes ,'max-table-notes');

$dodb = 'y';
defaultstdin('Do you want the DB-tables to be created? [y|n]', $dodb, 'create-tables');

$license = 'B';
defaultstdin('Is this a Phoundry Bronze, Silver or Gold license? [B|S|G]', $license , 'license');

if     ($license == 'B') $nrUsers = 5;
elseif ($license == 'S') $nrUsers = 20;
elseif ($license == 'G') $nrUsers = 50;
defaultstdin('Enter number of Phoundry users', $nrUsers ,'max-users');

$server_name = '';
defaultstdin('Enter domain-name(s) this Phoundry will run on. Separate multiple domain names with a comma (don\'t use spaces!)', $server_name ,'server-name');

$g_prodKey = WPencode($server_name . '|' . $license . '|' . $nrUsers);

// Check if install-directory exists:

checkDir($installDir);

// Start doing some real work:

printVerbose("-> Changing to directory $installDir...\n");

if ($win) {
	printVerbose("-> Copying files...\n");
	$instDir = str_replace('/', "\\", $installDir);
	shell_exec("xcopy $fromDir\core $instDir\core /E /I /Y /Q");
	shell_exec("xcopy $fromDir\admin $instDir\admin /E /I /Y /Q");
	shell_exec("xcopy $fromDir\custom $instDir\custom /E /I /Y /Q");
	shell_exec("copy $fromDir\index.php $instDir");
	//shell_exec("copy $instDir\core\menu.php $instDir\custom\menu.php");
}
else {
	printVerbose("-> Softlinking & Copying files...");
	shell_exec("/bin/cp -r $fromDir/custom $installDir");
}
printVerbose("...Done!\n\n");

// Create PREFS.php file.

$useLocks  = yes($useLocks)  ? 'true' : 'false';
$useNotify = yes($useNotify) ? 'true' : 'false';
$useCopy   = yes($useCopy)   ? 'true' : 'false';

$DBoptions = "'DBoptions'    => array(),";

$baseDir = dirname($installDir);

$PROJECT_DIR = dirname($installDir);

$prefs =<<<EOP
<?php

//
// Phoundry core settings:
//

\$PHprefs = array(

// Main preferences.
'product'		=> 'Phoundry',
'productKey'	=> '$g_prodKey',
'languages'		=> array('$language'),
'baseDir'		=> realpath('$baseDir'),
'distDir'		=> realpath('$fromDir'),
'instDir'		=> realpath('$installDir'),
'tmpDir'			=> realpath('$baseDir/tmp'),
'url'				=> '$phoundryUrl',
'customUrls'	=> array(),
'menuTabs'		=> array('Phoundry'),

// Other preferences.
// Charset for DB & web pages. Either 'iso-8859-1' or 'utf-8' (lowercase)
'charset'		=> '$charset',
'showNews'		=> true,
'manualUrl'		=> 'http://portal.webpower.nl/manuals/phoundry/',
'uploadDir'		=> realpath('$uploadDir'),
'uploadUrl'		=> '$uploadUrl',
'recordStrlen'	=> $recordStrlen,
'recordsPerPage' => 100,
'useLocks'		=> $useLocks,
'useNotify'		=> $useNotify,
'useCopy'		=> $useCopy,
'nrNotes'		=> $nrNotes,
'logFile'		=> '$logFile',

// DMfacts account (if available):
'DMfactsUserid'	=> '',
'DMfactsPasswd'	=> ''
);

require_once('PREFS.sandbox.php');

define('PROJECT_DIR',    '{$PROJECT_DIR}/');
define('PHOUNDRY_DIR',   '{$installDir}/');
define('UPLOAD_DIR',     '{$uploadDir}/');
define('TMP_DIR',        PROJECT_DIR . 'tmp/');
define('TEMPLATE_DIR',   PROJECT_DIR . 'templates/');
define('INCLUDE_DIR',    PROJECT_DIR . 'include/');
define('CACHE_DIR',      TMP_DIR . 'cache/');
define('PHP_UPLOAD_DIR', TMP_DIR . 'php/');

// Settings specific for this website.
// Add settings or functions below.
//

\$WEBprefs = array();

?>
EOP;

checkDir($configDir);
printVerbose("-> Generating 'PREFS.php' file...");
$fp = fopen("$configDir/PREFS.php", 'w');
if ($fp) {
	fwrite($fp, $prefs);
	fclose($fp);
	if (!$win)
		shell_exec("chmod 644 $configDir/PREFS.php");
}
else 
{
	printVerbose("Unable to create 'PREFS.php' file, exiting!\n");
	exit(1);
}

$prefsSandbox =<<<EOP
<?php

#[phoundry settings]#
\$PHprefs = array_merge(\$PHprefs, array(
// Database preferences.
'DBtype'			=> 'mysql',
'DBserver'		=> '$DBserver',
'DBuserid'		=> '$DBuserid',
'DBpasswd'		=> '$DBpasswd',
'DBname'			=> '$DBname',
'DBport'			=> $DBport,
'DBpersistent'	=> false,
$DBoptions

// SMTP settings
'SMTPserver'	=> '$g_SMTPserver',
'SMTPport'     	=> 25,
'SMTPfrom'		=> '$g_SMTPfrom',
'SMTPhelo'		=> php_uname('n'),

// Paths to executables
'paths'			=> array('htmldoc'=>'$htmldocPath'), 
));
#[/phoundry settings]#

?>
EOP;

$fp = fopen("$configDir/PREFS.sandbox.php", 'w');
if ($fp) {
	fwrite($fp, $prefsSandbox);
	fclose($fp);
	if (!$win)
		shell_exec("chmod 644 $configDir/PREFS.sandbox.php");
}
else 
{
	printVerbose("Unable to create 'PREFS.sandbox.php' file, exiting!\n");
	exit(1);
}

printVerbose("...Done!\n\n");

if ( yes($dodb) ) 
{
	$coreInclude = "$fromDir/core/include";

	// Make database connection....
	$PHprefs = array(
		'DBtype' => 'mysql',
		'DBserver' => $DBserver,
		'DBuserid' => $DBuserid,
		'DBpasswd' => $DBpasswd,
		'DBport' => $DBport,
		'DBname' => $DBname,
		'distDir'=> $fromDir,
		'url'		=> $phoundryUrl,
		'charset' => $charset,
		$DBoptions
	);

	printVerbose("-> Creating database tables....");

	$skeleton = file_get_contents("{$fromDir}/skeleton/phoundrydb.dmp");

	// Substitute stuff:
	$DBcharset = ($charset == 'iso-8859-1') ? 'latin1' : 'utf8';
	$skeleton = str_replace('{$charset}', $DBcharset, $skeleton);

	$tmpfile = tempnam('/tmp', 'PHskeleton');
	file_put_contents($tmpfile, $skeleton);

	shell_exec("/usr/bin/mysql -h {$DBserver} -u {$DBuserid} -p\"{$DBpasswd}\" {$DBname} < {$tmpfile}");
	unlink($tmpfile);

	// We need a Metabase connection now:
	require "{$fromDir}/core/include/metabase/connect.php";
	$db = DBconnect(true);

	$sql = "INSERT INTO phoundry_user (name, username, e_mail, password, password_set) VALUES('Web Power', 'webpower', 'phoundry_admin@webpower.nl', '', NOW())";
	$res = $db->Query($sql)
		or die ("Query $sql failed!\n");
}

printVerbose( "-> All done!\n\n" );

if ( !$cmdLinePrefs['quiet'] )
{
	print <<<EOL
	1) Add following lines to the Virtual Host (Apache only!):
		Alias /phoundry/custom /project/clientname.nl/phoundry/custom
		Alias /phoundry /project/MODULES/phoundry
		Alias /uploaded /project/clientname.nl/uploaded
		SetEnv CONFIG_DIR /project/clientname.nl/config
	
	2) Set php.ini directives:
		session.cache_limiter =;

EOL;
}

?>
