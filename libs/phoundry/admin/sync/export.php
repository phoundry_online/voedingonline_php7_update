<?php
$res = $this->_query(
	"SELECT
		string_id as value,
		COALESCE(description, name) as text
	FROM
		phoundry_table
	ORDER BY
		string_id"
);

$tables = array();
while ($row = mysql_fetch_assoc($res)) {
	$tables[$row['value']] = $row['value'].": ".PH::langWord($row['text']);
}

include 'html/export.tpl.php';