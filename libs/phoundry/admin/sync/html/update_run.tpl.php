<?if(!$update_statements['import'] && !$update_statements['update']):?>
	Phoundry is in Sync
<?else:?>
<fieldset>
	<legend><strong>Sync</strong></legend>
	<form action="<?=htmlspecialchars($_SERVER['REQUEST_URI'])?>" method="post">
	<input type="hidden" name="tab" value="update" />
	<div><label><input name="update_desc" value="1" type="checkbox" checked="checked" /> Update beschrijvingen</label></div>
	<div>
		<?if($update_statements['import']):?>
		<fieldset>
		<legend><img src="<?=$PHprefs['url']?>/core/icons/plugin_add.png" alt="+" /> Nieuwe tabellen</legend>
			<?foreach($update_statements['import'] as $string_id => $table):?>
			<div>
			<input class="table" name="import[]" value="<?=htmlspecialchars($string_id)?>" type="checkbox" />
			<?=$string_id?> : 
			<?=PH::langWord($table['phoundry_table']['description'])?>
			</div>
			<?endforeach?>
		</fieldset>
		<?endif?>
		<?if($update_statements['update']):?>
		<fieldset>
		<legend><img src="<?=$PHprefs['url']?>/core/icons/plugin_edit.png" alt="*" /> Aangepaste tabellen</legend>
			<?foreach($update_statements['update'] as $string_id => $table):?>
			<div>
			<input class="table" name="update[]" value="<?=htmlspecialchars($string_id)?>" type="checkbox" checked="checked" />
			<?=$string_id?> : 
			<?=PH::langWord($update_trees[$string_id]['phoundry_table']['description'])?>
			</div>
			<?endforeach?>
		</fieldset>
		<?endif?>
	</div>
	<div><input type="submit" value="Sync" name="action[updateRun]" /></div>
	</form>
</fieldset>
<?endif?>