<div>
	<form action="<?=htmlspecialchars($_SERVER['REQUEST_URI'])?>" method="post">
	<input type="hidden" name="tab" value="insert" />
	<label>Alles: <input type="checkbox" checked="checked" onclick="$(':checkbox', $(this).parents('div.pane')).prop('checked', this.checked);" /></label>
	<?foreach($insert_table_trees as $tid => $table_tree):?>
	<div>
		<fieldset>
		<legend>
			<input class="insert_table_check" name="insert_tables[]" type="checkbox" value="<?=$tid?>" checked="checked" />
			<?=htmlspecialchars($table_tree['phoundry_table']['name'])?>
			:
			<?=PH::langWord($table_tree['phoundry_table']['description'])?>
		</legend>
		<div>
			String id:
			<input class="to_strings" name="to_strings[<?=$tid?>]" type="text" value="<?=$table_tree['phoundry_table']['string_id']?>" />
		</div>
		<div>
			Table name:
			<select name="to_tables[<?=$tid?>]">
			<option value=""></option>
			<?foreach($to_tables as $text): ?>
				<option value="<?=$text?>"<?
				if(
					(!isset($posted['to_tables'][$tid]) && $text == $table_tree['phoundry_table']['name']) ||
					(isset($posted['to_tables'][$tid]) && $posted['to_tables'][$tid] == $text)
				) echo ' selected="selected"';
				?>><?=htmlspecialchars($text)?></option>
			<?endforeach?>
			</select>
		</div>
		</fieldset>
	</div>
	<?endforeach?>
	<div><input type="submit" value="Import" name="action[insertRun]" /></div>
	</form>
</div>
