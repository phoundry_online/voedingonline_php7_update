<!DOCTYPE html>
<html>
	<head>
		<title>Table Interface Copy</title>
		<meta http-equiv="content-type" content="text/html; charset=<?=$PHprefs['charset']?>" />
		<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
		<link rel="stylesheet" href="<?=$PHprefs['cssUrl']?>/phoundry.css" type="text/css" />
		<!--[if IE]>
		<link rel="stylesheet" href="<?=$PHprefs['cssUrl']?>/msie.css" type="text/css" media="screen" />
		<![endif]-->
		<script type="text/javascript" src="<?=$PHprefs['url']?>/core/csjs/global.js.php?<?=QS(1)?>"></script>
	<script type="text/javascript">
	jQuery(function($){
		$('div#tabstrip a').click(function(e){
			var tab = $(this).parent('li'), pane = $('#pane_'+tab.attr('id').replace(/^tab_/, ''));
			if(pane && pane.length > 0) {
				$('div#tabstrip li').removeClass('active');
				tab.addClass('active');
				$('div.contentFrame div.pane').hide();
				pane.show();
				this.blur();
			}
			return false;
		});
		
		var current_string_ids = (<?=json_encode($this->currentStringIds())?>);
		$('input.to_strings').bind('change keyup', function(){
			$(this).toggleClass('exists', -1 !== $.inArray($(this).val(), current_string_ids));
		}).trigger('change');

		$('#pane_update input.table').bind('click', function(){

		});
	});
	</script>
	<style type="text/css">
	input.exists {
		color: red;
	}
	.pane {
		clear:both;
	}
	</style>
	</head>
	<body class="frames">
		<div id="headerFrame" class="headerFrame">
			<?=getHeader('Phoundry Sync')?>
		</div>
		<div id="contentFrame" class="contentFrame">
			<div id="tabstrip" class="tabstrip-bg">
				<ul>
					<li id="tab_export" class="<?=($this->tab == 'export' ? 'active' : '')?>"><a href="#">Export</a></li>
					<li id="tab_insert" class="<?=($this->tab == 'insert' ? 'active' : '')?>"><a href="#">Import</a></li>
					<li id="tab_update" class="<?=($this->tab == 'update' ? 'active' : '')?>"><a href="#"><img class="icon" src="<?=$PHprefs['url']?>/core/icons/arrow_refresh_small.png" style="height: 15px;" />Sync</a></li>
				</ul>
			</div>
			<?if($messages = $this->messages()):?>
			<fieldset class="message">
				<legend>Meldingen</legend>
				<?foreach($messages as $message):?>
				<div><?=$message?></div>
				<?endforeach?>
			</fieldset>
			<?endif?>
			<?if($errors = $this->errors()): ?>
			<fieldset class="errors">
				<legend>Errors</legend>
				<?foreach($errors as $error):?>
				<div><?=htmlspecialchars(ucfirst($error))?></div>
				<?endforeach?>
			</fieldset>
			<?endif?>
			<div id="pane_export" class="pane" style="display: <?=($this->tab == 'export' ? 'block' : 'none')?>;">
				<?include('export.php')?>
			</div>
			<div id="pane_insert" class="pane" style="display: <?=($this->tab == 'insert' ? 'block' : 'none')?>;">
				<?include('insert.php')?>
			</div>
			<div id="pane_update" class="pane" style="display: <?=($this->tab == 'update' ? 'block' : 'none')?>;">
				<?include('update.php')?>
			</div>
			<div>
			</div>
		</div>
		<div id="footerFrame" class="footerFrame">
		<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
			<td>
			</td>
			<td align="right">
			<input type="button" value="<?=word(46)?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
			</td>
		</tr></table>
		</div>
	</body>
</html>
