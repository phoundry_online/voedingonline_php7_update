<?php
error_reporting(error_reporting() & E_DEPRECATED);
ob_start();
require_once(getenv('CONFIG_DIR') . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
$site_identifier = (!empty($_GET['site_identifier'])) ? $_GET['site_identifier'] : NULL;

if(isset($PHprefs['DBtype']) && $PHprefs['DBtype'] != 'mysql') {
	trigger_error("Database sync does not support db type ".$PHprefs['DBtype'], E_USER_ERROR);
}

checkAccess('admin');

switch (strtolower($PHprefs['charset'])) {
	case 'latin':
	case 'latin1':
	case 'iso-8859-1':
		$collation = 'latin1';
		break;
		
	case 'utf-8':
	case 'utf8':
	default:
		$collation = 'utf8';
		break;
}

$db = mysql_connect(
	$PHprefs['DBserver'], $PHprefs['DBuserid'], $PHprefs['DBpasswd'], true
);
mysql_select_db($PHprefs['DBname'], $db);

require_once "classes/PhoundrySync.php";
require_once "classes/PhoundrySyncGUI.php";

$gui = new PhoundrySyncGUI(
	$db,
	new PhoundrySync($db, $PHprefs['charset']),
	!empty($_REQUEST['tab']) ? $_REQUEST['tab'] : 'export'
);
ob_clean();
header("Content-type: text/html; charset={$PHprefs['charset']}");
$gui->run($_POST);
