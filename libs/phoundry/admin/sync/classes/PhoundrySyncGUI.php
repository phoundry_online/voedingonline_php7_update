<?php
/**
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class PhoundrySyncGUI
{
	/**
	 * @var PDO
	 */
	protected $_db;

	/**
	 * @var string the current selected tab
	 */
	public $tab;

	/**
	 * @var PhoundrySync
	 */
	protected $_sync;

	/**
	 * @var array Containing the errors that occured
	 */
	protected $_errors;

	public function __construct($db, PhoundrySync $sync, $tab)
	{
		if (!$db) {
			throw new InvalidArgumentException('DB should be a valid resource');
		}
		$this->_db = $db;
		$this->_sync = $sync;
		$this->tab = $tab;

		$this->_errors = array();
		$this->_messages = array();
	}

	/**
	 * Process the submitted action and render the gui
	 * @param array $posted
	 * @return void
	 */
	public function run(array $posted)
	{
		global $PHprefs, $WEBprefs;
		$action = !empty($posted['action']) && is_array($posted['action']) ?
			key($posted['action']) : '';

		// Process the submitted action
		$output = array();
		if($action && method_exists($this, $action."Action")) {
			$output = $this->{$action."Action"}($posted);
		}

		if($output !== false) {
			if(is_array($output) && $output) {
				extract($output, EXTR_SKIP);
			}
			include dirname(__FILE__).'/../html/index.tpl.php';
		}
	}

	/**
	 * Export the given tables
	 *
	 * @param array $posted
	 * @return bool|void
	 */
	public function exportAction(array $posted)
	{
		$table_trees = array();
		if(empty($posted['from_table']) || !is_array($posted['from_table'])) {
			$this->_error('no table selected to copy from');
		}
		else {
			foreach($posted['from_table'] as $string_id) {
				if($table = $this->_sync->export($string_id)) {
					$table_trees[$string_id] = $table;
				}
				else {
					$this->_error('No table found with string_id '.$string_id);
				}
			}
		}

		if($table_trees) {
			global $PRODUCT;
			header('Content-type: text/plain');
			header('Content-Disposition: attachment; filename="table_interface_'.$PRODUCT['version'].'.dat"');
			echo "<?php return(".var_export($table_trees, true).");";
			return false;
		}
	}

	/**
	 * Processes a submitted file
	 *
	 * @param array $posted
	 * @return array
	 */
	public function insertUploadAction($posted)
	{
		if(!isset($_FILES['from_file']) ||
			!is_uploaded_file($_FILES['from_file']['tmp_name']) ||
			$_FILES['from_file']['error'] != 0) {
			$this->_error('no file uploaded to insert');
		}
		else {
			$filename = $GLOBALS['PHprefs']['tmpDir'] . '/' . uniqid(time(), true) . '.dat';
			move_uploaded_file($_FILES['from_file']['tmp_name'], $filename);
			$insert_table_trees = eval('?>'.file_get_contents($filename));
			if(!is_array($insert_table_trees)) {
				$this->_error('invalid file contents');
			}
			else {
				$_SESSION['insert_table_trees'] = $insert_table_trees;
			}
			unlink($filename);
		}

		if($this->errors()) {
			// If we get errors the following variables are invalid
			unset($insert_upload_file);
		}
		else {
			return compact('insert_table_trees');
		}
	}

	/**
	 * Process a insert
	 *
	 * @param array $posted
	 * @return array
	 */
	public function insertRunAction(array $posted)
	{
		if(!isset($_SESSION['insert_table_trees'])) {
			$this->_error("Session data not found!");
		}
		else {
			$insert_table_trees = $_SESSION['insert_table_trees'];
		}

		if(empty($posted['to_strings'])) {
			$this->_error('geen doel string_id\'s');
		}
		if(empty($posted['insert_tables'])) {
			$this->_error('geen interfaces geselecteerd');
		}

		if(!$this->errors()) {
			foreach($insert_table_trees as $tid => $table) {
				// Only insert the checked tables
				if(!in_array($tid, $posted['insert_tables']) ||
				!array_key_exists($tid, $posted['to_strings']) ||
				!array_key_exists($tid, $posted['to_tables'])) {
					continue;
				}

				$name = $posted['to_tables'][$tid];
				$string_id = $posted['to_strings'][$tid];

				if($new_tid = $this->_sync->import($table, $name, $string_id)) {
					$this->_message($string_id. ' successfully inserted!');
				}
				else {
					$this->_error($string_id. ' insert failed!');
				}
			}
			unset($insert_upload_file, $_SESSION['insert_upload_file']);
		}
	}

	/**
	 * File uploaded on the Sync tab
	 *
	 * @param array $posted
	 * @return array
	 */
	public function updateUploadAction(array $posted)
	{
		if(!isset($_FILES['from_file']) ||
			!is_uploaded_file($_FILES['from_file']['tmp_name']) ||
			$_FILES['from_file']['error'] != 0) {
			// No file upload
			$this->_error('no file uploaded to update');
		}
		else {
			$filename = $GLOBALS['PHprefs']['tmpDir'] . '/' . uniqid(time(), true) . '.dat';
			move_uploaded_file($_FILES['from_file']['tmp_name'], $filename);
			$update_trees = eval('?>'.file_get_contents($filename));
			unlink($filename);
		}

		if($this->errors()) {
			// If we get errors the following variables are invalid
			unset($update_trees);
		}
		else {
			$update_statements = array(
				'import' => array(), 'update' => array()
			);

			$current_trees = array();

			foreach($update_trees as $tid => $v) {
				if(isset($v['phoundry_table']['string_id'])) {
					$string_id = $v['phoundry_table']['string_id'];

					if($table = $this->_sync->export($string_id)) {
						if($diff = $this->_sync->diff($table, $v)) {
							$update_statements['update'][$string_id] = $v;
							$current_trees[$string_id] = $table;
						}
					}
					else {
						$update_statements['import'][$string_id] = $v;
					}
				}
			}

			$_SESSION['update_statements'] = $update_statements;
			return compact('update_statements', 'current_trees', 'update_trees');
		}
	}

	/**
	 * Run a sync
	 *
	 * @param array $posted
	 * @return array
	 */
	public function updateRunAction(array $posted)
	{
		if(!isset($_SESSION['update_statements'])) {
			$this->_error("Session data not found!");
		}

		if(empty($posted['import']) && empty($posted['update'])) {
			$this->_error('Geen records geselecteerd om te syncen');
		}

		$update_desc = !empty($posted['update_desc']);

		$update_statements = $_SESSION['update_statements'];

		if(!$this->errors()) {

			if(!empty($posted['import'])) {
				foreach($update_statements['import'] as $string_id => $table) {
					if(in_array($string_id, $posted['import'])) {
						if($new_tid = $this->_sync->import($table)) {
							$this->_message($string_id.' successfully inserted with id '.$new_tid.'!');
						}
						else {
							$this->_error($string_id.' insert failed!');
						}
					}
				}
			}

			if(!empty($posted['update'])) {
				foreach($update_statements['update'] as $string_id => $table) {
					if(in_array($string_id, $posted['update'])) {
						if($tid = $this->_sync->update($string_id, $table, $update_desc)) {
							$this->_message($string_id.' successfully updated table with id '.$tid.'!');
						}
						else {
							$this->_error($string_id.' update failed!');
						}
					}
				}
			}

			unset($update_statements);
		}
	}

	/**
	 * The current in use string_ids for the tables
	 * @return array
	 */
	public function currentStringIds()
	{
		return $this->_fetchAll(
			$this->_query("SELECT string_id FROM phoundry_table")
		);
	}

	/**
	 * Returns the errors that occured
	 * @return array
	 */
	public function errors()
	{
		return $this->_errors;
	}

	/**
	 * Returns the messages generated
	 * @return array
	 */
	public function messages()
	{
		return $this->_messages;
	}

	/**
	 * Log a error
	 *
	 * @param string $error
	 * @return PhoundrySyncGUI self
	 */
	protected function _error($error)
	{
		$this->_errors[] = $error;
		return $this;
	}

	/**
	 * Log a message
	 *
	 * @param string $message
	 * @return PhoundrySyncGUI self
	 */
	protected function _message($message)
	{
		$this->_messages[] = $message;
		return $this;
	}

	/**
	 * Execute a SQL statement
	 *
	 * @throws Exception If mysql returns a error
	 * @param string $sql
	 * @return resource|bool
	 */
	protected function _query($sql)
	{
		$res = mysql_query($sql, $this->_db);
		if (false === $res) {
			throw new Exception(
				mysql_error($this->_db), mysql_errno($this->_db)
			);
		}
		return $res;
	}

	/**
	 * Fetches all rows and put them in an array
	 *
	 * @param resource $res
	 * @return array
	 */
	protected function _fetchAll($res)
	{
		$all = array();
		while ($row = mysql_fetch_assoc($res)) {
			$all[] = $row;
		}
		return $all;
	}
}
