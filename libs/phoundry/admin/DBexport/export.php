<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	set_time_limit(0);
	ob_implicit_flush(true);

	$tables = $_POST['tables'];
	$how    = $_POST['how'];
	$asfile = isset($_POST['asfile']) ? 1 : 0;
	$what   = $_POST['what'];
	$complete_inserts = isset($_POST['complete_inserts']);
	$drop_table = isset($_POST['drop_table']);

	require "{$PHprefs['distDir']}/core/include/metabase/metabase_parser.php";
	require "{$PHprefs['distDir']}/core/include/metabase/metabase_manager.php";
	require "{$PHprefs['distDir']}/core/include/metabase/metabase_interface.php";
	require "{$PHprefs['distDir']}/core/include/metabase/xml_parser.php";

	//// Functions.

	function xmlDump($output)
	{
		echo $output;
	}

	// Return $table's CREATE definition
	// Returns a string containing the CREATE statement on success
	function mysqlGetTableDef($table)
	{
		global $drop_table, $db;

		$table = escDBquote($table);

		$schema_create = '';
		if(!empty($drop_table)) {
			$schema_create .= "DROP TABLE IF EXISTS $table;\n";
		}

		$cur = $db->Query("SHOW CREATE TABLE $table");
		$schema_create .= $db->FetchResult($cur,0,1) . ";\n";

		return $schema_create;
	}

	// Get the content of $table as a series of INSERT statements.
	// After every row, a custom callback function $handler gets called.
	// $handler must accept one parameter ($sql_insert);
	function mysqlGetTableContent($table)
	{
		global $db, $complete_inserts;

		$table = escDBquote($table);

		$page = 0;
		$limit = 1000;
		$x = 0;

		$table_list = null;
		while ($page === 0 || $x === $limit) {
			$cur = $db->Query("SELECT * FROM $table LIMIT ".($page * $limit).", $limit");
			$i = 0;
			for ($x = 0; !$db->EndOfResult($cur); $x++)
			{
				if (!$table_list) {
					$table_list = '(';

					$nrf = $db->NumberOfColumns($cur);
					for($j=0; $j<$nrf;$j++) {
						if ($j !== 0) {
							$table_list .= ', ';
						}
						$table_list .= $db->FetchColumnName($cur,$j);
					}

					$table_list = substr($table_list,0,-2);
					$table_list .= ')';
				}

				if ($complete_inserts)
					echo "INSERT INTO $table $table_list VALUES (";
				else
					echo "INSERT INTO $table VALUES (";

				  for($j=0; $j<$nrf; $j++)
				  {
					  if ($j !== 0) {
						  echo ', ';
					  }
						$value = $db->FetchResult($cur,$x,$j);
						if (!isset($value))
							echo 'NULL';
						elseif($value != '')
							echo "'" . escDBquote($value) . "'";
						else
							echo "''";
				  }
				  echo ');'."\n";
				  $i++;
			 }
			$page += 1;
		 }
		 return (true);
	}

	function mysqlDumpTable($table) {
		global $what;

		print "\n#\n";
		print "# Table structure for table '$table'\n";
		print "#\n\n";

		print mysqlGetTableDef($table) . "\n";

		if ($what == 'data') {
			print "\n#\n";
			print "# Dumping data for table $table\n";
			print "#\n\n";
			mysqlGetTableContent($table, "mysqlDump");
		}
	}

	//// End Functions.

	if ($how == 'xml') {
		// XML Export
		$options = isset($PHprefs['DBoptions']) ? $PHprefs['DBoptions'] : array();
		$arguments = array(
				'Type'=>$PHprefs['DBtype'],
				'Host'=>$PHprefs['DBserver'],
				'User'=>$PHprefs['DBuserid'],
				'Password'=>$PHprefs['DBpasswd'],
				'Port'=>$PHprefs['DBport'],
				'IncludePath'=>"{$PHprefs['distDir']}/core/include/metabase/",
				'Database'=>$PHprefs['DBname'],
				'Options'=>$options
		);

		$manager = new metabase_manager_class;

		if(strlen($error=$manager->GetDefinitionFromDatabase($arguments,0,$tables))==0)
		{
			if ($asfile) {
				header("Content-disposition: attachment; filename={$PHprefs['DBname']}.xml");
				header('Content-type: application/octet-stream');
			}
			else {
				header('Content-type: text/xml');
			}

			$error=$manager->DumpDatabase(array(
				'Output'=>'xmlDump',
				'EndOfLine'=>"\n"),
				$what == 'data'
			);
		}
		else
			echo "Error: $error\n";
	}
	else if ($how == 'mysql') {
		// MySQL export
		if ($asfile) {
			header("Content-disposition: attachment; filename={$PHprefs['DBname']}.sql");
			header('Content-type: application/octetstream');
		}
		else {
			header("Content-type: text/plain");
		}

		print "# Phoundry MySQL-Dump\n";
		print "# http://www.phoundry.com\n";
		print "# Dump generated on " . date('d F Y', time()) . "\n";
		print "# --------------------------------------------------------\n\n";

		for ($x = 0; $x < count($tables); $x++) {
			mysqlDumpTable($tables[$x]);
		}
	}
