<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Database Export</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

function exportAs(what) {
	if (what == 'xml') {
		$('#optionsDiv').html('<input type="checkbox" name="asfile" value="1" /> Save as file');
	}
	else if (what == 'mysql') {
		$('#optionsDiv').html('<input type="checkbox" name="asfile" value="1" /> Save as file <input type="checkbox" name="drop_table" value="1" /> Add \'drop table\' <input type="checkbox" name="complete_inserts" value="1" /> Complete inserts');
	}
}

function checkTables(el) {
	for (var x = 0; x < el.options.length; x++) {
		if (el.options[x].selected)
			return true;
	}
	alert('Select some tables!');
	return false;
}

//]]>
</script>
</head>
<body class="frames">
<form action="export.php" method="post" onsubmit="return checkTables(_D.forms[0]['tables[]'])">

<div id="headerFrame" class="headerFrame">
<?= getHeader('Database Export'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
	
<fieldset><legend><b>Export as</b></legend>
<select name="how" onchange="exportAs(this.options[this.selectedIndex].value)">
<option value="xml">XML</option>
<?php if ($PHprefs['DBtype'] == 'mysql') { ?>
<option value="mysql">MySQL</option>
<?php } ?>
</select>
</fieldset>

<fieldset><legend><b>Select tables to export</b> (<a href="#" onclick="selectAll(_D.forms[0]['tables[]']);return false">select all</a>)</legend>
<select name="tables[]" multiple="multiple" size="15">
<?php
	$db->ListTables($DBtables)
		or trigger_error('Cannot list tables: ' . $db->Error(), E_USER_ERROR);
	foreach($DBtables as $table) {
		print '<option value="' . $table . '">' . $table . '</option>';
	}
?>
</select>
</fieldset>

<fieldset><legend><b>Export what?</b></legend>
<input type="radio" name="what" value="structure" checked="checked">Structure only
<input type="radio" name="what" value="data">Structure and data
</fieldset>
	
<fieldset><legend><b>Options</b></legend>
<div id="optionsDiv">
<input type="checkbox" name="asfile" value="1" /> Save as file
</div>
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Export" onclick="_D.forms[0].target=(event.shiftKey)?'_blank':'_self'" />
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
