<!DOCTYPE>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Database Interface</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<style type="text/css">

</style>
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[


//]]>
</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Database Structure Sync'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<p>
<form action="" method="post">
	<input type="submit" name="dump" value="Download Dump">
</form>
</p>

<p>
<form action="" method="post" enctype="multipart/form-data">
	<input type="file" name="sync_file" />
	<input type="submit" name="upload" value="Upload and show Diff" />
</form>
</p>

<?if(isset($diff)):?>
<p>
<form action="" method="post">
	<fieldset>
		<legend><strong>New tables</strong></legend>
		<?if(!empty($diff['create'])):?>
		<?foreach($diff['create'] as $sql):?>
		<div>
		<input name="create[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" checked="checked" />
		<?=htmlspecialchars($sql)?>
		</div>
		<?endforeach?>
		<?else:?>
		No new tables
		<?endif?>
	</fieldset>
	
	<fieldset>
		<legend><strong>Altered tables (new/changed colomns)</strong></legend>
		<?if(!empty($diff['update'])):?>
		<?foreach($diff['update'] as $sql):?>
		<div>
		<input name="update[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" checked="checked" />
		<?=htmlspecialchars($sql)?>
		</div>
		<?endforeach?>
		<?else:?>
		No updated tables
		<?endif?>
	</fieldset>
	
	<fieldset>
		<legend><strong>Altered tables (deleted columns)</strong></legend>
		<?if(!empty($diff['delete'])):?>
		<?foreach($diff['delete'] as $sql):?>
		<div>
		<input name="delete[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" />
		<?=htmlspecialchars($sql)?>
		</div>
		<?endforeach?>
		<?else:?>
		No updated tables
		<?endif?>
	</fieldset>
	
	<fieldset>
		<legend><strong>Deleted tables</strong></legend>
		<?if(!empty($diff['drop'])):?>
		<?foreach($diff['drop'] as $sql):?>
		<div>
		<input name="drop[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" />
		<?=htmlspecialchars($sql)?>
		</div>
		<?endforeach?>
		<?else:?>
		No updated tables
		<?endif?>
	</fieldset>
	<?if(!empty($diff['create']) || !empty($diff['update']) || !empty($diff['delete']) || !empty($diff['drop'])):?>
	<input type="submit" name="execute" value="Run queries" />
	<?endif?>
</form>
</p>
<?elseif(isset($sqls)):?>
<fieldset>
	<legend><strong>Executed sql statements</strong></legend>
	<div><?=implode(';</div><div>', $sqls)?>;</div>
</fieldset>
<?endif?>


<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
   <td>&nbsp;</td>
   <td align="right">
   <button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
   </td>
</tr></table>
</div>
</body>
</html>
