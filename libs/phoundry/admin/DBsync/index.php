<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
checkAccess('admin');
	
require 'DB_Sync.php';

switch (strtolower($PHprefs['charset'])) {
	case 'latin':
	case 'latin1':
	case 'iso-8859-1':
		$collation = 'latin1';
		break;
		
	case 'utf-8':
	case 'utf8':
	default:
		$collation = 'utf8';
		break;
}

if (!is_resource($db->connection)) {
	
}

mysql_query("SET NAMES ".$collation, $db->connection);

if(isset($_POST['dump'])) {
	$sync = new DB_Sync($db->connection);
	$c = $sync->dump();

    global $PHprefs;
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="'.$PHprefs['DBname'].'.sql"');
	echo implode(";\n\n", $c).";\n";
	exit;
}

if(isset($_POST['upload']) && isset($_FILES['sync_file']) &&
0 === $_FILES['sync_file']['error']) {
					
	$sync = new DB_Sync($db->connection);
	$to = explode(";\n\n", rtrim(file_get_contents($_FILES['sync_file']['tmp_name']), ";\n"));
	$diff = $sync->diff($to);
}

if(isset($_POST['execute'])) {
	$sqls = array();
	
	if(isset($_POST['create']) && is_array($_POST['create'])) {
		$sqls = array_merge($sqls, $_POST['create']);
	}
	
	if(isset($_POST['update']) && is_array($_POST['update'])) {
		$sqls = array_merge($sqls, $_POST['update']);
	}
	
	if(isset($_POST['truncate']) && is_array($_POST['truncate'])) {
		$sqls = array_merge($sqls, $_POST['truncate']);
	}
	
	if(isset($_POST['delete']) && is_array($_POST['delete'])) {
		$sqls = array_merge($sqls, $_POST['delete']);
	}
	
	if(isset($_POST['drop']) && is_array($_POST['drop'])) {
		$sqls = array_merge($sqls, $_POST['drop']);
	}

	if (function_exists('untaint')) {
		untaint($sqls);
	}
	if($sqls) {
		foreach($sqls as $sql) {
			$res = mysql_query($sql, $db->connection);
			if (false === $res) {
				throw new Exception(
					mysql_error($db->connection), mysql_errno($db->connection)
				);
			}
		}
	}
}
?>

<!DOCTYPE>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Database Interface</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<style type="text/css">

</style>
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Database Structure Sync'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<form action="" method="post">
    <p><input type="submit" name="dump" value="Download Dump"></p>
</form>

<form action="" method="post" enctype="multipart/form-data">
    <p>
		<input type="file" name="sync_file" />
		<input type="submit" name="upload" value="Upload and show Diff" />
    </p>
</form>

<?if(isset($diff)):?>
<form action="" method="post">
    <p>
    <fieldset>
		<legend><strong>New tables</strong></legend>
		<?if(!empty($diff['create'])):?>
		<?foreach($diff['create'] as $sql):?>
		<div>
		<input name="create[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" checked="checked" />
		<?=htmlspecialchars($sql)?>;
		</div>
		<?endforeach?>
		<?else:?>
		No new tables
		<?endif?>
	</fieldset>
	
	<fieldset>
		<legend><strong>Altered tables (new/changed colomns)</strong></legend>
		<?if(!empty($diff['update'])):?>
		<?foreach($diff['update'] as $sql):?>
		<div>
		<input name="update[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" checked="checked" />
		<?=htmlspecialchars($sql)?>;
		</div>
		<?endforeach?>
		<?else:?>
		No updated tables
		<?endif?>
	</fieldset>
	
	<fieldset>
		<legend><strong>Altered tables (changed colomns that might truncate the content)</strong></legend>
		<?if(!empty($diff['truncate'])):?>
		<?foreach($diff['truncate'] as $sql):?>
		<div>
		<input name="truncate[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" />
		<?=htmlspecialchars($sql)?>;
		</div>
		<?endforeach?>
		<?else:?>
		No updated tables
		<?endif?>
	</fieldset>
	
	<fieldset>
		<legend><strong>Altered tables (deleted columns)</strong></legend>
		<?if(!empty($diff['delete'])):?>
		<?foreach($diff['delete'] as $sql):?>
		<div>
		<input name="delete[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" />
		<?=htmlspecialchars($sql)?>;
		</div>
		<?endforeach?>
		<?else:?>
		No updated tables
		<?endif?>
	</fieldset>
	
	<fieldset>
		<legend><strong>Deleted tables</strong></legend>
		<?if(!empty($diff['drop'])):?>
		<?foreach($diff['drop'] as $sql):?>
		<div>
		<input name="drop[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" />
		<?=htmlspecialchars($sql)?>;
		</div>
		<?endforeach?>
		<?else:?>
		No updated tables
		<?endif?>
	</fieldset>
	<?if(!empty($diff['create']) || !empty($diff['update']) || !empty($diff['truncate']) || !empty($diff['delete']) || !empty($diff['drop'])):?>
	<input type="submit" name="execute" value="Run queries" />
	<?endif?>
    </p>
</form>
<?elseif(isset($sqls)):?>
<fieldset>
	<legend><strong>Executed sql statements</strong></legend>
	<div><?=implode(';</div><div>', $sqls)?>;</div>
</fieldset>
<?endif?>


<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
   <td>&nbsp;</td>
   <td align="right">
   <button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
   </td>
</tr></table>
</div>
</body>
</html>
