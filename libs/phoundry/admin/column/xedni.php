<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	// Determine tablename:
	$TID = (int)$_GET['TID'];
	$sql = "SELECT name FROM phoundry_table WHERE id = $TID";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$tableName = $db->FetchResult($cur,0,'name');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Columns</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript">

var activeRow = null, CID = null, rowType = rowExtra = null;

function setRow(row, type, extra) {
	if (activeRow)
		activeRow['elem'].className = activeRow['class'];
	activeRow = [];
	activeRow['elem']  = row;
	activeRow['class'] = row.className;
	row.className = 'active';
	
	rowType = type; rowExtra = extra;
	if (rowType == 'DBcol') {
		$('#addBut,#dropBut').prop('disabled',false);
		$('#editBut,#delBut').prop('disabled',true);
	}
	else if (rowType == 'PHfake') {
		$('#addBut,#dropBut').prop('disabled',true);
		$('#editBut,#delBut').prop('disabled',false);
	}
	else {
		$('#addBut').prop('disabled',true);
		$('#editBut,#delBut,#dropBut').prop('disabled',false);
	}
	if (/^[0-9]+$/.test(extra)) {
		_W.status = extra;
	}
}

function addButton() {
	if (rowType == 'DBcol')
		_D.location='edit.php?TID=<?= $TID ?>&columnName='+rowExtra;
}

function editButton() {
	if (rowType == 'PHcol')
		_D.location='edit.php?TID=<?= $TID ?>&CID='+rowExtra; 
	else if (rowType == 'PHxref')
		_D.location='xref/edit.php?TID=<?= $TID ?>&CID='+rowExtra; 
	else if (rowType == 'PHfake') 
		_D.location='fakeColumn/edit.php?TID=<?= $TID ?>&CID='+rowExtra; 
}

function deleteButton() {
	if (confirm('Are you sure you want to delete this column?'))
		_D.location='delete.php?TID=<?= $TID ?>&CID='+rowExtra;
}

function dropButton() {
	if (confirm('Are you sure you want to drop this column?')) {
		if (rowType == 'DBcol' || rowType == 'PHcol')
			_D.location='drop.php?TID=<?= $TID ?>&CID='+rowExtra;
		else if (rowType == 'PHxref')
			_D.location='xref/drop.php?TID=<?= $TID ?>&CID='+rowExtra;
	}
}

</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader("Table $tableName") ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
	<input type="button" disabled="disabled" id="addBut" value="Add" onclick="addButton()" />
	<img src="../../core/pics/div.gif" alt="|" width="3" height="18" />
	<input type="button" disabled="disabled" id="editBut" value="Edit" onclick="editButton()" />
	<input type="button" disabled="disabled" id="delBut" value="Delete" onclick="deleteButton()" />
	<?php if ($PHprefs['DBtype'] == 'mysql' || $PHprefs['DBtype'] == 'mssql') { ?>
	<input type="button" disabled="disabled" id="dropBut" value="Drop" onclick="dropButton()" />
	<?php } ?>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<table id="recordTBL" class="sort-table" cellspacing="0">
<thead>
<tr>
	<th>Column name</th>
	<th>String Id</th>
	<th>Order</th>
	<th>Required?</th>
	<th>In overview?</th>
	<th>Description</th>
	<th>Datatype</th>
	<th>Inputtype</th>
</tr>
</thead>
<tbody>
<?php
	$DBcolnames = $PHcolnames = array();
	$db->ListTableFields($tableName, $DBcolnames)
		or trigger_error("Cannot list fields for table $tableName: " . $db->Error());

	$cur = $db->Query("SELECT id,name FROM phoundry_column WHERE datatype != 'DTfake' AND table_id = $TID ORDER BY order_by");
	for ($x = 0; !$db->EndOfResult($cur); $x++)
		$PHcolnames[$db->FetchResult($cur,$x,'id')] = $db->FetchResult($cur,$x,'name');

	$x = 0;
?>

<tr><td colspan="8"><h2>Phoundry columns</h2></td></tr>

<?php
	foreach ($PHcolnames as $PHcolname) {
		$sql = "SELECT id, string_id, order_by, description, required, searchable, datatype, inputtype FROM phoundry_column WHERE table_id = $TID AND name = '" . escDBquote($PHcolname) . "'";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if ($db->EndOfResult($cur))
			continue;
		if (preg_match('/^DTX/', $db->FetchResult($cur,0,'datatype')))
			continue;

		print '<tr class="' . (($x++ & 1) ? 'even' : 'odd') . '" onclick="setRow(this,\'PHcol\',\'' . $db->FetchResult($cur,0,'id') . '\')" ondblclick="editButton()">';

		print '<td>' . $PHcolname . '</td>';
		print '<td>' . $db->FetchResult($cur,0,'string_id') . '</td>';
		print '<td>' . $db->FetchResult($cur,0,'order_by') . '</td>';
		print '<td>' . ($db->FetchResult($cur,0,'required') == 0 ? 'No' : 'Yes') . '</td>';
		print '<td>' . (in_array((int)$db->FetchResult($cur,0,'searchable'), array(1,3,5)) ? 'Yes' : 'No') . '</td>';
		$description = $db->FetchResult($cur,0,'description');
		if (PH::is_serialized($description) && ($tmp = @unserialize($description)) !== false) {
			$description = $tmp[$PHprefs['languages'][0]];
		}
		print '<td>' . $description . '</td>';
		print '<td>' . substr($db->FetchResult($cur,0,'datatype'),2) . '</td>';
		print '<td>' . substr($db->FetchResult($cur,0,'inputtype'),2) . '</td>';
		if (!in_array($PHcolname, $DBcolnames))
			print '<td><img src="../../core/pics/ipt_error.gif" alt="This column is no longer available in the database!" /></td>';
		print "</tr>\n";
	}
?>
<tr>
<td colspan="3"><h2>Phoundry cross references</h2></td>
<td colspan="5">
	<form action="xref/edit.php" method="get">
	<input type="hidden" name="TID" value="<?= $TID ?>" />
	<select name="xref">
	<?php
		$uneditable_tmp = array('phoundry_column', 'phoundry_group_column', 'phoundry_image', 'phoundry_image_cat', 'phoundry_notify', 'phoundry_table', 'phoundry_user_shortcut', 'phoundry_menu');
		$uneditable = array();

		if (!isset($PHprefs['DBschemas'])) {
			//$PHprefs['DBschemas'] = array($PHprefs['DBname']);
			$uneditable = $uneditable_tmp;
			$db->ListTables($DBtables)
				or trigger_error('Cannot list tables: ' . $db->Error(), E_USER_ERROR);
		} else {
			foreach($PHprefs['DBschemas'] as $schema) {
				foreach ($uneditable_tmp as $ut) {
					$uneditable[] = $schema . "." . $ut;
				}

				$sql = "SHOW TABLES FROM `{$schema}`";
				$cur = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				for ($x = 0; !$db->EndOfResult($cur); $x++) {
					$tableName = $db->FetchResult($cur,$x,0);
					$DBtables[] = $schema.".".$tableName;
				}
			}
		}
		
		$options = array(
			'new' => array(),
			'existing' => array()
		);
		foreach($DBtables as $DBtable) {

			if (in_array($DBtable, $uneditable))
				continue;

			$sql = "SELECT id, name FROM phoundry_table WHERE name = '" . escDBquote($DBtable) . "'";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$new = $db->EndOfResult($cur);
			$options[($new ? 'new' : 'existing')][] = '<option value="' . $DBtable . '">' . $DBtable . '</option>';
		}
	?>
		<?php if ($options['new']) {?>
		<optgroup label="Not in Phoundry">
			<?php echo implode('', $options['new']) ?>
		</optgroup>
		<?php } ?>
		<?php if ($options['existing']) {?>
		<optgroup label="Phoundry Tables">
			<?php echo implode('', $options['existing']) ?>
		</optgroup>
		<?php } ?>
	</select>
	<input type="submit" value="Add" />
	</form>
</td>
</tr>
<?php
	foreach (array_unique($PHcolnames) as $PHcolname) {
		$sql = "SELECT id, string_id, order_by, description, required, searchable, datatype, inputtype FROM phoundry_column WHERE table_id = $TID AND name = '" . escDBquote($PHcolname) . "' AND datatype LIKE 'DTX%' ORDER BY order_by";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($i = 0; !$db->EndOfResult($cur); $i++) {

		print '<tr class="' . (($x++ & 1) ? 'even' : 'odd') . '" onclick="setRow(this,\'PHxref\',\'' . $db->FetchResult($cur,$i,'id') . '\')" ondblclick="editButton()">';

		print '<td>' . $PHcolname . '</td>';
		print '<td>' . $db->FetchResult($cur,$i,'string_id') . '</td>';
		print '<td>' . $db->FetchResult($cur,$i,'order_by') . '</td>';
		print '<td>' . ($db->FetchResult($cur,$i,'required') == 0 ? 'No' : 'Yes') . '</td>';
		print '<td>' . ($db->FetchResult($cur,$i,'searchable') == 0 ? 'No' : 'Yes') . '</td>';
		$description = $db->FetchResult($cur,$i,'description');
		if (PH::is_serialized($description) && ($tmp = @unserialize($description)) !== false) {
			$description = $tmp[$PHprefs['languages'][0]];
		}
		print '<td>' . $description . '</td>';
		print '<td>' . substr($db->FetchResult($cur,$i,'datatype'),2) . '</td>';
		print '<td>' . substr($db->FetchResult($cur,$i,'inputtype'),3) . '</td>';
		print "</tr>\n";
		}
	}
?>
<tr><td colspan="8"><h2>Database columns</h2></td></tr>
<?php
	foreach($DBcolnames as $DBcolname) {
		if (in_array($DBcolname, $PHcolnames))
			continue;
		$sql = "SELECT id, order_by, description, required, datatype, inputtype FROM phoundry_column WHERE table_id = $TID AND name = '" . escDBquote($DBcolname) . "'";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$redactable = !$db->EndOfResult($cur);

		print '<tr class="' . (($x++ & 1) ? 'even' : 'odd') . '" onclick="setRow(this,\'DBcol\',\'' . $DBcolname . '\')" ondblclick="editButton()">';

		print '<td>' . $DBcolname . '</td>';
		if ($redactable) {
			print '<td>' . $db->FetchResult($cur,0,'order_by') . '</td>';
			$description = $db->FetchResult($cur,0,'description');
			if (PH::is_serialized($description) && ($tmp = @unserialize($description)) !== false) {
				$description = $tmp[$PHprefs['languages'][0]];
			}
			print '<td>' . $description . '</td>';
			print '<td>' . ($db->FetchResult($cur,0,'required') == 0 ? 'No' : 'Yes') . '</td>';
			print '<td>' . substr($db->FetchResult($cur,0,'datatype'),2) . '</td>';
			print '<td>' . substr($db->FetchResult($cur,0,'inputtype'),2) . '</td>';
		}
		print "</tr>\n";
	}
?>
<tr><td colspan="3"><h2>Fake columns</h2></td><td colspan="3">
	<form action="fakeColumn/edit.php" method="get">
	<input type="hidden" name="TID" value="<?= $TID ?>" />
	<input type="submit" value="Add" />
	</form>
	</td></tr>
<?php
	$sql = "SELECT id, name, string_id, order_by, description, required, datatype, inputtype FROM phoundry_column WHERE table_id = $TID AND datatype = 'DTfake' ORDER BY order_by";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($i = 0; !$db->EndOfResult($cur); $i++) {

		print '<tr class="' . (($x++ & 1) ? 'even' : 'odd') . '" onclick="setRow(this,\'PHfake\',\'' . $db->FetchResult($cur,$i,'id') . '\')" ondblclick="editButton()">';
		print '<td>' . $db->FetchResult($cur,$i,'name') . '</td>';
		print '<td>' . $db->FetchResult($cur,$i,'string_id') . '</td>';
		print '<td>' . $db->FetchResult($cur,$i,'order_by') . '</td>';
		print '<td>N/A</td>';
		print '<td>Yes</td>';
		$description = $db->FetchResult($cur,$i,'description');
		if (PH::is_serialized($description) && ($tmp = @unserialize($description)) !== false) {
			$description = $tmp[$PHprefs['languages'][0]];
		}
		print '<td>' . $description . '</td>';
		print '<td>N/A</td>';
		print '<td>N/A</td>';
		print '</tr>';
	}
?>
</tbody>
</table>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
   <table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<button type="button" onclick="_D.location='index.php'">Cancel</button>
	</td>
	</tr></table>
</div>

</body>
</html>
