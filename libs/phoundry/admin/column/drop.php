<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');

	$TID = (int)$_GET['TID']; $CID = $_GET['CID'];

	// Determine tablename:
	$sql = "SELECT name FROM phoundry_table WHERE id = $TID";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$tableName = $db->FetchResult($cur,0,'name');


	if (preg_match('/^[0-9]+$/', $CID)) {
		// Determine column name:
		$sql = "SELECT name FROM phoundry_column WHERE id = $CID";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$columnName = $db->FetchResult($cur,0,'name');

		@$db->Query("DELETE FROM phoundry_group_column WHERE column_id = $CID");
		$sql = "DELETE FROM phoundry_column WHERE id = $CID";
		$res = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}
	else {
		$columnName = $CID;
	}

	$db->DropColumn($tableName, escDBquote($columnName));

	header('Status: 302 moved');
	header('Location: xedni.php?TID=' . $TID);
?>
