<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require "{$PHprefs['distDir']}/core/include/common.php";
	require "{$PHprefs['distDir']}/core/include/Column.php";
	checkAccess('admin');

$colName = filter_input(INPUT_GET, 'colName', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tableName = filter_input(INPUT_GET, 'tableName', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$datatype = filter_input(INPUT_GET, 'datatype', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$inputtype = filter_input(INPUT_GET, 'inputtype', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

	$column = new Column((int) $_GET['id'], $colName, $tableName, -1, $_GET['required'], $datatype, $inputtype);

	header('Expires: 0'); // Prevent caching
	header('Content-type: application/json; charset=' . $PHprefs['charset']);
	print PH::php2javascript($column->inputtype->getExtra());
