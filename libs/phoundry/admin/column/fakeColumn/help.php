<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Fake column help - Edit</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body>
	<!-- Content -->
	<fieldset><legend><b>Help</b></legend>
	Fake columns are &quot;columns&quot; that are not available in the database, but are displayed in Phoundry as such.
	<br />
	Fake columns allow you to show some extra information for a record, or to display some action-buttons or links per record.
	Fake columns can never be edited (insert/update/delete), they can only be viewed. 
	<br />
	Fake columns are available on the records-page (records.php) and/or on the view-page (preview.php). 
	<ul>
	<li>If you want a fake column to be visible on the records-page (where many records are displayed), you need to enter the fields 'Javascript code (for records page)' and 'HTML/PHP code (for records page)'.</li>
	<li>If you want a fake column to be visible on the view-page (where a single record is displayed), you need to enter the field 'HTML/PHP code (for view page)' and 'HTML/PHP code (for records page)'.</li>
	</ul>
	</fieldset>

	<fieldset><legend><b>Example Javascript code</b></legend>
	<pre style="margin:0;padding:0">
&lt;?php
   function PHPfunction($arg) {
      print $arg;
   }
?&gt;
function JSfunction(rid) {
   alert(rid);
} 
	</pre>
	</fieldset>

	<fieldset><legend><b>Example HTML/PHP code</b></legend>
	<pre style="margin:0;padding:0">
Click:
&lt;a href=&quot;#&quot; onclick=&quot;JSfunction(&lt;?= $RID ?&gt;);return false&quot;&gt;
&lt;img src=&quot;&lt;?= $PHprefs['url'] ?&gt;/core/icons/information.png&quot; border=&quot;0&quot; /&gt;
&lt;/a&gt;
&lt;?php print PHPfunction($RID); ?&gt;
	</pre>
	</fieldset>

	<p align="right">
	<button type="button" onclick="parent.killPopup()">Close</button>
	</p>
	<!-- /Content -->

</body>
</html>
