<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = (int) $_GET['TID'];
	$CID = isset($_GET['CID']) ? (int) $_GET['CID'] : null;

	// Determine tablename:
	$sql = "SELECT name FROM phoundry_table WHERE id = $TID";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$tableName = $db->FetchResult($cur,0,'name');

	$description = array();
	foreach($PHprefs['languages'] as $lang) {
		$description[$lang] = '';
	}

    $string_id = null;
	if (!is_null($CID)) {
		$sql = "SELECT name, string_id, description, order_by, datatype_extra FROM phoundry_column WHERE id = $CID";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$db->FetchResultAssoc($cur,$fakeCol,0);

		$description = PH::is_serialized($fakeCol['description']) ? @unserialize($fakeCol['description']) : $fakeCol['description'];
		if ($description === false) {
			$description = array();
			foreach($PHprefs['languages'] as $lang)
				$description[$lang] = $db->FetchResult($cur,0,'description');
		}
		$fakeCol['description'] = $description;
		$fakeCol['datatype_extra'] = unserialize($fakeCol['datatype_extra']);
        $string_id = $fakeCol['string_id'];
	}
	else {
		$cur = $db->Query("SELECT MAX(order_by) + 10 AS max_order FROM phoundry_column WHERE table_id = $TID");
		if (!$db->EndOfResult($cur))
			if (!$db->ResultIsNull($cur,0,'max_order'))
				$order_by = $db->FetchResult($cur,0,'max_order');
		if (empty($order_by))
			$order_by = 10;

		$fakeCol = array('name'=>'', 'description'=>$description, 'order_by'=>$order_by, 'datatype_extra'=>array('html_code'=>'','js_code'=>'', 'view_html_code'=>'', 'edit_html_code'=>''));
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Fake column - Edit</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../../../core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

function winPrio(e, tid) {
   makePopup(e,220,150,'Priorities','../prio.php?TID='+tid);
}

function checkFormExtra(F) {
	if (!/^[a-z]\w+$/i.test(F['name'].value)) {
		alert('The name can only contain letters and digits!');
		return false;
	}
	return true;
}

//]]>
</script>
</head>
<body class="frames">
<form action="editRes.php" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="TID" value="<?= $TID ?>" />
<?php if (!is_null($CID)) { ?>
<input type="hidden" name="CID" value="<?= $CID ?>" />
<?php } ?>

<div id="headerFrame" class="headerFrame">
<?= getHeader("Table $tableName, Fake column - " . (is_null($CID) ? 'Add' : 'Edit')); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b>The <span class="req">red</span> fields are required!</b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<fieldset><legend><img class="icon" src="../../../core/icons/help.png" alt="Help" /> <b>Help</b></legend>
<a href="help.php" class="popupWin" title="Fake column help|-100|400">Click here for help.</a>
</fieldset>

<fieldset><legend><b class="req">Name</b></legend>
<input type="text" name="name" size="60" maxlength="80" alt="1|string" value="<?= PH::htmlspecialchars($fakeCol['name']) ?>" />
</fieldset>

<fieldset><legend><b class="req">String Id</b></legend>
    <div class="miniinfo">Unique string representation of this column. This is used for syncing tables.</div>
    <input type="text" name="string_id" alt="1|text" size="40" maxlength="80" value="<?=htmlspecialchars($string_id)?>" />
</fieldset>

<fieldset><legend><b class="req">Order</b> (<a href="#" onclick="winPrio(event, <?= $TID ?>);return false">priorities</a>)</legend>
<input type="text" name="order_by" alt="1|number" size="10" maxlength="10" value="<?= $fakeCol['order_by'] ?>" />
</fieldset>

<fieldset><legend><b class="req">Description</b></legend>
<?php
	foreach($PHprefs['languages'] as $lang)
		print '<input type="text" alt="1|text" name="description_' . $lang . '" size="60" maxlength="80" value="' . PH::htmlspecialchars(isset($fakeCol['description'][$lang]) ? $fakeCol['description'][$lang] : '') . '" /> (' . $lang . ')<br />';
?>
</fieldset>

<fieldset><legend><b>Javascript code (for records page)</b></legend>
<div class="miniinfo">This code is included <i>once</i>, in the Javascript section of the page <tt>&lt;head&gt;</tt>.</div>
<textarea name="js_code" rows="10" cols="60" spellcheck="false"><?= PH::htmlspecialchars($fakeCol['datatype_extra']['js_code']) ?></textarea>
</fieldset>

<fieldset><legend><b>HTML/PHP code (for records page)</b></legend>
<div class="miniinfo">
This code is included <i>many</i> times, once per record.<br />
You can combine HTML and PHP code here. jQuery is available.<br />
Within PHP code, you have access to the following (global) variables: <tt>$db, $PHprefs, $PHSes, $_POST, $_GET, $_SERVER</tt> etc.
<br />The id of the 'current' table is available as <tt>$TID</tt>.
<br />The id of the 'current' record is available as <tt>$RID</tt>.
</div>
<textarea name="html_code" rows="10" cols="60" spellcheck="false"><?= PH::htmlspecialchars($fakeCol['datatype_extra']['html_code']) ?></textarea>
</fieldset>

<fieldset><legend><b>HTML/PHP code (for view page)</b></legend>
<div class="miniinfo">
You can combine HTML and PHP code here. jQuery is available.<br />
Within PHP code, you have access to the following (global) variables: <tt>$db, $PHprefs, $PHSes, $_POST, $_GET, $_SERVER</tt> etc.
<br />The id of the 'current' table is available as <tt>$TID</tt>.
<br />The id of the 'current' record is available as <tt>$RID</tt>.
</div>
<textarea name="view_html_code" rows="10" cols="60" spellcheck="false"><?= PH::htmlspecialchars(isset($fakeCol['datatype_extra']['view_html_code']) ? $fakeCol['datatype_extra']['view_html_code'] : '') ?></textarea>
</fieldset>

<fieldset><legend><b>HTML/PHP code (for edit page)</b></legend>
<div class="miniinfo">
You can combine HTML and PHP code here. jQuery is available.<br />
Within PHP code, you have access to the following (global) variables: <tt>$db, $PHprefs, $PHSes, $_POST, $_GET, $_SERVER</tt> etc.
<br />The id of the 'current' table is available as <tt>$TID</tt>.
<br />The id of the 'current' record is available as <tt>$RID</tt>.
</div>
<textarea name="edit_html_code" rows="10" cols="60" spellcheck="false"><?= PH::htmlspecialchars(isset($fakeCol['datatype_extra']['edit_html_code']) ? $fakeCol['datatype_extra']['edit_html_code'] : '') ?></textarea>
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Submit" />
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='../xedni.php?TID=<?= $TID ?>'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
