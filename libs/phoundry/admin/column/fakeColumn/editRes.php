<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once "{$PHprefs['distDir']}/core/include/Column.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = $_POST['TID'];
	$CID = isset($_POST['CID']) ? (int)$_POST['CID'] : null;
	
	$order_by    = $_POST['order_by'];
	$name        = $_POST['name'];
    $string_id = $_POST['string_id'];

	// description is multi-language:
	if (count($PHprefs['languages']) == 1) {
		$description = trim($_POST['description_' . $PHprefs['languages'][0]]);
	}
	else {
		$descs = array();
		foreach($PHprefs['languages'] as $lang) {
			$descs[$lang] = trim($_POST['description_' . $lang]);
		}
		$description = serialize($descs);
	}

	$errors = array();
	if ((int)$order_by < 0 || (int)$order_by > 99999)
		$errors[] = 'Order has to be between 0 and 99999!';

	// Check if the 'order' is unique:
	$sql = "SELECT name FROM phoundry_column WHERE table_id = $TID AND order_by = $order_by";
	if (!is_null($CID)) $sql .= " AND id != $CID";
	$cur = $db->Query($sql);
	if (!$db->EndOfResult($cur)) 
		$errors[] = "Another column (" . $db->FetchResult($cur,0,"name") . ") already has order $order_by! (the order <b>has</b> to be unique)";

	$datatype_extra = array(
		'html_code' => $_POST['html_code'],
		'view_html_code' => $_POST['view_html_code'],
		'edit_html_code' => $_POST['edit_html_code'],
		'js_code'   => $_POST['js_code']
	);

	if (count($errors) == 0) {
		if (is_null($CID)) {
			$sql = "INSERT INTO phoundry_column (table_id, order_by, name, string_id, description, datatype, datatype_extra, inputtype, required, searchable, info) VALUES ($TID, $order_by, '" . escDBquote($name) . "', '" . escDBquote($string_id) . "', '" . escDBquote($description) . "', 'DTfake', '" . escDBquote(serialize($datatype_extra)) . "', 'ITfake', 0, 1, '')";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$newId = $db->GetLastInsertId('phoundry_column', 'id');
		}
		else {
			$sql = "UPDATE phoundry_column SET order_by = $order_by, name = '" . escDBquote($name) . "', string_id = '" . escDBquote($string_id) . "', description = '" . escDBquote($description) . "', datatype_extra = '" . escDBquote(serialize($datatype_extra)) . "' WHERE id = $CID";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}
		
		header('Status: 302 moved');
		header("Location: ../xedni.php?TID=$TID");
		exit;
	}
	else {
		$errMsg = '<ul>';
		foreach($errors as $error)
			$errMsg .= '<li>' . $error . '</li>';
		$errMsg .= '</ul>';
		trigger_error($errMsg, E_USER_ERROR);
	}
