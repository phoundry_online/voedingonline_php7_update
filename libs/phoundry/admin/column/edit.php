<?php
	/**
	 * Explanation of meaning phoundry_column.searchable
	 * 
	 *   Searchable   | In_overview | DB-value
	 *   =============+=============+=========
	 *   enabled      |    hide     |    0
	 *   enabled      |    show     |    1
	 *   disabled     |    hide     |    2
	 *   disabled     |    show     |    3
	 *   disabled_all |    hide     |    4
	 *   disabled_all |    show     |    5
	 */

	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = (int)$_GET['TID'];
	$CID = isset($_GET['CID']) ? (int)$_GET['CID'] : -1;

	$DTmapping = array('text'=>'string', 'varchar'=>'string', 'date'=>'date', 'datetime'=>'datetime', 'integer'=>'integer', 'float'=>'float');

	// Determine tablename:
	$sql = "SELECT name FROM phoundry_table WHERE id = $TID";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$tableName = $db->FetchResult($cur,0,'name');

	// Determine column name:
	if (!empty($_GET['columnName']))
		$columnName = $_GET['columnName'];
	else {
		$sql = "SELECT name FROM phoundry_column WHERE id = $CID";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$columnName = $db->FetchResult($cur,0,'name');
	}

	function getFiles($dir, $skipPrefix, $bgcolor = '#fff') {
		$opts = array();
		$html = '';
		if (file_exists($dir) && $dh = @opendir($dir)) {
			while(($file = readdir($dh)) !== false) {
				if (preg_match('/^' . $skipPrefix . '[^A-Z]/', $file))
					$opts[] = $file;
			}
			closedir($dh);
		}
		else {
			return '';
		}
		sort($opts);
		foreach($opts as $opt) {
			$opt = preg_replace('/\.php$/', '', $opt);
			$html .= '<option value="' . $opt . '" style="background:' . $bgcolor . '">' . preg_replace('/^' . $skipPrefix . '/', '', $opt) . '</option>';
		}
		return $html;
	}

	// Make sure number of columns is not exceeded for license:
	if ($CID == -1) {
		include_once '../info/license.php';
		$licenseInfo = getLicenseInfo($PHprefs['productKey']);
		$allowedColumns = $licenseInfo['nrAllowedColumns'];
		if ($allowedColumns != -1) {
			$allowedColumns += round($allowedColumns * 0.1);
			if ($licenseInfo['nrUsedColumns'] >= $allowedColumns)
				PH::phoundryErrorPrinter('The number of columns for this license has been exceeded!<br />Click <a href="../info/">here</a> for details.', true);
		}
	}

	// Determine primary key fields:
	$db->GetTableIndexDefinition($tableName,'PRIMARY',$definition)
		or trigger_error("Cannot get PRIMARY key definition for table $tableName: " . $db->Error(), E_USER_ERROR);
	$primaryKeyFields = array_keys($definition['FIELDS']);

	// Determine properties of this column:
	$db->GetTableFieldDefinition($tableName, $columnName, $DBprops)
		or trigger_error("Cannot get table fields for table $tableName: " . $db->Error(), E_USER_ERROR);
	$DBprops = $DBprops[0];

	$cur = $db->Query("SELECT MAX(order_by) + 10 AS max_order FROM phoundry_column WHERE table_id = $TID");
	if (!$db->EndOfResult($cur))
		if (!$db->ResultIsNull($cur,0,'max_order'))
			$order_by = $db->FetchResult($cur,0,'max_order');
	if (empty($order_by))
		$order_by = 10;
	$description = array();
	foreach($PHprefs['languages'] as $lang)
		$description[$lang] = ucfirst(str_replace('_', ' ', $columnName));
	$datatype = '';
	$inputtype = '';
	$required = isset($DBprops['notnull']) ? 1 : 0;
	$string_id = $columnName;
	$inputlen = '';
	$searchable = 0;

	if ($CID != -1) {
		$sql = "SELECT * FROM phoundry_column WHERE id = $CID";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$string_id = $db->FetchResult($cur,0,'string_id');
		$order_by = $db->FetchResult($cur,0,'order_by');
		$description = PH::is_serialized($db->FetchResult($cur,0,'description')) ?
			@unserialize($db->FetchResult($cur,0,'description')) :
			$db->FetchResult($cur,0,'description');
		if ($description === false) {
			$description = array();
			foreach($PHprefs['languages'] as $lang)
				$description[$lang] = $db->FetchResult($cur,0,'description');
		}
		$datatype = $db->FetchResult($cur,0,'datatype');
		$inputtype = $db->FetchResult($cur,0,'inputtype');
		$required = $db->FetchResult($cur,0,'required');
		$searchable = (int)$db->FetchResult($cur,0,'searchable');
	}

	$infos = ($CID == -1) ? false : (PH::is_serialized($db->FetchResult($cur,0,'info')) ? @unserialize($db->FetchResult($cur,0,'info')) : $db->FetchResult($cur,0,'info'));
	if ($infos === false) {
		$infos = array();
		foreach($PHprefs['languages'] as $lang)
			$infos[$lang] = ($CID == -1) ? '' : $db->FetchResult($cur,0,'info');
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Column - Edit</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript" src="../csjs/dynform.js.php"></script>
<script type="text/javascript">
//<![CDATA[

var IThelp = DThelp = '', helpWin, tableName = <?=json_encode($tableName) ?>, colName = <?=json_encode($columnName)?>;

function setDatatype(datatype, CID) {
	if (!CID) CID = -1;
	var IT, x, html = '';
	if (!datatype) {
		$('#datatype_extraDiv').html('');
		$('#inputtype_extraDiv').html('');
		return;
	}
	
	$.ajax({async:false, url:'getDT.php', data:{id:CID, tableName: tableName, colName: colName, required:<?= isset($DBprops['notnull']) ? 1 : 0 ?>, datatype:datatype}, dataType:'json', success:function(json) {
		DTs = json['DTs'], compITs = json['compITs'], restITs = json['restITs'];
		for (x in DTs) {
			html += makeFieldset(DTs[x], 'DT');
		}

		html += '<fieldset>';
		html += '<legend><b class="req">Inputtype</b></legend>';
		html += 'Compatible: <select name="inputtype" style="width:160px" onchange="setInputtype(0,\''+datatype+'\',this.options[this.selectedIndex].value)">';
		html += '<option value="">[select]</option>';
		for (x in compITs) {
			ITname = compITs[x].replace(/^IT/, '');
			html += '<option value="' + compITs[x] + '">' + ITname + '</option>';
		}
		html += '</select>';
		html += ' Other: <select name="inputtype_other" style="width:160px" onchange="setInputtype(1,\''+datatype+'\',this.options[this.selectedIndex].value)">';
		html += '<option value="">[select]</option>';
		for (x in restITs) {
			ITname = restITs[x].replace(/^IT/, '');
			html += '<option value="' + restITs[x] + '">' + ITname + '</option>';
		}
		html += '</select>';
		html += '</fieldset>';
	
		$('#datatype_extraDiv').html(html);
		_D.forms[0].datatype.value = datatype;
		if ($.browser.msie) _D.body.focus();
	}
	});
}

function setInputtype(other, datatype, inputtype, CID) {

	if (!CID) CID = -1;
	var x, html = '';
	if (!inputtype) {
		$('#inputtype_extraDiv').html('');
		return;
	}
	
	$.ajax({async:false, url:'getIT.php', data:{id:CID, tableName: tableName, colName: colName, required:<?= isset($DBprops['notnull']) ? 1 : 0 ?>, datatype:datatype, inputtype:inputtype}, dataType:'json', success:function(ITs) {
		for (x in ITs) {
			html += makeFieldset(ITs[x], 'IT');
		}

		$('#inputtype_extraDiv').html(html);
		if (!other) {
			_D.forms[0].inputtype.value = inputtype;
			_D.forms[0].inputtype_other.selectedIndex = 0;
		}
		else {
			_D.forms[0].inputtype_other.value = inputtype;
			_D.forms[0].inputtype.selectedIndex = 0;
		}
		if ($.browser.msie) _D.body.focus();
	}
	});
}

function winPrio(e, tid) {
   makePopup(e,220,150,'Priorities','prio.php?TID='+tid);
}

function initInputtype(selName, inputtype) {
	var el = _D.forms[0][selName];
	for (var x = 0; x < el.options.length; x++) {
		if (el.options[x].value == inputtype)
			return true;
	}
	return false;
}

function checkFormExtra(F) {
	if (F['ITquery'] && /^SELECT/i.test(F['ITquery'].value)) {
		if (F['ITqueryFields'] && (F['ITqueryFields'].value == '' || F['ITqueryFields'].value == '{}')) {
			alert('Option value, option text and search text are not defined!');
			return false;
		}
	}
	return true;
}

function init() {
	if (<?= $CID ?> != -1) {
		// We're updating, initialise datatype and inputtype:
		setDatatype('<?= $datatype ?>', <?= $CID ?>);
		setInputtype(initInputtype('inputtype', '<?= $inputtype ?>') ? 0 : 1, '<?= $datatype ?>','<?= $inputtype ?>', <?= $CID ?>);
	}
	else {
		<?php if (isset($DTmapping[$DBprops['type']])) { ?>
		setDatatype('DT<?= $DTmapping[$DBprops['type']] ?>');
		<?php } ?>
	}
}

//]]>
</script>
</head>
<body class="frames" onload="init()">
<form action="editRes.php" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="TID" value="<?= $TID ?>" />
<input type="hidden" name="CID" value="<?= $CID ?>" />
<input type="hidden" name="DBlength" value="<?= isset($DBprops['length']) ? $DBprops['length'] : '' ?>" />
<input type="hidden" name="columnName" value="<?= PH::htmlspecialchars($columnName) ?>" />
<input type="hidden" name="tableName" value="<?= PH::htmlspecialchars($tableName) ?>" />

<div id="headerFrame" class="headerFrame">
<?= getHeader("Table $tableName, Column $columnName - " . (($CID == -1) ? 'Add' : 'Edit')); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b>The <span class="req">red</span> fields are required!</b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

	<h2>Database properties</h2>
	<fieldset><legend><b class="req">Column name</b></legend>
	<?= PH::htmlspecialchars($columnName) ?>
	</fieldset>

	<fieldset><legend><b class="req">String Id</b></legend>
	<div class="miniinfo">Unique string representation of this column. This is used for syncing tables.</div>
	<input type="text" name="string_id" alt="1|text" size="40" maxlength="80" value="<?=htmlspecialchars($string_id)?>" />
	</fieldset>

	<fieldset><legend><b class="req">Required?</b></legend>
	<?php
		if ($required) {
			print '<input id="req0" type="radio" name="required" value="0" /> <label for="req0">No</label>';
			print '<input id="req1" type="radio" name="required" value="1" checked="checked" /> <label for="req1">Yes</label>';
		}
		else {
			print '<input id="req0" type="radio" name="required" value="0" checked="checked" /> <label for="req0">No</label>';
			print '<input id="req1" type="radio" name="required" value="1" /> <label for="req1">Yes</label>';
		}
	?>
	</fieldset>

	<fieldset><legend><b class="req">Datatype</b></legend>
	<?= $DBprops['type'] ?>
	</fieldset>

	<fieldset><legend><b class="req">Maximum length</b></legend>
	<?= isset($DBprops['length']) ? $DBprops['length'] : '[none]'; ?>
	</fieldset>

	<fieldset><legend><b class="req">Primary key?</b></legend>
	<?php if (in_array($columnName, $primaryKeyFields)) print 'Yes'; else print 'No'; ?>
	</fieldset>

	<fieldset><legend><b>Default value</b></legend>
	<?php
		if (!isset($DBprops['default']))
			print '[none]';
		else if (empty($DBprops['default']))
			print '&nbsp;';
		else
			print $DBprops['default'];
	?>
	</fieldset>

	<h2>Phoundry properties</h2>
	<fieldset><legend><b class="req">Order</b> (<a href="#" onclick="winPrio(event, <?= $TID ?>);return false">priorities</a>)</legend>
	<input type="text" name="order_by" alt="1|number" size="10" maxlength="10" value="<?= $order_by ?>" />
	</fieldset>

	<fieldset><legend><b class="req">Description</b></legend>
	<?php
		foreach($PHprefs['languages'] as $lang)
			print '<input type="text" alt="1|text" name="description_' . $lang . '" size="40" maxlength="80" value="' . PH::htmlspecialchars(isset($description[$lang]) ? $description[$lang] : '') . '" /> ' . $lang . '<br />';
	?>
	</fieldset>

	<fieldset><legend><b>Search options</b></legend>
	<input id="search0" type="radio" name="search" value="enabled" <?= in_array($searchable,array(0,1)) ? 'checked="checked"' : '' ?> /> <label for="search0">Enabled by default</label>
	<input id="search1" type="radio" name="search" value="disabled" <?= in_array($searchable,array(2,3)) ? 'checked="checked"' : '' ?> /> <label for="search1">Disabled by default</label>
	<input id="search2" type="radio" name="search" value="disabled_all" <?= in_array($searchable,array(4,5)) ? 'checked="checked"' : '' ?> /> <label for="search2">Disabled completely</label>
	</fieldset>

	<fieldset><legend><b>Show in overview?</b></legend>
	<input id="show1" type="radio" name="show" value="0" <?= in_array($searchable,array(0,2,4)) ? 'checked="checked"' : '' ?> /> <label for="show1">No</label>
	<input id="show0" type="radio" name="show" value="1" <?= in_array($searchable,array(1,3,5)) ? 'checked="checked"' : '' ?> /> <label for="show0">Yes</label>
	</fieldset>

	<fieldset><legend><b>Info</b></legend>
	<?php
		foreach($PHprefs['languages'] as $lang)
			print '<textarea name="info_' . $lang . '" cols="60" rows="3" wrap="soft">' . PH::htmlspecialchars(isset($infos[$lang]) ? $infos[$lang] : '') . '</textarea> ' . $lang . '<br />';
	?>
	</fieldset>

	<fieldset><legend><b class="req">Datatype</b></legend>
	<select name="datatype" alt="1|text" style="width:160px" onchange="setDatatype(this.options[this.selectedIndex].value)">
	<option value="">[select]</option>
	<?php
		print getFiles("{$PHprefs['distDir']}/core/include/../Datatypes", 'DT', '#fff');
		print getFiles("{$PHprefs['distDir']}/dmdelivery/Datatypes", 'DT', '#ccc');
		print getFiles("{$PHprefs['instDir']}/custom/Datatypes", 'DT', '#ccc');
	?>
	</select>
	</fieldset>

	<div id="datatype_extraDiv"><br /><br /><br /><br /><br /><br /></div>
	<div id="inputtype_extraDiv"><br /><br /><br /><br /><br /><br /></div>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Submit" />
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='xedni.php?TID=<?= $TID ?>'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
