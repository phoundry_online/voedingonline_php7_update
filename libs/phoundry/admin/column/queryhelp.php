<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Query-help</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body>
<p>
The query-field can be used in a number of situations:
<table>
<tr><th>Inputtype</th><th>Query affects</th></tr>
<tr class="odd"><td>radio</td><td>The options and shown values of a group of radiobuttons</td></tr>
<tr class="even"><td>creditcard</td><td>The allowed creditcards</td></tr>
<tr class="odd"><td>priority</td><td>The existing priorities</td></tr>
<tr class="even"><td>select</td><td>The &lt;OPTION&gt;'s in a SELECT-box</td></tr>
<tr class="odd"><td>file</td><td>The allowed file extensions</td></tr>
</table>
</p>

<h2>radio</h2>

<pre>
SELECT id, fields FROM table
  [WHERE id = $childId] ORDER BY name
</pre>
The first field in the SELECT-part will be the VALUE. The other fields in the
SELECT-part will be showed to the user.<br />
The WHERE-part (between square brackets) is used for identifying the current
record (on the search-page and the update-page). The brackets are required!
The $childId variable will be filled with the correct value by Phoundry.
<pre>
0=False|1=True|2=Maybe
</pre>
In this case, the options don't come from the database, but they are predefined.
The syntax is <tt>value=name</tt>, the options are separated by a pipe. The
<tt>value</tt> is the VALUE for the radiobutton, everything on the right of
the =-sign will be showed to the user.

<h2>creditcard</h2>

As soon as you select the creditcard-inputtype, all possible creditcards are
displayed in the query-field. Just remove the types you don't allow.
<h2>priority</h2>
<pre>
SELECT prio, name FROM table ORDER BY prio DESC
</pre>
Use a query like this to show the already existing priorities to the user,
so that he/she can decide which priority to give a new record.

<h2>select</h2>

<pre>
SELECT id, fields FROM table
  [WHERE id = $childId] ORDER BY name
</pre>
The first field in the SELECT-part will be the VALUE of the OPTION. The other
fields will be showed as text to the user.<br />
The WHERE-part (between square brackets) is used for identifying the current
record (on the search-page and the update-page). The brackets are required!
The $childId variable will be filled with the correct value by Phoundry.
<pre>
green=I like green|red=I like red|blue=I like blue
</pre>
In this case, the OPTIONs don't come from the database, but they are predefined.
The syntax is <tt>value=name</tt>, the OPTIONs are separated by a pipe. The
<tt>value</tt> is the VALUE for the OPTION, everything on the right of
the =-sign will be showed to the user.
<pre>
SELECT id,name,#parent# FROM category
  [WHERE id = $childId] ORDER BY name
</pre>
This type of query is used when a table contains a reference to itself. This
is often seen in tables containing categories. The first field in the SELECT-part will be the VALUE of the OPTION. The last field, which has to be enclosed by
<tt>#</tt>, identifies the field that references to the primary key (id) in the
same table.

<h2>file</h2>

<pre>
gif|jpg|jpeg|png
</pre>
With inputtype <tt>file</tt>, you can force for certain types of files to
be uploaded. Just enter the extensions you allow, separated by a pipe. 
If you don't enter any extensions (leave the query-field empty), all files
will be accepted.
</p>

<p align="right">
<button type="button" onclick="parent.killPopup()">Cancel</button>
</p>
</body>
</html>
