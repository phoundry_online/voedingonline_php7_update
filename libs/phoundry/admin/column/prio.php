<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Order</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body>
<table cellspacing="0" cellpadding="2" border="0" width="100%">
<tr>
<th>Order</th>
<th>Column name</th>
</tr>
<?
	$prios = array();
	$sql = "SELECT order_by, name FROM phoundry_column WHERE table_id = " . (int)$_GET['TID'] . " ORDER BY order_by";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		print '<tr class="' . (($x & 1) ? 'even' : 'odd') . '">';
		print '<td>' . $db->FetchResult($cur,$x,'order_by') . '</td>';
		print '<td>' . $db->FetchResult($cur,$x,'name') . '</td>';
		print "</tr>\n";
	}
?>
</table>
<p align="right">
<button type="button" onclick="parent.killPopup()">Cancel</button>
</p>
</body>
</html>
