<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Default value help</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body>
<p>
The default value can be any string or number. However, there are a few special
features here. Those special features all start with an underscore!
</p>
<table>
<tr>
	<th>Default value</th>
	<th>Meaning</th>
</tr>
<tr class="odd">
	<td valign="top">_UNIQID</td>
	<td>A 13-character unique id will be generated. Example: <b>3ba9f8c0a660c</b></td>
</tr>
<tr class="even">
	<td valign="top">_USERID</td>
	<td>The userid (column 'id' in table 'phoundry_user') of the current Phoundry-user</td>
</tr>
<tr class="odd">
	<td valign="top">_MOD_USERID</td>
	<td>The userid (column 'id' in table 'phoundry_user') of the current Phoundry-user. Will be updated each update.</td>
</tr>
<tr class="even">
	<td valign="top">_USERNAME</td>
	<td>The username (column 'name' in table 'phoundry_user') of the current Phoundry-user</td>
</tr>
<tr class="odd">
	<td valign="top">_MOD_USERNAME</td>
	<td>The username (column 'name' in table 'phoundry_user') of the current Phoundry-user. Will be updated each update.</td>
</tr>
<tr class="even">
	<td valign="top">_TODAY</td>
	<td>
	The current date/time (at time of input). If you chose datatype 'date', only 
	the date will be the default value, if you chose datatype 'datetime', both 
	date and time will be the default value.<br />
	Other options: <b>_TODAY[+|-]x[Y|M|D|h|m|s]</b><br />(the date at time of 
	input, minus or plus a period x).
	</td>
</tr>
<tr class="odd">
	<td valign="top">_MOD_DATE</td>
	<td>
	Almost the same as '_TODAY', but now the field will be updated with the
	current date/time every time the record is modified.
	</td>
</tr>
<tr class="even">
	<td valign="top">_SELECT ...</td>
	<td>
	Use this if the default-value is some value stored in the database.
	Enter a SELECT-statement that returns one row, f.e:<br />
	<tt>_SELECT name FROM product WHERE id = 12</tt><br />
	In this case, the 'name' of product with id 12 is the default value.
	</td>
</tr>
</table>
<p align="right">
<button type="button" onclick="parent.killPopup()">Cancel</button>
</p>
</body>
</html>
