<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');

	$sql = $_GET['sql'];
	$sql = preg_replace('/\{\$[^\]]+}/', 'NULL', $sql);
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$db->GetColumnNames($cur, $names);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<title>Recursive Select Query</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
<script type="text/javascript">
//<![CDATA[

var _D = document;

function init() {
	var F = _D.forms[0], x, y, len, struct = parent.document.forms[0].ITqueryFields.value;
	if (struct == '')
		return;
	
	eval('var struct = ' + struct);
	len = F['optionvalue'].length;
	for (x = 0; x < len; x++) {
		if (F['optionvalue'][x].value == struct['optionvalue'])
			F['optionvalue'][x].checked = true;
	}
	for (x = 0; x < len; x++) {
		if (F['parent'][x].value == struct['parent'])
			F['parent'][x].checked = true;
				if (F['parent'][x].value == struct['parent'])
					F['parent'][x].checked = true;
	}
	len = F['optiontext'].length;
	for (x = 0; x < len; x++) {
		val = F['optiontext'][x].value;
		for (y in struct['optiontext']) {
			if (struct['optiontext'][y] == val)
				F['optiontext'][x].checked = true;
		}
	}
	len = F['searchtext'].length;
	for (x = 0; x < len; x++) {
		val = F['searchtext'][x].value;
		for (y in struct['searchtext']) {
			if (struct['searchtext'][y] == val)
				F['searchtext'][x].checked = true;
		}
	}
}

function setFields() {
	var F = _D.forms[0], x, len, qs = '', tmp = '', err = '', idx;

	var struct = '';

	len = F['optionvalue'].length;
	for (x = 0; x < len; x++) {
		if (F['optionvalue'][x].checked)
			tmp = "'optionvalue':'" + F['optionvalue'][x].value + "'";
	}
	if (tmp != '')
		qs += tmp;
	else
		err += 'Choose an option value!\n';

	tmp = ''; idx = 0;
	len = F['parent'].length;
	for (x = 0; x < len; x++) {
		if (F['parent'][x].checked)
			tmp = ",'parent':'" + F['parent'][x].value + "'";
	}
	if (tmp != '')
		qs += tmp;
	else
		err += 'Choose a parent!\n';

	tmp = ''; idx = 0;
	len = F['optiontext'].length;
	for (x = 0; x < len; x++) {
		if (F['optiontext'][x].checked) {
			if (idx > 0) tmp += ',';
			tmp += "'" + idx + "':'" + F['optiontext'][x].value + "'";
			idx++;
		}
	}
	if (tmp != '')
		qs += ",'optiontext':{" + tmp + "}";
	else
		err += 'Choose an option text!\n';

	tmp = ''; idx = 0;
	len = F['searchtext'].length;
	for (x = 0; x < len; x++) {
		if (F['searchtext'][x].checked) {
			if (idx > 0) tmp += ',';
			tmp += "'" + idx + "':'" + F['searchtext'][x].value + "'";
			idx++;
		}
	}
	if (tmp != '')
		qs += ",'searchtext':{" + tmp + "}";
	else
		err += 'Choose a search text!\n';

	if (err != '')
		alert(err);
	else {
		qs = '{' + qs + '}';
		parent.document.forms[0]['ITqueryFields'].value = qs;
		parent.killPopup();
	}
}

//]]>
</script>
</head>
<body onload="init()">
<form>
<table class="view-table" width="100%">
<tr>
	<th>Column</th>
	<th>Option value</th>
	<th>Parent field</th>
	<th>Option text</th>
	<th>Record page text</th>
</tr>
<?php
	$row = 0;
	foreach($names as $name=>$index) {
		print '<tr class="' . ($row & 1 ? 'even' : 'odd') . '">';
		print '<td>' . $name . '</td>';
		print '<td align="center"><input type="radio" name="optionvalue" value="' . $name . '" /></td>';
		print '<td align="center"><input type="radio" name="parent" value="' . $name . '" /></td>';
		print '<td align="center"><input type="checkbox" name="optiontext" value="' . $name . '" /></td>';
		print '<td align="center"><input type="checkbox" name="searchtext" value="' . $name . '" /></td>';
		print "</tr>\n";
		$row++;
	}
?>
</table>
<p align="right">
<input type="button" value="Submit" onclick="setFields()" />
</p>
</form>
</body>
</html>
