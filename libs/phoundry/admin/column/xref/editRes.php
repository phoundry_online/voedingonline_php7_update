<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once "{$PHprefs['distDir']}/core/include/Column.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = $_POST['TID'];
	$CID = $_POST['CID'];
	$xref = $_POST['xref'];
	
	$tableName   = $_POST['tableName'];
	$order_by    = $_POST['order_by'];
	$datatype    = $_POST['datatype'];
	$inputtype   = $_POST['inputtype'];
	$searchable  = (int)$_POST['searchable'];
	$required    = $_POST['required'];
	$string_id	 = isset($_POST['string_id']) ? $_POST['string_id'] : "";

	// description and info are multi-language:
	if (count($PHprefs['languages']) == 1) {
		$description = escDBquote(trim($_POST['description_' . $PHprefs['languages'][0]]));
		$info        = escDBquote(trim($_POST['info_' . $PHprefs['languages'][0]]));
	}
	else {
		$descs = $infos = array();
		foreach($PHprefs['languages'] as $lang) {
			$descs[$lang] = trim($_POST['description_' . $lang]);
			$infos[$lang] = trim($_POST['info_' . $lang]);
		}
		$description = escDBquote(serialize($descs));
		$info        = escDBquote(serialize($infos));
	}

	$errors = array();
	if ((int)$order_by < 0 || (int)$order_by > 99999)
		$errors[] = 'Order has to be between 0 and 99999!';

	// Check if the 'order' is unique:
	$sql = "SELECT name FROM phoundry_column WHERE table_id = $TID AND order_by = $order_by";
	if ($CID != -1) $sql .= " AND id != $CID";
	$cur = $db->Query($sql);
	if (!$db->EndOfResult($cur)) 
		$errors[] = "Another column (" . $db->FetchResult($cur,0,"name") . ") already has order $order_by! (the order <b>has</b> to be unique)";

	// Instantiate datatype:
	$column = new Column($CID, '', $tableName, -1, $required, $datatype, $inputtype);
	$datatypeOK = $column->datatype->setExtra($msg);
	if ($datatypeOK === false)
		$errors[] = $msg;
	else
		$datatype_extra = escDBquote(serialize($datatypeOK));

	// Instantiate inputtype:
	$inputtypeOK = $column->inputtype->setExtra($msg);
	if ($inputtypeOK === false)
		$errors[] = $msg;
	else
		$inputtype_extra = escDBquote(serialize($inputtypeOK));

	if (count($errors) == 0) {
		if ($CID == -1) {
			$sql = "INSERT INTO phoundry_column (table_id, string_id, order_by, name, description, datatype, datatype_extra, inputtype, inputtype_extra, required, searchable, info) VALUES ($TID, '$string_id', $order_by, '$xref', '" . $description . "', '$datatype', '$datatype_extra', '$inputtype', '$inputtype_extra', '$required', {$searchable}, '$info')";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$newId = $db->GetLastInsertId('phoundry_column', 'id');
		}
		else {
			$sql = "UPDATE phoundry_column SET string_id = '$string_id', required = '$required', order_by = $order_by, description = '" . $description . "', datatype = '$datatype', datatype_extra = '$datatype_extra', inputtype = '$inputtype', inputtype_extra = '$inputtype_extra', searchable = {$searchable}, info = '" . $info . "' WHERE id = $CID";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}
		
		header('Status: 302 moved');
		header("Location: ../xedni.php?TID=$TID");
		exit;
	}
	else {
		$errMsg = '<ul>';
		foreach($errors as $error)
			$errMsg .= '<li>' . $error . '</li>';
		$errMsg .= '</ul>';
		PH::phoundryErrorPrinter($errMsg, true);
	}
?>
