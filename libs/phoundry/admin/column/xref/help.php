<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Column - Edit</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body>
	<!-- Content -->
	<?php

	if (isset($_GET['inputtype'])) {
		$inputtype = $_GET['inputtype'];
		require_once '../../../core/Inputtypes/' . $inputtype . '.php';
		$inst = null;
		$inputtype = new $inputtype($inst);
		print $inputtype->getExtraHelp($_GET['help']);
	}
	elseif (isset($_GET['datatype'])) {
		$datatype = $_GET['datatype'];
		require_once '../../../core/Datatypes/' . $datatype . '.php';
		$inst = null;
		$datatype = new $datatype($inst);
		print $datatype->getExtraHelp($_GET['help']);
	}

	?>
	<p align="right">
	<button type="button" onclick="parent.killPopup()">Close</button>
	</p>
	<!-- /Content -->

</body>
</html>
