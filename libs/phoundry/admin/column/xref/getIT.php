<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require "{$PHprefs['distDir']}/core/include/common.php";
	require "{$PHprefs['distDir']}/core/include/Column.php";
	checkAccess('admin');


	$column = new Column($_GET['id'], '', $_GET['tableName'], -1, $_GET['required'], $_GET['datatype'], $_GET['inputtype']);

	header('Expires: 0'); // Prevent caching
	header('Content-type: application/json; charset=' . $PHprefs['charset']);
	print PH::php2javascript($column->inputtype->getExtra());
