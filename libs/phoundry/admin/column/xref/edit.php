<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = (int)$_GET['TID'];
	$CID = isset($_GET['CID']) ? (int)$_GET['CID'] : -1;

	// Determine tablename:
	$sql = "SELECT name FROM phoundry_table WHERE id = $TID";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$tableName = $db->FetchResult($cur,0,'name');

	if ($CID != -1) {
		$sql = "SELECT name FROM phoundry_column WHERE id = $CID";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$xref = $db->FetchResult($cur,0,'name');
	}
	else
		$xref = $_GET['xref'];

	function getFiles($dir, $skipPrefix, $bgcolor = '#fff') {
		$opts = array();
		$html = '';
		if (file_exists($dir) && $dh = @opendir($dir)) {
			while(($file = readdir($dh)) !== false) {
				if (preg_match('/^' . $skipPrefix . '[^A-Z]/', $file))
					$opts[] = $file;
			}
			closedir($dh);
		}
		else {
			return '';
		}
		sort($opts);
		foreach($opts as $opt) {
			$opt = preg_replace('/\.php$/', '', $opt);
			$html .= '<option value="' . $opt . '" style="background:' . $bgcolor . '">' . preg_replace('/^' . $skipPrefix . '/', '', $opt) . '</option>';
		}
		return $html;
	}
	
	$cur = $db->Query("SELECT MAX(order_by) + 10 AS max_order FROM phoundry_column WHERE table_id = $TID");
	if (!$db->EndOfResult($cur))
		if (!$db->ResultIsNull($cur,0,'max_order'))
			$order_by = $db->FetchResult($cur,0,'max_order');
	if (empty($order_by))
		$order_by = 10;
	$description = array();
	foreach($PHprefs['languages'] as $lang)
		$description[$lang] = '';
	$datatype = '';
	$inputtype = '';
	$inputlen = '';
	$searchable = 0;
	$required = false;
	$string_id = $xref;
	$info = '';

	if ($CID != -1) {
		$sql = "SELECT * FROM phoundry_column WHERE id = $CID";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$required = $db->FetchResult($cur,0,'required') == 1;
		$string_id = $db->FetchResult($cur,0,'string_id');
		$order_by = $db->FetchResult($cur,0,'order_by');
		$description = PH::is_serialized($db->FetchResult($cur,0,'description')) ? @unserialize($db->FetchResult($cur,0,'description')) : $db->FetchResult($cur,0,'description');
		if ($description === false) {
			$description = array();
			foreach($PHprefs['languages'] as $lang)
				$description[$lang] = $db->FetchResult($cur,0,'description');
		}
		$datatype = $db->FetchResult($cur,0,'datatype');
		$inputtype = $db->FetchResult($cur,0,'inputtype');
		$searchable = (int)$db->FetchResult($cur,0,'searchable');
		$xref = $db->FetchResult($cur,0,'name');
	}
	else
		$xref = $_GET['xref'];

	$infos = ($CID == -1) ? false : (PH::is_serialized($db->FetchResult($cur,0,'info')) ? @unserialize($db->FetchResult($cur,0,'info')) : $db->FetchResult($cur,0,'info'));
	if ($infos === false) {
		$infos = array();
		foreach($PHprefs['languages'] as $lang)
			$infos[$lang] = ($CID == -1) ? '' : $db->FetchResult($cur,0,'info');
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Cross reference - Edit</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../../../core/csjs/global.js.php"></script>
<script type="text/javascript" src="../../csjs/dynform.js.php"></script>
<script type="text/javascript">
//<![CDATA[

var IThelp = DThelp = '', helpWin;

function setDatatype(datatype, CID) {
	if (!CID) CID = -1;
	var IT, x, html = '';
	if (!datatype) {
		$('#datatype_extraDiv').html('');
		$('#inputtype_extraDiv').html('');
		return;
	}
	
	var req = _D.forms[0]['required'][1].checked ? 1 : 0;
	$.ajax({async:false, url:'getDT.php', data:{xref:<?= json_encode($xref) ?>, id:CID, tableName:<?=json_encode($tableName)?>, required:req, datatype:datatype}, dataType:'json', 'success':function(DTs) {
		for (x in DTs) {
			html += makeFieldset(DTs[x], 'DT');
		}

		$('#datatype_extraDiv').html(html);
		_D.forms[0].datatype.value = datatype;
	}
	});
}

function setInputtype(datatype, inputtype, CID) {
	if (!CID) CID = -1;
	var x, html = '';
	if (!inputtype) {
		$('#inputtype_extraDiv').html('');
		return;
	}
	
	var req = _D.forms[0]['required'][1].checked ? 1 : 0;
	$.ajax({async:false, url:'getIT.php', data:{id:CID, tableName:'<?= $tableName ?>', required:req, datatype:datatype, inputtype:inputtype}, dataType:'json', 'success':function(ITs) {
			for (x in ITs) {
				html += makeFieldset(ITs[x], 'IT');
			}
		
			$('#inputtype_extraDiv').html(html);
			_D.forms[0].inputtype.value = inputtype;
		}
	});
}

function winPrio(e, tid) {
   makePopup(e,220,150,'Priorities','../prio.php?TID='+tid);
}

function checkFormExtra(F) {
	if (F['ITqueryFields'] && (F['ITqueryFields'].value == '' || F['ITqueryFields'].value == '{}')) {
		alert('Option value, option text and search text are not defined!');
		return false;
	}
	return true;
}

function init() {
	if (<?= $CID ?> != -1) {
		// We're updating, initialise datatype and inputtype:
		setDatatype('<?= $datatype ?>', <?= $CID ?>);
		setInputtype('<?= $datatype ?>','<?= $inputtype ?>', <?= $CID ?>);
	}
}

//]]>
</script>
</head>
<body class="frames" onload="init()">
<form action="editRes.php" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="TID" value="<?= $TID ?>" />
<input type="hidden" name="CID" value="<?= $CID ?>" />
<input type="hidden" name="xref" value="<?= $xref ?>" />
<input type="hidden" name="tableName" value="<?= $tableName ?>" />

<div id="headerFrame" class="headerFrame">
<?= getHeader("Table $tableName, Cross reference - " . (($CID == -1) ? 'Add' : 'Edit'), 'X-ref table ' . $xref); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b>The <span class="req">red</span> fields are required!</b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

	<h2>Phoundry properties</h2>
	
	<fieldset><legend><b class="req">String Id</b></legend>
	<div class="miniinfo">Unique string representation of this column. This is used for syncing tables.</div>
	<input type="text" name="string_id" alt="1|text" size="40" maxlength="80" value="<?=htmlspecialchars($string_id)?>" />
	</fieldset>

	<fieldset><legend><b class="req">Required?</b></legend>
	<?php
		if ($required) {
			print '<input type="radio" name="required" value="0" /> No';
			print '<input type="radio" name="required" value="1" checked="checked" /> Yes';
		}
		else {
			print '<input type="radio" name="required" value="0" checked="checked" /> No';
			print '<input type="radio" name="required" value="1" /> Yes';
		}
	?>
	</fieldset>
	
	<fieldset><legend><b class="req">Order</b> (<a href="#" onclick="winPrio(event, <?= $TID ?>);return false">priorities</a>)</legend>
	<input type="text" name="order_by" alt="1|number" size="10" maxlength="10" value="<?= $order_by ?>" />
	</fieldset>

	<fieldset><legend><b class="req">Description</b></legend>
	<?php
		foreach($PHprefs['languages'] as $lang)
			print '<input type="text" alt="1|text" name="description_' . $lang . '" size="40" maxlength="80" value="' . PH::htmlspecialchars(isset($description[$lang]) ? $description[$lang] : '') . '" /> (' . $lang . ')<br />';
	?>
	</fieldset>

	<fieldset><legend><b>Show in overview?</b></legend>
	<input id="show0" type="radio" name="searchable" value="1" <?= $searchable ? 'checked="checked"' : '' ?> /> <label for="show0">Yes</label>
	<input id="show1" type="radio" name="searchable" value="0" <?= $searchable ? '' : 'checked="checked"' ?> /> <label for="show1">No</label>
	</fieldset>

	<fieldset><legend><b>Info</b></legend>
	<?php
		foreach($PHprefs['languages'] as $lang)
			print '<textarea name="info_' . $lang . '" cols="60" rows="3" wrap="soft">' . PH::htmlspecialchars(isset($infos[$lang]) ? $infos[$lang] : '') . '</textarea> (' . $lang . ')<br />';
	?>
	</fieldset>

	<fieldset><legend><b class="req">Datatype</b></legend>
   <select name="datatype" alt="1|text" style="width:160px" onchange="setDatatype(this.options[this.selectedIndex].value)">
   <option value="">[select]</option>
	<?php
		print getFiles("{$PHprefs['distDir']}/core/include/../Datatypes", 'DTX', '#fff');
		print getFiles("{$PHprefs['distDir']}/dmdelivery/Datatypes", 'DTX', '#ccc');
		print getFiles("{$PHprefs['instDir']}/custom/Datatypes", 'DTX', '#ccc');
	?>
   </select>
   </fieldset>

	<div id="datatype_extraDiv"></div>

	<fieldset>
	<legend><b class="req">Inputtype</b></legend>
	<select name="inputtype" alt="1|text" style="width:160px" onchange="setInputtype(_D.forms[0]['datatype'].value,this.options[this.selectedIndex].value)">
	<option value="">[select]</option>
	<?php
		if (file_exists("{$PHprefs['distDir']}/core/include/../Inputtypes") && $dh = opendir("{$PHprefs['distDir']}/core/include/../Inputtypes")) {
			while(($file = readdir($dh)) !== false) {
				if (!preg_match('/^ITX/', $file)) continue;
				$file = preg_replace('/\.php$/', '', $file);
				print '<option value="' . $file . '">' . preg_replace('/^ITX/', '', $file) . '</option>';
			}
			closedir($dh);
		}

		if (file_exists("{$PHprefs['distDir']}/dmdelivery/Inputtypes") && $dh = @opendir("{$PHprefs['distDir']}/dmdelivery/Inputtypes")) {
			while(($file = readdir($dh)) !== false) {
				if (!preg_match('/^ITX/', $file)) continue;
				$file = preg_replace('/\.php$/', '', $file);
				print '<option value="' . $file . '" style="background:#ccc">' . preg_replace('/^ITX/', '', $file) . '</option>';
			}
			closedir($dh);
		}

		if (is_dir("{$PHprefs['instDir']}/custom/Inputtypes") &&
			$dh = opendir("{$PHprefs['instDir']}/custom/Inputtypes"))
		{
			while(($file = readdir($dh)) !== false) {
				if (!preg_match('/^ITX/', $file)) continue;
				$file = preg_replace('/\.php$/', '', $file);
				print '<option value="' . $file . '" style="background:#ccc">' . preg_replace('/^ITX/', '', $file) . '</option>';
			}
			closedir($dh);
		}
	?>
	</select>
	</fieldset>

	<div id="inputtype_extraDiv"><br /><br /><br /><br /><br /><br /></div>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Submit" />
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='../xedni.php?TID=<?= $TID ?>'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
