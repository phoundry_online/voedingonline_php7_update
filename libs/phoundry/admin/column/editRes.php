<?php
	/**
	 * Explanation of meaning phoundry_column.searchable
	 * 
	 *   Searchable   | In_overview | DB-value
	 *   =============+=============+=========
	 *   enabled      |    hide     |    0
	 *   enabled      |    show     |    1
	 *   disabled     |    hide     |    2
	 *   disabled     |    show     |    3
	 *   disabled_all |    hide     |    4
	 *   disabled_all |    show     |    5
	 */

	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once "{$PHprefs['distDir']}/core/include/Column.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = (int)$_POST['TID']; $CID = (int)$_POST['CID'];
	
	$tableName   = filter_input(INPUT_POST, 'tableName', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$columnName  = filter_input(INPUT_POST, 'columnName', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$order_by    = (int)$_POST['order_by'];
	$datatype    = filter_input(INPUT_POST, 'datatype', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$inputtype   = filter_input(INPUT_POST, 'inputtype', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	if (empty($inputtype)) {
		$inputtype = filter_input(INPUT_POST, 'inputtype_other', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	}

	switch(true) {
		case $_POST['search'] == 'enabled' && $_POST['show'] == 0: $searchable = 0; break;
		case $_POST['search'] == 'enabled' && $_POST['show'] == 1: $searchable = 1; break;
		case $_POST['search'] == 'disabled' && $_POST['show'] == 0: $searchable = 2; break;
		case $_POST['search'] == 'disabled' && $_POST['show'] == 1: $searchable = 3; break;
		case $_POST['search'] == 'disabled_all' && $_POST['show'] == 0: $searchable = 4; break;
		case $_POST['search'] == 'disabled_all' && $_POST['show'] == 1: $searchable = 5; break;
	}

	$required     = (int)$_POST['required'];
	$string_id = trim(filter_input(INPUT_POST, 'string_id', FILTER_SANITIZE_FULL_SPECIAL_CHARS));

	// description and info are multi-language:
	if (count($PHprefs['languages']) == 1) {
		$description = trim($_POST['description_' . $PHprefs['languages'][0]]);
		$info        = trim($_POST['info_' . $PHprefs['languages'][0]]);
	}
	else {
		$nrNonemptyInfos = 0; 
		$descs = $infos = array();
		foreach($PHprefs['languages'] as $lang) {
			$desc = trim($_POST['description_' . $lang]);
			$info = trim($_POST['info_' . $lang]);
			if (!empty($info)) { $nrNonemptyInfos++; }

			$descs[$lang] = $desc;
			$infos[$lang] = $info;
		}
		$description = serialize($descs);
		$info        = $nrNonemptyInfos == 0 ? '' : serialize($infos);
	}

	$errors = array();
	if ((int)$order_by < 0 || (int)$order_by > 99999) {
		$errors[] = 'Order has to be between 0 and 99999!';
	}

	if (empty($inputtype)) {
		$errors[] = 'You have to select an inputtype!';
	}

	// Check if the 'order' is unique:
	$sql = "SELECT name FROM phoundry_column WHERE table_id = $TID AND order_by = $order_by";
	if ($CID != -1) $sql .= " AND id != $CID";
	$cur = $db->Query($sql);
	if (!$db->EndOfResult($cur)) {
		$errors[] = "Another column (" . $db->FetchResult($cur,0,"name") . ") already has order $order_by! (the order <b>has</b> to be unique)";
	}

	// Instantiate datatype:
	$column = new Column($CID, $columnName, $tableName, -1, $required, $datatype, $inputtype);
	$datatypeOK = $column->datatype->setExtra($msg);
	if ($datatypeOK === false) {
		$errors[] = $msg;
	}
	else {
		$datatype_extra = serialize($datatypeOK);
	}

	if (empty($errors)) {
		// Instantiate inputtype:
		$inputtypeOK = $column->inputtype->setExtra($msg);
		if ($inputtypeOK === false) {
			$errors[] = $msg;
		}
		else {
			$inputtype_extra = serialize($inputtypeOK);
		}
	}

	if (count($errors) == 0) {
		if ($CID == -1) {
			$sql = "INSERT INTO phoundry_column (string_id, table_id, order_by, name, description, datatype, datatype_extra, inputtype, inputtype_extra, required, searchable, info) VALUES ('".escDBquote($string_id)."', $TID, $order_by, '" . escDBquote($columnName) . "', '" . escDBquote($description) . "', '" . escDBquote($datatype) . "', '" . escDBquote($datatype_extra) . "', '" . escDBquote($inputtype) . "', '" . escDBquote($inputtype_extra) . "', {$required}, {$searchable}, '" . escDBquote($info) . "')";
			$db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$newId = $db->GetLastInsertId('phoundry_column', 'id');
		}
		else {
			$sql = "UPDATE phoundry_column SET string_id = '".escDBquote($string_id)."',required = {$required}, order_by = {$order_by}, description = '" . escDBquote($description) . "', datatype = '" . escDBquote($datatype) . "', datatype_extra = '" . escDBquote($datatype_extra) . "', inputtype = '" . escDBquote($inputtype) . "', inputtype_extra = '" . escDBquote($inputtype_extra) . "', searchable = {$searchable}, info = '" . escDBquote($info) . "' WHERE id = $CID";
			$db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}
		
		header('Status: 302 moved');
		header("Location: xedni.php?TID=$TID");
		exit;
	}
	else {
		$errMsg = '<ul>';
		foreach($errors as $error) {
			$errMsg .= '<li>' . $error . '</li>';
		}
		$errMsg .= '</ul>';
		trigger_error($errMsg, E_USER_ERROR);
	}
?>
