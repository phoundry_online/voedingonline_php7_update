<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	if (isset($PHprefs['freeze']) && $PHprefs['freeze']) {
		PH::phoundryErrorPrinter("This phoundry instance is frozen. Use Phoundry Sync to sync changes from Dev.", true);
	}
	
	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Columns</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript" src="../../core/csjs/jquery.tablesorter.pack.js"></script>
<script type="text/javascript">
//<![CDATA[

var activeRow = null;

function editRow(row, tid) {
	if (activeRow)
		activeRow['elem'].className = activeRow['class'];
	activeRow = [];
	activeRow['elem']  = row;
	activeRow['class'] = row.className;
	row.className = 'active';
	_D.location='xedni.php?TID='+tid;
}

$(function() {
	$('#recordTBL').tablesorter({
		cssHeader: 'sort',
		cssAsc: 'sortHeaderDesc',
		cssDesc: 'sortHeaderAsc',
		widgets: ['zebra'],
		widgetZebra: {css:['odd','even']}
	});
});

//]]>
</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Columns', 'Select table') ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<table id="recordTBL" class="sort-table" cellspacing="0">
<thead>
<tr>
	<th>Schema</th>
	<th>Tablename</th>
	<th>Description</th>
</tr>
</thead>
<tbody>
<?
	$sql = 'SELECT id, name, description FROM phoundry_table WHERE is_plugin = 0 ORDER BY order_by';
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($row = 0; !$db->EndOfResult($cur); $row++) {
		$tableName = $db->FetchResult($cur,$row,'name');

		print '<tr class="' . ($row & 1 ? 'even' : 'odd') . '" onclick="editRow(this,' . $db->FetchResult($cur,$row,'id') . ')">';

		if (preg_match('/(.*)\.(.*)/', $tableName, $reg)) {
			// Tablename includes schema name:
			print '<td>' . $reg[1] . '</td>';
			print '<td>' . $reg[2] . '</td>';
		}
		else {
			print '<td>' . $PHprefs['DBname'] . '</td>';
			if (preg_match('/^phoundry_/', $tableName)) {
				print '<td><b style="color:#be361f">' . $tableName . '</b></td>';
			}
			else {
				print '<td>' . $tableName . '</td>';
			}
		}

		$description = $db->FetchResult($cur,$row,'description');
		if (PH::is_serialized($description) && ($tmp = @unserialize($description)) !== false) {
			$description = $tmp[$PHprefs['languages'][0]];
		}

		print '<td>' . PH::htmlspecialchars($description) . '</td>';
		print "</tr>\n";
	}
?>
</tbody>
</table>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
	</td>
	</tr></table>
</div>

</body>
</html>
