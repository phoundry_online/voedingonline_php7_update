<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require "{$PHprefs['distDir']}/core/include/common.php";
	require "{$PHprefs['distDir']}/core/include/Column.php";
	checkAccess('admin');

	function getFiles($dir, $skipPrefix) {
		$opts = array();
		if (file_exists($dir) && $dh = @opendir($dir)) {
			while(($file = readdir($dh)) !== false) {
				if (preg_match('/^' . $skipPrefix . '[^A-Z]/', $file))
					$opts[] = preg_replace('/\.php$/', '', $file);
			}
			closedir($dh);
		}
		else {
			return array();
		}
		sort($opts);
		return $opts;
	}

    $colName = filter_input(INPUT_GET, 'colName', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $tableName = filter_input(INPUT_GET, 'tableName', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $datatype = filter_input(INPUT_GET, 'datatype', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

	$column = new Column((int)$_GET['id'], $colName, $tableName, -1, $_GET['required'], $datatype);

	// Determine compatible inputtypes:
	$compITs = $column->datatype->getCompatibleInputtypes();

	// Determine inputtypes that are not "compatible":
	$restITs = array_merge(
					array_diff(getFiles("{$PHprefs['distDir']}/core/include/../Inputtypes", 'IT'), $compITs),
					getFiles("{$PHprefs['distDir']}/dmdelivery/Inputtypes", 'IT', '#ccc'),
					getFiles("{$PHprefs['instDir']}/custom/Inputtypes", 'IT', '#ccc')
	);

	header('Expires: 0'); // Prevent caching
	header('Content-type: application/json; charset=' . $PHprefs['charset']);
	print PH::php2javascript(array('DTs'=>$column->datatype->getExtra(), 'compITs'=>$compITs, 'restITs'=>$restITs));
?>

