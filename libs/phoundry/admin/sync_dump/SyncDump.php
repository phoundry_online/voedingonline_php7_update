<?php
class SyncDump
{
	private $db;
	
	private $exclude = array(
		'check_url_s',
		'shared_files',
		'phoundry_event',
		'phoundry_table',
		'phoundry_column',
		'phoundry_filter',
		'phoundry_filter_1',
		'phoundry_user',
		'phoundry_group',
		'phoundry_group_table',
		'phoundry_content_node',
		'phoundry_tag',
		'phoundry_tag_content_node',
	);
	
	public function run()
	{
		$this->structure_dump();
		$this->phoundry_dump();
	}

	public function structure_dump()
	{
		$db_sync = new DB_Sync($this->getDb());
		$db_sync->ignored = array();
		file_put_contents($this->getDumpDir().'/database_structure.sql',
			implode(";\n\n", $db_sync->dump()).";\n");
	}
	
	public function phoundry_dump()
	{
		$exclude = $this->exclude;
		$phoundry_sync = new PhoundrySync($this->getDb());
		
		file_put_contents($this->getDumpDir().'/table_interfaces.dat', 
			"<?php return(".var_export($phoundry_sync->exportAll($exclude), true).");");
	}
	
	private function getDumpDir()
	{
		global $PHprefs;
		return $PHprefs['baseDir'].'/config';
	}
	
	/**
	 * @return resource
	 */
	private function getDb()
	{
		if(null === $this->db) {
			global $PHprefs;
			$host = $PHprefs['DBserver'];
			$host .= ':' . $PHprefs['DBport'];
			$this->db = mysql_connect($host, $PHprefs['DBuserid'], $PHprefs['DBpasswd']);
			mysql_select_db($PHprefs['DBname'], $this->db);
		}
		return $this->db;
	}
}