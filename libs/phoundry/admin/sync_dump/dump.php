<?php
// This is a commandline script
$argv = $_SERVER['argv'];
$scriptname = array_shift($argv);
$prefs = array_shift($argv);

if(!$prefs || !file_exists($prefs)) {
	die("Usage: ".basename($scriptname).' <path_to_prefs>'."\r\n");
}

require_once $prefs;
require_once dirname(__FILE__) . '/../DBsync/DB_Sync.php';
require_once dirname(__FILE__) . '/../sync/classes/PhoundrySync.php';

require_once "SyncDump.php";

$script = new SyncDump();
$script->run();