<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header('Expires: 0'); // Prevent caching
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$tableName = preg_replace('/[^\w\.\-]/', '', $_GET['table']);
	if (strpos($tableName, '.') !== false) {
		list($schema, $table) = explode('.', $tableName);
	}

	// Determine fields:
	$sql = "SHOW COLUMNS FROM `{$schema}`.`{$table}`";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$fields = array();
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$db->FetchResultAssoc($cur, $row, $x);
		$fields[] = $row;
	}

   print '<table class="tableDef" cellspacing="0">';
   print '<tr class="keys">';
   // Primary keys:
   foreach($fields as $field) {
      if ($field['Key'] == 'PRI') {
         print '<td class="key"></td>';
      }
      else {
         print '<td class="nokey"></td>';
      }
   }
   print "</tr>\n<tr class=\"fields\">\n";
   // Column names + not null + auto increment
   foreach($fields as $field) {
      print '<td class="' . ($field['Null'] == 'NO' ? 'notnull' : 'null') . '">';
      print '<u class="ptr" onclick="pasteTA(\'' . $field['Field']. '\')">' . $field['Field'] . '</u>';
      if ($field['Extra'] == 'auto_increment') {
         print '<sup>*</sup>';
      }
      print '</td>';
   }
   print "</tr>\n<tr class=\"types\">\n";

   // Datatypes + lengths
   foreach($fields as $field) {
      print '<td class="col" align="center">';
      print $field['Type'];
      print '</td>';
   }
   print "</tr></table>\n";

	print 'Table ' . $schema . '.<u class="ptr" onclick="pasteTA(\'' . $table . '\')">' . $table . '</u>';
