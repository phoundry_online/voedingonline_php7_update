<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	if (!isset($PHprefs['DBschemas'])) {
		$PHprefs['DBschemas'] = array($PHprefs['DBname']);
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Database Interface</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<style type="text/css">

.tableDef .keys td {
	height: 6px;
	border: none;
}

.tableDef .fields {
}

.tableDef .types {
}

.tableDef td {
	border: 1px solid #000;
	padding: 3px;
}

.key {
	background: red;
}

.notnull {
	color: #fff;
	background: #c00;
	font-weight: bold;
}

.null
{
	color: #fff;
	background: #444;
}

.col
{
	background: #ccc;
	color: #000;
}

div.grippie {
	background: #eee url(../../core/icons/grippie.png) no-repeat scroll center 2px;
	border-color: #ddd;
	border-style: solid;
	border-width: 0pt 1px 1px;
	cursor: s-resize;
	height: 9px;
	overflow: hidden;
}

</style>
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript" src="../../core/csjs/jquery.textarearesizer.pack.js"></script>
<script type="text/javascript">
//<![CDATA[

jQuery.fn.outerHTML = function(s) {
	return s ? this.before(s).remove() : jQuery('<p>').append(this.eq(0).clone()).html(); 
}

function setCaretToPos(input, pos) {
	setSelectionRange(input, pos, pos);
}

function setSelectionRange(input, selectionStart, selectionEnd) {
	if (input.setSelectionRange) {
		input.focus();
		input.setSelectionRange(selectionStart, selectionEnd);
	}
	else if (input.createTextRange) {
		var range = input.createTextRange();
		range.collapse(true);
		range.moveEnd('character', selectionEnd);
		range.moveStart('character', selectionStart);
		range.select();
	}
}

function replaceSelection(input, replaceString) {
	if (input.setSelectionRange) {
		var selectionStart = input.selectionStart;
		var selectionEnd = input.selectionEnd;
		input.value = input.value.substring(0, selectionStart) + replaceString + input.value.substring(selectionEnd);
		if (selectionStart != selectionEnd)
			setSelectionRange(input, selectionStart, selectionStart + replaceString.length);
		else
			setCaretToPos(input, selectionStart + replaceString.length);
	}
	else if (document.selection) {
		var range = document.selection.createRange();
		if (range.parentElement() == input) {
			var isCollapsed = range.text == '';
			range.text = replaceString;
			if (!isCollapsed)  {
				range.moveStart('character', -replaceString.length);
				range.select();
			}
		}
	}
}

function pasteTA(text) {
	var el = $('#'+curTab+'Div')[0];
	el.focus();
	replaceSelection(el, text);
}

var curTab = 'tab1';
function setTab(tab) {
	if (tab == curTab) return;
	$('#'+curTab).attr('class', 'inactive');
	$('#'+tab).attr('class', 'active');

	var oldDiv = $('#'+curTab+'Div'), newDiv = $('#'+tab+'Div'), val = oldDiv.val();
	oldDiv.hide().parents('.resizable-textarea').outerHTML(oldDiv.removeClass('processed').outerHTML());
	$('#'+curTab+'Div').val(val);
	newDiv.show().TextAreaResizer().focus();

	curTab = tab;
}

function showTableDef(table) {
	if (table == '') {
		$('#tableDefDiv').html('');
	}
	else {
		$.ajax({url:'tableDef.php?<?= QS(0) ?>', data:{table:table}, dataType:'html', success:function(tableDef) {
			$('#tableDefDiv').html(tableDef);
		}
		});
	}
}

function submitMe(F) {
	F['query'].value = $('#'+curTab+'Div').val();
	if (F['query'].value == '') {
		alert('Enter a query!');
		return false;
	}
	return true;
}

$(function() {
	$('iframe.resizable:not(.processed)').TextAreaResizer();
	$('textarea.resizable:not(.processed)').eq(0).TextAreaResizer();
	$('#tab1Div').focus();
});

//]]>
</script>
</head>
<body class="frames">
<form action="results.php" method="post" target="resultsFrame" onsubmit="return submitMe(this)">
<input type="hidden" name="query" />

<div id="headerFrame" class="headerFrame">
<?= getHeader('Database Interface'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<fieldset><legend><b>Table definition</b></legend>

<select id="tables" onchange="showTableDef(this.options[this.selectedIndex].value)">
<?php
	foreach($PHprefs['DBschemas'] as $schema) {
		print '<optgroup label="Schema: ' . $schema . '">';
		$sql = "SHOW TABLES FROM `{$schema}`";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$tableName = $db->FetchResult($cur,$x,0);
			print '<option value="`' . $schema . '`.' . $tableName . '">' . $tableName . '</option>';
		}
		print "</optgroup>\n";
	}
?>
</select>

<div id="tableDefDiv" style="margin-top:5px">&nbsp;</div>
</fieldset>

<fieldset><legend><b>Query</b> (Schema: <select name="schema">
<?php
	foreach($PHprefs['DBschemas'] as $schema) {
		print '<option value="' . $schema . '">' . $schema . '</option>';
	}
?>
</select>)
<?php
	if (isset($PHprefs['DBserverSecondary'])) {
		print '<input type="checkbox" name="secondary" value="1" /> Run on secondary DB';
	}
?>
</legend>

<div id="tabstrip"><ul>
	<li id="tab1" class="active"><a href="#" onclick="setTab('tab1');this.blur()">Query 1</a></li>
	<li id="tab2" class="inactive"><a href="#" onclick="setTab('tab2');this.blur()">Query 2</a></li>
	<li id="tab3" class="inactive"><a href="#" onclick="setTab('tab3');this.blur()">Query 3</a></li>
	<li id="tab4" class="inactive"><a href="#" onclick="setTab('tab4');this.blur()">Query 4</a></li>
	<li id="tab5" class="inactive"><a href="#" onclick="setTab('tab5');this.blur()">Query 5</a></li>
</ul></div>

<div style="margin-bottom:5px">
<textarea class="resizable" id="tab1Div" rows="10" cols="40" style="width:98%" spellcheck="false"></textarea>
<textarea class="resizable" id="tab2Div" rows="10" cols="40" style="width:98%;display:none" spellcheck="false"></textarea>
<textarea class="resizable" id="tab3Div" rows="10" cols="40" style="width:98%;display:none" spellcheck="false"></textarea>
<textarea class="resizable" id="tab4Div" rows="10" cols="40" style="width:98%;display:none" spellcheck="false"></textarea>
<textarea class="resizable" id="tab5Div" rows="10" cols="40" style="width:98%;display:none" spellcheck="false"></textarea>
</div>

<input type="submit" value="Submit" onclick="_D.forms[0].target=(event.shiftKey)?'_blank':'resultsFrame'" />
<input type="checkbox" name="csv" value="1" />Save as CSV
</fieldset>

<fieldset><legend><b>Results</b></legend>
<iframe class="resizable" name="resultsFrame" src="<?= $PHprefs['url'] ?>/core/blank.html" style="width:98%;height:200px;border:1px solid #a5acb2" frameborder="0"></iframe>
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
   <td>&nbsp;</td>
   <td align="right">
   <button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
   </td>
</tr></table>
</div>

</form>
</body>
</html>
