<?php
$PHprefs = array();
/** @var metabase_mysql_class $db */
$inc = @include_once('PREFS.php');
if ($inc === false) {
    require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
require "{$PHprefs['distDir']}/core/include/common.php";

checkAccess('admin');

$defaultSchema = $PHprefs['DBname'];
$defaultServer = $PHprefs['DBserver'];

$PHprefs['DBname'] = $_POST['schema'];
if (isset($_POST['secondary']) && $_POST['secondary'] == 1) {
    $PHprefs['DBserver'] = $PHprefs['DBserverSecondary'];
}
$db = PH::DBconnect();

$query = trim($_POST['query']);
if (function_exists('untaint')) {
    untaint($query);
}
$csv = isset($_POST['csv']);
if ($query[strlen($query) - 1] == ';') {
    $query = substr($query, 0, -1);
}

function microtimeFloat()
{
    list($usec, $sec) = explode(' ', microtime());
    return ((float) $usec + (float) $sec);
}

if ($csv && (strtolower(substr($query, 0, 6)) == 'select' || strtolower(
    substr($query, 0, 7)
) == '(select' || strtolower(substr($query, 0, 4)) == 'show' || strtolower(substr($query, 0, 4)) == 'desc')
) {
    header("Content-Disposition: attachment; filename=query.csv");
    header("Content-Type: application/csv");
    header("Content-Transfer-Encoding: binary");

    PH::CSVexport($query);
    exit;
} else {
    PH::getHeader('text/html');
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Query Results</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
</head>
<body style="margin:6px">
<?php

// Start timer:
$timer = microtimeFloat();

if ($db->connection) {
    if (preg_match('/^\(?(select|show|desc|explain)/i', $query)) {
        // SELECT query, display results as table.

        $cur = $db->Query($query);
        if (!$cur) {
            print '<p>Error in query!<br /><b>' . PH::htmlspecialchars($query) . '</b><br />' . $db->Error() . "</p>\n";
        } else {
            $fields = $db->NumberOfColumns($cur);
            $rows = $db->NumberOfRows($cur);
            print '<p><b>' . PH::htmlspecialchars($query) . '</b></p>';
            print '<table class="sort-table" cellspacing="0">';
            print "<tr>";
            for ($i = 0; $i < $fields; $i++) {
                print '<th>' . $db->FetchColumnName($cur, $i) . '</th>';
            }
            print "</tr>\n";
            for ($row = 0; $row < $rows; $row++) {
                print '<tr class="' . (($row & 1) ? 'even' : 'odd') . '">';
                for ($i = 0; $i < $fields; $i++) {
                    print '<td>';
                    if ($db->ResultIsNull($cur, $row, $i)) {
                        print '<i>NULL</i>';
                    } else {
                        print PH::htmlspecialchars($db->FetchResult($cur, $row, $i));
                    }
                    print '</td>';
                }
                print "</tr>\n";
            }
            print "<tr><th colspan=\"$fields\" align=\"left\">$fields fields, $rows rows (" . round(
                microtimeFloat() - $timer,
                3
            ) . "s)</th></tr>\n";
            print '</table>';
        }
    } else {
        // INSERT, UPDATE or DELETE.
        $res = $db->Query($query);
        if (!$res) {
            print '<p>Error in query!<br /><b>' . PH::htmlspecialchars($query) . '</b><br />' . $db->Error() . "</p>\n";
        } else {
            $nrRows = 0;
            $res = $db->AffectedRows($nrRows);
            print '<p>Query<br /><b>' . PH::htmlspecialchars($query) . '</b><br />succeeded!<br />';
            if ($res) {
                print "<br />($nrRows rows affected, " . round(microtimeFloat() - $timer, 3) . "s)";
            }
            print '</p>';
        }
    }
}

// Always reset back to the $defaultServer && $defaultSchema
if ($PHprefs['DBserver'] != $defaultServer || $PHprefs['DBname'] != $defaultSchema) {
    $PHprefs['DBserver'] = $defaultServer;
    $PHprefs['DBname'] = $defaultSchema;
    PH::DBconnect();
}
?>
</body>
</html>
