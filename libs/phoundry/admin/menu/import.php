<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}

$PHprefs['charset'] = 'utf-8';
header("Content-type: text/html; charset=utf-8");
require_once "{$PHprefs['distDir']}/core/include/common.php";

// Make sure the current user in the current role has access to this page:
checkAccess('admin');

if (!class_exists('DB_MetaBase')) {
	require_once $PHprefs['distDir']."/core/include/DB/MetaBase.php";
}
if (!class_exists('PhoundryMenuSync')) {
	require_once $PHprefs['distDir']."/core/include/PhoundryMenuSync.php";
}

if (!isset($_FILES['menu_import'])) {
	header('Bad Request', true, 400);
	echo 'Missing uploaded file';
	exit(1);
}

$filename = $_FILES['menu_import']['tmp_name'];
if (!is_file($filename)) {
	header('Internal Server Error', true, 500);
	echo 'Uploaded file cannot be read';
	exit(1);
}

$contents = file_get_contents($filename);
$json = json_decode($contents, true);

$sync = new PhoundryMenuSync(new DB_MetaBase($db));
$sync->import($json);

PH::trigger_redirect($PHprefs['url'] . '/admin/menu/?imported=1');