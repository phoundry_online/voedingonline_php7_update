<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once "{$PHprefs['distDir']}/core/include/PhoundryMenu.class.php";

// Make sure the current user in the current role has access to this page:
checkAccess('admin');

$primaryLang = $PHprefs['languages'][0];
$pm = new PhoundryMenu($primaryLang);

if(isset($_POST['menutree']))
{
	$menutree = json_decode(($PHprefs['charset'] != 'utf-8') ? utf8_encode($_POST['menutree']) : $_POST['menutree'], true);
	$parsed_menu_ids = array();
	$index = 0;
	$translated = array();
	$sql = "SELECT id, name FROM phoundry_menu";
	$cur = $db->Query($sql) or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$id = (int)$db->FetchResult($cur,$x,'id');
		$name = $db->FetchResult($cur,$x,'name');
   	if (PH::is_serialized($name) && ($tmp = @unserialize($name)) !== false) {
			$name = $tmp;
		} else {
			$name = array($primaryLang=>$name);
		}

		$translated[$id] = $name;
	}

	$sql = "DELETE FROM phoundry_menu";
	$cur = $db->Query($sql) or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	function parseChildren($children, $tab_id, $parent = null)
	{
		global $PHprefs, $index, $translated, $primaryLang, $db, $parsed_menu_ids;

		foreach($children as $item)
		{
			$index++;
			$id = explode('_', $item['id']);
			// Type $id[0]
			// Id $id[1]
			// Table Id $id[2]

			$item['name'] = ($PHprefs['charset'] != 'utf-8') ? utf8_decode($item['name']) : $item['name'];

			if(empty($id[1]) || !array_key_exists($id[1], $translated))
			{
				$name = array($primaryLang => $item['name']);
			}
			else
			{
				$name = $translated[$id[1]];
				$name[$primaryLang] = $item['name'];
			}

			// Update if we got an phoundry menu id
			$sql = sprintf('
				INSERT INTO
					phoundry_menu (id, tab_id, name, table_id, parent)
				VALUES
					(%d, %d, "%s", %s, %s)
				',
				$index,
				$tab_id,
				escDBquote(serialize($name)),
				'MenuFolder' == $item['rel'] ? 'NULL' : (int) $id[2],
				is_null($parent) ? 'NULL' : (int) $parent
			);

			$cur = $db->Query($sql) or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

			if(!empty($item['children'])) parseChildren($item['children'], $tab_id, $index);
		}
	}

	foreach($menutree as $tab_id => $items)
	{
		$id = explode('_', $tab_id);

		parseChildren($items, $id[1]);
	}
}

$sql = "SELECT id, name, description FROM phoundry_table";
$cur = $db->Query($sql)
	or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
$menu_items = array();
for ($x = 0; !$db->EndOfResult($cur); $x++) {
	$menu_items[$db->FetchResult($cur,$x,'id')] = $db->FetchResult($cur,$x,'description');
}
// Sort items alphabetically:
asort($menu_items);
$tab_menu_items = array();
foreach($PHprefs['menuTabs'] as $id => $name){
	$tab_menu_items[$id] = $pm->getPhoundryMenu(sprintf("
		SELECT
			id,
			parent,
			name,
			table_id
		FROM
			phoundry_menu
		WHERE
			tab_id = %d
		AND
			parent IS NULL
		ORDER BY
			id
		", $id
	));
}
list($CSRFname, $CSRFtoken) = PH::startCSRF();
PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Phoundry Menu</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="tree_component.css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.json.min.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/csrf.js"></script>
<script type="text/javascript" src="tree_component.js"></script>
<script type="text/javascript" src="css.js"></script>


<script type="text/javascript">
//<![CDATA[
function getChildren(li) {
	var children = [];
	li.children('ul').children('li').each(function(){
		var $this = $(this), child = {
			'id'		: $this.attr('id') || "",
			'name'		: $this.children('a').text(),
			'rel'		: $this.attr('rel'),
			'children'	: getChildren($this)
		};

		children.push(child);
	});
	return children;
};

jQuery(function($){

	<?php if(isset($_POST['menutree']) || isset($_GET['imported'])) { ?>
	parent.frames['PHmenu'].location.reload();
	<?php } ?>

 	var tree = $.tree_create();
	tree.init('#treeAll', {
		rules : {
			type_attr : "rel",
			creatable : 'none',
			multitree : true,
			drag_copy : 'on',
			draggable : 'all',
			dragrules : [
				'!* * RootNode', // Dont allow any node to be dragged in the "all" tree
				'!* inside MenuItem',
			],
			droppable : false,
		},
		ui :{ theme_name:'default',context : []}
	});

	$('#submit_button').click(function(e){
		var serialized = {};
		$('.menuTabs').each(function(){
			serialized[$(this).attr('id')] = getChildren($('ul li[rel="RootNode"]' ,this));
		});
		$('#menutree').val($.compactJSON(serialized));
		return true;
	});
});
<?foreach($PHprefs['menuTabs'] as $id => $name):?>
jQuery(function($){
	var tree = $.tree_create();
	tree.init('#tree_<?=$id?>', {
		rules : {
			type_attr : "rel",
			multitree : true,
			renameable: ['MenuItemNew', 'MenuItem', 'MenuFolder'],
			drag_copy : false,
			draggable : 'all',
			creatable : ['RootNode', 'MenuFolder'],
			deletable : ['MenuFolder', 'MenuItem', 'MenuItemNew'],
			dragrules : [
			 	'!RootNode * *',
			 	'!* before RootNode',
			 	'!* after RootNode',
			 	'!* inside MenuItem',
			 	'* * *'
			 ],
			droppable : ['MenuItemNew']
		},
		callback : {
			onselect : function(NODE, TREE_OBJ){
				$('#newfolder_<?=$id?>').prop('disabled', !(tree.selected.length && tree.check('creatable', tree.selected)));
				$('#rename_<?=$id?>').prop('disabled', !(tree.selected.length && tree.check('renameable', tree.selected)));
				$('#delete_<?=$id?>').prop('disabled', !(tree.selected.length && tree.check('deletable', tree.selected)));
				return true;
			},
			ondeselect : function(NODE, TREE_OBJ){
				$('#newfolder_<?=$id?>').prop('disabled', true);
				$('#rename_<?=$id?>').prop('disabled', true);
				$('#delete_<?=$id?>').prop('disabled', true);
				return true;
			}
		},
		ui :{ theme_name:'default', context : []}
	});
	$('#newfolder_<?=$id?>').click(function(e){
		tree.create({attributes: {rel: 'MenuFolder', 'class': 'folder'}});
		return false;
	});
	$('#rename_<?=$id?>').click(function(e){
		tree.rename();
		return false;
	});
	$('#delete_<?=$id?>').click(function(e){
		tree.remove(tree.selected);
		return false;
	});
});
<?endforeach?>

function init() {
    CSRF.insert('<?= $CSRFname ?>', '<?= $CSRFtoken ?>');
}
//]]>
</script>
<style type="text/css">
.tree-default li.open li.menuitem a { background-image:url(<?=$PHprefs['url']?>/core/pics/tree/ext_other.png); }
#jstree-dragged { z-index: 99 }
</style>
</head>
<body class="frames" onload="init();">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Phoundry Menu'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<h2 style="color:red">Enter all menu items in language <i><?= $PHprefs['languages'][0] ?></i>!</h2>
Afterwards, translate into other languages <a href="../translate/?<?= QS(1,'_anchor=phoundry_menu.name') ?>">here</a>.

<fieldset>
	<legend>All</legend>
	<div id="treeAll" style="width:400px;height:200px;background:#f5f5f5;border:1px solid silver;overflow: auto;">
	<ul>
		<li id="root_tree_all" class="open folder" rel="RootNode">
	    <a href="#">/</a>
	    <ul>
		<?foreach($menu_items as $id => $desc):?>
		<li id="i_0_<?=$id?>" rel="MenuItemNew" class="menuitem">
		    <a href="#"><?=htmlspecialchars($pm->word($desc))?></a>
		</li>
		<?endforeach?>
		</ul>
		</li>
	</ul>
	</div>
</fieldset>

<?foreach($PHprefs['menuTabs'] as $id => $name):?>
<fieldset style="float: left;">
	<legend><?=htmlspecialchars($name)?></legend>
	<div class="menu">
		<button type="button" id="newfolder_<?=$id?>" disabled="disabled">
			<img src="<?=$PHprefs['url']?>/core/icons/folder_add.png" alt="New folder" />
		</button>
		<button type="button" id="rename_<?=$id?>" disabled="disabled">
			<img src="<?=$PHprefs['url']?>/core/icons/textfield_rename.png" alt="rename" />
		</button>
		<button type="button" id="delete_<?=$id?>" disabled="disabled">
			<img src="<?=$PHprefs['url']?>/core/icons/bin.png" alt="delete" />
		</button>
	</div>
	<div id="tree_<?=$id?>" class="menuTabs" style="width:400px;height:200px;background:#f5f5f5;border:1px solid silver;overflow: auto;">
	<ul>
		<li id="root_tree_<?=$id?>" class="open folder" rel="RootNode">
		    <a href="#">/</a>
		    <?if(!empty($tab_menu_items[$id])):?>
		    <ul>
			<?
			$curDepth = 1;
			$str = '';
			$cnt = 0;
			foreach($tab_menu_items[$id] as $item)
			{
				if ($item->depth != $curDepth) {
					$diff = $item->depth - $curDepth;
					if ($diff < 0) {
						echo str_repeat('</li></ul></li>', abs($diff));
					}
					else
					{
						echo '<ul>';
					}
					$curDepth = $item->depth;
				}
				else if($cnt)
				{
					echo '</li>';
				}

				$name = $pm->word($item->name);
				if ($item->table_id) {
					echo '
					<li id="i_' . $item->id . '_' . $item->table_id .'" rel="MenuItem" class="menuitem">
			    		<a href="#">' . PH::htmlspecialchars($name) . '</a>
					';
				}
				else {
					echo '
					<li id="f_' . $item->id . '" class="open folder" rel="MenuFolder">
			    		<a href="#">' . PH::htmlspecialchars($name) . '</a>
					';
				}
				$cnt++;
			}

			while ($curDepth-- > 1) {
				echo '</li></ul>';
			}
			if($cnt)
			{
				echo '</li>';
			}
			?>
			</ul>
			<?endif?>
		</li>
	</ul>
	</div>
</fieldset>
<?endforeach?>
<div style="clear: both;"></div>

<div>
	<fieldset>
		<legend>Export / Replace</legend>
		<div><a href="<?=$PHprefs['url']?>/admin/menu/export.php">Export</a></div>
		<div>
			<form action="<?= $PHprefs['url'] ?>/admin/menu/import.php"
				  method="post" enctype="multipart/form-data">
				<div><input type="file" name="menu_import"></div>
				<div>
					<input type="submit" value="Replace"/>
				</div>
			</form>
		</div>
	</fieldset>
</div>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<form action="?" method="post">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input id="menutree" type="hidden" value="" name="menutree" />
	<input id="submit_button" type="submit" value="Submit" />
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
	</tr></table>
</form>
</div>

</body>
</html>
