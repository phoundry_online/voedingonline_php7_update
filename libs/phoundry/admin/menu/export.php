<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}

$PHprefs['charset'] = 'utf-8';
header("Content-type: text/html; charset=utf-8");
require_once "{$PHprefs['distDir']}/core/include/common.php";

// Make sure the current user in the current role has access to this page:
checkAccess('admin');

if (!class_exists('DB_MetaBase')) {
	require_once $PHprefs['distDir']."/core/include/DB/MetaBase.php";
}
if (!class_exists('PhoundryMenuSync')) {
	require_once $PHprefs['distDir']."/core/include/PhoundryMenuSync.php";
}

$sync = new PhoundryMenuSync(new DB_MetaBase($db));

$menuitems = $sync->export();

header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="'.$PHprefs['DBname'].'.menu"');

echo json_encode($menuitems);
