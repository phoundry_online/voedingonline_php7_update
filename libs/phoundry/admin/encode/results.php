<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	function DMencode5($str) 
	{
		return DMD::DMencode($str);
	}

	function DMdecode5($str) 
	{
		return DMD::DMdecode($str);
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Encoding/Decoding Results</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
</head>
<body style="margin:6px">
<?php

	$string = $_POST['string'];
	$encoding = $_POST['encoding'];
	if ($encoding == 'md5')
		print "String <b>$string</b> MD5-encoded:<br /><br />" . md5($string) . "<br /><br />";
	elseif ($encoding == 'sha1') 
		print "String <b>$string</b> SHA1-encoded:<br /><br />" . sha1($string) . "<br /><br />";
	elseif ($encoding == 'crypt')
		print "String <b>$string</b> crypted:<br /><br />" . crypt($string) . "<br /><br />";
	elseif ($encoding == 'crc32')
		print "String <b>$string</b> crc32'd:<br /><br />" . crc32($string) . "<br /><br />";
	elseif ($encoding == 'WPencode_old')
		print "String <b>$string</b> WP-encoded (old):<br /><br />" . WPencode_old($string) . "<br /><br />";
	elseif ($encoding == 'WPdecode_old')
		print "String <b>$string</b> WP-decoded (old):<br /><br />" . WPdecode_old($string) . "<br /><br />";
	elseif ($encoding == 'WPencode')
		print "String <b>$string</b> WP-encoded:<br /><br />" . PH::WPencode($string) . "<br /><br />";
	elseif ($encoding == 'WPdecode')
		print "String <b>$string</b> WP-decoded:<br /><br />" . PH::WPdecode($string) . "<br /><br />";
	elseif ($encoding == 'DMencode3')
		print "String <b>$string</b> DM-encoded:<br /><br />" . DMencode3($string) . "<br /><br />";
	elseif ($encoding == 'DMdecode3')
		print "String <b>$string</b> DM-decoded:<br /><br />" . DMdecode3($string) . "<br /><br />";
	elseif ($encoding == 'DMencode5')
		print "String <b>$string</b> DM-encoded:<br /><br />" . DMencode5($string) . "<br /><br />";
	elseif ($encoding == 'DMdecode5')
		print "String <b>$string</b> DM-decoded:<br /><br />" . DMdecode5($string) . "<br /><br />";

?>
</body>
</html>
