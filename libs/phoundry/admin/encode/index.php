<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>String Encoder/Decoder</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
</head>
<body class="frames">
<form action="results.php" method="post" target="resultsFrame">

<div id="headerFrame" class="headerFrame">
<?= getHeader('String Encoder/Decoder'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
	<fieldset>
	<legend><b>Encoding</b></legend>
	<p>
	License key format: <tt>domain(s)|license|nr_users</tt>.<br /><tt>License</tt> can either be (B)ronze, (S)ilver or (G)old.
	</p>
	<p>
	String<br />
	<input type="text" name="string" size="60" />
	</p>
	<p>
	Encoding<br />
	<select name="encoding">
	<option value="WPencode">WPencode</option>
	<option value="WPdecode">WPdecode</option>
	<?php if (class_exists('DMD')) { ?>
	<option value="DMencode5">DMencode</option>
	<option value="DMdecode5">DMdecode</option>
	<?php } ?>
	<option value="md5">MD5</option>
	<option value="sha1">SHA1</option>
	<option value="crypt">crypt</option>
	<option value="crc32">crc32</option>
	</select>
	</p>

	<input type="submit" value="Submit" onclick="_D.forms[0].target=(event.shiftKey)?'_blank':'resultsFrame'" />
	</fieldset>

	<fieldset><legend><b>Results</b></legend>
	<iframe name="resultsFrame" src="<?= $PHprefs['url'] ?>/core/blank.html" style="width:500px;height:200px;margin:5px;border:1px solid;border-color:#000 #999 #999 #000" frameborder="0"></iframe>
	</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>&nbsp;</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
	</td>
</tr></table>
</div>

</form>
</body>
</html>
