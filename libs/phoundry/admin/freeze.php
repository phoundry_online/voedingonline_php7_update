<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";

if (isset($PHprefs['freeze']) && $PHprefs['freeze']) {
	PH::phoundryErrorPrinter("This phoundry instance is frozen. Use Phoundry Sync to sync changes from Dev.", true);
}

PH::trigger_redirect($PHprefs['url'].'/core/records.php?'.QS(false));
