<!DOCTYPE>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Search and Replace</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<style type="text/css">
ins {
	background: yellow;
}
del {
	background: red;
}
.sql {
	white-space: no-wrap;
}
.diff {
	white-space: pre-wrap;
}
</style>
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript">
jQuery(function($) {
	function addRow(body) {
		var first_row = $('tr:first', body),
		new_row = first_row.clone();
		new_row.find('input').val('').prop('checked', false);
		first_row.parent('tbody').append(new_row);
	}

	$('.view-table').delegate('tr', 'change', function(e) {
		var $this = $(this);
		if (!$this.next().is('tr')) {
			addRow($this.parent('tbody'));
		}
	});
	$('[name=show]').bind('click', function() {
		if ($(this).val() == 'sql') {
			$('.sql').show();
			$('.diff').hide();
		} else {
			$('.sql').hide();
			$('.diff').show();
		}
	});
});
</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Search and Replace'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>
<form action="" method="post" enctype="multipart/form-data">
<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?php if (isset($executed)): ?>
<fieldset>
<legend>Executed</legend>
<?php foreach ($executed as $sql): ?>
<div><?=htmlspecialchars($sql)?>;</div>
<?php endforeach;?>
</fieldset>
<?php endif;?>

<?php if (isset($result)):
require_once "{$PHprefs['distDir']}/core/include/Text_Diff-1.1.1/Diff.php";
require_once "{$PHprefs['distDir']}/core/include/Text_Diff-1.1.1/Diff/Renderer.php";
require_once "{$PHprefs['distDir']}/core/include/Text_Diff-1.1.1/Diff/Renderer/inline.php";
?>
<div>Toon: Sql <input name="show" type="radio" value="sql"> Diff <input name="show" type="radio" value="diff" checked></div>
<fieldset>
	<legend>Replace</legend>
	<?if(!empty($result['redo'])):?>
	<?foreach($result['redo'] as $i => $sql):
	$value = json_encode(array('redo' => $sql, 'undo' => $result['undo'][$i]));
	$diff = new Text_Diff('auto', array(explode("\n", $result['old'][$i]), explode("\n", $result['new'][$i])));
	$renderer = new Text_Diff_Renderer_inline();
	$diff = $renderer->render($diff);
	?>
	<div>
		<input name="redo[]" type="checkbox" value="<?=htmlspecialchars($value)?>" checked="checked" />
		<span class="sql" style="display: none;"><?=htmlspecialchars($sql)?>;</span>
		<span class="diff"><?=$diff?></span>
	</div>
	<?endforeach?>
	<?else:?>
	No redo statements
	<?endif?>
</fieldset>
<?php elseif (isset($undo)):?>
<fieldset>
	<legend>Undo</legend>
	<?if(!empty($undo)):?>
	<?foreach($undo as $sql):?>
	<div>
	<input name="undo[]" type="checkbox" value="<?=htmlspecialchars($sql)?>" checked="checked" />
	<?=htmlspecialchars($sql)?>;
	</div>
	<?endforeach?>
	<?else:?>
	No redo statements
	<?endif?>
</fieldset>
<?php else:?>
<fieldset>
<legend>Datatypes comma seperated</legend>
<input type="text" name="datatypes" value="DTstring, DTemail, DTurl">
</fieldset>
<fieldset>
<legend>Inputtypes comma seperated (empty is all)</legend>
<input type="text" name="inputtypes" value="">
</fieldset>
<div class="miniinfo">
Wrap preg_match calls in slashes like /di?t/ imU are added as flags.<br>
Search for &lt;a&gt; tags and strip the style tags from them: /(&lt;a.*)\sstyle="[^"]*"/ replace with: $1
</div>
<table class="view-table">
	<thead>
	<tr>
	<th>Search</th>
	<th>Replace</th>
	</tr>
	</thead>
	<tbody>
	<tr>
	<td><input type="text" name="search[]"></td>
	<td><input type="text" name="replace[]"></td>
	</tr>
	</tbody>
</table>
<?php endif?>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
   <td>
   <input type="submit" value="Ok">
   </td>
   <td align="right">
   <button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
   </td>
</tr></table>
</div>
</form>
</body>
</html>
