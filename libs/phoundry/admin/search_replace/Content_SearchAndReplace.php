<?php
/**
 * Class for replacing content tekst
 * 
 * - If it doesn't work, replace more...
 * 
 * Bart's attempt to create a content replacing tool
 * that prevents Peter from pulling his hair out.
 * 
 * @author Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 2.0
 * @copyright 2011 Web Power, http://www.webpower.nl
 */
class Content_SearchAndReplace
{
	/**
	 * Array of the new values
	 * @var array
	 */
	private $new;
	
	/**
	 * Array of the old values
	 * @var array
	 */
	private $old;
	
	/**
	 * Array containing redo statements
	 * @var array 
	 */
	private $redo;
	
	/**
	 * Array containing undo statements
	 * @var array
	 */
	private $undo;
	
	/**
	 * Row update counter
	 * @var int
	 */
	private $update_count = 0;

	/**
	 * The database connection to use
	 * @var resource
	 */
	private $db;
	
	/**
	 * Datatypes that will be searched
	 * @var array
	 */
	private $datatypes;
	
	/**
	 * Inputtypes that will be searched
	 * @var array
	 */
	private $inputtypes;
	
	/**
	 * Create the search and replacer
	 * 
	 * @param resource $db
	 * @param array $datatypes default DTstring, DTemail and DTurl
	 * @param array $inputtypes default all
	 * @throws InvalidArgumentException when db is no valid resource
	 */
	public function __construct($db, array $datatypes = null, array $inputtypes = null)
	{
		if (!is_resource($db)) {
			throw new InvalidArgumentException("db should be a valid resource");
		}
		if (null === $datatypes) {
			$datatypes = array('DTstring', 'DTemail', 'DTurl');
		}
		$this->db = $db;
		$this->datatypes = $datatypes;
		$this->inputtypes = $inputtypes;
	}
	
	/**
	 * 
	 * @param array $replacements Array with replacement options
	 */
	public function start(array $replacements)
	{
		// reset stacks
		$this->redo = array();
		$this->undo = array();
		
		if (!$replacements) {
			return;
		}
		
		$where = array();
		if ($this->inputtypes) {
			$inputtypes = array_map(array($this, 'quote'), $this->inputtypes);
			$where[] = "inputtype IN (".implode(',', $inputtypes).")";
		}	
		if ($this->datatypes) {
			$datatypes = array_map(array($this, 'quote'), $this->datatypes);
			$where[] = "datatype IN (".implode(',', $datatypes).")";
		}	
		
		$sql = sprintf(
			"SELECT
				c.id,
				c.name,
				t.name AS table_name
			FROM phoundry_column AS c
			INNER JOIN phoundry_table AS t
			ON t.id = c.table_id
			%s",
			($where ? 'WHERE '. implode(' AND ', $where) : '')
		);
		
		$res = $this->query($sql);
		while ($row = mysql_fetch_assoc($res)) {
        	$this->replaceColumn(
        		$row['table_name'], $row['name'], $replacements
        	);
        }
        
        $res = array(
        	'new' => $this->new,
        	'old' => $this->old,
        	'redo' => $this->redo,
        	'undo' => $this->undo,
        	'count' => $this->update_count
        );
   		// reset stacks
		$this->redo = array();
		$this->undo = array();
		
		return $res;
	}
	
	protected function replaceColumn($table_name, $colname, $replacements)
	{
		$rid = $this->getRidColumn($table_name);
		if (!$rid) {
			return;
		}
		
		$sql = sprintf(
			"SELECT `%s` AS id, `%s` AS data
			FROM `%s`",
			$rid,
			$colname,
			$table_name
		);
		
		try {
        	$res = $this->query($sql);
		} catch(Exception $e) {
			return;
		}
        while ($row = mysql_fetch_assoc($res)) {
        	$str = $this->getReplacedData($row['data'], $replacements);
        	if (!$str || $str == $row['data']) {
        		continue;
        	}
        	
        	$this->new[] = $str;
        	$this->old[] = $row['data'];
			$this->undo[] = sprintf(
				"UPDATE %s SET `%s` = %s WHERE `%s` = %s", 
				$table_name,
				$colname,
				$this->quote($row['data']),
				$rid, 
				$this->quote($row['id'])
			);
			$this->redo[] = sprintf(
				"UPDATE %s SET `%s` = %s WHERE `%s` = %s",
				$table_name, $colname,
				$this->quote($str),
				$rid, 
				$this->quote($row['id'])
			);
			
			$this->update_count++;
	    }
	}
	
	protected function getReplacedData($data, array $replacements)
	{
		if ($replacements) {
			$str = $data;
			foreach ($replacements as $mode => $replacement) {
				if ($mode == 'preg_replace') {
					foreach ($replacement as $item) {
						if (count($item) == 2) {
							$str = preg_replace(
								$item[0].'imU',
								$item[1],
								$str
							);
						}
					}
				}
				if ($mode == 'str_replace') {
					foreach ($replacement as $item) {
						if (count($item) == 2) {
							$str = str_replace(
								$item[0],
								$item[1],
								$str
							);
						}
					}
				}
			}
			return $str;
		} 
		return null;
	}
	
	private function query($sql)
	{
		$res = mysql_query($sql, $this->db);
		if (false === $res) {
			throw new Exception(
				mysql_error($this->db), mysql_errno($this->db)
			);
		}
		return $res;
	}
	
	private function quote($sql, $quote = true)
	{
		$sql = mysql_real_escape_string($sql, $this->db);
		return $quote ? "'".$sql."'" : $sql;
	}
	
	/**
	 * Fetch the column name used by phoundry as the record identifier
	 *
	 * @param string $table_name Table name
	 * @return string Column used as record identifier by phoundry
	 */
	private function getRidColumn($table_name)
	{
		$sql = sprintf(
			"SHOW KEYS FROM `%s` WHERE Key_name = 'PRIMARY'",
			$this->quote($table_name, false)
		);
		try {
			$res = $this->query($sql);
		} catch (Exception $e) {
			return false;
		}
		if ($row = mysql_fetch_assoc($res)) {
			return $row['Column_name'];
		}
		
		return 'id'; // Gevaarlijke Arjan aanname
	}
}
