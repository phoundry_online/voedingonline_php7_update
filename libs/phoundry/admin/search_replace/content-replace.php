<?php

require_once dirname(dirname(__FILE__)).'/config/PREFS.php';
require 'SearchAndReplace.php';
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

$me = new Content_SearchAndReplace();
$me->log_file = TMP_DIR. 'sar-'.date('Ymd-His').'.log';
echo 'Log file: '.$me->log_file. "\n"; 
$me->undo_file = TMP_DIR. 'sar-undo-'.date('Ymd-His').'.sql';
echo 'Undo file: '.$me->undo_file. "\n"; 
$me->redo_file = TMP_DIR. 'sar-redo-'.date('Ymd-His').'.sql';
echo 'Redo file: '.$me->redo_file. "\n"; 
$me->original_file = TMP_DIR. 'sar-original-'.date('Ymd-His').'.log';
echo 'Original file: '.$me->original_file. "\n"; 
$me->updated_file = TMP_DIR. 'sar-updated-'.date('Ymd-His').'.log';
echo 'Updated file: '.$me->updated_file. "\n"; 
$me->update_content = (isset($argv[1]) && $argv[1] == '--go');

$me->replacements = array(
	'preg_replace' => array(
		array('(<a.*)\sstyle="[^"]*"', '$1'),
	),
);

$me->start();
echo "Done\n";
