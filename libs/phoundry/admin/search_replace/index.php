<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
require 'Content_SearchAndReplace.php';
checkAccess('admin');

if (isset($_POST['search'], $_POST['replace'])) {
	$replacements = array();
	foreach ($_POST['search'] as $i => $search) {
		if (empty($search) || empty($_POST['replace'][$i])) {
			continue;
		}
		$replace = $_POST['replace'][$i];
		
		if ($search[0] === '/') {
			if (!isset($replacements['preg_replace'])) {
				$replacements['preg_replace'] = array();
			}
			$replacements['preg_replace'][] = array($search, $replace);
		} else {
			if (!isset($replacements['str_replace'])) {
				$replacements['str_replace'] = array();
			}
			$replacements['str_replace'][] = array($search, $replace);
		}
	}
	
	$sar = new Content_SearchAndReplace(
		$db->connection,
		array_map('trim', explode(',', $_POST['datatypes'])),
		$_POST['inputtypes'] ?
		array_map('trim', explode(',', $_POST['inputtypes'])) : null 
	);
	
	$result = $sar->start($replacements);
} else if (isset($_POST['redo'])) {
	$undo = array();
	$executed = array();
	foreach ($_POST['redo'] as $json) {
		$json = json_decode($json, true);
		$json['redo'];
		$res = mysql_query($json['redo'], $db->connection);
		if (false === $res) {
			throw new Exception(
				mysql_error($db->connection), mysql_errno($db->connection)
			);
		}
		$executed[] = $json['redo'];
		$undo[] = $json['undo'];
	}
	
} else if (isset($_POST['undo'])) {
	$executed = array();
	foreach ($_POST['undo'] as $sql) {
		$res = mysql_query($sql, $db->connection);
		if (false === $res) {
			throw new Exception(
				mysql_error($db->connection), mysql_errno($db->connection)
			);
		}
		$executed[] = $sql;
	}
}

include 'index.tpl.php';
