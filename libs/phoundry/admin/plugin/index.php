<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');
	
	if (isset($PHprefs['freeze']) && $PHprefs['freeze']) {
		PH::phoundryErrorPrinter("This phoundry instance is frozen. Use Phoundry Sync to sync changes from Dev.", true);
	}
	
	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Plugins</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript" src="../../core/csjs/jquery.tablesorter.pack.js"></script>
<script type="text/javascript">
//<![CDATA[

var activeRow = null, activeDBtable = null, activePluginID = null;

function setRow(row, pluginID) {
	if (activeRow)
		activeRow['elem'].className = activeRow['class'];
	activeRow = [];
	activeRow['elem']  = row;
	activeRow['class'] = row.className;
	row.className = 'active';

	activePluginID = pluginID ? pluginID : null;

	$('#editBut,#delBut').prop('disabled',pluginID?false:true);
	$('#addBut').prop('disabled',false);

	if (activePluginID) window.status = activePluginID;
}

function editRow() {
	_D.location='edit.php?TID='+activePluginID;
}

$(function() {
	$('#recordTBL').tablesorter({
		cssHeader: 'sort',
		cssAsc: 'sortHeaderDesc',
		cssDesc: 'sortHeaderAsc',
		widgets: ['zebra'],
		widgetZebra: {css:['odd','even']}
	});
});

<?php
	if (isset($_GET['reload']))
		print "parent.frames['PHmenu'].location.reload();\n";
?>

//]]>
</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Plugins') ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
	<input type="button" id="addBut" value="Add" onclick="_D.location='edit.php'" />
	<img class="div" src="../../core/pics/div.gif" alt="|" width="3" height="18" />
	<input type="button" disabled="disabled" id="editBut" value="Edit" onclick="editRow()" />
	<input type="button" disabled="disabled" id="delBut" value="Delete" onclick="if(confirm('Are you sure?'))_D.location='delete.php?TID='+activePluginID" />
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<table id="recordTBL" class="sort-table" cellspacing="0">
<thead>
<tr>
	<th>Description</th>
	<th>Start URL</th>
	<th>Order</th>
</tr>
</thead>
<tbody>
<?php
	$sql = "SELECT id, description, extra, order_by FROM phoundry_table WHERE is_plugin = 1 ORDER BY order_by";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$extra = unserialize($db->FetchResult($cur,$x,'extra'));
		print '<tr class="' . (($x & 1) ? 'even' : 'odd') . '" onclick="setRow(this,' . $db->FetchResult($cur,$x,'id') . ')" ondblclick="editRow()">';
		$description = $db->FetchResult($cur,$x,'description');
		if (PH::is_serialized($description) && ($tmp = @unserialize($description)) !== false)
			$description = $tmp[$PHprefs['languages'][0]];
		print '<td>' . $description . '</td>';
		print '<td>' . $extra['sUrl'] . '</td>';
		print '<td>' . $db->FetchResult($cur,$x,'order_by') . '</td>';
		print "</tr>\n";
	}
?>
</tbody>
</table>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>&nbsp;</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
	</td>
	</tr></table>
</div>

</body>
</html>
