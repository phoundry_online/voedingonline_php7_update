<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = (isset($_GET['TID'])) ? (int)$_GET['TID'] : '';

	if (!empty($TID)) {
		$tbl = $db->Query("SELECT * FROM phoundry_table WHERE id = $TID");
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Plugin - Edit</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

function winPrio(e, field) {
   var fieldEl = _D.forms[0][field];
   makePopup(e,220,150,'Priorities','../table/prio.php');
}

//]]>
</script>
</head>
<body class="frames">
<form action="editRes.php" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="TID" value="<?= $TID ?>" />

<div id="headerFrame" class="headerFrame">
<?php
	$action = (empty($TID)) ? 'Add' : 'Edit';
	print getHeader("Plugin - $action");
?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b>The <span class="req">red</span> fields are required!</b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

	<fieldset><legend><b class="req">Order</b> (<a href="#" onclick="winPrio(event,'order_by');return false">priorities</a>)</legend>
	<?php
		if (!empty($TID))
			$mo = $db->FetchResult($tbl,0,'order_by');
		else
		{
			$mo = 0;
			$sql = "SELECT MAX(order_by)+10 AS max_order FROM phoundry_table WHERE order_by < 100000";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur))
				if (!$db->ResultIsNull($cur,0,'max_order'))
					$mo = $db->FetchResult($cur,0,'max_order');
			if ($mo == '') $mo = 0;
		}
	?>
	<input type="text" name="order_by" alt="1|number" size="5" maxlength="10" value="<?= $mo ?>" /> (0-99999)
	</fieldset>
	
	<fieldset><legend><b class="req">String Id</b></legend>
	<input type="text" name="string_id" alt="1|text" value="<?=!empty($TID) ? PH::htmlspecialchars($db->FetchResult($tbl,0,'string_id')) : ""?>" />
	</fieldset>

	<fieldset><legend><b class="req">Description</b></legend>
	<?php
		$langs = empty($TID) ? false : (PH::is_serialized($db->FetchResult($tbl,0,'description')) ? @unserialize($db->FetchResult($tbl,0,'description')) : $db->FetchResult($tbl,0,'description'));
		if ($langs === false) {
			$langs = array();
			foreach($PHprefs['languages'] as $lang)
				$langs[$lang] = empty($TID) ? '' : $db->FetchResult($tbl,0,'description');
		}
		foreach($PHprefs['languages'] as $lang)
			print '<input type="text" name="description_' . $lang . '" alt="1|text" size="60" maxlength="80" value="' . PH::htmlspecialchars(isset($langs[$lang]) ? $langs[$lang] : '') . '" /> ' . $lang . '<br />';
	?>
	</fieldset>

	<fieldset><legend><b class="req">Show in menu</b></legend>
	<select name="show_in_menu" alt="1|string">
	<?php
		$options = array();
		$options['hide'] = '[Hide]';
		foreach($PHprefs['menuTabs'] as $tabID=>$name) {
			$options["tab{$tabID}"] = $name;
		}
		$options['service'] = 'Service';
		$options['admin'] = 'Admin';
		$options['admin_brickwork'] = 'Brickwork admin';
		foreach($options as $value=>$name) {
			$selected = (!empty($TID) && $db->FetchResult($tbl,0,'show_in_menu') == $value) ? ' selected="selected"' : '';
			print '<option value="' . $value . '"' . $selected . '>' . $name . '</option>';
		}
	?>
	</select>
	</fieldset>
	
	<fieldset><legend><b>Info</b></legend>
	<?php
		$langs = empty($TID) ? false : (PH::is_serialized($db->FetchResult($tbl,0,'info')) ? @unserialize($db->FetchResult($tbl,0,'info')) : $db->FetchResult($tbl,0,'info'));
		if ($langs === false) {
			$langs = array();
			foreach($PHprefs['languages'] as $lang)
				$langs[$lang] = empty($TID) ? '' : $db->FetchResult($tbl,0,'info');
		}
		foreach($PHprefs['languages'] as $lang)
			print '<textarea name="info_' . $lang . '" cols="60" rows="3" wrap="soft">' . PH::htmlspecialchars(isset($langs[$lang]) ? $langs[$lang] : '') . '</textarea> ' . $lang . '<br />';
	?>
	</fieldset>

	<fieldset><legend><b>Group(s)</b> (<a href="#" onclick="selectAll(_D.forms[0]['groups[]']);return false">select all</a>)</legend>
	<div class="miniinfo">Select the users that will have access to this table.</div>
	<select multiple="multiple" size="5" name="groups[]">
	<?
		$sql = "SELECT id, name FROM phoundry_group ORDER BY name";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$selected = ' ';
			if (!empty($TID)) {
				$sql = "SELECT * FROM phoundry_group_table WHERE group_id = " . $db->FetchResult($cur,$x,'id') . " AND table_id = $TID";
				$tmp = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				if (!$db->EndOfResult($tmp)) $selected = ' selected="selected" ';
			}
			print '<option' . $selected . 'value="' . $db->FetchResult($cur,$x,'id') . '">' . $db->FetchResult($cur,$x,'name') . "</option>\n";
		}
	?>
	</select>
	</fieldset>

	<h2>Extra's</h2>

	<?php
		$extra = array();
		if (!empty($TID)) {
			$extra = $db->FetchResult($tbl,0,'extra');
			if (!empty($extra))
				$extra = unserialize($extra);
		}
	?>
	<fieldset><legend><b class="req">Start URL</b></legend>
	<input type="text" name="sUrl" alt="1|string" size="60" value="<?= isset($extra['sUrl']) ? $extra['sUrl'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>Are global role rights applicable?</b></legend>
	<?php
		if (!isset($extra['rolerights_global']) || $extra['rolerights_global'] == false) {
			print '<input type="radio" name="rolerights_global" value="1" /> Yes ';
			print '<input type="radio" name="rolerights_global" value="0" checked="checked" /> No';
		}
		else {
			print '<input type="radio" name="rolerights_global" value="1" checked="checked" /> Yes ';
			print '<input type="radio" name="rolerights_global" value="0" /> No';
		}
	?>
	</fieldset>

	<fieldset><legend><b>Are detailed role rights applicable?</b></legend>
	<?php
		if (!isset($extra['rolerights_detail']) || $extra['rolerights_detail'] == false) {
			print '<input type="radio" name="rolerights_detail" value="1" /> Yes ';
			print '<input type="radio" name="rolerights_detail" value="0" checked="checked" /> No';
		}
		else {
			print '<input type="radio" name="rolerights_detail" value="1" checked="checked" /> Yes ';
			print '<input type="radio" name="rolerights_detail" value="0" /> No';
		}
	?>
	</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Submit" />
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='index.php'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
