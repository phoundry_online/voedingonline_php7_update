<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');
	
	$TID          = $_POST['TID'];
	$order_by     = $_POST['order_by'];
	$string_id	  = $_POST['string_id'];
	$show_in_menu = $_POST['show_in_menu'];
	$groups       = isset($_POST['groups']) ? $_POST['groups'] : array();

	// description and info are multi-language:
	if (count($PHprefs['languages']) == 1) {
		$description = trim($_POST['description_' . $PHprefs['languages'][0]]);
		$info        = trim($_POST['info_' . $PHprefs['languages'][0]]);
	}
	else {
		$nrNonemptyInfos = 0;
		$descs = $infos = array();
		foreach($PHprefs['languages'] as $lang) {
			$desc = trim($_POST['description_' . $lang]);
			$info = trim($_POST['info_' . $lang]);
			if (!empty($info)) { $nrNonemptyInfos++; }

			$descs[$lang] = $desc;
			$infos[$lang] = $info;
		}
		$description = serialize($descs);
		$info        = $nrNonemptyInfos == 0 ? '' : serialize($infos);
	}

	$extra       = array();
	if (!empty($_POST['sUrl'])) {
		$extra['sUrl'] = $_POST['sUrl'];
	}
	$extra['rolerights_global'] = ($_POST['rolerights_global'] == 1) ? true : false;
	$extra['rolerights_detail'] = ($_POST['rolerights_detail'] == 1) ? true : false;
	$extra = count($extra) == 0 ? '' : serialize($extra);

	$errors = array();
	if ($description == '')
		$errors[] = 'Required field Description is empty!';
	if ($order_by == '')
		$errors[] = 'Required field Order is empty!';
	if ((int)$order_by < 0 || (int)$order_by > 99999)
		$errors[] = 'Order has to be between 0 and 99999!';
	
	if (count($errors) == 0) {
		if ($TID != '') {
			$sql = "UPDATE phoundry_table SET order_by = $order_by, string_id = '".escDBquote($string_id)."', description = '" . escDBquote($description) . "', info = '" . escDBquote($info) . "', show_in_menu = '" . escDBquote($show_in_menu) . "', extra = '" . escDBquote($extra) . "' WHERE id = $TID";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}
		else {
			$sql = "INSERT INTO phoundry_table(order_by, string_id, name, description, record_order, info, is_plugin, show_in_menu, extra) VALUES ($order_by, '".escDBquote($string_id)."', '', '" . escDBquote($description) . "', '', '" . escDBquote($info) . "',  1, '" . escDBquote($show_in_menu) . "', '" . escDBquote($extra) . "')";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			// Determine new ID:
			$newTID = $db->GetLastInsertId('phoundry_table', 'id');
		}
		
		// Handle user(s):
		if ($TID != '') {
			// This table existed already.
			$rights = array();
			
			// Existing rights must be preserved!
			$sql = "SELECT group_id, rights FROM phoundry_group_table WHERE table_id = $TID";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++) {
				$rights[$db->FetchResult($cur,$x,'group_id')] = $db->FetchResult($cur,$x,'rights');
			}
			
			$sql = "DELETE FROM phoundry_group_table WHERE table_id = $TID";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; $x < count($groups); $x++) {
				if (isset($rights[$groups[$x]])) {
					// Preserve rights
					$res = $db->Query("INSERT INTO phoundry_group_table VALUES($groups[$x], $TID, '" . $rights[$groups[$x]] . "')");
				}
				else
					// Default rights (all)
					$res = $db->Query("INSERT INTO phoundry_group_table (group_id, table_id) VALUES ($groups[$x], $TID)");
			}
		}
		else {
			// This is a new table.
			for ($x = 0; $x < count($groups); $x++) {
				$sql = "INSERT INTO phoundry_group_table (group_id, table_id) VALUES ($groups[$x], $newTID)";
				$res = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

			}
		}
		header('Status: 302 moved');
		header("Location: index.php?reload=1");
		exit();
	}
	else {
		$errMsg = '<ul>';
		foreach($errors as $error)
			$errMsg .= '<li>' . $error . '</li>';
		$errMsg .= '</ul>';
		PH::phoundryErrorPrinter($errMsg);
	}
?>
