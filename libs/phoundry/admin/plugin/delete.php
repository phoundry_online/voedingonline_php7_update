<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');
	
	$TID = (int)$_GET['TID'];
	
	@$db->Query("DELETE FROM phoundry_lock WHERE table_id = $TID");

	@$db->Query("DELETE FROM phoundry_notify WHERE table_id = $TID");

	@$db->Query("DELETE FROM phoundry_group_table WHERE table_id = $TID");

	@$db->Query("DELETE FROM phoundry_user_shortcut WHERE table_id = $TID");

	@$db->Query("DELETE FROM phoundry_menu WHERE table_id = $TID");

	$ids = array(-1);
	$cur = $db->Query("SELECT id FROM phoundry_column WHERE table_id = $TID");
	for ($x = 0; !$db->EndOfResult($cur); $x++)
		$ids[] = $db->FetchResult($cur,$x,'id');
	@$db->Query("DELETE FROM phoundry_group_column WHERE column_id IN (" . implode(',',$ids) . ')');

	@$db->Query("DELETE FROM phoundry_column WHERE table_id = $TID");

	$sql = "DELETE FROM phoundry_table WHERE id = $TID";
	$res = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	
	header('Status: 302 moved');
	header('Location: index.php?reload=1');
?>
