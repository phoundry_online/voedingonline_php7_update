<?php
   $inc = @include_once('PREFS.php');
   if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
   }
   require_once "{$PHprefs['distDir']}/core/include/common.php";

   header('Content-type: application/x-javascript');
?>
function showHelp(what,help) {
	var url, title, selected;
	if (what == 'DT') {
		selected = _D.forms[0].datatype.value;
		url = 'help.php?datatype='+selected+'&help='+escape(help);
	}
	else if (what == 'IT') {
		selected = _D.forms[0].inputtype.value;
        if (selected == '') {
            selected = _D.forms[0].inputtype_other.value;
        }
        url = 'help.php?inputtype='+selected+'&help='+escape(help);
	}
	title = selected.substr(2,selected.length);
	makePopup(null,500,400,'Help '+title,url);
}

function htmlspecialchars(str) {
	if (typeof(str) == 'string') {
		str = str.replace(/&/g, '&amp;'); // must do &amp; first
		str = str.replace(/"/g, '&quot;');
		str = str.replace(/</g, '&lt;');
		str = str.replace(/>/g, '&gt;');
	}
	return str;
}

function makeFieldset(struct, what) {
	var html = '';
	if (struct['input']['type'] == 'hidden') {
		html += '<input type="hidden" name="' + struct['name'] + '"';
		if (struct['input']['value'])
			html += ' value="' + htmlspecialchars(struct['input']['value']) + '"';
		html += ' />';
		return html;
	}

	html += '<fieldset>';
	html += '<legend>';
	html += (struct['required'] ? '<b class="req">' : '<b>') + struct['label'] + '</b>';
	if (struct['help'])
		html += ' (<a href="#" onclick="showHelp(\'' + what + '\',\'' + struct['name'] + '\');return false">help</a>)';
	html += '</legend>';
	if (struct['info'])
		html += '<div class="miniinfo">' + struct['info'] + '</div>';
	switch(struct['input']['type']) {
		case 'text':
			html += '<input type="text" name="' + struct['name'] + '" size="' + struct['input']['size'] + '"';
			if (struct['input']['maxlength'])
				html += ' maxlength="' + struct['input']['maxlength'] + '"'
			if (struct['input']['value'])
				html += ' value="' + htmlspecialchars(struct['input']['value']) + '"';
			html += ' alt="' + (struct['required']?'1':'0') + '|' + struct['syntax'] + '" />';
			break;
		case 'textarea':
			html += '<textarea name="' + struct['name'] + '" cols="' + struct['input']['cols'] + '" rows="' + struct['input']['rows'] + '"';
			html += ' alt="' + (struct['required']?'1':'0') + '|' + struct['syntax'] + '" spellcheck="false">';
			if (struct['input']['value'])
				html += htmlspecialchars(struct['input']['value']);
			html += '</textarea>';
			break;
		case 'select':
			html += '<select name="' + struct['name'] + '" size="' + (struct['size'] ? struct['size'] : 1) + '">';
			if (!struct['required'])
				html += '<option value="">[select]</option>';
			for(x in struct['input']['options']) {
				html += '<option';
				if (struct['input']['selected'] == x)
					html += ' selected="selected"';
				html += ' value="' + htmlspecialchars(x) + '">' + htmlspecialchars(struct['input']['options'][x]) + '</option>';
			}
			html += '</select>';
			break;
	}
	html += '</fieldset>';
	return html;
}
