<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	// Retrieve license info:
	require_once 'license.php';
	$licenseInfo = getLicenseInfo($PHprefs['productKey']);

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>License &amp; PHP-info</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<style type="text/css">

html, body {
	height: 100%;
}

</style>
<script type="text/javascript" src="../../core/csjs/global.js.php?<?= QS(1) ?>"></script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?php
	$extra = 'Phoundry ' . $licenseInfo['license'];
	echo getHeader('License & PHP-info', $extra);
?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="0">
	<tr>
		<td>Nr. of allowed users for <?= $licenseInfo['license'] ?> license:</td>
		<td><?= $licenseInfo['nrAllowedUsers'] ?></td>
		<td>Nr. of users in license key:</td>
		<td><?= $licenseInfo['nrUsers'] ?></td>
		<td rowspan="2" width="30%">&nbsp;</td>
	</tr><tr>
		<td>Nr. of columns allowed for <?= $licenseInfo['license'] ?> license:</td>
		<td><?= $licenseInfo['nrAllowedColumns'] ?></td>
		<td>Number of columns used (currently):</td>
		<td><?= $licenseInfo['nrUsedColumns'] ?></td>
	</tr>
</table>
</div>

<div id="contentFrame" class="contentFrame" style="padding:0;overflow:hidden;height:100%">
	<iframe src="phpinfo.php" frameborder="0" style="width:100%;height:100%"></iframe>
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
	</td>
	</tr></table>
</div>

</body>
</html>
