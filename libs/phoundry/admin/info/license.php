<?php

function getLicenseInfo($prodKey) {
	global $db;

	$retval = array();

	// Determine license version:
	$decKey = explode('|', PH::WPdecode($prodKey));
	$license = $decKey[1]; $nrUsers = $decKey[2];
	$retval['nrUsers'] = $nrUsers;

	if ($license == 'G') {
		$retval['license'] = 'Gold';
		$retval['nrAllowedColumns'] = -1;
		$retval['nrAllowedUsers'] = 50;
	}
	elseif ($license == 'S') {
		$retval['license'] = 'Silver';
		$retval['nrAllowedColumns'] = 150;
		$retval['nrAllowedUsers'] = 20;
	}
	elseif ($license == 'B') {
		$retval['license'] = 'Bronze';
		$retval['nrAllowedColumns'] = 75;
		$retval['nrAllowedUsers'] = 5;
	}

	$sql = "SELECT id FROM phoundry_table WHERE name NOT LIKE 'phoundry\_%' AND name NOT LIKE 'module\_%' AND name NOT LIKE 'brickwork\_%' AND name != ''";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$ids = array(-1);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$ids[] = (int)$db->FetchResult($cur,$x,'id');
	}

	$nrTables = count($ids) - 1; // Skip -1!
	
	$sql = "SELECT COUNT(*) FROM phoundry_column WHERE datatype != 'CrossReference' AND table_id IN (" . implode(',', $ids) . ")";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$nrCols = $db->FetchResult($cur,0,0) - $nrTables;

	$sql = "SELECT COUNT(*) FROM phoundry_column WHERE datatype = 'CrossReference' AND table_id IN (" . implode(',', $ids) . ")";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$nrRefs = $db->FetchResult($cur,0,0);
	
	$nrCols += 2 * $nrRefs;

	$retval['nrUsedTables'] = $nrTables;
	$retval['nrUsedColumns'] = $nrCols;

	return $retval;
}
