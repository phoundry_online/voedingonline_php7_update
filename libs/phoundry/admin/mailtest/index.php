<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Phoundry</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
</head>
<body class="frames">
<form action="mailRes.php" method="post" onsubmit="return checkForm(this)">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Mail test'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b><?= word(32) ?></b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<fieldset><legend><b class="req">Use SMTP?</b></legend>
<input type="radio" name="useSMTP" value="0" checked="checked"> No
<input type="radio" name="useSMTP" value="1"> Yes
</fieldset>

<fieldset><legend><b>SMTP hostname</b></legend>
<input type="text" name="SMTPhostname" alt="0|string" size="60" value="<?= is_array($PHprefs['SMTPserver']) ? $PHprefs['SMTPserver'][0] : $PHprefs['SMTPserver'] ?>" />
</fieldset>

<fieldset><legend><b>SMTP localhost</b></legend>
<input type="text" name="SMTPlocalhost" alt="0|string" size="60" value="<?= $PHprefs['SMTPserver'] ?>" />
</fieldset>

<fieldset><legend><b class="req">From address</b></legend>
<input type="text" name="from" alt="1|email" size="60" />
</fieldset>

<fieldset><legend><b class="req">To address</b></legend>
<input type="text" name="to" alt="1|email" size="60" />
</fieldset>

<fieldset><legend><b class="req">Subject</b></legend>
<input type="text" name="subject" alt="1|string" size="60" />
</fieldset>

<fieldset><legend><b class="req">Body</b></legend>
<textarea name="body" cols="60" rows="10" alt="1|string"></textarea>
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Ok" />
	</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
	</td>
</tr></table>
</div>

</form>
</body>
</html>
