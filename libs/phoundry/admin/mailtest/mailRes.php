<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Phoundry</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[


//]]>
</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Mail test'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

<?php
	if (!$_POST['useSMTP']) {
		$res = mail($_POST['to'], $_POST['subject'], $_POST['body'], "From: {$_POST['from']}\nX-Mailer: PHP/" . phpversion());
		if ($res) print "Mail sent successfully!<br />";
		else print "Could not send mail!<br />";
	} else {
		include "{$PHprefs['distDir']}/core/include/DMDsmtp.php";
		
		$hdrs = array(
			'X-Mailer'	=> 'PHP/' . phpversion(),
			'From'		=> $_POST['from'],
			'To'			=> $_POST['to'],
			'Subject'	=> $_POST['subject']
		);

		$smtp = new DMDsmtp();
		$smtp->connect(array('host'=>$_POST['SMTPhostname'], 'helo'=>$_POST['SMTPlocalhost']));
		$res = $smtp->send($_POST['from'], $_POST['to'], $hdrs, $_POST['body']);
		
		if ($res) 
			print "SMTP Mail sent successfully!<br />";
		else {
			print "Could not send SMTP-mail:<ul>";
			foreach($smtp->errors as $err) {
				print '<li>' . $err . '</li>';
			}
			print '</ul>';
		}
	}
?>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Ok" />
	</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
	</td>
</tr></table>
</div>

</body>
</html>
