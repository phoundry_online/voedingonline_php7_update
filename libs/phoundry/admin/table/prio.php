<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Priorities</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/popup.css" type="text/css" />
</head>
<body>
<table cellspacing="0" cellpadding="2" border="0" width="100%">
<tr>
<th>Order</th>
<th>Table description</th>
</tr>
<?
	$sql = "SELECT order_by, description, is_plugin FROM phoundry_table WHERE order_by < 100000 ORDER BY order_by";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($row = 0; !$db->EndOfResult($cur); $row++) {
		print '<tr class="' . (($row & 1) ? 'even' : 'odd') . '">';
		print '<td>' . $db->FetchResult($cur,$row,'order_by') . '</td>';
		print '<td>';
		print PH::langWord($db->FetchResult($cur,$row,'description'));
		if ($db->FetchResult($cur,$row,'is_plugin') == 1)
			print ' (p)';
		print '</td>';
		print "</tr>\n";
	}
?>
</table>
<b>p = plugin</b>
<p align="right">
<button type="button" onclick="parent.killPopup()">Cancel</button>
</p>
</body>
</html>
