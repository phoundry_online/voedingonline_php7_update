<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');
	
	$TID          = (int)$_POST['TID'];
	$string_id    = trim($_POST['string_id']);
	$order_by     = (int)$_POST['order_by'];
	if ($_POST['record_order'] === '') {
		$record_order = '';
	}
	else {
		$record_order = $_POST['record_order'] . ' ' . $_POST['record_order_asc_desc'];
	}
	$max_records  = (int)$_POST['max_records'];
	$show_in_menu = $_POST['show_in_menu'];
	$tableName    = $_POST['tableName'];
	$groups       = isset($_POST['groups']) ? $_POST['groups'] : array();

	// description and info are multi-language:
	if (count($PHprefs['languages']) == 1) {
		$description = trim($_POST['description_' . $PHprefs['languages'][0]]);
		$info        = trim($_POST['info_' . $PHprefs['languages'][0]]);
	}
	else {
		$nrNonemptyInfos = 0;
		$descs = $infos = array();
		foreach($PHprefs['languages'] as $lang) {
			$desc = trim($_POST['description_' . $lang]);
			$info = trim($_POST['info_' . $lang]);
			if (!empty($info)) { $nrNonemptyInfos++; }

			$descs[$lang] = $desc;
			$infos[$lang] = $info;
		}
		$description = serialize($descs);
		$info        = $nrNonemptyInfos == 0 ? '' : serialize($infos);
	}

	$extra       = array();
	if (!empty($_POST['sUrl']))
		$extra['sUrl'] = $_POST['sUrl'];
	if (!empty($_POST['rUrl']))
		$extra['rUrl'] = $_POST['rUrl'];
	if (!empty($_POST['iUrl']))
		$extra['iUrl'] = $_POST['iUrl'];
	if (!empty($_POST['uUrl']))
		$extra['uUrl'] = $_POST['uUrl'];
	if (!empty($_POST['dUrl']))
		$extra['dUrl'] = $_POST['dUrl'];
	if (!empty($_POST['cUrl']))
		$extra['cUrl'] = $_POST['cUrl'];
	if (!empty($_POST['vUrl']))
		$extra['vUrl'] = $_POST['vUrl'];
	if (!empty($_POST['reloadMenu']))
		$extra['reloadMenu'] = $_POST['reloadMenu'];
	$extra['rolerights_global'] = ($_POST['rolerights_global'] == 1) ? true : false;
	$extra['rolerights_detail'] = ($_POST['rolerights_detail'] == 1) ? true : false;
	$extra['ok_and_ins_again'] = ($_POST['ok_and_ins_again'] == 1) ? true : false;
	$extra['versioning'] = (isset($_POST['versioning']) && $_POST['versioning'] == 1);
	$extra['versioning_message'] = isset($_POST['versioning_message']) ? $_POST['versioning_message'] : '';
	
	if (!empty($_POST['timetable_from']) && !empty($_POST['timetable_till'])) {
		// We want a timetable:
		$extra['timetable'] = array(
			'fromField' => $_POST['timetable_from'],
			'tillField' => $_POST['timetable_till'],
			'timespan' => $_POST['timetable_timespan'],
			'showFields' => $_POST['timetable_fields'],
			'where' => $_POST['timetable_where']
		);
	}

	$extra = count($extra) == 0 ? '' : serialize($extra);

	$errors = array();
	if ($string_id == '') {
		$errors[] = 'Required field String Id is empty!';
	}
	if ($description == '') {
		$errors[] = 'Required field Description is empty!';
	}
	if ($order_by == '') {
		$errors[] = 'Required field Order is empty!';
	}
	if (!preg_match('/^phoundry_/', $tableName) && ($order_by < 0 || $order_by > 99999)) {
		$errors[] = 'Order has to be between 0 and 99999!';
	}
	if ($_POST['max_records'] === '') { $max_records = 'NULL'; }
	
	if (count($errors) == 0) {
		if ($TID != null && $TID != 0 && $TID != '') {
			$sql = "UPDATE phoundry_table SET string_id = '".escDBquote($string_id)."', order_by = {$order_by}, description = '" . escDBquote($description) . "', max_records = {$max_records}, record_order = '" . escDBquote($record_order) . "', info = '" . escDBquote($info) . "', show_in_menu = '" . escDBquote($show_in_menu) . "', extra = '" . escDBquote($extra) . "' WHERE id = $TID";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}
		else {
			$sql = "INSERT INTO phoundry_table(string_id, order_by, name, description, max_records, record_order, info, is_plugin, show_in_menu, extra) VALUES ('".escDBquote($string_id)."', $order_by, '" . escDBquote($tableName) . "', '" . escDBquote($description) . "', $max_records, '" . escDBquote($record_order) . "', '" . escDBquote($info) . "', 0, '" . escDBquote($show_in_menu) . "', '" . escDBquote($extra) . "')";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			// Determine new ID:
			$newTableId = $db->GetLastInsertId('phoundry_table', 'id');
		}

		// Handle user(s):
		if ($TID != '') {
			// This table existed already.
			$rights = array();
			
			// Existing rights must be preserved!
			$sql = "SELECT group_id, rights FROM phoundry_group_table WHERE table_id = $TID";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; !$db->EndOfResult($cur); $x++) {
				$rights[$db->FetchResult($cur,$x,'group_id')] = $db->FetchResult($cur,$x,'rights');
			}
			
			$sql = "DELETE FROM phoundry_group_table WHERE table_id = $TID";
			$res = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			for ($x = 0; $x < count($groups); $x++) {
				if (isset($rights[$groups[$x]])) {
					// Preserve rights
					$res = $db->Query("INSERT INTO phoundry_group_table VALUES($groups[$x], $TID, '" . $rights[$groups[$x]] . "')");
				}
				else
					// Default rights (all)
					$res = $db->Query("INSERT INTO phoundry_group_table (group_id, table_id) VALUES ($groups[$x], $TID)");
			}
		}
		else {
			// This is a new table.
			for ($x = 0; $x < count($groups); $x++) {
				$sql = "INSERT INTO phoundry_group_table (group_id, table_id) VALUES ($groups[$x], $newTableId)";
				$res = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			}
			
			header('Status: 302 moved');
			header("Location: ".$PHprefs['url']."/admin/column/xedni.php?TID=".$newTableId);
			exit();
		}
		
		header('Status: 302 moved');
		header("Location: index.php?reload=1");
		exit();
	}
	else {
		$errMsg = '<ul>';
		foreach($errors as $error)
			$errMsg .= '<li>' . $error . '</li>';
		$errMsg .= '</ul>';
		PH::phoundryErrorPrinter($errMsg);
	}
