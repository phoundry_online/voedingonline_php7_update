<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	header("Content-type: text/html; charset={$PHprefs['charset']}");
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess('admin');
	
	$sqls = array();

	$TID = (int)$_GET['TID'];

	$DBcols = array();
	$db->ListTableFields('phoundry_table', $DBcols)
		or trigger_error("Cannot list fields for table phoundry_table: " . $db->Error(), E_USER_ERROR);
	$DBcols = array_flip($DBcols);

	$sql = "SELECT * FROM phoundry_table WHERE id = $TID";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	foreach($DBcols as $col=>$idx) {
		$DBcols[$col] = $db->FetchResult($cur,0,$col);
	}
	
	//$DBcols['id'] = $newTID;
	unset($DBcols['id']);
	$DBcols['order_by'] = $_GET['order_by'];
	$DBcols['string_id'] = $_GET['string_id'];

	$sql = 'INSERT INTO phoundry_table (';
	$sql .= implode(',', array_keys($DBcols));
	$sql .= ') VALUES (';
	$y = 0;
	foreach($DBcols as $name=>$value) {
		if ($y++ > 0) $sql .= ', ';
		$sql .= ($value === NULL) ? 'NULL' : "'" . escDBquote($value) . "'";
	}
	$sql .= ')';
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$newTID = $db->GetLastInsertId('phoundry_table', 'id');
	
	// Copy columns:

	$DBcols = array();
	$db->ListTableFields('phoundry_column', $DBcols)
		or trigger_error("Cannot list fields for table phoundry_column: " . $db->Error(), E_USER_ERROR);
	$DBcols = array_flip($DBcols);

	$sql = "SELECT * FROM phoundry_column WHERE table_id = $TID ORDER BY order_by";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		foreach($DBcols as $col=>$idx)
			$DBcols[$col] = $db->FetchResult($cur,$x,$col);
		unset($DBcols['id']);
		$DBcols['table_id'] = $newTID;
		$sql = 'INSERT INTO phoundry_column (';
		$sql .= implode(',', array_keys($DBcols));
		$sql .= ') VALUES (';
		$y = 0;
		foreach($DBcols as $name=>$value) {
			if ($y++ > 0) $sql .= ',';
			$sql .= ($value === NULL) ? 'NULL' : "'" . escDBquote($value) . "'";
		}
		$sql .= ')';
		$sqls[] = $sql;
	}

	foreach($sqls as $sql) {
		$res = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}

	header("Location: index.php?copied=$newTID");
?>
