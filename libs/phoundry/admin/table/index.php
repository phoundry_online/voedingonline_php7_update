<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');
	
	if (isset($PHprefs['freeze']) && $PHprefs['freeze']) {
		PH::phoundryErrorPrinter("This phoundry instance is frozen. Use Phoundry Sync to sync changes from Dev.", true);
	}

	if (!isset($PHprefs['DBschemas'])) {
		$PHprefs['DBschemas'] = array($PHprefs['DBname']);
	}

	$uneditable = array('phoundry_column', 'phoundry_group_column', 'phoundry_image', 'phoundry_image_cat', 'phoundry_notify', 'phoundry_table', 'phoundry_user_shortcut', 'phoundry_menu', 'phoundry_lock', 'phoundry_user_group', 'phoundry_note', 'phoundry_login', 'dtproperties');

	$DBtables = $PHtables = array();

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Tables</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="../DBinterface/tableDef.css" type="text/css" />
<style type="text/css">

.tableDef .keys td {
	height: 6px;
	border: none;
}

.tableDef .fields {
}

.tableDef .types {
}

.tableDef td {
	border: 1px solid #000;
	padding: 3px;
}

.key {
	background: red;
}

.notnull {
	color: #fff;
	background: #c00;
	font-weight: bold;
}

.null
{
	color: #fff;
	background: #444;
}

.col
{
	background: #ccc;
	color: #000;
}

.copied {
	background: #f66;
}

</style>
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript" src="../../core/csjs/jquery.tablesorter.pack.js"></script>
<script type="text/javascript">
//<![CDATA[

var activeRow = null, activeDBtable = null, activeTableID = null;

function setRow(row, DBtable, tableID) {
	if (activeRow)
		activeRow['elem'].className = activeRow['class'];
	activeRow = [];
	activeRow['elem']  = row;
	activeRow['class'] = row.className;
	row.className = 'active';

	activeTableID = tableID ? tableID : null;
	activeDBtable = DBtable ? DBtable : null;

	$('.but').prop('disabled',false);
	
	if (tableID) window.status = tableID;
}

function editRow() {
	_D.location='edit.php?TID='+activeTableID+'&tableName='+activeDBtable;
}

function copyTable() {
	var order = prompt('Enter order for copy of table:', ''),
	string_id = prompt('Enter new string_id for copy of the table:', '');
	if (order && /^[0-9]+/.test(order))
		_D.location = 'copy.php?TID='+activeTableID+'&tableName='+activeDBtable+
			'&order_by='+order+'&string_id='+string_id;
}

function dropTable() {
	if (confirm('Are you sure you want to drop table '+activeDBtable+'?')) {
		var url = 'drop.php?tableName='+activeDBtable;
		if (activeTableID)
			url += '&TID=' + activeTableID;
		_D.location=url;
	}
}

function showTableDef(table) {
	if (table == '') {
		$('#tableDefDiv').html('');
	}
	else {
		$.ajax({url:'../DBinterface/tableDef.php?<?= QS(0) ?>', data:{table:table}, dataType:'html', success:function(tableDef) {
			$('#tableDefDiv').html(tableDef);
		}
		});
	}
}

$(function() {
	$('#recordTBL').tablesorter({
		cssHeader: 'sort',
		cssAsc: 'sortHeaderDesc',
		cssDesc: 'sortHeaderAsc',
		widgets: ['zebra'],
		widgetZebra: {css:['odd','even']}
	});
});

<?php
	if (isset($_GET['reload']))
		print "parent.frames['PHmenu'].location.href='{$PHprefs['customUrls']['menu']}';\n";
?>

//]]>
</script>
</head>
<body class="frames">
<form>
<div id="headerFrame" class="headerFrame">
<?= getHeader('Tables') ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<fieldset><legend><b>Available tables</b></legend>
<select id="tables" onchange="showTableDef(this.options[this.selectedIndex].value)">
<?php
	foreach($PHprefs['DBschemas'] as $schema) {
		print '<optgroup label="Schema: ' . $schema . '">';
		$sql = "SHOW TABLES FROM `{$schema}`";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$tableName = $db->FetchResult($cur,$x,0);
			if (in_array($tableName, $uneditable))
				continue;
			$DBtables[] = $tableName;
			print '<option value="' . $schema . '.' . $tableName . '">' . $tableName . '</option>';
		}
		print "</optgroup>\n";
	}
?>
</select>
<input type="button" value="Add" onclick="_D.location='edit.php?tableName='+$('#tables').val()" />
<input type="button" value="Drop" onclick="_D.location='drop.php?tableName='+$('#tables').val()" />
<div id="tableDefDiv" style="margin-top:5px">&nbsp;</div>
</fieldset>

<fieldset><legend><b>Tables in Phoundry</b></legend>

	<input type="button" disabled="disabled" class="but" value="Add" onclick="_D.location='edit.php?tableName='+activeDBtable" />
	<img src="../../core/pics/div.gif" alt="|" width="3" height="18" />
	<input type="button" disabled="disabled" class="but" value="View" onclick="_D.location='view.php?TID='+activeTableID+'&amp;tableName='+activeDBtable" />
	<input type="button" disabled="disabled" class="but" value="Edit" onclick="_D.location='edit.php?TID='+activeTableID+'&amp;tableName='+activeDBtable" />
	<input type="button" disabled="disabled" class="but" value="Copy" onclick="copyTable()" />
	<input type="button" disabled="disabled" class="but" value="Delete" onclick="if(confirm('Are you sure?'))_D.location='delete.php?TID='+activeTableID" />
	<input type="button" disabled="disabled" class="but" value="Drop" onclick="dropTable()" />

<table id="recordTBL" class="sort-table" cellspacing="0" style="margin-top:5px">
<thead>
<tr>
	<th>TID</th>
	<th>Schema</th>
	<th>Tablename</th>
	<th>String Id</th>
	<th>Description</th>
	<th>Order</th>
	<?if (!empty($PHprefs['enableVersioning'])):?>
	<th>Versioning</th>
	<?endif?>
	<th>User filters</th>
	<th>System filters</th>
	<th>Events</th>
</tr>
</thead>
<tbody>
<?php
	$sql = "SELECT * FROM phoundry_table WHERE is_plugin = 0 ORDER BY order_by";
	$cur = $db->Query($sql) 
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$tid = (int)$db->FetchResult($cur,$x,'id');
		$tableName = $db->FetchResult($cur,$x,'name');
		$PHtables[$tid] = $tableName;

		print '<tr class="' . ($x & 1 ? 'even' : 'odd') . '" onclick="setRow(this,\'' . $db->FetchResult($cur,$x,'name') . '\',' . $tid . ')" ondblclick="editRow()">';
		print '<td>' . $tid . '</td>';
		if (preg_match('/(.*)\.(.*)/', $tableName, $reg)) {
			// Tablename includes schema name:
			print '<td>' . $reg[1] . '</td>';
			print '<td>' . $reg[2] . '</td>';
		}
		else {
			print '<td>' . $PHprefs['DBname'] . '</td>';

			if (strpos($tableName, 'phoundry_') === 0)
				print '<td><b style="color:#be361f">' . $tableName . '</td>';
			else
				print '<td>' . $tableName . '</td>';
		}
		$description = $db->FetchResult($cur,$x,'description');
		if (PH::is_serialized($description) && ($tmp = @unserialize($description)) !== false) {
			$description = $tmp[$PHprefs['languages'][0]];
		}
		print '<td>' . PH::htmlspecialchars($db->FetchResult($cur,$x,'string_id')) . '</td>';
		print '<td>' . PH::htmlspecialchars($description) . '</td>';
		print '<td>' . $db->FetchResult($cur,$x,'order_by') . '</td>';

		if (!empty($PHprefs['enableVersioning'])) {
			$extra = $db->FetchResult($cur, $x, 'extra');
			$extra = empty($extra) ? array() : unserialize($extra);
			print '<td align="center">';
			if (isset($extra['versioning']) && $extra['versioning'] == true) {
				print '<img class="icon" src="../../core/icons/tick.png" alt="" />';
			}
			print '</td>';
		}
		
		print '<td align="center">';
		$tmp = $db->Query("SELECT COUNT(*) FROM phoundry_filter WHERE table_id = $tid AND type = 'user'");
		if ($db->FetchResult($tmp,0,0) > 0)
			print '<img class="icon" src="../../core/icons/tick.png" alt="" />';
		print '</td>';

		print '<td align="center">';
		$tmp = $db->Query("SELECT COUNT(*) FROM phoundry_filter WHERE table_id = $tid AND type = 'system'");
		if ($db->FetchResult($tmp,0,0) > 0)
			print '<img class="icon" src="../../core/icons/tick.png" alt="" />';
		print '</td>';

		print '<td align="center">';
		$tmp = $db->Query("SELECT COUNT(*) FROM phoundry_event WHERE table_id = $tid");
		if ($db->FetchResult($tmp,0,0) > 0)
			print '<img class="icon" src="../../core/icons/tick.png" alt="" />';
		print '</td>';
		print "</tr>\n";
	}

	// Print the table that are still defined in Phoundry, but that have
	// been dropped from the database:
	$diff = array_diff($PHtables, $DBtables);
	$cnt = 0;
	foreach($diff as $id=>$name) {
		$class = ($cnt++ & 1) ? 'even' : 'odd';
		if ($cnt == 0) {
			print '<tr bgcolor="#ff6666"><td colspan="6">Tables no longer available in database:</td></tr>' . "\n";
		}
		print '<tr class="' . $class . '">';
		print '<td align="center"><a href="delete.php?TID=' . $id . '">delete</a></td>';
		print '<td>' . $name . '</td>';
		print "</tr>\n";
	}

?>
</tbody>
</table>
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>&nbsp;</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Cancel</button>
	</td>
	</tr></table>
</div>
</form>
</body>
</html>
