<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = (isset($_GET['TID'])) ? (int)$_GET['TID'] : '';
	$tableName = $_GET['tableName'];

	// What field(s) make up the primary key?
	$db->GetTableIndexDefinition($tableName, 'PRIMARY', $index)
		or trigger_error("Cannot get PRIMARY index for table $tableName", E_USER_ERROR);
	$primKey = array_keys($index['FIELDS']);

	$tbl = $db->Query("SELECT * FROM phoundry_table WHERE id = $TID");

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Table - View</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
</head>
<body class="frames">

<div id="headerFrame" class="headerFrame">
<?= getHeader("$tableName - View"); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td><input type="button" value="Edit" onclick="_D.location='edit.php?TID=<?= $TID ?>&amp;tableName=<?= $tableName ?>'" /></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

	<fieldset><legend><b class="req">Name</b></legend>
	<?= $tableName ?>
	</fieldset>

	<fieldset><legend><b class="req">Order</b> (<a href="#" onclick="winPrio(event,'order_by');return false">priorities</a>)</legend>
	<?= $db->FetchResult($tbl,0,'order_by'); ?>
	</fieldset>

	<fieldset><legend><b class="req">Description</b></legend>
	<?php
		$description = $db->FetchResult($tbl,0,'description');
		if (PH::is_serialized($description) && ($tmp = @unserialize($description)) !== false) {
			print '<table>';
			foreach($PHprefs['languages'] as $lang)
				print '<tr><td align="right">' . $lang . ':</td><td>' . $tmp[$lang] . "</td></tr>\n";
			print '</table>';
		}
		else
			print $description;
	?>
	</fieldset>

	<fieldset><legend><b class="req">Show in menu</b></legend>
	<?php
		$menu = $db->FetchResult($tbl,0,'show_in_menu');
		switch($menu) {
			case 'service': print 'Service'; break;
			case 'admin':   print 'Admin';   break;
			case 'admin_brickwork': print 'Brickwork admin'; break;
			case 'hide':    print '[Hide]';  break;
			default:
				preg_match('/tab(\d+)/', $menu, $reg);
				print $PHprefs['menuTabs'][$reg[1]];
		}
	?>
	</fieldset>
	
	<fieldset><legend><b>Max. #records</b></legend>
	<?= $db->FetchResult($tbl,0,'max_records'); ?>&nbsp;
	</fieldset>

	<fieldset><legend><b>Default record order</b></legend>
	<?= $db->FetchResult($tbl,0,'record_order'); ?>
	</fieldset>

	<fieldset><legend><b>Info</b></legend>
	<?php
		$info = $db->FetchResult($tbl,0,'info');
		if (PH::is_serialized($info) && ($tmp = @unserialize($info)) !== false) {
			print '<table>';
			foreach($PHprefs['languages'] as $lang)
				print '<tr><td align="right" valign="top">' . $lang . ':</td><td>' . PH::htmlspecialchars($tmp[$lang]) . "</td></tr>\n";
			print '</table>';
		}
		else
			print PH::htmlspecialchars($info);
	?>&nbsp;
	</fieldset>

	<fieldset><legend><b>Group(s)</b></legend>
	<?php
		$sql = "SELECT pg.name FROM phoundry_group pg, phoundry_group_table pgt WHERE pgt.table_id = $TID AND pgt.group_id = pg.id";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			print $db->FetchResult($cur,$x,'name') . '<br />';
		}
	?>&nbsp;
	</fieldset>

	<h2>Extra's</h2>

	<?php
		$extra = $db->FetchResult($tbl,0,'extra');
		$extra = empty($extra) ? array() : unserialize($extra);
	?>
	<fieldset><legend><b>Start URL</b></legend>
	<?= isset($extra['sUrl']) ? $extra['sUrl'] : '' ?>&nbsp;
	</fieldset>

	<fieldset><legend><b>Record overview URL</b></legend>
	<?= isset($extra['rUrl']) ? $extra['rUrl'] : '' ?>&nbsp;
	</fieldset>

	<fieldset><legend><b>Insert record URL</b></legend>
	<?= isset($extra['iUrl']) ? $extra['iUrl'] : '' ?>&nbsp;
	</fieldset>

	<fieldset><legend><b>Update record URL</b></legend>
	<?= isset($extra['uUrl']) ? $extra['uUrl'] : '' ?>&nbsp;
	</fieldset>

	<fieldset><legend><b>Delete record URL</b></legend>
	<?= isset($extra['dUrl']) ? $extra['dUrl'] : '' ?>&nbsp;
	</fieldset>

	<fieldset><legend><b>Copy record URL</b></legend>
	<?= isset($extra['cUrl']) ? $extra['cUrl'] : '' ?>&nbsp;
	</fieldset>

	<fieldset><legend><b>View record URL</b></legend>
	<?= isset($extra['vUrl']) ? $extra['vUrl'] : '' ?>&nbsp;
	</fieldset>

	<fieldset><legend><b>Global rolerights applicable?</b></legend>
	<?= (!isset($extra['rolerights_global']) || $extra['rolerights_global'] == true) ? 'Yes' : 'No' ?>
	</fieldset>
	
	<fieldset><legend><b>Detailed rolerights applicable?</b></legend>
	<?= (!isset($extra['rolerights_detail']) || $extra['rolerights_detail'] == true) ? 'Yes' : 'No' ?>
	</fieldset>

	<?php
		$sql = "SELECT pt.id AS tid, pf.id AS fid FROM phoundry_table pt, phoundry_filter pf WHERE pt.name = 'phoundry_event' AND pf.table_id = pt.id";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		print '<h2><a href="../../core/records.php?TID=' . $db->FetchResult($cur,0,'tid') . '&amp;PHfltr_' . $db->FetchResult($cur,0,'fid') . '=' . $TID . '">Events</a></h2>';
		$sql = "SELECT * FROM phoundry_event WHERE table_id = $TID";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			print '<fieldset><legend><b>' . $db->FetchResult($cur,$x,'event') . '</b></legend>';
			print '<pre>' . $db->FetchResult($cur,$x,'code') . '</pre>';
			print "</fieldset>\n";
		}
		if ($x == 0) {
			print '<b>None</b>';
		}
	?>

	<?php
		$sql = "SELECT pt.id AS tid, pf.id AS fid FROM phoundry_table pt, phoundry_filter pf WHERE pt.description LIKE '%User Filters%' AND pf.table_id = pt.id AND pf.type = 'user'";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		print '<h2><a href="../../core/records.php?TID=' . $db->FetchResult($cur,0,'tid') . '&amp;PHfltr_' . $db->FetchResult($cur,0,'fid') . '=' . $TID . '">User Filters</a></h2>';

		$sql = "SELECT pf.group_id, pf.name, pf.required, pf.query, pf.matchfield, pf.wherepart, pg.name as group_name FROM phoundry_filter pf LEFT JOIN phoundry_group pg ON pf.group_id = pg.id WHERE table_id = $TID AND type = 'user'";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			print '<fieldset><legend><b>' . $db->FetchResult($cur,$x,'name') . '</b></legend>';
			print '<b>Required?</b> ' . ($db->FetchResult($cur,$x,'required') == 1 ? 'yes' : 'no') . '<br />';
			print '<b>Group:</b> ';
			print ($db->FetchResult($cur,$x,'group_name') ? $db->FetchResult($cur,$x,'group_name') : '[all]') . '<br />';
			print '<b>Query:</b> ' . PH::htmlspecialchars($db->FetchResult($cur,$x,'query')) . '<br />';
			print '<b>Matchfield:</b> ' . $db->FetchResult($cur,$x,'matchfield') . '<br />';
			print '<b>Wherepart:</b> ' . $db->FetchResult($cur,$x,'wherepart');
			print "</fieldset>\n";
		}
		if ($x == 0)
			print '<b>None</b>';
	?>


	<?php
		$sql = "SELECT pt.id AS tid, pf.id AS fid FROM phoundry_table pt, phoundry_filter pf WHERE pt.description LIKE '%System Filters%' AND pf.table_id = pt.id AND pf.type = 'user'";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		print '<h2><a href="../../core/records.php?TID=' . $db->FetchResult($cur,0,'tid') . '&amp;PHfltr_' . $db->FetchResult($cur,0,'fid') . '=' . $TID . '">System Filters</a></h2>';

		$sql = "SELECT pf.group_id, pf.name, pf.wherepart, pg.name as group_name FROM phoundry_filter pf LEFT JOIN phoundry_group pg ON pf.group_id = pg.id WHERE table_id = $TID AND type = 'system'";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			print '<fieldset><legend><b>' . $db->FetchResult($cur,$x,'name') . '</b></legend>';
			print '<b>Group:</b> ';
			print ($db->FetchResult($cur,$x,'group_name') ? $db->FetchResult($cur,$x,'group_name') : '[all]') . '<br />';
			print implode(' AND ', PH::getFilter(unserialize($db->FetchResult($cur,$x,'wherepart'))));
			print "</fieldset>\n";
		}
		if ($x == 0)
			print '<b>None</b>';

	?>

	<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='index.php'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
