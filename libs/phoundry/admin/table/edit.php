<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	$TID = (isset($_GET['TID'])) ? (int)$_GET['TID'] : '';
	$tableName = $_GET['tableName'];
	if (preg_match('/(.*)\.(.*)/', $tableName, $reg)) {
		// $tableName includes schema name.
		if ($reg[1] == $PHprefs['DBname']) {
			$tableName = $reg[2];
		}
	}

	// What field(s) make up the primary key?
	$db->GetTableIndexDefinition($tableName, 'PRIMARY', $index)
		or trigger_error("Cannot get PRIMARY index for table $tableName: " . $db->Error(), E_USER_ERROR);
	$primKey = array_keys($index['FIELDS']);

	$tbl = '';
	if (!empty($TID)) {
		$tbl = $db->Query("SELECT * FROM phoundry_table WHERE id = $TID");
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Table - Edit</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript" src="../csjs/dynform.js.php"></script>
<script type="text/javascript">
//<![CDATA[

function winPrio(e, field) {
   var fieldEl = _D.forms[0][field];
   makePopup(e,220,150,'Priorities','prio.php');
}

//]]>
</script>
</head>
<body class="frames">
<form action="editRes.php" method="post" onsubmit="return checkForm(this)">
<input type="hidden" name="tableName" value="<?= $tableName ?>" />
<input type="hidden" name="TID" value="<?= $TID ?>" />

<div id="headerFrame" class="headerFrame">
<?php
	$action = (empty($TID)) ? 'Add' : 'Edit';
	print getHeader(htmlspecialchars($tableName) . " - $action");
?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD"><b>The <span class="req">red</span> fields are required!</b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->

	<fieldset><legend><b class="req">Name</b></legend>
	<?=htmlspecialchars($tableName)?>
	</fieldset>

	<fieldset><legend><b class="req">String Id</b></legend>
	<div class="miniinfo">Unique string representation of this table. This is used for syncing tables.</div>
	<input type="text" name="string_id" alt="1|text" size="60" maxlength="80" value="<?=htmlspecialchars(empty($TID) ? $tableName : $db->FetchResult($tbl,0,'string_id'))?>" />
	</fieldset>
	
	<fieldset><legend><b class="req">Order</b> (<a href="#" onclick="winPrio(event,'order_by');return false">priorities</a>)</legend>
	<?php
		if (!empty($TID))
			$mo = $db->FetchResult($tbl,0,'order_by');
		else
		{
			$mo = 0;
			$sql = "SELECT MAX(order_by)+10 AS max_order FROM phoundry_table WHERE order_by < 100000";
			$cur = $db->Query($sql) 
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			if (!$db->EndOfResult($cur))
				if (!$db->ResultIsNull($cur,0,'max_order'))
					$mo = $db->FetchResult($cur,0,'max_order');
			if ($mo == '') $mo = 0;
		}
	?>
	<input type="text" name="order_by" alt="1|number" size="5" maxlength="10" value="<?= $mo ?>" /> (0-99999)
	</fieldset>

	<fieldset><legend><b class="req">Description</b></legend>
	<?php
		$langs = empty($TID) ? false : (PH::is_serialized($db->FetchResult($tbl,0,'description')) ? @unserialize($db->FetchResult($tbl,0,'description')) : $db->FetchResult($tbl,0,'description'));
		if ($langs === false) {
			$langs = array();
			foreach($PHprefs['languages'] as $lang) {
				$langs[$lang] = empty($TID) ? ucfirst(str_replace('_', ' ', preg_replace('/^(.*)\./', '', $tableName))) : $db->FetchResult($tbl,0,'description');
			}
		}
		foreach($PHprefs['languages'] as $lang) {
			print '<input type="text" name="description_' . $lang . '" alt="1|text" size="60" maxlength="80" value="' . PH::htmlspecialchars(isset($langs[$lang]) ? $langs[$lang] : '') . '" /> ' . $lang . '<br />';
		}
	?>
	</fieldset>

	<fieldset><legend><b class="req">Show in menu</b></legend>
	<select name="show_in_menu" alt="1|string">
	<?php
		$options = array();
		$options['hide'] = '[Hide]';
		foreach($PHprefs['menuTabs'] as $tabID=>$name) {
			$options["tab{$tabID}"] = $name;
		}
		$options['service'] = 'Service';
		$options['admin'] = 'Admin';
		$options['admin_brickwork'] = 'Brickwork admin';
		foreach($options as $value=>$name) {
			$selected = (!empty($TID) && $db->FetchResult($tbl,0,'show_in_menu') == $value) ? ' selected="selected"' : '';
			print '<option value="' . $value . '"' . $selected . '>' . $name . '</option>';
		}
	?>
	</select>
	</fieldset>
	
	<fieldset><legend><b>Max. #records</b></legend>
	<input type="text" name="max_records" alt="0|number" size="5" maxlength="11" value="<?php if(!empty($TID)) echo $db->FetchResult($tbl,0,'max_records') ?>" />
	</fieldset>

	<fieldset><legend><b>Default record order</b></legend>
	<select name="record_order">
	<option value="">[none]</option>
	<?php
		if (!empty($TID))
			$record_order = $db->FetchResult($tbl,0,'record_order');
		else
			$record_order = $primKey[0] . ' desc';

		$db->ListTableFields($tableName, $DBcolnames)
			or trigger_error("Cannot list fields for table $tableName: " . $db->Error(), E_USER_ERROR);
		foreach($DBcolnames as $colName) {
			$selected = (preg_match("/^$colName/", $record_order)) ? 'selected="selected"' : '';
			print '<option ' . $selected . ' value="' . $colName . '">' . $colName . '</option>';
		}
	?>
	</select>
	<select name="record_order_asc_desc">
	<?php
		if (preg_match('/desc$/i', $record_order)) {
			print '<option value="ASC">ascending</option>';
			print '<option value="DESC" selected="selected">descending</option>';
		}
		else {
			print '<option value="ASC" selected="selected">ascending</option>';
			print '<option value="DESC">descending</option>';
		}
	?>
	</select>
	</fieldset>

	<fieldset><legend><b>Info</b></legend>
	<?php
		$langs = empty($TID) ? false : (PH::is_serialized($db->FetchResult($tbl,0,'info')) ? @unserialize($db->FetchResult($tbl,0,'info')) : $db->FetchResult($tbl,0,'info'));
		if ($langs === false) {
			$langs = array();
			foreach($PHprefs['languages'] as $lang)
				$langs[$lang] = empty($TID) ? '' : $db->FetchResult($tbl,0,'info');
		}
		foreach($PHprefs['languages'] as $lang)
			print '<textarea name="info_' . $lang . '" cols="60" rows="3" wrap="soft">' . PH::htmlspecialchars(isset($langs[$lang]) ? $langs[$lang] : '') . '</textarea> ' . $lang . '<br />';
	?>
	</fieldset>

	<fieldset><legend><b>Group(s)</b> (<a href="#" onclick="selectAll(_D.forms[0]['groups[]']);return false">select all</a>)</legend>
	<div class="miniinfo">Select the users that will have access to this table.</div>
	<select multiple="multiple" size="5" name="groups[]">
	<?
		$sql = "SELECT id, name FROM phoundry_group ORDER BY name";
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$selected = ' ';
			if (!empty($TID)) {
				$sql = "SELECT * FROM phoundry_group_table WHERE group_id = " . $db->FetchResult($cur,$x,'id') . " AND table_id = $TID";
				$tmp = $db->Query($sql) 
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				if (!$db->EndOfResult($tmp)) $selected = ' selected="selected" ';
			}
			print '<option' . $selected . 'value="' . $db->FetchResult($cur,$x,'id') . '">' . $db->FetchResult($cur,$x,'name') . "</option>\n";
		}
	?>
	</select>
	</fieldset>

	<h2>Extra's</h2>

	<?php
		$extra = array();
		if (!empty($TID)) {
			$extra = $db->FetchResult($tbl,0,'extra');
			$extra = empty($extra) ? array() : unserialize($extra);
		}
	?>
	<fieldset><legend><b>Start URL</b></legend>
	<input type="text" name="sUrl" alt="0|string" size="60" value="<?= isset($extra['sUrl']) ? $extra['sUrl'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>Record overview URL</b></legend>
	<input type="text" name="rUrl" alt="0|string" size="60" value="<?= isset($extra['rUrl']) ? $extra['rUrl'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>Insert record URL</b></legend>
	<input type="text" name="iUrl" alt="0|string" size="60" value="<?= isset($extra['iUrl']) ? $extra['iUrl'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>Update record URL</b></legend>
	<input type="text" name="uUrl" alt="0|string" size="60" value="<?= isset($extra['uUrl']) ? $extra['uUrl'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>Delete record URL</b></legend>
	<input type="text" name="dUrl" alt="0|string" size="60" value="<?= isset($extra['dUrl']) ? $extra['dUrl'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>Copy record URL</b></legend>
	<input type="text" name="cUrl" alt="0|string" size="60" value="<?= isset($extra['cUrl']) ? $extra['cUrl'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>View record URL</b></legend>
	<input type="text" name="vUrl" alt="0|string" size="60" value="<?= isset($extra['vUrl']) ? $extra['vUrl'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>Are global role rights applicable?</b></legend>
	<?php
		if (!isset($extra['rolerights_global']) || $extra['rolerights_global'] == true) {
			print '<input type="radio" name="rolerights_global" value="1" checked="checked" /> Yes ';
			print '<input type="radio" name="rolerights_global" value="0" /> No';
		}
		else {
			print '<input type="radio" name="rolerights_global" value="1" /> Yes ';
			print '<input type="radio" name="rolerights_global" value="0" checked="checked" /> No';
		}
	?>
	</fieldset>

	<fieldset><legend><b>Are detailed role rights applicable?</b></legend>
	<?php
		if (!isset($extra['rolerights_detail']) || $extra['rolerights_detail'] == true) {
			print '<input type="radio" name="rolerights_detail" value="1" checked="checked" /> Yes ';
			print '<input type="radio" name="rolerights_detail" value="0" /> No';
		}
		else {
			print '<input type="radio" name="rolerights_detail" value="1" /> Yes ';
			print '<input type="radio" name="rolerights_detail" value="0" checked="checked" /> No';
		}
	?>
	</fieldset>

	<fieldset><legend><b>Show 'Ok &amp; Insert again' button on insert page?</b></legend>
	<?php
		if (!isset($extra['ok_and_ins_again']) || $extra['ok_and_ins_again'] == true) {
			print '<input type="radio" name="ok_and_ins_again" value="1" checked="checked" /> Yes ';
			print '<input type="radio" name="ok_and_ins_again" value="0" /> No';
		}
		else {
			print '<input type="radio" name="ok_and_ins_again" value="1" /> Yes ';
			print '<input type="radio" name="ok_and_ins_again" value="0" checked="checked" /> No';
		}
	?>
	</fieldset>
	
	<?if (!empty($PHprefs['enableVersioning'])):?>
	<fieldset><legend><b>Versioning</b></legend>
	<?php
		if (isset($extra['versioning']) && $extra['versioning'] == true) {
			print '<input type="radio" name="versioning" value="1" checked="checked" /> Yes ';
			print '<input type="radio" name="versioning" value="0" /> No';
		}
		else {
			print '<input type="radio" name="versioning" value="1" /> Yes ';
			print '<input type="radio" name="versioning" value="0" checked="checked" /> No';
		}
	?>
	</fieldset>
	<fieldset><legend><b>Versioning message</b></legend>
	<?php 
		$selected = "";
		if (isset($extra['versioning_message'])) {
			$selected = $extra['versioning_message'];
		}
	?>
	<select name="versioning_message">
		<option value="off"<?=$selected==="off"?' selected="selected"':""?>>Off</option>
		<option value="optional"<?=$selected==="optional"?' selected="selected"':""?>>Optional</option>
		<option value="required"<?=$selected==="required"?' selected="selected"':""?>>Required</option>
	</select>
	</fieldset>
	<?endif?>

	<fieldset><legend><b>Reload menu after</b></legend>
	<div class="miniinfo">Enter the actions whereafter the menu needs to be reloaded: [insert,update,delete,copy]. Example: <b>insert,update</b>.</div>
	<input type="text" name="reloadMenu" alt="0|string" size="60" value="<?= isset($extra['reloadMenu']) ? $extra['reloadMenu'] : '' ?>" />
	</fieldset>

	<fieldset><legend><b>Timetable</b></legend>
	<table><tr>
	<td valign="top">
	Show fields:<br />
	<select name="timetable_fields[]" size="6" multiple="multiple">
	<?php
		$sql = "SHOW COLUMNS FROM " . escDBquote($tableName);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$field = $db->FetchResult($cur,$x,'Field');
			$selected = isset($extra['timetable']) && in_array($field, $extra['timetable']['showFields']) ? ' selected="selected" ' : ' ';
			print '<option' . $selected . 'value="' . $field . '">' . $field . '</option>';
		}
	?>
	</select>
	</td>
	<td valign="top">
	From: <select name="timetable_from">
	<option value="">[Select]</option>
	<?php
		$sql = "SHOW COLUMNS FROM " . escDBquote($tableName);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$field = $db->FetchResult($cur,$x,'Field');
			if (preg_match('/^date/', $db->FetchResult($cur,$x,'Type'))) {
				$selected = isset($extra['timetable']) && $extra['timetable']['fromField'] == $field ? ' selected="selected" ' : ' ';
				print '<option' . $selected . 'value="' . $field . '">' . $field . '</option>';
			}
		}
	?>
	</select>
	Till: <select name="timetable_till">
	<option value="">[Select]</option>
	<?php
		$sql = "SHOW COLUMNS FROM " . escDBquote($tableName);
		$cur = $db->Query($sql) 
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$field = $db->FetchResult($cur,$x,'Field');
			if (preg_match('/^date/', $db->FetchResult($cur,$x,'Type'))) {
				$selected = isset($extra['timetable']) && $extra['timetable']['tillField'] == $field ? ' selected="selected" ' : ' ';
				print '<option' . $selected . 'value="' . $field . '">' . $field . '</option>';
			}
		}
	?>
	</select>
	Timespan: <input type="text" name="timetable_timespan" size="3" value="<?= isset($extra['timetable']) ? $extra['timetable']['timespan'] : '7' ?>"/>
	<br /><br />
	WHERE part:<br />
	<input type="text" name="timetable_where" size="60" value="<?= (isset($extra['timetable']) ? PH::htmlspecialchars($extra['timetable']['where']) : '') ?>"/>
	</td>
	</tr></table>
	</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Submit" />
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='index.php'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
