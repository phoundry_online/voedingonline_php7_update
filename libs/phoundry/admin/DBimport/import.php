<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	set_time_limit(0);

	$how        = $_POST['how'];
	$charset    = empty($_POST['charset']) ?
		$PHprefs['charset'] : $_POST['charset'];
	switch ($charset) {
		case 'utf-8':
		case 'utf8':
			$charset = 'utf8';
			break;

		case 'iso-8859-1':
		case 'latin1':
			$charset = 'latin1';
			break;

		default:
			$charset = 'utf8';
			break;
	}
	$php_name   = $_FILES['file']['tmp_name'];
	$org_name   = $_FILES['file']['name'];
	$mysql_exec = 'mysql';

	require "{$PHprefs['distDir']}/core/include/metabase/metabase_parser.php";
	require "{$PHprefs['distDir']}/core/include/metabase/metabase_manager.php";
	require "{$PHprefs['distDir']}/core/include/metabase/xml_parser.php";

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Database Import</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
</script>

<body class="frames">

<div id="headerFrame" class="headerFrame">
<?= getHeader('Database Import'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td><b>Importing <?= PH::htmlspecialchars($org_name) ?>...</b></td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?php

	if ($how == 'xml') {
		// XML import:
		$arguments = array(
			'Type'=>$PHprefs['DBtype'],
			'Host'=>$PHprefs['DBserver'],
			'User'=>$PHprefs['DBuserid'],
			'Password'=>$PHprefs['DBpasswd'],
			'Port'=>$PHprefs['DBport'],
			'IncludePath'=>"{$PHprefs['distDir']}/core/include/metabase/",
		);
		$variables = array();

		$manager = new metabase_manager_class;
		$manager->fail_on_invalid_names = false;

		if (!($success = $manager->InitialiseDatabase($php_name, $arguments, $variables)))
			print '<p><b>Error: ' . $manager->error . '</b></p>';
		else
			print '<p><b>Success!</b></p>';
	}

	if ($how == 'mysql') {
		// MySQL import:
		$out = '';
		$cmd = "{$mysql_exec} --host=" . escapeshellarg($PHprefs['DBserver']) . ' --user=' . escapeshellarg($PHprefs['DBuserid']) . ' --password=' . escapeshellarg($PHprefs['DBpasswd']) . ' --default-character-set=' . escapeshellarg($charset) . ' ' . escapeshellarg($PHprefs['DBname']) . ' < ' . escapeshellarg($php_name);
		$out = shell_exec($cmd . ' 2>&1');
		if ($out == '') {
			print '<p><b>Success!</b></p>';
		}
		else {
			print '<p><b>' . $out . '</b></p>';
		}
	}

	if ($how == 'csv') {

		// CSV import:
		$error = "";
		$db_table = "csv_test";
		$success = false;

		// File exists?
		if (empty($php_name)) {
			$error = "No file was uploaded.";
		} else {
			// Get first row from csv, which should be the column names
			ini_set('auto_detect_line_endings',true);
			$fp = fopen($php_name, 'r+');
			$columns = fgetcsv($fp, 1000, ";");
			if (empty($columns)) {
				$error = "Uploaded file was empty or unreadable.";
			}
			foreach ($columns as $key=>$col) {
				$columns[$key] = '`'.$col.'`';
			}
		}

		if (empty($error)) {

			// Retreive a description of the db table
			// var_dump($columns);

			//$db->FetchResult($cur,0,0);
			//print PH::htmlspecialchars($db->FetchResult($cur,0,0));

			$query = sprintf("
				LOAD DATA LOCAL INFILE '%s'
				INTO TABLE `%s`
				FIELDS TERMINATED BY ';'
				LINES TERMINATED BY '\r\n'
				IGNORE 1 LINES
				( %s );
			",
				$php_name,
				$db_table,
				implode(", ", $columns)
			);

			$cur = $db->Query($query);

			if (!empty($db->last_error)) {
				$error = $db->last_error;
			} else {
				$success = true;
			}

		}

		if (!$success)
			print '<p><b>Error: ' . $error . '</b></p>';
		else
			print '<p><b>Success!</b></p>';

	}

?>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<button type="button" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'">Back</button>
	</td>
	</tr></table>
</div>
</body>
</html>
