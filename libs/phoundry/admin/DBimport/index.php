<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess('admin');

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Database Import</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

function importAs(what) {
	if (what == 'xml' || what == 'csv') { $('#mysqlDiv').hide(); }
	else if (what == 'mysql') { $('#mysqlDiv').show(); }
}

//]]>
</script>
</head>
<body class="frames">
<form action="import.php" method="post" enctype="multipart/form-data">

<div id="headerFrame" class="headerFrame">
<?= getHeader('Database Import'); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
	
<fieldset><legend><b>Import as</b></legend>
<select name="how" onchange="importAs(this.options[this.selectedIndex].value)">
<option value="xml">XML</option>
<?php if ($PHprefs['DBtype'] == 'mysql') { ?>
<option value="mysql">MySQL</option>
<?php } ?>
<option value="csv">CSV</option>
</select>
</fieldset>

<fieldset><legend><b>Import file</b></legend>
<input type="file" name="file" size="40" />
</fieldset>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Import" onclick="_D.forms[0].target=(event.shiftKey)?'_blank':'_self'" />
	</td>
	<td align="right">
	<input type="button" value="Cancel" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
	</tr></table>
</div>

</form>
</body>
</html>
