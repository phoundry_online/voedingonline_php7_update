<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once 'translate.php';
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess();

	$db->SetCharset('utf8');

	$unwritables = array();
	$words = array();
	foreach($PREFS['langs'] as $lang) 
	{
		unset($coreWORDS);
		$incFile = str_replace('{$lang}', $lang, $PREFS['wordFiles'][$_GET['what']]['file']);
		if (!is_writable($incFile)) {
			$unwritables[] = $incFile;
		}
		include $incFile;
		$words[$lang] = $coreWORDS;
	}

	PH::getHeader('text/html', 'utf-8');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Words</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<style type="text/css">

.word {
	width:150px;
}

.focusedWord {
	width:260px;
	height:70px;
}

</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">

function setWord(el) {
	$el = $(el);
	$el.val($el.parents('tr').find('.sourceWord').text());
}

function swapEl(el) {
	el.className = (el.className == 'word') ? 'focusedWord' : 'word';
}

</script>
</head>
<body style="background:transparent">
<?php	if (count($unwritables) > 0) { ?>
<fieldset><legend class="caution"><img class="icon" src="<?= $PHprefs['url'] ?>/core/icons/exclamation.png" alt="" align="left" />Caution!</legend>
The following files are not writable, you cannot save changes to them:
<ul>
<?php
	foreach($unwritables as $unwritable) {
		print '<li>' . $unwritable . '</li>';
	}
?>
</ul>
</fieldset><br />
<?php	} ?>
<form action="file_wordsRes.php?<?= QS(1) ?>" method="post">
<table id="recordTBL" class="sort-table" cellspacing="0">
<tr><th colspan="2"><?= ucfirst($_GET['what']) ?></th>
<?php
	foreach($PREFS['langs'] as $lang) {
		print '<th>' . $lang . '</th>';
	}
?>
</tr>
<?php

$row = 0;
foreach($words[$PREFS['langs'][0]] as $idx=>$word) {
	print '<tr class="' . ($row & 1 ? 'even' : 'odd') . '">';
	print '<td><b>' . $idx . '</b></td>';
	print '<td class="sourceWord">' . PH::htmlspecialchars($words[$PREFS['langs'][0]][$idx]) . '</td>';
	foreach($PREFS['langs'] as $lang) {
		print '<td nowrap="nowrap"><textarea class="word" name="word___' . $idx . '[' . $lang . ']" cols="20" rows="1" ondblclick="setWord(this)" onfocus="swapEl(this)" onblur="swapEl(this)" />' . PH::htmlspecialchars($words[$lang][$idx]) . '</textarea></td>';
	}
	print "</tr>\n";
	$row++;
}

?>
</table>
<input type="submit" value="Save" />

</form>
</body>
</html>
