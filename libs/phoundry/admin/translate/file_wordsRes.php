<?php
	header('Content-type: text/html; charset=utf-8');
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once 'translate.php';
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess();
	$db->SetCharset('utf8');

	$translations = array();
	foreach($PREFS['langs'] as $lang) {
		$translations[$lang] = array();
	}

	foreach($_POST as $name=>$value) {
		$bla = explode('___', $name);
		$idx = $bla[1];

		foreach($value as $lang=>$translation) {
			$translations[$lang][$idx] = trim($translation);
		}
	}

	$nrErrors = 0;
	foreach($translations as $lang=>$words) {
		$incFile = str_replace('{$lang}', $lang, $PREFS['wordFiles'][$_GET['what']]['file']);
		if ($fp = @fopen($incFile, 'w')) {
			fwrite($fp, "<?php\n//\n// This is a UTF-8 file (中文)!!!\n// Please keep it like that...\n//\n\n\$myWORDS = array(\n");
			foreach($words as $idx=>$word) {
				fwrite($fp, "{$idx} => \"" . str_replace(array('"', "\r", "\n", '{$'), array('\"', '', "\\n", '{\$'), $word) . "\",\n");
			}
			fwrite($fp, ");\n\$coreWORDS = isset(\$coreWORDS) ? \$myWORDS + \$coreWORDS : \$myWORDS;\nunset(\$myWORDS);");
			fclose($fp);
		}
		else {
			$nrErrors++;
			print "Cannot save words to {$incFile}!<br />\n";
		}
	}

	if ($nrErrors == 0) {
		header('Location: file_words.php?' . QS(0));
	}
	exit;
