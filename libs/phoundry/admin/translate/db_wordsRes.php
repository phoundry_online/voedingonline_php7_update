<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
    require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header('Content-type: text/html; charset=' . $PHprefs['charset']);
require_once 'translate.php';
require_once "{$PHprefs['distDir']}/core/include/common.php";

// Make sure the current user in the current role has access to this page:
checkAccess();

function trimStrings($str)
{
    return is_string($str) ? trim($str) : $str;
}

if (isset($_POST['serialized'])) {
    parse_str($_POST['serialized'], $entries);
} else {
    $entries = $_POST;
}

foreach ($entries as $name => $value) {
    $bla = explode('___', $name);
    if (count($bla) < 3) {
        continue;
    }
    $tableName = $bla[0];
    $fieldName = $bla[1];
    $id = $bla[2];

    $value = array_map('trimStrings', $value);

    $sql = "UPDATE {$tableName} SET {$fieldName} = '" . escDBquote(serialize($value)) . "' WHERE id = {$id}";
    $db->Query($sql)
        or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
}

echo 'OK';
exit;
