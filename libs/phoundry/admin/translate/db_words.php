<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		@require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once 'translate.php';
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess();

	function getWords($table, $field, $where = '') {
		global $db, $PREFS, $myLang;
		static $even = false;
		$html = '';
		$sql = "SELECT id, {$field} FROM {$table} WHERE ({$field} != '' AND {$field} IS NOT NULL)";
		if (!empty($where)) {
			$sql .= " AND {$where}";
		}
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for($x = 0; !$db->EndOfResult($cur); $x++) {
			$id   = $db->FetchResult($cur,$x,'id');
			$word = $db->FetchResult($cur,$x,$field);

			if (PH::is_serialized($word) && ($tmp = @unserialize($word)) !== false) {
				foreach($PREFS['langs'] as $l) {
					$word = isset($tmp[$l]) ? $tmp[$l] : '';
					if (!empty($word)) {
						//$word = $l . ':' . $word;
						break;
					}
				}
			}
			else {
				$tmp = array($PREFS['langs'][0]=>$word);
			}
			
			if ($word == '') continue;
			$html .= '<tr class="' . ($even ? 'even' : 'odd') . '">';

			$html .= '<td class="sourceWord">' . PH::htmlspecialchars($word) . '</td>';

			foreach($PREFS['langs'] as $l) {
				$word = isset($tmp[$l]) ? $tmp[$l] : '';
				$class = 'word';
				if (!$word) $class .= ' missing';
				$html .= '<td nowrap="nowrap"><textarea class="' . $class . '" cols="20" rows="1" name="' . $table . '___' . $field . '___' . $db->FetchResult($cur,$x,'id') . '[' . $l . ']" ondblclick="setWord(this)" onfocus="swapEl(this)" onblur="swapEl(this)" />' . PH::htmlspecialchars($word) . '</textarea></td>';
			}
			$html .= "</tr>\n";
			$even = !$even;
		}
		return $html;
	}

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Words</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<style type="text/css">

.word {
	width:150px;
}

.focused {
	width:260px;
	height:70px;
}

.missing {
	border: 1px solid red;
}

</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

function setWord(el) {
	$el = $(el);
	$el.val($el.parents('tr').find('.sourceWord').text());
}

function swapEl(el) {
	$el = $(el);
	$el.toggleClass('focused');
	if ($el.val() != '') $el.removeClass('missing');
	else $el.addClass('missing');
}

function setDefaults() {
	$('textarea').each(function(i) {
		var $el = $(this);
		if ($el.val() == '') {
			$el.val($el.parents('tr').find('.sourceWord').text()).removeClass('missing');

		}
	});
}

jQuery(function($) {
	$('form').bind('submit', function(e) {
		var serialized = $(this).serialize();
		$.ajax({
			url : $(this).attr('action'),
			data: {'serialized' : serialized},
			type: 'POST',
			success: function(text) {
				if (text == 'OK') {
					window.location.reload();
				} else {
					alert(text);
				}
			},
			error: function(text) {
				alert('error' + text);
			}
		});
		e.preventDefault();
	});
});
	
//]]>
</script>
</head>
<body style="background:transparent">
<form action="db_wordsRes.php?<?= QS(1) ?>" method="post">

	<input type="button" onclick="setDefaults()" value="Set defaults" />
	<input type="submit" value="Save" />

<table id="recordTBL" class="sort-table" cellspacing="0">
<tr><th style="width:60%"><a name="phoundry_table.description">phoundry_table.description</a> (<?= $PREFS['langs'][0] ?>)</th>
<?php	foreach($PREFS['langs'] as $lang) { print '<th>' . $lang . '</th>'; } ?>
</tr>
<?= getWords('phoundry_table', 'description'); ?>

<tr><th><a name="phoundry_table.info">phoundry_table.info</a> (<?= $PREFS['langs'][0] ?>)</th>
<?php	foreach($PREFS['langs'] as $lang) { print '<th>' . $lang . '</th>'; } ?>
</tr>
<?= getWords('phoundry_table', 'info'); ?>

<tr><th><a name="phoundry_column.description">phoundry_column.description</a> (<?= $PREFS['langs'][0] ?>)</th>
<?php	foreach($PREFS['langs'] as $lang) { print '<th>' . $lang . '</th>'; } ?>
</tr>
<?= getWords('phoundry_column', 'description'); ?>

<tr><th><a name="phoundry_column.info">phoundry_column.info</a> (<?= $PREFS['langs'][0] ?>)</th>
<?php	foreach($PREFS['langs'] as $lang) { print '<th>' . $lang . '</th>'; } ?>
</tr>
<?= getWords('phoundry_column', 'info'); ?>

<tr><th><a name="phoundry_filter.name">phoundry_filter.name</a> (<?= $PREFS['langs'][0] ?>)</th>
<?php	foreach($PREFS['langs'] as $lang) { print '<th>' . $lang . '</th>'; } ?>
</tr>
<?= getWords('phoundry_filter', 'name', "type='user'"); ?>

<tr><th><a name="phoundry_menu.name">phoundry_menu.name</a> (<?= $PREFS['langs'][0] ?>)</th>
<?php	foreach($PREFS['langs'] as $lang) { print '<th>' . $lang . '</th>'; } ?>
</tr>
<?= getWords('phoundry_menu', 'name'); ?>
</table>

	<input type="button" onclick="setDefaults()" value="Set defaults" />
	<input type="submit" value="Save" />

</form>

</body>
</html>
