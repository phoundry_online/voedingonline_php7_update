<?php
global $PHprefs;

if (!isset($PHprefs)) {
	error_log('PHprefs not defined in ' . __FILE__);
	exit();
}

$PREFS = array(
	// All languages we support.
	'langs' => $PHprefs['languages'],
	'wordFiles' => array(
		'phoundry' => array('file'=>$PHprefs['distDir'] . '/core/lang/{$lang}/words.php'),
		'dmdelivery' => array('file'=>$PHprefs['distDir'] . '/dmdelivery/lang/{$lang}/words.php', 'array'=>'coreWORDS'),
		'custom' => array('file'=>$PHprefs['instDir'] . '/custom/lang/{$lang}/words.php')
	)
);
