<?php
	$inc = @include_once('PREFS.php');
	if ($inc === false) {
		require_once(getenv('CONFIG_DIR') . '/PREFS.php');
	}
	require_once 'translate.php';
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	// Make sure the current user in the current role has access to this page:
	checkAccess();

	$lang = isset($_GET['_lang']) ? $_GET['_lang'] : 'en';

	PH::getHeader('text/html');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="Author" content="Phoundry BV, www.phoundry.nl" />
<title>Phoundry translations</title>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['customUrls']['css'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

var curTab = 'tab1';
function setTab(tab) {
	$('#'+curTab).attr('class', 'inactive');
	$('#'+tab).attr('class', 'active');
   curTab = tab;
}

//]]>
</script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?php
	print getHeader('Translations', 'Based on $PHprefs[\'languages\']: ' . implode(',', $PREFS['langs']));
?>
<div id="tabstrip" class="tabstrip-bg">
<ul>
<li id="tab1" class="active"><a href="db_words.php?<?= QS(1) ?>" target="translateFrame" onclick="setTab('tab1');this.blur()">DB words</a></li>
<?php
	$idx = 2;
	foreach($PREFS['wordFiles'] as $what=>$props) {
		if (file_exists(str_replace('{$lang}', 'en', $props['file']))) {
			print '<li id="tab' . $idx . '" class="inactive"><a href="file_words.php?' . QS(1,'what=' . $what) . '" target="translateFrame" onclick="setTab(\'tab' . $idx . '\');this.blur()">' . ucfirst($what) . ' words</a></li>';
			$idx++;
		}
	}
?>
</ul>
</div>
</div>

<div id="contentFrame" class="contentFrame" style="padding:0;overflow:hidden">
<!-- Content -->

<iframe id="translateFrame" name="translateFrame" src="db_words.php?<?= isset($_GET['_anchor']) ? QS(1) . '#' . $_GET['_anchor'] : QS(1) ?>" frameborder="0" style="width:100%;height:100%" allowtransparency="true"></iframe>

<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	</td>
</tr></table>
</div>

</body>
</html>
