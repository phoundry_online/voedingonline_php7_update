<?php
if(!isset($_SERVER['argv'][1]) || !file_exists($_SERVER['argv'][1])) {
	die("Usage: ".basename($_SERVER['argv'][0]).' <path_to_prefs>'."\n");
}

$prefs = $_SERVER['argv'][1];
require_once $prefs;
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

/**
 * Parse Directory for files containing classes
 *
 * @param string $directory Pathname of directory
 * @return array
 */
function parseDirectory($directory, array $extensions, $hidden = false)
{
	$preg = array();
	foreach ($extensions as $ext) {
		$preg[] = preg_quote($ext, '/');
	}
	
	$dir = new RegexIterator(
		new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator($directory)
		),
		'/^.+\.('.implode('|', $preg).')$/i', RegexIterator::GET_MATCH
	);
	
	$classes = array();
	foreach ($dir as $file) {
		$file = new SplFileInfo($file[0]);
		if ((!$file->isFile() && !$file->isLink()) || !$file->isReadable()) {
			continue;
		}
		$filename = $file->getFilename();
		if(!$hidden && $filename[0] == '.') {
			continue;
		}
		
		$result = array();
		preg_match_all('~^\s*(?:abstract\s+|final\s+)?(?:class|interface)\s+(\w+)~mi',
			file_get_contents($file->getPathname()), $result);
		
		if(!empty($result[1])){
			foreach($result[1] as $class_name) {
				$classes[$class_name] = $file->getPathname();
			}
		}
	}
	
	return $classes;
}

$loader = Loader::getInstance()->getLoader('class');
$paths = $loader->getPaths();
$paths = array_reverse($paths);
$exts = array('php');
$classes = array();
/* @var $loader Loader_Class */
foreach ($paths as $path) {
	if (file_exists($path)) {
		$classes = array_merge($classes, parseDirectory($path, $exts));
	}
}

$cache_file = CACHE_DIR.'/autoloader.cache';
$temp = tempnam(dirname($cache_file),
	basename($cache_file));
	
file_put_contents($temp,
	'<'.'?php return ('.var_export($classes, true).');');
rename($temp, $cache_file);
chmod($cache_file, 0777);

echo 'Succesfully written cache for '.count($classes).' classes!'."\n";