<?php
$argv = $_SERVER['argv'];
$scriptname = array_shift($argv);
$prefs = array_shift($argv);

if(!$prefs || !file_exists($prefs)) {
	die("Usage: ".basename($scriptname).' <path_to_prefs>'."\r\n");
}

require_once $prefs;
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

require_once 'upgrade/PhoundrySync.php';

$script = new Brickwork_Upgrade_Script($scriptname, $argv, STDOUT, STDIN);
// @php When we're at php 5.3 we can change this in $script();
// $script('upgrade');
$script->__invoke('upgrade');
