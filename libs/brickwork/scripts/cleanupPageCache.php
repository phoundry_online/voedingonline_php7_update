<?php
if(!isset($_SERVER['argv'][1]) || !is_dir($_SERVER['argv'][1]) ||
	!file_exists( $_SERVER['argv'][1] . '/config/PREFS.php')) {
	die("Usage: " . basename($_SERVER['argv'][0])  . ' <projectdir>');
}

$projectdir = $_SERVER['argv'][1];

require_once $projectdir . '/config/PREFS.php';
if(!isset($WEBprefs['pagecache']['timeout'])) {
	die('$WEBprefs[\'pagecache\'][\'timeout\'] is not set in PREFS.php');
}

if(!defined("TESTMODE")) {
	define("TESTMODE", false);
}
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

$pv = PressureValve::getInstance();

if(!$pv->allowed(false)) {
	if(TESTMODE) {
		echo "Load too high, not running\n";
	}
	exit(0);
}

$timeout = isset($WEBprefs['pagecache']['timeout']) ?
	$WEBprefs['pagecache']['timeout'] : null;
	
$command = 'find '.CACHE_DIR.'/pages/ '.
	($timeout ? ('-mmin +'.round($timeout / 60)) : '').' -type f -delete';

if(TESTMODE) {
	if($timeout) {
		echo "Cleaning up files with modified time before ".
			date('Y-m-d H:i:s', time() - $timeout)."\n".$command."\n";
	}
	else {
		echo "Cleaning up all files\n".$command."\n";
	}
}
shell_exec($command);

$cleanup = 'cd '.CACHE_DIR.'/pages && find -depth -type d -empty -exec rmdir {} \\;';

if(TESTMODE) {
	echo "Removing empty directories: ".$cleanup."\n";
}
shell_exec($cleanup);
