<?php
$argv = $_SERVER['argv'];
$scriptname = array_shift($argv);
$task = trim(array_shift($argv));
if(!$task) {
	$task = 'dump';
}

require 'BuildTasks.php';
$config = require 'config.php';
$builder = new BuildTasks($config);

if(!in_array($task, $builder->getTasks())) {
	echo "Unknown task {$task}\n";
	echo "Valid tasks are:\n";
	echo implode("\n", $builder->getTasks());
	exit(1);
}

call_user_func_array(array($builder, $task), $argv);
