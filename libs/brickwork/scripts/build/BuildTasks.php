<?php
class BuildTasks
{
	private $config;
	private $db;
	
	public function __construct(array $config)
	{
		$this->config = $config;
	}
	
	public function getTasks()
	{
		return array(
			'dump', 'structure_dump', 'phoundry_dump', 'database_dump'
		);
	}
	
	public function dump()
	{
		$this->structure_dump();
		$this->phoundry_dump();
		$this->database_dump();
	}
	
	public function structure_dump()
	{
		if (!empty($this->config['structure']['exclude'])) {
			$exclude = $this->config['structure']['exclude'];
		} else if (!empty($this->config['structure']['include'])) {
			$exclude = array_diff(
				$this->allTables(),
				$this->config['structure']['include']
			);
		} else {
			$exclude = array();
		}
		
		require_once dirname(__FILE__).'/../../classes/DB/Sync.php';
		$db_sync = new DB_Sync($this->getDb());
		file_put_contents(
			$this->config['structure']['file'],
			implode(";\n\n", $db_sync->dump($exclude)).";\n"
		);
	}
	
	public function phoundry_dump()
	{
		if (!empty($this->config['phoundry']['exclude'])) {
			$exclude = $this->config['phoundry']['exclude'];
		} else if (!empty($this->config['phoundry']['include'])) {
			$exclude = array_diff(
				$this->allTables(),
				$this->config['phoundry']['include']
			);
		} else {
			$exclude = array();
		}
		
		require_once dirname(__FILE__).'/../upgrade/PhoundrySync.php';
		$phoundry_sync = new PhoundrySync($this->getDb());
		file_put_contents(
			$this->config['phoundry']['file'], 
			"<?php return(".var_export($phoundry_sync->exportAll($exclude), true).");"
		);
	}
	
	public function database_dump()
	{
		$db = $this->config['db_connection'];
		if (!empty($this->config['database']['exclude'])) {
			$included = array_diff(
				$this->allTables(),
				$this->config['database']['exclude']
			);
		} else if (!empty($this->config['database']['include'])) {
			$included = $this->config['database']['include'];
		} else {
			$included = array(); // nothing is everything
		}
		$sql_path = $this->config['database']['file'];
		
		shell_exec(
			sprintf(
				'mysqldump -h %s -u%s -p%s --extended-insert=FALSE '.
				'--no-tablespaces --skip-comments %s %s > %s',
				escapeshellarg($db['host']),
				escapeshellarg($db['username']),
				escapeshellarg($db['password']),
				escapeshellarg($db['name']),
				implode(' ', array_map('escapeshellarg', $included)),
				escapeshellarg($sql_path)
			)
		);
	}
	
	private function allTables()
	{
		$db = $this->getDb();
		$res = mysql_query('SHOW TABLES', $db);
		if (false === $res) {
			throw new Exception(mysql_error($db), mysql_errno($db));
		}
		
		$tables = array();
		while ($row = mysql_fetch_row($res)) {
			$tables[] = $row[0];
		}
		return $tables;
	}
	
	/**
	 * @return resource
	 */
	private function getDb()
	{
		if (null === $this->db) {
			$db = $this->config['db_connection'];
			
			$this->db = mysql_connect(
				$db['host'].':'.$db['port'],
				$db['username'],
				$db['password'],
				true
			);
			
			if (!mysql_select_db($db['name'], $this->db)) {
				throw new Exception(
					mysql_error($this->db), mysql_errno($this->db)
				);
			}
			
			mysql_query("SET NAMES utf8", $this->db);
		}
		return $this->db;
	}
}