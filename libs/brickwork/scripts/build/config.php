<?php
return array(
	'db_connection' => array(
		'host' => '192.168.2.24',
		'username' => 'christiaan',
		'password' => 'estraab',
		'name' => 'skeletondb_1_8',
		'port' => '3306',
	),
	'phoundry' => array(
		'file' => dirname(__FILE__).'/../../sql/table_interfaces.dat',
		'exclude' => array(
			'check_url_s',
			'shared_files',
			'phoundry_session',
			'phoundry_event',
			'phoundry_table',
			'phoundry_column',
			'phoundry_filter',
			'phoundry_filter_1',
			'phoundry_user',
			'phoundry_group',
			'phoundry_group_table',
			'phoundry_content_node',
			'phoundry_tag',
			'phoundry_tag_content_node',
		),
	),
	'structure' => array(
		'file' => dirname(__FILE__).'/../../sql/database_structure.sql',
		'exclude' => array(),
	),
	'database' => array(
		'file' => dirname(__FILE__).'/../../sql/skeleton.sql',
		'exclude' => array(),
	)
);

