<?php
/**
 * Phoundry Sync main features
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class PhoundrySync
{
	/**
	 * The used db connection
	 * @var resource
	 */
	protected $_db;
	
	/**
	 * The charset of the Phoundry Installation
	 * @var string
	 */
	protected $_charset;
	
	/**
	 * Sets up the sync instance requiring its dependencies
	 * 
	 * @param resource $db Db connection that will be used
	 * @param string $charset the charset of the project
	 */
	public function __construct($db, $charset = 'utf-8')
	{
		if (!is_resource($db)) {
			throw new InvalidArgumentException('DB should be a valid resource');
		}
		
		$this->_db = $db;
		$this->_charset = (string) $charset;
		if($charset === "utf-8") {
			$this->_query("SET NAMES utf8");
		}
		else {
			$this->_query("SET NAMES latin1");
		}
	}
	
	/**
	 * Export the given table to a PhoundrySync_Table
	 * @param string $string_id String id
	 * @return array|bool
	 */
	public function export($string_id)
	{
		$string_id = (string) $string_id;
		
		$table = array();
		$table['phoundry_table'] = mysql_fetch_assoc(
			$this->_query(
				sprintf(
					"SELECT
						id,
						string_id,
						order_by,
						name,
						description,
						max_records,
						record_order,
						info,
						is_plugin,
						show_in_menu,
						extra
					FROM
						phoundry_table
					WHERE
						string_id = %s
					",
					$this->_quoteValue($string_id)
				)
			)
		);
		
		if(!$table['phoundry_table']) {
			// Table not found
			return false;
		}
		$tid = (int) $table['phoundry_table']['id'];

		$res = $this->_fetchAll(
			$this->_query(
				sprintf(
					"SELECT
						string_id,
						table_id,
						order_by,
						name,
						description,
						datatype,
						datatype_extra,
						inputtype,
						inputtype_extra,
						required,
						searchable,
						info
					FROM
						phoundry_column
					WHERE
						table_id = %d
					",
					$tid
				)
			)
		);
		$table['phoundry_column'] = array();
		foreach($res as $row) {
			$table['phoundry_column'][$row['string_id']] = $row;
		}

		$res = $this->_fetchAll(
			$this->_query(
				sprintf(
					"SELECT
						string_id,
						table_id,
						event,
						code
					FROM
						phoundry_event
					WHERE
						table_id = %d
					",
					$tid
				)
			)
		);
		$table['phoundry_event'] = array();
		foreach($res as $row) {
			$table['phoundry_event'][$row['string_id']] = $row;
		}

		$res = $this->_fetchAll(
			$this->_query(
				sprintf(
					"SELECT
						string_id,
						table_id,
						type,
						required,
						group_id,
						name,
						query,
						matchfield,
						wherepart
					FROM
						phoundry_filter
					WHERE
						table_id = %d
					",
					$tid
				)
			)
		);
		$table['phoundry_filter'] = array();
		foreach($res as $row) {
			$table['phoundry_filter'][$row['string_id']] = $row;
		}
		
		// Only export the super user rights as the other groups can be out of sync
		$table['phoundry_group_table'] = mysql_fetch_assoc(
			$this->_query(
				sprintf(
					"SELECT
						group_id,
						table_id,
						rights
					FROM
						phoundry_group_table
					WHERE
						table_id = %d
					AND
						group_id = 1
					",
					$tid
				)
			)
		);
		
		if ($table['phoundry_group_table']) {
			$rights = str_split($table['phoundry_group_table']['rights']);
			sort($rights, SORT_STRING);
			$table['phoundry_group_table']['rights'] = implode('', $rights);
		}
		
		if($this->_charset !== 'utf-8') {
			$table = $this->_convertData($table, "utf8");
		}
		
		return $table;
	}
	
	/**
	 * Import the Table
	 * @param array $table
	 * @return int Newly created table_id
	 */
	public function import(array $table, $name = null, $string_id = null)
	{
		if($this->_charset !== 'utf-8') {
			$table = $this->_convertData($table, "latin1");
		}
		
		$table['phoundry_table']['id'] = '';
		if(null !== $name) {
			$table['phoundry_table']['string_id'] = $string_id;
			$table['phoundry_table']['name'] = $name;
		}
		$this->_insertInto(array($table['phoundry_table']), 'phoundry_table');
		$new_tid = (int) mysql_insert_id($this->_db);

		// Colums
		foreach($table['phoundry_column'] as $k => $v) {
			$table['phoundry_column'][$k]['id'] = '';
			$table['phoundry_column'][$k]['table_id'] = $new_tid;
		}
		if($table['phoundry_column']) {
			$this->_insertInto($table['phoundry_column'], 'phoundry_column');
		}
		
		// Events
		foreach($table['phoundry_event'] as $k => $v) {
			$table['phoundry_event'][$k]['id'] = '';
			$table['phoundry_event'][$k]['table_id'] = $new_tid;
		}
		if($table['phoundry_event']) {
			$this->_insertInto($table['phoundry_event'], 'phoundry_event');
		}

		// Filters
		foreach($table['phoundry_filter'] as $k => $v) {
			$table['phoundry_filter'][$k]['id'] = '';
			$table['phoundry_filter'][$k]['table_id'] = $new_tid;
		}
		if($table['phoundry_filter']) {
			$this->_insertInto($table['phoundry_filter'], 'phoundry_filter');
		}
		
		// Superuser rights
		$table['phoundry_group_table']['table_id'] = $new_tid;
		$this->_insertInto(array($table['phoundry_group_table']), 'phoundry_group_table');
		
		return $new_tid;
	}
	
	/**
	 * Create a set with the differences
	 *
	 * @param array $from Table A
	 * @param array $to Table B
	 * @return array Array containing the differences
	 */
	public function diff(array $from, array $to)
	{
		// Dont use any numeric id's
		$to = $this->_wipeTableId($to);
		$from = $this->_wipeTableId($from);
		
		return $this->_diffArr($to, $from);
	}
	
	/**
	 * Update a table
	 * 
	 * @param string $string_id
	 * @param array $to
	 * @return int Id of table that has been updated
	 */
	public function update($string_id, $to, $update_description = true)
	{
		if($this->_charset !== 'utf-8') {
			$to = $this->_convertData($to, "latin1");
		}
		
		$string_id = (string) $string_id;
		$res = $this->_query(
			sprintf(
				"SELECT
					id
				FROM
					phoundry_table
				WHERE
					string_id = %s
				LIMIT 1",
				$this->_quoteValue($string_id)
			)
		);
		
		$tid = false;
		if ($res) {
			if ($row = mysql_fetch_assoc($res)) {
				$tid = (int) $row['id'];
			}
		}
		
		if(!$tid) {
			return false;
		}
		
		$to = $this->_fillTableId($to, $tid);
		$desc_fields = array('description', 'info');
		
		if(!empty($to['phoundry_table'])) {
			$values = array();
			foreach($to['phoundry_table'] as $col => $val) {
				if(!$update_description && in_array($col, $desc_fields)) {
					continue;
				}
				$values[] = $col.' = '.$this->_quoteValue($val);
			}
			
			$this->_query(sprintf("
				UPDATE phoundry_table
				SET %s
				WHERE id = %d",
				implode(',', $values),
				$tid
			));
		}
		
		if(!empty($to['phoundry_group_table'])) {
			$this->_insertInto(
				array($to['phoundry_group_table']),
				'phoundry_group_table',
				$update_description ? true : $desc_fields
			);
		}
		
		foreach($to as $table_name => $records) {
			if(is_array(current($records))) {
				$this->_insertInto(
					$records,
					$table_name,
					$update_description ?
						true : $desc_fields
				);
			}
		}
		
		return $tid;
	}
	
	/**
	 * Export all table interfaces for the current database
	 * 
	 * @param array $exclude [optional] string_id's which will be excluded
	 * @return array
	 */
	public function exportAll(array $exclude = array())
	{
		$string_ids = array_diff($this->_listTables(), $exclude);
		$tables = array();
		foreach($string_ids as $string_id) {
			if($table = $this->export($string_id)) {
				$tables[$string_id] = $table;
			}
			else {
				throw new RuntimeException("Found string_id '".$string_id.
					"' in phoundry_table but could not export it");
			}
		}
		return $tables;
	}
	
	/**
	 * Compares 2 arrays with table interfaces and returns a array 
	 * containing the diffs
	 * 
	 * @param array $from
	 * @param array $to
	 * @return array
	 */
	public function diffAll(array $from, array $to)
	{
		$update = array();
		$import = array();
		
		foreach($to as $tid => $v) {
			if(isset($v['phoundry_table']['string_id'])) {
				$string_id = $v['phoundry_table']['string_id'];
				
				if(isset($from[$string_id])) {
					if($diff = $this->diff($v, $from[$string_id])) {
						$update[$string_id] = $v;
					}
				}
				else {
					$import[$string_id] = $v;
				}
			}
			else {
				throw new InvalidArgumentException("Malformed to array");
			}
		}
		
		return compact('update', 'import');
	}
	
	/**
	 * Convert data containing serialized string from or to utf8
	 * 
	 * @param mixeddata $data
	 * @param bool $utf8 encoding to utf8?
	 * @return mixeddata
	 */
	protected function _convertData($data, $charset = "utf8")
	{
		if (is_array($data)) {
			foreach($data as $key => $val) {
				$data[$this->_convertData($key, $charset)] =
					$this->_convertData($val, $charset);
			}
		}
		else if(is_string($data)) {
			$unser = $this->is_serialized($data) ? @unserialize($data) : $data;
			if(false !== $unser) {
				$data = serialize($this->_convertData($unser, $charset));
			}
			else {
				$data = $charset === "utf8" ?
					utf8_encode($data) : utf8_decode($data);
			}
		}
		return $data;
	}

	static public function is_serialized($data, $strict = true)
    {
        // if it isn't a string, it isn't serialized.
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace = strpos($data, '}');
            // Either ; or } must exist.
            if (false === $semicolon && false === $brace)
                return false;
            // But neither must be in the first X characters.
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            // or else fall through
            case 'a' :
            case 'O' :
                return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }
	
	/**
	 * Fetch all string_id's of the tables in the current database
	 * 
	 * @return array
	 */
	protected function _listTables()
	{
		$tables = array();
		
		$res = $this->_query(
			"SELECT string_id FROM phoundry_table WHERE string_id != ''"
		);
		
		while ($row = mysql_fetch_assoc($res)) {
			$tables[] = $row['string_id'];
		}
			
		return $tables;
	}
	
	/**
	 * Generates a diff between arrays
	 * 
	 * @param array $to
	 * @param array $from
	 * @return array
	 */
	protected function _diffArr(array $to, array $from)
	{
		$diff = array();
		foreach($to as $k => $val) {
			if(is_array($val)) {
				$f = isset($from[$k]) && is_array($from[$k]) ?
					$from[$k] : array();
					
				if($val = $this->_diffArr($val, $f)) {
					$diff[$k] = $val; 
				}
			}
			else if(!array_key_exists($k, $from) || $val != $from[$k]) {
				$diff[$k] = $val;
			}
		}
		return $diff;
	}
	
	/**
	 * Wipes a table clean from its table_id
	 * 
	 * @param array $t
	 * @return array
	 */
	protected function _wipeTableId(array $t)
	{
		$t['phoundry_table']['id'] = '';
		$t['phoundry_group_table']['table_id'] = '';
		
		foreach(array_keys($t['phoundry_column']) as $k) {
			$t['phoundry_column'][$k]['table_id'] = '';
		}
		foreach(array_keys($t['phoundry_event']) as $k) {
			$t['phoundry_event'][$k]['table_id'] = '';
		}
		foreach(array_keys($t['phoundry_filter']) as $k) {
			$t['phoundry_filter'][$k]['table_id'] = '';
		}
		return $t;
	}
	
	/**
	 * Fills a table
	 * 
	 * @param array $t
	 * @param int $tid
	 * @return array
	 */
	protected function _fillTableId(array $t, $tid)
	{
		$t['phoundry_table']['id'] = $tid;
		$t['phoundry_group_table']['table_id'] = $tid;
		
		foreach(array_keys($t['phoundry_column']) as $k) {
			$t['phoundry_column'][$k]['table_id'] = $tid;
		}
		foreach(array_keys($t['phoundry_event']) as $k) {
			$t['phoundry_event'][$k]['table_id'] = $tid;
		}
		foreach(array_keys($t['phoundry_filter']) as $k) {
			$t['phoundry_filter'][$k]['table_id'] = $tid;
		}
		return $t;
	}
	
	/**
	 * Execute a SQL statement
	 * 
	 * @throws Exception If mysql returns a error
	 * @param string $sql
	 * @return resource|bool
	 */
	protected function _query($sql)
	{
		$res = mysql_query($sql, $this->_db);
		if (false === $res) {
			throw new Exception(
				mysql_error($this->_db), mysql_errno($this->_db)
			);
		}
		return $res;
	}
	
	/**
	 * Quote the database values
	 *
	 * @param scalar $value
	 * @return string
	 */
	protected function _quoteValue($value)
	{
		if(is_null($value)) {
			return 'NULL';
		}
		
		if(is_numeric($value)) {
			return $value;
		}

		return '"'.mysql_real_escape_string($value, $this->_db).'"';
	}
	
	/**
	 * Fetches all rows and put them in an array
	 * 
	 * @param resource $res
	 * @return array
	 */
	protected function _fetchAll($res)
	{
		$all = array();
		while ($row = mysql_fetch_assoc($res)) {
			$all[] = $row;
		}
		return $all;
	}
	
	/**
	 * Inserts the given rows
	 *
	 * @param array $rows Rows
	 * @param string $table
	 * @param array|bool $update_dupl if array; fields that wont get updated
	 * @return string
	 */
	protected function _insertInto(array $rows, $table, $update_dupl = null)
	{
		if(strpos($table, '`') !== false) {
			throw new InvalidArgumentException("Tablename contains a backtick");
		}
		
		if(!current($rows)) {
			
		}
		
		$fields = array_keys(current($rows));
		if(!$fields) {
			throw new InvalidArgumentException("Rows does not contain column names");
		}
		
		if(strpos(implode($fields), '`') !== false) {
			throw new InvalidArgumentException("One of the column names ".
				"contains a backtick");
		}
		
		if($update_dupl) {
			$on_dupl = array();
			foreach($fields as $field) {
				if(!is_array($update_dupl) || !in_array($field, $update_dupl)) {
					$on_dupl[] = $field." = VALUES(".$field.")";
				}
			}
		}
		
		$values = array();
		foreach($rows as $row) {
			$values[] = '('.implode(',', array_map(array($this, '_quoteValue'), $row)).')';
		}
		
		return $this->_query(sprintf("
			INSERT INTO
				%s (%s)
			VALUES
				%s
			%s",
			$table,
			'`'.implode('`, `', $fields).'`',
			implode(', ', $values),
			$update_dupl ? 'ON DUPLICATE KEY UPDATE '.implode(",\n", $on_dupl) : ''
		));
	}
}
