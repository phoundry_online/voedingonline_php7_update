<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("toUriValue is deprecated in favor of Utility::toUri", E_USER_ERROR);
}

/**
 * Converteert een string naar een leesbare string in een uri
 * Maakt gebruik van custom transcribe() functie.
 *
 * v1.1 - jurriaan 29.11.2006
 */
function toUriValue($string)
{
	$string = trim(strip_tags($string)); # HTML strippen
	$string = strtolower($string); # Lowercase
	$string = transcribe($string); # Umlauts e.d. verwijderen
	$string = preg_replace('/[^a-z\d&\(\)\!\-_ ]+/', '', $string); # Ongeldige tekens strippen. Tekens die we met - willen vervangen laten staan
	$string = str_replace(array(' ', '_', '&'), '-', $string); # 'Vreemde' tekens met - vervangen
	$string = preg_replace('/(-+)+/', '-', $string); # Dubbele streepjes verwijderen
	return $string;
}

/**
 * Vervangt alle letters met accenten en umlauts, ligaturen en runen bekend in iso-8859-1 met gewone letters.
 */
function transcribe($string) {
    $string = strtr($string,"\xA1\xAA\xBA\xBF\xC0\xC1\xC2\xC3\xC5\xC7\xC8\xC9\xCA\xCB\xCC\xCD\xCE\xCF\xD0\xD1\xD2\xD3\xD4\xD5\xD8\xD9\xDA\xDB\xDD\xE0\xE1\xE2\xE3\xE5\xE7\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF0\xF1\xF2\xF3\xF4\xF5\xF8\xF9\xFA\xFB\xFD\xFF", "!ao?AAAAACEEEEIIIIDNOOOOOUUUYaaaaaceeeeiiiidnooooouuuyy");
    $string = strtr($string, array("\xC4"=>"Ae", "\xC6"=>"AE", "\xD6"=>"Oe", "\xDC"=>"Ue", "\xDE"=>"TH", "\xDF"=>"ss", "\xE4"=>"ae", "\xE6"=>"ae", "\xF6"=>"oe", "\xFC"=>"ue", "\xFE"=>"th"));
    return($string);
}

?>