<?php

function _tr( $text, $lang = null, $file = null, $line = 0 )
{
	if ( is_null($lang) )
	{
		if ( isset($GLOBALS['translations']) )
			$l = $GLOBALS['translations'];
		else if ( isset(Framework::$site) && count(Framework::$site->availableLang) )
		{
			if ( is_object(Framework::$site->currentLang) )
				$lang = Framework::$site->currentLang;
			else
				$lang = current( Framework::$site->availableLang );
			$l= Translation::factory( $lang->code );
		}
		else
			return $text;
	}
	else
	{
		$l = new Translation( $lang );
	}
	
	if ( !is_null($file) )
		return $l->tr($text, $file, $line);
	return $l[$text];
}

