<?php
/*
* VERSION 1.2
* @last_modified 2/3/2008
*/

/**
* Return current Unix timestamp with microseconds as a float
*
* @author PHP.net
* @authot Jørgen Teunis <jorgen.teunis@webpower.nl>
* @version 1.0.1
* @deprecated
* @return float
*/
if (!function_exists('microtime_float')) {
	function microtime_float() {
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of microtime_float() is deprecated, ".
				"use microtime(true) instead", E_USER_ERROR);
		}
		return microtime(true);
	}
}

/**
* escSquote
*
* Escaped single quotes from a string to make it javascript compatible
*
* @author Arjan Haverkamp
* @param string $str input string
* @return string
*/
if (!function_exists('escSquote')) {
	function escSquote($str) {
		return str_replace("'", "\\'", $str);
	}
}

// show memory uses and includes only in TESTMODE!
if(TESTMODE){
	if (!function_exists('show_me_the_damage')) {
		function show_me_the_damage(){
			if(defined('HIDE_SHOW_ME_THE_DAMAGE')){
				return true;
			} else {
				global $page;

		        print '<div style="position: absolute; top: 0; right: 0; background: #fff; z-index: 1000; width: 150px">';
		        print '<a href="#" onclick="this.parentNode.parentNode.removeChild(this.parentNode);">close</a><br />';
		        print 'Mem usage: '.round((memory_get_usage()/1024)/1024,2).'MB<br />';
		        print 'Used caching: '.((defined('USE_TEMPLATE_CACHE') && USE_TEMPLATE_CACHE) ? 'yes ' : 'no') . '<br />';
		        // execution time
				if(defined('PHP_TIMESTAMP_FLOAT')){
					$endtime = microtime(true);
			        $execution_time = $endtime-PHP_TIMESTAMP_FLOAT;
			        print 'User time: '.round($execution_time, 3).' sec<br />';
				}
		        print 'Queries: '.DB::getQueryCount().'<br />';
		        print '- query time: '.round(DB::getTotalQueryTime(),3).'<br />';
		        print '<a href="#" onclick="document.getElementById(\'__debug_includes\').style.display=\'\';return false">Includes</a> '.count(get_included_files()).'<br />';
		        print '<ul id="__debug_includes" style="display: none; background: #fff; height: 300px; overflow:auto"><li>'.implode('</li><li>',get_included_files()).'</li></ul>';
		        print '</div>';
			}
   		}
	}
}

// usage for show me the damage and other time critical stuff
// important to set at the beginning of the script!
if(!defined('PHP_TIMESTAMP_FLOAT')){
	define('PHP_TIMESTAMP_FLOAT', microtime(true));
}

/**
* Abbreviate a chunk of text.
*
* @author Arjan Haverkamp
*
* @param $text string The text to abbreviate.
* @param $length int The nr. of characters to abbreviate to.
* @return string containing abbreviated text.
*/
if (!function_exists('abbreviate')) {
	function abbreviate($text, $length = 300) {
		$text = strip_tags($text);
		$text = str_replace(array("\r", "\n"), array('', ' '), $text);
		$intro = strlen($text) <= $length ? $text . ' ' : substr($text, 0, $length);
		if (preg_match("/(.*?)\s+[^\s]*$/", $intro, $regs))
			$intro = $regs[1];
		$intro .= (strlen($text) <= $length ? '' : '...');

		return $intro;
	}
}

/**
* functie urlencodeURL
*
* urlencodeURL encodeert een URL
*
* @author Arjan Haverkamp
* @copyright Web Power b.v.
* @param string $uri de url
* @deprecated
* @return string de ge-encodeerde url
*/
if (!function_exists('urlencodeURL')) {
	function urlencodeURL($uri) {
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of urlencodeURL is deprecated, ".
				"Utility::urlencodeUrl() instead", E_USER_ERROR);
		}

		return implode('/', array_map('rawurlencode', explode('/', $uri)));
	}
}

/**
* Put a debug message in the logfile.
*
* @author Niek Kerkemeijer
* @param String $message The message to log.
* @param String $logdir The directory for the logfiles.
*/
if (!function_exists('logDebug')) {
	function logDebug($message, $logdir = '' ) {
		global $PHprefs,$WEBprefs;

		$logdir = empty($logdir) ? $PHprefs['tmpDir'] : $logdir;

		// default == TESTMODE
		$doDebug = TESTMODE;

		if (isset($WEBprefs['debug']) || isset($WEBprefs['Debug']) ) {
			$doDebug = false;
			foreach ($WEBprefs['debugIPs'] as $ip) {
				if (substr($_SERVER['REMOTE_ADDR'], 0, strlen($ip)) == $ip) {
					$doDebug = true;
					break;
				}
			}
		}
		// still need debugging?
		if ($doDebug) {
			$filename = realpath($logdir) . "/{$_SERVER['REMOTE_ADDR']}_debug.log";

			error_log(date('Y-m-d H:i:s', mktime()) . ": " . $message . "\n",3,$filename );
			/*
			 * Kansloos, doe dat toch met error_log, lekker atomic!
			 *
			// In ons voorbeeld openen we $filename in 'toevoeg' modus.
			// De bestands pointer is aan het einde van het bestand
			// en daar gaat $message naar toe als we het fwrite()'en.
			$handle = fopen($filename, 'a')
				or trigger_error("Could not open file $filename", E_USER_ERROR);

			// Schrijf $message naar ons bestand dat we geopend hebben.
			fwrite($handle, date('Y-m-d H:i:s', mktime()) . ": " . $message . "\n")
				or trigger_error("Could not write content to debug file $filename", E_USER_ERROR);

			fclose($handle);
			*/
		}
	}
}

/**
* First
* return first element of array
*
* @author Mick van der Most van Spijk
*/
if (!function_exists('first')) {
	function first($arr)
	{
		if(is_array($arr)) {
			return reset($arr);
		}

		if($arr instanceof ResultSet) {
			return $arr->first();
		}

		if($arr instanceof Traversable) {
			foreach($arr as $v) { return $v; }
			return false;
		}

		if($arr instanceof ArrayAccess) {
			return isset($arr[0]) ? $arr[0] : null;
		}
	}
}

/**
* getlocale
*
* Return current locale settings for all or specified category
*
* @author Mick van der Most van Spijk
* @last_modifed 7/10/2006
* @package None
*/
if (!function_exists('get_locale')) {
	function getlocale($category = LC_ALL) {
		return setlocale($category, 0);
	}
}

/**
 * Trigger redirect
 *
 * Redirect to url in a correct way and stop script execution directly after redirect headers.
 * @author Jørgen Teunis <jorgen.teunis@webpower.nl>
 * @param	String url
 * @param Integer Status: 301=Moved Permanently, 302=Found, 307 = Temporary redirect
 */
if (!function_exists('trigger_redirect')) {
	function trigger_redirect($url, $status = 302) {
        $status = (int) $status;
		switch($status) {
			case 301: // echt verplaatst
				$msg = 'Moved Permanently';
			break;
			case 302: // eerste url MOET in de toekomst nog steeds gebruikt worden
				$msg = 'Found';
			break;
			case 303: // see other, niet ondersteunt voor 1.1
				$msg = 'See other';
			break;
			case 307:
				$msg = 'Temporary Redirect';
			break;
			default:
				throw new InvalidArgumentException('Status should be either '.
					'301, 302, 303 or 307. '.$status.' given.');
		}
		header('HTTP/1.1 '.$status.' '.$msg, true, $status);
		header('URI: '.$url);
		header('Location: '.$url);
		print $msg.":\n".$url."\n";
		exit;
	}
}

/**
* Maak van een array een komma gescheiden string
*
* @author Mick van der Most van Spijk
* @param array
* @return string
* @access public
* @version 1.0.1
*/
if (!function_exists('commalize')) {
	function commalize($array, $quote=false) {
		if (!is_array($array)) { // @since 1.0.1
			trigger_error('First parameter must be of type array', E_USER_NOTICE);
			return $array;
		}

		$string = '';

		$teller = 0;
		$len = count($array);
		foreach ($array as $key => $value) {
			if (!$quote) {
				$string .= $value;
			} else {
				$string .= "'".$value."'";
			}
			if ($teller < ($len - 1))
				$string .= ", ";

			$teller++;
		}
		return $string;
	}
}

/**
* Concat
* glue a comma seperated list of string together
*
* @author Mick van der Most van Spijk
* @param String any number of strings to glue together
* @deprecated
* @return String
*/
if (!function_exists('concat')) {
	function concat() {
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of concat is deprecated. ".
				"Why would you want to use this any way!", E_USER_ERROR);
		}
		$args = func_get_args();
		return implode('', $args);
	}
}

/**
* Coalesce
*
* Returns the first non-NULL, non empty value in the list, or NULL if there
* are no non-NULL values.
*
* 11/16/2006 mick works on arrays as well
*
* @author Mick van der Most van Spijk
* @param Mixed any number of args
* @return Mixed first non-NULL value
*/
if (!function_exists('coalesce')) {
	function coalesce() {
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of coalesce() is deprecated, ".
				"use Utility::coalesce() instead", E_USER_ERROR);
		}

		$args = func_get_args();

		foreach($args as $arg) {
			if (is_array($arg) && $arg) {
				return $arg;
			}

			if(is_object($arg)) {
					trigger_error('Objects are not allowed in coalesce', E_USER_ERROR);
			}

			if(!empty($arg)) {
				return $arg;
			}
		}

		return NULL;
	}
}

/**
* sorteer een array op object keys
*
* @author Mick van der Most van Spijk
* @version 1.1.0
* @param array
* @param string
* @return array
*/
if (!function_exists('oksort')) {
	function oksort(&$array, $key) {
		foreach ($array as $element) {
			$sortarr[] = $element->$key;
		}

		if (empty($sortarr))
			return $array;

		array_multisort($sortarr, SORT_ASC, SORT_REGULAR, $array);

		return $array;
	}
}

/**
* Alert
*
* vardump a variable
*
* @author Jørgen Teunis
* @param mixed
*/
if (!function_exists('alert')) {
	function alert($mixed){
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of alert() is deprecated, ".
				"use var_dump() instead", E_USER_ERROR);
		}
		var_dump($mixed);
	}
}


/**
* Formatteer str zodat deze kan worden gebruikt in dmfacts
* @deprecated
*/
if (!function_exists('DMFactsFormat')) {
	function DMFactsFormat ($str) {
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of ".__FUNCTION__."() is deprecated. ".
				"This shouldnt be a global function.", E_USER_ERROR);
		}

		// replace . and newlines
		$str = str_replace(array(".", "\r\n", "\n", "\r"), '', $str);
		$str = trim($str);
		$str = urlencode($str);

		return $str;
	}
}

/**
* Functie om een statische functie aan te roepen
*
* @author Mick van der Most van Spijk <mick@webpower.nl>
* @version 1.0.0
* @deprecated
*
* @param classname
* @param functionname
* @params args
*/
if (!function_exists('call_static_method')) {
	function call_static_method() {
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of ".__FUNCTION__."() is deprecated. ".
				"Use call_user_func() instead.", E_USER_ERROR);
		}

		$args = func_get_args();
		$classname = array_shift($args);
		$method = array_shift($args);

		if (!$classname || !$method || !class_exists($classname) ||
			!method_exists($classname, $method)) {
			return false;
		}

		return call_user_func_array(array($classname, $method), $args);
	}
}

/**
 * get static html from table to display in the template
 * @param array $params
 * @param Smarty $parent_smarty
 * @deprecated
 * @return string HTML
 */
if (!function_exists('insert_staticHTML')) {
	function insert_staticHTML($params, $parent_smarty){
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of ".__FUNCTION__."() is deprecated. ".
				"Use snippets instead.", E_USER_ERROR);
		}

		if(empty($params['code'])){
			trigger_error('staticHTML functions expects param \'code\'', E_USER_ERROR);
		}

		$body = '';
		$code = $params['code'];

		$sql = sprintf('
			SELECT
				body
			FROM
				brickwork_static_html
			WHERE
				site_identifier = "%s"
			AND
				("%s" BETWEEN online_date AND offline_date)
			LIMIT 1
		',
			DB::quote(Framework::$site->identifier),
			date('Y-m-d H:i:s', PHP_TIMESTAMP)
		);

		$res = DB::iQuery($sql);

		if (isError($res)) {
			trigger_error('query error in staticHTML function', E_USER_ERROR);
		}

		if (isset($res->result[0])){
			$body = $res->result[0]->body;
		}

		return $body;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
