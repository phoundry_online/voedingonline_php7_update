/**
 * ClipEvents engine
 * 
 * @author Christiaan Baartse
 * @copyright 2010 Web Power, http://www.webpower.nl
 */
(function($){
	$.ClipEvents = {
		'data' : {},
		'Running' : {},
		'LoadVideo' : function loadVideo(player_id, video_id) {
			$.ClipEvents.LoadedVideoId[player_id] = video_id;
			var player = $.ClipEvents.JWPlayer.getPlayer(player_id);
			
			player.sendEvent("LOAD", {
				'file' : $.ClipEvents.data.ClipVideos[video_id].file
			});
		},
		'LoadedVideoId' : {}
	};
	
	$.ClipEvents.JWPlayer = {
		getPlayer : function getPlayer(gid) {
			return swfobject.getObjectById(gid);
		},
		Listeners : {
			'TIME'		: function(obj) {
				var clip_video = $.ClipEvents.Running[obj.id];
				clip_video.position = obj.position;
				clip_video.duration = obj.duration;
				clip_video.notify('TIME');				
			},
			'STOP'		: function(obj) {
				var clip_video = $.ClipEvents.Running[obj.id];
				clip_video.position = 0;
				clip_video.duration = 0;
				clip_video.notify('STOP');	
			},
			'LOADED'	: function(obj) {
				var clip_video = $.ClipEvents.Running[obj.id];
				clip_video.loadedSize = obj.loaded;
				clip_video.totalSize = obj.total;
				clip_video.notify('LOADED');
			},
			'STATE'		: function(obj) {
				var clip_video = $.ClipEvents.Running[obj.id];
				clip_video.oldState = obj.oldstate;
				clip_video.newState = obj.newstate;
				clip_video.notify('STATE');
			},
			'ITEM'		: function(obj) {
				setTimeout(function(){
					var player = $.ClipEvents.JWPlayer.getPlayer(obj.id);
					$.ClipEvents.Running[obj.id] = new ClipVideo(
						$.ClipEvents.LoadedVideoId[obj.id],
						player.getPlaylist()[obj.index],
						player
					);
					
					$.ClipEvents.Running[obj.id].notify('ITEM');
				}, 0);
			},
			'PLAYER_READY' : function(obj) {
				var player = $.ClipEvents.JWPlayer.getPlayer(obj.id);
				
				// Omdat dit string literals zijn jQuery gebruiken ipv $
				player.addModelListener('TIME', 'jQuery.ClipEvents.JWPlayer.Listeners.TIME'); 
				player.addModelListener('LOADED', 'jQuery.ClipEvents.JWPlayer.Listeners.LOADED');
				player.addModelListener('STATE', 'jQuery.ClipEvents.JWPlayer.Listeners.STATE');
				player.addControllerListener('ITEM', 'jQuery.ClipEvents.JWPlayer.Listeners.ITEM');
				
				$.ClipEvents.LoadVideo(obj.id, $.ClipEvents.LoadedVideoId[obj.id]);
			}
		}
	};
	
	window.playerReady = $.ClipEvents.JWPlayer.Listeners.PLAYER_READY;

	function ClipVideo(video_id, video, player) {
		this.loadedSize = 0;
		this.totalSize = 0;
		this.oldState = 'IDLE';
		this.newState = null;
		this.flashPlayer = player;
		this.current_video = video;
		this.position = 0;
		this.duration = 0;
		this.video = $.ClipEvents.data.ClipVideos[video_id];
		this.activegroup = [];
	};
	
	/**
	 * Notify the ClipEvent_Actions for the current event
	 * 
	 * @param string {type} They type of the event (passed to the events update function)
	 * @return int The amount of events that have been informed
	 */
	ClipVideo.prototype.notify = function notify(type) {
		var video = this, group = parseInt(this.position/10, 10);
		
		if(video.video.groups[group]) {
			group = video.video.groups[group];

			// Set the current items as active
			$.each(group, function(i, action_index){
				video.activegroup[action_index] = true;
			});
			
			$.each(video.activegroup, function(action_index, status){
				var action = video.video.actions[action_index];
				if(!$.isFunction(action.code)) {
					action.code = new Function('video', 'type', action.code);
				}
				action.code(video, type);
				if($.inArray(action_index, group) == -1) {
					delete video.activegroup[action_index];
				}
			});
			
		}
		
		/*
		$.each(this.video.actions, function(){
			if(!$.isFunction(this.code)) {
				this.code = new Function('video', 'type', this.code);
			}
			this.code(video, type);
		});
		*/
	};
	
})(jQuery);