jQuery(function($) {
	var module = $('.PhotogalleryModule .coolslideThumbs');
    module.find('.slideshow_pics').cycle({
	fx:     'scrollHorz',
	speed:  'slow',
	timeout: 0,
	after:	function(curr, next, opts) {
		var msg = 'Slide ' + (opts.currSlide + 1) + ' of ' + opts.slideCount;
		module.find('.caption').html(msg);
	},
	pager:  module.find('.slideshow_nav'),
	pagerAnchorBuilder: function(idx, slide) {
	    return module.find('.slideshow_nav li:eq(' + (idx) + ') a');
	},
	next:	module.find('.navnext'),
	prev:	module.find('.navprev'),
	nowrap:	1
    });
});