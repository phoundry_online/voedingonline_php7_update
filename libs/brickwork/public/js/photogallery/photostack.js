jQuery(function($) {
	var module = $('.PhotogalleryModule .photostack');
    module.find('.slideshow_pics').cycle({
	fx:     'shuffle',
	easing: 'jswing',
	delay:  1000,
	timeout: 0,
	next:	module.find('.navnext'),
	prev:	module.find('.navprev'),
	shuffle: { top:-35, left: 510 }
    });
});