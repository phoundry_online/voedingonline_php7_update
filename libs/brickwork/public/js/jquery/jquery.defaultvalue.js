;(function($) {
	$.fn.defaultvalue = function() {
		// Keep our arguments as we'll dive into some more functions
		var args = arguments;
		
		return 	this.each(function(i) {				
			var def = args[i],
			el = $(this);
			
			if(el.is('[type="password"]')) {
				el.val('');
				var new_password = $($('<div>').html(el.clone()).html().replace(/type=(["']|)password(["']|)/ig, 'type="text"')).hide().removeAttr('name').removeAttr('id');
				el.after(new_password);
				new_password.val(def).focus(function(){
					new_password.hide();
					el.show().focus();												
				});
				el.blur(function(){
					if(el.val() == "") {
						el.hide();							
						new_password.show();
					}
				});
				if(el.val() == '') {
					el.hide();
					new_password.show();
				}
			} else {
				if(el.val() == '') el.val(def);
				el.focus(function() {
					if(el.val() == def) {
						el.val("");
					}
				});
				el.blur(function() {
					if(el.val() == "") {
						el.val(def);
					}
				});
			}
		});
	}
})(jQuery);