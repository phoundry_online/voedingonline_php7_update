/**
 * @author Christiaan
 */
jQuery(function($){
	$.fn.nyroModal.settings.endShowContent = function(elts, settings){
		var iframe = elts.content.find('iframe');
		if (iframe.length && iframe.attr('src').indexOf(window.location.hostname) !== -1) {
			var body= iframe.contents().find('body');
			if (body.height() > 0) {
				$.nyroModalSettings({
					height: body.height(),
					width: body.width()
				});
			}
			
			iframe.bind('load', function(){
				var body= iframe.contents().find('body');
				if (body.length) {
					$.nyroModalSettings({
						height: body.height(),
						width: body.width()
					});
				}
			});
		}
	};
});
