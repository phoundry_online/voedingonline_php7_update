/**
 * Looks for nodes with class swfobject and replaces those with flash embed
 * 
 * @author Christiaan Baartse
 * @copyright 2010 Web Power, http://www.webpower.nl
 * @uses swfobject
 */
(function($){
	if(!swfobject) {
		throw new Error("jquery.swfobject.js needs swfobject.js");
	}
	
	function embedSwf(node, data) {
		if(!node.attr('id')) {
			throw new Error("id is empty");
		}
		if(!data.swfUrl) {
			throw new Error("swfUrl is missing");
		}
		node.addClass($.fn.swfobject.defaults.enabled_class);
		
		var attributes = $.extend({
			id: node.attr('id'),
			name: node.attr('name'),
			'class' : node.attr('class')
		}, data.attributes);
		var params = $.extend({}, $.fn.swfobject.defaults.params, data.params);
		
		swfobject.embedSWF(
			data.swfUrl,
			node.attr('id'),
			(data.width ? data.width : node.width()),
			(data.height ? data.height : node.height()),
			data.version ? data.version : $.fn.swfobject.defaults.version,
			data.expressInstallSwfurl ? data.expressInstallSwfurl : null,
			data.flashVars ? data.flashVars : {},
			params,
			attributes,
			data.callbackFn ? data.callbackFn : null
		);
	};
	
	$.fn.swfobject = function(data) {
		this.each(function(i){
			var id,
			$this = $(this),
			inline_data = $this.text();
			inline_data = inline_data ? eval("("+inline_data+")") : {};
			
			if($this.attr('id')) {
				id = $this.attr('id');
			}
			else {
				id = "swfobject_gen_"+i;
				$this.attr('id', id);
			}
			embedSwf($this, $.extend({}, data, inline_data));
		});
	};
	
	$.fn.swfobject.defaults = {
		params : {
			allowfullscreen : 'true',
			allowscriptaccess : 'always',
			wmode : 'transparent'
		},
		version : "8.0.0",
		enabled_class : "swfobject_enabled"
	};
})(jQuery);