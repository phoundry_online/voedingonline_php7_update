/**
 * Date picker for Jeditable
 *
 * @author Christiaan Baartse
 * @copyright 2010 Web Power, http://www.webpower.nl
 */
 
$.editable.addInputType('date', {
    /* Create input element. */
    element : function(settings, original) {
        
        var dateinput = $('<input class="date_">');
        
        if($.datepicker) {
	        if(settings.date) {
				$(dateinput).datepicker(settings.date.datepicker);
			} else {
				$(dateinput).datepicker({
					dateFormat: 'yy-mm-dd',
					minDate: null
				});
			}
			if(settings.date.readonly && settings.date.readonly == true) {
				$(dateinput).attr('readonly', 'readonly');
			}
		}
        
        if($(this).is('form')) $(this).append(dateinput);
        
        return(dateinput);
    },
    /* Set content / value of previously created input element. */
    content : function(string, settings, original) {
        $('.date_', this).val(string);
    }
});
