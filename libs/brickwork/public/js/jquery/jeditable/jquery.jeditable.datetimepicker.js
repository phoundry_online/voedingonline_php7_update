/**
 * Date Time picker for Jeditable
 *
 * @author Christiaan Baartse
 * @copyright 2010 Web Power, http://www.webpower.nl
 */
 
$.editable.addInputType('datetime', {
    /* Create input element. */
    element : function(settings, original) {
        
        var dateinput = $.editable.types.date.element(settings, original);

        /* Create and pulldowns for hours and minutes. Append them to */
        /* form which is accessible as variable this.                 */                
        var hourselect = $('<select class="hour_">');
        var minselect  = $('<select class="min_">');
        var secselect  = $('<select class="sec_">');
        
        
        for (var hour=1; hour <= 24; hour++) {
            if (hour < 10) {
                hour = '0' + hour;
            }
            var option = $('<option>').val(hour).append(hour);
            hourselect.append(option);
        }

        for (var min=0; min <= 59; min++) {
            if (min < 10) {
                min = '0' + min;
            }
            var option = $('<option>').val(min).append(min);
            minselect.append(option);
        }
        
       	for (var min=0; min <= 59; min++) {
            if (min < 10) {
                min = '0' + min;
            }
            var option = $('<option>').val(min).append(min);
            secselect.append(option);
        }
        
        /* Last create an hidden input. This is returned to plugin. It will */
        /* later hold the actual value which will be submitted to server.   */
        var hidden = $('<input class="return" type="hidden">');
        
        $(this).append(dateinput);
        $(this).append('&nbsp;');
        $(this).append(hourselect);
        $(this).append(':');
        $(this).append(minselect);
        $(this).append(':');
        $(this).append(secselect);
        $(this).append(hidden);
        
        return(hidden);
    },
    /* Set content / value of previously created input element. */
    content : function(string, settings, original) {
        
        /* Select correct hour and minute in pulldowns. */
        var date = string.substr(0,10);
        var hour = parseInt(string.substr(11,2), 10);
        var min  = parseInt(string.substr(14,2), 10);
        var sec  = parseInt(string.substr(17,2), 10);
        $('.date_', this).val(date);
        
        $('.hour_', this).children().each(function() {
            if (hour == $(this).val()) {
                $(this).attr('selected', 'selected');
            }
        });
        $('.min_', this).children().each(function() {
            if (min == $(this).val()) {
                $(this).attr('selected', 'selected')
            }
        });
        $('.sec_', this).children().each(function() {
            if (sec == $(this).val()) {
                $(this).attr('selected', 'selected')
            }
        });

    },
    /* Call before submit hook. */
    submit: function (settings, original) {
        var date 	= $('.date_').val();
        var hour	= $('.hour_').val();
        var min		= $('.min_').val();
        var sec		= $('.sec_').val();
        
        
        var value = date+" "+hour+':'+min+':'+sec;
        $('input.return', this).val(value);
    }
});
