jQuery(function($) {
	$.fn.nyroModal.settings.processHandler = function(settings) {
	    var from = settings.from;
	    if (from && from.href && from.href.indexOf('http://www.youtube.com/watch?v=') != -1) {
	      $.nyroModalSettings({
	        type: 'swf',
	        height: 355,
	        width: 425,
	        url: from.href.replace(new RegExp("watch\\?v=", "i"), 'v/')
	      });
	    }
	};
});