/**
 * Normalize range selections on text areas
 * 
 * @author Christiaan Baartse
 * @copyright 2010 Web Power, http://www.webpower.nl
 */
;(function($){
	
	function textareaSelection(text, position) {
		this.text = text;
		this.position = position;
		this.length = text.length;
	};
	
	textareaSelection.prototype.toString = function toString() {
		return this.text;
	}
	
	$.selection = {
		'get' : function getSelection(field) {
			var selection, position;
			field = $(field);
			field.focus();
			
			if (document.selection) {
				selection = document.selection.createRange().text;
				if ($.browser.msie) { // ie
					var range = document.selection.createRange(), rangeCopy = range.duplicate();
					rangeCopy.moveToElementText(field.get(0));
					position = -1;
					while(rangeCopy.inRange(range)) { // fix most of the ie bugs with linefeeds...
						rangeCopy.moveStart('character');
						position ++;
					}
				} else { // opera
					position = field.get(0).selectionStart;
				}
			} else { // gecko
				position = field.get(0).selectionStart;
				selection = field.val().substring(position, field.get(0).selectionEnd);
			}
			
			return new textareaSelection(selection, position);
		},
		'set'	: function setSelection(field, start, len, lengthfix)	{
			field = $(field);
			if (field.get(0).createTextRange){
				// quick fix to make it work on Opera 9.5
				if ($.browser.opera && $.browser.version >= 9.5 && len == 0) {
					return false;
				}
				range = field.get(0).createTextRange();
				range.collapse(true);
				range.moveStart('character', start); 
				range.moveEnd('character', len); 
				range.select();
				if (!lengthfix) { // IE && Opera? fix
					var selection = $.selection.get(field); 
					if(selection.length > len) {
						$.selection.set(field, start, (len - (selection.length - len)), true);
					}
				}
			} else if (field.get(0).setSelectionRange ){
				field.get(0).setSelectionRange(start, start + len);
			}
			return true;
		},
		'replace'	: function replaceSelection(field, value) {
			var field = $(field),
			selection = $.selection.get(field),
			scrollPosition = field.get(0).scrollTop;
			
			if (document.selection) {
				var newSelection = document.selection.createRange();
				newSelection.text = value;
			} else {
				field.val(field.val().substring(0, selection.position)	+ value + field.val().substring(selection.position + selection.length, field.val().length));
			}
			
			field.get(0).scrollTop = scrollPosition;
			field.focus();
			$.selection.set(field, selection.position, value.length);
			
			return true;
		}
	};
	
	$.fn.selection = function(value, len) {
		if(undefined !== value) {
			if(undefined !== len) {
				$.selection.set(this.get(0), value, len);
			} else {
				$.selection.replace(this.get(0), value)
			}
			return this;
		}
		return $.selection.get(this.get(0));
	};
	
})(jQuery);
