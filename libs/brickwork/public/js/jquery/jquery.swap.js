/*
	uses jquery.ancestry.js
*/
;(function($){
	$.fn.swap = function(b) {
		$.swap(this, b);
		return this;
	};
	
	$.swap = function(a, b) {
		a = $($(a).get(0));
		b = $($(b).get(0));
		if(a && b && a.length && b.length) {// && !a.descendantOf(b.get(0)) && !b.descendantOf(a.get(0))) {
			var t = $('<div>').insertBefore(a);
			a.insertBefore(b);
			b.insertBefore(t);
			t.remove();
			return true;		
		}
		return false;
	};
})(jQuery);