/**
 * Phoundry Me
 * 
 * @version 2.0
 * @author Christiaan Baartse
 * @copyright 2010 Web Power, http://www.webpower.nl 
 */
if(window.jQuery) {
	jQuery(function($){
		$('html').bind('dblclick', function(event){
			if($.browser.msie && window.event.ctrlKey || event.ctrlKey){
				var clicked = $(event.target), url = false;
				if(!clicked.is('[class*="phoundry_"]')) {
					clicked = clicked.parents('[class*="phoundry_"]:first');
				}
				if(clicked.length) {
					var classes = clicked.attr('class').split(' ');
					$.each(classes, function(){
						if(this.match(/^phoundry_/i)) {
							var splitted = this.replace(/^phoundry_/i, '').split('|');
							if(splitted[0] && splitted[1]) {
								url = '/index.php/phoundryMe/record/'+splitted[0]+'/'+splitted[1];
							}
						}
					});
				}
				
				if (!url) {
					var page = $('meta[name="PhoundryMe"]').attr('content');
					if (page) {
						url = '/index.php/phoundryMe/structure/' + page + '?url=' + escape(document.location);
					}
				}
				
				if(url) {
					try {
						window.open(url, 'Phoundry');
					} 
					catch (e) { // when popup failes
						window.location.href = url;
					}
					return false;
				}
			}
		});
	});
} else {
	document.ondblclick = function phoundryMe(event){
		if((event.ctrlKey) || (window.event.ctrlKey)){
			var page = document.getElementById('PhoundryMe');
			if(page.content) {
				window.open('/index.php/phoundryMe/structure/'+page.content + '?url=' + escape(document.location), 'Phoundry');
			}
		}
		return false;
	};
}