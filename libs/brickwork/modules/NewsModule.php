<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("NewsModule is deprecated in favor of BrickworkNewsModule", E_USER_ERROR);
}

/**
* NewsModule
*
* Haal alle nieuwsitems op en dien als koppeling tussen de module en de nieuwsartikelen
* Maakt gebruik van database om gegevens uit te laden en in artikelobjecten om te toveren
*
* @author vincent@webpower.nl
* @version 1.0.0
* @last_modified 3-7-2007
* @deprecated
*/

class NewsModule extends Webmodule{
	/*
	* @var integer
	* @acces private
	*/
	private $_show_intro = NULL;

	/*
	* @var integer
	* @acces private
	*/
	private $_article_id = NULL;

	/*
	* has the viewable category id's from the config
	* comma separated
	*
	* @var string
	* @acces private
	*/
	private $_cats;

	/*
	* has the viewable categories from the config or user
	*
	* @var array
	* @acces private
	*/
	private $_categories;

	/*
	* @var integer
	* @acces private
	*/
	private $_mini_type = 'news';

	/*
	* @var integer
	* @acces private
	*/
	private $_items_per_page = 5;

	/*
	* @var string
	* @acces private
	*/
	private $_view = 'Recent';

	/*
	* @var object
	* @acces private
	*/
	private $_errormessage;

	/*
	* type of content showed by module
	*
	* @var string
	* @acces private
	*/
	private $_module_content = 'recent';

	/*
	* @var int
	* @acces private
	*/
	private $_page_nr;

	/*
	* @var object
	* @acces public
	*/
	public $archive_articles;

	/*
	* @var string
	* @acces public
	*/
	public $filter_cats;

	/*
	* @var object
	* @acces public
	*/
	public $archive_article;

	/*
	* @var object
	* @acces public
	*/
	public $mini_articles;

	/*
	* @var object
	* @acces public
	*/
	public $recent_article;

	/*
	* @var object
	* @acces public
	*/
	public $recent_articles;

	/*
	* @var object
	* @acces public
	*/
	public $artination;

	/*
	* @var bool
	* @acces public
	*/
	public $pagination;

	/*
	* @var bool
	* @acces public
	*/
	public $show_pagination = TRUE;

	/*
	* holds pagenumber for the "back" link on detailview
	*
	* @var string
	* @acces public
	*/
	public $return_page_id = 0;

	/*
	* holds all the id's from articles showed in the current overview
	*
	* @var array
	* @acces private
	*/
	private $_id_list = array();

	/**
	* Holds the page  numbers for pagination
	* @var
	* @acces public
	*/
	public $pages;

	/**
	* Holds the page  numbers for pagination
	* @var integer
	* @acces public
	*/
	public $_current_page_id;

	/**
	* Holds the "next" article id for artination
	* @var int
	* @acces public
	*/
	public $next_article;

	/**
	* Holds the "previous" article id for artination
	* @var int
	* @acces public
	*/
	public $prev_article;

	/*
	* possible values for $_view
	* @var array
	* @acces private
	*/
	private $_view_options = array(
		'Recent',
		'DetailRecent',
		'Archive',
		'DetailArchive',
		'MiniRecent',
		'Highlight',
		'MiniArchive'
	);

	public function __construct($id, $cc){
		parent::__construct($id, $cc);

		// set article_id and page_nr
		$this->_article_id	= (!empty($this->get['article_id']))	?(int)$this->get['article_id']	: NULL;
		$this->_page_nr		= (!empty($this->get['page_nr']))		?(int)$this->get['page_nr']		: 0;	

		return true;
	}

	public function __get($name){
		switch($name){
			case 'view':
				return $this->_view;
				break;
			case 'detail_page_id':
				return $this->_detail_page_id;
				break;
			case 'current_page_id':
				return $this->_current_page_id;
				break;
			case 'module_content':
				return $this->_module_content;
				break;
			case 'show_intro':
				return $this->_show_intro;
				break;
			case 'categories':
				return $this->_categories;
				break;
			case 'cats':
				return $this->_cats;
				break;
			case 'errormessage':
				return $this->_errormessage;
				break;
			case 'filter_array':
				return $this->_filter_array;
				break;
			default:
				trigger_error('kanniet getten -->'.$name);
		}
	}

	protected function LoadData(){

		// stuk als er geen config is, anders kan het zomaar gebeuren dat er query's uitgevoerd worden op 
		// tabellen die niet bestaan
		if (!isset($this->config)){
			return TRUE;
		}

		// set tables for the news manager
		if (isset($this->config->lookup_table)) {
			NewsManager::setTable('lookup_table', $this->config->lookup_table);
		}else{
			NewsManager::setTable('lookup_table', 'lookup_module_news_site');
		}		
		if (isset($this->config->content_table)) {
			NewsManager::setTable('content_table', $this->config->content_table);
		}else{
			NewsManager::setTable('content_table', $this->content_table);
		}
		if (isset($this->config->category_table)) {
			NewsManager::setTable('category_table', $this->config->category_table);
		}else{
			NewsManager::setTable('category_table', $this->content_table.'_category');
		}

		// set show_intro
		$this->_show_intro			= (isset($this->config->show_intro))		? $this->config->show_intro				: NULL;

		// set show_pagination
		$this->show_pagination		= (isset($this->config->show_pagination) && $this->config->show_pagination == 1)		? TRUE				: FALSE;

		// set detail_page_id
		$this->_detail_page_id		= (isset($this->config->detail_page_id))		? $this->config->detail_page_id				: NULL;

		// set the type of view for the miniview to-> archive || news
		$this->_mini_type			= (isset($this->config->mini_type))			? $this->config->mini_type				: $this->_mini_type;

		// set type of news showed
		$this->_module_content		= (isset($this->config->module_content))	? $this->config->module_content			: $this->_module_content;

		// set the # of items showed in a page
		$this->_items_per_page		= (isset($this->config->items_per_page))	? (int)$this->config->items_per_page	: $this->_items_per_page;

		// this->cats holds the categories->id's that can be viewed
		$this->_cats				= (isset($this->config->viewable_cats))		? $this->config->viewable_cats			: NULL;

		// default filter_cats = cats
		$this->filter_cats			= $this->_cats;

		// set $this->_view according to content container, article_id, get['view'] and the config (_view_type).
		if ($this->_module_content == 'recent'){

			switch($this->contentContainer->code) {
				case 'cc_content':
					if (isset($this->_article_id)){
						$this->_view = 'DetailRecent';
					}else{
						$this->_view = 'Recent';
					}
				break;
				case 'cc_top':
				case 'cc_left':
				case 'cc_right':
					$this->_view = 'MiniRecent';
				break;
			}

		}elseif($this->_module_content == 'archive'){

			switch($this->contentContainer->code) {
				case 'cc_content':
					if (isset($this->_article_id)){
						$this->_view = 'DetailArchive';
					}else{
						$this->_view = 'Archive';
					}
				break;
				case 'cc_top':
				case 'cc_left':
				case 'cc_right':
					$this->_view = 'MiniArchive';
				break;
			}

		}elseif($this->_module_content == 'recent_archive'){

			switch($this->contentContainer->code) {
				case 'cc_content':
					if (isset($this->_article_id)){
						if (isset($this->get['view']) == 'Archive'){
							$this->_view = 'DetailArchive';
						}else{
							$this->_view = 'DetailRecent';
						}
					}elseif (isset($this->get['view']) && in_array($this->get['view'], $this->_view_options)){
						$this->_view = $this->get['view'];
					}
				break;
				case 'cc_top':
				case 'cc_left':
				case 'cc_right':
					// default
					$this->_view = 'MiniRecent';
					if (isset($this->_mini_type)){
						switch($this->_mini_type){
							case 'archive':
								$this->_view = 'MiniArchive';
								break;
							case 'news':

							default:
								$this->_view = 'MiniRecent';
								break;
						}
					}
				break;
			}

		}elseif($this->_module_content == 'highlight'){

			$this->_view = 'Highlight';

		}

		// overwrite filter_cats if there is input, get the array of categories and set the current category.
		if (isset($this->request['filter_cats'])){
			$this->filter_cats					= $this->request['filter_cats'];
			$this->session['filter_cats']		= $this->request['filter_cats'];
		}elseif (isset($this->session['filter_cats'])){
			$this->filter_cats					= $this->session['filter_cats'];
		}

		// checks what value $view has and returns the needed methods.
		switch($this->_view){
			case 'Archive':
				// haal alle archief artikelen op
				$this->archive_articles		= NewsManager::getArchivedArticles($this->filter_cats, $this->_items_per_page, $this->_page_nr);
				// zet alle benodigde dingen voor category nav
				$this->cartination			= $this->_cartination($this->_cats, $this->filter_cats, $this->_view);
				// zet pagina navigatie
				$this->pagination			= $this->_pagination(NewsManager::$totalItems, $this->_items_per_page, $this->_page_nr);


				return TRUE;
				break;
			case 'DetailArchive':
				// haal artikel op
				$this->archive_article		= NewsManager::getArchivedArticle($this->_article_id);
				// zet <title>
				$this->contentContainer->contentHandler->title .= ' - '.$this->archive_article->title;
				// zet id lijst
				$this->_id_list				= NewsManager::getArticleIds(Article::archived, $this->filter_cats);
				// zet artikel navigatie
				$this->artination			= $this->_artination($this->archive_article->id, $this->_id_list);
				// zet alle benodigde dingen voor category nav
				$this->cartination			= $this->_cartination($this->_cats, $this->filter_cats, $this->_view);
				// zet pagina nummer van artikel
				$this->return_page_id		= $this->_get_return_page_id($this->archive_article->id, $this->_items_per_page, $this->_id_list);
				return TRUE;
				break;
			case 'DetailRecent':
				// haal artikel op
				$this->recent_article		= NewsManager::getRecentArticle($this->_article_id);
				// zet <title>
				$this->contentContainer->contentHandler->title .= ' - '.$this->recent_article->title;
				// zet id lijst
				$this->_id_list				= NewsManager::getArticleIds(Article::recent, $this->filter_cats);
				// zet artikel navigatie
				$this->artination			= $this->_artination($this->recent_article->id, $this->_id_list);
				// zet alle benodigde dingen voor category nav
				$this->cartination			= $this->_cartination($this->_cats, $this->filter_cats, $this->_view);
				// zet pagina nummer van artikel
				$this->return_page_id		= $this->_get_return_page_id($this->recent_article->id, $this->_items_per_page, $this->_id_list);
				return TRUE;
				break;
			case 'MiniRecent':
				// haal alle mini nieuws artikelen op
			
				$this->mini_articles		= NewsManager::getMiniRecentArticles($this->_cats, $this->_items_per_page, $this->_page_nr);
				return TRUE;
				break;
			case 'MiniArchive':
				// haal alle mini archief artikelen op
				$this->mini_articles		= NewsManager::getMiniArchiveArticles($this->_cats, $this->_items_per_page, $this->_page_nr);
				return TRUE;
				break;
			case 'Highlight':
				$this->high_articles		= NewsManager::getHighlightedArticles($this->filter_cats, $this->_items_per_page, $this->_page_nr, TRUE);
				return TRUE;
				break;
			case 'Recent':				
			default:				
				// haal alle nieuws artikelen op
				$this->recent_articles		= NewsManager::getRecentArticles($this->filter_cats, $this->_items_per_page, $this->_page_nr);
				// zet alle benodigde dingen voor category nav
				$this->cartination			= $this->_cartination($this->_cats, $this->filter_cats, $this->_view);
				// zet pagina navigatie
				$this->pagination			= $this->_pagination(NewsManager::$totalItems, $this->_items_per_page, $this->_page_nr);
				return TRUE;
				break;
		}

	}

	/**
	* create set get etc. categories
	*
	* @param String: cat id's die getoond mogen worden aan de user
	* @param String: cat id's waarop de user filters
	* @param String: type view
	*/
	private function _cartination($cats, $filter_cats, $view){

		// set flag according to the type of view. default = recent
		$flag = Article::recent;
		$flag = ($view == 'Archive' || $view == 'ArchiveDetail') ? Article::archived : $flag ;




		// haal de categorien op in goede volgorde. als er al gefilterd word zal de
		// betreffende cat op key 0 worden gereturned
		$this->_categories			= NewsManager::getCategories($cats, $filter_cats, $flag);

		// set filter array. hier zitten de categorieen waarop de gebruiker kan filteren
		// de optie "alles" is hieraan toegevoegd. de huidige selectie staat bovenaan in de
		// array
		$this->_filter_array		= $this->_get_filter_array($cats, $filter_cats, $this->_categories);

		// set current cat. toont de categorie waarop gefilterd is als de detailview actief is.
		switch ($view){
			case 'DetailArchive':
				$this->current_cat			= ($cats !== $filter_cats)? $this->_categories[0] : NULL ;
			break;
			case 'DetailRecent':
				$this->current_cat			= ($cats !== $filter_cats)? $this->_categories[0] : NULL ;
			break;
		}

		// Kijk of filtercats gezet is. Zo ja, haal dan de cats op en kijk
		// of de gevraagde cat bestaat binnen de array. Zo niet toon een ErrorArtikel
		// met een link naar complete overzicht van de betreffende view.
		if ($cats !== $filter_cats){
			$i=0;
			foreach ($this->_filter_array as $cat){
				if ($cat->id == $this->filter_cats){
				$i++;
				}
			}
			if ($i == 0){
				if ($this->view == 'Archive'){
					$this->_errormessage	= new ErrorArticle('', 'Geen artikel', '');
					$this->_errormessage->intro = 'Er staan geen artikelen in het archief voor deze categorie, klik op onderstaande link ';
					$this->_errormessage->intro.= 'voor een volledig overzicht van het archief';
				}else{
					$this->_errormessage	= new ErrorArticle('', 'Geen artikel', '');
					$this->_errormessage->intro = 'Er zijn geen artikelen binnen deze categorie, klik op onderstaande link ';
					$this->_errormessage->intro.= 'voor een volledig overzicht van alle artikelen';

				}

			}
		}

		return TRUE;
	}

	/**
	* _get_filter_array()
	*
	* genereert de array met categorien waarop gefilterd kan worden. met de huidige waarde op key 0
	*
	* @param String: cat id's die getoond mogen worden aan de user
	* @param String: cat id's waarop de user filters
	* @param Array: an array with objects of categories holding name & id
	*/
	private function _get_filter_array($cats, $filter_cats, $categories){

			// als filter_cats en cats gelijk zijn worden alle categorien getoond. dus dan
			// moet de optie "alles" op key 0. zo niet staat "alles" als laatst in de array
			$filter_array = array();
			if ($filter_cats == $cats){
				$filter_array[] = new Categorie($cats, 'Alles');
				foreach ($categories as $cat){
					$filter_array[]  = $cat;
				}
			}else{
				foreach ($categories as $cat){
					$filter_array[]  = $cat;
				}
				$filter_array[] = new Categorie($cats, 'Alles');
			}

			return $filter_array;
	}

	/**
	* _get_return_page_id
	*
	* genereert het pagina nummer waar het huidige artikel opstaat
	*
	* @access private
	* @param Int: concerned article id
	* @param Int: has the number of viewable items on a page
	* @param Array: holding the id's of the current viewable articles
	*/
	private function _get_return_page_id($article_id, $items_per_page, $list){

		// get arraykey of current article
		$option = array_search($article_id, $list);

		// deel het aantal artikelen op een pagina door de key van het huidge artikel
		if($option){
			return floor($option/$items_per_page);
		}else{
			return 0;
		}
	}

	/**
	* pagination
	*
	* maak de navigatie tussen verschillende overzicht pagina's
	*
	* @access private
	* @param Int: total concerned items
	* @param Int: items per page, from config
	* @param Int: page number from get
	*/
	private function _pagination($totalItems, $itemsPerPage, $page_nr){

		if ($itemsPerPage == 0){
			return FALSE;
		}

		// set pages and round off pages
		$pages = $totalItems / $itemsPerPage;
		$pages = (is_integer($pages)) ? ceil($pages-1) : $pages;

		// fill the array $this->pages
		for($i=0;$i<=$pages;$i++){
			$this->pages[$i] = (isset($page_nr) && $page_nr == $i) ? TRUE : FALSE;
		}

		// set previous / next
		$this->next		= isset($this->pages[$page_nr+1]) ? $page_nr+1 : NULL;
		$this->previous = isset($this->pages[$page_nr-1]) ? $page_nr-1 : NULL;

		return TRUE;
	}

	/**
	* artination
	*
	* maak de navigatie tussen verschillende detail pagina's
	*
	* @access private
	* @param Int: concerned article id
	* @param Array: holding the id's of the current viewable articles
	*/
	private function _artination($article_id, $list){

		// kijk of de key van het huidige artikel in de lijst voorkomt.
		$key = array_search($article_id, $list);

		// zo ja, zet zijn buurman en buurvrouw als de er zijn binnen de lijst.
		if(is_int($key)){
			$this->next_article = isset($list[$key+1]) ? $list[$key+1] : NULL;
			$this->prev_article = isset($list[$key-1]) ? $list[$key-1] : NULL;
		}

		return TRUE;
	}
}


/**
* NewsManager
*
* Haal alle nieuwsitems op en dien als koppeling tussen de module en de nieuwsartikelen
* Maakt gebruik van database om gegevens uit te laden en in artikelobjecten om te toveren
*
* @author Vincent
* @version 1.0.0
* @last_modified 1-3-2007
*/
class NewsManager {
	// teller die het aantal rijen teruggeeft van laatst uitgevoerde select
	public static $totalItems = 0;

	// tables
	private static $_content_table;
	private static $_category_table;
	private static $_lookup_table;

	public function __construct(){
		trigger_error('NewsManager can only be used as class', E_USER_ERROR);
	}

	public function setTable($code, $value) {

		switch ($code) {
			case 'lookup_table':
				self::$_lookup_table = $value; break;
			case 'content_table':
				self::$_content_table = $value; break;
			case 'category_table':
				self::$_category_table = $value; break;
			default:
				trigger_error('Can\'t set table, unknown table code', E_USER_WARNING);
		}

		return TRUE;
	}

	/**
	* Maak een statische database verbinding
	*
	* @access private
	* @static
	*/
	private static function _connect_db() {
		static $db = FALSE;

		if (FALSE === $db) {
			$db = DB::connect_db();
		}

		return $db;
	}

	/**
	* Gets a number of highlighted articles
	*/
	public static function getHighlightedArticles ($cats, $itemsPerPage, $pageNr=0, $highlight) {
		$arts = self::_get_articles(Article::recent, $cats, $itemsPerPage, $pageNr, $highlight);
		return $arts;
	}

	/**
	* Haal alle gearchiveerde artikelen op
	*/
	public static function getArchivedArticles ($cats, $itemsPerPage, $pageNr=0) {
		$arts = self::_get_articles(Article::archived, $cats, $itemsPerPage, $pageNr);
		return $arts;
	}

	/**
	* Haal alle normale artikelen op
	*/
	public static function getRecentArticles ($cats, $itemsPerPage, $pageNr=0) {
		$arts = self::_get_articles(Article::recent, $cats, $itemsPerPage, $pageNr);
		return $arts;
	}

	/**
	* Haal alle normale artikelen op voor de mini view
	*/
	public static function getMiniRecentArticles ($cats, $itemsPerPage, $pageNr=0) {
		$arts = self::_get_articles(Article::recent, $cats, $itemsPerPage, $pageNr);
		return $arts;
	}

	/**
	* Haal alle archief artikelen op voor de mini view
	*/
	public static function getMiniArchiveArticles ($cats, $itemsPerPage, $pageNr=0) {
		$arts = self::_get_articles(Article::archived, $cats, $itemsPerPage, $pageNr);
		return $arts;
	}

	/**
	* Haal een specifiek archief artikel op
	*/
	public static function getArchivedArticle ($art_id) {
		$art = self::_get_article(Article::archived, $art_id);
		return $art;
	}

	/**
	* Haal een specifiek normaal artikel op
	*/
	public static function getRecentArticle ($art_id) {
		$art = self::_get_article(Article::recent, $art_id);
		return $art;
	}

	/**
	* Haal artikelen uit database
	*
	* Geef met vlag aan of het een gearchiveerd artikel is of een normaal nieuws artikel
	*
	* @access private
	* @param Const Article::archived || Article::news
	* @param String ids van categorieen, comma separated
	*/
	private static function _get_articles($flag, $cats, $itemsPerPage, $pageNr, $highlight = FALSE) {
		$db = self::_connect_db();

		$offset = self::_calc_offset($itemsPerPage, $pageNr);
		$fields = self::_get_select_fields();
		$_is_archive = (Article::archived == $flag) ? 1 : 0;

		$and_highlight = '';
		// create sql to get highlighted articles
		if (TRUE ==  $highlight){
			$and_highlight = 'AND highlight = 1';
		}


		// bouw mooi stukje sql voor categorieen
		$categories = '';
		if (strlen($cats) > 0) {
			$categories = sprintf("AND cn.category_id IN (%s)", $cats);
		}

		// zoeken in archief?
		$sql_archive = '';
		if ($_is_archive) {
			$sql_archive = sprintf('("%s" NOT BETWEEN cn.online_date AND cn.offline_date) AND cn.archive = 1', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}else{
			$sql_archive = sprintf('("%s" BETWEEN cn.online_date AND cn.offline_date)', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}

		// limiteren?
		$limit = '';
		if ('*' != $itemsPerPage) {
			$limit = sprintf("LIMIT %d, %d", abs($offset), abs($itemsPerPage));
		}

		$sql = sprintf('
			SELECT SQL_CALC_FOUND_ROWS
				%s
			FROM			
				brickwork_site s
			INNER JOIN 
				%s mns ON mns.site_identifier = s.identifier
			INNER JOIN 
				%s cn ON cn.id = mns.news_id
			LEFT JOIN		
				%s cnc ON cn.category_id = cnc.id
			WHERE			%s

			%s
			%s
            AND				s.identifier = "%s"

			ORDER BY		cn.create_date DESC, cn.id DESC
			%s
		',
			$fields,
			DB::quote(self::$_lookup_table),
			DB::quote(self::$_content_table),
			DB::quote(self::$_category_table),
			$sql_archive,
			$categories,
			$and_highlight,
			DB::quote(Framework::$site->identifier),
			$limit
		);

		$res = DB::iQuery($sql);

		if(self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);
		}

		// set result. when there is no result return "not available article"
		$arts = array();
		if ($res->current())
		{
			while ($res->current()) 
			{
			    $row = $res->current();
				if ($_is_archive) {
					$art = new ArchivedArticle($row->id, $row->title, $row->body);
				} else {
					$art = new RecentArticle($row->id, $row->title, $row->body);
				}

				if ($row->image) {
					$dimensions			= self::_get_image_dimensions($row->image);
					$art->image			= $row->image;
					$art->image_width	= $dimensions[0];
					$art->image_height	= $dimensions[1];
				}

				$art->intro			= $row->intro;
				$art->category_name = $row->name;
				$art->create_date	= $row->create_date;

				$arts[] = $art;
				$res->next();
			}

			// vertel manager totaal aantal rijen
			self::$totalItems = count($arts);
		}

		return $arts;
	}

	/**
	* Haal een specifiek artikel op uit de database
	*
	* Geef met vlag aan of het een gearchiveerd artikel is of een normaal nieuws artikel
	*
	* @access private
	* @param Const Article::archived || Article::news
	* @param Int artikel id
	* @return Object
	*/
	private static function _get_article($flag, $art_id) {
		$db				= self::_connect_db();
		$fields			= self::_get_select_fields();
		$_is_archive	= (Article::archived == $flag) ? 1 : 0;

		// zoeken op dit id:
		$sql_id			= sprintf('AND cn.id =%d', $art_id);

		// zoeken in archief?
		$sql_archive = '';
		if ($_is_archive) {
			$sql_archive = sprintf('("%s" NOT BETWEEN cn.online_date AND cn.offline_date) AND cn.archive = 1', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}else{
			$sql_archive = sprintf('("%s" BETWEEN cn.online_date AND cn.offline_date)', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}

		$sql = sprintf('
			SELECT
				%s
			FROM		
				brickwork_site s	
			INNER JOIN 
				%s mns ON mns.site_identifier = s.identifier
			INNER JOIN 
				%s cn ON cn.id = mns.news_id
			LEFT JOIN		
				%s cnc ON cn.category_id = cnc.id
			WHERE
				%s
			%s
            AND				s.identifier = "%s"
			LIMIT
				1
		',
			$fields,
			DB::quote(self::$_lookup_table),
			DB::quote(self::$_content_table),
			DB::quote(self::$_category_table),
			$sql_archive,
			$sql_id,
			DB::quote(Framework::$site->identifier)
		);

		$res = DB::iQuery($sql);

		if(self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);
		}

		// set result. when there is no result return "not available article"
		$art = NULL;
		if ( $res->current() )
		{
			//$row = $res->result[0];
			$row = $res->first();
			
			if ($_is_archive) 
			{
					$art = new ArchivedArticle($row->id, $row->title, $row->body);
				} else {
					$art = new RecentArticle($row->id, $row->title, $row->body);
				}

				if ($row->image) {
					$dimensions			= self::_get_image_dimensions($row->image);
					$art->image			= $row->image;
					$art->image_width	= $dimensions[0];
					$art->image_height	= $dimensions[1];
				}

				$art->intro			= $row->intro;
				$art->create_date	= $row->create_date;

		}else{
			$art		= new ErrorArticle('', 'Fout', '');
			$art->intro = 'Er is een artikel opgevraagd dat niet in de database gevonden kan worden klik op onderstaande link';
			$art->intro.= 'voor een volledig overzicht van het de artikelen';
		}

		return $art;
	}

	/**
	* _get_image_dimensions()
	*
	* @param Str
	*/
	private static function _get_image_dimensions($path){

		if (file_exists(UPLOAD_DIR.$path)) {
			$values = getimagesize(UPLOAD_DIR.$path);
		} else {
			return FALSE;
		}

		if (!$values){
			return FALSE;
		}else{
			$dimensions[] = $values[0];
			$dimensions[] = $values[1];
		}

		return $dimensions;
	}

	/**
	* Bepaal selectievelden voor de sql
	*
	* @return String
	*/
	private function _get_select_fields() {
		$fields = 'cn.id, cn.title, cn.intro, cn.body,	cn.create_date,	cn.archive, cn.category_id, cn.image, cnc.name';
		return $fields;
	}

	/**
	* geeft een leeg artikel terug voor als er een fout is
	*
	* @return Object
	* @param Bool: artikel in archief
	*/
	public function _set_empty_article($is_archive = 0) {
		if ($is_archive) {
			$art = new ArchivedArticle('', 'Er is geen archief(artikel) gevonden', '');
		} else {
			$art = new RecentArticle('', 'Er is geen nieuws(artikel) gevonden', '');
		}

		return $art;
	}

	/*
	* getCategories()
	*
	* haalt een array op met de categorien die beschikbaar zijn voor de gebruiker.
	* en checked of er wel nieuws staat in de categorie. zo nee wordt deze ook niet meegegeven.
	*
	* @return array
	* @param string: viewable category id's
	*/
	public static function getCategories($cats = NULL, $sortby = NULL, $flag){
		$db = self::_connect_db();

		$sort = '';
		if (strlen($sortby) == 1){
			$sort = sprintf('cnc.id = %d DESC,', $sortby);
		}

		$sql_extra = '1=1';
		if ($cats){
			$sql_extra = sprintf("cnc.id IN (%s)", $cats);
		}

		$_is_archive	= (Article::archived == $flag) ? 1 : 0;

		// zoeken in archief?
		$sql_archive = '';
		if ($_is_archive) {
			$sql_archive = sprintf('AND ("%s" NOT BETWEEN cn.online_date AND cn.offline_date) AND cn.archive = 1', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}else{
			$sql_archive = sprintf('AND ("%s" BETWEEN cn.online_date AND cn.offline_date)', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}

		$sql = sprintf('
			SELECT
				cnc.id,
				cnc.name,
				(SELECT count(*) FROM %s cn WHERE cn.category_id = cnc.id AND %s %s) as cnt
			FROM
				%s cnc
			WHERE
				site_identifier = \'%s\' OR site_identifier = NULL
			ORDER BY
				%s
				cnc.name

		',
			DB::quote(self::$_content_table),
			$sql_extra,
			$sql_archive,
			DB::quote(self::$_category_table),
			DB::quote(Framework::$site->identifier),
			$sort
		);

		$res = DB::iQuery($sql);

		if(self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);
		}

		$categories = array();
		if ($res->current()){
			while($res->current()){
			    $row = $res->current();
				if ($row->cnt > 0){
					$categories[] = new Categorie($row->id, $row->name);
				}
				$res->next();
			}
		}

		return $categories;
	}

	/**
	* Haal alle id's van de artikelen uit database
	*
	* Geef met vlag aan of het een gearchiveerd artikel is of een normaal nieuws artikel
	*
	* @access private
	* @param Const Article::archived || Article::news
	* @param String ids van categorieen, comma separated
	* @return Array
	*/
	public static function getArticleIds($flag, $cats) {
		$db = self::_connect_db();

		$_is_archive = (Article::archived == $flag) ? 1 : 0;

		// bouw mooi stukje sql voor categorieen
		$categories = '';
		if (!empty($cats)) {
			$categories = sprintf("AND cn.category_id IN (%s)", $cats);
		}

		// zoeken in archief?
		$sql_archive = '';
		if ($_is_archive) {
			$sql_archive = sprintf('("%s" NOT BETWEEN cn.online_date AND cn.offline_date) AND cn.archive = 1', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}else{
			$sql_archive = sprintf('("%s" BETWEEN cn.online_date AND cn.offline_date)', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}

		$sql = sprintf('
			SELECT
				cn.id
			FROM			
				brickwork_site s
			INNER JOIN 
				%s mns ON mns.site_identifier = s.identifier
			INNER JOIN 
				%s cn ON cn.id = mns.news_id
			INNER JOIN		
				%s cnc
			ON
				cn.category_id = cnc.id
			WHERE
				%s
			%s
            AND				s.identifier = "%s"

			ORDER BY
				cn.create_date DESC, cn.id DESC
		',
			DB::quote(self::$_lookup_table),
			DB::quote(self::$_content_table),			
			DB::quote(self::$_category_table),
			$sql_archive,
			$categories,
			DB::quote(Framework::$site->identifier)

		);

		$res = DB::iQuery($sql);
		
		if(self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);
		}
		
		$ids = array();
		if ($res->current()){
			while ($res->current()) {
			    $row = $res->current();
				$ids[] = $row->id;
				$res->next();
			}
		}

		return $ids;
	}

	/**
	* Berekend de offset voor de pagination
	*/
	private function _calc_offset($itemsPerPage, $pageNr){

		if (!is_int($itemsPerPage)) {
			trigger_error('oops', E_USER_WARNING);
		}

		if (!is_int($pageNr)) {
			trigger_error('oops', E_USER_WARNING);
		}

		$offset	= $pageNr * $itemsPerPage;

		return $offset;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

/**
* Article
*
* Basis artikel voor alle nieuwsberichten. Overerf deze klasse om van de juiste artikelen gebruik te kunnen maken
*
* @author Vincent
* @version 1.0.0
* @last_modified 9-3-2007
*/
abstract class Article {
	const archived = 'a_archived';
	const recent   = 'a_news';
	const error	   = 'a_error';

	protected $_id;
	protected $_title;
	protected $_intro;
	protected $_body;
	protected $_image;
	protected $_image_width;
	protected $_image_height;
	protected $_create_date;
	protected $_archive;
	protected $_category_id;
	protected $_category_name;

	public function __construct($id, $title, $body){
		$this->_id				= $id;
		$this->_title			= $title;
		$this->_body			= $body;
	}

	public function __get($name){
		switch($name){
			case 'id':
				return $this->_id;
				break;
			case 'title':
				return $this->_title;
				break;
			case 'intro':
				return $this->_intro;
				break;
			case 'body':
				return $this->_body;
				break;
			case 'image':
				return $this->_image;
				break;
			case 'image_width':
				return $this->_image_width;
				break;
			case 'image_height':
				return $this->_image_height;
				break;
			case 'create_date':
				return $this->_create_date;
				break;
			case 'archive':
				return $this->_archive;
				break;
			case 'in_archive':
				return $this->_in_archive;
				break;
			case 'category_id':
				return $this->_category_id;
				break;
			case 'category_name':
				return $this->_category_name;
				break;
			default:
				trigger_error('kanniet getten');
		}
	}

	public function __set($name, $value){
		switch($name){
			case 'id':
				$this->_id = (int)$value;
				break;
			case 'title':
				$this->_title = $value;
				break;
			case 'intro':
				$this->_intro = $value;
				break;
			case 'body':
				$this->_body = $value;
				break;
			case 'image':
				$this->_image = $value;
				break;
			case 'image_width':
				$this->_image_width = $value;
				break;
			case 'image_height':
				$this->_image_height = $value;
				break;
			case 'create_date':
				$this->_create_date = $value;
				break;
			case 'archive':
				$this->_archive = (int)$value;
				break;
			case 'category_id':
				$this->_category_id = (int)$value;
				break;
			case 'category_name':
				$this->_category_name = $value;
				break;
			default:
				trigger_error('kanniet setten');
		}
	}
}

/**
* ArchivedArticle
*
* Artikel dat uit het archief komt
*/
class ArchivedArticle extends Article {
}

class RecentArticle extends Article {
}

class ErrorArticle extends Article {
}


/**
* Categorie
*
* @author vincent@webpower.nl
* @version 1.0.0
* @last_modified 9-3-2007
*/
class Categorie {

	protected $_id;
	protected $_name;

	public function __construct($id, $name){
		$this->_id				= $id;
		$this->_name			= $name;
	}

	public function __get($name){
		switch($name){
			case 'id':
				return $this->_id;
				break;
			case 'name':
				return $this->_name;
				break;
			default:
				trigger_error('kanniet getten');
		}
	}

	public function __set($name, $value){
		switch($name){
			case 'id':
				$this->_id = (int)$value;
				break;
			case 'name':
				$this->_name = $value;
				break;
			default:
				trigger_error('kanniet setten');
		}
	}
}
?>