<?php
/**
 * Forum Module
 */
class ForumModule extends Webmodule
{

	/**
	 * @var int Page to return to
	 */
	public $return_page_id = 0;

	/**
	 * @var boolean
	 * @todo Weg, Webmodule heeft namelijk een property _use_cache
	 */
	protected $use_cache		= FALSE;

	/**
	 * @var array Used to filter views
	 */
	protected $validViews	= array('overview', 'detail');

	/**
	 * @var integer
	 */
	protected $topic_id;

	/**
	 * @var string the current view
	 */
	public $view = 'overview';

	/**
	 * @var string The various forums are defined as groups
	 */
	public $topic_group = 'group1';

	/**
	 * @var array
	 */
	public $topics = array();

	/**
	 * @var array
	 */
	public $posts = array();

	/**
	 * @var array
	 */
	public $errors = array();
	
	/**
	 * @var array
	 */
	public $errors_by_name;

	/**
	 * @var int
	 */
	public $topics_per_page = 30;

	/**
	 * @var int
	 */
	public $posts_per_page = 15;

	/**
	 * @var int
	 */
	public $page_id = 0;

	/**
	 * @var int
	 */
	public $cnt_topics;

	/**
	 * @var int
	 */
	public $previous;

	/**
	 * @var int
	 */
	public $next;

	/**
	 * @var array
	 */
	public $pages = array();

	/**
	 * @var string
	 */
	public $topic = '';

	/**
	 * @var string
	 */
	public $description	= '';

	/**
	 * @var string
	 */
	public $author_name  = '';

	/**
	 * @var string
	 */
	public $create_date	= '';

	/**
	 * @var int
	 */
	public $post_count	=	NULL;

	/**
	 * Construct
	 */
	public function __construct($id, $cc)
	{
		parent::__construct($id, $cc);

		$this->cacheParams = array($id, $this->topic_id, $this->view);
		$this->cachestring = sha1(implode('_', $this->cacheParams));

		// Check for topic id
		$this->topic_id = (!empty($this->get['topic_id']) && is_numeric($this->get['topic_id'])) ? (int)$this->get['topic_id'] : NULL;

		if(!empty($this->topic_id)){
			$this->view = 'detail';
		}

		// Set offset
		$this->page_id = (isset($this->request['page_id'])) ? (int)$this->request['page_id'] : $this->page_id;
	}

	/**
	 * Load data
	 */
	protected function loadData()
	{
		// kijken of er een user is op te defaulten
		$_auth = Auth::singleton();

		if(!$_auth->user->isAnonymous()){
			if(empty($this->post['name'])){
				$this->post['name'] =$_auth->user->name->strfname("%f");
			}
			if(empty($this->post['email']) && !empty($_auth->user->email)){
				$this->post['email'] =$_auth->user->email->address;
			}
		}

		$this->handlePostData();

		$this->topics_per_page  = (isset($this->config->topics_per_page)) ?
			$this->config->topics_per_page : $this->topics_per_page ;

		switch($this->view) {
			case 'overview':
				return $this->loadOverview();
			case 'detail':
				// @Poll & Vin 5/23/2007
				// zet id lijst
				$idList				= $this->getArticleIds();
				// haal pagina id op waar artikel op staat
				$this->return_page_id = $this->_get_return_page_id($this->topic_id, $this->topics_per_page, $idList);
				return $this->loadDetailView();
		}
		return false;
	}

	/**
	 * Adds new topic to forum
	 *
	 * @param string $name
	 * @param string $email
	 * @param string $topic
	 * @param string $description
	 */
	public function addTopic($name, $email, $topic, $description)
	{
		if(!isset($this->config) || !is_object($this->config)){
			trigger_error('Geen config aanwezig', E_USER_ERROR);
		}

		// Topic group
		$this->topic_group = isset($this->config->show_topic_group) ?
			$this->config->show_topic_group : $this->topic_group;

		$sql = sprintf('
			INSERT INTO module_content_forum
			(
				site_identifier,
				create_date,
				last_post_date,
				topic_group,
				author_name,
				author_email,
				topic,
				description
			)
				VALUES
			(
				"%s",
				NOW(),
				NOW(),
				"%s",
				"%s",
				"%s",
				"%s",
				"%s"
			)
			',
			DB::quote(Framework::$site->identifier),
			DB::quote($this->topic_group),
			DB::quote($name),
			DB::quote($email),
			DB::quote($topic),
			DB::quote($description)
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($res, E_USER_ERROR);
		}

		return true;
	}

	/*
	 * addPost
	 *
	 * Adds news post to topic
	 *
	 * @param string
	 * @param string
	 * @param string

	 */
	public function addPost($name, $email, $message) {

		if(!isset($this->config) || !is_object($this->config)){
			trigger_error('Geen config aanwezig', E_USER_ERROR);
		}

		$sql = sprintf('
			INSERT INTO %s
			(
				topic_id,
				post_date,
				author_name,
				author_email,
				message
			)
				VALUES
			(
				%d,
				NOW(),
				"%s",
				"%s",
				"%s"
			)
		',
		DB::quote($this->content_table.'_posts'),
		(int)$this->topic_id,
		DB::quote($name),
		DB::quote($email),
		DB::quote($message)
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($res, E_USER_ERROR);
		}

		// New post added, set data in forum table
		$sql = sprintf('
			UPDATE
				module_content_forum
			SET
				last_post_date = NOW(),
				post_count = post_count+1
			WHERE
				id = %d
			',
			$this->topic_id
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($res, E_USER_ERROR);
		}
		return true;
	}

	/*
	 * loadOverview
	 *
	 * Loads the forum topic overiew
	 *
	 * @acces protected
	 */
	protected function loadOverview(){

		if(!isset($this->config) || !is_object($this->config)){
			return false;
		}

		// Topic group
		$this->topic_group = isset($this->config->show_topic_group) ?
			$this->config->show_topic_group : $this->topic_group;

		// Set topics per page
		$this->topics_per_page = isset($this->config->topics_per_page) ?
			$this->config->topics_per_page : $this->topics_per_page;

		// Calculate offset
		$this->offset = $this->page_id * $this->topics_per_page;

		// Get topics
		$sql = sprintf('
			SELECT SQL_CALC_FOUND_ROWS
				id,
				author_name,
				author_email,
				topic,
				post_count,
				last_post_date,
				create_date
			FROM
				%s
			WHERE
				site_identifier ="%s"
			AND
				topic_group = "%s"
			ORDER BY
				create_date DESC
			LIMIT
				%d,%d

			',
			// values
			DB::quote($this->content_table),
			DB::quote(Framework::$site->identifier),
			DB::quote($this->topic_group),
			$this->offset,
			$this->topics_per_page
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)) {
			trigger_error($res, E_USER_ERROR);
		}

		// total topic number
		$this->cnt_topics = $res->total_rows;

		// Assign topics
		$this->topics = $res->getAsObjectArray();

		// Set pages
		$pages = $this->cnt_topics / $this->topics_per_page;
		$pages = (is_integer($pages)) ? ceil($pages-1) : $pages;
		for($i = 0; $i <= $pages; $i++){
			$this->pages[$i] = (isset($this->page_id) && $this->page_id == $i);
		}

		// Calculate Previous & next
		$this->next = isset($this->pages[$this->page_id+1]) ? $this->page_id+1 : null;
		$this->previous = isset($this->pages[$this->page_id-1]) ? $this->page_id-1 : null;

		return true;
	}

	/*
	 * loadDetailView
	 *
	 * Loads the posts form the topic
	 *
	 * @acces protected
	 */
	protected function loadDetailView(){
		if(!isset($this->config) || !is_object($this->config)){
			return false;
		}

		// Get topic details
		$sql = sprintf('
				SELECT
					id,
					author_name,
					author_email,
					create_date,
					topic,
					description,
					post_count
				FROM
					%s
				WHERE
					id = %d
				AND
					site_identifier="%s"
			',
			DB::quote($this->content_table),
			DB::quote($this->topic_id),
			DB::quote(Framework::$site->identifier)
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($result, E_USER_ERROR);
		}

		// Assign topic details
		if($row = $res->first()) {
			$this->author_name	= $row->author_name;
			$this->topic		= $row->topic;
			$this->description	= $row->description;
			$this->create_date	= $row->create_date;
		}

		// Set posts per page
		if(isset($this->config->posts_per_page)) {
			$this->posts_per_page = $this->config->posts_per_page;
		}

		// Calculate offset
		$this->offset = $this->page_id * $this->posts_per_page;

		// Get posts
		$sql = sprintf('
			SELECT SQL_CALC_FOUND_ROWS
				p.id,
				p.topic_id,
				p.post_date,
				p.author_name,
				p.author_email,
				p.message
			FROM
				%s p
			WHERE
				topic_id = %d
			ORDER BY
				post_date
			LIMIT
				%d,%d
			',
			// values
			DB::quote($this->content_table.'_posts'),
			(int)$this->topic_id,
			$this->offset,
			$this->posts_per_page
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($res, E_USER_ERROR);
		}

		// Total posts
		$this->cnt_posts = $res->total_rows;

		// Assign posts
		$this->posts = $res->getAsObjectArray();

		// Set pages
		$pages = $this->cnt_posts / $this->posts_per_page;
		$pages = (is_integer($pages)) ? ceil($pages-1) : $pages;
		for($i = 0; $i <= $pages; $i++){
			$this->pages[$i] = (isset($this->page_id) && $this->page_id == $i);
		}
		// Calculate Previous & next
		$this->next = isset($this->pages[$this->page_id+1]) ? $this->page_id+1 : null;
		$this->previous = isset($this->pages[$this->page_id-1]) ? $this->page_id-1 : null;

		return true;
	}

	/**
	 * Add topic or post with post data
	 */
	private function handlePostData(){

		// Handle different actions
		if(isset($this->post['action'])) {
			switch($this->post['action']) {
				case 'addTopic':
					// Check values
					$errors = array();
					$errors_by_name = array();
					if(!((current($_SESSION['CaptchaField']) == $this->post['captcha']) && (current($_SESSION['CaptchaField']) != ''))) {
						$errors_by_name['captcha'] =
						$errors[] = 'Ongeldige beveiligingscode';
					}

					if(strlen($this->post['name']) < 2) {
						$errors_by_name['name'] =
						$errors[] = 'Je hebt je naam nog niet ingegeven';
					}

					if(strlen($this->post['email']) < 2) {
						$errors_by_name['email'] =
						$errors[] = 'Je hebt je e-mail adres nog niet ingegeven';
					}
					else {
						if(!Email::isValid($this->post['email'])) {
							$errors_by_name['email'] =
							$errors[] = 'Je hebt een onjuist e-mail adres ingegeven';
						}
					}

					if(strlen($this->post['topic']) < 2) {
						$errors_by_name['topic'] =
						$errors[] = 'Je hebt nog geen onderwerp ingegeven';
					}

					if(strlen($this->post['description']) < 2) {
						$errors_by_name['description'] =
						$errors[] = 'Je hebt nog geen tekst ingegeven';
					}

					if(!$errors) {
						// Data ok, make topic
						if($this->addTopic($this->post['name'], $this->post['email'], $this->post['topic'], $this->post['description'])) {
							$this->session['message'] = 'Onderwerp toegevoegd';
							trigger_redirect($_SERVER['PHP_SELF']);
						}
					}
					else {
						// Return errors
						$this->errors = $errors;
						$this->errors_by_name = $errors_by_name;
					}

					break;

				case 'addPost':
					// Check values
					$errors = array();
					$errors_by_name = array();
					if(!((current($_SESSION['CaptchaField']) == $this->post['captcha']) && (current($_SESSION['CaptchaField']) != ''))) {
						$errors_by_name['captcha'] =
						$errors[] = 'Ongeldige beveiligingscode';
					}

					if(strlen($this->post['name']) < 2) {
						$errors_by_name['name'] =
						$errors[] = 'Je hebt je naam nog niet ingegeven';
					}

					if(strlen($this->post['email']) < 2) {
						$errors_by_name['email'] =
						$errors[] = 'Je hebt je e-mail adres nog niet ingegeven';
					} else {
						if(!Email::isValid($this->post['email'])) {
							$errors_by_name['email'] =
							$errors[] = 'Je hebt een onjuist e-mail adres ingegeven';
						}
					}

					if(strlen(trim($this->post['message'])) < 2) {
						$errors_by_name['message'] =
						$errors[] = 'Je hebt nog geen tekst ingegeven';
					}

					if(!$errors) {
						// Data ok, make topic
						if($this->addPost($this->post['name'], $this->post['email'], $this->post['message'])){
							$this->session['message'] = 'Post toegevoegd';
							trigger_redirect($_SERVER['PHP_SELF'].'?'.$this->qsPart('topic_id').'='.$this->topic_id.'&'.$this->qsPart('page_id').'='.$this->page_id);
						}
					}
					else {
						// Return errors
						$this->errors = $errors;
						$this->errors_by_name = $errors_by_name;
					}
					break;
				default:
					die('default action');
			}
		}
		else {
			$this->message = !empty($this->session['message']) ?
				$this->session['message'] : null;
			unset($this->session['message']);
		}
	}

	/**
	 * _get_return_page_id
	 *
	 * genereert het pagina nummer waar het huidige artikel opstaat
	 *
	 * @access private
	 * @param Int: concerned article id
	 * @param Int: has the number of viewable items on a page
	 * @param Array: holding the id's of the current viewable articles
	 */
	private function _get_return_page_id($article_id, $items_per_page, $list)
	{
		// get arraykey of current article
		$option = array_search($article_id, $list);

		// deel het aantal artikelen op een pagina door de key van het huidge artikel
		return $option ? (int) floor($option/$items_per_page) : 0;
	}

	/**
	 * getArticleIds()
	 *
	 * get all the id's that are showed on the overview page,
	 * needed for the return page id
	 *
	 * @param: Int get id's from this group
	 */
	public function getArticleIds($group = null){


		// get ids from specific group?
		$sql_extra = '';

		if(isset($group)) {
			$sql_extra = sprintf('WHERE topic_group = %d', $group);
		}

		$sql = sprintf('
			SELECT
				id
			FROM
				%s
			%s
			ORDER BY
				create_date DESC
			',
			DB::quote($this->content_table),
			$sql_extra
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($res, E_USER_ERROR);
		}

		$ids = array();
		foreach($res as $row) {
			$ids[] = $row->id;
		}

		return $ids;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
