<?php
require_once INCLUDE_DIR . 'classes/Form.package.php';
require_once INCLUDE_DIR . 'classes/AvoidForm.package.php';

/**
 * E-mail Formulier Module
 *
 * @author         Unknown
 * @author         Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 * @lastmod        26 jan 2009
 * @copyright      Web Power
 *
 * @package        Brickwork
 * @subpackage     Module
 */
class EmailFormulierModule extends Webmodule
{
    /**
     * The form
     *
     * @var    AvoidForm
     */
    public $form;

    /**
     * The name of the sender
     *
     * @var    String
     */
    private $_sender_name;

    /**
     * The Email address of the sender
     *
     * @var    Email
     */
    private $_sender_email;

    /**
     * Errors
     *
     * @var    Array
     */
    private $_errors = array();

    /**
     * The recipients of the form
     *
     * @var    Array
     */
    private $_recipients = array();

    /**
     * The folder for the uploads
     *
     * @var    String
     */
    private $_upload_folder;

    /**
     * Constructor
     *
     * @param    Integer          $id
     * @param    ContentContainer $cc
     *
     * @return    void
     */
    public function __construct($id, ContentContainer $cc)
    {
        parent::__construct($id, $cc);

        $this->_sender_name = Framework::$site->name;

        // ff e-mailadres van organisatie checken
        if (is_null(Framework::$site->organisation) || empty(Framework::$site->organisation->email_info->address)) {
            trigger_error('Voor dit contactformulier dient het organisatorisch e-mailadres te zijn ingevuld, dat is nu niet het geval', E_USER_ERROR);
        }

        $this->_sender_email = new Email(Framework::$site->organisation->email_info->address);

        $this->_upload_folder = UPLOAD_DIR . "FILES/EmailFormUpload/";
    }

    /**
     * Magic Getter
     *
     * @param    String $name
     *
     * @return    MixedVar
     */
    public function __get($name)
    {
        switch ($name) {
            case 'senderName' :
                return $this->_sender_name;

            case 'senderEmail' :
                return $this->_sender_email;

            case 'hasErrors' :
                return count($this->form->errors) > 0;

            case 'recipients' :
                return (is_array($this->_recipients) ? (array)$this->_recipients : $this->_recipients);

            default:
                throw new AttributeNotFoundException(sprintf('Attribute %s not found', $name));
                break;
        }
    }

    /**
     * Magic Setter
     *
     * @param    String   $name
     * @param    MixedVar $value
     *
     * @return    void
     */
    public function __set($name, $value)
    {
        switch ($name) {
            case 'senderName' :
                $this->_sender_name = $value;
                break;

            case 'senderEmail' :
                $this->_sender_email = $value;
                break;

            case 'recipients' :
                $this->_recipients = $value;
                break;

            default:
                /*
                try {
                    return parent::__set($name, $value);
                } catch (AttributeNotFoundException $e) {
                    throw $e;
                }
                break;
                */
        }
    }

    /**
     * (non-PHPdoc)
     * @see classes/Module#loadData()
     */
    protected function loadData()
    {
        if (!isset($this->config->form_code)) {
            return false;
        }

        $this->form = new AvoidForm($this->config->form_code, '?');

        if (file_exists($this->_upload_folder) && is_dir($this->_upload_folder)) {
            $this->form->setUploadFolder($this->_upload_folder);
        } else {
            $this->form->setUploadFolder(null);
        }

        // Voorbereiding van de omschakeling van form code naar form id koppeling
        $form_code = isset($this->config->form_code) ? $this->config->form_code : null;
        $form_id = isset($this->config->form_id) ? (int)$this->config->form_id : null;

        $sql = sprintf('
			SELECT
				title,
				description,
				header,
				footer,
				notify_emailaddresses
			FROM
				brickwork_form
			WHERE
				site_identifier = "%s"
			AND %s
			LIMIT 1
		', DB::quote(Framework::$site->identifier), (is_null($form_id) ? 'code' : 'id') . " = " . DB::quote(is_null($form_id) ? $form_code : $form_id, true));

        $res = DB::iQuery($sql);

        $emailaddresses = array();

        if (!self::isError($res) && $res->num_rows == 1) {
            $row = $res->first();

            $this->form->title = $row->title;
            $this->form->description = $row->description;
            $this->form->header = $row->header;
            $this->form->footer = $row->footer;

            $emailaddresses = preg_split('/[\s;]+/s', $row->notify_emailaddresses);
        } # ^- Wil en Jur vinden dit niet netjes! -> nu wel hopelijk met s-modifier en geen newlines en formfeeds er meer in (Mick 1/16/2007)

        $es = array();
        foreach ($emailaddresses as $e) {
            if (empty($e)) {
                continue;
            }

            if (Email::isValid($e)) {
                $es[] = new Email($e);
            } else {
                trigger_error(sprintf('%s->%s Recipient email: %s is not valid', __CLASS__, $this->name, $recipient), E_USER_NOTICE);
            }
        }

        $this->form->emailaddresses = $es;

        if (!empty($this->config->sender_name)) {
            $this->senderName = $this->config->sender_name;
        }

        if (!empty($this->config->sender_email) && Email::isValid($this->config->sender_email)) {
            $this->senderEmail = new Email($this->config->sender_email);
        }

        /**
         * Recipients uit tekstveld lezen
         */
        $recipients = preg_split("/,|;|\s/is", $this->config->email_recipients);

        $r = array();
        foreach ($recipients as $rec) {
            $tmp = trim($rec);

            if (empty($tmp)) {
                continue;
            }

            if (!empty($tmp) && Email::isValid($tmp)) {
                $r[] = new Email($tmp);
            } else {
                trigger_error(sprintf('%s->%s Recipient email: \'%s\' is not valid', __CLASS__, $this->name, $rec), E_USER_NOTICE);
            }
        }

        $this->recipients = $r + $this->form->emailaddresses;
        $this->form->load();

        $downloadstructureid = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($this->form->handleRequest($_POST)) {
                // build message for e-mail

                // sleutels voor velden bepalen
                $_keys = array_keys($this->form->fields);

                // Alle e-mail adressen in het formulier
                $formEmail = Array();

                // alle data zit in fields
                $data = '';
                $uploadedData = '';
                $uploadedDataFeedback = '';
                foreach ($this->form->fields as $field) {
                    if ($field instanceOf CaptchaField) {
                        continue;
                    }

                    if ($field instanceOf MailSelectField) {
                        if (is_array($field->value)) {
                            foreach ($field->value as $val) {
                                // strip md5 hash in front of the value (which makes him unique)
                                if (eregi('\|', $val)) {
                                    $val = substr($val, strpos($val, '|'));
                                }
                                if (Email::isValid(base64_decode($val))) {
                                    $this->_recipients[] = new Email(base64_decode($val));
                                }
                            }
                        }
                        $data .= sprintf("%s: %s\n", $field->label, str_replace("\r", '', $field->selectedValue));
                        continue;
                    }

                    // PE: Toevoeging voor reply-to
                    if ($field instanceOf EmailField) {
                        if (Email::isValid($field->value)) {
                            array_push($formEmail, new Email($field->value));
                        }
                    }

                    if ($field instanceOf SelectField) {
                        if (is_array($field->value)) {
                            $data .= sprintf("%s: %s\n", $field->label, str_replace("\r", '', implode(", ", $field->value)));
                            continue;
                        }
                    }

                    if ($field instanceof FileUploadField) {
                        if (is_null($downloadstructureid)) {
                            $sql = sprintf("SELECT
										ss.id
									FROM
										brickwork_site_structure ss
									INNER JOIN
										brickwork_page p ON (p.site_structure_id = ss.id)
									INNER JOIN
										brickwork_page_content pc ON (pc.page_id = p.id)
									INNER JOIN
										brickwork_module bm ON (bm.code = pc.module_code)
									WHERE
										bm.class = 'FormulierDownloadModule'
									AND
										ss.site_identifier = '%s'
									LIMIT 1", DB::quote(Framework::$site->identifier));

                            $res = DB::iQuery($sql);

                            if (self::isError($res)) {
                                $downloadstructureid = 0;
                                continue;
                            } else {
                                if ($res->numRows == 0) {
                                    $downloadstructureid = 0;
                                    continue;
                                }
                            }
                            $downloadstructureid = (int)$res->first()->id;
                        }
                        if ($field->value['error'] == 0 && !is_null($downloadstructureid) && $downloadstructureid != 0) {
                            $code = substr(md5(time() . $field->value['name'] . $_SERVER['HTTP_USER_AGENT']), 10, 10);
                            rename(UPLOAD_DIR . "FILES/EmailFormUpload/" . $field->value['name'], UPLOAD_DIR . "FILES/EmailFormUpload/" . $code . "_" . $field->value['name']);
                            $hashcode = substr(md5($field->value['name'] . $field->value['size'] . time()), 5, 10);
                            $sql = sprintf("INSERT INTO brickwork_avoid_form_upload (code, site_identifier, filename, hashcode, filesize) VALUES (%s, %s, %s, %s, %d)", DB::quote($code, true), DB::quote(Framework::$site->identifier, true), DB::quote($field->value['name'], true), DB::quote($hashcode, true), DB::quote((int)$field->value['size']));
                            $res = DB::iQuery($sql);
                            if (self::isError($res)) {
                                if (TESTMODE) {
                                    throw new Exception("Query " . $sql . " failed. " . $res->errstr);
                                }
                                continue;
                            }
                            $download_id = $res->lastInsertId;
                            $uploadedData .= sprintf("%s:\n  Bestandsnaam: %s\n  Bestandsgrootte: %d bytes\n  Te downloaden via: http://%s/page/%s?mod[FormulierDownloadModule][fid]=%d&mod[FormulierDownloadModule][fcode]=%s\n  Gebruik als toegangscode: %s\n", $field->label, $field->value['name'], $field->value['size'], (TESTMODE ? $_SERVER['HTTP_HOST'] : Framework::$site->host), SiteStructure::getPathByStructureId($downloadstructureid, Framework::$site->identifier), urlencode($download_id), urlencode($code), $hashcode);
                            $uploadedDataFeedback .= sprintf("%s: %s\n", $field->label, $field->value['name']);
                        }
                        continue;
                    }
                    $data .= sprintf("%s: %s\n", $field->label, str_replace("\r", '', $field->value));
                }

                // start storing to database csv table
                $ARR_hash = array();
                foreach ($this->form->fields as $field) {
                    $ARR_hash[] = $field->name;
                }
                $hash = md5(implode('_', $ARR_hash));

                if (isset($this->config->store_submissions) && $this->config->store_submissions == 1) {
                    // Sloop bepaalde velden van een uploadveld er uit.
                    foreach ($this->form->fields as $field) {
                        if ($field instanceOf FileUploadField) {
                            $value = $field->value;
                            if (is_array($value)) {
                                unset($value['tmp_name']);
                                unset($value['type']);
                                unset($value['formname']);
                                unset($value['error']);
                            }
                            $field->value = $value;
                        }
                    }

                    $sql = sprintf("
							INSERT INTO
								brickwork_avoid_form_submission
								(insert_date, form_code, hash, ip,
								fields_object, form_name, url)
							VALUES
								('%s','%s','%s',INET_ATON('%s'),'%s','%s', '%s')
							", date('Y-m-d H:i:s', PHP_TIMESTAMP), DB::quote($this->form->name), DB::quote($hash), DB::quote($_SERVER['REMOTE_ADDR']), DB::quote(gzcompress(serialize($this->form->fields), 9)), DB::quote($this->form->title), DB::quote($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']));
                    $res = DB::iQuery($sql);
                }

                // start mailing the data

                $msg = '';
                $msg .= isset($this->config->email_header) && strlen($this->config->email_header) > 0 ? $this->config->email_header . '<br />' : '';
                $msg .= $data . $uploadedData;
                $msg .= isset($this->config->email_footer) && strlen($this->config->email_footer) > 0 ? '<br />' . $this->config->email_footer : '';

                $teller = 0;
                if (count($this->recipients) > 0) {
                    foreach ($this->recipients as $recipient) {
                        // Set header
                        $headers = array(
                            'Errors-To'    => $this->_sender_email->address,
                            'X-Priority'   => 3,
                            'From'         => $this->_sender_name . '<' . $this->_sender_email->address . '>',
                            'To'           => $recipient,
                            'Subject'      => 'Verzonden formulier: ' . $this->form->title,
                            'Content-Type' => 'text/html',
                            "X-Mailer"     => false,
                        );

                        // Zet reply-to (en zo mogelijk from adres) op eerste e-mail adres
                        if (count($formEmail) > 0) {
                            $headers['Reply-To'] = first($formEmail);
                            if (isset($this->config->use_from) && $this->config->use_from) {
                                $headers['From'] = first($formEmail)->address;
                            }
                        }

                        if (!$this->_mailForm($this->_sender_email, $this->_sender_name, $recipient, $headers, nl2br($msg))) {
                            error_log('Sending of email to one ore more recpients failed. Maybe there\'s something wrong', 3, TMP_DIR . '/error.log');
                        } else {
                            $teller++;
                        }
                    } // foreach recipients

                    if ($teller == 0) {
                        $this->form->error = new ErrorVoedingonline(E_USER_WARNING, 'Sending of email failed.');
                    }

                } // count recpients

                // Toevoeging PE 2008-08-18
                // FO: Uitbreiding formulieren module
                if (isset($this->config->send_feedback) && $this->config->send_feedback && count($formEmail) > 0) {
                    $msg = $this->config->feedback_message . "\n";
                    if (isset($this->config->feedback_has_formfields) && $this->config->feedback_has_formfields) {
                        $msg .= $data . $uploadedDataFeedback;
                    }
                    // Don't send any empty messages
                    if (trim($msg)) {
                        $teller = 0;
                        foreach ($formEmail as $recipient) {
                            $headers = array(
                                'Errors-To'    => $this->_sender_email->address,
                                'X-Priority'   => 3,
                                'From'         => $this->_sender_name . '<' . $this->_sender_email->address . '>',
                                'To'           => $recipient,
                                'Subject'      => 'Verzonden formulier: ' . $this->form->title,
                                'Content-Type' => 'text/html',
                                "X-Mailer"     => false,
                            );

                            if (!$this->_mailForm($this->_sender_email, $this->_sender_name, $recipient, $headers, nl2br($msg))) {
                                error_log("Sending of feedback email to $recipient failed. Maybe there\'s something wrong", 3, TMP_DIR . '/error.log');
                            } else {
                                $teller++;
                            }

                        }

                        if ($teller != count($formEmail)) {
                            $this->form->error = new ErrorVoedingonline(E_USER_WARNING, 'Sending of email failed.');
                        }
                    }
                }

                // trigger final event
                if (count($this->form->errors) == 0) {
                    $this->form->onSuccessHandleRequest();
                }
            }
        }

        // alles goed, return true
        return true;
    }

    /**
     * _mailForm result
     *
     * @param    Email  $from_mail
     * @param    String $from_name
     * @param    Email  $recipient_mail
     * @param    Array  $headers
     * @param    String $message
     *
     * @return    Boolean
     */
    protected function _mailForm(Email $from_mail, $from_name, Email $recipient_mail, $headers, $message)
    {
        $SMTP = new SMTP();
        if (!$SMTP->connect()) {
            return false;
        }

        // zenden
        if (Utility::isUTF8($message)) {
            $message = utf8_decode($message);
        }

        return $SMTP->send($from_mail->address, $recipient_mail->address, $headers, $message);
    }

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
