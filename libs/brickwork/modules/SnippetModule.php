<?php
/**
 * Snippet Module
 * 
 * http://en.wikipedia.org/wiki/Snippets
 * 
 * Can be used for pieces of code that are often used on pages.
 * Like a footer HTML text or Google Analytics script stuff
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 0.0.2
 * 
CREATE TABLE `module_content_snippet` (                  
	`id` int(10) unsigned NOT NULL auto_increment,         
	`site_identifier` char(15) default NULL,               
	`name` varchar(20) NOT NULL,                           
	`title` varchar(255) default '',                       
	`code` text NOT NULL,                                  
	PRIMARY KEY  (`id`)                                    
);
                        
CREATE TABLE `module_config_snippet` (
	`page_content_id` int(10) unsigned NOT NULL default '0',  
	`snippet_id` int(10) unsigned NULL,
	`snippet_name` varchar(20) NOT NULL,
	PRIMARY KEY  (`page_content_id`)
);
 */

class SnippetModule extends Webmodule
{
	/**
	 * The default content table used for snippets
	 * @var string
	 */
	public static $default_content_table = 'module_content_snippet';
	
	/**
	 * The snippet if one is configured
	 *
	 * @var stdClass
	 */
	public $snippet;

	/**
	 * (non-PHPdoc)
	 * @see Module#loadData()
	 */
	public function loadData()
	{
		if(!empty($this->config->snippet_name)) {
			$this->snippet = self::getSnippetByName($this->config->snippet_name);
		}
		else if(!empty($this->config->snippet_id)) {
			$this->snippet = self::getSnippet($this->config->snippet_id, !empty($this->content_table) ? $this->content_table : null);
		}
		return true;
	}
	
	/**
	 * (decrepated) Get a Code Snippet by Id
	 *
	 * @throws Exception
	 * @param int $id
	 * @return stdClass|bool
	 */
	public static function getSnippet( $id, $content_table = null)
	{
		if(is_null($content_table)) $content_table = self::$default_content_table;
		
		return DB::iQuery(sprintf("
			SELECT
				`id`,
				`name`,
				`site_identifier`,
				`title`,
				`mod_datetime`,
				`code`
			FROM
				`%s`
			WHERE
				`id` = %d
			LIMIT 1
			",
			$content_table,
			$id
		))->first();
	}
	
	/**
	 * Get a Snippet by its Name
	 * 
	 * @throws Exception
	 * @param string	$name
	 * @param string $content_table
	 * @return stdClass|bool
	 */
	public static function getSnippetByName($name, $site_identifier = null, $content_table = null)
	{
		if(is_null($site_identifier)) $site_identifier = Framework::$site->identifier; 
		if(is_null($content_table)) $content_table = self::$default_content_table;
		
		return DB::iQuery(sprintf("
			SELECT
				`id`,
				`name`,
				`site_identifier`,
				`title`,
				`mod_datetime`,
				`code`
			FROM
				`%s`
			WHERE
				`name` = %s
			AND
				(`site_identifier` = %s OR `site_identifier` IS NULL)
			ORDER BY `site_identifier` DESC
			LIMIT 1
			",
			$content_table,
			DB::quoteValue($name),
			DB::quoteValue($site_identifier)
		))->first();
	}
	
	public static function getSnippetModDatetimeByName($name, $site_identifier = null, $content_table = null)
	{
		if(is_null($site_identifier)) $site_identifier = Framework::$site->identifier; 
		if(is_null($content_table)) $content_table = self::$default_content_table;
		
		return DB::iQuery(sprintf("
			SELECT
				`mod_datetime`
			FROM
				`%s`
			WHERE
				`name` = %s
			AND
				(`site_identifier` = %s OR `site_identifier` IS NULL)
			ORDER BY `site_identifier` DESC
			LIMIT 1
			",
			$content_table,
			DB::quoteValue($name),
			DB::quoteValue($site_identifier)
		))->first();
	}
}