<?php

/* 
 *  Config table SQL 
 * 
CREATE TABLE `module_config_youtube_feed` (
  `page_content_id` int(10) NOT NULL default '0',
  `display_mode` varchar(10) NOT NULL default 'movie',
  `item_amount` int(11) NOT NULL default '5',
  `movie_url` varchar(200) default NULL,
  `feed_url` varchar(200) default NULL,
  `cache_time` int(11) NOT NULL default '0',
  `width` int(11) NOT NULL default '425',
  `height` int(11) NOT NULL default '350',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
 */

/**
 * YouTube Module
 * 
 * Displays youtube feed
 * 
 * @author Adrie den Hartog <adrie.denhartog@webpower.nl>
 */
class YouTubeFeedModule extends Webmodule {

	/*
	* @var string
	* @access private
	*/
	private $_config_displayMode;

	/*
	* @var int
	* @access private
	*/
	private $_config_itemAmount;

	/*
	* @var string
	* @access private
	*/
	private $_config_movieURL;

	/*
	* @var string
	* @access private
	*/
	private $_config_feedURL;

	/*
	* @var int
	* @access private
	*/
	private $_config_feed_cachetime;

	/*
	* @var array
	* @access private
	*/
	private $_data_feedItems;

	/*
	* @var int
	* @access private
	*/
	private $_data_feedAmount;

	/*
	* @var int
	* @access private
	*/
	private $_config_width;

	/*
	* @var int
	* @access private
	*/
	private $_config_height;

	/*
	* @var boolean
	* @access private
	*/
	private $_cache_used;

	/*
	* @var boolean
	* @access private
	*/
	private $_store_cache_file;

	public function __construct($id, $cc){
		parent::__construct($id, $cc);
	}

    public function __get($value){
		switch($value){
			case 'displayMode':
				return $this->_config_displayMode;	
				break;	
			case 'itemAmount':
				return $this->_config_itemAmount;	
				break;	
			case 'movieURL':
				return $this->_config_movieURL;	
				break;	
			case 'feedURL':
				return $this->_config_feedURL;	
				break;	
			case 'cacheTime':
				return $this->_config_feed_cachetime;	
				break;	
			case 'feedItems':
				return $this->_data_feedItems;	
				break;	
			case 'feedAmount':
				return $this->_data_feedAmount;	
				break;	
			case 'width':
				return $this->_config_width;	
				break;	
			case 'height':
				return $this->_config_height;	
				break;	
			default:
				trigger_error('Deze Get-waarde bestaat niet: --> '.$value);
		}
	}

	protected function loadData(){

        // fetch config settings
		$this->_config_displayMode = (isset($this->config->display_mode)) ? $this->config->display_mode : NULL;
		$this->_config_itemAmount = (isset($this->config->item_amount)) ? (int)$this->config->item_amount : 5;
		$this->_config_movieURL = (isset($this->config->movie_url)) ? $this->config->movie_url : NULL;
		$this->_config_feedURL = (isset($this->config->feed_url)) ? $this->config->feed_url : NULL;
		$this->_config_feed_cachetime = (isset($this->config->cache_time)) ? (int)$this->config->cache_time : NULL;
		$this->_config_width = (isset($this->config->width)) ? (int)$this->config->width : NULL;
		$this->_config_height = (isset($this->config->height)) ? (int)$this->config->height : NULL;

		if ($this->_config_displayMode == "rss" && $this->_config_feedURL) {

			// set the name for a cachefile, always set it. even when not used.
			$this->_cache_file = realpath('..')."/tmp/youtube_mod_".$this->id.".cache";

			// handle the cache logic here:
			// check if we're supposed to look for a cachefile
			if ($this->_config_feed_cachetime && file_exists($this->_cache_file)) {
				// if the last modification time is younger than the set cache time in seconds..
				if ((time()-filemtime($this->_cache_file)) < ($this->_config_feed_cachetime*60)) {
					// ..use the cachefile
					$this->_config_feedURL = $this->_cache_file;
					$this->_cache_used = true;
					$this->_store_cache_file = false;
				}
				else {
					// set a flag to warn the coming script that it needs to store it's retreived data in cache
					$this->_store_cache_file = true;
				}
			}
			// If we're supposed to use the cache but it isn't there yet: create it.
			elseif ($this->_config_feed_cachetime && !file_exists($this->_cache_file)) {
				$this->_store_cache_file = true;
			}
			// If set to zero and there still is a cache, delete it. Handy for resetting the cache as well.
			elseif (!$this->_config_feed_cachetime && file_exists($this->_cache_file)) {
				unlink($this->_cache_file);
			}

			// retreive items
			if ($this->_config_feedURL) {

				// Check if we need to store this info
				if ($this->_store_cache_file) {
					$handle = fopen($this->_cache_file, "w");
					if (fwrite($handle, file_get_contents($this->_config_feedURL)) === FALSE) {
					   echo "Cannot write to file (".$this->_cache_file.")";
					   exit;
					}
					fclose($handle);
				}

				// Initialize SimpleXML
				$xml = simplexml_load_file($this->_config_feedURL);

				// Parse it as RSS2
				$this->_data_feedItems = self::getFeedItems_RSS2($this->_config_itemAmount, $xml, 0);

				// For info:
				// Returned array is: (description, link, title)

				if (isset($this->_data_feedItems['amount'])) {
					$this->_data_feedAmount = $this->_data_feedItems['amount'];
					unset($this->_data_feedItems['amount']);
				}

				// Set $this->_config_movieURL to the first out of the parsed array (unless a noscript clip request comes by)
				if (isset($this->get['clip'])) {
					$this->_config_movieURL = $this->get['clip'];
				} else {
					$this->_config_movieURL = $this->_data_feedItems[1]['preview'];
				}

			}			

		} elseif ($this->_config_displayMode == "movie" && $this->_config_movieURL) {

			// Do nothing, the template will pick this up
		
		} else {
		
			// No mode set or an url is missing.. Exit with failure.
			return false;

		}
		
		return true;
	}

	/**
	* Retreive an x amount of rss feed items for display [RSS 2.0]
	*/
	public static function getFeedItems_RSS2 ($amount, $xml, $pageOffset) {

		// Init array
		$itemArray = array();

		// $i is used for count, $cnt for the offset
		$i=0;
		$cnt=0;

		// Loop though
		foreach($xml->channel->item as $key=>$item) {
			$cnt++;
			if ($cnt > $pageOffset) {
				++$i;
				if($i == ($amount+1)) {	break; }
				$itemArray[$i]['description'] = utf8_decode((string) $item->description); // Only in RSS2, add to others..
				$itemArray[$i]['link'] = (string) $item->link;
				$itemArray[$i]['title'] = utf8_decode((string) $item->title);
				
				// The following is optional only for videostreams..
				if (isset($item->enclosure['url'])) {
					$itemArray[$i]['preview'] = (string) $item->enclosure['url'];
				}
				if (isset($item->video['encoding'])) {
					$itemArray[$i]['mime'] = (string) $item->video['encoding'];
					if ($itemArray[$i]['mime'] == "") {
						$itemArray[$i]['mime'] = "automatic";
					}
				}
			}
	    }

		$itemArray['amount'] = count($xml->channel->item);

		return $itemArray;
	}

}

?>