<?php
/**
 * Google Maps Module
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @since 2009-04-07
 * @copyright Web Power, http://www.webpower.nl
 *
 CREATE TABLE `brickwork_googlemaps_host_api` (
 `http_host` varchar(255) NOT NULL,
 `api_key` varchar(255) NOT NULL,
 PRIMARY KEY  (`http_host`)
 ) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_google_maps` (
`page_content_id` int(10) unsigned NOT NULL default '0',
`maptype` enum('NORMAL','HYBRID','SATELLITE') NOT NULL default 'NORMAL',
`zoomsize` enum('Small','Large') NOT NULL default 'Small',
`wheel_zoom` tinyint(1) default NULL,
`width` int(4) default NULL,
`height` int(4) NOT NULL default '500',
`cannot_connect` varchar(255) NOT NULL default '<p>Google Maps</p><p>Momenteel niet bereikbaar.</p>',
PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_google_maps` (
`id` int(10) unsigned NOT NULL auto_increment,
`site_identifier` varchar(20) NOT NULL default '',
`page_content_id` int(10) unsigned NOT NULL default '0',
`position` varchar(200) default NULL,
`title` varchar(255) default NULL,
`body` text,
`mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
`mod_phoundry_user_id` int(10) unsigned default NULL,
PRIMARY KEY  (`id`),
KEY `site_pcid` (`site_identifier`(3),`page_content_id`)
) DEFAULT CHARSET=utf8;
 */
class GoogleMapsModule extends Webmodule
{

    /**
     * API key, as set within the brickwork_googlemaps_host_api table.
     * Get your own @ http://code.google.com/apis/maps/signup.html
     * @var string
	 */
	public $api_key;

	/**
	 * Array with markers
	 * @var array
	 */
	public $markers;
	
	/**
	 * (non-PHPdoc)
	 * @see Module#loadData()
	 */
	protected function loadData()
	{

		$this->api_key = self::getApiKeyByHost($_SERVER['HTTP_HOST'], ($_SERVER["SERVER_PORT"]=='443') ? 'https' : 'http');
		$this->markers = self::getMarkers(
			$this->id,
			!empty($this->content_table) ? $this->content_table : 'module_content_google_maps'
		);
		
		return true;
	}

	/**
	 * Get markers for a given module
	 * @param int $module_id Module id
	 * @param string $table Tablename of the content
	 * @return array
	 */
	public static function getMarkers($module_id, $table = 'module_content_google_maps')
	{
		$markers = array();
		$res = DB::iQuery(sprintf("
			SELECT
				content.`position`,
				content.`title`,
				content.`body`
			FROM
				`%s` AS content
			WHERE
				content.`page_content_id` = %d
			",
			$table,
			$module_id
		));
		
		foreach($res as $row)
		{
			$latlng = explode(',', $row->position);
			
			// Only add the marker if it contains a latitude and a longitude
			if(2 == count($latlng))
			{
				$markers[] = array(
					'lat' => $latlng[0],
					'lng' => $latlng[1],
					'textmsg' => $row->body
				);
			}
		}
		return $markers;
	}
	
	/**
	 * Get a GoogleMaps API key for the specified host
	 * @param string $host
	 * @param string $protocol http|https
	 * @return string|bool
	 */
	public static function getApiKeyByHost ($host, $protocol = 'http')
	{
		$res = DB::iQuery(sprintf('
			SELECT
				`api_key`
			FROM
				`brickwork_googlemaps_host_api`
			WHERE
				`http_host` = "%s"
			LIMIT 1',
			DB::quote($protocol.'://'.trim(strtolower($host)))
		));
		return ($row = $res->first()) ? $row->api_key : false;
	}

}
