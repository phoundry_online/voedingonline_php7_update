<?php
/*
* HTdigModule
*
* @author Remco Bos <remco@webpower.nl>
* @last_modified 15/11/2006
* @version
*/
class HTdigModule extends Webmodule {
	/*
	* @var array
	* @acces public
	*/
	public $items = array();

	/*
	* @var array
	* @acces public
	*/
	public $valid_matchesperpage = array('5', '10', '15', '20');

	/*
	* @var integer
	* @acces public
	*/
	public $matches	= 0;

	/*
	* @var string
	* @acces public
	*/
	public $view = 'overview';

	/*
	* @var string
	* @access private
	*/
	private $default_matches_per_page = 10;

	/*
	* Constructor
	*
	* @param integer $id
	* @param object $cc
	*
	* @acces public
	*/
	public function __construct($id, ContentContainer $cc){
		parent::__construct($id, $cc);
	}

	/*
	* LoadData
	*
 	* Handles different types of data and function. Function must return TRUE to succesfully load the current module.
	*
	* @return boolean
	*/
	protected function loadData() {

		// Decide view
		switch($this->contentContainer->code) {

			case 'cc_right':
			case 'cc_small':
				$this->view = 'miniview';
				break;
			case 'cc_quicknav':
				$this->view = 'headerview';
				break;

			case 'cc_content':
			default:
				$this->view = 'overview';
		}

		// Decide wich configuration file to use (htdig.CONFIG_NAAM.conf
		$config = !empty($this->config->htdig_config)  ? $this->config->htdig_config : 'daily';

		// Check for items_page_page in config. If not set, make default to avoid nasty errors
		$this->config->items_per_page = isset($this->config->items_per_page) ? $this->config->items_per_page : $this->default_matches_per_page;

		// Get matchesperpage
		$this->matchesperpage = isset($this->get['matchesperpage']) ? $this->get['matchesperpage'] : $this->config->items_per_page;

		// Get method
		$this->method = !empty($this->get['method']) ? $this->get['method'] : 'and';

		// Make instance if HTdig
		$this->htdig = new HTdig($config, $this->method, $this->matchesperpage);

		// Get searchterm
		$this->get['words'] = !empty($this->get['words']) ? $this->get['words'] : NULL;

		// Decide current page
		$this->htdig->cur_page = isset($this->get['page']) ? $this->get['page'] : $this->htdig->cur_page;

		// If there's a searchtem, dig!
		if(!is_null($this->get['words'])) {
			$res = $this->htdig->doDig($this->get['words'], $this->htdig->cur_page);
		}

		// Validate
		if(self::isError($res)){
			// Catch error
			if($res->errstr != 'HTdig syntax error'){
				trigger_error($res->errstr, $res->errno);
			}
		}

		// Previous
		$this->previous = ($this->htdig->cur_page > 1) ? $this->htdig->cur_page -1 : NULL;

		// Next
		$this->next = ($this->htdig->nr_pages > $this->htdig->cur_page) ? $this->htdig->cur_page + 1 : NULL;

		// Matches
		if ($this->htdig->matches > 0) {
			$this->matches = $this->htdig->matches;
			$this->items = $res;
		}

		// Return
		return TRUE;
	}

	/*
	* getNextUrl
	*
	* Calculates the next url
	*
	* @return string part of query string
	*/
	public function getNextUrl(){
		$tmp = array();
		$page = $this->htdig->cur_page + 1;
		$args = $this->htdig->getQSarray($page);
		foreach($args as $key=>$arg){
			$tmp[] = $this->qsPart($key).'='.urlencode($arg);
		}
		return implode('&amp;', $tmp);
	}

	/*
	* getPreviousUrl
	*
	* Calculates the previous url
	*
	* @return string part of query string
	*/
	public function getPreviousUrl(){
		$tmp = array();
		$page = $this->htdig->cur_page -1;
		$args = $this->htdig->getQSarray($page);
		foreach($args as $key=>$arg){
			$tmp[] = $this->qsPart($key).'='.urlencode($arg);
		}
		return implode('&amp;', $tmp);
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

?>
