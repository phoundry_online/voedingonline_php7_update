<?php
/**
 * News Module
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 0.0.2
 * @package News
 *  
 */
class BrickworkNewsModule extends Webmodule
{
	/**
	 * Id of the sitestructure containing the detailpage
	 *
	 * @var string
	 */
	public $detail_page;
	
	public $reactions = null;
	
	public $errors = array();
	
	/**
	 * The configured view
	 *
	 * @var string
	 */
	public $view = null;
	
	/**
	 * Pagination for the articles
	 *
	 * @var Pagination
	 */
	public $page;
	
	/**
	 * The currently selected news article (null if none selected)
	 *
	 * @var News_Article
	 */
	public $article = null;
	
	/**
	 * The current selected news category (parent category if an article is selected)
	 *
	 * @var News_Category
	 */
	public $category = null;
	
	/**
	 * The articles in the current category
	 *
	 * @var ActiveRecordIterator
	 */
	public $articles = null;
	
	/**
	 * Id of the configured root category
	 *
	 * @var int
	 */
	public $root_category = null;
	
	/**
	 * Display wich articles
	 *
	 * @var int
	 */
	public $type = null;
	
	protected function loadData()
	{
		// Fetch a token using the ModuleContent handler for posting a reaction
		if(Framework::$contentHandler instanceof ModuleContentHandler && isset($this->get['get_token'])) {
			$this->session['token'] = md5(uniqid());
			die($this->session['token']);
		}
		
		// Allows loadData() overloading
		if(is_null($this->category)) {
			$this->root_category = isset($this->config->root_category) ? $this->config->root_category : 0;
			$active_category_id = !empty($this->get['c']) ? $this->get['c'] : $this->root_category;
			
			$this->category = News::categoryFactory($active_category_id);
			
			if($this->root_category != 0 && !in_array($this->root_category, $this->category->getPathIds())) {
				$this->category = News::categoryFactory($this->root_category);
			}
		}
		
		$items_pp = !empty($this->config->items_pp) ? $this->config->items_pp : 15;
		
		if(is_null($this->reactions)) {
			$this->reactions = isset($this->config->reactions) ? (bool) $this->config->reactions : false;
		}
		
		if(is_null($this->view)) {
			$this->view = isset($this->config->view) ? $this->config->view : 'mini';
		}
		
		$this->type = coalesce(
			Utility::arrayValue($this->get, 'type'),
			(isset($this->config->type) ? $this->config->type : null),
			News::RECENT
		);
		
		if($this->view == 'mini') {
			if(isset($this->config->detail_id)) {
				$this->detail_page = $this->config->detail_id;
			} else {
				$res = DB::iQuery(sprintf("
					SELECT
						brickwork_site_structure.id
					FROM
						%s AS c
					INNER JOIN
						brickwork_page_content
					ON
						c.page_content_id = brickwork_page_content.id
					INNER JOIN
						brickwork_page
					ON
						brickwork_page_content.page_id = brickwork_page.id
					INNER JOIN
						brickwork_site_structure
					ON
						brickwork_page.site_structure_id = brickwork_site_structure.id
					WHERE
						c.view = 'detail';",
					DB::quote($this->config_table)
				));
				
				if($row = $res->first()) {
					$this->detail_page = $row->id;					
				}
			}
			
			$this->detail_page = SiteStructure::getPathByStructureId($this->detail_page);
			
			$this->articles = $this->category->getAllArticles($this->type, $items_pp);
		}
		
		if($this->view == 'detail') {
			
			if(!empty($this->get['n'])) {
				$article = News::articleFactory($this->get['n']);
				if(
					$article->isLoaded() &&
					(
						$this->root_category == 0 ||
						in_array($this->root_category, $this->category->getPathIds())
					)
				){
					$this->article = $article;
					if($this->reactions) $this->reactions = (bool) $this->article['reactions'];
					
					Framework::$contentHandler->page->title .= ' - '.$this->article['title'];
					if( isset(Framework::$contentHandler->page->breadcrumb) &&
						is_array(Framework::$contentHandler->page->breadcrumb) &&
						$this->category)
					{
						foreach($this->category->getPath() as $path)
						{
							if($path['id'] == $this->root_category) continue;
							$new_crumb = null;
							$new_crumb->title = $path['name'];
							$new_crumb->link = '?'.$this->qsPart('c').'='.$path['id'];
							
							Framework::$contentHandler->page->breadcrumb[] = $new_crumb;
						}
						$new_crumb = null;
						$new_crumb->title = $this->article['title'];
						Framework::$contentHandler->page->breadcrumb[] = $new_crumb;					
					}
					
					// Post a Reaction
					if($this->reactions && !empty($this->post['reaction'])) {
						$new_reaction = ActiveRecord::factory(News::$reaction_class);
						$name	= trim($this->post['reaction']['name']);
						$email	= trim($this->post['reaction']['email']);
						$title	= trim($this->post['reaction']['title']);
						$body	= trim($this->post['reaction']['body']);
						$this->errors = array();
						
						
						if(!(
							(isset($this->session['token']) && $this->session['token'] == $this->post['captcha']) ||
							(isset($_SESSION['CaptchaField']['NewsModule_'.$this->id]) && $_SESSION['CaptchaField']['NewsModule_'.$this->id] == $this->post['captcha'])
						)){
							$this->errors[] = 'Anti spam kon niet gevalideert worden';
						}
						
						if($new_reaction->where($this->post['reaction'])->getCount(false) != 0) {
							$this->errors[] = 'Deze reactie is al geplaatst';
						}
						if(empty($name)) {
							$this->errors[] = 'Naam is niet ingevuld';
						}
						if(empty($email) || !Email::isValid($email)) {
							$this->errors[] = 'Geen geldig emailadres ingevuld';
						}
						
						if(empty($body)) {
							$this->errors[] = 'Geen reactie ingevuld';
						}
						
						if(count($this->errors) == 0) {
							$new_reaction['name'] = $name;
							$new_reaction['email'] = $email;
							$new_reaction['title'] = $title;
							$new_reaction['body'] = $body;
							$new_reaction['datetime'] = date('Y-m-d H:i:s', PHP_TIMESTAMP);
							
							$new_reaction['ipadress'] = $_SERVER['REMOTE_ADDR'];
							
							if($new_reaction->save()) {
								$this->article->add('reactions', $new_reaction);
								$this->post['reaction'] = array();
							} else {
								$this->errors[] = 'opslaan van de reactie is mislukt wegens technische fout';
							}
						}
					}
				}
			}
			
			if(!($this->article instanceof News_Article )) {
				if( isset(Framework::$contentHandler->page->breadcrumb) &&
					is_array(Framework::$contentHandler->page->breadcrumb) )
				{
					foreach($this->category->getPath() as $path)
					{
						if($path['id'] == $this->root_category) continue;
						$new_crumb = null;
						$new_crumb->title = $path['name'];
						$new_crumb->link = '?'.$this->qsPart('c').'='.$path['id'];
						
						Framework::$contentHandler->page->breadcrumb[] = $new_crumb;
					}
				}
				
				$this->page = new Pagination(
					$items_pp,
					isset($_GET['page'.$this->id]) ? $_GET['page'.$this->id] : 0,
					null,
					'page'.$this->id
				);
				
				$this->articles = $this->category->getArticles(
					$this->type,
					$this->page->limit,
					$this->page->offset,
					$total_items
				);
				$this->page->setTotalItems($total_items);
			}
		}
		
		return true;
	}
	
}
