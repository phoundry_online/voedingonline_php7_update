<?php
/**
 * File Cabinet Module
 * Check de ParelsnoerFileCabinetModule in de Parelsnoer.nl site hoe te extenden
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @package FileCabinet
 * 
 * @since 01-07-2008
 * @version 0.0.2
 * 
 * SQL Create statements
CREATE TABLE `file_cabinet_file` (
`id` int(10) unsigned NOT NULL auto_increment,
`folder_id` int(10) unsigned NOT NULL,
`user_id` int(10) default NULL,
`prio` float default NULL,
`create_datetime` datetime NOT NULL,
`size` int(10) default NULL,
`mime` varchar(80) default NULL,
`name` varchar(255) NOT NULL default '',
`filename` varchar(255) NOT NULL default '',
`description` text,
PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `file_cabinet_folder` (
`id` int(10) unsigned NOT NULL auto_increment,
`parent_id` int(10) unsigned default NULL,
`prio` float default NULL,
`create_datetime` datetime default NULL,
`name` varchar(255) NOT NULL,
`description` text,
PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `file_cabinet_module_config` (
`page_content_id` int(10) unsigned NOT NULL default '0',
`file_rootdir` varchar(255) NULL default '',
`root_folder` int(10) unsigned NOT NULL default '0',
PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `file_cabinet_right` (
  `folder_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `rights` set('read','write','delete') default NULL,
  PRIMARY KEY  (`folder_id`,`role_id`)
) DEFAULT CHARSET=latin1;
 * 
 */
class FileCabinetModule extends Webmodule
{
	/**
	 * Template data
	 * 
	 * @var array
	 */
	public $td;
	
	/**
	 * File Cabinet
	 *
	 * @var FileCabinet_Folder
	 */
	protected $active_folder = null;
	
	/**
	 * The currently logged in user
	 *
	 * @var SiteUser
	 */
	protected $site_user;
	
	/**
	 * Gets called by the PageContentHandler if no cache is found
	 *
	 * @return bool
	 */
	protected function loadData()
	{
		// Allows loadData() overloading
		if(is_null(FileCabinet::$file_rootdir)) {
			 FileCabinet::$file_rootdir =
			 	(!empty($this->config->file_rootdir) &&
			 	is_dir($this->config->file_rootdir)) ?
			 		$this->config->file_rootdir : 
			 		UPLOAD_DIR."FILES/FileCabinet";
		}
		
		$this->site_user = Auth::singleton()->user;
		$this->td['errors'] = array();
		
		// Allows loadData() overloading
		if(is_null($this->active_folder)) {
			$root_id = isset($this->config->root_folder) ? $this->config->root_folder : null;
			$active_folder_id = !empty($this->get['f']) ? $this->get['f'] : $root_id;
			$this->active_folder = FileCabinet::folderFactory($active_folder_id);
			
			// If we're not showing the full tree, and the root folder is no
			// parent of the active folder this folder is forbidden
			if($root_id != 0 && !in_array($root_id, $this->active_folder->getPathIds())) {
				return false;
			}
		}
		
		if(!empty($this->get['read']) && is_numeric($this->get['read'])) {
			$read = $this->get['read'];
		}

		// Beautifull download url's
		if($this->contentContainer->contentHandler instanceof ModuleContentHandler &&
			count($this->contentContainer->contentHandler->path) != 0
		) {
			$path = $this->contentContainer->contentHandler->path;
			if(in_array($this->id, $path) && isset($path[(array_search($this->id, $path) + 1)])) {
				$read = $path[(array_search($this->id, $path) + 1)];				
			}
		}
		
		if(isset($read) && is_numeric($read)) {
			$this->getFile($read);
		}
		
		if(!empty($this->get['delete']) && is_numeric($this->get['delete'])) {
			$status = $this->deleteFile((int) $this->get['delete']);
			
			// Deleting using the ModuleContentHandler is an ajax request
			if($this->contentContainer->contentHandler instanceof ModuleContentHandler) {
				die((int) $status);
			}
		}
		
		if(count($this->post) > 0 && isset($_FILES[get_class($this).'_'.$this->id])) {
			$status = $this->addFile($this->post, $_FILES[get_class($this).'_'.$this->id]);
			
			if(is_array($status)) {
				$this->td['errors'] = $status;
			}
			
			if($status === true) {
				trigger_redirect($_SERVER['REQUEST_URI']);
			}
		}
		
		// Set template variables
		$this->td['is_root'] = is_null($this->active_folder['id']);
		$this->td['current_folder'] = $this->active_folder;
		$this->td['folders'] = $this->active_folder->getChildren();
		$this->td['files'] = $this->active_folder->getFiles();
		$this->td['path'] = $this->active_folder->getPath();
		
		return true;
	}

	/**
	 * Save a file in the cabinet
	 *
	 * @param array $post
	 * @param array $uploaded_file the $_FILES file
	 * @return bool/array Returns either an array holding errors or the status of the save as bool
	 */
	protected function addFile($post, $uploaded_file)
	{
		$errors = array();
		
		if(!$this->active_folder->hasAccess('write', false)) {
			$errors[] = 'U bent niet bevoegd om bestanden in deze map te plaatsen';
		}
		else {
			$description = trim($post['description']);
			if(empty($description)) {
				$errors[] = 'geen beschrijving opgegeven';
			}
			else {
				if(!isset($uploaded_file['tmp_name']) || !is_uploaded_file($uploaded_file['tmp_name'])) {
					$errors[] = 'Geen bestand geselecteerd om te plaatsen';
				}
				else {
					if($uploaded_file['error'] != UPLOAD_ERR_OK) {
						switch ($uploaded_file['error']) {
							case UPLOAD_ERR_INI_SIZE:
							case UPLOAD_ERR_FORM_SIZE:
								$errors[] = 'het bestand is groter als de maximale toegestaane grootte';
							break;
							case UPLOAD_ERR_PARTIAL:
								$errors[] = 'het bestand is niet volledig geupload';
							break;
							case UPLOAD_ERR_NO_FILE:
								$errors[] = 'geen bestand geupload';
							break;
							case UPLOAD_ERR_NO_TMP_DIR:
								$errors[] = 'missing a temporary folder';
							break;
							case UPLOAD_ERR_CANT_WRITE:
								$errors[] = 'failed to write file to disk';						
							break;
							default:
								$errors[] = 'bestand kon niet worden opgeslagen vanwege een server fout';
							break;
						}
						
					}
					else {
						$new_filename = md5_file($uploaded_file['tmp_name']);
						$sub_dir = substr($new_filename, 0, 2);
						$new_filename = substr($new_filename, 2);
						if(!is_dir(FileCabinet::$file_rootdir."/".$sub_dir) &&
							!mkdir(FileCabinet::$file_rootdir."/".$sub_dir, 0777, true)) {
							trigger_error(FileCabinet::$file_rootdir."/".$sub_dir.' could not be created');
							$errors[] = 'interne server fout';
						}
						else {
							if(!move_uploaded_file($uploaded_file['tmp_name'], FileCabinet::$file_rootdir."/".$sub_dir."/".$new_filename)) {
								trigger_error('uploaded file couldn\'t be saved using move_uploaded_file');
								$errors[] = 'interne server fout';
							}
						}
					}
				}
			}
		}

		
		if(!empty($errors)) {
			return $errors;
		}
		
		
		
		$file = FileCabinet::fileFactory();
		
		$file['folder_id']		= $this->active_folder['id'];
		$file['user_id']		= $this->site_user->getAuthId();
		$file['prio'] 			= 0;
		$file['create_datetime']= date("Y-m-d H:i:s", PHP_TIMESTAMP);
		
		$file['size']			= $uploaded_file['size'];
		$file['mime']			= $uploaded_file['type'];
		$file['name']			= $uploaded_file['name'];
		$file['filename']		= $sub_dir."/".$new_filename;
		$file['description']	= $description;
		
		return $file->save();
	}
	
	/**
	 * Get the file for download dies after outputting file
	 *
	 * @param int $id File Identifier
	 */
	protected function getFile($id)
	{
		$file = FileCabinet::fileFactory($id);
		if($file->isLoaded() && $file['filename'] &&
			is_readable(FileCabinet::$file_rootdir."/".$file['filename'])) {
			
			if($file->hasAccess('read')) {
				if(!empty($file['mime'])) {
					header('Content-type: '.$file['mime']);
				}
				header('Content-Disposition: attachment; filename="'.$file['name'].'"');
				readfile(FileCabinet::$file_rootdir."/".$file['filename']);
			}
			else {
				header("HTTP/1.0 403 Forbidden", true, 403);
				echo 'Forbidden';
			}
		}
		else {
			header("HTTP/1.0 404 Not Found", true, 404);
			echo 'File Not Found';
		}
		
		exit(0);
	}
	
	/**
	 * Delete the specified file
	 *
	 * @param int $id File identifier
	 * @return bool
	 */
	protected function deleteFile($id)
	{
		$file = FileCabinet::fileFactory($id);
		if($file->isLoaded() && $file->hasAccess('delete')) {
			// Param true tells it to also delete the associated
			// file on the filesystem
			return $file->delete(true);
		}
		
		return false;
	}
	
	public function getExtensionImg(FileCabinet_File $file)
	{
		switch ($file['mime']) {
			case 'text/plain':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_txt.png';

			case 'text/csv':
			case 'application/csv':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_csv.png';
				
			case 'text/html':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_html.png';

			case 'application/zip':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_zip.png';
			
			case 'application/pdf':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_pdf.png';
			
			case 'application/msword':
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_doc.png';
			
			case 'image/jpeg':
			case 'image/gif':
			case 'image/png':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_jpeg.png';
				
			case 'application/vnd.ms-powerpoint':
			case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_ppt.png';
				
			case 'application/vnd.ms-excel':
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_xls.png';
			
			default:
				return $GLOBALS['PHprefs']['url'].'/core/icons/ext_other.png';
		}
	}
}
