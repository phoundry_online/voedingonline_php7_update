<?php
/*
* FAQ Module
*
* @version 0.0.1
* @author Vincent Poulissen
* @todo iets met SID in de config en content tabel
*/

class FaqModule extends Webmodule {

	/*
	* Array with categories
	* @acces private
	* @var Array
	*/
	private $_categories	= array();

	/*
	* Array with questions and answers
	* @acces public
	* @var Array
	*/
	public $items		= array();

	/*
	* Array with questions and answers
	* @acces public
	* @var Array
	*/
	public $faq		= array();

	/**
	* @var object
	* @acces private
	*/
	private $_auth = NULL;

	/*
	* @var integer
	* @acces public
	*/
	private $_category_id;

	/*
	* @var integer
	* @acces public
	*/
	private $_article_id;

	/*
	* @var string
	* @acces public
	*/
	public $view;

	/*
	* @var integer
	* @acces private
	*/
	private $_number_showed_questions = 3;

	/*
	* @var string
	* @acces public
	*/
	public $sortby;

	/*
	* Valid sortby options
	* @var array
	* @acces private
	*/
	private $_sortby_types = array('alphabet','prio','populair');

	/*
	* Construct
	* @acces public
	*/
	public function __construct($id, $cc){
		parent::__construct($id, $cc);

		// set category id & article id
		$this->_category_id	= (isset($this->get['category_id']))	? (int)$this->get['category_id']	: NULL;
		$this->_article_id	= (isset($this->get['article_id']))		? (int)$this->get['article_id']		: NULL;

		// Load auth
		$this->_auth = Auth::singleton();
	}

	public function __get($name) {
		switch ($name) {
			case 'category_id':
				return $this->_category_id;
				break;
			case 'article_id':
				return $this->_article_id;
				break;
			case 'number_showed_questions':
				return $this->_number_showed_questions;
				break;
			case 'categories';
				return $this->_categories;
				break;
			case 'user';
				return $this->_auth->user;
				break;
			default:
				trigger_error(sprintf('Attribute %s not found', $name), E_USER_WARNING);
		}
	}

	public function __set($name, $value) {
		switch ($name) {
			case 'category_id':
				$this->_category_id = (int)$value;
				break;
			case 'article_id':
				$this->_article_id = (int)$value;
				break;
			case 'number_showed_questions':
				$this->_number_showed_questions = (int)$value;
				break;
			
			default:
				trigger_error(sprintf('Attribute %s not found', $name), E_USER_WARNING);
		}
	}

	/*
	* Load data
	* @acces protected
	*/
	protected function loadData(){

		// config?
		if(!isset($this->config)) {
			return FALSE; 
		}

		// check sortby options
		if (isset($this->get['sortby']) && in_array($this->get['sortby'], $this->_sortby_types)){
			$this->sortby = $this->get['sortby'];
			$this->session['sortby'] = $this->get['sortby'];
		} elseif(isset($this->session['sortby'])) {
			$this->sortby = $this->session['sortby'];
		} else {
			$this->sortby = $this->config->get('sort_by');
		}

		// load categories
		$this->_load_categories();

		if (isset($this->_category_id) && isset($this->_article_id)){
			$this->view = 'ArticleView';
			return $this->load_article_view($this->_category_id, $this->_article_id, $this->sortby);
		}
		elseif(isset($this->_category_id) && count($this->_categories) > 0){
			$this->view = 'CategoryView';
			return $this->load_faqs($this->_category_id);
			$result = $this->load_faqs();
			return $result;
		}
		else{
			$this->view = 'OverView';
			$result = $this->load_faqs();
			return $result;
		}
	}

	/*
	* Function load_article_view()
	* @acces protected
	* @param Integer category_id
	* @param Integer article_id
	*/
	protected function load_article_view($category_id, $article_id, $sortby){
		// de gevraagde category en vragen ophalen. De gevraagde vraag bovenaan tonen met het antwoord.

		// indien error, lekker doorgaan, dan de ip tabel maar niet updaten
		$res = $this->check_ip($category_id, $article_id);

		switch ($this->sortby) {
			case 'prio':
				$column = 'prio';
				$order = 'DESC';
				break;
			case 'populair':
				$column = 'times_showed';
				$order = 'DESC';
				break;
			case 'alphabet':
			default:
				$column = 'question';
				$order = 'ASC';
		}

		// data query
		$sql = sprintf('
			SELECT
				fa.id,
				fa.question,
				fa.answer,
				fa.times_showed,
				fa.prio
			FROM
				module_content_faq_article fa
			WHERE
				category_id = %d
			ORDER BY
				id = %d DESC,
				%s %s
			LIMIT 1
		',
			$category_id,
			$article_id,
			$column,
			$order
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)) {
			return new ErrorVoedingonline(E_USER_WARNING, 'Kan FAQ-artikel gegevens niet laden', __FILE__, __LINE__);
		}

		// put items in multidimensional array
		if ($res->current()){
			while($res->current()){
			    $item = $res->current();
				$this->_categories[$category_id]->items[] = $item;
				$res->next();
			}
		}

		return TRUE;
	}

	/*
	* Function load_faqs()
	* Load answers and questions in an array. And put them together with the category where they belong to.
	* @acces protected
	* @param Integer category_id
	*/
	protected function load_faqs($category_id = NULL){

		// get and check LIMIT from config, default = 3 
		if (isset($this->config->number_showed_questions)) {
			$this->number_showed_questions = $this->config->number_showed_questions;
		}

		switch ($this->sortby) {
			case 'prio':
				$column = 'prio';
				$order = 'DESC';
				break;
			case 'populair':
				$column = 'times_showed';
				$order = 'DESC';
				break;
			case 'alphabet':
			default:
				$column = 'question';
				$order = 'ASC';
		}

		foreach($this->_categories as $category){

			// Check if we have to get item from specific category_id
			if(!is_null($category_id)) {
				$sql_extra = '';
				// Only continue if current category->id matches given category_id
				if($category->id != $category_id)
					continue;
			} else {
				// No specific category, decide limit
				$sql_extra = ' LIMIT '.$this->number_showed_questions;
			}

			// Create query
			$sql = sprintf('
				SELECT
					fa.id,
					fa.question,
					fa.answer,
					fa.times_showed,
					fa.prio
				FROM
					module_content_faq_article fa
				WHERE
					category_id=%d
				ORDER BY
					fa.%s %s
				%s
			',
				$category->id,
				$column,
				$order,
				$sql_extra
			);

			// Execute sql
			$res = DB::iQuery($sql);

			if (self::isError($res)) {
				return new ErrorVoedingonline(E_USER_WARNING, 'Kan vragen en antwoorden niet laden', __FILE__, __LINE__);
			}

			// Store items
			if ($res->current()){
				while($res->current()){
				    $item = $res->current();
					$this->_categories[$category->id]->items[] = $item;
					$res->next();
				}
			}
		}
		return TRUE;
	}

	/*
	* Function _load_categories()
	* Load's the categories in an array()
	* @acces private
	*/
	private function _load_categories(){

		switch($this->sortby)
		{
			case 'prio':
				$colomn = 'prio';				
				break;
			case 'alphabet':
				$colomn = 'category';				
				break;
			default:
				$colomn = 'category';				
		}
			
		
		$sql = sprintf('
			SELECT
				fc.id, fc.category
			FROM
				module_content_faq_category fc
			WHERE
				(SELECT count(*) FROM module_content_faq_article fa WHERE fa.category_id = fc.id) > 0
			ORDER BY
				fc.%s ASC
		',
			$colomn
			
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)) {
			return new ErrorVoedingonline(E_USER_WARNING, 'Kan categorien niet laden', __FILE__, __LINE__);
		}

        while($res->current()){
		    $category = $res->current();
            $this->_categories[$category->id] = new FaqCategory($category->id, $category->category);
            $res->next();
        }

		// category_id = -1

		return TRUE;
	}

	/*
	* Function check_ip()
	* Check's if the answer is already viewed by the user. If so, do nothing. Else store_ip() & count_answer()
	* @acces private
	* @param Integer category_id
	* @param Integer article_id
	*/
	private function check_ip($category_id, $article_id){
		// set ip
		$ip = $_SERVER['REMOTE_ADDR'];

		// no ip? don't count.
		if (empty($ip)) {
			return;
		}

		// check if this question is already viewed from this ip
		$sql = sprintf('
			SELECT
				id
			FROM
				module_content_faq_ip fi
			WHERE
				fi.ip = "%s"
			AND
				fi.article_id = %d
		',
			$ip,
			$article_id
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)) {
			return new ErrorVoedingonline(E_USER_WARNING, 'Kan ip-teller niet updaten', __FILE__, __LINE__);
		}

		// When there's no result, count & store.
		if($res->num_rows == 0){
			$this->_count_answer($category_id, $article_id);
			$this->_store_ip($article_id, $ip);
		}
		return TRUE;
	}

	/*
	* Function _count_answer()
	* Counts how many times an answer is viewed
	* @acces private
	* @param Integer category_id
	* @param Integer article_id
	*/
	private function _count_answer($category_id, $article_id){
		// Count!
		$sql = sprintf('
			UPDATE
				module_content_faq_article fa
			SET
				fa.times_showed = fa.times_showed + 1
			WHERE
				id = %d
			AND
				category_id = %d
		',
			$article_id,
			$category_id
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			return new ErrorVoedingonline(E_USER_WARNING, 'Kan ip-teller niet updaten', __FILE__, __LINE__);
		}
		return TRUE;
	}

	/*
	* Function _store_ip()
	* Store's the ip when an answer is viewed.
	* @acces private
	* @param Integer article_id
	*/
	private function _store_ip($article_id, $ip){
		//Store ip!
		$sql = sprintf('
			INSERT INTO
				module_content_faq_ip (article_id, ip)
			VALUES
				(%d, "%s")
		',
			$article_id,
			$ip
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			return new ErrorVoedingonline(E_USER_WARNING, 'Kan ip niet opslaan', __FILE__, __LINE__);
		}
		return TRUE;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}



class FaqCategory {
	private $_id;
	private $_title;
	// questions?
	public $items = array();

	function __construct($id, $title){
		$this->_id = $id;
		$this->_title = $title;
	}

	function __get($name){
		switch($name){
			case 'id':
				return $this->_id;
			break;
			case 'title':
				return $this->_title;
			break;
		}
	}
}

?>