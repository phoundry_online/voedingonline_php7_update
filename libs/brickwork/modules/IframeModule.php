<?php
/**
 * Module om een website in een iframe te tonen
 * 
CREATE TABLE `module_config_iframe` (                       
`page_content_id` int(10) unsigned NOT NULL default '0',
`width` int(5) unsigned default NULL,
`height` int(5) unsigned default NULL,
`scrolling` enum('auto','yes','no') default NULL,
`frameborder` tinyint(1) default '0',
`marginheight` int(5) default NULL,
`marginwidth` int(5) default NULL,
`url` text NOT NULL,
PRIMARY KEY  (`page_content_id`)
);
 *
 * Template:
<iframe src="{$module->config->url|default}" scrolling="{$module->config->scrolling|default:'auto'}" frameborder="{$module->config->frameborder|default:0}" width="{$module->config->width|default:'100%'}" height="{$module->config->height|default:'100%'}" marginheight="{$module->config->marginheight|default:0}" marginwidth="{$module->config->marginwidth|default:0}"></iframe>
 * 
 */
class IframeModule extends Webmodule
{
	public function loadData()
	{
		return true;
	}
}