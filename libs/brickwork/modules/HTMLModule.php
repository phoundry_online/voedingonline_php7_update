<?php
/**
 * HTMLModule
 *
 * @version 2.0.0
 */
class HTMLModule extends Webmodule implements PageContentCopyable
{

	/**
	 * Id of the current HTML item
	 * @var int
	 */
	public $content_id;

	/**
	 * Header for the HTML module
	 * @var string
	 */
	public $title = '';

	/**
	 * HTML content from WYSIWYG editor
	 * @var string
	 */
	public $text  = '';

	/**
	 * Pages
	 * @var array
	 */
	public $pages = array();

	/**
	 * Load HTML content
	 * @return bool
	 */
	protected function loadData()
	{
		if(empty($this->content_table)) throw new Exception(__CLASS__.' needs content table');

		$res = $this->getContent(Utility::arrayValue($this->get, 'page'));

		if(!($record = $res->first())) return false;

		if(!empty($this->config->title)
		&& isset(Framework::$contentHandler->page->title_array, $record->title)
		&& is_array(Framework::$contentHandler->page->title_array)
		){
			switch($this->config->title)
			{
				case 'prepend'	: array_unshift(Framework::$contentHandler->page->title_array, $record->title);
				break;
				case 'append'	: Framework::$contentHandler->page->title_array[] = $record->title;
				break;
				case 'replace'	: Framework::$contentHandler->page->title_array = array($record->title);
				break;
			}
		}
		if (!empty($this->config->description) &&
			property_exists(Framework::$contentHandler->page, 'description') &&
			property_exists($record, 'description'))
		{
			if(empty(Framework::$contentHandler->page->description))
			{
				Framework::$contentHandler->page->description = $record->description;
			}
			else
			{
				switch($this->config->description)
				{
					case 'prepend'	: Framework::$contentHandler->page->description = $record->description.' '.Framework::$contentHandler->page->description;
					break;
					case 'append'	: Framework::$contentHandler->page->description.= ' '.$record->description;
					break;
					case 'replace'	: Framework::$contentHandler->page->description = $record->description;
					break;
				}
			}
		}
			
		if (!empty($this->config->keywords) &&
			property_exists(Framework::$contentHandler->page, 'keywords') &&
			property_exists($record, 'keywords'))
		{
			if(empty(Framework::$contentHandler->page->keywords))
			{
				Framework::$contentHandler->page->keywords = $record->keywords;
			}
			else
			{
				switch($this->config->keywords)
				{
					case 'prepend'	: Framework::$contentHandler->page->keywords = $record->keywords.' '.Framework::$contentHandler->page->keywords;
					break;
					case 'append'	: Framework::$contentHandler->page->keywords.= ' '.$record->keywords;
					break;
					case 'replace'	: Framework::$contentHandler->page->keywords = $record->keywords;
					break;
				}
			}
		}

		$this->content_id	= $record->id;
		$this->title		= $record->title;
		$this->text			= $record->text;
		$this->pages		= $this->getPages($record->id)->getAsObjectArray();
		if(1 == count($this->pages)) $this->pages = array();

		return true;
	}
	
	/**
	 * Get a given content record
	 * 
	 * @param int $page Default the first page
	 * @return DB_ResultSet
	 */
	public function getContent($page = null)
	{
		return DB::iQuery(sprintf('
			SELECT
				*
			FROM
				%s
			WHERE
				site_identifier = "%s"
			AND
				page_content_id = %d
			%s
			ORDER BY
				prio DESC
			LIMIT 1
			',
			// values
			DB::quote($this->content_table),
			DB::quote(Framework::$site->identifier),
			$this->id,
			!empty($page) ? 'AND id = '.(int) $page : ''
		));
	}
	
	/**
	 * Get all the content for the module
	 * 
	 * @param int $current_page_id
	 * @return DB_ResultSet
	 */
	public function getPages($current_page_id)
	{
		return DB::iQuery(sprintf('
			SELECT
				id,
				title,
				IF(id=%d,1,0) as current
			FROM
				%s
			WHERE
				site_identifier = "%s"
			AND
				page_content_id = %d
			ORDER BY
				prio DESC
			',
			// values
			$current_page_id,
			DB::quote($this->content_table),
			DB::quote(Framework::$site->identifier),
			DB::quote($this->id)
		));
	}

	public static function copyContent($page_content_id, $new_page_content_id, $page_content_tid, $site_identifier)
	{
		if($table = PhoundryTools::getTableName($page_content_tid))
		{
			$res = DB::iQuery(sprintf(
				'SELECT
					*
				FROM
					`%s`
				WHERE
					page_content_id = %d
				',
				$table,
				$page_content_id
			));
				
			if($res instanceof DB_ResultSet)
			{
				$rows = array();
				foreach($res->getAsArray() as $row)
				{
					$row['id'] = ''; // Set id to empty so it uses autoincrement
					$row['site_identifier'] = $site_identifier;
					$row['page_content_id'] = $new_page_content_id;
					$rows[] = $row;
				}
				$inserts = DB::insertRows($rows, $table);
				return (count($rows) == count($inserts));
			}
		}
		return false;
	}
}
