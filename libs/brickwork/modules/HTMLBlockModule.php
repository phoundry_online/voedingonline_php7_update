<?php
/**
 * HTMLBlockModule
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 0.1.0
CREATE TABLE `module_config_htmlblock` (                       
`page_content_id` int(10) unsigned NOT NULL default '0',
`html` text NULL,
PRIMARY KEY  (`page_content_id`)
);
 *
 * Template:
{$module->config->html|default}
 * 
 */
class HTMLBlockModule extends Webmodule
{
	protected function loadData()
	{
		return true;
	}
}
