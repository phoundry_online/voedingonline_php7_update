<?php
require_once INCLUDE_DIR.'classes/Form.package.php';
require_once INCLUDE_DIR.'classes/AvoidForm.package.php';

class AvoidFormModule extends Webmodule {
	public $form;

	public function __construct($id, $cc){
		parent::__construct($id, $cc);
	}

	protected function loadData(){
		if (!isset($this->config)) {
			return FALSE;
		}

		$this->form = new AvoidForm($this->config->form_code, '?');
		$this->form->load();
		$this->form->handleRequest($_POST);

		// alles goed, return true
		return TRUE;
	}


	public function __set($name, $value) {
		switch ($name) {
			default:
				throw new AttributeNotFoundException('Attribute not found, this attribute is not present in this module, major oops?');
				break;
		}
	}

	public function __get($name) {
		switch ($name) 	{
			case 'errors' :
				return (is_array($this->errors_) ? (array)$this->errors_ : $this->errors_);

			default:
				throw new AttributeNotFoundException('Attribute not found, this attribute is not present in this module, major oops?');
				break;
		}
	}
}
?>