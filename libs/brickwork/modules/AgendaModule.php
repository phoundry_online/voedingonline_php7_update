<?php
/**
 * AgendaModule
 */
class AgendaModule extends Webmodule
{
	/**
	 * @var string The current sub template to be used
	 */
	public $view = 'overview';

	/**
	 * @var array Agenda items to be shown
	 */
	public $items = array();

	/**
	 * @var int
	 */
	public $agenda_id = NULL;

	/**
	 * @var int Items that are shown per page if paginating
	 */
	public $items_per_page = 2;

	/**
	 * @var int Current page (zero indexed)
	 */
	public $page_nr = 0;

	/**
	 * @var string Viewable event type's
	 */
	public $event_type_ids;

	/**
	 * @var int detail page # for the mini view, default home page
	 */
	public $detail_page_id = 1;

	/**
	 * @var int $_event_type_ids Events that should only be shown
	 */
	protected $_event_type_ids;

	// pagination
	/**
	 * @deprecated
	 */
	public $pages;
	/**
	 * @deprecated
	 */
	public $next;
	/**
	 * @deprecated
	 */
	public $previous;
	/**
	 * @deprecated
	 */
	public $return_page_id = 0;

	/**
	 * @var Pagination
	 */
	public $pagination;

	// tables
	protected static $_content_table = 'module_content_agenda';
	protected static $_event_table = 'module_content_agenda_event_type';
	protected static $_lookup_table = 'brickwork_agenda_site';

	/**
	 * Construct
	 * This should always be executed
	 */
	public function __construct($id, ContentContainer $cc)
	{
		parent::__construct($id, $cc);
		$this->use_cache = false;

		$this->agenda_id = (!empty($this->get['agenda_id']) && is_numeric($this->get['agenda_id'])) ? (int)$this->get['agenda_id'] : NULL;
		$this->view = (!empty($this->get['view'])) ? $this->get['view'] : $this->view;
		$this->page_nr = (isset($this->request['page_nr'])) ? (int)$this->request['page_nr'] : $this->page_nr;

		// PE: fix voor tabellen: kijken of we een upgrade zijn (lookup_module_agenda_site)
		//     of een nieuwe versie die werkt op brickwork_agenda_site
		if (Reflection_Table::exists('lookup_module_agenda_site')) {
			self::$_lookup_table = 'lookup_module_agenda_site';
		}
		else {
			self::$_lookup_table = 'brickwork_agenda_site';
		}
	}

	/**
	 * Load data
	 */
	protected function loadData()
	{
		// set custom tables or use the default ones?
		// the default setting of the content table is from parent class WebModule
		self::$_lookup_table = !empty($this->config->lookup_table) ?
			$this->config->lookup_table : self::$_lookup_table ;

		self::$_event_table = !empty($this->config->event_table) ?
			$this->config->event_table : self::$_event_table ;

		self::$_content_table = !empty($this->config->content_table) ?
			$this->config->content_table : self::$_content_table;

		// Set items per page
		$this->items_per_page = isset($this->config->items_per_page) ?
			$this->config->items_per_page : $this->items_per_page;

		$this->detail_page_id = isset($this->config->detail_page_id) ?
			$this->config->detail_page_id : $this->detail_page_id;

		// show only items from this type of event
		$this->_event_type_ids	= (isset($this->config->event_type_ids)) ?
			$this->config->event_type_ids : NULL;

		switch(isset($this->config->behavior) ? $this->config->behavior : 'normal')
		{
			case 'mini':
				$this->view = 'miniview';
				return $this->loadMiniView();
				break;

			case 'normal':
			default:
				if(!is_null($this->agenda_id)) {
					$this->view = 'detailview';

					// Load item
					return $this->loadDetailView($this->agenda_id);
				}
				else {
					switch($this->view) {
						case 'archive':
							return $this->loadArchiveView();
							break;
						case 'overview':
						default:
							return $this->loadOverview();
							break;
					}
				}
				break;
		}
	}

	/**
	 * loadOverview
	 */
	protected function loadOverview()
	{
	    // bouw mooi stukje sql voor event type's
		$filter = (strlen($this->_event_type_ids) > 0) ?
			sprintf("AND ac.event_type_id IN (%s)", $this->_event_type_ids) : '';

		// moet de module zich alleen bezig houden met events uit deze site, of alle events?
		if (isset($this->config->site_level) && $this->config->site_level == 1){
			$filter.= sprintf(' AND aet.site_identifier = "%s"', Framework::$site->identifier);
		}

		// Calculate offset
		$this->offset = $this->page_nr * $this->items_per_page;

		$sql =	sprintf('
			SELECT SQL_CALC_FOUND_ROWS
				ac.*,
				aet.name as event_type
			FROM
				%s ac
			INNER JOIN
				%s mas ON mas.agenda_id = ac.id
			INNER JOIN
				%s aet ON aet.id = ac.event_type_id
			WHERE
				mas.site_identifier = "%s"
			AND
				("%s" BETWEEN ac.online_date AND ac.offline_date)
			%s
			ORDER BY
				ac.from_date ASC, ac.till_date ASC
			LIMIT
				%d,%d
		',
			DB::quote(self::$_content_table),
			DB::quote(self::$_lookup_table),
			DB::quote(self::$_event_table),
			DB::quote(Framework::$site->identifier),
			date('Y-m-d H:i:s', PHP_TIMESTAMP),
			$filter,
			$this->offset,
			$this->items_per_page
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)) {
			trigger_error($res->errstr, E_USER_ERROR);
		}

		$this->items = $res->getAsObjectArray();

		// Total items
		$this->cnt_items = $res->total_rows;

		// set pagination
		$this->_pagination($this->cnt_items, $this->items_per_page, $this->page_nr);

		$this->pagination = new Pagination($this->items_per_page,
				$this->page_nr, $res->total_rows, $this->qsPart('page_nr'));

		return TRUE;
	}

	/**
	 * loadMiniView
	 */
	protected function loadMiniView()
	{
		// bouw mooi stukje sql voor event type's (inderdaad onwijs mooi..)
		$filter = (strlen($this->_event_type_ids) > 0) ?
			sprintf("AND ac.event_type_id IN (%s)", $this->_event_type_ids) : '';

		$sql = sprintf('
			SELECT
				ac.*,
				aet.name as event_type
			FROM
				%s ac
			INNER JOIN
				%s mas ON mas.agenda_id = ac.id
			INNER JOIN
				%s aet ON aet.id = ac.event_type_id
			WHERE
				mas.site_identifier = "%s"
			AND
				("%s" BETWEEN ac.online_date AND ac.offline_date)
			%s
			ORDER BY
				ac.from_date ASC, ac.till_date ASC
			LIMIT
				%d
		',
			DB::quote(self::$_content_table),
			DB::quote(self::$_lookup_table),
			DB::quote(self::$_event_table),
			DB::quote(Framework::$site->identifier),
			date('Y-m-d H:i:s', PHP_TIMESTAMP),
			$filter,
			$this->items_per_page
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)) {
			trigger_error($res->errstr, E_USER_ERROR);
		}

		$this->items = $res->getAsObjectArray();

		return true;
	}

	/**
	 * loadArchiveView
	 */
	protected function loadArchiveView()
	{
		// bouw mooi stukje sql voor event type's
		$filter = (strlen($this->_event_type_ids) > 0) ?
			sprintf("AND ac.event_type_id IN (%s)", $this->_event_type_ids) : '';

		$sql =	sprintf('
			SELECT
				ac.*,
				aet.name as event_type
			FROM
				%s ac
			INNER JOIN
				%s mas ON mas.agenda_id = ac.id
			INNER JOIN
				%s aet ON aet.id = ac.event_type_id
			WHERE
				mas.site_identifier = "%s"
			AND
				("%s" NOT BETWEEN ac.online_date AND ac.offline_date)
			%s
			ORDER BY
				ac.from_date ASC, ac.till_date ASC
		',
			DB::quote(self::$_content_table),
			DB::quote(self::$_lookup_table),
			DB::quote(self::$_event_table),
			DB::quote(Framework::$site->identifier),
			date('Y-m-d H:i:s', PHP_TIMESTAMP),
			$filter
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)) {
			trigger_error($res->errstr, E_USER_ERROR);
		}

		$this->items = $res->getAsObjectArray();
		return true;
	}

	private function _get_ids()
	{
		if ($this->view == 'archive') {
			$sql_extra = sprintf('("%s" NOT BETWEEN ac.online_date AND ac.offline_date) AND cn.archive = 1', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}
		else {
			$sql_extra = sprintf('("%s" BETWEEN ac.online_date AND ac.offline_date)', date('Y-m-d H:i:s', PHP_TIMESTAMP));
		}

		$sql =	sprintf('
			SELECT
				ac.id
			FROM
				%s ac
			INNER JOIN
				%s mas ON mas.agenda_id = ac.id
			INNER JOIN
				%s aet ON aet.id = ac.event_type_id
			WHERE
				%s
			AND
				mas.site_identifier = "%s"
			ORDER BY
				ac.from_date ASC, ac.till_date ASC
			',
			DB::quote(self::$_content_table),
			DB::quote(self::$_lookup_table),
			DB::quote(self::$_event_table),
			$sql_extra,
			DB::quote(Framework::$site->identifier)
		);

		$res = DB::iQuery($sql);

		$ids = array();
		foreach ($res as $row) {
			$ids[] = (int) $row->id;
		}

		// Backward compatibility we never return an empty array instead null
		return $ids ? $ids : null;
	}

	/**
	 * loadDetailView
	 */
	protected function loadDetailView($agenda_id)
	{
		$sql =	sprintf('
			SELECT
				ac.*,
				aet.name as event_type
			FROM
				%s ac
			INNER JOIN
				%s mas ON mas.agenda_id = ac.id
			INNER JOIN
				%s aet ON aet.id = ac.event_type_id
			WHERE
				ac.id=%d
			AND
				mas.site_identifier = "%s"
		',
			DB::quote(self::$_content_table),
			DB::quote(self::$_lookup_table),
			DB::quote(self::$_event_table),
			$agenda_id,
			DB::quote(Framework::$site->identifier)
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)) {
			trigger_error($res->errstr, E_USER_ERROR);
		}

		foreach($res as $item) {
			$item->archived = !(date('Y-m-d H:i:s', PHP_TIMESTAMP) < $item->offline_date);
			$this->items[] = $item;
		}

		// get all id's of viewable agenda items. used for the return page.
		$list = $this->_get_ids();

		// fix when agenda data only has one item with offline_date == online_date (Bart)
		if ($list) {
			// set return page id
			$this->return_page_id = $this->_get_return_page_id($agenda_id, $this->items_per_page, $list);
		}

		return true;
	}

	/**
	 * pagination
	 *
	 * maak de navigatie tussen verschillende overzicht pagina's
	 *
	 * @deprecated
	 * @param Int: total concerned items
	 * @param Int: items per page, from config
	 * @param Int: page number from get
	 */
	protected function _pagination($totalItems, $itemsPerPage, $page_nr)
	{
		if ($itemsPerPage == 0) {
			return false;
		}

		// set pages and round off pages
		$pages = $totalItems / $itemsPerPage;
		$pages = (is_integer($pages)) ? ceil($pages-1) : $pages;

		// fill the array $this->pages
		for($i = 0; $i <= $pages; $i++) {
			$this->pages[$i] = (isset($page_nr) && $page_nr == $i);
		}

		// set previous / next
		$this->next		= isset($this->pages[$page_nr+1]) ? $page_nr+1 : null;
		$this->previous = isset($this->pages[$page_nr-1]) ? $page_nr-1 : null;

		return true;
	}

	/**
	 * _get_return_page_id
	 *
	 * genereert het pagina nummer waar het huidige artikel opstaat
	 *
	 * @param Int: concerned article id
	 * @param Int: has the number of viewable items on a page
	 * @param Array: holding the id's of the current viewable articles
	 */
	private static function _get_return_page_id($article_id, $items_per_page, $list)
	{
		// get arraykey of current article
		$option = array_search($article_id, $list);

		// deel het aantal artikelen op een pagina door de key van het huidge artikel
		return $option ? floor($option/$items_per_page) : 0;
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
