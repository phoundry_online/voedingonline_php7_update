<?php
/**
 * Module om uploads via EmailFormulierModule te downloaden
 *
 * Via deze module kunnen bezoekers die een automatische mail verstuurd krijgen bij een
 * formulier submit de upgeloade bestanden downloaden.
 *
 * @author		Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 * @copyright	Web Power
 * @created		27 jan 2009
 *
 * @package		Brickwork
 * @subpackage	Module
 */
/**
 * $Id: FormulierDownloadModule.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class FormulierDownloadModule extends Webmodule
{
	/**
	 * The ID of the uploaded file
	 *
	 * @var	Integer
	 */
	public $download_id;
	
	/**
	 * The extra code to identify the uploaded file
	 *
	 * @var	String
	 */
	public $download_code;
	
	/**
	 * Is the download still available
	 *
	 * @var	Boolean
	 */
	public $available;
	
	/**
	 * The name of the file
	 *
	 * @var	String
	 */
	public $filename;
	
	/**
	 * The size of the file in bytes
	 *
	 * @var	Integer
	 */
	public $filesize;
	
	/**
	 * Access code for the file.
	 *
	 * @var	String
	 */
	private $_access_code;
	
	/**
	 * Error
	 *
	 * @var	Boolean
	 */
	public $error;
	
	/**
	 * @var DB_Adapter
	 */
	private $db;

	/**
	 * Constructor
	 *
	 * @param	Integer				$id
	 * @param	ContentContainer	$cc
	 * @return	void
	 */
	public function __construct($id, ContentContainer $cc)
	{
		parent::__construct($id, $cc);
		
		$this->download_id = isset($this->get['fid']) ? $this->get['fid'] : 0;
		$this->download_code = isset($this->get['fcode']) ? $this->get['fcode'] : null;
		$this->available = false;
		$this->filename = "";
		$this->filesize = 0;
		$this->error = false;
		$this->db = DB::getDb();
		
		$this->_checkDownload();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Module#loadData()
	 */
	public function loadData()
	{
		if (isset($this->post['code']) && $this->available) { 
			if ($this->post['code'] === $this->_access_code) {
				$sql = sprintf("UPDATE brickwork_avoid_form_upload
						SET downloadcount = downloadcount + 1
						WHERE id = %d",
					$this->download_id
				);
				$res = $this->db->iQuery($sql);
				
				header('Content-Disposition: attachment; filename="' . $this->filename . '"');
				header('Content-type: application/force-download');
				header('Content-type: application/octet-stream');
				header('Content-Description: File Transfer');
				header('Content-Length: ' . $this->filesize);
				readfile($this->getFilePath($this->download_code, $this->filename));
				exit();
			} else {
				$this->error = true;
			}
		}
		return true;
	}
	
	/**
	 * Check if the download is still available and that the identification is valid
	 *
	 * @return	void
	 */
	private function _checkDownload()
	{
		$sql = sprintf("SELECT
					id,
					code,
					filename,
					removed,
					filesize,
					hashcode
				FROM
					brickwork_avoid_form_upload
				WHERE
					id = %d
				AND
					code = %s
				AND
					site_identifier = %s
				LIMIT 1",
			$this->download_id,
			$this->db->quoteValue($this->download_code),
			$this->db->quoteValue(Framework::$site->identifier)
		);
		
		try {
			$res = $this->db->iQuery($sql);
		} catch (DB_Exception $e) {
			return;
		}
		
		if ($row = $res->first()) {
			$this->filename = $row->filename;
			$this->filesize = $row->filesize;
			$this->download_id = $row->id;
			$this->download_code = $row->code;
			$this->_access_code = $row->hashcode;

			$this->available = $row->removed == 0 &&
					file_exists($this->getFilePath($row->code, $row->filename));
		}
		else {
			$this->download_id = 0;
			$this->download_code = '';
		}
	}

	private function getFilePath($code, $filename)
	{
		return sprintf('%s/FILES/EmailFormUpload/%s_%s',
				UPLOAD_DIR, $code, $filename);
	}
}
