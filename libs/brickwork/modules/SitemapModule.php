<?php

/*
* SitemapModule
*
* Load full sitemap from site structure
*
* @author unknown
*/
class SitemapModule extends Webmodule {

	/*
	* @var boolean
	* @acces protected
	*/
	protected $use_cache = TRUE;

	/*
	* @var array
	* @acces public
	*/
	public $items = array();

	/*
	* Contructor
	*
	* @acces public
	*/
	public function __construct($id, $cc){
		parent::__construct($id, $cc);
	}

	/*
	* loadData
	*
	* Handles different types of data and function. Function must return TRUE to succesfully load the current module.
	*
	* @return boolean
	*/
	protected function loadData(){
		$this->items = self::getSitemap();
		
		// Return true
		return TRUE;
	}

	public static function getSitemap(){
		$items = array();
	
		// Load Site stack
		$structure = SiteStructure::getSiteStructure(Framework::$site->identifier);
		$stack = SiteStructure::getSiteStack(Framework::$site->identifier);

		if(is_null($stack)){
			return $items;
		}

		// collect all structure id's and determine whats connected
		$structure_ids = array();
		foreach($stack as $item){
			$structure_ids[] = (int)$item->id;
		}
		
		// Get pages
		$pages = SiteStructure::getPages($structure_ids);
		
		// Get pages
		$testpages = SiteStructure::getTestPages($structure_ids);
		
		// Get urls
		$urls  = SiteStructure::getUrls($structure_ids);
		
		// Create menu_shortcuts array
		$menu_shortcuts = array();

		// Create menu array
		$menu = array();

		// Create invisible parent array
		$invisible_parents = array();

		// Loop trough the stack and add valid items
		foreach($stack as $item){

			// Check for url id and page id
			if(!isset($urls[$item->id]) && !isset($pages[$item->id])){
				continue;
			}
			
			if (isset($testpages[$item->id]) && $testpages[$item->id] == $pages[$item->id]) {
				// All pages below this site structure item have staging status 'test'
				continue;
			}
			
			// Put item as invisible when not valid for visible sitemap
			if(!$item->showSitemap){
				$invisible_parents[] = (int)$item->id;
				continue;
			}
			
			// Create the url
			if(!empty($pages[$item->id]))
			{
				$url = SiteStructure::getFullPathByStructureId($item->id);
				$target = '_self';
			} else if(!empty($urls[$item->id])) {
				$url = $urls[$item->id]['link'];
				$target = $urls[$item->id]['target'];
			}

			// Create menu object
			if(empty($item->parent)){
				$m = new Menu($item->name,$url,$item->name,0);
				$m->id = $item->id;
				$m->target = $target;
				$m->selected = $item->isActive;
				$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
				$menu[$item->id] = $m;
				$menu_shortcuts[$item->id] = $m;
				$items[] = $m;
			} else {
				$m = new Menu($item->name,$url,$item->name,1);
				$m->id = $item->id;
				$m->target = $target;
				$m->selected = $item->isActive;
				$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
				$menu_shortcuts[$item->parent]->items[] = $m;
				$menu_shortcuts[$item->id] = $m;
			}
			$m->structure = $item;
		}
		return $items;
	}
}

?>
