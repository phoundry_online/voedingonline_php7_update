<?php
/**
 * Controller Module implementing the C part of the MVC design pattern
 * 
 * By setting the $this->action pre loadData() the given action is executed
 * 
 * @author		Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @author		Peter Eussen <peter.eussen@webpower.nl>
 * @version		1.1
 * @package		Brickwork
 * @subpackage	Module
 */
abstract class ControllerModule extends Webmodule
{
	/**
	 * Template data
	 *
	 * @var array
	 */
	public $td;
	
	/**
	 * Action to be executed should be set to a valid action from
	 * the extending class before calling loadData()
	 *
	 * @var string
	 */
	public $action;
	
	/**
	 * Initialisation of the ControllerModule
	 *
	 * @param int $id
	 * @param ContentContainer $cc
	 * @param string $code
	 */
	public function __construct($id, ContentContainer $cc, $code)
	{
		$this->td = array();
		$this->action = 'index';
		parent::__construct($id, $cc, $code);
	}
	
	protected function loadData()
	{
		if(empty($this->action)) $this->action = 'index';
		if(method_exists($this, $this->action.'Action'))
		{
			return $this->{$this->action.'Action'}();
		}
		else if(method_exists($this, 'defaultAction'))
		{
			return $this->defaultAction();
		}
		else
		{
			throw new Exception($this->action.'Action method not found in '.get_class($this));
		}
	}

	public function __get($name)
	{
		switch ($name) {
			case 'action_get': 
			case 'action_post':
			case 'action_request':
			case 'action_cookie':
			case 'action_session':
				$var_name = substr(strrchr($name, '_'), 1);
				return isset($this->{$var_name}[$this->action]) ? (array) $this->{$var_name}[$this->action] : (array) array();
		}
		
		throw new Exception('Var '.$name.' doesnt Exist in'.get_class($this));
	}
	
	public function __set($name, $value)
	{
		switch ($name) {
			case 'action_get': 
			case 'action_post':
			case 'action_request':
			case 'action_cookie':
			case 'action_session':
				$var_name = substr(strrchr($name, '_'), 1);
				if(isset($this->{$var_name}[$this->action]))
				{
					$this->{$var_name}[$this->action] = $value;
				}
		}
		
		throw new Exception('Var '.$name.' doesnt Exist in'.get_class($this));
	}
	
	/**
	 * Action specific qsPart
	 *
	 * @param string $key
	 * @param bool $global Class global
	 * @return string
	 */
	public function qsAction($key, $global = false)
	{
		return $this->qsPart($this->action, $global) . '['.$key.']';
	}
	
}