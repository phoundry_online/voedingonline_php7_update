<?php
/**
 * Voor het tonen van alle mogelijke GoogleAdSense advertenties
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @since 2008-09-04
 * 
 CREATE TABLE `module_config_google_adsense` (
 `page_content_id` int(10) NOT NULL default '0',
 `client` varchar(255) NOT NULL,
 `slot` varchar(255) NOT NULL,
 `width` int(4) NOT NULL,
 `height` int(4) NOT NULL,
 PRIMARY KEY  (`page_content_id`)
 );
 * Template:
<script type="text/javascript"><!--
google_ad_client = "{$module->config->client}";
google_ad_slot = "{$module->config->slot}";
google_ad_width = {$module->config->width};
google_ad_height = {$module->config->height};
//-->
</script>
<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
 */
class GoogleAdSenseModule extends Webmodule
{
	protected function loadData()
	{
		return isset(
			$this->config->client,
			$this->config->slot,
			$this->config->width,
			$this->config->height
		);
	}
}