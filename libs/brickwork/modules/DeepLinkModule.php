<?php
/**
 * Standard DeepLinkModule
 *
 * Original code copied from the EKD site 
 *
 * @author 		Adrie den Hartog	<adrie.denhartog@webpower.nl>
 * @author			Christiaan Baartse	<christiaan.baartse@webpower.nl>
 * @since			2009-05-18
 * @version		1.0
 *
 * @link		http://projectwiki.php5.office.webpower.nl/index.php/Brickwork:standaard_modules:DeepLinkModule
 */
class DeepLinkModule extends Webmodule
{
	/**
	 * Magic Get for the DeepLinkModule
	 *
	 * @param	string	$name
	 * @return	mixedvar
	 */
	public function __get ($name)
	{
		switch ($name) 
		{
			case "items" : return self::getDeepLinkItems($this->config->page_content_id);
		}
		
		throw new InvalidArgumentException();
	}

	/**
	 * loadData for the DeepLinkModule
	 *
	 * @return	Boolean
	 */
	protected function loadData() 
	{	
		return true;
	}

	/**
	 * Retreives title and description for all configured site_structure_id's
	 *
	 * @return array
	 */
	public static function getDeepLinkItems($page_content_id, $lang = null)
	{
		if(is_null($lang)) $lang = isset(Framework::$site) && Framework::$site->isMultiLingual() ? Framework::$site->currentLang : null;
		
		$res = DB::iQuery(sprintf('
			SELECT
				ss.id AS site_structure_id,
				ss.site_identifier, 
				ss.name AS title,
				p.description
			FROM
				lookup_module_config_deeplink_page AS dp
			INNER JOIN
				brickwork_site_structure AS ss
			ON
				ss.id = dp.site_structure_id
			INNER JOIN
				brickwork_page AS p
			ON
				ss.id = p.site_structure_id
			WHERE
				dp.page_content_id = %d
			ORDER BY
				dp.prio ASC
			',			
			$page_content_id
		));
		
		$items = array();
		foreach($res->getAsArray() as $row)
		{
			
			$row['url'] = SiteStructure::getFullPathByStructureId($row['site_structure_id'], $row['site_identifier'], $lang);
			$items[] = $row;
		}

		// Pass back the array of items
		return $items;
	}

}
