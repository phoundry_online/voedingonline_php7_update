<?php
/**
 * Hiermee kunnen links vanuit de database op de website worden getoond.
 *
 * @author		Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 */
class LinkModule extends Webmodule
{
	/**
	 * Sorteer volgorde van de links
	 *
	 * 0 = Niet
	 * 1 = ASC
	 * 2 = DESC
	 * 3 = Prioriteit
	 *
	 * @var	integer
	 */
	private $_config_link_sorting_type;
	
	/**
	 * Sorteer volgorde van de categoriën
	 *
	 * 0 = Niet
	 * 1 = ASC
	 * 2 = DESC
	 * 3 = Prioriteit
	 *
	 * @var	integer
	 */
	private $_config_category_sorting_type;
	
	/**
	 * Zijn er links gevonden
	 *
	 * @var	Boolean
	 */
	private $_retreive_links_success;
	
	/**
	 * Link Items
	 *
	 * @var Array
	 */
	private $_link_items;
	
	/**
	 * Beschikbare categoriën
	 *
	 * @var	Array
	 */
	private $_config_viewable_categories;
	
	/**
	 * Content table containing the link data
	 *
	 * @var	String
	 */
	protected static $_content_table = "module_content_link";
	
	/**
	 * Content table containing the link categories
	 *
	 * @var	String
	 */
	protected static $_category_table = "module_content_link_category";
	
	/**
	 * Lookup table that links the link to the category in the config
	 *
	 * @var	String
	 */
	protected static $_lookup_config_table = "lookup_module_config_link_category";
	
	/**
	 * Lookup table that links the link to the category in the content
	 *
	 * @var	String
	 */
	protected static $_lookup_content_table = "lookup_module_content_link_category";
	
	public function __get($value)
	{
		switch($value) {
			case 'config_link_sorting_type':
				return $this->_config_link_sorting_type;
				
			case 'config_category_sorting_type':
				return $this->_config_category_sorting_type;
				
			case 'link_items':
				return $this->_link_items;
				
			case 'current_category':
				return $this->_current_category;
				
			case 'retreive_links_success':
				return $this->_retreive_links_success;
				
			default:
				trigger_error('Deze Get-waarde bestaat niet: --> '.$value);
		}
	}
	
	protected function loadData()
	{
		// Set the tables used
		self::$_lookup_content_table = isset($this->config->lookup_table) ?
			$this->config->lookup_table : self::$_lookup_content_table;
			
		self::$_category_table = isset($this->config->event_table) ?
			$this->config->event_table : self::$_category_table;
			
		self::$_content_table = isset($this->config->content_table) ?
			$this->config->content_table : self::$_content_table;
		
		// fetch config settings
		$this->_config_link_sorting_type = isset($this->config->link_title_sort_id) ?
			(int) $this->config->link_title_sort_id : 0;
		$this->_config_category_sorting_type = isset($this->config->category_title_sort_id) ?
			(int) $this->config->category_title_sort_id : 0;
		
		// Get viewable categories
		$this->_config_viewable_categories = self::getCategories($this->id);
		
		// Fetch links, pass optional category and sorting type
		if ($this->_config_viewable_categories !== false) {
			$this->_link_items = self::getLinks(
				$this->_config_viewable_categories,
				$this->_config_category_sorting_type,
				$this->_config_link_sorting_type
			);
		}
		
		// Check for success
		$this->_retreive_links_success = is_array($this->_link_items);
		
		return true;
	}
	
	/**
	 * Retreive categories by pageid.
	 *
	 * @param	Integer	$pageID
	 *
	 * @return	Boolean|Array
	 */
	public static function getCategories ($pageID)
	{
		// Build query
		$sql = sprintf("SELECT category_id
			FROM %s
			WHERE page_content_id = %d",
			self::$_lookup_config_table,
			$pageID
		);
		
		// Execute query and get raw data
		$res = DB::iQuery($sql);
		if (self::isError($res)) {
			return false;
		}
		
		$catArray = array();
		while ($res->current()) {
		    $cat = $res->current();
			$catArray[] = (int) $cat->category_id;
			$res->current();
		}
			
		return $catArray ? $catArray : false;
	}
	
	/**
	 * Retreive links, filtered on category.
	 * Sorting ID's: 0 = None, 1 = ASC, 2 = DESC, 3 = PRIO.
	 *
	 * @param	Array	$categories
	 * @param	Integer	$sort_categories
	 * @param	Integer	$sort_links
	 *
	 * @return	Array
	 */
	public static function getLinks ($categories, $sort_categories, $sort_links)
	{
/**		// Build query using the given parameters
		$sql = "SELECT module_content_link.id, module_content_link.target, module_content_link.link, module_content_link.title, module_content_link.description, ";
		$sql .= "module_content_link_category.title as category ";
		$sql .= "FROM module_content_link, module_content_link_category, lookup_module_content_link_category WHERE ";
		$sql .= "module_content_link.id = lookup_module_content_link_category.link_id AND ";
		$sql .= "lookup_module_content_link_category.category_id = module_content_link_category.id AND (";
		
		foreach ($categories as $category)
		{
			$sql .= "module_content_link_category.id = ".$category." OR ";
		}
		
		$sql = substr($sql, 0, strlen($sql)-4).") ";
		
		$sql .= (($sort_categories OR $sort_links) ? "ORDER BY " : "");
		switch ($sort_categories)
		{
			case 1 : $sql .= "category ASC"; break;
			case 2 : $sql .= "category DESC"; break;
			case 3 : $sql .= "module_content_link_category.prio DESC"; break;
			default : break;
		}
		switch ($sort_links)
		{
			case 1 : $sql .= ($sort_categories ? ", " : "")."title ASC"; break;
			case 2 : $sql .= ($sort_categories ? ", " : "")."title DESC"; break;
			case 3 : $sql .= ($sort_categories ? ", " : "")."module_content_link.prio DESC"; break;
			default : break;
		}
		*/
		$order = ($sort_categories > 0 || $sort_links > 0) ? "ORDER BY " : "";
		switch ($sort_categories)
		{
			case 1:
				$order .= "category ASC";
				break;
				
			case 2:
				$order .= "category DESC";
				break;
				
			case 3:
				$order .= "mclc.prio DESC";
				break;
		}
		$order .= ($sort_categories > 0 && $sort_links > 0) ? ", " : "";
		switch ($sort_links)
		{
			case 1:
				$order .= "mcl.title ASC";
				break;
				
			case 2:
				$order .= "mcl.title DESC";
				break;
				
			case 3:
				$order .= "mcl.prio DESC";
				break;
		}
		
		$sql = sprintf("SELECT mcl.id,
					mcl.target,
					mcl.link,
					mcl.title,
					mcl.description,
					mclc.title as category
				FROM %s mcl
				INNER JOIN %s lmclc ON (mcl.id = lmclc.link_id)
				INNER JOIN %s mclc ON (mclc.id = lmclc.category_id)
				WHERE mclc.id IN (%s)
				%s
				",
			self::$_content_table,
			self::$_lookup_content_table,
			self::$_category_table,
			implode(", ", $categories),
			$order
		);
		
		// Execute query and get raw data
		$res = DB::iQuery($sql);
		
		// Rearrange the results as a 2D array for easy displaying within the smarty template
		$linkArray = array();
		
		if(!self::isError($res)) {
			while($res->current()) {
			    $row = $res->current();
				if(!isset($linkArray[$row->category])) {
					$linkArray[$row->category] = array();
				}
				$linkArray[$row->category][$row->id]['id'] = $row->id;
				$linkArray[$row->category][$row->id]['link'] = $row->link;
				$linkArray[$row->category][$row->id]['title'] = $row->title;
				$linkArray[$row->category][$row->id]['category'] = $row->category;
				$linkArray[$row->category][$row->id]['description'] = $row->description;
				$linkArray[$row->category][$row->id]['target'] = $row->target;
				$res->next();
			}
		}
		
		return $linkArray;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
