<?php
/*
*
* PollModule
*
* @author Sven Pool <pool@rr-solutions.nl>
* @last_modified 21/11/2006
*
*********************************
* Changelog
*********************************
* 01) Constante PARTNER = SITE_IDENTIFIER
* 02) $DB-> = DB::
* 03) Cookie name = getclass($this)
* 04) pluimUrlencode() = BrickWorkCrypt::urlEncode()
* 05) pluimUrldecode() = BrickWorkCrypt::urlDecode()
*/

if(!defined('POLL_MAX_IPCOUNT')){
	define('POLL_MAX_IPCOUNT', 5);
}

class PollModule extends Webmodule
{

	public $poll_id;

	/*
	** @var string
	** @acces public */
	public $view = "vote";

	/*
	** @var integer
	** @acces public */
	public $page_id = 0;

	/*
	** @var boolean
	** @acces public */
	public $voted = FALSE;

	/*
	** @var boolean
	** @acces public */
	public $poll;

	/*
	** Cookie array
	** @var array
	**
	**	voorbeeld cookie array:
	**	array(1=>true,3=>'asdasdasd') */
	public $cookie_array = array();

	public function __construct($id, $cc){
		// get, post, cookies etc afvangen
		parent::__construct($id, $cc);
	
		$this->view = (isset($this->get['view'])) ? $this->get['view'] : $this->view;

		// Set offset
		$this->page_id = (isset($this->request['page_id'])) ? (int)$this->request['page_id'] : $this->page_id;

		//$this->poll->show_result = TRUE;

		/*
		** Controle of cookie is aangemaakt */
		if(isset($_COOKIE[get_class($this)])){
			$this->cookie_array = BrickWorkCrypt::urlDecode($_COOKIE[get_class($this)]);
		}
	}

	protected function loadData(){

		/*
		** Controleren of er een Poll ID is opgegeven,
		** als deze ontbreekt kiezen we een random Poll */
		if(!empty($this->request['poll_id'])) {
			$this->poll_id = (int)$this->request['poll_id'];
		} elseif (!empty($this->config->poll_id)) {
			$this->poll_id = (int)$this->config->poll_id;
		} else {
			$this->poll_id = (int)Poll::getRandomPoll();
		}

		/** gezet? */
		if(empty($this->poll_id)) 
			return FALSE;
		
		/*
		** Als er nog niet gestemd is op deze poll dan een record in cookie array aanmaken */
		if(!isset($this->cookie_array[$this->poll_id])){
			$this->cookie_array[$this->poll_id] = uniqid();
		}
		
		/*
		** Haal Poll op! */
		$this->poll = Poll::getPoll($this->poll_id);

		/*
		** Hebben we wel iets binnen gehaald? */
		if(!is_null($this->poll)){
			/*
			** JA dus EINDE! cookie opslaan, 
			** indien headers nog niet zijn verzonden: */
			if (!headers_sent()) { // 3/7/2006 Mick
				$cookie = BrickWorkCrypt::urlEncode( $this->cookie_array );
				setcookie(get_class($this), $cookie, time()+31536000, '/', $_SERVER['HTTP_HOST']);
			}
		} else {
			/** Geen poll, dan niet laden */
			return FALSE;
		}

		/* 
		** Checken of er al gestemd is op deze poll 
		** Als de cookie niet bestaat dan moeten we even de error ondervangen.

		** Wat zeer onwaarschijnlijk is omdat 15 regels hierboven de cookie wordt gezet.
		** Laad problemen? */ 

		if (isset($_COOKIE[get_class($this)])) {
			$this->voted = Poll::isVoted($this->poll_id, $this->cookie_array, $_COOKIE[get_class($this)]);
		} elseif (isset($this->cookie_array[$this->poll_id]))  {
			$this->voted = Poll::isVoted($this->poll_id, $this->cookie_array);
		}
 
		/*
		** Even een stem opslaan*/
		if (!empty($this->post["poll_id"]) && !$this->voted) {
			$bOK = Poll::storeVote($this->post["answer_id"]);
			if ($bOK) {
				$this->cookie_array[$this->poll_id] = TRUE;
				$this->voted = TRUE;
			} else {
				return FALSE;
			}	
		}
		
		/* 
		** Er is al een keer gestemd of er wordt om de uitslag gevraagd */
		if($this->voted || $this->view == "show_results"){
			/*
			** Dan moeten we statistieken bij de poll zoeken 
			** En we moeten de view even aanpassen */		
			if(Poll::loadStatisticsByPoll($this->poll)){
				$this->view = "show_results";
				//$this->poll->show_result = TRUE;
			}
		}	
		return TRUE;
	}
}
?>