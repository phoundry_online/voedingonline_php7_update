<?php

/*
* DMFactsProModule
*
* @author Sven Pool <pool@rr-solutions.nl>
* @author Remco Bos <remco@webpower.nl>
* @last_modified 07/01/2007
*/
class DMFactsProModule extends Webmodule {

	/*
	* @var ?
	* @acces pubic
	*/
	public $account;

	/*
	* @var ineteger
	* @acces public
	*/
	public $counter;

	/*
	* Contructor
	*
	* @acces public
	*/
	public function __construct($id, $cc){
		parent::__construct($id, $cc);
	}

	/*
	* LoadData
	*
 	* Handles different types of data and function. Function must return TRUE to succesfully load the current module.
	*
	* @return boolean
	*/
	protected function loadData(){

		// Check for config
		if(empty($this->config)){
			return FALSE;
		}

		// Check for account & counter id
		if(empty($this->config->dmfacts_account) || empty($this->config->counter_id)){
			return FALSE;
		}

		// Get data from db
		$sql = sprintf("
			SELECT
				dmfacts_account_code,
				site_identifier,
				script
			FROM
				brickwork_dmfacts_pro_account
			WHERE
				site_identifier = '%s'
			AND
				id = %d
			LIMIT
				1",
			/* values */
				DB::quote(Framework::$site->identifier),
				$this->config->dmfacts_account
		);

		// Execute query
		$res = DB::iQuery($sql);

		// Error?
		if (self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);
		}

		// Check if there's a record found
		if ($res->num_rows > 0) {
				// Set values
				$this->account = $res->first()->dmfacts_account_code;
				$this->counter = $this->config->counter_id;
		} else {
			// No record, return false
			return FALSE;
		}
		// return true
		return TRUE;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
?>