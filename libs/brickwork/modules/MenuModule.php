<?php

/**
 * Loads the website menu
 *
 * @version	1.1.1
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 * @link http://www.webpower.nl
 */
class MenuModule extends Webmodule
{
	/**
	 * @var bool
	 */
	protected $_use_cache_ = true;

	/**
	 * @var array
	 */
	public $items = array();

	/**
	 * @var bool
	 */
	public $isSubMenu = false;
	
	/**
	 * @var StructureItem Start item of a submenu
	 */
	public $startItem;
	
	/**
	 * Handles different types of data and function. Function must return TRUE to succesfully load the current module.
	 *
	 * @return boolean
	 */
	protected function loadData()
	{
		// Load Site stack
		$stack = SiteStructure::getSiteStack(Framework::$site->identifier);

		$structure_ids = $delete_parent_reference = array();
		
		/*
		 * This part makes the MenuModule configurable to start at a specific item
		 * within the structure and/or loop recursive or fetch only one level.
		 * 
		 * config table needs: start_item column (NULL, int) and recursive (0,1)
		 */
		$start_item	= NULL;
		$recursive	= TRUE;
		
		if(isset($this->config)){
			if(isset($this->config->start_item)){
				$start_item = $this->config->start_item;
			}
			if(isset($this->config->recursive)){
				$recursive = $this->config->recursive;
			}			
		}
		
		if(!empty($start_item)){
			// fetch only structure id's from configged start item
			
			if(isset($stack[$start_item])){
				$this->startItem = $stack[$start_item];
				$structure_ids = $stack[$start_item]->getElements($recursive);
				$delete_parent_reference = $stack[$start_item]->getElements(false);
				$this->isSubMenu = true;
			} else {
				return false;
			}
		} else {
			// collect all structure id's and determine whats connected
			
			foreach($stack as $item){
				$structure_ids[] = (int)$item->id;
			}				
		}
		
		// Get pages
		$pages = SiteStructure::getPages($structure_ids);
		
		// Get testpages
		$testpages = SiteStructure::getTestPages($structure_ids);
		
		// Get urls
		$urls  = SiteStructure::getUrls($structure_ids);
		
		// Create shorcuts array
		$menu_shortcuts = array();
		
		// Creat menu array
		$menu = array();
		
		// Create invisible parent array
		$invisible_parents = array();
		
		// Loop trough the stack and add valid items
		foreach($stack as $item) {
			// Check for url id and page id
			if(!isset($urls[$item->id]) && !isset($pages[$item->id])) {
				continue;
			}
			
			if (isset($testpages[$item->id]) && $pages[$item->id] == $testpages[$item->id]) {
				// All pages havs staging status 'test'
				continue;
			}
			
			// unset parent is this is the first level of the submenu
			if($this->isSubMenu && in_array($item->id, $delete_parent_reference)) {
				// We make a clone of the item so we can safely change some
				// properties without affecting other code that might be using
				// the same menu items
				$item = clone($item);
				$item->parent = NULL;
			}
			
			// Put item as invisible when not valid for visible menu
			if(!$item->showMenu || in_array($item->parent, $invisible_parents)) {
				$invisible_parents[] = (int)$item->id;
				continue;
			}

			// Create the url
			if(!empty($pages[$item->id]))
			{
				$url = SiteStructure::getFullPathByStructureId($item->id);
				$target = '_self';
			}
			else if(!empty($urls[$item->id])) {
				$url = $urls[$item->id]['link'];
				$target = $urls[$item->id]['target'];
			}

			if($item->secure && $item->restricted) {
				if(is_null(Framework::$acl)) {
					Framework::$acl = ACL::singleton();
				}
				
				$user = Auth::singleton()->user;
				
				$hasAccess = Framework::$acl->hasAccess($user,$item);
			}
			else {
				$user = Auth::singleton()->user;
				$hasAccess = $item->checkAccess($user);
			}
			
			// Create menu object
			if(empty($item->parent)) {
				$m = new Menu($item->name,$url,$item->name,0);
				$m->hasAccess = $hasAccess;
				$m->id = $item->id;
				$m->target = $target;
				$m->selected = $item->isActive;
				$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
				$menu[$item->id] = $m;
				$menu_shortcuts[$item->id] = $m;
				$this->items[] = $m;
			}
			else {
				$m = new Menu($item->name,$url,$item->name,1);
				$m->hasAccess = $hasAccess;
				$m->id = $item->id;
				$m->target = $target;
				$m->selected = $item->isActive;
				$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
				$menu_shortcuts[$item->parent]->items[] = $m;
				$menu_shortcuts[$item->id] = $m;
			}
			
			$m->structure = $item;
		}

		// Return true
		return TRUE;
	}


}
