<?php
/**
 * Photogallery Module
 *
 * @author		Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 * @author		Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright	Web Power
 * @version	1.0
 * @package	Photogallery
 */
class PhotogalleryModule extends ControllerModule
{
	/**
	 * Keep the albums for the getAlbums method cached in here
	 * @var ActiveRecordIterator
	 */
	protected $_albums;
	
	/**
	 * (non-PHPdoc)
	 * @see ControllerModule#loadData()
	 */
	protected function loadData()
	{
		$this->action = $this->config->get("action", "index");
		return parent::loadData();
	}
	
	protected function defaultAction()
	{
		if(!is_null($photo_id = Utility::arrayValue($this->get, 'photo'))) {
			$photo = ActiveRecord::factory('Photogallery_Photo', $photo_id);
			$next_id = $prev_id = null;
			if(
				isset($this->session['photos_order']) &&
				is_array($this->session['photos_order']) &&
				false !== ($current = array_search($photo_id, $this->session['photos_order']))
			) {
				$next_id = Utility::arrayValue($this->session['photos_order'], $current+1);
				$prev_id = Utility::arrayValue($this->session['photos_order'], $current-1); 
			}
			
			
			$this->td = compact('photo', 'next_id', 'prev_id');
		}
		
		if(!is_null($album_id = Utility::arrayValue($this->get, 'album'))) {
			$album = ActiveRecord::factory('Photogallery_Album', $album_id);
			$photos = $this->getPhotos($album['id']);
			$photo_order = array();
			foreach($photos as $photo) {
				$photo_order[] = $photo['id'];
			}
			$this->session['photos_order'] = $photo_order;
			
			$this->td = compact('album', 'photos');
		}
		
		return true;
	}
	
	/**
	 * The slidebar display
	 * @return bool
	 */
	protected function slidebarAction()
	{
		$photo = ActiveRecord::factory('Photogallery_Photo', Utility::arrayValue($this->get, 'photo'));
		$album_ids = DB::iQuery('SELECT album_id FROM brickwork_photogallery_lookup WHERE page_content_id = '.$this->id)->
			getSelect('album_id');
		
		if(!$photo->isLoaded()) {
			$photo = $this->getPhotos($album_ids, null, 1)->first();
		}
		
		if(!$photo || !in_array($photo['album_id'], $album_ids)) {
			return false;
		}
		
		$next_id = $prev_id = null;
		if(!isset($this->session['photos_order']) || !is_array($this->session['photos_order'])) {
			$this->session['photos_order'] = $this->getPhotos($album_ids)->getSelect('id');
		}
		
		$photos_order = $this->session['photos_order'];
		if(false !== ($current = array_search($photo['id'], $photos_order))) {
			$next = array_slice($photos_order, $current+1);
			$prev = array_slice($photos_order, 0, $current);
			$prev = array_reverse($prev);
		}
		
		$this->td = compact('photo', 'next', 'prev', 'current');
		return true;
	}
	
	/**
	 * Get the albums for this module
	 * @return ActiveRecordIterator
	 */
	public function getAlbums($order_by = null, $limit = null, $offset = 0, &$count = false)
	{
		if(is_null($order_by)) {
			$sort_colum = array('alphabet' => 'title', 'priority' => 'prio');
			$order_by = array(
				$sort_colum[$this->config->get("album_sort", "priority")],
				'ASC'
			);
		}
		
		$query = ActiveRecord::factory('Photogallery_Album');
		
		$query->where('id', '(SELECT album_id FROM brickwork_photogallery_lookup WHERE page_content_id = '.$this->id.')', 'IN', false);

		if(!is_array(current($order_by))) {
			$order_by = array($order_by);
		}

		foreach($order_by as $ob) {
			if(2 == count($ob))$query->orderBy($ob[0], $ob[1]);
		}
		
		if(!is_null($limit)) {
			$query->limit($limit, $offset);
		}
		$res = $query->get();
		if(false !== $count) {
			$count = $query->getCount();
		}
		return $res;
	}
	
	/**
	 * Get the Photos that are in the albums
	 * @param array $album_ids Ids of the albums
	 * @param array $order_by
	 * @param int $limit
	 * @param int $offset
	 * @return ActiveRecordIterator
	 */
	public function getPhotos($album_ids, $order_by = null, $limit = null, $offset = 0, &$count = false)
	{
		if(!is_array($album_ids)) $album_ids = array($album_ids);
		if(is_null($order_by))
		{
			$sort_colum = array('alphabet' => 'title', 'priority' => 'prio', 'date' => 'pubdate');
			$order_by = array(
				$sort_colum[$this->config->get("photo_sort", "priority")],
				'ASC'
			);
		}
		
		$query = ActiveRecord::factory('Photogallery_Photo');
		
		$query->whereIn('album_id', $album_ids);
		
		if(!is_array(current($order_by)))
		{
			$order_by = array($order_by);
		}

		foreach($order_by as $ob)
		{
			if(2 == count($ob))$query->orderBy($ob[0], $ob[1]);
		}
		
		if(!is_null($limit)) $query->limit($limit, $offset);
		$res = $query->get();
		if(false != $count) $count = $query->getCount();
		return $res;
	}
	
	/**
	 * Makes it possible to turn an id into a Photogallery_Photo from the template
	 * @param int $id
	 * @return Photogallery_Photo
	 */
	public function getPhotoById($id)
	{
		return ActiveRecord::factory('Photogallery_Photo', $id);
	}
}
