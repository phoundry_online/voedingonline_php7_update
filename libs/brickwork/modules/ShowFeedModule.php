<?php

/*
* ShowFeedModule
*
* Function:
* Display rss feed
*
* @author		Adrie den Hartog	<adrie.denhartog@webpower.nl>
* @author		J�rgen Teunis	<jorgen.teunis@webpower.nl>
* @version      1.0.0
* @last edit	2007-08-10
*/

class ShowFeedModule extends Webmodule {

	/**
	 * @var boolean
	 * @acces protected
	 */
	protected $use_cache            = TRUE;

    /*
	* @var integer
	* @access private
	*/
	private $_config_shown_amount;

    /*
	* @var integer
	* @access private
	*/
	private $_current_page;

    /*
	* @var string
	* @access private
	*/
	private $_config_feed_url;

    /*
	* @var string
	* @access private
	*/
	private $_config_feed_name;

    /*
	* @var string
	* @access private
	*/
	private $_feed_link;

    /*
	* @var array
	* @access private
	*/
	private $_rssitems;

    /*
	* @var string (filename)
	* @access private
	*/
	private $_cache_file;

    /*
	* @var string 
	* @access private
	*/
	private $_config_target;

    /*
	* @var boolean
	* @access private
	*/
	private $_store_cache_file = false;

    /*
	* @var boolean
	* @access private
	*/
	private $_cache_used = false;

    /*
	* @var boolean
	* @access private
	*/
	private $_config_does_media = false;

    /*
	* @var integer
	* @access private
	*/
	private $_config_detail_page_id;

    /*
	* @var integer
	* @access private
	*/
	private $_config_feed_cachetime;

    /*
	* @var integer
	* @access private
	*/
	private $_config_amount_preopened_items;

    /*
	* @var integer
	* @access private
	*/
	private $_rssitem_amount;

	/**
	* System required constructor
	*/
	public function __construct($id, $cc){
		parent::__construct($id, $cc);

		return TRUE;
	}

	/**
	* System required constructor function
	*/
	protected function loadData() {


		// fetch config settings
	    $this->_config_shown_amount = (isset($this->config->item_amount)) ? (int)$this->config->item_amount : 5;
		$this->_config_feed_url = (isset($this->config->feed_url)) ? $this->config->feed_url : NULL;
		$this->_config_feed_name = (isset($this->config->name)) ? $this->config->name : NULL;
		$this->_config_feed_type = (isset($this->config->feed_type)) ? $this->config->feed_type : NULL;
		$this->_config_target = (isset($this->config->target)) ? $this->config->target : "_blank";
		$this->_config_feed_cachetime = (isset($this->config->cache_time) && (int)$this->config->cache_time>1) ? (int)$this->config->cache_time : 1;
		$this->_config_does_media = (isset($this->config->does_media)) ? ($this->config->does_media ? True : False) : False;
		$this->_config_detail_page_id = (isset($this->config->detail_page_id)) ? $this->config->detail_page_id : NULL;
		$this->_config_amount_preopened_items = (isset($this->config->amount_preopened_items)) ? $this->config->amount_preopened_items : 0;

		// Fetch the page if we got it
		if (isset($this->get['page'])) {
			$this->_current_page = (int) $this->get['page'];
		} else {
			$this->_current_page = 1;
		}

		$pageOffset = ($this->_current_page - 1) * $this->_config_shown_amount;

		// If we're to use the media player, check for a detailpage. If not stop the module.
		//if ($this->_config_does_media && !isset($this->_config_detail_page_id)) {
		//	return false;
		//}

		// set the name for a cachefile, always set it. even when not used.
		if(!@is_dir(TMP_DIR.'/rss_mod/')){
			if(!@mkdir(TMP_DIR.'/rss_mod/')){
				trigger_error('Unable to create cache dir', E_USER_NOTICE);
				return false;
			}
		}
		$this->_cache_file = TMP_DIR."/rss_mod/".$this->id.".cache";


		$download_feed = true;

		// check if cache exists 
		if(file_exists($this->_cache_file)){
			// ja: check nieuwer
			if ((time()-filemtime($this->_cache_file)) < ($this->_config_feed_cachetime*60)) {
				// ..use the cachefile
				// niks meer te doen, dat komt straks
				$download_feed = false;
			} else {
				// touch file to prevent other requests to download the feed.
				touch($this->_cache_file);
				clearstatcache();
			}
		}

		//
		if($download_feed){
			// determine wget command to download feed on background
			/*
			$wget_cmd = exec('which wget');
			if(empty($wget_cmd)){
				trigger_error('Unable to locate wget command', E_USER_NOTICE);
				return false;
			}
			*/
			
			$_tmp = @file_get_contents($this->_config_feed_url);
			
			if(!empty($_tmp)){
				if(!@file_put_contents($this->_cache_file, $_tmp)){
					trigger_error('Unable to write cache file', E_USER_NOTICE);
				}
			}
			/*
			$cmd = sprintf("%s %s -nc -o /dev/null -O %s && test -s %s && mv %s %s",
				$wget_cmd, 
				escapeshellarg($this->_config_feed_url),
				escapeshellarg($this->_cache_file.'.tmp'),	// -O option
				escapeshellarg($this->_cache_file.'.tmp'),	// test tmpp file size greater then zero
				escapeshellarg($this->_cache_file.'.tmp'),	// mv source file
				escapeshellarg($this->_cache_file),				// mv destination file
				escapeshellarg($this->_cache_file.'.tmp')		// remove tmp file
			);
			exec($cmd);
			*/
		}

		// retreive items
		if (file_exists($this->_cache_file) && filesize($this->_cache_file)>0) {

			// Initialize SimpleXML
			$xml = @simplexml_load_file($this->_cache_file);

			// xml parsing failed
			if(!$xml){
				return false;
			}

			// Choose a parser
			switch ($this->_config_feed_type) {
				case "RSS2" :
					$this->_rssitems = self::getFeedItems_RSS2($this->_config_shown_amount, $xml, $pageOffset);
					break;
				case "RSS1" :
					$this->_rssitems = self::getFeedItems_RSS1($this->_config_shown_amount, $xml, $pageOffset);
					break;
				case "ATOM" :
					$this->_rssitems = self::getFeedItems_ATOM($this->_config_shown_amount, $xml, $pageOffset);
					break;
				default :
					$this->_rssitems = self::getFeedItems_RSS2($this->_config_shown_amount, $xml, $pageOffset);
			}

			if (isset($this->_rssitems['amount'])) {
				$this->_rssitem_amount = $this->_rssitems['amount'];
				unset($this->_rssitems['amount']);
			}
			if (isset($this->_rssitems['feed_link'])) {
				$this->_feed_link = $this->_rssitems['feed_link'];
				unset($this->_rssitems['feed_link']);
			}
		} else {
			return false; // cachefile does not exist
		}

		if (count($this->_rssitems)) {
			return true;
		}
		else {
			return false;
		}

	}

	/**
	* System required __get
	*/
    public function __get($value){
		switch($value){
			case 'config_shown_amount':
				return $this->_config_shown_amount;	
				break;	
			case 'rssitem_amount':
				return $this->_rssitem_amount;	
				break;	
			case 'cache_used':
				return $this->_cache_used;	
				break;	
			case 'config_target':
				return $this->_config_target;	
				break;	
			case 'config_feed_url':
				return $this->_config_feed_url;	
				break;	
			case 'config_feed_name':
				return $this->_config_feed_name;	
				break;	
			case 'rssitems':
				return (is_array($this->_rssitems) ? (array)$this->_rssitems : $this->_rssitems);	
				break;	
			case 'config_does_media':
				return $this->_config_does_media;	
				break;	
			case 'config_detail_page_id':
				return $this->_config_detail_page_id;	
				break;	
			case 'config_amount_preopened_items':
				return $this->_config_amount_preopened_items;	
				break;	
			case 'current_page':
				return $this->_current_page;	
				break;	
			case 'feed_link':
				return $this->_feed_link;	
				break;					
			default:
				trigger_error('Deze Get-waarde bestaat niet: --> '.$value);
		}
	}

	/**
	* Retreive an x amount of rss feed items for display [RSS 2.0]
	*/
	public static function getFeedItems_RSS2 ($amount, $xml, $pageOffset) {

		// Init array
		$itemArray = array();

		// $i is used for count, $cnt for the offset
		$i=0;
		$cnt=0;

		// set global link of the feed
		if(!empty($xml->channel->link)){
			$itemArray['feed_link'] = $xml->channel->link;
		}

		$namespaces = $xml->getNamespaces(true);

		// Loop though
		foreach($xml->channel->item as $key=>$item) {

			//echo $mediaTag = $item->children($namespaces['media'])->flashlink;

			$cnt++;
			if ($cnt > $pageOffset) {
				++$i;
				if($i == ($amount+1)) {	break; }
				$itemArray[$i]['description'] = self::getProperEncoding((string) $item->description); // Only in RSS2, add to others..
				$itemArray[$i]['title'] = self::getProperEncoding((string) $item->title);

				if (isset($item->link)) {
					$itemArray[$i]['link'] = (string) $item->link;
				} else {
					if (count($namespaces) && !empty($namespaces['media'])) {
						$ns = $namespaces['media'];
						$url = $item->children($ns)->flashlink;
						$itemArray[$i]['link'] = $url;
					}
				}

				// The following is optional only for videostreams..
				if (isset($item->mediacontent['url'])) {
					$itemArray[$i]['preview'] = (string) $item->mediacontent['url'];
				} else {
					if (count($namespaces) && !empty($namespaces['media'])) {
						$ns = $namespaces['media'];
						$url = $item->children($ns)->still_image;
						$itemArray[$i]['preview'] = $url;
					}
				}

				if (isset($item->video['encoding'])) {
					$itemArray[$i]['mime'] = (string) $item->video['encoding'];
					if ($itemArray[$i]['mime'] == "") {
						$itemArray[$i]['mime'] = "automatic";
					}
				}
			}
	    }

		$itemArray['amount'] = count($xml->channel->item);

		return $itemArray;
	}

	/**
	 * encode in the right charset
	 *
	 * @param string $str
	 * @return string
	 */
	public function getProperEncoding($str){
		if(function_exists('htmlspecialchars_decode')){
			$str = html_entity_decode($str, ENT_QUOTES);
		}
		$str = str_replace('&apos;','\'', $str);
		
		if(strtolower(Framework::$PHprefs['charset']) == 'utf-8' && !self::isUTF8($str)){
			return utf8_encode($str);
		} elseif(strtolower(Framework::$PHprefs['charset']) != 'utf-8' && self::isUTF8($str)) {
			return utf8_decode($str);
		} 
		return $str;		
	}
	
	/**
	* Retreive an x amount of rss feed items for display [RSS 1.0]
	*/
	public static function getFeedItems_RSS1 ($amount, $xml, $pageOffset) {

		// Init array
		$itemArray = array();

		// $i is used for count, $cnt for the offset
		$i=0;
		$cnt=0;

		// set global link of the feed
		if(!empty($xml->channel->link)){
			$itemArray['feed_link'] = $xml->channel->link;
		}

		// Loop though
		foreach($xml->item as $key=>$item) {
			$cnt++;
			if ($cnt > $pageOffset) {
				++$i;
				if($i == ($amount+1)) {	break; }
				$itemArray[$i]['link'] = (string) $item->link;
				$itemArray[$i]['title'] = (string) $item->title;
			}
	    }

		$itemArray['amount'] = count($xml->item);

		return $itemArray;
	}

	/**
	* Retreive an x amount of rss feed items for display [ATOM]
	*/
	public static function getFeedItems_ATOM ($amount, $xml, $pageOffset) {

		// Init array
		$itemArray = array();

		// $i is used for count, $cnt for the offset
		$i=0;
		$cnt=0;

		// Loop though
		foreach($xml->entry as $key=>$item) {
			$cnt++;
			if ($cnt > $pageOffset) {
				++$i;
				if($i == ($amount+1)) {	break; }
				$itemArray[$i]['link'] = (string) $item->link['href'];
				$itemArray[$i]['title'] = (string) $item->title;
			}
	    }

		$itemArray['amount'] = count($xml->entry);

		return $itemArray;
	}
	
	
	public static function isUTF8($string)
	{
	    return (utf8_encode(utf8_decode($string)) == $string);
	}
}

?>