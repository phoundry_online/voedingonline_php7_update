<?php
/**
 * Standard Banner Module
 *
 * Code copied from the Banner Module implementation of BDU Kranten
 *
 * @author		Stefan Grootscholten	<stefan.grootscholten@webpower.nl>
 * @copyright	Web Power
 * @since		20 jan 2009
 * @version	1.0
 *
 * @link		http://projectwiki.office.webpower.nl/index.php/Brickwork:standaard_modules:BannerModule

CREATE TABLE `module_config_banner` (
  `page_content_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `max_banners` INT(10) UNSIGNED NOT NULL DEFAULT '2',
  `banner_group` INT(10) UNSIGNED DEFAULT NULL,
  `sort` VARCHAR(20) DEFAULT NULL,
  `adsense` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `lookup_site_banner` (
  `site_identifier` VARCHAR(20) NOT NULL DEFAULT '',
  `banner_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`site_identifier`,`banner_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `lookup_module_banner_group` (
  `banner_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `group_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_id`,`group_id`)
) DEFAULT CHARSET=utf8

CREATE TABLE `module_content_banner` (
  `id` INT(11) NOT NULL auto_increment,
  `name` VARCHAR(255) NOT NULL DEFAULT '',
  `image` VARCHAR(255) DEFAULT NULL,
  `image_url` VARCHAR(255) DEFAULT NULL,
  `url` VARCHAR(255) DEFAULT NULL,
  `popup` TINYINT(1) NOT NULL DEFAULT '0',
  `clickcounter` INT(11) NOT NULL DEFAULT '0',
  `custom_javascript` LONGTEXT,
  `width` INT(11) DEFAULT NULL,
  `height` INT(11) DEFAULT NULL,
  `bgcolor` VARCHAR(10) DEFAULT NULL,
  `online` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `offline` DATETIME DEFAULT NULL,
  `prio` INT(11) DEFAULT NULL,
  `mod_user` VARCHAR(255) NOT NULL DEFAULT '',
  `mod_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `file` VARCHAR(50) DEFAULT NULL,
  `param` VARCHAR(255) DEFAULT NULL,
  `token` VARCHAR(50) DEFAULT NULL,
  `adsense_client` VARCHAR(40) DEFAULT NULL,
  `adsense_slot` VARCHAR(20) DEFAULT NULL,
  `analytics` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_bannergroup` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;
 */

class BannerModule extends Webmodule
{
	/**
	 * The banners which will be shown on the website
	 *
	 * @var	Array
	 */
	public $banners;
	
	/**
	 * Constructor for the BannerModule
	 *
	 * @param	Integer				$id
	 * @param	ContentContainer	$cc
	 *
	 * @return	Boolean
	 * @throws	Exception in case of DB errors
	 */
	public function __construct($id, ContentContainer $cc)
	{
		parent::__construct($id, $cc);
		
		$this->banners = array();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Module#loadData()
	 */
	protected function loadData()
	{
		if (empty($this->content_table)) {
			$this->content_table = 'module_content_banner';
			//throw new Exception("Content table for " . __CLASS__ . " is not set.");
		}
		
		// Banner Module is called through the Module Content Handler
		// so that means we're processing a clicktrough
		if (Framework::$contentHandler instanceof ModuleContentHandler &&
			isset(Framework::$contentHandler->path[1]) &&
			is_numeric(Framework::$contentHandler->path[1])
		) {
			$this->_bannerClick(Framework::$contentHandler->path[1]);
		}
		
		$max_banners	= isset($this->config->max_banners)		? (int) $this->config->max_banners : 2;
		$group			= isset($this->config->banner_group)	? (int) $this->config->banner_group : null;
		$adsense		= isset($this->config->adsense)			? (bool) $this->config->adsense : true;
		$orderby		= isset($this->config->sort)			? $this->config->sort : 'RAND()';
		
		$this->banners	= Banner::getBanners($max_banners, $group, $this->content_table, $orderby, $adsense);
		return true;
	}
	
	/**
	 * Processes the click on a banner
	 * @param unknown_type $banner_id
	 * @return unknown_type
	 */
	protected function _bannerClick($banner_id)
	{
		$banner_id = (int) Framework::$contentHandler->path[1];
			
		$res = DB::iQuery(sprintf("SELECT
					b.id,
					b.url,
					b.clickcounter
				FROM %s b
				WHERE id = %d
				LIMIT 1",
			DB::quote($this->content_table),
			DB::quote($banner_id)
		));
		
		if($row = $res->first()) {
			
			// Increment the clicks by one
			$res = DB::iQuery(sprintf("
				UPDATE
					%s
				SET
					clickcounter = %d
				WHERE
					id = %d
				",
				DB::quote($this->content_table),
				$row->clickcounter + 1,
				$row->id
			));
			
			if($row->url) {
				trigger_redirect($row->url); // trigger_red does a exit;
			}
		}

		header("HTTP/1.0 404 Not Found", true, 404);
		die('404 Not Found');
	}
}
