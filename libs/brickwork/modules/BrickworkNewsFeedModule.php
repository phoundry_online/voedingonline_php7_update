<?php
/**
 * News Feed Module
 * 
 * Laad een BrickworkNewsModule in en toont diens newsberichten
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 0.0.1
 * @package News
 * 
 */
class BrickworkNewsFeedModule extends FeedModule
{
	/**
	 * Titel van de feed
	 * @var string
	 */
	public $title;

	/**
	 * SubTitel van de feed
	 * @var string
	 */
	public $description;

	/**
	 * Link van de feed
	 * @var string Url
	 */
	public $link;

	/**
	 * Wordt nog niet gebruikt
	 * @var int
	 */
	public $descriptionTruncSize = 50;

	/**
	 * Wordt nog niet gebruikt
	 * @var bool
	 */
	public $descriptionHtmlSyndicated;

	/**
	 * Een array met feed items die via de _get van de feedmodule op te halen is als $module->feed
	 * @todo totaal niet consistent met de bovengenoemde variabelen. Waarom opeens via _get?
	 * @var array
	 */
	protected $_items;

	/**
	 * LET OP!!! Totaal niet volgens de standaarden moet deze functie PUBLIC zijn!
	 */
	public function loadData()
	{
		$this->_items = array();
		
		if(!isset($this->params[0])) die();
		try {
			$res = DB::iQuery(sprintf("
				SELECT
					brickwork_site_structure.id,
					module_config_news.root_category,
					brickwork_module.class
				FROM
					module_config_news
				INNER JOIN
					brickwork_page_content
				ON
					module_config_news.page_content_id = brickwork_page_content.id
				INNER JOIN
					brickwork_module
				ON
					brickwork_module.code = brickwork_page_content.module_code
				INNER JOIN
					brickwork_page
				ON
					brickwork_page_content.page_id = brickwork_page.id
				INNER JOIN
					brickwork_site_structure
				ON
					brickwork_page.site_structure_id = brickwork_site_structure.id
				WHERE
					module_config_news.page_content_id = %d
				AND
					module_config_news.view = 'detail'
				LIMIT 1
			", (int) $this->params[0]
			));
		}
		catch (DB_Exception $e) {
			return true;
		}
		
		if($row = $res->first()) {
			$detail_page = $row->id;					
			$category = News::categoryFactory($row->root_category);
			$news_module_class = $row->class;
		} else {
			return true;
		}
		
		
		if(TESTMODE){
			$host = $_SERVER['HTTP_HOST'];
		} else {
			$host = Framework::$site->host;
		}
		
		$current_proto = (isset($_SERVER['HTTPS']) && 'on' == $_SERVER['HTTPS']) ? 'https' : 'http';
		$protocols = explode(',', Framework::$site->protocol);
		$protocol = in_array($current_proto, $protocols) ? $current_proto : first($protocols);
		
		$this->link = $protocol."://".$host."/page/".SiteStructure::getPathByStructureId($detail_page);
		$this->title = 'Nieuws - '.$category['name'];
		$this->description = $category['description'];
		
		
		
		// vul hier de $this->_items array
		$this->_items = array();		
		foreach($category->getAllArticles(News::RECENT, 25) as $newsitem) {
			$item = new FeedItem(); // De constructor neemt geen enkele parameter
			$item->id = $newsitem['id'];
			$item->guid = md5($item->id);
			$item->title = $newsitem['title'];
			$item->link = $this->link."?mod[".$news_module_class."][n]=".$newsitem['id'];
			$item->date = strtotime($newsitem['online_datetime']);
			// Voor de rest dus nog de link naar de detail pagina. Zie BDURSSFeedModule

			// Optional
			$item->description = $newsitem['intro']; // Dit wil je mischien nog truncate of strippen
			$item->source = $protocol."://".$host;
			$item->author = $newsitem['author'];

			$this->_items[] = $item;
		}

		// Al geven we hier 'Jorgen is gek' terug het maakt eigenlijk helemaal niets uit er gebeurt namelijk niets mee.
		// Return false haalt dus ook niets uit.
		return TRUE;
	}
}
