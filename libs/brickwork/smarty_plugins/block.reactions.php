<?php
/**
 * Smarty block function to enable the ReactionGroups
 *
 * @author		Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright	Web Power
 * @since		19 nov 2008
 *
 * Usage:
 * {reactions assign="reactiongroup" template="html/modules/blocks/reaction.tpl" relation_id=10 relation_name=web_artikel}{/reactions}
 * {reactions relation_id=$PAGE->id relation_name=Pages title=$PAGE->title}{/reactions}
 * 
 * @param string $title The title of the reactiongroup
 * @param int $relation_id The id of the relation
 * @param string $relation_name The name of the relation (default: 'default')
 * @param string $assign The variable name to which the reactiongroup is assigned within the template
 * @param string $template Template to use for the reactions (default: 'modules/blocks/reactions.tpl')
 */

function smarty_block_reactions($params, $content, &$smarty, &$repeat)
{
	// Set Defaults
	$title = '';
	$assign = 'reactiongroup';
	$relation_id = '';
	$relation_name = 'default';
	$template = isset($GLOBALS['WEBprefs']['reactions']['template']) ?
		$GLOBALS['WEBprefs']['reactions']['template'] :
		'modules/blocks/reactions.tpl';
	
	// Merge Params with defaults
	extract($params);
	
	if (true === $repeat) {
		// Opening tag
		$group = Reactions_Group::getByRelation(
			$relation_name, $relation_id, $title, true
		);
		
		$files = UploadContentHandler_File::getFilesAsArray(
			$group['id'], 'Reactionfiles'
		);
		
		$smarty->assign($assign, $group);
		$smarty->assign(
			$assign.'_post',
			Utility::arrayValue(
				$_SESSION, 'ReactionPosted_'.$group['id'], array()
			)
		);
		
		$smarty->assign(
			$assign.'_errors',
			Utility::arrayValue(
				$_SESSION, 'ReactionErrors_'.$group['id'], array()
			)
		);
	
		$smarty->assign($assign.'_files', $files);
		$smarty->assign($assign.'_params', $params);
	} else {
		// We are at the closing tag
		return !empty($content) ? $content : $smarty->fetch($template);
	}
}
