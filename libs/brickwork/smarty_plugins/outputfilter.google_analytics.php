<?php
/**
 * Google Analytics outputfilter
 * 
 * This outputfilter injects the google analytics pageTracker in the <head>
 * if the current site has a google_analytics config value
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @since 2009-09-02
 * 
 * @param string $tpl_source
 * @param Smarty $smarty
 * @return string
 */
function smarty_outputfilter_google_analytics($tpl_source, &$smarty)
{
	if((!defined('TESTMODE') || !TESTMODE) && isset(Framework::$site) &&
	  false !== ($index = strpos($tpl_source, '</head>'))) {

		$ga = Framework::$site->getConfig('google_analytics');
		if(!empty($ga)) {
			$keys = array();
			
			$json = json_decode($ga, true);
			if (is_array($json)) {
				foreach($json as $entry) {
					$keys[] = "_gaq.push(".json_encode($entry).");";
				}
			}
			else {
				foreach(explode(",", $ga) as $i => $key) {
					if($key = trim($key)) {
						if(0 === $i) { // First item
							$keys[] = sprintf(
								"_gaq.push(['_setAccount', '%s']);",
								$key
							);
						}
						else if(false !== strpos($key, "=")) {
							$key = explode("=", $key, 2);
							$keys[] = sprintf(
								"_gaq.push(['%s._setAccount', '%s']);",
								$key[0],
								$key[1]
							);
						}
					}
				}
				$keys[] = '_gaq.push(["_trackPageview"]);';
			}

			if($keys) {
				$str = '
<script type="text/javascript">
	var _gaq = _gaq || [];
	'.implode("\n", $keys).'
    (function() {
		var ga = document.createElement("script");
		ga.type = "text/javascript";
		ga.async = true;
		ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
		(document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(ga);
    })();
</script>
';
			
				return substr_replace($tpl_source, $str, $index, 0);
			}
		}
	}
	return $tpl_source;
}