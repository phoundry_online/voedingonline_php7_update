<?php
/**
 * Create Automatic Hyperlink in text
 */
function smarty_modifier_autolink($var) {
	return Utility::autolinkUrls($var, false);
}