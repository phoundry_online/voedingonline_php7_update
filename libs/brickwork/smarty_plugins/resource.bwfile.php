<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     resource.bwfile.php
 * Type:     resource
 * Name:     text
 * Purpose:  Eat raw html/text
 * -------------------------------------------------------------
 */
function smarty_resource_bwfile_source($tpl_name, &$tpl_source, &$smarty)
{
	static $loader = null;
	if (null === $loader) {
		$loader = Loader::getInstance()->getLoader('template');
	}
	
	if(!$fullpath = $loader->find($smarty->contentType.'/'.$tpl_name)) {
		$fullpath = $loader->find($tpl_name);
	}
	if($fullpath && is_file($fullpath)) {
		$tpl_source = file_get_contents($fullpath);
		return true;
	}
	return false;
}

function smarty_resource_bwfile_timestamp($tpl_name, &$tpl_timestamp, &$smarty)
{
	static $loader = null;
	if (null === $loader) {
		$loader = Loader::getInstance()->getLoader('template');
	}
	
	if(!$fullpath = $loader->find($smarty->contentType.'/'.$tpl_name)) {
		$fullpath = $loader->find($tpl_name);
	}
	if($fullpath && is_file($fullpath)) {
		$tpl_timestamp = filemtime($fullpath);
		return true;
	}
	return false;
}

function smarty_resource_bwfile_secure($tpl_name, &$smarty)
{
    // assume all templates are secure
    return true;
}

function smarty_resource_bwfile_trusted($tpl_name, &$smarty)
{
    // not used for templates
}
