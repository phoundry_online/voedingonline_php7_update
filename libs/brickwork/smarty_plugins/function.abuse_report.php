<?php

/**
 * Smarty function abuse_report
 * 
 * @author		Bart Lagerweij <bart.lagerweij>
 * @copyright	Web Power
 * @version		0.0.1
 *
 * Example:
 * 
 * {abuse_report title="Abuse message for posting #1345"}
 *
 * Optional:
 * Create a named anchor in your page and specify it using the hash param
 * This will give you the option to jump into the page to the correct position using the hash on the Url
 *
 * <a name="post_{record.id}">
 * ...
 * {abuse_report hash="post_`$record.id`" title="Abuse message for posting #`$record.id` - `$record.title`"}
 * 
 */

function smarty_function_abuse_report($params,&$smarty) {
	if (!empty($params)) {
		if (empty($params['hash'])) {
			$params['hash']= uniqid('x_'); 
		}
		$smarty->assign('params', $params);
	}
	$smarty->display('modules/blocks/abuse/link.tpl');
}
