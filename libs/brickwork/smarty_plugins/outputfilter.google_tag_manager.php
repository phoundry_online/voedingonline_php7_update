<?php
/**
 * Google Tag Manager outputfilter
 *
 * This outputfilter injects the Google Tag Manager code in the <body>
 *
 * @author Martijn van Maurik <martijn.van.maurik@phoundry.nl>
 *
 * @param string $tpl_source
 *
 * @return string
 */
function smarty_outputfilter_google_tag_manager($tpl_source)
{
    if (strstr($tpl_source, '<!-- GTM_DISABLE -->')) {
        return $tpl_source;
    }

    if (strstr($tpl_source, '<!-- Google Tag Manager -->') !== false) {
        return $tpl_source;
    }

    if ($gtm = Framework::$site->getConfig('google_tag_manager', false)) {
        $gtmCode = <<<'EOF'
%1$s
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=%2$s"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','%2$s');</script>
<!-- End Google Tag Manager -->
EOF;

        if (strstr($tpl_source, '<!-- GTM_ALT_PLACEMENT -->')) {
            return str_replace('<!-- GTM_ALT_PLACEMENT -->', trim(sprintf($gtmCode, '', $gtm)), $tpl_source);
        } else {
            $tpl_source = preg_replace_callback('/<body[^>]*>/i', function ($matches) use ($gtmCode, $gtm) {
                return sprintf($gtmCode, trim($matches[0]), $gtm);
            }, $tpl_source, 1);
        }
    }

    return $tpl_source;
}
