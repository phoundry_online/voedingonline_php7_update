<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     resource.mailtemplate.php
 * Type:     resource
 * Name:     mailtemplate
 * Purpose:  Render a mailtemplate as smarty template
 * -------------------------------------------------------------
 */
function smarty_resource_mailtemplate_source($tpl_name, &$tpl_source, &$smarty)
{
	if(!empty($tpl_name)) {
		$args = explode(":", $tpl_name);
		$type = $args[0];
		$key = array($args[1], $args[2]);
		
		$template = ActiveRecord::factory("Mailtemplate", $key);
		if ($template->isLoaded()) {
			if ($type === 'body') {
				$tpl_source = $template['body'];
				return true;
			}
			else if ($type === 'subject') {
				$tpl_source = $template['subject'];
				return true;
			}
		}
	}
	return false;
}

function smarty_resource_mailtemplate_timestamp($tpl_name, &$tpl_timestamp, &$smarty)
{
	$tpl_timestamp = time();
	return true;
}

function smarty_resource_mailtemplate_secure($tpl_name, &$smarty) { return true; }
function smarty_resource_mailtemplate_trusted($tpl_name, &$smarty) {}
