<?php
/**
 * Smarty function to display snippets
 *
 * @author		Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright	Web Power, http://www.webpower.nl
 * @since		21-07-2009
 *
 * Usage:
 * {snippet name=footer site_identifier=default table="module_content_snippet"}
 */
function smarty_function_snippet($params, &$smarty)
{
	$name = false;
	$site_identifier = null;
	$table = null;
	extract($params);
	$html = '';
	if($name) {
		$html = $smarty->fetch('snippet:'.(json_encode(compact('name', 'site_identifier', 'table'))));
	}
	
	return $html;
}
