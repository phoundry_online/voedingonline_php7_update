<?php
function smarty_prefilter_script($source, &$smarty)
{
	global $PHprefs, $WEBprefs;
	if (empty($WEBprefs['ScriptContentHandler']['prefilter'])) {
		return $source;
	}
	
	$dir = $PHprefs['uploadDir']."/scriptContentHandler";
	$url = $PHprefs['uploadUrl']."/scriptContentHandler";
	if (!file_exists($dir) && !mkdir($dir, 0777, true)) {
		throw new Exception("Could not create storage dirctory");
	}
	
	$service = new Script_Service(
		Loader::getInstance()->getLoader('script'),
		Framework::$site->identifier
	);
	$service->setStorage(new File_Storage_Local($dir, $url));
	
	return $service->replaceScriptUrls($source);
}
