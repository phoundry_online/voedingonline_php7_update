<?php
function smarty_modifier_script($files, $type)
{
	static $service = null;
	
	if (null === $service) {
		global $PHprefs;
		$dir = $PHprefs['uploadDir']."/scriptContentHandler";
		$url = $PHprefs['uploadUrl']."/scriptContentHandler";
		if (!file_exists($dir) && !mkdir($dir, 0777, true)) {
			throw new Exception("Could not create storage dirctory");
		}
		
		$service = new Script_Service(
			Loader::getInstance()->getLoader('script'),
			Framework::$site->identifier
		);
		$service->setStorage(new File_Storage_Local($dir, $url));
	}
	
	$files = explode(",", $files);
	
	return $service->getStorageUrl($type, $files, true, true);
}
