<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     resource.php.php
 * Type:     resource
 * Name:     text
 * Purpose:  Eat raw html/text
 * -------------------------------------------------------------
 */
function smarty_resource_text_source($tpl_name, &$tpl_source, &$smarty)
{
	if(!empty($tpl_name))
	{
		$tpl_source = $tpl_name;
		return true;
	}
	return false;
}

function smarty_resource_text_timestamp($tpl_name, &$tpl_timestamp, &$smarty)
{
	$tpl_timestamp = time();
	return true;
}

function smarty_resource_text_secure($tpl_name, &$smarty)
{
    // assume all templates are secure
    return true;
}

function smarty_resource_text_trusted($tpl_name, &$smarty)
{
    // not used for templates
}
