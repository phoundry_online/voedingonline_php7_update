<?php
/**
 * Smarty function for login form
 *
 * @author		Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @copyright	Web Power
 * @since		26-06-2009
 *
 * Usage:
 * {authorization template='mylogin.tpl' show_always=1}
 */
function smarty_function_authorization($params, &$smarty)
{
	if(Features::brickwork('authorization')) {
		$show		= (isset($params['show_always']) && $params['show_always']);
		$template	= !empty($params['template']) ? $params['template'] : 'modules/blocks/authorization.tpl';
		
		if($show ||
			(
				Framework::$contentHandler instanceof PageContentHandler 
				&& Framework::$contentHandler->page->needsAuthentication
				&& !Framework::$contentHandler->page->isAuthenticated
			)
		){
			$auth = Auth::singleton();
			$smarty->assign('authorization', array(
				'errors'	=> $auth->errors,
				'user'		=> $auth->user,
				'welcome'	=> (
					$auth->user->isLoggedIn() &&
					Utility::isPosted() &&
					isset($_POST['authorization']['user'], $_POST['authorization']['password'])
				)
			));
			return $smarty->fetch($template);
		}
	}
	return null;
}
