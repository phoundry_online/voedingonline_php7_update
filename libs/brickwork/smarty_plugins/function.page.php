<?php
/**
 * Page metadata helper
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2011 Web Power BV, http://www.webpower.nl
 *
 * Usage:
 * {page title=Titel breadcrumb=Breadcrumb_title link=breadcrumb_link}
 */
function smarty_function_page($params, &$smarty)
{
	$page = $smarty->get_template_vars('PAGE');
	if (isset($params['title'], $page->title_array) &&
			is_array($page->title_array)) {
		$page->title_array[] = $params['title'];
	}
	
	if (isset($params['breadcrumb'], $page->breadcrumb) &&
			is_array($page->breadcrumb)) {
		$new_crumb = null;
		$new_crumb->title = $params['breadcrumb'];
		if (isset($params['link'])) {
			$new_crumb->link = $params['link'];
		}
		
		$page->breadcrumb[] = $new_crumb;
	}
	return '';
}
