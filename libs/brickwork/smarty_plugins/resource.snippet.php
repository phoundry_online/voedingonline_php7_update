 <?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     resource.snippet.php
 * Type:     resource
 * Name:     snippet
 * Purpose:  Serve a snippet
 * -------------------------------------------------------------
 */
function smarty_resource_snippet_source($tpl_name, &$tpl_source, &$smarty)
{
	if(!empty($tpl_name)) {
		$params = json_decode($tpl_name, true);
		
		if(!empty($params['name'])) {
			$snippet = SnippetModule::getSnippetByName(
				$params['name'],
				Utility::arrayValue($params, 'site_identifier'),
				Utility::arrayValue($params, 'table')
			);
			
			if($snippet) {
				$tpl_source = $snippet->code;
				return true;
			}
		}
	}
	return false;
}

function smarty_resource_snippet_timestamp($tpl_name, &$tpl_timestamp, &$smarty)
{
	if(!empty($tpl_name)) {
		$params = json_decode($tpl_name, true);
		
		if(!empty($params['name'])) {
			$snippet = SnippetModule::getSnippetModDatetimeByName(
				$params['name'],
				Utility::arrayValue($params, 'site_identifier'),
				Utility::arrayValue($params, 'table')
			);
							
			if($snippet) {
				$tpl_timestamp = strtotime($snippet->mod_datetime);
				return true;
			}
		}
	}
	
	return false;
}

function smarty_resource_snippet_secure($tpl_name, &$smarty)
{
    // assume all templates are secure
    return true;
}

function smarty_resource_snippet_trusted($tpl_name, &$smarty)
{
    // not used for templates
}
 