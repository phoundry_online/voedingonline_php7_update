<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once INCLUDE_DIR.'/classes/SiteStructure.class.php';
	require_once INCLUDE_DIR.'/functions/autoloader.include.php';
	require_once 'WebPower/Smarty/Smarty.class.php';
	// defun, recursive functions
	require_once 'WebPower/Smarty/plugins/compiler.fun.php';


	checkAccess();

	$site_identifier = !empty($_GET['site_identifier']) ? $_GET['site_identifier'] : '';
	if(empty($site_identifier)){
		trigger_error('No site selected', E_USER_ERROR);
	}
	$sitestructure = SiteStructure::getSiteStructure($site_identifier); 
	$stack = SiteStructure::getSiteStack($site_identifier);

	// collect all structure id's and determine whats connected
	$structure_ids = array();
	foreach($stack as $item){
		$structure_ids[] = (int)$item->id;
	}
	$pages = SiteStructure::getPages($structure_ids);
	$urls  = SiteStructure::getUrls($structure_ids);

	$menu_shortcuts = array();
	$menu = array();
	
	$invisible_parents = array();

	foreach($stack as $item){
		if(!isset($urls[$item->id]) && !isset($pages[$item->id])){
			continue;
		}


		if(!empty($pages[$item->id])){
			$url = '/page/'.SiteStructure::getPathByStructureId($item->id, $site_identifier);
			$target = '_self';
		} else if(!empty($urls[$item->id])) {
			$url = $urls[$item->id]['link'];
			$target = $urls[$item->id]['target'];
		}
		if(empty($item->parent)){
			$m = new Menu($item->name,$url,$item->name,0);
			$m->id = $item->id;
			$m->target = $target;
			$m->selected = $item->isActive;
			$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
			$menu[$item->id] = $m;
			$menu_shortcuts[$item->id] = $m;
			$sitemap->items[] = $m;
		} else {
			$m = new Menu($item->name,$url,$item->name,1);
			$m->id = $item->id;
			$m->target = $target;
			$m->selected = $item->isActive;
			$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
			$menu_shortcuts[$item->parent]->items[] = $m;
			$menu_shortcuts[$item->id] = $m;
		}

	}

	$tpl = new Smarty();
	$tpl->template_dir   = '.';
	$tpl->compile_dir   = TMP_DIR.'/templates_c/';
	$tpl->cache_dir     = false;


	$tpl->register_compiler_function('fun', 'smarty_compiler_fun');
	$tpl->register_compiler_function('/defun', 'smarty_compiler_defun_close');
	$tpl->register_postfilter('smarty_postfilter_defun');


	$tpl->assign('phoundryUrl', $PHprefs['url'] );
	$tpl->assign('menu', $menu);
	$tpl->display('link_intern.tpl');

?>

