<?php
	header('Content-type: application/x-javascript');
?>

function getURIParameterValue(uri, parameterName)
{
  var queryStr = uri.split("?")[1];
  var params = queryStr.split("&");
  for (var i=0; i < params.length; i++)
  {
    var keyValue = params[i].split("=");
    if(keyValue[0] == parameterName)
    {
      return keyValue[1];
    }
  } 
  return "";
}

PhoundryEditor.prototype.linkIntern=function() {
	var A=[];
	var site_identifier = getURIParameterValue(self.location.href,'site_identifier');
	if(site_identifier == ''){
		return;
	}
	var url = '<?= $PHprefs['url'] ?>/custom/Inputtypes/link_intern.php?site_identifier='+site_identifier+'&'+Math.random();
	$.fn.InlinePopup({width:400,height:300,title:'Link intern',url:url,modal:true,event:null});
}

PhoundryEditor.prototype.linkInternText = function(link){
   if($.browser.msie){
      this.saveCursor();
      var el = this._tempLink();
      el.href = link;
   } else {
      this.insertHTML('<a href="' + link + '">', '</a>', 1);
   }
}
