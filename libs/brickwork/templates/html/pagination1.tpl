{if $pagination->prev_page_items > 0}
	<a href="?{$pagination->qsPart()|qsmod:$pagination->prev_page}">&laquo;&nbsp;&nbsp;</a>
{/if}
{if $pagination->pages > 1}
	<span>
	{section name=pagination start=0 loop=$pagination->pages}
	{if $smarty.section.pagination.index==$pagination->current_page}
		{$smarty.section.pagination.index+1}
	{else}
		<a href="?{$pagination->qsPart()|qsmod:$smarty.section.pagination.index}">{$smarty.section.pagination.index+1}</a>
	{/if}
	{/section}
	</span>			
{/if}
{if $pagination->next_page_items > 0}
	<a href="?{$pagination->qsPart()|qsmod:$pagination->next_page}">&nbsp;&nbsp;&raquo;</a>
{/if}
