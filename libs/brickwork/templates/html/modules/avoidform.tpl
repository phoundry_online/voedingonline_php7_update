{assign var="form" value=$module->form}

{if $module->hasErrors === FALSE && $smarty.server.REQUEST_METHOD == 'POST'}
	<div><h1>Bedankt</h1></div>
	{$module->config->confirm_message}
	
	{assign var=ga value=$SITE->getConfig('google_analytics')|trim}
	{if $ga}
	<script type="text/javascript">
	<!--//--><![CDATA[//><!--
	if(typeof(_gaq) != 'undefined') {ldelim}
	    _gaq.push(['_trackEvent', 'Formuliergenerator', 'Submit', '{$form->name}']);
	{rdelim}
	else if(typeof(pageTracker) != 'undefined') {ldelim}
	    pageTracker._trackEvent('Formuliergenerator', 'Submit', '{$form->name}');
	{rdelim}
	//--><!]]>
	</script>
	{/if}
{else}
	{if $module->hasErrors}
	<div>
	<h1>Er zijn fouten opgetreden</h1>
		<p>De volgende velden zijn niet (correct) ingevuld:
		<ol>
		{section loop=$form->errors name=err}
			<li>{$form->errors[err]->errstr}</li>
		{/section}
		</ol>
	</div>
	<br />
	{/if}

	<div>
	<h1>{$form->title}</h1>
	<br />
	{if $form->header != ''}
	<div class="formHeader">
		{$form->header|htmlspecialchars|nl2br}
	</div>
	{/if}

	<form name="{$form->name}" action="{$form->action}" method="{$form->method}">	
	{foreach from=$form->fields item=field}
		{if is_a($field, 'CursorField')}
			<input type="hidden" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|htmlspecialchars}" />
		{elseif is_a($field, 'HiddenField')}
			<input type="hidden" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|htmlspecialchars}" />
		{elseif is_a($field, 'PhoneField')}
			<div>
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<input type="text" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|htmlspecialchars}" class="Field PhoneField{if $field->required} required{/if}" />
				{/if}
			</div>
		{elseif is_a($field, 'YearField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<select name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" class="Field YearField{if $field->required} required{/if}">
						{math equation="x - y" x=$field->value y=50 assign="min"}
						{math equation="x + y" x=$field->value y=50 assign="max"}
						{section name=foo start=$min loop=$max step=1}
							<option value="{$smarty.section.foo.index|htmlspecialchars}"{if $field->value == $smarty.section.foo.index}selected="selected"{/if}>{$smarty.section.foo.index|htmlspecialchars}</option>
						{/section}
					</select>
				{/if}
			</div>
		{elseif is_a($field, 'DateField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}[Day]" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					{html_select_date prefix='' field_array=$field->formFieldName field_order='DMY' time=$field->currentValue->timestamp start_year='-100' all_extra="class='Field DateField'"}
				{/if}
			</div>
		{elseif is_a($field, 'FileField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<input type="hidden" name="MAX_FILE_SIZE" value="{$field->maxFileSize|htmlspecialchars}" />
					<input type="file" accept="jpg" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" class="Field FileField{if $field->required} required{/if}" />
				{/if}
			</div>
		{elseif is_a($field, 'PhotoField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<input type="file" accept="jpg" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" class="Field PhotoField{if $field->required} required{/if}" />
				{/if}
			</div>
		{elseif is_a($field, 'NumberField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<input type="text" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|htmlspecialchars}" class="Field NumberField{if $field->required} required{/if}" />
				{/if}
			</div>
		{elseif is_a($field, 'RadioField')}
			<div style="margin-top:5px;">
				<label style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left;" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<div style="float:left">
						{foreach from=$field->options item=option}
							<input type="radio" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}_{$option->name|htmlspecialchars}" value="{$option->value|htmlspecialchars}" {if $option->selected}checked="checked"{/if} />
							<label for="{$field->id|htmlspecialchars}_{$option->name|htmlspecialchars}">{$option->value|htmlspecialchars}</label><br />
						{/foreach}
					</div>
					<br clear="all" />
				{/if}
			</div>
		{elseif is_a($field, 'CheckboxField')}
			<div style="margin-top:5px;">
				<label style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|default:$field->defaultValue|htmlspecialchars}<br />
				{else}
					<div style="margin-top:5px;" style="display: table-cell;">
					{foreach from=$field->options item=option}
						<input type="checkbox" name="{$field->formFieldName|htmlspecialchars}[{$option->name|htmlspecialchars}]" id="{$field->id|htmlspecialchars}_{$option->name|htmlspecialchars}" value="{$option->value|htmlspecialchars}" {if $option->selected}checked="checked"{/if} class="Field CheckField{if $field->required} required{/if}" onChange="{if $field->required}{literal}if (this.checked) { document.forms['{/literal}{$form->name}{literal}'].elements['submitThisForm'].disabled=false; } else { document.forms['{/literal}{$form->name}{literal}'].elements['submitThisForm'].disabled=true; }{/literal}{/if}" />
						<label for="{$field->id|htmlspecialchars}_{$option->name|htmlspecialchars}">{$option->value|htmlspecialchars}</label><br />
						{if $field->required && !$option->selected}<script>disable_submit = 1;</script>{/if}
					{foreachelse}
						Geen keuzes beschikbaar
					{/foreach}
					</div>
				{/if}
			</div>
		{elseif is_a($field, 'MultiSelectField')}
			{* @TODO: Eigenschappen van MultiSelect field (size enz.)  dit wordt niet gebruikt daar een selectfield zelf multiple kan zijn *}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float:left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}<sup>1</sup></label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<select name="{$field->formFieldName|htmlspecialchars}[]" id="{$field->id|htmlspecialchars}" class="Field SelectField{if $field->required} required{/if}" multiple="multiple" size="3">
						{foreach from=$field->options item=option}
							<option value="{$option->name|htmlspecialchars}"{if $option->selected} selected="selected"{/if}>{$option->value|htmlspecialchars}</option>
						{/foreach}
					</select>
					<!-- {$field->selectedIndex|htmlspecialchars} -->
				{/if}
			</div>
		{elseif is_a($field, 'DMDSubscribeField')} 
		<div style="margin-top:5px;">
			<div style="margin: 5px 0 5px 0;">
			{*<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left; width: 150px;" title="{$field->info|htmlspecialchars}">{$field->label}{if $field->required}<sup>*</sup>{/if}</label>*}
			{if $field->readonly}
				{$field->value|default:$field->defaultValue|htmlspecialchars}<br />
			{else}
				{foreach from=$field->options item=option}
					<input type="checkbox" name="{$field->formFieldName|htmlspecialchars}[{$option->name|htmlspecialchars}]" id="{$field->id|htmlspecialchars}_{$option->name|htmlspecialchars}" value="{$option->value|htmlspecialchars}" {if $option->selected}checked="checked"{/if} class="Field CheckField{if $field->required} required{/if}" />
					<label for="{$field->id|htmlspecialchars}_{$option->name|htmlspecialchars}">{$option->value|htmlspecialchars}</label><br />
				{foreachelse}
					Geen keuzes beschikbaar
				{/foreach}
	
				<!-- {$field->selectedIndex|htmlspecialchars} -->
			{/if}
			</div>
		</div>
		{elseif is_a($field, 'SelectField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|default:$field->defaultValue|htmlspecialchars}<br />
				{else}
					<select name="{$field->formFieldName|htmlspecialchars}{if $field->multiple == TRUE}[]{/if}" id="{$field->id|htmlspecialchars}" class="Field SelectField{if $field->required} required{/if}" size="{$field->field->extra.size}"{if $field->multiple == TRUE} multiple="multiple"{/if}>
						{if $field->required}
							<option value="">-- Maak een keuze --</option>
						{/if}
						{foreach from=$field->options item=option}
							<option value="{$option->name|htmlspecialchars}"{if $option->selected} selected="selected"{/if}>{$option->value|htmlspecialchars}</option>
						{/foreach}
					</select>
					<!-- {$field->selectedIndex|htmlspecialchars} -->
				{/if}
			</div>
		{elseif is_a($field, 'PasswordField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title={$field->info|htmlspecialchars}>{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<input type="password" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|default:$field->defaultValue|htmlspecialchars}" class="Field PasswordField{if $field->required} required{/if}" size="{$field->field->extra.size}" maxlength="{$field->field->extra.maxlength}" />
				{/if}
			</div>
		{elseif is_a($field, 'IBANField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<input type="text" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|htmlspecialchars}" class="Field IBANField{if $field->required} required{/if}" />
				{/if}
			</div>
		{elseif is_a($field, 'EmailField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|default:$field->defaultValue|htmlspecialchars}<br />
				{else}
					<input type="text" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|default:$field->defaultValue|htmlspecialchars}" class="Field EmailField{if $field->required} required{/if}" size="{$field->field->extra.size}" maxlength="{$field->field->extra.maxlength}" />
				{/if}
			</div>
		{elseif is_a($field, 'FreetextField')}
			<div>
				{if $field->field->extra.showLabel}
				<br clear="all" />
					<strong>{$field->label|htmlspecialchars}</strong>
				{/if}
				<strong>{$field->defaultValue|htmlspecialchars|nl2br}</strong><br clear="all" />
			</div>
		{elseif is_a($field, 'CaptchaField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					<input type="hidden" name="{$field->formFieldName|htmlspecialchars}" value="{$field->value}" />
					goedgekeurd
				{else}
				<div style="margin-top:5px;" style="float: left;">
					<img src="/index.php/captcha?type=CaptchaField&amp;field_id={$field->id|md5}&amp;bg=ffff33" alt="Neem deze code over" onclick="this.src = this.src.replace(/&regenerate=\d+/,'')+'&amp;regenerate='+Math.random().toString().replace('.','');" style="margin: 1px solid black; padding: 0 0 2px 0"/><br />
					<input type="text" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|htmlspecialchars}" {if $field->disabled}disabled="disabled"{/if} class="Field TextField{if $field->required} required{/if}" />
				</div>
				<br clear="all" />
				{/if}
			</div>
		{elseif is_a($field, 'TextareaField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					<div style="margin-top:5px;" style="margin: 0px; padding: 0px; margin-left: 120px">{$field->value|htmlspecialchars|nl2br}</div><br />
				{else}
					<textarea name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" class="Field TextareaField{if $field->required} required{/if}" rows="{$field->field->extra.rows}" cols="{$field->field->extra.columns}">{$field->value|default:$field->defaultValue|htmlspecialchars}</textarea>
				{/if}
			</div>
		{else is_a($field, 'TextField')}
			<div style="margin-top:5px;">
				<label for="{$field->id|htmlspecialchars}" style="{if strlen($field->info) > 0}cursor: help;{/if} width: 120px; float: left" title="{$field->info|htmlspecialchars}">{$field->label|htmlspecialchars}:{if $field->required}<sup>*</sup>{/if}</label>
				{if $field->readonly}
					{$field->value|htmlspecialchars}<br />
				{else}
					<input type="text" name="{$field->formFieldName|htmlspecialchars}" id="{$field->id|htmlspecialchars}" value="{$field->value|default:$field->defaultValue|htmlspecialchars}" {if $field->disabled}disabled="disabled"{/if} class="Field TextField{if $field->required} required{/if}" size="{$field->field->extra.size}" maxlength="{$field->field->extra.maxlength}" />
				{/if}
			</div>
		{/if}
	{/foreach}
	<br clear="all" />
	<table width="100%">
		{* foreach from=$form->fields item=field}
			{if $field->field->required}
				{assign var="required" value="required"}
			{else}
				{assign var="required" value=""}
			{/if}

			{assign var="extra" value=$field->field->extra}
			{if $field->value != ''}
				{assign var="value" value=$field->value}
			{elseif !empty($extra.value)}
				{assign var="value" value=$extra.value}
			{else}
				{assign var="value" value=""}
			{/if}

			{if $field->field->info != ''}
				<tr><td></td><td>{$field->field->info|htmlspecialchars}</td></tr>
			{/if}

			{* textareafield is afgeleid van textfield  dus die moet voor }
			{if is_a($field, 'TextareaField')}
				<tr>
					<td valign="top"><label style="font-weight:bold;" for="{$field->id}">{$field->label}:</label></td>
					<td><textarea name="{$field->formFieldName}" id="{$field->id}" rows="{$extra.rows|default:4}" cols="{$extra.columns|default:30}" wrap="{$extra.wrap|default:'soft'}">{$value|htmlspecialchars}</textarea></td>
				</tr>
			{elseif is_a($field, 'TextField') OR is_a($field, 'EmailField')}
				<tr>
					<td><label style="font-weight:bold;" for="{$field->id}">{$field->label}:</label></td>
					<td><input type="text" class="{$required}" name="{$field->formFieldName}" id="{$field->id}" {if !empty($extra.size)}size="{$extra.size}"{/if} {if !empty($extra.maxlength)}maxlength="{$extra.maxlength}"{/if} value="{$value|htmlspecialchars}" /></td>
				</tr>

			{elseif is_a($field, 'PasswordField')}
				<tr>
					<td><label style="font-weight:bold;" for="{$field->id}">{$field->label}:</label></td>
					<td><input type="password" name="{$field->formFieldName}" id="{$field->id}" {if !empty($extra.size)}size="{$extra.size}"{/if} {if !empty($extra.maxlength)}maxlength="{$extra.maxlength}"{/if} /></td>
				</tr>
			{elseif is_a($field, 'HiddenField')}
				<input type="hidden" name="{$field->formFieldName}"  value="{$value|htmlspecialchars}" />
			{elseif is_a($field, 'RadioField')}
				<tr>
					<td valign="top"><label style="font-weight:bold;" for="{$field->id}">{$field->label}:</label></td>
					<td>{foreach from=$extra.options key=key item=item }
						<input type="radio" name="{$field->formFieldName}" id="radio_{$key}" value="{$key|htmlspecialchars}"{if $value == $key} checked="checked"{/if} /> <label for="radio_{$key}">{$item|htmlspecialchars}</label><br />
					{/foreach}</td>
				</tr>
			{elseif is_a($field, 'SelectField')}
				{* Avoidform heeft eigen manier van options onthouden -- wordt momenteel niet via
				   de mogelijkheden van SelectField gedaan }
				<tr>
					<td{if $extra.multiple == 'true' } valign="top"{/if}><label style="font-weight:bold;" for="{$field->id}">{$field->label}:</label></td>
					<td><select name="{$field->formFieldName}"{if $extra.multiple == 'true' } multiple="multiple"{/if} {if $extra.size > 1}size="{$extra.size}"{/if}>{foreach from=$field->options item=option}
						<option value="{$option->value}"{if $value == $option->value} selected="selected"{/if}>{$option->name}</option>
					{/foreach}</select></td>
				</tr>
			{elseif is_a($field, 'CheckboxField')}
				<tr>
					<td><label style="font-weight:bold;">{$field->label}:</label></td>
					<td><input type="checkbox" name="{$field->formFieldName}" id="{$field->id}" {if $field->value == $extra.value OR ($smarty.server.REQUEST_METHOD != 'POST' AND $extra.checked)}checked="checked"{/if} value="{$extra.value|htmlspecialchars}" /></td>
				</tr>
			{/if}
		{/foreach *}
			<tr>
				<td colspan="2" align="center"><input type="submit" value="Verzend" name="submitThisForm"/></td>
			</tr>
		</table>
		</form>

		{if $form->footer != ''}
		<div>
			<br clear="all" />{$form->footer|htmlspecialchars}<br clear="all" /><br />
		</div>
		{/if}
	</div>
{/if}

{literal}
<script>
if (disable_submit==1) {
	{/literal}
	document.forms['{$form->name}'].elements['submitThisForm'].disabled=true;
	{literal}
}
</script>
{/literal}