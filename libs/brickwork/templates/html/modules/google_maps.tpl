{if !empty($module->api_key)}
<!-- stack name="javascript_src" assign="google_maps" -->
<script type="text/javascript" src="http://maps.google.com/maps?file=api&amp;v=2&amp;key={$module->api_key}"></script>
<!-- /stack -->
<!-- stack name="css" assign="google_maps" -->
{literal}
.google_map .gmnoprint p
{
	margin: 0;
}
{/literal}
<!-- /stack -->
<!-- stack name="css" -->
#module_{$module->id} div.google_map {ldelim}
	{if !empty($module->config->width)}width: {$module->config->width}px; {/if}
	height: {$module->config->height|default:500}px;
{rdelim}
<!-- /stack -->
<!-- stack name="javascript" -->
jQuery(function($){ldelim}
	var markers = ({$module->markers|@json_encode}),
	map_div = $('#module_{$module->id} div.google_map');
{literal}
	if (typeof(GBrowserIsCompatible)!= "function") {
		map_div.html({/literal}{$module->config->cannot_connect|default:"'<p>Google Maps</p><p>Momenteel niet bereikbaar.</p>'"|@json_encode}{literal});
		return;
	}

	function createMarker(latlng, icon, textmsg) {
		var marker = new GMarker(latlng, { icon: icon });
	    GEvent.addListener(marker, "click", function() { marker.openInfoWindowHtml(textmsg); });
		return marker;
	}
	
	function fitMap(map, points)
	{
	   var bounds = new GLatLngBounds();
	   for (var i=0; i< points.length; i++) {
	      bounds.extend(points[i]);
	   }
	   map.setZoom(map.getBoundsZoomLevel(bounds));
	   map.setCenter(bounds.getCenter());
	}
	
	if (GBrowserIsCompatible()) {
		var map = new GMap2(map_div.get(0)),
		geocoder = new GClientGeocoder();
		map.addControl(new G{/literal}{$module->config->zoomsize|default:'Small'}{literal}MapControl());
		map.addControl(new GMapTypeControl());
		{/literal}{if !isset($module->config->wheel_zoom) || $module->config->wheel_zoom}
		map.enableScrollWheelZoom();
		{/if}{literal}
		map.setMapType({/literal}G_{$module->config->maptype|default:'NORMAL'|upper}_MAP{literal});
		map.setCenter(new GLatLng(52.12615, 5.56562), 13);

		var icon = new GIcon();
		icon.image = "/brickwork/pics/marker.png";
		icon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
		icon.iconSize = new GSize(20, 34);
		icon.shadowSize = new GSize(30, 34);
		icon.iconAnchor = new GPoint(10, 34);
		icon.infoWindowAnchor = new GPoint(7, 1);
		
		var latlngs = [];
		$.each(markers, function(){
			var latlng = new GLatLng(this.lat, this.lng);
			map.addOverlay(createMarker(latlng, icon, this.textmsg));
			latlngs.push(latlng);
		});
				
		fitMap(map, latlngs);
	}
});
{/literal}
<!-- /stack -->
<div class="google_map"></div>
{/if}