{foreach name=deeplink  from=$module->items key=key item=item}
	<div class="item">
		<div class="title">
			<a href="{$item.url}" title="{$item.title|htmlspecialchars}">{$item.title|htmlspecialchars}</a>
		</div>
		<div class="description">{$item.description}</div>
		<div class="more">
		<a href="{$item.url}">&raquo;&raquo;</a>
		</div>
	</div>
{/foreach}