{******** DETAIL ********}
{if $module->view == 'detail'}
	{if $module->article instanceof News_Article}
		<div class="detail phoundry_module_news_article|{$module->article.id}">
			<h1>Nieuws</h1>
			<h2>{$module->article.title|htmlspecialchars}</h2>
			<div class="date">{$module->article.create_datetime|date_format:"H:i:s d-m-Y"}</div>
			<div>
				{if $module->article.image != ''}
				<div class="image">
					<a href="/uploaded{$module->article.image}" target="_blank">
						<img alt="" src="/index.php/image/news_thumb/{$module->article.image|replace:"/IMAGES/news/":""}" />
					</a>
				</div>
				{/if}
				<div class="intro">{$module->article.intro}</div>
				<div class="body">{$module->article.body}</div>
			</div>
			{if $module->reactions}
				{reactions relation_id=$module->article.id relation_name=BrickworkNews_Article title=$module->article.title}{/reactions}
			{/if}
			<a href="#" onclick="history.back(); return false;">&laquo; terug</a>
		</div>
{******** Overview ********}
	{else}
		<div class="overview">
			<h1>Nieuws</h1>
			<div><a href="?{$module->qsPart('type')|qsmod:1}">Actueel</a>|<a href="?{$module->qsPart('type')|qsmod:2}">Archief</a></div>
			
			<div class="current_category phoundry_module_news_category|{$module->category.id}">Category: <span class="title">{$module->category.name|htmlspecialchars}</span></div>
			
			{if count($module->category->getChildren()) > 0}
			<div class="subcategories">
				<div>Subcategorieën</div>
				{foreach from=$module->category->getChildren() item=item}
					<div class="category"><a href="?{$module->qsPart('c')|qsmod:$item.id}">{$item.name}</a></div>
				{/foreach}
			</div>
			{/if}
			
			<table class="articles">
				<thead>
					<tr>
						<th>Titel</th>
						<th>Intro</th>
						<th>Datum</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$module->articles item=item}
					<tr>
						<td><a href="?{$module->qsPart('n')}={$item.id}&amp;{$module->qsPart('c')}={$module->category.id}">{$item.title|htmlspecialchars}</a></td>
						<td>{$item.intro|strip_tags|truncate:150}</td>
						<td>{$item.create_datetime|date_format:"d-m-Y"}</td>
					</tr>
				{/foreach} 
				</tbody>
			</table>
			
			{if $module->page instanceof Pagination && $module->page->pages > 1}
			<div class="pagination">{include file='html/pagination1.tpl' pagination=$module->page}</div>
			{/if}
		</div>
	{/if}
{******** Mini ********}
{else if $module->view == 'mini'}
	<div class="mini">
	{foreach from=$module->articles item=item}
		<div class="article">
			<div><a href="/page/{$module->detail_page}?{$module->qsPart('n', true)}={$item.id}">{$item.title|htmlspecialchars}</a></div>
			<div>{$item.intro|truncate:150}</div>
		</div>
	{/foreach} 
	</div>
{/if}
