<div>
  {defun name="sitemap" list=$module->items}
    {foreach from=$list item=element}
      {if !is_null($element->parent) }
        <li> 
      {/if}
      <a href="{$element->link}" target="{$element->target|default:"_self"}">{$element->title}</a><br />
      {if count($element->items)>0 }
        <ul>
        {fun name="sitemap" list=$element->items}
        </ul>
      {/if}
      {if !is_null($element->parent) }
        </li>
      {/if}
    {/foreach}
  {/defun}
</div>