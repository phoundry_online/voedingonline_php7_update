<h2>Links</h2>
{if $module->retreive_links_success}
{foreach from=$module->link_items key=category item=mainloop}
  <div class="linkmodule_category">
    <h3>{$category}</h3>
    {foreach from=$mainloop item=link}
    <a href="{$link.link}" target="{$link.target}" class="linkmodule_link" title="{$link.description}">{$link.title}</a><br/>
    {/foreach}
  </div><br class="clear" />
{/foreach}
{else}
Geen categorieën..
{/if}
