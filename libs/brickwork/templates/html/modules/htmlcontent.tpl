{if !empty($module->title)}
	<h1>{$module->title|htmlspecialchars}</h1>
{/if}
<div class="html_body phoundry_module_content_html|{$module->content_id}">
{$module->text}
</div>
{if !empty($module->pages)}
<ul class="pages">
{foreach from=$module->pages key=key item=page }
	{if $page->current }
		<li>{$page->title|htmlspecialchars}</li>
	{else}
		<li><a href="?mod[{$module->id}][page]={$page->id}" title="{$page->title|htmlspecialchars}">{$page->title|htmlspecialchars}</a></li>
	{/if}
{/foreach}
</ul>
{/if}