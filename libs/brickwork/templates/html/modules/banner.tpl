<!-- stack name="javascript_src" assign="swfobject" -->
<script type="text/javascript" src="/brickwork/js/swfobject.js"></script>
<!-- /stack -->
{if $module->banners && count($module->banners)}
	<div class="banners">
{foreach from=$module->banners key=key item=banner}
		<!-- banner: {$banner->name} -->
		<div class="banner phoundry_module_content_banner|{$banner->id}">
			<div class="banner_inner" {if isset($banner->bgcolor) && !empty($banner->bgcolor)}style="background-color: {$banner->bgcolor}"{/if}>
{if $banner->filetype == 'adsense'}
				<div class="banner_item adsense">
					<script type="text/javascript">
					//<![CDATA[
					google_ad_client = "{$banner->adsense_client}";
					google_ad_slot = "{$banner->adsense_slot}";
					google_ad_width = {$banner->width};
					google_ad_height = {$banner->height};
					//]]>
					</script>
					<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
				</div>
{elseif $banner->filetype == 'flash'}
				<div class="banner_item flash" id="banner_{$module->id}_{$banner->id}">
					<div class="text">
						<p>
						Om flash weer te geven moet uw browser javascript ondersteunen en heeft u de <a target="_blank" href="http://www.adobe.com/products/flashplayer/">Adobe Flash Player</a> nodig.
						</p>
					</div>
				</div>
				
				<script type="text/javascript">
				//<![CDATA[
				(function(){ldelim}
					var banner_box= $('#banner_{$module->id}_{$banner->id}');
					if (banner_box.length) {ldelim}
						var banner_width = banner_box.width();
						var banner_height = banner_box.height();
						
						var flashvars = {ldelim}
						{rdelim};
						var params = {ldelim}
							bgcolor: "white",
							wmode: "transparent",
							quality: "high"
						{rdelim};
						var attributes = {ldelim}
						{rdelim};
{if $banner->height}
						banner_height= {$banner->height};
{/if}
						if (typeof(swfobject) != 'undefined') {ldelim}
							swfobject.embedSWF("{$banner->image}",
								"banner_{$module->id}_{$banner->id}",
								banner_width,
								banner_height,
								"9",
								"", flashvars, params, attributes
							);
						{rdelim}
					{rdelim}
				{rdelim})();
				//]]>
				</script>
{elseif $banner->filetype == 'image'}
				<div class="banner_item image"{if $banner->height} style="height: {$banner->height}px;"{/if}>
{if $banner->url}
					<a title="{$banner->name|htmlspecialchars}"
{*						href="{$banner->url}" Directe Link *}
						href="/module/BannerModule/{$banner->id}"{* Klikteller *}
						target="{$banner->target}"
{* Optioneel voor Google Analytics
						{if @$SITE->getConfig('google_analytics') && $banner->analytics}onclick="if (typeof(pageTracker)== 'object') pageTracker._trackPageview('/banner/{$banner->name|escape:url}'); return true;"{/if}
*}
						>
{/if}
						<img src="{$banner->image}" title="{$banner->name|htmlspecialchars}" alt="{$banner->name|htmlspecialchars}" border="0" />
{if $banner->url}
					</a>
{/if}
				</div>
{elseif $banner->filetype == 'javascript'}
				<div class="banner_item javascript">
					{$module->custom_javascript}
				</div>
{else}
				<div class="banner_item movie" id="banner_{$module->id}_{$banner->id}">
					<div class="text">
						<p>
						Om film beeld weer te geven moet uw browser javascript ondersteunen en heeft u de <a target="_blank" href="http://www.adobe.com/products/flashplayer/">Adobe Flash Player</a> nodig.
						</p>
					</div>
				</div>
	{if $banner->ext == 'wmv'}
				<!-- stack name="javascript_src" assign="jwsilverlight" -->
				<script type="text/javascript" src="/brickwork/js/jwplayer/silverlight.js"></script>
				<script type="text/javascript" src="/brickwork/js/jwplayer/wmvplayer.js"></script>
				<!-- /stack -->
				<script type="text/javascript">
				//<![CDATA[
				(function(jw){ldelim}
					var container = document.getElementById("banner_{$module->id}_{$banner->id}"),
					player = new jw.Player(
							container,
							'/brickwork/silverlight/jwplayer/wmvplayer.xaml',
							{ldelim}
								file: '{$banner->flashvars.file}',
								{if isset($banner->flashvars.streamer)}
								image: '{$banner->flashvars.streamer}',
								{/if}
								{if $banner->url}
								linktarget: "{$banner->target}",
								link: "{$banner->url}",
								{/if}
								height: '{$banner->height}',
								width: '{$banner->width}'
							{rdelim}
						
					);
					
				{rdelim})(jeroenwijering);
				//]]>
				</script>
	{else}
				<script type="text/javascript">
				//<![CDATA[
				(function(){ldelim}
					var banner_box = $('#banner_{$module->id}_{$banner->id}');
					if (banner_box.length)
					{ldelim}
						var banner_width = banner_box.width();
						var banner_height = banner_box.height();
						
						var flashvars = {ldelim}
							height: banner_height,
							width: banner_width,
							{foreach from=$banner->flashvars key=flashvar item=flashitem}
								{$flashvar}: "{$flashitem}",
							{/foreach}
							autostart: "true",
							repeat: "always",
							controlbar: "bottom",
							{if $banner->url}
								displayclick: "link",
								linktarget: "{$banner->target}",
								link: "{$banner->url}",
							{/if}
							screencolor: "#ffffff"
						{rdelim};
						var params = {ldelim}
							bgcolor: "white",
							wmode: "transparent",
							allowfullscreen: "true",
							allowscriptaccess: "always",
							quality: "high"
						{rdelim};
						var attributes = {ldelim}
						{rdelim};
						{if $banner->height}
							banner_height = {$banner->height};
						{/if}
						if (typeof(swfobject) != 'undefined') {ldelim}
							swfobject.embedSWF("/brickwork/flash/jwplayer/player.swf",
								"banner_{$module->id}_{$banner->id}",
								banner_width,
								banner_height,
								"9",
								"", flashvars, params, attributes
							);
						{rdelim}
					{rdelim}
				{rdelim})();
				//]]>
				</script>
	{/if}
{/if}
			</div>
		</div>
		<!-- end of banner: {$banner->name} -->
{/foreach}
	</div>
{/if}
