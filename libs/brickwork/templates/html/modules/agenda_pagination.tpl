{if count($module->pages) > 1}

	{assign var='qs' value=''}

	{if $module->view == 'Archive'}
		{assign var='qsPart' value=$module->qsPart('view', TRUE)}
		{assign var='view' value=$module->view}
		{assign var='qs' value="&$qsPart=$view"}
	{/if}

	<div class="pagination">
		{if !is_null($module->previous)}
			<a href="{$smarty.server.PHP_SELF}?{$module->qsPart('page_nr')}={$module->previous}{$qs}">&laquo;</a>&nbsp;&nbsp;
		{else}
			&laquo;&nbsp;&nbsp;
		{/if}

		{foreach from=$module->pages key=page item=bool }
			{if $bool == 1}
				{$page+1}
			{else}
				<a href="?{$module->qsPart('page_nr')}={$page}{$qs}">{$page+1}</a>
			{/if}
		{/foreach}

		{if !is_null($module->next)}
			&nbsp;&nbsp;<a href="{$smarty.server.PHP_SELF}?{$module->qsPart('page_nr')}={$module->next}{$qs}">&raquo;</a>
		{else}
			&nbsp;&nbsp;&raquo;
		{/if}
	</div>

{/if}