<!-- stack name="css_src" assign="function_abuse_form" -->
	<link rel="stylesheet" href="/index.php/script/css/abuse.css" type="text/css" media="screen" />
<!-- /stack --> 

<!-- stack name="javascript" assign="function_abuse_form" -->
	{literal}
	function abuseFormSubmit(form) {
		hash= $(form).attr('name');
		data= $(form).serialize();
		$.post('/index.php/service/json/abuse_report/form?hash='+hash+'&r='+Math.random(), data,
	        function(data, textStatus) {
				if (data.status) {
					html= data.content;
					if (html) {
						$('div#abuse_'+ hash).html(html);
					}
				}
			}, 'json'
		);
		
		return false;
	}
	{/literal}
<!-- /stack --> 

<!-- stack name="javascript" assign="function_abuse_link" -->
	{literal}
	jQuery(function($){
		$('a.abuse_link').click(function(e){
			hash= $(this).attr('href').substring(1);
			target= $('div#abuse_'+ hash);
			if (!$(target).size()) {
				$(this).after('<div id="abuse_'+ hash+ '"><\/div>');
				target= $('div#abuse_'+ hash);
			}
			if ($(target).size()) {
				if ($(target).children().size()) {
					$(target).empty();
				}
				else {
					$.getJSON('/index.php/service/json/abuse_report/form?hash='+hash+'&r='+Math.random(),
				        function(data) {
							if (data.status) {
								html= data.content;
								if (html) {
									$(target).html(html);
								}
							}
						}
					);
				}
			}
			return false;
		});
	});
	{/literal}
<!-- /stack --> 

<div class="abuse_link" >
	<a class="abuse_link" href="#{$params.hash|default}" title="{$params.title|default|htmlspecialchars}" >{_"Misbruik melden"}</a>
</div>
