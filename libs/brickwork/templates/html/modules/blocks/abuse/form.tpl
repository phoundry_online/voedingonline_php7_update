<div class="abuse_form">

	<div class="head">
		{_"Misbruik melden"}
	</div>
	
	<div class="info">
		{foreach from=$td.info key=key item=item}
			<div class="item">{$item}</div>
		{/foreach}
	</div>
	
	{if $td.error|@count}
		<div class="error">
			<div class="head">
				{_"Er is iets fout gegaan"}.
			</div>
			{foreach from=$td.error key=key item=item}
				{if is_numeric($key)}
					<div class="item">{$item}</div>
				{/if}
			{/foreach}
		</div>
	{/if}
	
	{if $td.show_form}
		<form action="?" method="post" name="{$td.hash|htmlspecialchars}" onSubmit="return abuseFormSubmit(this)">
			
			<div><label for="abuse[name]" >{_"Naam"}:</label></div>
			{if isset($td.error.name)}
				<div class="error">{$td.error.name|tr|htmlspecialchars}</div>
			{/if}
			<div><input name="abuse[name]" type="text" size="30" value="{$td.abuse.name|default|htmlspecialchars}"></div>
			
			<div><label for="abuse[email]" >{_"E-mail"}:</label></div>
			{if isset($td.error.email)}
				<div class="error">{$td.error.email|tr|htmlspecialchars}</div>
			{/if}
			<div><input name="abuse[email]" type="text" size="30" value="{$td.abuse.email|default|htmlspecialchars}"></div>
	
			<div><label for="abuse[message]" >{_"Misbruik bericht"}:</label></div>
			{if isset($td.error.message)}
				<div class="error">{$td.error.message|tr|htmlspecialchars}</div>
			{/if}
			<div><textarea name="abuse[message]" rows="10" cols="50">{$td.abuse.message|default|htmlspecialchars}</textarea> </div>
	
			<div><input name="abuse[submit]" type="submit" value="{_"Verstuur"}"></div>
			<div class="captcha"><input name="abuse[captcha]" type="text" size="8" value="{$td.abuse.captcha|default|htmlspecialchars}"></div>
			
		</form>
	{/if}
</div>
