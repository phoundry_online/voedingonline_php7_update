{if isset($reactiongroup) && !is_null($reactiongroup->relationConfig) && $reactiongroup->relationConfig.enabled}
<!-- stack name="css_src" assign="reactions" -->
<link rel="stylesheet" href="/index.php/script/css/reactions|reactions.css" type="text/css" media="screen" />
<!-- /stack -->
<!-- stack name="css_src" assign="nyromodal" -->
<link rel="stylesheet" href="/index.php/script/css/jquery|nyroModal.css" type="text/css" media="screen" />
<!-- /stack -->
<!-- stack name="javascript_src" assign="nyromodal" -->
<script type="text/javascript" src="/index.php/script/js/jquery|nyroModal.js/jquery|nyroModal.youtube.js"></script>
<!-- /stack -->
{foreach name=reactions from=$reactiongroup->getReactions() item=reaction}
{if $smarty.foreach.reactions.first}<div class="reactions">{/if}
	<div class="reaction{if $smarty.foreach.reactions.first} first{/if}{if $smarty.foreach.reactions.last} last{/if}{if $smarty.foreach.reactions.index %2 == 0} odd{else} even{/if} phoundry_{$WEBprefs.reactions.reactiontid|default:'brickwork_reaction'}|{$reaction.id}">
		<a name="reactie_{$reaction.id}"></a>
		<div class="author"><span class="author">{$reaction.author|htmlspecialchars}</span> op <span class="created_on">{$reaction.created_on|date_format:"d-m-y H:i"}</span></div>
		<div class="body">{$reaction.body|htmlspecialchars|autolink|nl2br}</div>
		<div class="media">
				{counter name=mediacnt assign=mediacnt start=1}
				{foreach name=media from=$reaction->media item=media}{if $mediacnt <= $reactiongroup->relationConfig.media}
				
				{if $reactiongroup->relationConfig->mediaTypeAllowed('image') && $media.mediatype == 'image'}
					<a href="/index.php/image/reactions_big/{$media.url|replace:$WEBprefs.reactions.imagedir:''}" class="nyroModal image" style="background-image: url(/index.php/image/reactions_thumb/{$media.url|replace:$WEBprefs.reactions.imagedir:''});" rel="reaction_media_{$reaction.id}"></a>
					{counter name=mediacnt assign=mediacnt}
				{elseif $reactiongroup->relationConfig->mediaTypeAllowed('youtube') && $media.mediatype == 'youtube'}
					<a href="{$media->youtubeUrl()}" class="nyroModal youtube" style="background-image: url({$media->youtubeThumb()});" rel="reaction_media_{$reaction.id}"><span class="play"></span></a>
				{elseif $reactiongroup->relationConfig->mediaTypeAllowed('url') && $media.mediatype == 'url'}
					<a href="{$media.url}" class="url"></a>
				{/if}
				{/if}{/foreach}
		</div>
		<div class="break"></div>
		{abuse_report hash="reactie_`$reaction.id`" title="Misbruik melding over reactie van `$reaction.author`"}
	</div>
{if $smarty.foreach.reactions.last}</div>{/if}
{/foreach}
{if !$reactiongroup.closed}
<div id="reactiongroup_{$reactiongroup.id}" class="reaction_form phoundry_{$WEBprefs.reactions.reactiongrouptid|default:'brickwork_reaction_group'}|{$reactiongroup.id}">
	<div class="reaction_form_head"><a name="reageren_{$reactiongroup.id}"></a>Reageer op {$reactiongroup_params.title|default:"dit bericht"}</div>
	<div class="errors">
	{if $reactiongroup->relationConfig.media}<noscript><div class="error"><img src="/phoundry/core/icons/exclamation.png" alt="" />Uw browser ondersteund geen Javascript. U kunt geen afbeeldingen toevoegen</div></noscript>{/if}
	{if !empty($reactiongroup_errors)}
	Er zijn de volgende fouten opgetreden:
	<ul>{foreach from=$reactiongroup_errors item=error}
	<li class="error">{$error|ucfirst}</li>
	{/foreach}</ul>
	{/if}
	</div>
	<form class="reaction_form" method="post" action="/index.php/module/reactions?hash={$reactiongroup->getHash()}">
		{* reageer formulier *}
		<div class="column">
			<div class="column_inner">
			<div class="">naam:</div>
			<div class=""><input name="{$reactiongroup->qsPart('author')}" value="{$reactiongroup_post.author|default:$PAGE->auth->user->displayname|default|htmlspecialchars}" type="text" tabindex="1" {if $PAGE->auth->user->isLoggedin()}readonly="readonly" {/if}/></div>
			{if $reactiongroup->relationConfig.media && $reactiongroup->relationConfig->mediaTypeAllowed('image')}
			<script type="text/javascript">
				document.write('<div class="upload_button" style="display: {if $reactiongroup->relationConfig.media <= count($reactiongroup_files)}none{else}block{/if};">'+"\n");
				document.write("<div>voeg een foto toe: (max. {$reactiongroup->relationConfig.media} foto('s))</div>\n");
				document.write('<div><input class="fileupload" name="file1" type="file" tabindex="3" /></div>');
				document.write('</div>');
				document.write('<div class="upload_max" style="display: {if $reactiongroup->relationConfig.media <= count($reactiongroup_files)}block{else}none{/if};">Maximale hoeveelheid fotos bijgevoegd</div>'+"\n");
			</script>
			{/if}
			</div>
		</div>
		
		<div class="column">
			<div class="column_inner">
			<div>e-mail:</div>
			<div><input name="{$reactiongroup->qsPart('imeel')}" value="{$reactiongroup_post.imeel|default:$PAGE->auth->user->email|default|htmlspecialchars}" type="text" tabindex="2" {if $PAGE->auth->user->isLoggedin()}readonly="readonly" {/if}/></div>
			{if $reactiongroup->relationConfig->mediaTypeAllowed('youtube')}
			<div>Youtube URL:</div>
			<div><input name="{$reactiongroup->qsPart('youtube')}" value="{$reactiongroup_post.youtube|default|htmlspecialchars}" type="text" tabindex="4" /></div>
			{/if}
			</div>			
		</div>
		<div class="thumbnails_wrap">
		<div class="current_size">{if !empty($reactiongroup_files)}{$reactiongroup_files|@count} foto(s){/if}</div>
		<div class="thumbnails">{if $reactiongroup->config.images != 0 && !empty($reactiongroup_files)}
			{foreach from=$reactiongroup_files key=key item=item}
			<div class="thumbwrapper">
				<div title="thumb_{$key}" class="thumb" style="background-image: url({$reactiongroup_thumburl}/{$item.tmp_name});">
					<a class="zoom" href="{$reactiongroup_zoomurl}/{$item.tmp_name}" target="_blank" title="Zoom">Zoom</a>
					<a class="delete" href="/index.php/upload/remove?upload_type=Reactionfiles&amp;upload_key={$reactiongroup.relation_name}_{$reactiongroup.relation_id}&amp;key={$key}" title="Delete">Delete</a>
				</div>
			</div>
			{/foreach}
		{/if}</div><div class="break"></div></div>
		
		<div class="reaction_body">
			<div>reactie:</div>
			<div><textarea name="{$reactiongroup->qsPart('body')}" cols="140" rows="10" tabindex="5">{$reactiongroup_post.body|default|htmlspecialchars}</textarea></div>
		</div>
		
		<div class="reaction_footer">
			<input class="submit_button" type="submit" name="{$reactiongroup->qsPart('submit')}" value="plaats reactie" tabindex="7" />
			<div class="break"></div>
		</div>
	</form>
	
	{if $reactiongroup->relationConfig.media}
	<!-- stack name="javascript_src" assign="ajaxupload" -->
	<script type="text/javascript" src="/index.php/script/js/jquery|jquery.ajaxUpload.js"></script>
	<!-- /stack -->
	<!-- stack name="javascript_src" assign="formAttachements" -->
	<script type="text/javascript" src="/index.php/script/js/formAttachments.js"></script>
	<!-- /stack -->
	<!-- stack name="javascript" -->
	jQuery(function($){ldelim}
		var reactiongroup = $('#reactiongroup_{$reactiongroup.id}'),
		submitted = false,
		attachments = new FormAttachments({ldelim}
			file_input			: $('input.fileupload', reactiongroup),
			thumbnails			: $('.thumbnails', reactiongroup),
			placeholder			: '<div class="placeholder"></div>',
			file_types			: ['jpg', 'gif', 'png'], // Empty array for all file extensions
			url					: '/index.php/upload?ajax&upload_type=Reactionfiles&upload_key={$reactiongroup.id}',
			delete_url			: '/index.php/upload/remove?ajax&upload_type=Reactionfiles&upload_key={$reactiongroup.id}&key=',
			maxfiles			: {$reactiongroup->relationConfig.media}, // 0 is infinite
			//maxfilesmessage		: $('.upload_max', reactiongroup),
			
			{literal}
			callbacks			: {
				postBuildThumbnails : function(){
					$('.current_size', reactiongroup).html(this.files.length + '  foto(s)');
					return true; // If return true the input gets replaced my the maxfilesmessage when needed
				},
				error				: function(code, responseText) {
					switch(code)
					{
						case 'timeout_expired':
							alert('Timeout verstreken.');
						break;
						case 'invalid_extension':
							alert('Bestandstype niet toegestaan.');
						break;
						case 'invalid_data':
							alert('De server gaf een ongeldig antwoord terug');
						break;
						default:
						alert('Kon het bestand niet uploaden '+ msg);
					}

					return true;
				},
				uploaded			: function(files) {
					if(submitted) {
						$('form.reaction_form', reactiongroup).submit();
					}
					return true;
				},
				preUpload			: function(file_input) {
					$('.current_size', reactiongroup).html('');
					return true;
				}
			},
			{/literal}
			timeout		: 180000

		{rdelim}, ({$reactiongroup_files|@json_encode}));

		{literal}
		$('form.reaction_form', reactiongroup).submit(function(e){
			if(attachments.uploading) {
				submitted = true;
				e.preventDefault();
				//$('form.reaction_form .submit_button', reactiongroup).attr("disabled","disabled");
				return false;
			}
			return true;
		});

		reactiongroup.find('input.fileupload').change(function(){
			attachments.upload();
		});
	});
	{/literal}
	<!-- /stack -->
	{/if}
	</div>
{else}
	<div id="reactiongroup_{$reactiongroup.id}" class="reaction_form phoundry_{$WEBprefs.reactions.reactiongrouptid|default:'brickwork_reaction_group'}|{$reactiongroup.id}">
		<div class="reaction_form_head"><a name="reageren_{$reactiongroup.id}"></a>Reageer op {$reactiongroup_params.title|default:"dit bericht"}</div>
		Helaas, er kunnen geen nieuwe reacties meer worden geplaatst
	</div>
{/if}
{/if}
