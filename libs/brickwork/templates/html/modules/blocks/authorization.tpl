<!--htdig_noindex-->
<!-- stack name="css_src" assign="login_module" -->
<link rel="stylesheet" href="/index.php/script/css/login|login.css" type="text/css" media="screen" />
<!-- /stack -->

<div class="authorization">
	<div class="body">
		{if !empty($authorization.errors)}
			<div class="row">
				{foreach from=$authorization.errors key=key item=item}
					<div class="error">
						{if is_numeric($key)}
							{$item|tr|htmlspecialchars}
						{/if}
					</div>
				{/foreach}
			</div>
		{/if}
		
		{if $authorization.welcome}
			<div class="login_ok">{"Welkom #1"|trParams:$authorization.user->displayname|htmlspecialchars}</div>
		{else}
			{if is_a($PAGE,'PageContentHandler') && $PAGE->needsAuthentication && $PAGE->isLoggedIn && !$PAGE->isAuthenticated}
				<div>{_"U bent ingelogd maar heeft niet voldoende rechten om deze pagina te bekijken."}</div>
			{elseif $authorization.user->isLoggedIn()}
				<div class="login_ok">{"U bent ingelogd als #1"|trParams:$authorization.user->displayname}</div>
			{else}
				<div>{_"U bent niet ingelogd. Gebruik onderstaand formulier om in te loggen."}</div>
			{/if}
		{/if}
		{if $authorization.user->isLoggedIn()}
			<div class="last_login">
				{assign var=previous value=$authorization.user->previousLogin(1)}
				{if count($previous)}
					{foreach from=$previous item=item}
						{assign var=date value=$item.modified_ts|date_format:'Y'}
						{"Vorige login was op #1 (#2)"|trParams:$date:$item.ip_address}
					{/foreach}
				{/if}
			</div>
		{/if}
		{assign var=profile_id value=$SITE->getConfig('authorization_profile_page')}
		{if $profile_id && $STRUCTURE->id!=$profile_id}
			<a href="{$profile_id|pageUrl}">{_"Profile"}</a>
			<div class="row">
				<a href="{$profile_id|pageUrl}?{$module->qspart('action',true)}=recover">{_"Forgot your account password?"}</a>
			</div>
			<div class="row">
				<a href="{$profile_id|pageUrl}?{$module->qspart('action',true)}=register">{_"Create new account?"}</a>
			</div>
			
		{/if}
		{if !($authorization.user->isLoggedIn())}
			<form action="?" method="post">
				<div class="row">
				{if !empty($authorization.errors.user)}
					<div class="error">{$authorization.errors.user|tr|htmlspecialchars}</div>
				{/if}
				<label for="username">{_"Gebruikersnaam of E-mailadres:"}</label><br />
				<input type="text" id="username"
					class="{if !empty($authorization.errors.user)}error{/if}"
					name="authorization[user]" value="{$smarty.post.authorization.user|default}" /><br/>
				</div>
			
				<div class="row">
				{if !empty($authorization.errors.password)}
					<div class="error">
						{$authorization.errors.password|tr|htmlspecialchars}
					</div>
				{/if}
				<label for="password">{_"Wachtwoord:"} </label><br />
				<input type="password" name="authorization[password]"
					class="{if !empty($authorization.errors.password)}error{/if}"
					id="password" value="{$smarty.post.authorization.password|default}" />
				</div>
				
				<div class="row captcha_">
				<label for="captcha_" class="{if !empty($authorization.errors.captcha_)}error{/if}">{_"Captcha:"}</label><br />
				<input type="text" size="50" id="captcha_" class="{if !empty($authorization.errors.captcha_)}error{/if}"
					name="authorization[captcha_]" value="{$smarty.post.authorization.captcha_|default|htmlspecialchars}" /><br/>
				</div>
			
				<div class="row submit">
				<input type="submit" name="authorization[login]"
					value="{_"Log in"}" class="submit"/>
				</div>
			</form>
		{else}
			<a href="?authorization[logout]=1">{_"Log out"}</a>
		{/if}
	</div>
</div>
<!--/htdig_noindex-->
