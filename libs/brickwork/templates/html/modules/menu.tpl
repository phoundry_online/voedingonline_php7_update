{defun name="recursion" list=$module->items}
	{foreach name=menu from=$list item=element}
		<a href="{$element->link}" target="{$element->target|default:"_self"}" class="{if $smarty.foreach.menu.last}last {/if}{if $smarty.foreach.menu.first}first {/if}{if $element->selected && is_null($element->parent)}active {/if}{if $element->selected && !is_null($element->parent)}sub_active {/if}{if !is_null($element->parent)} sub{/if}"><!--htdig_noindex-->{$element->title|htmlspecialchars}<!--/htdig_noindex--></a>				
		{if count($element->items) > 0}
			<span class="sub_menu">
				{fun name="recursion" list=$element->items}
			</span>
		{/if}
	{/foreach}
{/defun}