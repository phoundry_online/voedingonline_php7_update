{if $module->view =='overview'}
<h1>Zoeken</h1>
	{* Overview *}
		{* Zoekformulier *}
			<form method="get" action="{$smarty.server.PHP_SELF}?" style="border:0; border-bottom:1px dashed #CCC; width:98%; padding-bottom: 20px;">
				<table>
					<tr>
						<td>Zoekterm:</td>
						<td>
							<input type="text" id="search" name="{$module->qsPart('words')}" value="{$module->get.words|default|htmlspecialchars}" style="width:150px;" />
							<select name="{$module->qsPart('method')}">
								<option label="Alle woorden" value="and" {if $module->get.method|default:'' == 'and'}selected="selected"{/if}>Alle woorden</option>
								<option label="E&eacute;n van de woorden" value="or" {if $module->get.method|default:'' == 'or'}selected="selected"{/if}>E&eacute;n van de woorden</option>

								{* Lastige optie voor vrouwen vermoeden wij ;)
									<option label="Boolean" value="boolean" {if $module->get.method|default:'' == 'boolean'}selected="selected"{/if}>Boolean</option>
								*}
							</select>
						</td>
					</tr>
					<tr>
						<td>Resultaten per pagina:</td>
						<td><select name="{$module->qsPart('matchesperpage')}">
								{foreach from=$module->valid_matchesperpage key=key item=mpp}
									<option label="{$mpp}" value="{$mpp}" {if $mpp == $module->matchesperpage|default:''}selected="selected"{/if}>{$mpp}</option>
								{/foreach}
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="submit" value="Zoeken" /></td>
					</tr>
				</table>
			</form>
		{* /Zoekformulier *}


		{* Zoekresultaten *}
			{if !is_null($module->get.words)}
			<br clear="all" />

				Resultaat {$module->htdig->first_match} - {$module->htdig->last_match} van in totaal {$module->matches}<br /><br />

				{foreach from=$module->items key=key item=item }
					<dl>
						<dt>
							<b>{$item->index}.&nbsp;<a href="{$item->url}">{$item->title|htmlspecialchars}</a></b> ({$item->percent}%)
						</dt>
						<dd>
							{$item->excerpt}
							<small style="font-size:0.8em;"><br />{$item->size} bytes - {$item->modified}</small>
						</dd>
					</dl>
				{/foreach}
				<br />
				{if !is_null($module->previous)}
					<div style="float:left;">
						<a href="{$smarty.server.PHP_SELF}?{$module->getPreviousUrl()}">Vorige</a>
					</div>
				{/if}
				{if !is_null($module->next)}
					<div style="float:right;">
						<a href="{$smarty.server.PHP_SELF}?{$module->getNextUrl()}">Volgende</a>
					</div>
				{/if}
			{/if}
		{* /Zoekresultaten *}
	{* /Overview *}

{elseif $module->view == 'miniview'}

	{* Miniview *}
		
			<div class="box_search">
				{* Zoekformulier *}
					{if !empty($module->config->detail_page_id)}
						<form method="get" action="/page/{$module->config->detail_page_id}">
					{else}
						<form method="get" action="{$smarty.server.PHP_SELF}?">
					{/if}
						<input type="hidden" name="{$module->qsPart('matchesperpage', TRUE)}" value="10" />
						<input class="search_style" id="search" type="text" name="{$module->qsPart('words', TRUE)}" value="{$module->get.words|default:''}" />
						<input type="image" src="/pics/glutenvrij/zoeken.gif" name="submit" value="Zoeken" />
						
					</form>
				{* /Zoekformulier *}

				{* Zoekresultaten *}
					{if !is_null($module->get.words)}
						<br />
						{section loop=$module->items name=item max=3}
							<div style="text-align:left;">
								<b>{$module->items[item]->index}.&nbsp;<a href="{$module->items[item]->url}">{$module->items[item]->title|truncate:"40"|htmlspecialchars}</a></b> ({$module->items[item]->percent}%)<br />
								<div style="margin-left:10px;">...{$module->items[item]->excerpt|truncate:"70"}</div>
							</div>
						{sectionelse}
							Geen resultaten
						{/section}
					{/if}
				{* /Zoekresultaten *}
			</div>
		
	{* /Miniview *}

{/if}