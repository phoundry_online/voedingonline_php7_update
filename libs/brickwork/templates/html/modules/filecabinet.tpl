{if $module->td.is_root}
	<div class="platformen cabinet_parts">{_"Mappen:"}</div>
	<div class="root_folders cabinet_parts">
	<ol class="folder">
	{foreach from=$module->td.folders item=item}
	{if $item->hasAccess('read')}
		<li><a href="?{$module->qsPart('f')}={$item.id}">{$item.name|htmlspecialchars|tr}</a></li>
	{/if}
	{foreachelse}
	<li>{_"Geen directories"}</li>
	{/foreach}
	</ol>
	</div>
{else}
	{if $module->td.current_folder->hasAccess('read')}
	<div class="header cabinet_parts phoundry_file_cabinet_folder|{$module->td.current_folder.id}">
		<span class="current_folder">{$module->td.current_folder.name|htmlspecialchars}</span>
	</div>
	<div class="folder_description cabinet_parts">
	{$module->td.current_folder.description|tr}
	</div>
	<div class="navigation cabinet_parts">
		<span class="prefix">{_"Navigatie:"}</span>
		{foreach name=navigatie from=$module->td.path item=item}
		{if $smarty.foreach.navigatie.last}
		<span class="folder active">{$item.name|htmlspecialchars}</span>
		{else}
		<span class="folder">
			<a href="?{$module->qsPart('f')}={$item.id}">{$item.name|htmlspecialchars}</a> &gt;
		</span>
		{/if}
		{/foreach}
	</div>
	
	{* SubFolders *}
	{if count($module->td.folders) != 0}
	{assign var=sub_folder_getoond value=false}
	{capture name=sub_folders}
	<div class="sub_folders cabinet_parts">
	<div>{_"Onderliggende mappen:"}</div>
	<ul class="folder">
	{foreach from=$module->td.folders item=item}
	{if $item->hasAccess('read')}
	{assign var=sub_folder_getoond value=true}
		<li><a href="?{$module->qsPart('f')}={$item.id}">{$item.name|htmlspecialchars}</a></li>
	{/if}
	{/foreach}
	</ul>
	</div>
	{/capture}
	{if $sub_folder_getoond}
		{$smarty.capture.sub_folders}
	{/if}
	{/if}
	
	{* Files *}
	{assign var=bestand_getoond value=false}
	<div class="files cabinet_parts">
		{if count($module->td.files) > 0}
			{capture name=bestanden_table}
				<table width="100%">
					<col width="20" /><col  width="255" /><col /><col width="90" />
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Document</th>
							<th>Description</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						{foreach from=$module->td.files item=item}
						{if $item->hasAccess('read')}
						{assign var=bestand_getoond value=true}
						<tr class="file phoundry_file_cabinet_file|{$item.id}">
							<td>
								<span class="filetype">
								<img src="{$module->getExtensionImg($item)}" alt="{$item.mime}" />
								</span>
							</td>
							<td>
								<div class="filename"><a href="/module/{$module->id}/{$item.id}/{$item.name|toUri}" title="{$item.name|htmlspecialchars}">{$item.name|truncate:40|htmlspecialchars}</a></div>
								<div class="username">{$item->getUser('displayname')|truncate:40|htmlspecialchars}</div>
							</td>
							<td>
								<span class="description">{$item.description|htmlspecialchars}</span>
							</td>
							<td>
								<span class="createdate">{$item.create_datetime|date_format:"Y-m-d"}</span>
								
								{* Check voor delete rechten binnen de huidige map *}
								{if $item->hasAccess('delete')}
								<span class="delete">
									<a href="?{$module->qsPart('f')}={$module->td.current_folder.id}&amp;{$module->qsPart('delete')}={$item.id}"><img src="/phoundry/core/icons/bin.png" alt="Delete" /></a>
								</span>
								{/if}
							</td>
						</tr>
						{/if}
						{/foreach}
					</tbody>
				</table>
			{/capture}
		{/if}
		{if $bestand_getoond}
			{$smarty.capture.bestanden_table}
		{else}
			{_"Geen documenten gevonden"}
		{/if}
	</div>
	
	{* File Upload *}
	{if $module->td.current_folder->hasAccess('write')}
	<div class="file_upload cabinet_parts">
		{if count($module->td.errors) > 0}
		<div class="errors">
			<div class="desc">{_"Bestand kon niet worden opgeslagen"}</div>
			{foreach from=$module->td.errors item=item}
			<div class="error">{$item|ucfirst|tr}</div>
			{/foreach}
		</div>
		{/if}
		<form action="{$smarty.server.REQUEST_URI|htmlspecialchars}" enctype="multipart/form-data" method="post">
			<div>
				<div>{_"Bestand:"}</div>
				<div><input name="{$module}_{$module->id}" type="file" /></div>
			</div>
			<div>
				<div>{_"Beschrijving:"}</div>
				<div><textarea name="{$module->qsPart('description')}" rows="5" cols="30"></textarea></div>
			</div>
			<div>
				<input type="submit" value="Submit" />
			</div>
		</form>
	</div>
	{/if}
	
	{else}
	{_"U moet ingelogd zijn en de juiste rechten hebben om deze map te kunnen bekijken"}
	{/if}
{/if}
