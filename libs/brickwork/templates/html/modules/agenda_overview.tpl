<span class="header3">Agenda items overview</span><br /><br />
{foreach from=$module->items key=key item=item}
		{if $key % 2 == 1}
			{assign var="class" value="odd"}
		{else}
			{assign var="class" value="even"}
		{/if}

		<div class="agenda_{$class}">			
			<div class="agenda_time">
				Datum: {* toon alleen "tot" datum als "van" en "tot" datum gelijk zijn, toon de tijd niet als deze gelijk is aan 00:00 *}
				{if $item->from_date|date_format:"d F" != $item->till_date|date_format:"d F"}
					{if $item->from_date|date_format:"H:i" != '00:00'}
						{$item->from_date|date_format:"d F Y H:i"} <i>tot</i><br/> {$item->till_date|date_format:"d F H:i"}
					{elseif $item->from_date|date_format:"H:i" == '00:00'}
						{$item->from_date|date_format:"d F Y"} <i>tot</i><br/> {$item->till_date|date_format:"d F"}
					{/if}
				{elseif $item->from_date|date_format:"d F" == $item->till_date|date_format:"d F"}
					{if $item->from_date|date_format:"H:i" != '00:00'}
						{* als de "van" en "tot" datum gelijk zijn laat dan niet de "tot" datum zien maar wel de "tot" tijd *}
						{if $item->from_date|date_format:"H:i" == $item->till_date|date_format:"H:i"}
							{$item->from_date|date_format:"d F Y H:i"}
						{elseif $item->from_date|date_format:"H:i" != $item->till_date|date_format:"H:i"}
							{$item->from_date|date_format:"d F Y H:i"} - {$item->till_date|date_format:"H:i"}
						{/if}
					{elseif $item->from_date|date_format:"H:i" == '00:00'}
						{$item->from_date|date_format:"d F Y"}
					{/if}
				{/if}
				<br />
				Locatie: {$item->location|htmlspecialchars}
			</div>
			<a href="?{$module->qsPart('agenda_id')}={$item->id}"><strong>{$item->title|htmlspecialchars}</strong></a>
			{if !empty($item->image)}		
			<div class="agenda_image">
				<img src="{$PHprefs.uploadUrl}{$item->image|urlencodeUrl}" />
			</div>
			{/if}
			<div class="agenda_description">
				{$item->description}
			</div>
			<div class="clear"></div>
		</div>		
	{foreachelse}
	<!--htdig_noindex-->
		<div>
			Er zijn geen agendaitems aanwezig
		</div>
	<!--/htdig_noindex-->
	{/foreach}
