{if $module->contentContainer->code == 'cc_content'}
<div class="pagination_holder">
	<h2>Agenda {if $module->view == 'archive' }archief{/if}</h2>
	{if $module->view =='overview'}
		<span class="right">
			<a href="{$smarty.server.PHP_SELF}?{$module->qsPart('view')}=archive">Archief</a>
		</span>
	{elseif $module->view == 'archive'}
		<span class="right">
			<a href="{$smarty.server.PHP_SELF}?{$module->qsPart('view')}=overview">Agenda</a>
		</span>
	{/if}
	<!--htdig_noindex-->
	{* pagination *}
		{include file="html/modules/agenda_pagination.tpl"}
	{* /pagination *}
	<!--/htdig_noindex-->
</div>
<br class="clear" /><br class="clear" />
{/if}

{if $module->view =='overview'}

	{include file="html/modules/agenda_overview.tpl"}

{elseif $module->view == 'archive'}

	{include file="html/modules/agenda_overview.tpl"}

	<br />
	<a href="{$smarty.server.PHP_SELF}">&laquo; terug naar actuele overzicht</a>

{elseif $module->view =='detailview'}
	{foreach from=$module->items key=key item=item}
	<div class="phoundry_{$module->config->content_table|default:$module->content_table}|{$item->id}">
		<strong>{$item->title|htmlspecialchars}</strong><br /><br />			
				{if !is_null($item->from_date)}
				<div class="type"><strong>Datum:</strong>
					{* toon alleen "tot" datum als "van" en "tot" datum gelijk zijn, toon de tijd niet als deze gelijk is aan 00:00 *}
					{if $item->from_date|date_format:"d F" != $item->till_date|date_format:"d F"}
						{if $item->from_date|date_format:"H:i" != '00:00'}
							{$item->from_date|date_format:"d F Y H:i"} <i>tot</i><br/> {$item->till_date|date_format:"d F H:i"}
						{elseif $item->from_date|date_format:"H:i" == '00:00'}
							{$item->from_date|date_format:"d F Y"} <i>tot</i><br/> {$item->till_date|date_format:"d F"}
						{/if}
					{elseif $item->from_date|date_format:"d F" == $item->till_date|date_format:"d F"}
						{if $item->from_date|date_format:"H:i" != '00:00'}
							{* als de "van" en "tot" datum gelijk zijn laat dan niet de "tot" datum zien maar wel de "tot" tijd *}
							{if $item->from_date|date_format:"H:i" == $item->till_date|date_format:"H:i"}
								{$item->from_date|date_format:"d F Y H:i"}
							{elseif $item->from_date|date_format:"H:i" != $item->till_date|date_format:"H:i"}
								{$item->from_date|date_format:"d F Y H:i"} - {$item->till_date|date_format:"H:i"}
							{/if}
						{elseif $item->from_date|date_format:"H:i" == '00:00'}
							{$item->from_date|date_format:"d F Y"}
						{/if}
					{/if}
				</div>
				<div class="clear" /></div>
				{/if}
				{if !is_null($item->location)}<div class="type"><strong>Locatie:</strong> {$item->location|htmlspecialchars}</div>{/if}
				{if !is_null($item->address)}<div class="type"><strong>Adres:</strong> {$item->address|htmlspecialchars}</div>{/if}
				{if !is_null($item->target_audience)}<div class="type"><strong>Voor wie:</strong> {$item->target_audience|htmlspecialchars}</div>{/if}
				{if !is_null($item->organisation)}<div class="type"><strong>Organisatie:</strong> {$item->organisation|htmlspecialchars}</div>{/if}
									
			{if !is_null($item->intro)}{$item->intro|htmlspecialchars|nl2br}<br />{/if}

		<strong>Wat: </strong><br />{$item->description}
		<br /><br />
		<!--htdig_noindex-->
			<a href="{$smarty.server.PHP_SELF}?{$module->qsPart('page_nr')}={$module->return_page_id}{if $item->archived}&amp;{$module->qsPart('view')}=archive{/if}">&laquo; Terug</a>
		<!--/htdig_noindex-->
	</div>
	{/foreach}

{elseif $module->view == 'miniview'}
	<span class="header3">Agenda items miniview</span>
	<div class="miniview_agenda">
		{foreach from=$module->items key=key item=item }
			<a class="miniview" href="/index.php/page/{$module->detail_page_id}?{$module->qsPart('agenda_id', TRUE)}={$item->id}"><strong>{$item->title|htmlspecialchars}</strong></a><br />
			<a class="miniview" href="/index.php/page/{$module->detail_page_id}?{$module->qsPart('agenda_id', TRUE)}={$item->id}">{$item->from_date|date_format:"d F H:i"}
			{$item->location|htmlspecialchars}</a>
			<br /><br />
		{foreachelse}
			<!--htdig_noindex--><br />Er zijn geen agendaitems<br /><!--/htdig_noindex-->
		{/foreach}
		{*<a href="/index.php/page/{$module->detail_page_id}"><!--htdig_noindex-->Bekijk volledige agenda &raquo;<!--/htdig_noindex--></a>*}
	</div>
{/if}
