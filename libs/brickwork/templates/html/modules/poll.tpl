{if $module->poll_id != 0 }
<div class="poll_body">	
	<h1>Poll</h1>
	<div class="message phoundry_module_content_poll_question|{$module->poll_id}"><strong>{$module->poll->question}</strong></div>
	{if $module->view == 'vote'}
		<form name="poll" method="post" action="">
			<input type="hidden" name="{$module->qsPart('action')}" value="sendReaction" />
			<input type="hidden" name="{$module->qsPart('poll_id')}" value="{$module->poll_id}" />
				{foreach from=$module->poll->answers key=key item=answer}
					<div class="row">
						<div class="col1">
							<input type="radio" name="{$module->qsPart('answer_id')}" id="poll_{$answer->id}" value="{$answer->id}" onclick="this.form.submit();" />
						</div>
						<div class="col2 txt phoundry_module_content_poll_answer|{$answer->id}">
							<label for="poll_{$answer->id}">
								{$answer->answer|htmlspecialchars}
							</label>
						</div>
						<div class="break"></div>
					</div>
				{foreachelse}
					<div>
					Er zijn geen antwoorden bij deze poll op dit moment
					</div>
				{/foreach}
				{if $module->voted != TRUE}
					{* Als er geen Javascript gebruikt wordt: *}
						<noscript>
							<div align="center">
								<input type="submit" name="submit" value="Verstuur" />
							</div>
						</noscript>
				{/if}
				{if $module->poll->show_result == TRUE}
					<div class="row">
						<div class="col1">&nbsp;</div>
						<div class="col2">
							<a href="{$smarty.server.PHP_SELF}?{$module->qsPart('view')}=show_results&amp;{$module->qsPart('poll_id')}={$module->poll_id}">Bekijk resultaten</a>
						</div>
						<div class="floatstop"></div>
					</div>
				{/if}
		</form>
	{else $module->view == 'show_results'}
		{if $module->poll->show_result == TRUE}
				<div class="row">
					<div class="col1">&nbsp;</div>
					<div class="col2">
						Aantal stemmen: {$module->poll->totalVote}
					</div>
					<div class="floatstop"></div>
				</div>
				{foreach from=$module->poll->answers key=key item=answer}
					<div class="row phoundry_module_content_poll_answer|{$answer->id}">
						<div class="col1">
						{*$answer->nrVote*}
						{if $answer->nrVote != 0 }
							{math equation="round(( x / y ) * 100 )" x=$answer->nrVote y=$module->poll->totalVote}%
						{else}
							0 %
						{/if}
						</div>
						<div class="col2">
						{$answer->answer|htmlspecialchars}
						</div>
						<div class="floatstop"></div>
					</div>
				{/foreach}
			{if $module->poll->totalVote == 0 }
				<div align="center">Er zijn geen resultaten bij deze poll op dit moment</div>
			{/if}
		{/if}
		{if $module->voted == TRUE }
			<div align="center"><strong>Bedankt voor uw stem!</strong></div>
		{/if}
	{/if}
</div>
{else}
	<div class="message">Er zijn geen polls op dit moment</div>
{/if}