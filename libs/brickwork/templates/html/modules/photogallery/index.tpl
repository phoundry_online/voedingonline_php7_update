<!-- stack name="css_src" assign="photogallery_index" -->
<link rel="stylesheet" href="/index.php/script/css/photogallery|index.css" type="text/css" />
<!-- /stack -->
<div class="index">
	{if !empty($module->td.album)}
		{assign var=album value=$module->td.album}
		{assign var=photos value=$module->td.photos}
	{else}
		{assign var=albums value=$module->getAlbums()}
		{if count($albums) == 1}
			{assign var=album value=$albums[0]}
			{assign var=photos value=$module->getPhotos($album.id)}
		{/if}
	{/if}
	
	{if !empty($album)}
		<!-- stack name="javascript_src" assign="nyromodal" -->
		<script type="text/javascript" src="/index.php/script/js/jquery|nyroModal.js"></script>
		<!-- /stack -->
		<!-- stack name="css_src" assign="nyromodal" -->
		<link rel="stylesheet" href="/index.php/script/css/jquery|nyroModal.css" type="text/css" />
		<!-- /stack -->
		<div class="album phoundry_brickwork_photogallery_album|{$album.id}">
			<h2 class="title">{$album.title|htmlspecialchars}</h2>
			<div class="photos">
			{foreach from=$photos item=photo}
				<a class="nyroModal photo" href="/index.php/image/photogallery_photo/{$photo.location|replace:'/IMAGES/photogallery/':''|urlencodeUrl}" rel="album{$photo.album_id}" title="{$photo.description|default}">
					<img alt="{$photo.title|htmlspecialchars}" src="/index.php/image/photogallery_album/{$photo.location|replace:'/IMAGES/photogallery/':''|urlencodeUrl}" />
					<span class="title">{$photo.title|htmlspecialchars|truncate:25}</span>
					<span class="description">{$photo.description|default}</span>
				</a>
			{foreachelse}
				<div class="nophotos">Dit album heeft geen fotos</div>
			{/foreach}
			</div>
		</div>
	{else}
		{if !empty($albums)}
		<div class="albums">
			<h2 class="title">Albums</h2>
		{foreach from=$albums item=album}
			<a class="album" href="?{$module->qsPart('album')}={$album.id}">
				<img alt="{$album.title|htmlspecialchars}" src="/index.php/image/photogallery_album/{$album.thumbnail|replace:'/IMAGES/photogallery/':''|urlencodeUrl}" />
				<span class="title">{$album.title|htmlspecialchars}</span>
			</a>
		{foreachelse}
			<div class="noalbums">Geen albums</div>
		{/foreach}
		</div>
		{/if}
	{/if}
	<div class="footer"></div>
</div>