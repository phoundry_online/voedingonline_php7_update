<!-- stack name="javascript_src" assign="photogallery_coolslide" -->
<script type="text/javascript" src="/brickwork/js/jquery/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/brickwork/js/jquery/jquery.cycle.all.js"></script>
<script type="text/javascript" src="/brickwork/js/photogallery/coolslideThumbs.js"></script>
<!-- /stack -->
<!-- stack name="css_src" assign="photogallery_coolslide" -->
<link rel="stylesheet" href="/brickwork/css/photogallery/coolslideThumbs.css" type="text/css" />
<!-- /stack -->
{foreach from=$module->getAlbums() item=album}
{assign var=photos value=$module->getPhotos($album.id)}
<div class="coolslideThumbs">
	<h2 class="title">Album {$album.title|htmlspecialchars}</h2>
	
	{if $photos}
	<div class="slideshow_pics">
	{foreach from=$photos item=photo} 
		<img alt="{$photo.title|htmlspecialchars}" src="/index.php/image/photogallery_coolslide/{$photo.location|replace:'/IMAGES/photogallery/':''}" width="500" height="350px" />
	{/foreach}
	</div>
	{else}
	<div class="nophotos">Dit album heeft geen fotos</div>
	{/if}
	
	<ul class="slideshow_nav">
	{foreach from=$photos item=photo name=galerynav} 
		<li><a href="#"><img alt="{$photo.title|htmlspecialchars}" src="/index.php/image/photogallery_slidebar_thumb/{$photo.location|replace:'/IMAGES/photogallery/':''|urlencodeURL}" /></a></li>
	{/foreach}
	</ul>
</div>
{/foreach}