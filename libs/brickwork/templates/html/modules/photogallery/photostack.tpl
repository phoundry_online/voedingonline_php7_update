<!-- stack name="javascript_src" assign="photogallery_photostack" -->
<script type="text/javascript" src="/brickwork/js/jquery/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/brickwork/js/jquery/jquery.cycle.all.js"></script>
<script type="text/javascript" src="/brickwork/js/photogallery/photostack.js"></script>
<!-- /stack -->
<!-- stack name="css_src" assign="photogallery_photostack" -->
<link rel="stylesheet" href="/brickwork/css/photogallery/photostack.css" type="text/css" />
<!-- /stack -->
{foreach from=$module->getAlbums() item=album}
{assign var=photos value=$module->getPhotos($album.id)}
<div class="photostack">
	<h2 class="title">Album {$album.title|htmlspecialchars}</h2>
	
	{if $photos}
	<div class="slideshow_pics">
	{foreach from=$photos item=photo} 
		<img alt="{$photo.title|htmlspecialchars}" src="/index.php/image/photogallery_coolslide/{$photo.location|replace:'/IMAGES/photogallery/':''}" width="500" height="350px" />
	{/foreach}
	</div>
	{else}
	<div class="nophotos">Dit album heeft geen fotos</div>
	{/if}
	
	<div class="slideshow_nextprev">
		<span class="navprev">&lt; Vorige</span>
		<span class="navnext">Volgende &gt;</span>
	</div>
</div>
{/foreach}