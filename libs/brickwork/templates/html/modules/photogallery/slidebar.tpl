<!-- stack name="css_src" assign="photogallery_slidebar" -->
<link rel="stylesheet" href="/index.php/script/css/photogallery|slidebar.css" type="text/css" />
<!-- /stack -->
<div class="slidebar">
	<div class="photo phoundry_brickwork_photogallery_photo|{$module->td.photo.id}">
		<h2 class="title">{$module->td.photo.title|htmlspecialchars}</h2>
		<div>
			<img alt="{$module->td.photo.title|htmlspecialchars}" src="/index.php/image/photogallery_photo/{$module->td.photo.location|replace:'/IMAGES/photogallery/':''}" />
		</div>
		<div class="description">{$module->td.photo.description|htmlspecialchars}</div>
	</div>
	<div class="filmstrip">
			{section name=prev start=3 loop=3 step=-1}
			{if !empty($module->td.prev[prev])}
			{assign var=prev_id value=$module->td.prev[prev]}
			{assign var=photo value=$module->getPhotoById($prev_id)}
			<a class="photo" href="?{$module->qsPart('photo')}={$prev_id}">
				<img alt="{$photo.title|htmlspecialchars}" src="/index.php/image/photogallery_slidebar_thumb/{$photo.location|replace:'/IMAGES/photogallery/':''|urlencodeUrl}" />
				<span class="title">{$photo.title|htmlspecialchars}</span>
				<span class="description">{$photo.description|default}</span>
			</a>
			{/if}
			{/section}
			{assign var=photo value=$module->td.photo}
			<span class="photo">
				<img alt="{$photo.title|htmlspecialchars}" src="/index.php/image/photogallery_slidebar_thumb/{$photo.location|replace:'/IMAGES/photogallery/':''|urlencodeUrl}" />
				<span class="title">{$photo.title|htmlspecialchars}</span>
				<span class="description">{$photo.description|default}</span>
			</span>
			{section name=next start=0 loop=3 step=1}
			{if !empty($module->td.next[next])}
			{assign var=next_id value=$module->td.next[next]}
			{assign var=photo value=$module->getPhotoById($next_id)}
			<a class="photo" href="?{$module->qsPart('photo')}={$next_id}">
				<img alt="{$photo.title|htmlspecialchars}" src="/index.php/image/photogallery_slidebar_thumb/{$photo.location|replace:'/IMAGES/photogallery/':''|urlencodeUrl}" />
				<span class="title">{$photo.title|htmlspecialchars}</span>
				<span class="description">{$photo.description|default}</span>
			</a>
			{/if}
			{/section}
	</div>
</div>