<h2>Veelgestelde vragen</h2>

{if $module->view =='ArticleView'}
	
	<fieldset><legend>veelgestelde vraag</legend>
	 	{foreach from=$module->categories item=category}
			{if $category->id == $module->category_id}
				<h3>{$category->title}</h3>
				{foreach from=$category->items item=faq}
					<div class="phoundry_module_content_faq_article|{$faq->id}">
						<strong>{$faq->question}</strong><br /><br />
						<div class="answer">{$faq->answer}</div>
					</div>
				{/foreach}
				<br />
			{/if}
		{/foreach}			
	</fieldset><br />
	<a href="javascript:history.go(-1)">&laquo; Terug</a>
{/if}

{if $module->view =='CategoryView'}
	<p>
	Sorteer op:<br />
	<a href="?{if $module->article_id}{$module->qsPart('article_id')}={$module->article_id}{/if}{if $module->category_id}&amp;{$module->qsPart('category_id')}={$module->category_id}{/if}&amp;{$module->qsPart('sortby')}=alphabet">alphabet</a>&nbsp;&nbsp;

	<a href="?{if $module->article_id}{$module->qsPart('article_id')}={$module->article_id}{/if}{if $module->category_id}&amp;{$module->qsPart('category_id')}={$module->category_id}{/if}&amp;{$module->qsPart('sortby')}=prio">prio</a>&nbsp;&nbsp;

	<a href="?{if $module->article_id}{$module->qsPart('article_id')}={$module->article_id}{/if}{if $module->category_id}&amp;{$module->qsPart('category_id')}={$module->category_id}{/if}&amp;{$module->qsPart('sortby')}=populair">populair</a>
	</p>
	
	{foreach from=$module->categories item=category}
		{if $category->id == $module->category_id}
			<h3>{$category->title}</h3><br/>
			<ol class="list">
			{foreach from=$category->items item=faq name=faq}
				<li><a href="?{$module->qsPart('category_id')}={$category->id}&amp;{$module->qsPart('article_id')}={$faq->id}">{$faq->question}</a><br /></li>
			{/foreach}
			</ol>
			<br />
		{/if}
	{/foreach}
	<a href="?">Terug naar categorieën</a>
{/if}


{if $module->view =='OverView'}
	
	<p>
	Sorteer op:<br />
	<a href="?{if $module->article_id}{$module->qsPart('article_id')}={$module->article_id}{/if}{if $module->category_id}&amp;{$module->qsPart('category_id')}={$module->category_id}{/if}&amp;{$module->qsPart('sortby')}=alphabet">alphabet</a>&nbsp;&nbsp;

	<a href="?{if $module->article_id}{$module->qsPart('article_id')}={$module->article_id}{/if}{if $module->category_id}&amp;{$module->qsPart('category_id')}={$module->category_id}{/if}&amp;{$module->qsPart('sortby')}=prio">prio</a>&nbsp;&nbsp;

	
	</p>
	{foreach from=$module->categories item=category}
		<h3><a href="?{$module->qsPart('category_id')}={$category->id}">{$category->title}</a></h3>
			<ol class="list">
			{foreach from=$category->items item=faq}
				<li><a href="?{$module->qsPart('category_id')}={$category->id}&amp;{$module->qsPart('article_id')}={$faq->id}">{$faq->question}</a><br /></li>
			{/foreach}
			</ol>
		<br />
	{/foreach}

	
{/if}