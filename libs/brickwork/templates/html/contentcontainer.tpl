<div class="content_container" id="{$name|htmlspecialchars}">
	{if isset($PAGE->cContainers[$name]) && count($PAGE->cContainers[$name]) > 0}
		{foreach from=$PAGE->cContainers[$name] key=key item=module}
			<div id="module_{$module->id}" class="module {$module} {$module->code}">{$module->html}</div>
		{/foreach}
	{/if}
</div>
