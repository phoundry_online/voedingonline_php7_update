<?php
$inc = @include('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";

require_once INCLUDE_DIR.'functions/autoloader.include.php';
require_once INCLUDE_DIR.'functions/common.php';

// Init the FileCabinet
FileCabinet::$file_rootdir = UPLOAD_DIR."FILES/FileCabinet";

$TID = Utility::arrayValue($_GET,'TID');
$RID = Utility::arrayValue($_GET,'RID');
$site_identifier = Utility::arrayValue($_GET,'site_identifier');
$action = Utility::arrayValue($_REQUEST,'action');
$access_levels = array('read', 'write', 'delete');
$roleid = (int) Utility::arrayValue($_REQUEST,'role_id', 0);
$root_folders = FileCabinet::folderFactory()->
	where('parent_id', 'NULL', 'IS', false)->
	orderBy('prio', 'DESC')->
	orderBy('create_datetime', 'DESC')->
get();

$tmp = array();

while ($root_folders->current()) {
    $tmp = $root_folders->current();
    $root_folders->next();
}

$root_folders = $tmp;

switch ($action) {
	case 'commitchanges':
		if(isset($_POST['displayed_folders'])) {
			foreach($_POST['displayed_folders'] as $folder) {
				$folder = FileCabinet::folderFactory($folder);
				if($folder->isLoaded()) {
					$folder_rights = array();
					foreach($access_levels as $acc_lev) {
						if(!empty($_POST['access'][$acc_lev][$folder['id']])) {
							$folder_rights[] = $acc_lev;
						}
					}
					
					$folder->setAccess($folder_rights, $roleid);
				}
			}
		}
	break;
}


include 'index.tpl.php';


// Functions
function getRoles()
{
    $res = DB::iQuery('SELECT name, id FROM brickwork_auth_role ORDER BY name');

    $tmp = array();

    while ($res->current()) {
        $tmp = $res->current();
        $res->next();
    }
    return $tmp;
}