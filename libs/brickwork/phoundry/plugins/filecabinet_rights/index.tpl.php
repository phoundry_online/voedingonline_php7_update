<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en">
	<head>
		<title>File Cabinet Rights</title>
		<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
		<meta name="Author" content="Web Power BV, www.webpower.nl" />
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
		<!--[if IE]>
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
		<![endif]-->
		<link rel="stylesheet" href="css/style.css" type="text/css" />
		<style type="text/css">
		html, body
		{
			margin: 0px;
			padding: 0px;
		}
		
		.access_table tbody tr img.recursive
		{
			cursor: pointer;
		}
		</style>
		<script type="text/javascript" src="<?=$PHprefs['url']?>/core/csjs/global.js.php"></script>
		<script type="text/javascript">
			jQuery(function($){
				if ($.fn.prop === undefined) {
					$.fn.prop = $.fn.attr;
				}
				$('.access_table tbody tr img.recursive').bind('click', function(e){
					var row = $(this).parents('tr:first');
					if(confirm(<?php echo json_encode(word(11850)/*'Weet u zeker dat u alle rechten ook wilt doorvoeren naar onderliggende mappen'*/)?>)) {
						var deep = row.attr('id').split('_')[3],
						read = $('.access_read', row).prop('checked'),
						write = $('.access_write', row).prop('checked'),
						del = $('.access_delete', row).prop('checked'),
						next_deep = 0,
						next_row = row.next('tr');

						while(next_row && next_row.attr('id')) {
							next_deep = next_row.attr('id').split('_')[3];

							if(next_deep > deep) {
								$('.access_read', next_row).prop('checked', read);
								$('.access_write', next_row).prop('checked', write);
								$('.access_delete', next_row).prop('checked', del);
								next_row = next_row.next('tr');
							}
							else {
								next_row = false;
							}
						}
					}
				});
			});
		</script>
	</head>
	<body class="frames">
		<div class="headerFrame">
		<?= getHeader(word(11851)/*'Documenten Kabinet Werkgroep rechten'*/); ?>
		<table class="buttons" cellspacing="0" cellpadding="2">
			<tr>
				<td>
					<form method="post" id="changerole" action="<?=htmlspecialchars($_SERVER['REQUEST_URI'])?>">
					<div>
					<input type="hidden" name="action" value="changerole" />
					<?php echo word(11853);/*Groep*/ ?>:
					<select name="role_id" id="role_id" onchange="this.form.submit();">
						<option value="0"><?php echo word(11852);/*Niet ingelogde bezoekers*/ ?></option>
						<?foreach(getRoles() as $role):?>
						<option value="<?= $role->id ?>"<?= ($roleid == $role->id ? ' selected="selected"' : '') ?>><?= $role->name ?></option>
						<?endforeach?>
					</select>
					</div>
					</form>
				</td>
			</tr>
		</table>
		</div>
	
		<form action="<?=htmlspecialchars($_SERVER['REQUEST_URI'])?>" method="post">
		<div class="contentFrame" style="padding: 0px;">
		<input type="hidden" name="action" value="commitchanges" />
		<input type="hidden" name="role_id" value="<?=$roleid?>" />
		<fieldset>
			<legend><strong><?php echo word(11854);/*Rechten*/ ?></strong></legend>
			<table class="access_table">
				<thead>
					<tr>
						<th><?php echo word(11855);/*Map*/ ?></th>
						<th><?php echo word(11856);/*Leesrechten*/ ?></th>
						<th><?php echo word(11857);/*Schrijfrechten*/ ?></th>
						<th><?php echo word(11858);/*Verwijderrechten*/ ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(isset($root_folders)) {
						function paintTree($folders, $deep = 0) {
							global $roleid, $PHprefs;
							foreach($folders as $child) {
								$children = $child->getChildren();
							?>
							<tr id="access_tr_<?=$child['id']?>_<?=$deep?>">
								<td>
									<?=str_repeat('--', $deep)?> <?=htmlspecialchars($child['name'])?>
									<?if(count($children)):?>
										<img class="recursive" src="<?=$PHprefs['url']?>/core/icons/folder_go.png" />
									<?endif?>
									<input class="folder_id" type="hidden" name="displayed_folders[]" value="<?=$child['id']?>" />
								</td>
								<td>
									<input id="access_read_<?=$child['id']?>" class="access_read" type="checkbox" name="access[read][<?=$child['id']?>]" value="1"<?=$child->hasAccess('read', true, $roleid)? ' checked="checked"': '';?> />
								</td>
								<td>
									<input id="access_write_<?=$child['id']?>" class="access_write" type="checkbox" name="access[write][<?=$child['id']?>]" value="1"<?=$child->hasAccess('write', false, $roleid)? ' checked="checked"': '';?> />
								</td>
								<td>
									<input id="access_delete_<?=$child['id']?>" class="access_delete" type="checkbox" name="access[delete][<?=$child['id']?>]" value="1"<?=$child->hasAccess('delete', false, $roleid)? ' checked="checked"': '';?> />
								</td>
							</tr>
							<?php
								paintTree($children, ($deep+1)); 
							}
						}
						paintTree($root_folders);
					}
					?>
				</tbody>
			</table>		
		</fieldset>
		</div>
		<div class="footerFrame">
		<table class="buttons" cellspacing="0" cellpadding="2"><tr>
			<td>
			<input id="okBut" type="submit" style="width:100px;" value="<?= word(82) ?>" />
			</td>
			<td align="right">
			<input type="button" value="<?= word(46) ?>" onclick="_D.location='<?=$PHprefs['url']?>/layout/home.php'" />
			</td>
		</tr></table>
		</div>
		</form>
	</body>
</html>