<?php
class SiteCopy
{
	protected $site_identifier;
	protected $data_file;
	
	public $data_tree;
	
	public function __construct()
	{
		$this->data_tree = array();
	}
	
	
	public function export($site_identifier, $data_file)
	{
		$this->site_identifier = $site_identifier;
		$this->data_file = $data_file;
		
		$res = DB::iQuery(sprintf("SELECT `template`FROM brickwork_site WHERE identifier = '%s'", $this->site_identifier));
		$this->data_tree['template'] = current(current($res->getAsArray()));
		
		// Fetch the direct child records
		// site config
		$res = DB::iQuery(sprintf("SELECT `site_identifier`, `attribute`, `value`, `modified` FROM brickwork_site_config WHERE site_identifier = '%s'", $this->site_identifier));
		$this->data_tree['brickwork_site_config'] = $res->getAsArray();
		
		// site phoundry users
		$res = DB::iQuery(sprintf("SELECT * FROM brickwork_site_phoundry_user WHERE site_identifier = '%s'", $this->site_identifier));
		$this->data_tree['brickwork_site_phoundry_user'] = $res->getAsArray();
		
//		// AvoidForms
//		$res = DB::iQuery(sprintf("SELECT * FROM brickwork_avoid_formbuilder WHERE site_identifier = '%s'", $this->site_identifier));
//		$this->data_tree['brickwork_avoid_formbuilder'] = $res->getAsArray();
//		
//		// Brickwork Form
//		$res = DB::iQuery(sprintf("SELECT * FROM brickwork_form WHERE site_identifier = '%s'", $this->site_identifier));
//		$this->data_tree['brickwork_form'] = $res->getAsArray();
		
		// Site Structure
		$res = DB::iQuery(sprintf("SELECT * FROM brickwork_site_structure WHERE site_identifier = '%s'", $this->site_identifier));
		$this->data_tree['brickwork_site_structure'] = $res->getAsArray();
		
		// Fetch the Site Structure's child records
		foreach($this->data_tree['brickwork_site_structure'] as $k => $site_structure)
		{
			// Custom Site Structure
			$res = DB::iQuery(sprintf("SELECT * FROM brickwork_custom_site_structure WHERE custom_id = '%s'", $site_structure['id']));
			$this->data_tree['brickwork_site_structure'][$k]['brickwork_custom_site_structure'] = $res->getAsArray();
			
			// Beautifull urls Path
			$res = DB::iQuery(sprintf("SELECT * FROM brickwork_site_structure_path WHERE site_identifier = '%s' AND site_structure_id = '%s'", $this->site_identifier, $site_structure['id']));
			$this->data_tree['brickwork_site_structure'][$k]['brickwork_site_structure_path'] = $res->getAsArray();
			
			// Beautifull urls Path Archive
			$res = DB::iQuery(sprintf("SELECT * FROM brickwork_site_structure_path_archive WHERE site_identifier = '%s' AND site_structure_id = '%s'", $this->site_identifier, $site_structure['id']));
			$this->data_tree['brickwork_site_structure'][$k]['brickwork_site_structure_path_archive'] = $res->getAsArray();
			
			// Readable links
			$res = DB::iQuery(sprintf("SELECT * FROM brickwork_readable_links WHERE site_identifier = '%s' AND site_structure_id = '%s'", $this->site_identifier, $site_structure['id']));
			$this->data_tree['brickwork_site_structure'][$k]['brickwork_readable_links'] = $res->getAsArray();
			
			// Quick Menu
			$res = DB::iQuery(sprintf("SELECT * FROM brickwork_quick_menu WHERE site_identifier = '%s' AND site_structure_id = '%s'", $this->site_identifier, $site_structure['id']));
			$this->data_tree['brickwork_site_structure'][$k]['brickwork_quick_menu'] = $res->getAsArray();
			
			// Brickwork Url
			$res = DB::iQuery(sprintf("SELECT * FROM brickwork_url WHERE site_structure_id = '%s'", $site_structure['id']));
			$this->data_tree['brickwork_site_structure'][$k]['brickwork_url'] = $res->getAsArray();
			
			// Brickwork Page
			$res = DB::iQuery(sprintf("SELECT * FROM brickwork_page WHERE site_structure_id = '%s'", $site_structure['id']));
			$this->data_tree['brickwork_site_structure'][$k]['brickwork_page'] = $res->getAsArray();
			
			// Page's child records
			foreach($this->data_tree['brickwork_site_structure'][$k]['brickwork_page'] as $p => $page)
			{
				$res = DB::iQuery(sprintf("SELECT * FROM brickwork_page_content WHERE page_id = '%s'", $page['id']));
				$this->data_tree['brickwork_site_structure'][$k]['brickwork_page'][$p]['brickwork_page_content'] = $res->getAsArray();
			}
		}
		
		return $this->data_tree;
	}
	
	public function import($site_identifier, $data_tree)
	{
		$this->site_identifier = $site_identifier;
		$this->data_tree = $data_tree;
		
		// Set the template
		$res = DB::iQuery(sprintf("UPDATE brickwork_site SET `template` = '%s' WHERE identifier = '%s'",DB::quote($this->data_tree['template']), DB::quote($this->site_identifier)));
		
		// Insert the site_config
		foreach($this->data_tree['brickwork_site_config'] as $k => $v)
		{
			$this->data_tree['brickwork_site_config'][$k]['id'] = '';
			$this->data_tree['brickwork_site_config'][$k]['site_identifier'] = $this->site_identifier;
		}
		$res = DB::iQuery($this->insertInto($this->data_tree['brickwork_site_config'], 'brickwork_site_config'));
		
		// Insert the Phoundry users
		foreach($this->data_tree['brickwork_site_phoundry_user'] as $k => $v)
		{
			$this->data_tree['brickwork_site_phoundry_user'][$k]['site_identifier'] = $this->site_identifier;
		}
		$res = DB::iQuery($this->insertInto($this->data_tree['brickwork_site_phoundry_user'], 'brickwork_site_phoundry_user'));
		
		
		// Site Structure strange loop to fix the parent_id thingies
		$site_structure_inserted = array();
		$last_site_structures = $site_structures = $this->data_tree['brickwork_site_structure'];
		$i = 0;
		while(count($site_structures) != 0)
		{
			$ss = $site_structures[$i];
									
			if(is_null($ss['parent_id']) || key_exists($ss['parent_id'], $site_structure_inserted))
			{
				$old_id = $ss['id'];
				$ss['id'] = '';
				$ss['parent_id'] = (!is_null($ss['parent_id']) ? $site_structure_inserted[$ss['parent_id']] : NULL);
				$ss['site_identifier'] = $this->site_identifier;
				
				$res = DB::iQuery($this->insertInto(array($ss), 'brickwork_site_structure'));
				$site_structure_inserted[$old_id] = $res->last_insert_id;
				
				// Insert the Childs
				foreach($ss['brickwork_custom_site_structure'] as $k => $v) $ss['brickwork_custom_site_structure'][$k]['custom_id'] =  $site_structure_inserted[$old_id];
				$res = DB::iQuery($this->insertInto($ss['brickwork_custom_site_structure'], 'brickwork_custom_site_structure'));

				foreach($ss['brickwork_site_structure_path'] as $k => $v)
				{
					$ss['brickwork_site_structure_path'][$k]['site_identifier'] =  $this->site_identifier;
					$ss['brickwork_site_structure_path'][$k]['site_structure_id'] =  $site_structure_inserted[$old_id];
				}
				$res = DB::iQuery($this->insertInto($ss['brickwork_site_structure_path'], 'brickwork_site_structure_path'));
				
				foreach($ss['brickwork_site_structure_path_archive'] as $k => $v)
				{
					$ss['brickwork_site_structure_path_archive'][$k]['site_identifier'] =  $this->site_identifier;
					$ss['brickwork_site_structure_path_archive'][$k]['site_structure_id'] =  $site_structure_inserted[$old_id];
				}
				$res = DB::iQuery($this->insertInto($ss['brickwork_site_structure_path_archive'], 'brickwork_site_structure_path_archive'));
				
				foreach($ss['brickwork_readable_links'] as $k => $v)
				{
					$ss['brickwork_readable_links'][$k]['id'] =  '';
					$ss['brickwork_readable_links'][$k]['site_identifier'] =  $this->site_identifier;
					$ss['brickwork_readable_links'][$k]['site_structure_id'] =  $site_structure_inserted[$old_id];
				}
				$res = DB::iQuery($this->insertInto($ss['brickwork_readable_links'], 'brickwork_readable_links'));
				
				foreach($ss['brickwork_quick_menu'] as $k => $v)
				{
					$ss['brickwork_quick_menu'][$k]['id'] =  '';
					$ss['brickwork_quick_menu'][$k]['site_identifier'] =  $this->site_identifier;
					$ss['brickwork_quick_menu'][$k]['site_structure_id'] =  $site_structure_inserted[$old_id];
				}
				$res = DB::iQuery($this->insertInto($ss['brickwork_quick_menu'], 'brickwork_quick_menu'));
				
				foreach($ss['brickwork_url'] as $k => $v)
				{
					$ss['brickwork_url'][$k]['id'] = '';
					$ss['brickwork_url'][$k]['site_structure_id'] =  $site_structure_inserted[$old_id];
				}
				$res = DB::iQuery($this->insertInto($ss['brickwork_url'], 'brickwork_url'));
				
				foreach($ss['brickwork_page'] as $page)
				{
					$page['id'] = '';
					$page['site_structure_id'] =  $site_structure_inserted[$old_id];
					$res = DB::iQuery($this->insertInto(array($page), 'brickwork_page'));
					$page['id'] = $res->last_insert_id;
					foreach($page['brickwork_page_content'] as $k => $v)
					{
						$page['brickwork_page_content'][$k]['id'] = '';
						$page['brickwork_page_content'][$k]['page_id'] = $page['id'];						
					}
					$res = DB::iQuery($this->insertInto($page['brickwork_page_content'], 'brickwork_page_content'));
				}
				
				
				array_splice($site_structures, $i, 1);
			}
			if(!isset($site_structures[$i+1]))
			{
				if($site_structures === $last_site_structures) break;
				$last_site_structures = $site_structures;
				$i = 0;
			}
			else
			{
				$i++;
			}
		}
		
		
		return true;
	}
	
	/**
	 * Create Insert Into statement
	 *
	 * @param array $rows Multi deminsional array
	 * @param string $table
	 * @return string
	 */
	protected function insertInto(array $rows, $table = '')
	{
		if(!empty($rows) && is_array(current($rows)))
		{
			$values = array();
			$fields = array();
			$first_run = true;
			
			foreach($rows as $row)
			{
				$row_values = array();
				foreach($row as $field => $value)
				{
					if(!is_array($value))
					{
						if($first_run) $fields[] = DB::quote($field);
						$row_values[] = $this->quoteValue($value);
					}
				}
				$values[] = "(".implode(", ", $row_values).")";
				$first_run = false;						
			}
			
			$sql = sprintf('
				INSERT INTO
				%s (%s)
				VALUES
				%s;',
				DB::quote($table),
				"`".implode("`, `", $fields)."`",
				implode(",\n", $values)
			);
			
			return $sql;
		}
	}
	
	/**
	 * Quote the database values
	 *
	 * @param string $value
	 * @return string
	 */
	protected function quoteValue($value)
	{
		if(is_null($value)) return 'NULL';
		
		if(is_numeric($value)) return $value;

		return "'".DB::quote($value)."'";
	}
}
