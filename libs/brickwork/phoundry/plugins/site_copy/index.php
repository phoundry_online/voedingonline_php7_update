<?php
ob_start();
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

$site_identifier = (!empty($_GET['site_identifier'])) ? $_GET['site_identifier'] : NULL;
require_once 'SiteCopy.class.php';

$res = DB::iQuery("SELECT identifier as value, name as text FROM brickwork_site");
$sites = array();
foreach($res as $row) $sites[$row->value] = $row->text;

if(count($_POST) != 0)
{
	$errors = array();
	
	$from_site = isset($_POST['from']) && $_POST['from'] == 'site';
	$from_file = isset($_POST['from']) && $_POST['from'] == 'file';
	
	$to_site = isset($_POST['to']) && $_POST['to'] == 'site';
	$to_file = isset($_POST['to']) && $_POST['to'] == 'file';
	
	if(empty($_POST['from']))
		$errors[] = 'nothing selected to copy from';
		
	if(empty($_POST['to']))
		$errors[] = 'nothing selected to copy to';
	
	if($from_site && (!isset($_POST['from_site']) || !key_exists($_POST['from_site'], $sites)))
		$errors[] = 'no site selected to copy from';
	
	if($from_file && (!isset($_FILES['from_file']) || !is_uploaded_file($_FILES['from_file']['tmp_name']) || $_FILES['from_file']['error'] != 0))
		$errors[] = 'no file uploaded to copy from';
	
	if($to_site && (!isset($_POST['to_site']) || !key_exists($_POST['to_site'], $sites)))
		$errors[] = 'no site selected to copy to';
	
	if($to_file && $from_file)
		$errors[] = 'can\'t copy from file to file, use your favorite filemanager for that instead!';
		
	if($from_site && $to_site && $_POST['from_site'] == $_POST['to_site'])
		$errors[] = 'can\'t copy to the same site as we\'re copying from';
	
	if(count($errors) == 0)
	{
		$site_copy = new SiteCopy;
		$succesfully_imported = false;
		
		if($from_file && $to_site)
		{
			$data_tree = saverIncludeDataTree($_FILES['from_file']['tmp_name']);
			
			$site_copy->import($_POST['to_site'], $data_tree);
			$succesfully_imported = true;
		}
		
		if($from_site)
		{
			if($to_site)
			{
				$site_copy->import($_POST['to_site'], $site_copy->export($_POST['from_site']));
				$succesfully_imported = true;
			}
			else if($to_file)
			{
				$data_tree = $site_copy->export($_POST['from_site']);
				ob_clean();
				header('Content-type: text/plain');
				header('Content-Disposition: attachment; filename="'.$_POST['from_site'].'.dat"');
				echo "<?php\n\$data_tree = ".var_export($data_tree, true).";\n";
				exit(0);
			}
			
		}
		
		
	}
	
}

/**
 * Somewhat saver, since it won't register global variables easy this way
 */
function saverIncludeDataTree($filename)
{
	include($filename);
	return isset($data_tree) ? $data_tree : array();
}
ob_flush();
include 'index.tpl.php';