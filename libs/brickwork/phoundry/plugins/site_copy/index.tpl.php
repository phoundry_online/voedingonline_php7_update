<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en">
	<head>
		<title>Site Structure Copy</title>
		<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
		<meta name="Author" content="Web Power BV, www.webpower.nl" />
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
		<!--[if IE]>
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
		<![endif]-->
		<link rel="stylesheet" href="css/style.css" type="text/css" />
		<style type="text/css">
		</style>
		<script type="text/javascript" src="<?=$PHprefs['url']?>/core/csjs/global.js.php"></script>
	</head>
	<body class="frames">
		<div class="headerFrame">
		<?= getHeader('Site Structure Copy'); ?>
		<table class="buttons" cellspacing="0" cellpadding="2">
			<tr>
				<td>
					
				</td>
			</tr>
		</table>
		</div>
	
		<form action="<?=htmlspecialchars($_SERVER['REQUEST_URI'])?>" enctype="multipart/form-data" method="post">
		<div class="contentFrame">
		<?if(isset($succesfully_imported)):?>
		<div class="message">
			<?if($succesfully_imported == true):?>
				Site succesfully copied!
			<?else:?>
				Site copying failed!
			<?endif?>
		</div>
		<?endif?>
		<?if(!empty($errors)): ?>
		<div clas="errors">
			<?foreach($errors as $error):?>
			<div><?=htmlspecialchars(ucfirst($error))?></div>
			<?endforeach?>
		</div>
		<?endif?>
		<fieldset>
		<legend>From</legend>
		<div>
			<div>
			<label><input id="from_site" name="from" value="site" type="radio" />
			Site:</label>
				<select name="from_site" onclick="$(this).parent('div').find('input:radio').click();">
				<option value=""></option>
				<?foreach($sites as $value => $text): ?>
				<option value="<?=$value?>"><?=htmlspecialchars($text)?></option>
				<?endforeach?>
				</select>
			</div>
		</div>
		<div>
			<div>
			<label><input id="from_file" name="from" value="file" type="radio" />
			File:</label>
				<input name="from_file" type="file" onclick="$(this).parent('div').find('input:radio').click();" />
			</div>
		</div>
		</fieldset>
		<fieldset>
		<legend>To:</legend>
		<div>
			<div>
				<label><input id="to_site" name="to" value="site" type="radio" />
				Site:</label>
				<select name="to_site" onclick="$(this).parent('div').find('input:radio').click();">
				<option value=""></option>
				<?foreach($sites as $value => $text): ?>
				<option value="<?=$value?>"><?=htmlspecialchars($text)?></option>
				<?endforeach?>
				</select>
			</div>
		</div>
		<div>	
			<div>
				<label><input id="to_file" name="to" value="file" type="radio" />
				File</label>
			</div>
		</div>
		</fieldset>
		</div>
		<div class="footerFrame">
		<table class="buttons" cellspacing="0" cellpadding="2"><tr>
			<td>
				<input type="submit" value="Copy" />
			</td>
			<td align="right">
			<input type="button" value="<?= word(46) ?>" onclick="_D.location='../home.php'" />
			</td>
		</tr></table>
		</div>
		</form>
	</body>
</html>