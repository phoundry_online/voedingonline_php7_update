// Jw Player roept dit aan als een flash succesvol ingeladen is soort document ready dus
function playerReady(obj) {
	var player = jQuery.ClipEditor.JWPlayer.getPlayer(obj.id);
	
	// Omdat dit string literals zijn jQuery gebruiken ipv $
	player.addModelListener('TIME', 'jQuery.ClipEditor.JWPlayer.TIME'); 
};

(function($){
	$.ClipEditor = {};
	
	$.ClipEditor.sortTimeList = function sortTimeList(container, record, sort_field){
		return;
		
		if(!container) container = 'div.clip_events_times';
		if(!record) record = 'div.clip_events_time';
		if(!sort_field) sort_field = 'input.start';
	
		var start_dom = {}, start_time = [];
		$(container+' '+record).each(function(){
			var time = $(sort_field, this), time_val = $(time).val();
			if(start_dom[time_val]) {
				start_dom[time_val].push(this);
			} else {
				start_dom[time_val] = [this];
			}
			start_time.push(time_val);
		});
		
		var start_time_sorted = start_time.concat(); // Aka array copy
		
		start_time_sorted.sort(function(a,b){ return parseFloat(a) - parseFloat(b);});
		
		if(start_time_sorted != start_time) {
			$(start_time_sorted).each(function(){
				$(start_dom[this]).appendTo(container);
			});
		}
	};
	
	$.ClipEditor.JWPlayer = {
		'TIME' : function(obj){
			$.ClipEditor.JWPlayer.position = obj.position;
			$.ClipEditor.JWPlayer.duration = obj.duration;
		},
		'getPlayer': function getPlayer(gid) {
			return swfobject.getObjectById(gid);
		}, 
		'position' : 0,
		'duration' : 0
	};
	
	$.ClipEditor.callbacks = {
		'action_type' : {}
	};

})(jQuery);

jQuery(function($){
	// Wordt gebruikt om de nieuwe times, acties en captions te nummeren
	var new_times = 0, new_actions = 0, new_captions = 0;
	
	/* Panes */
	$('.pane_btn').click(function(e){
		var pane = $('#pane_'+this.id.replace(/^btn_/, ''));
		if(pane && pane.length != 0) {
			$('.pane_btn, div.contentFrame div.pane').removeClass('active');
			$(this).addClass('active');
			pane.addClass('active');
		}
	});

	/* Options */
	$('#prompt_title_new').attr('checked', $.cookie(site_identifier+'_prompt_title_new'));
	$('#prompt_title_new').click(function(e){
		$.cookie(site_identifier+'_prompt_title_new', $(this).attr('checked') ? 'checked' : null, { expires: 14});
	});
	
	
	$('#end_last_time_auto').attr('checked', $.cookie(site_identifier+'_end_last_time_auto'));
	$('#end_last_time_auto').click(function(e){
		$.cookie(site_identifier+'_end_last_time_auto', $(this).attr('checked') ? 'checked' : null, { expires: 14});
	});
	
	/* Video User Filter */
	$('#video_select').change(function(e){
		window.location = $.query.set('RID', $(this).val()).toString();
	});
	
	$('#add_new_time').click(function(e){
		e.preventDefault();
		var player = $.ClipEditor.JWPlayer.getPlayer('player_1');
		var prompt_title_new = $('#prompt_title_new').attr('checked');
		if(prompt_title_new) player.sendEvent("PLAY", "false");
		var name = prompt_title_new ? prompt('Naam:') : '';
		++new_times;
		
		html = $('<div id="clip_events_time_new_'+new_times+'" class="clip_events_time new last_added">').
			tmpl('clip_events_time_template', {new_id : new_times});
		$('.title input', html).val(name);
		$('.start input.start', html).val($.ClipEditor.JWPlayer.position);
		
		var clip_events_time_last = $('div.clip_events_times div.clip_events_time.last_added').removeClass('last_added');
		if($('#end_last_time_auto').attr('checked')) {
			if($('input.end', clip_events_time_last).val() == '') {
				$('input.end', clip_events_time_last).val(parseFloat($.ClipEditor.JWPlayer.position) - 0.1);
			}
		}
		
		$('div.clip_events_times').append(html);
		$.ClipEditor.sortTimeList()
		if(prompt_title_new) player.sendEvent("PLAY", "true");
		return false;
	});
	
	$('#add_new_caption').click(function(e){
		e.preventDefault();
		var player = $.ClipEditor.JWPlayer.getPlayer('player_1');
		var prompt_title_new = $('#prompt_title_new').attr('checked');
		++new_captions;
		
		html = $('<div id="clip_events_caption_new_'+new_captions+'" class="clip_events_caption columns new last_added">').
			tmpl('clip_events_caption_template', {new_id : new_captions});
		
		$('.start input.start', html).val($.ClipEditor.JWPlayer.position);
		
		var clip_events_caption_last = $('div.clip_events_captions div.clip_events_caption.last_added').removeClass('last_added');
		if(clip_events_caption_last.length != 0) {
			
			if($('.title input', clip_events_caption_last).val() == '') {
				if(prompt_title_new) player.sendEvent("PLAY", "false");
				var name = prompt_title_new ? prompt('Naam:') : '';
				$('.title input', clip_events_caption_last).val(name);
				if(prompt_title_new) player.sendEvent("PLAY", "true");
			}
			if($('#end_last_time_auto').attr('checked')) {
				if($('input.end', clip_events_caption_last).val() == '') {
					$('input.end', clip_events_caption_last).val(parseFloat($.ClipEditor.JWPlayer.position) - 0.1);
				}
			}
		}
		
		$('div.clip_events_captions').append(html);
		$.ClipEditor.sortTimeList('div.clip_events_captions', 'div.clip_events_caption');
		return false;
	});
	
	$('#panes').change(function(e){
		var changed = $(e.target);
		if(changed.is('select.action_type_select')) {
			var clip_events_action = changed.parents('div.clip_events_action:first');
			$('.edit input:hidden', clip_events_action).val('');
		}else if(changed.is('input.start')){
			if(changed.parents('div.clip_events_caption').length != 0) {
				$.ClipEditor.sortTimeList('div.clip_events_captions', 'div.clip_events_caption');
			} else {
				$.ClipEditor.sortTimeList();
			}
		}
		
	});
	
	$('#panes').click(function(e){
		var clicked = $(e.target);
		var record = clicked.parents('div.clip_events_time:first, div.clip_events_caption:first');
		
		if(clicked.is('.btn_start')) {
			$('input.start', record).val($.ClipEditor.JWPlayer.position);
		}else if(clicked.is('.btn_end')) {
			$('input.end', record).val($.ClipEditor.JWPlayer.position);
			return false;
		}else if(clicked.is('.btn_play_start')) {
			$.ClipEditor.JWPlayer.getPlayer('player_1').sendEvent("SEEK", $('input.start', record).val());
			return false;
		}else if(clicked.is('.btn_play_end')) {
			$.ClipEditor.JWPlayer.getPlayer('player_1').sendEvent("SEEK", $('input.end', record).val());
			return false;
		}else if(clicked.is('.btn_delete')) {
			var clip_events_action = clicked.parents('div.clip_events_action:first');
			if(clip_events_action && clip_events_action.length == 1) {
				if(confirm('Weet u zeker dat u '+clip_events_action.find('select.action_type_select option:selected').html()+' wilt verwijderen?')) clip_events_action.remove();
			} else {
				if(confirm('Weet u zeker dat u '+record.find('div.title input').val()+' wilt verwijderen?')) record.remove();
			}
			return false;
		}
	});
	
	$('#pane_times').click(function(e){
		var clicked = $(e.target);
		var clip_events_time = clicked.parents('div.clip_events_time:first');
		
		if(clicked.is('.btn_toggle')) {
			var src = clicked.attr('src');
			var alt = clicked.attr('alt');
			
			var time_actions = $('.actions', clip_events_time);
			if(time_actions.hasClass('open')) {
				time_actions.removeClass('open');
			} else {
				time_actions.addClass('open');
			}
			clicked.attr('src', alt);
			clicked.attr('alt', src);
			return false;
		}else if(clicked.is('.btn_action_add')) {
			++new_actions;
			
			var time_id = clip_events_time.attr('id').split('_');
			time_id = time_id[time_id.length-1];
			var time_new = (clip_events_time.hasClass('new')) ? 'new' : 'cur';
			
			html = $('<div id="clip_events_action_new_'+new_actions+'" class="clip_events_action columns new">').
				tmpl('clip_events_action_template', {new_id : new_actions, time_id : time_id, time_new : time_new});
			
			$('div.clip_events_actions', clip_events_time).append(html);
			return false;
		}else if(clicked.is('.btn_action_edit')) {
			var clip_events_action = clicked.parents('div.clip_events_action:first');
		
			var TID = action_type_tids[$('.type select', clip_events_action).val()];
			var RID = $('.edit input:hidden', clip_events_action).val();
			
			var action_id = clip_events_action.attr('id');
			var callback = "parent.jQuery.ClipEditor.callbacks.action_type['"+action_id+"']";
			
			$.ClipEditor.callbacks.action_type[action_id] = function(new_id) {
				$('.edit input:hidden', clip_events_action).val(new_id);
			};
			
			var url = (RID && RID != '' && RID != 0) ? 
				phoundry_url+'/core/update.php?_video_id='+video_id+'&TID='+TID+'&site_identifier='+site_identifier+'&RID='+RID+'&popup=close&callback='+urlencode(callback) :
				phoundry_url+'/core/insert.php?_video_id='+video_id+'&TID='+TID+'&site_identifier='+site_identifier+'&popup=close&callback='+urlencode(callback);
			
			$.fn.InlinePopup({
				width: -10,
				height: -10,
				title: 'Actie',
				modal: true,
				event: null,
				html:null,
				url: url
			});
			
			return false;
		}
		
	});
	
	
	function urlencode(str) {
		return escape(str).replace('+', '%2B').replace('%20', '+').replace('*', '%2A').replace('/', '%2F').replace('@', '%40');
	}
	
	function urldecode(str) {
		return unescape(str.replace('+', ' '));
	}
});