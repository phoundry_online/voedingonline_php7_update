<?php
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';
$site_identifier = (!empty($_GET['site_identifier'])) ? $_GET['site_identifier'] : NULL;
$TID = getTID();
if (isset($_GET['RID'])) {
	$RID = (int)$_GET['RID'];
	$page = 'update';
}
else{
	$page = 'insert';
}

DB::setCollation('utf8');

$video = ActiveRecord::factory('ClipEvents_Video', isset($RID) ? $RID : null);

$videos = array();
foreach(ActiveRecord::factory('ClipEvents_Video')->get() as $vid) {
	$videos[$vid['id']] = $vid['name'];
}

$action_types = array();
$action_type_tids = array();
foreach(ActiveRecord::factory('ClipEvents_ActionType')->get() as $action_type) {
	$action_types[$action_type['id']] = $action_type['name'];
	$action_type_tids[$action_type['id']] = $action_type['tid'];
}

$domtargets = array();
foreach($video->domtargets as $domtarget) {
	$domtargets[$domtarget['id']] = $domtarget['name'];
}

if(!empty($_POST)) {
	
	// Geen gezeik met variables in de globale scope
	function processPost($post, $video) {
		
		$current_times = isset($post['clip_events_time']['cur']) ? $post['clip_events_time']['cur'] : array();
		$new_times = isset($post['clip_events_time']['new']) ? $post['clip_events_time']['new'] : array();
		
		$time_ids = array_keys($current_times);
		$action_ids = array();
		foreach($current_times as $time) {
			if(isset($time['cur_actions'])) {
				$action_ids = array_merge($action_ids, array_keys($time['cur_actions']));
			}
		}
		
		foreach($video->times as $time) {
			if(!in_array($time['id'], $time_ids)) {
				$time->delete();
			}
		}
		
		foreach($video->actions as $action) {
			if(!in_array($action['id'], $action_ids)) {
				$action->delete();
			}
		}
		
		foreach($current_times as $k => $time) {
			$updTime = ActiveRecord::factory('ClipEvents_Time', $k);
			if($updTime->isLoaded()) {
				foreach($time as $k => $value) {
					if(!is_array($value)) {
						$updTime[$k] = $value;
					}
				}
				$updTime->save();
				
				if(isset($time['cur_actions'])) {
					foreach($time['cur_actions'] as $k => $action) {
						$updAction = ActiveRecord::factory('ClipEvents_Action', $k);
						
						if($updAction->isLoaded()) {
							$updAction['time_id'] = $updTime['id'];
							foreach ($action as $k => $value) {
								$updAction[$k] = $value;
							}
							$updAction->save();
						}
					}
				}
				
				if(isset($time['new_actions'])) {
					foreach($time['new_actions'] as $action) {
						$newAction = ActiveRecord::factory('ClipEvents_Action')->where('time_id', $updTime['id'])->where($action)->get(TRUE);
						
						if(!$newAction->isLoaded()) {
							$newAction['time_id'] = $updTime['id'];
							$newAction['video_id'] = $video['id'];
							foreach($action as $k => $value) {
								$newAction[$k] = $value;
							}
							
							$newAction->save();
						}
					}
				}
			}
		}
		
		foreach($new_times as $time) {
			$where = array();
			foreach($time as $k => $v) {
				if(!is_array($v)) {
					$where[$k] = $v;
				}
			}
			
			$newTime = ActiveRecord::factory('ClipEvents_Time')->where($where)->get(TRUE);
			
			if(!$newTime->isLoaded()) {
				$newTime['video_id'] = $video['id'];
				foreach($time as $k => $value) {
					if(!is_array($value)) {
						$newTime[$k] = $value;
					}
				}
				$newTime->save();
				
				if(isset($time['cur_actions'])) {
					foreach($time['cur_actions'] as $k => $action) {
						$updAction = ActiveRecord::factory('ClipEvents_Action', $k);
						
						if($updAction->isLoaded()) {
							$updAction['time_id'] = $newTime['id'];
							foreach ($action as $k => $value) {
								$updAction[$k] = $value;
							}
							$updAction->save();
						}
					}
				}
				
				if(isset($time['new_actions'])) {
					foreach($time['new_actions'] as $action) {
						$newAction = ActiveRecord::factory('ClipEvents_Action')->where('time_id', $newTime['id'])->where($action)->get(TRUE);
						
						if(!$newAction->isLoaded()) {
							$newAction['time_id'] = $newTime['id'];
							$newAction['video_id'] = $video['id'];
							foreach($action as $k => $value) {
								$newAction[$k] = $value;
							}
							
							$newAction->save();
						}
					}
				}
			}
		}
		
		$cur_captions = isset($post['clip_events_caption']['cur']) ? $post['clip_events_caption']['cur'] : array();
		$new_captions = isset($post['clip_events_caption']['new']) ? $post['clip_events_caption']['new'] : array();

		$caption_ids = array_keys($cur_captions);
		foreach($video->captions as $caption) {
			if(!in_array($caption['id'], $caption_ids)) {
				$caption->delete();
			}
		}
		
		foreach($cur_captions as $k => $caption) {
			$updCaption = ActiveRecord::factory('ClipEvents_Caption', $k);
			if($updCaption->isLoaded()) {
				foreach($caption as $k => $value) {
					$updCaption[$k] = $value;
				}
				$updCaption->save();
			}
		}
		
		foreach($new_captions as $caption) {
			$newCaption = ActiveRecord::factory('ClipEvents_Caption')->where($caption)->get(TRUE);
			
			if(!$newCaption->isLoaded()) {
				$newCaption['video_id'] = $video['id'];
				foreach($caption as $k => $value) {
					$newCaption[$k] = $value;
				}
				$newCaption->save();
			}
		}
		
		$video->resetRelationCache();
		
		return true;
	}
		
	if(processPost($_POST, $video)) {
		trigger_redirect($PHprefs['url'].'/core/records.php?TID='.$TID.'&site_identifier='.$site_identifier.'&_hilite='.$video['id']);
	}
}

include 'html/index.tpl.php';