<?php
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?=$PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Nieuws beheren</title>
<link rel="stylesheet" href="<?=$PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?=$PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->

<script type="text/javascript" src="<?=$PHprefs['url']?>/core/csjs/global.js.php"></script>
<script type="text/javascript" src="/index.php/script/js/swfobject.js/jquery|jquery.query.js/jquery|jquery.cookie.js/jquery|jquery.tmpl.js?nopack"></script>
<script type="text/javascript" src="js/clipeditor.js"></script>
<link rel="stylesheet" href="style.css" type="text/css" />

<script type="text/javascript">
var isDirty = false,
site_identifier = '<?=$site_identifier?>',
action_type_tids = <?=json_encode($action_type_tids)?>,
video_id = '<?=$video['id']?>',
phoundry_url = '<?=$PHprefs['url']?>';
jQuery(function($){
	$('body').change(function(){
		isDirty = true;
	});
});
</script>
</head>
<body class="frames" onbeforeunload="if(isDirty)return '<?php echo escSquote(word(79));?>'">
<div id="headerFrame" class="headerFrame">
<?=getHeader('Clip Editor');?>

<table style="position: absolute; top: 6px; right: 6px;" class="filter-table"><tbody><tr><td><b>Video</b>:<br/>
<?=Html::inputSelect(array('name' => 'video_select', 'id' => 'video_select'), $videos, $video['id'])?>
</td></tr></tbody></table>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD">
<!-- Buttons -->
<div id="header">
<label><input type="checkbox" id="prompt_title_new" />Direct titel invullen</label>
<label><input type="checkbox" id="end_last_time_auto" />Automatisch laatste tijdselectie sluiten</label>
</div>
<!-- /Buttons -->
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<div id="pane_errors" ondblclick="$(this).hide();">
<?if(!empty($errors) && is_array($errors)):?>
<fieldset>
	<legend><strong><span id="save_error_label">Errors</span></strong></legend>
	<div class="errors">
		<?foreach($errors as $error):?>
		<div class="error"><?=$error?></div>
		<?endforeach?>
	</div>
</fieldset>
<?endif?>
</div>
<div style="width: 975px;">
	<fieldset style="float: left; width: 465px;">
		<legend><strong>Video</strong> <?=isset($video)?$video['name']:''?></legend>
		<div id="clipfcontainer" style="width: 460px; height: 350px;"></div>
		<script type="text/javascript">
		jQuery(function(){
			swfobject.embedSWF(
				"/brickwork/flash/jwplayer/player.swf",
				"clipfcontainer",
				"460",
				"350",
				"8.0.0",
				"swfobject/expressInstall.swf",
				{
					width : '460',
					height: '350',
					javascriptid: 'player_1',
					enablejs: 'true',
					file: '<?=(null === $video['url'] ? '/uploaded'.$video['file'] : $video['url'])?>',
					autostart : 'false',
					stretching : 'exactfit',
					repeat : 'none',
					controlbar : 'bottom',
					bufferlength: 3
				},
				{
					allowfullscreen: 'true',
					allowscriptaccess: 'always',
					wmode: 'transparent'
				},
				{
				  id: "player_1",
				  name: "player_1"
				}
			);
		});
		</script>
	</fieldset>
	<form id="clip_events_form" enctype="multipart/form-data" action="<?=$_SERVER['REQUEST_URI']?>" method="post" autocomplete="off">
	<input type="hidden" name="site_identifier" value="<?php echo $site_identifier ?>" />
	<input type="hidden" name="video_id" value="<?=isset($video) ? $video['id'] : ''?>" />
	<fieldset id="panes" style="float: left; width: 465px;">
		<legend><strong id="btn_times" class="pane_btn active">Tijd Selecties</strong> | <strong id="btn_captions" class="pane_btn">Ondertiteling</strong></legend>
		
		<div id="pane_times" class="pane active" style="height: 350px; width: 465px;">
			<div class="columns">
				<div class="toggle">&nbsp;</div>
				<div class="delete">&nbsp;</div>
				<div class="title">Titel</div>
				<div class="play">&nbsp;</div>
				<div class="start">Start</div>
				<div class="play">&nbsp;</div>
				<div class="end">Eind</div>
			</div>
			<div class="clip_events_times" style="overflow: auto; height: 315px; clear: both;">
				<?foreach($video->times as $time):?>
				<div id="clip_events_time_<?=$time['id']?>" class="clip_events_time">
					<div class="columns">
						<div class="toggle">
							<img class="btn_toggle" src="<?=$PHprefs['url']?>/core/icons/add.png" alt="<?=$PHprefs['url']?>/core/icons/delete.png" title="toggle" />
						</div>
						<div class="delete">
							<img class="btn_delete" src="<?=$PHprefs['url']?>/core/icons/bin.png" alt="delete" title="delete" />
						</div>
						<div class="title">
							<input name="clip_events_time[cur][<?=$time['id']?>][name]" type="text" value="<?=htmlspecialchars($time['name'])?>" />
						</div>
						<div class="play">
							<img class="btn_play_start" src="<?=$PHprefs['url']?>/core/icons/control_play.png" alt="play" title="play" />
						</div>
						<div class="start">
							<img class="btn_start" src="<?=$PHprefs['url']?>/core/icons/film_go.png" alt="nu" />
							<input class="start" name="clip_events_time[cur][<?=$time['id']?>][start]" type="text" value="<?=htmlspecialchars($time['start'])?>" />
						</div>
						<div class="play">
							<img class="btn_play_end" src="<?=$PHprefs['url']?>/core/icons/control_play.png" alt="play" title="play" />
						</div>
						<div class="end">
							<img class="btn_end" src="<?=$PHprefs['url']?>/core/icons/film_go.png" alt="nu" />
							<input class="end" name="clip_events_time[cur][<?=$time['id']?>][end]" type="text" value="<?=htmlspecialchars($time['end'])?>" />
						</div>
					</div>
					<div class="actions">
						<div class="columns">
							<div class="delete">&nbsp;</div>
							<div class="domtarget">Pagina onderdeel</div>
							<div class="type">Type</div>	
							<div class="edit"><a class="btn_action_add" href="#">[add]</a></div>
						</div>
						<div class="clip_events_actions">
							<?foreach($time->actions as $action):?>
							<div id="clip_events_action_<?=$action['id']?>" class="clip_events_action columns">
								<div class="delete">
									<img class="btn_delete" src="<?=$PHprefs['url']?>/core/icons/bin.png" alt="delete" title="delete" />
								</div>
								<div class="domtarget">
									<?=Html::inputSelect('clip_events_time[cur]['.$time['id'].'][cur_actions]['.$action['id'].'][domtarget_id]', $domtargets, $action['domtarget_id'])?>
								</div>
								<div class="type">
									<?=Html::inputSelect(array('name' => 'clip_events_time[cur]['.$time['id'].'][cur_actions]['.$action['id'].'][action_type_id]', 'class' => 'action_type_select'), $action_types, $action['action_type_id'])?>
								</div>	
								<div class="edit">
									<input name="clip_events_time[cur][<?=$time['id']?>][cur_actions][<?=$action['id']?>][type_pk]" type="hidden" value="<?=$action['type_pk']?>" />
									<a class="btn_action_edit" href="#">[edit]</a>
								</div>
							</div>
							<?endforeach?>
						</div>
					</div>
				</div>
				<?endforeach?>
			</div>
			<input type="button" id="add_new_time" value="Nieuwe selectie" />
		</div>
		
		<div id="pane_captions" class="pane">
			<div class="columns">
				<div class="delete">&nbsp;</div>
				<div class="title">Titel</div>
				<div class="play">&nbsp;</div>
				<div class="start">Start</div>
				<div class="play">&nbsp;</div>
				<div class="end">Eind</div>
			</div>
			<div class="clip_events_captions" style="overflow: auto; height: 315px; clear: both;">
				<?foreach($video->captions as $caption):?>
				<div id="clip_events_caption_<?=$caption['id']?>" class="clip_events_caption columns">
					<div class="delete">
						<img class="btn_delete" src="<?=$PHprefs['url']?>/core/icons/bin.png" alt="delete" title="delete" />
					</div>
					<div class="title">
						<input name="clip_events_caption[cur][<?=$caption['id']?>][text]" type="text" value="<?=htmlspecialchars($caption['text'])?>" />
					</div>
					<div class="play">
						<img class="btn_play_start" src="<?=$PHprefs['url']?>/core/icons/control_play.png" alt="play" title="play" />
					</div>
					<div class="start">
						<img class="btn_start" src="<?=$PHprefs['url']?>/core/icons/film_go.png" alt="nu" />
						<input class="start" name="clip_events_caption[cur][<?=$caption['id']?>][start]" type="text" value="<?=htmlspecialchars($caption['start'])?>" />
					</div>
					<div class="play">
						<img class="btn_play_end" src="<?=$PHprefs['url']?>/core/icons/control_play.png" alt="play" title="play" />
					</div>
					<div class="end">
						<img class="btn_end" src="<?=$PHprefs['url']?>/core/icons/film_go.png" alt="nu" />
						<input class="end" name="clip_events_caption[cur][<?=$caption['id']?>][end]" type="text" value="<?=htmlspecialchars($caption['end'])?>" />
					</div>
				</div>
				<?endforeach?>
			</div>
			<input type="button" id="add_new_caption" value="Nieuwe titel" />
		</div>
		
	</fieldset>
	</form>
</div>
<br clear="all" />
<!-- /Content -->
</div>
<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
		<input id="save" type="button" value="<?= word(82) ?>" onclick="isDirty = false; $('#clip_events_form').submit(); return true;" />
	</td>
	<td align="right">
		<input type="button" value="<?= word(46 /* Cancel */) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
	</td>
</tr></table>
</div>


<script type="text/html" id="clip_events_time_template">
	<div class="columns">
		<div class="toggle">
			<img class="btn_toggle" src="<?=$PHprefs['url']?>/core/icons/add.png" alt="<?=$PHprefs['url']?>/core/icons/delete.png" title="toggle" />
		</div>
		<div class="delete">
			<img class="btn_delete" src="<?=$PHprefs['url']?>/core/icons/bin.png" alt="delete" title="delete" />
		</div>
		<div class="title">
			<input name="clip_events_time[new][<%=new_id%>][name]" type="text" />
		</div>
		<div class="play">
			<img class="btn_play_start" src="<?=$PHprefs['url']?>/core/icons/control_play.png" alt="play" title="play" />
		</div>
		<div class="start">
			<img class="btn_start" src="<?=$PHprefs['url']?>/core/icons/film_go.png" alt="nu" />
			<input class="start" name="clip_events_time[new][<%=new_id%>][start]" type="text" />
		</div>
		<div class="play">
			<img class="btn_play_end" src="<?=$PHprefs['url']?>/core/icons/control_play.png" alt="play" title="play" />
		</div>
		<div class="end">
			<img class="btn_end" src="<?=$PHprefs['url']?>/core/icons/film_go.png" alt="nu" />
			<input class="end" name="clip_events_time[new][<%=new_id%>][end]" type="text" />
		</div>
	</div>
	<div class="actions">
		<div class="columns">
			<div class="delete">&nbsp;</div>
			<div class="domtarget">Pagina onderdeel</div>
			<div class="type">Type</div>	
			<div class="edit"><a class="btn_action_add" href="#">[add]</a></div>
		</div>
		<div class="clip_events_actions">
		</div>
	</div>
</script>

<script type="text/html" id="clip_events_action_template">
		<div class="delete">
			<img class="btn_delete" src="<?=$PHprefs['url']?>/core/icons/bin.png" alt="delete" title="delete" />
		</div>
		<div class="domtarget">
			<?=Html::inputSelect('clip_events_time[<%=time_new%>][<%=time_id%>][new_actions][<%=new_id%>][domtarget_id]', $domtargets)?>
		</div>
		<div class="type">
			<?=Html::inputSelect('clip_events_time[<%=time_new%>][<%=time_id%>][new_actions][<%=new_id%>][action_type_id]', $action_types)?>
		</div>	
		<div class="edit">
			<input name="clip_events_time[<%=time_new%>][<%=time_id%>][new_actions][<%=new_id%>][type_pk]" type="hidden" />
			<a class="btn_action_edit" href="#">[edit]</a>
		</div>
</script>

<script type="text/html" id="clip_events_caption_template">
	<div class="delete">
		<img class="btn_delete" src="<?=$PHprefs['url']?>/core/icons/bin.png" alt="delete" title="delete" />
	</div>
	<div class="title">
		<input name="clip_events_caption[new][<%=new_id%>][text]" type="text" />
	</div>
	<div class="play">
		<img class="btn_play_start" src="<?=$PHprefs['url']?>/core/icons/control_play.png" alt="play" title="play" />
	</div>
	<div class="start">
		<img class="btn_start" src="<?=$PHprefs['url']?>/core/icons/film_go.png" alt="nu" />
		<input class="start" name="clip_events_caption[new][<%=new_id%>][start]" type="text" />
	</div>
	<div class="play">
		<img class="btn_play_end" src="<?=$PHprefs['url']?>/core/icons/control_play.png" alt="play" title="play" />
	</div>
	<div class="end">
		<img class="btn_end" src="<?=$PHprefs['url']?>/core/icons/film_go.png" alt="nu" />
		<input class="end" name="clip_events_caption[new][<%=new_id%>][end]" type="text" />
	</div>
</script>
</body>
</html>