<?php
/**
 * Page Contents Plugin
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class PageContentsPlugin extends Phoundry_Brickwork_Plugin
{
	/**
	 * @var Model_Page
	 */
	private $_page;
	
	/**
	 * Initialize page, title and subtitle
	 * 
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_page = ActiveRecord::factory("Model_Page", $this->_rid);
		
		$this->_title = "Pagina inhoud"; //$this->word(11600 /* Structuur */);
		$this->_subtitle = $this->_page->site_structure['name'];
	}
	
	/**
	 * Default Page
	 * 
	 * @return void
	 */
	public function indexAction()
	{
		PH::checkAccess($this->_tid);
		if(!$this->_page->isLoaded()) {
			PH::phoundryErrorPrinter(word(11801) /* Record not found */, true);
		}
		
		if(isset($_POST['state'], $_POST['order'])) {
			$this->_setState($_POST['state'], $_POST['order']);
			$this->_page->resetRelationCache();
		}
		
		$this->_data["contents"] = $this->_page->page_contents;
		$this->_data["content_containers"] = $this->_getContentContainers(
			$this->_page->page_type);
		$this->_data["modules"] = $this->_getModules($this->_page->page_type);
		$this->_data['state'] = $this->_getState($this->_page);
		$this->_data['modules_css'] = $this->_getModulesCss($this->_page->page_type);
		
		$this->render('html/index.tpl.php', true);
	}
	
	/**
	 * Render the page_layout html with containers
	 * 
	 * @return void
	 */
	public function pageAction()
	{
		if(!$this->_page->isLoaded()) {
			PH::phoundryErrorPrinter(word(11801) /* Record not found */, true);
		}
		
		echo $this->_getContentSpecification($this->_page->page_type);
	}
	
	/**
	 * Redirect to the properties page
	 * 
	 * @param int $page_content_id
	 * @return void
	 */
	public function propertiesAction($page_content_id)
	{
		global $PHprefs;
		$tid = PhoundryTools::getTableIdByString("brickwork_page_content");
		$url = array($PHprefs['url'],
			"/core/update.php?TID=", $tid,
			'&RID=', $page_content_id,
			'&popup=close&site_identifier=', $this->site_identifier);
		
		trigger_redirect(implode("", $url));
	}
	
	/**
	 * Redirect to the config page
	 * 
	 * @param int $page_content_id
	 * @return void
	 */
	public function configAction($page_content_id)
	{
		global $PHprefs;
		$page_content = ActiveRecord::factory("Model_Page_Content", $page_content_id);
		$tid = $page_content->module['ptid_config'];
		$found = false;
		
		if($table = PhoundryTools::getTableName($tid)) {
			$res = DB::iQuery(sprintf("
				SELECT COUNT(*) AS cnt
				FROM %s
				WHERE page_content_id = %d",
				$table,
				$page_content_id
			));
			
			if($row = $res->first()) {
				$found = "1" === $row->cnt;
			}
		}
		
		$url = array($PHprefs['url'],
			"/core/", ($found ? "update" : "insert"), ".php?TID=", $tid,
			'&RID=', $page_content_id,
			'&popup=close&site_identifier=', $this->site_identifier);
		
		trigger_redirect(implode("", $url));
	}
	
	/**
	 * Redirect to the content record list
	 * 
	 * @param int $page_content_id
	 * @return void
	 */
	public function contentAction($page_content_id)
	{
		global $PHprefs;
		$page_content = ActiveRecord::factory("Model_Page_Content", $page_content_id);
		$tid = $page_content->module['ptid_content'];
		
		$url = array($PHprefs['url'], "/core/records.php?TID=", $tid,
			"&page_content_id=", $page_content_id,
			"&popup=close&site_identifier=", $this->site_identifier);
		
		trigger_redirect(implode("", $url));
	}
	
	/**
	 * Return the state of the page as a array
	 * 
	 * @param Model_Page $page
	 */
	private function _getState(Model_Page $page)
	{
		$state = array();
		foreach($page->page_type->page_content_containers as $cc) {
			$state[$cc['code']] = array();
		}
		foreach($page->page_contents as $content) {
			if(array_key_exists($content['content_container_code'], $state)) {
				$state[$content['content_container_code']][$content['id']] =
					$content->getAsArray();
			}
		}
		return $state;
	}
	
	private function _setState($state, $order)
	{
		$state = json_decode($state, true);
		$order = json_decode($order, true);
		$current = $this->_getState($this->_page);
		
		foreach($state as $cc => $modules) {
			foreach($modules as $id => $module) {
				
				if(is_string($id) && "new_" === substr($id, 0, 4)) {
					$mod = ActiveRecord::factory("Model_Page_Content");
					$mod['content_container_code'] = $cc;
					$mod['page_id'] = $this->_page['id'];
					$mod['module_code'] = $module;
					$mod['prio'] = array_search($id, $order[$cc]) * 10;
					$mod['state'] = 'ok'; 
					$mod['staging_status'] = 'live';
					$mod['respect_auth'] = 1;
					$mod['online_date'] = new DB_Expression("NOW()");
					$mod['offline_date'] = new DB_Expression("DATE_ADD(NOW(), INTERVAL 10 YEAR)");
					$mod->save();
				}
				else {
					$mod = ActiveRecord::factory("Model_Page_Content", $id);
					if($mod->isLoaded()) {
						$mod['content_container_code'] = $cc;
						$mod['page_id'] = $this->_page['id'];
						$mod['prio'] = array_search($id, $order[$cc]) * 10;
						$mod->save();
					}
				}
			}
		}
		
		foreach($current as $cc => $modules) {
			foreach($modules as $id => $module) {
				if(!isset($state[$cc][$id])) {
					$mod = ActiveRecord::factory('Model_Page_Content', $id);
					$mod->delete(true);
				}
			}
		}
	}
	
	/**
	 * Get the content specification for a pagetype
	 * @param Model_Page_Type $page_type
	 * @return array Array with first style tags and as 2nd the body
	 */
	private function _getContentSpecification(Model_Page_Type $page_type)
	{
		global $PHprefs;
		$html = $page_type->getContentSpecification($this->_page->site_structure->site, false);
		
		$styles =<<<EOT
.content_container .add_button,
.content_container .edit_button
{
	cursor : pointer;
}

.content_container .module {
	cursor : move;
}

.inlineWindow .content .module .edit_button {
	display: none;
}

.content_container .module {
	padding: 3px;
	color: #000;
	font-size: 11px;
	background-color: #fff;
	border: 1px solid #000;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
}

.content_container .placeholder {
	border: 3px dashed black;
}
EOT;

	$styles .= $this->_getModulesCss($page_type);
		
		$html = str_replace("</head>", "<style type=\"text/css\">\n$styles\n\n</style>\n"."</head>", $html);
		
		$qs = QS(true);
		$javascript =<<<EOT
<script src="{$PHprefs['url']}/core/csjs/global.js.php?{$qs}&noFrames=true"></script>
<script src="{$PHprefs['url']}/custom/brickwork/page_contents/assets/jquery.ui.core.js"></script>
<script src="{$PHprefs['url']}/custom/brickwork/page_contents/assets/jquery.ui.widget.js"></script>
<script src="{$PHprefs['url']}/custom/brickwork/page_contents/assets/jquery.ui.mouse.js"></script>
<script src="{$PHprefs['url']}/custom/brickwork/page_contents/assets/jquery.ui.sortable.js"></script>
<script>
window.intializeSortables = function(){
	jQuery(".content_container").sortable({forcePlaceholderSize: true,
		placeholder: "placeholder", items: ".module", containment: "parent"});
};
</script>
EOT;
		
		$html = str_replace("</head>", "$javascript\n"."</head>", $html);
		
		return $html;
	}
	
	private function _getContentContainers(Model_Page_Type $page_type)
	{
		$res = array();
		
		foreach($page_type->page_content_containers as $cc) {
			$res[$cc['code']] = array(
				'id' => $cc['id'],
				'allowed_modules' => $cc->allowed_modules->getSelect('code')
			);
		}
		
		return $res;
	}
	
	private function _getModules(Model_Page_Type $page_type)
	{
		$tmp = new Model_Module();
		$modules = new ActiveRecordIterator("Model_Module", DB::iQuery(sprintf("
			SELECT %s
			FROM %s AS t
			INNER JOIN brickwork_content_container_allows_module AS a
			ON a.module_code = t.code
			INNER JOIN brickwork_content_container AS c
			ON c.id = a.content_container_id AND c.page_type_code = %s
			",
			$tmp->getColumnsSql("t"),
			$tmp->getTableName(),
			DB::quote($page_type['code'], true)
		)));
		
		$res = array();
		foreach($modules as $mod) {
			$mod = $mod->getAsArray();
			if(is_numeric($mod['name'])) {
				$mod['name'] = word($mod['name']);
			}
			$res[$mod['code']] = $mod;
		}
		
		return $res;
	}
	
	private function _getModulesCss(Model_Page_Type $page_type)
	{
		global $PHprefs;
		$styles = "
	.module_icon {
		display: block;
		width: 45px;
		height: 43px;
		background-repeat: no-repeat;
	}
	";
		
		foreach($this->_getModules($page_type) as $module) {
			$styles.= "
	.mod_icon_{$module['code']} {
		background-image: url({$PHprefs['url']}/custom/brickwork/page_contents/pics/{$module['code']}.jpg);
	}
	
	.mod_icon_{$module['code']}:HOVER,
	.mod_icon_hover_{$module['code']} {
		background-image: url({$PHprefs['url']}/custom/brickwork/page_contents/pics/{$module['code']}_hover.jpg);
	}
	";
		}
		return $styles;
	}
}