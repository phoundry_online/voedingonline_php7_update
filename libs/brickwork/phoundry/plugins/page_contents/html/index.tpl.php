<?$this->stack('head')?>
<meta name="pageID" id="pageID" content="brickwork.page_contents" />
<link rel="stylesheet" href="/brickwork/css/tree_component.css" type="text/css" />
<script src="/brickwork/js/jquery/jquery.hotkeys.js"></script>
<script src="assets/json2.js"></script>
<script src="assets/PageContentsPlugin.js"></script>
<script src="/brickwork/js/htmlspecialchars.js"></script>
<script src="assets/jquery.ui.core.js"></script>
<script src="assets/jquery.ui.widget.js"></script>
<script src="assets/jquery.ui.mouse.js"></script>
<script src="assets/jquery.ui.sortable.js"></script>

<style type="text/css">
div.contentFrame {
	padding: 0;
}

.inlineWindow .content .module {
	float: left;
	overflow: hidden;
	width: 100px;
	height: 100px;
}

<?=$modules_css?>
</style>
<script>
//<![CDATA[
(function($){
	var initialOrder, initialState = (<?=json_encode($state)?>),
	iframe, iframeWin,
	plugin = new PageContentsPlugin(
			(<?=json_encode($this->tid)?>),
			(<?=json_encode($this->rid)?>),
			(<?=json_encode($this->site_identifier)?>),
			(<?=json_encode($content_containers)?>),
			(<?=json_encode($modules)?>),
			(<?=json_encode($state)?>));

	function getOrder() {
		var order = {};
		iframe.find(".content_container").each(function(i, cc) {
			var modules = [];
			$(cc).find(".module").each(function(i, module) {
				modules.push($(module).attr("id").replace(/^page_content_/, ''));
			});
			order[$(cc).attr('id')] = modules;
		});
		return order;
	};
	
	$(function(){
		window.onBeforeUnload = function(){
			if (JSON.stringify(initialOrder) !== JSON.stringify(getOrder()) ||
				JSON.stringify(plugin.state) !== JSON.stringify(initialState)) {
				return (<?=json_encode(word(11050 /* Pagina is aangepast. De wijzigingen zullen verloren gaan als u doorgaat. */))?>);
			}
		};

		function createModule(module) {
			return $('#module_tmpl').html().
				replace(/\{module\.code\}/g, htmlspecialchars(module.code)).
				replace(/\{module\.name\}/g, htmlspecialchars(module.name));
		}
		
		var $iframe = $("#content_iframe");
		$iframe.bind("load", function() {
			iframeWin = window.content_iframe;
			iframe = $(this).contents();

			$('#state_form').bind("submit", function() {
				$('#state_form input[name=state]').val(JSON.stringify(plugin.state));
				$('#state_form input[name=order]').val(JSON.stringify(getOrder()));
				delete window.onBeforeUnload;
			});
			
			function addModule(cc, mod_code) {
				var node, mod = plugin.modules[mod_code], id;
				if(mod) {
					id = plugin.addModule(cc, mod_code);
					node = iframeWin.$(createModule(mod));
					node.attr("id", "page_content_" + id);
					node.hide();
					iframeWin.$("#"+cc, iframe).append(node).sortable("refresh");
					node.show("fast");
				}
			}
			$.each(plugin.state, function(cc, modules) {
				var container = iframeWin.$("#"+cc);
				$.each(modules, function(prio, module) {
					var mod = plugin.modules[module.module_code], node;
					
					if(mod) {
						node = iframeWin.$(createModule(mod));
						node.data("page_content", module);
						node.attr("id", "page_content_" + module.id);
						container.append(node);
					}
				});
			});

			$.each(plugin.content_containers, function(i, cc) {
				var node = iframeWin.$("#"+i);
				node.addClass("content_container");
				
				iframeWin.$('<img class="add_button" alt="+" src="<?=$PHprefs['url']?>/core/icons/add.png" />').
					bind("click", function(e) {
						var modules = [], popup;
	
						$.each(cc.allowed_modules, function(n, code) {
							if(plugin.modules[code]) {
								modules.push(createModule(plugin.modules[code]));
							}
						});
						
						popup = $.fn.inlineWindow({
							width: 800,
							height: 400,
							windowTitle: (<?=json_encode(word(11051))?>),
							modal: true,
							content: modules.join("")
						});
	
						popup.node.delegate(".module", "click", function(e) {
							addModule(i, $(this).attr("title"));
							popup.close();
						}).delegate(".module", "mouseenter mouseleave", function(e){
	 						$(this).toggleClass("hover", e.type === "mouseover");
						});
				}).prependTo(node);
	
				node.delegate(".edit_button", "click", function(e) {
					var module = iframeWin.$(this).parents(".module"), popup, moddata, modtype;
	
					popup = $.fn.inlineWindow({
						width: 400,
						height: 400,
						windowTitle: (<?=json_encode(word(11052))?>),
						modal: true,
						content: $("#edit_window").html()
					});
	
					popup.node.find(".delete_button").bind("click", function() {
						if(confirm(<?=json_encode(word(11053))?>)) {
							var id = module.attr("id").replace(/^page_content_/i, "");
							if(id) {
								plugin.removeModule(i, id); 
								module.hide("fast", function(){
									module.remove();
								});
							}
							popup.close();
						}
					}); 
					
					moddata = module.data("page_content");
					if(moddata) {
						popup.node.find(".properties_button").show().bind("click", function(){
							var url = plugin.modulePropertiesUrl(moddata.id);
							makePopup(null, 800, 600, <?=json_encode(word(11054))?>, url);
						});
	
						modtype = plugin.modules[moddata.module_code];
						if(modtype) {
							if(modtype.ptid_config) {
								popup.node.find(".config_button").show().bind("click", function(){
									var url = plugin.moduleConfigUrl(moddata.id);
									if(url) {
										makePopup(null, 800, 600, <?=json_encode(word(11055))?>, url);
									}
								});
							}
							
							if(modtype.ptid_content) {
								popup.node.find(".content_button").show().bind("click", function(){
									var url = plugin.moduleContentUrl(moddata.id);
									makePopup(null, 800, 600, <?=json_encode(word(11056))?>, url);
								});
							}
						}
					}
					return false;
				});
			});
			iframeWin.intializeSortables();
			initialOrder = getOrder();
		});
		$iframe.attr('src', 'index.php/page?<?=QS(false)?>');
	});
})(jQuery);
//]]>
</script>
<?$this->stack('head')?>
<?$this->stack('header_buttons')?>

<?$this->stack('header_buttons')?>
<!-- Content -->
<iframe name="content_iframe" id="content_iframe" frameborder="0" height="100%" width="100%" src="about:blank" style="width: 100%; height: 100%;"></iframe>

<div id="module_tmpl" style="display: none;">
	<div class="module" title="{module.code}">
		<a href="#{module.code}" class="edit_button module_icon mod_icon_{module.code}">
		</a>
		<div class="name">{module.name}</div>
	</div>
</div>

<div id="edit_window" style="display: none;">
	<div><a href="#" class="delete_button"><?=htmlspecialchars(word(11057))?></a></div>
	<div><a href="#" class="properties_button" style="display: none;"><?=htmlspecialchars(word(11058))?></a></div>
	<div><a href="#" class="config_button" style="display: none;"><?=htmlspecialchars(word(11059))?></a></div>
	<div><a href="#" class="content_button" style="display: none;"><?=htmlspecialchars(word(11060))?></a></div>
</div>
<?$this->stack('footer_buttons_left')?>
<form id="state_form" action="?<?=QS(true)?>" method="post">
	<input type="hidden" name="state" />	<input type="hidden" name="order" />	<input class="okBut" type="submit" value="<?=word(82 /* Ok */)?>" /></form>
<?$this->stack('footer_buttons_left')?>
<?$this->stack('footer_buttons_right')?>
<?if(!empty($_GET['returnPage'])):?>
<input type="button" value="<?=word(45)/* Back */?>" onclick="isDirty=false; window.location = '<?=$_GET['returnPage']?>'; return false;" class="okBut" />
<?endif?>
<?$this->stack('footer_buttons_right')?>