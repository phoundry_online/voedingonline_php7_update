function PageContentsPlugin (tid, page_id, site_identifier, content_containers,
		modules, state) {
	this.tid = tid;
	this.page_id = page_id;
	this.site_identifier = site_identifier;
	this.content_containers = content_containers;
	this.modules = modules;
	this.state = state;
	
	this.ids = 0;
}

PageContentsPlugin.prototype = {
	getModulesForContentContainer : function (container) {
		var cc, i, res = [];
		if(!this.content_containers[container]) {
			throw new Error("Content container " + container + "not found!");
		}
		cc = this.content_containers[container];
		for (i = 0; i < cc.allowed_modules.length; i += 1) {
			res.push(this.modules[i]);
		}
		return res;
	},
	
	getState : function() {
		return this.state;
	},
	
	addModule : function(cc, module_code) {
		this.ids += 1;
		var id = "new_" + this.ids;
		if (this.state[cc] instanceof Array) {
			this.state[cc] = {};
		}
		this.state[cc][id] = module_code;
		return id;
	},
	
	removeModule : function(cc, module_id) {
		if(this.state[cc][module_id]) {
			delete this.state[cc][module_id];
		}
	},
		
	moduleConfigUrl : function (page_content_id) {
		return this._createModuleUrl("config", page_content_id);
	},
	
	moduleContentUrl : function (page_content_id) {
		return this._createModuleUrl("content", page_content_id);
	},
	
	modulePropertiesUrl : function (page_content_id) {
		return this._createModuleUrl("properties", page_content_id);
	},
	
	_createModuleUrl : function(action, page_content_id) {
		var path = ["index.php", action, page_content_id].join("/"),
		query = [
		         "site_identifier=" + this.site_identifier,
		         "RID=" + this.page_id,
		         "TID=" + this.tid
		         ].join("&");
		
		return path + "?" + query;
	}
};