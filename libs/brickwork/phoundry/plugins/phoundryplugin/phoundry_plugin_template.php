<?php PH::getHeader("text/html")?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?=$PHprefs['charset']?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title><?=$this->title?></title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript">
	jQuery(function($){
		setTimeout('initNotes()',1500);
	});
</script>
<?=isset($head)?$head:''?>

</head>
<body class="frames" onbeforeunload="if(typeof(window.onBeforeUnload) !== 'undefined'){ return onBeforeUnload(); }" onunload="if(typeof(window.onUnload) !== 'undefined'){ return window.onUnload(); }">
<?if (isset($form_action)):?>
<form action="<?=$form_action?>" method="post" enctype="multipart/form-data">
<?endif?>
<div id="headerFrame" class="headerFrame">
<?=getHeader($this->title, $this->subtitle)?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
	<?=isset($header_buttons)?$header_buttons:''?>
	<img class="div" alt="|" src="<?= $PHprefs['url'] ?>/core/pics/div.gif"/>

	<img src="<?=$PHprefs['url']?>/core/icons/shortcut_<?=$this->shortcut ? 'delete' : 'add' ?>.png" class="ptr" width="16" height="16" alt="<?=$this->word(19).' '.($this->shortcut? '-' : '+')?>" onclick="shortCut(this, <?=$this->tid?>)" />
	<?if ($PHprefs['nrNotes'] > 0 && $this->notes['count'] < $PHprefs['nrNotes']):?>
	<img src="<?=$PHprefs['url']?>/core/icons/3mnote.png" class="ptr popupWin" width="16" height="16" alt="<?=$this->word(196)?>" title="<?=$this->word(196)?>|300|320|0|<?= $PHprefs['url'] ?>/core/popups/editNote.php?<?= QS(1) ?>" />
	<?endif?>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<?=$plugin_html?>
</div>

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<?=isset($footer_buttons_left)?$footer_buttons_left:''?>
	</td>
	<td align="right">
	<?=isset($footer_buttons_right)?$footer_buttons_right:''?>
	</td>
</tr></table>
</div>
<?if (isset($form_action)):?>
</form>
<?endif?>
<?php if ($this->notes['count'] > 0) { ?>
<div id="notesGrippie"></div>
<div id="notesPane">
<?= $this->notes['html'] ?>
</div>
<?php } ?>
</body>
</html>