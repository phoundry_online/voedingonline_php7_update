<?php
/**
 * PhoundryPlugin
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
abstract class PhoundryPlugin
{
	/**
	 * Path
	 * @var array
	 */
	protected $_path;

	/**
	 * Variables that are available in the template
	 * @var array
	 */
	protected $_data;

	/**
	 * Stacked variables that are available in the base template
	 * @var array
	 */
	protected $_stack;

	/**
	 * Are we stacking a variable
	 * @var array
	 */
	protected $_stacking;

	/**
	 * Title of the Plugin
	 * @var string
	 */
	protected $_title;

	/**
	 * Subtitle of the Plugin
	 * @var string
	 */
	protected $_subtitle;

	/**
	 * Phoundry Table id
	 * @var int
	 */
	protected $_tid;

	/**
	 * Record id if any
	 * @var int|void
	 */
	protected $_rid;

	/**
	 * Notes for this plugin
	 * @var array
	 */
	protected $_notes;

	/**
	 * Plugin shortcutted
	 * @var bool
	 */
	protected $_shortcut;

	/**
	 * Initialize variables and check access
	 */
	public function  __construct()
	{
		$this->_path = isset($_SERVER['PATH_INFO']) ? explode('/', trim($_SERVER['PATH_INFO'], '/')) : array();

		$this->_tid = (int) PH::getTID();
		$this->_rid = !empty($_GET['RID']) ? $_GET['RID'] : NULL;
		$this->_notes = PH::getNotes($this->_tid, $GLOBALS['PHSes']->userId);
		$this->_shortcut = PhoundryTools::isShortcut($this->_tid);

		$this->_data = array();
		$this->_title = 'Phoundry plugin';
		$this->_subtitle = '';
		$this->_stacking = array();
		$this->_stack = array();
	}

	/**
	 * Readonly access to plugin attributes
	 * @param string $name
	 * @return mixedvar
	 */
	public function  __get($name)
	{
		switch (strtolower($name))
		{
			case 'tid':
				return $this->_tid;
			case 'rid':
				return $this->_rid;

			case 'title':
				return $this->_title;
			case 'subtitle':
				return $this->_subtitle;

			case 'notes':
				return (array) $this->_notes;
			case 'shortcut':
				return $this->_shortcut;

			default:
				throw new InvalidArgumentException();
		}
	}

	/**
	 * Render the plugin specific template
	 * @param string $template
	 * @return string
	 */
	protected function renderPlugin($template)
	{
		global $PHprefs, $WEBprefs;

		extract($this->_data);
		ob_start();
		include $template;
		if(!empty($this->_stacking))
		{
			foreach($this->_stacking as $k => $v)
			{
				throw new Exception('$this->stack(\''.$k.'\') started but never ended');
			}
		}
		return ob_get_clean();
	}

	/**
	 * Render the whole page
	 * @param string $template
	 * @param bool	 $decorate
	 */
	public function render($template, $decorate = true, $form_action = null)
	{
		global $PHprefs;

		// this variable is used in the overall template
		$plugin_html = $this->renderPlugin($template);
		if(!$decorate)
		{
			echo $plugin_html;
			return;
		}
		extract($this->_stack, EXTR_SKIP);
		include 'phoundry_plugin_template.php';
	}

	/**
	 * Load data function like with the Webmodules
	 */
	public function loadData()
	{
	    $path = $this->_path;

		$action = array_shift($path);
		if(is_null($action)) $action = 'index';

		if(! method_exists($this, $action.'Action'))
		{
			throw new BadFunctionCallException('Action "'.$action.'Action" does not exist');
		}
		call_user_func_array(array($this, $action.'Action'), $path);
	}

	/**
	 * Show a Phoundry Error message
	 * @param string $message
	 * @param bool	 $die
	 */
	public function error($message, $die = false)
	{
		PH::phoundryErrorPrinter($message, $die);
	}

	/**
	 * To add javascript or css to the <head> wrap it between two $this->stack() calls
	 */
	public function stack($type)
	{
		if(isset($this->_stacking[$type]))
		{
			if(!isset($this->_stack[$type]))
			{
				$this->_stack[$type] = '';
			}
			$this->_stack[$type] .= ob_get_clean();
			unset($this->_stacking[$type]);
		}
		else
		{
			ob_start();
			$this->_stacking[$type] = true;
		}
	}

	/**
	 * Get a word from the language-file.
	 * Has a variable number of arguments, one is required: the id of the word to
	 * get. The other arguments represent values that should be replaced in the
	 * word.
	 * Example: word[24] = 'This is example #1 and #2'.
	 * A call to word(24,'one','two') would result in 'This is example one and two'.
	 * This function uses two word-arrays: customWORDS and coreWORDS, customWORDS
	 * has a higher precedence (core-words can be overwritten by custom-words).
	 *
	 * @author Arjan Haverkamp
	 * @return The desired word(s).
	 * @returns string
	 */
	public function word()
	{
		$args = func_get_args();
		return call_user_func_array('word', $args);
	}
}
