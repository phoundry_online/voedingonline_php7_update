<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en">
	<head>
		<title><?= word(10000); ?></title>
		<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
		<meta name="Author" content="Web Power BV, www.webpower.nl" />
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
		<!--[if IE]>
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
		<![endif]-->
		<link rel="stylesheet" href="css/style.css" type="text/css" />
		<style type="text/css">
		html, body
		{
			margin: 0px;
			padding: 0px;
		}
		.treecheckbox
		{
			float: left; 
			width: 40px;
		}
		.line
		{
			border-bottom: 1px solid #fff;
		}
		.deny
		{
			padding: 2px;
			background-color: #faa;
		}
		.allow
		{
			padding: 2px;
			background-color: #cfc;
		}
		.blockwide
		{
			float: left;
			width: 300px;
		}
		.block
		{
			float: left;
			width: 15%;
		}
		.instanceinfo
		{
			width: 200px;
			float: right;
		}
		.line a
		{
			color: #005;
		}
		#header
		{
			background-color: #444;
			padding: 4px;
			color: #fff;
			font-weight: bold;
		}
		#header .instanceinfo
		{
			width: 220px;
		}
		</style>
		<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
		<script type="text/javascript">
		</script>
	</head>
	<body class="frames">
		<div class="headerFrame">
		<span><?= getHeader('ACL'); ?></span>
		<table class="buttons" cellspacing="0" cellpadding="2">
			<tr>
				<td>
					<form method="post" id="changeuser" action="<?= $_SERVER['REQUEST_URI'] ?>">
						<input type="hidden" name='action' value='searchuser' />
					user:
					<input name="usrname" id="user_id" value="<?= Utility::arrayValue($_POST,'usrname') ?>">
					<input type="submit" value="<?= word(10006); ?>" />
					<strong><?= ( ( !is_null($usr) && $usr != 0) ? word(10001) . ' '. Utility::arrayValue($_POST,'usrname') : '') ?></strong>
					</form>
				</td>
			</tr>
		</table>
				<div id="header" style="position: relative;">
					<div class="blockwide"><?= word(10002); ?></div>
					<div class="block"><?= word(10003); ?></div>
					<div class="block"><?= word(10004); ?></div>
					<div class="instanceinfo"><?= word(10005); ?></div>
					<br style="clear: both" />
				</div>
		</div>
	
		<div class="contentFrame" style="padding: 0px; margin-top: 23px;">
			<?
			$tree = getAllowedStructure( $sid, $usr );
			
			function buildTreeLine( $tree, $indent = 0, $linect = 0)
			{
				$padstring = str_repeat('&nbsp;',$indent * 5);
				
				foreach( $tree as $line )
				{
					$linect++;
					
					$grant = (count($line['grants']) > 0);
					
					?>
					<div class="<?= ($grant ? 'allow' : 'deny') ?> line">
						<input type="hidden" id="grant<?= $line['id'] ?>_wipe" name="grant[<?= $line['id'] ?>]" value="<?= ($line['grant'] ? 'wipe' : '') ?>" />
						<div class="blockwide">
							<a href="#" onclick="$('#childrenof_<?= $line['id'] ?>').toggle();$('this').class='open'">+</a>
							<?= $padstring . $line['description'] ?>
						</div>
						<div class="block">
						<? foreach( $line['grants'] as $role )  { ?>
						<a href="index.php?site_identifier=<?= Utility::arrayValue($_GET,'site_identifier'); ?>&amp;action=changerole&amp;role_id=<?= $role->getId() ?>"><?= (string)$role ?></a>
						<? } ?>
							&nbsp;
						</div>
						<div class="block">
						<? foreach( $line['denies'] as $role )  { ?>
						<a href="index.php?site_identifier=<?= Utility::arrayValue($_GET,'site_identifier'); ?>&amp;action=changerole&amp;role_id=<?= $role->getId() ?>"><?= (string)$role ?></a>
						<? } ?>
						&nbsp;
						</div>
						<div class="instanceinfo">
						<?=  $line['instance'] ?>
						</div>
						<br style="clear: both" />
					</div> 
					<?
					
					if ( count($line['children']) > 0 )
					{
						?>
						<div id="childrenof_<?= $line['id'] ?>">
						<?
						$linect = buildTreeLine($line['children'], $indent+1, $linect);
						?>
						</div>
						<?
					}
				}
				
				return $linect;
			}
			?>
			<form method="post" action="<?= $_SERVER['REQUEST_URI'] ?>">
				<input type="hidden" name="action" value="commitchanges" />
				<input type="hidden" name="usrid" value="<?= $usr ?>" />
			
			<div id="table">
				<div>
				<? if ( !is_null($usr) && $usr != 0) { buildTreeLine($tree); } ?>
				</div>
			</div>
		</div>
		<div class="footerFrame">
		<table class="buttons" cellspacing="0" cellpadding="2"><tr>
			<td>
			</td>
		</tr></table>
		</div>
	</body>
</html>