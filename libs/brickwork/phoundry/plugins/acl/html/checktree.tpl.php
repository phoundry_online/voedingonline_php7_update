<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en">
	<head>
		<title><?= word(10000); ?></title>
		<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
		<meta name="Author" content="Web Power BV, www.webpower.nl" />
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
		<!--[if IE]>
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
		<![endif]-->
		<link rel="stylesheet" href="css/style.css" type="text/css" />
		<style type="text/css">
		html, body
		{
			margin: 0px;
			padding: 0px;
		}
		.treecheckbox
		{
			float: left; 
			width: 40px;
		}
		.oddline
		{
			padding: 2px;
			background-color: #fff;
		}
		.evenline
		{
			padding: 2px;
			background-color: #ccc;
		}
		.instanceinfo
		{
			width: 200px;
			float: right;
		}
		#header
		{
			background-color: #444;
			padding: 4px;
			color: #fff;
			font-weight: bold;
		}
		#header .instanceinfo
		{
			width: 220px;
		}
		</style>
		<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
		<script type="text/javascript">
		</script>
	</head>
	<body class="frames">
		<div class="headerFrame">
		<span><?= getHeader('ACL'); ?></span>
		<table class="buttons" cellspacing="0" cellpadding="2">
			<tr>
				<td>
					<form method="post" id="changerole" action="<?= $_SERVER['REQUEST_URI'] ?>">
					<input type="hidden" name="action" value="changerole" />
					Rol:
					<select name="role_id" id="role_id" onchange="document.getElementById('changerole').submit();">
						<option value="0">-- <?= word(10009); ?> --</option>
						<? foreach( getRoles() as $role ) { ?>
						<option value="<?= $role->id ?>"<?= ($roleid == $role->id ? ' selected="selected"' : '') ?>><?= $role->name ?></option>
						<? } ?>
					</select>
					</form>
				</td>
			</tr>
		</table>

				<div id="header" style="position: relative;">
					<div class="treecheckbox"><?= word(10007); ?></div>
					<div class="treecheckbox"><?= word(10008); ?></div>
					<div style="float: left"><?= word(10002); ?></div>
					<div class="instanceinfo"><?= word(10005); ?></div>
					<br style="clear: both" />
				</div>
		</div>
	
		<div class="contentFrame" style="padding: 0px;margin-top: 23px;">
			<?
			$tree = getTree($roleid);
			
			function buildTreeLine( $tree, $indent = 0, $linect = 0)
			{
				$padstring = str_repeat('&nbsp;',$indent * 5);
				
				foreach( $tree as $line )
				{
					$linect++;
					?>
					<div class="<?= ($linect % 2 ? 'evenline' : 'oddline') ?>">
						<input type="hidden" id="grant<?= $line['id'] ?>_wipe" name="grant[<?= $line['id'] ?>]" value="<?= ($line['grant'] ? 'wipe' : '') ?>" />
						<div class="treecheckbox"><input class="allowcheckbox" type="checkbox" id="grant<?= $line['id'] ?>_allow" name="grant[<?= $line['id'] ?>]" value="allow" <?= ($line['grant'] == 'allow' ? ' checked="checked"' : '') ?> onclick="$('#grant<?= $line['id'] ?>_deny').attr('checked','');"/></div> 
						<div class="treecheckbox"><input class="denycheckbox" type="checkbox" id="grant<?= $line['id'] ?>_deny" name="grant[<?= $line['id'] ?>]" value="deny"  <?= ($line['grant'] == 'deny' ? ' checked="checked"' : '') ?> onclick="$('#grant<?= $line['id'] ?>_allow').attr('checked','');"/></div>
						<div style="float: left;">
							<a href="#" onclick="$('#childrenof_<?= $line['id'] ?>').toggle();$('this').attr('class','open');">+</a>
							<?= $padstring . $line['description'] ?>
						</div>
						<div class="instanceinfo">
						<?=  $line['instance'] ?>
						</div>
						<br style="clear: both" />
					</div> 
					<?
					
					if ( count($line['children']) > 0 )
					{
						?>
						<div id="childrenof_<?= $line['id'] ?>">
						<?
						$linect = buildTreeLine($line['children'], $indent+1, $linect);
						?>
						</div>
						<?
					}
				}
				
				return $linect;
			}
			?>
			<form method="post" action="<?= $_SERVER['REQUEST_URI'] ?>">
				<input type="hidden" name="action" value="commitchanges" />
				<input type="hidden" name="role_id" value="<?= $roleid ?>" />
			
			<div id="table">
				<div>
				<? buildTreeLine($tree) ?>
				</div>
			</div>
		</div>
		<div class="footerFrame">
		<table class="buttons" cellspacing="0" cellpadding="2"><tr>
			<td>
			<? if (isset($roleid) && $roleid > 0 ) { ?>
			<input id="okBut" type="submit" style="width:100px;" value="<?= word(82) ?>" />
			<? } ?>
			</td>
			<td align="right">
			<? if (isset($roleid) && $roleid > 0 ) { ?>
			<input type="button" value="<?= word(46) ?>" onclick="_D.location='../home.php'" />
			<? } ?>
			</td>
		</tr></table>
		</div>
	</body>
</html>