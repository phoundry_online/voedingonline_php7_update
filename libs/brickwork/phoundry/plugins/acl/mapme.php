<?php
$inc = @include('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once INCLUDE_DIR.'functions/autoloader.include.php';
require_once INCLUDE_DIR.'functions/common.php';
require_once INCLUDE_DIR."classes/ACL.class.php";
require_once INCLUDE_DIR.'classes/ACL/Cache.class.php';

$TID 	= Utility::arrayValue($_GET,'TID');
$action	= Utility::arrayValue($_POST,'action');
$RID	= Utility::arrayValue($_GET,'RID');
$sid	= Utility::arrayValue($_GET,'site_identifier');
$usr    = getuser();
$cache  = new ACLCache( $sid );
$acl    = ACL::singleton();

include('html/mapme.tpl.php');

function getUser()
{
	if ( Utility::isPosted() && Utility::arrayValue($_POST,'action') == 'searchuser')
	{
		$res = DB::iQuery( sprintf("SELECT id FROM brickwork_site_user WHERE username = '%s'", Utility::arrayValue($_POST,'usrname')) );
		
		if ( !isError($res) && $res->numRows >= 1)
		{
			$row = $res->first();
			
			return ($row->id);
		}
		return null;
	}
	else
		return Utility::arrayValue($_REQUEST,'RID'); 
}

function getAllowedStructure( $sid, $usrid )
{
	global $cache;
	
	$roles 	= getUserRoles( $usrid );
	$root 	= getSiteRoot($sid);
	
	$aTree 	= getFullTree( $root );

	foreach( $roles as $role )
	{
		//echo "Checking role: " . $role->getName() . "<br />\n";
		traverseTree($aTree, $role);
	}
	return $aTree;
}

function checkACLAccess( $objectid, $roleid )
{
	if ( $objectid == 0 || is_null($objectid))
		return true;
		
	$res = DB::iQuery( sprintf('SELECT parent_id FROM brickwork_auth_object WHERE id = %s',$objectid));
	$row = $res->first();
	
	//echo sprintf('SELECT parent_id FROM brickwork_auth_object WHERE id = %s',$objectid) . "<br />\n";
	
	if ( $row === false )
	{
		debug_print_backtrace();
		exit();
		return true;
	}
	else
		$status = checkACLAccess( $row->parent_id, $roleid );
	
	$res = DB::iQuery( sprintf('SELECT `grant` FROM brickwork_auth_right WHERE auth_object_id = %s AND auth_role_id = %s',$objectid, $roleid ) );
	$row = $res->first();
	
	//echo sprintf('SELECT `grant` FROM brickwork_auth_right WHERE auth_object_id = %s AND auth_role_id = %s',$objectid, $roleid ) . "<br />\n";
	if ( $row === false )
		return $status;
	else
		return $status && ($row->grant == 'ALLOW');
}

function traverseTree( &$parent, $role )
{
	global $cache, $acl;
	
	foreach( $parent as &$child )
	{
		$key = $child['id'];
		
		//echo "Checking " . $key  . " against role " . $role->getId() . "<br />\n";
		
		if ( $cache->cached( $role->getId(), $key ) )
		{
			$grant = $cache->get( $role->getId(), $key ); 
		}
		else
		{
			$grant = checkACLAccess( $key, $role->getId() );
			$cache->store( $role->getId(), $key, $grant );
		}
		
		if ( $grant )
		{
			array_push($child['grants'], $role);
		}
		else
			array_push($child['denies'], $role);

		$child['grant'] = $grant;
		 
		if ( count($child['children']) )
		{
			traverseTree( $child['children'], $role );
		}
	}
}

function getFullTree( $parent = null )
{
	$aTmp = Array();
	
	$sql = sprintf('SELECT ao.id, instanceid, description, ao.parent_id ' .
					 '  FROM brickwork_auth_object ao '. 
					 ' WHERE ao.parent_id  %s',
					(is_null($parent) ? ' IS NULL' : ' = ' . (int)$parent)) ;
					
	$res = DB::iQuery( $sql ); 
	
	if ( isError($res))
	{
		trigger_error( sprintf('SQL error: %s', $res->errstr), E_USER_ERROR);
	}
	
	while( $res->current() )
	{
	    $element = $res->current();
		$aTmp[ $element->id ] = Array( 'id'			=> $element->id,
									   'instance' 	=> $element->instanceid,
									   'grant'	  	=> true,
									   'description'=> $element->description,
									   'children' 	=> getFullTree($element->id),
									   'grants'		=> Array(),
									   'denies'		=> Array()
									 );
		$res->next();
	}
	
	return $aTmp;
}

function getSiteRoot( $sid )
{
	$obj = new ACLObject( 'Site/' . $sid );
	return $obj->getAuthObjectId();
}

function getUserRoles( $usrid )
{
	$auth_roles = Array();

	$sql = sprintf('SELECT r.name ' . 
					 '  FROM brickwork_auth_role r ' . 
					 ' INNER JOIN brickwork_site_user_auth_role ur' . 
					 '    ON (ur.auth_role_id = r.id) ' . 
					 " WHERE ur.site_user_id = '%s'", DB::quote($usrid));

	$res = DB::iQuery($sql);

	if (isError($res) )
		return $auth_roles;
		
	
	while ( $res->current() )
	{
	    $row = $res->current();
		$role = new ACLRole( $row->name );
		array_push($auth_roles, $role);
		$auth_roles = array_merge($auth_roles, $role->getChildRoles());
		$res->next();
	}
	return $auth_roles;
}

/**
 * Test if an object is of type error
 *
 * @return Boolean
 */
function isError(&$e) {
    $v = PHP_VERSION;

    switch (TRUE) {
        // old php
        case ($v < 4.2):
            if ((get_class($e) == 'error')
                ||	(is_subclass_of($e, 'error'))) {
                return TRUE;
            }
            break;

        // php 4.2 < 5
        case ($v >= 4.2 && $v < 5):
            return (is_a($e, 'Error'));
            break;

        // php 5
        case ($v >= 5):
            return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
            break;
    }

    // not an error
    return FALSE;
}

?>