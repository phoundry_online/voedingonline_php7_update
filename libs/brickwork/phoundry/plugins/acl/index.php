<?php
$inc = @include('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once INCLUDE_DIR.'functions/autoloader.include.php';
require_once INCLUDE_DIR.'functions/common.php';
require_once INCLUDE_DIR.'classes/ACL/Cache.class.php';

$TID 	= Utility::arrayValue($_GET,'TID');
$action	= Utility::arrayValue($_REQUEST,'action');
$RID	= Utility::arrayValue($_GET,'RID');
$sid	= Utility::arrayValue($_GET,'site_identifier');

$roleid	= 0;

switch ($action) {
	case 'changerole':
			$roleid = (int)Utility::arrayValue($_REQUEST,'role_id');
			break;
	case 'commitchanges':
			$roleid = (int)Utility::arrayValue($_REQUEST,'role_id');
			$grants = Utility::arrayValue($_POST,'grant');
			storeRights($roleid,$grants);
			$oCache = new ACLCache( $sid );
			$oCache->flush( );
			break;
}

function storeRights( $roleid, $grants )
{
	if ( $roleid == 0)
		return true;
		
	$sqlupdate = "INSERT INTO brickwork_auth_right ( auth_object_id, auth_role_id, `grant` ) " .
		   		 " VALUES('%s','%s','%s') ON DUPLICATE KEY UPDATE `grant` = '%s'";
	$sqlwipe   = "DELETE FROM brickwork_auth_right WHERE auth_role_id = '%s' AND auth_object_id = '%s'";
	
	foreach( $grants as $id => $setting )
	{
		if ( $setting == 'wipe')
			$sql = sprintf($sqlwipe, $roleid, $id );
		else if ( $setting != '')
			$sql = sprintf($sqlupdate, $id, $roleid, $setting, $setting);
		else
			$sql = '';
			
		if ( $sql )
		{
			$res = DB::iQuery( $sql );
			if (isError($res))
			{
				trigger_error( $res->errstr );
			}
		}
	}
	return true;
}

function getRoles()
{
    $res = DB::iQuery('SELECT name, id FROM brickwork_auth_role ORDER BY name');

    if ( isError($res))
        return Array();

    $tmp = array();
    while ($res->current()) {
        $tmp[] = $res->current();
        $res->next();
    }

    return $tmp;
}

function getTree( $role, $parent = null )
{
	if ($role == 0)
		return Array();
	$aTmp = Array();
	
	$sql = sprintf('SELECT ao.id, instanceid, description, `grant`, ao.parent_id ' .
					 '  FROM brickwork_auth_object ao '. 
					 '  LEFT OUTER JOIN brickwork_auth_right ar ' .
					 '    ON (auth_object_id = ao.id AND auth_role_id = %s)' .
					 ' WHERE ao.parent_id  %s',
					(int)$role,
					(is_null($parent) ? ' IS NULL' : ' = ' . (int)$parent)) ;
					
	$res = DB::iQuery( $sql ); 
	
	if ( isError($res))
	{
		trigger_error( sprintf('SQL error: %s', $res->errstr), E_USER_ERROR);
	}
	
	while( $res->current())
	{
		$element = $res->current();
		$aTmp[ $element->id ] = Array( 'id'			=> $element->id,
									   'instance' 	=> $element->instanceid,
									   'grant'	  	=> $element->grant,
									   'description'=> $element->description,
									   'children' 	=> getTree($role, $element->id)
									 );
		$res->next();
	}
	
	return $aTmp;
}

/**
     * Test if an object is of type error
     *
     * @return Boolean
     */
 function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }


include('html/checktree.tpl.php');
?>
