<?php
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

// Make sure the current user in the current role has access to this page:
checkAccess();

// Page gets called by AJAX wich is always utf8
DB::setCollation('utf8');

$page_id = (int) Utility::arrayValue($_GET, 'page_id');
if(empty($page_id)) die('No Page selected');
$page = ActiveRecord::factory('Model_Page', $page_id);
if(!$page->isLoaded()) die('No Valid Page selected');

$HASACL = Features::brickwork('ACL');

$content_containers = $page->page_type->content_containers;

$page_contents = array();
foreach($page->page_contents as $content)
{
	$page_contents[$content['id']] = array(
		'id' => $content['id'],
		'content_container_code' => $content['content_container_code'],
		'module_code' => $content['module_code'],
		'prio' => $content['prio'],
	);
}

$content_container_ids = array();
$all_modules = array();
foreach($page->page_type->content_containers as $cc)
{
	$content_container_ids[] = $cc['code'];
	foreach($cc->allowed_modules as $module)
	{
		$all_modules[] = $module;
	}
}

$all_modules = array_unique($all_modules);
$all_modules_js = array();

foreach($all_modules as $module)
{
	$allowed_in = array();
	foreach($module->allowed_content_containers as $cc)
	{
		// Filter out the contentcontainers that are not on the current page
		if($cc['page_type_code'] == $page->page_type['code']) $allowed_in[] = $cc['code'];
	}
	
	$all_modules_js[$module['code']] = array(
		'code' => $module['code'],
		'ptid_content' => $module['ptid_content'],
		'ptid_config' => $module['ptid_config'],
		'name' => $module['name'],
		'description' => $module['description'],
		'image_src' => $module['image_src'],
		'allowed_in' => $allowed_in
	);
	try {
	$all_modules_js[$module['code']]['content_copyable'] = class_exists($module['class']) && in_array('PageContentCopyable', class_implements($module['class']));
	}catch (Exception $e){}
}

echo json_encode(array(
	'page'			=> $page->getAsArray(), 
	'page_type'		=> $page->page_type->getAsArray(),
	'page_contents'	=> $page_contents,
	'modules'		=> $all_modules_js,
));