<?php
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title><?=word(11600 /* Structure */)?></title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="/index.php/script/css/tree_component.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<script type="text/javascript" src="/index.php/script/js/jquery|jquery.json.js/jquery|jquery.listen.js/jquery|jquery.metadata.js/jquery|jquery.hotkeys.js/jquery|jquery.query.js/css.js/tree_component.js?nopack"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
<?if(empty($gathered['step']) && array_key_exists('site', $gathered)):?>
var tree = {};
jQuery(function($){
	tree = new tree_component();
	tree.init($('#structure_tree'), {
		path	: '/brickwork/pics/tree_component/',
		ui		: {
			dots		: false,
			animation	: 0,
			theme_path	: '/brickwork/pics/tree_component/',
			context		: []
		},
		rules	: {
			type_attr	: 'rel',
			createat	: 'bottom',
			draggable	: [],
			clickable	: ['MainStructure', 'SiteStructure'],
			renameable	: [],
			deletable	: [],
			dragrules	: 'none'
		},
		callback :	{
			onchange	: function(node, tree_obj) {
				$('#parent_structure').val(
					$(node).is('[id^=ss_][rel=SiteStructure]') ?
						$(node).attr('id').replace(/^ss_/, '') :
						0
				);
			} 
		},
		data	: {
			type	: 'json',
			async	: false,
			json	: (<?=json_encode($tree_structure)?>)
		},
		'selected'	: ['<?=is_null($gathered['parent_structure']) ? 'Structuur' : ('ss_'.$gathered['parent_structure'])?>']
	});

	function refreshIcons(ul) {
		ul.find('li').each(function(e){
			var $this = $(this),
			icon = '<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/page';

			if($this.hasClass('show_menu'))		icon += '_m';
			if($this.hasClass('show_sitemap'))	icon += '_s';
			if($this.hasClass('secure'))		icon += '_x';
			icon += '.gif';
			$this.children('a').css('background-image', 'url('+icon+')');
		});
	};

	refreshIcons($('#Structuur > ul'));
});
<?endif?>
<?if($gathered['step'] == 2):?>
jQuery(function($){
	var	exclude = $('#page_contents_exclude').val().split('|'),
	content_exclude = $('#page_contents_content_exclude').val().split('|'),
	config_exclude = $('#page_contents_config_exclude').val().split('|');
	
	$('#page_select').change(function(e){
		if($(this).val() && $(this).val() != 0)
		{
			$.ajax({
				type		: 'GET',
				url			: 'page_contents.php?page_id='+$(this).val(),
				cache		: false,
				dataType	: 'json',
				success		: function(data, textStatus) {
					$('#page_contents').html(data.page_type.contentspecification);

					$.each(data.page_contents, function() {
						var enabled = $.inArray(this.id, exclude) == -1,
						content = $.inArray(this.id, content_exclude) == -1,
						config = $.inArray(this.id, config_exclude) == -1;
						
						var html = $('<li id="pagecontent_' + this.id + '" class="exists"></li>');
						html.append('<div class="koppie"><label><input class="exclude" type="checkbox"'+(enabled ? ' checked="checked"' : '')+' />'+data.modules[this.module_code].name+'</label></div>');

						if(data.modules[this.module_code].ptid_content)
							html.append('<div><label><input class="exclude_content" type="checkbox"'+(data.modules[this.module_code].content_copyable && content ? ' checked="checked"' : '')+(data.modules[this.module_code].content_copyable ? '' : ' disabled="disabled"')+' /><?=word(11750)?></label></div>');
						if(data.modules[this.module_code].ptid_config)
							html.append('<div><label><input class="exclude_config" type="checkbox"'+(config ? ' checked="checked"' : '')+' /><?=word(11751)?></label></div>');
						
						$('#'+this.content_container_code).append(html);
					});
				},
				error		: function(request, text, error) {
					
				}
			});
		}
	});

	$('#page_contents').click(function(e){
		var clicked = $(e.target),
		page_content_id = clicked.parents('li').attr('id').replace(/^pagecontent_/, '');
		
		if(clicked.is('input.exclude')) {
			if(clicked.attr('checked'))
			{
				var index = $.inArray(page_content_id, exclude);
				if(index != -1) exclude.splice(index, 1);
			} else {
				var index = $.inArray(page_content_id, exclude);
				if(index == -1) {
					exclude.push(page_content_id);
				}
			}
		} else if(clicked.is('input.exclude_content')) {
			
			if(clicked.attr('checked'))
			{
				var index = $.inArray(page_content_id, content_exclude);
				if(index != -1) content_exclude.splice(index, 1);
			} else {
				var index = $.inArray(page_content_id, content_exclude);
				if(index == -1) {
					content_exclude.push(page_content_id);
				}
			}
		} else if(clicked.is('input.exclude_config')) {
			if(clicked.attr('checked'))
			{
				var index = $.inArray(page_content_id, config_exclude);
				if(index != -1) config_exclude.splice(index, 1);
			} else {
				var index = $.inArray(page_content_id, config_exclude);
				if(index == -1) {
					config_exclude.push(page_content_id);
				}
			}
		}

		$('#page_contents_exclude').val(exclude.join('|'));
		$('#page_contents_content_exclude').val(content_exclude.join('|'));
		$('#page_contents_config_exclude').val(config_exclude.join('|'));
	});

	$('#page_select').change();
});
<?endif?>
//--><!]]>
</script>
<style type="text/css">
.tree li#Structuur li a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/page.gif);
}

.tree li#Structuur li.deleted a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/page_d.gif);
}

.tree li#Structuur li.new a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/page_n.gif);
}

.tree li#Structuur.open,
.tree li#Structuur.closed
{
	background: none;
}

.tree li#Structuur.closed > ul
{
	display: block !important;
}

.tree li#Structuur.closed ul
{
	_display: block !important;
}

.tree li#Structuur a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/base.gif);
}

h1 {
	color: #000;
	font-family: arial;
	font-size: 11pt;
}

fieldset legend {
	font-weight: bold;
}

button
{
	padding: 0;
}

ul,
li
{
	padding: 0;
	margin : 0;
}

ul.sortable li {
	position: relative;
}

ul.boxy {
	list-style-type: none;
	padding: 4px 4px 0 4px;
	margin: 0px;
	width: 10em;
	font-size: 13px;
	font-family: Arial, sans-serif;
	border: 1px solid #eee;
}
ul.boxy li {
	margin-bottom: 4px;
	padding: 2px 2px;
	border: 1px solid #ccc;
	background-color: #eee;
}

ul.boxy li.exists {
	background-color: #D0FFCC!important;
}

</style>
</head>
<body class="frames">
<form action="" method="post">
<input type="hidden" name="gathered" value="<?=htmlentities(json_encode($gathered))?>" />
<div id="headerFrame" class="headerFrame">
	<?= getHeader(word(11752), $structure['name']);?>
	<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	
	</td>
	</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?if(empty($gathered['step'])):?>
<div id="step_1">
	<fieldset>
		<legend><?=word(11753)?></legend>
		<input name="new[structure_name]" type="text" value="<?=Utility::arrayValue($gathered, 'structure_name')?>" />
	</fieldset>
	<?php if($nr_of_sites > 1):?>
	<fieldset>
		<legend><?=word(11754)?></legend>
		<select name="new[site]" onchange="this.form.submit();">
			<option value="0">-- <?=word(116)/* Choose */?> --</option>
			<?foreach(ActiveRecord::factory('LiveEdit_Site')->get() as $site):?>
			<option value="<?=htmlspecialchars($site['identifier'])?>"<?if(Utility::arrayValue($gathered, 'site') == $site['identifier']):?> selected="selected"<?endif?>><?=$site['name']?></option>
			<?endforeach?>
		</select>
	</fieldset>
	<?endif?>
	<?if(!empty($gathered['site'])):?>
	<fieldset>
		<legend><?=word(11755)?></legend>
		<input id="parent_structure" type="hidden" name="new[parent_structure]" value="<?=Utility::arrayValue($gathered, 'parent_structure')?>" />
		<div id="structure_tree" style="overflow: auto;"></div>
	</fieldset>
	<?endif?>
</div>	
<?elseif($gathered['step'] == 1):?>
<div id="step_2">
	<?if(count($structure->pages)):?>
	<fieldset>
		<legend><?=word(11631)?></legend>
		<table cellspacing="1" cellpadding="3" class="sort-table structure_pages" style="width: 100%; clear: both;">
			<tbody>
			<tr>
				<th width="16"></th>
				<th><?=word(11756)?></th>
	   			<th width="115"><?=word(11633)?></th>
				<?if(isset($languages) && count($languages)):?>
				<th style="width: 30px;"><?=word(11629)?></th>
				<?endif?>
			</tr>
			<?foreach($structure->pages as $k => $item):?>
			<tr class="<?=($k%2 ==1)?'even':'odd'?>" id="structure_page_<?=$item['id']?>">
				<td><input name="new[pages][]" type="checkbox" value="<?=$item['id']?>"<?if(in_array($item['id'], $gathered['pages'])):?> checked="checked"<?endif?> /></td>
				<td class="page_title"><?=Utility::truncate($item['title'], 20)?></td>
				<td><?=date("d-m-'y",strtotime($item['online_date']))?> t/m <?=date("d-m-'y",strtotime($item['offline_date']))?></td>
				<?if(isset($languages) && count($languages)):?>
		   		<td align="center"><?=$item->language['code']?></td>
				<?endif?>
			</tr>
			<?endforeach?>
		</tbody></table>
	</fieldset>
	<?endif?>
	<?if(count($structure->urls)):?>
	<fieldset>
		<legend><?=word(11638)?></legend>
		<table cellspacing="1" cellpadding="3" class="sort-table structure_urls" style="width: 100%; clear: both;">
		<tbody>
		<tr>
			<th width="16"></th>
			<th><?=word(11757)?></th>
			<th><?=word(11636)?></th>
			<?if(isset($languages) && count($languages)):?>
		   	<th style="width: 30px;"><?=word(11629)?></th>
			<?endif?>
		</tr>
		<?foreach($structure->urls as $k => $item):?>
		<tr class="<?=($k%2 ==1)?'even':'odd'?>" id="structure_url_<?=$item['id']?>">
			<td><input name="new[urls][]" type="checkbox" value="<?=$item['id']?>"<?if(in_array($item['id'], $gathered['urls'])):?> checked="checked"<?endif?> /></td>
			<td class="url_link" title="<?=$item['link']?>"><?=Utility::truncate($item['link'],40,'[...]',false,true)?></td>
		   	<td><?=$item['target']?></td>
			<?if(isset($languages) && count($languages)):?>
			<td align="center"><?=$item->language['code']?></td>
			<?endif?>
		</tr>
		<?endforeach?>
		</tbody>
		</table>
		<?endif?>
	</fieldset>
</div>
<?elseif($gathered['step'] == 2):?>
<div id="step_3">
	<fieldset>
		<legend><?=word(11017)?></legend>
		<input type="hidden" id="page_contents_exclude" name="new[page_contents_exclude]" value="<?=htmlentities(Utility::arrayValue($gathered, 'page_contents_exclude'))?>" />
		<input type="hidden" id="page_contents_content_exclude" name="new[page_contents_content_exclude]" value="<?=htmlentities(Utility::arrayValue($gathered, 'page_contents_content_exclude'))?>" />
		<input type="hidden" id="page_contents_config_exclude" name="new[page_contents_config_exclude]" value="<?=htmlentities(Utility::arrayValue($gathered, 'page_contents_config_exclude'))?>" />
		<select id="page_select">
			<option value="0">-- <?=word(116)/* Choose */?> --</option>
			<?foreach($gathered['pages'] as $page_id): $page = ActiveRecord::factory('LiveEdit_Page', $page_id)?>
			<option value="<?=$page['id']?>" <?if(count($gathered['pages']) == 1):?> selected="selected"<?endif?>>
				<?=Utility::truncate($page['title'], 20)?>
				<?if(isset($languages) && count($languages)):?> <?=$page->language['code']?><?endif?>
			</option>
			<?endforeach?>
		</select>
	</fieldset>
	<fieldset>
		<legend><?=word(11634)?></legend>
		<div id="page_contents"></div>
	</fieldset>
</div>
<?endif?>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
		<td>
			<?if(!empty($gathered['step'])):?>
			<input type="submit" name="prev" value="<?=word(298)?>" />
			<?endif?>
			<?if(Utility::arrayValue($gathered, 'step') != 2):?>
			<input type="submit" name="next" value="<?=word(299)?>" />
			<?else:?>
			<input type="submit" name="finalize" value="<?=word(82)/* Ok */?>" />
			<?endif?>
		</td>
		<td align="right">
			<?if(!empty($_GET['returnPage'])):?>
				<input type="submit" value="<?=word(45)/* Back */?>" onclick="isDirty=false; window.location = '<?=htmlentities($_GET['returnPage'])?>'; return false;" class="okBut" />
			<?endif?>
		</td>
	</tr></table>
</div>
</form>
</body>
</html>	
