<?php
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	PH::phoundryErrorPrinter("Deze functionaliteit is vervangen door de kopieer en plak knop op de Site Structuur interface", true);
}

// Make sure the current user in the current role has access to this page:
checkAccess();

$structure_id = (int) Utility::arrayValue($_GET, 'structure_id');
if(empty($structure_id)) die('No SiteStructure selected');

$structure = ActiveRecord::factory('LiveEdit_SiteStructure', $structure_id);
if(!$structure->isLoaded()) die('No Valid SiteStructure selected');

$res = DB::iQuery("SHOW CREATE TABLE brickwork_site_structure")->getAsArray(1);
$utf8 = !strpos($res[0]['Create Table'], "latin1");
$HASACL = Features::brickwork('ACL');
$nr_of_sites = DB::iQuery('SELECT count(*) AS cnt FROM brickwork_site')->first()->cnt;

$pages = array();
foreach($structure->pages as $item) $pages[] = $item['id'];

$urls = array();
foreach($structure->urls as $item) $urls[] = $item['id'];

// Array that keeps persistant data among the various steps
$gathered = array(
	'structure_name' => $structure['name'],
	'parent_structure' => $structure['parent_id'],
	'step' => 0,
	'site' => $structure['site_identifier'],
	'pages' => $pages,
	'urls' => $urls,
	'page_contents_exclude' => '',
	'page_contents_content_exclude' => '',
	'page_contents_config_exclude' => '',
);
if (isset($_POST['gathered'])) {
	$gathered = $_POST['gathered'];
	if (!Utility::isUtf8($gathered)) {
		$gathered = utf8_encode($gathered);
	}
	$gathered = array_merge($gathered, json_decode($gathered, true));
}
if(isset($_POST['new']))
	$gathered = array_merge($gathered, $_POST['new']);

if($nr_of_sites == 1)
	$gathered['site'] = DB::iQuery('SELECT identifier FROM brickwork_site')->first()->identifier;

if(array_key_exists('next', $_POST))
	$gathered['step'] = Utility::arrayValue($gathered, 'step') + 1;
	
if(array_key_exists('prev', $_POST))
	$gathered['step'] = Utility::arrayValue($gathered, 'step') - 1;

	
if(array_key_exists('finalize', $_POST))
{
	function addAcl( $instance, $description, $class, $relation = null, $parent = null )
	{
		$sql = sprintf('INSERT INTO brickwork_auth_object (parent_id, instanceid, description, class, foreign_id )' .
					   " VALUES(%s,'%s','%s','%s',%d)" .
					   " ON DUPLICATE KEY UPDATE description = '%3\$s', parent_id = %1\$s, foreign_id = %5\$s, class='%4\$s'",
						(is_null($parent) ? 'NULL': (int)$parent),
						DB::quote($instance),
						DB::quote($description),
						DB::quote($class),
						(is_null($relation) ? 'NULL' : (int)$relation)
					  );

		$res = DB::iQuery($sql);

		if ( isError($res))
			error_log($res->errstr );
			
		$sql = sprintf("SELECT id FROM brickwork_auth_object WHERE instanceid = '%s'",DB::quote($instance));
		$res = DB::Query( $sql );
	
		if ( isError($res)) error_log($res->errstr );
			
		$row = $res->first();
		
		return $row->id;
		
	}
	
	// Copy the site structure
	$new_structure = ActiveRecord::factory('LiveEdit_SiteStructure');
	foreach($structure->getAsArray() as $k => $v) $new_structure[$k] = $v;
	$new_structure['id'] = null;
	$new_structure['site_identifier'] = $gathered['site'];
	$new_structure['name'] = $gathered['structure_name'];
	if(!empty($gathered['parent_structure']))
	{
		$new_structure['parent_id'] = $gathered['parent_structure'];
	}
	$new_structure['prio'] = $structure['prio'] + 1;
	$new_structure->save();
	
	if($HASACL)
	{
		$structureACLmap = Array();
		$structureACLmap['site'] = addAcl('Site/' . $new_structure['site_identifier'], $new_structure['site_identifier'], 'Site');
		$structureACLmap['page'] = addAcl('PageContentHandler','Website page content handler','Site', $structureACLmap['site'] );
		
		// Get the parent id
		if(!empty($gathered['parent_structure']))
		{
			$res = DB::iQuery('SELECT id FROM brickwork_auth_object WHERE instanceid = '.DB::quoteValue('StructureItem/'.$new_structure['parent_id']));
		}
		
		$structureACLmap[ (int) $new_structure['id'] ] = addAcl(
			'StructureItem/' . $new_structure['id'],
			$new_structure['name'],
			'SiteStructure',
			$new_structure['id'],
			(isset($res) && $res instanceof ResultSet && $res->current() ? $res->first()->id : $structureACLmap['page'])
		);
	}
		
	
	foreach($structure->pages as $page)
	{
		if(in_array($page['id'], $gathered['pages']))
		{
			$new_page = ActiveRecord::factory('LiveEdit_Page');
			foreach($page->getAsArray() as $k => $v) $new_page[$k] = $v;
			$new_page['id'] = null;
			$new_page['site_structure_id'] = $new_structure['id'];
			$new_page->save();
			
			foreach($page->page_contents as $page_content)
			{
				// Copy Page Content
				if(!in_array($page_content['id'], explode('|', $gathered['page_contents_exclude'])))
				{
					$new_content = ActiveRecord::factory('LiveEdit_PageContent');
					foreach($page_content->getAsArray() as $k => $v) $new_content[$k] = $v;
					$new_content['id'] = null;
					$new_content['page_id'] = $new_page['id'];
					$new_content->save();
					
					// Copy Module Content
					if($page_content->module['ptid_content'] && !in_array($page_content['id'], explode('|', $gathered['page_contents_content_exclude'])))
					{
						if(in_array('PageContentCopyable', class_implements($page_content->module['class'])))
						{
							call_user_func(
								array($page_content->module['class'], 'copyContent'),
								$page_content['id'],
								$new_content['id'],
								$page_content->module['ptid_content'],
								$gathered['site']
							);
						}
					}
					
					// Copy Module Config
					if($page_content->module['ptid_config'] && !in_array($page_content['id'], explode('|', $gathered['page_contents_config_exclude'])))
					{
						$table = PhoundryTools::getTableName($page_content->module['ptid_config']);
						if($table)
						{
							$res = DB::iQuery(sprintf('SELECT * FROM %s WHERE page_content_id = %d', $table, $page_content['id']));
							
							if(!isError($res) && $res instanceof ResultSet && $res->current())
							{
								$config = get_object_vars($res->first());
								$config['page_content_id'] = $new_content['id'];
								DB::insertRows(array($config), $table);
							}
						}
					}
				}
			}
		}
	} // Pages Foreach
	
	foreach($structure->urls as $url)
	{
		if(in_array($url['id'], $gathered['urls']))
		{
			$new_url = ActiveRecord::factory('LiveEdit_Url');
			foreach($url->getAsArray() as $k => $v) $new_url[$k] = $v;
			$new_url['id'] = null;
			$new_url['site_structure_id'] = $new_structure['id'];
			$new_url->save();
		}
	}
	
	if(!empty($_GET['returnPage']))
	{
		$return_page = parse_url($_GET['returnPage']);
		
		// Redirect to the newly created structure if it is in the same site
		if($structure['site_identifier'] == $new_structure['site_identifier'])
		{
			$return_page['fragment'] = $new_structure['id'];
		}
		
		trigger_redirect(
			$return_page['scheme'].'://'.$return_page['host'].
			$return_page['path'].'?'.$return_page['query'].'#'.$return_page['fragment']
		);
	}
	
	exit;
}

try {
	$languages = ActiveRecord::factory('LiveEdit_SiteLanguage')->where('site_identifier', $structure['site_identifier'])->get();
}catch (Exception $e){}
if(array_key_exists('site', $gathered))
{
	$root_structures = ActiveRecord::factory('LiveEdit_SiteStructure')->where('parent_id', 'NULL', 'IS', false)->where('site_identifier', $gathered['site'])->get();
	
	function getChildren($children)
	{
		global $HASACL, $utf8;
		
		$items = array();
		foreach($children as $child)
		{
			$classes = array();
			if($child['show_menu'])		$classes[] = 'show_menu';
			if($child['show_sitemap'])	$classes[] = 'show_sitemap';
			if($child['secure'])		$classes[] = 'secure';
			if($HASACL) {
				if($child['restricted'])	$classes[] = 'restricted';
			}
			
			$items[] = array(
				'attributes'	=> array(
					'id'	=> 'ss_'.$child['id'],
					'rel'	=> 'SiteStructure',
					'class'	=> implode(' ', $classes)
				),
				'data'			=> ($utf8 ? $child['name'] : utf8_encode($child['name'])),
				'state'			=> (count($child->children) != 0) ? 'open' : '',
				'children'		=> getChildren($child->children)
			);
		}
		return $items;
	}
	
	$tree_structure = array(
		'attributes'	=> array('id' => 'Structuur', 'rel' => 'MainStructure'),
		'data'			=> word(11600 /* Structure */),
		'state'			=> 'open',
		'children'		=> getChildren($root_structures)
	);
}

/**
 * Test if an object is of type error
 *
 * @return Boolean
 */
function isError(&$e) {
    $v = PHP_VERSION;

    switch (TRUE) {
        // old php
        case ($v < 4.2):
            if ((get_class($e) == 'error')
                ||	(is_subclass_of($e, 'error'))) {
                return TRUE;
            }
            break;

        // php 4.2 < 5
        case ($v >= 4.2 && $v < 5):
            return (is_a($e, 'Error'));
            break;

        // php 5
        case ($v >= 5):
            return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
            break;
    }

    // not an error
    return FALSE;
}

header('Content-type: text/html; charset=' . ($utf8 ? 'utf-8' : 'iso-8859-1'));
include 'structurecopy.tpl.php';