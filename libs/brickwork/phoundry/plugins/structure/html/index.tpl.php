<?$this->stack('head')?>
<meta name="pageID" id="pageID" content="brickwork.structure" />
<link rel="stylesheet" href="/brickwork/css/tree_component.css" type="text/css" />
<script type="text/javascript" src="/brickwork/js/htmlspecialchars.js"></script>
<script type="text/javascript" src="/brickwork/js/json2.js"></script>
<script type="text/javascript" src="/brickwork/js/jquery/jquery.tmpl.js"></script>
<script type="text/javascript" src="/brickwork/js/jquery/jquery.listen.js"></script>
<script type="text/javascript" src="/brickwork/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="/brickwork/js/shortcut.js"></script>
<script type="text/javascript" src="/brickwork/js/css.js"></script>
<script type="text/javascript" src="/brickwork/js/tree_component.js"></script>
<?include 'js/main.js.php';?>
<script type="text/javascript">
//<![CDATA[
window.onBeforeUnload = function() {
	if(siteStructure.initial_structure != siteStructure.serialize()) {
		return '<?=escSquote($this->word(11604))?>';
	}
}

window.onUnload = function() {
	if (<?=json_encode($edit_mode)?>) {
		siteStructure.setLock(false, true);
	}
}

jQuery(function($){
	if ($.fn.prop === undefined) {
		$.fn.prop = $.fn.attr;
	}

	if (<?=json_encode($edit_mode)?>) {
		siteStructure.setLock(true);
	}
	$.ajaxSetup({cache : false});

	// Set the Ajax Spinner
	$('#ajax_loading').
		ajaxStart(function(){
			$(this).show();
			$('#save_structures').prop('disabled', true);
		}).
		ajaxStop(function(){
			$(this).hide();
			$('#structure_tree').triggerHandler('change',
				$('#ss_'+siteStructure.activestructure.id).get(0));
		});

	$("#structure_tree").bind("click", function(e) {
		var clicked = $(e.target);
		if(clicked.is("#Structuur")) {
			var nodes = clicked.children("ul").children("li");
			if(clicked.is(".open")) {
				nodes.each(function(){
					siteStructure.tree.close_branch(this);
				});
				clicked.removeClass("open").addClass("closed");
			}
			else {
				nodes.each(function(){
					siteStructure.tree.open_branch(this);
				});
				clicked.addClass("open").removeClass("closed");
			}
			e.preventDefault();
			e.stopPropagation();
		}
	});

	$("#structure_tree").bind("click", function(e) {
		var clicked = $(e.target);
		if(clicked.is("#Structuur")) {
			var nodes = clicked.children("ul").children("li");
			if(clicked.is(".open")) {
				nodes.each(function(){
					siteStructure.tree.close_branch(this);
				});
				clicked.removeClass("open").addClass("closed");
			}
			else {
				nodes.each(function(){
					siteStructure.tree.open_branch(this);
				});
				clicked.addClass("open").removeClass("closed");
			}
			e.preventDefault();
			e.stopPropagation();
		}
	});

	$("#structure_tree").bind("click", function(e) {
		var clicked = $(e.target);
		if(clicked.is("#Structuur")) {
			var nodes = clicked.children("ul").children("li");
			if(clicked.is(".open")) {
				nodes.each(function(){
					siteStructure.tree.close_branch(this);
				});
				clicked.removeClass("open").addClass("closed");
			}
			else {
				nodes.each(function(){
					siteStructure.tree.open_branch(this);
				});
				clicked.addClass("open").removeClass("closed");
			}
			e.preventDefault();
			e.stopPropagation();
		}
	});

	$('#edit_mode').bind('click', function(){
		$.ajax({
			url : '<?=$PHprefs['url']?>/core/checkLock.php?<?=QS(false, 'RID='.$site_identifier)?>',
			type: 'post',
			data: 'RID=<?= $site_identifier ?>',
			success : function(lock){
				if (!lock || lock === '_' || confirm(lock)) {
					location.search = (<?=json_encode(QS(false, '_edit_mode=1'))?>);
				}
			}
		});
	});
	$(window).bind('resize', function(){
		$('#structure_tree').css(
			'height', ($('#contentFrame').innerHeight()-110)+'px'
		);
	}).resize();

	// Buttons
	$('#refresh_tree').bind('click', function(){ siteStructure.rebuild(); return false; });
	$('#new_item').bind('click', function(){ siteStructure.createStructure(); return false; });
	$('#rename_item').bind('click', function(){ siteStructure.renameStructure(); return false; });
	$('#copy_item').bind('click', function(){ siteStructure.copyStructures(); return false; });
	$('#paste_item').bind('click', function(){ siteStructure.pasteStructures(); return false; });
	$('#del_item').bind('click', function(){ siteStructure.deleteStructure(); return false;	});
	$('#move_up').bind('click', function(){ siteStructure.moveStructure('up'); return false; });
	$('#move_down').bind('click', function(){ siteStructure.moveStructure('down'); return false; });
	$('#move_left').bind('click', function(){ siteStructure.moveStructure('left'); return false; });
	$('#move_right').bind('click', function(){ siteStructure.moveStructure('right'); return false; });
	$('#save_structures').bind('click', function(){ siteStructure.saveStructures(); return false; });
	$('#filter input').bind('keyup', function(){
		var str = $(this).val();
		siteStructure.filter(str);
		$('#filter span').toggleClass('filtered', !!str);
	});
	$('#filter span').bind('click', function(){
		$('#filter input').val('').triggerHandler('keyup');
		$('#filter span').removeClass('filtered');
	});

	// Button toggeling
	$('#structure_tree').bind('change rename move delete create', function(e, node){
		if(node)
		{
			node = $(node);
			var structure = siteStructure.getStructure(node);

			$('#new_item').prop('disabled', !(node.length == 1));
			$('#copy_item, #paste_item').prop('disabled', !node.length || structure.state === 'deleted');

			// Renameable
			$('#rename_item').prop('disabled', !(node.length == 1 &&
					siteStructure.tree.check("renameable", node)));

			// Deletable
			if(node.length == 1 && -1 !== $.inArray(node.attr('rel'), ['SiteStructure']))
			{
				$('#del_item').prop('disabled', false);
			}
			else
			{
				$('#del_item').prop('disabled', true);
			}

			// Moving around
			if(node.is('[rel=MainStructure]')) {
				$('#move_up, #move_down, #move_left, #move_right').prop('disabled', true);
			}
			else {
				$('#move_up').prop('disabled', !node.prev('li').length);
				$('#move_down').prop('disabled', !node.next('li').length);
				$('#move_left').prop('disabled', !node.parents('li:first:not(#Structuur)').length);
				$('#move_right').prop('disabled', !node.prev('li').length);
			}
		}
		else {
			$('button.structure_button').prop('disabled', true);
		}
		// Structure changed
		if(siteStructure.initial_structure != siteStructure.serialize())
		{
			if($('#save_structures').prop('disabled'))
			{
				$('#save_structures').fadeOut('fast').prop('disabled', false).fadeIn('fast');
			}
		}
		else
		{
			$('#save_structures').prop('disabled', true);
		}
	});

	// Hotkeys
	(function(){
	var dis = {disable_in_input: true};
	<?if($edit_mode && $this->access("site_structure", "u")):?>
	shortcut.add("f2", function(){ siteStructure.renameStructure();});
	shortcut.add("ctrl+up", function() { siteStructure.moveStructure('up');});
	shortcut.add("ctrl+down", function() { siteStructure.moveStructure('down');});
	shortcut.add("ctrl+left", function() { siteStructure.moveStructure('left');});
	shortcut.add("ctrl+right", function() { siteStructure.moveStructure('right');});
	shortcut.add("ctrl+s", function() { siteStructure.saveStructures();});
	<?endif?>
	<?if($edit_mode && $this->access("site_structure", "d")):?>
	shortcut.add("delete", function() { siteStructure.deleteStructure();}, dis);
	<?endif?>
	<?if($edit_mode && $this->access("site_structure", "i")):?>
	shortcut.add("insert", function() { siteStructure.createStructure();}, dis);
	<?endif?>
	<?if($edit_mode && $this->access("site_structure", "c")):?>
	shortcut.add("ctrl+c", function() { siteStructure.copyStructures();}, dis);
	shortcut.add("ctrl+v", function() { siteStructure.pasteStructures();}, dis);
	<?endif?>
	shortcut.add("return", function() { siteStructure.tree.select_branch();}, dis);
	shortcut.add("space", function() { siteStructure.tree.select_branch();}, dis);
	shortcut.add("up", function() { siteStructure.tree.get_prev();}, dis);
	shortcut.add("down", function() { siteStructure.tree.get_next();}, dis);
	shortcut.add("left", function() { siteStructure.tree.get_left();}, dis);
	shortcut.add("right", function() { siteStructure.tree.get_right();}, dis);
	}());

	// Info boxes
	(function() {
		var cur_info, cur_legend,
		infoTimer = false,
		clearTimer = function(){
			if(infoTimer) {
				clearTimeout(infoTimer);
				infoTimer = false;
			}
		},
		show = function() {
			var legend = $(this),
			info = legend.siblings('.miniinfo');

			if(info.length) {
				if(cur_info) {
					cur_info.hide();
				}
				if(cur_legend) {
					cur_legend.addClass("info");
				}

				cur_info = info;
				cur_legend = legend;
				legend.removeClass("info");
				info.fadeIn();
			}
		};

		$('fieldset legend.info').live('click', function(){
			clearTimer();
			show.call(this);
		}).
		live('mouseenter', function() {
			clearTimer();
			infoTimer = setTimeout($.proxy(show, this), 750);
		}).live('mouseleave', clearTimer);
	})();

	// Structure Properties
	$('#structure_pane').
	listen('click', '.structure_show_menu', function(){
		siteStructure.activestructure.show_menu = 0+$(this).prop('checked');
		siteStructure.refresh(siteStructure.activestructure);
		$('#structure_tree').triggerHandler('change',  siteStructure.getNode(siteStructure.activestructure));
	}).
	listen('click', '.structure_show_sitemap', function(){
		siteStructure.activestructure.show_sitemap = 0+$(this).prop('checked');
		siteStructure.refresh(siteStructure.activestructure);
		$('#structure_tree').triggerHandler('change',  siteStructure.getNode(siteStructure.activestructure));
	}).
	listen('click', '.structure_secure', function(){
		siteStructure.activestructure.secure = 0+$(this).prop('checked');
		siteStructure.refresh(siteStructure.activestructure);
		$('#structure_tree').triggerHandler('change',  siteStructure.getNode(siteStructure.activestructure));
	}).
	listen('click', 'a.structure_roles', function(){
		siteStructure.structureRoles(siteStructure.activestructure);
		return false
	}).
	listen('click', '.structure_restricted', function(){
		siteStructure.activestructure.restricted = 0+$(this).prop('checked');
		siteStructure.refresh(siteStructure.activestructure);
		$('#structure_tree').triggerHandler('change',  siteStructure.getNode(siteStructure.activestructure));
	}).
	listen('change', '.structure_linked_to', function(){
		var linked_to = $(this).val();
		siteStructure.activestructure.linked_to = linked_to ? linked_to : null;
		siteStructure.refresh(siteStructure.activestructure);
		$('#structure_tree').triggerHandler('change',  siteStructure.getNode(siteStructure.activestructure));
	}).
	listen('click', '.structure_ssl', function(){
		siteStructure.activestructure.ssl = 0+$(this).prop('checked');
		siteStructure.refresh(siteStructure.activestructure);
		$('#structure_tree').triggerHandler('change',  siteStructure.getNode(siteStructure.activestructure));
	}).
	listen('keyup', '.translation', function(){
		// Fix for json_encode of an empty array resulting in an hey guess what.. array!
		if(siteStructure.activestructure.translations instanceof Array) siteStructure.activestructure.translations = {};
		siteStructure.activestructure.translations[$(this).attr('name')] = $(this).val();
	}).
	listen('blur', '.translation', function(){
		$('#structure_tree').triggerHandler('change', siteStructure.getNode(siteStructure.activestructure));
	}).
	listen('click', 'a.custom_site_structure', function(){
		siteStructure.customProperties(siteStructure.activestructure);
		return false;
	}).
	delegate('#structure_pages button.contents', "click", function(){
		var page_id = $(this).parents('fieldset:first').
		find("table tr.active").attr('id').replace(/^structure_page_/i,'');
		if(page_id) {
			siteStructure.pageContents(page_id);
		}
		return false;
	}).
	delegate("#structure_pages button.add", 'click', function(){
		siteStructure.addPage(siteStructure.activestructure);
		return false;
	}).
	delegate("#structure_pages button.view", 'click', function(){
		siteStructure.viewPage($(this).parents('fieldset:first').
			find("table tr.active").attr('id').
			replace(/^structure_page_/i,''), siteStructure.activestructure);
		return false;
	}).
	delegate("#structure_pages button.edit", 'click', function(){
		siteStructure.editPage($(this).parents('fieldset:first').
			find("table tr.active").attr('id').
			replace(/^structure_page_/i,''), siteStructure.activestructure);
		return false;
	}).
	delegate("#structure_pages button.copy", 'click', function(){
		siteStructure.copyPage($(this).parents('fieldset:first').
			find("table tr.active").attr('id').
			replace(/^structure_page_/i,''), siteStructure.activestructure);
		return false;
	}).
	delegate("#structure_pages button.delete", 'click', function(){
		if(siteStructure.activestructure) {
			var confirm_msg = (<?=$this->json_encode($this->word(11611 /* Weet u zeker dat u '{page_title}' wilt verwijderen? */ ))?>);
			if(confirm(confirm_msg.replace(/\{page_title\}/gi, $(this).parents('fieldset:first').
					find("table tr.active .page_title").text()))) {
				siteStructure.deletePage($(this).parents('fieldset:first').
				find("table tr.active").attr('id').replace(/^structure_page_/i,''));
			}
		}
		return false;
	}).
	delegate("#structure_urls button.add", 'click', function(){
		siteStructure.addUrl(siteStructure.activestructure);
		return false;
	}).
	delegate("#structure_urls button.view", 'click', function(){
		siteStructure.viewUrl($(this).parents('fieldset:first').
			find("table tr.active").attr('id').
			replace(/^structure_url_/i,''), siteStructure.activestructure);
		return false;
	}).
	delegate("#structure_urls button.edit", 'click', function(){
		siteStructure.editUrl($(this).parents('fieldset:first').
			find("table tr.active").attr('id').
			replace(/^structure_url_/i,''), siteStructure.activestructure);
		return false;
	}).
	delegate("#structure_urls button.copy", 'click', function(){
		siteStructure.copyUrl($(this).parents('fieldset:first').
			find("table tr.active").attr('id').
			replace(/^structure_url_/i,''), siteStructure.activestructure);
		return false;
	}).
	delegate("#structure_urls button.delete", 'click', function(){
		if(siteStructure.activestructure) {
			var confirm_msg = (<?=$this->json_encode($this->word(11641 /* Weet u zeker dat u '{url_link}' wilt verwijderen? */ ))?>);
			if(confirm(confirm_msg.replace(/\{url_link\}/gi, $(this).parents('fieldset:first').
					find("table tr.active .url_link").text()))) {
				siteStructure.deleteUrl($(this).parents('fieldset:first').
				find("table tr.active").attr('id').replace(/^structure_url_/i,''));
			}
		}
		return false;
	}).
	delegate("#structure_pages tr td", "click", function(){
		$(this).parents("tr:first").siblings("tr").andSelf().
			removeClass("active").end().end().addClass("active");

		$(this).parents("fieldset:first").
			find("button.record_action").attr("disabled", false);
	}).
	delegate("#structure_pages tr td", "dblclick", function(e){
		var page_id = $(this).parents('tr:first').attr('id').replace(/^structure_page_/i,'');
		if(page_id) {
			siteStructure.pageContents(page_id);
		}
		return false;
	}).
	delegate("#structure_urls tr td", "click", function(){
		$(this).parents("tr:first").siblings("tr").andSelf().
			removeClass("active").end().end().addClass("active");

		$(this).parents("fieldset:first").
			find("button.record_action").attr("disabled", false);
	}).
	delegate("#structure_urls tr td", "dblclick", function(){
		siteStructure.editUrl($(this).parents('tr:first').attr('id').replace(/^structure_url_/i,''), siteStructure.activestructure);
		return false;
	});

	// Initialization
	siteStructure.access_rights =
		{ site_structure : (<?=json_encode($this->getAccessRights("site_structure"))?>)
		, brickwork_url : (<?=json_encode($this->getAccessRights("brickwork_url"))?>)
		, brickwork_page : (<?=json_encode($this->getAccessRights("brickwork_page"))?>)
	};
	siteStructure.saved = (<?=json_encode($saved)?>);
	siteStructure.structures = (<?=json_encode($structures)?>);
	siteStructure.initial_structure = siteStructure.serialize();

	// See if we got a modified structure parked in the topmenu frame
	if(typeof(window.parent.frames['PHtopmenu'].siteStructure_structures_<?=$site_identifier?>) != 'undefined')
	{
		if(siteStructure.saved)
		{
			siteStructure.saved = false;
		}
		else
		{
			siteStructure.structures = window.parent.frames['PHtopmenu'].siteStructure_structures_<?=$site_identifier?>;
		}

		window.parent.frames['PHtopmenu'].siteStructure_structures_<?=$site_identifier?> = undefined;
	}

	siteStructure.rebuild();
	siteStructure.draggable(<?=json_encode($edit_mode)?>);
	window.focus();
});
//]]>
</script>
<style type="text/css">
#filter
{
	padding: 5px;
	position: relative;
}

#filter input
{
	width: 99%;
}

#filter span.filtered
{
	position: absolute;
	display: bock;
	right: 8px;
	top: 0px;
	width: 16px;
	height: 100%;
	cursor: pointer;
	background : url(<?=$PHprefs['url']?>/core/icons/cancel.png) center center no-repeat;
}

/* DEFAULT ICON */
.tree li#Structuur li a
 {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/page.gif);
}

.tree li#Structuur a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/base.gif);
}

h1 {
	color: #000;
	font-family: arial;
	font-size: 11pt;
}

fieldset legend {
	font-weight: bold;
}

button
{
	padding: 0;
}

.buttonBar {
   background: #ededed url(html/buttonbar_bg.gif) top left repeat-x;
   border: 1px solid #bbb;
   -moz-border-radius: 3px;
   -webkit-border-radius: 3px;
   border-radius: 3px;
}

.tree li.MainStructure ul
{
	display: block;
}

.tree li.MainStructure li.closed ul
{
	display: none;
}

.tree li a.search
{
	font-weight: bold;
}

.contains-search li
{
   display : none;
}

li.contains-search
{
	display: block;
}

legend.info
{
	padding-right: 16px;
	background: transparent url(<?=$PHprefs['url']?>/layout/pics/info2.png) center right no-repeat;
	cursor: help;
}

div.miniinfo
{
	display: none;
}

</style>
<?$this->stack('head')?>
<div style="float: left; width: 400px;">
<div class="buttonBar">
	<span id="ajax_loading" style="float: right; display: none;"><img src="html/ajax-loader.gif" /></span>
	<?if(!$edit_mode && $this->access("site_structure", "u")):?>
	<button id="edit_mode"><?=$this->word(11618 /* Wijzig Modus */)?></button>
	<?endif?>
	<?if($edit_mode && $this->access("site_structure", "i")):?>
	<button id="new_item" class="structure_button" disabled="disabled" title="<?= $this->word(71 /* Nieuw */)?>"><img alt="<?= $this->word(71 /* Nieuw*/)?>" src="<?= $PHprefs['url'] ?>/core/icons/add.png" /></button>
	<?endif?>
	<?if($edit_mode && $this->access("site_structure", "u")):?>
	<button id="rename_item" class="structure_button" disabled="disabled" title="<?= $this->word(297); /* Rename */?>"><img alt="<?= $this->word(297); /* Rename */?>" src="<?= $PHprefs['url'] ?>/core/icons/page_paintbrush.png" /></button>
	<?endif?>
	<?if($edit_mode && $this->access("site_structure", "c")):?>
	<button id="copy_item" class="structure_button" disabled="disabled" title="<?= $this->word(109); /* Copy */?>"><img alt="<?= $this->word(109); /* Copy */?>" src="<?= $PHprefs['url'] ?>/core/icons/page_copy.png" /></button>
	<button id="paste_item" class="structure_button" disabled="disabled" title="<?= $this->word(11639); /* Paste */?>"><img alt="<?= $this->word(11639); /* Paste */?>" src="<?= $PHprefs['url'] ?>/core/icons/page_paste.png" /></button>
	<?endif?>
	<?if($edit_mode && $this->access("site_structure", "d")):?>
	<button id="del_item" class="structure_button" disabled="disabled" title="<?= $this->word(8); /* Delete */ ?>"><img alt="<?= $this->word(8); /* Delete */ ?>" src="<?= $PHprefs['url'] ?>/core/icons/bin.png" /></button>
	<?endif?>
	<?if($edit_mode && $this->access("site_structure", "u")):?>
	<img class="div" alt="|" src="<?= $PHprefs['url'] ?>/core/pics/div.gif"/>
	<button id="move_up" class="structure_button" disabled="disabled" title="<?= $this->word(11619); /* Naar boven */ ?>"><img alt="<?= $this->word(11619); /* Naar boven */ ?>" src="<?= $PHprefs['url'] ?>/core/icons/control_play_blue_up.png" /></button>
	<button id="move_down" class="structure_button" disabled="disabled" title="<?= $this->word(11620); /* Naar beneden */ ?>"><img alt="<?= $this->word(11620); /* Naar beneden */ ?>" src="<?= $PHprefs['url'] ?>/core/icons/control_play_blue_down.png" /></button>
	<button id="move_left" class="structure_button" disabled="disabled" title="<?= $this->word(11621); /* Naar links */ ?>"><img alt="<?= $this->word(11621); /* Naar links */ ?>" src="<?= $PHprefs['url'] ?>/core/icons/control_start_blue.png" /></button>
	<button id="move_right" class="structure_button" disabled="disabled" title="<?= $this->word(11622); /* Naar rechts */ ?>"><img alt="<?= $this->word(11622); /* Naar rechts */ ?>" src="<?= $PHprefs['url'] ?>/core/icons/control_end_blue.png" /></button>
	<?endif?>
	<button id="refresh_tree" title="Refresh" style="display: none;"><img alt="Refresh" src="<?= $PHprefs['url'] ?>/core/icons/arrow_refresh.png" /></button>
</div>
<fieldset>
<legend>Structuur</legend>
	<div id="filter"><span></span><input placeholder="Filter"></div>
	<div id="structure_tree" style="overflow: auto;"></div>
</fieldset>
</div>
<div id="structure_pane" style="float: left; width: 400px;"></div>

<script type="text/html" id="template_structure">
	<h2><%=htmlspecialchars(structure.name)%></h2>
	<fieldset id="structure_properties">
		<legend class="info"><?=word(11625 /* Eigenschappen */)?></legend>
		<div class="miniinfo"><?=word(11645)?></div>
		<div>
			<?if($edit_mode && $this->access("site_structure", "u")):?>
			<div><label><input type="checkbox" class="structure_show_menu" value="1"<%=(structure.show_menu ? ' checked="checked"' : '')%> /> <?=word(11030 /* Toon in menu */)?></label></div>
			<?else:?>
			<div><img <%=(structure.show_menu ? 'src="<?=$PHprefs['url']?>/core/icons/tick.png" alt="on"' : 'src="<?=$PHprefs['url']?>/core/icons/cross.png" alt="off"')%> /> <?=word(11030 /* Toon in menu */)?></div>
			<?endif?>
			<?if(Features::brickwork('sitemap')):?>
			<?if($edit_mode && $this->access("site_structure", "u")):?>
			<div><label><input type="checkbox" class="structure_show_sitemap" value="1"<%=(structure.show_sitemap ? ' checked="checked"' : '')%> /> <?=word(11031 /* Toon in sitemap */)?></label></div>
			<?else:?>
			<div><img <%=(structure.show_sitemap ? 'src="<?=$PHprefs['url']?>/core/icons/tick.png" alt="on"' : 'src="<?=$PHprefs['url']?>/core/icons/cross.png" alt="off"')%> /> <?=word(11031 /* Toon in sitemap */)?></div>
			<?endif?>
			<?endif?>
			<?if(Features::brickwork('authorization')):?>
			<div>
				<?if($edit_mode && $this->access("site_structure", "u")):?>
				<label><input type="checkbox" class="structure_secure" value="1"<%=(structure.secure ? ' checked="checked"' : '')%> /> <?=word(11032 /* Beveiligd */)?></label>
				<?else:?>
				<img <%=(structure.secure ? 'src="<?=$PHprefs['url']?>/core/icons/tick.png" alt="on"' : 'src="<?=$PHprefs['url']?>/core/icons/cross.png" alt="off"')%> /> <?=word(11032 /* Beveiligd */)?>
				<?endif?>

				<?if(!empty($site_structure_tid) && $this->access("brickwork_site_structure", "u")):?>
				<%if(structure.state == 'current' || structure.state == 'deleted') {%>
				<a class="structure_roles" title="<?=word(11643)?>" href="#"><img height="16" border="0" width="16" src="<?=$PHprefs['url']?>/core/icons/group_key.png" alt="Roles" /></a>
				<%}%>
				<?endif?>
			</div>
			<?endif?>
			<?if(Features::brickwork('ACL')):?>
			<?if($edit_mode && $this->access("site_structure", "u")):?>
			<div><label><input type="checkbox" class="structure_restricted" value="1"<%=(structure.restricted ? ' checked="checked"' : '')%> /> <?=word(11033 /* Authorisatie nodig */)?></label></div>
			<?else:?>
			<div><img <%=(structure.restricted ? 'src="<?=$PHprefs['url']?>/core/icons/tick.png" alt="on"' : 'src="<?=$PHprefs['url']?>/core/icons/cross.png" alt="off"')%> /> <?=word(11033 /* Authorisatie nodig */)?></div>
			<?endif?>
			<?endif?>
			<?if (false !== strpos(Framework::$site->protocol, 'https')):?>
			<?if($edit_mode && $this->access("site_structure", "u")):?>
			<div><lable><input type="checkbox" class="structure_ssl" value="1"<%=(structure.ssl ? ' checked="checked"' : '')%> /> <?=word(11649 /* Https */)?></label></div>
			<?else:?>
			<div><img <%=(structure.ssl ? 'src="<?=$PHprefs['url']?>/core/icons/tick.png" alt="on"' : 'src="<?=$PHprefs['url']?>/core/icons/cross.png" alt="off"')%> /> <?=word(11649 /* Https */)?></div>
			<?endif?>
			<?endif?>
			<?if($edit_mode && $this->access("site_structure", "u")):?>
			<div><?=word(11034 /* Gelinkt aan */)?>
				<select class="structure_linked_to">
					<option value=""></option>
					<%for(var i = 0, structures = siteStructure.toArray(), len = structures.length; i < len; i++) {
						if(structure.id != structures[i].id && !structures[i].linked_to) {%>
					<option value="<%=structures[i].id%>"<%=structure.linked_to==structures[i].id ? ' selected="selected"': ''%>>
						<%for(var j = 0; j < structures[i].depth; j++) {%>-<%}%>
						<%=structures[i].name%>
					</option>
					<%}}%>
				</select>
			</div>
			<?else:?>
			<div><?=word(11034 /* Gelinkt aan */)?>
					<%for(var i = 0, structures = siteStructure.toArray(), len = structures.length; i < len; i++) {
						if(structure.id != structures[i].id && !structures[i].linked_to && structure.linked_to == structures[i].id) {%>
						<%=structures[i].name%>
					<%}}%>
			</div>
			<?endif?>
			<?if(!empty($custom_site_structure)):?>
			<%if(structure.state == 'current' || structure.state == 'deleted') {%>
			<div><a href="#" class="custom_site_structure"><?=word(11609 /* extra eigenschappen */)?></a></div>
			<%}%>
			<?endif?>
		</div>
	</fieldset>
	<?if(isset($site_languages) && count($site_languages) > 1):?>
	<fieldset id="structure_translations">
		<legend class="info"><?=word(11628 /* Vertalingen */)?></legend>
		<div class="miniinfo"><?=word(11646)?></div>
			<?foreach($site_languages as $k => $translation):?>
			<div>
				<?if($edit_mode && $this->access("site_structure", "u")):?>
				<input class="translation" name="<?=$translation['id']?>" type="text" size="50" value="<%=htmlspecialchars(structure.translations[<?=$translation['id']?>])%>" />
				<?else:?>
				<%=htmlspecialchars(structure.translations[<?=$translation['id']?>])%>
				<?endif?>
				(<?=$translation['code']?>)
			</div>
			<?endforeach?>
	</fieldset>
	<?endif?>
	<%if(structure.state == 'current' || structure.state == 'deleted') {%>
	<fieldset id="structure_pages">
		<legend class="info"><%=<?=$this->json_encode($this->word(11631 /* Pagina's */))?>%></legend>
		<div class="miniinfo"><%=<?=$this->json_encode($this->word(11647))?>%></div>
		<div class="buttonBar">
			<?if($this->access("brickwork_page", "i")):?>
			<button class="add"><?=word(6 /* Voeg Toe */)?></button>
			<img class="div" alt="|" src="<?=$PHprefs['url']?>/core/pics/div.gif" />
			<?endif?>
			<?if($this->access("brickwork_page", "p")):?>
			<button class="view record_action" disabled="disabled"><?=word(9 /* Bekijk */)?></button>
			<?endif?>
			<?if($this->access("brickwork_page", "c")):?>
			<button class="copy record_action" disabled="disabled"><?=word(109 /* Kopieer */)?></button>
			<?endif?>
			<?if($this->access("brickwork_page", "u")):?>
			<button class="edit record_action" disabled="disabled"><?=word(7 /* Wijzig */)?></button>
			<?endif?>
			<?if($this->access("brickwork_page", "d")):?>
			<button class="delete record_action" disabled="disabled"><?=word(8 /* Verwijder*/)?></button>
			<?endif?>
			<?if($this->access("brickwork_page", "p")):?>
			<button class="contents record_action" disabled="disabled"><?=word(11634 /* Pagina Indeling */)?></button>
			<?endif?>
		</div>
		<table cellspacing="1" cellpadding="3" class="sort-table structure_pages" style="width: 100%; clear: both;">
		<thead>
			<tr>
				<th><?=word(198 /* Titel */)?></th>
				<th width="115"><?=word(11633 /* Online periode */)?></th>
				<?if(isset($site_languages) && count($site_languages)):?>
				<th style="width: 30px;"><?=word(11629 /* Taal */)?></th>
				<?endif?>
			</tr>
		</thead>
		<tbody>
		<%for ( var i=0; i < structure.pages.length; i++ ) {%>
		<tr class="<%=(i%2==1 ? 'even' : 'odd')%>" id="structure_page_<%=structure.pages[i].id%>">
			<td class="page_title"><%=htmlspecialchars(structure.pages[i].title)%></td>
			<td class="page_date"><%=structure.pages[i].date%></td>
			<?if(isset($site_languages) && count($site_languages)):?>
			<td class="page_translation_code" align="center"><%=htmlspecialchars(structure.pages[i].language_code)%></td>
			<?endif?>
		</tr>
		<%}%>
		</tbody></table>
	</fieldset>

	<fieldset id="structure_urls">
		<legend class="info"><?=word(11638 /* Url's */)?></legend>
		<div class="miniinfo"><?=word(11648)?></div>
		<div class="buttonBar">
			<?if($this->access("brickwork_url", "i")):?>
			<button class="add"><?=word(6 /* Voeg Toe */)?></button>
			<img class="div" alt="|" src="<?=$PHprefs['url']?>/core/pics/div.gif" />
			<?endif?>
			<?if($this->access("brickwork_url", "p")):?>
			<button class="view record_action" disabled="disabled"><?=word(9 /* Bekijk */)?></button>
			<?endif?>
			<?if($this->access("brickwork_url", "c")):?>
			<button class="copy record_action" disabled="disabled"><?=word(109 /* Kopieer */)?></button>
			<?endif?>
			<?if($this->access("brickwork_url", "u")):?>
			<button class="edit record_action" disabled="disabled"><?=word(7 /* Wijzig */)?></button>
			<?endif?>
			<?if($this->access("brickwork_url", "d")):?>
			<button class="delete record_action" disabled="disabled"><?=word(8 /* Verwijder*/)?></button>
			<?endif?>
		</div>
		<table cellspacing="1" cellpadding="3" class="sort-table structure_urls" style="width: 100%; clear: both;">
		<thead>
		<tr>
			<th>Url</th>
			<th><?=word(11642 /* Venster */)?></th>
			<?if(isset($site_languages) && count($site_languages)):?>
			<th style="width: 30px;"><?=word(11629 /* Taal */)?></th>
			<?endif?>
		</tr>
		</thead>
		<tbody>
		<%for ( var i=0; i < structure.urls.length; i++ ) {%>
		<tr class="<%=(i%2==1 ? 'even' : 'odd')%>" id="structure_url_<%=structure.urls[i].id%>">
			<td class="url_link" title="<%=htmlspecialchars(structure.urls[i].link)%>"><%=htmlspecialchars(structure.urls[i].link_truncated)%></td>
			<td><%=htmlspecialchars(structure.urls[i].target)%></td>
			<?if(isset($site_languages) && count($site_languages)):?>
			<td class="url_translation_code" align="center"><%=htmlspecialchars(structure.urls[i].language_code)%></td>
			<?endif?>
		</tr>
		<%}%>
		</tbody>
		</table>
	</fieldset>
	<%} else {%>
	<fieldset>
		<legend><?=word(11651 /* Onderliggend content */)?></legend>
		<?=word(11644 /* Element dient eerst opgeslagen te worden */)?>
	</fieldset>
	<%}%>
</script>

<?$this->stack('footer_buttons_left')?>
<?if($edit_mode && $this->access("site_structure", "u")):?>
<input class="okBut" type="button" id="save_structures" title="<?= $this->word(11623); /* Wijzigingen opslaan */ ?>" disabled="disabled" value="<?=word(82 /* Ok */)?>" />
<?endif?>
<?$this->stack('footer_buttons_left')?>
<?$this->stack('footer_buttons_right')?>
<?if($edit_mode):?>
<input type="button" value="<?= $this->word(46) ?>" onclick='location.search = (<?=json_encode(QS(false, '_edit_mode'))?>);' />
<?endif?>
<?$this->stack('footer_buttons_right')?>
