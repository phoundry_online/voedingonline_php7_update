<?php
/**
 * StructurePlugin
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class StructurePlugin extends Phoundry_Brickwork_Plugin
{
	/**
	 * Prio used for numbering the site_structure's
	 * @var int
	 */
	private $_prio = 0;

	/**
	 * Array with all structures for the current site
	 * @var array
	 */
	private $_all_structures;

	/**
	 * Provide old to new id mapping for new and copied items
	 * @var array
	 */
	private $_id_mapping;

	/**
	 * @var array Access rights for string ids
	 */
	private $_acces_rights;

	/**
	 * @var bool Edit mode enabled
	 */
	private $_edit_mode;

	public function  __construct()
	{
		parent::__construct();
		$this->_title = $this->word(11600 /* Structuur */);
		$this->_subtitle = Framework::$site->name;

		$this->_data['site_identifier'] = $this->site_identifier;

		if(!empty($_SESSION[get_class($this)]['saved'])) {
			$this->_data['saved'] = true;
			unset($_SESSION[get_class($this)]['saved']);
		}
		else {
			$this->_data['saved'] = false;
		}

		$this->_acces_rights = array();

		$this->_edit_mode = isset($_GET['_edit_mode']) && $_GET['_edit_mode'] === '1';
	}

	/**
	 * The main action that actually shows the interface
	 */
	public function indexAction()
	{
		$structures = array();
		$root_structures = ActiveRecord::factory('Model_Site_Structure')->
			whereNull('parent_id', true)->
			where('site_identifier', $this->site_identifier)->
		get()->getAsArray();

		foreach($root_structures as $ss)
		{
			$structures[] = $ss->getAsJsArray();
		}

		$this->_data['edit_mode'] = $this->_edit_mode;
		$this->_data['structures'] = $structures;
		$this->_data['custom_site_structure'] = PhoundryTools::getTableTID('brickwork_custom_site_structure');
		$this->_data['site_structure_tid'] = PhoundryTools::getTableTID('brickwork_site_structure');
		try {
			$this->_data['site_languages'] = ActiveRecord::factory('Model_Site_Language')->where('site_identifier', $this->site_identifier)->get();
		}
		catch(Exception $e) {
			$this->_data['site_languages'] = null;
		}
		$this->render('html/index.tpl.php', true);
	}

	/**
	 * Echos an listing of the current pages bound to the structure
	 * @param int $id Site Structure ID
	 */
	public function getPagesAction($id = null)
	{
		if(!is_null($id))
		{
			$pages = array();
			foreach(ActiveRecord::factory('Model_Page')->where('site_structure_id', $id)->get()->getAsArray() as $page)
			{
				$pages[] = $page->getAsJsArray();
			}
			echo json_encode($pages);
			exit;
		}

		echo 0;
	}

	/**
	 * Deletes the given page ID and then echos the current pagelisting for the structure
	 * @param int $id Page ID
	 */
	public function deletePageAction($id = null)
	{
		if($this->access("brickwork_page", "d") && !is_null($id))
		{
			$page = ActiveRecord::factory('Model_Page', $id);
			if($page->isLoaded())
			{
				$structure_id = $page['site_structure_id'];
				$page->delete();
				$this->getPagesAction($structure_id);
			}
		}
		echo 0;
	}

	/**
	 * Echos a listing of the current URLS bound to the structure
	 * @param int $id Site Structure ID
	 */
	public function getUrlsAction($id = null)
	{
		if(!is_null($id))
		{
			$urls = array();
			foreach(ActiveRecord::factory('Model_Url')->where('site_structure_id', $id)->get()->getAsArray() as $url)
			{
				$urls[] = $url->getAsJsArray();
			}
			echo json_encode($urls);
			exit;
		}

		echo 0;
	}

	/**
	 * Delete the given url and returns the remaining urls for the bound structure
	 * @param int $id Url ID
	 */
	public function deleteUrlAction($id = null)
	{
		if($this->access("brickwork_url", "d") && !is_null($id))
		{
			$url = ActiveRecord::factory('Model_Url', $id);
			if($url->isLoaded())
			{
				$structure_id = $url['site_structure_id'];
				$url->delete();
				$this->getUrlsAction($structure_id);
			}
		}
		echo 0;
	}

	public function saveStructuresAction()
	{
		$structures = Utility::arrayValue($_POST, 'structures');
		if (!Utility::isUtf8($structures)) {
			$structures = utf8_encode($structures);
		}
		$structures = json_decode($structures, true);
		if(is_null($structures))
		{
			$this->error($this->word(11608 /* Wijzigingen konden niet worden opgeslagen. */ ), true);
		}
		$this->_all_structures = array();
		$this->_id_mapping = array();

		// Load all structures in 1 query;
		foreach(ActiveRecord::factory('Model_Site_Structure')->where('site_identifier', $this->site_identifier)->get()->getAsArray() as $ss)
		{
			$this->_all_structures[$ss['id']] = $ss;
		}

		// Prepare the structures for updating so that all id mappings are set
		$structures = array_map(array($this, '_prepareStructure'), $structures);

		// All id mappings are set up so start updating everything now
		foreach($structures as $data) {
			$this->_updateStructure($data);
		}

		SiteStructure::removeCacheFile($this->site_identifier);
		if(!isset($_SESSION[get_class($this)])) $_SESSION[get_class($this)] = array();
		$_SESSION[get_class($this)]['saved'] = true;
		self::trigger_redirect($GLOBALS['PHprefs']['url'].'/custom/brickwork/structure/?site_identifier='.$this->site_identifier.'&TID='.$this->tid);
	}

	/**
	 * Check a access right for the current user for a given string_id
	 *
	 * @param string $string_id
	 * @param string $right
	 * @return bool
	 */
	public function access($string_id, $right = "p")
	{
		return in_array($right, $this->getAccessRights($string_id));
	}

	/**
	 * Get the access rights for the current user for the given string_id
	 *
	 * @param string $string_id
	 * @return array Array as returned by PH#getAccessRights()
	 */
	public function getAccessRights($string_id)
	{
		if(!isset($this->_acces_rights[$string_id])) {
			global $PHSes;

			$tid = PhoundryTools::getTableIdByString($string_id);
			if(false === $tid || $PHSes->isAdmin) {
				$this->_acces_rights[$string_id] = str_split("iudpcnxm");
			}
			else {
				$this->_acces_rights[$string_id] =
					PH::getAccessRights($tid, str_split("iudpcnxm"));
			}
		}
		return $this->_acces_rights[$string_id];
	}

	protected function _prepareStructure($data)
	{
		switch($data['state']) {
			case 'new':
				if($this->access("site_structure", "i")) {
					$structure = ActiveRecord::factory('Model_Site_Structure');
					// TODO: Take this out when the old site structure plugins aren't used anymore
					$structure['stype'] = 'page';
					$structure->save();
					$this->_all_structures[$structure['id']] = $structure;
				}
			break;
			case 'deleted':
				if(isset($this->_all_structures[$data['id']])) {
					$structure = $this->_all_structures[$data['id']];

					if($this->access("site_structure", "d")) {
						$structure->delete();
					}
					else {
						$data['state'] = "current";
					}
				}
			break;
			case 'copy':
				if($this->access("site_structure", "c")) {
					$copy_id = explode('_', $data['id']);
					// Grab it from the DB because it can be in a different site
					// so it might not be in the $_all_structures
					$structure = ActiveRecord::factory('Model_Site_Structure', Utility::arrayValue($copy_id, 1));
					if($structure->isLoaded()) {
						$structure = $structure->copy($this->site_identifier, false);
						$this->_all_structures[$structure['id']] = $structure;
					}
				}
			break;
			case 'current':
				if(isset($this->_all_structures[$data['id']])) {
					$structure = $this->_all_structures[$data['id']];
				}
			break;
		}

		if(isset($structure) && $structure->isLoaded()) {
			$this->_id_mapping[$data['id']] = $structure['id'];
		}
		else {
			$parent_id = Utility::arrayValue($data, 'parent_id');
			if(!is_null($parent_id)) $parent_id = Utility::arrayValue($this->_id_mapping, $parent_id);
			$this->_id_mapping[$data['id']] = $parent_id;
		}

		if(!empty($data['children']) && is_array($data['children'])) {
			$data['children'] =
				array_map(array($this, '_prepareStructure'), $data['children']);
		}

		return $data;
	}

	protected function _updateStructure($data)
	{
		// Skip deleted items, as they are now referring to their parent
		if('deleted' != $data['state']) {
			if(array_key_exists($this->_id_mapping[$data['id']], $this->_all_structures)) {
				/* @var $structure Model_Site_Structure */
				$structure = $this->_all_structures[$this->_id_mapping[$data['id']]];
			}

			if(isset($structure) && $structure->isLoaded())
			{
				$parent_id = Utility::arrayValue($data, 'parent_id');
				if(!is_null($parent_id)) $parent_id = Utility::arrayValue($this->_id_mapping, $parent_id);

				$structure['site_identifier'] = $this->site_identifier;
				$structure['parent_id'] = $parent_id;
				$structure['name'] = $data['name'];
				$structure['prio'] = ++$this->_prio;
				$structure['show_menu'] = (int) !empty($data['show_menu']);
				$structure['show_sitemap'] = (int) !empty($data['show_sitemap']);
				$structure['secure'] = (int) !empty($data['secure']);
				$structure['linked_to'] = !empty($data['linked_to']) ? Utility::arrayValue($this->_id_mapping, $data['linked_to']) : null;
				$structure['ssl'] = (int) !empty($data['ssl']);
				if(Features::brickwork('ACL')) $structure['restricted'] = (int) !empty($data['restricted']);

				// It can be that the values didnt change so save would return FALSE
				// so we're not checking its return value here
				$structure->save();
				$structure->updatePath();
				$structure->updateAcl();
				$structure->updateTranslations($data['translations']);
			}
		}

		if(!empty($data['children']) && is_array($data['children']))
		{
			foreach($data['children'] as $child)
			{
				$this->_updateStructure($child);
			}
		}
	}

	function json_encode($val)
	{
		global $PHprefs;
		if ($PHprefs['charset'] === 'iso-8859-1') {
			$val = utf8_encode($val);
		}
		return json_encode($val);
	}

    function trigger_redirect($url, $status = 302) {
        $status = (int) $status;
        switch($status) {
            case 301: // echt verplaatst
                $msg = 'Moved Permanently';
                break;
            case 302: // eerste url MOET in de toekomst nog steeds gebruikt worden
                $msg = 'Found';
                break;
            case 303: // see other, niet ondersteunt voor 1.1
                $msg = 'See other';
                break;
            case 307:
                $msg = 'Temporary Redirect';
                break;
            default:
                throw new InvalidArgumentException('Status should be either '.
                    '301, 302, 303 or 307. '.$status.' given.');
        }
        header('HTTP/1.1 '.$status.' '.$msg, true, $status);
        header('URI: '.$url);
        header('Location: '.$url);
        print $msg.":\n".$url."\n";
        exit;
    }
}
