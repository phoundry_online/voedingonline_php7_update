<script type="text/javascript">
//<![CDATA[
(function($){
var siteStructure = this.siteStructure = {

	/**
	 * The initialized jsTree
	 * @var jsTree
	 */
	tree : {},

	/**
	 * The rights for the current user to the various table interfaces
	 * @var object
	 */
	access_rights : {},
	
	/**
	 * Whole sitestructure as multidimensional array
	 * @var array
	 */
	structures : [],

	/**
	 * The current selected structure
	 * @var object|bool
	 */
	activestructure : false,

	/**
	 * Whole site structure serialized as it currently is in the db
	 * @var string
	 */
	initial_structure : '',

	/**
	 * Counter to determine the next new structure id
	 * @var int
	 */
	new_structure_counter : 0,

	/**
	 * Callbacks
	 * @var object
	 */
	callbacks : {},

	/**
	 * Counter to make unique callbacks
	 * @var int
	 */
	cbcounter : 0,

	/**
	 * Did we just save the structure succesfully
	 * @var bool
	 */
	saved : false,

	/**
	 * Build the jsTree or when there is already one delete it and rebuilt it
	 * @return jsTree the newly created tree
	 */
	rebuild : function() {
		var selected = [];
		if(window.location.hash) {
			selected = ['ss_'+window.location.hash.substr(1)];
		}

		var json = {
			'attributes' : {'id' : 'Structuur', 'rel' : 'MainStructure', 'class' : 'MainStructure'},
			'data'		 : 'Structuur',
			'state'		 : 'open',
			'children'	 : this.structuresToJsTree(siteStructure.structures)
		};

		this.tree = $.tree_create();
		this.tree.init(
			$('#structure_tree').empty(),
			{
				path	: '/brickwork/pics/tree_component/',
				data : {
					type : 'json',
					json : json
				},
				ui : {
					dots : true,
					animation : 0,
					hover_mode : true,
					theme_path : '/brickwork/pics/tree_component/',
					theme_name : 'default',
					context : [
						// Context menu
					]
				},
				lang : {
					new_node	: (<?=$this->json_encode($this->word(11602 /* Nieuwe pagina */))?>),
					loading		: (<?=$this->json_encode($this->word(11603 /* Laden... */))?>)
				},
				rules : {
					multiple	: false,
					type_attr	: 'rel',
					createat	: 'bottom',
					draggable	: [],
					clickable	: ['MainStructure', 'SiteStructure'],
					renameable	: this.access("site_structure", "u") ?
						['SiteStructure'] : [],
					deletable	: this.access("site_structure", "d") ?
						['SiteStructure'] : [],
					dragrules	: ['!* before MainStructure', '!* after MainStructure', '* * *']
				},
				callback : {
					onchange	: this.onchangeTree,
					onrename	: this.onrenameTree,
					onmove		: this.onmoveTree,
					oncreate	: this.oncreateTree,
					ondblclk	: this.ondblclk
				},
				selected : selected
			}
		);

		$.each(siteStructure.structures, function(){ siteStructure.refresh(this); });
		return this.tree;
	},

	/**
	 * Function used by jsTree
	 */
	onchangeTree: function(node, tree_obj)
	{
		$('#structure_tree').triggerHandler('change', node);
		if($(node).is('[id^=ss_][rel=SiteStructure]')) {
			siteStructure.selectStructure(node);
		}
		else
		{
			siteStructure.selectStructure(false);
		}
		$(document).focus();
	},

	ondblclk: function(node, tree_obj)
	{
		var structure = siteStructure.getStructure(node);
		if(structure) {
			if(structure.pages.length === 1) {
				siteStructure.pageContents(structure.pages[0].id);
			}
			else if(structure.urls.length === 1) {
				siteStructure.editUrl(structure.urls[0].id, structure);
			}
			else if(structure.pages.length === 0 && structure.urls.length === 0) {
				siteStructure.addPage(structure);
			}
		}
	},
	
	/**
	 * Function used by jsTree
	 */
	onrenameTree: function(node, tree_obj)
	{
		if($(node).is('[id^=ss_][rel=SiteStructure]')) {
			var ss_id = siteStructure.getId(node),
			structure = siteStructure.getStructure(ss_id);
			if(false !== structure)
			{
				structure.name = $(node).children('a').text();
				siteStructure.refresh(structure);
			}
		}
		$('#structure_tree').triggerHandler('rename', node);
	},
	
	/**
	 * Function used by jsTree
	 */
	onmoveTree: function(node, refnode, type, tree, rb)
	{
		node = $(node);
		refnode = $(refnode);

		var structure = siteStructure.getStructure(node),
		ref = siteStructure.getStructure(refnode);
		switch(type)
		{
			case 'after':
				var parent = siteStructure.getStructure(refnode.parents('li:first'));
				siteStructure.repositionStructure(structure, parent, ref, 'after');
			break;
			case 'before':
				var parent = siteStructure.getStructure(refnode.parents('li:first'));
				siteStructure.repositionStructure(structure, parent, ref, 'before');
			break;
			case 'inside':
				siteStructure.repositionStructure(structure, ref);
			break;
			default:
				throw 'Unknown type.. '+type;
		}
		$('#structure_tree').triggerHandler('move', node[0]);
	},

	oncreateTree: function(node, refnode, type, tree, rb)
	{
		$('#structure_tree').triggerHandler('create', node);
	},

	/**
	 * Move a structure in the array structure
	 */
	repositionStructure : function(structure, parent, refnode, position)
	{
		if(structure)
		{
			var container, index;
			container = this.getContainingArray(structure);
			index = $.inArray(structure, container);
			if(index !== -1) container.splice(index, 1);

			container = parent ? parent.children : this.structures;
			if(refnode)
			{
				index = (position == 'after') ? $.inArray(refnode, container)+1 : $.inArray(refnode, container);
			}
			else
			{
				index = container.length;
			}
			container.splice(index, 0, structure);
		}
	},

	/**
	 * Transform the structure format to the jsTree format
	 * @param array {structures}
	 * @return array
	 */
	structuresToJsTree : function(structures) {
		var self = this, children = [];
		$.each(structures, function(i, structure){
			children.push({
				attributes	: {
					'id' : 'ss_'+structure.id,
					'rel' : 'SiteStructure'
				},
				data		: {
					title : structure.name
				},
				state		: (structure.children.length ? 'open' : ''),
				children	: self.structuresToJsTree(structure.children)
			});
		});
		return children;
	},

	/**
	 * Generate path to the icon corresponding to the structure
	 * @param object {structure}
	 * @return string
	 */
	getIcon : function(structure)
	{
		var icon = '<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/';
		
		if(structure.state == 'deleted')
		{
			icon += 'page_d';
		}
		else if(structure.state == 'new')
		{
			icon += 'page_n';
		}
		else
		{
			icon += (structure.urls.length > structure.pages.length) ? 'www' : 'page';
			if(structure.show_menu) icon += '_m';
			if(structure.show_sitemap) icon += '_s';
			if(!(structure.urls.length > structure.pages.length) && structure.secure) icon += '_x';
		}
		icon += '.gif';
		return icon;
	},

	/**
	 * Refresh a structure item's representation in the tree
	 */
	refresh : function(structure, parent)
	{
		var node = this.getNode(structure),
		pages = structure.pages && structure.pages.length ? structure.pages.length : 0,
		urls = structure.urls && structure.urls.length ? structure.urls.length : 0,
		color;
		
		if(0 == pages && 0 == urls && structure.state != 'copy' && structure.state != 'new' && !structure.linked_to)
		{
			color = 'red';
		}
		else if(structure.state == 'copy' || structure.state == 'new')
		{
			color = '#FFD045';
		}
		else
		{
			color = '#000000';
		}
		node.children('a').css({
			'color' : color,
			'background-image' : 'url('+this.getIcon(structure)+')'
		});

		if(structure.id == siteStructure.activestructure.id) {
			siteStructure.selectStructure(siteStructure.activestructure);
		}

		if(structure.children && structure.children.length)
		{
			$.each(structure.children, function(i, child){
				siteStructure.refresh(child, structure);
			});
		}
	},

	/**
	 * Select a structure to display in the right pane
	 * @param int {id}
	 */
	selectStructure : function(structure)
	{
		structure = this.getStructure(structure);
		if(false !== structure)
		{
			$('#structure_pane').tmpl('template_structure', {structure : structure, siteStructure : siteStructure});
			window.location.hash = '#'+structure.id;
			this.activestructure = structure;
		}
		else
		{
			$('#structure_pane').html('');
			window.location.hash = '#';
			this.activestructure = false;
		}
	},

	/**
	 * Delete the given structure or mark it for deletion
	 * @param objec		{structure}
	 * @param string	{status}
	 */
	deleteStructure : function(structure, recursive)
	{
		if(!this.access("site_structure", "d")) {
			return;
		}
			
		if(structure === undefined) {
			var node;
			if(siteStructure.tree.hovered) {
				node = siteStructure.tree.hovered;
			}
			else if(siteStructure.tree.selected_arr) {
				$.each(siteStructure.tree.selected_arr, function(){
					siteStructure.deleteStructure(siteStructure.getStructure(this), recursive);
				});
				return;
			}
			else {
				node = siteStructure.tree.selected;
			}

			if(node.length) {
				structure = siteStructure.getStructure(node);
			}
		}
		else {
			var node = siteStructure.getNode(structure);
		}

		if(recursive === undefined && structure.children && structure.children.length != 0) {
			recursive = confirm(<?=$this->json_encode($this->word(11606 /* Ook alle onderliggende structuur elementen? */ ))?>);
		}

		var status = 'delete';
		if(structure.state == 'deleted') {
			status = 'undelete';
		}
		
		siteStructure._deleteStructure(structure, status, recursive);
		siteStructure.refresh(structure);
		$('#structure_tree').triggerHandler('delete', node);
	},

	_deleteStructure : function(structure, status, recursive)
	{
		if(structure && status)
		{
			if(recursive && structure.children && structure.children.length) {
				for(var i = structure.children.length-1; i >= 0; i--) {
					siteStructure._deleteStructure(structure.children[i], status, true);
				};
			}
			
			switch(structure.state) {
				case 'current':
				case 'deleted':
					if(status == 'delete') {
						structure.state = 'deleted';
					}
					else if(status == 'undelete') {
						structure.state = 'current';
					}
				break;
				case 'copy':
				case 'new':
					if(status == 'delete') {
						// Move all underlying structures that still exist to the parent
						if(recursive && structure.children && structure.children.length) {
							$.each(structure.children, function(){
								siteStructure.tree.moved(
									siteStructure.getNode(this),
									siteStructure.getNode(structure).parents('li:first').children('a'),
									'inside'
								);
							});
						}

						var container = siteStructure.getContainingArray(structure);
						if(container) {
							var index = $.inArray(structure, container);
							if(-1 !== index) {
								container.splice(index, 1);
							}
						}

						siteStructure.tree.remove(siteStructure.getNode(structure));
					}
				break;
			}
		}
	},

	/**
	 * Rename the given structure
	 */
	renameStructure : function(structure)
	{
		if(!this.access("site_structure", "u")) {
			return;
		}
		
		var node;
		if(typeof(structure) == 'undefined')
		{
			node = (siteStructure.tree.hovered ? siteStructure.tree.hovered : siteStructure.tree.selected);
		}
		else
		{
			node = siteStructure.getNode(structure);
		}
		if(node.length == 1) siteStructure.tree.rename(node);
	},

	/**
	 * Create a new structure in the given parent
	 * @param object {parent}
	 */
	createStructure : function(node)
	{
		if(!this.access("site_structure", "i")) {
			return;
		}
		
		if(!node) node = (siteStructure.tree.hovered ? siteStructure.tree.hovered : siteStructure.tree.selected);

		var structure = {
			state : 'new',
			id : 'new_'+(++siteStructure.new_structure_counter),
			site_identifier : '<?=$site_identifier?>',
			name : (<?=$this->json_encode($this->word(11602 /* Nieuwe pagina */))?>),
			show_menu : 0,
			show_sitemap : 0,
			secure : 0,
			restricted : 0,
			translations : {},
			children : []
		};

		var parent = siteStructure.getStructure(node);

		if(!parent)
		{
			siteStructure.structures.push(structure);
			node = $('#Structuur');
		}
		else
		{
			parent.children.push(structure);
		}

		var json = siteStructure.structuresToJsTree([structure]);
		siteStructure.tree.create(json[0], node);
		siteStructure.refresh(structure);
		siteStructure.tree.select_branch(siteStructure.getNode(structure));
		siteStructure.renameStructure(structure);
	},

	/**
	 * Move a structure in a given direction in the tree
	 */
	moveStructure : function(direction, structure)
	{
		if(!this.access("site_structure", "u")) {
			return;
		}
		
		if(!structure) structure = (this.tree.hovered ? this.tree.hovered : this.tree.selected);

		switch (direction)
		{
			case 'up':
				var prev = structure.prev('li');
				if(prev.length) this.tree.moved(structure, prev.children('a'), 'before');
			break;
			case 'down':
				var next = structure.next('li');
				if(next.length) this.tree.moved(structure, next.children('a'), 'after');
			break;
			case 'left':
				var parent = structure.parents('li:first:not(#Structuur)');
				if(parent.length) this.tree.moved(structure, parent.children('a'), 'after');
			break;
			case 'right':
				var prev = structure.prev('li');
				if(prev.length) this.tree.moved(structure, prev.children('a'), 'inside');
			break;
			default:
				throw 'Invalid direction "'+direction+'" given!';
		}
	},


	customProperties : function(structure)
	{
		if(!structure) structure = siteStructure.activestructure;
		if(structure)
		{
			var cb_nr = 'cb_'+(this.cbcounter++),
			url = '<?=$PHprefs['url']?>/core/'+(structure.custom_rid ? 'update':'insert')+'.php'+
			'?site_identifier=<?=$site_identifier?>'+
			'&TID=<?=PhoundryTools::getTableTID('brickwork_custom_site_structure')?>'+
			'&RID='+(structure.custom_rid ? structure.custom_rid : '')+
			'&popup=close'+
			'&callback=parent.siteStructure.callbacks.'+cb_nr+
			'&_custom_id='+structure.id;
			this.callbacks[cb_nr] = function(key){
				if(!structure.custom_rid)
				{
					structure.custom_rid = key;
				}
			};
			$.fn.InlinePopup({
				width: -20,
				height: -20,
				title: (<?=$this->json_encode($this->word(11609 /* Extra eigenschappen */ ))?>),
				modal: true,
				event: null,
				html:null,
				url: url
			});
		}
	},

	structureRoles : function(structure)
	{
		if(!structure) structure = siteStructure.activestructure;
		if(structure)
		{
			var cb_nr = 'cb_'+(this.cbcounter++),
			url = '<?=$PHprefs['url']?>/core/update.php'+
			'?site_identifier=<?=$site_identifier?>'+
			'&TID=<?=PhoundryTools::getTableTID('brickwork_site_structure')?>'+
			'&RID='+structure.id+
			'&popup=close'+
			'&callback=parent.siteStructure.callbacks.'+cb_nr;
			this.callbacks[cb_nr] = function() {
				delete siteStructure.callbacks[cb_nr];
			};
			$.fn.InlinePopup({
				width: -20,
				height: -20,
				title: (<?=$this->json_encode($this->word(11609 /* Extra eigenschappen */ ))?>),
				modal: true,
				event: null,
				html:null,
				url: url
			});
		}
	},

	/**
	 * Update the pages for a structure
	 */
	getPages : function(structure, cb)
	{
		if(!structure) structure = siteStructure.activestructure;
		if(structure)
		{
			$.ajax({
				type: "GET",
				url: 'index.php/getPages/'+structure.id+'?site_identifier=<?=$site_identifier?>&TID=<?=$this->tid?>',
				dataType: "json",
				success : function(data) {
					if(cb && $.isFunction(cb))
					{
						cb(data);
					}
					else
					{
						structure.pages = data;
						siteStructure.refresh(structure);
						if(structure == siteStructure.activestructure) siteStructure.selectStructure(structure);
					}
				}
			});
		}
	},

	/**
	 * Add a new page to the given structure
	 */
	addPage : function(structure)
	{
		if(!structure) structure = siteStructure.activestructure;
		if(structure)
		{
			var cb_nr = 'cb_'+(this.cbcounter++),
			url = '<?=$PHprefs['url']?>/core/insert.php'+
			'?site_identifier=<?=$site_identifier?>'+
			'&TID=<?=PhoundryTools::getTableTID('brickwork_page')?>'+
			'&popup=close'+
			'&callback=parent.siteStructure.callbacks.'+cb_nr+
			'&site_structure_id='+structure.id;
			this.callbacks[cb_nr] = function(){
				siteStructure.getPages(structure);
			};
			$.fn.InlinePopup({
				width: -20,
				height: -20,
				title: (<?=$this->json_encode($this->word(11613 /* Pagina Toevoegen */))?>),
				modal: true,
				event: null,
				html:null,
				url: url
			});
		}
	},

	/**
	 * View the given page
	 */
	viewPage : function(page_id, structure)
	{
		var page;
		
		if(page_id) {
			if(!structure) {
				structure = siteStructure.activestructure;
			}
			if(structure && structure.pages) {
				$.each(structure.pages, function(i, p){
					if(p.id && p.id === page_id) {
						page = p;
						return false;
					}
				});

				if(page && page.preview_url) {
					window.open(page.preview_url);
				}
			}
		}
	},

	/**
	 * Edit the given page
	 */
	editPage : function(page_id, structure)
	{
		if(page_id)
		{
			if(!structure) structure = siteStructure.activestructure;
			if(structure)
			{
				var cb_nr = 'cb_'+(this.cbcounter++),
				url = '<?=$PHprefs['url']?>/core/update.php'+
				'?site_identifier=<?=$site_identifier?>'+
				'&TID=<?=PhoundryTools::getTableTID('brickwork_page')?>'+
				'&popup=close&'+
				'&callback=parent.siteStructure.callbacks.'+cb_nr+
				'&RID='+page_id+
				'&site_structure_id='+structure.id;
				this.callbacks[cb_nr] = function(){
					siteStructure.getPages(structure);
				};
				$.fn.InlinePopup({
					width: -20,
					height: -20,
					title: (<?=$this->json_encode($this->word(11614 /* Pagina Aanpassen */))?>),
					modal: true,
					event: null,
					html:null,
					url: url
				});
			}
		}
	},

	/**
	 * copy the given page
	 */
	copyPage : function(page_id, structure)
	{
		if(page_id) {
			if(!structure) {
				structure = siteStructure.activestructure;
			}
			if(structure) {
				var cb_nr = 'cb_'+(this.cbcounter++),
				url = '<?=$PHprefs['url']?>/core/copy.php'+
				'?site_identifier=<?=$site_identifier?>'+
				'&TID=<?=PhoundryTools::getTableTID('brickwork_page')?>'+
				'&popup=close&'+
				'&callback=parent.siteStructure.callbacks.'+cb_nr+
				'&RID='+page_id+
				'&site_structure_id='+structure.id;
				this.callbacks[cb_nr] = function(){
					siteStructure.getPages(structure);
				};
				$.fn.InlinePopup({
					width: -20,
					height: -20,
					title: (<?=$this->json_encode($this->word(11630 /* Pagina Kopiëren */))?>),
					modal: true,
					event: null,
					html:null,
					url: url
				});
			}
		}
	},

	/**
	 * Delete a page for a given structure
	 */
	deletePage : function(page_id, structure)
	{
		if(!this.access("brickwork_page", "d")) {
			return;
		}
		
		if(page_id)
		{
			if(!structure) structure = siteStructure.activestructure;
			if(structure)
			{
				$.ajax({
					type: "GET",
					url: 'index.php/deletePage/'+page_id+'?site_identifier=<?=$site_identifier?>&TID=<?=$this->tid?>',
					dataType: "json",
					success : function(data) {
						if(data === 0)
						{
							alert(<?=$this->json_encode($this->word(11612 /* Pagina kon niet worden verwijdert. */))?>);
						}
						else
						{
							structure.pages = data;
							siteStructure.refresh(structure);
							if(structure == siteStructure.activestructure) siteStructure.selectStructure(structure);
						}
					},
					error : function(){
						alert(<?=$this->json_encode($this->word(11612 /* Pagina kon niet worden verwijdert. */))?>);
					}
				});
			}
		}
	},

	/**
	 * Update the urls for a structure
	 */
	getUrls: function(structure, cb)
	{
		if(!structure) structure = siteStructure.activestructure;
		if(structure)
		{
			$.ajax({
				type: "GET",
				url: 'index.php/getUrls/'+structure.id+'?site_identifier=<?=$site_identifier?>&TID=<?=$this->tid?>',
				dataType: "json",
				success : function(data) {
					if(cb && $.isFunction(cb))
					{
						cb(data);
					}
					else
					{
						structure.urls = data;
						siteStructure.refresh(structure);
						if(structure == siteStructure.activestructure) siteStructure.selectStructure(structure);
					}
				}
			});
		}
	},

	/**
	 * Add a new url to the given structure
	 */
	addUrl : function(structure)
	{
		if(!structure) structure = siteStructure.activestructure;
		if(structure)
		{
			var cb_nr = 'cb_'+(this.cbcounter++),
			url = '<?=$PHprefs['url']?>/core/insert.php'+
			'?site_identifier=<?=$site_identifier?>'+
			'&TID=<?=PhoundryTools::getTableTID('brickwork_url')?>'+
			'&popup=close'+
			'&callback=parent.siteStructure.callbacks.'+cb_nr+
			'&site_structure_id='+structure.id;
			this.callbacks[cb_nr] = function(){
				siteStructure.getUrls(structure);
			};
			$.fn.InlinePopup({
				width: -20,
				height: -20,
				title: (<?=$this->json_encode($this->word(11616 /* Url Toevoegen */))?>),
				modal: true,
				event: null,
				html:null,
				url: url
			});
		}
	},

	/**
	 * View the given url
	 */
	viewUrl : function(url_id, structure)
	{
		if(url_id) {
			if(!structure) {
				structure = siteStructure.activestructure;
			}
			if(structure) {
				var cb_nr = 'cb_'+(this.cbcounter++),
				url = '<?=$PHprefs['url']?>/core/preview.php'+
				'?site_identifier=<?=$site_identifier?>'+
				'&TID=<?=PhoundryTools::getTableTID('brickwork_url')?>'+
				'&popup=close&'+
				'&callback=parent.siteStructure.callbacks.'+cb_nr+
				'&RID='+url_id+
				'&site_structure_id='+structure.id;
				this.callbacks[cb_nr] = function(){
					siteStructure.getUrls(structure);
				};
				$.fn.InlinePopup({
					width: -20,
					height: -20,
					title: (<?=$this->json_encode($this->word(9 /* Bekijk */))?>),
					modal: true,
					event: null,
					html:null,
					url: url
				});
			}
		}
	},

	/**
	 * Copy the given url
	 */
	copyUrl : function(url_id, structure)
	{
		if(url_id) {
			if(!structure) {
				structure = siteStructure.activestructure;
			}
			if(structure) {
				var cb_nr = 'cb_'+(this.cbcounter++),
				url = '<?=$PHprefs['url']?>/core/copy.php'+
				'?site_identifier=<?=$site_identifier?>'+
				'&TID=<?=PhoundryTools::getTableTID('brickwork_url')?>'+
				'&popup=close&'+
				'&callback=parent.siteStructure.callbacks.'+cb_nr+
				'&RID='+url_id+
				'&site_structure_id='+structure.id;
				this.callbacks[cb_nr] = function(){
					siteStructure.getUrls(structure);
				};
				$.fn.InlinePopup({
					width: -20,
					height: -20,
					title: (<?=$this->json_encode($this->word(109 /* Kopieer */))?>),
					modal: true,
					event: null,
					html:null,
					url: url
				});
			}
		}
	},

	/**
	 * Edit the given url
	 */
	editUrl : function(url_id, structure)
	{
		if(url_id)
		{
			if(!structure) structure = siteStructure.activestructure;
			if(structure)
			{
				var cb_nr = 'cb_'+(this.cbcounter++),
				url = '<?=$PHprefs['url']?>/core/update.php'+
				'?site_identifier=<?=$site_identifier?>'+
				'&TID=<?=PhoundryTools::getTableTID('brickwork_url')?>'+
				'&popup=close&'+
				'&callback=parent.siteStructure.callbacks.'+cb_nr+
				'&RID='+url_id+
				'&site_structure_id='+structure.id;
				this.callbacks[cb_nr] = function(){
					siteStructure.getUrls(structure);
				};
				$.fn.InlinePopup({
					width: -20,
					height: -20,
					title: (<?=$this->json_encode($this->word(11617 /* Url Aanpassen */))?>),
					modal: true,
					event: null,
					html:null,
					url: url
				});
			}
		}
	},

	/**
	 * Delete a url for a given structure
	 */
	deleteUrl : function(url_id, structure)
	{
		if(!this.access("brickwork_url", "d")) {
			return;
		}
		
		if(url_id)
		{
			if(!structure) structure = siteStructure.activestructure;
			if(structure)
			{
				$.ajax({
					type: "GET",
					url: 'index.php/deleteUrl/'+url_id+'?site_identifier=<?=$site_identifier?>&TID=<?=$this->tid?>',
					dataType: "json",
					success : function(data) {
						if(data === 0)
						{
							alert(<?=$this->json_encode($this->word(11615 /* Url kon niet worden verwijdert. */))?>);
						}
						else
						{
							structure.urls = data;
							siteStructure.refresh(structure);
							if(structure == siteStructure.activestructure) siteStructure.selectStructure(structure);
						}
					},
					error : function(){
						alert(<?=$this->json_encode($this->word(11615 /* Url kon niet worden verwijdert. */))?>);
					}
				});
			}
		}
	},

	/**
	 * Place a copy of the current slected structures in memory
	 */
	copyStructures : function(structures)
	{
		if(!this.access("site_structure", "c")) {
			return;
		}
		
		if(!structures)
		{
			var node = siteStructure.tree.hovered ? siteStructure.tree.hovered : siteStructure.tree.selected;
			structures = [];
			if(node.is('[rel=MainStructure]'))
			{
				node.find('> ul > li').each(function(){
					structures.push(siteStructure.getStructure($(this)));
				});
			}
			else
			{
				structures.push(siteStructure.getStructure(node));
			}
		}

		var structure, newstructures = [];
		for(var i = 0; i < structures.length; i++) {
			structure = $.extend(true, {}, structures[i]);
			newstructures.push(structure);
		}
		window.parent.frames['PHtopmenu'].siteStructure_copyStructures = newstructures;
	},

	/**
	 * Create a pastable structure from the copied structures
	 */
	morphToCopied : function(structure)
	{
		if (structure.state === 'new') {
			structure.id = 'new_'+(++siteStructure.new_structure_counter);
			structure.site_identifier = '<?=$site_identifier?>';
		} else if (structure.state === 'copy') {
			structure.id = 'copy'+(++siteStructure.new_structure_counter)+'_'+
				structure.id.replace(/^copy\d+_/, '');
		} else if (structure.state === 'deleted') {
			return false; // don't copy a structure marked for deletion
		} else {
			structure.id = 'copy'+(++siteStructure.new_structure_counter)+'_'+structure.id;
			structure.state = 'copy';
		}
		structure.pages = [];
		structure.urls = [];

		$.each(structure.children, function(i, child){
			siteStructure.morphToCopied(child);
		});

		return structure;
	},

	/**
	 * Paste the copied structures
	 * @param Node {parent} The node to paste the copies in
	 */
	pasteStructures : function(parent)
	{
		if(!this.access("site_structure", "c")) {
			return;
		}
			
		var node, children;
		if(!parent)
		{
			node = siteStructure.tree.hovered ? siteStructure.tree.hovered : siteStructure.tree.selected;
			if(!node || node.is('#Structuur'))
			{
				node = $('#Structuur');
				children = siteStructure.structures;
			}
			else
			{
				parent = siteStructure.getStructure(node);
				children = parent.children;
			}
		}
		else
		{
			node = siteStructure.getNode(parent);
			children = parent.children;
		}

		if(window.parent.frames['PHtopmenu'].siteStructure_copyStructures) {
			var json, structures = window.parent.frames['PHtopmenu'].siteStructure_copyStructures,
			structure, newstructures = [];
			for(var i = 0; i < structures.length; i++) {
				structure = $.extend(true, {}, structures[i]);
				structure = siteStructure.morphToCopied(structure);
				if (structure) {
					newstructures.push(structure);
				}
			}

			$.each(newstructures, function(i, structure){
				children.push(structure);
				json = siteStructure.structuresToJsTree([structure]);
				siteStructure.tree.create(json[0], node);
				siteStructure.refresh(structure);
			});
		}
	},

	/**
	 * Get the given node's or structures structure id
	 * @param DOMnode|object|string {obj}
	 * @return string|bool returns false if it has no id
	 */
	getId : function(obj)
	{
		if(typeof obj != 'undefined')
		{
			if(typeof(obj) == 'string') return obj;
			if(obj.state) return obj.id;
			obj = $(obj);
			if(obj.length)
			{
				var id = obj.attr('id');
				if(id)
				{
					return id.replace(/^ss_/i,'');
				}
			}
		}
		return false;
	},

	/**
	 * Get the DOMnode representing the given structure id
	 * @param DOMnode|object|string {id}
	 * @return jQuery
	 */
	getNode : function(obj)
	{
		if(obj instanceof jQuery) return obj;
		return $('#ss_'+this.getId(obj));
	},

	/**
	 * Get the structure from the structures array
	 * @param DOMnode|object|string {obj}
	 * @param array {structures}
	 * @return bool|object
	 */
	getStructure : function(obj, structures)
	{
		var found = false;
		if(typeof obj != 'undefined')
		{
			if(obj.state) return obj;
			var id = this.getId(obj);

			if(typeof(structures) == 'undefined') structures = this.structures;
			for (var i = 0; i < structures.length; i++)
			{
				if(structures[i].id == id)
				{
					found = structures[i];
					break;
				}

				if(structures[i].children && structures[i].children.length)
				{
					found = this.getStructure(id, structures[i].children);
					if(false !== found)
					{
							break;
					}
				}
			}
		}
		return found;
	},

	/**
	 * Get the array holding the given id
	 * @param DOMnode|object|string {id}
	 * @param array {structures}
	 * @return bool|array
	 */
	getContainingArray : function(id, structures)
	{
		id = siteStructure.getId(id);
		if(typeof(structures) == 'undefined') structures = siteStructure.structures;
		var found = false;
		for (var i = 0; i < structures.length; i++)
		{
			if(structures[i].id == id)
			{
				found = structures;
				break;
			}

			if(structures[i].children && structures[i].children.length)
			{
				found = this.getContainingArray(id, structures[i].children);
				if(false !== found)
				{
					break;
				}
			}
		}
		return found;
	},

	/**
	 * Toggle the ability to drag structure items around
	 * @param bool {toggle}
	 * @return bool
	 */
	draggable : function(toggle)
	{
		if(!this.access("site_structure", "u")) {
			return;
		}
			
		if(typeof(toggle) == 'undefined')
		{
			return 
				(this.tree && this.tree.settings && this.tree.settings.rules && this.tree.settings.rules.draggable) ?
				!!this.tree.settings.rules.draggable.length :
				false;
		}
		else if(toggle)
		{
			this.tree.settings.rules.draggable = ['SiteStructure'];
		}
		else
		{
			this.tree.settings.rules.draggable = [];
		}
	},

	/**
	 * Get the structures as a flat array
	 * @return Array
	 */
	toArray : function()
	{
		// Flat the tree and use the parent_id field instead.. since php#json_decode doesnt support more then 20 nesting
		function flatten(structures, parent_id, depth) {
			var flat = [];
			$.each(structures, function(i, structure){
				var copy = $.extend({}, structure);
				copy.parent_id = parent_id;
				copy.depth = depth;
				delete copy.children;
				delete copy.pages;
				delete copy.urls;
				delete copy.custom_rid;

				flat.push(copy);
				if(structure.children && structure.children.length)
				{
					flat = flat.concat(flatten(structure.children, copy.id, depth+1));
				}
			});
			return flat;
		};

		return flatten(siteStructure.structures, null, 0);
	},
	
	/**
	 * Serializes the current tree as 
	 * @return string
	 */
	serialize : function()
	{
		return JSON.stringify(siteStructure.toArray());
	},

	pageContents : function(page_id)
	{
		var url = (<?=json_encode(PhoundryTools::getPluginUrl("PLUGIN_page_contents"))?>);

		if (url) {
			url = '<?=$PHprefs['url']?>/'+url+'?site_identifier=<?=$site_identifier?>&TID=<?=PhoundryTools::getTableIdByString("PLUGIN_page_contents")?>&RID='+page_id+'&page_id='+page_id;
		}
		else {
			url = '<?=$PHprefs['url']?>/custom/brickwork/page_contents/index.php?site_identifier=<?=$site_identifier?>&TID=<?=PhoundryTools::getTableIdByString("PLUGIN_page_contents")?>&RID='+page_id;
		}
		window.location = url+'&returnPage='+escape(window.location);
		return false;
	},
	
	saveStructures : function()
	{
		window.parent.frames['PHtopmenu'].siteStructure_structures_<?=$site_identifier?> = siteStructure.structures;
		this.initial_structure = siteStructure.serialize();

		// Ajax doesn't like special characters thats why we submit using an ancient <form>
		$('<form action="<?=$PHprefs['url']?>/custom/brickwork/structure/index.php/saveStructures?site_identifier=<?=$site_identifier?>&amp;TID=<?=$this->tid?>" method="post"><input type="hidden" name="structures" value="" /></form>')
		.find('input').val(siteStructure.serialize()).end()
		.appendTo('body')
		.submit();
	},

	/**
	 * Checks if the user has a specified right to a table
	 * @param {String} string_id
	 * @param {String} right
	 * @return bool
	 */
	access : function(string_id, right)
	{
		return string_id in this.access_rights &&
			-1 !== $.inArray(right, this.access_rights[string_id]);
	},

	filter : function(keyword) {
		this.tree.search(keyword);
		var li = $('#structure_tree li');
		li.removeClass('contains-search');
		if (keyword) {
			$('#structure_tree').addClass('contains-search');
			li.filter(':has(a.search)').addClass('contains-search');
		} else {
			$('#structure_tree').removeClass('contains-search');
		}
	},

	setLock : function(lock, sync) {
		$.ajax({
			async : !sync,
			url : '<?=$PHprefs['url']?>/core/doLock.php?<?=QS(false, 'RID='.$site_identifier)?>&lock=' + (lock ? 1 : 0)
		});
	}
	
};

})(jQuery);
//]]>
</script>
