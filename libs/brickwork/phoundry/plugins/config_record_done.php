<?php
$inc = @include('PREFS.php');
if ($inc === false) {
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
include_once "{$PHprefs['distDir']}/core/include/common.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

if (!isset($_GET['site_identifier'], $_GET['TID'], $_GET['_hilite'])) {
	trigger_redirect($PHprefs['url'].'/layout/home.php');
}

$site_identifier = $_GET['site_identifier'];
$TID = (int) $_GET['TID'];
$RID = $_GET['_hilite'];

$db = DB::getDb();

// check if the table_id is a config table
$res = $db->iQuery(sprintf("
	SELECT code
	FROM brickwork_module
	WHERE ptid_config = %d
	LIMIT 1",
	$TID
));

if (count($res) === 0) {
	trigger_redirect($PHprefs['url'].'/layout/home.php');
}

// find the page that contains the page_content_id
$res = $db->iQuery(sprintf("
	SELECT page_id
	FROM brickwork_page_content
	WHERE id = %d
	LIMIT 1
	",
	$RID
));

if (count($res) === 0) {
	trigger_redirect($PHprefs['url'].'/layout/home.php');
}

$page_id = $res->first()->page_id;

$url = PhoundryTools::getPluginUrl("PLUGIN_page_contents");

if ($url) {
	$url = $PHprefs['url'].$url.'?site_identifier='.$site_identifier.
		'&TID='.PhoundryTools::getTableIdByString("PLUGIN_page_contents").
		'&RID='.$page_id.'&page_id='.$page_id;
}
else {
	$url = $PHprefs['url'].'/custom/brickwork/page_contents/index.php?site_identifier='.
		$site_identifier.
		'&TID='.PhoundryTools::getTableIdByString("PLUGIN_page_contents").
		'&RID='.$page_id;
}

// redirect to the page that contains the just edited module
trigger_redirect($url);
