<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	checkAccess();
	$logos = array('B'=>'bronze', 'S'=>'silver', 'G'=>'gold');
?>
<?= '<?xml version="1.0" encoding="' . $PHprefs['charset'] . '"?>' . "\n" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Phoundry</title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?=$PHprefs['url'];?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

function init() {
	try {_P.frames['PHmenu'].setGroupSelect(true)} catch(e) {}
<?php
   if (isset($_GET['_msg']) && !isset($_GET['groupId']))
   	print "alert('" . escSquote($_GET['_msg']) . "');";
?>
}

//]]>
</script>
</head>
<body class="frames" onload="init()">
<div class="headerFrame">
<?= getHeader($PRODUCT['version'], 'Brickwork edition',$PRODUCT['date']); ?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->

<br /><br />
<p align="center">
<img src="<?=$PHprefs['url'];?>/core/pics/<?= $logos[$PHprefs['license']] ?>.gif" alt="Phoundry Logo" width="428" height="116" />
</p>

<?php if (!empty($PHprefs['showNews'])) { ?>
<center>
<fieldset style="width:420px"><legend><b><?= word(184) ?></b></legend>
<iframe src="http://www.phoundry.nl/phoundry_news.php?v=<?= urlencode($g_phoundryVersion) ?>&amp;u=<?= urlencode($_SERVER['HTTP_HOST']) ?>&amp;lang=<?= $PHSes->lang ?>&amp;license=<?= urlencode($PHprefs['license']) ?>" width="416" height="200" frameborder="0" allowtransparency="true"></iframe>
</fieldset>
</center>
<?php } ?>

<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	&copy; 2000 - <?= date('Y') ?> <a href="http://www.webpower.nl" target="_blank">Web Power</a>
	</td>
	<td align="right">
	<img src="<?=$PHprefs['url'];?>/core/pics/w3c-xhtml.gif" alt="Valid XHTML" width="80" height="15" />
	<img src="<?=$PHprefs['url'];?>/core/pics/w3c-css.gif" alt="Valid CSS" width="80" height="15"/>
	</td>
</tr></table>
</div>

</body>
</html>
