<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	include_once "{$PHprefs['distDir']}/core/include/common.php";
	include_once "{$PHprefs['distDir']}/core/include/menu.php";

	checkAccess();


/**
 * Read the Phoundry-serviceMenu into a datastructure.
 *
 * @author Arjan Haverkamp
 * @return An array of PhoundryMenuItems, representing the Service menu.
 * @returns array
 * @private
 */
function custom_readServiceMenu($level = -2) {
   global $db, $PHprefs, $PHSes;
   $menu = array();

	$sql = "SELECT id, description FROM phoundry_table WHERE is_service = {$level} ORDER BY order_by";

   $cur = $db->Query($sql)
      or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

   for ($x = 0; !$db->EndOfResult($cur); $x++) {
      $table_id = $db->FetchResult($cur,$x,'id');
      $menu[] = new PhoundryMenuItem(langWord($db->FetchResult($cur,$x,'description')), $table_id, 1, getPhoundryMenuUrl($table_id));
   }
   return $menu;
}


	/*************************** Add/Delete shortcut **************************/

	if (isset($_GET['_addShortcut']) || isset($_GET['_delShortcut'])) {
		// Add or remove shortcut:
		if (isset($_GET['_addShortcut'])) {
			$sql = "INSERT INTO phoundry_user_shortcut VALUES({$PHSes->userId}, " . (int)$_GET['_addShortcut'] . ")";
		}
		elseif (isset($_GET['_delShortcut'])) {
			$sql = "DELETE FROM phoundry_user_shortcut WHERE user_id = {$PHSes->userId} AND table_id = " . (int)$_GET['_delShortcut'];
		}
		$res = $db->Query($sql);
		// Do not catch error: shortcut might already be there!
	}

	/************************** /Add/Delete shortcut **************************/

	$top = count($PHSes->groups) > 1 ? 44 : 22;
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Phoundry menu</title>
<link rel="stylesheet" type="text/css" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" />
<style type="text/css">
<!--

body {
	background: #f0f0f0; 
	margin: 0px;
	padding: 0px;
}

select {
	font: Icon;
}

div.headerDiv {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: <?= $top ?>px;
	z-index: 2;
}

div.contentDiv {
	position: absolute;
	width: 100%;
	overflow-y: auto;
	overflow-x: hidden;
	top: <?= $top ?>px;
	bottom: 24px;
	z-index: 1;
}

* html div.contentDiv {
	left: 0;
	top: 0;
	right: 0;
	bottom: 0;
	height: 100%;
	border-bottom: 24px solid #f0f0f0;
	border-top: <?= $top ?>px solid #f0f0f0;
}

div.footerDiv {
	position: absolute;
	bottom: 0;
	width: 150px;
	height: 24px;
	text-align: center;
	z-index: 2;
}
-->
</style>
<script type="text/javascript">
//<![CDATA[

var imgpath = '<?= $PHprefs['url'] ?>/core/pics/tree';
var _D = document, _P = parent;

function setGroup(groupId) {
	var href = _D.location.href;
	href = href.replace(/(&|\?)?setGroupId=(-)?[0-9]+/g, '');
	var sep = (href.indexOf('?') == -1) ? '?' : '&';
	_D.location.href = href + sep + 'setGroupId=' + groupId;
}

function setGroupSelect(state) {
	var el = _D.getElementById('groupSelect');
	if (el)
		el.disabled = !state;
}

function init() {
	if(_P.document.getElementById('mainFS').cols) {
		_P.document.getElementById('mainFS').cols = '215,*';
	}
}
<?php
	if (isset($_GET['setGroupId']))
		print "_P.frames['PHcontent'].location.href = '{$PHprefs['customUrls']['home']}';\n";
?>

//]]>
</script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/dtree.js"></script>
</head>
<body onload="init()">

<div class="headerDiv">
<?php
	// Groups-select-list:
	if (count($PHSes->groups) > 1) {
		print '<form style="margin:0px;background:buttonFace;border-bottom:2px groove">';
		print '<select id="groupSelect" style="background:#f0f0f0;width:100%" onchange="setGroup(this.options[this.selectedIndex].value)">';
		foreach($PHSes->groups AS $groupId=>$groupName) {
			$selected = ($PHSes->groupId == $groupId) ? ' selected="selected" ' : ' ';
			print '<option' . $selected . 'value="' . $groupId . '">' . $groupName . '</option>';
		}
		print '</select></form>';
	}

	$sites = array();
	$sql = "SELECT identifier, name, protocol, domain, COALESCE(lspu.site_identifier, 0) as access, template FROM brickwork_site s LEFT JOIN brickwork_site_phoundry_user lspu ON lspu.site_identifier = s.identifier AND lspu.phoundry_user_id = {$PHSes->userId} ORDER BY name ASC";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if(!$db->EndOfResult($cur)){
		$use_specific_access = FALSE;

		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$sites[$db->FetchResult($cur,$x,'identifier')] =array(
							'identifier'=>$db->FetchResult($cur,$x,'identifier'),
							'name'=>$db->FetchResult($cur,$x,'name'),
							'protocol'=>$db->FetchResult($cur,$x,'protocol'),
							'domain'=>$db->FetchResult($cur,$x,'domain'),
							'template'=>$db->FetchResult($cur,$x,'template'),
							'access'=>$db->FetchResult($cur,$x,'access')
						);

			$access = $db->FetchResult($cur,$x,'access');
		
			if(!empty($access)) {
				$use_specific_access = TRUE;
			}
		}

		if($use_specific_access){
			foreach($sites as $indentifier=>$site){
				if(empty($site['access'])){
					unset($sites[$indentifier]);
				}
			}
		}
	}

	if(count($sites)>1){
		print '<form action="'.$PHprefs['customUrls']['menu'].'" onsubmit="">';
		print '<select name="site_identifier" onchange="this.form.submit();_P.frames[\'PHcontent\'].location.href=\'' . $PHprefs['customUrls']['home'].'\'" style="width: 100%">';
		print '<option value="">-- kies een site --</option>';
		foreach($sites as $identifier=>$site){
			if($use_specific_access && empty($site['access'])){
				continue;
			}
			if(isset($_GET['site_identifier']) && $_GET['site_identifier'] == $identifier){
				$current_site = $site;
				$GLOBALS['current_site']=&$current_site;
				$selected_html = ' selected="selected"';
			} else {
				$selected_html = '';
			}
			print '<option value="'.$site['identifier'].'" '.$selected_html .'>'.$site['name']."</option>\n";
		}
		print '</select></form>';
	} else {
		$current_site = array_pop($sites);
		$GLOBALS['current_site']=&$current_site;
		$_REQUEST['site_identifier'] =	$_GET['site_identifier'] = $current_site['identifier'];
	}

	$rootName = 'Phoundry';
	if (preg_match('/([A-z0-9\-]+\.\w+)$/', $_SERVER['HTTP_HOST'], $reg))
		$rootName = $reg[1];
?>

</div>

<div class="contentDiv">


<script type="text/javascript">
//<![CDATA[

p = new dTree('p');
p.config.closeSameLevel = true;
p.config.target = 'PHcontent';
p.add(0,-1,'<b><?= $rootName ?></b>');

// Shortcuts
<?php
	$x = 1;
	$shortcuts = getShortcuts($PHSes->userId);
	foreach($shortcuts as $name=>$url)
		print "p.add(" . $x++ . ",0,'" . escSquote($name) . "','$url','','','','shortcut.gif');\n";


if(!empty($_GET['site_identifier'])){
?>
// Phoundry Menu
p.add(<?= $x ?>,0,'<b><?=escSquote($current_site['name']);?></b>','','','','','phoundry.gif','phoundry_open.gif');
<?=getJSmenuData($x,readPhoundryMenu($PHSes->userId, $PHSes->groupId));?>

<?
}
?>

// Service Menu
p.add(500,0,'<b>Service</b>','','','','','phoundry.gif','phoundry_open.gif');
<?= getJSmenuData(500, readServiceMenu()); ?>



<?php if ($PHSes->userId == 1) { ?>
	// Module configuraties
	p.add(2000,0,'<b>Modules</b>','','','','','phoundry.gif','phoundry_open.gif');
	<?= getJSmenuData(2000, custom_readServiceMenu(-2)); ?>

	// Module configuraties
	p.add(3000,0,'<b>Module Admin</b>','','','','','phoundry.gif','phoundry_open.gif');
	<?= getJSmenuData(3000, custom_readServiceMenu(-3)); ?>
<?php } ?>

<?php if ($PHSes->isAdmin && $PHSes->groupId == -1) { ?>
	// Admin Menu
	p.add(1000,0,'<b>Admin</b>','','','','','admin.gif','admin.gif');
	<?= getJSmenuData(1000, readAdminMenu()); ?>
<?php } ?>

_D.write(p);

//]]>
</script>
</div>

<div class="footerDiv">
<img src="<?= $PHprefs['url'] ?>/core/pics/webpower.gif" alt="Phoundry is a product by Web Power" width="125" height="18" />
</div>

</body>
</html>
