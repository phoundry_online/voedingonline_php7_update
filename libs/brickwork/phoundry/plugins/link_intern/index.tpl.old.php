<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Link intern</title>
<?php if(file_exists("{$PHprefs['distDir']}/core/Inputtypes/tiny_mce/tiny_mce_popup.js")) {?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/Inputtypes/tiny_mce/tiny_mce_popup.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/Inputtypes/tiny_mce/utils/mctabs.js"></script>
<?php } else {?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/tiny_mce/tiny_mce_popup.js"></script>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/tiny_mce/utils/mctabs.js"></script>
<?php }?>

<?php if(file_exists("{$PHprefs['distDir']}/core/csjs/jquery-1.3.2.min.js")) {?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery-1.3.2.min.js"></script>
<?php } else {?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<?php }?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.json.min.js"></script>
<script type="text/javascript" src="/brickwork/js/css.js"></script>
<script type="text/javascript" src="/brickwork/js/tree_component.js"></script>

<link rel="stylesheet" href="/brickwork/css/tree_component.css" type="text/css" />
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var tree = {}, active_ss = 0;
jQuery(function($){
	tree = $.tree_create();
	tree.init($('#structure_tree'), {
	    path  : '/brickwork/pics/tree_component/',
		ui		: {
			theme_path : '/brickwork/pics/tree_component/',
			theme_name : 'default',
			dots		: false,
			animation	: 0,
			context		: []
		},
		rules	: {
			type_attr	: 'rel',
			createat	: 'bottom',
			draggable	: [],
			clickable	: ['MainStructure', 'SiteStructure'],
			renameable	: [],
			deletable	: [],
			dragrules	: 'none'
		},
		callback :	{
			onselect	: function(node, tree_obj) {
				active_ss = $(node).is('[id^=ss_][rel=SiteStructure]') ? $(node).attr('id').replace(/^ss_/i, '') : '';
				return true;
			} 
		},
		data	: {
			type	: 'json',
			async	: false,
			json	: (<?=json_encode($tree_structure)?>)
		}
	});

	function refreshIcons(ul) {
		ul.find('li').each(function(e){
			var $this = $(this),
			icon = '<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/';

			icon += $this.hasClass('www') ? 'www' : 'page';
			if($this.hasClass('show_menu'))		icon += '_m';
			if($this.hasClass('show_sitemap'))	icon += '_s';
			if($this.hasClass('secure'))		icon += '_x';
			icon += '.gif';
			$this.children('a').css('background-image', 'url('+icon+')');
		});
	};

	refreshIcons($('.Structuur > ul'));
});
	function init() {
		tinyMCEPopup.resizeToInnerSize();
	}

	function setInternLink(structure, lang) {
		$.ajax({
			type	: 'GET',
			url		: 'get_link.php?site_identifier=<?=$site_identifier?>&structure='+structure+'&lang='+(lang?lang:''),
			cache	: false,
			success	: function(data){
				var input = tinyMCEPopup.getWindowArg('input');
				input.value = data;
				tinyMCEPopup.close();
			},
			error	: function(){
				alert('Error: Couldn\'t get url for specified structure item');
			}
		});
	}

	tinyMCEPopup.onInit.add(init);
//--><!]]>
</script>
<style type="text/css">
.tree .tree-default li.Structuur li a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/page.gif);
}

.tree .tree-default li.Structuur a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/base.gif);
}

li.test a {
	font-style: italic;
}
</style>
</head>
<body>
<form onsubmit="setInternLink(active_ss, $('select[name=\'lang\']').val());return false" action="#">
	<div class="tabs">
		<ul>
			<li id="link1_tab" class="current"><span><a href="javascript:mcTabs.displayTab('link1_tab','link1_panel');" onmousedown="return false;">Link intern</a></span></li>
		</ul>
	</div>

	<div class="panel_wrapper">
		<div id="link1_panel" class="panel current" style="height:385px">
			<fieldset>
			<legend><?=word(11751/* Kies structuur item */)?></legend>
				<div id="structure_tree" style="height:330px; overflow: auto;"></div>
			</fieldset>
			<?if(isset($site_languages) && count($site_languages) > 1):?>
			<fieldset>
			<legend><?=word(11752/* Taal */)?></legend>
			<select name="lang">
				<?foreach($site_languages as $lang):?>
				<option value="<?=$lang['id']?>"><?=$lang['code']?></option>
				<?endforeach?>
			</select>
			</fieldset>
			<?endif?>
		</div>
	</div>

	<div class="mceActionPanel">
		<div style="float: left">
			<input type="submit" id="insert" name="insert" value="{#insert}" />
		</div>

		<div style="float: right">
			<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" />
		</div>
	</div>
</form>
</body>
</html>