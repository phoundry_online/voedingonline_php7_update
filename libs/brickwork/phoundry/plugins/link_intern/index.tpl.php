<?php
$callback = 'parent.setUrl';
if (isset($_GET['callback'])) {
    if ($_SERVER['HTTPS'] == 'on')
        $_GET['callback'] = str_replace('http:', 'https:', $_GET['callback']);

    $callback = $_GET['callback'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Link intern</title>
<?php if(file_exists("{$PHprefs['distDir']}/core/csjs/jquery-1.3.2.min.js")) {?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery-1.3.2.min.js"></script>
<?php } else {?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<?php }?>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/jquery.json.min.js"></script>
<script type="text/javascript" src="/brickwork/js/css.js"></script>
<script type="text/javascript" src="/brickwork/js/tree_component.js"></script>

<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/layout/css/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/layout/css/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/layout/css/form.css" type="text/css" />
<link rel="stylesheet" href="/brickwork/css/tree_component.css" type="text/css" />
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var tree = {}, active_ss = 0;
jQuery(function($){
	tree = $.tree_create();
	tree.init($('#structure_tree'), {
	    path  : '/brickwork/pics/tree_component/',
		ui		: {
			theme_path : '/brickwork/pics/tree_component/',
			theme_name : 'default',
			dots		: false,
			animation	: 0,
			context		: []
		},
		rules	: {
			type_attr	: 'rel',
			createat	: 'bottom',
			draggable	: [],
			clickable	: ['MainStructure', 'SiteStructure'],
			renameable	: [],
			deletable	: [],
			dragrules	: 'none'
		},
		callback :	{
			onselect	: function(node, tree_obj) {
				active_ss = $(node).is('[id^=ss_][rel=SiteStructure]') ? $(node).attr('id').replace(/^ss_/i, '') : '';
				$('#insert').attr('disabled', !active_ss);
				return true;
			} 
		},
		data	: {
			type	: 'json',
			async	: false,
			json	: (<?=json_encode($tree_structure)?>)
		}
	});

	function refreshIcons(ul) {
		ul.find('li').each(function(e){
			var $this = $(this),
			icon = '<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/';

			icon += $this.hasClass('www') ? 'www' : 'page';
			if($this.hasClass('show_menu'))		icon += '_m';
			if($this.hasClass('show_sitemap'))	icon += '_s';
			if($this.hasClass('secure'))		icon += '_x';
			icon += '.gif';
			$this.children('a').css('background-image', 'url('+icon+')');
		});
	};

	refreshIcons($('.Structuur > ul'));
});
function setInternLink(structure, lang) {
	$.ajax({
		type	: 'GET',
		url		: 'get_link.php?site_identifier=<?=$site_identifier?>&structure='+structure+'&lang='+(lang?lang:''),
		cache	: false,
		success	: function(data){
			<?php echo $callback ?>(data);
		},
		error	: function(){
			alert('Error: Couldn\'t get url for specified structure item');
		}
	});
}
//--><!]]>
</script>
<style type="text/css">
.tree .tree-default li.Structuur li a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/page.gif);
}

.tree .tree-default li.Structuur a {
	background-image:url(<?=$PHprefs['url']?>/custom/brickwork/site_structure/pics/base.gif);
}

li.test a {
	font-style: italic;
}

.left {
	margin-right : 100px;
}

.right {
	float: right;
 	width: 90px;
	margin-right: 10px;
}

</style>
</head>
<body>
<form onsubmit="setInternLink(active_ss, $('select[name=\'lang\']').val());return false" action="#">
	<div class="right">
		<?if(isset($site_languages) && count($site_languages) > 1):?>
		<fieldset>
		<legend><?=word(11752/* Taal */)?></legend>
		<select name="lang">
			<?foreach($site_languages as $lang):?>
			<option value="<?=$lang['id']?>"><?=$lang['code']?></option>
			<?endforeach?>
		</select>
		</fieldset>
		<?endif?>
	
		<div>
			<input type="submit" id="insert" name="insert" value="<?=word(82)?>" disabled="disabled" />
		</div>
	</div>
	<div class="left">
		<fieldset>
		<legend><?=word(11751/* Kies structuur item */)?></legend>
			<div id="structure_tree" style="height:238px; overflow: auto;"></div>
		</fieldset>
	</div>
</form>
</body>
</html>