<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

$site_identifier = Utility::arrayValue($_GET, 'site_identifier');
Framework::loadSiteByIdentifier($site_identifier);
$structure = Utility::arrayValue($_GET, 'structure');
$lang = !empty($_GET['lang']) ? $_GET['lang'] : null;

echo SiteStructure::getFullPathByStructureId($structure, $site_identifier, $lang);