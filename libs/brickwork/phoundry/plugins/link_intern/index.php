<?php
$inc = @include_once('PREFS.php');
if ($inc === false) {
	require_once(getenv('CONFIG_DIR') . '/PREFS.php');
}
header("Content-type: text/html; charset={$PHprefs['charset']}");
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

$site_identifier = Utility::arrayValue($_GET, 'site_identifier');
$page_content_id = Utility::arrayValue($_GET, 'page_content_id');
DB::setCollation('utf8');

try{
	$site_languages = ActiveRecord::factory('Model_Site_Language')->where('site_identifier', $site_identifier)->get();
} catch (Exception $e) {}


$root_structures = ActiveRecord::factory('Model_Site_Structure')->
	where('parent_id', 'NULL', 'IS', false)->
	where('site_identifier', $site_identifier)->get();

$staging_status = 'live';
if($page_content_id) {
	$sql = sprintf('
		SELECT bp.staging_status FROM brickwork_page_content AS bpc
		INNER JOIN brickwork_page AS bp ON bp.id = bpc.page_id
		WHERE bpc.id = %d
		LIMIT 1
		', $page_content_id);
	if($row = DB::iQuery($sql)->first()) {
		$staging_status = $row->staging_status;
	}
}

function getChildren($children)
{
	global $HASACL, $site_identifier, $staging_status;
	
	$items = array();
	foreach($children as $child) {
		$livepages = false;
		foreach($child->pages as $page) {
			if($page['staging_status'] == 'live') {
				$livepages = true;
				break;
			}
		}
		/*
		if($staging_status == 'live' && !$livepages) {
			// Current page content item is on a live page
			// Current structure item does not have any live pages.
			continue;
		}
		//*/
		$classes = array();
		if (!$livepages) {
			$classes[] = count($child->urls) === 0 ? 'test' : 'www';
		}
		if($child['show_menu'])				$classes[] = 'show_menu';
		if($child['show_sitemap'])			$classes[] = 'show_sitemap';
		if($child['secure'])				$classes[] = 'secure';
		if($HASACL && $child['restricted'])	$classes[] = 'restricted';
		
		$items[] = array(
			'attributes'	=> array(
				'id'	=> 'ss_'.$child['id'],
				'rel'	=> 'SiteStructure',
				'class'	=> implode(' ', $classes),
			),
			'data'			=> $child['name'],
			'state'			=> (count($child->children) != 0) ? 'open' : '',
			'children'		=> getChildren($child->children)
		);
	}
	return $items;
}

$tree_structure = array(
	'attributes'	=> array('class' => 'Structuur', 'rel' => 'MainStructure'),
	'data'			=> word(11600 /* Structure */),
	'state'			=> 'open',
	'children'		=> getChildren($root_structures)
);


if (!file_exists("{$PHprefs['distDir']}/core/popups/media_browser") || !isset($_GET['type'])) {
	include 'index.tpl.old.php';
} else {
	include 'index.tpl.php';
}