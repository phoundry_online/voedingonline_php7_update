<?php
	header('content-type: application/x-javascript');
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	
	// load settings
	require_once '../settings.php';

?>

// Define some global vars.
var nodeInfo = null;
var curIndex = null;
var contextEl = null;
var contextTmr = null;

/**
 * The WPtree object
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 */
function WPtree() {
	this.tree = new Array();
}

/**
 * The WPtree Node object
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 */
function WPnode(name, dbId, state) {
	this.name = name;
	this.dbId = dbId;
	this.state = state;
	this.childs = new Array();
}

/**
 * Adds a node to the tree as childs of the curent node
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 */
var newNode;
function addNode() {
	if(nodeInfo === null) {alert('<?= word(11007) /* no item selected */ ?>');return false;}
	var info = nodeInfo.split(',');

	// make a string of the curent node.
	var thisNode = makeObjectString(info);

	// check if item is open
	var isOpen = thisNode +".state";
	isOpen = eval(isOpen);
	if(!isOpen) {alert('<?= word(11008) /* the node must be open */ ?>');return false;}

	newNode = thisNode +".childs.length";
	// Add the item
	thisNode +=".childs.push(new WPnode('New Folder',0,true))";
	eval(thisNode);

	// Re-build the tree on screen.
	tree = '';
	init();

	// Get new node rowIndex.
	newNode = eval(newNode);
	newNode = nodeInfo+','+(newNode-1);
	newNode = gE(newNode);
	newNode = newNode.rowIndex;

	// Focus the new item. (setTimeout needed for IE).
	setTimeout("selectNode(newNode, 'next')", 10);
	return true;
}

/**
 * Deletes a node to the tree as childs of the curent node
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 */
function deleteNode() {
	if(nodeInfo === null) {alert('<?= word(11007) /* No item selected */ ?>'); return false;}

	var info = nodeInfo.split(',');
	var infoCount = info.length;
	if(infoCount == 1 && info[0] == 0) {alert('<?= word(11009) /* The root cannot be deleted! */ ?>');return false;}

	// make a string of the curent node.
	var thisNode = makeObjectString(info);

	var nr_childs = eval(thisNode+'.childs.length');

	// If there are subitems, the node may not be deleted.
	// if we do not check this the whole node including subnodes will be deleted.
	if(nr_childs > 0) { alert('<?= word(11010) /* Only empty folders can be deleted! */ ?>'); return false; }

	// Remember the dbId.
	var DBid = eval(thisNode+'.dbId');
	if(DBid != 0) {
		// Add the id to the hidden field 'remove'
		F = _D.forms[0];
		F['remove'].value += DBid+',';
	}

	// Delete the node
	eval("delete "+thisNode);

	// Make a new string zo we can reset object indexes.
	delete info[infoCount-1];
	var resetString = makeObjectString(info)+'.childs';
	var childs = eval(resetString);
	eval("delete "+resetString);
	eval(resetString+' = new Array');
	var y = 0;
	for(x in childs) {
		eval(resetString+'[y] = childs[x]');
		y++;
	}

	// Re-build the tree on screen.
	tree = '';
	init();
	nodeInfo = null;

	// Focus on the item above the deleted item (setTimeout needed for IE).
	setTimeout("selectNode(curIndex-1, 'next')", 10);
	return true;
}

/**
 * Move a Node including subnodes one level up
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 */
function moveUp() {
	if(nodeInfo === null) {alert('<?= word(11007) /* no item selected */ ?>');	return false;}
	var info = nodeInfo.split(',');
	var infoCount = info.length;
	var menuTBL = _D.getElementById('menuTBL');

	// make a string of the curent node.
	var thisNode = makeObjectString(info);

	// make a string of the previous node.
	// Lower the last item of info so we get the previous node
	info[infoCount-1] = info[infoCount-1] - 1;
	var prevNode = makeObjectString(info);

	// Switch the items
	var tmp = eval(prevNode);
	// First item in node, we can not move this.
	if(typeof(tmp) == 'undefined') {alert('<?= word(11011) /* This item can not be moved up! */ ?>');return false;}
	eval(prevNode+' = '+thisNode);
	eval(thisNode+' = tmp');

	// Re-build the tree on screen.
	tree = '';
	init();

	// Focus moved item. (setTimeout needed for IE).
	info = info.join(',')
	curIndex = gE(info);
	curIndex = curIndex.rowIndex;
	setTimeout("selectNode(curIndex, 'next')", 10);
	return true;
}

/**
 * Move a Node including subnodes one level down
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 */
function moveDown() {
	var info = nodeInfo.split(',');
	var infoCount = info.length;

	// make a string of the curent node.
	var thisNode = makeObjectString(info);

	// make a string of the next node.
	// Upper the last item of info so we get the next node
	info[infoCount-1] = parseInt(info[infoCount-1]) + 1;
	var nextNode = makeObjectString(info);

	// Switch the items
	var tmp = new Array();
	tmp[0] = eval(thisNode);
	tmp[1] = eval(nextNode);
	// Last Item in node, we can not move this.
	if(typeof(tmp[1]) == 'undefined') {alert('<?= word(11012) /* This item can not be moved down! */ ?>');return false;}
	eval(nextNode+' = '+thisNode);
	eval(thisNode+' = tmp[1]');

	// Re-build the tree on screen.
	tree = '';
	init();

	// Focus moved item. (setTimeout needed for IE).
	info = info.join(',')
	curIndex = gE(info);
	curIndex = curIndex.rowIndex;
	setTimeout("selectNode(curIndex, 'next')", 10);
	return true;
}

/**
 * Make a string that we can eval.
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	array recursive id of node
 */
function makeObjectString(info) {
	var infoCount = info.length;
	var str = 'WPtree.tree[#]';
	for(x in info) {
		if(x == 0) {
			str = str.replace('#', info[0]);
		}
		else {
			str += '.childs['+info[x]+']';
		}
	}
	return str;
}

/**
 * Submit the form.
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	Object WPtree
 * @param	integer id of the parent
 * @param	integer dbid of the parent
 */
var retVal_index = 1;
var retVal = '';
function submit(theObj, parentIndex, parentDbId) {
	for(var p in theObj){
		if(typeof(theObj[p]) == 'object') {
			submit(theObj[p], parentIndex, parentDbId);
		}
		else {
			if(theObj['name'] == '<?= escSquote($menuSettings['rootName']);?>' && theObj['dbId'] == -1) {
				continue;
			}
			else {
				if (retVal_index > 1) retVal += '\n';
				var dbId = theObj['dbId'] == 0 ? 'NULL' : theObj['dbId'];
				tmpParentDbId = parentDbId == 0 ? 'NULL' : parentDbId;
				retVal += retVal_index + ',' + parentIndex + ',' + tmpParentDbId + ',' + theObj['name'].replace(/,/ig, ':|:') + ',' + dbId;
				retVal_index++;
				if(theObj['childs'].length > 0) {
					submit(theObj['childs'], retVal_index-1, theObj['dbId']);
				}
			}
			break;
		}
	}
}
function submitForm(redir) {
	F = _D.forms[0];
	retVal_index = 1;
	retVal = '';
	submit(WPtree.tree, 0, 'NULL');
	F['menu'].value = retVal;
	if(redir) {
		F.submit();
	}
	return true;
}

/**
 * Make a string of the HTML
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	Object	WPtree
 * @param	integer	peace of the id
 * @param	bool		either true or false, if true the node is visible.
 */
var tree = '';
function showTree(theObj, what, state) {
	var id;
	what = what ? what : undefined;
	for(var p in theObj){
		if (what == undefined) {
			id = p;
		} else {
			id = what+','+p;
		}
		if(!state) {
			state = false;
		}
		else if (p == 'state') {
			state = theObj[p]
		}
		if(typeof(theObj[p]) == 'object') {
			showTree(theObj[p], id, state);
		}
		else if(p=='name' || p=='state') {
			continue;
		}
		else {
			id = id.replace('tree,','');
			id = id.replace(/childs,/g,'');
			id = id.replace(',dbId','');
			// Calculate depth
			var curDepth = id.split(',');
			curDepth = curDepth.length-1;
			// Make node.
			var display = state ? 'block' : 'none' ;
			tree += '<tr id="'+id+'" style="display:'+display+'">';
			tree += '<td>';
			tree += '<img src="pics/px.gif" width="'+(curDepth*18) +'" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
			// Must the node be open or closed.
			if(theObj['childs'].length > 0 && theObj['dbId'] != 99999 && typeof(theObj['childs'][0]) != 'undefined') {
				if(state && theObj['state'] === false) {
					tree += '<img src="pics/plus.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" 	onclick="toggleMenu(this)"  />';
					tree += '<img src="pics/folder.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
				}
				else if(!state) {
					tree += '<img src="pics/plus.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" 	onclick="toggleMenu(this)" />';
					tree += '<img src="pics/folder.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
				}
				else{
					tree += '<img src="pics/minus.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" 	onclick="toggleMenu(this)" />';
					tree += '<img src="pics/folderopen.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
				}
			}
			else if(theObj['dbId'] == 99999) {
				tree += '<img src="pics/px.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
				tree += '<img src="pics/folder.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
			}
			else {
				tree += '<img src="pics/px.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
				tree += '<img src="pics/folder.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
			}
			if(theObj['dbId'] == 99999) {
				tree += '<input type="text" name="item[]" class="inputItem" onfocus="inputFocus(this)" onblur="inputBlur(this)" autocomplete="off" onkeydown="if(event.keyCode==13) return false" onkeyup="keyAction(event)" value="'+ theObj['name'].replace(/_/g, '\'') +'" style="width: 400px;" />';
			}
			else {
				tree += '<input type="text" name="item[]" class="inputItem" onfocus="inputFocus(this)" onblur="inputBlur(this)" autocomplete="off" onkeydown="if(event.keyCode==13) return false" onkeyup="keyAction(event)" oncontextmenu="showContext(event);return false;" value="'+ theObj['name'].replace(/_/g, '\'') +'" style="width: 400px;" />';
			}

			tree += '</td>';
			tree += '</tr>';
		}
	}
}

/**
 * Show the tree on the steen
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 */
function init() {
	tree = '<table cellpadding="0" cellspacing="0" border="0" id="menuTBL">';
	showTree(WPtree.tree, false, true);
	tree += '</table>';
	var div = gE('menuDiv')
	div.innerHTML = tree;
}

/**
 * inputFocus
 *
 *	Assign the data that is stored in de row id
 * and change the class of the selected input field.
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	object selected input field
 * @return	void
 */
function inputFocus(el) {
	el.className = 'inputItemSelect';
	nodeInfo = el.parentNode.parentNode.id;
	curIndex = el.parentNode.parentNode.rowIndex;
}

/**
 * inputBlur
 *
 *	Change the class of the selected input field.
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	object selected input field
 * @return	void
 */
function inputBlur(el) {
	// Set the name of the node.
	var info = nodeInfo.split(',');
	var infoCount = info.length;
	el.className = 'inputItem';
	if(infoCount == 1 && info[0] == 0) {
		return false;
	}
	// make a string of the curent node.
	var thisNode = makeObjectString(info);
	//var value = el.value.replace(/\'/g, '_');
	// eruit gehaald op 20060910 door jorgen: bovenstaande niet nodig
	var value = el.value;
	if(value != '') {
		thisNode += '.name = value';
		eval(thisNode);
	}
}

/**
 * Control key actions
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	object event
 */
function keyAction(e) {
	if (!e) e	= window.event;
	var menuTBL = _D.getElementById('menuTBL');

	if(e.keyCode == 38 && curIndex != 0) { // up key
		// Select previous item.
		selectNode(curIndex-1, 'prev');
	}
	else if(e.keyCode == 40 && curIndex != menuTBL.rows.length-1) { // down key
		// Select next item.
		selectNode(curIndex+1, 'next');
	}
	else if(e.keyCode == 33) { // Page up key.
		// Move up the selected item, including subitems.
		moveUp();
	}
	else if(e.keyCode == 34) { // Page down key
		// Move up the selected item, including subitems.
		moveDown();
	}
	return true;
}

/**
 * Change the focus on the given rowIndex
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	integer rowIndex of the node that must be focused
 * @param	string either 'prev' or 'next'
 * @return	type
 */
function selectNode(idx, what) {
	var menuTBL = _D.getElementById('menuTBL');
	var checkIdx;
	if(idx < 0 || idx >= menuTBL.rows.length) {
		return false;
	}
	if(what == 'prev') {
		checkIdx = idx-1;
	}
	else if(what == 'next') {
		checkIdx = idx+1;
	}
	else {
		return false;
	}

	if(menuTBL.rows[idx].style.display == 'none') {
		selectNode(checkIdx, what);
	}
	else {
		menuTBL.rows[idx].cells[0].childNodes[3].focus();
	}
	return false;
}

/**
 * The rightclick menu
 *
 * @author	Jurriaan Nijkamp <jurriaan.nijkamp@webpower.nl>
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @return	type
 */
function showContext(e) {
	hideContext();
	var hasMenu = false;

	var top = e.clientY, left = e.clientX, html = '';

	html = '<ul class="contextEl">';
	html += '<li><a href="#" onclick="saveAndEdit();return false"><?= word(11014); ?></a></li>';
	html += '<li>-------------------</li>';
	html += '<li><a href="#" onclick="makePopup(null,400,400,\'<?= word(11015) ?>\',\'moveTo.php?TID='+TID+'\');return false"><?= word(11015) ?></a></li>';
	html += '<li><a href="#" onclick="addNode();return false"><?= word(11001) ?></a></li>';
	html += '<li><a href="#" onclick="deleteNode();return false"><?= word(11002) /*Verwijder menu */ ?></a></li>';
	html += '<li><a href="#" onclick="moveUp();return false"><?= word(11003) ?></a></li>';
	html += '<li><a href="#" onclick="moveDown();return false"><?= word(11004) ?></a></li>';
	html += '</ul>';

	if (is.ie) {
		var sl = (is.ieBox) ? _D.body.scrollLeft : _D.documentElement.scrollLeft;
		var st = (is.ieBox) ? _D.body.scrollTop  : _D.documentElement.scrollTop;
	}
	else {
		sl = _W.pageXOffset;
		st = _W.pageYOffset;
	}
	contextEl = _D.createElement('div');
	contextEl.className = 'contextEl';
	contextEl.style.left = left+'px';
	contextEl.style.top  = top+'px';
	contextEl.innerHTML = html;
	contextEl.onmouseout = function(){contextTmr=setTimeout('hideContext()',100)};
	contextEl.onmouseover = function(){clearTimeout(contextTmr)};
	_D.body.appendChild(contextEl);
}

/**
 * Hide the rightclick menu
 *
 * @author	Jurriaan Nijkamp <jurriaan.nijkamp@webpower.nl>
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @return	type
 */
function hideContext() {
	var d = gE('details');
	if (d) HE(d);
	clearTimeout(contextTmr);
	_D.oncontextmenu = null;
	if (contextEl) {
		_D.body.removeChild(contextEl);
		contextEl = null;
	}
}

/**
 * Open or close the menu items
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	object
 */
function toggleMenu(el) {
	var info = el.parentNode.parentNode.id;
	var info = info.split(',');
	// make a string of the curent node.
	var thisNode = makeObjectString(info);

	// Get the current state of the node
	var curState = thisNode +=".state";
	curState = eval(curState);
	curState = curState ? false : true;
	thisNode +=" = "+curState;
	eval(thisNode);

	// Re-build the tree on screen.
	tree = '';
	init();
	return true;
}

/**
 * Save the form and go to the edit page of this item.
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	object
 */
/**
 * Save the form and go to the edit page of this item.
 *
 * @author	Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @param	object
 */
function saveAndEdit() {
	F = _D.forms[0];

	if(nodeInfo !== null) {
		// Try to get the DB ID of the selected node.
		var info = nodeInfo.split(',');

		// make a string of the curent node.
		var thisNode = makeObjectString(info);

		// Get DB ID
		var DBID = thisNode +".dbId";
		DBID = eval(DBID);

		if(DBID == 0) {
			alert('<?= word(11013) /* Nieuwe items kunnen niet direct ge-edit worden */ ?>');
			return false;
		}
		F['selDBID'].value = DBID;
	}

	F['redir'].value = 'true';
	submitForm(true);
}


function print_r(theObj){
	document.write("<ul>");
	for(var p in theObj){
		if(typeof(theObj[p]) == 'object' || typeof(theObj[p]) == 'array') {
			document.write("<li>["+p+"] => "+typeof(theObj)+"</li>");
			document.write("<ul>")
			print_r(theObj[p]);
			document.write("</ul>")
		}
		else {
			document.write("<li>["+p+"] => "+theObj[p]+"</li>");
		}
	}
	document.write("</ul>")
}


var msg = '';
function print_rr(theObj){
	msg += "<ul>";
	for(var p in theObj){
		if(typeof(theObj[p]) == 'object' || typeof(theObj[p]) == 'array') {
			msg += "<li>["+p+"] => "+typeof(theObj)+" =>"+ theObj[p] +"</li>";
			msg += "<ul>";
			print_rr(theObj[p]);
			msg += "</ul>";
		}
		else {
			msg += "<li>["+p+"] => "+theObj[p]+"</li>";
		}
	}
	msg += "</ul>";
	gE('tree').innerHTML = msg;
}
