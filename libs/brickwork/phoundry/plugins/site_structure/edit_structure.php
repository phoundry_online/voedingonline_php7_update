<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once 'settings.php';
	checkAccess();
	
	if(!function_exists('getSiteInfo')) {
		function getSiteInfo(){
			global $db;
			if(!isset($_GET['site_identifier'])) return array();
			$sql = 'SELECT * FROM brickwork_site WHERE identifier = "'.escDBquote($_GET['site_identifier']).'"';
			$res = $db->query($sql);
			$result = array();
			if(!$db->EndOfResult($res)){
				$db->FetchResultAssoc($res,$result,0);
			}
			return $result;
		}
	}

   $site_identifier = !empty($_REQUEST['site_identifier']) ? $_REQUEST['site_identifier'] : '';

   if(empty($site_identifier)){
      die('no identifier');
   }

	$HASACL = (isset($WEBprefs['acl']['enable']) ? $WEBprefs['acl']['enable'] : true );
	
   // Process change to node (via context menu):
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['node_id'])) {
		$sets = array();
		$sets[] = "show_menu = " . (isset($_POST['menu']) ? 1 : 0);
		$sets[] = "show_sitemap = " . (isset($_POST['sitemap']) ? 1 : 0);
		$sets[] = "stype = '" . escDBquote($_POST['stype']) . "'";
		
		if ( $HASACL )
		{
			$sets[] = "restricted = " . (isset($_POST['restricted']) ? 1 : 0);
			$sets[] = "secure = " . (isset($_POST['secure']) || isset($_POST['restricted']) ? 1 : 0);
		}
		else
			$sets[] = "secure = " . (isset($_POST['secure']) ? 1 : 0);
		
		
		$sql = "UPDATE brickwork_site_structure SET " . implode(', ', $sets) . " WHERE id = {$_POST['node_id']}";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	}

	$site_info = getSiteInfo();
		
	function getMenu($curid = '') {
		global $db,$site_identifier, $HASACL;

		$nullQuery = "
			SELECT
				DISTINCT s.id,
				parent_id,
				name,
				s.stype as type,
				show_menu,
				show_sitemap,
				secure" .
			( $HASACL ? ', restricted' : '' ) .
			"
			
			FROM
				brickwork_site_structure s
			WHERE
				s.site_identifier = '{$site_identifier}'
				AND
				s.parent_id is null
			ORDER BY prio
			";
		$menu = array();

		$sql = $nullQuery;
		if ($curid != '')
			$sql = str_replace('is null', "= '$curid'", $sql);
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$nextid = $db->FetchResult($cur,$x,'id');
			$parent = $db->FetchResult($cur,$x,'parent_id');
			$type = ($db->FetchResult($cur,$x,'type')=='') ? 'notlinked' : $db->FetchResult($cur,$x,'type');
			$openbaar = 1;

			if ( $HASACL )
				$restricted = (bool)$db->FetchResult($cur,$x,'restricted');
			else
				$restricted = false;
				
			$secure = (bool)$db->FetchResult($cur,$x,'secure');
			$show_menu = $db->FetchResult($cur,$x,'show_menu');
			$show_sitemap = $db->FetchResult($cur,$x,'show_sitemap');

			$fileParts = '';
			if ($show_menu) $fileParts .= '_m';
			if ($show_sitemap) $fileParts .= '_s';
			if ($secure) $fileParts .= '_x';
			if ($restricted) $fileParts .= '_r';
			
			switch($type){
				case 'url':
					$icon = "www{$fileParts}.gif";
					break;
				case 'page':
					$icon = "page{$fileParts}.gif";
					break;
				default:
					$icon = 'notlinked.gif';
			}
			$text = $db->FetchResult($cur,$x,'name');
			$menu[] = array('id' => $nextid,
				             'text' => $text,
								 'parent' => is_null($parent) ? 0 : $parent,
								 'icon' => is_null($type) ? '' : $icon
				            );


	      $menu = array_merge($menu, getMenu($nextid));
		}
		return $menu;
	}

	
	function getTIDbyName($name) {
		global $db;

		$sql = sprintf('
			SELECT
				DISTINCT id
			FROM
				phoundry_table
			WHERE
				name = "%s" LIMIT 1
			',escDBquote($name));
		
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if(!$db->EndOfResult($cur)) {
			return $db->FetchResult($cur,0,'id');
		}
		return false;
	}
	
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title><?=htmlspecialchars($menuSettings['rootName']);?></title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<style type="text/css">
<!--

div.contextEl {
	position: absolute;
	background: #fff;
	border: 1px solid black;
	z-index: 11;
	filter:progid:DXImageTransform.Microsoft.Shadow(color=#777777,Direction=135,Strength=2)
}

ul.contextEl {
	margin: 0;
	padding: 4px;
	list-style: none;
	width: 140px;
}

ul.contexEl li {
	position: relative;
}

ul.contextEl li a:link, ul.contextEl li a:visited {
	display: block;
	text-decoration: none;
	color: #0046d5;
	padding: 2px;
}

ul.contextEl li a:hover {
	background: #ccc;
	color: #000;
	text-decoration: none;
}
.DT {
   font: 11px Futura;
   font: Icon;
   color: #666;
   white-space: nowrap;
}
.DT img {
   border: 0px;
   vertical-align: middle;
}
.DT a:link, .DT a:visited, .DT a:active {
   color: #333;
   text-decoration: none;
}
.DT a.N, .DT a.NS {
   white-space: nowrap;
   padding-left: 2px;
}
.DT a.N:hover, .DT a.NS:hover {
   text-decoration: underline;
}
.DT a.NS:link, .DT a.NS:visited, .DT a.NS:active {
   background-color: #0c2e82;
   color: #fff;
}
.DT .clip {
   overflow: hidden;
}

img {
	border: 0;
}
-->
</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript" src="js/dtree.js"></script>
<script type="text/javascript">
//<![CDATA[

var contextEl = null, contextTmr = null;

function showContext(e, elem, node_id) {
	var top = e.clientY, left = e.clientX, html = '', checked;
	hideContext();

	// Find image for this node:
	var imgs = elem.parentNode.getElementsByTagName('img');
	var img = imgs[imgs.length-1].src;
	img = img.substr(img.lastIndexOf('/')+1);

	html += '<form action="<?= $_SERVER['PHP_SELF'] ?>?<?= QS(1) ?>" method="post"><input type="hidden" name="node_id" value="' + node_id + '" /><ul class="contextEl">';

	checked = /_m(.*)\.gif$/.test(img) ? ' checked="checked" ' : ' ';
	html += '<li><input' + checked + 'type="checkbox" name="menu" value="1" onclick="this.form.submit()" /> <?= word(11030) /* Toon in menu */ ?></li>';

	checked = /_s(.*)\.gif$/.test(img) ? ' checked="checked" ' : ' ';
	html += '<li><input' + checked + 'type="checkbox" name="sitemap" value="1" onclick="this.form.submit()" /> <?= word(11031) /* Toon in sitemap*/ ?></li>';

	checked = /_x(.*)\.gif$/.test(img) ? ' checked="checked" ' : ' ';
	html += '<li><input' + checked + 'type="checkbox" name="secure" value="1" onclick="this.form.submit()" /> <?= word(11032) /* Beveiligd*/ ?></li>';

	<? if ( $HASACL ) {  ?>
	checked = /_r(.*)\.gif$/.test(img) ? ' checked="checked" ' : ' ';
	html += '<li><input' + checked + 'type="checkbox" name="restricted" value="1" onclick="this.form.submit()" /> <?= word(11033) /* Autorisatie nodig*/ ?></li>';
	<? } ?>
	
	checked = /^www(.*)\.gif$/.test(img) ? ' checked="checked" ' : ' ';
	html += '<li><input' + checked + 'type="radio" name="stype" value="url" onclick="this.form.submit()" /> <?= word(11018) /* URL*/ ?>';

	checked = !/^www(.*)\.gif$/.test(img) ? ' checked="checked" ' : ' ';
	html += '<input' + checked + 'type="radio" name="stype" value="page" onclick="this.form.submit()" /> <?= word(11017) /* Pagina*/ ?></li>';

	html += '</ul></form>';

	if (is.ie) {
		sl = (is.ieBox) ? _D.body.scrollLeft : _D.documentElement.scrollLeft;
		st = (is.ieBox) ? _D.body.scrollTop  : _D.documentElement.scrollTop;
	}
	else {
		sl = _W.pageXOffset;
		st = _W.pageYOffset;
	}
	contextEl = _D.createElement('div');
	contextEl.className = 'contextEl';
	contextEl.style.left = left+'px';
	contextEl.style.top  = top+'px';
	contextEl.innerHTML = html;
	contextEl.onmouseout = function(){contextTmr=setTimeout('hideContext()',1000)};
	contextEl.onmouseover = function(){clearTimeout(contextTmr)};
	_D.body.appendChild(contextEl);
}

function hideContext() {
	d = gE('details');
	if (d) HE(d);
	clearTimeout(contextTmr);
	_D.oncontextmenu = null;
	if (contextEl) {
		_D.body.removeChild(contextEl);
		contextEl = null;
	}
}

function ctxtNode(e, elem, node_id) {
	showContext(e, elem, node_id);
}

function submitIt(event, elem, structure_id) {
	var F = _D.forms[0];
	
	// Find image for this node:
	var imgs = elem.parentNode.getElementsByTagName('img');
	var img = imgs[imgs.length-1].src;
	img = img.substr(img.lastIndexOf('/')+1);

	if(img.substr(0,4) == 'page'){
		F.TID.value = '<?=getTIDbyName('brickwork_page') ?>';
	}

	if(img.substr(0,3) == 'www'){
		F.TID.value = '<?=getTIDbyName('brickwork_url') ?>';
	}
	if(F.TID.value == ''){
		ctxtNode(event,elem,structure_id);
		return;
	}

	F.site_structure_id.value = structure_id;
	F.submit();
}

//]]>
</script>
</head>
<body class="frames" onclick="hideContext()">
<form action="<?=$PHprefs['url']?>/core/records.php">
<input type="hidden" name="TID" value="" />
<input type="hidden" name="site_structure_id" />
<input type="hidden" name="site_identifier" value="<?=htmlspecialchars($site_identifier);?>" />
<div class="headerFrame">
<?= getHeader($menuSettings['rootName'], $site_info['name']); ?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->


<fieldset style="position:absolute; right:25px">
<legend><b><?= word(11016) ?></b></legend>
<img src="pics/page.gif" height="18" width="18" border="0" alt="pagina" /><?= word(11017) /* Pagina */?><br />
<img src="pics/www.gif" height="18" width="18" border="0" alt="link" /><?= word(11018) /* Url */?><br />
<img src="pics/www_m.gif" height="18" width="18" border="0" alt="link" /><?= word(11019) /* Url (in menu) */?><br />
<img src="pics/www_s.gif" height="18" width="18" border="0" alt="link" /><?= word(11020) /* Url (in sitemap) */?><br />
<img src="pics/www_m_s.gif" height="18" width="18" border="0" alt="link" /><?= word(11021) /*  Url (in menu &amp; sitemap) */?><br />
<img src="pics/notlinked.gif" height="18" width="18" border="0" alt="<?= word(11029) /* Nog niet gelinkt */?>" /><?= word(11022) /*  Leeg */?><br />
<img src="pics/page_m.gif" height="18" width="18" border="0" alt="Menu" /><?= word(11023) /*  Pagina (in menu) */?><br />
<img src="pics/page_s.gif" height="18" width="18" border="0" alt="Sitemap" /><?= word(11024) /*  Pagina (in sitemap) */?><br />
<img src="pics/page_m_s.gif" height="18" width="18" border="0" alt="Menu en sitemap" /><?= word(11025) /*  Pagina (in menu &amp; sitemap) */?><br />
<img src="pics/page_m_x.gif" height="18" width="18" border="0" alt="Menu secure" /><?= word(11026) /*  Menu secure */?><br />
<img src="pics/page_s_x.gif" height="18" width="18" border="0" alt="Sitemap secure" /><?= word(11027) /*  Sitemap secure */?><br />
<img src="pics/page_m_s_x.gif" height="18" width="18" border="0" alt="Menu en sitemap secure" /><?= word(11028) /*  Menu &amp; sitemap secure */?><br />
</fieldset>

<script type="text/javascript">
//<![CDATA[

var Tree = new dTree('Tree', 'pics');
Tree.config.inOrder = true;
Tree.add(0,-1,'<b><?=$menuSettings['rootName'];?></b>','','','','','','',true);

<?php
	$menu = getMenu();
	foreach($menu as $item) {
		$idx = $item['id'];
		$text = escSquote($item['text']);

		print "Tree.add({$idx},{$item['parent']},'$text','#','submitIt(event, this, $idx)','','ctxtNode(event,this,{$idx})','','','','{$item['icon']}',1);\n";
	}
?>
_D.write(Tree);

//]]>
</script>
<br />
<br />
<br />
<br />


<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<input type="button" value="<?= word(10061) ?>" onclick="_D.location='<?= $PHprefs['url'] ?>/layout/home.php'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
