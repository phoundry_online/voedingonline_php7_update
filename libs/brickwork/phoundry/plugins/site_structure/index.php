<?php
$inc = @include('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}

if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	PH::phoundryErrorPrinter("Deze functionaliteit is vervangen door de nieuwe Site Structuur interface", true);
}

require_once "{$PHprefs['distDir']}/core/include/common.php";
$TID = getTID();

// Make sure the current user in the current role has access to this page:
checkAccess();

$site_identifier = !empty($_REQUEST['site_identifier']) ? $_REQUEST['site_identifier'] : '';

if(empty($site_identifier)){
	die('no identifier');
}

// load settings
require_once 'settings.php';

// Shortcut for $menuSettings:
$MS = &$menuSettings;

$MS['sql'] = "SELECT {$MS['keyField']}, {$MS['parentField']}, {$MS['showField']} FROM {$MS['tableName']} WHERE site_identifier = '{$site_identifier}' AND {$MS['parentField']} IS NULL";

if (isset($MS['orderField']) && !empty($MS['orderField']))
	$MS['sql'] .= " ORDER BY {$MS['orderField']}";
else
	$MS['sql'] .= " ORDER BY {$MS['keyField']}";

function getMenu($curid = '', $depth = 1) {
	global $db, $MS;

	$menu = array();

	$sql = $MS['sql'];
	if ($curid != '')
		$sql = str_replace('IS NULL', "= '$curid'", $sql);
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++) {
		$nextid = $db->FetchResult($cur,$x,$MS['keyField']);
		$parent = $db->FetchResult($cur,$x,$MS['parentField']);
		$menu[] = array('id'			=> (int)$nextid,
			             'text'		=> $db->FetchResult($cur,$x,$MS['showField']),
							 'parentDB'	=> is_null($parent) ? 0 : (int)$parent,
							 'depth'		=> $depth,
							 'nrChild'	=> getChilds($nextid),
			            );
      $menu = array_merge($menu, getMenu($nextid, $depth+1));
	}
	return $menu;
}

function getChilds($id) {
	global $db, $MS, $site_identifier;
	$sql = "SELECT count(*) as nrChild FROM {$MS['tableName']} WHERE site_identifier = '{$site_identifier}' AND {$MS['parentField']} = {$id}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	$nrChild = $db->FetchResult($cur,0,'nrChild');
	return $nrChild;
}


function makeObjectString($info) {
	$infoCount = count($info);
	$str = 'WPtree.tree[#]';	
	foreach($info as $idx => $val) {
		if($idx == 0) {
			$str = str_replace('#', $val, $str);	
		}
		else {
			$str .= '.childs['.$val.']';
		}
	}
	return $str;
}

PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Kies menu</title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript" src="js/WPtree.js.php"></script>

<script type="text/javascript">
//<![CDATA[
var TID = <?= $TID; ?>;

// Make the object WPtree.
var WPtree = new WPtree();
WPtree.tree[0] = new WPnode('<?= escSquote($menuSettings['rootName']);?>', -1, true);

<?php
	// get menu data
	$menuData = getMenu();
	$mapping = array();
	$count = 1;

	$shir = Array();

	$lastDepth = -1;
	foreach($menuData as $index => $item) {		
		if(!isset($shir[$item['depth']])) {
			$shir[$item['depth']] = 0;
		}
		else if($item['depth'] > $lastDepth) {			
			$shir[$item['depth']] = 0;
		}
		else if($item['depth'] <= $lastDepth) {
			$shir[$item['depth']] = ($shir[$item['depth']] + 1);
		}		
		$trId = Array('0');
		for($x = 1; $x<=$item['depth']; $x++) {
			$trId[] = $shir[$x];
		}
		printf("%s = new WPnode('%s', %d, true);\n", makeObjectString($trId), escSquote($item['text']), $item['id']);
		
		$lastDepth = $item['depth'];
		$mapping[$count++] = $item['id'];
	}

?>

//onclick="hideContext()"

//]]>
</script>
</head>
<body class="frames" onload="init()">
<div class="headerFrame">
<?= getHeader($MS['title']); ?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
<button onclick="addNode();"><?= word(11001); /* New menu */?></button>
<button onclick="deleteNode()"><?= word(11002); /* Delete menu */ ?></button>
<img src="<?= $PHprefs['url'] ?>/core/pics/div.gif" alt="|" width="3" height="18" />
<button onclick="moveUp()"><?= word(11003); /* Move up */ ?></button>
<button onclick="moveDown()"><?= word(11004); /* Move down */ ?></button>
<? if ( $PHSes->isAdmin){ ?>
<button onclick="print_rr(WPtree.tree)"><?= word('11000')  /*Show tree*/?></button>
<? } ?>
</td>
</tr></table>
</div>

<form action="menu.php?<?= QS(1) ?>" method="post" onsubmit="return submitForm(false)">

<div class="contentFrame" style="padding:6px;">
<!-- Content -->
<fieldset><legend class="caution"><img src="<?= $PHprefs['url'] ?>/core/icons/exclamation.png" /> <b><?= word(11005); /* Pay attension */ ?></b></legend>
<?= word(11006); /* attension */ ?>
</fieldset>
<br />
<div id="menuDiv"></div>
<input type="hidden" name="menu" value="" />
<input type="hidden" name="mapping" value="<?= htmlspecialchars(serialize($mapping)) ?>" />
<input type="hidden" name="remove" value="" />
<input type="hidden" name="selDBID" value="" />
<input type="hidden" name="selInfo" value="" />
<input type="hidden" name="redir" value="false" />

<div id="tree"></div>
<!-- /Content -->



</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input id="okBut" type="submit" style="width:100px;" value="<?= word(82) ?>" />
	</td>
	<td align="right">
	<input type="button" value="<?= word(46) ?>" onclick="_D.location='<?= $PHprefs['url'] ?>/layout/home.php'" />
	</td>
</tr></table>
</div>


</form>
</body>
</html>
