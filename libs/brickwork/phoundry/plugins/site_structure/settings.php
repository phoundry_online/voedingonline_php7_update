<?php
	/*
	 * The settings here are translated into DB-queries automagically.
	 * You need a table that looks like this to make this plugin work:
	 * <----->
	 * +-----+-----------+-------------+----------+
	 * | id  | parent_id |    name     | order_by |
	 * | int |    int    | varchar(80) |   int    |
	 * +-----+-----------+-------------+----------+
	 *                                    
	 *
	 * The settings you enter are translated into a recursive query like this:
	 *    SELECT id, name, parent_id 
	 *    FROM site_menu 
	 *    WHERE parent_id IS NULL ORDER BY order_by;
	 */

	$menuSettings = array(
		'title'       => 'Site structuur',
		'width'       => 400,			// In pixels
		'height'      => 400,   		// In pixels
		'rootName'    => 'Structuur',		// Name of the root element.
		'tableName'   => 'brickwork_site_structure',
		'keyField'    => 'id',
		'parentField' => 'parent_id',
		'orderField'  => 'prio',
		'showField'   => 'name',
		// records page
		'recordsTable'=> 'brickwork_page',
		'recordsTable_menuId'=> 'site_structure_id',
		'recordsTable_permitDelete'=> TRUE,		// permit to delete records in record table when menu id is deleted

	);


	$indent		= 18;
	$curParent  = 0;
	$curDepth	= 1;
	$redirURL	= sprintf('%s/core/records.php?TID=%d&menu_id=##MENU_ID##&site_structure_id=##MENU_ID##&site_identifier=%s', $PHprefs['url'] , getPageLayoutTID(), isset($_GET['site_identifier']) ? $_GET['site_identifier']: '' );
	
	
	function getPageLayoutTID()
	{
		global $db;
		
		$sql = "SELECT id FROM phoundry_table WHERE name = 'brickwork_page' LIMIT 1";
		
		$res = $db->Query($sql) or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

		return $db->FetchResult($res,0,'id');
	}

	
?>
