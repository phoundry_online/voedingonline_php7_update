<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

	$TID = getTID();

	// Make sure the current user in the current role has access to this page:

	checkAccess();
	// get settings
	require_once 'settings.php';
	
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Phoundry</title>
<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/core/css/popup.css" type="text/css" />
<style type="text/css">
#menuDiv,
#menuDiv td 
{
	font-family: Arial, Verdana, Helvetica;
	font-size: 11px;
	font-weight: bold;
}
#menuDiv a
{
	color: #000;
	text-decoration: none;
}
#menuDiv a:hover
{
	color: #00b;
}
legend
{
	font-family: Arial, Verdana, Helvetica;
	font-size: 12px;
}
</style>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

var tree = '';
function showTree(theObj, what, state) {
	var id;
	what = what ? what : undefined;
	for(var p in theObj){
		if (what == undefined) {
			id = p;
		} else {
			id = what+','+p;
		}		
		if (p == 'state') {		
			state = theObj[p]
		}
		if(typeof(theObj[p]) == 'object') {
			showTree(theObj[p], id, state);
		}
		else if(p=='name' || p=='state') {
			continue;
		}
		else {			
			id = id.replace('tree,','');
			id = id.replace(/childs,/g,'');
			id = id.replace(',dbId','');
			// Calculate depth
			var idArr = id.split(',');
			curDepth = idArr.length-1;
			// Make node.
			tree += '<tr id="'+id+'">';
			tree += '<td>';
			tree += '<img src="pics/px.gif" width="'+(curDepth*18) +'" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
			
			// Must the node be open or closed.
			if(theObj['childs'].length > 0 && theObj['dbId'] != 99999) {
				tree += '<img src="pics/minus.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
			}
			else if(theObj['dbId'] == 99999) {
				tree += '<img src="pics/px.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';			
			}
			else {
				tree += '<img src="pics/px.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';
			}

			tree += '<img src="pics/folder.gif" width="18" height="18" alt="" border="0" align="left" hspace="0" vspace="0" />';

			var lenSelID = parent.nodeInfo.length;
			var compare = id.substring(0, parent.nodeInfo.length);
			
			if(compare == parent.nodeInfo) {
				tree += theObj['name'];
			}
			else {
				tree += '<a href="#" onclick="moveTo(this.parentNode.parentNode.id); return false;">'+ theObj['name'] +'<'+'/'+'a>';
			}

			tree += '</td>';
			tree += '</tr>';
		}
	}
}

function moveTo(toInfo) {
	var info = parent.nodeInfo.split(',');
	var infoCount = info.length;

	// make a string of the curent node.
	var thisNode = 'parent.'+parent.makeObjectString(info);
	var nodeToMove = eval(thisNode);

	// Delete thisNode
	eval("delete "+thisNode);	

	// make a string of node to move to.
	info = toInfo.split(',');
	infoCount = info.length;

	// make a string of the curent node.
	thisNode = 'parent.'+parent.makeObjectString(info)+'.childs.push(nodeToMove)';

	// Move the node.
	eval(thisNode);
	
	parent.tree = '';
	parent.init();
	parent.killPopup();
	return true;
}

function initMoveTo() {
	tree = '<table cellpadding="0" cellspacing="0" border="0" id="menuTBL">';
	showTree(parent.WPtree.tree, false, true);
	tree += '</table>';
	var div = gE('menuDiv')
	div.innerHTML = tree;
}
//]]>
</script>
</head>
<body onload="initMoveTo();">
<form action="moveTo.php" method="get">
<input type="hidden" name="TID" value="<?= $TID; ?>" />

<fieldset>
	<legend><b>Verplaats naar</b></legend>
	<div id="menuDiv" name="menuDiv">

	</div>
</fieldset>


<p align="right">
<input type="button" value="<?= word(46) ?>" onclick="parent.killPopup()" />
</p>

</form>
</body>
</html>
