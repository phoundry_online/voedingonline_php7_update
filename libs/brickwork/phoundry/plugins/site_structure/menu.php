<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once BRICKWORK_DIR.'/functions/autoloader.include.php';
	$TID 	= getTID();
	$HASACL = (isset($WEBprefs['acl']['enable']) ? $WEBprefs['acl']['enable'] : true );

	// Make sure the current user in the current role has access to this page:
	checkAccess();

	// get settings
	require_once 'settings.php';

	// Shortcut for $menuSettings:
	$MS = &$menuSettings;

	// Debugging?
	$debug = false;

   $site_identifier = !empty($_REQUEST['site_identifier']) ? $_REQUEST['site_identifier'] : '';
   
   if(empty($site_identifier)){
   		die('no identifier');
   }
   
   Framework::loadSiteByIdentifier($site_identifier);
   
   if(!$_SERVER['REQUEST_METHOD']=='POST'){
   		die('i need a post');
   }
   
   // als de tabellen niet bestaan dan aanmaken:
   $sql_path_table = "CREATE TABLE IF NOT EXISTS `brickwork_site_structure_path` (
					  `site_identifier` varchar(20) NOT NULL default '',
					  `hash` varchar(32) NOT NULL default '',
					  `name` varchar(255) NOT NULL default '',
					  `site_structure_id` int(10) unsigned NOT NULL default '0',
					  PRIMARY KEY  (`site_identifier`,`hash`),
					  KEY `ind_site_ident_structure_id` (`site_identifier`(4),`site_structure_id`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";
   
   $sql_archive_table = "CREATE TABLE IF NOT EXISTS  `brickwork_site_structure_path_archive` (
						  `site_identifier` varchar(20) NOT NULL default '',
						  `hash` varchar(32) NOT NULL default '',
						  `site_structure_id` int(10) unsigned NOT NULL default '0',
						  `archive_date` datetime NOT NULL default '0000-00-00 00:00:00',
						  PRIMARY KEY  (`site_identifier`,`hash`)
						) ENGINE=MyISAM DEFAULT CHARSET=latin1";
	
	$cur = $db->Query($sql_path_table);
	$cur = $db->Query($sql_archive_table);

   
	$mapping = unserialize($_POST['mapping']);
	$menus = explode("\r\n", $_POST['menu']);
	$removeIDs = explode(",", $_POST['remove']);
	if ($debug) {
		print '<xmp>';
		var_dump($_POST);
		print '</xmp>';
		exit;
	}
	$newNodes = array(0=>'NULL');
	$DBids = array();
	$ordr = 1;

	// PE: AUTH!
	// Add site as root object
	$sql = sprintf("SELECT name, domain FROM brickwork_site WHERE identifier = '%s'", $site_identifier);
	
	$res = DB::iQuery($sql);
	$row = $res->first();
	
	if ( $HASACL )
	{
		$sql = sprintf('INSERT INTO brickwork_auth_object (parent_id, instanceid, description, class, foreign_id )' .
					   " VALUES(NULL,'%s','%s','Site',NULL)" .
					   " ON DUPLICATE KEY UPDATE description = '%s', parent_id = NULL, foreign_id = NULL",
						'Site/' . $site_identifier,
						$row->name,
						$row->name);
				
		$res = DB::iQuery($sql);
			
		$sql = sprintf("SELECT id FROM brickwork_auth_object WHERE instanceid = 'Site/%s'",$site_identifier);
		$res = DB::Query( $sql );
	
		$row = $res->first();
		
		$site_parent_id = $row->id;
		
		// We have the site key id 
		// Now we can add the page content handler
		$sql = sprintf('INSERT INTO brickwork_auth_object (parent_id, instanceid, description, class, foreign_id )' .
					   " VALUES(%s,'%s','%s','Site',NULL)" .
					   " ON DUPLICATE KEY UPDATE description = '%s', parent_id = %s, foreign_id = NULL",
						$site_parent_id,
						'PageContentHandler',
						'Website page content handler',
						'Website page content handler',
						$site_parent_id);
		
		$res = DB::iQuery($sql);
			
		$sql = sprintf("SELECT id FROM brickwork_auth_object WHERE instanceid = 'PageContentHandler'");
		$res = DB::Query( $sql );
	
		$row = $res->first();
		
		$roleid = Array('NULL' => $row->id);
	}
	else
		$roleid = Array('NULL' => NULL );
	
	// EOA
	
	foreach($menus as $menu) 
	{
		if (empty($menu)) continue;
		$vars = explode(',', $menu);
		$iIndex  = (int)$vars[0];		// Offset in de array
		$iParent = (int)$vars[1];		// Offset van de parent in de array, 0 is de root node
		$parent  = $vars[2];			// DB id van de parent (NULL voor de root node)
		$sName   = escDBquote(str_replace(':|:', ',', $vars[3]));// Naam van het menu item
		$DBid    = $vars[4];			// DBid van het menu item

		if ($DBid === 'NULL') {
			// This is a new node... insert:
			$sql = "INSERT INTO {$MS['tableName']} ({$MS['keyField']}, site_identifier, {$MS['parentField']}, {$MS['showField']}, {$MS['orderField']}) VALUES (NULL, '{$site_identifier}', " . ($parent == 'NULL' ? $newNodes[$iParent] : $parent) . ", '$sName', $ordr)";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			$DBid = $db->getLastInsertId('site_menu', 'id');
			$newNodes[$iIndex] = $DBid;
			
			
		}
		else {
			// This is an existing node... update:
			$DBids[] = $DBid;
			$parent = ($parent == 'NULL') ? isset($newNodes[$iParent]) ? $newNodes[$iParent] : 'NULL' : $parent;
			$sql = "UPDATE {$MS['tableName']} SET {$MS['showField']} = '$sName', {$MS['orderField']} = $ordr, {$MS['parentField']} = $parent WHERE {$MS['keyField']} = $DBid AND site_identifier = '{$site_identifier}'";
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}

		// PE: AUTH! Voor nu Auth boom ook opbouwen
		if ( $HASACL )
		{
			$sql = sprintf('INSERT INTO brickwork_auth_object (parent_id, instanceid, description, class, foreign_id )' .
						   " VALUES(%s,'%s','%s','SiteStructure',%s)" .
						   " ON DUPLICATE KEY UPDATE description = '%s', parent_id = %s, foreign_id = %s",
							$roleid[$parent],
							'StructureItem/' . $DBid,
							$sName,
							$DBid,
							$sName,
							$roleid[$parent],
							$DBid);
				
			$res = DB::iQuery($sql);
			
			$sql = sprintf("SELECT id FROM brickwork_auth_object WHERE instanceid = 'StructureItem/%s'",$DBid);
			$res = DB::Query( $sql );
			
			if ( isError($res))
				trigger_error('Query ' . $sql . ' yielded error ' . $res->errstr, E_USER_ERROR);
			$row = $res->first();
			$roleid[$DBid] = $row->id;
		}
		// EOA
		
		// PE 2008-12-02: eerst een lijst opbouwen en daarna readable paths maken om duplicate prio's te voorkomen
		$readablePathItems[] = Array( $site_identifier, $sName, $DBid );
		
		if ($debug) {
			print "$sql<br>";
			print '<xmp>';
			var_dump($newNodes);
			print '</xmp>';
		}
		$ordr++;
	}

	// En nu pas de create Readable Paths aanmaken.
	foreach ( $readablePathItems as $entry )
	{
		list( $site_identifier, $sName, $DBid ) = $entry;
		Model_Site_Structure::createReadablePath($site_identifier, $DBid, $sName);
	}
	
	
	// Delete nodes that were removed:
	$toDel = array_diff($mapping, $DBids);
	foreach($toDel as $DBid) {
		// Compare the generated IDs of this file with the post IDs
		if(in_array($DBid, $removeIDs)) {
			$may_delete_menu_item = TRUE;		// later gebruiken om te checken
			
			if(!empty($MS['recordsTable']) && isset($MS['recordsTable_permitDelete']) && $MS['recordsTable_permitDelete'] === TRUE) {
				$sql = "DELETE FROM {$MS['recordsTable']} WHERE {$MS['recordsTable_menuId']} = $DBid";
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
					
				// delete from readable url and readable url archive
				$sql = "DELETE FROM brickwork_site_structure_path WHERE {$MS['recordsTable_menuId']} = $DBid";
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				$sql = "DELETE FROM brickwork_site_structure_path_archive WHERE {$MS['recordsTable_menuId']} = $DBid";
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
			} 

			// when records table is set and not permit to delete items, check if there are items left in the table for 
			// given menu id

			/*
			if(!empty($MS['recordsTable']) && $may_delete_menu_item === FALSE){
				$sql = "SELECT count(*) as cnt FROM {$MS['recordsTable']} WHERE {$MS['recordsTable_menuId']} = $DBid";
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				$cnt = $db->FetchResult($cur, 0, 'cnt');
				if($cnt === 0){
					$may_delete_menu_item = TRUE;
				}
			}
			*/

			// if 
			if($may_delete_menu_item === TRUE){
				$sql = "DELETE FROM {$MS['tableName']} WHERE {$MS['keyField']} = $DBid";
				$cur = $db->Query($sql)
					or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
				if ($debug) print "$sql<br>";
			}
			
			// PE AUTH! Ook uit Auth boom removen
			if ( $HASACL )
			{
				$sql = sprintf("DELETE FROM brickwork_auth_object WHERE instanceid = 'StructureItem/%s'", $DBid);
				$res = DB::iQuery($sql);
	
				if ( isError($res) )
					trigger_error( sprintf('Query %s failed: %s', $sql, $res->errstr ) );
			}
			// EOA
		}
	}

	// remove cache file if exists
	SiteStructure::removeCacheFile($site_identifier);		

	if (!$debug) {
		if($_POST['redir'] == 'true' && !empty($_POST['selDBID'])) {
			$redirURL = str_replace('##MENU_ID##', $_POST['selDBID'], $redirURL);
			header("Location: {$redirURL}");
		}
		else {
			header('Location: index.php?' . QS(0));
		}
	}

	/**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }



?>
