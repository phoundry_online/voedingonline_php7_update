
jQuery(function($){
	initNotes();
	window.focus();

	$('fieldset.toggleable > legend').click(function(e){
		var fieldset = $(this).parents('fieldset:first');
		if(fieldset.hasClass('closed'))
		{
			fieldset.removeClass('closed').addClass('open');
		}
		else
		{
			fieldset.removeClass('open').addClass('closed');
		}
	});	
});