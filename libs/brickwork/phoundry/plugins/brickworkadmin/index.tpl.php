<?php
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Structuur</title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="style.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript" src="/index.php/script/js/jquery|jquery.scrollTo.js/jquery|jquery.query.js?nopack"></script>
<script type="text/javascript" src="script.js"></script>
</head>
<body class="frames">
<div id="headerFrame" class="headerFrame">
<?= getHeader('Brickwork Admin', ''); ?>
<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
	<img src="<?= $PHprefs['url'] ?>/core/icons/shortcut_<?= $isShortcut ? 'delete' : 'add' ?>.png" class="ptr" width="16" height="16" alt="<?= word(19) . ' ' . ($isShortcut ? '-' : '+') ?>" onclick="shortCut(this, <?=$TID?>)" />
	<?if($PHprefs['nrNotes'] > 0 && $notes['count'] < $PHprefs['nrNotes']):?>
	<img src="<?= $PHprefs['url'] ?>/core/icons/3mnote.png" class="ptr popupWin" width="16" height="16" alt="<?= word(196) ?>" title="<?= word(196) ?>|300|320|0|<?= $PHprefs['url'] ?>/core/popups/editNote.php?TID=<?=$TID?>" />
	<?endif?>
	<img class="div" alt="|" src="<?= $PHprefs['url'] ?>/core/pics/div.gif"/>
	
	<?foreach($buttons as $button):?>
	<input <?=Html::attributes($button)?> />
	<?endforeach?>
</td>
</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<?foreach($addons as $addon):?><?if($addon->display):?>
<fieldset class="<?=$addon?><?=($addon->class?' ':'').$addon->class?>">
	<legend><strong><?=htmlspecialchars($addon->name)?></strong></legend>
	<div class="addonbody <?=$addon?>">
	<?=$addon->fetch()?>
	</div>
</fieldset>
<?endif?><?endforeach?>
</div><!-- /contentFrame -->

<div id="footerFrame" class="footerFrame">
<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<input type="button" value="<?= word(46) ?>" onclick="_D.location='<?= $PHprefs['url'] ?>/layout/home.php'" />
	</td>
</tr></table>
</div>

</body>
</html>