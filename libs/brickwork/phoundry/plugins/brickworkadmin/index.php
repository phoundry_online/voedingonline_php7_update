<?php
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';
require_once 'Addon.class.php';
$site_identifier = Utility::arrayValue($_GET, 'site_identifier');
if (empty($site_identifier)) {
	$sql = 'SELECT identifier FROM brickwork_site LIMIT 1';
	$res = DB::getDb()->iQuery($sql);

	if ($res->current()) {
		$site_identifier = $res->first()->identifier;
	} else {
		die('Geen Site Gekozen');
	}
}
$TID = getTID();

checkAccess($TID);

$class = 'BrickBootstrap';
BrickBootstrap::run(Array(
	10	=> Array($class, 'initErrorHandler'),
	20 	=> function() {return Framework::loadSiteByIdentifier("voedingonline");},
	30	=> Array($class, 'initClient'),
	40 	=> Array($class, 'initRepresentation'),
	50 	=> Array($class, 'initFramework')
));

$GLOBALS['buttons'] = array();

define('BRICKWORKADMIN_CUSTOM_ADDON_DIR', rtrim($PHprefs['instDir'], '/').'/custom/brickworkadmin/addons');
define('BRICKWORKADMIN_ADDON_DIR', rtrim(BRICKWORK_DIR, '/').'/phoundry/plugins/brickworkadmin/addons');

$isShortcut = PhoundryTools::isShortcut($TID);
$notes = PhoundryTools::getNotes($TID);

$addon_files = array();

if(is_dir(BRICKWORKADMIN_CUSTOM_ADDON_DIR))
{
	foreach (new DirectoryIterator(BRICKWORKADMIN_CUSTOM_ADDON_DIR) as $file)
	{
		if($file->isFile() && stripos($file, '.class.php'))
		{
			$addon_files['BrickworkAdminAddon_'.substr($file, 0, strripos($file, '.class.php'))] = $file->getPathName();
		}
	}
}

if(is_dir(BRICKWORKADMIN_ADDON_DIR))
{
	foreach (new DirectoryIterator(BRICKWORKADMIN_ADDON_DIR) as $file)
	{
		if($file->isFile() && stripos($file, '.class.php') && !isset($addons['BrickworkAdmin_Addon_'.substr($file, 0, strripos($file, '.class.php'))]))
		{
			$addon_files['BrickworkAdminAddon_'.substr($file, 0, strripos($file, '.class.php'))] = $file->getPathName();
		}
	}
}

$addons = array();
ksort($addon_files);
foreach ($addon_files as $class => $file)
{
	include_once $file;
	$addon = new $class($site_identifier);
	if(!($addon instanceof BrickworkAdminAddon)) {
		throw new Exception($class.' is no extend from BrickworkAdminAddon');
	}
	$addons[$class] = $addon;
}


header('Content-type: text/html; charset=utf-8');
include 'index.tpl.php';