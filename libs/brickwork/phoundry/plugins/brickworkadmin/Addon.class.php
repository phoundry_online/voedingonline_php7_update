<?php
/**
 * Brickwork Admin Addon
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 */
abstract class BrickworkAdminAddon
{
	/**
	 * Name that is shown on the fieldset
	 * @var string
	 */
	public $name;
	
	/**
	 * Classes on the fieldset
	 * @var string
	 */
	public $class = '';
	
	/**
	 * Display the fieldset or not
	 * @var bool
	 */
	public $display = true;
	
	/**
	 * Site Identifier
	 * @var string
	 */
	public $site_identifier;
	
	/**
	 * Construct is called before any output is given
	 *
	 * @param string $site_identifier
	 */
	public function __construct($site_identifier)
	{
		$this->site_identifier = $site_identifier;
	}
	
	/**
	 * Fetch gets called at the place where the addon can output its html
	 *
	 * @return string
	 */
	public function fetch()
	{
		Framework::$representation->assign('this', $this);
		
		if(is_file(BRICKWORKADMIN_CUSTOM_ADDON_DIR.'/'.str_replace('BrickworkAdminAddon_', '', get_class($this)).'.tpl'))
		{
			return Framework::$representation->fetch('file:'.BRICKWORKADMIN_CUSTOM_ADDON_DIR.'/'.str_replace('BrickworkAdminAddon_', '', get_class($this)).'.tpl');
		}
		else if(is_file(BRICKWORKADMIN_ADDON_DIR.'/'.str_replace('BrickworkAdminAddon_', '', get_class($this)).'.tpl'))
		{
			return Framework::$representation->fetch('file:'.BRICKWORKADMIN_ADDON_DIR.'/'.str_replace('BrickworkAdminAddon_', '', get_class($this)).'.tpl');
		}
	}
	
	/**
	 * Querystring Part
	 * 
	 * @param string $name
	 * @param bool $array
	 * @return string|array
	 */
	public function qsPart($name, $array = false)
	{
		return $array ? 
			array('addon', $this->__toString(), $name): 
			'addon['.$this->__toString().']['.$name.']';
	}
	
	/**
	 * Get a $_GET variable that can be set by qsPart
	 * 
	 * @param string $name
	 * @return string
	 */
	public function get($name)
	{
		return Utility::arrayValueRecursive($_GET, $this->qsPart($name, true));
	}
	
	/**
	 * Get a $_POST variable that can be set by qsPart
	 * 
	 * @param string $name
	 * @return string
	 */
	public function post($name)
	{
		return Utility::arrayValueRecursive($_POST, $this->qsPart($name, true));
	}
	
	/**
	 * Returns the addon code
	 * 
	 * @return string
	 */
	public function __toString()
	{
		return str_replace(__CLASS__.'_', '', get_class($this));
	}
}