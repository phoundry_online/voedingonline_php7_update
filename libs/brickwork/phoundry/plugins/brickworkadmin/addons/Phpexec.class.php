<?php
class BrickworkAdminAddon_Phpexec extends BrickworkAdminAddon
{
	public $name = 'PHP Executer';
	public $class = 'toggleable';
	
	public $code;
	
	/**
	 * Construct is called before any output is given
	 *
	 * @param string $site_identifier
	 */
	public function __construct($site_identifier)
	{
		$this->class .= $this->get('open') ? " open" : " closed";
		
		parent::__construct($site_identifier);
		if ($code = $this->post('code')) {
			ob_start();
			eval('?>'.$code);
			$this->code = ob_get_clean();
		}
	}
	
	/**
	 * Fetch gets called at the place where the addon can output its html
	 *
	 * @return string
	 */
	public function fetch()
	{
		return parent::fetch();
	}
}
