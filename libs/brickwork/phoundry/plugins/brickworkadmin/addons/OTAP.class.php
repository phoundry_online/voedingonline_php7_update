<?php
class BrickworkAdminAddon_OTAP extends BrickworkAdminAddon
{
	public $name = 'OTAP';
	public $class = 'toggleable closed';

	public $otap;

	/**
	 * Fetch gets called at the place where the addon can output its html
	 *
	 * @return string
	 */
	public function fetch()
	{
		if (file_exists('/usr/bin/otap')) {
			$this->otap = shell_exec(sprintf('HOME=%1$s /usr/bin/otap show -e %2$s %1$s --no-ansi 2>&1', PROJECT_DIR, getenv('APP_MODE')));
		}

		return parent::fetch();
	}
}
