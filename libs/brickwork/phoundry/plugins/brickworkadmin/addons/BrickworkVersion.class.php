<?php
/**
 * Brickwork Admin Addon to show the Brickwork Version
 *
 * Shows the version information using the Framework::version() method
 *
 * @author        Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 * @copyright    Web Power
 * @created        30 jan 2009
 *
 * @package        Brickwork
 * @subpackage    Brickwork Admin
 */
/**
 * $Id: BrickworkVersion.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class BrickworkAdminAddon_BrickworkVersion extends BrickworkAdminAddon
{
    /**
     * String representation of this class
     *
     * @var    String
     */
    public $name = "Brickwork Version";

    /**
     * CSS class of this part
     *
     * @var    String
     */
    public $class = "";

    /**
     * Contains the Brickwork Version
     *
     * @var    String
     */
    public $brickwork_version;

    /**
     * @var array
     */
    public $extension_versions;

    public function fetch()
    {
        $this->brickwork_version = Framework::version();

        global $WEBprefs;

        if (! empty($WEBprefs['extensions'])) {
            $extension_path = empty($WEBprefs['extensions.Path']) ?
                rtrim(PROJECT_DIR, '/').'/libs' : $WEBprefs['extensions.Path'];

            foreach ($WEBprefs['extensions'] as $extension) {
                $versionFile = sprintf(
                    '%s/%s/VERSION',
                    $extension_path,
                    $extension
                );

                $version = 'unknown';
                if (file_exists($versionFile)) {
                    $version = file_get_contents($versionFile);
                }

                $this->extension_versions[$extension] = $version;
            }
        }

        return parent::fetch();
    }
}