{strip}
	{$this->brickwork_version}
	{if $this->extension_versions != null && count($this->extension_versions) > 0}
		{foreach from=$this->extension_versions item="version" key="extension"}
			<br>
			{$extension}: {$version}
		{/foreach}
	{/if}
{/strip}
