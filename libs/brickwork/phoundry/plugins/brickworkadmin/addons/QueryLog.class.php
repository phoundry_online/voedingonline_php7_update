<?php
class BrickworkAdminAddon_QueryLog extends BrickworkAdminAddon
{
	public $name = 'Query Log';
	public $class = 'toggleable closed';
	
	public $file;
	
	/**
	 * Construct is called before any output is given
	 *
	 * @param string $site_identifier
	 */
	public function __construct($site_identifier)
	{
		parent::__construct($site_identifier);
		
		if($this->get('empty'))
		{
			file_put_contents(TMP_DIR.'DB.query.log', '');
			
			trigger_redirect('?'.Utility::replaceGet($this->qsPart('empty'), null));
		}
		
		$GLOBALS['buttons'][] = array(
			'type'	=> 'button',
			'onclick' => "window.location = ''+$.query.set('".$this->qsPart('empty')."', 1); return false;",
			'value' => 'Clear Query Log',
		);
	}
	
	/**
	 * Fetch gets called at the place where the addon can output its html
	 *
	 * @return string
	 */
	public function fetch()
	{
		$this->file = shell_exec('tail -100 '.TMP_DIR.'DB.query.log');
		// $this->file = array_reverse($this->file);
		$this->file = explode("\n", $this->file);
		return parent::fetch();
	}
}