<?php
class BrickworkAdminAddon_Features extends BrickworkAdminAddon
{
	public $name = 'Features';
	public $class = 'toggleable closed';
	
	public $features;
	
	/**
	 * Construct is called before any output is given
	 *
	 * @param string $site_identifier
	 */
	public function __construct($site_identifier)
	{
		parent::__construct($site_identifier);		
	}
	
	/**
	 * Fetch gets called at the place where the addon can output its html
	 *
	 * @return string
	 */
	public function fetch()
	{
		$this->features[] = "<u>Brickwork</u>";
		$this->features[] = "ACL : ".Features::brickwork('ACL');
		$this->features[] = "Translation : ".Features::brickwork('translation');
		return parent::fetch();
	}
}