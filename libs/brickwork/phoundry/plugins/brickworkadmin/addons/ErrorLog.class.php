<?php
class BrickworkAdminAddon_ErrorLog extends BrickworkAdminAddon
{
	public $name = 'Error log';
	public $class = 'toggleable closed';
	
	/**
	 * Contents of the Errorfile
	 * @var array
	 */
	public $file;
	
	/**
	 * Construct is called before any output is given
	 *
	 * @param string $site_identifier
	 */
	public function __construct($site_identifier)
	{
		parent::__construct($site_identifier);
		
		if($this->get('empty'))
		{
			file_put_contents(TMP_DIR.'error.log', '');
			
			trigger_redirect('?'.Utility::replaceGet($this->qsPart('empty'), null));
		}
		
		$GLOBALS['buttons'][] = array(
			'type'	=> 'button',
			'onclick' => "window.location = ''+$.query.set('".$this->qsPart('empty')."', 1); return false;",
			'value' => 'Clear Error Log',
		);
	}
	
	/**
	 * Fetch gets called at the place where the addon can output its html
	 *
	 * @return string
	 */
	public function fetch()
	{
		$output = shell_exec('tail -100 '.TMP_DIR.'error.log');
		$output = explode("\n", $output);
		$this->file = array_reverse($output);
		return parent::fetch();
	}
}
