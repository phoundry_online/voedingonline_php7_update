<?php
class BrickworkAdminAddon_FileBrowser extends BrickworkAdminAddon
{
	public $name = 'File Browser';
	public $class = 'toggleable closed';
	public $tmpdir;
	public $lang;
	
	/**
	 * Construct is called before any output is given
	 *
	 * @param string $site_identifier
	 */
	public function __construct($site_identifier)
	{
		parent::__construct($site_identifier);
		
		if(!is_null($this->get('directory')))
		{
			$defaults = array(
				'asciiOnly' => $GLOBALS['PHprefs']['charset'] != 'utf-8',
				'rootUrl' => null,
				'width' => null,
				'height' => null,
				'uploadExtensions' => array(),
				'selectExtensions' => array(),
				'allowFiles'   => true,
				'allowFolders' => true,
				'thumbnailWidth' => 0,
				'thumbnailHeight' => 0,
				'allowedWidth' => null,
				'allowedHeight' => null,
				'allowedSize' => null // In Megabytes (float)
			);
			
			switch ($this->get('directory'))
			{
				case 'tmp':
					$_SESSION['jsTree'] = array_merge($defaults, array(
						'rootDir' => realpath(rtrim(TMP_DIR, '/')),
					));
				break;
				case 'cache':
					$_SESSION['jsTree'] = array_merge($defaults, array(
						'rootDir' => realpath(rtrim(CACHE_DIR, '/')),
					));
				break;
			}
			
			self::trigger_redirect($GLOBALS['PHprefs']['url'].'/core/jsTree/?lang='.$GLOBALS['PHSes']->lang.'&amp;_='.mt_rand(0,9999));
		}
	}

    function trigger_redirect($url, $status = 302) {
        $status = (int) $status;
        switch($status) {
            case 301: // echt verplaatst
                $msg = 'Moved Permanently';
                break;
            case 302: // eerste url MOET in de toekomst nog steeds gebruikt worden
                $msg = 'Found';
                break;
            case 303: // see other, niet ondersteunt voor 1.1
                $msg = 'See other';
                break;
            case 307:
                $msg = 'Temporary Redirect';
                break;
            default:
                throw new InvalidArgumentException('Status should be either '.
                    '301, 302, 303 or 307. '.$status.' given.');
        }
        header('HTTP/1.1 '.$status.' '.$msg, true, $status);
        header('URI: '.$url);
        header('Location: '.$url);
        print $msg.":\n".$url."\n";
        exit;
    }
	
	/**
	 * Fetch gets called at the place where the addon can output its html
	 *
	 * @return string
	 */
	public function fetch()
	{
		return parent::fetch();
	}
	
}
