<?php
/**
 * TableInterface
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @uses FileManagerModule
 */
class TableInterface5_1 extends TableInterface
{
	public function export($tid)
	{
		$tids = array($tid);
		$table = array();
		
		$res = mysql_query(sprintf("
			SELECT
				id,
				order_by,
				name,
				description,
				max_records,
				record_order,
				info,
				is_plugin,
				is_service,
				extra
			FROM
				phoundry_table
			WHERE
				id = %d
			",
			$tid
		));
		$table = current($this->dbResultToArray($res));

		$res = mysql_query(sprintf("
			SELECT
				id,
				table_id,
				order_by,
				name,
				description,
				datatype,
				datatype_extra,
				inputtype,
				inputtype_extra,
				required,
				searchable,
				info
			FROM
				phoundry_column
			WHERE
				table_id = %d
			",
			$tid
		));
		$table['phoundry_column'] = $this->dbResultToArray($res);

		$res = mysql_query(sprintf("
			SELECT
				id,
				table_id,
				event,
				code
			FROM
				phoundry_event
			WHERE
				table_id = %d
			",
			$tid
		));
		$table['phoundry_event'] = $this->dbResultToArray($res);

		$res = mysql_query(sprintf("
			SELECT
				id,
				table_id,
				type,
				required,
				group_id,
				name,
				query,
				matchfield,
				wherepart
			FROM
				phoundry_filter
			WHERE
				table_id = %d
			",
			$tid
		));
		$table['phoundry_filter'] = $this->dbResultToArray($res);
		
		// Only export the super user rights as the other groups can be out of sync
		$res = mysql_query(sprintf("
			SELECT
				group_id,
				table_id,
				rights
			FROM
				phoundry_group_table
			WHERE
				table_id = %d
			AND
				group_id = 1
			",
			$tid
		));
		$table['phoundry_group_table'] = $this->dbResultToArray($res);

		return $table;
		
	}
	
}

