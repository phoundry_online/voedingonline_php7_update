<?php
ob_start();
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
$site_identifier = (!empty($_GET['site_identifier'])) ? $_GET['site_identifier'] : NULL;

// Columns changed with phoundry 5.5 check if we find the changed field
$res = mysql_query('SHOW COLUMNS FROM phoundry_table');
$phoundry_5_1 = true;
while($row = mysql_fetch_assoc($res)) {
	if($row['Field'] == 'show_in_menu') $phoundry_5_1 = false;
}

if($phoundry_5_1) {
	require_once 'TableInterface.class.php';
	require_once 'TableInterface5.1.class.php';
	$tableInterface	= new TableInterface5_1;
} else {
	require_once 'TableInterface.class.php';
	$tableInterface	= new TableInterface;
}

$tab			= (!empty($_REQUEST['tab'])?$_REQUEST['tab']:'export');
$action			= (!empty($_POST['action']) && is_array($_POST['action'])) ? key($_POST['action']) : '';
$errors			= array();
$messages		= array();
$to_tables		= $tableInterface->getShowTables();


if(count($_POST) != 0)
{
	switch ($action) {
		case 'export':
			if(empty($_POST['from_table']))
				$errors[] = 'no table selected to copy from';
			
			
			if(count($errors) == 0)
			{
				$table_trees = array();
				foreach($_POST['from_table'] as $tid)
				{
					if(is_numeric($tid)){
						$table_trees[$tid] = $tableInterface->export($tid);
					}
				}
				$varExported = array(
					'phoundry_version' => $PRODUCT['version'],
					'table_trees' => $table_trees,
				);
				ob_clean(); // dump all the output we've already generated
				header('Content-type: text/plain');
				header('Content-Disposition: attachment; filename="table_interface_'.$PRODUCT['version'].'.dat"');
				echo "<?php\n\$varExported = ".var_export($varExported, true).";\n";
				exit(0);
			}
		break;
		
		case 'insert_upload':
			if(!isset($_FILES['from_file']) || !is_uploaded_file($_FILES['from_file']['tmp_name']) || $_FILES['from_file']['error'] != 0)
			{
				// No file upload
				$errors[] = 'no file uploaded to insert';
			} else {
				$insert_upload_file = saverIncludeVarExport($_FILES['from_file']['tmp_name']);
				if(!isset($insert_upload_file['phoundry_version']) || $insert_upload_file['phoundry_version'] != $PRODUCT['version'])
				{
					$messages[] = 'LET OP: Versie van phoundry wijkt af. Huidige versie: "'.$PRODUCT['version'].'" Geimporteerde versie: "'.$insert_upload_file['phoundry_version'].'"';
				}
				$_SESSION['insert_upload_file'] = $insert_upload_file;
			}
				
			if(count($errors) != 0)
			{
				// If we get errors the following variables are invalid
				unset($insert_upload_file);
			}	
		break;
		
		case 'insert_run':
			if(!isset($_SESSION['insert_upload_file']))
			{
				trigger_error("Session data not found!");
			}
			$insert_upload_file = $_SESSION['insert_upload_file'];
			
			if(empty($_POST['to_tables']))
			{
				$errors[] = 'geen doel tabellen';
			}
			
			if(count($errors) == 0)
			{
				foreach($insert_upload_file['table_trees'] as $tid => $table_tree)
				{
					// Only insert the checked tables
					if(!in_array($tid, $_POST['insert_tables'])) continue;
					
					if($tableInterface->insert($table_tree, $_POST['to_tables'][$tid])) {
						$messages[] = langWord($table_tree['description']). ' successfully inserted!';
					} else {
						$errors[] = langWord($table_tree['description']). ' insert failed!';
					}
				}
				unset($insert_upload_file, $_SESSION['insert_upload_file']);
			}
		break;
		
		case 'update_upload':
			if(!isset($_FILES['from_file']) || !is_uploaded_file($_FILES['from_file']['tmp_name']) || $_FILES['from_file']['error'] != 0)
			{
				// No file upload
				$errors[] = 'no file uploaded to update';
			} else {
				$update_upload_file = saverIncludeVarExport($_FILES['from_file']['tmp_name']);
				if(!isset($update_upload_file['phoundry_version']) || $update_upload_file['phoundry_version'] != $PRODUCT['version'])
				{
					$messages[] = 'LET OP: Versie van phoundry wijkt af. Huidige versie: "'.$PRODUCT['version'].'" Geimporteerde versie: "'.$update_upload_file['phoundry_version'].'"';
				}
			}
				
			if(count($errors) != 0)
			{
				// If we get errors the following variables are invalid
				unset($update_upload_file);
			} else {
				
				$current_export = array();
				foreach($update_upload_file['table_trees'] as $tid => $val) {
					$current_export[$tid] = $tableInterface->export($tid);
				}
				
				// create the update statements
				$_SESSION['update_statements'] = 
				$update_statements = 
				$tableInterface->createUpdateStatements($update_upload_file['table_trees'], $current_export);
			}
		break;
		
		case 'update_run':
			if(!isset($_SESSION['update_statements']))
			{
				trigger_error("Session data not found!");
			}
			if(!isset($_POST['update']) || !is_array($_POST['update'])) {
				$errors[] = 'Geen records geselecteerd om te syncen';
			}
			$update_statements = $_SESSION['update_statements']; 
			
			if(count($errors) == 0) {
				foreach($update_statements as $action => $tables) {
					foreach($tables as $table_name => $table_rows) {
						foreach($table_rows as $id => $row) {
							// filter out the rows that aren't selected
							if(isset($_POST['update'][$table_name]) && in_array($id, $_POST['update'][$table_name])) {
								switch ($action) {
									case 'insert':
										mysql_query($tableInterface->insertInto(array($row), $table_name));
										$messages[] = 'Inserted '.$id.' into '.$table_name;
									break;
									case 'update':
										$update_values = array();
										foreach($row as $column => $value) {
											$update_values[] = sprintf(
												"`%s` = %s",
												$column,
												$tableInterface->quoteValue($value)
											);
										}
										mysql_query(sprintf(
											'UPDATE %s SET %s WHERE `%s` = "%s"',
											$table_name,
											implode(', ', $update_values),
											$tableInterface->primary_keys[$table_name],
											$tableInterface->quoteValue($id)
										));
										
										$messages[] = 'Updated '.$id.' in '.$table_name;
									break;
									case 'delete':
										
										mysql_query(sprintf(
											'DELETE FROM `%s` WHERE `%s` = "%s"',
											$table_name,
											$tableInterface->primary_keys[$table_name],
											$tableInterface->quoteValue($id)
										));
										
										$messages[] = 'Deleted '.$id.' from '.$table_name;
									break;
									case 'unchanged':
									break;
									default:
										$errors[] = 'Ongeldige actie gevonden: '.$action;
									break;
								}
							}
						}
					}
				}
				
				unset($update_statements);
			}
		break;
		
		default:
		break;
	}
}

ob_flush();
include 'index.tpl.php';


/**
 * Somewhat saver, since it won't register global variables easy this way
 */
function saverIncludeVarExport($filename)
{
	@include($filename);
	return isset($varExported) ? $varExported : array();
}
