<form action="<?=htmlspecialchars($_SERVER['REQUEST_URI'])?>" enctype="multipart/form-data" method="post">
<fieldset>
	<legend><strong>Tables</strong></legend>
	<select name="from_table[]" multiple="multiple" size="20" style="width: 100%;">
	<?foreach($tables as $value => $text): ?>
	<option value="<?=$value?>"><?=htmlspecialchars($text)?></option>
	<?endforeach?>
	</select>
	<p>
		<input type="submit" value="Export" name="action[export]" />
		<a href="#" onclick="$('select[name=\'from_table[]\'] option').attr('selected', true); return false;">Selecteer alles</a>
	</p>
</fieldset>
</form>
