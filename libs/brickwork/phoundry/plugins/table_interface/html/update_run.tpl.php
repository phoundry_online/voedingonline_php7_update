<?ob_start()?>
<?foreach($update_statements as $action => $action_tables):?>
<?if(!empty($action_tables['phoundry_table'])):?>
<?ob_start()?>
	<?foreach($action_tables['phoundry_table'] as $tid => $phoundry_table):?>
	<?ob_start()?>
			<?ob_start()?>
			<?foreach($update_statements as $act => $act_tables):?>
				<?ob_start()?>
				<?foreach($act_tables['phoundry_column'] as $id => $row): ?>
				<?if($row['table_id'] == $tid):?>
				<div>
					<input type="checkbox" name="update[phoundry_column][]" value="<?=$id?>"  />
					<?=$row['name']?>
				</div>
				<?endif?>
				<?endforeach?>
				<?$capture = ob_get_clean()?>
				<?if(trim($capture) != ''):?>
				<fieldset>
					<legend><?=$act?></legend>
					<?=$capture?>
				</fieldset>
				<?endif?>
			<?endforeach?>
			<?$capture = ob_get_clean()?>
			<?if(trim($capture) != ''):?>
			<fieldset class="columns">
				<legend>
				<input type="checkbox"  onclick="$(':checkbox', $(this).parents('.columns')).attr('checked', this.checked);" />
				Columns
				</legend>
				<?=$capture?>
			</fieldset>
			<?endif?>
			
			<?ob_start()?>
			<?foreach($update_statements as $act => $act_tables):?>
				<?ob_start()?>
				<?foreach($act_tables['phoundry_event'] as $id => $row): ?>
				<?if($row['table_id'] == $tid):?>
				<div>
					<input type="checkbox" name="update[phoundry_event][]" value="<?=$id?>"  />
					<?=$row['event']?>
				</div>
				<?endif?>
				<?endforeach?>
				<?$capture = ob_get_clean()?>
				<?if(trim($capture) != ''):?>
				<fieldset>
					<legend><?=$act?></legend>
					<?=$capture?>
				</fieldset>
				<?endif?>
			<?endforeach?>
			<?$capture = ob_get_clean()?>
			<?if(trim($capture) != ''):?>
			<fieldset class="events">
				<legend>
				<input type="checkbox"  onclick="$(':checkbox', $(this).parents('.events')).attr('checked', this.checked);" />
				Events
				</legend>
				<?=$capture?>
			</fieldset>
			<?endif?>
			
			<?ob_start()?>
			<?foreach($update_statements as $act => $act_tables):?>
				<?ob_start()?>
				<?foreach($act_tables['phoundry_filter'] as $id => $row): ?>
				<?if($row['table_id'] == $tid):?>
				<div>
					<input type="checkbox" name="update[phoundry_filter][]" value="<?=$id?>"  />
					<?=$row['name']?>
				</div>
				<?endif?>
				<?endforeach?>
				<?$capture = ob_get_clean()?>
				<?if(trim($capture) != ''):?>
				<fieldset>
					<legend><?=$act?></legend>
					<?=$capture?>
				</fieldset>
				<?endif?>
			<?endforeach?>
			<?$capture = ob_get_clean()?>
			<?if(trim($capture) != ''):?>
			<fieldset class="filters">
				<legend>
				<input type="checkbox"  onclick="$(':checkbox', $(this).parents('.filters')).attr('checked', this.checked);" />
				Filters
				</legend>
				<?=$capture?>
			</fieldset>
			<?endif?>
			
			<?ob_start()?>
			<?foreach($update_statements as $act => $act_tables):?>
				<?ob_start()?>
				<?foreach($act_tables['phoundry_group_table'] as $id => $row): ?>
				<?if($row['table_id'] == $tid):?>
				<div>
					<input type="checkbox" name="update[phoundry_group_table][]" value="<?=$id?>"  />
					Rights
				</div>
				<?endif?>
				<?endforeach?>
				<?$capture = ob_get_clean()?>
				<?if(trim($capture) != ''):?>
				<fieldset>
					<legend><?=$act?></legend>
					<?=$capture?>
				</fieldset>
				<?endif?>
			<?endforeach?>
			<?$capture = ob_get_clean()?>
			<?if(trim($capture) != ''):?>
			<fieldset class="group_table">
				<?=$capture?>
			</fieldset>
			<?endif?>
			
	<?$capture = ob_get_clean()?>
	<?if($action != 'unchanged' || trim($capture) != ''):?>
	<div>
	<fieldset class="table">
		<legend>
			<input class="update_table_check" name="update[phoundry_table][]" type="checkbox" value="<?=$tid?>" />
			<?=htmlspecialchars($phoundry_table['id'])?>
			:
			<?=htmlspecialchars($phoundry_table['name'])?>
			:
			<?=langWord($phoundry_table['description'])?>
		</legend>
		<?=$capture?>
	</fieldset>
	</div>
	<?endif?>
	<?endforeach?>
	<?$capture = ob_get_clean()?>
	<?if(trim($capture) != ''):?>
		<fieldset class="action <?=$action?>">
			<legend><?=$action?> tabellen</legend>
			<?=$capture?>
		</fieldset>
	<?endif?>
<?endif?>
<?endforeach?>
<?$capture = ob_get_clean()?>
<?if(trim($capture) != ''):?>
<div>
	<form action="<?=htmlspecialchars($_SERVER['REQUEST_URI'])?>" method="post">
	<input type="hidden" name="tab" value="update" />
	<label>Alles: <input type="checkbox"  onclick="$(':checkbox', $(this).parents('div.pane')).attr('checked', this.checked);" /></label>
	<div>
		<?=$capture?>
	</div>
	<div><input type="submit" value="Sync" name="action[update_run]" /></div>
	</form>
</div>
<?else:?>
	Phoundry is in Sync!
	<?include 'update_upload.tpl.php'?>
<?endif?>