<div>
	<form action="<?=htmlspecialchars($_SERVER['REQUEST_URI'])?>" method="post">
	<input type="hidden" name="tab" value="insert" />
	<label>Alles: <input type="checkbox" checked="checked" onclick="$(':checkbox', $(this).parents('div.pane')).attr('checked', this.checked);" /></label>
	<?foreach($insert_upload_file['table_trees'] as $tid => $table_tree):?>
	<div>
		<fieldset>
		<legend>
			<input class="insert_table_check" name="insert_tables[]" type="checkbox" value="<?=$tid?>" checked="checked" />
			<?=htmlspecialchars($table_tree['name'])?>
			:
			<?=langWord($table_tree['description'])?>
		</legend>
		New table:
		<select name="to_tables[<?=$tid?>]">
		<option value=""></option>
		<?foreach($to_tables as $text): ?>
			<option value="<?=$text?>"<?
			if(
				(!isset($_POST['to_tables'][$tid]) && $text == $table_tree['name']) ||
				(isset($_POST['to_tables'][$tid]) && $_POST['to_tables'][$tid] == $text)
			) echo ' selected="selected"';
			?>><?=htmlspecialchars($text)?></option>
		<?endforeach?>
		</select>
		</fieldset>
	</div>
	<?endforeach?>
	<div><input type="submit" value="Import" name="action[insert_run]" /></div>
	</form>
</div>