<?php

$res = mysql_query("
	SELECT
		id as value,
		COALESCE(description, name) as text
	FROM
		phoundry_table
	WHERE
		SUBSTR(name, 1, 9) != 'phoundry_'
	ORDER BY
		id
");
$tables = array();
while($row = mysql_fetch_assoc($res)) $tables[$row['value']] = $row['value'].": ".langWord($row['text']);

include 'html/export.tpl.php';