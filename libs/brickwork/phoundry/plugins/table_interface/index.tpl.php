<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en">
	<head>
		<title>Table Interface Copy</title>
		<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
		<meta name="Author" content="Web Power BV, www.webpower.nl" />
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/tabstrip.css" type="text/css" />
		<!--[if IE]>
		<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
		<![endif]-->
		<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
	<script type="text/javascript">
	//<![CDATA[
	jQuery(function($){
		$('div#tabstrip a').click(function(e){
			var tab = $(this).parent('li'), pane = $('#pane_'+tab.attr('id').replace(/^tab_/, ''));
			if(pane && pane.length > 0) {
				$('div#tabstrip li').removeClass('active');
				tab.addClass('active');
				$('div.contentFrame div.pane').hide();
				pane.show();
				this.blur();
			}
			return false;
		});
	});
	//]]>
	</script>
	</head>
	<body class="frames">
		<div id="headerFrame" class="headerFrame">
			<?= getHeader('Phoundry Sync'); ?>
			<div id="tabstrip" class="tabstrip-bg">
				<ul>
					<li id="tab_export" class="<?=($tab == 'export' ? 'active' : '')?>"><a href="#">Export</a></li>
					<li id="tab_insert" class="<?=($tab == 'insert' ? 'active' : '')?>"><a href="#">Import</a></li>
					<li id="tab_update" class="<?=($tab == 'update' ? 'active' : '')?>"><a href="#">Sync BETA</a></li>
				</ul>
			</div>
		</div>
		<div id="contentFrame" class="contentFrame">
			<?if(!empty($messages)):?>
			<fieldset class="message">
				<legend>Meldingen</legend>
				<?foreach($messages as $message):?>
				<div><?=$message?></div>
				<?endforeach?>
			</fieldset>
			<?endif?>
			<?if(!empty($errors)): ?>
			<fieldset class="errors">
				<legend>Errors</legend>
				<?foreach($errors as $error):?>
				<div><?=htmlspecialchars(ucfirst($error))?></div>
				<?endforeach?>
			</fieldset>
			<?endif?>
			<div id="pane_export" class="pane" style="display: <?=($tab == 'export' ? 'block' : 'none')?>;">
				<?include('export.php')?>
			</div>
			<div id="pane_insert" class="pane" style="display: <?=($tab == 'insert' ? 'block' : 'none')?>;">
				<?include('insert.php')?>
			</div>
			<div id="pane_update" class="pane" style="display: <?=($tab == 'update' ? 'block' : 'none')?>;">
				<?include('update.php')?>
			</div>
			<div>
			</div>
		</div>
		<div id="footerFrame" class="footerFrame">
		<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
			<td>
			</td>
			<td align="right">
			<input type="button" value="<?= word(46) ?>" onclick="_D.location='<?= $PHprefs['customUrls']['home'] ?>'" />
			</td>
		</tr></table>
		</div>
	</body>
</html>
