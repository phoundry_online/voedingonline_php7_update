<?php
/**
 * TableInterface
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 */
class TableInterface
{
	public $primary_keys = array(
		'phoundry_table' => 'id',
		'phoundry_column' => 'id',
		'phoundry_event' => 'id',
		'phoundry_filter' => 'id',
		'phoundry_group_table' => 'table_id',
	);
	
	public function export($tid)
	{
		$tids = array($tid);
		$table = array();
		
		$res = mysql_query(sprintf("
			SELECT
				id,
				order_by,
				name,
				description,
				max_records,
				record_order,
				info,
				is_plugin,
				show_in_menu,
				extra
			FROM
				phoundry_table
			WHERE
				id = %d
			",
			$tid
		));
		$table = current($this->dbResultToArray($res));

		$res = mysql_query(sprintf("
			SELECT
				id,
				table_id,
				order_by,
				name,
				description,
				datatype,
				datatype_extra,
				inputtype,
				inputtype_extra,
				required,
				searchable,
				info
			FROM
				phoundry_column
			WHERE
				table_id = %d
			",
			$tid
		));
		$table['phoundry_column'] = $this->dbResultToArray($res);

		$res = mysql_query(sprintf("
			SELECT
				id,
				table_id,
				event,
				code
			FROM
				phoundry_event
			WHERE
				table_id = %d
			",
			$tid
		));
		$table['phoundry_event'] = $this->dbResultToArray($res);

		$res = mysql_query(sprintf("
			SELECT
				id,
				table_id,
				type,
				required,
				group_id,
				name,
				query,
				matchfield,
				wherepart
			FROM
				phoundry_filter
			WHERE
				table_id = %d
			",
			$tid
		));
		$table['phoundry_filter'] = $this->dbResultToArray($res);
		
		// Only export the super user rights as the other groups can be out of sync
		$res = mysql_query(sprintf("
			SELECT
				group_id,
				table_id,
				rights
			FROM
				phoundry_group_table
			WHERE
				table_id = %d
			AND
				group_id = 1
			",
			$tid
		));
		$table['phoundry_group_table'] = $this->dbResultToArray($res);
		
		return $table;
	}
	
	public function insert($table, $name = null)
	{
		$table['id'] = '';
		if(!is_null($name)) $table['name'] = $name;
		$res = mysql_query($this->insertInto(array($table), 'phoundry_table'));
		$new_tid = mysql_insert_id();

		foreach($table['phoundry_column'] as $k => $v)
		{
			$table['phoundry_column'][$k]['id'] = '';
			$table['phoundry_column'][$k]['table_id'] = $new_tid;
		}
		mysql_query($this->insertInto($table['phoundry_column'], 'phoundry_column'));
		
		foreach($table['phoundry_event'] as $k => $v)
		{
			$table['phoundry_event'][$k]['id'] = '';
			$table['phoundry_event'][$k]['table_id'] = $new_tid;
		}
		mysql_query($this->insertInto($table['phoundry_event'], 'phoundry_event'));

		foreach($table['phoundry_filter'] as $k => $v)
		{
			$table['phoundry_filter'][$k]['id'] = '';
			$table['phoundry_filter'][$k]['table_id'] = $new_tid;
		}
		mysql_query($this->insertInto($table['phoundry_filter'], 'phoundry_filter'));
		
		foreach($table['phoundry_group_table'] as $k => $v)
		{
			$table['phoundry_group_table'][$k]['table_id'] = $new_tid;
		}
		mysql_query($this->insertInto($table['phoundry_group_table'], 'phoundry_group_table'));
		
		return true;
	}
	
	/**
	 * Delete the nested tables from their parents and add em all in one multidimensional array
	 *
	 * @param array $export
	 * @return array
	 */
	public function exportToUpdateArray($export)
	{
		$new_array = array(
			'phoundry_table'	=> array(),
			'phoundry_column'	=> array(),
			'phoundry_event'	=> array(),
			'phoundry_filter'	=> array(),	
			'phoundry_group_table' => array(),
		);
		
		foreach($export as $tid => $table_tree) {
			$table = array();
			foreach($table_tree as $key => $val) {
				if(!is_array($val)) {
					$table[$key] = $val;
				} else {
					// Extract subtable, we just go 1 deep
					foreach($val as $row) {
						$new_row = array();
						foreach($row as $k => $v) {
							if(!is_array($v)) $new_row[$k] = $v;
						}
						if(key_exists($key, $new_array) && key_exists($key, $this->primary_keys)) {
							// Us the primary key as array key
							$pk = $this->primary_keys[$key];
							
							if(isset($new_row[$pk])) {
								$new_array[$key][$new_row[$pk]] = $new_row;
							}
						}
					}
				}
			}
			
			if(!empty($table)) $new_array['phoundry_table'][$tid] = $table;
		}
		
		return $new_array;
	}
	
	/**
	 * Create the set of update statements
	 *
	 * @param array $export_file
	 * @param array $export_current
	 */
	public function createUpdateStatements($export_file, $export_current)
	{
		// Make sure we got flattened out arrays
		if(!key_exists('phoundry_column', $export_file)) $export_file = $this->exportToUpdateArray($export_file);
		if(!key_exists('phoundry_column', $export_current)) $export_current = $this->exportToUpdateArray($export_current);
		
		// Init the arrays
		$unchanged =
		$delete =
		$insert =
		$update = array();
		
		
		// Find the update, inserts and unchanged rows
		foreach($export_file as $table_name => $table_rows) {
			$insert[$table_name] =
			$update[$table_name] =
			$unchanged[$table_name] = array();

			foreach($table_rows as $id => $row) {
				// If its not in the current export we need to insert it.
				if(!key_exists($id, $export_current[$table_name])) {
					$insert[$table_name][$id] = $row;
					continue;
				}
				
				$diff = array_diff_assoc($row, $export_current[$table_name][$id]);
				if(count($diff) != 0) {
					$update[$table_name][$id] = $row;
					continue;
				}
				
				// Apprantly it's unchanged
				if($table_name == 'phoundry_table') $unchanged[$table_name][$id] = $row;
			}
		}
		
		// find the deleted stuff
		foreach($export_current as $table_name => $table_rows) {
			$delete[$table_name] = array();
			
			foreach($table_rows as $id => $row) {
				// If its not in the file export we need to delete it.
				if(!key_exists($id, $export_file[$table_name])) {
					$delete[$table_name][$id] = $row;
					continue;
				}
			}
		}		
		
		return compact(
			'insert',
			'update',
			'delete',
			'unchanged'
		);
	}
	
	
	/**
	 * Get the SHOW TABLES from the db
	 *
	 * @return array
	 */
	public function getShowTables()
	{
		$res = mysql_query('SHOW TABLES');
		$to_tables = $this->dbResultToArray($res);
		foreach($to_tables as $k => $row) $to_tables[$k] = current($row); // make it a normal array
		return $to_tables;
	}
	
	/**
	 * Create Insert Into statement
	 *
	 * @param array $rows Multi deminsional array
	 * @param string $table
	 * @return string
	 */
	public function insertInto(array $rows, $table = '')
	{
		if(!empty($rows) && is_array(current($rows)))
		{
			$values = array();
			$fields = array();
			$first_run = true;
			
			foreach($rows as $row)
			{
				$row_values = array();
				foreach($row as $field => $value)
				{
					if(!is_array($value))
					{
						if($first_run) $fields[] = mysql_escape_string($field);
						$row_values[] = $this->quoteValue($value);
					}
				}
				$values[] = "(".implode(", ", $row_values).")";
				$first_run = false;						
			}
			
			$sql = sprintf('
				INSERT INTO
				%s (%s)
				VALUES
				%s;',
				mysql_escape_string($table),
				"`".implode("`, `", $fields)."`",
				implode(",\n", $values)
			);
			
			return $sql;
		}
	}
	
	/**
	 * Quote the database values
	 *
	 * @param string $value
	 * @return string
	 */
	public function quoteValue($value)
	{
		if(is_null($value)) return 'NULL';
		
		if(is_numeric($value)) return $value;

		return "'".mysql_escape_string($value)."'";
	}
	
	/**
	 * Create a multideminsional array from a mysql result
	 *
	 * @param resource $result
	 * @return array
	 */
	protected function dbResultToArray($result)
	{
		$return_array = array();
		while($row = mysql_fetch_assoc($result)) {
			$return_array[] = $row;				
		}
		return $return_array;
	}
}