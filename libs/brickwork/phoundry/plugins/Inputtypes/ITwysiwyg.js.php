<?php
	// geen header. Deze file wordt geinclude vanuit /phoundry/custom/Inputtypes/ITwysiwyg.js.php
	require_once "{$PHprefs['distDir']}/core/include/common.php"; // nodig voor de words() functie
?>

function getURIParameterValue(uri, parameterName)
{
  var queryStr = uri.split("?")[1];
  var params = queryStr.split("&");
  for (var i=0; i < params.length; i++)
  {
    var keyValue = params[i].split("=");
    if(keyValue[0] == parameterName)
    {
      return keyValue[1];
    }
  } 
  return "";
}

PhoundryEditor.prototype.linkIntern=function() {
	var A=[];
	var site_identifier = getURIParameterValue(self.location.href,'site_identifier');
	var page_content_id = getURIParameterValue(self.location.href,'page_content_id');
	
	if(site_identifier == ''){
		return;
	}
	var url = '<?= $PHprefs['url'] ?>/custom/brickwork/Inputtypes/link_intern.php?site_identifier='+site_identifier+'&page_content_id='+page_content_id+'&'+Math.random();
	$.fn.InlinePopup({width:400,height:300,title:'<?= word(10150) /* Link intern */?>',url:url,modal:true,event:null});
}

PhoundryEditor.prototype.linkInternText = function(link, title){
   if($.browser.msie){
      this.saveCursor();
      var el = this._tempLink();
      el.href = link;
   } else {
      this.insertHTML('<a href="' + link + '" title="' + title + '">', '</a>', 1);
   }
}
