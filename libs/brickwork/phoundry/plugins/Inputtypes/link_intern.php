<?php
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once INCLUDE_DIR.'/functions/autoloader.include.php';
	require_once 'WebPower/Smarty/Smarty.class.php';
	require_once 'WebPower/Smarty/plugins/compiler.fun.php';
	
	checkAccess();
	
	$site_identifier = !empty($_GET['site_identifier']) ? $_GET['site_identifier'] : '';
	$page_content_id = !empty($_GET['page_content_id']) && is_numeric($_GET['page_content_id']) ? (int)$_GET['page_content_id'] : '';
	
	if(empty($site_identifier))
	{
		PH::phoundryErrorPrinter(word(11800) /* no site selected */, true);
	}
	//
	$lang = null;
	$staging_status = 'live';

	if ((int) $page_content_id > 0) {
		$sql = sprintf('SELECT staging_status FROM brickwork_page WHERE id = (SELECT page_id FROM brickwork_page_content WHERE id = %d LIMIT 1)', $page_content_id);
		$res = DB::iQuery($sql);
		if (isError($res))
			trigger_error('Error in SQL statement: ' . $res->errstr, E_USER_ERROR);
		$staging_status = $res->first()->staging_status;
	}
	
	// check site language
	// TODO: feature check?
	$site = Framework::loadSiteByIdentifier($site_identifier);
	$language_list = array();
	if ( Framework::$site->isMultiLingual() )
	{
		// language select tonen
		foreach(Framework::$site->availableLang as $l)
		{
			$language_list[$l->code] = $l; // TODO: translate?
		}
		
		// if we have a page content id, we can look back for language id in page table
		if($page_content_id>0 && empty($_GET['lang']))
		{
			$sql = sprintf('SELECT lang_id FROM brickwork_page WHERE id = (SELECT page_id FROM brickwork_page_content WHERE id = %d LIMIT 1)', $page_content_id);
			$res = DB::iQuery($sql);
			if ( isError($res) )
				trigger_error('Error in SQL statement: ' . $res->errstr , E_USER_ERROR);
			
			$lang = Site_Language::getSiteLanguageById($res->first()->lang_id);
		}
		else
		{
			if(empty($_GET['lang']) OR !isset($language_list[$_GET['lang']]))
			{
				if(count(Framework::$site->availableLang)>0)
				{
					print 'Kies een taal:<br />';
					foreach($language_list as $code=>$l)
					{
						printf('<a href="?%s">%s</a><br />', QS(1,'lang='.$code), $l->code);
					}
					
					exit;
				}
				else
				{
					$lang = first(Framework::$site->availableLang);
				}
			}
			else
			{
				$lang = $language_list[$_GET['lang']];
			}
		}
		// load language to framework for site structure purpose
		Framework::$site->currentLang = $lang;
	}
	
	$sitestructure = SiteStructure::getSiteStructure($site_identifier, $lang);
	$stack = SiteStructure::getSiteStack($site_identifier);
	
	// collect all structure id's and determine whats connected
	$structure_ids = array();
	foreach($stack as $item)
	{
		$structure_ids[] = (int)$item->id;
	}
	$pages = SiteStructure::getPages($structure_ids);
	$testpages = SiteStructure::getTestPages($structure_ids);
	$urls  = SiteStructure::getUrls($structure_ids);
	
	$menu_shortcuts = array();
	$menu = array();
	
	$invisible_parents = array();
	
	foreach($stack as $item)
	{
		if(!isset($urls[$item->id]) && !isset($pages[$item->id])) {
			continue;
		}

		$testpage = false;
		if (isset($testpages[$item->id]) && $testpages[$item->id] == $pages[$item->id]) {
			$testpage = true;
		}
		if ($staging_status == 'live' && $testpage) {
			continue;
		}

		if(!empty($pages[$item->id]))
		{
			if(Framework::$site->isMultiLingual())
			{
				$url = '/page/'.$lang->code.'/'.SiteStructure::getPathByStructureId($item->id, $site_identifier, $lang);
			}
			else
			{
				$url = '/page/'.SiteStructure::getPathByStructureId($item->id, $site_identifier);
			}
			$target = '_self';
		}
		else if(!empty($urls[$item->id]))
		{
			$url = $urls[$item->id]['link'];
			$target = $urls[$item->id]['target'];
		}
		if(empty($item->parent))
		{
			$m = new Menu($item->name,$url,$item->name,0);
			$m->id = $item->id;
			$m->target = $target;
			$m->selected = $item->isActive;
			$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
			$menu[$item->id] = $m;
			$menu_shortcuts[$item->id] = $m;
			$sitemap->items[] = $m;
		}
		else
		{
			$m = new Menu($item->name,$url,$item->name,1);
			$m->id = $item->id;
			$m->target = $target;
			$m->selected = $item->isActive;
			$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
			$menu_shortcuts[$item->parent]->items[] = $m;
			$menu_shortcuts[$item->id] = $m;
		}
		if ($testpage) {
			$m->type = 'test';
		}
	}
	
	if (!is_dir(TMP_DIR . "/templates_c/Phoundry/Inputtypes"))
	{
		mkdir(TMP_DIR . "/templates_c/Phoundry/Inputtypes", 0777, true);
	}
	
	$tpl = new Smarty();
	$tpl->template_dir	= dirname(__FILE__);
	$tpl->compile_dir	= TMP_DIR . '/templates_c/Phoundry/Inputtypes/';
	$tpl->cache_dir		= false;
	
	$tpl->register_compiler_function('fun', 'smarty_compiler_fun');
	$tpl->register_compiler_function('/defun', 'smarty_compiler_defun_close');
	$tpl->register_postfilter('smarty_postfilter_defun');
	
	$tpl->assign('phoundryUrl', $PHprefs['url'] );
	$tpl->assign('menu', $menu);
	$tpl->assign('SITE', Framework::$site);
	$tpl->assign('language_select', $language_list);
	$tpl->display('link_intern.tpl');

/**
 * Test if an object is of type error
 *
 * @return Boolean
 */
function isError(&$e) {
    $v = PHP_VERSION;

    switch (TRUE) {
        // old php
        case ($v < 4.2):
            if ((get_class($e) == 'error')
                ||	(is_subclass_of($e, 'error'))) {
                return TRUE;
            }
            break;

        // php 4.2 < 5
        case ($v >= 4.2 && $v < 5):
            return (is_a($e, 'Error'));
            break;

        // php 5
        case ($v >= 5):
            return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
            break;
    }

    // not an error
    return FALSE;
}