<html>
<head>
<title>Link intern</title>
<link rel="stylesheet" href="{$phoundryUrl}/core/Inputtypes/wysiwyg/dialog.css.php?bg=buttonface" type="text/css" />
<script type="text/javascript" src="{$phoundryUrl}/core/csjs/jquery.pack.js"></script>
<script type="text/javascript" src="{$phoundryUrl}/core/csjs/jquery-exts.js"></script>
{literal}
	<script type="text/javascript">
	//<![CDATA[ 

		function slide(el) {

			// Get div
			var divElement = gE("sub_"+el.title);			
			
			// Check attrib
			if(!divElement.getAttribute('slideStatus')) {
				// Set default slideStatus
				divElement.setAttribute("slideStatus", "closed");
			} 

			// Get slideStatis
			var slideStatus = divElement.getAttribute('slideStatus');

			// Check status
			if(slideStatus == 'open') {
				
				// Div was open, slide up
				$("div#sub_"+el.title+"").slideUp("fast");
				
	
				// Set new status
				divElement.setAttribute("slideStatus", "closed");
			
			} else if(slideStatus == 'closed') {
				
				// Div was closed or else... slideDown
				$("div#sub_"+el.title+"").slideDown("fast");

				// Set new status
				divElement.setAttribute("slideStatus", "open");		

			} else {
				// Wrong status
				return false;
			}

			// Set arrows
			els = document.getElementsByTagName("a");
			for(var i = 0; i < els.length; i++) {
				if(els[i].title == el.title) {
					if(els[i].className == 'klapperClosed' || els[i].className == 'klapperOpen') {
						if(slideStatus == 'closed') {
							els[i].className = 'klapperOpen';
						} else {
							els[i].className = 'klapperClosed';
						}
						
					}
				}
			}

			return true;
		}

	
	//]]> 
	</script>
<script type="text/javascript">
//<![CDATA[

_W = window; _D = document;

function submitMe(uri, title) {
	parent._curED.linkInternText(uri, title);
	parent.killPopup();
}


//]]>
</script>
<style type="text/css">
ul {
	margin-top: 0px;
	margin-bottom: 0px;
}
</style>
{/literal}
</head>
<body onload="$('fieldset').slideDown('slow');">
<div style="padding: 5px;">
<div style="float:left; font-weight:bold">Site structuur</div>
{if count($language_select)>0}
	<div style="text-align: right">
	Taal: 
	{foreach from=$language_select item=lang}
		{if $lang->code == $SITE->currentLang->code}
			<strong>{$lang->code}</strong>
		{else}
			<a href="?{$smarty.server.QUERY_STRING|regex_replace:'/\&lang=\w+/':''}&lang={$lang->code}">{$lang->code}</a>
		{/if}
		
	{/foreach}
	</div>
{/if}
<form>
<br />
<div>
	{defun name="sitemap" list=$menu}
		{foreach from=$list item=element}
			{if !is_null($element->parent) }
				<li>
			{/if}
			{if $element->type == 'test'}
				<i>
			{/if}
				<a href="#" onclick="submitMe('{$element->link}','{$element->title|htmlspecialchars|replace:"'":"\'"}'); return false;" target="{$element->target|default:"_self"}">{$element->title|htmlspecialchars}</a>
			{if $element->type == 'test'}
				</i>
			{/if}
				<br />
				{if count($element->items)>0 }
					<ul>
						{fun name="sitemap" list=$element->items}
					</ul>
				{/if}
			{if !is_null($element->parent) }
				</li>
			{/if}
		{/foreach}
	{/defun}
</div>


</form>
</div>
</body>
</html>
