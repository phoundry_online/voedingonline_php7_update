<?php
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';
require_once '../phoundryplugin/PhoundryPlugin.php';
require_once 'PageTypePlugin.php';

$plugin = new PageTypePlugin();
$plugin->loadData();