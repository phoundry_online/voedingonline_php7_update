<?php
class PageTypePlugin extends PhoundryPlugin
{
	public function thumbAction($page_type_code, $width = 200, $height = 200)
	{
		$url = "http".(!empty($_SERVER['HTTPS']) ? "s":"")."://".
			$_SERVER['HTTP_HOST'];
			
		$path = substr($_SERVER['PHP_SELF'], 0,
			strpos($_SERVER['PHP_SELF'], $_SERVER['PATH_INFO']));
		
		$url .= $path."/html/".$page_type_code;
		
		$hash = sha1($url.$width.$height);
		
		$cache = Cache::factory("dir://page_type_thumbs");
		if(!isset($cache[$hash])) {
			include 'WebPower_Thumbnail_Service.php';
			$thumbservice = new WebPower_Thumbnail_Service();
			$cache[$hash] = $thumbservice->getImage($url, $width, $height, 800, 600);
		}
		header('Content-type: image/jpeg');
		echo $cache[$hash];
	}
	
	public function htmlAction($page_type_code)
	{
		$page_type = ActiveRecord::factory("Model_Page_Type", $page_type_code);
		if(!$page_type->isLoaded()) {
			header("404 File not Found", true, 404);
			echo "404 File not Found";
			exit;
		}
		
		$site = $page_type->template->sites->first();
		if(false === $site) {
			header("x", true, 404);
			exit;
		}
		
		echo $page_type->getContentSpecification($site);
	}
}