<?php
$inc = @include('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

if(!empty($_GET['TID'])) {
	$tid = (int) $_GET['TID'];
	
	$row = DB::iQuery(sprintf("
		SELECT *
		FROM phoundry_table
		WHERE id = %d
		LIMIT 1", $tid))->first();
	
	
	if($row) {
		$newrow = array(
			'id' => $row->id,
			'extra' => serialize(array(
				'sUrl' => 'custom/brickwork/brickworkadmin/',
				'rolerights_global' => false,
				'rolerights_detail' => false,
			))
		);
		DB::updateRows(array($newrow), 'phoundry_table');
		
	}
}

trigger_redirect($PHprefs['url'].'/custom/brickwork/brickworkadmin/?'.
	$_SERVER['QUERY_STRING']);