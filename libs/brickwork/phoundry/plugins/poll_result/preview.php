<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";

	$TID = getTID();
	$RID = $_GET['RID'];

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'view');

	$phoundry = new phoundry($TID);

	function getPollResult() {
		global $db, $RID;

		$sql = sprintf('
			SELECT
				answer.id as id,
				answer.answer as answer,
				count(vote.answer_id) as nrVote
			FROM
				module_content_poll_answer answer
			INNER JOIN
				module_content_poll_question question
			ON
				question.id = answer.poll_id AND site_identifier="%s"
			LEFT JOIN
				module_content_poll_vote vote
			ON
				answer.id = vote.answer_id
			WHERE
				answer.poll_id = %d
			GROUP BY
				answer.id
		',
			escDBquote($_GET['site_identifier']),
			$RID
		);

		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		$result = array();
		for ($x = 0; !$db->EndOfResult($cur); $x++) {
			$result[] = array(
								'answer' => $db->FetchResult($cur,$x,'answer'),
								'nrVote' => $db->FetchResult($cur,$x,'nrVote'),
							);
		}

		// bereken het aantal keer dat er gestemt is
		$totalVote = 0;
		for($x=0; $x < count($result); $x++) {
			$totalVote += $result[$x]['nrVote'];
		}

		// Bereken het getal om de percentages uit te rekenen
		if($totalVote>0){
			$mod = 100 / $totalVote;
		} else {
			$mod = 0;
		}

		foreach($result as $idx => $value) {
			$tmp[] = array(
							'answer' => $value['answer'],
							'nrVote' => $value['nrVote'],
							'procent' => round($value['nrVote']*$mod,1),
						);
		}

		$res = array();
		$res['data'] = $tmp;
		$res['nrVote'] = $totalVote;
		$res['poll'] = getPoll($RID);
		return $res;
	}

	function getPoll($id) {
		global $db, $RID;
		$question = '';
		$sql = "
			SELECT
				module_content_poll_question.question
			FROM
				module_content_poll_question
			WHERE
				module_content_poll_question.id = " . (int)$id . "
			LIMIT 1
		";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if(!$db->EndOfResult($cur)) {
			$question = $db->FetchResult($cur,0,'question');
		}
		return $question;
	}

	$poll_results = getPollResult();

header('Content-Type: text/html;charset=' . $PHprefs['charset']);
?>
<?= '<?xml version="1.0" encoding="' . $PHprefs['charset'] . '"?>' . "\n" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Phoundry</title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="../../core/csjs/global.js.php"></script>
<style type="text/css">
img.balk {
	filter:alpha(style=0,opacity=80);
	-moz-opacity:0.8;
	opacity:0.8;
	-khtml-opacity:0.8;
	filter: progid:DXImageTransform.Microsoft.Alpha(opacity = 80);
	background: red;
	height: 15px;
}
</style>
</head>
<body class="frames">
<div class="headerFrame">
<?= getHeader('Poll result', $poll_results['poll']); ?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->

<?php

if($poll_results['nrVote'] == 0) {
	print word(11500) /*'Er is nog niet gestemd op deze poll'*/;
}
else {
	print '<div style="width: 300px; border:2px solid #000;background: url(bg.gif);">';
	print '<table cellpadding="0" cellspacing="0" width="100%">';
	foreach($poll_results['data'] as $idx => $value) {
		print '<tr><td bgcolor="#ffffff" style="height:17px;padding-left:2px;"><strong>' .$value['answer'] . ' <br /><nobr>(' . htmlspecialchars($value['procent']) . '% - ' . htmlspecialchars($value['nrVote']) . ' ' . word(11501) /* stemmen */ .')</nobr></td></tr>';
		print '<tr><td style="height:28px;">';
		$tmp = '<img src="px.gif" class="balk" style="width:';
			if($value['procent'] == 0) {
				$tmp .= '3';
			}
			else {
				$tmp .= round($value['procent']*3);
			}
			$tmp .= 'px;" vspace="0" hspace="0" /><br />';
			print $tmp;
	}
	print '</td></tr></table>';
	print '</div>';
	print '<br />';
	print word(11502,$poll_results['nrVote']) /* 'In totaal is er <strong>' . $poll_results['nrVote'] . '</strong> keer gestemd.<br />' */;
}
?>

<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	</td>
	<td align="right">
	<button onclick="_D.location='<?= $phoundry->rUrl ?>?<?= QS(1) ?>'"><?= htmlspecialchars(word(46)) ?></button>
	</td>
</tr></table>
</div>

</body>
</html>
