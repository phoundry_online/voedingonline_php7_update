<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
	require_once INCLUDE_DIR.'/functions/autoloader.include.php';

	$TID = getTID();
	$RID = (!empty($_GET['RID'])) ? $_GET['RID'] : NULL;

	// Make sure the current user in the current role has access to this page:
	checkAccess();

	if(empty($TID) || empty($RID)){
		trigger_error('TID or RID required', E_USER_ERROR);
	}

	$sql = sprintf('SELECT name FROM phoundry_table WHERE id=%d AND is_plugin=0', $TID);
	$res = DB::iQuery($sql);

	if($res->num_rows == 0){
		trigger_error('Config table does not exist', E_USER_ERROR);
	} else {
		//$table_name = $res->result[0]->name;
		$row = $res->first();
		$table_name = $row->name;
		$sql = sprintf('SELECT COUNT(*) as config_records FROM %s WHERE page_content_id = %d', DB::quote($table_name), $RID);
		$res = DB::iQuery($sql);
		
		if($res->num_rows == 0){
			trigger_error('Query failed', E_USER_ERROR);
		} 
		else 
		{
			$row = $res->first();
			if($row->config_records == 1){
				// update
				$page = 'update';
			} else {
				// insert
				$page = 'insert';
			}
			$location = sprintf('Location: %s/core/%s.php?%s', $PHprefs['url'], 
							// values
							$page, 
							QS(0));

			header('HTTP/1.1 302 Moved');
			header($location);
			exit;
		}
	}
?>