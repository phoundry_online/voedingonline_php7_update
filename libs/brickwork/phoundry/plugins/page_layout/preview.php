<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
	require_once INCLUDE_DIR.'/functions/autoloader.include.php';

	$page_id = (!empty($_GET['RID'])) ? $_GET['RID'] : NULL;
	$site_identifier = (!empty($_GET['site_identifier'])) ? $_GET['site_identifier'] : NULL;

	$TID = getTID();

	// Make sure the current user in the current role has access to this page:
	checkAccess();

	// Instantiate Phoundry opbject:
	// $phoundry = new Phoundry($TID);

	if(empty($page_id)) { 
		PH::phoundryErrorPrinter(word(11801) /* Record not found */, true);
	}
	if(empty($site_identifier)) {
		PH::phoundryErrorPrinter(word(11800) /* no site selected */, true);
	}


// hier eerst ophalen:

// site info + template (dmv site_identifier)
$sql = sprintf('
					SELECT
						p.id as page_id,
						p.title as page_title,
						pt.code as page_type_code,
						pt.name,
						pt.contentspecification,
						ss.id as site_structure_id,
						p.staging_status,
						s.protocol, 
						s.domain
					FROM
						brickwork_site_structure ss
					INNER JOIN
						brickwork_page p ON p.site_structure_id = ss.id
					INNER JOIN
						brickwork_page_type pt ON pt.code = p.page_type_code
					INNER JOIN
						brickwork_template t ON t.identifier = pt.template_identifier
					INNER JOIN 
						brickwork_site s ON s.identifier = ss.site_identifier 
					WHERE
						ss.site_identifier ="%s" AND p.id=%d
					',
					// values
					DB::quote($site_identifier),
					$page_id
					);
$res = DB::iQuery($sql);

if($res->num_rows != 1 ){
	trigger_error('Pagina niet gevonden', E_USER_ERROR);
}

//$page = $res->result[0];
$page = $res->first();

if(TESTMODE){
	$host = $_SERVER['HTTP_HOST'];
} else {
	$host = $page->domain;
}

$test_vars = array('pid'=>$page->page_id, 'sid'=>$page->site_structure_id, 'staging'=>$page->staging_status);
$hash = BrickWorkCrypt::urlEncode($test_vars);

$url = sprintf('%s://%s/index.php/page/%d/?brickwork_test=%s', $page->protocol, $host, $page->site_structure_id, $hash);
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Pagina layout</title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>

<style type="text/css">

#center {
    width: 180px;
    float: left;
    margin-left: 5px;
}

#unused_modules {
    width: 130px;
    float: left;
    margin-left: 5px;
}

form {
  clear: left;
}

br {
	clear: left;
}

li.exists {
	background-color: #D0FFCC!important;
}

li.new {
	/*color: red;*/
}

</style>

<link rel="stylesheet" href="dd_files/lists.css" type="text/css" />
<script type="text/javascript" src="dd_files/coordinates.js"></script>
<script type="text/javascript" src="dd_files/drag.js"></script>
<script type="text/javascript" src="dd_files/dragdrop.js"></script>
<script type="text/javascript">
//<![CDATA[
var opened = false;
<? printf("opened = window.open('%s');", $url);?>

if(opened){
	history.go(-1);
}

//]]>
</script>
</head>
<body class="frames">

<div class="headerFrame">
<?= getHeader($page->page_title); 
?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td id="checkTD">
</td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->

<script type="text/javascript">
//<![CDATA[
if(!opened){
	_D.write('<h1 class="req">U heeft uw popup-blocker aanstaan.</h1>');
	_D.write('<? printf('<a href="%s" target="_blank">open een nieuw venster</a>', $url);?>');
}

//]]>
</script>
<a href="">

<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="Ok" onclick="isDirty=false" class="okBut" />
	</td>
	<td align="right">

	</td>
</tr></table>
</div>

</body>
</html>
