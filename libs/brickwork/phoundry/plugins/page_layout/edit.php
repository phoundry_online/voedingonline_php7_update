<?php
/**
 * Edit pagina
 *
 * @author Unknown
 * @author Mick van der Most van Spijk
 * @version 1.0.1
 * @changes:
 * v1.0.1 Containers wrappen titel niet meer
 */
$inc = @include('PREFS.php');
if ($inc === false) {
    require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

$page_id = (!empty($_GET['page_id'])) ? $_GET['page_id'] : NULL;
$site_identifier = (!empty($_GET['site_identifier'])) ? $_GET['site_identifier'] : NULL;
$HASACL = (isset($WEBprefs['acl']['enable']) ? $WEBprefs['acl']['enable'] : true );
$TID = getTID();

// Make sure the current user in the current role has access to this page:
checkAccess();

// Instantiate Phoundry opbject:
// $phoundry = new Phoundry($TID);


if(empty($page_id)) {
    PH::phoundryErrorPrinter(word(11801) /* Record not found */, true);
}
if(empty($site_identifier)) {
    PH::phoundryErrorPrinter(word(11800) /* no site selected */, true);
}

// hier eerst ophalen:

// site info + template (dmv site_identifier)
if ( $HASACL )
    $sql = sprintf('
						SELECT
							p.id as page_id,
							p.title as page_title,
							pt.code as page_type_code,
							pt.name,
							pt.contentspecification,
							ss.id as site_structure_id,
							ao.id as auth_object_id
						FROM
							brickwork_site_structure ss
						INNER JOIN
							brickwork_page p ON p.site_structure_id = ss.id
						INNER JOIN
							brickwork_page_type pt ON pt.code = p.page_type_code
						INNER JOIN
							brickwork_template t ON t.identifier = pt.template_identifier
						LEFT OUTER JOIN brickwork_auth_object ao
						  ON (ao.foreign_id = ss.id AND ao.class = "SiteStructure")
						WHERE
							ss.site_identifier ="%s" AND p.id=%d
						',
        // values
        DB::quote($site_identifier),
        $page_id
    );
else
    $sql = sprintf('
						SELECT
							p.id as page_id,
							p.title as page_title,
							pt.code as page_type_code,
							pt.name,
							pt.contentspecification,
							ss.id as site_structure_id
						FROM
							brickwork_site_structure ss
						INNER JOIN
							brickwork_page p ON p.site_structure_id = ss.id
						INNER JOIN
							brickwork_page_type pt ON pt.code = p.page_type_code
						INNER JOIN
							brickwork_template t ON t.identifier = pt.template_identifier
						WHERE
							ss.site_identifier ="%s" AND p.id=%d
						',
        // values
        DB::quote($site_identifier),
        $page_id
    );

$res = DB::iQuery($sql);

if($res->num_rows != 1 ){
    PH::phoundryErrorPrinter(word(11801) /* Record not found */, true);
}

//$page = $res->result[0];
$page = $res->first();

// content containers ophalen
$sql = sprintf('
					SELECT
						cc.code
					FROM
						brickwork_content_container cc
					WHERE
						cc.page_type_code ="%s"
					',
    // values
    DB::quote($page->page_type_code)
);

$res = DB::iQuery($sql);

if($res->num_rows == 0 ){
    trigger_error('Geen content containers voor deze pagina gevonden');
}

$content_containers = array();
foreach($res as $cc)
{
    $content_containers[$cc->code] = array();
    $content_containers[$cc->code]['allowed_modules']	= array();
    $content_containers[$cc->code]['placed_modules']	= array();
}

// a.d.v. het pagetype de modules ophalen die voor dit pagina type gekoppeld mogen worden
# (table: module, lookup_container_allows_module, content_container, page_type)

$sql = sprintf('
					SELECT
						m.*
					FROM
						brickwork_content_container cc
					INNER JOIN
						brickwork_content_container_allows_module lcam ON lcam.content_container_id = cc.id
					INNER JOIN
						brickwork_module m ON m.code = lcam.module_code
					WHERE
						cc.page_type_code ="%s"
					',
    // values
    DB::quote($page->page_type_code)
);

$res = DB::iQuery($sql);

if($res->num_rows == 0 ){
    trigger_error('Geen modules gevonden voor deze pagina');
}
$modules = array();
while($res->current()){
    $mod = $res->current();
    if(is_numeric($mod->name)) {
        $mod->name = word($mod->name);
    }
    $modules[$mod->code] = $mod;
    $res->next();
}

// Ophalen van beschikbare modules en hun classes
$sql = 'SELECT code, class FROM brickwork_module';

$res = DB::iQuery($sql);
$moduleClasses = Array();
while($res->current()) {
    $row = $res->current();
    $moduleClasses[$row->code] = $row->class;
    $res->next();
}

// ophalen welke modules er in welke content container passen
$sql = sprintf('
					SELECT
						cc.code,
						lcam.module_code
					FROM
						brickwork_content_container cc
					INNER JOIN
						brickwork_content_container_allows_module lcam ON lcam.content_container_id = cc.id
					WHERE
						cc.page_type_code ="%s"
					',
    // values
    DB::quote($page->page_type_code)
);

$res = DB::iQuery($sql);
if($res->num_rows == 0 ){
    trigger_error('Geen modules gevonden voor deze pagina');
}


while($res->current()){
    $combi = $res->current();
    if(isset($content_containers[$combi->code])){
        $content_containers[$combi->code]['allowed_modules'][] = $combi->module_code;
    }
    $res->next();
}


// verwerken van de POST met nieuwe pagina indeling
if(!empty($_POST['module_version'])){
    handlePost($_POST['module_version'], $_POST, $page);
}


function handlePost($version, $post, $page){
    global $modules, $moduleClasses, $HASACL;

    switch($version){
        case 1:
            if(!empty($post['order'])){
                $sql = array();
                $prio = 50;

                $post_containers = explode(':', $post['order']);
                $containers = array();
                foreach($post_containers as $p_cont){
                    if(preg_match('/([a-z0-9_]+)\(([a-z0-9,_-]+)\)/i', $p_cont, $match)){
                        $containers[$match[1]] = explode(',', $match[2]);
                    }
                }
                if(count($containers)>0){
                    foreach($containers as $container_code => $post_modules)
                    {

                        switch($container_code){
                            case 'unused_modules':
                                foreach($post_modules as $module)
                                {
                                    $mod = explode('-', $module);
                                    $action = ($mod[2] != 'new') ? 'delete' : 'ignore';
                                    if($action == 'delete')
                                    {
                                        ActiveRecord::factory('Model_Page_Content', $mod[2])->delete();

                                        // PE AUTH!
                                        if ( $HASACL )
                                            removeModule( $moduleClasses[$mod[0]], $mod[2] );
                                        // EOA
                                    }
                                }
                                break;
                            default:

                                // PE AUTH! content container als parent maken
                                if ( $HASACL )
                                    $contentAuthID = addPageContentContainer( $page, $container_code );
                                // EOA

                                foreach($post_modules as $module)
                                {
                                    $mod = explode('-', $module);
                                    $action = ($mod[2] == 'new') ? 'insert' : 'update';
                                    $prio = $prio + 10;

                                    if($action == 'insert'){
                                        $sql[] = sprintf('
															INSERT INTO brickwork_page_content (
																content_container_code,
																page_id,
																module_code,
																prio,
																staging_status,
																respect_auth,
																online_date,
																offline_date,
																state
																) VALUES (
																"%s",
																%d,
																"%s",
																%d,
																"%s",
																%d,
																NOW(),
																DATE_ADD(NOW(), INTERVAL 10 YEAR),
																"ok"
																)',
                                            // values
                                            DB::quote($container_code),
                                            DB::quote($page->page_id),
                                            DB::quote($mod[0]),
                                            DB::quote($prio),
                                            'live',
                                            1
                                        );
                                        if ( $HASACL )
                                            $sql[] = "-- addModule( '{$moduleClasses[$mod[0]]}', %s, {$contentAuthID});";
                                    }
                                    else
                                    {
                                        $sql[] = sprintf('
															UPDATE
																brickwork_page_content
															SET
																content_container_code="%s",
																prio = %d
															WHERE
																id = %d AND page_id = %d
															',
                                            // values
                                            DB::quote($container_code),
                                            DB::quote($prio),
                                            $mod[2],
                                            $page->page_id
                                        );

                                        if ( $HASACL )
                                            addModule( $moduleClasses[$mod[0]], $mod[2], $contentAuthID);
                                    }

                                }
                            // end of default

                        } // end switch

                    }// end foreach containers

                    if(count($sql)>0){
                        foreach($sql as $query){
                            if ( substr($query,0,2) == '--' )
                            {
                                $query = sprintf('$status = ' . substr($query,2), $res->lastInsertId);
                                eval($query);

                            }
                            else
                            {
                                $res = DB::iQuery($query);
                                if(isError($res)){
                                    var_dump($res);exit;
                                }
                            }
                        }
                    }


                } // end count containers

            }
            break;
        default:
            die('invalid version');
    }
}

function warnConfig($page_content_id, $tid){
    $sql = sprintf('SELECT name FROM phoundry_table WHERE id=%d AND is_plugin=0', $tid);
    $res = DB::iQuery($sql);

    if($res->num_rows == 0){
        return 0;
        //trigger_error('Config table does not exist', E_USER_ERROR);
    } else {
        $table_name = $res->result[0]->name;
        $sql = sprintf('SELECT COUNT(*) as config_records FROM %s WHERE page_content_id = %d', DB::quote($table_name), $page_content_id);
        $res = DB::iQuery($sql);
        if($res->num_rows == 0){
            trigger_error('Query failed', E_USER_ERROR);
        } else {
            if($res->result[0]->config_records == 0){
                return 1;
            }
        }
    }
    return 0;
}

function warnContent($page_content_id, $tid){
    $sql = sprintf('SELECT name FROM phoundry_table WHERE id=%d AND is_plugin=0', $tid);
    $res = @DB::iQuery($sql);

    if($res->num_rows == 0){
        return 0;
        //trigger_error('Content table does not exist', E_USER_ERROR);
    } else {
        $table_name = $res->result[0]->name;
        $sql = sprintf('SELECT COUNT(*) as content_records FROM %s WHERE page_content_id = %d', DB::quote($table_name), $page_content_id);
        $res = @DB::iQuery($sql);

        if(isError($res)) {
            return 0;
        }

        if($res->num_rows == 0){
            // geen content specifieke tabel
            return 0;
        } else {
            if($res->result[0]->content_records == 0){
                return 1;
            }
        }
    }
    return 0;
}

// ophalen welke modules er al gekoppeld zitten in deze pagina
# (table: page_content, gesorteerd op prio)


// PE AUTH! Functies voor aanpassingen in object tabel
// @TODO: fix als containers verwijderd worden.

function addPageContentContainer( $page, $content_container )
{
    $instanceid = 'PageContentHandler/' . $page->page_id . '/' . $content_container;

    $sql = sprintf('INSERT INTO brickwork_auth_object (parent_id, instanceid, description, class, foreign_id ) ' .
        " VALUES(%s, '%s','%s', '%s', NULL) " .
        " ON DUPLICATE KEY UPDATE parent_id = %s, description = '%s'",
        $page->auth_object_id,
        $instanceid,
        $content_container,
        'ContentContainer',
        $page->auth_object_id,
        $content_container
    );

    $res = DB::iQuery($sql);

    if (isError($res))
        trigger_error( sprintf("Query '%s' failed: %s", $sql, $res->errstr) );

    $sql = sprintf("SELECT id FROM brickwork_auth_object WHERE instanceid = '%s'", $instanceid);
    $res = DB::iQuery($sql);

    $row = $res->first();
    return $row->id;
}

function addModule( $class, $module_id, $parent_auth_object_id)
{
    $instanceid = $class . '/' . $module_id;

    $sql = sprintf('INSERT INTO brickwork_auth_object (parent_id, instanceid, description, class, foreign_id ) ' .
        " VALUES(%s, '%s','%s', '%s', NULL) " .
        " ON DUPLICATE KEY UPDATE parent_id = %s",
        $parent_auth_object_id,
        $instanceid,
        $class,
        $class,
        $parent_auth_object_id
    );

    $res = DB::iQuery($sql);

    if (isError($res))
        trigger_error( sprintf("Query '%s' failed: %s", $sql, $res->errstr) );
    return $res->lastInsertId;
}

function removeModule( $class, $module_id )
{
    $sql = sprintf("SELECT id FROM brickwork_auth_object WHERE instanceid = '%s/%s'", $class, $module_id);
    $res = DB::iQuery($sql);

    if($row = $res->first())
    {
        $id  = $row->id;

        $sql = sprintf("DELETE FROM brickwork_auth_object WHERE instanceid = '%s/%s'", $class, $module_id );

        $res = DB::iQuery($sql);

        if (isError($res))
            trigger_error("Query '%s' failed: %s", $sql, $res->errstr );

        $sql = sprintf("DELETE FROM brickwork_auth_right WHERE auth_object_id = '%s'", $id );

        $res = DB::iQuery($sql);

        if (isError($res))
            trigger_error("Query '%s' failed: %s", $sql, $res->errstr );
    }
    return true;
}

function isError(&$e) {
    $v = PHP_VERSION;

    switch (TRUE) {
        // old php
        case ($v < 4.2):
            if ((get_class($e) == 'error')
                ||  (is_subclass_of($e, 'error'))) {
                return TRUE;
            }
            break;

        // php 4.2 < 5
        case ($v >= 4.2 && $v < 5):
            return (is_a($e, 'Error'));
            break;

        // php 5
        case ($v >= 5):
            return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
            break;
    }

    // not an error
    return FALSE;
}

// EOA

$sql = sprintf('
	SELECT
		pc.*
	FROM
		brickwork_page_content pc
	WHERE
		pc.page_id = %d
	ORDER BY
		pc.prio ASC
	',
    // values
    DB::quote($page->page_id)
);

$res = DB::iQuery($sql);

if($res->num_rows > 0 ){

    while($res->current()){
        $pm = $res->current();
        if(array_key_exists($pm->module_code, $modules) &&
            isset($content_containers[$pm->content_container_code])) {
            $content_containers[$pm->content_container_code]['placed_modules'][] = array(
                'module_id' => $pm->id,
                'module_code'=>$pm->module_code,
                'page_content_id'=>$pm->id,
                'warnConfig'=>warnConfig($pm->id, $modules[$pm->module_code]->ptid_config),
                'warnContent'=>warnContent($pm->id, $modules[$pm->module_code]->ptid_content)
            ); // array content container z'n placed modules
        }
        $res->next();
    }
}

// algemene modules aanmaken in javascript


// Haal PTD op van page_content
$sql = 'SELECT id FROM phoundry_table WHERE name=\'brickwork_page_content\'';
$result = DB::iQuery($sql);

if($result->num_rows > 0) {
    $ptid_page_content = $result->result[0]->id;
} else {
    $ptid_page_content = 'null';
}

define('PTID_PAGE_CONTENT', $ptid_page_content);
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
    <meta name="Author" content="Web Power BV, www.webpower.nl" />
    <title>Pagina layout</title>
    <link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
    <!--[if IE]>
    <link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
    <![endif]-->
    <script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>

    <style type="text/css">

        #center {
            width: 180px;
            float: left;
            margin-left: 5px;
        }

        #unused_modules {
            width: 130px;
            float: left;
            margin-left: 5px;
        }

        form {
            clear: left;
        }

        br {
            clear: left;
        }

        li.exists {
            background-color: #D0FFCC!important;
        }

        li.new {
            /*color: red;*/
        }

        div.koppie {
            overflow: hidden;
            white-space: nowrap;
        }
    </style>

    <link rel="stylesheet" href="dd_files/lists.css" type="text/css" />
    <script type="text/javascript" src="dd_files/coordinates.js"></script>
    <script type="text/javascript" src="dd_files/drag.js"></script>
    <script type="text/javascript" src="dd_files/dragdrop.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        function gup( name )
        {
            var regexS = "[\\?&]"+name+"=([^&#]*)";
            var regex = new RegExp( regexS );
            var tmpURL = window.location.href;
            var results = regex.exec( tmpURL );
            if( results == null )
                return "";
            else
                return results[1];
        }


        function showValue()
        {
            order = _D.getElementById("order");
            alert(order.value);
        }


        function ModuleHandler(){
            this.version = 1;
            this.containers = new Array();
            this.dirtyHash = false;

            // menu container
            <?
            foreach($content_containers as $code=>$cc){
                printf("this.containers['%s']=Array();\n", $code);
                printf("this.containers['%s']['placed_modules']=new Array();\n", $code);

                $allowed_modules = count($cc['allowed_modules'])>0 ? "'".implode("','", $cc['allowed_modules'])."'" : '';

                printf("this.containers['%s']['allowed_modules']= new Array(%s);\n", $code, $allowed_modules);
                foreach($cc['placed_modules'] as $index=>$pm){
                    printf("this.containers['%s']['placed_modules'][%d]=new Array();\n", $code, $index);
                    printf("this.containers['%s']['placed_modules'][%d]['module_id']='%s';\n", $code, $index, escSquote($pm['module_id']));
                    printf("this.containers['%s']['placed_modules'][%d]['code']='%s';\n", $code, $index, escSquote($pm['module_code']));
                    printf("this.containers['%s']['placed_modules'][%d]['page_content_id']=%d;\n", $code, $index, $pm['page_content_id']);
                    printf("this.containers['%s']['placed_modules'][%d]['warnConfig']=%d;\n", $code, $index, $pm['warnConfig']);
                    printf("this.containers['%s']['placed_modules'][%d]['warnContent']=%d;\n", $code, $index, $pm['warnContent']);
                }
            }
            ?>

            this.modules= new Array();
            <?
            foreach($modules as $module){
                $js = sprintf('
		// %s
		this.modules["__CODE__"]=Array();
		this.modules["__CODE__"]["code"] = "%s";
		this.modules["__CODE__"]["name"] = "%s";
		this.modules["__CODE__"]["max_per_page"] = %d;
		this.modules["__CODE__"]["in_page"] = %d;
		this.modules["__CODE__"]["ptid_content"] = %s;
		this.modules["__CODE__"]["ptid_config"] = %s;
		this.modules["__CODE__"]["ptid_page_content"] = %s;
	',
                    // values
                    $module->class,
                    $module->code,
                    $module->name,
                    empty($module->max_per_page) ? -1 : $module->max_per_page,
                    0,
                    !empty($module->ptid_content) ? $module->ptid_content : 'null',
                    !empty($module->ptid_config) ? $module->ptid_config : 'null',
                    PTID_PAGE_CONTENT
                );
                $js = str_replace('__CODE__', $module->code, $js);
                print $js;

            }
            ?>


            this.createModule = function(code, p, cid, wCf, wCt){
                var place_in	= p	|| 'unused_modules';
                var content_id	= cid	|| null;
                var warnConfig	= wCf	|| 0;
                var warnContent= wCt	|| 0;

                if (!this.createAllowed(code)){
                    return false;
                }
                var m = this.modules[code];
                m.in_page++;
                var id = m.in_page;
                var container = _D.getElementById(place_in);
                var newLi = _D.createElement('li');
                newLi.id = code +'-'+ id + '-';
                newLi.id += cid ? cid : 'new';

                newLi.lastContainer_id = place_in;
                if(place_in != 'unused_modules'){
                    newLi.className = 'exists';
                } else {
                    newLi.className = 'new';
                }

                var name = m.name;
                name += (id>1) ? ' ' : '';

                var text = _D.createTextNode(name);
                var d = _D.createElement('div');

                d.setAttribute('class', 'koppie');
                d.appendChild(text);
                newLi.appendChild(d);

                var linkjes = _D.createElement('span');
                if(place_in == 'unused_modules'){
                    linkjes.style.display='none';
                }

                // MODULE CONFIG
                if(m.in_page != null && m.ptid_page_content != null){

                    var moduleLink = _D.createElement('a');

                    moduleLink.href='<?= $PHprefs['url'] ?>/core/update.php?TID='+m.ptid_page_content+'&RID='+content_id+'&popup=close'+'&site_identifier='+gup('site_identifier');
                    moduleLink.onclick=function(event){makePopup(event,-40,-40,'Configuratie',this.href);return false};
                    var img = _D.createElement("img");
                    img.src= (warnContent == 1) ? 'pics/module.gif' : 'pics/module.gif';
                    img.border=0;
                    moduleLink.appendChild(img);
                    var br = _D.createElement('br');
                    moduleLink.appendChild(br);
                    linkjes.appendChild(moduleLink);
                }

                // MODULE CONTENT
                if(m.ptid_content != null){
                    var contentLink = _D.createElement('a');
                    contentLink.href='<?= $PHprefs['url'] ?>/core/records.php?TID='+m.ptid_content+'&page_content_id='+content_id+'&popup=close&site_identifier='+gup('site_identifier');
                    contentLink.onclick=function(event){makePopup(event, -40,-40,'Content',this.href);return false};
                    var img = _D.createElement("img");
                    img.src= (warnContent == 1) ? 'pics/warnContent.gif' : 'pics/content.gif';
                    img.border=0;
                    contentLink.appendChild(img);
                    var br = _D.createElement('br');
                    contentLink.appendChild(br);
                    linkjes.appendChild(contentLink);
                }

                // MODULE CONTENT CONFIG
                if(m.ptid_config != null){
                    var configLink = _D.createElement('a');
                    configLink.href='config.php?TID='+m.ptid_config+'&RID='+content_id+'&page_content_id='+content_id+'&popup=close'+'&site_identifier='+gup('site_identifier');
                    configLink.onclick=function(event){makePopup(event,-40,-40,'Configuratie',this.href);return false};
                    var img = _D.createElement("img");
                    img.src= (warnConfig == 1) ? 'pics/warnConfig.gif' : 'pics/config.gif';
                    img.border=0;
                    configLink.appendChild(img);
                    linkjes.appendChild(configLink);
                }
                newLi.appendChild(linkjes);

                // user drag drop object
                DragDrop.makeItemDragable(newLi);

                // add module to container
                container.appendChild(newLi);
                return true;
            }

            // First initial creation of the available modules
            // param string Module code
            this.loadAvailableModules = function(){
                // load modules
                for (var i in this.modules){
                    var m = this.modules[i];
                    if (this.createAllowed(m.code))
                    {
                        this.createModule(m.code);
                    }
                }
            }

            this.loadContainers = function(f){
                var first = f || false;

                // first load content conainers
                for (var c in this.containers){
                    if(first){
                        var placed = this.containers[c].placed_modules;
                        for (var p in placed){
                            this.createModule(placed[p].code, c, placed[p].page_content_id, placed[p].warnConfig, placed[p].warnContent);
                        }
                    }
                    this.loadSingleContainer(c);
                }
                this.loadAvailableModules();
                this.loadSingleContainer('unused_modules');
                if(first){
                    this.dirtyHash = DragDrop.serData('g1', null);
                }
            }

            this.loadSingleContainer = function(c){
                var list = _D.getElementById(c);
                DragDrop.makeListContainer( list, 'g1' );
                if(list){
                    list.onDragOver = function() { this.style["background"] = "#EEF"; };
                    list.onDragOut = function() { this.style["background"] = "none"; };
                }
                return true;
            }

            // check wheter a creation of a module is allowed
            // param string Module code
            this.createAllowed = function(code){
                if(!this.modules[code]) return false;
                var m = this.modules[code];
                return (m.in_page<m.max_per_page || m.max_per_page == -1);
            }

            this.dropModule = function(parentNode, mod){
                var myID = mod.id.split('-');
                // rare javascript evalueert direct de hele if, dus ff voorkomen dat ie niet de unused checkt in aparte if
                // iemand hier een oplossing voor?
                if(parentNode.id != 'unused_modules'){
                    if(!js_in_array(myID[0], this.containers[parentNode.id]['allowed_modules'])){
                        // mag niet in deze container
                        alert('Module not allowed for this container');
                        return false;
                    }
                }

                if(parentNode.id == 'unused_modules'){
                    mod.childNodes[1].style.display='none';
                    var lis  = mod.parentNode.getElementsByTagName('li');
                    var found = new Array();
                    var remove = new Array();
                    for(var x in lis) {
                        try {
                            var unused_mod = lis[x].id.split('-');
                            if(found[unused_mod[0]] && (typeof lis[x].className !='undefined' && lis[x].className != 'exists')){
                                // remove
                                remove[remove.length] = lis[x].id;
                            }
                            found[unused_mod[0]] = true;
                        } catch(e) {};
                    }

                    for(var i in remove) {
                        try {
                            this.modules[myID[0]].in_page--;
                            var item = _D.getElementById(remove[i]);
                            item.parentNode.removeChild( item );
                        } catch(e) {};
                    }
                }
                if(this.dirtyHash != DragDrop.serData('g1', null)){
                    isDirty = true;
                }

                if (parentNode.id != 'unused_modules') {
                    // I was dropped on a content container, so show me the linkjes!

                    if(mod.lastContainer_id != parentNode.id){
                        this.createModule(myID[0]);
                        isDirty = true;
                    }

                    if(mod.className == 'exists'){
                        mod.childNodes[1].style.display='block';
                    }
                }
                mod.lastContainer_id = parentNode.id;
                return true;
            }

            // Highlight allowed containers
            // Parameter: mod	Module which is dragged
            // 			  drop	is module Droped?
            this.highlightAllowedContainers = function(mod, drop)
            {
                var myID = mod.id.split('-');
                for (var c in this.containers)
                {
                    var allowed = true;
                    if (!js_in_array(myID[0], this.containers[c].allowed_modules))
                    {
                        allowed = false;
                    }
                    if (document.getElementById(c))
                    {
                        if (drop)
                        {
                            document.getElementById(c).style.borderColor = 'gray';
                        }
                        else if (! allowed)
                        {
                            document.getElementById(c).style.borderColor = '#f00';
                        }
                        else
                        {
                            document.getElementById(c).style.borderColor = '#3f3';
                        }
                    }
                }
            }

            // create post string
            this.postModules = function()
            {
                var order = _D.getElementById("order");
                order.value = DragDrop.serData('g1', null);
                var ver = _D.getElementById("module_version")
                ver.value = this.version;
            }
            // end of module handler
        }

        function js_in_array(the_needle, the_haystack){
            var the_hay = the_haystack.toString();
            if(the_hay == ''){
                return false;
            }
            var the_pattern = new RegExp(the_needle, 'g');
            var matched = the_pattern.test(the_haystack);
            return matched;
        }

        var ModuleHandlerObj = new ModuleHandler();
        var isDirty = false;

        //]]>
    </script>
</head>
<body class="frames" onload="ModuleHandlerObj.loadContainers(true)" onbeforeunload="if(isDirty)return '<?= escSquote(word(79)) ?>'">
<form action="edit.php?<?=QS(1);?>" method="post" onsubmit="ModuleHandlerObj.postModules();">
    <input type="hidden" name="site_identifier" value="<?=$site_identifier?>" />
    <input type="hidden" name="page_id" value="<?=$page_id?>" />
    <input type="hidden" name="module_version" id="module_version" value="" />
    <input type="hidden" name="order" id="order" value="" />

    <div class="headerFrame" id="headerFrame">
        <?= getHeader($page->page_title);
        ?>
        <table class="buttons" cellspacing="0" cellpadding="2" id="headerButtons"><tr>
                <td id="checkTD">
                </td>
            </tr></table>
    </div>

    <div class="contentFrame" id="contentFrame">
        <!-- Content -->


        <?
        print $page->contentspecification;
        ?>


        <ul id="unused_modules" class="sortable boxy" style="min-height: 50px">

        </ul>

        <!-- /Content -->
    </div>

    <div class="footerFrame" id="footerFrame">
        <table class="buttons" cellspacing="0" cellpadding="2" id="footerButtons"><tr>
                <td>
                    <input type="submit" value="<?=word(82)/* Ok */?>" onclick="isDirty=false" class="okBut" />
                </td>
                <td align="right">
                    <?if(!empty($_GET['returnPage'])):?>
                        <input type="submit" value="<?=word(45)/* Back */?>" onclick="isDirty=false; window.location = '<?=$_GET['returnPage']?>'; return false;" class="okBut" />
                    <?endif?>
                </td>
            </tr></table>
    </div>
</form>
</body>
</html>
