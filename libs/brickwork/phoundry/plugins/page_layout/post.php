<?PHP
if(!empty($_POST['module_version'])){
	switch($_POST['module_version']){
		case 1:
			if(!empty($_POST['order'])){

				$post_containers = explode(':', $_POST['order']);
				$containers = array();
				foreach($post_containers as $p_cont){
					if(preg_match('/([a-z0-9_]+)\(([a-z0-9,_-]+)\)/', $p_cont, $match)){
						$containers[$match[1]] = explode(',', $match[2]);
					}
				}
				if(count($containers)>0){
					foreach($containers as $container_code => $modules){
						switch($container_code){
							case 'unused_modules':
							foreach($modules as $module){
								$mod = explode('-', $module);
								$action = ($mod[2] != 'new') ? 'delete' : 'ignore';
								print "{$action} {$mod[0]} {$mod[2]} in {$container_code}<br />\n";
							}
							break;
							default:
							foreach($modules as $module){
								$mod = explode('-', $module);
								$action = ($mod[2] == 'new') ? 'insert' : 'update';
								print "{$action} {$mod[0]} in {$container_code}<br />\n";
							}
							// end of default

						} // end switch

					}// end foreach containers

				} // end count containers
				
			}
		break;
		default:
			die('invalid version');
	}
}
?>