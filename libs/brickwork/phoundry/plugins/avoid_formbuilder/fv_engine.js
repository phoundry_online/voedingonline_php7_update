var imgPath = './';
var _W=window,_D=document;
_setEvent(_W,'load',_initCheck);

function _word()
{
	var args = _word.arguments, re, word = words[args[0]], x;
	for (x = 1; x < args.length; x++) {
		re = new RegExp('#' + x);
		word = word.replace(re, args[x]);
	}
	return word;
}

function _setEvent(el,evt,fn) {
	if (el.addEventListener)
		el.addEventListener(evt,fn,false);
	else if (el.attachEvent)
		el.attachEvent('on'+evt,fn);
}

function _initCheck() {
	var x, y, el, F, alt;
	for (x = 0; x < _D.forms.length; x++) {
		F = _D.forms[x];
		if (F.getAttribute('alt') != 'fv') continue;
		//_setEvent(F, 'submit', _formCheck);

		for (y = 0; y < F.elements.length; y++) {
			el = F.elements[y];
			alt = el.getAttribute('alt');
			if (!alt) continue;
			var props = alt.split('|'), req = props[0] == 1;
			if (el.type == 'text' || el.type == 'textarea' || el.type == 'password')
				_setEvent(el,'blur',_checkField);
			if (el.type == 'select-one' || el.type == 'select-multiple')
				_setEvent(el,'change',_checkField);
			if (el.type == 'checkbox')
				_setEvent(el,'click',_checkField);
			if (req)
				_checkField(null,el,'fv-req');
		}
	}
}

function _getValue(el, dt) {
	var value, tmp;
	if ('textarea|hidden|password|select-multiple|select-one'.indexOf(el.type) != -1)
		value = el.value;
	else if ('radio|checkbox'.indexOf(el.type) != -1) {
		tmp = el;
		if (tmp.length) {
			for(i=0;i<tmp.length;i++)
				if(tmp[i].checked) {value=tmp[i].value;break}
		}
		else
			value = tmp.checked ? tmp.value : '';
	}
	value = value.replace(/^[\s]+/g,'').replace(/[\s]+$/g,''); // Trim
	if (dt == 'url' && value == 'http://')
		value = '';
	return value;
}

function _modClass(el, state, className) {
	var classes = el.className.split(' '), hasFV = false, x;
	for (x = 0; x < classes.length; x++) {
		if (/^fv\-/.test(classes[x])) {
			classes[x] = (state) ? className : '';
			hasFV = true;
			break;
		}
	}
	if (!hasFV && state)
		classes[classes.length] = className;

	className = ''
	for (x = 0; x < classes.length; x++) {
		if (x > 0) className += ' ';
		className += classes[x];
	}
	el.className = className;
}

function _checkField(e, el, className) {
	var el = el ? el : e.srcElement ? e.srcElement : e.target ? e.target : null;
	if (!el) return;

	if (!className) className = 'fv-forget';
	var alt = el.getAttribute('alt'), props = alt.split('|'), req = props[0] == 1, dt = props[1], value = _getValue(el, dt);
	if (req)
		_modClass(el, value == '', className);
}

function _formCheck(F) {
	var y, msg = '';

	for (y = 0; y < F.elements.length; y++) {
		var el = F.elements[y], alt = el.getAttribute('alt');
		if (!alt) continue;
		var desc, props = alt.split('|'), req = props[0] == 1, dt = props[1], value = _getValue(el, dt), desc = el.getAttribute('title');
		if (!desc) desc = el.name;
		// Check required:
		if (req && value == '')
			msg += _word(6, desc) + '\n';

		// Check maxlength:
		if (props[2]) {
			if (value.length > props[2]) {
				msg += _word(9, desc, value.length, props[2]) + '\n';
				_modClass(el, 1, 'fv-syntax');
			}
			else
				_modClass(el, 0, 'fv-syntax');
		}

		// Check syntax:
		if (value != '' && dt) {
			// Call syntax-check function (if it exists):
			eval('fn = typeof _checkDT'+dt);
			if (fn == 'function') {
				eval('fn = function(desc,value){return _checkDT'+dt+'(desc,value)}');
				syntaxMsg = fn(desc,value);
				if (syntaxMsg) {
					msg += syntaxMsg + '\n';
					_modClass(el, 1, 'fv-syntax');
				}
				else
					_modClass(el, 0, 'fv-syntax');
			}
		}
	}

	if (msg == '')
		return true;
	alert(msg);
	return false;
}

function _checkDTdate(desc, value) {
	var D, M, Y, tmp;
	
	D = parseInt(value.substring(dateFormat.indexOf('DD'), dateFormat.indexOf('DD')+2),10);
	M = parseInt(value.substring(dateFormat.indexOf('MM'), dateFormat.indexOf('MM')+2),10)-1;
	Y = parseInt(value.substring(dateFormat.indexOf('YYYY'), dateFormat.indexOf('YYYY')+4),10);
	tmp = new Date(Y,M,D);
	if (!((tmp.getFullYear()==Y) && (tmp.getMonth()==M) && (tmp.getDate()==D)))
		return _word(3, desc);
}

function _checkDTemail(desc, value) {
	if (!/^([a-z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-z0-9\-]+\.)+))([a-z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value))
		return _word(4, desc);
}

function _checkDTzipcode(desc, value) {
	var re = new RegExp('^' + zipcodeFormat + '$');
	if (!re.test(value))
		return _word(2, desc);
}

function _checkDTurl(desc, value) {
	if (!/^(((http|https|ftp):\/\/)|(news:)|(mailto:)).+$/i.test(value))
		return _word(5, desc);
}

function _checkDThexcolor(desc, value) {
	if (!/^#[ABCDEF0123456789]{6}$/i.test(value))
		return _word(7, desc);
}

function _checkDTfloat(desc, value) {
	if (!/^-?[0-9]+([\.,][0-9]+)?$/.test(value))
		return _word(2, desc);
}

function _checkDTinteger(desc, value) {
	if (!/^-?[0-9]+$/.test(value))
		return _word(0, desc);
}

function _checkDTtime(desc, value) {
	var re = new RegExp('^([0-9]{2}):([0-9]{2})$');
	if (!re.test(value) || RegExp.$1 > 23 || RegExp.$2 > 59)
		return _word(8, desc);
}
