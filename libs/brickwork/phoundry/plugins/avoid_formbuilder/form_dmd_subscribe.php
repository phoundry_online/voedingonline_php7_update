<?php
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once "duallist.php";

	if(empty($_GET['site_identifier'])) {
		phoundryErrorPrinter('No site identifier', TRUE);
	}

	##
	## Get all DMdelivery SOAP Campaign
	##
	$soapCampaigns = array();
	$sql = sprintf("
		SELECT	id,
					campaign_title AS title
		FROM		brickwork_dmdelivery_soap_campaign
		WHERE		site_identifier = '%s'
	",
		$_GET['site_identifier']
	);
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($cur)) {
		for($x=0; !$db->EndOfResult($cur); $x++) {
			$soapCampaigns[$db->FetchResult($cur,$x,'id')] = $db->FetchResult($cur,$x,'title');
		}
	}


	//php2javascript
?>
<html>
<head>
<title><?= word(10103); ?></title>
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function init() {
	var F = _D.forms['phoundry'], extra, Pextra;
	extra = {'soapCampaigns':''}, Pextra;
	if (_P.curExtra != '') {
		eval('Pextra = ' + _P.curExtra);
		if (Pextra['type'] == 'dmd_subscribe') {
			extra = Pextra;
		}
	}

	var soapCampaigns = extra['soapCampaigns'].split('|');
	var curOptions = F.all_FsoapCampaigns.options;
	for(var x=0; x<soapCampaigns.length; x++) {
		for(var i=0; i<curOptions.length; i++) {
			if(curOptions[i].value == soapCampaigns[x]) {
				F.all_FsoapCampaigns.selectedIndex = i;
				moveTo('FsoapCampaigns','right');
			}
		}
	}

 	// alert(F.all_FsoapCampaigns.options[0].value);

	//moveTo('FsoapCampaigns','right');


}

function escSquote(str) {
	return str.replace(/'/g, "\\'", str);
}

function submitMe() {
	var F = _D.forms['phoundry'];

	var extra = "{'type':'dmd_subscribe','soapCampaigns':'" + escSquote(F.FsoapCampaigns.value) + "'}";
	_P.setExtra(extra);

	_P.killPopup();
}

//]]>
</script>
<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/core/css/phoundry.css" type="text/css" />
<link rel="stylesheet" href="<?= $PHprefs['url'] ?>/core/css/form.css" type="text/css" />
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body onload="init()" style="background:#fff;">
<form name="phoundry">
<?php
	duallist('FsoapCampaigns', $soapCampaigns, array(), word(10103), word(10104) );
?>

<!--
<fieldset>
<legend><b>Eigenschappen DMdelivery aanmelden</b></legend>
<table>
<tr>
	<td>SOAP gebruikersnaam:</td>
	<td><input class="txt" type="text" name="Fusername" size="30" /></td>
</tr>
<tr>
	<td>SOAP wachtwoord:</td>
	<td><input class="txt" type="text" name="Fpassword" size="30" /></td>
</tr>
</table>
</fieldset>
-->
<p align="right">
	<input type="button" value="<?= word(10060); ?>" onclick="submitMe()" />
	<input type="button" value="<?= word(10061); ?>" onclick="_P.killPopup()" />
</p>
</form>
</body>
</html>
