<?php
// @TODO maken? zo te zien nooit afgemaakt ofzo
	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	$TID = getTID();
	
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	$page = 'update';
	if (isset($_GET['RID']))
	{
		$RID = (int) $_GET['RID'];
	}
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, $page);
	
	$sql = "SELECT id, code, filename, downloadcount, removed, filesize FROM brickwork_avoid_form_upload WHERE id = '" . escDBquote($RID) . "' AND site_identifier = '" . escDBquote($_GET['site_identifier']) . "'";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($cur))
	{
		$filename = $db->FetchResult($cur, 0, 'filename');
		$filesize = $db->FetchResult($cur, 0, 'filesize');
		$downloadcount = $db->FetchResult($cur, 0, 'downloadcount');
		$filecode = $db->FetchResult($cur, 0, 'code');
		$removed = $db->FetchResult($cur, 0, 'removed');
		$file_id = $db->FetchResult($cur, 0, 'id');
	}

PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title><?= word(10080); ?></title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
</head>
<body class="frames">
<form action="edituploadRes.php?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<div class="headerFrame">
<?= getHeader(word(10081), word(7)); ?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td></td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->
<input type="hidden" name="site_identifier" value="<?= $_GET['site_identifier'] ?>" />

<fieldset>
<legend><b><?= word(10082); ?></b><?php if ($removed == 0) { ?> (<a target="_blank" href="<?= $PHprefs['uploadUrl'] ?>/FILES/EmailFormUpload/<?= $filecode ?>_<?= $filename ?>"><?= word(10083); ?></a>)<?php } ?></legend>
<input type="hidden" name="id" value="<?= $file_id ?>" /> <?= $filename ?><br />(<?= $filesize ?> bytes)
</fieldset>

<fieldset>
<?php if ($removed == 0) { ?>
<legend><b class="req"><?= word(10084); ?></b></legend>
<input id="removed_0" alt="1|integer" name="removed" value="0" checked="checked" type="radio" /><label for="removed_0"><img src="<?= $PHprefs['url'] ?>/core/icons/cross.png" alt="Nee" border="0" width="16" height="16" /></label> <input id="removed_1" name="removed" value="1" type="radio" /><label for="removed_1"><img src="<?= $PHprefs['url'] ?>/core/icons/tick.png" alt="Ja" border="0" width="16" height="16" /></label>
<?php } else { ?>
<legend><b><?= word(10084); ?></b></legend>
<img src="<?= $PHprefs['url'] ?>/core/icons/tick.png" alt="Ja" border="0" width="16" height="16" /> <?= word(10085); ?>.
<?php } ?>
</fieldset>

<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="<?= word(10060); ?>" />
	</td>
	<td align="right">
	<input type="button" value="<?= word(10061); ?>" onclick="_D.location='<?= $PHprefs['url'] ?>/core/records.php?<?= QS(0, 'RID') ?>'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
