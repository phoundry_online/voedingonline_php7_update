<?php
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	require_once INCLUDE_DIR . "/classes/Form.package.php";
	require_once INCLUDE_DIR . "/functions/autoloader.include.php";
	// spl_autoload autoloader uitschakelen anders komen er errors,
	// omdat die autoloader de AvoidField class niet kan vinden
	spl_autoload_unregister('spl_autoload');
	
	$TID = getTID();
	
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	if (isset($_GET['RID']))
	{
		$RID = $_GET['RID'];
		$page = 'update';
	}
	else
	{
		PH::phoundryErrorPrinter(word(10151) /* Invalid form */, true);
	}
		
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, $page);
	
	$site_identifier = !empty($_REQUEST['site_identifier']) ? $_REQUEST['site_identifier'] : '';
	$download_hash = !empty($_REQUEST['download_hash']) ? $_REQUEST['download_hash'] : '';
	$delete_hash = !empty($_REQUEST['delete_hash']) ? $_REQUEST['delete_hash'] : '';
	
	if(empty($site_identifier))
	{
		PH::phoundryErrorPrinter(word(11800) /* no site selected */, true);
	}
	
	if(!empty($delete_hash))
	{
		$sql = sprintf("DELETE FROM brickwork_avoid_form_submission WHERE hash = %s",
			DB::quote($delete_hash, true)
		);
		
		$res = DB::iQuery($sql);
		if (isError($res))
		{
			throw new Exception("Query " . $sql . " failed. " . $res->errstr);
		}
		
		header ('Location: export.php?'.QS(0));
		exit;
	}
	
	$seperator = !empty($_REQUEST['sep']) ? $_REQUEST['sep']:  ';';
	
	$rowcount = 0;
	
	$sql = sprintf("
			SELECT COUNT(*) AS count
			  FROM brickwork_avoid_form_submission bafs
			 INNER JOIN brickwork_form bf ON (bf.code = bafs.form_code)
			 WHERE bf.site_identifier = %s
			   AND bf.code = %s
			   AND bafs.hash = %s
			",
		DB::quote($site_identifier, true),
		DB::quote($RID, true),
		DB::quote($download_hash, true)
	);
	
	$res = DB::iQuery($sql);
	
	if (isError($res))
	{
		die($res->errstr);
	}
	else
	{
		$rowcount = $res->first()->count;
	}
	
	if ($rowcount == 0)
	{
		die(word(10094));
	}
	
	$fieldClasses = array();
	
	$sql = sprintf("
			SELECT fields_object,
			       INET_NTOA(ip) AS ip,
			       insert_date,
			       form_name,
			       url
			  FROM brickwork_avoid_form_submission bafs
			 INNER JOIN brickwork_form bf ON (bf.code = bafs.form_code)
			 WHERE bf.site_identifier = %s
			   AND bf.code = %s
			   AND bafs.hash = %s
			 ORDER BY bafs.id ASC
			",
		DB::quote($site_identifier, true),
		DB::quote($RID, true),
		DB::quote($download_hash, true)
	);
	
	$res = DB::iQuery($sql);
	if (isError($res))
	{
		die($res->errstr);
	}
	else if ($res->numRows > 0)
	{
		$rand = rand(1,5000);
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Disposition: attachment; filename={$RID}_{$download_hash}_{$rand}.csv");
		header("Content-type: application/csv");
		header("Content-Transfer-Encoding: binary");
		
		$row = $res->first();
		if ($PHprefs['charset'] === 'utf-8') {
			print chr(239).chr(187).chr(191);
		}
		
		// Print header
		$fields_object = unserialize(gzuncompress($row->fields_object));
		
		print word(10093)." '" . $row->form_name."'\n";
		
		$y = 0;
		foreach ($fields_object as $field)
		{
			$fieldClasses[$field->name] = get_class($field);
			print $y != 0 ? $seperator : "";
			print '"' . str_replace(array("\r\n",'"'),array("\\n",'\"'),$field->name) . '"';
			$fieldClasses[$field->name] = get_class($field);
			$y++;
		}
		// Add insert_date, ip_address and form_title
		print $seperator . '"insert_date"' . $seperator . '"ip_address"' . $seperator . '"url"' . "\n";
	}
	
	// Parameters for the str_replace
	$datasearch = array(
		$seperator,
		"\r\n",
		'"'
	);
	$datareplace = array(
		"\\" . $seperator,
		"\\n",
		'""'
	);
	
	// Print data
	foreach ($res as $result)
	{
		$fields_object = unserialize(gzuncompress($result->fields_object));
		
		$y = 0;
		foreach($fields_object as $field)
		{
			print $y != 0 ? $seperator : "";
			if(is_array( $field->value ))
			{
				switch ($fieldClasses[$field->name])
				{
					case "CheckBoxField":
						$keys = array_keys($field->value);
						$value = $keys[0];
						break;
						
					case "MailSelectField":
						$value = $field->selectedValue;
						break;

					default:
						$value = implode(',', $field->value);
						break;
				}
			}
			else
			{
				$value = $field->value;
			}
			print '"' . str_replace($datasearch, $datareplace, $value) . '"';
			$y++;
		}
		
		// add insert date
		print $seperator . '"' . str_replace($datasearch, $datareplace, $result->insert_date) . '"';
		// add ip
		print $seperator . '"' . str_replace($datasearch, $datareplace, $result->ip) . '"';
		// add url
		print $seperator . '"' . str_replace($datasearch, $datareplace, $result->url) . '"';
		// add form_title
//		print $seperator . '"' . str_replace($datasearch, $datareplace, $result->form_name) . '"';
//		print $seperator . memory_get_usage() . $seperator . memory_get_usage(true);
		print "\n";
	}

/**
 * Test if an object is of type error
 *
 * @return Boolean
 */
function isError(&$e) {
    $v = PHP_VERSION;

    switch (TRUE) {
        // old php
        case ($v < 4.2):
            if ((get_class($e) == 'error')
                ||	(is_subclass_of($e, 'error'))) {
                return TRUE;
            }
            break;

        // php 4.2 < 5
        case ($v >= 4.2 && $v < 5):
            return (is_a($e, 'Error'));
            break;

        // php 5
        case ($v >= 5):
            return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
            break;
    }

    // not an error
    return FALSE;
}
	exit(1);
