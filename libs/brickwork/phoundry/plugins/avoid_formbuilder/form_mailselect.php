<?
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

?><html>
<head>
<title><?= word(10110); ?></title>
<script type="text/javascript" src="Base64.js"></script>
<script type="text/javascript">
//<![CDATA[


var _W=window, _D=document, _P=parent;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function trim(str) {
	return (str || "").replace(/^\s+|\s+$/g, "");
}

function init() {
	var F = _D.forms['phoundry'], extra, Pextra;
	extra = {'size':'1', 'multiple':false, 'options':{}};
	if (_P.curExtra != '') {
		eval('Pextra = ' + _P.curExtra);
		if (Pextra['type'] == 'select') {
			extra = Pextra;
		}
	}
	F.Fsize.value = F.example.size = extra['size'];

	if (extra['multiple'] == 'true') {
		F['Fmselect'].checked = true;
		setMultiple(true);
	}

	var i = 0;
	for (var x in extra['options']) {
		F.example.options[i++]=new Option(extra['options'][x],x);
	}
	F.example.selectedIndex = -1;
	F.Fsize.focus();
}

function setMultiple(on) {
	_D.forms['phoundry'].example.multiple = on;
}

function setSize(size) {
	_D.forms['phoundry'].example.size = size;
}

function doOption() {
	var f = _D.forms['phoundry'],
	text = trim(f.FoptText.value),
	value = trim(f.FoptValue.value);
	f.FoptText.value = text;
	f.FoptValue.value = value;
	
	if(text == '' || value == '') {
		alert('<?= word(10111); ?>');
		f.FoptText.focus();
		return;
	}
	var base64 = new jsBase64(value);	
	var oValue = base64.encode();
	
	var opt = new Option(text, oValue);
	if (f.actBut.value == '<?= word(10112); ?>')
		f.example.options[f.example.length] = opt;
	else
		f.example.options[f.example.selectedIndex] = opt;
	f.actBut.value = '<?= word(10112); ?>';
	f.FoptText.value = f.FoptValue.value = '';
}

function delOption() {
	var f = _D.forms['phoundry'];
	if (f.example.selectedIndex<0) return;
	f.example.options[f.example.selectedIndex] = null;
	f.FoptText.value = f.FoptValue.value = '';
	f.actBut.value = '<?= word(10112); ?>';
}

function setOption() {
	var f = _D.forms['phoundry'];
	if(!f.example.options.length)return;

	var base64 = new jsBase64(f.example.options[f.example.selectedIndex].value);	
	f.FoptText.value = f.example.options[f.example.selectedIndex].text;
	f.FoptValue.value = base64.decode();
	f.actBut.value = '<?= word(10113); ?>';
}

function escSquote(str) {
	return str.replace(/'/g, "\\'", str);
}

function submitMe() {
	var F = _D.forms['phoundry'];

	var options = '{';
	for (var x = 0; x < F.example.options.length; x++) {
		if (x > 0) options += ',';
		options += "'" + escSquote(F.example.options[x].value) + "':'" + escSquote(F.example.options[x].text) + "'";
	}
	options += '}';

	if (F.Fsize.value == '') { F.Fsize.value = 1; }
	var extra = "{'type':'select','size':'" + F.Fsize.value + "','multiple':'" + (F.Fmselect.checked?'true':'false') + "','options':" + options + "}";
	//alert(extra);
	_P.setExtra(extra);

	_P.killPopup();
}

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body onload="init()" style="background:#fff;">
<form name="phoundry">
<!-- totdat form.package.php is geupgrade: -->
<!-- <input type="hidden" name="Fmselect" value="0" /> -->
<fieldset>
<legend><b><?= word(10110); ?></b></legend>
<table><tr><td valign="top">

<table>
<tr>
	<td><?= word(10114); ?>:</TD>
	<td><input class="txt" type="text" name="Fsize" size="4" onblur="this.value=checkDigit(this.value);setSize(this.value)" /></td>
</tr>
<!-- werkt nog niet met Form.package.php -->
<tr>
	<td><label for="Fmselect"><?= word(10115); ?>?</label></td>
	<td><input type="checkbox" name="Fmselect" value="1" onclick="setMultiple(this.checked)" /></td>
</tr>
<!-- werkt nog niet met Form.package.php -->
<tr><td colspan="2"><hr noshade="noshade" size="1" /></td></tr>
<tr>
	<td><b><?= word(10116); ?>:</b></td>
	<td><input class="txt" type="text" name="FoptValue" maxlength="70" /></td>
</tr>
<tr>
	<td><b><?= word(10117); ?>:</b></td>
	<td><input class="txt" type="text" name="FoptText" maxlength="70" /></td>
</tr>
<tr>
	<td colspan="2" align="right">
	<input name="actBut" type="button" value="<?= word(10112); ?>" onclick="doOption()" />
	<input type="button" value="<?= word(10118); ?>" onclick="delOption()" />
	</td>
</tr>
</table>

</td><td valign="top">
<fieldset style="width:150px; height:140px">
<legend><b><?= word(10119); ?></b></legend>
<center>
<select name="example" size="1" onchange="setOption()" /></select>
</center>
</fieldset>
</td>
</tr>
</table>

</fieldset>

<p align="right">
	<input type="button" value="<?= word(10060); ?>" onclick="submitMe()" />
	<input type="button" value="<?= word(10061); ?>" onclick="_P.killPopup()" />
</p>
</form>
</body>
</html>
