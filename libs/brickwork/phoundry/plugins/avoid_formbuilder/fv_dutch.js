var words = new Array(
'Veld #1 bevat geen geldig geheel getal!',
'Veld #1 bevat geen geldig getal!',
'Veld #1 bevat geen geldige postcode!',
'Veld #1 bevat geen geldige datum!',
'Veld #1 bevat geen geldig e-mail adres! (wie@waar.nl)',
'Veld #1 bevat geen geldige URL (beginnend met http://)',
'Verplicht veld #1 is niet ingevuld!',
'Veld #1 bevat geen geldige HEX-kleurcode (#aabbcc)',
'Veld #1 bevat geen geldig tijdstip! (HH:MM)',
'Veld #1 bevat #2 karakters, maximum is #3!'
);
dateFormat = 'DD-MM-YYYY';
zipcodeFormat = '[0-9]{4} ?[A-z]{2}';
