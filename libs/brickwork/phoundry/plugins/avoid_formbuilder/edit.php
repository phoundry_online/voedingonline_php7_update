<?php
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	$TID = getTID();
	
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	if (isset($_GET['RID']))
	{
		$RID = $_GET['RID'];
		$page = 'update';
	}
	else
	{
		$page = 'insert';
	}
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, $page);
	
	$form_els = array('extra'=>array());
	
	if ($page == 'update')
	{
		$sql = "SELECT * FROM brickwork_avoid_formbuilder WHERE form_code = '" . escDBquote($RID) . "'";
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		if (!$db->EndOfResult($cur))
		{
			$form_els = unserialize( $db->FetchResult($cur,0,'elements'));
		}
	}

	$escape = function($value) use($PHprefs) {
		return htmlspecialchars($value, ENT_QUOTES | ENT_SUBSTITUTE, $PHprefs['charset']);
	};

	$input_types = array(
		'text'			=> word(10021),
		'password'		=> word(10022),
		'hidden'		=> word(10023),
		'select'		=> word(10024),
		'mailselect'	=> word(10025),
		'radio'			=> word(10026),
		'checkbox'		=> word(10027),
		'textarea'		=> word(10028),
		//'submit'		=> word(10029),
		//'reset'			=> word(10030),
		'freetext'		=> word(10031),
		'captcha'		=> word(10032),
		'dmd_subscribe'	=> word(10033),
	);
	
	// Tells us if a input type can be set on required
	$input_types_required = array(
		'text'			=> true,
		'password'		=> true,
		'hidden'		=> false,
		'select'		=> true,
		'mailselect'	=> true,
		'radio'			=> true,
		'checkbox'		=> true,
		'textarea'		=> true,
		'submit'		=> false,
		'reset'			=> false,
		'freetext'		=> false,
		'captcha'		=> true,
		'dmd_subscribe'	=> true,
	);
	
	if (file_exists($PHprefs['uploadDir'] . "/FILES/EmailFormUpload") && is_dir($PHprefs['uploadDir'] . "/FILES/EmailFormUpload"))
	{
		$input_types['fileupload'] = 'Bestand upload';
	}
	
	$data_types = array(
		'integer'	=> word(10034),
		'email'		=> word(10035),
		'date'		=> word(10036),
		// TO DO, deze actief maken als er ook daadwerkelijk een postcode check bestaat!
		// 'zipcode'	=> word(10037)
	);

PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title><?= word(10038); ?></title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?=QS(true)?>"></script>
<script type="text/javascript">
//<![CDATA[

function moveField(curEdit,dir) {
   if (!curEdit) return;
   var tbl = curEdit.parentNode, idx = curEdit.rowIndex, tmpEl;
   if (dir == 'up' && idx > 1) {
      tmpEl = tbl.removeChild(tbl.rows[idx]);
      tbl.insertBefore(tmpEl, tbl.rows[idx-1]);
   }
   else if (dir == 'down' && idx < tbl.rows.length-1) {
      tmpEl = tbl.removeChild(tbl.rows[idx+1]);
      tbl.insertBefore(tmpEl, tbl.rows[idx]);
   }
}

function delField(el) {
	if (confirm('<?= word(10070);?>')) {
		el.parentNode.deleteRow(el.rowIndex);
	}
}

function addField() {
	var tbl = gE('formTbl'), tr, td;
	tr = tbl.insertRow(tbl.rows.length);
	td = tr.insertCell(0); td.innerHTML = '<input type="hidden" name="extra[]" value="" /><img class="ptr" src="<?= $PHprefs['url'] ?>/core/pics/trash.gif" width="16" height="16" onclick="delField(this.parentNode.parentNode)" /> <img class="ptr" src="<?= $PHprefs['url'] ?>/core/pics/up.gif" width="16" height="16" onclick="moveField(this.parentNode.parentNode, \'up\')" /> <img class="ptr" src="<?= $PHprefs['url'] ?>/core/pics/down.gif" width="16" height="16" onclick="moveField(this.parentNode.parentNode, \'down\')" />'; td.noWrap = true;
	td = tr.insertCell(1); td.innerHTML = '<input type="text" name="text[]" value="Label '+tr.rowIndex+'" size="20" />';
	td = tr.insertCell(2); td.innerHTML = '<input type="text" name="name[]" value="" size="20" />';
	td = tr.insertCell(3); td.innerHTML = '<select name="type[]" onchange="setType(this.parentNode.parentNode)"><option value="">[Select]</option><?php foreach($input_types as $value=>$name) { print '<option value="' . $value . '">' . $name . '</option>'; } ?></select>&nbsp;<a href="#" onclick="setType(this.parentNode.parentNode);return false">edit</a>';
	td = tr.insertCell(4); td.innerHTML = '<select name="syntax[]"><option value="">[<?=word(10059);?>]</option><?php foreach($data_types as $value=>$name) { print '<option value="' . $value . '">' . $name . '</option>'; } ?></select>';
	td = tr.insertCell(5); td.align='center'; td.innerHTML = '<input type="checkbox" name="required[]" value="1" />';
	td = tr.insertCell(6); td.innerHTML = '<textarea name="info[]" cols="40" rows="2"></textarea>'; td.style.padding=0;

}

var curRow = null, curExtra = '';

function setType(trEl) {
	curRow = trEl.rowIndex;
	curExtra = trEl.getElementsByTagName('input')[0].value;

	
	var type = trEl.getElementsByTagName('select')[0].value;
	if (type == '') {
		killPopup(); return;
		trEl.getElementsByTagName('input')[3].disabled = false;
	}
	
	var type_required = (<?=json_encode($input_types_required)?>);
	trEl.getElementsByTagName('input')[3].disabled = !type_required[type];
	if(!type_required[type]) {
		trEl.getElementsByTagName('input')[3].checked = false;
	}

	switch (type)
	{
		case 'text' : typeTxt = '<?=word(10039); ?>'; break;
		case 'textarea' : typeTxt = '<?=word(10040); ?>'; break;
		case 'checkbox' : typeTxt = '<?=word(10041); ?>'; break;
		case 'select' : typeTxt = '<?=word(10042); ?>'; break;
		case 'radio' : typeTxt = '<?=word(10043); ?>'; break;
		case 'dmd_subscribe' : typeTxt = '<?=word(10044); ?>'; break;
		default: typeTxt = type;
	}
	var site_identifier = _D.forms[0]['site_identifier'].value;
	makePopup(null,400,300,'<?=word(10050); ?> '+typeTxt,'form_'+type+'.php?site_identifier='+site_identifier);
}

function setExtra(extra) {
	if (!curRow) {
		alert('<?=word(10051); ?>'); return;
	}
	var tbl = gE('formTbl');
	// Get handle to hidden input field:
	var handle = tbl.rows[curRow].getElementsByTagName('input')[0];
	handle.value = extra;
}

function checkForm(F) {
	if (F['form_code'].value == '') {
		alert('<?=word(10052); ?>');
		return false;
	}
	var tbl = gE('formTbl'), tr, name, text, inputEls;
	if (tbl.rows.length < 2) {
		alert('<?=word(10053); ?>');
		return false;
	}

	var foundLabels = new Array();

	for (var x = 1; x < tbl.rows.length; x++) {
		tr = tbl.rows[x];

		// Label and name are required:
		inputEls = tr.getElementsByTagName('input');
		
		name = inputEls[2];
		text = inputEls[1];
		
		if (name.value == '' || /^\d+/.test(name.value)) {
			alert('<?=word(10054); ?>' + x + '!');
			name.focus();
			return false;
		}

		// Type is required:
		if ($('[name="type[]"]', tr).val() === '') {
			alert('<?=word(10071)?>' + x + '!');
			$('[name="type[]"]', tr).focus();
			return false;
		}
		
		// check for double names
		if(jQuery.inArray(name.value, foundLabels)>=0){
			alert('<?=word(10055); ?> ' + name.value + ' <?=word(10056); ?>');
			name.focus();
			return false;
		}
		if (text.value == '') {
			alert('<?=word(10057); ?>' + x + '!');
			text.focus();
			return false;
		}
		
		reqd = inputEls[3];
		if (reqd && !reqd.checked && reqd.parentElement) {
			reqd.parentElement.innerHTML = '<input type="hidden" name="required[]" value="0" />';
		}
		foundLabels[foundLabels.length]=name.value;
	}
	
	return true;
}

//]]>
</script>
</head>
<body class="frames">
<form action="editRes.php?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<div class="headerFrame">
<?= getHeader(word(10058), word($page == 'update' ? 7 : 6)); ?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td></td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->
<input type="hidden" name="site_identifier" value="<?= $_GET['site_identifier'] ?>" />

<fieldset>
<legend><b class="req"><?= word(10015); ?></b></legend>
<?php
if (!empty($RID))
{
	$sql = "
		SELECT
			form_code,
			title
		FROM
			brickwork_form bf
		INNER JOIN
			brickwork_avoid_formbuilder baf ON bf.code = baf.form_code
		WHERE
			baf.form_code = '" . escDBquote($RID) . "'
		AND
			bf.site_identifier = '" . escDBquote($_GET['site_identifier']) . "'
		AND
			baf.site_identifier = '". escDBquote($_GET['site_identifier']) . "'
		LIMIT		1
	";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	print $db->FetchResult($cur, 0, 'title');
	print '<input type="hidden" name="form_code" value="' . $db->FetchResult($cur, 0, 'form_code') . '" />';
}
else
{
	
?>
<select name="form_code">
<option value="">[<?= word(10016); ?>]</option>
<?php
	$sql = "
		SELECT	bf.code,
				bf.title
		FROM	brickwork_form bf
		LEFT JOIN brickwork_avoid_formbuilder baf ON bf.code = baf.form_code AND baf.site_identifier = '". escDBquote($_GET['site_identifier']) . "'
		WHERE		bf.site_identifier = '". escDBquote($_GET['site_identifier']) ."' AND baf.form_code IS NULL
			AND	bf.type_code = 'avoid'
		ORDER BY	title
		";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	for ($x = 0; !$db->EndOfResult($cur); $x++)
	{
		print '<option value="' . $db->FetchResult($cur,$x,'code') . '">' . $db->FetchResult($cur,$x,'title') . "</option>\n";
	}
?>
</select>
<?php
}
?>
</fieldset>

<fieldset>
<legend><b class="req"><?= word(10017); ?></b> (<a href="#" onclick="addField();return false"><?= word(10018); ?></a>)</legend>
<table class="sort-table" id="formTbl" cellspacing="1" cellpadding="3">
<tr>
	<th></th>
	<th><span style="cursor: help; border-bottom: 1px dotted #000;" title="<?= word(10019); ?>"><?= word(10062); ?></span></th>
	<th><span style="cursor: help; border-bottom: 1px dotted #000;" title="<?= word(10020); ?>"><?= word(10063); ?></span></th>
	<th><?= word(10064); ?></th>
	<th><?= word(10065); ?></th>
	<th><?= word(10066); ?>?</th>
	<th><?= word(10067); ?></th>
</tr>
<?php for ($x = 0; $x < count($form_els['extra']); $x++) { ?>
<tr>
	<td nowrap="nowrap">
	<input type="hidden" name="extra[]" value="<?= $escape($form_els['extra'][$x]) ?>" /><img class="ptr" src="<?= $PHprefs['url'] ?>/core/pics/trash.gif" width="16" height="16" onclick="delField(this.parentNode.parentNode)" /> <img class="ptr" src="<?= $PHprefs['url'] ?>/core/pics/up.gif" width="16" height="16" onclick="moveField(this.parentNode.parentNode, 'up')" /> <img class="ptr" src="<?= $PHprefs['url'] ?>/core/pics/down.gif" width="16" height="16" onclick="moveField(this.parentNode.parentNode, 'down')" />
	</td>
	<td>
	<input type="text" name="text[]" value="<?= $escape($form_els['text'][$x]) ?>" size="20" />
	</td>
	<td>
	<input type="text" name="name[]" value="<?= $escape($form_els['name'][$x]) ?>" size="20" />
	</td>
	<td>
	<select name="type[]" onchange="setType(this.parentNode.parentNode)">
	<option value="">[<?= word(10068); ?>]</option>
	<?php
		foreach($input_types as $value=>$name) {
			$selected = ($form_els['type'][$x] == $value) ? ' selected="selected" ' :' ';
			print '<option' . $selected . 'value="' . $value . '">' . $name . "</option>\n";
		}
	?>
	</select>&nbsp;<a href="#" onclick="setType(this.parentNode.parentNode);return false"><?= word(10069); ?></a>
	</td>
	<td>
	<select name="syntax[]"><option value="">[<?=word(10059);?>]</option>
	<?php
		foreach($data_types as $value=>$name) {
			$selected = ($form_els['syntax'][$x] == $value) ? ' selected="selected" ' :' ';
			print '<option' . $selected . 'value="' . $value . '">' . $name . "</option>\n";
		}
	?>
	</select>
	</td>
	<td align="center">
	<input type="checkbox" name="required[<?= $x ?>]" value="1" <?= isset($form_els['required'][$x]) && $form_els['required'][$x] == 1 ? 'checked="checked"' : '' ?> />
	</td>
	<td style="padding:0">
	<textarea name="info[]" cols="60" rows="3"><?= $escape($form_els['info'][$x]) ?></textarea>
	</td>
</tr>
<?php } ?>
</table>
</fieldset>

<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="<?=word(10060);?>" class="okBut" />
	</td>
	<td align="right">
	<input type="button" value="<?=word(10061);?>" onclick="_D.location='<?= $PHprefs['url'] ?>/core/records.php?<?= QS(0, 'RID') ?>'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
