<?php
	$inc = @include('PREFS.php');
	
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	
	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	if (isset($_GET['RID']))
	{
		$RID = $_GET['RID'];
		$page = 'update';
	}
	else {
		$page = 'insert';
	}
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, $page);
	
	$CODE = $_POST['form_code'];
	$site_identifier = $_POST['site_identifier'];
	unset($_POST['form_code']);
	unset($_POST['site_identifier']);
	
	if ($page == 'update')
	{
		$sql = "
			UPDATE	brickwork_avoid_formbuilder
			SET		elements = '" . escDBquote(serialize($_POST)) . "'
			WHERE	form_code = '" . escDBquote($CODE) . "'
			LIMIT	1
		";
	}
	else
	{
		$sql = "
			INSERT INTO	brickwork_avoid_formbuilder
			(	form_code, site_identifier,
				elements
			) VALUES (
				'" . escDBquote($CODE) . "',
				'" . escDBquote($site_identifier) . "',
				'" . escDBquote(serialize($_POST)) . "'
			)
		";
	}
	
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	
	header('Location: ' . $PHprefs['url'] . '/core/records.php?' . QS(0, 'RID'));
?>