<?
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

?><html>
<head>
<title><?= word(10125); ?></title>
<script type="text/javascript">
//<![CDATA[


var _W=window, _D=document, _P=parent;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function trim(str) {
	return (str || "").replace(/^\s+|\s+$/g, "");
}

function init() {
	var F = _D.forms['phoundry'], extra, Pextra;
	extra = {'options':{}};
	if (_P.curExtra != '') {
		eval('Pextra = ' + _P.curExtra);
		if (Pextra['type'] == 'radio') {
			extra = Pextra;
		}
	}

	var i = 0;
	for (var x in extra['options']) {
		F.example.options[i++]=new Option(extra['options'][x],x);
	}
	F.example.selectedIndex = -1;
}

function setMultiple(on) {
	_D.forms['phoundry'].example.multiple = on;
}

function doOption() {
	var f = _D.forms['phoundry'],
	text = trim(f.FoptText.value),
	value = trim(f.FoptValue.value);
	f.FoptText.value = text;
	f.FoptValue.value = value;
	if(text == '' || value == '') {
		alert(<?=json_encode(word(10132/* Voer zowel een naam als een waarde in! */))?>);
		f.FoptText.focus();
		return;
	}
	var opt = new Option(text, value);
	if (f.actBut.value == <?=json_encode(word(10128/* Voeg toe */))?>) {
		f.example.options[f.example.length] = opt;
	} else { 
		f.example.options[f.example.selectedIndex] = opt;
	}
	f.actBut.value = <?=json_encode(word(10128/* Voeg toe */))?>;
	f.FoptText.value = f.FoptValue.value = '';
}

function delOption() {
	var f = _D.forms['phoundry'];
	if (f.example.selectedIndex<0) return;
	f.example.options[f.example.selectedIndex] = null;
	f.FoptText.value = f.FoptValue.value = '';
	f.actBut.value = <?=json_encode(word(10128/* Voeg toe */))?>;
}

function setOption() {
	var f = _D.forms['phoundry'];
	if(!f.example.options.length)return;
	f.FoptText.value = f.example.options[f.example.selectedIndex].text;
	f.FoptValue.value = f.example.options[f.example.selectedIndex].value;
	f.actBut.value = <?=json_encode(word(10134/* Wijzig */))?>;
}

function escSquote(str) {
	return str.replace(/'/g, "\\'", str);
}

function submitMe() {
	var F = _D.forms['phoundry'];

	var options = '{';
	for (var x = 0; x < F.example.options.length; x++) {
		if (x > 0) options += ',';
		options += "'" + escSquote(F.example.options[x].value) + "':'" + escSquote(F.example.options[x].text) + "'";
	}
	options += '}';

	var extra = "{'type':'radio','options':" + options + "}";
	_P.setExtra(extra);

	_P.killPopup();
}

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body onload="init()" style="background:#fff;">
<form name="phoundry">
<fieldset>
<legend><b><?= word(10125); ?></b></legend>
<table><tr><td valign="top">

<table>
<tr>
	<td><b><?= word(10126); ?>:</b></td>
	<td><input class="txt" type="text" name="FoptValue" /></td>
</tr>
<tr>
	<td><b><?= word(10127); ?>:</b></td>
	<td><input class="txt" type="text" name="FoptText" /></td>
</tr>
<tr>
	<td colspan="2" align="right">
	<input name="actBut" type="button" value="<?= word(10128); ?>" onclick="doOption()" />
	<input type="button" value="<?= word(10129); ?>" onclick="delOption()" />
	</td>
</tr>
</table>

</td><td valign="top">
<fieldset style="width:150px; height:140px">
<legend><b><?= word(10119); ?></b></legend>
<center>
<select name="example" size="1" onchange="setOption()" /></select>
</center>
</fieldset>
</td>
</tr>
</table>

</fieldset>

<p align="right">
	<input type="button" value="<?= word(10060); ?>" onclick="submitMe()" />
	<input type="button" value="<?= word(10061); ?>" onclick="_P.killPopup()" />
</p>
</form>
</body>
</html>
