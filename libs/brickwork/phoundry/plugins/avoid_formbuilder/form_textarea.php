<?
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

?><html>
<head>
<title><?= word(10138); ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function trim(str) {
	return (str || "").replace(/^\s+|\s+$/g, "");
}

function init() {
	var F = _D.forms['phoundry'], extra, Pextra;
	extra = {'columns':'', 'rows':'', 'wrap':'off', 'value':''};
	if (_P.curExtra != '') {
		eval('Pextra = ' + _P.curExtra);
		if (Pextra['type'] == 'textarea') {
			extra = Pextra;
		}
	}
	F.Fcols.value = extra['columns'];
	F.Frows.value = extra['rows'];
	F.Fwrap.value = extra['wrap'];
	F.Fvalue.value = unescape(extra['value']);
	
	F.Fcols.focus();
}

function escSquote(str) {
	return str.replace(/'/g, "\\'", str);
}

function submitMe() {
	var F = _D.forms['phoundry'],
	cols = trim(F.Fcols.value),
	rows = trim(F.Frows.value);
	F.Fcols.value = cols;
	F.Frows.value = rows;

	if (cols == '') {
		alert('<?= word(10139); ?>!');
		F.Fcols.focus();
		return;
	}
	if (rows == '') {
		alert('<?= word(10140); ?>!');
		F.Frows.focus();
		return;
	}
	
	var extra = "{'type':'textarea','columns':'" + cols + "','rows':'" + rows + "','wrap':'" + F.Fwrap.value + "','value':'" + escape(F.Fvalue.value) + "'}";
	_P.setExtra(extra);

	_P.killPopup();
}

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body onload="init()" style="background:#fff;">
<form name="phoundry">
<fieldset>
<legend><b><?= word(10138); ?></b></legend>
<table>
<tr>
	<td><?= word(10141); ?>:</td>
	<td><input class="txt" type="text" name="Fcols" size="4" onblur="this.value=checkDigit(this.value,0)" /></td>
	<td><?= word(10142); ?>:</td>
	<td><input class="txt" type="text" name="Frows" size="4" onblur="this.value=checkDigit(this.value,0)" /></td>
</tr>
<tr>
	<td><?= word(10143); ?>:</td>
	<td colspan="3"><select name="Fwrap">
	<option value="off"><?= word(10144); ?></option>
	<option value="soft"><?= word(10145); ?></option>
	<option value="hard"><?= word(10146); ?></option>
	</select></td>
</tr>
<tr>
	<td><?= word(10147); ?>:</td>
	<td colspan="3">
	<textarea class="txt" name="Fvalue" style="width:200px; height:50px"></textarea>
	</td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= word(10060); ?>" onclick="submitMe()" />
	<input type="button" value="<?= word(10061); ?>" onclick="_P.killPopup()" />
</p>
</form>
</body>
</html>
