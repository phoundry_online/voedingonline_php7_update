<?php
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	$TID = getTID();
	
	if (isset($_GET['RID']))
	{
		$RID = $_GET['RID'];
		$page = 'update';
	}
	else {
		PH::phoundryErrorPrinter(word(10151) /* Invalid form */, true);
	}
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, $page);
	
	$site_identifier = !empty($_REQUEST['site_identifier']) ? $_REQUEST['site_identifier'] : '';
	
	if(empty($site_identifier))
	{
		PH::phoundryErrorPrinter(word(11800) /* no site selected */, true);
	}
PH::getHeader('text/html');
?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title><?= word(10086); ?></title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[
function confirm_delete(hash)
{
	if(confirm('<?= word(10087); ?>'))
	{
		document.getElementById('delete_hash').value = hash;
		return true;
	}
	else
	{
		return false;
	}
}
function download(hash)
{
	document.getElementById('download_hash').value = hash;
	return true;
}
//]]>
</script>
</head>
<body class="frames">
<form action="exportRes.php?<?= QS(1) ?>" method="post" onsubmit="return checkForm(this)">
<div class="headerFrame">
<?= getHeader(word(10086)); ?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
<?= word(10088); ?>: <select name="sep">
<option value=";">;</option>
<option value=",">,</option>
</select>
</td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->
<input type="hidden" name="site_identifier" value="<?= $_GET['site_identifier'] ?>" />
<input type="hidden" name="delete_hash" id="delete_hash" value="" />
<input type="hidden" name="download_hash" id="download_hash" value="" />
<?
$csv_list = array();
$sql = sprintf("
		SELECT
			bafs.form_code,
			bafs.hash,
			count(*) as cnt,
			MIN(insert_date) as first_date,
			MAX(insert_date) as last_date
		FROM
			brickwork_avoid_form_submission bafs
		INNER JOIN
			brickwork_form bf ON (bf.code=bafs.form_code)
		WHERE
			bf.site_identifier='%s'
		AND
			bf.code = '%s'
		GROUP BY
			form_code, hash
		ORDER BY bafs.id ASC",
	escDBquote($site_identifier),
	escDBquote($RID)
);

$cur = $db->Query($sql)
	or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
if (!$db->EndOfResult($cur))
{
	for ($x = 0; !$db->EndOfResult($cur); $x++)
	{
		$csv_list[] = array(
			'hash'			=> $db->FetchResult($cur,$x,'hash'),
			'count'			=> $db->FetchResult($cur,$x,'cnt'),
			'first_date'	=> $db->FetchResult($cur,$x,'first_date'),
			'last_date'		=> $db->FetchResult($cur,$x,'last_date')
		);
	}
}

if(count($csv_list) > 0)
{
	?>
	<table id="recordTBL" class="sort-table" cellspacing="1" cellpadding="3">
	<tr>
		<th>#</th>
		<th><?= word(10089); ?></th>
		<th><?= word(10090); ?></th>
		<th><?= word(10091); ?></th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr>
	<?
	foreach($csv_list as $index => $item)
	{
		printf("<tr id=\"row%d\" class=\"odd\">
				<td style=\"text-align: right\">%d</td>
				<td>%s</td>
				<td>%s</td>
				<td>%s</td>
				<td><input type=\"image\" onclick=\"return confirm_delete('%s');\" src=\"" . $PHprefs['url'] . "/core/pics/trash.gif\" /></td>
				<td><input type=\"image\" onclick=\"return download('%s');\" src=\"download.gif\" /></td>
				</tr>",
			$index,
			$index+1,
			$item['first_date'],
			$item['last_date'],
			$item['count'],
			$item['hash'],
			$item['hash']
		);
	}
	?>
	</table>
	<?
}
else
{
	print word(10092);
}
?>

<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	&nbsp;
	</td>
	<td align="right">
	<input type="button" value="<?= word(10061); ?>" onclick="_D.location='<?= $PHprefs['url'] ?>/core/records.php?<?= QS(0, 'RID') ?>'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
