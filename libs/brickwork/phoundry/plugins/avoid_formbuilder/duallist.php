<?php

function duallist($name, $list, $selected, $title, $desc) {
	global $PHprefs;

	$options = '';
	$sel_options = '';
	foreach($list as $index => $value) {
		if(in_array($index, $selected)) {
			$sel = ' selected="selected"';
			$sel_options .= '<option value="'. $index .'" '. $sel .'>'. $value .'</option>' . "\n";
		} else {
			$sel = '';
			$options .= '<option value="'. $index .'" '. $sel .'>'. $value .'</option>' . "\n";
		}
	}
?>
	<fieldset>
	<legend><b class="req"><?php echo $title ?></b></legend>
	<table><tr><td valign="top"><img src="<?= $PHprefs['url'] ?>/core/pics/info.gif" alt="Info" width="16" height="16" /></td><td><?php echo $desc; ?></td></tr></table>
	<input type="hidden" alt="0|xref" name="<?php echo $name; ?>" value="<?php echo join('|', $selected); ?>" />
	<table cellspacing="1" cellpadding="1">
		<tr>
			<th><b><?= word(10075); ?>:</b></th>
			<td></td>
			<th><b><?= word(10076); ?>:</b></th>
		</tr>
		<tr>
			<td>
				<select multiple="multiple" size="6" name="all_<?php echo $name; ?>" style="min-width:160px">
				<?= $options ?>
				</select>
			</td>
			<td>
			<input type="button" onclick="moveTo('<?php echo $name; ?>','right')" value="&gt;&gt;" /><br />
			<input type="button" onclick="moveTo('<?php echo $name; ?>','left')" value="&lt;&lt;" />
			</td>
			<td>
				<select multiple="multiple" size="6" name="dsp_<?php echo $name; ?>" style="min-width:160px">
				<?= $sel_options ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="center"><a href="#" onclick="selectAll(_D.forms[0]['all_<?php echo $name; ?>']);return false"><?= word(10077); ?></a></td>
			<td></td>
			<td align="center"><a href="#" onclick="selectAll(_D.forms[0]['dsp_<?php echo $name; ?>']);return false"><?= word(10077); ?></a></td>
		</tr>
	</table>
	</fieldset>
<?php
}
?>
