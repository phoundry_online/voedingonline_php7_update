var words = new Array(
'Field #1 does not contain a valid integer!',
'Field #1 does not contain a valid number!',
'Field #1 does not contain a valid zipcode!',
'Field #1 does not contain a valid date!',
'Field #1 does not contain a valid e-mail address! (foo@company.com)',
'Field #1 does not contain a valid URL! (starting with http://)',
'Required field #1 has not been entered!',
'Field #1 does not contain a valid HEX-colorcode (#aabbcc)',
'Field #1 does not contain a valid time! (HH:MM)',
'Field #1 contains #2 characters, maximum is #3!'
);
dateFormat = 'MM-DD-YYYY';
zipcodeFormat = '[0-9]{5}';
