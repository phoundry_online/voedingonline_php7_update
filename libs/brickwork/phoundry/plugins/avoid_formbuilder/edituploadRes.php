<?php
// @TODO maken? zo te zien nooit afgemaakt ofzo
	$inc = @include('PREFS.php');
	
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	
	$TID = getTID();
	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');
	
	$RID = $_GET['RID'];
	$page = 'update';
	
	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, $page);
	
	$file_id = (int) $_POST['id'];
	$site_identifier = $_POST['site_identifier'];
	$remove = isset($_POST['removed']) ? (int) $_POST['removed'] : 0;
	unset($_POST['id']);
	unset($_POST['site_identifier']);
	
	if ($file_id != 0 && $remove != 0)
	{
		$sql = "SELECT code, filename FROM brickwork_avoid_form_upload WHERE id = ". escDBquote($file_id) . " AND site_identifier = '" . escDBquote($site_identifier) . "' AND removed = 0";
		
		$cur = $db->Query($sql)
			or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		
		if (! $db->EndOfResult($cur))
		{
			$code = $db->FetchResult($cur, 0, 'code');
			$filename = $db->FetchResult($cur, 0, 'filename');
			
			if (file_exists($PHprefs['uploadDir'] . "/FILES/EmailFormUpload/" . $code . "_" . $filename))
			{
				unlink($PHprefs['uploadDir'] . "/FILES/EmailFormUpload/" . $code . "_" . $filename);
			}
			
			$sql = "
				UPDATE	brickwork_avoid_form_upload
				SET		removed = ". escDBquote($remove) . "
				WHERE	id = '" . escDBquote($file_id) . "'
				AND		site_identifier = '" . escDBquote($site_identifier) . "'
				LIMIT	1
			";
			
			$cur = $db->Query($sql)
				or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		}
	}
	
	header('Location: ' . $PHprefs['url'] . '/core/records.php?' . QS(0, 'RID'));
