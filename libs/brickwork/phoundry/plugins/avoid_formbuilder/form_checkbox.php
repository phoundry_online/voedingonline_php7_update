<?php
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

?><html>
<head>
<title><?= word(10099); ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function trim(str) {
	return (str || "").replace(/^\s+|\s+$/g, "");
}

function init() {
	var F = _D.forms['phoundry'], extra, Pextra;
	extra = {'checked':false, 'value':''}, Pextra;
	if (_P.curExtra != '') {
		eval('Pextra = ' + _P.curExtra);
		if (Pextra['type'] == 'checkbox') {
			extra = Pextra;
		}
	}
	if (extra['checked'] == true) {
		F['Fchecked'].checked = true;
	}
	F.Fvalue.value = extra['value'];
	F.Fvalue.focus();
}

function escSquote(str) {
	return str.replace(/'/g, "\\'", str);
}

function submitMe() {
	var F = _D.forms['phoundry'],
	value = trim(F.Fvalue.value);
	F.Fvalue.value = value;
	
	if (value == '') {
		alert('<?= word(10102); ?>');
		return;
	}

	_P.setExtra("{'type':'checkbox','checked':" + F.Fchecked.checked + ",'value':'" + escSquote(value) + "'}");
	_P.killPopup();
}

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body onload="init()" style="background:#fff;">
<form name="phoundry">
<fieldset>
<legend><b><?= word(10099); ?></b></legend>
<table>
<tr>
	<td><?= word(10100); ?>:</td>
	<td><input class="txt" type="text" name="Fvalue" size="40" /></td>
</tr>
<tr>
	<td><?= word(10101); ?>?</td>
	<td><input type="checkbox" name="Fchecked" value="1" /></td>
</tr>
</table>
</fieldset>
<p align="right">
	<input type="button" value="<?= word(10060); ?>" onclick="submitMe()" />
	<input type="button" value="<?= word(10061); ?>" onclick="_P.killPopup()" />
</p>
</form>
</body>
</html>
