<?
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

?><html>
<head>
<title><?= word(10105); ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function init() {
	var F = _D.forms['phoundry'], extra, Pextra;

	extra = {'value':''};
	if (_P.curExtra != '') {
		eval('Pextra = ' + _P.curExtra);
		if (Pextra['type'] == 'freetext') {
			extra = Pextra;
		}
	}

	if (extra['showLabel'] == true) {
		F['FshowLabel'].checked = true;
	}

	F.Fvalue.value = unescape(extra['value']);


}

function escSquote(str) {
	return str.replace(/'/g, "\\'", str);
}

function submitMe() {
	var F = _D.forms['phoundry'];

	var extra = "{'type':'freetext','value':'" + escape(F.Fvalue.value) + "','showLabel':" + F.FshowLabel.checked + "}";
	_P.setExtra(extra);

	_P.killPopup();
}

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body onload="init()" style="background:#fff;">
<form name="phoundry">
<fieldset>
<legend><b><?= word(10105); ?></b></legend>
<table>
<tr>
	<td><?= word(10106); ?>?</td>
	<td><input type="checkbox" name="FshowLabel" /></td>
</tr>
<tr>
	<td><?= word(10107); ?>:</td>
	<td>
	<textarea class="txt" name="Fvalue" style="width:200px; height:50px"></textarea>
	</td>
</tr>
</table>
</fieldset>

<p align="right">
	<input type="button" value="<?= word(10060); ?>" onclick="submitMe()" />
	<input type="button" value="<?= word(10061); ?>" onclick="_P.killPopup()" />
</p>
</form>
</body>
</html>
