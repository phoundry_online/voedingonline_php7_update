<?php
$inc = @include('PREFS.php');
if ($inc === false) {
	require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
}
require_once "{$PHprefs['distDir']}/core/include/common.php";
require_once INCLUDE_DIR . "/functions/autoloader.include.php";
$TID = getTID();

if (isset($_GET['RID'])) {
	$RID = $_GET['RID'];
	$page = 'copy';
}
else {
	PH::phoundryErrorPrinter(word(10151) /* Invalid form */, true);
}

$site_identifier = isset($_REQUEST['site_identifier']) ?
	trim($_REQUEST['site_identifier']) : '';

if(empty($site_identifier)) {
	PH::phoundryErrorPrinter(word(11800) /* no site selected */, true);
}

// Make sure the current user in the current role has access to this page:
checkAccess($TID, $page);


$db = DB::getDb();

if(!empty($_POST['form_code'])) {
	$db->iQuery(sprintf("
		INSERT INTO brickwork_avoid_formbuilder
		(SELECT '%s', '%s', elements, NOW(), NULL
		FROM brickwork_avoid_formbuilder
		WHERE form_code = '%s'
		AND site_identifier = '%s');",
		$db->quote($_POST['form_code']),
		$db->quote($site_identifier),
		$db->quote($RID),
		$db->quote($site_identifier)
	));
	
	trigger_redirect($PHprefs['url'].
		"/custom/brickwork/avoid_formbuilder/edit.php?".
		QS(false, 'RID='.$_POST['form_code']), 302);
}

$forms = $db->iQuery(sprintf("
	SELECT
		bf.code,
		bf.title
	FROM
		brickwork_form AS bf
	LEFT JOIN brickwork_avoid_formbuilder AS baf
		ON bf.code = baf.form_code
		AND baf.site_identifier = bf.site_identifier
	WHERE
		bf.site_identifier = '%s'
	AND
		baf.form_code IS NULL
	AND
		bf.type_code = 'avoid'
	ORDER BY
		title ASC",
	$site_identifier
));

?>
<?php PH::getHeader("text/html")?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title><?= word(10038); ?></title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/form.css" type="text/css" />
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
</head>
<body class="frames">
<form action="copy.php?<?=QS(1)?>" method="post" onsubmit="return checkForm(this)">
<div class="headerFrame">
<?=getHeader(word(10058), 109)?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td></td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->
<fieldset>
<legend><b class="req"><?=word(10015)?></b></legend>
<select name="form_code">
	<option value="">[<?=word(10016)?>]</option>
	<?foreach ($forms as $form):?>
	<option value="<?=htmlspecialchars($form->code)?>"><?=htmlspecialchars($form->title)?></option>
	<?endforeach?>
</select>
</fieldset>


<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	<input type="submit" value="<?=word(10060)?>" class="okBut" />
	</td>
	<td align="right">
	<input type="button" value="<?=word(10061)?>" onclick="_D.location='<?= $PHprefs['url'] ?>/core/records.php?<?= QS(0, 'RID') ?>'" />
	</td>
</tr></table>
</div>

</form>
</body>
</html>
