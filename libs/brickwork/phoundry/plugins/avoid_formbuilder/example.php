<?php

// @deprecated?

	$inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";
	$TID = getTID();

	if (isset($PHprefs['PHenvFunction']))
		eval('$PHenv = ' . $PHprefs['PHenvFunction'] . '();');

	// Make sure the current user in the current role has access to this page:
	checkAccess($TID, 'view');

	$RID = (int)$_GET['RID'];
	$form_els = array('extra'=>array());
	
	$sql = "SELECT elements FROM event_form WHERE id = {$RID}";
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
	if (!$db->EndOfResult($cur)) {
		$form_els = unserialize($db->FetchResult($cur,0,'elements'));
	}
PH::getHeader('text/html');
?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Form</title>
<script type="text/javascript" src="fv_english.js"></script>
<script type="text/javascript" src="fv_engine.js"></script>
<script type="text/javascript">
//<![CDATA[

//]]>
</script>
<style type="text/css">
<!--

.fv-req {
	background-color: #fcc;
}

.fv-forget {
	background-color: #fcc;
	border: 2px solid #f00;
}

.fv-syntax {
	background-color: #fcc;
	border: 2px solid #f00;
}

-->
</style>
</head>
<body>
<form action="process.php" method="post" alt="fv" onsubmit="return _formCheck(this)">
<?php

function js2php($struct) {
	$struct = str_replace('{', 'array(', $struct);
	$struct = str_replace('}', ')', $struct);
	$struct = str_replace(':', '=>', $struct);
	return $struct;
}

function getForm($els) {
	$html = '';
	
	for ($x = 0; $x < count($els['extra']); $x++) { 
		$name  = htmlspecialchars($els['name'][$x]);
		$title = htmlspecialchars($els['text'][$x]);
		$info  = htmlspecialchars($els['info'][$x]);
		$alt   = isset($els['required'][$x]) ? $els['required'][$x] : '';
		eval('$extra = ' . js2php($els['extra'][$x]) . ';');

		if (!empty($els['syntax'][$x])) { $alt .= '|' . $els['syntax'][$x]; }

		$html .= '<fieldset>';
		$html .= '<legend><b>' . $title . '</b></legend>';
		if (!empty($info)) {
			$html .= $info . '<br />';
		}
		switch($els['type'][$x]) {
			case 'text':
				$html .= '<input type="text" name="' . $name . '" title="' . $title . '" alt="' . $alt . '" size="' . $extra['size'] . '" value="' . htmlspecialchars($extra['value']) . '" />';
				break;
			case 'password':
				$html .= '<input type="password" name="' . $name . '" title="' . $title . '" alt="' . $alt . '" size="' . $extra['size'] . '" value="' . htmlspecialchars($extra['value']) . '" />';
				break;
			case 'hidden':
				$html .= '<input type="hidden" name="' . $name . '" value="' . htmlspecialchars($extra['value']) . '" />';
				break;
			case 'select':
				$html .= '<select name="' . $name . '" title="' . $title . '" alt="' . $alt . '" size="' . $extra['size'] . '"';
				if ($extra['multiple'] == 'true') {
					$html .= 'multiple="multiple"';
				}
				$html.= '>';
				foreach($extra['options'] as $value=>$name) {
					$html .= '<option value="' . htmlspecialchars($value) . '">' . htmlspecialchars($name) . '</option>';
				}
				$html .= '</select>';
				break;
			case 'radio':
				$i = 0;
				foreach($extra['options'] as $value=>$name) {
					$html .= '<input type="radio" name="' . $name . '" value="' . $value . '"';
					if ($i == 0) {
						$html .= ' title="' . $title . '" alt="' . $alt . '"';
					}
					$html .= ' />';
					$html .= htmlspecialchars($name);
					$i++;
				}
				break;
			case 'checkbox':
				$checked = ($extra['checked']) ? ' checked="checked" ' : ' ';
				$html .= '<input type="checkbox" name="' . $name . '" title="' . $title . '" alt="' . $alt . '"' . $checked . 'value="' . htmlspecialchars($extra['value']) . '" />';
				break;
			case 'textarea':
				$html .= '<textarea name="' . $name . '" title="' . $title . '" alt="' . $alt . '" cols="' . $extra['columns'] . '"  rows="' . $extra['rows'] . '">' . htmlspecialchars($extra['value']) . '</textarea>';
				break;
			case 'submit':
			case 'reset':
		}
		$html .= "</fieldset>\n";
	}
	return $html;
}

print getForm($form_els);

?>
<input type="submit" value="Ok" />
</form>
</body>
</html>
