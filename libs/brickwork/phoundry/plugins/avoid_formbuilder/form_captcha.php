<?
	$inc = @include('PREFS.php');
	if ($inc === false)
	{
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	require_once "{$PHprefs['distDir']}/core/include/common.php";

?>
<html>
<head>
<title><?= word(10095); ?></title>
<script type="text/javascript">
//<![CDATA[

var _W=window, _D=document, _P=parent;

function checkDigit(nr, allowPercent) {
	if (allowPercent)
		return nr.replace(/[^0-9%]/g, '');
	else
		return nr.replace(/[^0-9]/g, '');
}

function init() {
	var F = _D.forms['phoundry'], extra, Pextra;
	extra = {'value':''}, ask = {'ask':''},Pextra;
	if (_P.curExtra != '') {
		eval('Pextra = ' + _P.curExtra);
		if (Pextra['type'] == 'captcha') {
			extra = Pextra;
		}
		if(Pextra['ask']=='always'){
			F.Fask[0].checked = true;
		} else {
			F.Fask[1].checked = true;
		}
	}
	F.Fvalue.value = extra['value'];
	F.Fvalue.focus();
}

function escSquote(str) {
	return str.replace(/'/g, "\\'", str);
}

function submitMe() {
	var F = _D.forms['phoundry'];
	if(!checkDigit(F.Fvalue.value) || F.Fvalue.value < 1 || F.Fvalue.value > 7){
		alert('<?= word(10096, 1, 7); ?>'); 
		return false;
	}
	var ask = F.Fask[0].checked ? F.Fask[0].value : F.Fask[1].value;
	var extra = "{'type':'captcha','value':'" + escSquote(F.Fvalue.value) + "','ask':'"+ escSquote(ask) +"'}";
	_P.setExtra(extra);

	_P.killPopup();
}

//]]>
</script>
<link rel="stylesheet" href="popup.css" type="text/css" />
</head>
<body onload="init()" style="background:#fff;">
<form name="phoundry">
<fieldset>
<legend><b><?= word(10095); ?></b></legend>
<table>
<tr>
	<td><?= word(10148); ?>:</td>
	<td><input class="txt" type="text" name="Fvalue" size="5" maxlength="2" /></td>
</tr>
<tr>
	<td><?= word(10149); ?>:</td>
	<td>
	
	<input type="radio" name="Fask" value="always" id="ask_always" checked="checked" /><label for="ask_always"><?= word(10097); ?></label>
	<input type="radio" name="Fask" value="once" id="ask_once" /><label for="ask_once"><?= word(10098); ?></label>
	
	</td>
</tr>
</table>
</fieldset>
<p align="right">
	<input type="button" value="<?= word(10060); ?>" onclick="submitMe()" />
	<input type="button" value="<?= word(10061); ?>" onclick="_P.killPopup()" />
</p>
</form>
</body>
</html>
