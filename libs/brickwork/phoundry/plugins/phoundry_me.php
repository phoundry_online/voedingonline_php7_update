<?php
   $inc = @include('PREFS.php');
	if ($inc === false) {
		require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname($_SERVER['SCRIPT_FILENAME']))) . '/PREFS.php');
	}
	include_once "{$PHprefs['distDir']}/core/include/common.php";
   checkAccess();

   	// get table id
	$sql = sprintf('SELECT id FROM phoundry_table WHERE name = "%s" LIMIT 1', 'brickwork_page');
	$cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);
		
	if ($db->EndOfResult($cur)) {
		trigger_error('page table not found', E_USER_ERROR);
	}
	$table_id = $db->fetchResult($cur,0, 'id');
	
	$page = !empty($_GET['page']) ? $_GET['page'] : '';
	if(empty($page)){
		header('Location: home.php');
		exit;
	} else {
		// page gevonden, nu ontleden en de juiste settings in het menu selecteren en redirecten naar de edit page
		$foo = explode(':', $page);
		if(count($foo)!=2){
			header('Location: home.php');
			exit;
		} else {
			$site_identifier = $foo[0];
			$rid = $foo[1];
		}
	}

	$sql = sprintf('SELECT s.identifier, s.name FROM brickwork_site s, brickwork_site_structure ss, brickwork_page p WHERE s.identifier = "%s" and p.site_structure_id = ss.id AND p.id = %d', escDBquote($site_identifier), $rid );
	 $cur = $db->Query($sql)
		or trigger_error("Query $sql failed: " . $db->Error(), E_USER_ERROR);

	if ($db->EndOfResult($cur)) {
		header('Location: home.php');
		exit;
	}
	else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?= $PHprefs['charset'] ?>" />
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title>Kies menu</title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php"></script>
<script type="text/javascript">
//<![CDATA[
	function redirectToEditScreen(){
		if(parent.frames[1]){
			parent.frames[1].document.location='<?= $PHprefs['url'] ?>/layout/menu.php?site_identifier=<?=$site_identifier;?>';
			document.location='<?= $PHprefs['url'] ?>/custom/brickwork/page_layout/edit.php?TID=<?=$table_id;?>&site_identifier=<?=$site_identifier;?>&page_id=<?=$rid;?>';
		} else {
			<?
			$redir = '/phoundry/custom/phoundry_me.php?page='.urlencode($page);
			?>
			document.location='<?= $PHprefs['url'] ?>/?_page=<?=urlencode($redir); ?>';
		}
	}
//]]>
</script>
</head>
<body class="frames" onload="redirectToEditScreen()">
<div class="headerFrame">
<?= getHeader('Phoundry Me'); ?>
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>&nbsp;</td>
</tr></table>
</div>

<div class="contentFrame" style="padding:6px">
<!-- Content -->

Bezig met het laden van het juiste edit scherm
<!-- /Content -->
</div>

<div class="footerFrame">
<table class="buttons" cellspacing="0" cellpadding="2"><tr>
<td>
</td>
<td align="right">
<input type="button" value="Annuleer" onclick="_D.location='home.php'" />
</td>
</tr></table>
</div>

</body>
</html>

<?
	}
?>
