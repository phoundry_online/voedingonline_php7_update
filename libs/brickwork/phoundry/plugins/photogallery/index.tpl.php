<?php
PH::getHeader('text/html');
?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="Author" content="Web Power BV, www.webpower.nl" />
<title><?=word(11704 /* Foto Gallerij */)?></title>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/phoundry.css" type="text/css" />
<!--[if IE]>
<link rel="stylesheet" href="<?= $PHprefs['cssUrl'] ?>/msie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?= $PHprefs['url'] ?>/core/csjs/global.js.php?<?= QS(1) ?>"></script>
<style type="text/css">
fieldset legend {
	font-weight: bold;
}

button
{
	padding: 0;
}

ul,
li
{
	padding: 0;
	margin : 0;
}

</style>
</head>
<body class="frames">
<form action="" method="post" enctype="multipart/form-data">
<div id="headerFrame" class="headerFrame">
	<?= getHeader(word(11704 /* Foto Gallerij */), word(11705 /* Batch upload */));?>
	<table id="headerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
	<td>
	
	</td>
	</tr></table>
</div>

<div id="contentFrame" class="contentFrame">
<!-- Content -->
<?if(!empty($added)):?>
<fieldset>
	<legend><?=word(11706 /* Success */)?></legend>
	<?=word(11707 /* Succesvol #1 foto(s) toegevoegt! */, $added)?>
</fieldset>
<?endif?>
<?if(!empty($errors)): ?>
<fieldset class="errors">
	<legend><?=word(11708 /* Errors */)?></legend>
	<?foreach($errors as $error):?>
	<div><?=htmlspecialchars(ucfirst($error))?></div>
	<?endforeach?>
</fieldset>
<?endif?>
<fieldset>
	<legend><?=word(11709 /* Album */)?></legend>
	<?=ActiveRecord::factory('Photogallery_Album', Utility::arrayValue($_REQUEST, 'album'))->offsetGet('title')?>
</fieldset>
<fieldset>
	<legend><?=word(11710 /* Map naam */)?></legend>
	<div class="miniinfo"><?=word(11713 /* Als de directory nog niet bestaat wordt deze aangemaakt */)?></div>
	<input name="directory" type="text" value="<?=Utility::arrayValue($_REQUEST, 'directory')?>" />
</fieldset>
<fieldset>
	<legend><?=word(11711 /* Bestand */)?></legend>
	<div class="miniinfo"><?=word(11714 /* Het te uploaden bestand dient een Zip bestand te zijn. Alle andere bestanden die geen geldige foto extensie hebben zullen worden overgeslagen, evenals fotos in submappen. */)?></div>
	<input name="zipfile" type="file" /> <?=word(11712 /* (Max #1) */, str_replace('M', 'MB', ini_get('upload_max_filesize')))?>
</fieldset>
<!-- /Content -->
</div>

<div id="footerFrame" class="footerFrame">
	<table id="footerButtons" class="buttons" cellspacing="0" cellpadding="2"><tr>
		<td>
			<input type="submit" value="<?=word(82)/* Ok */?>" />
		</td>
		<td align="right">
			<input type="button" value="<?= word(46) ?>" onclick="_D.location='../home.php'" />
		</td>
	</tr></table>
</div>
</form>
</body>
</html>	