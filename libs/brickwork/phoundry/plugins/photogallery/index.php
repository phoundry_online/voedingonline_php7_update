<?php
require_once((getenv('CONFIG_DIR') !== false ? getenv('CONFIG_DIR') : dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) . '/PREFS.php');
require_once "{$PHprefs['distDir']}/core/include/phoundry.php";
require_once INCLUDE_DIR.'/functions/autoloader.include.php';

if(!isset($_REQUEST['album'])) {
	PH::phoundryErrorPrinter(word(11700 /* album niet gevonden */), true);
}

if(Utility::isPosted())
{
	$errors = array();
	if(empty($_REQUEST['album']))
	{
		$errors[] = word(11700 /* album niet gevonden */);
	}
	else
	{
		$album = ActiveRecord::factory('Photogallery_Album', $_REQUEST['album']);
		if(!$album->isLoaded())
		{
			$errors[] = word(11700 /* album niet gevonden */);
		}
	}
	
	$uploaddir = '/IMAGES/photogallery/'.str_replace('.', '', $_POST['directory']);
	if(!is_dir(UPLOAD_DIR.$uploaddir) && !mkdir(UPLOAD_DIR.$uploaddir, 0777, true))
	{
		$errors[] = word(11701 /* ongeldige doelmap */);
	}
	
	if(
		!isset($_FILES['zipfile']) ||
		!is_uploaded_file($_FILES['zipfile']['tmp_name']) ||
		'.zip' != substr($_FILES['zipfile']['name'], -4) ||
		$_FILES['zipfile']['error'] != 0
	)
	{
		$errors[] = word(11702 /* geen geldig bestand geupload */);
	}
	else
	{
		$tempname = tempnam(TMP_DIR, 'photogallery');
		if(!move_uploaded_file($_FILES['zipfile']['tmp_name'], $tempname))
		{
			$errors[] = word(11703 /* bestand kon niet worden verplaatst */);
		}
	}
	
	
	if(empty($errors))
	{
		$added = 0;
		$prio = 0;
		$exts = array('jpg', 'jpeg', 'gif', 'png');
		$zipfile = new ZipFile(basename($tempname), dirname($tempname).'/');
		$tempdir = tempnam(TMP_DIR, 'photogallery');
		unlink($tempdir); // remove the file tempnam created, we just want a name!
		mkdir($tempdir, 0777);
		$zipfile->extractArchive($tempdir);
		foreach(new DirectoryIterator($tempdir) as $entry)
		{
			if(
				'.' == $entry ||
				'..' == $entry ||
				!$entry->isFile() ||
				!$entry->isReadable() ||
				!in_array(strtolower(substr($entry, strrpos($entry, '.')+1)), $exts)
			) continue;
			
			$location = $uploaddir.'/'.$entry;
			if(copy($entry->getPathname(), UPLOAD_DIR.$location))
			{
				$new_photo = ActiveRecord::factory('Photogallery_Photo');
				$new_photo['album_id'] = $album['id'];
				$new_photo['title'] = ucfirst(str_replace('_', ' ', substr($entry, 0, strrpos($entry, '.'))));
				$new_photo['location'] = $location;
				$new_photo['pubdate'] = date('Y-m-d H:i:s');
				$new_photo['prio'] = ++$prio;
				if($new_photo->save())
				{
					$added++;
				}
			}
		}
		
		// remove temp files
		PH::RmDirR($tempdir);
		unlink($tempname);
	}
}

include 'index.tpl.php';
