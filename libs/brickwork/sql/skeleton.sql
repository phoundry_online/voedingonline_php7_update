
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `brickwork_abuse_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_abuse_report` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `url` varchar(255) NOT NULL default '',
  `hash` varchar(20) default NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `action` varchar(128) default '',
  `status` enum('open','closed','pending') NOT NULL default 'open',
  `user` varchar(128) default NULL,
  `ip` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `site_identifier` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `status` (`status`),
  KEY `site` (`site_identifier`),
  KEY `created_on` (`created_on`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_abuse_report` WRITE;
/*!40000 ALTER TABLE `brickwork_abuse_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_abuse_report` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_auth_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_auth_object` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(10) unsigned default NULL,
  `instanceid` varchar(80) NOT NULL default '',
  `description` varchar(255) default NULL,
  `class` varchar(80) NOT NULL,
  `foreign_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`instanceid`),
  KEY `parent_id` (`parent_id`),
  KEY `FK_FOREIGN_ENTITY` (`class`,`foreign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_auth_object` WRITE;
/*!40000 ALTER TABLE `brickwork_auth_object` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_auth_object` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_auth_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_auth_right` (
  `id` int(11) NOT NULL auto_increment,
  `auth_object_id` int(11) NOT NULL,
  `auth_role_id` int(11) NOT NULL,
  `grant` enum('deny','allow') default 'allow',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `auth_object_id` (`auth_object_id`,`auth_role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_auth_right` WRITE;
/*!40000 ALTER TABLE `brickwork_auth_right` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_auth_right` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_auth_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_auth_role` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(11) default NULL,
  `name` varchar(80) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_auth_role` WRITE;
/*!40000 ALTER TABLE `brickwork_auth_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_auth_role` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_avoid_form_submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_avoid_form_submission` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `insert_date` datetime default NULL,
  `form_code` varchar(20) NOT NULL default '',
  `hash` varchar(32) NOT NULL default '',
  `ip` int(10) unsigned NOT NULL default '0',
  `fields_object` blob,
  `form_name` varchar(255) default NULL,
  `url` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `hash` (`form_code`,`hash`(5))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_avoid_form_submission` WRITE;
/*!40000 ALTER TABLE `brickwork_avoid_form_submission` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_avoid_form_submission` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_avoid_form_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_avoid_form_upload` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(10) NOT NULL,
  `site_identifier` varchar(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `hashcode` varchar(32) default NULL,
  `downloadcount` int(10) unsigned NOT NULL default '0',
  `removed` tinyint(3) unsigned NOT NULL default '0',
  `filesize` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_avoid_form_upload` WRITE;
/*!40000 ALTER TABLE `brickwork_avoid_form_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_avoid_form_upload` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_avoid_formbuilder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_avoid_formbuilder` (
  `form_code` varchar(20) NOT NULL default '',
  `site_identifier` varchar(20) NOT NULL default '',
  `elements` text NOT NULL,
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`form_code`,`site_identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_avoid_formbuilder` WRITE;
/*!40000 ALTER TABLE `brickwork_avoid_formbuilder` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_avoid_formbuilder` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_content_container`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_content_container` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(20) NOT NULL default '',
  `page_type_code` varchar(20) NOT NULL default '',
  `prio` decimal(12,4) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`,`page_type_code`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_content_container` WRITE;
/*!40000 ALTER TABLE `brickwork_content_container` DISABLE KEYS */;
INSERT INTO `brickwork_content_container` VALUES (1,'cc_content','welcome',NULL);
/*!40000 ALTER TABLE `brickwork_content_container` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_content_container_allows_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_content_container_allows_module` (
  `content_container_id` int(10) unsigned NOT NULL default '0',
  `module_code` char(20) NOT NULL default '',
  PRIMARY KEY  (`content_container_id`,`module_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_content_container_allows_module` WRITE;
/*!40000 ALTER TABLE `brickwork_content_container_allows_module` DISABLE KEYS */;
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'agenda');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'avoidform');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'banner');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'brickworknews');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'deeplink');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'faq');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'google_adsense');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'google_maps');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'htdig_search');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'htmlblock');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'htmlcontent');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'iframe');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'link');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'menu');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'photogallery');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'poll');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'reactions');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'sitemap');
INSERT INTO `brickwork_content_container_allows_module` VALUES (1,'snippet');
/*!40000 ALTER TABLE `brickwork_content_container_allows_module` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_content_container_default_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_content_container_default_module` (
  `content_container_id` int(10) unsigned NOT NULL default '0',
  `module_code` char(20) NOT NULL default '',
  PRIMARY KEY  (`content_container_id`,`module_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_content_container_default_module` WRITE;
/*!40000 ALTER TABLE `brickwork_content_container_default_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_content_container_default_module` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_country` (
  `code` varchar(10) NOT NULL default '',
  `name` varchar(20) default NULL,
  PRIMARY KEY  (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_country` WRITE;
/*!40000 ALTER TABLE `brickwork_country` DISABLE KEYS */;
INSERT INTO `brickwork_country` VALUES ('AN','Nederlandse Antillen');
INSERT INTO `brickwork_country` VALUES ('AT','Oostenrijk');
INSERT INTO `brickwork_country` VALUES ('AU','Australië');
INSERT INTO `brickwork_country` VALUES ('BE','België');
INSERT INTO `brickwork_country` VALUES ('CA','Canada');
INSERT INTO `brickwork_country` VALUES ('CH','Zwitserland');
INSERT INTO `brickwork_country` VALUES ('CN','China');
INSERT INTO `brickwork_country` VALUES ('CZ','Tsjechië');
INSERT INTO `brickwork_country` VALUES ('DE','Duitsland');
INSERT INTO `brickwork_country` VALUES ('DK','Denemarken');
INSERT INTO `brickwork_country` VALUES ('ES','Spanje');
INSERT INTO `brickwork_country` VALUES ('FI','Finland');
INSERT INTO `brickwork_country` VALUES ('FR','Frankrijk');
INSERT INTO `brickwork_country` VALUES ('GB','Verenigd Koninkrijk');
INSERT INTO `brickwork_country` VALUES ('GR','Griekenland');
INSERT INTO `brickwork_country` VALUES ('IE','Ierland');
INSERT INTO `brickwork_country` VALUES ('IL','Israël');
INSERT INTO `brickwork_country` VALUES ('IT','Italië');
INSERT INTO `brickwork_country` VALUES ('JP','Japan');
INSERT INTO `brickwork_country` VALUES ('LU','Luxemburg');
INSERT INTO `brickwork_country` VALUES ('MA','Marokko');
INSERT INTO `brickwork_country` VALUES ('MX','Mexico');
INSERT INTO `brickwork_country` VALUES ('NL','Nederland');
INSERT INTO `brickwork_country` VALUES ('NO','Noorwegen');
INSERT INTO `brickwork_country` VALUES ('NZ','Nieuw-Zeeland');
INSERT INTO `brickwork_country` VALUES ('PT','Portugal');
INSERT INTO `brickwork_country` VALUES ('SE','Zweden');
INSERT INTO `brickwork_country` VALUES ('SR','Suriname');
INSERT INTO `brickwork_country` VALUES ('TR','Turkije');
INSERT INTO `brickwork_country` VALUES ('US','Verenigde Staten');
INSERT INTO `brickwork_country` VALUES ('ZA','Zuid-Afrika');
INSERT INTO `brickwork_country` VALUES ('NA','Nederlandse Antillen');
/*!40000 ALTER TABLE `brickwork_country` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_country_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_country_2` (
  `iso` char(2) NOT NULL default '',
  `name_en` varchar(63) default NULL,
  `name_fr` varchar(63) default NULL,
  `name_es` varchar(63) default NULL,
  `name_de` varchar(63) default NULL,
  `name_nl` varchar(63) default NULL,
  `name_pl` varchar(63) default NULL,
  PRIMARY KEY  (`iso`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_country_2` WRITE;
/*!40000 ALTER TABLE `brickwork_country_2` DISABLE KEYS */;
INSERT INTO `brickwork_country_2` VALUES ('AD','Andorra','Andorre','Andorra','Andorra','Andorra','Andora');
INSERT INTO `brickwork_country_2` VALUES ('AE','United Arab Emirates','Émirats arabes unis','Emiratos Árabes Unidos','Vereinigte Arabische Emirate','Verenigde Arabische Emiraten','Zjednoczone Emiraty Arabskie');
INSERT INTO `brickwork_country_2` VALUES ('AF','Afghanistan','Afghanistan','Afganistán','Afghanistan','Afghanistan','Afganistan');
INSERT INTO `brickwork_country_2` VALUES ('AG','Antigua and Barbuda','Antigua-et-Barbuda','Antigua y Barbuda','Antigua und Barbuda','Antigua en Barbuda','Antigua i Barbuda');
INSERT INTO `brickwork_country_2` VALUES ('AI','Anguilla','Anguilla','Anguila','Anguilla','Anguilla','Anguilla');
INSERT INTO `brickwork_country_2` VALUES ('AL','Albania','Albanie','Albania','Albanien','Albanië','Albania');
INSERT INTO `brickwork_country_2` VALUES ('AM','Armenia','Arménie','Armenia','Armenien','Armenië','Armenia');
INSERT INTO `brickwork_country_2` VALUES ('AN','Netherlands Antilles','Antilles néerlandaises','Antillas Neerlandesas','Niederländische Antillen','Nederlandse Antillen','Antyle Holenderskie');
INSERT INTO `brickwork_country_2` VALUES ('AO','Angola','Angola','Angola','Angola','Angola','Angola');
INSERT INTO `brickwork_country_2` VALUES ('AQ','Antarctica','Antarctique','Antártida','Antarktis','Antarctica','Antarktyka');
INSERT INTO `brickwork_country_2` VALUES ('AR','Argentina','Argentine','Argentina','Argentinien','Argentinië','Argentyna');
INSERT INTO `brickwork_country_2` VALUES ('AS','American Samoa','Samoa américaines','Samoa Americana','Amerikanisch-Samoa','Amerikaans-Samoa','Samoa Ameryka&#324;skie');
INSERT INTO `brickwork_country_2` VALUES ('AT','Austria','Autriche','Austria','Österreich','Oostenrijk','Austria');
INSERT INTO `brickwork_country_2` VALUES ('AU','Australia','Australie','Australia','Australien','Australië','Australia');
INSERT INTO `brickwork_country_2` VALUES ('AW','Aruba','Aruba','Aruba','Aruba','Aruba','Aruba');
INSERT INTO `brickwork_country_2` VALUES ('AZ','Azerbaijan','Azerbaïdjan','Azerbaiyán','Aserbaidschan','Azerbeidzjan','Azerbejd&#380;an');
INSERT INTO `brickwork_country_2` VALUES ('BA','Bosnia and Herzegovina','Bosnie-Herzégovine , la  Bosnie-et-Herzégovine','Bosnia y Hercegovina','Bosnien und Herzegowina','Bosnië en Herzegovina','Bo&#347;nia i Hercegowina');
INSERT INTO `brickwork_country_2` VALUES ('BB','Barbados','Barbade','Barbados','Barbados','Barbados','Barbados');
INSERT INTO `brickwork_country_2` VALUES ('BD','Bangladesh','Bangladesh','Bangladesh','Bangladesch','Bangladesh','Bangladesz');
INSERT INTO `brickwork_country_2` VALUES ('BE','Belgium','Belgique','Bélgica','Belgien','België','Belgia');
INSERT INTO `brickwork_country_2` VALUES ('BF','Burkina Faso','Burkina Faso','Burkina Faso','Burkina Faso','Burkina Faso','Burkina Faso');
INSERT INTO `brickwork_country_2` VALUES ('BG','Bulgaria','Bulgarie','Bulgaria','Bulgarien','Bulgarije','Bu&#322;garia');
INSERT INTO `brickwork_country_2` VALUES ('BH','Bahrain','Bahreïn','Bahráin','Bahrain','Bahrein','Bahrajn');
INSERT INTO `brickwork_country_2` VALUES ('BI','Burundi','Burundi','Burundi','Burundi','Burundi','Burundi');
INSERT INTO `brickwork_country_2` VALUES ('BJ','Benin','Bénin','Benín','Benin','Benin','Benin');
INSERT INTO `brickwork_country_2` VALUES ('BM','Bermuda','Bermudes','Bermudas','die Bermudas','Bermuda','Bermudy');
INSERT INTO `brickwork_country_2` VALUES ('BN','Brunei','Brunei','Brunéi','Brunei Darussalam','Brunei','Brunei Darussalam');
INSERT INTO `brickwork_country_2` VALUES ('BO','Bolivia','Bolivie','Bolivia','Bolivien','Bolivia','Boliwia');
INSERT INTO `brickwork_country_2` VALUES ('BR','Brazil','Brésil','Brasil','Brasilien','Brazilië','Brazylia');
INSERT INTO `brickwork_country_2` VALUES ('BS','The Bahamas','Bahamas','Bahamas','Bahamas','Bahama\'s','Bahamy');
INSERT INTO `brickwork_country_2` VALUES ('BT','Bhutan','Bhoutan','Bután','Bhutan','Bhutan','Bhutan');
INSERT INTO `brickwork_country_2` VALUES ('BV','Bouvet Island','Ile Bouvet','Isla Bouvet','Bouvetinsel','Bouveteiland','Wyspa Bouvet');
INSERT INTO `brickwork_country_2` VALUES ('BW','Botswana','Botswana','Botsuana','Botsuana','Botswana','Botswana');
INSERT INTO `brickwork_country_2` VALUES ('BY','Belarus','Biélorussie ; le Belarus','Bielorrusia','Belarus','Belarus; Wit-Rusland','Bia&#322;oru&#347;');
INSERT INTO `brickwork_country_2` VALUES ('BZ','Belize','Belize','Belice','Belize','Belize','Belize');
INSERT INTO `brickwork_country_2` VALUES ('CA','Canada','Canada','Canadá','Kanada','Canada','Kanada');
INSERT INTO `brickwork_country_2` VALUES ('CC','Cocos (Keeling) Islands','Iles des Cocos (Keeling)','Islas Cocos','Kokosinseln','Cocoseilanden','Wyspy Kokosowe (Wyspy Keelinga)');
INSERT INTO `brickwork_country_2` VALUES ('CD','Democratic Republic of the Congo','République démocratique du Congo','República Democrática del Congo','Demokratische Republik Kongo','Democratische Republiek Congo','Kongo (Demokratyczna Republika Konga)');
INSERT INTO `brickwork_country_2` VALUES ('CF','Central African Republic','République centrafricaine','República Centroafricana','Zentralafrikanische Republik','Centraal-Afrikaanse Republiek','Republika &#346;rodkowoafryka&#324;ska');
INSERT INTO `brickwork_country_2` VALUES ('CG','Congo','Congo','Congo','Kongo','Congo','Kongo');
INSERT INTO `brickwork_country_2` VALUES ('CH','Switzerland','Suisse','Suiza','Schweiz','Zwitserland','Szwajcaria');
INSERT INTO `brickwork_country_2` VALUES ('CI','Côte d\'Ivoire','Côte d\'Ivoire','Costa de Marfil','Côte d\'Ivoire','Ivoorkust','Wybrze&#380;e Ko&#347;ci S&#322;oniowej (Côte d\'Ivoire)');
INSERT INTO `brickwork_country_2` VALUES ('CK','Cook Islands','Iles Cook','Islas Cook','Cookinseln','Cookeilanden','Wyspy Cooka');
INSERT INTO `brickwork_country_2` VALUES ('CL','Chile','Chili','Chile','Chile','Chili','Chile');
INSERT INTO `brickwork_country_2` VALUES ('CM','Cameroon','Cameroun','Camerún','Kamerun','Kameroen','Kamerun');
INSERT INTO `brickwork_country_2` VALUES ('CN','China','Chine','China','China','China','Chiny');
INSERT INTO `brickwork_country_2` VALUES ('CO','Colombia','Colombie','Colombia','Kolumbien','Colombia','Kolumbia');
INSERT INTO `brickwork_country_2` VALUES ('CR','Costa Rica','Costa Rica','Costa Rica','Costa Rica','Costa Rica','Kostaryka');
INSERT INTO `brickwork_country_2` VALUES ('CU','Cuba','Cuba','Cuba','Kuba','Cuba','Kuba');
INSERT INTO `brickwork_country_2` VALUES ('CV','Cape Verde','Cap-Vert','Cabo Verde','Kap Verde','Kaapverdië','Wyspy Zielonego Przyl&#261;dka');
INSERT INTO `brickwork_country_2` VALUES ('CX','Christmas Island','Ile Christmas','Isla Christmas','Weihnachtsinsel','Christmaseiland','Wyspa Bo&#380;ego Narodzenia');
INSERT INTO `brickwork_country_2` VALUES ('CY','Cyprus','Chypre','Chipre','Zypern','Cyprus','Cypr');
INSERT INTO `brickwork_country_2` VALUES ('CZ','Czech Republic','République tchèque','República Checa; Chequia','Tschechische Republik','Tsjechië','Czechy');
INSERT INTO `brickwork_country_2` VALUES ('DE','Germany','Allemagne','Alemania','Deutschland','Duitsland','Niemcy');
INSERT INTO `brickwork_country_2` VALUES ('DJ','Djibouti','Djibouti','Yibuti','Dschibuti','Djibouti','D&#380;ibuti');
INSERT INTO `brickwork_country_2` VALUES ('DK','Denmark','Danemark','Dinamarca','Dänemark','Denemarken','Dania');
INSERT INTO `brickwork_country_2` VALUES ('DM','Dominica','Dominique','Dominica','Dominica','Dominica','Dominika');
INSERT INTO `brickwork_country_2` VALUES ('DO','Dominican Republic','République dominicaine','República Dominicana','Dominikanische Republik','Dominicaanse Republiek','Dominikana');
INSERT INTO `brickwork_country_2` VALUES ('DZ','Algeria','Algérie','Argelia','Algerien','Algerije','Algieria');
INSERT INTO `brickwork_country_2` VALUES ('EC','Ecuador','Équateur','Ecuador','Ecuador','Ecuador','Ekwador');
INSERT INTO `brickwork_country_2` VALUES ('EE','Estonia','Estonie','Estonia','Estland','Estland','Estonia');
INSERT INTO `brickwork_country_2` VALUES ('EG','Egypt','Égypte','Egipto','Ägypten','Egypte','Egipt');
INSERT INTO `brickwork_country_2` VALUES ('EH','Western Sahara','Sahara occidental','Sáhara Occidental','Westsahara','Westelijke Sahara','Sahara Zachodnia');
INSERT INTO `brickwork_country_2` VALUES ('ER','Eritrea','Érythrée','Eritrea','Eritrea','Eritrea','Erytrea');
INSERT INTO `brickwork_country_2` VALUES ('ES','Spain','Espagne','España','Spanien','Spanje','Hiszpania');
INSERT INTO `brickwork_country_2` VALUES ('ET','Ethiopia','Éthiopie','Etiopía','Äthiopien','Ethiopië','Etiopia');
INSERT INTO `brickwork_country_2` VALUES ('FI','Finland','Finlande','Finlandia','Finnland','Finland','Finlandia');
INSERT INTO `brickwork_country_2` VALUES ('FJ','Fiji','Iles Fidji','Fiyi','Fidschi','Fiji','Fid&#380;i');
INSERT INTO `brickwork_country_2` VALUES ('FK','Falkland Islands','Iles Falkland','Islas Malvinas','Falklandinseln','Falklandeilanden','Falklandy (Malwiny)');
INSERT INTO `brickwork_country_2` VALUES ('FM','Micronesia','Micronésie','Micronesia','Mikronesien','Micronesia','Mikronezja, Sfederowane Stany');
INSERT INTO `brickwork_country_2` VALUES ('FO','Faeroe Islands','Iles Féroé','Islas Feroe','Färöer','Faeröer','Wyspy Owcze');
INSERT INTO `brickwork_country_2` VALUES ('FR','France','France','Francia','Frankreich','Frankrijk','Francja');
INSERT INTO `brickwork_country_2` VALUES ('GA','Gabon','Gabon','Gabón','Gabun','Gabon','Gabon');
INSERT INTO `brickwork_country_2` VALUES ('GB','United Kingdom','Royaume-Uni','Reino Unido','Vereinigtes Königreich','Verenigd Koninkrijk','Wielka Brytania');
INSERT INTO `brickwork_country_2` VALUES ('GD','Grenada','Grenade','Granada','Grenada','Grenada','Grenada');
INSERT INTO `brickwork_country_2` VALUES ('GE','Georgia','Géorgie','Georgia','Georgien','Georgië','Gruzja');
INSERT INTO `brickwork_country_2` VALUES ('GF','French Guiana','Guyane française','Guayana Francesa','Französisch-Guayana','Frans-Guyana','Gujana Francuska');
INSERT INTO `brickwork_country_2` VALUES ('GH','Ghana','Ghana','Ghana','Ghana','Ghana','Ghana');
INSERT INTO `brickwork_country_2` VALUES ('GI','Gibraltar','Gibraltar','Gibraltar','Gibraltar','Gibraltar','Gibraltar');
INSERT INTO `brickwork_country_2` VALUES ('GL','Greenland','Groenland','Groenlandia','Grönland','Groenland','Grenlandia');
INSERT INTO `brickwork_country_2` VALUES ('GM','The Gambia','Gambie','Gambia','Gambia','Gambia','Gambia');
INSERT INTO `brickwork_country_2` VALUES ('GN','Guinea','Guinée','Guinea','Guinea','Guinee','Gwinea');
INSERT INTO `brickwork_country_2` VALUES ('GP','Guadeloupe','Guadeloupe','Guadalupe','Guadeloupe','Guadeloupe','Gwadelupa');
INSERT INTO `brickwork_country_2` VALUES ('GQ','Equatorial Guinea','Guinée équatoriale','Guinea Ecuatorial','Äquatorialguinea','Equatoriaal-Guinea','Gwinea Równikowa');
INSERT INTO `brickwork_country_2` VALUES ('GR','Greece','Grèce','Grecia','Griechenland','Griekenland','Grecja');
INSERT INTO `brickwork_country_2` VALUES ('GS','South Georgia and the South Sandwich Islands','Iles Géorgie du Sud et Sandwich du  Sud','Islas Georgia del Sur y Sandwich del Sur','Südgeorgien und Südliche  Sandwichinseln','Zuid-Georgië en Zuidelijke Sandwicheilanden','Wyspy South Georgia i The South Sandwich');
INSERT INTO `brickwork_country_2` VALUES ('GT','Guatemala','Guatemala','Guatemala','Guatemala','Guatemala','Gwatemala');
INSERT INTO `brickwork_country_2` VALUES ('GU','Guam','Guam','Guam','Guam','Guam','Guam');
INSERT INTO `brickwork_country_2` VALUES ('GW','Guinea-Bissau','Guinée-Bissao ; la Guinée-Bissau','Guinea-Bissau','Guinea-Bissau','Guinee-Bissau','Gwinea-Bissau');
INSERT INTO `brickwork_country_2` VALUES ('GY','Guyana','Guyana','Guyana','Guyana','Guyana','Gujana');
INSERT INTO `brickwork_country_2` VALUES ('HK','Hong Kong','Hong Kong','Hong Kong','Hongkong','Hongkong','Hongkong SAR');
INSERT INTO `brickwork_country_2` VALUES ('HM','Heard Island and McDonald Islands','Iles Heard et McDonald','Islas Heard y McDonald','Heard und McDonaldinseln','Heard- en McDonaldeilanden','Wyspy Heard i Mc Donald');
INSERT INTO `brickwork_country_2` VALUES ('HN','Honduras','Honduras','Honduras','Honduras','Honduras','Honduras');
INSERT INTO `brickwork_country_2` VALUES ('HR','Croatia','Croatie','Croacia','Kroatien','Kroatië','Chorwacja (nazwa lokalna: Hrvatska)');
INSERT INTO `brickwork_country_2` VALUES ('HT','Haiti','Haïti','Haití','Haiti','Haïti','Haiti');
INSERT INTO `brickwork_country_2` VALUES ('HU','Hungary','Hongrie','Hungría','Ungarn','Hongarije','W&#281;gry');
INSERT INTO `brickwork_country_2` VALUES ('ID','Indonesia','Indonésie','Indonesia','Indonesien','Indonesië','Indonezja');
INSERT INTO `brickwork_country_2` VALUES ('IE','Ireland','Irlande','Irlanda','Irland','Ierland','Irlandia');
INSERT INTO `brickwork_country_2` VALUES ('IL','Israel','Israël','Israel','Israel','Israël','Izrael');
INSERT INTO `brickwork_country_2` VALUES ('IN','India','Inde','India (la)','Indien','India','Indie');
INSERT INTO `brickwork_country_2` VALUES ('IO','British Indian Ocean Territory','Territoire britannique de l\'Océan  Indien','Territorio Británico del Océano Indico','Britisches Territorium im Indischen  Ozean','Brits Territorium in de Indische Oceaan','Brytyjskie Terytorium Oceanu Indyjskiego');
INSERT INTO `brickwork_country_2` VALUES ('IQ','Iraq','Iraq','Iraq','Irak','Irak','Irak');
INSERT INTO `brickwork_country_2` VALUES ('IR','Iran','Iran','Irán','Iran','Iran','Iran (Islamska Republika)');
INSERT INTO `brickwork_country_2` VALUES ('IS','Iceland','Islande','Islandia','Island','IJsland','Islandia');
INSERT INTO `brickwork_country_2` VALUES ('IT','Italy','Italie','Italia','Italien','Italië','W&#322;ochy');
INSERT INTO `brickwork_country_2` VALUES ('JM','Jamaica','Jamaïque','Jamaica','Jamaika','Jamaica','Jamajka');
INSERT INTO `brickwork_country_2` VALUES ('JO','Jordan','Jordanie','Jordania','Jordanien','Jordanië','Jordania');
INSERT INTO `brickwork_country_2` VALUES ('JP','Japan','Japon','Japón','Japan','Japan','Japonia');
INSERT INTO `brickwork_country_2` VALUES ('KE','Kenya','Kenya','Kenia','Kenia','Kenia; Kenya','Kenia');
INSERT INTO `brickwork_country_2` VALUES ('KG','Kyrgyzstan','Kirghizistan ; le Kirghizstan','Kirguizistán','Kirgisistan','Kirgizië; Kirgizstan','Kirgistan');
INSERT INTO `brickwork_country_2` VALUES ('KH','Cambodia','Cambodge','Camboya','Kambodscha','Cambodja','Kambod&#380;a');
INSERT INTO `brickwork_country_2` VALUES ('KI','Kiribati','Kiribati','Kiribati','Kiribati','Kiribati','Kiribati');
INSERT INTO `brickwork_country_2` VALUES ('KM','Comoros','Comores','Comoras','Komoren','Comoren','Komory');
INSERT INTO `brickwork_country_2` VALUES ('KN','Saint Kitts and Nevis','Saint-Christophe-et-Niévès ;  Saint-Christophe-et-Nevis','San Cristóbal y Nieves','St. Kitts und Nevis','Saint Kitts en Nevis','Saint Kitts i Nevis');
INSERT INTO `brickwork_country_2` VALUES ('KP','North Korea','Corée du Nord','Corea del Norte','Demokratische Volksrepublik Korea','Noord-Korea','Korea&#324;ska Republika Ludowo-Demokratyczna');
INSERT INTO `brickwork_country_2` VALUES ('KR','South Korea','Corée du Sud','Corea del Sur','Republik Korea','Zuid-Korea','Korea, Republika');
INSERT INTO `brickwork_country_2` VALUES ('KW','Kuwait','Koweït','Kuwait','Kuwait','Koeweit','Kuwejt');
INSERT INTO `brickwork_country_2` VALUES ('KY','Cayman Islands','Iles Cayman','Islas Caimán','Kaimaninseln','Caymaneilanden','Kajmany');
INSERT INTO `brickwork_country_2` VALUES ('KZ','Kazakhstan','Kazakhstan','Kazajistán','Kasachstan','Kazachstan','Kazachstan');
INSERT INTO `brickwork_country_2` VALUES ('LA','Laos','Laos','Laos','Laos','Laos','Laota&#324;ska Republika Ludowo-Demokratyczna');
INSERT INTO `brickwork_country_2` VALUES ('LB','Lebanon','Liban','Líbano','Libanon','Libanon','Liban');
INSERT INTO `brickwork_country_2` VALUES ('LC','Saint Lucia','Sainte-Lucie','Santa Lucía','St. Lucia','Saint Lucia','Saint Lucia');
INSERT INTO `brickwork_country_2` VALUES ('LI','Liechtenstein','Liechtenstein','Liechtenstein','Liechtenstein','Liechtenstein','Liechtenstein');
INSERT INTO `brickwork_country_2` VALUES ('LK','Sri Lanka','Sri Lanka','Sri Lanka','Sri Lanka','Sri Lanka','Sri Lanka');
INSERT INTO `brickwork_country_2` VALUES ('LR','Liberia','Liberia','Liberia','Liberia','Liberia','Liberia');
INSERT INTO `brickwork_country_2` VALUES ('LS','Lesotho','Lesotho','Lesoto','Lesotho','Lesotho','Lesotho');
INSERT INTO `brickwork_country_2` VALUES ('LT','Lithuania','Lituanie','Lituania','Litauen','Litouwen','Litwa');
INSERT INTO `brickwork_country_2` VALUES ('LU','Luxembourg','Luxembourg','Luxemburgo','Luxemburg','Luxemburg','Luksemburg');
INSERT INTO `brickwork_country_2` VALUES ('LV','Latvia','Lettonie','Letonia','Lettland','Letland','&#321;otwa');
INSERT INTO `brickwork_country_2` VALUES ('LY','Libya','Libye','Libia','Libyen','Libië','Libijska Arabska D&#380;amahiria');
INSERT INTO `brickwork_country_2` VALUES ('MA','Morocco','Maroc','Marruecos','Marokko','Marokko','Maroko');
INSERT INTO `brickwork_country_2` VALUES ('MC','Monaco','Monaco','Mónaco','Monaco','Monaco','Monako');
INSERT INTO `brickwork_country_2` VALUES ('MD','Moldova','Moldavie ; la Moldova','Moldavia','Republik Moldau','Moldavië','Mo&#322;dawia, Republika');
INSERT INTO `brickwork_country_2` VALUES ('MG','Madagascar','Madagascar','Madagascar','Madagaskar','Madagaskar','Madagaskar');
INSERT INTO `brickwork_country_2` VALUES ('MH','Marshall Islands','Iles Marshall','Islas Marshall','Marshallinseln','Marshalleilanden','Wyspy Marshalla');
INSERT INTO `brickwork_country_2` VALUES ('MK','Former Yugoslav Republic of Macedonia','ex-République yougoslave de Macédoine;l\'ancienne République ','Macedonia','ehemalige jugoslawische Republik  Mazedonien','Voormalige Joegoslavische Republiek Macedonië','Macedonia, By&#322;a Jugos&#322;owia&#324;ska Republika');
INSERT INTO `brickwork_country_2` VALUES ('ML','Mali','Mali','Malí','Mali','Mali','Mali');
INSERT INTO `brickwork_country_2` VALUES ('MM','Myanmar','Birmanie ; le Myanmar','Birmania; Myanmar','Myanmar','Myanmar','Myanmar');
INSERT INTO `brickwork_country_2` VALUES ('MN','Mongolia','Mongolie','Mongolia','Mongolei','Mongolië','Mongolia');
INSERT INTO `brickwork_country_2` VALUES ('MO','Macau','Macao','Macao','Macau','Macau','Makau SAR');
INSERT INTO `brickwork_country_2` VALUES ('MP','Northern Marianas','Mariannes du Nord','Islas Marianas del Norte','Nördliche Marianen','Noordelijke Marianen','Wyspy Mariany Pó&#322;nocne');
INSERT INTO `brickwork_country_2` VALUES ('MQ','Martinique','Martinique','Martinica','Martinique','Martinique','Martynika');
INSERT INTO `brickwork_country_2` VALUES ('MR','Mauritania','Mauritanie','Mauritania','Mauretanien','Mauritanië','Mauretania');
INSERT INTO `brickwork_country_2` VALUES ('MS','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat');
INSERT INTO `brickwork_country_2` VALUES ('MT','Malta','Malte','Malta','Malta','Malta','Malta');
INSERT INTO `brickwork_country_2` VALUES ('MU','Mauritius','Maurice','Mauricio','Mauritius','Mauritius','Mauritius');
INSERT INTO `brickwork_country_2` VALUES ('MV','Maldives','Maldives','Maldivas','Malediven','Maldiven','Malediwy');
INSERT INTO `brickwork_country_2` VALUES ('MW','Malawi','Malawi','Malaui','Malawi','Malawi','Malawi');
INSERT INTO `brickwork_country_2` VALUES ('MX','Mexico','Mexique','México','Mexiko','Mexico','Meksyk');
INSERT INTO `brickwork_country_2` VALUES ('MY','Malaysia','Malaisie','Malasia','Malaysia','Maleisië','Malezja');
INSERT INTO `brickwork_country_2` VALUES ('MZ','Mozambique','Mozambique','Mozambique','Mosambik','Mozambique','Mozambik');
INSERT INTO `brickwork_country_2` VALUES ('NA','Namibia','Namibie','Namibia','Namibia','Namibië','Namibia');
INSERT INTO `brickwork_country_2` VALUES ('NC','New Caledonia','Nouvelle-Calédonie','Nueva Caledonia','Neukaledonien','Nieuw-Caledonië','Nowa Kaledonia');
INSERT INTO `brickwork_country_2` VALUES ('NE','Niger','Niger','Níger','Niger','Niger','Niger');
INSERT INTO `brickwork_country_2` VALUES ('NF','Norfolk Island','Ile Norfolk','Isla Norfolk','Norfolkinsel','Norfolkeiland','Wyspa Norfolk');
INSERT INTO `brickwork_country_2` VALUES ('NG','Nigeria','Nigeria','Nigeria','Nigeria','Nigeria','Nigeria');
INSERT INTO `brickwork_country_2` VALUES ('NI','Nicaragua','Nicaragua','Nicaragua','Nicaragua','Nicaragua','Nikaragua');
INSERT INTO `brickwork_country_2` VALUES ('NL','Netherlands','Pays-Bas','Países Bajos','Niederlande','Nederland','Holandia');
INSERT INTO `brickwork_country_2` VALUES ('NO','Norway','Norvège','Noruega','Norwegen','Noorwegen','Norwegia');
INSERT INTO `brickwork_country_2` VALUES ('NP','Nepal','Népal','Nepal','Nepal','Nepal','Nepal');
INSERT INTO `brickwork_country_2` VALUES ('NR','Nauru','Nauru','Nauru','Nauru','Nauru','Nauru');
INSERT INTO `brickwork_country_2` VALUES ('NU','Niue','Nioué','Niue','Niue','Niue','Niue');
INSERT INTO `brickwork_country_2` VALUES ('NZ','New Zealand','Nouvelle-Zélande','Nueva Zelanda','Neuseeland','Nieuw-Zeeland','Nowa Zelandia');
INSERT INTO `brickwork_country_2` VALUES ('OM','Oman','Oman','Omán','Oman','Oman','Oman');
INSERT INTO `brickwork_country_2` VALUES ('PA','Panama','Panama','Panamá','Panama','Panama','Panama');
INSERT INTO `brickwork_country_2` VALUES ('PE','Peru','Pérou','Perú','Peru','Peru','Peru');
INSERT INTO `brickwork_country_2` VALUES ('PF','French Polynesia','Polynésie française','Polinesia Francesa','Französisch-Polynesien','Frans-Polynesië','Polinezja Francuska');
INSERT INTO `brickwork_country_2` VALUES ('PG','Papua New Guinea','Papouasie-Nouvelle-Guinée','Papúa-Nueva Guinea','Papua-Neuguinea','Papoea-Nieuw-Guinea','Papua Nowa Gwinea');
INSERT INTO `brickwork_country_2` VALUES ('PH','Philippines','Philippines','Filipinas','Philippinen','Filipijnen','Filipiny');
INSERT INTO `brickwork_country_2` VALUES ('PK','Pakistan','Pakistan','Pakistán','Pakistan','Pakistan','Pakistan');
INSERT INTO `brickwork_country_2` VALUES ('PL','Poland','Pologne','Polonia','Polen','Polen','Polska');
INSERT INTO `brickwork_country_2` VALUES ('PM','Saint Pierre and Miquelon','Saint-Pierre-et-Miquelon','San Pedro y Miquelón','St. Pierre und Miquelon','Saint-Pierre en Miquelon','St. Pierre i Miquelon');
INSERT INTO `brickwork_country_2` VALUES ('PN','Pitcairn Islands','Iles Pitcairn','Islas Pitcairn','Pitcairninseln','Pitcairneilanden','Pitcairn');
INSERT INTO `brickwork_country_2` VALUES ('PR','Puerto Rico','Porto Rico','Puerto Rico','Puerto Rico','Puerto Rico; Porto Rico','Portoryko');
INSERT INTO `brickwork_country_2` VALUES ('PT','Portugal','Portugal','Portugal','Portugal','Portugal','Portugalia');
INSERT INTO `brickwork_country_2` VALUES ('PW','Palau','Belau ; Palau','Palaos','Palau','Palau','Palau');
INSERT INTO `brickwork_country_2` VALUES ('PY','Paraguay','Paraguay','Paraguay','Paraguay','Paraguay','Paragwaj');
INSERT INTO `brickwork_country_2` VALUES ('QA','Qatar','Qatar','Qatar','Katar','Qatar','Katar');
INSERT INTO `brickwork_country_2` VALUES ('RE','Réunion','Réunion','Reunión','Réunion','Réunion','Wyspa Reunion');
INSERT INTO `brickwork_country_2` VALUES ('RO','Romania','Roumanie','Rumania; Rumanía','Rumänien','Roemenië','Rumunia');
INSERT INTO `brickwork_country_2` VALUES ('RU','Russia','Russie','Rusia','Russische Föderation','Rusland','Federacja Rosyjska');
INSERT INTO `brickwork_country_2` VALUES ('RW','Rwanda','Rwanda','Ruanda','Ruanda','Rwanda','Rwanda');
INSERT INTO `brickwork_country_2` VALUES ('SA','Saudi Arabia','Arabie saoudite','Arabia Saudí','Saudi-Arabien','Saudi-Arabië; Saoedi-Arabië','Arabia Saudyjska');
INSERT INTO `brickwork_country_2` VALUES ('SB','Solomon Islands','Iles Salomon','Islas Salomón','Salomonen','Salomonseilanden','Wyspy Salomona');
INSERT INTO `brickwork_country_2` VALUES ('SC','Seychelles','Seychelles','Seychelles','Seychellen','Seychellen','Seszele');
INSERT INTO `brickwork_country_2` VALUES ('SD','Sudan','Soudan','Sudán','Sudan','Sudan; Soedan','Sudan');
INSERT INTO `brickwork_country_2` VALUES ('SE','Sweden','Suède','Suecia','Schweden','Zweden','Szwecja');
INSERT INTO `brickwork_country_2` VALUES ('SG','Singapore','Singapour','Singapur','Singapur','Singapore','Singapur');
INSERT INTO `brickwork_country_2` VALUES ('SH','Saint Helena','Sainte-Hélène','Santa Helena','St. Helena','Sint-Helena','&#346;wi&#281;ta Helena');
INSERT INTO `brickwork_country_2` VALUES ('SI','Slovenia','Slovénie','Eslovenia','Slowenien','Slovenië','S&#322;owenia');
INSERT INTO `brickwork_country_2` VALUES ('SJ','Svalbard and Jan Mayen','Iles Svalbard et Jan Mayen','Svalbard y Jan Mayen','Svalbard und Jan Mayen','Svalbard en Jan Mayen','Wyspy Svalbard i Jan Mayen');
INSERT INTO `brickwork_country_2` VALUES ('SK','Slovakia','Slovaquie','Eslovaquia','Slowakei','Slowakije; Slovakije','S&#322;owacja (Republika S&#322;owacka)');
INSERT INTO `brickwork_country_2` VALUES ('SL','Sierra Leone','Sierra Leone','Sierra Leona','Sierra Leone','Sierra Leone','Sierra Leone');
INSERT INTO `brickwork_country_2` VALUES ('SM','San Marino','Saint-Marin','San Marino','San Marino','San Marino','San Marino');
INSERT INTO `brickwork_country_2` VALUES ('SN','Senegal','Sénégal','Senegal','Senegal','Senegal','Senegal');
INSERT INTO `brickwork_country_2` VALUES ('SO','Somalia','Somalie','Somalia','Somalia','Somalië','Somalia');
INSERT INTO `brickwork_country_2` VALUES ('SR','Suriname','Suriname','Surinam','Suriname','Suriname','Surinam');
INSERT INTO `brickwork_country_2` VALUES ('ST','São Tomé and Príncipe','Sao Tomé-et-Principe ; São Tomé e Príncipe','Santo Tomé y Príncipe','São Tomé und Príncipe','Sao Tomé en Principe','Wyspy &#346;wi&#281;tego Tomasza i Ksi&#261;&#380;&#281;ca');
INSERT INTO `brickwork_country_2` VALUES ('SV','El Salvador','Salvador ; l\'El Salvador','El Salvador','El Salvador','El Salvador','Salwador');
INSERT INTO `brickwork_country_2` VALUES ('SY','Syria','Syrie','Siria','Syrien','Syrië','Syryjska Republika Arabska');
INSERT INTO `brickwork_country_2` VALUES ('SZ','Swaziland','Swaziland','Suazilandia','Swasiland','Swaziland','Suazi');
INSERT INTO `brickwork_country_2` VALUES ('TC','Turks and Caicos Islands','Iles Turks-et-Caicos','Islas Turcas y Caicos','Turks- und Caicosinseln','Turks- en Caicoseilanden','Wyspy Turks i Caicos');
INSERT INTO `brickwork_country_2` VALUES ('TD','Chad','Tchad','Chad','Tschad','Tsjaad','Czad');
INSERT INTO `brickwork_country_2` VALUES ('TF','French Southern Territories','Terres australes françaises','Territorios Australes Franceses','Französische Gebiete im südlichen Indischen  Ozean','Franse Gebieden in de zuidelijke Indische Oceaan','Francuskie Terytoria Po&#322;udniowe');
INSERT INTO `brickwork_country_2` VALUES ('TG','Togo','Togo','Togo','Togo','Togo','Togo');
INSERT INTO `brickwork_country_2` VALUES ('TH','Thailand','Thaïlande','Tailandia','Thailand','Thailand','Tajlandia');
INSERT INTO `brickwork_country_2` VALUES ('TJ','Tajikistan','Tadjikistan','Tayikistán','Tadschikistan','Tadzjikistan','Tad&#380;ykistan');
INSERT INTO `brickwork_country_2` VALUES ('TK','Tokelau','Tokélaou','Tokelau','Tokelau','Tokelau-eilanden','Tokelau');
INSERT INTO `brickwork_country_2` VALUES ('TL','East Timor','Timor Oriental','Timor Oriental','Osttimor','Oost-Timor','Timor Wschodni');
INSERT INTO `brickwork_country_2` VALUES ('TM','Turkmenistan','Turkménistan','Turkmenistán','Turkmenistan','Turkmenistan','Turkmenistan');
INSERT INTO `brickwork_country_2` VALUES ('TN','Tunisia','Tunisie','Túnez','Tunesien','Tunesië','Tunezja');
INSERT INTO `brickwork_country_2` VALUES ('TO','Tonga','Tonga','Tonga','Tonga','Tonga','Tonga');
INSERT INTO `brickwork_country_2` VALUES ('TR','Turkey','Turquie','Turquía','Türkei','Turkije','Turcja');
INSERT INTO `brickwork_country_2` VALUES ('TT','Trinidad and Tobago','Trinité-et-Tobago ; Trinidad-et-Tobago','Trinidad y Tobago','Trinidad und Tobago','Trinidad en Tobago','Trynidad i Tobago');
INSERT INTO `brickwork_country_2` VALUES ('TV','Tuvalu','Tuvalu','Tuvalu','Tuvalu','Tuvalu','Tuvalu');
INSERT INTO `brickwork_country_2` VALUES ('TW','Taiwan','Taïwan','Taiwán','Taiwan','Taiwan','Tajwan');
INSERT INTO `brickwork_country_2` VALUES ('TZ','Tanzania','Tanzanie','Tanzania','Tansania','Tanzania','Tanzania, Zjednoczona Republika');
INSERT INTO `brickwork_country_2` VALUES ('UA','Ukraine','Ukraine','Ucrania','Ukraine','Oekraïne','Ukraina');
INSERT INTO `brickwork_country_2` VALUES ('UG','Uganda','Ouganda','Uganda','Uganda','Uganda; Oeganda','Uganda');
INSERT INTO `brickwork_country_2` VALUES ('UM','United States Minor Outlying Islands','Iles mineures éloignées des  États-Unis','Islas menores alejadas de los Estados Unidos','Kleinere amerikanische Überseeinseln','Amerikaanse ondergeschikte afgelegen eilanden','Stany Zjednoczone - wyspy zewn&#281;trzne');
INSERT INTO `brickwork_country_2` VALUES ('US','United States','États-Unis','Estados Unidos','Vereinigte Staaten','Verenigde Staten','Stany Zjednoczone');
INSERT INTO `brickwork_country_2` VALUES ('UY','Uruguay','Uruguay','Uruguay','Uruguay','Uruguay','Urugwaj');
INSERT INTO `brickwork_country_2` VALUES ('UZ','Uzbekistan','Ouzbékistan','Uzbekistán','Usbekistan','Oezbekistan','Uzbekistan');
INSERT INTO `brickwork_country_2` VALUES ('VA','Vatican City','Saint-Siège','El Vaticano','Vatikanstadt','Vaticaanstad','Stolica Apostolska (Pa&#324;stwo Watyka&#324;skie)');
INSERT INTO `brickwork_country_2` VALUES ('VC','Saint Vincent and the Grenadines','Saint-Vincent-et-les-Grenadines','San Vicente y las Granadinas','St. Vincent und die Grenadinen','Saint Vincent en de Grenadines','Saint Vincent i Grenadyny');
INSERT INTO `brickwork_country_2` VALUES ('VE','Venezuela','Venezuela','Venezuela','Venezuela','Venezuela','Wenezuela');
INSERT INTO `brickwork_country_2` VALUES ('VG','British Virgin Islands','Iles Vierges britanniques','Islas Vírgenes Británicas','Britische Jungferninseln','Britse Maagdeneilanden','Wyspy Dziewicze (Brytyjskie)');
INSERT INTO `brickwork_country_2` VALUES ('VI','US Virgin Islands','Iles Vierges américaines','Islas Vírgenes Americanas','Amerikanische Jungferninseln','Amerikaanse Maagdeneilanden','Wyspy Dziewicze (Stanów Zjednoczonych)');
INSERT INTO `brickwork_country_2` VALUES ('VN','Vietnam','Viêt Nam','Vietnam','Vietnam','Vietnam','Wietnam');
INSERT INTO `brickwork_country_2` VALUES ('VU','Vanuatu','Vanuatu','Vanuatu','Vanuatu','Vanuatu','Vanuatu');
INSERT INTO `brickwork_country_2` VALUES ('WF','Wallis and Futuna','Wallis-et-Futuna','Wallis y Futuna','Wallis und Futuna','Wallis en Futuna','Wyspy Wallis i Futuna');
INSERT INTO `brickwork_country_2` VALUES ('WS','Samoa','Samoa','Samoa','Samoa','Samoa','Samoa');
INSERT INTO `brickwork_country_2` VALUES ('YE','Yemen','Yémen','Yemen','Jemen','Jemen','Jemen');
INSERT INTO `brickwork_country_2` VALUES ('YT','Mayotte','Mayotte','Mayotte','Mayotte','Mayotte','Wyspa Majotta');
INSERT INTO `brickwork_country_2` VALUES ('YU','Yugoslavia','Yougoslavie','Yugoslavia','Jugoslawien','Joegoslavië','Jugos&#322;awia');
INSERT INTO `brickwork_country_2` VALUES ('ZA','South Africa','Afrique du Sud','Sudáfrica','Südafrika','Zuid-Afrika','Republika Po&#322;udniowej Afryki');
INSERT INTO `brickwork_country_2` VALUES ('ZM','Zambia','Zambie','Zambia','Sambia','Zambia','Zambia');
INSERT INTO `brickwork_country_2` VALUES ('ZW','Zimbabwe','Zimbabwe','Zimbabue','Simbabwe','Zimbabwe','Zimbabwe');
/*!40000 ALTER TABLE `brickwork_country_2` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_custom_site_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_custom_site_structure` (
  `custom_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`custom_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_custom_site_structure` WRITE;
/*!40000 ALTER TABLE `brickwork_custom_site_structure` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_custom_site_structure` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_db_form_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_db_form_field` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_db_form_field` WRITE;
/*!40000 ALTER TABLE `brickwork_db_form_field` DISABLE KEYS */;
INSERT INTO `brickwork_db_form_field` VALUES (1,'TextField');
INSERT INTO `brickwork_db_form_field` VALUES (2,'SelectField');
INSERT INTO `brickwork_db_form_field` VALUES (3,'CheckboxField');
INSERT INTO `brickwork_db_form_field` VALUES (4,'GenderField');
INSERT INTO `brickwork_db_form_field` VALUES (5,'NumberField');
INSERT INTO `brickwork_db_form_field` VALUES (6,'UnsignedNumberField');
INSERT INTO `brickwork_db_form_field` VALUES (7,'DateField');
INSERT INTO `brickwork_db_form_field` VALUES (8,'EmailField');
INSERT INTO `brickwork_db_form_field` VALUES (9,'PhoneField');
INSERT INTO `brickwork_db_form_field` VALUES (10,'FileField');
INSERT INTO `brickwork_db_form_field` VALUES (11,'PhotoField');
INSERT INTO `brickwork_db_form_field` VALUES (12,'CountryField');
INSERT INTO `brickwork_db_form_field` VALUES (13,'HiddenField');
INSERT INTO `brickwork_db_form_field` VALUES (14,'CursorField');
INSERT INTO `brickwork_db_form_field` VALUES (15,'IBANField');
INSERT INTO `brickwork_db_form_field` VALUES (16,'YearField');
INSERT INTO `brickwork_db_form_field` VALUES (17,'URLField');
INSERT INTO `brickwork_db_form_field` VALUES (18,'BooleanField');
INSERT INTO `brickwork_db_form_field` VALUES (19,'HouseNumberField');
INSERT INTO `brickwork_db_form_field` VALUES (20,'PostalcodeField');
INSERT INTO `brickwork_db_form_field` VALUES (21,'ButtonField');
INSERT INTO `brickwork_db_form_field` VALUES (22,'PasswordField');
INSERT INTO `brickwork_db_form_field` VALUES (23,'RadioField');
INSERT INTO `brickwork_db_form_field` VALUES (24,'TextareaField');
INSERT INTO `brickwork_db_form_field` VALUES (25,'BankAccountField');
/*!40000 ALTER TABLE `brickwork_db_form_field` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_db_form_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_db_form_mapping` (
  `id` int(11) NOT NULL auto_increment,
  `brickwork_form_code` varchar(31) NOT NULL default '',
  `fieldtype` varchar(31) NOT NULL default '',
  `dst_scheme` varchar(31) NOT NULL default '',
  `dst_table` varchar(80) NOT NULL default '',
  `dst_field` varchar(80) NOT NULL default '',
  `src_scheme` varchar(31) default NULL,
  `src_table` varchar(80) default NULL,
  `src_field` varchar(80) default NULL,
  `src_label` varchar(80) default NULL,
  `src_constraint` varchar(255) default NULL,
  `label` varchar(255) NOT NULL default '',
  `prio` float NOT NULL default '0',
  `multiplicity` char(1) NOT NULL default '1',
  `disabled` tinyint(1) NOT NULL default '0',
  `active` tinyint(1) NOT NULL default '1',
  `required` tinyint(1) NOT NULL default '0',
  `info` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UN_db_form` (`dst_scheme`,`dst_table`,`dst_field`,`brickwork_form_code`),
  KEY `name` (`prio`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_db_form_mapping` WRITE;
/*!40000 ALTER TABLE `brickwork_db_form_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_db_form_mapping` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_dmdelivery_soap_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_dmdelivery_soap_campaign` (
  `id` int(10) unsigned NOT NULL,
  `site_identifier` varchar(20) NOT NULL default '',
  `campaign_title` varchar(255) default NULL,
  `campaign_id` int(10) unsigned NOT NULL default '0',
  `soap_url` varchar(255) NOT NULL default '',
  `username` varchar(255) NOT NULL default '',
  `password` varchar(255) NOT NULL default '',
  `optin_mailing_id` int(11) default NULL,
  `dmd_subgroup` int(11) default NULL,
  `dmd_unsubgroup` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_identifier` (`site_identifier`(5))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_dmdelivery_soap_campaign` WRITE;
/*!40000 ALTER TABLE `brickwork_dmdelivery_soap_campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_dmdelivery_soap_campaign` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_dmdelivery_soap_campaign_field_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_dmdelivery_soap_campaign_field_mapping` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `soap_id` int(10) unsigned NOT NULL default '0',
  `dmd_fieldname` varchar(255) default NULL,
  `user_fieldname` varchar(255) default NULL,
  `fieldtype` varchar(31) default NULL,
  `combined_unique` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `soap_id` (`soap_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_dmdelivery_soap_campaign_field_mapping` WRITE;
/*!40000 ALTER TABLE `brickwork_dmdelivery_soap_campaign_field_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_dmdelivery_soap_campaign_field_mapping` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_dmfacts_pro_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_dmfacts_pro_account` (
  `id` int(10) unsigned NOT NULL,
  `site_identifier` varchar(20) NOT NULL default '',
  `dmfacts_account_code` varchar(255) NOT NULL default '',
  `script` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_dmfacts_pro_account` WRITE;
/*!40000 ALTER TABLE `brickwork_dmfacts_pro_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_dmfacts_pro_account` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_form` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(31) NOT NULL default '',
  `site_identifier` varchar(20) NOT NULL default '',
  `title` varchar(63) NOT NULL default '',
  `description` mediumtext,
  `header` text,
  `footer` text,
  `type_code` varchar(20) NOT NULL default '',
  `multiplicity` varchar(7) NOT NULL default '1',
  `active` tinyint(1) NOT NULL default '1',
  `feedbackable` tinyint(1) NOT NULL default '0',
  `notify_emailaddresses` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_form` WRITE;
/*!40000 ALTER TABLE `brickwork_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_form` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_form_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_form_types` (
  `code` varchar(20) NOT NULL default '',
  `title` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_form_types` WRITE;
/*!40000 ALTER TABLE `brickwork_form_types` DISABLE KEYS */;
INSERT INTO `brickwork_form_types` VALUES ('avoid','Avoid');
INSERT INTO `brickwork_form_types` VALUES ('db','DB mapping');
/*!40000 ALTER TABLE `brickwork_form_types` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_googlemaps_host_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_googlemaps_host_api` (
  `http_host` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  PRIMARY KEY  (`http_host`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_googlemaps_host_api` WRITE;
/*!40000 ALTER TABLE `brickwork_googlemaps_host_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_googlemaps_host_api` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_mailtemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_mailtemplate` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` char(40) NOT NULL default '',
  `site_identifier` char(20) NOT NULL default '',
  `from_email` varchar(255) default '',
  `from_name` varchar(255) default '',
  `subject` text,
  `body` text,
  `bcc` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`code`,`site_identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_mailtemplate` WRITE;
/*!40000 ALTER TABLE `brickwork_mailtemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_mailtemplate` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_module` (
  `code` varchar(20) NOT NULL default '',
  `class` varchar(50) default NULL,
  `ptid_content` int(10) unsigned default NULL,
  `ptid_config` int(10) unsigned default NULL,
  `name` varchar(255) default NULL,
  `description` text,
  `image_src` varchar(255) default NULL,
  `max_per_page` int(10) unsigned default NULL,
  `template` varchar(63) default NULL,
  `cachetime` int(10) unsigned default '0',
  `is_custom` tinyint(3) unsigned default '0',
  PRIMARY KEY  (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_module` WRITE;
/*!40000 ALTER TABLE `brickwork_module` DISABLE KEYS */;
INSERT INTO `brickwork_module` VALUES ('menu','MenuModule',NULL,NULL,'11213',NULL,NULL,3,'menu.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('htmlcontent','HTMLModule',218,333,'11210','HTML weergave',NULL,0,'htmlcontent.tpl',5,0);
INSERT INTO `brickwork_module` VALUES ('htdig_search','HTdigModule',NULL,274,'11208',NULL,NULL,2,'htdig_search.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('avoidform','EmailFormulierModule',NULL,273,'11200',NULL,NULL,1,'avoidform.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('google_adsense','GoogleAdSenseModule',NULL,339,'11206',NULL,NULL,NULL,'google_adsense.tpl',300,0);
INSERT INTO `brickwork_module` VALUES ('sitemap','SitemapModule',NULL,NULL,'11217',NULL,NULL,0,'sitemap.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('google_maps','GoogleMapsModule',329,328,'11207',NULL,NULL,NULL,'google_maps.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('photogallery','PhotogalleryModule',NULL,330,'11214',NULL,NULL,NULL,'photogallery.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('htmlblock','HTMLBlockModule',NULL,334,'11209',NULL,NULL,NULL,'htmlblock.tpl',300,0);
INSERT INTO `brickwork_module` VALUES ('deeplink','DeepLinkModule',NULL,335,'11204',NULL,NULL,NULL,'deeplink.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('iframe','IframeModule',NULL,340,'11211',NULL,NULL,NULL,'iframe.tpl',300,0);
INSERT INTO `brickwork_module` VALUES ('snippet','SnippetModule',NULL,342,'11218',NULL,NULL,NULL,'snippet.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('agenda','AgendaModule',NULL,354,'11201',NULL,NULL,NULL,'agenda.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('banner','BannerModule',NULL,346,'11202',NULL,NULL,NULL,'banner.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('poll','PollModule',NULL,373,'11215',NULL,NULL,NULL,'poll.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('faq','FaqModule',NULL,364,'11205',NULL,NULL,NULL,'faq.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('link','LinkModule',NULL,365,'11212',NULL,NULL,NULL,'link.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('brickworknews','BrickworkNewsModule',NULL,371,'11203',NULL,NULL,NULL,'news.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('reactions','Reactions_Module',NULL,NULL,'11216',NULL,NULL,NULL,'reactions.tpl',0,0);
INSERT INTO `brickwork_module` VALUES ('newsfeed','BrickworkNewsFeedModule',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
/*!40000 ALTER TABLE `brickwork_module` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_organisation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_organisation` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `street` varchar(255) default NULL,
  `house_nr` int(10) unsigned default NULL,
  `house_nr_add` varchar(10) default NULL,
  `city` varchar(255) default NULL,
  `country` varchar(10) default NULL,
  `phone_area_code` varchar(10) default NULL,
  `phone_number` int(10) unsigned default NULL,
  `email_info` varchar(255) default NULL,
  `email_privacy` varchar(255) default NULL,
  `email_support` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_organisation` WRITE;
/*!40000 ALTER TABLE `brickwork_organisation` DISABLE KEYS */;
INSERT INTO `brickwork_organisation` VALUES (1,'Web Power','Anthonie Fokkerstraat',5,NULL,'Barneveld','NL',NULL,NULL,'info@webpower.nl','info@webpower.nl','support@webpower.nl');
/*!40000 ALTER TABLE `brickwork_organisation` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_page` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `page_type_code` varchar(20) NOT NULL default '',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned default NULL,
  `title` varchar(255) default NULL,
  `description` text,
  `keywords` varchar(255) default '',
  `online_date` datetime default NULL,
  `offline_date` datetime default NULL,
  `staging_status` enum('test','live') default 'test',
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_structure` (`site_structure_id`,`lang_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_page` WRITE;
/*!40000 ALTER TABLE `brickwork_page` DISABLE KEYS */;
INSERT INTO `brickwork_page` VALUES (1,'welcome',1,NULL,'Brickwork & Phoundry zijn geinstalleerd',NULL,'','2009-07-20 10:14:00','2019-03-27 10:14:00','live','2010-03-05 09:14:34',NULL);
INSERT INTO `brickwork_page` VALUES (2,'welcome',2,NULL,'Pagina niet gevonden',NULL,'','2011-01-14 11:06:00','2021-01-14 11:06:00','live','2011-01-14 10:06:14',NULL);
/*!40000 ALTER TABLE `brickwork_page` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_page_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_page_content` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `content_container_code` char(20) NOT NULL default '',
  `page_id` int(10) unsigned NOT NULL default '0',
  `module_code` char(20) NOT NULL default '',
  `prio` float(10,3) default NULL,
  `online_date` datetime default NULL,
  `offline_date` datetime default NULL,
  `staging_status` enum('test','live') default 'live',
  `respect_auth` tinyint(1) unsigned NOT NULL default '1',
  `state` enum('ok','new','delete') NOT NULL default 'ok' COMMENT 'State',
  `state_date` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `page` (`page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_page_content` WRITE;
/*!40000 ALTER TABLE `brickwork_page_content` DISABLE KEYS */;
INSERT INTO `brickwork_page_content` VALUES (1,'cc_content',1,'htmlblock',60.000,'2009-07-20 10:19:43','2019-07-20 10:19:43','live',1,'ok',NULL);
INSERT INTO `brickwork_page_content` VALUES (3,'cc_content',2,'htmlblock',60.000,'2011-01-14 11:06:18','2021-01-14 11:06:18','live',1,'ok',NULL);
/*!40000 ALTER TABLE `brickwork_page_content` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_page_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_page_type` (
  `code` varchar(20) NOT NULL default '',
  `template_identifier` varchar(15) NOT NULL default '',
  `name` varchar(255) default NULL,
  `contentspecification` text,
  PRIMARY KEY  (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_page_type` WRITE;
/*!40000 ALTER TABLE `brickwork_page_type` DISABLE KEYS */;
INSERT INTO `brickwork_page_type` VALUES ('welcome','default','Welcome to brickwork','<div style=\"border: 1px solid black; background-color: white; width:450px; float:left; margin-left:10px;\" align=\"center\">\r\n<table border=\"0\" style=\"width:440px;\" cellpadding=\"0\" cellspacing=\"0\">\r\n<tr>\r\n	<td>\r\n		<ul id=\"cc_content\" class=\"sortable boxy\" style=\"height: 300px; width: 300px;border:1px solid #ACACAC;\"></ul>\r\n	</td>\r\n</tr>\r\n</table>\r\n</div>');
/*!40000 ALTER TABLE `brickwork_page_type` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_photogallery_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_photogallery_album` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `active` tinyint(1) default '1',
  `title` varchar(255) NOT NULL,
  `prio` float NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_photogallery_album` WRITE;
/*!40000 ALTER TABLE `brickwork_photogallery_album` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_photogallery_album` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_photogallery_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_photogallery_lookup` (
  `page_content_id` int(10) unsigned NOT NULL,
  `album_id` int(10) unsigned NOT NULL,
  KEY `album` (`page_content_id`,`album_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_photogallery_lookup` WRITE;
/*!40000 ALTER TABLE `brickwork_photogallery_lookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_photogallery_lookup` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_photogallery_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_photogallery_photo` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `album_id` int(10) unsigned NOT NULL,
  `title` varchar(255) default NULL,
  `description` varchar(255) default NULL,
  `location` varchar(255) NOT NULL,
  `pubdate` datetime NOT NULL default '0000-00-00 00:00:00',
  `prio` decimal(8,4) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_photogallery_photo` (`album_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_photogallery_photo` WRITE;
/*!40000 ALTER TABLE `brickwork_photogallery_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_photogallery_photo` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_quick_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_quick_menu` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `position` varchar(255) NOT NULL default 'footer',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `prio` float(10,3) NOT NULL default '0.000',
  PRIMARY KEY  (`id`),
  KEY `site_identifier` (`site_identifier`),
  KEY `site_structure` (`site_structure_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_quick_menu` WRITE;
/*!40000 ALTER TABLE `brickwork_quick_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_quick_menu` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_reaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_reaction` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `group_id` int(10) unsigned NOT NULL,
  `site_user_id` int(10) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL default '0',
  `author` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `body` text,
  `ip` int(15) NOT NULL,
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created_on` datetime NOT NULL,
  `post_url` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `byGroup` (`group_id`,`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_reaction` WRITE;
/*!40000 ALTER TABLE `brickwork_reaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_reaction` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_reaction_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_reaction_group` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `relation_id` int(10) unsigned NOT NULL,
  `relation_name` varchar(50) NOT NULL default '',
  `closed` tinyint(1) default NULL,
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created_datetime` datetime NOT NULL,
  `reaction_count` int(10) unsigned NOT NULL,
  `title` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `related` (`relation_id`,`relation_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_reaction_group` WRITE;
/*!40000 ALTER TABLE `brickwork_reaction_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_reaction_group` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_reaction_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_reaction_media` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `reaction_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `mediatype` enum('audio','video','image','youtube','link') NOT NULL default 'image',
  `created_datetime` datetime NOT NULL,
  `original` varchar(255) default NULL,
  `mime` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `reaction_id` (`reaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_reaction_media` WRITE;
/*!40000 ALTER TABLE `brickwork_reaction_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_reaction_media` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_reaction_relationconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_reaction_relationconfig` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `relation_name` varchar(255) NOT NULL default '',
  `enabled` tinyint(1) default NULL,
  `order_by` tinyint(1) default NULL,
  `media` int(11) default NULL,
  `mediatypes` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_reaction_relationconfig` WRITE;
/*!40000 ALTER TABLE `brickwork_reaction_relationconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_reaction_relationconfig` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_reaction_site_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_reaction_site_config` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `relation_name` varchar(50) default '',
  `enabled` tinyint(1) unsigned NOT NULL default '0',
  `reaction_order_by_colom` varchar(50) NOT NULL default 'created_on',
  `reaction_order_by` enum('ASC','DESC') NOT NULL default 'ASC',
  `images` int(2) unsigned NOT NULL default '4',
  `youtube` int(2) unsigned NOT NULL default '1',
  `allow_html` tinyint(1) unsigned NOT NULL default '0',
  `allow_ubb` tinyint(1) unsigned NOT NULL default '0',
  `allow_smilies` tinyint(1) unsigned NOT NULL default '0',
  `auto_link` tinyint(1) unsigned NOT NULL default '1',
  `bad_words` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Uniek` (`site_identifier`,`relation_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_reaction_site_config` WRITE;
/*!40000 ALTER TABLE `brickwork_reaction_site_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_reaction_site_config` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_readable_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_readable_links` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `subdir` varchar(255) NOT NULL default '',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `site_identifier` (`site_identifier`,`subdir`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_readable_links` WRITE;
/*!40000 ALTER TABLE `brickwork_readable_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_readable_links` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_service_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_service_module` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(20) NOT NULL,
  `class` varchar(40) NOT NULL,
  `name` varchar(128) default NULL,
  `secure` tinyint(4) NOT NULL default '0',
  `debug` tinyint(4) NOT NULL default '0',
  `auth_method` varchar(20) default NULL,
  `ptid_config` int(10) unsigned default NULL,
  `ptid_content` int(10) unsigned default NULL,
  `cachetime` int(11) NOT NULL default '0',
  `restrict_ip` text,
  `created_on` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_service_module` WRITE;
/*!40000 ALTER TABLE `brickwork_service_module` DISABLE KEYS */;
INSERT INTO `brickwork_service_module` VALUES (1,'web','Service_Module_Webmodule','Webmodule Redirector',0,0,NULL,NULL,NULL,0,NULL,'2008-10-15 12:52:00');
INSERT INTO `brickwork_service_module` VALUES (2,'abuse_report','Abuse_ServiceModule','Misbruik Melden',0,0,NULL,NULL,NULL,0,NULL,'2009-10-30 16:56:00');
/*!40000 ALTER TABLE `brickwork_service_module` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_service_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_service_user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `disabled` tinyint(4) NOT NULL default '0',
  `description` varchar(255) default NULL,
  `restrict_ip` text,
  `created_on` datetime NOT NULL,
  `created_by` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_service_user` WRITE;
/*!40000 ALTER TABLE `brickwork_service_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_service_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_service_user_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_service_user_module` (
  `service_user_id` int(10) unsigned NOT NULL,
  `service_module_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`service_user_id`,`service_module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_service_user_module` WRITE;
/*!40000 ALTER TABLE `brickwork_service_user_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_service_user_module` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_session` (
  `identifier` char(32) character set ascii NOT NULL,
  `site_identifier` varchar(20) NOT NULL,
  `lastaccess` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `ip` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY  (`identifier`,`site_identifier`(10))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_session` WRITE;
/*!40000 ALTER TABLE `brickwork_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_session` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site` (
  `identifier` varchar(20) NOT NULL default '',
  `name` varchar(63) NOT NULL default '',
  `organisation_id` int(10) unsigned NOT NULL default '0',
  `protocol` set('http','https') default 'http',
  `domain` varchar(255) default NULL,
  `status` enum('test','live') NOT NULL default 'test',
  `template` varchar(15) NOT NULL default 'default',
  `auth_user` varchar(63) NOT NULL default 'SiteUser',
  `default_lang_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site` WRITE;
/*!40000 ALTER TABLE `brickwork_site` DISABLE KEYS */;
INSERT INTO `brickwork_site` VALUES ('default','Default brickwork site',1,'http,https','skeleton.dev.local','live','default','SiteUser',NULL);
/*!40000 ALTER TABLE `brickwork_site` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_config` (
  `id` int(11) NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL,
  `attribute` varchar(80) NOT NULL,
  `value` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQ_SITE_IDENTIFIER_ATTRIBUTE` (`site_identifier`,`attribute`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_config` WRITE;
/*!40000 ALTER TABLE `brickwork_site_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_config` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_config_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_config_page` (
  `name` varchar(80) NOT NULL default '',
  `title` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_config_page` WRITE;
/*!40000 ALTER TABLE `brickwork_site_config_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_config_page` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_host_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_host_alias` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `protocol` set('http','https') NOT NULL default 'http,https',
  `host` varchar(50) NOT NULL default '',
  `site_identifier` varchar(20) NOT NULL default '',
  `redirect` tinyint(1) unsigned NOT NULL default '0',
  `prio` decimal(8,3) NOT NULL default '100.000',
  `default_lang_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `host` (`host`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_host_alias` WRITE;
/*!40000 ALTER TABLE `brickwork_site_host_alias` DISABLE KEYS */;
INSERT INTO `brickwork_site_host_alias` VALUES (1,'http','*','default',0,'0.000',NULL);
/*!40000 ALTER TABLE `brickwork_site_host_alias` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_language` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL,
  `code` char(4) NOT NULL,
  `locale` char(25) NOT NULL default 'nl_NL',
  `namespace` varchar(20) NOT NULL default 'site',
  `user_selectable` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`site_identifier`,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_language` WRITE;
/*!40000 ALTER TABLE `brickwork_site_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_language` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_phoundry_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_phoundry_user` (
  `site_identifier` varchar(20) default NULL,
  `phoundry_user_id` int(10) unsigned NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_phoundry_user` WRITE;
/*!40000 ALTER TABLE `brickwork_site_phoundry_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_phoundry_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_structure` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` char(20) NOT NULL default '',
  `parent_id` int(10) unsigned default NULL,
  `name` char(40) default NULL,
  `prio` int(5) default NULL,
  `show_menu` tinyint(3) unsigned default '1',
  `show_sitemap` tinyint(3) unsigned default '1',
  `stype` enum('page','url') default NULL,
  `secure` tinyint(1) default '0',
  `restricted` tinyint(1) default '0',
  `linked_to` int(10) unsigned default NULL,
  `ssl` tinyint(1) unsigned default '0',
  PRIMARY KEY  (`id`),
  KEY `identifier` (`site_identifier`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_structure` WRITE;
/*!40000 ALTER TABLE `brickwork_site_structure` DISABLE KEYS */;
INSERT INTO `brickwork_site_structure` VALUES (1,'default',NULL,'Home',1,1,0,'page',0,NULL,NULL,0);
INSERT INTO `brickwork_site_structure` VALUES (2,'default',NULL,'404-Not-found',2,0,0,'page',0,NULL,NULL,0);
/*!40000 ALTER TABLE `brickwork_site_structure` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_structure_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_structure_i18n` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_structure_id` int(10) unsigned NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(80) default NULL,
  PRIMARY KEY  (`id`),
  KEY `IDX_SITE_STRUCTURE_LANG` (`site_structure_id`,`lang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_structure_i18n` WRITE;
/*!40000 ALTER TABLE `brickwork_site_structure_i18n` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_structure_i18n` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_structure_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_structure_path` (
  `site_identifier` varchar(20) NOT NULL default '',
  `hash` varchar(32) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`site_identifier`,`hash`,`lang_id`),
  UNIQUE KEY `unique_path` (`site_structure_id`,`lang_id`),
  KEY `site_structure_id` (`site_structure_id`),
  KEY `ind_site_ident_structure_id` (`site_identifier`(4),`site_structure_id`,`lang_id`,`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_structure_path` WRITE;
/*!40000 ALTER TABLE `brickwork_site_structure_path` DISABLE KEYS */;
INSERT INTO `brickwork_site_structure_path` VALUES ('default','8cf04a9734132302f96da8e113e80ce5','Home',1,0);
INSERT INTO `brickwork_site_structure_path` VALUES ('default','ffd57249e83b54b7575caf74ddcdb285','404-Not-found',2,0);
/*!40000 ALTER TABLE `brickwork_site_structure_path` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_structure_path_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_structure_path_archive` (
  `site_identifier` varchar(20) NOT NULL default '',
  `hash` varchar(32) NOT NULL default '',
  `name` varchar(255) NOT NULL,
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned NOT NULL default '0',
  `archive_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`site_identifier`,`hash`,`lang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_structure_path_archive` WRITE;
/*!40000 ALTER TABLE `brickwork_site_structure_path_archive` DISABLE KEYS */;
INSERT INTO `brickwork_site_structure_path_archive` VALUES ('default','ffd57249e83b54b7575caf74ddcdb285','404-Not-found',2,0,'2011-01-14 11:05:59');
/*!40000 ALTER TABLE `brickwork_site_structure_path_archive` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_structure_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_structure_role` (
  `site_structure_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`site_structure_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_structure_role` WRITE;
/*!40000 ALTER TABLE `brickwork_site_structure_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_structure_role` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(128) default NULL,
  `password` varchar(32) default NULL,
  `displayname` varchar(255) default NULL,
  `email` varchar(128) default NULL,
  `createdon` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `IDX_USERNAME` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_user` WRITE;
/*!40000 ALTER TABLE `brickwork_site_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_user_auth_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_user_auth_role` (
  `site_user_id` int(11) NOT NULL,
  `auth_role_id` int(11) NOT NULL,
  PRIMARY KEY  (`site_user_id`,`auth_role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_user_auth_role` WRITE;
/*!40000 ALTER TABLE `brickwork_site_user_auth_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_user_auth_role` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_user_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_user_data` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `site_user_id` int(10) unsigned NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `value` varchar(255) default NULL,
  `modified_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQ_USER_DATA` (`site_user_id`,`attribute`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_user_data` WRITE;
/*!40000 ALTER TABLE `brickwork_site_user_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_user_data` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_user_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_user_login` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `site_user_id` int(10) unsigned NOT NULL,
  `modified_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ip` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_user_id` (`site_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_user_login` WRITE;
/*!40000 ALTER TABLE `brickwork_site_user_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_user_login` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_site_user_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_site_user_site` (
  `site_user_id` int(11) unsigned NOT NULL,
  `site_identifier` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`site_user_id`,`site_identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_site_user_site` WRITE;
/*!40000 ALTER TABLE `brickwork_site_user_site` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_site_user_site` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_template` (
  `identifier` varchar(15) NOT NULL default '',
  `name` varchar(63) default NULL,
  PRIMARY KEY  (`identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_template` WRITE;
/*!40000 ALTER TABLE `brickwork_template` DISABLE KEYS */;
INSERT INTO `brickwork_template` VALUES ('default','Default brickwork site');
/*!40000 ALTER TABLE `brickwork_template` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_translation` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `labelhash` varchar(40) NOT NULL default '',
  `systemstring` varchar(255) default NULL,
  `lang` varchar(4) NOT NULL default '',
  `site_identifier` varchar(20) NOT NULL default '',
  `localstring` text,
  `template` varchar(255) default NULL,
  `line` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `labelhash` (`lang`,`site_identifier`,`labelhash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_translation` WRITE;
/*!40000 ALTER TABLE `brickwork_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_translation` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brickwork_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brickwork_url` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(11) default NULL,
  `link` varchar(255) default NULL,
  `target` enum('_self','_top','_blank') default NULL,
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_site_structure_id` (`site_structure_id`,`lang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brickwork_url` WRITE;
/*!40000 ALTER TABLE `brickwork_url` DISABLE KEYS */;
/*!40000 ALTER TABLE `brickwork_url` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `file_cabinet_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_cabinet_file` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `folder_id` int(10) unsigned NOT NULL,
  `user_id` int(10) default NULL,
  `prio` float default NULL,
  `create_datetime` datetime NOT NULL,
  `size` int(10) default NULL,
  `mime` varchar(80) default NULL,
  `name` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `file_cabinet_file` WRITE;
/*!40000 ALTER TABLE `file_cabinet_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_cabinet_file` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `file_cabinet_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_cabinet_folder` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(10) unsigned default NULL,
  `prio` float default NULL,
  `create_datetime` datetime default NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `file_cabinet_folder` WRITE;
/*!40000 ALTER TABLE `file_cabinet_folder` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_cabinet_folder` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `file_cabinet_module_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_cabinet_module_config` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `file_rootdir` varchar(255) default '',
  `root_folder` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `file_cabinet_module_config` WRITE;
/*!40000 ALTER TABLE `file_cabinet_module_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_cabinet_module_config` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `file_cabinet_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_cabinet_right` (
  `folder_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `rights` set('read','write','delete') default NULL,
  PRIMARY KEY  (`folder_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `file_cabinet_right` WRITE;
/*!40000 ALTER TABLE `file_cabinet_right` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_cabinet_right` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `lookup_module_agenda_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lookup_module_agenda_site` (
  `agenda_id` int(10) NOT NULL default '0',
  `site_identifier` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`agenda_id`,`site_identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `lookup_module_agenda_site` WRITE;
/*!40000 ALTER TABLE `lookup_module_agenda_site` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookup_module_agenda_site` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `lookup_module_banner_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lookup_module_banner_group` (
  `banner_id` int(10) unsigned NOT NULL default '0',
  `group_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`banner_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `lookup_module_banner_group` WRITE;
/*!40000 ALTER TABLE `lookup_module_banner_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookup_module_banner_group` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `lookup_module_config_deeplink_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lookup_module_config_deeplink_page` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `prio` float(10,3) unsigned NOT NULL default '1000.000',
  UNIQUE KEY `page_content_id` (`page_content_id`,`site_structure_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `lookup_module_config_deeplink_page` WRITE;
/*!40000 ALTER TABLE `lookup_module_config_deeplink_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookup_module_config_deeplink_page` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `lookup_module_config_link_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lookup_module_config_link_category` (
  `page_content_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  KEY `page_content_id` (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `lookup_module_config_link_category` WRITE;
/*!40000 ALTER TABLE `lookup_module_config_link_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookup_module_config_link_category` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `lookup_module_content_link_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lookup_module_content_link_category` (
  `link_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  KEY `link_id` (`link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `lookup_module_content_link_category` WRITE;
/*!40000 ALTER TABLE `lookup_module_content_link_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookup_module_content_link_category` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `lookup_site_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lookup_site_banner` (
  `site_identifier` char(20) NOT NULL default '',
  `banner_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`site_identifier`,`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `lookup_site_banner` WRITE;
/*!40000 ALTER TABLE `lookup_site_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookup_site_banner` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_agenda_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_agenda_site` (
  `agenda_id` tinyint(10) NOT NULL default '0',
  `site_identifier` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`agenda_id`,`site_identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_agenda_site` WRITE;
/*!40000 ALTER TABLE `module_agenda_site` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_agenda_site` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_agenda` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `items_per_page` tinyint(3) unsigned NOT NULL default '0',
  `order_by` enum('from_date','till_date','create_date') NOT NULL default 'create_date',
  `detail_page_id` int(10) unsigned NOT NULL default '0',
  `event_type_ids` varchar(31) NOT NULL default '',
  `behavior` varchar(31) NOT NULL default '',
  `site_level` int(1) NOT NULL default '0',
  `lookup_table` varchar(63) default NULL,
  `event_table` varchar(63) default NULL,
  `content_table` varchar(63) default NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_agenda` WRITE;
/*!40000 ALTER TABLE `module_config_agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_agenda` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_avoidform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_avoidform` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `form_code` varchar(20) NOT NULL default '',
  `email_recipients` text,
  `sender_email` varchar(255) default NULL,
  `sender_name` varchar(255) default NULL,
  `store_submissions` tinyint(4) NOT NULL default '0',
  `use_from` tinyint(1) unsigned NOT NULL default '0',
  `send_feedback` tinyint(4) NOT NULL default '0',
  `feedback_message` text,
  `feedback_has_formfields` tinyint(4) default '0',
  `confirm_message` text,
  `email_header` text,
  `email_footer` text,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_avoidform` WRITE;
/*!40000 ALTER TABLE `module_config_avoidform` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_avoidform` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_banner` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `max_banners` int(10) unsigned NOT NULL default '2',
  `banner_group` int(10) unsigned default NULL,
  `sort` varchar(20) default NULL,
  `adsense` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_banner` WRITE;
/*!40000 ALTER TABLE `module_config_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_banner` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_deeplink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_deeplink` (
  `page_content_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_deeplink` WRITE;
/*!40000 ALTER TABLE `module_config_deeplink` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_deeplink` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_dmdelivery_soap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_dmdelivery_soap` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `soap_campaign_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_dmdelivery_soap` WRITE;
/*!40000 ALTER TABLE `module_config_dmdelivery_soap` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_dmdelivery_soap` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_dmfacts_pro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_dmfacts_pro` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `dmfacts_account` int(10) unsigned NOT NULL default '0',
  `counter_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_dmfacts_pro` WRITE;
/*!40000 ALTER TABLE `module_config_dmfacts_pro` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_dmfacts_pro` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_faq` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `sort_by` char(20) NOT NULL default '',
  `showed_questions` tinyint(3) NOT NULL default '0',
  `number_showed_questions` tinyint(3) NOT NULL default '0',
  `category_id` int(10) default NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_faq` WRITE;
/*!40000 ALTER TABLE `module_config_faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_faq` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_google_adsense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_google_adsense` (
  `page_content_id` int(10) NOT NULL default '0',
  `client` varchar(255) NOT NULL,
  `slot` varchar(255) NOT NULL,
  `width` int(4) NOT NULL,
  `height` int(4) NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_google_adsense` WRITE;
/*!40000 ALTER TABLE `module_config_google_adsense` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_google_adsense` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_google_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_google_maps` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `maptype` enum('NORMAL','HYBRID','SATELLITE') NOT NULL default 'NORMAL',
  `zoomsize` enum('Small','Large') NOT NULL default 'Small',
  `wheel_zoom` tinyint(1) default NULL,
  `width` int(4) default NULL,
  `height` int(4) NOT NULL default '500',
  `cannot_connect` varchar(255) NOT NULL default '<p>Google Maps</p><p>Momenteel niet bereikbaar.</p>',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_google_maps` WRITE;
/*!40000 ALTER TABLE `module_config_google_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_google_maps` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_htdig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_htdig` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `items_per_page` tinyint(3) unsigned NOT NULL default '10',
  `htdig_config` varchar(20) NOT NULL default 'daily',
  `detail_page_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_htdig` WRITE;
/*!40000 ALTER TABLE `module_config_htdig` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_htdig` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_html`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_html` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `title` enum('prepend','append','replace') default NULL,
  `description` enum('prepend','append','replace') default NULL,
  `keywords` enum('prepend','append','replace') default NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_html` WRITE;
/*!40000 ALTER TABLE `module_config_html` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_html` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_htmlblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_htmlblock` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `html` mediumtext,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_htmlblock` WRITE;
/*!40000 ALTER TABLE `module_config_htmlblock` DISABLE KEYS */;
INSERT INTO `module_config_htmlblock` VALUES (1,'<p>Brickwork &amp; Phoundry zijn succesvol geinstalleerd</p>');
INSERT INTO `module_config_htmlblock` VALUES (3,'<p>Deze pagina kon niet worden gevonden.</p>\r\n<p>U wordt binnen 5 seconden automatisch doorverwezen naar de homepagina.</p>\r\n<script>\r\n setTimeout(function(){ location = \'/\';}, 5000);\r\n</script>');
/*!40000 ALTER TABLE `module_config_htmlblock` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_iframe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_iframe` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `width` int(5) unsigned default NULL,
  `height` int(5) unsigned default NULL,
  `scrolling` enum('auto','yes','no') default NULL,
  `frameborder` tinyint(1) default '0',
  `marginheight` int(5) default NULL,
  `marginwidth` int(5) default NULL,
  `url` text NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_iframe` WRITE;
/*!40000 ALTER TABLE `module_config_iframe` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_iframe` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_link` (
  `page_content_id` int(11) NOT NULL default '0',
  `link_title_sort_id` int(11) NOT NULL default '0',
  `category_title_sort_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_link` WRITE;
/*!40000 ALTER TABLE `module_config_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_link` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_news` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `root_category` int(10) unsigned default NULL,
  `detail_id` int(10) unsigned default NULL,
  `items_pp` int(2) unsigned default '0',
  `view` enum('mini','detail') NOT NULL default 'mini',
  `type` int(1) NOT NULL default '1',
  `reactions` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_news` WRITE;
/*!40000 ALTER TABLE `module_config_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_news` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_photogallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_photogallery` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `detail_id` int(10) unsigned default NULL,
  `action` varchar(50) NOT NULL default 'index',
  `album_sort` enum('alphabet','priority') NOT NULL,
  `photo_sort` enum('alphabet','priority','date') NOT NULL,
  `photo_count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_photogallery` WRITE;
/*!40000 ALTER TABLE `module_config_photogallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_photogallery` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_poll` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `poll_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_poll` WRITE;
/*!40000 ALTER TABLE `module_config_poll` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_poll` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_config_snippet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_config_snippet` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `snippet_id` int(10) unsigned default NULL,
  `snippet_name` varchar(20) NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_config_snippet` WRITE;
/*!40000 ALTER TABLE `module_config_snippet` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_config_snippet` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_agenda` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(512) NOT NULL default '',
  `location` varchar(512) NOT NULL default '',
  `target_audience` varchar(255) default '',
  `organisation` varchar(255) default '',
  `additional_organisation` varchar(255) default NULL,
  `address` varchar(255) default '',
  `intro` text,
  `description` text,
  `from_date` datetime default NULL,
  `till_date` datetime default NULL,
  `create_date` datetime default NULL,
  `offline_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `online_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_modified_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_modified_by` varchar(255) NOT NULL default '',
  `event_type_id` int(10) NOT NULL default '0',
  `prio` float NOT NULL default '0',
  `costs` varchar(255) default NULL,
  `discount` varchar(255) default NULL,
  `route` text,
  `url` varchar(255) default NULL,
  `contact_person` varchar(255) default NULL,
  `contact_phonenumber` varchar(16) default NULL,
  `contact_email` varchar(255) default NULL,
  `subscribe_url` varchar(255) default NULL,
  `image` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_agenda` WRITE;
/*!40000 ALTER TABLE `module_content_agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_agenda` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_agenda_event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_agenda_event_type` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  `site_identifier` varchar(31) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_agenda_event_type` WRITE;
/*!40000 ALTER TABLE `module_content_agenda_event_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_agenda_event_type` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_banner` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `image` varchar(255) default NULL,
  `image_url` varchar(255) default NULL,
  `url` varchar(255) default NULL,
  `popup` tinyint(1) NOT NULL default '0',
  `clickcounter` int(11) NOT NULL default '0',
  `custom_javascript` longtext,
  `width` int(11) default NULL,
  `height` int(11) default NULL,
  `bgcolor` varchar(10) default NULL,
  `online` datetime NOT NULL default '0000-00-00 00:00:00',
  `offline` datetime default NULL,
  `prio` int(11) default NULL,
  `mod_user` varchar(255) NOT NULL default '',
  `mod_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `file` varchar(50) default NULL,
  `param` varchar(255) default NULL,
  `token` varchar(50) default NULL,
  `adsense_client` varchar(40) default NULL,
  `adsense_slot` varchar(20) default NULL,
  `analytics` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_banner` WRITE;
/*!40000 ALTER TABLE `module_content_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_banner` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_bannergroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_bannergroup` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_bannergroup` WRITE;
/*!40000 ALTER TABLE `module_content_bannergroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_bannergroup` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_faq_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_faq_article` (
  `id` int(10) NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `category_id` int(10) NOT NULL default '0',
  `times_showed` int(10) NOT NULL default '0',
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `answer_intro` text NOT NULL,
  `prio` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_faq_article` WRITE;
/*!40000 ALTER TABLE `module_content_faq_article` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_faq_article` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_faq_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_faq_category` (
  `id` int(10) NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `category` varchar(100) NOT NULL,
  `category_image` varchar(255) default '',
  `description` varchar(255) NOT NULL,
  `prio` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_faq_category` WRITE;
/*!40000 ALTER TABLE `module_content_faq_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_faq_category` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_faq_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_faq_ip` (
  `id` int(10) NOT NULL auto_increment,
  `article_id` int(10) NOT NULL default '0',
  `ip` char(15) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `article_id` (`article_id`,`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_faq_ip` WRITE;
/*!40000 ALTER TABLE `module_content_faq_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_faq_ip` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_google_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_google_maps` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `position` varchar(200) default NULL,
  `title` varchar(255) default NULL,
  `body` text,
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_pcid` (`site_identifier`(3),`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_google_maps` WRITE;
/*!40000 ALTER TABLE `module_content_google_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_google_maps` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_html`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_html` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `title` varchar(63) default NULL,
  `keywords` varchar(255) default NULL,
  `description` mediumtext,
  `text` mediumtext,
  `prio` float NOT NULL default '0',
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_pcid` (`site_identifier`(3),`page_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_html` WRITE;
/*!40000 ALTER TABLE `module_content_html` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_html` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_link` (
  `id` int(11) NOT NULL auto_increment,
  `link` varchar(250) NOT NULL default '',
  `title` varchar(50) NOT NULL default '',
  `description` varchar(100) default NULL,
  `prio` float NOT NULL default '1',
  `target` varchar(31) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_link` WRITE;
/*!40000 ALTER TABLE `module_content_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_link` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_link_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_link_category` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL default '',
  `prio` float NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_link_category` WRITE;
/*!40000 ALTER TABLE `module_content_link_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_link_category` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_link_sort_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_link_sort_types` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_link_sort_types` WRITE;
/*!40000 ALTER TABLE `module_content_link_sort_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_link_sort_types` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_poll_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_poll_answer` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` int(10) unsigned NOT NULL default '0',
  `answer` varchar(255) NOT NULL default '',
  `prio` float(10,2) NOT NULL default '100.00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_poll_answer` WRITE;
/*!40000 ALTER TABLE `module_content_poll_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_poll_answer` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_poll_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_poll_question` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `visible` enum('yes','no') NOT NULL default 'no',
  `show_result` enum('yes','no') NOT NULL default 'no',
  `question` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_poll_question` WRITE;
/*!40000 ALTER TABLE `module_content_poll_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_poll_question` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_poll_vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_poll_vote` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` int(10) unsigned NOT NULL default '0',
  `answer_id` int(10) unsigned NOT NULL default '0',
  `ipaddress` int(10) unsigned NOT NULL default '0',
  `browser_hash` varchar(32) default NULL,
  `uniqid` varchar(20) default NULL,
  `vote_date` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_poll_vote` WRITE;
/*!40000 ALTER TABLE `module_content_poll_vote` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_poll_vote` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_content_snippet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_content_snippet` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` char(15) default NULL,
  `mod_datetime` datetime NOT NULL,
  `name` varchar(20) NOT NULL default '',
  `title` varchar(255) default '',
  `code` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_content_snippet` WRITE;
/*!40000 ALTER TABLE `module_content_snippet` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_content_snippet` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_news_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_news_article` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `category_id` int(10) unsigned default NULL,
  `archivable` tinyint(1) NOT NULL default '1',
  `reactions` tinyint(1) NOT NULL default '1',
  `create_datetime` datetime NOT NULL,
  `online_datetime` datetime NOT NULL,
  `offline_datetime` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) default NULL,
  `image` varchar(255) default NULL,
  `intro` text NOT NULL,
  `body` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_news_article` WRITE;
/*!40000 ALTER TABLE `module_news_article` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_news_article` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_news_article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_news_article_category` (
  `article_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`article_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_news_article_category` WRITE;
/*!40000 ALTER TABLE `module_news_article_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_news_article_category` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_news_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_news_category` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(10) unsigned default NULL,
  `prio` float default NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_news_category` WRITE;
/*!40000 ALTER TABLE `module_news_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_news_category` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `module_news_reaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_news_reaction` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `article_id` int(10) unsigned NOT NULL,
  `ipadress` int(10) NOT NULL,
  `datetime` datetime default NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `module_news_reaction` WRITE;
/*!40000 ALTER TABLE `module_news_reaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_news_reaction` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_column` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `string_id` varchar(80) default NULL,
  `table_id` int(11) NOT NULL default '0',
  `order_by` int(11) NOT NULL default '0',
  `name` char(80) NOT NULL default '',
  `description` char(255) NOT NULL default '',
  `datatype` char(40) NOT NULL default '',
  `datatype_extra` text,
  `inputtype` char(40) NOT NULL default '',
  `inputtype_extra` text,
  `required` int(11) NOT NULL default '0',
  `searchable` int(11) NOT NULL default '0',
  `info` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `string_id` (`string_id`,`table_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6049 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_column` WRITE;
/*!40000 ALTER TABLE `phoundry_column` DISABLE KEYS */;
INSERT INTO `phoundry_column` VALUES (1,'id',1,10,'id','Id','DTinteger','a:1:{s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:1:{s:5:\"value\";s:0:\"\";}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2,'name',1,20,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:4:\"Name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (3,'address',1,60,'address','a:2:{s:2:\"nl\";s:5:\"Adres\";s:2:\"en\";s:7:\"Address\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4,'zipcode',1,70,'zipcode','a:2:{s:2:\"nl\";s:8:\"Postcode\";s:2:\"en\";s:7:\"Zipcode\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:7;s:9:\"maxlength\";s:1:\"7\";}',0,0,'');
INSERT INTO `phoundry_column` VALUES (5,'city',1,80,'city','a:2:{s:2:\"nl\";s:10:\"Woonplaats\";s:2:\"en\";s:4:\"City\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";}',0,0,'');
INSERT INTO `phoundry_column` VALUES (6,'tel_nr',1,90,'tel_nr','a:2:{s:2:\"nl\";s:8:\"Tel. nr.\";s:2:\"en\";s:8:\"Tel. nr.\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:24;s:9:\"maxlength\";s:2:\"24\";}',0,0,'');
INSERT INTO `phoundry_column` VALUES (7,'fax_nr',1,100,'fax_nr','a:2:{s:2:\"nl\";s:7:\"Fax nr.\";s:2:\"en\";s:7:\"Fax nr.\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:24;s:9:\"maxlength\";s:2:\"24\";}',0,0,'');
INSERT INTO `phoundry_column` VALUES (8,'e_mail',1,50,'e_mail','a:2:{s:2:\"nl\";s:6:\"E-mail\";s:2:\"en\";s:6:\"E-mail\";}','DTemail','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";}',1,0,'');
INSERT INTO `phoundry_column` VALUES (9,'username',1,30,'username','a:2:{s:2:\"nl\";s:8:\"Username\";s:2:\"en\";s:8:\"Username\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:24;s:9:\"maxlength\";s:2:\"24\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (10,'password',1,14,'password','a:2:{s:2:\"nl\";s:10:\"Wachtwoord\";s:2:\"en\";s:8:\"Password\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:6:\"wpsha1\";}','ITpassword','a:3:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:12:\"showStrength\";b:1;}',1,4,'');
INSERT INTO `phoundry_column` VALUES (11,'password_set',1,110,'password_set','Password set','DTdate','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (12,'phoundry_user_group',1,130,'phoundry_user_group','a:2:{s:2:\"nl\";s:6:\"Rollen\";s:2:\"en\";s:8:\"Group(s)\";}','DTXref','a:2:{s:7:\"src_col\";s:7:\"user_id\";s:7:\"dst_col\";s:8:\"group_id\";}','ITXmultipleselect','a:5:{s:5:\"query\";s:49:\"SELECT id, name FROM phoundry_group ORDER BY name\";s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'');
INSERT INTO `phoundry_column` VALUES (13,'id',2,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (14,'name',2,20,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:4:\"Name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (15,'phoundry_group_table',2,30,'phoundry_group_table','a:2:{s:2:\"nl\";s:8:\"Gebieden\";s:2:\"en\";s:5:\"Areas\";}','DTXref','a:2:{s:7:\"src_col\";s:8:\"group_id\";s:7:\"dst_col\";s:8:\"table_id\";}','ITXduallist','a:6:{s:5:\"query\";s:114:\"SELECT id, description FROM phoundry_table WHERE show_in_menu NOT LIKE \'admin%\' ORDER BY show_in_menu, description\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:11:\"description\";}s:10:\"searchtext\";a:1:{i:0;s:11:\"description\";}}',0,1,'');
INSERT INTO `phoundry_column` VALUES (16,'phoundry_user_group',2,40,'phoundry_user_group','a:2:{s:2:\"nl\";s:10:\"Gebruikers\";s:2:\"en\";s:5:\"Users\";}','DTXref','a:2:{s:7:\"src_col\";s:8:\"group_id\";s:7:\"dst_col\";s:7:\"user_id\";}','ITXduallist','a:5:{s:5:\"query\";s:68:\"SELECT id, name FROM phoundry_user WHERE admin IS NULL ORDER BY name\";s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'');
INSERT INTO `phoundry_column` VALUES (17,'id',4,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (18,'table_id',4,20,'table_id','Table','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:4:{s:5:\"query\";s:73:\"SELECT id, name FROM phoundry_table WHERE is_plugin = 0 ORDER BY order_by\";s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,0,'');
INSERT INTO `phoundry_column` VALUES (19,'type',4,40,'type','Type','DTstring','a:3:{s:7:\"default\";s:6:\"system\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (20,'required',4,50,'required','Required?','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"1\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (21,'group_id',4,60,'group_id','Group','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:4:{s:5:\"query\";s:49:\"SELECT id, name FROM phoundry_group ORDER BY name\";s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'');
INSERT INTO `phoundry_column` VALUES (22,'name',4,70,'name','Name','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";}',1,1,'Enter the name of the filter.');
INSERT INTO `phoundry_column` VALUES (23,'wherepart',4,80,'wherepart','Wherepart','DTAfilter','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITAfilter','a:4:{s:4:\"cols\";i:60;s:4:\"rows\";i:10;s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;}',0,1,'When the \'LIKE\' operator is chosen, you can use ? and * as wildcards.<br />If the value is left empty, then this is applied in the filter. This way, you can filter on (non-)empty fields.');
INSERT INTO `phoundry_column` VALUES (24,'id',5,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (25,'table_id',5,20,'table_id','Table','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:4:{s:5:\"query\";s:73:\"SELECT id, name FROM phoundry_table WHERE is_plugin = 0 ORDER BY order_by\";s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (26,'type',5,30,'type','Type','DTstring','a:3:{s:7:\"default\";s:4:\"user\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (27,'required',5,40,'required','Required?','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"2\";}}','ITradio','a:2:{s:5:\"query\";s:80:\"0=No|1=Yes (insert uses chosen filter)|2=Yes (insert does not use chosen filter)\";s:9:\"updatable\";b:1;}',1,1,'Does the user <b>have</b> to pick a filter?');
INSERT INTO `phoundry_column` VALUES (28,'group_id',5,50,'group_id','Group','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:4:{s:5:\"query\";s:49:\"SELECT id, name FROM phoundry_group ORDER BY name\";s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'');
INSERT INTO `phoundry_column` VALUES (29,'name',5,60,'name','Name','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";}',1,1,'Enter the name of the filter.');
INSERT INTO `phoundry_column` VALUES (30,'query',5,70,'query','Query','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:4:{s:4:\"cols\";i:60;s:4:\"rows\";i:5;s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (31,'matchfield',5,80,'matchfield','Matchfield','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITAtablefields','a:2:{s:4:\"size\";i:40;s:9:\"maxlength\";s:2:\"40\";}',1,0,'');
INSERT INTO `phoundry_column` VALUES (32,'wherepart',5,90,'wherepart','a:2:{s:2:\"nl\";s:9:\"Wherepart\";s:2:\"en\";s:9:\"Wherepart\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:5:{s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:1:\"5\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:96:\"Enter the WHERE-part (the actual filter). Use {$FILTER_VALUE} for matches with the filter query.\";s:2:\"en\";s:96:\"Enter the WHERE-part (the actual filter). Use {$FILTER_VALUE} for matches with the filter query.\";}');
INSERT INTO `phoundry_column` VALUES (33,'id',6,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (34,'table_id',6,20,'table_id','Table id','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:4:{s:5:\"query\";s:73:\"SELECT id, name FROM phoundry_table WHERE is_plugin = 0 ORDER BY order_by\";s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,0,'');
INSERT INTO `phoundry_column` VALUES (35,'event',6,30,'event','Event','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITAeventevent','',1,1,'');
INSERT INTO `phoundry_column` VALUES (36,'code',6,40,'code','Code','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:4:\"code\";s:8:\"encoding\";s:0:\"\";}','ITAeventcode','a:4:{s:4:\"cols\";i:120;s:4:\"rows\";i:20;s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;}',1,1,'<p>If you wish to show error messages, make the event return the error message as a string.<br />You can use the following (global) variables:</p><ul><li>$event string containing the type of event, \"pre-update\", \"post-delete\" etc</li><li>$this Phoundry object which can be accessed for the id, string_id, name, description etc</li><li>$db</li><li>$PHprefs</li><li>$PHSes</li><li>$_POST, $_GET, $_SERVER etc.</li></ul>');
INSERT INTO `phoundry_column` VALUES (37,'last_login_date',1,112,'last_login_date','a:2:{s:2:\"en\";s:10:\"Last login\";s:2:\"nl\";s:13:\"Laatste login\";}','DTdatetime','','ITreadonly','',0,1,'');
INSERT INTO `phoundry_column` VALUES (38,'last_login_ip',1,114,'last_login_ip','a:2:{s:2:\"en\";s:13:\"Last login IP\";s:2:\"nl\";s:16:\"Laatste login IP\";}','DTstring','','ITreadonly','',0,1,'');
INSERT INTO `phoundry_column` VALUES (39,'send_login',1,160,'send_login','a:2:{s:2:\"nl\";s:8:\"Verstuur\";s:2:\"en\";s:4:\"Send\";}','DTfake','a:2:{s:9:\"html_code\";s:163:\"<a href=\"#\" onclick=\"sendLogin(<?= $RID ?>, event);return false\"><img src=\"<?= $PHprefs[\'url\'] ?>/core/icons/email_go.png\" border=\"0\" width=\"16\" height=\"16\" /></a>\";s:7:\"js_code\";s:178:\"function sendLogin(rid, evt) {\r\n   makePopup(evt, 400, 300, \'<?= escSquote(word(309, $GLOBALS[\'PRODUCT\'][\'name\'])) ?>\', \'popups/sendLogin.php?<?= QS(0,\'RID\') ?>&RID=\'+rid);\r\n} \r\n\";}','ITfake',NULL,0,1,'');
INSERT INTO `phoundry_column` VALUES (2037,'id',208,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2038,'name',208,20,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:4:\"Name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2039,'street',208,30,'street','a:2:{s:2:\"nl\";s:6:\"Straat\";s:2:\"en\";s:6:\"Street\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2040,'house_nr',208,40,'house_nr','a:2:{s:2:\"nl\";s:10:\"Huisnummer\";s:2:\"en\";s:12:\"House number\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2041,'house_nr_add',208,50,'house_nr_add','a:2:{s:2:\"nl\";s:21:\"Huisnummer toevoeging\";s:2:\"en\";s:21:\"House number addition\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2042,'city',208,60,'city','a:2:{s:2:\"nl\";s:6:\"Plaats\";s:2:\"en\";s:4:\"City\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2043,'country',208,70,'country','a:2:{s:2:\"nl\";s:4:\"Land\";s:2:\"en\";s:7:\"Country\";}','DTstring','a:3:{s:7:\"default\";s:2:\"NL\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:57:\"SELECT code,name FROM brickwork_country ORDER BY name ASC\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:4:\"code\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2044,'phone_area_code',208,80,'phone_area_code','a:2:{s:2:\"nl\";s:9:\"netnummer\";s:2:\"en\";s:15:\"Phone area code\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2045,'phone_number',208,90,'phone_number','a:2:{s:2:\"nl\";s:10:\"Telefoonnr\";s:2:\"en\";s:12:\"Phone number\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2046,'email_info',208,100,'email_info','a:2:{s:2:\"nl\";s:16:\"E-mailadres info\";s:2:\"en\";s:11:\"E-mail info\";}','DTemail','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2047,'email_privacy',208,110,'email_privacy','a:2:{s:2:\"nl\";s:19:\"E-mailadres privacy\";s:2:\"en\";s:14:\"E-mail privacy\";}','DTemail','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2048,'email_support',208,120,'email_support','a:2:{s:2:\"nl\";s:19:\"E-mailadres support\";s:2:\"en\";s:14:\"E-mail support\";}','DTemail','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2091,'code',215,10,'code','a:2:{s:2:\"nl\";s:4:\"Code\";s:2:\"en\";s:4:\"Code\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:17:\"^[A-z][A-z0-9_]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2092,'class',215,20,'class','a:2:{s:2:\"nl\";s:5:\"Class\";s:2:\"en\";s:0:\"\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:17:\"^[A-z][A-z0-9_]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:50;s:9:\"maxlength\";s:2:\"50\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (2093,'name',215,30,'name','a:2:{s:2:\"nl\";s:4:\"Name\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (2094,'template',215,40,'template','a:2:{s:2:\"nl\";s:8:\"Template\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (2095,'cachetime',215,50,'cachetime','a:2:{s:2:\"nl\";s:9:\"Cachetime\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:10:\"in seconds\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2096,'ptid_content',215,60,'ptid_content','a:2:{s:2:\"nl\";s:13:\"Content Tabel\";s:2:\"en\";s:13:\"Content table\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:73:\"select id, name from phoundry_table where is_plugin = 0 order by name asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2097,'ptid_config',215,70,'ptid_config','a:2:{s:2:\"nl\";s:18:\"Configuratie tabel\";s:2:\"en\";s:12:\"Config table\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:73:\"select id, name from phoundry_table where is_plugin = 0 order by name asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2105,'max_per_page',215,80,'max_per_page','a:2:{s:2:\"nl\";s:12:\"Max per page\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:3:\"100\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (2098,'id',218,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2099,'site_identifier',218,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2100,'page_content_id',218,30,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:18:\"{$page_content_id}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2110,'prio',218,70,'prio','a:2:{s:2:\"nl\";s:10:\"Prioriteit\";s:2:\"en\";s:8:\"Priority\";}','DTfloat','a:1:{s:7:\"default\";s:90:\"{$SELECT max(prio)+10 FROM module_content_html WHERE page_content_id = {$page_content_id}}\";}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:105:\"SELECT prio, title FROM module_content_html WHERE page_content_id = {$page_content_id} ORDER BY prio DESC\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2101,'title',218,40,'title','a:2:{s:2:\"nl\";s:5:\"Titel\";s:2:\"en\";s:5:\"Title\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2102,'description',218,50,'description','a:2:{s:2:\"nl\";s:12:\"Omschrijving\";s:2:\"en\";s:11:\"Description\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:3:\"255\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2103,'text',218,60,'text','a:2:{s:2:\"nl\";s:4:\"HTML\";s:2:\"en\";s:4:\"HTML\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:4:\"html\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:19:\"IMAGES/htmlcontent/\";s:9:\"attachDir\";s:18:\"FILES/htmlcontent/\";s:6:\"config\";s:2:\"{}\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4297,'keywords',218,55,'keywords','a:2:{s:2:\"nl\";s:8:\"Keywords\";s:2:\"en\";s:8:\"Keywords\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2111,'id',222,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2112,'page_type_code',222,20,'page_type_code','a:2:{s:2:\"nl\";s:11:\"Pagina type\";s:2:\"en\";s:9:\"Page Type\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradioThumb','a:6:{s:5:\"query\";s:174:\"SELECT pt.code,pt.name from brickwork_page_type pt INNER JOIN brickwork_site s ON s.identifier = \'{$site_identifier}\' AND s.template = pt.template_identifier ORDER BY pt.name\";s:6:\"imgUrl\";s:62:\"/phoundry/custom/brickwork/page_type/index.php/thumb/{$option}\";s:9:\"updatable\";b:1;s:11:\"optionvalue\";s:7:\"pt.code\";s:10:\"optiontext\";a:1:{i:0;s:7:\"pt.name\";}s:10:\"searchtext\";a:1:{i:0;s:7:\"pt.name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2113,'site_structure_id',222,30,'site_structure_id','a:2:{s:2:\"nl\";s:17:\"Site structure id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:20:\"{$site_structure_id}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2114,'title',222,40,'title','a:2:{s:2:\"nl\";s:5:\"Titel\";s:2:\"en\";s:5:\"Title\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2115,'description',222,50,'description','a:2:{s:2:\"nl\";s:12:\"Omschrijving\";s:2:\"en\";s:11:\"Description\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:3:\"255\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2116,'online_date',222,60,'online_date','a:2:{s:2:\"nl\";s:12:\"Online datum\";s:2:\"en\";s:11:\"Online date\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2117,'offline_date',222,70,'offline_date','a:2:{s:2:\"nl\";s:13:\"Offline datum\";s:2:\"en\";s:12:\"Offline date\";}','DTdatetime','a:1:{s:7:\"default\";s:12:\"{$TODAY+10Y}\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2118,'staging_status',222,80,'staging_status','a:2:{s:2:\"nl\";s:14:\"Staging status\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:4:\"live\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradio','a:2:{s:5:\"query\";s:19:\"test=Test|live=Live\";s:9:\"updatable\";b:1;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (2550,'keywords',222,55,'keywords','a:2:{s:2:\"nl\";s:8:\"Keywords\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (2551,'layout',222,90,'layout','a:2:{s:2:\"nl\";s:6:\"Layout\";s:2:\"en\";s:0:\"\";}','DTfake','a:2:{s:9:\"html_code\";s:292:\"<a href=\"<?=$PHprefs[\'url\']?>/custom/brickwork/page_layout/edit.php?RID=<?=$RID?>&amp;TID=<?=$TID?>&amp;page_id=<?=$RID?>&amp;site_identifier=<?=$_GET[\'site_identifier\']?>\">\r\n<img src=\"<?=$PHprefs[\'url\']?>/core/icons/layout_content.png\" height=\"16\" width=\"16\" alt=\"Layout\" border=\"0\" /></a>\r\n\";s:7:\"js_code\";s:0:\"\";}','ITfake',NULL,0,1,'');
INSERT INTO `phoundry_column` VALUES (4295,'lang_id',222,100,'lang_id','a:2:{s:2:\"nl\";s:4:\"Taal\";s:2:\"en\";s:8:\"Language\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:89:\"SELECT id, code FROM brickwork_site_language where site_identifier = \'{$site_identifier}\'\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:26:\"brickwork_site_language.id\";s:10:\"optiontext\";a:1:{i:0;s:28:\"brickwork_site_language.code\";}s:10:\"searchtext\";a:1:{i:0;s:28:\"brickwork_site_language.code\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2119,'code',223,10,'code','a:2:{s:2:\"nl\";s:4:\"Code\";s:2:\"en\";s:0:\"\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:17:\"^[A-z][A-z0-9_]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2120,'template_identifier',223,20,'template_identifier','a:2:{s:2:\"nl\";s:8:\"Template\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:6:{s:5:\"query\";s:48:\"select identifier, name from brickwork_template \";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:10:\"identifier\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2121,'name',223,30,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:4:\"Name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2122,'contentspecification',223,40,'contentspecification','a:2:{s:2:\"nl\";s:24:\"Content Container layout\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:5:{s:4:\"cols\";s:3:\"120\";s:4:\"rows\";s:2:\"50\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (2126,'identifier',225,10,'identifier','a:2:{s:2:\"nl\";s:10:\"Identifier\";s:2:\"en\";s:0:\"\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:17:\"^[A-z][A-z0-9_]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2127,'protocol',225,20,'protocol','a:2:{s:2:\"nl\";s:8:\"Protocol\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:4:\"http\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITmultipleselect','a:2:{s:5:\"query\";s:21:\"http=http|https=https\";s:4:\"size\";i:6;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2128,'name',225,30,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:4:\"Name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2129,'domain',225,25,'domain','a:2:{s:2:\"nl\";s:6:\"Domein\";s:2:\"en\";s:6:\"Domain\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:50;s:9:\"maxlength\";s:2:\"50\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2130,'status',225,40,'status','a:2:{s:2:\"nl\";s:6:\"Status\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:4:\"test\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradio','a:2:{s:5:\"query\";s:19:\"test=Test|live=Live\";s:9:\"updatable\";b:1;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2131,'template',225,50,'template','a:2:{s:2:\"nl\";s:8:\"Template\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:7:\"default\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:6:{s:5:\"query\";s:64:\"select identifier,name from brickwork_template order by name asc\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:10:\"identifier\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2132,'organisation_id',225,60,'organisation_id','a:2:{s:2:\"nl\";s:11:\"Organisatie\";s:2:\"en\";s:12:\"Organisation\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:61:\"SELECT id, name from brickwork_organisation order by name asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2390,'default_lang_id',225,70,'default_lang_id','a:2:{s:2:\"nl\";s:14:\"Standaard taal\";s:2:\"en\";s:16:\"Default Language\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:91:\"SELECT id, code FROM brickwork_site_language WHERE site_identifier = \'{$RID}\' ORDER BY code\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"code\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"code\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2133,'identifier',226,10,'identifier','a:2:{s:2:\"nl\";s:10:\"Identifier\";s:2:\"en\";s:0:\"\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:17:\"^[A-z][A-z0-9_]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:15;s:9:\"maxlength\";s:2:\"15\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2134,'name',226,20,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:4:\"Name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2135,'code',227,10,'code','a:2:{s:2:\"nl\";s:4:\"Code\";s:2:\"en\";s:0:\"\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:17:\"^[A-z][A-z0-9_]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2136,'page_type_code',227,5,'page_type_code','a:2:{s:2:\"nl\";s:11:\"Pagina type\";s:2:\"en\";s:9:\"Page type\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:41:\"select code,name from brickwork_page_type\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:4:\"code\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2554,'brickwork_content_container_allows_module',227,20,'brickwork_content_container_allows_module','a:2:{s:2:\"nl\";s:18:\"Toegestane modules\";s:2:\"en\";s:15:\"Allowed modules\";}','DTXref','a:4:{s:7:\"src_col\";s:20:\"content_container_id\";s:7:\"dst_col\";s:11:\"module_code\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:67:\"SELECT code, code AS code_2 FROM brickwork_module ORDER BY code ASC\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:21:\"brickwork_module.code\";s:10:\"optiontext\";a:1:{i:0;s:23:\"brickwork_module.code_2\";}s:10:\"searchtext\";a:1:{i:0;s:23:\"brickwork_module.code_2\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4294,'brickwork_content_container_default_module',227,30,'brickwork_content_container_default_module','a:2:{s:2:\"nl\";s:17:\"Standaard modules\";s:2:\"en\";s:15:\"Default modules\";}','DTXref','a:4:{s:7:\"src_col\";s:20:\"content_container_id\";s:7:\"dst_col\";s:11:\"module_code\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:216:\"SELECT code, code AS code_2 FROM brickwork_module AS bm\r\nINNER JOIN `brickwork_content_container_allows_module` AS bccal\r\nON bccal.module_code = bm.code\r\nWHERE bccal.content_container_id = \'{$RID}\'\r\nORDER BY code ASC\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:7:\"bm.code\";s:10:\"optiontext\";a:1:{i:0;s:9:\"bm.code_2\";}s:10:\"searchtext\";a:1:{i:0;s:9:\"bm.code_2\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2138,'protocol',228,10,'protocol','a:2:{s:2:\"nl\";s:8:\"Protocol\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:10:\"http,https\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITmultipleselect','a:2:{s:5:\"query\";s:21:\"http=http|https=https\";s:4:\"size\";i:6;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2139,'host',228,20,'host','a:2:{s:2:\"nl\";s:4:\"Host\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:50;s:9:\"maxlength\";s:2:\"50\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2140,'site_identifier',228,5,'site_identifier','a:2:{s:2:\"nl\";s:4:\"Site\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:6:{s:5:\"query\";s:61:\"SELECT identifier, name from brickwork_site order by name asc\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:10:\"identifier\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2141,'redirect',228,30,'redirect','a:2:{s:2:\"nl\";s:8:\"Redirect\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:2:{s:5:\"query\";s:10:\"1=Yes|0=No\";s:9:\"updatable\";b:1;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2552,'id',228,3,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2553,'prio',228,40,'prio','a:2:{s:2:\"nl\";s:10:\"Prioriteit\";s:2:\"en\";s:8:\"Priority\";}','DTfloat','a:1:{s:7:\"default\";s:96:\"{$SELECT max(prio)+1 FROM brickwork_site_host_alias WHERE site_identifier=\"{$site_identifier}\" }\";}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:110:\"SELECT prio, host FROM brickwork_site_host_alias WHERE site_identifier=\"{$site_identifier}\" ORDER BY prio DESC\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2256,'id',251,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2257,'site_structure_id',251,20,'site_structure_id','a:2:{s:2:\"nl\";s:14:\"Site structure\";s:2:\"en\";s:14:\"Site structure\";}','DTinteger','a:2:{s:7:\"default\";s:20:\"{$site_structure_id}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2258,'link',251,30,'link','a:2:{s:2:\"nl\";s:3:\"Url\";s:2:\"en\";s:3:\"Url\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2259,'target',251,40,'target','a:2:{s:2:\"nl\";s:4:\"Doel\";s:2:\"en\";s:6:\"Target\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:35:\"_self=_self|_top=_top|_blank=_blank\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4296,'lang_id',251,50,'lang_id','a:2:{s:2:\"nl\";s:4:\"Taal\";s:2:\"en\";s:8:\"Language\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:89:\"SELECT id, code FROM brickwork_site_language where site_identifier = \'{$site_identifier}\'\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:26:\"brickwork_site_language.id\";s:10:\"optiontext\";a:1:{i:0;s:28:\"brickwork_site_language.code\";}s:10:\"searchtext\";a:1:{i:0;s:28:\"brickwork_site_language.code\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2307,'code',262,10,'code','a:2:{s:2:\"nl\";s:4:\"Code\";s:2:\"en\";s:0:\"\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:17:\"^[A-z][A-z0-9_]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2308,'title',262,20,'title','a:2:{s:2:\"nl\";s:5:\"Titel\";s:2:\"en\";s:5:\"Title\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2309,'code',263,10,'code','a:2:{s:2:\"nl\";s:4:\"Code\";s:2:\"en\";s:4:\"Code\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:17:\"^[A-z][A-z0-9_]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2310,'site_identifier',263,20,'site_identifier','a:2:{s:2:\"nl\";s:4:\"Site\";s:2:\"en\";s:4:\"Site\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2311,'title',263,30,'title','a:2:{s:2:\"nl\";s:5:\"Titel\";s:2:\"en\";s:5:\"Title\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2312,'type_code',263,40,'type_code','a:2:{s:2:\"nl\";s:4:\"Type\";s:2:\"en\";s:4:\"Type\";}','DTstring','a:3:{s:7:\"default\";s:5:\"avoid\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2547,'header',263,50,'header','a:2:{s:2:\"nl\";s:8:\"Koptekst\";s:2:\"en\";s:6:\"Header\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2548,'footer',263,60,'footer','a:2:{s:2:\"nl\";s:9:\"Voettekst\";s:2:\"en\";s:6:\"Footer\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2331,'soap_campaign_id',268,20,'soap_campaign_id','a:2:{s:2:\"nl\";s:13:\"Soap campaign\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:6:{s:5:\"query\";s:108:\"select id, campaign_title from brickwork_dmdelivery_soap_campaign where site_identifier=\"{$site_identifier}\"\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:14:\"campaign_title\";}s:10:\"searchtext\";a:1:{i:0;s:14:\"campaign_title\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2330,'page_content_id',268,10,'page_content_id','a:2:{s:2:\"nl\";s:12:\"Page content\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2332,'id',269,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2333,'site_identifier',269,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2334,'campaign_title',269,30,'campaign_title','a:2:{s:2:\"nl\";s:14:\"Campagne titel\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2335,'campaign_id',269,40,'campaign_id','a:2:{s:2:\"nl\";s:11:\"Campaign id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2336,'soap_url',269,50,'soap_url','a:2:{s:2:\"nl\";s:8:\"Soap url\";s:2:\"en\";s:0:\"\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2337,'username',269,60,'username','a:2:{s:2:\"nl\";s:8:\"Username\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2338,'password',269,70,'password','a:2:{s:2:\"nl\";s:8:\"Password\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2350,'form_code',272,10,'form_code','a:2:{s:2:\"nl\";s:9:\"Formulier\";s:2:\"en\";s:4:\"Form\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:56:\"select code,title from brickwork_form order by title asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:4:\"code\";s:10:\"optiontext\";a:1:{i:0;s:5:\"title\";}s:10:\"searchtext\";a:1:{i:0;s:5:\"title\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (654,'send_feedback',273,100,'send_feedback','a:2:{s:2:\"nl\";s:19:\"Confirmatie sturen?\";s:2:\"en\";s:17:\"Send confirmation\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,0,'a:2:{s:2:\"nl\";s:91:\"Dit werkt alleen als er een veld in het formulier staat die controleert op een e-mailsyntax\";s:2:\"en\";s:87:\"This only works if there is a minimum of one fields in the database with a email syntax\";}');
INSERT INTO `phoundry_column` VALUES (545,'email_footer',273,80,'email_footer','a:2:{s:2:\"nl\";s:18:\"E-mail tekst onder\";s:2:\"en\";s:13:\"E-mail footer\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:105:\"Deze tekst wordt onder de formulier gegevens getoont in de e-mail naar de \"E-mailadressen van ontvangers\"\";s:2:\"en\";s:84:\"This message will be shown below the form data in the email send to the \"Recipients\"\";}');
INSERT INTO `phoundry_column` VALUES (352,'page_content_id',273,10,'page_content_id','a:2:{s:2:\"nl\";s:12:\"Page content\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (353,'form_code',273,20,'form_code','a:2:{s:2:\"nl\";s:9:\"Formulier\";s:2:\"en\";s:4:\"Form\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:80:\"select code,title from brickwork_form where type_code=\'avoid\' order by title asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:4:\"code\";s:10:\"optiontext\";a:1:{i:0;s:5:\"title\";}s:10:\"searchtext\";a:1:{i:0;s:5:\"title\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (355,'email_recipients',273,30,'email_recipients','a:2:{s:2:\"nl\";s:29:\"E-mailadressen van ontvangers\";s:2:\"en\";s:10:\"Recipients\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:29:\"komma of puntkomma gescheiden\";s:2:\"en\";s:44:\"comma or semicolon separated email addresses\";}');
INSERT INTO `phoundry_column` VALUES (356,'sender_email',273,40,'sender_email','a:2:{s:2:\"nl\";s:20:\"E-mailadres afzender\";s:2:\"en\";s:13:\"E-mail sender\";}','DTemail','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (357,'sender_name',273,50,'sender_name','a:2:{s:2:\"nl\";s:13:\"Afzender naam\";s:2:\"en\";s:11:\"Sender name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (544,'email_header',273,70,'email_header','a:2:{s:2:\"nl\";s:18:\"E-mail tekst boven\";s:2:\"en\";s:13:\"E-mail header\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:105:\"Deze tekst wordt boven de formulier gegevens getoond in de e-mail naar de \"E-mailadressen van ontvangers\"\";s:2:\"en\";s:84:\"This message will be shown above the form data in the email send to the \"Recipients\"\";}');
INSERT INTO `phoundry_column` VALUES (532,'confirm_message',273,60,'confirm_message','a:2:{s:2:\"nl\";s:13:\"Bedankt tekst\";s:2:\"en\";s:15:\"Confirm message\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:4:\"html\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:19:\"IMAGES/htmlcontent/\";s:9:\"attachDir\";s:18:\"FILES/htmlcontent/\";s:6:\"config\";s:2:\"{}\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (643,'store_submissions',273,90,'store_submissions','a:2:{s:2:\"nl\";s:19:\"Opslaan in database\";s:2:\"en\";s:28:\"Store submission in database\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (655,'feedback_message',273,110,'feedback_message','a:2:{s:2:\"nl\";s:19:\"Confirmatie bericht\";s:2:\"en\";s:20:\"Confirmation message\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:111:\"Dit bericht wordt naar de bezoeker verstuurd met daaronder de ingevulde formulier gegevens (mits dit aan staat)\";s:2:\"en\";s:136:\"This message will be send to the website visitor. Right under this message will be the values that were entered in the form (if enabled)\";}');
INSERT INTO `phoundry_column` VALUES (656,'feedback_has_formfields',273,120,'feedback_has_formfields','a:2:{s:2:\"nl\";s:35:\"Confirmatie bevat formuliergegevens\";s:2:\"en\";s:30:\"Confirmation includes formdata\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2358,'page_content_id',274,10,'page_content_id','a:2:{s:2:\"nl\";s:12:\"Page content\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2359,'items_per_page',274,20,'items_per_page','a:2:{s:2:\"nl\";s:21:\"Resutlaten per pagina\";s:2:\"en\";s:14:\"Items per page\";}','DTinteger','a:2:{s:7:\"default\";s:2:\"10\";s:5:\"range\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"*\";}}','ITselect','a:4:{s:5:\"query\";s:21:\"5=5|10=10|15=15|20=20\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2360,'htdig_config',274,30,'htdig_config','a:2:{s:2:\"nl\";s:17:\"Configuratie file\";s:2:\"en\";s:11:\"Config file\";}','DTregexp','a:2:{s:7:\"default\";s:5:\"daily\";s:6:\"format\";s:19:\"^[A-z][A-z0-9_\\.]*$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2367,'detail_page_id',274,40,'detail_page_id','a:2:{s:2:\"nl\";s:13:\"Detail pagina\";s:2:\"en\";s:11:\"Detail page\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:268:\"select \r\nss.id, \r\nss.name \r\nfrom \r\nbrickwork_site_structure ss \r\ninner join \r\nbrickwork_page p ON p.site_structure_id = ss.id \r\nAND \r\nnow() \r\nBETWEEN p.online_date AND p.offline_date \r\nWHERE \r\nsite_identifier=\'{$site_identifier}\' and stype=\'page\'\r\nORDER BY ss.prio ASC\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2361,'id',275,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2362,'site_identifier',275,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2363,'site_structure_id',275,30,'site_structure_id','a:2:{s:2:\"nl\";s:14:\"Site structuur\";s:2:\"en\";s:14:\"Site structure\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:105:\"SELECT id,name from brickwork_site_structure where site_identifier=\'{$site_identifier}\' order by prio asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2364,'prio',275,40,'prio','a:2:{s:2:\"nl\";s:10:\"Prioriteit\";s:2:\"en\";s:8:\"Priority\";}','DTfloat','a:1:{s:7:\"default\";s:5:\"0.000\";}','ITpriority','a:2:{s:5:\"order\";s:3:\"asc\";s:5:\"query\";s:194:\"SELECT bqm.prio, ss.name FROM brickwork_quick_menu bqm INNER JOIN brickwork_site_structure ss ON ss.id = bqm.site_structure_id WHERE bqm.site_identifier=\'{$site_identifier}\' ORDER BY ss.prio ASC\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2382,'position',275,15,'position','a:2:{s:2:\"nl\";s:7:\"Positie\";s:2:\"en\";s:8:\"Position\";}','DTstring','a:3:{s:7:\"default\";s:6:\"footer\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradio','a:3:{s:5:\"query\";s:27:\"footer=footer|header=header\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2383,'id',282,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2384,'site_identifier',282,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2385,'dmfacts_account_code',282,30,'dmfacts_account_code','a:2:{s:2:\"nl\";s:15:\"DMfacts account\";s:2:\"en\";s:0:\"\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:12:\"^[A-z0-9_]+$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2386,'script',282,40,'script','a:2:{s:2:\"nl\";s:6:\"Script\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:2:{s:5:\"query\";s:10:\"1=Ja|0=Nee\";s:9:\"updatable\";b:1;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2387,'page_content_id',283,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2388,'dmfacts_account',283,20,'dmfacts_account','a:2:{s:2:\"nl\";s:15:\"Dmfacts account\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:6:{s:5:\"query\";s:143:\"select id, dmfacts_account_code from brickwork_dmfacts_pro_account where site_identifier=\"{$site_identifier}\" order by dmfacts_account_code asc\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:20:\"dmfacts_account_code\";}s:10:\"searchtext\";a:1:{i:0;s:20:\"dmfacts_account_code\";}}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2389,'counter_id',283,30,'counter_id','a:2:{s:2:\"nl\";s:13:\"Teller nummer\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4288,'systemstring',290,20,'systemstring','a:2:{s:2:\"nl\";s:4:\"Bron\";s:2:\"en\";s:6:\"Source\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4289,'localstring',290,30,'localstring','a:2:{s:2:\"nl\";s:9:\"Vertaling\";s:2:\"en\";s:11:\"Translation\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:0:\"\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4290,'lang',290,40,'lang','a:2:{s:2:\"nl\";s:8:\"Taalcode\";s:2:\"en\";s:8:\"Language\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:6:{s:5:\"query\";s:127:\"SELECT code FROM brickwork_site_language WHERE site_identifier = \'{$site_identifier}\' [AND code = \'{$RECORD_ID}\'] ORDER BY code\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:4:\"code\";s:10:\"optiontext\";a:1:{i:0;s:4:\"code\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"code\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4291,'site_identifier',290,49,'site_identifier','a:2:{s:2:\"nl\";s:4:\"Site\";s:2:\"en\";s:4:\"Site\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:93:\"SELECT identifier, name FROM brickwork_site [WHERE identifier = \'{$RECORD_ID}\'] ORDER BY name\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:10:\"identifier\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4292,'template',290,60,'template','a:2:{s:2:\"nl\";s:8:\"Sjabloon\";s:2:\"en\";s:8:\"Template\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITreadonly','a:0:{}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4293,'line',290,70,'line','a:2:{s:2:\"nl\";s:7:\"Regelnr\";s:2:\"en\";s:4:\"Line\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITreadonly','a:0:{}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4283,'id',290,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:2:\"Id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4278,'id',292,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:2:\"Id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4279,'site_identifier',292,20,'site_identifier','a:2:{s:2:\"nl\";s:4:\"Site\";s:2:\"en\";s:4:\"Site\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:99:\"SELECT identifier, name FROM brickwork_site [WHERE identifier = \'{$RECORD_ID}\'] ORDER BY identifier\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:10:\"identifier\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4280,'code',292,30,'code','a:2:{s:2:\"nl\";s:8:\"Taalcode\";s:2:\"en\";s:13:\"Language Code\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:4;s:9:\"maxlength\";s:1:\"4\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4281,'locale',292,40,'locale','a:2:{s:2:\"nl\";s:6:\"Locale\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:5:\"nl_NL\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:5;s:9:\"maxlength\";s:2:\"25\";s:7:\"counter\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4282,'namespace',292,50,'namespace','a:2:{s:2:\"nl\";s:9:\"Namespace\";s:2:\"en\";s:9:\"Namespace\";}','DTstring','a:3:{s:7:\"default\";s:4:\"site\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2475,'id',300,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2476,'site_identifier',300,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2477,'subdir',300,30,'subdir','a:2:{s:2:\"nl\";s:6:\"Subdir\";s:2:\"en\";s:4:\"Path\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:29:\"^([a-z0-9_\\/|-]+)\\?{0,1}(.*)$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:219:\"Deze link wordt achter het domein geplaatst en mag bestaan uit alle letters van het alfabet, underscores (_), forward-slashes(/), pipes(|) en min-tekes(-). Er wordt geen verschil gemaakt tussen hoofd- en kleine letters.\";s:2:\"en\";s:175:\"The path on which the link is reachable. The path may only consist of alphanumeric characters and underscores, forward-slashes, pipes and hyphen. The path is case insensitive.\";}');
INSERT INTO `phoundry_column` VALUES (2478,'site_structure_id',300,40,'site_structure_id','a:2:{s:2:\"nl\";s:14:\"Site Structuur\";s:2:\"en\";s:14:\"Site Structure\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:109:\"SELECT id, name from brickwork_site_structure ss where site_identifier=\"{$site_identifier}\" order by prio asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2537,'required',305,180,'required','a:2:{s:2:\"nl\";s:9:\"Verplicht\";s:2:\"en\";s:8:\"Required\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2549,'info',305,190,'info','a:2:{s:2:\"nl\";s:4:\"Info\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (2494,'id',305,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2495,'brickwork_form_code',305,20,'brickwork_form_code','a:2:{s:2:\"nl\";s:14:\"Formulier code\";s:2:\"en\";s:9:\"Form code\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:31;s:9:\"maxlength\";s:2:\"31\";s:7:\"counter\";b:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2496,'fieldtype',305,30,'fieldtype','a:2:{s:2:\"nl\";s:8:\"Veldtype\";s:2:\"en\";s:10:\"Field type\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:60:\"select name, name from brickwork_db_form_field order by name\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:4:\"name\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2497,'dst_scheme',305,40,'dst_scheme','a:2:{s:2:\"nl\";s:11:\"Doel Schema\";s:2:\"en\";s:18:\"Destination Schema\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:31;s:9:\"maxlength\";s:2:\"31\";s:7:\"counter\";b:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2498,'dst_table',305,50,'dst_table','a:2:{s:2:\"nl\";s:10:\"Doel Tabel\";s:2:\"en\";s:17:\"Destination Table\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2499,'dst_field',305,60,'dst_field','a:2:{s:2:\"nl\";s:9:\"Doel Veld\";s:2:\"en\";s:17:\"Destination Field\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2500,'src_scheme',305,70,'src_scheme','a:2:{s:2:\"nl\";s:11:\"Bron Schema\";s:2:\"en\";s:13:\"Source Schema\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:31;s:9:\"maxlength\";s:2:\"31\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2501,'src_table',305,80,'src_table','a:2:{s:2:\"nl\";s:9:\"SRC Tabel\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (2502,'src_field',305,90,'src_field','a:2:{s:2:\"nl\";s:9:\"Bron Veld\";s:2:\"en\";s:12:\"Source field\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2503,'src_label',305,100,'src_label','a:2:{s:2:\"nl\";s:10:\"Bron Label\";s:2:\"en\";s:12:\"Source Label\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2504,'src_constraint',305,110,'src_constraint','a:2:{s:2:\"nl\";s:14:\"Bron Beperking\";s:2:\"en\";s:17:\"Source Constraint\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2505,'label',305,25,'label','a:2:{s:2:\"nl\";s:5:\"Label\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (2506,'prio',305,170,'prio','a:2:{s:2:\"nl\";s:10:\"Prioriteit\";s:2:\"en\";s:8:\"Priority\";}','DTfloat','a:1:{s:7:\"default\";s:1:\"0\";}','ITpriority','a:2:{s:5:\"order\";s:3:\"asc\";s:5:\"query\";s:68:\"SELECT prio, label FROM brickwork_db_form_mapping ORDER BY prio DESC\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2507,'multiplicity',305,140,'multiplicity','a:2:{s:2:\"nl\";s:13:\"Multiplicatie\";s:2:\"en\";s:12:\"Multiplicity\";}','DTstring','a:3:{s:7:\"default\";s:1:\"1\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:65:\"+=1 of meer|*=0 of meer|1=1|2=2|3=3|4=4|5=5|6=6|7=7|8=8|9=9|10=10\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2508,'disabled',305,150,'disabled','a:2:{s:2:\"nl\";s:13:\"Uitgeschakeld\";s:2:\"en\";s:8:\"Disabled\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2509,'active',305,160,'active','a:2:{s:2:\"nl\";s:6:\"Actief\";s:2:\"en\";s:6:\"Active\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2538,'id',311,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (2539,'online_date',311,20,'online_date','a:2:{s:2:\"nl\";s:12:\"Online datum\";s:2:\"en\";s:11:\"Online date\";}','DTdatetime','a:1:{s:7:\"default\";s:0:\"\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2540,'offline_date',311,30,'offline_date','a:2:{s:2:\"nl\";s:13:\"Offline datum\";s:2:\"en\";s:12:\"Offline date\";}','DTdatetime','a:1:{s:7:\"default\";s:0:\"\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (2542,'staging_status',311,50,'staging_status','a:2:{s:2:\"nl\";s:6:\"Status\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:4:\"live\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradio','a:2:{s:5:\"query\";s:19:\"live=Live|test=Test\";s:9:\"updatable\";b:1;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (2543,'respect_auth',311,60,'respect_auth','a:2:{s:2:\"nl\";s:38:\"Rekening houden met pagina afscherming\";s:2:\"en\";s:21:\"Respect page security\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4113,'id',320,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4114,'name',320,20,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:4:\"Name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4115,'description',320,30,'description','a:2:{s:2:\"nl\";s:12:\"Omschrijving\";s:2:\"en\";s:11:\"Description\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:1:\"3\";s:9:\"maxlength\";s:3:\"255\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4116,'parent_id',320,40,'parent_id','a:2:{s:2:\"nl\";s:6:\"Parent\";s:2:\"en\";s:6:\"Parent\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:80:\"SELECT id, name FROM brickwork_auth_role [WHERE id = {$RECORD_ID}] ORDER BY name\";s:9:\"updatable\";b:0;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4298,'brickwork_site_user_site',322,90,'brickwork_site_user_site','a:2:{s:2:\"nl\";s:5:\"Sites\";s:2:\"en\";s:0:\"\";}','DTXref','a:4:{s:7:\"src_col\";s:12:\"site_user_id\";s:7:\"dst_col\";s:15:\"site_identifier\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:57:\"SELECT identifier, name FROM brickwork_site ORDER BY name\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:25:\"brickwork_site.identifier\";s:10:\"optiontext\";a:1:{i:0;s:19:\"brickwork_site.name\";}s:10:\"searchtext\";a:1:{i:0;s:19:\"brickwork_site.name\";}}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4118,'id',322,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4119,'username',322,20,'username','a:2:{s:2:\"nl\";s:10:\"Login naam\";s:2:\"en\";s:12:\"Account name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"128\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4120,'password',322,30,'password','a:2:{s:2:\"nl\";s:10:\"Wachtwoord\";s:2:\"en\";s:8:\"Password\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITpassword','a:2:{s:4:\"size\";i:32;s:9:\"maxlength\";s:2:\"32\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4121,'displayname',322,40,'displayname','a:2:{s:2:\"nl\";s:10:\"Schermnaam\";s:2:\"en\";s:12:\"Display name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4122,'email',322,50,'email','a:2:{s:2:\"nl\";s:11:\"E-mailadres\";s:2:\"en\";s:6:\"E-mail\";}','DTemail','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"128\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4123,'createdon',322,60,'createdon','a:2:{s:2:\"nl\";s:12:\"Aanmaakdatum\";s:2:\"en\";s:13:\"Creation date\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4124,'brickwork_site_user_auth_role',322,70,'brickwork_site_user_auth_role','a:2:{s:2:\"nl\";s:6:\"Rollen\";s:2:\"en\";s:5:\"Roles\";}','DTXref','a:4:{s:7:\"src_col\";s:12:\"site_user_id\";s:7:\"dst_col\";s:12:\"auth_role_id\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:54:\"SELECT id, name FROM brickwork_auth_role ORDER BY name\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (6046,'brickwork_site_phoundry_user',1,170,'brickwork_site_phoundry_user','a:2:{s:2:\"nl\";s:4:\"Site\";s:2:\"en\";s:4:\"Site\";}','DTXref','a:5:{s:7:\"src_col\";s:16:\"phoundry_user_id\";s:7:\"dst_col\";s:15:\"site_identifier\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";s:12:\"extra_values\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:57:\"SELECT identifier, name FROM brickwork_site ORDER BY name\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:25:\"brickwork_site.identifier\";s:10:\"optiontext\";a:1:{i:0;s:19:\"brickwork_site.name\";}s:10:\"searchtext\";a:1:{i:0;s:19:\"brickwork_site.name\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4131,'id',324,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4132,'code',324,20,'code','a:2:{s:2:\"nl\";s:4:\"Code\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4133,'class',324,30,'class','a:2:{s:2:\"nl\";s:5:\"Class\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:40;s:9:\"maxlength\";s:2:\"40\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4134,'name',324,40,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:4:\"Name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"128\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4135,'secure',324,50,'secure','a:2:{s:2:\"nl\";s:9:\"Beveiligd\";s:2:\"en\";s:6:\"Secure\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4136,'debug',324,60,'debug','a:2:{s:2:\"nl\";s:5:\"Debug\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4137,'auth_method',324,70,'auth_method','a:2:{s:2:\"nl\";s:21:\"Authenticatie methode\";s:2:\"en\";s:21:\"Authentication method\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:58:\"basic=Service Authenticate|phoundry=Phoundry Authenticatie\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4138,'restrict_ip',324,80,'restrict_ip','a:2:{s:2:\"nl\";s:14:\"IP Restricties\";s:2:\"en\";s:15:\"IP restrictions\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:57:\"Per regel een IP adres. De * wildcard mag gebruikt worden\";s:2:\"en\";s:49:\"One IP adress per line. * can be used as wildcard\";}');
INSERT INTO `phoundry_column` VALUES (4139,'ptid_config',324,90,'ptid_config','a:2:{s:2:\"nl\";s:18:\"Configuratie tabel\";s:2:\"en\";s:12:\"Config table\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:73:\"select id, name from phoundry_table where is_plugin = 0 order by name asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4140,'ptid_content',324,100,'ptid_content','a:2:{s:2:\"nl\";s:13:\"Content tabel\";s:2:\"en\";s:13:\"Content table\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:73:\"select id, name from phoundry_table where is_plugin = 0 order by name asc\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4141,'cachetime',324,110,'cachetime','a:2:{s:2:\"nl\";s:10:\"Cache tijd\";s:2:\"en\";s:10:\"Cache time\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:11;s:9:\"maxlength\";s:2:\"11\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4142,'created_on',324,120,'created_on','a:2:{s:2:\"nl\";s:10:\"Gemaakt op\";s:2:\"en\";s:10:\"Created on\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','IThidden','a:0:{}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4143,'id',325,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4144,'username',325,20,'username','a:2:{s:2:\"nl\";s:14:\"Gebruikersnaam\";s:2:\"en\";s:8:\"Username\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:32;s:9:\"maxlength\";s:2:\"32\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4145,'password',325,30,'password','a:2:{s:2:\"nl\";s:10:\"Wachtwoord\";s:2:\"en\";s:8:\"Password\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITpassword','a:2:{s:4:\"size\";i:32;s:9:\"maxlength\";s:2:\"32\";}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4146,'description',325,40,'description','a:2:{s:2:\"nl\";s:12:\"Omschrijving\";s:2:\"en\";s:11:\"Description\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:3:\"255\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4147,'restrict_ip',325,50,'restrict_ip','a:2:{s:2:\"nl\";s:14:\"IP Restricties\";s:2:\"en\";s:15:\"IP restrictions\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:57:\"Een ip adres per regel, De wildcard * kan gebruikt worden\";s:2:\"en\";s:51:\"One ip adress per line, * can be used as a wildcard\";}');
INSERT INTO `phoundry_column` VALUES (4148,'disabled',325,60,'disabled','a:2:{s:2:\"nl\";s:6:\"Actief\";s:2:\"en\";s:7:\"Enabled\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$check}|1={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4149,'created_on',325,70,'created_on','a:2:{s:2:\"nl\";s:13:\"Aanmaak datum\";s:2:\"en\";s:16:\"Date of creation\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','ITreadonly','a:0:{}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4150,'created_by',325,80,'created_by','a:2:{s:2:\"nl\";s:15:\"Aangemaakt door\";s:2:\"en\";s:10:\"Created by\";}','DTstring','a:3:{s:7:\"default\";s:12:\"{$USER_NAME}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITreadonly','a:0:{}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4151,'brickwork_service_user_module',325,61,'brickwork_service_user_module','a:2:{s:2:\"nl\";s:19:\"Toegang tot modules\";s:2:\"en\";s:18:\"Accessible modules\";}','DTXref','a:4:{s:7:\"src_col\";s:15:\"service_user_id\";s:7:\"dst_col\";s:17:\"service_module_id\";s:8:\"ordr_col\";s:17:\"service_module_id\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:59:\"SELECT id, name FROM brickwork_service_module ORDER BY name\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4299,'http_host',327,10,'http_host','a:2:{s:2:\"nl\";s:9:\"Http host\";s:2:\"en\";s:0:\"\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4300,'api_key',327,20,'api_key','a:2:{s:2:\"nl\";s:7:\"Api key\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4301,'page_content_id',328,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4302,'maptype',328,20,'maptype','a:2:{s:2:\"nl\";s:10:\"Type Kaart\";s:2:\"en\";s:8:\"Map type\";}','DTstring','a:3:{s:7:\"default\";s:6:\"NORMAL\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:45:\"NORMAL=Kaart|SATELLITE=Satelliet|HYBRID=Beide\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4303,'zoomsize',328,30,'zoomsize','a:2:{s:2:\"nl\";s:14:\"Inzoom grootte\";s:2:\"en\";s:9:\"Zoom size\";}','DTstring','a:3:{s:7:\"default\";s:5:\"Small\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:23:\"Small=Klein|Large=Groot\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4304,'wheel_zoom',328,40,'wheel_zoom','a:2:{s:2:\"nl\";s:13:\"Muiswiel zoom\";s:2:\"en\";s:14:\"Mouswheel zoom\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4305,'width',328,50,'width','a:2:{s:2:\"nl\";s:19:\"Breedte (in pixels)\";s:2:\"en\";s:14:\"Width (pixels)\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:4;s:9:\"maxlength\";s:1:\"4\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:43:\"Laat leeg om de gehele breedte te gebruiken\";s:2:\"en\";s:32:\"Empty to use all available width\";}');
INSERT INTO `phoundry_column` VALUES (4306,'height',328,60,'height','a:2:{s:2:\"nl\";s:18:\"Hoogte (in pixels)\";s:2:\"en\";s:15:\"Height (pixels)\";}','DTinteger','a:2:{s:7:\"default\";s:3:\"500\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:4;s:9:\"maxlength\";s:1:\"4\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4307,'cannot_connect',328,70,'cannot_connect','a:2:{s:2:\"nl\";s:27:\"Google maps offline bericht\";s:2:\"en\";s:27:\"Google maps failure message\";}','DTstring','a:3:{s:7:\"default\";s:51:\"<p>Google Maps</p><p>Momenteel niet bereikbaar.</p>\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:19:\"IMAGES/htmlcontent/\";s:9:\"attachDir\";s:18:\"FILES/htmlcontent/\";s:6:\"config\";s:2:\"{}\";}',1,0,'a:2:{s:2:\"nl\";s:77:\"Dit bericht wordt getoond als Google maps niet bereikbaar is voor de bezoeker\";s:2:\"en\";s:95:\"This message will be displayed if the user fails to load the Google Maps libraries from Google.\";}');
INSERT INTO `phoundry_column` VALUES (4308,'id',329,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4309,'site_identifier',329,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4310,'page_content_id',329,30,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:18:\"{$page_content_id}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4311,'title',329,40,'title','a:2:{s:2:\"nl\";s:5:\"Titel\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4312,'position',329,50,'position','a:2:{s:2:\"nl\";s:7:\"Positie\";s:2:\"en\";s:0:\"\";}','DTgoogleMapCoords','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"200\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4313,'body',329,60,'body','a:2:{s:2:\"nl\";s:12:\"Ballon Tekst\";s:2:\"en\";s:12:\"Ballon Tekst\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:3:\"95%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:19:\"IMAGES/htmlcontent/\";s:9:\"attachDir\";s:18:\"FILES/htmlcontent/\";s:6:\"config\";s:2:\"{}\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4314,'brickwork_photogallery_lookup',330,80,'brickwork_photogallery_lookup','a:2:{s:2:\"nl\";s:6:\"Albums\";s:2:\"en\";s:0:\"\";}','DTXref','a:4:{s:7:\"src_col\";s:15:\"page_content_id\";s:7:\"dst_col\";s:8:\"album_id\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:65:\"SELECT id,title FROM brickwork_photogallery_album ORDER BY title;\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:31:\"brickwork_photogallery_album.id\";s:10:\"optiontext\";a:1:{i:0;s:34:\"brickwork_photogallery_album.title\";}s:10:\"searchtext\";a:1:{i:0;s:34:\"brickwork_photogallery_album.title\";}}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4315,'album_sort',330,50,'album_sort','a:2:{s:2:\"nl\";s:22:\"Album sorteer volgorde\";s:2:\"en\";s:16:\"Album sort order\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:42:\"alphabet=Op alfabet|priority=Op prioriteit\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4316,'action',330,20,'action','a:2:{s:2:\"nl\";s:8:\"Weergave\";s:2:\"en\";s:5:\"Style\";}','DTstring','a:3:{s:7:\"default\";s:5:\"index\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:114:\"index=Album|slidebar=Filmstrip|coolslide=Coolslide|photostack=Stapelen|coolslideThumbs=Coolslide met miniweergaves\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4317,'page_content_id',330,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4318,'photo_sort',330,60,'photo_sort','a:2:{s:2:\"nl\";s:21:\"Foto sorteer volgorde\";s:2:\"en\";s:16:\"Photo sort order\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:56:\"alphabet=Op alfabet|priority=Op prioriteit|date=Op datum\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4321,'batch_upload',331,70,'batch_upload','a:2:{s:2:\"nl\";s:12:\"Batch upload\";s:2:\"en\";s:0:\"\";}','DTfake','a:2:{s:9:\"html_code\";s:87:\"<a href=\"<?=$PHprefs[\'url\']?>custom/brickwork/photogallery/?album=<?=$RID?>\">Upload</a>\";s:7:\"js_code\";s:0:\"\";}','ITfake',NULL,0,1,'');
INSERT INTO `phoundry_column` VALUES (4322,'id',331,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4323,'active',331,50,'active','a:2:{s:2:\"nl\";s:6:\"Actief\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4324,'title',331,30,'title','a:2:{s:2:\"nl\";s:10:\"Albumtitel\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:38:\"Geef hier een titel voor dit fotoalbum\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4325,'prio',331,40,'prio','a:2:{s:2:\"nl\";s:10:\"Prioriteit\";s:2:\"en\";s:8:\"Priority\";}','DTfloat','a:1:{s:7:\"default\";s:0:\"\";}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:71:\"SELECT prio, title FROM brickwork_photogallery_album ORDER BY prio DESC\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4326,'thumbnail',331,60,'thumbnail','a:2:{s:2:\"nl\";s:10:\"Afbeelding\";s:2:\"en\";s:0:\"\";}','DTfile','a:5:{s:7:\"savedir\";s:19:\"IMAGES/photogallery\";s:12:\"allow_upload\";b:1;s:13:\"allow_makedir\";b:1;s:17:\"select_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}s:17:\"upload_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}}','ITfile','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4327,'id',332,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4328,'album_id',332,20,'album_id','a:2:{s:2:\"nl\";s:5:\"Album\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:90:\"SELECT id, title FROM brickwork_photogallery_album [WHERE id={$RECORD_ID}] ORDER BY title;\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:5:\"title\";}s:10:\"searchtext\";a:1:{i:0;s:5:\"title\";}}',1,1,'a:2:{s:2:\"nl\";s:59:\"Selecteer hier in welk album deze foto moet worden getoond.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4329,'title',332,30,'title','a:2:{s:2:\"nl\";s:5:\"Titel\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:34:\"Geef hier een titel voor deze foto\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4330,'description',332,40,'description','a:2:{s:2:\"nl\";s:12:\"Beschrijving\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:39:\"Geef hier een beschrijving bij de foto.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4331,'location',332,50,'location','a:2:{s:2:\"nl\";s:7:\"Bestand\";s:2:\"en\";s:0:\"\";}','DTfile','a:5:{s:7:\"savedir\";s:19:\"IMAGES/photogallery\";s:12:\"allow_upload\";b:1;s:13:\"allow_makedir\";b:0;s:17:\"select_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}s:17:\"upload_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}}','ITfile','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4332,'pubdate',332,60,'pubdate','a:2:{s:2:\"nl\";s:16:\"Publicatie Datum\";s:2:\"en\";s:0:\"\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4333,'prio',332,70,'prio','a:2:{s:2:\"nl\";s:10:\"Prioriteit\";s:2:\"en\";s:8:\"Priority\";}','DTfloat','a:1:{s:7:\"default\";s:0:\"\";}','ITpriority','a:2:{s:5:\"order\";s:3:\"asc\";s:5:\"query\";s:70:\"SELECT prio, title FROM brickwork_photogallery_photo ORDER BY prio ASC\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4334,'page_content_id',333,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4335,'title',333,20,'title','a:2:{s:2:\"nl\";s:22:\"Titel als pagina titel\";s:2:\"en\";s:5:\"Title\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:52:\"prepend=Vooraan toevoegen|append=Achteraan toevoegen\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',0,1,'a:2:{s:2:\"nl\";s:238:\"<p>Hiermee kan de titel van deze html pagina aan de globale pagina toegevoegd worden.</p>\r\n<p>De titel zal binnen de &lt;title&gt; tag voor de bestaande of achter de bestaande titel getoond worden, afhankelijk van wat wordt ingesteld.</p>\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4336,'description',333,30,'description','a:2:{s:2:\"nl\";s:36:\"Beschrijving als pagina beschrijving\";s:2:\"en\";s:11:\"Description\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:90:\"prepend=Vooraan toevoegen|append=Achteraan toevoegen|replace=Gehele beschrijving vervangen\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',0,0,'a:2:{s:2:\"nl\";s:160:\"<p>De beschrijving van de HTML aan de beschrijving van de pagina toevoegen.</p>\r\n<p>Deze instelling heeft invloed op de &lt;meta name=\"description\"&gt; tag.</p>\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4337,'keywords',333,40,'keywords','a:2:{s:2:\"nl\";s:28:\"Keywords als pagina keywords\";s:2:\"en\";s:8:\"Keywords\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:90:\"prepend=Vooraan toevoegen|append=Achteraan toevoegen|replace=Gehele beschrijving vervangen\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',0,0,'a:2:{s:2:\"nl\";s:146:\"<p>Keywords van de HTML aan de keywords van de pagina toevoegen.</p>\r\n<p>Deze instelling heeft invloed op de &lt;meta name=\"keywords\"&gt; tag.</p>\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4338,'page_content_id',334,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4339,'html',334,20,'html','a:2:{s:2:\"nl\";s:4:\"Html\";s:2:\"en\";s:4:\"Html\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:4:\"html\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:3:\"95%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:19:\"IMAGES/htmlcontent/\";s:9:\"attachDir\";s:18:\"FILES/htmlcontent/\";s:6:\"config\";s:2:\"{}\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4340,'page_content_id',335,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4341,'lookup_module_config_deeplink_page',335,20,'lookup_module_config_deeplink_page','a:2:{s:2:\"nl\";s:8:\"Pagina\'s\";s:2:\"en\";s:5:\"Pages\";}','DTXref','a:4:{s:7:\"src_col\";s:15:\"page_content_id\";s:7:\"dst_col\";s:17:\"site_structure_id\";s:8:\"ordr_col\";s:4:\"prio\";s:7:\"default\";s:0:\"\";}','ITXduallist_ordered','a:6:{s:5:\"query\";s:185:\"SELECT id, name FROM brickwork_site_structure ss where site_identifier=\'{$site_identifier}\' AND (SELECT count(*) FROM brickwork_page WHERE site_structure_id=ss.id) > 0 ORDER BY prio ASC\";s:8:\"addTable\";i:0;s:4:\"size\";i:8;s:11:\"optionvalue\";s:5:\"ss.id\";s:10:\"optiontext\";a:1:{i:0;s:7:\"ss.name\";}s:10:\"searchtext\";a:1:{i:0;s:7:\"ss.name\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4344,'value',337,40,'value','a:2:{s:2:\"nl\";s:6:\"Waarde\";s:2:\"en\";s:5:\"Value\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:3:\"255\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4345,'attribute',337,30,'attribute','a:2:{s:2:\"nl\";s:9:\"Attribuut\";s:2:\"en\";s:9:\"Attribute\";}','DTregexp','a:2:{s:7:\"default\";s:0:\"\";s:6:\"format\";s:12:\"^[A-z0-9_]+$\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4346,'id',337,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4347,'site_identifier',337,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4348,'modified',337,50,'modified','a:2:{s:2:\"nl\";s:9:\"Aangepast\";s:2:\"en\";s:8:\"Modified\";}','DTdatetime','a:1:{s:7:\"default\";s:11:\"{$MOD_DATE}\";}','ITreadonly','a:0:{}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4349,'id',338,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4350,'site_identifier',338,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4351,'attribute',338,30,'attribute','a:2:{s:2:\"nl\";s:4:\"Type\";s:2:\"en\";s:4:\"Type\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:94:\"SELECT name, title FROM brickwork_site_config_page [WHERE name = \'{$RECORD_ID}\'] ORDER BY name\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:31:\"brickwork_site_config_page.name\";s:10:\"optiontext\";a:1:{i:0;s:32:\"brickwork_site_config_page.title\";}s:10:\"searchtext\";a:1:{i:0;s:32:\"brickwork_site_config_page.title\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4352,'value',338,40,'value','a:2:{s:2:\"nl\";s:6:\"Pagina\";s:2:\"en\";s:4:\"Page\";}','DTstring','a:3:{s:7:\"default\";s:5:\"_page\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:130:\"SELECT id, name FROM brickwork_site_structure WHERE site_identifier = \'{$site_identifier}\' [AND id = \'{$RECORD_ID}\'] ORDER BY name\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4353,'modified',338,50,'modified','a:2:{s:2:\"nl\";s:9:\"Aangepast\";s:2:\"en\";s:8:\"Modified\";}','DTdatetime','a:1:{s:7:\"default\";s:11:\"{$MOD_DATE}\";}','ITreadonly','a:0:{}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4354,'page_content_id',339,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4355,'client',339,20,'client','a:2:{s:2:\"nl\";s:11:\"Client code\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4356,'slot',339,30,'slot','a:2:{s:2:\"nl\";s:17:\"AdSense slot code\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4357,'width',339,40,'width','a:2:{s:2:\"nl\";s:7:\"Breedte\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:4;s:9:\"maxlength\";s:1:\"4\";s:7:\"counter\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4358,'height',339,50,'height','a:2:{s:2:\"nl\";s:6:\"Hoogte\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:4;s:9:\"maxlength\";s:1:\"4\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4359,'marginwidth',340,80,'marginwidth','a:2:{s:2:\"nl\";s:23:\"Linker en rechter marge\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:5;s:9:\"maxlength\";s:1:\"5\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4360,'marginheight',340,70,'marginheight','a:2:{s:2:\"nl\";s:20:\"Boven en onder marge\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:5;s:9:\"maxlength\";s:1:\"5\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4361,'frameborder',340,60,'frameborder','a:2:{s:2:\"nl\";s:4:\"Rand\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:1;s:9:\"maxlength\";s:1:\"1\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4362,'url',340,40,'url','a:2:{s:2:\"nl\";s:7:\"Website\";s:2:\"en\";s:0:\"\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:0:\"\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4363,'scrolling',340,50,'scrolling','a:2:{s:2:\"nl\";s:12:\"Scrollbalken\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:4:\"auto\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradio','a:3:{s:5:\"query\";s:36:\"auto=Automatisch|yes=Altijd|no=Nooit\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4364,'height',340,30,'height','a:2:{s:2:\"nl\";s:6:\"Hoogte\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:5;s:9:\"maxlength\";s:1:\"5\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:102:\"Laat leeg om de hele hoogte te gebruiken (werkt alleen als het paginaonderdeel een vaste hoogte heeft)\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4365,'width',340,20,'width','a:2:{s:2:\"nl\";s:7:\"Breedte\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:5;s:9:\"maxlength\";s:1:\"5\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:67:\"Laat leeg om de gehele breedte van de contentcontainer te gebruiken\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4366,'page_content_id',340,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4367,'code',341,40,'code','a:2:{s:2:\"nl\";s:4:\"Code\";s:2:\"en\";s:4:\"Code\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:19:\"IMAGES/htmlcontent/\";s:9:\"attachDir\";s:18:\"FILES/htmlcontent/\";s:6:\"config\";s:2:\"{}\";}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4384,'mod_datetime',341,50,'mod_datetime','a:2:{s:2:\"nl\";s:17:\"Modificatie datum\";s:2:\"en\";s:0:\"\";}','DTdatetime','a:1:{s:7:\"default\";s:11:\"{$MOD_DATE}\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4368,'title',341,30,'title','a:2:{s:2:\"nl\";s:5:\"Titel\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4369,'name',341,25,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4370,'id',341,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4371,'site_identifier',341,20,'site_identifier','a:2:{s:2:\"nl\";s:15:\"Site identifier\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4373,'page_content_id',342,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4380,'snippet_name',342,30,'snippet_name','a:2:{s:2:\"nl\";s:7:\"Snippet\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:7:{s:5:\"query\";s:134:\"SELECT DISTINCT name FROM module_content_snippet WHERE site_identifier = \'{$site_identifier}\' OR site_identifier IS NULL ORDER BY name\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:27:\"module_content_snippet.name\";s:10:\"optiontext\";a:1:{i:0;s:27:\"module_content_snippet.name\";}s:10:\"searchtext\";a:1:{i:0;s:27:\"module_content_snippet.name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4375,'code',343,40,'code','a:2:{s:2:\"nl\";s:4:\"Code\";s:2:\"en\";s:4:\"Code\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:19:\"IMAGES/htmlcontent/\";s:9:\"attachDir\";s:18:\"FILES/htmlcontent/\";s:6:\"config\";s:2:\"{}\";}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4376,'title',343,30,'title','a:2:{s:2:\"nl\";s:5:\"Titel\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4377,'name',343,25,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4378,'id',343,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4593,'user',377,80,'user','Medewerker','DTstring','a:3:{s:7:\"default\";s:16:\"{$MOD_USER_NAME}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"128\";s:7:\"counter\";b:0;}',0,1,'Deze melding is afgehandeld door');
INSERT INTO `phoundry_column` VALUES (4592,'status',377,70,'status','Status','DTstring','a:3:{s:7:\"default\";s:4:\"open\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradio','a:3:{s:5:\"query\";s:57:\"open=Open{$nocheck}|pending=Bezig|closed=Gesloten{$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4590,'message',377,45,'message','Bericht','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4591,'action',377,60,'action','Actie','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"128\";s:7:\"counter\";b:0;}',0,1,'Welke actie is er ondernomen?');
INSERT INTO `phoundry_column` VALUES (4588,'name',377,30,'name','Naam','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"128\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4589,'email',377,40,'email','E-mail','DTemail','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4586,'id',377,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4587,'hash',377,120,'hash','Key','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITreadonly','a:0:{}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4387,'page_content_id',346,10,'page_content_id','a:2:{s:2:\"nl\";s:15:\"Page content id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4388,'max_banners',346,20,'max_banners','a:2:{s:2:\"nl\";s:22:\"Maximum aantal banners\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"2\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:74:\"Geef hier het maximum aantal banners wat in de module getoond moet worden.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4389,'banner_group',346,30,'banner_group','a:2:{s:2:\"nl\";s:12:\"Banner groep\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:88:\"SELECT id, name FROM module_content_bannergroup [WHERE id = {$RECORD_ID}] ORDER BY name;\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:29:\"module_content_bannergroup.id\";s:10:\"optiontext\";a:1:{i:0;s:31:\"module_content_bannergroup.name\";}s:10:\"searchtext\";a:1:{i:0;s:31:\"module_content_bannergroup.name\";}}',0,1,'a:2:{s:2:\"nl\";s:67:\"Selecteer hier welke banner groep in de module getoond moet worden.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4390,'adsense',346,40,'adsense','a:2:{s:2:\"nl\";s:23:\"Google Adsense toestaan\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:62:\"Selecteer of banners van Google AdSense getoond moeten worden.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4391,'sort',346,50,'sort','a:2:{s:2:\"nl\";s:10:\"Sorteer op\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:61:\"=Niet|prio DESC=prioriteit|mod_date DESC=laatste toegevoeging\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4392,'id',347,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4393,'name',347,20,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:34:\"Geef hier een naam voor de banner.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4394,'image',347,30,'image','a:2:{s:2:\"nl\";s:10:\"Afbeelding\";s:2:\"en\";s:5:\"Image\";}','DTfile','a:5:{s:7:\"savedir\";s:14:\"IMAGES/banners\";s:12:\"allow_upload\";b:1;s:13:\"allow_makedir\";b:1;s:17:\"select_extensions\";a:7:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";i:4;s:3:\"swf\";i:5;s:3:\"flv\";i:6;s:3:\"wmv\";}s:17:\"upload_extensions\";a:7:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";i:4;s:3:\"swf\";i:5;s:3:\"flv\";i:6;s:3:\"wmv\";}}','ITfile','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";}',0,1,'a:2:{s:2:\"nl\";s:51:\"Hier kunt u een afbeelding of flash movie uploaden.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4395,'url',347,40,'url','a:2:{s:2:\"nl\";s:3:\"Url\";s:2:\"en\";s:0:\"\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:55:\"Vul hier de URL in waar de banner naar toe moet linken.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4396,'popup',347,50,'popup','a:2:{s:2:\"nl\";s:13:\"Nieuw Venster\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,0,'a:2:{s:2:\"nl\";s:56:\"Kies hier of de link in een nieuw venster wordt geopend.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4397,'adsense_slot',347,60,'adsense_slot','a:2:{s:2:\"nl\";s:19:\"Google AdSense slot\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:20;s:9:\"maxlength\";s:2:\"20\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:70:\"Geef hier de slot identifier vanuit de Google AdSense javascript code.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4398,'adsense_client',347,70,'adsense_client','a:2:{s:2:\"nl\";s:21:\"Google AdSense client\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:40;s:9:\"maxlength\";s:2:\"40\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:72:\"Geef hier de client identifier vanuit de Google AdSense javascript code.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4399,'analytics',347,130,'analytics','a:2:{s:2:\"nl\";s:16:\"Google Analytics\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:83:\"Voeg code toe om kliks te tellen met Google Analytics (afhankelijk van de template)\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4400,'bgcolor',347,100,'bgcolor','a:2:{s:2:\"nl\";s:16:\"Achtergrondkleur\";s:2:\"en\";s:0:\"\";}','DThexcolor','a:1:{s:7:\"default\";s:0:\"\";}','ITcolor','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:10;s:9:\"maxlength\";s:2:\"10\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:44:\"Kies hier de achtergrondkleur van de banner.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4401,'online',347,110,'online','a:2:{s:2:\"nl\";s:6:\"Online\";s:2:\"en\";s:0:\"\";}','DTdatetime','a:1:{s:7:\"default\";s:16:\"0000-00-00 00:00\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'a:2:{s:2:\"nl\";s:54:\"De banner is zichtbaar vanaf de hier ingestelde datum.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4402,'offline',347,120,'offline','a:2:{s:2:\"nl\";s:7:\"Offline\";s:2:\"en\";s:0:\"\";}','DTdatetime','a:1:{s:7:\"default\";s:0:\"\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',0,1,'a:2:{s:2:\"nl\";s:116:\"De banner is zichtbaar tot de hier ingesteld datum.<br />Als er geen datum is ingevuld blijft deze banner zichtbaar.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4403,'custom_javascript',347,180,'custom_javascript','a:2:{s:2:\"nl\";s:17:\"Custom javascript\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:4:\"5000\";s:7:\"preview\";b:0;s:7:\"counter\";b:1;}',0,0,'a:2:{s:2:\"nl\";s:68:\"Vul hier javascript code in van niet standaard ondersteunde banners.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4404,'prio',347,140,'prio','a:2:{s:2:\"nl\";s:10:\"Prioriteit\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:63:\"SELECT prio, name FROM module_content_banner ORDER BY prio DESC\";}',0,1,'a:2:{s:2:\"nl\";s:28:\"De prioriteit van de banner.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4405,'clickcounter',347,150,'clickcounter','a:2:{s:2:\"nl\";s:12:\"Aantal Kliks\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITreadonly','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:46:\"Het aantal kliks dat met de module is gemeten.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4406,'mod_user',347,160,'mod_user','a:2:{s:2:\"nl\";s:21:\"Laatst gewijzigd door\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:16:\"{$MOD_USER_NAME}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4407,'mod_date',347,170,'mod_date','a:2:{s:2:\"nl\";s:19:\"Laatst gewijzigd op\";s:2:\"en\";s:0:\"\";}','DTdatetime','a:1:{s:7:\"default\";s:11:\"{$MOD_DATE}\";}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4408,'lookup_site_banner',347,190,'lookup_site_banner','a:2:{s:2:\"nl\";s:4:\"Site\";s:2:\"en\";s:0:\"\";}','DTXref','a:4:{s:7:\"src_col\";s:9:\"banner_id\";s:7:\"dst_col\";s:15:\"site_identifier\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:57:\"SELECT identifier, name FROM brickwork_site ORDER BY name\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:25:\"brickwork_site.identifier\";s:10:\"optiontext\";a:1:{i:0;s:19:\"brickwork_site.name\";}s:10:\"searchtext\";a:1:{i:0;s:19:\"brickwork_site.name\";}}',0,0,'a:2:{s:2:\"nl\";s:21:\"Koppel de banner aan \";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4409,'lookup_module_banner_group',347,200,'lookup_module_banner_group','a:2:{s:2:\"nl\";s:5:\"Groep\";s:2:\"en\";s:0:\"\";}','DTXref','a:4:{s:7:\"src_col\";s:9:\"banner_id\";s:7:\"dst_col\";s:8:\"group_id\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:65:\"SELECT id, name FROM module_content_bannergroup ORDER BY name ASC\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:29:\"module_content_bannergroup.id\";s:10:\"optiontext\";a:1:{i:0;s:31:\"module_content_bannergroup.name\";}s:10:\"searchtext\";a:1:{i:0;s:31:\"module_content_bannergroup.name\";}}',0,0,'a:2:{s:2:\"nl\";s:21:\"Koppel de banner aan \";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4410,'image_url',347,35,'image_url','a:2:{s:2:\"nl\";s:14:\"URL Afbeelding\";s:2:\"en\";s:0:\"\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:70:\"Hier kunt u de URL van een externe afbeelding of flash movie invullen.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4411,'width',347,80,'width','a:2:{s:2:\"nl\";s:7:\"Breedte\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:11;s:9:\"maxlength\";s:2:\"11\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:149:\"Geef hier de breedte van de banner op.<br />\r\nDeze instelling is verplicht in geval Google AdSense banners of Flash banners/filmpjes worden gebruikt.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4412,'height',347,90,'height','a:2:{s:2:\"nl\";s:6:\"Hoogte\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:11;s:9:\"maxlength\";s:2:\"11\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:148:\"Geef hier de hoogte van de banner op.<br />\r\nDeze instelling is verplicht in geval Google AdSense banners of Flash banners/filmpjes worden gebruikt.\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4413,'id',348,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:0:\"\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4414,'name',348,20,'name','a:2:{s:2:\"nl\";s:4:\"Naam\";s:2:\"en\";s:0:\"\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4415,'id',349,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:2:\"Id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4416,'group_id',349,20,'group_id','a:2:{s:2:\"nl\";s:14:\"Reaction groep\";s:2:\"en\";s:14:\"Reaction group\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:61:\"SELECT id, title FROM brickwork_reaction_group ORDER BY title\";s:9:\"updatable\";b:0;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:27:\"brickwork_reaction_group.id\";s:10:\"optiontext\";a:1:{i:0;s:30:\"brickwork_reaction_group.title\";}s:10:\"searchtext\";a:1:{i:0;s:30:\"brickwork_reaction_group.title\";}}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4417,'site_user_id',349,30,'site_user_id','a:2:{s:2:\"nl\";s:9:\"Site user\";s:2:\"en\";s:9:\"Site user\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:90:\"SELECT id, username FROM brickwork_site_user [WHERE id = \'{$RECORD_ID}\'] ORDER BY username\";s:9:\"updatable\";b:0;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:22:\"brickwork_site_user.id\";s:10:\"optiontext\";a:1:{i:0;s:28:\"brickwork_site_user.username\";}s:10:\"searchtext\";a:1:{i:0;s:28:\"brickwork_site_user.username\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4418,'active',349,40,'active','a:2:{s:2:\"nl\";s:6:\"Actief\";s:2:\"en\";s:6:\"Active\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4419,'author',349,50,'author','a:2:{s:2:\"nl\";s:6:\"Auteur\";s:2:\"en\";s:6:\"Author\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4420,'email',349,60,'email','a:2:{s:2:\"nl\";s:5:\"Email\";s:2:\"en\";s:5:\"Email\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4421,'body',349,70,'body','a:2:{s:2:\"nl\";s:5:\"Tekst\";s:2:\"en\";s:4:\"Text\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4422,'last_update',349,80,'last_update','a:2:{s:2:\"nl\";s:16:\"Laatst aangepast\";s:2:\"en\";s:11:\"Last update\";}','DTdatetime','a:1:{s:7:\"default\";s:11:\"{$MOD_DATE}\";}','ITreadonly','a:0:{}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4423,'created_on',349,90,'created_on','a:2:{s:2:\"nl\";s:13:\"Aangemaakt op\";s:2:\"en\";s:10:\"Created on\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','ITreadonly','a:0:{}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4424,'ip',349,100,'ip','a:2:{s:2:\"nl\";s:8:\"Ip adres\";s:2:\"en\";s:9:\"Ip adress\";}','DTipaddress','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:13;s:9:\"maxlength\";s:2:\"13\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4425,'post_url',349,110,'post_url','a:2:{s:2:\"nl\";s:7:\"Lokatie\";s:2:\"en\";s:8:\"Location\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:60;s:9:\"maxlength\";s:0:\"\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4426,'photos',349,120,'Photos','a:2:{s:2:\"nl\";s:6:\"Foto\'s\";s:2:\"en\";s:7:\"Photo\'s\";}','DTfake','a:3:{s:9:\"html_code\";s:159:\"<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'imagetid\']?>&site_identifier={$site_identifier}&reaction_id=<?=$RID?>\">Foto\'s</a>\";s:14:\"view_html_code\";s:234:\"<?if(!empty($GLOBALS[\'WEBprefs\'][\'reactions\'][\'imagetid\'])):?>\r\n<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'imagetid\']?>&site_identifier={$site_identifier}&reaction_id=<?=$RID?>\">Foto\'s</a>\r\n<?endif?>\";s:7:\"js_code\";s:0:\"\";}','ITfake',NULL,0,1,'');
INSERT INTO `phoundry_column` VALUES (4427,'youtube',349,130,'Youtube','a:2:{s:2:\"nl\";s:7:\"Video\'s\";s:2:\"en\";s:7:\"Video\'s\";}','DTfake','a:3:{s:9:\"html_code\";s:239:\"<?if(!empty($GLOBALS[\'WEBprefs\'][\'reactions\'][\'youtubetid\'])):?>\r\n<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'youtubetid\']?>&site_identifier={$site_identifier}&reaction_id=<?=$RID?>\">Video\'s</a>\r\n<?endif?>\";s:14:\"view_html_code\";s:239:\"<?if(!empty($GLOBALS[\'WEBprefs\'][\'reactions\'][\'youtubetid\'])):?>\r\n<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'youtubetid\']?>&site_identifier={$site_identifier}&reaction_id=<?=$RID?>\">Video\'s</a>\r\n<?endif?>\";s:7:\"js_code\";s:0:\"\";}','ITfake',NULL,0,1,'');
INSERT INTO `phoundry_column` VALUES (4428,'id',350,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:2:\"Id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4429,'reaction_id',350,20,'reaction_id','a:2:{s:2:\"nl\";s:11:\"Reaction id\";s:2:\"en\";s:11:\"Reaction id\";}','DTinteger','a:2:{s:7:\"default\";s:14:\"{$reaction_id}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4430,'url',350,30,'url','a:2:{s:2:\"nl\";s:3:\"Url\";s:2:\"en\";s:3:\"Url\";}','DTfile','a:5:{s:7:\"savedir\";s:15:\"IMAGES/Reaction\";s:12:\"allow_upload\";b:1;s:13:\"allow_makedir\";b:1;s:17:\"select_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}s:17:\"upload_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}}','ITfile','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4431,'mediatype',350,40,'mediatype','a:2:{s:2:\"nl\";s:9:\"Mediatype\";s:2:\"en\";s:9:\"Mediatype\";}','DTstring','a:3:{s:7:\"default\";s:5:\"image\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4432,'created_datetime',350,50,'created_datetime','a:2:{s:2:\"nl\";s:13:\"Aangemaakt op\";s:2:\"en\";s:16:\"Created datetime\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','IThidden','a:0:{}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4433,'original',350,60,'original','a:2:{s:2:\"nl\";s:23:\"Origineele bestandsnaam\";s:2:\"en\";s:17:\"Original filename\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4434,'id',351,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:2:\"Id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4435,'reaction_id',351,20,'reaction_id','a:2:{s:2:\"nl\";s:11:\"Reaction id\";s:2:\"en\";s:11:\"Reaction id\";}','DTinteger','a:2:{s:7:\"default\";s:14:\"{$reaction_id}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4436,'mediatype',351,30,'mediatype','a:2:{s:2:\"nl\";s:9:\"Mediatype\";s:2:\"en\";s:9:\"Mediatype\";}','DTstring','a:3:{s:7:\"default\";s:7:\"youtube\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4437,'created_datetime',351,40,'created_datetime','a:2:{s:2:\"nl\";s:13:\"Aangemaakt op\";s:2:\"en\";s:16:\"Created datetime\";}','DTdatetime','a:1:{s:7:\"default\";s:0:\"\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4438,'original',351,50,'original','a:2:{s:2:\"nl\";s:3:\"Url\";s:2:\"en\";s:3:\"Url\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4439,'id',353,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4440,'relation_name',353,20,'relation_name','Relation name','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4441,'title',353,30,'title','Titel','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITreadonly','a:0:{}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4442,'reaction_count',353,40,'reaction_count','Reacties','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITreadonly','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4443,'last_update',353,50,'last_update','Laatste wijziging','DTdatetime','a:1:{s:7:\"default\";s:17:\"CURRENT_TIMESTAMP\";}','ITreadonly','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4444,'reactions',353,60,'reactions','a:2:{s:2:\"nl\";s:8:\"Reacties\";s:2:\"en\";s:9:\"Reactions\";}','DTfake','a:3:{s:9:\"html_code\";s:327:\"<?if(!empty($GLOBALS[\'WEBprefs\'][\'reactions\'][\'reactiontid\'])):?>\r\n<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'reactiontid\']?>&site_identifier={$site_identifier}&group_id=<?= $RID ?>\"><img src=\"/phoundry/core/icons/application_side_expand.png\" alt=\"bekijk reacties\" border=\"0\"/></a>\r\n<?endif?>\";s:14:\"view_html_code\";s:249:\"<?if(!empty($GLOBALS[\'WEBprefs\'][\'reactions\'][\'reactiontid\'])):?>\r\n<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'reactiontid\']?>&site_identifier={$site_identifier}&group_id=<?= $RID ?>\">Ga naar reacties</a>\r\n<?endif?>\";s:7:\"js_code\";s:0:\"\";}','ITfake',NULL,0,1,'');
INSERT INTO `phoundry_column` VALUES (4445,'closed',353,70,'closed','Gesloten voor reacties','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4446,'page_content_id',354,10,'page_content_id','Page content id','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4447,'items_per_page',354,20,'items_per_page','Aantal getoonde items','DTinteger','a:2:{s:7:\"default\";s:1:\"8\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:3;s:9:\"maxlength\";s:1:\"3\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (6029,'detail_page_id',354,30,'detail_page_id','a:2:{s:2:\"nl\";s:13:\"Detail Pagina\";s:2:\"en\";s:11:\"Detail Page\";}','DTsiteStructure','a:1:{s:7:\"default\";s:0:\"\";}','ITsiteStructure','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4449,'behavior',354,60,'behavior','a:2:{s:2:\"nl\";s:8:\"Weergave\";s:2:\"en\";s:8:\"Weergave\";}','DTstring','a:3:{s:7:\"default\";s:6:\"normal\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:24:\"mini=Mini|normal=Normaal\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4450,'site_level',354,70,'site_level','Site level','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4451,'event_type_ids',354,50,'event_type_ids','Toon volgende evenement soorten','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITmultipleselect','a:5:{s:5:\"query\";s:112:\"SELECT id, name FROM module_content_agenda_event_type WHERE site_identifier = \'{$site_identifier}\' ORDER BY name\";s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4452,'id',355,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4453,'lookup_module_agenda_site',355,5,'lookup_module_agenda_site','Toon op site:','DTXref','a:4:{s:7:\"src_col\";s:9:\"agenda_id\";s:7:\"dst_col\";s:15:\"site_identifier\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:4:\"site\";}','ITXduallist','a:6:{s:5:\"query\";s:61:\"select identifier, name from brickwork_site order by name asc\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:10:\"identifier\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4454,'title',355,30,'title','Titel','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4455,'location',355,40,'location','Locatie','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4456,'target_audience',355,50,'target_audience','Voor wie','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4457,'organisation',355,60,'organisation','Organisatie','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4458,'address',355,70,'address','Adres','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4459,'intro',355,80,'intro','Inleiding','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:5:{s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4460,'description',355,90,'description','a:2:{s:2:\"nl\";s:12:\"Omschrijving\";s:2:\"en\";s:12:\"Omschrijving\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:6:\"IMAGES\";s:9:\"attachDir\";s:5:\"FILES\";s:6:\"config\";s:2:\"{}\";}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4461,'from_date',355,100,'from_date','a:2:{s:2:\"nl\";s:5:\"Vanaf\";s:2:\"en\";s:5:\"Vanaf\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'a:2:{s:2:\"nl\";s:66:\"Als de tijd op 00:00 staat zal deze niet op de site getoond worden\";s:2:\"en\";s:66:\"Als de tijd op 00:00 staat zal deze niet op de site getoond worden\";}');
INSERT INTO `phoundry_column` VALUES (4462,'till_date',355,110,'till_date','a:2:{s:2:\"nl\";s:3:\"Tot\";s:2:\"en\";s:3:\"Tot\";}','DTdatetime','a:1:{s:7:\"default\";s:11:\"{$TODAY+1D}\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'a:2:{s:2:\"nl\";s:102:\"Vul hier dezelfde datum in als bij \"vanaf\" om geen \"tot datum\" te tonen. Ditzelfde geldt voor de tijd.\";s:2:\"en\";s:102:\"Vul hier dezelfde datum in als bij \"vanaf\" om geen \"tot datum\" te tonen. Ditzelfde geldt voor de tijd.\";}');
INSERT INTO `phoundry_column` VALUES (4463,'create_date',355,120,'create_date','Aanmaak datum','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4464,'event_type_id',355,150,'event_type_id','Soort evenement','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:6:{s:5:\"query\";s:112:\"SELECT id, name FROM module_content_agenda_event_type WHERE site_identifier = \'{$site_identifier}\' ORDER BY name\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:141;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4465,'offline_date',355,140,'offline_date','Offline datum','DTdatetime','a:1:{s:7:\"default\";s:11:\"{$TODAY+1M}\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4466,'online_date',355,130,'online_date','Online datum','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4467,'additional_organisation',355,65,'additional_organisation','Mede organisatoren','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4468,'image',355,35,'image','a:2:{s:2:\"nl\";s:10:\"Afbeelding\";s:2:\"en\";s:5:\"Image\";}','DTfile','a:5:{s:7:\"savedir\";s:18:\"IMAGES/htmlcontent\";s:12:\"allow_upload\";b:1;s:13:\"allow_makedir\";b:1;s:17:\"select_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}s:17:\"upload_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}}','ITfile','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4469,'site_identifier',356,30,'site_identifier','Site identifier','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4470,'id',356,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4471,'name',356,20,'name','Naam','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"63\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4503,'id',362,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4504,'site_identifier',362,20,'site_identifier','Site identifier','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4505,'category',362,30,'category','a:2:{s:2:\"nl\";s:12:\"Naam rubriek\";s:2:\"en\";s:4:\"Naam\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"100\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4506,'prio',362,40,'prio','Prio','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:73:\"SELECT prio, category FROM module_content_faq_category ORDER BY prio DESC\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4507,'category_image',362,50,'category_image','Rubriek afbeelding','DTfile','a:5:{s:7:\"savedir\";s:15:\"IMAGES/BANNERS/\";s:12:\"allow_upload\";b:1;s:13:\"allow_makedir\";b:0;s:17:\"select_extensions\";a:5:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";i:4;s:3:\"swf\";}s:17:\"upload_extensions\";a:5:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";i:4;s:3:\"swf\";}}','ITfile','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4508,'description',362,60,'description','Rubriek beschrijving','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4509,'id',363,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4510,'site_identifier',363,20,'site_identifier','Site identifier','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4511,'category_id',363,45,'category_id','a:2:{s:2:\"nl\";s:7:\"Rubriek\";s:2:\"en\";s:11:\"Category id\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:92:\"SELECT id, category FROM module_content_faq_category [WHERE id=\'{$RECORD_ID}\'] ORDER BY prio\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:30:\"module_content_faq_category.id\";s:10:\"optiontext\";a:1:{i:0;s:36:\"module_content_faq_category.category\";}s:10:\"searchtext\";a:1:{i:0;s:36:\"module_content_faq_category.category\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4512,'times_showed',363,40,'times_showed','Aantal keer getoond','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITreadonly','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4513,'question',363,50,'question','Vraag','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:0:\"\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4514,'answer',363,60,'answer','a:2:{s:2:\"nl\";s:8:\"Antwoord\";s:2:\"en\";s:8:\"Antwoord\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:6:\"IMAGES\";s:9:\"attachDir\";s:5:\"FILES\";s:6:\"config\";s:2:\"{}\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4515,'prio',363,70,'prio','Prio','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:72:\"SELECT prio, question FROM module_content_faq_article ORDER BY prio DESC\";}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4516,'page_content_id',364,10,'page_content_id','Page content id','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4517,'sort_by',364,20,'sort_by','Gesorteerd op','DTstring','a:3:{s:7:\"default\";s:8:\"alphabet\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:4:{s:5:\"query\";s:43:\"alphabet=alphabet|prio=prio|popular=popular\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4518,'number_showed_questions',364,30,'number_showed_questions','Aantal getoonde vragen in overzicht','DTinteger','a:2:{s:7:\"default\";s:2:\"10\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:3;s:9:\"maxlength\";s:1:\"3\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4519,'category_id',364,40,'category_id','Categorie','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:139:\"SELECT id, category FROM module_content_faq_category WHERE site_identifier = \'{$site_identifier}\' [AND id = {$RECORD_ID}] ORDER BY category\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:30:\"module_content_faq_category.id\";s:10:\"optiontext\";a:1:{i:0;s:36:\"module_content_faq_category.category\";}s:10:\"searchtext\";a:1:{i:0;s:36:\"module_content_faq_category.category\";}}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4520,'page_content_id',365,10,'page_content_id','Page content id','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4521,'link_title_sort_id',365,20,'link_title_sort_id','a:2:{s:2:\"nl\";s:10:\"Sorteer op\";s:2:\"en\";s:10:\"Sorteer op\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:77:\"0=Niet|1=Alfabetisch Oplopend [a-Z]|2=Alfabetisch Aflopend [Z-a]|3=Prioriteit\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:5:\"title\";}s:10:\"searchtext\";a:1:{i:0;s:5:\"title\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4522,'category_title_sort_id',365,30,'category_title_sort_id','a:2:{s:2:\"nl\";s:16:\"Rubriek sorteren\";s:2:\"en\";s:16:\"Rubriek sorteren\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:77:\"0=Niet|1=Alfabetisch Oplopend [a-Z]|2=Alfabetisch Aflopend [Z-a]|3=Prioriteit\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:5:\"title\";}s:10:\"searchtext\";a:1:{i:0;s:5:\"title\";}}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4523,'lookup_module_config_link_category',365,40,'lookup_module_config_link_category','Selecteer de te tonen categori','DTXref','a:4:{s:7:\"src_col\";s:15:\"page_content_id\";s:7:\"dst_col\";s:11:\"category_id\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:65:\"SELECT id, title FROM module_content_link_category ORDER BY title\";s:8:\"addTable\";i:132;s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:5:\"title\";}s:10:\"searchtext\";a:1:{i:0;s:5:\"title\";}}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4524,'id',366,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4525,'link',366,20,'link','Url','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"250\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4526,'title',366,30,'title','Titel','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:50;s:9:\"maxlength\";s:2:\"50\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4527,'description',366,40,'description','Omschrijving','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:5:{s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:3:\"100\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4528,'prio',366,50,'prio','Prio','DTfloat','a:1:{s:7:\"default\";s:1:\"1\";}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:62:\"SELECT prio, title FROM module_content_link ORDER BY prio DESC\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4529,'lookup_module_content_link_category',366,60,'lookup_module_content_link_category','Link, rubriek','DTXref','a:4:{s:7:\"src_col\";s:7:\"link_id\";s:7:\"dst_col\";s:11:\"category_id\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXduallist','a:6:{s:5:\"query\";s:65:\"SELECT id, title FROM module_content_link_category ORDER BY title\";s:8:\"addTable\";i:132;s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:5:\"title\";}s:10:\"searchtext\";a:1:{i:0;s:5:\"title\";}}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4530,'target',366,70,'target','Openen in','DTstring','a:3:{s:7:\"default\";s:5:\"_self\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:3:{s:5:\"query\";s:41:\"_self=Zelfde venster|_blank=Nieuw venster\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4531,'id',367,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4532,'title',367,20,'title','Naam','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:50;s:9:\"maxlength\";s:2:\"50\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4533,'prio',367,30,'prio','Prio','DTfloat','a:1:{s:7:\"default\";s:1:\"1\";}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:71:\"SELECT prio, title FROM module_content_link_category ORDER BY prio DESC\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4534,'id',368,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:2:\"Id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4535,'relation_name',368,20,'relation_name','a:2:{s:2:\"nl\";s:12:\"Relatie naam\";s:2:\"en\";s:13:\"Relation name\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4536,'enabled',368,30,'enabled','a:2:{s:2:\"nl\";s:6:\"Actief\";s:2:\"en\";s:7:\"Enabled\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:45:\"Zijn reacties voor deze groepen ingeschakeld.\";s:2:\"en\";s:39:\"Are reactions enabled for these groups.\";}');
INSERT INTO `phoundry_column` VALUES (4537,'order_by',368,40,'order_by','a:2:{s:2:\"nl\";s:8:\"Volgorde\";s:2:\"en\";s:5:\"Order\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:21:\"0=Oplopend|1=Aflopend\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (6020,'name',379,20,'name','Naam','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (6021,'parent_id',379,30,'parent_id','a:2:{s:2:\"nl\";s:23:\"Bovenliggende categorie\";s:2:\"en\";s:23:\"Bovenliggende categorie\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtree','a:5:{s:5:\"query\";s:66:\"SELECT id, parent_id, name FROM module_news_category ORDER BY prio\";s:11:\"optionvalue\";s:2:\"id\";s:6:\"parent\";s:9:\"parent_id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (6019,'id',379,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (6018,'description',379,40,'description','a:2:{s:2:\"nl\";s:12:\"Beschrijving\";s:2:\"en\";s:11:\"Description\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:7:\"IMAGES/\";s:9:\"attachDir\";s:6:\"FILES/\";s:6:\"config\";s:2:\"{}\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4543,'author',370,80,'author','Auteur','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4544,'image',370,90,'image','Afbeelding','DTfile','a:5:{s:7:\"savedir\";s:11:\"IMAGES/news\";s:12:\"allow_upload\";b:1;s:13:\"allow_makedir\";b:1;s:17:\"select_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}s:17:\"upload_extensions\";a:4:{i:0;s:3:\"gif\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"png\";}}','ITfile','a:2:{s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";}',0,0,'');
INSERT INTO `phoundry_column` VALUES (4545,'intro',370,100,'intro','a:2:{s:2:\"nl\";s:5:\"Intro\";s:2:\"en\";s:5:\"Intro\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:7:\"IMAGES/\";s:9:\"attachDir\";s:6:\"FILES/\";s:6:\"config\";s:2:\"{}\";}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4546,'body',370,110,'body','a:2:{s:2:\"nl\";s:6:\"Inhoud\";s:2:\"en\";s:4:\"Body\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtiny_mce','a:6:{s:9:\"updatable\";b:1;s:5:\"width\";s:4:\"100%\";s:6:\"height\";s:5:\"400px\";s:8:\"imageDir\";s:7:\"IMAGES/\";s:9:\"attachDir\";s:6:\"FILES/\";s:6:\"config\";s:2:\"{}\";}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4547,'title',370,70,'title','Titel','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4548,'online_datetime',370,50,'online_datetime','a:2:{s:2:\"nl\";s:12:\"Online vanaf\";s:2:\"en\";s:11:\"Online date\";}','DTdatetime','a:1:{s:7:\"default\";s:0:\"\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4549,'offline_datetime',370,60,'offline_datetime','a:2:{s:2:\"nl\";s:13:\"Offline vanaf\";s:2:\"en\";s:12:\"Offline date\";}','DTdatetime','a:1:{s:7:\"default\";s:0:\"\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4550,'create_datetime',370,40,'create_datetime','Aanmaak datum','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4551,'category_id',370,15,'module_news_article_category','a:2:{s:2:\"nl\";s:12:\"Categorieën\";s:2:\"en\";s:10:\"Categories\";}','DTXref','a:4:{s:7:\"src_col\";s:10:\"article_id\";s:7:\"dst_col\";s:11:\"category_id\";s:8:\"ordr_col\";s:0:\"\";s:7:\"default\";s:0:\"\";}','ITXrecursivemultipleselect','a:7:{s:5:\"query\";s:66:\"SELECT id, parent_id, name FROM module_news_category ORDER BY name\";s:8:\"addTable\";i:0;s:4:\"size\";i:6;s:11:\"optionvalue\";s:2:\"id\";s:6:\"parent\";s:9:\"parent_id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4552,'archivable',370,30,'archivable','Archiveerbaar','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'Bericht in het archief plaatsen nadat de publicatie tijd verstreken is');
INSERT INTO `phoundry_column` VALUES (4553,'id',370,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4554,'reactions',370,32,'reactions','Reacties toegestaan','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4555,'items_pp',371,50,'items_pp','a:2:{s:2:\"nl\";s:16:\"Items per pagina\";s:2:\"en\";s:14:\"Items per page\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:2;s:9:\"maxlength\";s:1:\"2\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4556,'detail_id',371,40,'detail_id','a:2:{s:2:\"nl\";s:13:\"Detail Pagina\";s:2:\"en\";s:13:\"Detail Pagina\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:6:{s:5:\"query\";s:421:\"SELECT brickwork_site_structure.id, brickwork_site_structure.name\r\nFROM module_config_news\r\nINNER JOIN brickwork_page_content\r\nON module_config_news.page_content_id = brickwork_page_content.id\r\nINNER JOIN brickwork_page\r\nON brickwork_page_content.page_id = brickwork_page.id\r\nINNER JOIN brickwork_site_structure\r\nON brickwork_page.site_structure_id = brickwork_site_structure.id\r\nWHERE\r\nmodule_config_news.view = \'detail\'\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4557,'root_category',371,20,'root_category','a:2:{s:2:\"nl\";s:9:\"Categorie\";s:2:\"en\";s:9:\"Categorie\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtree','a:5:{s:5:\"query\";s:66:\"SELECT id, parent_id, name FROM module_news_category ORDER BY prio\";s:11:\"optionvalue\";s:2:\"id\";s:6:\"parent\";s:9:\"parent_id\";s:10:\"optiontext\";a:1:{i:0;s:4:\"name\";}s:10:\"searchtext\";a:1:{i:0;s:4:\"name\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4558,'view',371,30,'view','View','DTstring','a:3:{s:7:\"default\";s:4:\"mini\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITselect','a:3:{s:5:\"query\";s:23:\"mini=Mini|detail=Detail\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4559,'page_content_id',371,10,'page_content_id','Page content id','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4560,'reactions',371,60,'reactions','a:2:{s:2:\"nl\";s:19:\"Reacties toegestaan\";s:2:\"en\";s:19:\"Reacties toegestaan\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4561,'type',371,35,'type','Type','DTinteger','a:2:{s:7:\"default\";s:1:\"1\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:28:\"1=Recent|2=Archief|3=Gemixed\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4598,'media',368,60,'media','a:2:{s:2:\"nl\";s:24:\"Hoeveelheid afbeeldingen\";s:2:\"en\";s:24:\"Maximum amount of images\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:11;s:9:\"maxlength\";s:2:\"11\";s:7:\"counter\";b:0;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4597,'mediatypes',368,50,'mediatypes','a:2:{s:2:\"nl\";s:10:\"Mediatypes\";s:2:\"en\";s:10:\"Mediatypes\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITmultipleselect','a:2:{s:5:\"query\";s:34:\"youtube=Youtube|image=Afbeeldingen\";s:4:\"size\";i:6;}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4596,'url',377,110,'url','Url','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITreadonly','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4594,'ip',377,50,'ip','Ip','DTipaddress','a:1:{s:7:\"default\";s:0:\"\";}','ITreadonly','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4595,'created_on',377,100,'created_on','Gemeld op','DTdatetime','a:1:{s:7:\"default\";s:0:\"\";}','ITcalendar','a:3:{s:9:\"updatable\";b:1;s:4:\"size\";i:16;s:9:\"maxlength\";s:2:\"16\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4569,'page_content_id',373,10,'page_content_id','Page content id','DTinteger','a:2:{s:7:\"default\";s:6:\"{$RID}\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4570,'poll_id',373,20,'poll_id','Poll','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:6:{s:5:\"query\";s:143:\"select id, question from module_content_poll_question WHERE site_identifier=\'{$site_identifier}\' [ AND id = {$RECORD_ID}] order by question asc\";s:9:\"updatable\";b:1;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:8:\"question\";}s:10:\"searchtext\";a:1:{i:0;s:8:\"question\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4571,'id',374,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4572,'question',374,20,'question','Vraag','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4573,'site_identifier',374,30,'site_identifier','Site identifier','DTstring','a:3:{s:7:\"default\";s:18:\"{$site_identifier}\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4574,'visible',374,40,'visible','Zichtbaar','DTstring','a:3:{s:7:\"default\";s:2:\"no\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradio','a:2:{s:5:\"query\";s:26:\"yes={$check}|no={$nocheck}\";s:9:\"updatable\";b:1;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4575,'show_result',374,50,'show_result','Toon het resultaat','DTstring','a:3:{s:7:\"default\";s:2:\"no\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITradio','a:3:{s:5:\"query\";s:26:\"yes={$check}|no={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4576,'id',375,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4577,'poll_id',375,20,'poll_id','Poll','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:6:{s:5:\"query\";s:138:\"select id,question from module_content_poll_question WHERE site_identifier=\'{$site_identifier}\' [AND id = {$RECORD_ID}]  order by question\";s:9:\"updatable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:2:\"id\";s:10:\"optiontext\";a:1:{i:0;s:8:\"question\";}s:10:\"searchtext\";a:1:{i:0;s:8:\"question\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4578,'answer',375,30,'answer','Antwoord','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4579,'prio',375,40,'prio','Prioriteit','DTfloat','a:1:{s:7:\"default\";s:6:\"100.00\";}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:212:\"select prio, answer from module_content_poll_answer pa inner join module_content_poll_question pq on pa.poll_id=pq.id and pq.site_identifier =\'{$site_identifier}\' and pa.poll_id = {$PHfltr_401} order by prio desc\";}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4580,'id',376,10,'id','Id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (4581,'answer_id',376,20,'answer_id','Antwoord','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:92:\"SELECT id, answer FROM module_content_poll_answer  [WHERE id = {$RECORD_ID}] ORDER BY answer\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:29:\"module_content_poll_answer.id\";s:10:\"optiontext\";a:1:{i:0;s:33:\"module_content_poll_answer.answer\";}s:10:\"searchtext\";a:1:{i:0;s:33:\"module_content_poll_answer.answer\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4582,'ipaddress',376,30,'ipaddress','IP','DTipaddress','a:1:{s:7:\"default\";s:0:\"\";}','ITreadonly','a:0:{}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4583,'browser_hash',376,40,'browser_hash','Browser checksum','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITreadonly','a:0:{}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4584,'poll_id',376,5,'poll_id','Poll id','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:98:\"SELECT id, question FROM module_content_poll_question  [WHERE id = {$RECORD_ID}] ORDER BY question\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:31:\"module_content_poll_question.id\";s:10:\"optiontext\";a:1:{i:0;s:37:\"module_content_poll_question.question\";}s:10:\"searchtext\";a:1:{i:0;s:37:\"module_content_poll_question.question\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (4585,'vote_date',376,60,'vote_date','Stem datum','DTdatetime','a:1:{s:7:\"default\";s:0:\"\";}','ITreadonly','a:0:{}',0,1,'');
INSERT INTO `phoundry_column` VALUES (4599,'id',378,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:2:\"Id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4600,'group_id',378,20,'group_id','a:2:{s:2:\"nl\";s:14:\"Reaction groep\";s:2:\"en\";s:14:\"Reaction group\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:61:\"SELECT id, title FROM brickwork_reaction_group ORDER BY title\";s:9:\"updatable\";b:0;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:27:\"brickwork_reaction_group.id\";s:10:\"optiontext\";a:1:{i:0;s:30:\"brickwork_reaction_group.title\";}s:10:\"searchtext\";a:1:{i:0;s:30:\"brickwork_reaction_group.title\";}}',1,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4601,'site_user_id',378,30,'site_user_id','a:2:{s:2:\"nl\";s:9:\"Site user\";s:2:\"en\";s:9:\"Site user\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:90:\"SELECT id, username FROM brickwork_site_user [WHERE id = \'{$RECORD_ID}\'] ORDER BY username\";s:9:\"updatable\";b:0;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:22:\"brickwork_site_user.id\";s:10:\"optiontext\";a:1:{i:0;s:28:\"brickwork_site_user.username\";}s:10:\"searchtext\";a:1:{i:0;s:28:\"brickwork_site_user.username\";}}',0,0,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4602,'active',378,40,'active','a:2:{s:2:\"nl\";s:6:\"Actief\";s:2:\"en\";s:6:\"Active\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"1={$check}|0={$nocheck}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4603,'author',378,50,'author','a:2:{s:2:\"nl\";s:6:\"Auteur\";s:2:\"en\";s:6:\"Author\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4604,'email',378,60,'email','a:2:{s:2:\"nl\";s:5:\"Email\";s:2:\"en\";s:5:\"Email\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4605,'body',378,70,'body','a:2:{s:2:\"nl\";s:5:\"Tekst\";s:2:\"en\";s:4:\"Text\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtextarea','a:6:{s:9:\"updatable\";b:1;s:4:\"cols\";s:2:\"60\";s:4:\"rows\";s:2:\"10\";s:9:\"maxlength\";s:0:\"\";s:7:\"preview\";b:0;s:7:\"counter\";b:0;}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4606,'last_update',378,80,'last_update','a:2:{s:2:\"nl\";s:16:\"Laatst aangepast\";s:2:\"en\";s:11:\"Last update\";}','DTdatetime','a:1:{s:7:\"default\";s:11:\"{$MOD_DATE}\";}','ITreadonly','a:0:{}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4607,'created_on',378,90,'created_on','a:2:{s:2:\"nl\";s:13:\"Aangemaakt op\";s:2:\"en\";s:10:\"Created on\";}','DTdatetime','a:1:{s:7:\"default\";s:8:\"{$TODAY}\";}','ITreadonly','a:0:{}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4608,'ip',378,100,'ip','a:2:{s:2:\"nl\";s:8:\"Ip adres\";s:2:\"en\";s:9:\"Ip adress\";}','DTipaddress','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:13;s:9:\"maxlength\";s:2:\"13\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4609,'post_url',378,110,'post_url','a:2:{s:2:\"nl\";s:7:\"Lokatie\";s:2:\"en\";s:8:\"Location\";}','DTurl','a:1:{s:7:\"default\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:0;s:4:\"size\";i:60;s:9:\"maxlength\";s:0:\"\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (4610,'photos',378,120,'Photos','a:2:{s:2:\"nl\";s:6:\"Foto\'s\";s:2:\"en\";s:7:\"Photo\'s\";}','DTfake','a:3:{s:9:\"html_code\";s:159:\"<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'imagetid\']?>&site_identifier={$site_identifier}&reaction_id=<?=$RID?>\">Foto\'s</a>\";s:14:\"view_html_code\";s:234:\"<?if(!empty($GLOBALS[\'WEBprefs\'][\'reactions\'][\'imagetid\'])):?>\r\n<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'imagetid\']?>&site_identifier={$site_identifier}&reaction_id=<?=$RID?>\">Foto\'s</a>\r\n<?endif?>\";s:7:\"js_code\";s:0:\"\";}','ITfake',NULL,0,1,'');
INSERT INTO `phoundry_column` VALUES (4611,'youtube',378,130,'Youtube','a:2:{s:2:\"nl\";s:7:\"Video\'s\";s:2:\"en\";s:7:\"Video\'s\";}','DTfake','a:3:{s:9:\"html_code\";s:239:\"<?if(!empty($GLOBALS[\'WEBprefs\'][\'reactions\'][\'youtubetid\'])):?>\r\n<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'youtubetid\']?>&site_identifier={$site_identifier}&reaction_id=<?=$RID?>\">Video\'s</a>\r\n<?endif?>\";s:14:\"view_html_code\";s:239:\"<?if(!empty($GLOBALS[\'WEBprefs\'][\'reactions\'][\'youtubetid\'])):?>\r\n<a href=\"/phoundry/core/records.php?TID=<?=$GLOBALS[\'WEBprefs\'][\'reactions\'][\'youtubetid\']?>&site_identifier={$site_identifier}&reaction_id=<?=$RID?>\">Video\'s</a>\r\n<?endif?>\";s:7:\"js_code\";s:0:\"\";}','ITfake',NULL,0,1,'');
INSERT INTO `phoundry_column` VALUES (40,'string_id',6,15,'string_id','a:2:{s:2:\"nl\";s:9:\"String id\";s:2:\"en\";s:9:\"String id\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (41,'string_id',4,15,'string_id','a:2:{s:2:\"nl\";s:9:\"String id\";s:2:\"en\";s:9:\"String id\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (42,'string_id',5,15,'string_id','a:2:{s:2:\"nl\";s:9:\"String id\";s:2:\"en\";s:9:\"String id\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (6017,'prio',379,50,'prio','a:2:{s:2:\"nl\";s:10:\"Prioriteit\";s:2:\"en\";s:10:\"Prioriteit\";}','DTfloat','a:1:{s:7:\"default\";s:0:\"\";}','ITpriority','a:2:{s:5:\"order\";s:4:\"desc\";s:5:\"query\";s:62:\"SELECT prio, name FROM module_news_category ORDER BY prio DESC\";}',0,1,'a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}');
INSERT INTO `phoundry_column` VALUES (6028,'default_lang_id',228,50,'default_lang_id','a:2:{s:2:\"nl\";s:15:\"Default lang id\";s:2:\"en\";s:15:\"Default lang id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:102:\"SELECT id, CONCAT(code, \" (\", site_identifier, \")\") AS name FROM brickwork_site_language ORDER BY name\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:26:\"brickwork_site_language.id\";s:10:\"optiontext\";a:1:{i:0;s:5:\".name\";}s:10:\"searchtext\";a:1:{i:0;s:5:\".name\";}}',0,0,'');
INSERT INTO `phoundry_column` VALUES (6030,'id',382,10,'id','a:2:{s:2:\"nl\";s:2:\"Id\";s:2:\"en\";s:2:\"Id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','IThidden','a:0:{}',1,0,'');
INSERT INTO `phoundry_column` VALUES (6031,'soap_id',382,20,'soap_id','a:2:{s:2:\"nl\";s:4:\"Soap\";s:2:\"en\";s:4:\"Soap\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITselect','a:7:{s:5:\"query\";s:89:\"SELECT id, campaign_title FROM brickwork_dmdelivery_soap_campaign ORDER BY campaign_title\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;s:8:\"addTable\";i:0;s:11:\"optionvalue\";s:37:\"brickwork_dmdelivery_soap_campaign.id\";s:10:\"optiontext\";a:1:{i:0;s:49:\"brickwork_dmdelivery_soap_campaign.campaign_title\";}s:10:\"searchtext\";a:1:{i:0;s:49:\"brickwork_dmdelivery_soap_campaign.campaign_title\";}}',1,1,'');
INSERT INTO `phoundry_column` VALUES (6032,'dmd_fieldname',382,30,'dmd_fieldname','a:2:{s:2:\"nl\";s:13:\"Dmd fieldname\";s:2:\"en\";s:13:\"Dmd fieldname\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (6033,'user_fieldname',382,40,'user_fieldname','a:2:{s:2:\"nl\";s:14:\"User fieldname\";s:2:\"en\";s:14:\"User fieldname\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"255\";s:7:\"counter\";b:0;}',0,1,'');
INSERT INTO `phoundry_column` VALUES (6034,'fieldtype',382,50,'fieldtype','a:2:{s:2:\"nl\";s:9:\"Fieldtype\";s:2:\"en\";s:9:\"Fieldtype\";}','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:31;s:9:\"maxlength\";s:2:\"31\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (6035,'combined_unique',382,60,'combined_unique','a:2:{s:2:\"nl\";s:15:\"Combined unique\";s:2:\"en\";s:15:\"Combined unique\";}','DTinteger','a:2:{s:7:\"default\";s:1:\"0\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITradio','a:3:{s:5:\"query\";s:23:\"0={$nocheck}|1={$check}\";s:9:\"updatable\";b:1;s:9:\"togglable\";b:0;}',1,0,'');
INSERT INTO `phoundry_column` VALUES (6043,'optin_mailing_id',269,80,'optin_mailing_id','a:2:{s:2:\"nl\";s:16:\"Optin mailing id\";s:2:\"en\";s:16:\"Optin mailing id\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:11;s:9:\"maxlength\";s:2:\"11\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (6044,'dmd_subgroup',269,90,'dmd_subgroup','a:2:{s:2:\"nl\";s:13:\"Aanmeld groep\";s:2:\"en\";s:15:\"Subscribe group\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:11;s:9:\"maxlength\";s:2:\"11\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (6045,'dmd_unsubgroup',269,100,'dmd_unsubgroup','a:2:{s:2:\"nl\";s:12:\"Afmeld groep\";s:2:\"en\";s:17:\"Unsubscribe group\";}','DTinteger','a:2:{s:7:\"default\";s:0:\"\";s:5:\"range\";a:2:{i:0;s:1:\"0\";i:1;s:1:\"*\";}}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:11;s:9:\"maxlength\";s:2:\"11\";s:7:\"counter\";b:0;}',0,0,'');
INSERT INTO `phoundry_column` VALUES (6047,'name',383,10,'name','a:2:{s:2:\"nl\";s:13:\"attibuut naam\";s:2:\"en\";s:14:\"attribute name\";}','DTstring','a:3:{s:7:\"default\";s:5:\"_page\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:2:\"80\";s:7:\"counter\";b:0;}',1,1,'a:2:{s:2:\"nl\";s:41:\"Attribuut naam dient te eindigen op _page\";s:2:\"en\";s:36:\"Attribute name should end with _page\";}');
INSERT INTO `phoundry_column` VALUES (6048,'title',383,20,'title','Omschrijving','DTstring','a:3:{s:7:\"default\";s:0:\"\";s:7:\"display\";s:0:\"\";s:8:\"encoding\";s:0:\"\";}','ITtext','a:4:{s:9:\"updatable\";b:1;s:4:\"size\";i:60;s:9:\"maxlength\";s:3:\"100\";s:7:\"counter\";b:0;}',1,3,'');
/*!40000 ALTER TABLE `phoundry_column` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_event` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `string_id` varchar(80) default NULL,
  `table_id` int(11) NOT NULL default '0',
  `event` char(40) NOT NULL default '',
  `code` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `string_id` (`string_id`,`table_id`)
) ENGINE=MyISAM AUTO_INCREMENT=416 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_event` WRITE;
/*!40000 ALTER TABLE `phoundry_event` DISABLE KEYS */;
INSERT INTO `phoundry_event` VALUES (1,'pre_insert',1,'pre-insert','// Username should not start with su.:\nif (preg_match(\'/^su\\./i\', $_POST[\'username\'])) {\n   return word(238);\n}');
INSERT INTO `phoundry_event` VALUES (2,'pre_copy',1,'pre-copy','// Username should not start with su.:\nif (preg_match(\'/^su\\./i\', $_POST[\'username\'])) {\n   return word(238);\n}\n');
INSERT INTO `phoundry_event` VALUES (3,'pre_update',1,'pre-update','// Username should not start with su.:\nif (preg_match(\'/^su\\./i\', $_POST[\'username\'])) {\n	return word(238);\n}');
INSERT INTO `phoundry_event` VALUES (4,'pre_delete',1,'pre-delete','$sqls = array();\n$sqls[] = \"DELETE FROM phoundry_notify WHERE user_id = $keyVal\";\n$sqls[] = \"DELETE FROM phoundry_lock WHERE user_id = $keyVal\";\n$sqls[] = \"DELETE FROM phoundry_user_group WHERE user_id = $keyVal\";\n$sqls[] = \"DELETE FROM phoundry_user_shortcut WHERE user_id = $keyVal\";\nforeach($sqls as $sql) {\n   $cur = $db->Query($sql)\n      or trigger_error(\"Query $sql failed: \" . $db->Error(), E_USER_ERROR);\n}\n     ');
INSERT INTO `phoundry_event` VALUES (8,'pre_delete',2,'pre-delete','// Remove everything associated with this role:\n$sqls = array();\n$sqls[] = \"DELETE FROM phoundry_group_column WHERE group_id = $keyVal\";\n$sqls[] = \"DELETE FROM phoundry_group_table WHERE group_id = $keyVal\";\n$sqls[] = \"DELETE FROM phoundry_user_group WHERE group_id = $keyVal\";\nforeach($sqls as $sql) {\n   $cur = $db->Query($sql)\n      or trigger_error(\"Query $sql failed: \" . $db->Error(), E_USER_ERROR);\n}\n     ');
INSERT INTO `phoundry_event` VALUES (9,'post_copy',2,'post-copy','$sqls = array();\n\n// Copy the table rights:\n$sql = \"SELECT table_id, rights FROM phoundry_group_table WHERE group_id = $oldKeyVal\";\n$cur = $db->Query($sql)\n   or trigger_error(\"Query $sql failed: \" . $db->Error(), E_USER_ERROR);\nfor ($x = 0; !$db->EndOfResult($cur); $x++) {\n   $sqls[] = \"UPDATE phoundry_group_table SET rights = \'\" . $db->FetchResult($cur,$x,\'rights\') . \"\' WHERE group_id = $keyVal AND table_id = \" . $db->FetchResult($cur,$x,\'table_id\');\n}\n\n// Copy the column rights:\n$sql = \"SELECT column_id, rights FROM phoundry_group_column WHERE group_id = $oldKeyVal\";\n$cur = $db->Query($sql)\n   or trigger_error(\"Query $sql failed: \" . $db->Error(), E_USER_ERROR);\nfor ($x = 0; !$db->EndOfResult($cur); $x++) {\n   $sqls[] = \"INSERT INTO phoundry_group_column VALUES($keyVal, \" . $db->FetchResult($cur,$x,\'column_id\') . \", \'\" . $db->FetchResult($cur,$x,\'rights\') . \"\')\";\n}\n\nforeach($sqls as $sql) {\n   $db->Query($sql)\n      or trigger_error(\"Query $sql failed: \" . $db->Error(), E_USER_ERROR);\n}\n     ');
INSERT INTO `phoundry_event` VALUES (400,'post_insert',222,'post-insert','$ccsQuery = \"SELECT bcc.id, code FROM brickwork_page AS bp\r\nINNER JOIN brickwork_content_container AS bcc\r\nON bcc.page_type_code = bp.page_type_code\r\nWHERE bp.id = \".$keyVal;\r\n\r\n$cur = $db->Query($ccsQuery);\r\n$row = 0;\r\n$ccs = array();\r\nwhile(!$db->EndOfResult($cur)){\r\n$ccs[$db->FetchResult($cur, $row, \'id\')] = $db->FetchResult($cur, $row, \'code\');\r\n$row++;\r\n}\r\n\r\nforeach($ccs as $ccid =>$cc){\r\n$sql = sprintf(\"INSERT\r\nINTO brickwork_page_content\r\n(`content_container_code`,\r\n`page_id`,\r\n`module_code`)\r\n(SELECT \'%s\', \'%s\', `module_code` FROM `brickwork_content_container_default_module`\r\nWHERE content_container_id = \'%s\')\",\r\n$cc,\r\n$keyVal,\r\n$ccid);\r\n\r\n$db->Query($sql);\r\n}\r\n');
INSERT INTO `phoundry_event` VALUES (405,'post_update',290,'post-update','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\n\r\n$tr = Translation::factory( $_POST[\'lang\'], $_GET[\'site_identifier\'] );\r\n$tr->flushCache();\r\n');
INSERT INTO `phoundry_event` VALUES (406,'post_insert',290,'post-insert','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\n\r\n$tr = Translation::factory( $_POST[\'lang\'], $_GET[\'site_identifier\'] );\r\n$tr->flushCache();\r\n');
INSERT INTO `phoundry_event` VALUES (401,'pre_delete',326,'pre-delete','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\nNews::categoryFactory($keyVal)->delete();');
INSERT INTO `phoundry_event` VALUES (402,'pre_delete',327,'pre-delete','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\nNews::articleFactory($keyVal)->delete();');
INSERT INTO `phoundry_event` VALUES (407,'pre_delete',330,'pre-delete','$sql = \"DELETE FROM brickwork_photogallery_lookup WHERE page_content_id = $keyVal\";\r\n$cur = $db->Query($sql)\r\n      or trigger_error(\"Query $sql failed: \" . $db->Error(), E_USER_ERROR);');
INSERT INTO `phoundry_event` VALUES (409,'pre_delete',370,'pre-delete','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\nNews::articleFactory($keyVal)->delete();');
INSERT INTO `phoundry_event` VALUES (410,'pre_delete',379,'pre-delete','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\nNews::categoryFactory($keyVal)->delete();');
INSERT INTO `phoundry_event` VALUES (411,'check_active_pages',263,'pre-delete','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\n$res = DB::iQuery(sprintf(\"\r\nSELECT ss.id, ss.site_identifier\r\nFROM brickwork_form AS form\r\nINNER JOIN module_config_avoidform AS conf\r\nON form.code = conf.form_code\r\nINNER JOIN brickwork_page_content AS pc\r\nON	pc.id = conf.page_content_id\r\nINNER JOIN brickwork_page AS page\r\nON pc.page_id = page.id\r\nINNER JOIN brickwork_site_structure AS ss\r\nON page.site_structure_id = ss.id\r\nWHERE form.id = %d\",\r\n$keyVal\r\n));\r\n\r\n$structures = array();\r\nforeach($res as $ss) {\r\n$structures[] = SiteStructure::getPathByStructureId($ss->id, $ss->site_identifier, null);\r\n}\r\n\r\nif($structures) {\r\nreturn word(10045, \"<li>\".implode(\"</li>\\n<li>\", $structures).\"</li>\\n\").\" \";\r\n}\r\n\r\n$res = DB::iQuery(sprintf(\"\r\nSELECT builder.form_code\r\nFROM brickwork_form AS form\r\nINNER JOIN brickwork_avoid_formbuilder AS builder\r\nON builder.form_code = form.code\r\nWHERE form.id = %d\",\r\n$keyVal\r\n));\r\n\r\nif(count($res) !== 0) {\r\nreturn word(10046).\" \";\r\n}');
INSERT INTO `phoundry_event` VALUES (412,'check_active_pages_formbuilder',272,'pre-delete','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\n$res = DB::iQuery(sprintf(\"\r\nSELECT ss.id, ss.site_identifier\r\nFROM module_config_avoidform AS conf\r\nINNER JOIN brickwork_page_content AS pc\r\nON	pc.id = conf.page_content_id\r\nINNER JOIN brickwork_page AS page\r\nON pc.page_id = page.id\r\nINNER JOIN brickwork_site_structure AS ss\r\nON page.site_structure_id = ss.id\r\nWHERE conf.form_code = \'%s\'\",\r\nDB::quote($keyVal)\r\n));\r\n\r\n$structures = array();\r\nforeach($res as $ss) {\r\n$structures[] = SiteStructure::getPathByStructureId($ss->id, $ss->site_identifier, null);\r\n}\r\n\r\nif($structures) {\r\nreturn word(10045, \"<li>\".implode(\"</li>\\n<li>\", $structures).\"</li>\\n\");\r\n}\r\n\r\n$res = DB::iQuery(sprintf(\"\r\nSELECT form_code\r\nFROM brickwork_avoid_form_submission\r\nWHERE form_code = \'%s\'\",\r\nDB::quote($keyVal)\r\n));\r\n\r\nif(count($res)) {\r\nreturn word(10047);\r\n}');
INSERT INTO `phoundry_event` VALUES (413,'post_copy',222,'post-copy','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\n$oldpage = ActiveRecord::factory(\"Model_Page\", $oldKeyVal);\r\nif($oldpage->isLoaded()) {\r\n  foreach($oldpage->page_contents as $page_content) {\r\n    $page_content->copy($keyVal);\r\n  }\r\n}');
INSERT INTO `phoundry_event` VALUES (414,'pre_delete',222,'pre-delete','require_once INCLUDE_DIR.\'/functions/autoloader.include.php\';\r\n$page = ActiveRecord::factory(\'Model_Page\', $keyVal);\r\nif($page->isLoaded()) {\r\n  $page->delete();\r\n}');
/*!40000 ALTER TABLE `phoundry_event` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_filter` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `string_id` varchar(80) default NULL,
  `table_id` int(11) NOT NULL default '0',
  `type` char(40) NOT NULL default '',
  `required` int(11) NOT NULL default '0',
  `group_id` int(11) default NULL,
  `name` char(80) NOT NULL default '',
  `query` text,
  `matchfield` char(40) default NULL,
  `wherepart` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `string_id` (`string_id`,`table_id`)
) ENGINE=MyISAM AUTO_INCREMENT=233 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_filter` WRITE;
/*!40000 ALTER TABLE `phoundry_filter` DISABLE KEYS */;
INSERT INTO `phoundry_filter` VALUES (1,'hide_webpower_user',1,'system',0,NULL,'Hide webpower user',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:8:\"username\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:8:\"webpower\";}}');
INSERT INTO `phoundry_filter` VALUES (2,'type',4,'system',0,NULL,'type',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:4:\"type\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:6:\"system\";}}');
INSERT INTO `phoundry_filter` VALUES (3,'table',4,'user',1,NULL,'Table','SELECT id, description FROM phoundry_table WHERE is_plugin = 0 ORDER BY description','table_id','table_id = {$FILTER_VALUE}');
INSERT INTO `phoundry_filter` VALUES (4,'type',5,'system',0,NULL,'type',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:4:\"type\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:4:\"user\";}}');
INSERT INTO `phoundry_filter` VALUES (5,'table',5,'user',1,NULL,'Table','SELECT id, description FROM phoundry_table WHERE is_plugin = 0 ORDER BY description','table_id','table_id = {$FILTER_VALUE}');
INSERT INTO `phoundry_filter` VALUES (6,'table',6,'user',1,NULL,'Table','SELECT id, description FROM phoundry_table ORDER BY description','table_id','table_id = {$FILTER_VALUE}');
INSERT INTO `phoundry_filter` VALUES (200,'page_content_id',218,'system',0,NULL,'page content id',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"page_content_id\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$page_content_id}\";}}');
INSERT INTO `phoundry_filter` VALUES (201,'site_structure',222,'system',0,NULL,'Site structure',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:17:\"site_structure_id\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:20:\"{$site_structure_id}\";}}');
INSERT INTO `phoundry_filter` VALUES (202,'site_structure_id',251,'system',0,NULL,'Site_structure_id',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:17:\"site_structure_id\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:20:\"{$site_structure_id}\";}}');
INSERT INTO `phoundry_filter` VALUES (203,'site',263,'system',0,NULL,'Site',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$site_identifier}\";}}');
INSERT INTO `phoundry_filter` VALUES (205,'site',272,'system',0,NULL,'Site',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$site_identifier}\";}}');
INSERT INTO `phoundry_filter` VALUES (204,'site',275,'system',0,NULL,'Site',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$site_identifier}\";}}');
INSERT INTO `phoundry_filter` VALUES (207,'positie',275,'user',1,NULL,'Positie','SELECT DISTINCT position, position FROM brickwork_quick_menu ORDER BY position ASC','position','position = \'{$FILTER_VALUE}\'');
INSERT INTO `phoundry_filter` VALUES (206,'site',282,'system',0,NULL,'site',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$site_identifier}\";}}');
INSERT INTO `phoundry_filter` VALUES (212,'site_translations_only',290,'system',0,NULL,'Site translations only',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$site_identifier}\";}}');
INSERT INTO `phoundry_filter` VALUES (213,'language_code',290,'user',1,NULL,'Language code','SELECT code, code FROM brickwork_site_language','lang','lang = \'{$FILTER_VALUE}\'');
INSERT INTO `phoundry_filter` VALUES (208,'site',300,'system',0,NULL,'Site',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$site_identifier}\";}}');
INSERT INTO `phoundry_filter` VALUES (209,'subform_selectie',305,'user',1,3,'Subform selectie','SELECT distinct code, code  from brickwork_form where type_code = \'db\' and site_identifier=\"{$site_identifier}\" order by code asc','brickwork_form_code','brickwork_form_code = \'{$FILTER_VALUE}\'');
INSERT INTO `phoundry_filter` VALUES (210,'subform_selectie_1',305,'user',1,1,'Subform selectie','SELECT distinct code, code  from brickwork_form where type_code = \'db\' and site_identifier=\"{$site_identifier}\" order by code asc','brickwork_form_code','brickwork_form_code = \'{$FILTER_VALUE}\'');
INSERT INTO `phoundry_filter` VALUES (214,'page_content_id',329,'system',0,NULL,'page_content_id',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"page_content_id\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$page_content_id}\";}}');
INSERT INTO `phoundry_filter` VALUES (215,'album_titel',332,'user',1,NULL,'Album Titel','SELECT id, title FROM brickwork_photogallery_album ORDER BY title','album_id','album_id = {$FILTER_VALUE}');
INSERT INTO `phoundry_filter` VALUES (216,'pages',338,'system',0,NULL,'pages',NULL,NULL,'a:2:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:9:\"attribute\";s:8:\"operator\";s:4:\"like\";s:5:\"value\";s:6:\"%_page\";}s:2:\"1a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$site_identifier}\";}}');
INSERT INTO `phoundry_filter` VALUES (218,'site',341,'system',0,NULL,'Site',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:18:\"{$site_identifier}\";}}');
INSERT INTO `phoundry_filter` VALUES (217,'site',343,'system',0,NULL,'Site',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:15:\"site_identifier\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:4:\"NULL\";}}');
INSERT INTO `phoundry_filter` VALUES (230,'site',377,'user',0,NULL,'Site','SELECT identifier, name FROM brickwork_site ORDER BY name','site_identifier','site_identifier=\'{$FILTER_VALUE}\'');
INSERT INTO `phoundry_filter` VALUES (219,'banner_groep',347,'user',0,NULL,'Banner Groep','SELECT id, name FROM module_content_bannergroup ORDER BY name','id','id IN (SELECT banner_id FROM lookup_module_banner_group WHERE group_id = {$FILTER_VALUE})');
INSERT INTO `phoundry_filter` VALUES (220,'group',349,'system',0,NULL,'Group',NULL,NULL,'a:1:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:8:\"group_id\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:11:\"{$group_id}\";}}');
INSERT INTO `phoundry_filter` VALUES (221,'reactie_foto_s',350,'system',0,NULL,'Reactie foto\'s',NULL,NULL,'a:2:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:11:\"reaction_id\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:14:\"{$reaction_id}\";}s:2:\"1a\";a:3:{s:9:\"fieldname\";s:9:\"mediatype\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:5:\"image\";}}');
INSERT INTO `phoundry_filter` VALUES (222,'evenement',355,'user',0,NULL,'Evenement','SELECT id, name from module_content_agenda_event_type WHERE site_identifier=\'{$site_identifier}\' ORDER BY name ASC','event_type_id','event_type_id = {$FILTER_VALUE}');
INSERT INTO `phoundry_filter` VALUES (223,'reactie_video_s',351,'system',0,NULL,'Reactie video\'s',NULL,NULL,'a:2:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:11:\"reaction_id\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:14:\"{$reaction_id}\";}s:2:\"1a\";a:3:{s:9:\"fieldname\";s:9:\"mediatype\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:7:\"youtube\";}}');
INSERT INTO `phoundry_filter` VALUES (224,'reactie_link_s',352,'system',0,NULL,'Reactie link\'s',NULL,NULL,'a:2:{s:2:\"0a\";a:3:{s:9:\"fieldname\";s:11:\"reaction_id\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:14:\"{$reaction_id}\";}s:2:\"1a\";a:3:{s:9:\"fieldname\";s:9:\"mediatype\";s:8:\"operator\";s:1:\"=\";s:5:\"value\";s:4:\"link\";}}');
INSERT INTO `phoundry_filter` VALUES (231,'status',377,'user',1,NULL,'Status','open=Open|pending=Bezig|closed=Gesloten','status','status=\'{$FILTER_VALUE}\'');
INSERT INTO `phoundry_filter` VALUES (228,'poll',375,'user',0,NULL,'Poll','SELECT id, question FROM module_content_poll_question ORDER BY question','poll_id','poll_id = {$FILTER_VALUE}');
INSERT INTO `phoundry_filter` VALUES (229,'selecteer_poll_vraag',376,'user',0,NULL,'Selecteer poll vraag','SELECT id, question FROM module_content_poll_question ORDER BY question','poll_id','poll_id = {$FILTER_VALUE}');
/*!40000 ALTER TABLE `phoundry_filter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_group` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` char(80) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unq_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_group` WRITE;
/*!40000 ALTER TABLE `phoundry_group` DISABLE KEYS */;
INSERT INTO `phoundry_group` VALUES (1,'Superuser');
/*!40000 ALTER TABLE `phoundry_group` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_group_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_group_column` (
  `group_id` int(11) NOT NULL default '0',
  `column_id` int(11) NOT NULL default '0',
  `rights` char(10) default 'viu',
  PRIMARY KEY  (`group_id`,`column_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_group_column` WRITE;
/*!40000 ALTER TABLE `phoundry_group_column` DISABLE KEYS */;
/*!40000 ALTER TABLE `phoundry_group_column` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_group_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_group_table` (
  `group_id` int(11) NOT NULL default '0',
  `table_id` int(11) NOT NULL default '0',
  `rights` char(10) default 'iudpcnxm',
  PRIMARY KEY  (`group_id`,`table_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_group_table` WRITE;
/*!40000 ALTER TABLE `phoundry_group_table` DISABLE KEYS */;
INSERT INTO `phoundry_group_table` VALUES (1,212,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,218,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,222,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,251,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,263,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,268,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,272,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,273,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,274,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,275,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,300,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,311,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,320,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,322,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,325,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,326,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,327,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,328,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,329,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,330,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,331,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,332,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,333,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,334,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,335,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,337,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,338,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,339,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,340,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,341,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,342,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,343,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,377,'cdmnpux');
INSERT INTO `phoundry_group_table` VALUES (1,346,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,347,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,348,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,349,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,350,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,351,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,352,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,353,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,354,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,355,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,356,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,362,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,363,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,364,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,365,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,366,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,367,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,368,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,379,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,370,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,371,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,378,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,373,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,374,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,375,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,376,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (1,382,'cdimnpux');
INSERT INTO `phoundry_group_table` VALUES (0,269,'iudpcnxm');
INSERT INTO `phoundry_group_table` VALUES (0,228,'iudpcnxm');
INSERT INTO `phoundry_group_table` VALUES (1,1,'iudpcnxm');
INSERT INTO `phoundry_group_table` VALUES (0,290,'iudpcnxm');
INSERT INTO `phoundry_group_table` VALUES (0,381,'iudpcnxm');
INSERT INTO `phoundry_group_table` VALUES (1,9,'iudpnmx');
INSERT INTO `phoundry_group_table` VALUES (1,383,'cdimnpux');
/*!40000 ALTER TABLE `phoundry_group_table` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_lock` (
  `table_id` int(11) NOT NULL default '0',
  `keyval` char(128) NOT NULL default '',
  `user_id` int(11) NOT NULL default '0',
  `since` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`table_id`,`keyval`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_lock` WRITE;
/*!40000 ALTER TABLE `phoundry_lock` DISABLE KEYS */;
/*!40000 ALTER TABLE `phoundry_lock` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_login` (
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `max_login_date` int(11) NOT NULL,
  `password` varchar(40) NOT NULL,
  `type` varchar(20) default NULL,
  UNIQUE KEY `idx_uid_ip` (`user_id`,`ip_address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_login` WRITE;
/*!40000 ALTER TABLE `phoundry_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `phoundry_login` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_login_attempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_login_attempt` (
  `ip_address` varchar(40) NOT NULL,
  `nr_attempts` int(11) NOT NULL,
  `last_attempt` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`ip_address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_login_attempt` WRITE;
/*!40000 ALTER TABLE `phoundry_login_attempt` DISABLE KEYS */;
/*!40000 ALTER TABLE `phoundry_login_attempt` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_menu` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tab_id` int(11) NOT NULL default '0',
  `name` char(255) NOT NULL default '',
  `table_id` int(11) default NULL,
  `parent` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_menu` WRITE;
/*!40000 ALTER TABLE `phoundry_menu` DISABLE KEYS */;
INSERT INTO `phoundry_menu` VALUES (1,0,'a:2:{s:2:\"nl\";s:4:\"Site\";s:2:\"en\";s:4:\"Site\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (2,0,'a:2:{s:2:\"nl\";s:14:\"Site structuur\";s:2:\"en\";s:14:\"Site Structure\";}',212,1);
INSERT INTO `phoundry_menu` VALUES (3,0,'a:2:{s:2:\"nl\";s:14:\"Leesbare links\";s:2:\"en\";s:12:\"Direct Links\";}',300,1);
INSERT INTO `phoundry_menu` VALUES (4,0,'a:2:{s:2:\"nl\";s:10:\"Quick menu\";s:2:\"en\";s:10:\"Quick menu\";}',275,1);
INSERT INTO `phoundry_menu` VALUES (5,0,'a:1:{s:2:\"nl\";s:6:\"Agenda\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (6,0,'a:1:{s:2:\"nl\";s:11:\"Evenementen\";}',355,5);
INSERT INTO `phoundry_menu` VALUES (7,0,'a:1:{s:2:\"nl\";s:17:\"Evenement soorten\";}',356,5);
INSERT INTO `phoundry_menu` VALUES (8,0,'a:2:{s:2:\"nl\";s:7:\"Banners\";s:2:\"en\";s:7:\"Banners\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (9,0,'a:2:{s:2:\"nl\";s:7:\"Banners\";s:2:\"en\";s:7:\"Banners\";}',347,8);
INSERT INTO `phoundry_menu` VALUES (10,0,'a:2:{s:2:\"nl\";s:14:\"Banner Groepen\";s:2:\"en\";s:17:\"Banner groups\r\n\r\n\";}',348,8);
INSERT INTO `phoundry_menu` VALUES (11,0,'a:1:{s:2:\"nl\";s:3:\"FAQ\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (12,0,'a:1:{s:2:\"nl\";s:9:\"Artikelen\";}',363,11);
INSERT INTO `phoundry_menu` VALUES (13,0,'a:1:{s:2:\"nl\";s:12:\"Categorieën\";}',362,11);
INSERT INTO `phoundry_menu` VALUES (14,0,'a:2:{s:2:\"nl\";s:11:\"Fotogalerie\";s:2:\"en\";s:13:\"Photo gallery\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (15,0,'a:2:{s:2:\"nl\";s:6:\"Albums\";s:2:\"en\";s:6:\"Albums\";}',331,14);
INSERT INTO `phoundry_menu` VALUES (16,0,'a:2:{s:2:\"nl\";s:6:\"Foto\'s\";s:2:\"en\";s:6:\"Photos\";}',332,14);
INSERT INTO `phoundry_menu` VALUES (17,0,'a:1:{s:2:\"nl\";s:5:\"Links\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (18,0,'a:1:{s:2:\"nl\";s:5:\"Links\";}',366,17);
INSERT INTO `phoundry_menu` VALUES (19,0,'a:1:{s:2:\"nl\";s:12:\"Categorieën\";}',367,17);
INSERT INTO `phoundry_menu` VALUES (20,0,'a:1:{s:2:\"nl\";s:8:\"Misbruik\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (21,0,'a:1:{s:2:\"nl\";s:9:\"Meldingen\";}',377,20);
INSERT INTO `phoundry_menu` VALUES (22,0,'a:1:{s:2:\"nl\";s:6:\"Nieuws\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (23,0,'a:1:{s:2:\"nl\";s:9:\"Artikelen\";}',370,22);
INSERT INTO `phoundry_menu` VALUES (24,0,'a:1:{s:2:\"nl\";s:12:\"Categorieën\";}',379,22);
INSERT INTO `phoundry_menu` VALUES (25,0,'a:1:{s:2:\"nl\";s:4:\"Poll\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (26,0,'a:1:{s:2:\"nl\";s:6:\"Vragen\";}',374,25);
INSERT INTO `phoundry_menu` VALUES (27,0,'a:1:{s:2:\"nl\";s:10:\"Antwoorden\";}',375,25);
INSERT INTO `phoundry_menu` VALUES (28,0,'a:1:{s:2:\"nl\";s:10:\"Resultaten\";}',376,25);
INSERT INTO `phoundry_menu` VALUES (29,0,'a:1:{s:2:\"nl\";s:8:\"Reacties\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (30,0,'a:1:{s:2:\"nl\";s:13:\"Alle Reacties\";}',378,29);
INSERT INTO `phoundry_menu` VALUES (31,0,'a:1:{s:2:\"nl\";s:12:\"Configuratie\";}',368,29);
INSERT INTO `phoundry_menu` VALUES (32,0,'a:2:{s:2:\"nl\";s:8:\"Snippets\";s:2:\"en\";s:8:\"Snippets\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (33,0,'a:2:{s:2:\"nl\";s:14:\"Site Specifiek\";s:2:\"en\";s:13:\"Site Specific\";}',341,32);
INSERT INTO `phoundry_menu` VALUES (34,0,'a:2:{s:2:\"nl\";s:7:\"Globaal\";s:2:\"en\";s:6:\"Global\";}',343,32);
INSERT INTO `phoundry_menu` VALUES (35,0,'a:2:{s:2:\"nl\";s:11:\"Formulieren\";s:2:\"en\";s:5:\"Forms\";}',NULL,NULL);
INSERT INTO `phoundry_menu` VALUES (36,0,'a:2:{s:2:\"nl\";s:8:\"Aanmaken\";s:2:\"en\";s:15:\"Create new form\";}',263,35);
INSERT INTO `phoundry_menu` VALUES (37,0,'a:2:{s:2:\"nl\";s:9:\"Inrichten\";s:2:\"en\";s:11:\"Design form\";}',272,35);
/*!40000 ALTER TABLE `phoundry_menu` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_note` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `table_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `till_date` date NOT NULL,
  `title` char(60) NOT NULL default '',
  `contents` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_note` WRITE;
/*!40000 ALTER TABLE `phoundry_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `phoundry_note` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_notify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_notify` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `table_id` int(11) NOT NULL default '0',
  `keyval` char(128) NOT NULL default '',
  `user_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_notify` WRITE;
/*!40000 ALTER TABLE `phoundry_notify` DISABLE KEYS */;
/*!40000 ALTER TABLE `phoundry_notify` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_session` (
  `session_id` char(64) NOT NULL,
  `data` mediumblob NOT NULL,
  `last_access` datetime NOT NULL,
  `ip` char(32) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY  (`session_id`),
  KEY `last_access` (`last_access`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `phoundry_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_setting` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(40) NOT NULL,
  `type` varchar(40) NOT NULL,
  `value` varchar(255) NOT NULL,
  `history` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unq_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_setting` WRITE;
/*!40000 ALTER TABLE `phoundry_setting` DISABLE KEYS */;
INSERT INTO `phoundry_setting` VALUES (1,'safe_login','security','0','');
INSERT INTO `phoundry_setting` VALUES (2,'password_reset_interval','security','0','');
/*!40000 ALTER TABLE `phoundry_setting` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_table` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `string_id` varchar(80) default NULL,
  `order_by` int(11) NOT NULL default '0',
  `name` char(80) default NULL,
  `description` char(255) default NULL,
  `max_records` int(11) default NULL,
  `record_order` char(40) default NULL,
  `info` text,
  `is_plugin` int(11) NOT NULL default '0',
  `show_in_menu` char(20) NOT NULL default 'tab0',
  `extra` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `string_id` (`string_id`)
) ENGINE=MyISAM AUTO_INCREMENT=384 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_table` WRITE;
/*!40000 ALTER TABLE `phoundry_table` DISABLE KEYS */;
INSERT INTO `phoundry_table` VALUES (1,'phoundry_user',100010,'phoundry_user','a:2:{s:2:\"nl\";s:10:\"Gebruikers\";s:2:\"en\";s:5:\"Users\";}',20,'id ASC','',0,'service','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (2,'phoundry_group',100020,'phoundry_group','a:2:{s:2:\"nl\";s:6:\"Rollen\";s:2:\"en\";s:5:\"Roles\";}',NULL,NULL,'',0,'service',NULL);
INSERT INTO `phoundry_table` VALUES (3,'phoundry_group_table',100030,'phoundry_group_table','a:2:{s:2:\"nl\";s:10:\"Rolrechten\";s:2:\"en\";s:11:\"Role rights\";}',NULL,NULL,'',0,'service','a:3:{s:4:\"sUrl\";s:30:\"core/restrict_access/index.php\";s:17:\"rolerights_global\";b:0;s:17:\"rolerights_detail\";b:0;}');
INSERT INTO `phoundry_table` VALUES (4,'phoundry_filter',100040,'phoundry_filter','a:2:{s:2:\"nl\";s:14:\"System Filters\";s:2:\"en\";s:14:\"System Filters\";}',NULL,'id DESC','',0,'admin','a:4:{s:4:\"rUrl\";s:16:\"admin/freeze.php\";s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (5,'phoundry_filter_1',100050,'phoundry_filter','a:2:{s:2:\"nl\";s:12:\"User Filters\";s:2:\"en\";s:12:\"User Filters\";}',NULL,'id DESC','',0,'admin','a:4:{s:4:\"rUrl\";s:16:\"admin/freeze.php\";s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (6,'phoundry_event',100060,'phoundry_event','a:2:{s:2:\"nl\";s:6:\"Events\";s:2:\"en\";s:6:\"Events\";}',NULL,'id DESC','',0,'admin','a:4:{s:4:\"rUrl\";s:16:\"admin/freeze.php\";s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (7,'check_url_s',100070,'','a:2:{s:2:\"nl\";s:16:\"Controleer URL\'s\";s:2:\"en\";s:11:\"Check URL\'s\";}',NULL,'','',1,'service','a:3:{s:4:\"sUrl\";s:25:\"core/check_urls/index.php\";s:17:\"rolerights_global\";b:0;s:17:\"rolerights_detail\";b:0;}');
INSERT INTO `phoundry_table` VALUES (8,'shared_files',100005,'','a:2:{s:2:\"nl\";s:18:\"Gedeelde bestanden\";s:2:\"en\";s:12:\"Shared files\";}',NULL,'','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',1,'service','a:3:{s:4:\"sUrl\";s:19:\"core/sharedBrowser/\";s:17:\"rolerights_global\";b:0;s:17:\"rolerights_detail\";b:0;}');
INSERT INTO `phoundry_table` VALUES (208,'brickwork_organisation',1,'brickwork_organisation','a:2:{s:2:\"nl\";s:11:\"Organisatie\";s:2:\"en\";s:12:\"Organisation\";}',NULL,'name ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (212,'site_structure',40,'','a:2:{s:2:\"nl\";s:14:\"Site structuur\";s:2:\"en\";s:14:\"Site Structure\";}',NULL,'','',1,'0','a:3:{s:4:\"sUrl\";s:27:\"custom/brickwork/structure/\";s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;}');
INSERT INTO `phoundry_table` VALUES (215,'brickwork_module',70,'brickwork_module','a:2:{s:2:\"nl\";s:7:\"Modules\";s:2:\"en\";s:7:\"Modules\";}',NULL,'code ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (218,'module_content_html',100,'module_content_html','a:2:{s:2:\"nl\";s:4:\"HTML\";s:2:\"en\";s:4:\"HTML\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (222,'brickwork_page',140,'brickwork_page','a:2:{s:2:\"nl\";s:8:\"Pagina\'s\";s:2:\"en\";s:5:\"Pages\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (223,'brickwork_page_type',150,'brickwork_page_type','a:2:{s:2:\"nl\";s:11:\"Pagina type\";s:2:\"en\";s:9:\"Page type\";}',NULL,'code DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (225,'brickwork_site',170,'brickwork_site','a:2:{s:2:\"nl\";s:4:\"Site\";s:2:\"en\";s:4:\"Site\";}',NULL,'name ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (226,'brickwork_template',180,'brickwork_template','a:2:{s:2:\"nl\";s:8:\"Template\";s:2:\"en\";s:8:\"Template\";}',NULL,'name ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (227,'brickwork_content_container',190,'brickwork_content_container','a:2:{s:2:\"nl\";s:17:\"Content container\";s:2:\"en\";s:17:\"Content Container\";}',NULL,'page_type_code ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (228,'brickwork_site_host_alias',200,'brickwork_site_host_alias','a:2:{s:2:\"nl\";s:15:\"Site host alias\";s:2:\"en\";s:15:\"Site host alias\";}',NULL,'host ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (251,'brickwork_url',430,'brickwork_url','a:2:{s:2:\"nl\";s:3:\"Url\";s:2:\"en\";s:3:\"Url\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (262,'brickwork_form_types',540,'brickwork_form_types','a:2:{s:2:\"nl\";s:15:\"Formulier types\";s:2:\"en\";s:10:\"Form types\";}',NULL,'title ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (263,'brickwork_form',550,'brickwork_form','a:2:{s:2:\"nl\";s:18:\"Formulier aanmaken\";s:2:\"en\";s:11:\"Create form\";}',NULL,'title ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (268,'module_config_dmdelivery_soap',600,'module_config_dmdelivery_soap','a:2:{s:2:\"nl\";s:15:\"DMdelivery SOAP\";s:2:\"en\";s:15:\"DMdelivery SOAP\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (269,'brickwork_dmdelivery_soap_campaign',610,'brickwork_dmdelivery_soap_campaign','a:2:{s:2:\"nl\";s:34:\"Brickwork dmdelivery soap campaign\";s:2:\"en\";s:34:\"Brickwork dmdelivery soap campaign\";}',NULL,'campaign_title ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (272,'brickwork_avoid_formbuilder',640,'brickwork_avoid_formbuilder','a:2:{s:2:\"nl\";s:19:\"Formulier inrichten\";s:2:\"en\";s:11:\"Design form\";}',NULL,'form_code DESC','',0,'hide','a:7:{s:4:\"iUrl\";s:43:\"custom/brickwork/avoid_formbuilder/edit.php\";s:4:\"uUrl\";s:43:\"custom/brickwork/avoid_formbuilder/edit.php\";s:4:\"cUrl\";s:43:\"custom/brickwork/avoid_formbuilder/copy.php\";s:4:\"vUrl\";s:46:\"custom/brickwork/avoid_formbuilder/preview.php\";s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (273,'module_config_avoidform',650,'module_config_avoidform','a:2:{s:2:\"nl\";s:22:\"Configuratie formulier\";s:2:\"en\";s:11:\"Form config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (274,'module_config_htdig',660,'module_config_htdig','a:2:{s:2:\"nl\";s:19:\"Configuratie zoeken\";s:2:\"en\";s:13:\"Search config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (275,'brickwork_quick_menu',670,'brickwork_quick_menu','a:2:{s:2:\"nl\";s:10:\"Quick menu\";s:2:\"en\";s:10:\"Quick menu\";}',NULL,'id ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (282,'brickwork_dmfacts_pro_account',740,'brickwork_dmfacts_pro_account','a:2:{s:2:\"nl\";s:19:\"DMfacts pro account\";s:2:\"en\";s:19:\"DMfacts pro account\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (283,'module_config_dmfacts_pro',750,'module_config_dmfacts_pro','a:2:{s:2:\"nl\";s:31:\"Configuratie DMfacts pro module\";s:2:\"en\";s:18:\"DMfacts pro config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (290,'brickwork_translation',1390,'brickwork_translation','a:2:{s:2:\"nl\";s:11:\"Vertalingen\";s:2:\"en\";s:12:\"Translations\";}',NULL,'id DESC','',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (292,'brickwork_site_language',175,'brickwork_site_language','a:2:{s:2:\"nl\";s:10:\"Site talen\";s:2:\"en\";s:16:\"Site Language(s)\";}',NULL,'id DESC','',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (300,'brickwork_readable_links',910,'brickwork_readable_links','a:2:{s:2:\"nl\";s:14:\"Leesbare links\";s:2:\"en\";s:14:\"Readable links\";}',NULL,'subdir ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (305,'brickwork_db_form_mapping',960,'brickwork_db_form_mapping','a:2:{s:2:\"nl\";s:20:\"DB Formulier mapping\";s:2:\"en\";s:15:\"DB Form mapping\";}',NULL,'prio ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (311,'brickwork_page_content',1020,'brickwork_page_content','a:2:{s:2:\"nl\";s:19:\"Module configuratie\";s:2:\"en\";s:13:\"Module config\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:0;}');
INSERT INTO `phoundry_table` VALUES (320,'brickwork_auth_role',1160,'brickwork_auth_role','a:2:{s:2:\"nl\";s:24:\"Website gebruiker rollen\";s:2:\"en\";s:18:\"Website user roles\";}',NULL,'name ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'service','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (322,'brickwork_site_user',1180,'brickwork_site_user','a:2:{s:2:\"nl\";s:18:\"Website Gebruikers\";s:2:\"en\";s:13:\"Website Users\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'service','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (324,'brickwork_service_module',80,'brickwork_service_module','a:2:{s:2:\"nl\";s:15:\"Service Modules\";s:2:\"en\";s:15:\"Service Modules\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (325,'brickwork_service_user',1200,'brickwork_service_user','a:2:{s:2:\"nl\";s:21:\"Webservice Gebruikers\";s:2:\"en\";s:16:\"Webservice Users\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'service','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (326,'admin_dashboard',1400,'','a:2:{s:2:\"nl\";s:15:\"Admin Dashboard\";s:2:\"en\";s:15:\"Admin Dashboard\";}',NULL,'','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',1,'admin_brickwork','a:3:{s:4:\"sUrl\";s:32:\"custom/brickwork/brickworkadmin/\";s:17:\"rolerights_global\";b:0;s:17:\"rolerights_detail\";b:0;}');
INSERT INTO `phoundry_table` VALUES (327,'brickwork_googlemaps_host_api',1410,'brickwork_googlemaps_host_api','a:2:{s:2:\"nl\";s:24:\"Googlemaps host api keys\";s:2:\"en\";s:24:\"Googlemaps host api keys\";}',NULL,'http_host ASC','a:2:{s:2:\"nl\";s:155:\"Een API key kan verkregen worden op <a href=\"http://code.google.com/apis/maps/signup.html\" target=\"_blank\">http://code.google.com/apis/maps/signup.html</a>\";s:2:\"en\";s:149:\"A API key can be retrieved at <a href=\"http://code.google.com/apis/maps/signup.html\" target=\"_blank\">http://code.google.com/apis/maps/signup.html</a>\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (328,'module_config_google_maps',1420,'module_config_google_maps','a:2:{s:2:\"nl\";s:24:\"Google Maps Configuratie\";s:2:\"en\";s:18:\"Google Maps Config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (329,'module_content_google_maps',1430,'module_content_google_maps','a:2:{s:2:\"nl\";s:11:\"Google Maps\";s:2:\"en\";s:11:\"Google Maps\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (330,'module_config_photogallery',1500,'module_config_photogallery','a:2:{s:2:\"nl\";s:24:\"Fotogalerie configuratie\";s:2:\"en\";s:20:\"Photo gallery config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (331,'brickwork_photogallery_album',1510,'brickwork_photogallery_album','a:2:{s:2:\"nl\";s:17:\"Fotogalerie Album\";s:2:\"en\";s:19:\"Photo gallery Album\";}',NULL,'prio DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (332,'brickwork_photogallery_photo',1520,'brickwork_photogallery_photo','a:2:{s:2:\"nl\";s:18:\"Fotogalerie Foto\'s\";s:2:\"en\";s:20:\"Photo gallery Photos\";}',NULL,'prio ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (333,'module_config_html',1530,'module_config_html','a:2:{s:2:\"nl\";s:17:\"HTML Configuratie\";s:2:\"en\";s:11:\"HTML config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (334,'module_config_htmlblock',1540,'module_config_htmlblock','a:2:{s:2:\"nl\";s:24:\"HTML simpel Configuratie\";s:2:\"en\";s:18:\"HTML simple config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (335,'module_config_deeplink',1400,'module_config_deeplink','a:2:{s:2:\"nl\";s:32:\"Pagina\'s uitlichten configuratie\";s:2:\"en\";s:22:\"Highlight pages config\";}',NULL,'','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (337,'brickwork_site_config',1120,'brickwork_site_config','a:2:{s:2:\"nl\";s:17:\"Site configuratie\";s:2:\"en\";s:11:\"Site config\";}',NULL,'attribute ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (338,'brickwork_site_config_1',1130,'brickwork_site_config','a:2:{s:2:\"nl\";s:17:\"Speciale pagina\'s\";s:2:\"en\";s:13:\"Special pages\";}',NULL,'attribute ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (339,'module_config_google_adsense',1560,'module_config_google_adsense','a:2:{s:2:\"nl\";s:27:\"Google AdSense Configuratie\";s:2:\"en\";s:21:\"Google AdSense config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (340,'module_config_iframe',1510,'module_config_iframe','a:2:{s:2:\"nl\";s:19:\"Iframe configuratie\";s:2:\"en\";s:13:\"Iframe config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (341,'module_content_snippet',1570,'module_content_snippet','a:2:{s:2:\"nl\";s:8:\"Snippets\";s:2:\"en\";s:8:\"Snippets\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (342,'module_config_snippet',1580,'module_config_snippet','a:2:{s:2:\"nl\";s:20:\"Snippet Configuratie\";s:2:\"en\";s:14:\"Snippet Config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (343,'module_content_snippet_1',1570,'module_content_snippet','a:2:{s:2:\"nl\";s:16:\"Globale Snippets\";s:2:\"en\";s:15:\"Global snippets\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (378,'brickwork_reaction',1620,'brickwork_reaction','a:2:{s:2:\"nl\";s:13:\"Alle Reacties\";s:2:\"en\";s:13:\"All Reactions\";}',NULL,'last_update DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (377,'brickwork_abuse_report',1810,'brickwork_abuse_report','a:2:{s:2:\"nl\";s:16:\"Misbruik melding\";s:2:\"en\";s:12:\"Report Abuse\";}',NULL,'created_on DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (346,'module_config_banner',1560,'module_config_banner','a:2:{s:2:\"nl\";s:20:\"Banners Configuratie\";s:2:\"en\";s:14:\"Banners Config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (347,'module_content_banner',1570,'module_content_banner','a:2:{s:2:\"nl\";s:7:\"Banners\";s:2:\"en\";s:7:\"Banners\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (348,'module_content_bannergroup',1580,'module_content_bannergroup','a:2:{s:2:\"nl\";s:14:\"Banner Groepen\";s:2:\"en\";s:13:\"Banner Groups\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (349,'brickwork_reaction_1',1620,'brickwork_reaction','a:2:{s:2:\"nl\";s:8:\"Reacties\";s:2:\"en\";s:9:\"Reactions\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (350,'brickwork_reaction_media',1630,'brickwork_reaction_media','a:2:{s:2:\"nl\";s:6:\"Foto\'s\";s:2:\"en\";s:7:\"Photo\'s\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (351,'brickwork_reaction_media_1',1640,'brickwork_reaction_media','a:2:{s:2:\"nl\";s:7:\"Video\'s\";s:2:\"en\";s:7:\"Video\'s\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (352,'brickwork_reaction_media_2',1650,'brickwork_reaction_media','a:2:{s:2:\"nl\";s:5:\"Url\'s\";s:2:\"en\";s:5:\"Url\'s\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (353,'brickwork_reaction_group',1820,'brickwork_reaction_group','a:2:{s:2:\"nl\";s:15:\"Reactie groepen\";s:2:\"en\";s:15:\"Reaction groups\";}',NULL,'last_update DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (354,'module_config_agenda',1070,'module_config_agenda','Module config agenda',NULL,'page_content_id DESC','',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (355,'module_content_agenda',1080,'module_content_agenda','Agenda',NULL,'id DESC','',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (356,'module_content_agenda_event_type',1250,'module_content_agenda_event_type','Agenda evenement soorten',NULL,'id DESC','',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (362,'module_content_faq_category',1210,'module_content_faq_category','a:2:{s:2:\"nl\";s:11:\"FAQ rubriek\";s:2:\"en\";s:11:\"FAQ rubriek\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (363,'module_content_faq_article',1230,'module_content_faq_article','a:2:{s:2:\"nl\";s:11:\"FAQ artikel\";s:2:\"en\";s:11:\"FAQ article\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (364,'module_config_faq',1690,'module_config_faq','a:2:{s:2:\"nl\";s:17:\"FAQ Module Config\";s:2:\"en\";s:17:\"FAQ Module Config\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (365,'module_config_link',1210,'module_config_link','a:2:{s:2:\"nl\";s:18:\"Configuratie Links\";s:2:\"en\";s:19:\"Configuration Links\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (366,'module_content_link',1220,'module_content_link','a:2:{s:2:\"nl\";s:5:\"Links\";s:2:\"en\";s:5:\"Links\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (367,'module_content_link_category',1230,'module_content_link_category','a:2:{s:2:\"nl\";s:14:\"Link rubrieken\";s:2:\"en\";s:15:\"Link categories\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (368,'brickwork_reaction_relationconfig',1830,'brickwork_reaction_relationconfig','a:2:{s:2:\"nl\";s:21:\"Reacties Configuratie\";s:2:\"en\";s:23:\"Reactions Configuration\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (370,'brickwork_module_news_article',1420,'module_news_article','a:2:{s:2:\"nl\";s:16:\"Nieuws Artikelen\";s:2:\"en\";s:13:\"News Articles\";}',NULL,'id DESC','',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (371,'module_config_news',1430,'module_config_news','a:2:{s:2:\"nl\";s:19:\"Nieuws Configuratie\";s:2:\"en\";s:18:\"News Configuration\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (373,'module_config_poll',1840,'module_config_poll','a:2:{s:2:\"nl\";s:17:\"Poll Configuratie\";s:2:\"en\";s:18:\"Poll configuration\";}',NULL,'page_content_id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (374,'module_content_poll_question',1200,'module_content_poll_question','a:2:{s:2:\"nl\";s:11:\"Poll vragen\";s:2:\"en\";s:13:\"Poll question\";}',NULL,'question ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:4:{s:4:\"vUrl\";s:40:\"custom/brickwork/poll_result/preview.php\";s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (375,'module_content_poll_answer',1210,'module_content_poll_answer','a:2:{s:2:\"nl\";s:15:\"Poll antwoorden\";s:2:\"en\";s:12:\"Poll answers\";}',NULL,'prio DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (376,'module_content_poll_vote',1360,'module_content_poll_vote','a:2:{s:2:\"nl\";s:15:\"Poll resultaten\";s:2:\"en\";s:12:\"Poll results\";}',NULL,'id DESC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (379,'module_news_category',1410,'module_news_category','a:2:{s:2:\"nl\";s:19:\"Nieuws Categorieën\";s:2:\"en\";s:15:\"News categories\";}',NULL,'prio ASC','a:2:{s:2:\"nl\";s:0:\"\";s:2:\"en\";s:0:\"\";}',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (381,'PLUGIN_page_contents',1860,'','a:2:{s:2:\"nl\";s:13:\"Pagina Inhoud\";s:2:\"en\";s:13:\"Page Contents\";}',NULL,'','',1,'hide','a:3:{s:4:\"sUrl\";s:37:\"custom/brickwork/page_layout/edit.php\";s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:0;}');
INSERT INTO `phoundry_table` VALUES (382,'brickwork_dmdelivery_soap_campaign_field_mapping',1850,'brickwork_dmdelivery_soap_campaign_field_mapping','a:2:{s:2:\"nl\";s:39:\"DMdelivery SOAP campagne veld koppeling\";s:2:\"en\";s:38:\"DMdelivery SOAP campaign field mapping\";}',NULL,'id DESC','',0,'hide','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
INSERT INTO `phoundry_table` VALUES (9,'security_center',100035,'','a:2:{s:2:\"nl\";s:24:\"Beveiligingsinstellingen\";s:2:\"en\";s:17:\"Security settings\";}',NULL,'','',1,'service','a:3:{s:4:\"sUrl\";s:20:\"core/securityCenter/\";s:17:\"rolerights_global\";b:0;s:17:\"rolerights_detail\";b:0;}');
INSERT INTO `phoundry_table` VALUES (383,'brickwork_site_config_page',2050,'brickwork_site_config_page','a:2:{s:2:\"nl\";s:30:\"Site page configuratie paginas\";s:2:\"en\";s:30:\"Site page configuratie paginas\";}',NULL,'name DESC','',0,'admin_brickwork','a:3:{s:17:\"rolerights_global\";b:1;s:17:\"rolerights_detail\";b:1;s:16:\"ok_and_ins_again\";b:1;}');
/*!40000 ALTER TABLE `phoundry_table` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` char(80) NOT NULL default '',
  `address` char(80) default NULL,
  `zipcode` char(7) default NULL,
  `city` char(80) default NULL,
  `tel_nr` char(24) default NULL,
  `fax_nr` char(24) default NULL,
  `e_mail` char(80) NOT NULL default '',
  `username` char(24) NOT NULL default '',
  `password` varchar(80) NOT NULL,
  `password_set` date NOT NULL,
  `last_login_date` datetime default NULL,
  `last_login_ip` char(20) default '',
  `login_token` char(40) default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unq_username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_user` WRITE;
/*!40000 ALTER TABLE `phoundry_user` DISABLE KEYS */;
INSERT INTO `phoundry_user` VALUES (1,'Web Power',NULL,NULL,NULL,NULL,NULL,'phoundry_admin@webpower.nl','webpower','','2009-10-23','2010-10-27 13:26:14','192.168.2.138','');
/*!40000 ALTER TABLE `phoundry_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_user_group` (
  `user_id` int(11) NOT NULL default '0',
  `group_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_user_group` WRITE;
/*!40000 ALTER TABLE `phoundry_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `phoundry_user_group` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phoundry_user_shortcut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoundry_user_shortcut` (
  `user_id` int(11) NOT NULL default '0',
  `table_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`table_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phoundry_user_shortcut` WRITE;
/*!40000 ALTER TABLE `phoundry_user_shortcut` DISABLE KEYS */;
/*!40000 ALTER TABLE `phoundry_user_shortcut` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `site_structure_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_structure_cache` (
  `key` char(32) NOT NULL,
  `mod_datetime` datetime NOT NULL,
  `value` blob NOT NULL,
  PRIMARY KEY  (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `site_structure_cache` WRITE;
/*!40000 ALTER TABLE `site_structure_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_structure_cache` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

