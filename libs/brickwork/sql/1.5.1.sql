ALTER TABLE `module_content_snippet` add column `mod_datetime` datetime NOT NULL after `site_identifier`;

CREATE TABLE IF NOT EXISTS `lookup_site_banner` (
  `site_identifier` char(20) NOT NULL default '',
  `banner_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`site_identifier`,`banner_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_banner` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `image` varchar(255) NOT NULL default '',
  `url` varchar(255) default NULL,
  `online` datetime NOT NULL default '0000-00-00 00:00:00',
  `offline` datetime default NULL,
  `prio` int(11) default NULL,
  `popup` tinyint(4) NOT NULL default '0',
  `clickcounter` int(11) NOT NULL default '0',
  `mod_user` varchar(255) NOT NULL default '',
  `mod_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `bgcolor` varchar(10) default NULL,
  `custom_javascript` text,
  `analytics` tinyint(1) default '0',
  `adsense_client` varchar(40) default NULL,
  `adsense_slot` varchar(20) default NULL,
  `image_url` varchar(255) default NULL,
  `width` int(11) default NULL,
  `height` int(11) default NULL,
  `file` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_bannergroup` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_banner` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `max_banners` int(10) unsigned NOT NULL default '8',
  `banner_group` int(10) unsigned default NULL,
  `adsense` tinyint(1) default NULL,
  `sort` enum('prio','RAND()') default NULL,
  PRIMARY KEY  (`page_content_id`)
) CHARSET=utf8;