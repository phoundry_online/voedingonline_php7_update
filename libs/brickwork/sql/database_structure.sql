CREATE TABLE IF NOT EXISTS `brickwork_abuse_report` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `url` varchar(255) NOT NULL default '',
  `hash` varchar(20) default NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `action` varchar(128) default '',
  `status` enum('open','closed','pending') NOT NULL default 'open',
  `user` varchar(128) default NULL,
  `ip` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `site_identifier` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `status` (`status`),
  KEY `site` (`site_identifier`),
  KEY `created_on` (`created_on`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_auth_object` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(10) unsigned default NULL,
  `instanceid` varchar(80) NOT NULL default '',
  `description` varchar(255) default NULL,
  `class` varchar(80) NOT NULL,
  `foreign_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`instanceid`),
  KEY `parent_id` (`parent_id`),
  KEY `FK_FOREIGN_ENTITY` (`class`,`foreign_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_auth_right` (
  `id` int(11) NOT NULL auto_increment,
  `auth_object_id` int(11) NOT NULL,
  `auth_role_id` int(11) NOT NULL,
  `grant` enum('deny','allow') default 'allow',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `auth_object_id` (`auth_object_id`,`auth_role_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_auth_role` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(11) default NULL,
  `name` varchar(80) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent_id` (`parent_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_avoid_form_submission` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `insert_date` datetime default NULL,
  `form_code` varchar(20) NOT NULL default '',
  `hash` varchar(32) NOT NULL default '',
  `ip` int(10) unsigned NOT NULL default '0',
  `fields_object` blob,
  `form_name` varchar(255) default NULL,
  `url` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `hash` (`form_code`,`hash`(5))
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_avoid_form_upload` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(10) NOT NULL,
  `site_identifier` varchar(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `hashcode` varchar(32) default NULL,
  `downloadcount` int(10) unsigned NOT NULL default '0',
  `removed` tinyint(3) unsigned NOT NULL default '0',
  `filesize` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_avoid_formbuilder` (
  `form_code` varchar(20) NOT NULL default '',
  `site_identifier` varchar(20) NOT NULL default '',
  `elements` text NOT NULL,
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`form_code`,`site_identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_content_container` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(20) NOT NULL default '',
  `page_type_code` varchar(20) NOT NULL default '',
  `prio` decimal(12,4) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`,`page_type_code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_content_container_allows_module` (
  `content_container_id` int(10) unsigned NOT NULL default '0',
  `module_code` char(20) NOT NULL default '',
  PRIMARY KEY  (`content_container_id`,`module_code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_content_container_default_module` (
  `content_container_id` int(10) unsigned NOT NULL default '0',
  `module_code` char(20) NOT NULL default '',
  PRIMARY KEY  (`content_container_id`,`module_code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_country` (
  `code` varchar(10) NOT NULL default '',
  `name` varchar(20) default NULL,
  PRIMARY KEY  (`code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_country_2` (
  `iso` char(2) NOT NULL default '',
  `name_en` varchar(63) default NULL,
  `name_fr` varchar(63) default NULL,
  `name_es` varchar(63) default NULL,
  `name_de` varchar(63) default NULL,
  `name_nl` varchar(63) default NULL,
  `name_pl` varchar(63) default NULL,
  PRIMARY KEY  (`iso`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_custom_site_structure` (
  `custom_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`custom_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_db_form_field` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_db_form_mapping` (
  `id` int(11) NOT NULL auto_increment,
  `brickwork_form_code` varchar(31) NOT NULL default '',
  `fieldtype` varchar(31) NOT NULL default '',
  `dst_scheme` varchar(31) NOT NULL default '',
  `dst_table` varchar(80) NOT NULL default '',
  `dst_field` varchar(80) NOT NULL default '',
  `src_scheme` varchar(31) default NULL,
  `src_table` varchar(80) default NULL,
  `src_field` varchar(80) default NULL,
  `src_label` varchar(80) default NULL,
  `src_constraint` varchar(255) default NULL,
  `label` varchar(255) NOT NULL default '',
  `prio` float NOT NULL default '0',
  `multiplicity` char(1) NOT NULL default '1',
  `disabled` tinyint(1) NOT NULL default '0',
  `active` tinyint(1) NOT NULL default '1',
  `required` tinyint(1) NOT NULL default '0',
  `info` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UN_db_form` (`dst_scheme`,`dst_table`,`dst_field`,`brickwork_form_code`),
  KEY `name` (`prio`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_dmdelivery_soap_campaign` (
  `id` int(10) unsigned NOT NULL,
  `site_identifier` varchar(20) NOT NULL default '',
  `campaign_title` varchar(255) default NULL,
  `campaign_id` int(10) unsigned NOT NULL default '0',
  `soap_url` varchar(255) NOT NULL default '',
  `username` varchar(255) NOT NULL default '',
  `password` varchar(255) NOT NULL default '',
  `optin_mailing_id` int(11) default NULL,
  `dmd_subgroup` int(11) default NULL,
  `dmd_unsubgroup` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_identifier` (`site_identifier`(5))
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_dmdelivery_soap_campaign_field_mapping` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `soap_id` int(10) unsigned NOT NULL default '0',
  `dmd_fieldname` varchar(255) default NULL,
  `user_fieldname` varchar(255) default NULL,
  `fieldtype` varchar(31) default NULL,
  `combined_unique` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `soap_id` (`soap_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_dmfacts_pro_account` (
  `id` int(10) unsigned NOT NULL,
  `site_identifier` varchar(20) NOT NULL default '',
  `dmfacts_account_code` varchar(255) NOT NULL default '',
  `script` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_form` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(31) NOT NULL default '',
  `site_identifier` varchar(20) NOT NULL default '',
  `title` varchar(63) NOT NULL default '',
  `description` mediumtext,
  `header` text,
  `footer` text,
  `type_code` varchar(20) NOT NULL default '',
  `multiplicity` varchar(7) NOT NULL default '1',
  `active` tinyint(1) NOT NULL default '1',
  `feedbackable` tinyint(1) NOT NULL default '0',
  `notify_emailaddresses` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_form_types` (
  `code` varchar(20) NOT NULL default '',
  `title` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_googlemaps_host_api` (
  `http_host` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  PRIMARY KEY  (`http_host`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_mailtemplate` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` char(40) NOT NULL default '',
  `site_identifier` char(20) NOT NULL default '',
  `from_email` varchar(255) default '',
  `from_name` varchar(255) default '',
  `subject` text,
  `body` text,
  `bcc` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`code`,`site_identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_module` (
  `code` varchar(20) NOT NULL default '',
  `class` varchar(50) default NULL,
  `ptid_content` int(10) unsigned default NULL,
  `ptid_config` int(10) unsigned default NULL,
  `name` varchar(255) default NULL,
  `description` text,
  `image_src` varchar(255) default NULL,
  `max_per_page` int(10) unsigned default NULL,
  `template` varchar(63) default NULL,
  `cachetime` int(10) unsigned default '0',
  `is_custom` tinyint(3) unsigned default '0',
  PRIMARY KEY  (`code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_organisation` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `street` varchar(255) default NULL,
  `house_nr` int(10) unsigned default NULL,
  `house_nr_add` varchar(10) default NULL,
  `city` varchar(255) default NULL,
  `country` varchar(10) default NULL,
  `phone_area_code` varchar(10) default NULL,
  `phone_number` int(10) unsigned default NULL,
  `email_info` varchar(255) default NULL,
  `email_privacy` varchar(255) default NULL,
  `email_support` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_page` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `page_type_code` varchar(20) NOT NULL default '',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned default NULL,
  `title` varchar(255) default NULL,
  `description` text,
  `keywords` varchar(255) default '',
  `online_date` datetime default NULL,
  `offline_date` datetime default NULL,
  `staging_status` enum('test','live') default 'test',
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_structure` (`site_structure_id`,`lang_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_page_content` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `content_container_code` char(20) NOT NULL default '',
  `page_id` int(10) unsigned NOT NULL default '0',
  `module_code` char(20) NOT NULL default '',
  `prio` float(10,3) default NULL,
  `online_date` datetime default NULL,
  `offline_date` datetime default NULL,
  `staging_status` enum('test','live') default 'live',
  `respect_auth` tinyint(1) unsigned NOT NULL default '1',
  `state` enum('ok','new','delete') NOT NULL default 'ok' COMMENT 'State',
  `state_date` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `page` (`page_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_page_type` (
  `code` varchar(20) NOT NULL default '',
  `template_identifier` varchar(15) NOT NULL default '',
  `name` varchar(255) default NULL,
  `contentspecification` text,
  PRIMARY KEY  (`code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_photogallery_album` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `active` tinyint(1) default '1',
  `title` varchar(255) NOT NULL,
  `prio` float NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_photogallery_lookup` (
  `page_content_id` int(10) unsigned NOT NULL,
  `album_id` int(10) unsigned NOT NULL,
  KEY `album` (`page_content_id`,`album_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_photogallery_photo` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `album_id` int(10) unsigned NOT NULL,
  `title` varchar(255) default NULL,
  `description` varchar(255) default NULL,
  `location` varchar(255) NOT NULL,
  `pubdate` datetime NOT NULL default '0000-00-00 00:00:00',
  `prio` decimal(8,4) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_photogallery_photo` (`album_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_quick_menu` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `position` varchar(255) NOT NULL default 'footer',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `prio` float(10,3) NOT NULL default '0.000',
  PRIMARY KEY  (`id`),
  KEY `site_identifier` (`site_identifier`),
  KEY `site_structure` (`site_structure_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `group_id` int(10) unsigned NOT NULL,
  `site_user_id` int(10) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL default '0',
  `author` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `body` text,
  `ip` int(15) NOT NULL,
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created_on` datetime NOT NULL,
  `post_url` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `byGroup` (`group_id`,`active`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction_group` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `relation_id` int(10) unsigned NOT NULL,
  `relation_name` varchar(50) NOT NULL default '',
  `closed` tinyint(1) default NULL,
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created_datetime` datetime NOT NULL,
  `reaction_count` int(10) unsigned NOT NULL,
  `title` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `related` (`relation_id`,`relation_name`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction_media` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `reaction_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `mediatype` enum('audio','video','image','youtube','link') NOT NULL default 'image',
  `created_datetime` datetime NOT NULL,
  `original` varchar(255) default NULL,
  `mime` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `reaction_id` (`reaction_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction_relationconfig` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `relation_name` varchar(255) NOT NULL default '',
  `enabled` tinyint(1) default NULL,
  `order_by` tinyint(1) default NULL,
  `media` int(11) default NULL,
  `mediatypes` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction_site_config` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `relation_name` varchar(50) default '',
  `enabled` tinyint(1) unsigned NOT NULL default '0',
  `reaction_order_by_colom` varchar(50) NOT NULL default 'created_on',
  `reaction_order_by` enum('ASC','DESC') NOT NULL default 'ASC',
  `images` int(2) unsigned NOT NULL default '4',
  `youtube` int(2) unsigned NOT NULL default '1',
  `allow_html` tinyint(1) unsigned NOT NULL default '0',
  `allow_ubb` tinyint(1) unsigned NOT NULL default '0',
  `allow_smilies` tinyint(1) unsigned NOT NULL default '0',
  `auto_link` tinyint(1) unsigned NOT NULL default '1',
  `bad_words` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Uniek` (`site_identifier`,`relation_name`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_readable_links` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `subdir` varchar(255) NOT NULL default '',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `site_identifier` (`site_identifier`,`subdir`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_service_module` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(20) NOT NULL,
  `class` varchar(40) NOT NULL,
  `name` varchar(128) default NULL,
  `secure` tinyint(4) NOT NULL default '0',
  `debug` tinyint(4) NOT NULL default '0',
  `auth_method` varchar(20) default NULL,
  `ptid_config` int(10) unsigned default NULL,
  `ptid_content` int(10) unsigned default NULL,
  `cachetime` int(11) NOT NULL default '0',
  `restrict_ip` text,
  `created_on` datetime default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_service_user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `disabled` tinyint(4) NOT NULL default '0',
  `description` varchar(255) default NULL,
  `restrict_ip` text,
  `created_on` datetime NOT NULL,
  `created_by` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_service_user_module` (
  `service_user_id` int(10) unsigned NOT NULL,
  `service_module_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`service_user_id`,`service_module_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_session` (
  `identifier` char(32) character set ascii NOT NULL,
  `site_identifier` varchar(20) NOT NULL,
  `lastaccess` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `ip` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY  (`identifier`,`site_identifier`(10))
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site` (
  `identifier` varchar(20) NOT NULL default '',
  `name` varchar(63) NOT NULL default '',
  `organisation_id` int(10) unsigned NOT NULL default '0',
  `protocol` set('http','https') default 'http',
  `domain` varchar(255) default NULL,
  `status` enum('test','live') NOT NULL default 'test',
  `template` varchar(15) NOT NULL default 'default',
  `auth_user` varchar(63) NOT NULL default 'SiteUser',
  `default_lang_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_config` (
  `id` int(11) NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL,
  `attribute` varchar(80) NOT NULL,
  `value` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQ_SITE_IDENTIFIER_ATTRIBUTE` (`site_identifier`,`attribute`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_config_page` (
  `name` varchar(80) NOT NULL default '',
  `title` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`name`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_host_alias` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `protocol` set('http','https') NOT NULL default 'http,https',
  `host` varchar(50) NOT NULL default '',
  `site_identifier` varchar(20) NOT NULL default '',
  `redirect` tinyint(1) unsigned NOT NULL default '0',
  `prio` decimal(8,3) NOT NULL default '100.000',
  `default_lang_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `host` (`host`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_language` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL,
  `code` char(4) NOT NULL,
  `locale` char(25) NOT NULL default 'nl_NL',
  `namespace` varchar(20) NOT NULL default 'site',
  `user_selectable` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`site_identifier`,`code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_phoundry_user` (
  `site_identifier` varchar(20) default NULL,
  `phoundry_user_id` int(10) unsigned NOT NULL default '0'
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_structure` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` char(20) NOT NULL default '',
  `parent_id` int(10) unsigned default NULL,
  `name` char(40) default NULL,
  `prio` int(5) default NULL,
  `show_menu` tinyint(3) unsigned default '1',
  `show_sitemap` tinyint(3) unsigned default '1',
  `stype` enum('page','url') default NULL,
  `secure` tinyint(1) default '0',
  `restricted` tinyint(1) default '0',
  `linked_to` int(10) unsigned default NULL,
  `ssl` tinyint(1) unsigned default '0',
  PRIMARY KEY  (`id`),
  KEY `identifier` (`site_identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_structure_i18n` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_structure_id` int(10) unsigned NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(80) default NULL,
  PRIMARY KEY  (`id`),
  KEY `IDX_SITE_STRUCTURE_LANG` (`site_structure_id`,`lang_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_structure_path` (
  `site_identifier` varchar(20) NOT NULL default '',
  `hash` varchar(32) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`site_identifier`,`hash`,`lang_id`),
  UNIQUE KEY `unique_path` (`site_structure_id`,`lang_id`),
  KEY `site_structure_id` (`site_structure_id`),
  KEY `ind_site_ident_structure_id` (`site_identifier`(4),`site_structure_id`,`lang_id`,`hash`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_structure_path_archive` (
  `site_identifier` varchar(20) NOT NULL default '',
  `hash` varchar(32) NOT NULL default '',
  `name` varchar(255) NOT NULL,
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(10) unsigned NOT NULL default '0',
  `archive_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`site_identifier`,`hash`,`lang_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_structure_role` (
  `site_structure_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`site_structure_id`,`role_id`)
) DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `brickwork_site_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(128) default NULL,
  `password` varchar(32) default NULL,
  `displayname` varchar(255) default NULL,
  `email` varchar(128) default NULL,
  `createdon` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `IDX_USERNAME` (`username`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_user_auth_role` (
  `site_user_id` int(11) NOT NULL,
  `auth_role_id` int(11) NOT NULL,
  PRIMARY KEY  (`site_user_id`,`auth_role_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_user_data` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `site_user_id` int(10) unsigned NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `value` varchar(255) default NULL,
  `modified_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQ_USER_DATA` (`site_user_id`,`attribute`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_user_login` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `site_user_id` int(10) unsigned NOT NULL,
  `modified_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ip` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_user_id` (`site_user_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_site_user_site` (
  `site_user_id` int(11) unsigned NOT NULL,
  `site_identifier` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`site_user_id`,`site_identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_template` (
  `identifier` varchar(15) NOT NULL default '',
  `name` varchar(63) default NULL,
  PRIMARY KEY  (`identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_translation` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `labelhash` varchar(40) NOT NULL default '',
  `systemstring` varchar(255) default NULL,
  `lang` varchar(4) NOT NULL default '',
  `site_identifier` varchar(20) NOT NULL default '',
  `localstring` text,
  `template` varchar(255) default NULL,
  `line` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `labelhash` (`lang`,`site_identifier`,`labelhash`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_url` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `lang_id` int(11) default NULL,
  `link` varchar(255) default NULL,
  `target` enum('_self','_top','_blank') default NULL,
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_site_structure_id` (`site_structure_id`,`lang_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `file_cabinet_file` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `folder_id` int(10) unsigned NOT NULL,
  `user_id` int(10) default NULL,
  `prio` float default NULL,
  `create_datetime` datetime NOT NULL,
  `size` int(10) default NULL,
  `mime` varchar(80) default NULL,
  `name` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `description` text,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `file_cabinet_folder` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(10) unsigned default NULL,
  `prio` float default NULL,
  `create_datetime` datetime default NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `file_cabinet_module_config` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `file_rootdir` varchar(255) default '',
  `root_folder` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `file_cabinet_right` (
  `folder_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `rights` set('read','write','delete') default NULL,
  PRIMARY KEY  (`folder_id`,`role_id`)
) DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `lookup_module_agenda_site` (
  `agenda_id` int(10) NOT NULL default '0',
  `site_identifier` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`agenda_id`,`site_identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lookup_module_banner_group` (
  `banner_id` int(10) unsigned NOT NULL default '0',
  `group_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`banner_id`,`group_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lookup_module_config_deeplink_page` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `prio` float(10,3) unsigned NOT NULL default '1000.000',
  UNIQUE KEY `page_content_id` (`page_content_id`,`site_structure_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lookup_module_config_link_category` (
  `page_content_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  KEY `page_content_id` (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lookup_module_content_link_category` (
  `link_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  KEY `link_id` (`link_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lookup_site_banner` (
  `site_identifier` char(20) NOT NULL default '',
  `banner_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`site_identifier`,`banner_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_agenda_site` (
  `agenda_id` tinyint(10) NOT NULL default '0',
  `site_identifier` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`agenda_id`,`site_identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_agenda` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `items_per_page` tinyint(3) unsigned NOT NULL default '0',
  `order_by` enum('from_date','till_date','create_date') NOT NULL default 'create_date',
  `detail_page_id` int(10) unsigned NOT NULL default '0',
  `event_type_ids` varchar(31) NOT NULL default '',
  `behavior` varchar(31) NOT NULL default '',
  `site_level` int(1) NOT NULL default '0',
  `lookup_table` varchar(63) default NULL,
  `event_table` varchar(63) default NULL,
  `content_table` varchar(63) default NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_avoidform` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `form_code` varchar(20) NOT NULL default '',
  `email_recipients` text,
  `sender_email` varchar(255) default NULL,
  `sender_name` varchar(255) default NULL,
  `store_submissions` tinyint(4) NOT NULL default '0',
  `use_from` tinyint(1) unsigned NOT NULL default '0',
  `send_feedback` tinyint(4) NOT NULL default '0',
  `feedback_message` text,
  `feedback_has_formfields` tinyint(4) default '0',
  `confirm_message` text,
  `email_header` text,
  `email_footer` text,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_banner` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `max_banners` int(10) unsigned NOT NULL default '2',
  `banner_group` int(10) unsigned default NULL,
  `sort` varchar(20) default NULL,
  `adsense` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_deeplink` (
  `page_content_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_dmdelivery_soap` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `soap_campaign_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_dmfacts_pro` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `dmfacts_account` int(10) unsigned NOT NULL default '0',
  `counter_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_faq` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `sort_by` char(20) NOT NULL default '',
  `showed_questions` tinyint(3) NOT NULL default '0',
  `number_showed_questions` tinyint(3) NOT NULL default '0',
  `category_id` int(10) default NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_google_adsense` (
  `page_content_id` int(10) NOT NULL default '0',
  `client` varchar(255) NOT NULL,
  `slot` varchar(255) NOT NULL,
  `width` int(4) NOT NULL,
  `height` int(4) NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_google_maps` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `maptype` enum('NORMAL','HYBRID','SATELLITE') NOT NULL default 'NORMAL',
  `zoomsize` enum('Small','Large') NOT NULL default 'Small',
  `wheel_zoom` tinyint(1) default NULL,
  `width` int(4) default NULL,
  `height` int(4) NOT NULL default '500',
  `cannot_connect` varchar(255) NOT NULL default '<p>Google Maps</p><p>Momenteel niet bereikbaar.</p>',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_htdig` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `items_per_page` tinyint(3) unsigned NOT NULL default '10',
  `htdig_config` varchar(20) NOT NULL default 'daily',
  `detail_page_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_html` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `title` enum('prepend','append','replace') default NULL,
  `description` enum('prepend','append','replace') default NULL,
  `keywords` enum('prepend','append','replace') default NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_htmlblock` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `html` mediumtext,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_iframe` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `width` int(5) unsigned default NULL,
  `height` int(5) unsigned default NULL,
  `scrolling` enum('auto','yes','no') default NULL,
  `frameborder` tinyint(1) default '0',
  `marginheight` int(5) default NULL,
  `marginwidth` int(5) default NULL,
  `url` text NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_link` (
  `page_content_id` int(11) NOT NULL default '0',
  `link_title_sort_id` int(11) NOT NULL default '0',
  `category_title_sort_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_news` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `root_category` int(10) unsigned default NULL,
  `detail_id` int(10) unsigned default NULL,
  `items_pp` int(2) unsigned default '0',
  `view` enum('mini','detail') NOT NULL default 'mini',
  `type` int(1) NOT NULL default '1',
  `reactions` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_photogallery` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `detail_id` int(10) unsigned default NULL,
  `action` varchar(50) NOT NULL default 'index',
  `album_sort` enum('alphabet','priority') NOT NULL,
  `photo_sort` enum('alphabet','priority','date') NOT NULL,
  `photo_count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_poll` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `poll_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_config_snippet` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `snippet_id` int(10) unsigned default NULL,
  `snippet_name` varchar(20) NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_agenda` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(512) NOT NULL default '',
  `location` varchar(512) NOT NULL default '',
  `target_audience` varchar(255) default '',
  `organisation` varchar(255) default '',
  `additional_organisation` varchar(255) default NULL,
  `address` varchar(255) default '',
  `intro` text,
  `description` text,
  `from_date` datetime default NULL,
  `till_date` datetime default NULL,
  `create_date` datetime default NULL,
  `offline_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `online_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_modified_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_modified_by` varchar(255) NOT NULL default '',
  `event_type_id` int(10) NOT NULL default '0',
  `prio` float NOT NULL default '0',
  `costs` varchar(255) default NULL,
  `discount` varchar(255) default NULL,
  `route` text,
  `url` varchar(255) default NULL,
  `contact_person` varchar(255) default NULL,
  `contact_phonenumber` varchar(16) default NULL,
  `contact_email` varchar(255) default NULL,
  `subscribe_url` varchar(255) default NULL,
  `image` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_agenda_event_type` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  `site_identifier` varchar(31) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_banner` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `image` varchar(255) default NULL,
  `image_url` varchar(255) default NULL,
  `url` varchar(255) default NULL,
  `popup` tinyint(1) NOT NULL default '0',
  `clickcounter` int(11) NOT NULL default '0',
  `custom_javascript` longtext,
  `width` int(11) default NULL,
  `height` int(11) default NULL,
  `bgcolor` varchar(10) default NULL,
  `online` datetime NOT NULL default '0000-00-00 00:00:00',
  `offline` datetime default NULL,
  `prio` int(11) default NULL,
  `mod_user` varchar(255) NOT NULL default '',
  `mod_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `file` varchar(50) default NULL,
  `param` varchar(255) default NULL,
  `token` varchar(50) default NULL,
  `adsense_client` varchar(40) default NULL,
  `adsense_slot` varchar(20) default NULL,
  `analytics` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_bannergroup` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_faq_article` (
  `id` int(10) NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `category_id` int(10) NOT NULL default '0',
  `times_showed` int(10) NOT NULL default '0',
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `answer_intro` text NOT NULL,
  `prio` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_faq_category` (
  `id` int(10) NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `category` varchar(100) NOT NULL,
  `category_image` varchar(255) default '',
  `description` varchar(255) NOT NULL,
  `prio` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_faq_ip` (
  `id` int(10) NOT NULL auto_increment,
  `article_id` int(10) NOT NULL default '0',
  `ip` char(15) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `article_id` (`article_id`,`ip`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_google_maps` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `position` varchar(200) default NULL,
  `title` varchar(255) default NULL,
  `body` text,
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_pcid` (`site_identifier`(3),`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_html` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `title` varchar(63) default NULL,
  `keywords` varchar(255) default NULL,
  `description` mediumtext,
  `text` mediumtext,
  `prio` float NOT NULL default '0',
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_pcid` (`site_identifier`(3),`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_link` (
  `id` int(11) NOT NULL auto_increment,
  `link` varchar(250) NOT NULL default '',
  `title` varchar(50) NOT NULL default '',
  `description` varchar(100) default NULL,
  `prio` float NOT NULL default '1',
  `target` varchar(31) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_link_category` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL default '',
  `prio` float NOT NULL default '1',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_link_sort_types` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_poll_answer` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` int(10) unsigned NOT NULL default '0',
  `answer` varchar(255) NOT NULL default '',
  `prio` float(10,2) NOT NULL default '100.00',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_poll_question` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `visible` enum('yes','no') NOT NULL default 'no',
  `show_result` enum('yes','no') NOT NULL default 'no',
  `question` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_poll_vote` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` int(10) unsigned NOT NULL default '0',
  `answer_id` int(10) unsigned NOT NULL default '0',
  `ipaddress` int(10) unsigned NOT NULL default '0',
  `browser_hash` varchar(32) default NULL,
  `uniqid` varchar(20) default NULL,
  `vote_date` datetime default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_content_snippet` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` char(15) default NULL,
  `mod_datetime` datetime NOT NULL,
  `name` varchar(20) NOT NULL default '',
  `title` varchar(255) default '',
  `code` text NOT NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_news_article` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `category_id` int(10) unsigned default NULL,
  `archivable` tinyint(1) NOT NULL default '1',
  `reactions` tinyint(1) NOT NULL default '1',
  `create_datetime` datetime NOT NULL,
  `online_datetime` datetime NOT NULL,
  `offline_datetime` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) default NULL,
  `image` varchar(255) default NULL,
  `intro` text NOT NULL,
  `body` text,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_news_article_category` (
  `article_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`article_id`,`category_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_news_category` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(10) unsigned default NULL,
  `prio` float default NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `module_news_reaction` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `article_id` int(10) unsigned NOT NULL,
  `ipadress` int(10) NOT NULL,
  `datetime` datetime default NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `phoundry_login_attempt` (
  `ip_address` varchar(40) NOT NULL,
  `nr_attempts` int(11) NOT NULL,
  `last_attempt` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`ip_address`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `phoundry_session` (
  `session_id` char(64) NOT NULL,
  `data` mediumblob NOT NULL,
  `last_access` datetime NOT NULL,
  `ip` char(32) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY  (`session_id`),
  KEY `last_access` (`last_access`)
) DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `phoundry_setting` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(40) NOT NULL,
  `type` varchar(40) NOT NULL,
  `value` varchar(255) NOT NULL,
  `history` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unq_name` (`name`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `site_structure_cache` (
  `key` char(32) NOT NULL,
  `mod_datetime` datetime NOT NULL,
  `value` blob NOT NULL,
  PRIMARY KEY  (`key`)
) DEFAULT CHARSET=latin1;
