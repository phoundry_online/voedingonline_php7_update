ALTER TABLE `brickwork_site_structure` ADD COLUMN `linked_to` int(10) UNSIGNED NULL AFTER `restricted`;
ALTER TABLE `module_content_agenda` ADD COLUMN `image` varchar(255)  DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `lookup_module_agenda_site` (
  `agenda_id` tinyint(10) NOT NULL default '0',
  `site_identifier` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`agenda_id`,`site_identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` INT(10) UNSIGNED NOT NULL,
  `site_user_id` INT(10) UNSIGNED NOT NULL,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `author` VARCHAR(255) DEFAULT NULL,
  `email` VARCHAR(255) DEFAULT NULL,
  `body` TEXT,
  `ip` INT(10) NOT NULL,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_on` DATETIME NOT NULL,
  `post_url` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction_group` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `relation_id` INT(10) UNSIGNED NOT NULL,
  `relation_name` VARCHAR(50) NOT NULL DEFAULT '',
  `closed` TINYINT(1) DEFAULT NULL,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_datetime` DATETIME NOT NULL,
  `reaction_count` INT(10) UNSIGNED NOT NULL,
  `title` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction_media` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `reaction_id` INT(10) UNSIGNED NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  `mediatype` ENUM('audio','video','image','youtube','link') NOT NULL DEFAULT 'image',
  `created_datetime` DATETIME NOT NULL,
  `original` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY  (`id`),
  KEY `reaction_id` (`reaction_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction_relationconfig` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `relation_name` VARCHAR(255) NOT NULL DEFAULT '',
  `enabled` TINYINT(1) DEFAULT NULL,
  `order_by` TINYINT(1) DEFAULT NULL,
  `media` INT(11) DEFAULT NULL,
  `mediatypes` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `brickwork_reaction_site_config` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_identifier` VARCHAR(20) NOT NULL DEFAULT '',
  `relation_name` VARCHAR(50) DEFAULT '',
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `reaction_order_by_colom` VARCHAR(50) NOT NULL DEFAULT 'created_on',
  `reaction_order_by` ENUM('ASC','DESC') NOT NULL DEFAULT 'ASC',
  `images` INT(2) UNSIGNED NOT NULL DEFAULT '4',
  `youtube` INT(2) UNSIGNED NOT NULL DEFAULT '1',
  `allow_html` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `allow_ubb` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `allow_smilies` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `auto_link` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `bad_words` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Uniek` (`site_identifier`,`relation_name`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lookup_module_content_link_category` (
  `link_id` INT(11) NOT NULL DEFAULT '0',
  `category_id` INT(11) NOT NULL DEFAULT '0',
  KEY `link_id` (`link_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lookup_module_config_link_category` (
  `page_content_id` INT(11) NOT NULL DEFAULT '0',
  `category_id` INT(11) NOT NULL DEFAULT '0',
  KEY `page_content_id` (`page_content_id`)
) DEFAULT CHARSET=utf8;

ALTER TABLE `brickwork_site_structure_path` ADD UNIQUE `unique_path` (`site_structure_id`, `lang_id`);
ALTER TABLE `brickwork_session`     CHANGE `data` `data` BLOB NOT NULL;
