-----
-- Nieuwe tabellen en kolommen
-----
CREATE TABLE `brickwork_content_container_default_module` (
  `content_container_id` int(10) unsigned NOT NULL default '0',
  `module_code` char(20) NOT NULL default '',
  PRIMARY KEY  (`content_container_id`,`module_code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `brickwork_mailtemplate` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` char(40) NOT NULL default '',
  `site_identifier` char(20) NOT NULL default '',
  `from_email` varchar(255) default '',
  `from_name` varchar(255) default '',
  `subject` text,
  `body` text,
  `bcc` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`code`,`site_identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `brickwork_photogallery_album` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `active` tinyint(1) default '1',
  `title` varchar(255) NOT NULL,
  `prio` float NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `brickwork_photogallery_lookup` (
  `page_content_id` int(10) unsigned NOT NULL,
  `album_id` int(10) unsigned NOT NULL,
  KEY `album` (`page_content_id`,`album_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `brickwork_photogallery_photo` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `album_id` int(10) unsigned NOT NULL,
  `title` varchar(255) default NULL,
  `description` varchar(255) default NULL,
  `location` varchar(255) NOT NULL,
  `pubdate` datetime NOT NULL default '0000-00-00 00:00:00',
  `prio` decimal(8,4) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_photogallery_photo` (`album_id`)
) DEFAULT CHARSET=utf8;

ALTER TABLE `brickwork_site_structure` CHANGE `prio` `prio` int(5) NULL ;

ALTER TABLE `brickwork_site_structure_path_archive` ADD COLUMN `name` varchar(255) NOT NULL AFTER `hash`;

CREATE TABLE `brickwork_site_user_login` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `site_user_id` int(10) unsigned NOT NULL,
  `modified_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ip` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_user_id` (`site_user_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `lookup_module_config_deeplink_page` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `site_structure_id` int(10) unsigned NOT NULL default '0',
  `prio` float(10,3) unsigned NOT NULL default '1000.000',
  UNIQUE KEY `page_content_id` (`page_content_id`,`site_structure_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_deeplink` (
  `page_content_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_google_adsense` (
 `page_content_id` int(10) NOT NULL default '0',
 `client` varchar(255) NOT NULL,wik
 `slot` varchar(255) NOT NULL,
 `width` int(4) NOT NULL,
 `height` int(4) NOT NULL,
 PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_google_maps` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `maptype` enum('NORMAL','HYBRID','SATELLITE') NOT NULL default 'NORMAL',
  `zoomsize` enum('Small','Large') NOT NULL default 'Small',
  `wheel_zoom` tinyint(1) default NULL,
  `width` int(4) default NULL,
  `height` int(4) NOT NULL default '500',
  `cannot_connect` varchar(255) NOT NULL default '<p>Google Maps</p><p>Momenteel niet bereikbaar.</p>',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_html` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `title` enum('prepend','append','replace') default NULL,
  `description` enum('prepend','append','replace') default NULL,
  `keywords` enum('prepend','append','replace') default NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_htmlblock` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `html` text,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_photogallery` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `detail_id` int(10) unsigned default NULL,
  `action` varchar(50) NOT NULL default 'index',
  `album_sort` enum('alphabet','priority') NOT NULL,
  `photo_sort` enum('alphabet','priority','date') NOT NULL,
  `photo_count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

ALTER TABLE `module_config_snippet` ADD COLUMN `snippet_name` varchar(20) NOT NULL AFTER `snippet_id`,CHANGE `snippet_id` `snippet_id` int(10) UNSIGNED NULL ;

CREATE TABLE `module_content_google_maps` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `position` varchar(200) default NULL,
  `title` varchar(255) default NULL,
  `body` text,
  `mod_datetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_phoundry_user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_pcid` (`site_identifier`(3),`page_content_id`)
) DEFAULT CHARSET=utf8;

ALTER TABLE `module_content_html` ADD COLUMN `keywords` varchar(255) NULL AFTER `title`, ADD COLUMN `description` text NULL AFTER `keywords`;

ALTER TABLE `module_news_category` CHANGE `parent_id` `parent_id` int(10) UNSIGNED NULL ;





-----
-- Controleren of onderstaande tabellen bestaan en overeenkomen
-- Vanaf volgende releases wordt er van uitgegaan dat deze tabellen er zijn
-----
CREATE TABLE `brickwork_avoid_form_upload` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(10) NOT NULL,
  `site_identifier` varchar(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `hashcode` varchar(32) default NULL,
  `downloadcount` int(10) unsigned NOT NULL default '0',
  `removed` tinyint(3) unsigned NOT NULL default '0',
  `filesize` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_agenda` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `items_per_page` tinyint(3) unsigned NOT NULL default '0',
  `order_by` enum('from_date','till_date','create_date') NOT NULL default 'create_date',
  `detail_page_id` int(10) unsigned NOT NULL default '0',
  `event_type_ids` varchar(31) NOT NULL default '',
  `behavior` varchar(31) NOT NULL default '',
  `site_level` int(1) NOT NULL default '0',
  `lookup_table` varchar(63) default NULL,
  `event_table` varchar(63) default NULL,
  `content_table` varchar(63) default NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_agenda_site` (
  `agenda_id` tinyint(10) NOT NULL default '0',
  `site_identifier` varchar(63) NOT NULL default '',
  PRIMARY KEY  (`agenda_id`,`site_identifier`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_agenda` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(63) NOT NULL default '',
  `location` varchar(63) NOT NULL default '',
  `target_audience` varchar(255) default '',
  `organisation` varchar(255) default '',
  `additional_organisation` varchar(255) default NULL,
  `address` varchar(255) default '',
  `intro` text,
  `description` text,
  `from_date` datetime default NULL,
  `till_date` datetime default NULL,
  `create_date` datetime default NULL,
  `offline_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `online_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_modified_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_modified_by` varchar(255) NOT NULL default '',
  `event_type_id` int(10) NOT NULL default '0',
  `prio` float NOT NULL default '0',
  `costs` varchar(255) default NULL,
  `discount` varchar(255) default NULL,
  `route` text,
  `url` varchar(255) default NULL,
  `contact_person` varchar(255) default NULL,
  `contact_phonenumber` varchar(16) default NULL,
  `contact_email` varchar(255) default NULL,
  `subscribe_url` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_agenda_event_type` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(63) NOT NULL default '',
  `site_identifier` varchar(31) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `brickwork_avoid_form_upload` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(10) NOT NULL,
  `site_identifier` varchar(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `hashcode` varchar(32) default NULL,
  `downloadcount` int(10) unsigned NOT NULL default '0',
  `removed` tinyint(3) unsigned NOT NULL default '0',
  `filesize` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_faq` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `sort_by` char(20) NOT NULL default '',
  `showed_questions` tinyint(3) NOT NULL default '0',
  `number_showed_questions` tinyint(3) NOT NULL default '0',
  `category_id` int(10) default NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_photogallery` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `detail_id` int(10) unsigned default NULL,
  `action` varchar(50) NOT NULL default 'index',
  `album_sort` enum('alphabet','priority') NOT NULL,
  `photo_sort` enum('alphabet','priority','date') NOT NULL,
  `photo_count` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_iframe` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `width` int(5) unsigned default NULL,
  `height` int(5) unsigned default NULL,
  `scrolling` enum('auto','yes','no') default NULL,
  `frameborder` tinyint(1) default '0',
  `marginheight` int(5) default NULL,
  `marginwidth` int(5) default NULL,
  `url` text NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_link` (
  `page_content_id` int(11) NOT NULL default '0',
  `link_title_sort_id` int(11) NOT NULL default '0',
  `category_title_sort_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_poll` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `poll_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_snippet` (
  `page_content_id` int(10) unsigned NOT NULL default '0',
  `snippet_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_faq_article` (
  `id` int(10) NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `category_id` int(10) NOT NULL default '0',
  `times_showed` int(10) NOT NULL default '0',
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `answer_intro` text NOT NULL,
  `prio` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_faq_category` (
  `id` int(10) NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `category` varchar(100) NOT NULL,
  `category_image` varchar(255) default '',
  `description` varchar(255) NOT NULL,
  `prio` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_faq_ip` (
  `id` int(10) NOT NULL auto_increment,
  `article_id` int(10) NOT NULL default '0',
  `ip` char(15) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `article_id` (`article_id`,`ip`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_link` (
  `id` int(11) NOT NULL auto_increment,
  `link` varchar(250) NOT NULL default '',
  `title` varchar(50) NOT NULL default '',
  `description` varchar(100) default NULL,
  `prio` float NOT NULL default '1',
  `target` varchar(31) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_link_category` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL default '',
  `prio` float NOT NULL default '1',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_link_sort_types` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_poll_answer` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` int(10) unsigned NOT NULL default '0',
  `answer` varchar(255) NOT NULL default '',
  `prio` float(10,2) NOT NULL default '100.00',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_poll_question` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `visible` enum('yes','no') NOT NULL default 'no',
  `show_result` enum('yes','no') NOT NULL default 'no',
  `question` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_poll_vote` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` int(10) unsigned NOT NULL default '0',
  `answer_id` int(10) unsigned NOT NULL default '0',
  `ipaddress` int(10) unsigned NOT NULL default '0',
  `browser_hash` varchar(32) default NULL,
  `uniqid` varchar(20) default NULL,
  `vote_date` datetime default NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_news_article` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`category_id` int(10) unsigned NOT NULL,
	`archivable` tinyint(1) NOT NULL default '1',
	`reactions` tinyint(1) NOT NULL default '1',
	`create_datetime` datetime NOT NULL,
	`online_datetime` datetime NOT NULL,
	`offline_datetime` datetime NOT NULL,
	`title` varchar(255) NOT NULL,
	`author` varchar(255) default NULL,
	`image` varchar(255) default NULL,
	`intro` text NOT NULL,
	`body` text,
	PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_news_category` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`parent_id` int(10) unsigned default NULL,
	`prio` float default NULL,
	`name` varchar(255) NOT NULL,
	`description` text,
	PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_news_reaction` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`article_id` int(10) unsigned NOT NULL,
	`ipadress` int(10) NOT NULL,
	`datetime` datetime default NULL,
	`email` varchar(255) NOT NULL,
	`name` varchar(255) NOT NULL,
	`title` varchar(255) NOT NULL,
	`body` text,
	PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_config_news` (
	`page_content_id` int(10) unsigned NOT NULL default '0',
	`root_category` int(10) unsigned default NULL,
	`detail_id` int(10) unsigned default NULL,
	`items_pp` int(2) unsigned default '0',
	`view` enum('mini','detail') NOT NULL default 'mini',
	`type` int(1) NOT NULL default '1',
	`reactions` tinyint(1) NOT NULL default '0',
	PRIMARY KEY  (`page_content_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `module_content_snippet` (                  
	`id` int(10) unsigned NOT NULL auto_increment,         
	`site_identifier` char(15) default NULL,               
	`name` varchar(20) NOT NULL,                           
	`title` varchar(255) default '',                       
	`code` text NOT NULL,                                  
	PRIMARY KEY  (`id`)                                    
) DEFAULT CHARSET=utf8;

ALTER TABLE `module_content_snippet` add column `mod_datetime` datetime NOT NULL after `site_identifier`;