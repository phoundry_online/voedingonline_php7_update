<?php
require_once('PHPUnit/Extensions/Database/TestCase.php');

abstract class UnitTest_TestCase_Database extends PHPUnit_Extensions_Database_TestCase
{
	protected	$pdo;
	
	public function __construct()
	{
		global $PHprefs, $WEBprefs;
		
		//$this->pdo = new PDO('mysql');
		$this->pdo = NULL;
		
		if ( defined('UNITTEST_PDO_CONNECTION')  && UNITTEST_PDO_CONNECTION != 'undefined')
		{
			$this->pdo = new PDO( UNITTEST_PDO_CONNECTION, UNITTEST_PDO_USERNAME, UNITTEST_PDO_PASSWORD );

			if ( isset($WEBprefs['UnitTest.Database']))
				$PHprefs = array_merge($PHprefs, $WEBprefs['UnitTest.Database']);
			else
			{
				$PHprefs['DBtype']			= 'mysql';
				$PHprefs['DBserver']		= '192.168.2.24';
				$PHprefs['DBuserid']		= UNITTEST_PDO_USERNAME;
				$PHprefs['DBpasswd']		= UNITTEST_PDO_PASSWORD;
				$PHprefs['DBname']			= UNITTEST_PDO_SCHEMA;
				$PHprefs['DBport']			= '3306';
				$PHprefs['DBpersistent']	= false;
				$PHprefs['DBoptions']		= array();
				$PHprefs['charset']			= 'utf-8';
			}
		}
		
		if ( !is_null($this->pdo) && method_exists( $this, 'createTables') )
			$this->createTables();
	}
	
	protected function getConnection()
	{
		return $this->createDefaultDBConnection( $this->pdo, UNITTEST_PDO_SCHEMA );
	}
	
	protected function setup()
	{
		if (!defined('UNITTEST_PDO_CONNECTION') && UNITTEST_PDO_CONNECTION != 'undefined')
			$this->markTestSkipped('No Database connection defined');
		parent::setup();
	}
	
}