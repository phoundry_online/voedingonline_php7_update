<?php

class UnitTest_TestCaseIterator extends FilterIterator
{
	public function accept( )
	{
		$cur = $this->current();
		
		$ok = ($cur->isDir() && !$cur->isDot() ) || ( $cur->isFile() && $cur->isReadable() && strpos($cur->getFilename(),'Test.php') );
		
		//echo $cur . "=> " . ($ok ? 'Accept' : 'Reject') . "\n";
		return $ok;
	}
}