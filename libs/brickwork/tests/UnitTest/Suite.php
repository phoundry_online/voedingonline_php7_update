<?php
include('TestCaseIterator.php');

class UnitTest_Suite
{
	public static function buildSuite( $dir, $parentClass )
	{
		$args   = explode('_', $parentClass);
		array_pop($args);
		$element= implode('_',$args);
		$suite 	= new PHPUnit_Framework_TestSuite('Brickwork Core');
		$di 	= new UnitTest_TestCaseIterator( new DirectoryIterator($dir) );
		
		foreach ( $di as $file )
		{
			if ( $file->isFile() && substr($file->getFileName(),-4) == '.php')
			{
				$filename = $file->getFileName();
				$class    = ($element ? $element . '_' : '' ) .substr($filename,0,-4);
				
				//echo "Requiring $dir/$filename\n";
				require_once( $dir . '/' .$filename);
				
				if ( class_exists( $class))
					$suite->addTestSuite($class);
			}
			else if ( substr($file->getFilename(),0,1) != '.')
			{
				$nd = $dir . '/' . $file->getFilename();

				if ( file_exists( $nd . '/AllTests.php') )
				{
					require( $nd . '/AllTests.php');
					
					$class = ($element ? $element . '_' : '' ) . $file->getFilename() . '_AllTests';
					 
					if ( class_exists( $class))
						$suite->addTestSuite( $class );
				}

			}
		}
		
		return $suite;
	}
}