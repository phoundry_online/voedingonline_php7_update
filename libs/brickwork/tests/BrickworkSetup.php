<?php
require_once('PREFS.php');
require_once('PHPUnit/Framework.php');
require_once(BRICKWORK_FUNCTION_DIR . '/common.php');
require_once(BRICKWORK_FUNCTION_DIR . '/autoloader.include.php');

// Add current dir for UnitTest classes
//ini_set('include_path', ini_get('include_path') . ':' . dirname(__FILE__) . '/classes/');
Loader::getInstance()->getLoader('class')->pushPath(dirname(__FILE__));
Loader::getInstance()->getLoader('class')->pushPath(dirname(__FILE__) . '/classes/');


if ( isset($WEBprefs['UnitTest.Database']) )
{
	define("UNITTEST_PDO_CONNECTION", sprintf("%s:host=%s;dbname=%s", $WEBprefs['UnitTest.Database']['DBtype'], $WEBprefs['UnitTest.Database']['DBhost']));
	define("UNITTEST_PDO_USERNAME",$WEBprefs['UnitTest.Database']['DBuser'] );
	define("UNITTEST_PDO_PASSWORD", $WEBprefs['UnitTest.Database']['passwd']);
	define("UNITTEST_PDO_SCHEMA", $WEBprefs['UnitTest.Database']['name']);
}
else
{
	define("UNITTEST_PDO_CONNECTION","mysql:host=192.168.2.24;dbname=brickwork_unittestdb");
	define("UNITTEST_PDO_USERNAME", 'unittest');
	define("UNITTEST_PDO_PASSWORD", 'tsettinu');
	define("UNITTEST_PDO_SCHEMA",	'brickwork_unittestdb');
}