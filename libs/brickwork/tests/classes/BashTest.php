<?php
class BashTest extends PHPUnit_Framework_TestCase
{
	/* @todo Er wordt 'string' afgedrukt dit gebeurd met de code op regel 15 en dan de bijbehorende test */
	public function testConstruct()
	{
		$this->markTestIncomplete("Incompatible with PHPunit");
		
		$bash = new Bash;
		$bashprintln = new Bash;
		$bashprint = new Bash;
		
		$bashprintln->println("string", "black", "black", "none");
		$bashprint->println("string", "black", "black", "underscore");
		//$bashprint->println("string", "black", "black", "bold"); // deze print "string" in de test results... van unit testing
		
		$this->assertNotNull($bash);
		$this->assertNotNull($bashprintln);
		$this->assertEquals($bashprintln, $bashprint);
		
	}
}
