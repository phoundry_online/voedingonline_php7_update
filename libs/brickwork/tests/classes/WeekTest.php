<?php
class WeekTest extends PHPUnit_Framework_TestCase
{
	public function testThisWeek()
	{
		$w = new Week();
		
		$cw = Date('Y') . Date('W');
		$this->assertEquals($cw, $w->nr);
	}
	
	public function testPreciseWeek()
	{
		$w = new Week('200835');
		
		$this->assertEquals( '200835', $w->nr );
		
		$w = new Week('25');
		$this->assertEquals( '000025', $w->nr );
	}
	
	/**
	 * Enter description here...
	 *
	 * @dataProvider	weekPeriodProvider
	 */
	public function testWeekStartsOnMonday( $weeknumber, $startofweek, $endofweek)
	{
		$this->markTestIncomplete('API unclear');
		
		$w = new Week($weeknumber);
		
		$this->assertEquals( $startofweek, $w->week_start, sprintf('Start of week does not match, expected %s but got %s', date("d-m-Y H:i:s",$startofweek), date("d-m-Y H:i:s",$w->week_start) ) );
		$this->assertEquals( $endofweek, $w->week_end, sprintf('End of week does not match, expected %s but got %s', date("d-m-Y H:i:s",$endofweek), date("d-m-Y H:i:s",$w->week_end) ) );
	}

	public function weekPeriodProvider()
	{
		return Array(
						Array('200835',mktime(0,0,0,8,25,2008),mktime(23,59,59,8,31,2008)), //
						Array('200809',mktime(0,0,0,2,25,2008),mktime(23,59,59,3,2,2008)), // Schrikkelweek verleden 
						Array('200801',mktime(0,0,0,12,31,2008),mktime(23,59,59,1,6,2008)), // Jaarovergang week 1
						Array('200901',mktime(0,0,0,12,29,2008),mktime(23,59,59,4,1,2009)), // Jaarovergang week 1 toekomst
						Array('200801',mktime(0,0,0,12,27,2004),mktime(23,59,59,2,1,2005)) // Jaarovergang week 53 27-12-2004 - 2-1-2005
					);	
	}
	
	public function testWeekSub()
	{
		$this->markTestIncomplete('API unclear');
		
		$w1 = new Week('200722');
		$w2 = new Week('200712');
		
		$this->assertEquals( $w2, Week::weekSub($w1,10));
		
	}
	
	public function testWeekDiff()
	{
		$w1 = new Week('200722');
		$w2 = new Week('200712');
		
		// Normale week
		$this->assertEquals( 10, Week::weekdiff($w1,$w2));
		
		// Schrikkelweek
		$this->assertEquals(2, Week::weekdiff(new Week('200810'), new Week('200808')) );
	}
	
	public function testPreviousWeek()
	{
		$w1 = new Week('200830');
		$w2 = new Week('200829');
		
		$this->assertEquals( $w2, Week::getPrevious($w1));
		
		$w1 = new Week('200501');
		$w2 = new Week('200453');
		
		$this->assertEquals( $w2, Week::getPrevious($w1));
	}
	
	public function textNextWeek()
	{
		$this->markTestIncomplete();
	}
}
