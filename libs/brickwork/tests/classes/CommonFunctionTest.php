<?php
class CommonFunctionTest extends PHPUnit_Framework_TestCase
{
	public function testCoalesce()
	{
		$this->assertEquals( 1, coalesce(1));
		$this->assertEquals( 1, coalesce(null,1));
		$this->assertEquals( 1, coalesce(null,null,1));
		$this->assertEquals( 1, coalesce(null,1,null));
		$this->assertEquals( 1, coalesce(1,null, null));
		$this->assertNull( coalesce(null));
		
		$this->assertNull( coalesce(0));
		$this->assertNull( coalesce(''));
		$this->assertNull( coalesce("0"));
		$this->assertNull( coalesce(null,false));
		$this->assertEquals('test', coalesce(null, null, 'test'));
		
	}	
	
	public function testEscQuote()
	{
		$this->assertEquals( 'test', escSquote('test'));
		$this->assertEquals( "\\'test", escSquote("'test"));
		$this->assertEquals( "\\'\\'", escSquote("''"));
		$this->assertEquals( "test\\'", escSquote("test'"));
	}
	
	public function testAbbreviate()
	{
		$this->assertEquals( "test", abbreviate("test",20) );
		$this->assertEquals( "tes...", abbreviate("test",3));			
	}
	
}