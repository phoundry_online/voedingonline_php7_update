<?php
class BrickBootstrapTest extends PHPUnit_Framework_TestCase
{
	public function testCheckDefaultLoadChain()
	{
		$expect = array(
			10	=> array('BrickBootstrap', 'initAutoloader'),
			15	=> array('BrickBootstrap', 'initTimezone'),
			30	=> array('BrickBootstrap', 'initErrorHandler'),
			50 	=> array('BrickBootstrap', 'initSite'),
			70	=> array('BrickBootstrap', 'initClient'),
			90 	=> array('BrickBootstrap', 'initRepresentation'),
			110	=> array('BrickBootstrap', 'initFramework'),
			115 => array('BrickBootstrap', 'checkServerLoad'),
			120	=> array('BrickBootstrap', 'initAuthorization'),
			130	=> array('BrickBootstrap', 'handleRequest'),
		);
						
		$this->assertEquals( $expect, BrickBootstrap::initLoadChain(), 'Default load chain is invalid' );
	}
	
	public function testAddChain()
	{
		$this->markTestIncomplete();
		//$this->assertTrue( BrickBootstrap::addChain( Array('BrickBootstrap','initClient'), 30 ) );
	}
	
	public function testAddChainInvalidfunction()
	{
		$this->markTestIncomplete();
		//$this->assertFalse( BrickBootstrap::addChain( 'helloworld' ));
		//$this->assertFalse( BrickBootstrap::addChain( Array('BrickBootstrap','helloworld') ));
	}
	

}