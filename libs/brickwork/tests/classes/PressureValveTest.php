<?php
class PressureValveTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		global $WEBprefs;
		$WEBprefs['pressurevalve'] = array(
			'cli' => array(4, 4, 4),
			'web' => array(4, 4, 4),
		);
	}
	
	public function testGetLoad()
	{
		$instance = new PressureValve(array(), array(2.3, 3.4, 4.5));
		$load = $instance->getLoad();
		
		// No rules defined so all load should be 0
		$this->assertEquals($load[0], 0);
		$this->assertEquals($load[1], 0);
		$this->assertEquals($load[2], 0);
		
		$instance = new PressureValve(array('cli' => array()), array(2.3, 3.4, 4.5));
		$load = $instance->getLoad();
		
		$this->assertEquals($load[0], 2.3);
		$this->assertEquals($load[1], 3.4);
		$this->assertEquals($load[2], 4.5);
	}
	
	/**
	 * @depends testGetLoad
	 */
	public function testDefaultLoad()
	{
		if(false !== @file_put_contents('/proc/loadavg', '3.17 3.28 3.33 1/328 23647')) {
			$instance = new PressureValve(array('cli' => array()));
			$load = $instance->getLoad();
			$this->assertEquals(array_shift($load), '3.17');
			$this->assertEquals(array_shift($load), '3.28');
			$this->assertEquals(array_shift($load), '3.33');
			
			unlink('/proc/loadavg');
			$instance = new PressureValve(array('cli' => array()));
			$load = $instance->getLoad();
			$this->assertEquals($load[0], 0);
			$this->assertEquals($load[1], 0);
			$this->assertEquals($load[2], 0);
		}
		else {
			$this->markTestSkipped('/proc/loadavg is not writable');
		}
	}
	
	/**
	 * @depends testGetLoad
	 */
	public function testTresholdExceeded()
	{
		$load = array(5, 3, 1);
		
		// Test with 3 values
		$instance = new PressureValve(array('cli' => array(5, 3, 1)), $load);
		$this->assertTrue($instance->allowed(false));
		
		$instance = new PressureValve(array('cli' => array(4.9, 2.9, 0.9)), $load);
		$this->assertFalse($instance->allowed(false));
		$instance = new PressureValve(array('cli' => array(1, 0, 0)), $load);
		$this->assertFalse($instance->allowed(false));
		
		// Test with 2 values only the first value should be used
		$instance = new PressureValve(array('cli' => array(3, 0)), $load);
		$this->assertTrue($instance->allowed(false));
		
		$instance = new PressureValve(array('cli' => array(2.9, 10)), $load);
		$this->assertFalse($instance->allowed(false));
		
		// Test with only 1 value
		$instance = new PressureValve(array('cli' => 3), $load);
		$this->assertTrue($instance->allowed(false));
		
		$instance = new PressureValve(array('cli' => 2.9), $load);
		$this->assertFalse($instance->allowed(false));
	}
	
	/**
	 * @depends testTresholdExceeded
	 */
	public function testCallbacks()
	{
		global $WEBprefs;
		$stub = new PressureValveTestClass;
		$stub2 = new PressureValveTestClass;
		$stub3 = new PressureValveTestClass;
		$WEBprefs['pressurevalve']['call'] = array(
			array('method' => array($stub, 'test'), 'load' => 2),
			array('method' => array($stub2, 'test2'), 'load' => 4),
			array('method' => array($stub, 'test2'), 'load' => 2),
			array('method' => array($stub3, 'test'), 'load' => 2),
		);
		
		$instance = new PressureValve($WEBprefs['pressurevalve'], array(10, 3, 1));
		$this->assertTrue($stub->called);
		$this->assertFalse($stub2->called);
		$this->assertFalse($stub3->called);
	}
	
	/**
	 * @expectedException LogicException
	 */
	public function testCallbacksException()
	{
		global $WEBprefs;
		$stub = new PressureValveTestClass;
		$WEBprefs['pressurevalve']['call'] = array(
			array('method' => array($stub, 'test'), 'load' => 2),
			array('method' => array($stub, 'undefinedMethod'), 'load' => 2),
		);
		
		$instance = new PressureValve($WEBprefs['pressurevalve'], array(10, 3, 1));
	}
	
	/**
	 * @depends testTresholdExceeded
	 */
	public function testDefines()
	{
		global $WEBprefs;
		$WEBprefs['pressurevalve']['define'] = array(
			'PressureValveTest' => 2,
			'PressureValveTest2' => 4,
		);
		$load = array(10, 3, 1);
		
		$instance = new PressureValve($WEBprefs['pressurevalve'], $load);
		$this->assertTrue(defined('PressureValveTest'));
		$this->assertTrue(PressureValveTest);
		$this->assertTrue(defined('PressureValveTest2'));
		$this->assertFalse(PressureValveTest2);

		// Change the load and check the defines aren't changed
		$load[1] = 10;
		$instance = new PressureValve($WEBprefs['pressurevalve'], $load);
		$this->assertTrue(defined('PressureValveTest'));
		$this->assertTrue(PressureValveTest);
		$this->assertTrue(defined('PressureValveTest2'));
		$this->assertFalse(PressureValveTest2);
	}
	
	public function testGetInstance()
	{
		$instance = PressureValve::getInstance();
		$this->assertTrue($instance instanceof PressureValve);
		$this->assertTrue($instance->isEnabled());
		$this->assertTrue($instance->allowed(true));
		$this->assertTrue($instance->allowed(false));
	}
	
	public function testIsEnabled()
	{
		global $WEBprefs;
		$instance = new PressureValve($WEBprefs['pressurevalve']);
		$this->assertTrue($instance->isEnabled());
		
		$WEBprefs['pressurevalve'] = null;
		$instance = new PressureValve($WEBprefs['pressurevalve']);
		$this->assertFalse($instance->isEnabled());
	}
	
	/**
	 * @depends testTresholdExceeded
	 */
	public function testAllowedTrue()
	{
		global $WEBprefs;
		$load = array(3, 3, 3);
		$instance = new PressureValve($WEBprefs['pressurevalve'], $load);
		$this->assertTrue($instance->allowed(true));
		$this->assertTrue($instance->allowed(false));
	}
	
	/**
	 * @depends testTresholdExceeded
	 */
	public function testAllowedFalse()
	{
		global $WEBprefs;
		$WEBprefs['pressurevalve'] = array(
			'cli' => array(2, 2, 2),
			'web' => array(2, 2, 2),
		);
		$load = array(3, 3, 3);
		$instance = new PressureValve($WEBprefs['pressurevalve'], $load);
		$this->assertFalse($instance->allowed(true));
		$this->assertFalse($instance->allowed(false));
		
	}
	
	public function testAllowedDefault()
	{
		// When there are no values to match to it should default to true
		$instance = new PressureValve();
		$this->assertTrue($instance->allowed(true));
		$this->assertTrue($instance->allowed(false));
		
		$instance = new PressureValve(array('web' => 3), array(2, 4, 10));
		$this->assertFalse($instance->allowed(true));
		$this->assertTrue($instance->allowed(false));
		
		$instance = new PressureValve(array('cli' => 3), array(2, 4, 10));
		$this->assertFalse($instance->allowed(false));
		$this->assertTrue($instance->allowed(true));
	}
}

class PressureValveTestClass
{
	public $called = false;
	
	public function test()
	{
		$this->called = true;
	}
	
	public function test2()
	{
		$this->called = true;
		return false;
	}
}