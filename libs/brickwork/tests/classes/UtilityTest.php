<?php
class UtilityTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testArrayValueNoArray()
	{
		$a = null;
		$b = 'what';
		
		Utility::arrayValue($b, $a);
	}
	
	public function testArrayValueWithArrayAsFirstArgument()
	{
		$a = array('what' => 'else');
		
		$this->assertEquals('else', Utility::arrayValue($a,'what'));
		$this->assertNull(Utility::arrayValue($a,0));
	}
	
	public function testArrayValueWithArrayAsSecondArgument()
	{
		$a = array('what' => 'else');
		
		$this->assertEquals('else', Utility::arrayValue('what', $a));
		$this->assertNull(Utility::arrayValue(0, $a));
	}
	
	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testArrayValueWithTwoArrays()
	{
		$a = Array('a','b');
		$b = Array('c','d');
		
		Utility::arrayValue($a, $b);
	}
	
	public function testArrayValueWithDefault()
	{
		$a = array('what' => 'else');
		
		$this->assertEquals('else', Utility::arrayValue($a, 'what', false));
		$this->assertFalse(Utility::arrayValue($a, 'watte', false));
	}
	
	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testObjectValueNoObject()
	{
		$o = true;
		Utility::objectValue($o, 'test');
	}
	
	public function testObjectValue()
	{
		$o = new stdClass();
		$o->test = 'christiaan';
		
		$this->assertEquals(Utility::objectValue($o, 'test'), 'christiaan');
		$this->assertEquals(Utility::objectValue($o, 'sjaak'), null);
		$this->assertEquals(Utility::objectValue($o, 'sjaak', 'trekhaak'), 'trekhaak');
	}
	
	public function testQuoteWithNull()
	{
		$this->assertEquals("''", Utility::quote(null));
	}
	
	public function testQuoteWithValidInput()
	{
		$this->assertEquals("'a'", Utility::quote('a'));
		$this->assertEquals("'0'", Utility::quote(0));
		$this->assertEquals("'0.1'", Utility::quote(0.1));
	}
	
	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testQuoteWithArray()
	{
		Utility::quote(Array());
	}
	
	public function testIsPosted()
	{
		$_SERVER['REQUEST_METHOD'] = 'GET';
		
		$this->assertFalse(Utility::isPosted(), 'Get request yieled TRUE on isPosted');
		
		
		$_SERVER['REQUEST_METHOD'] = 'POST';
		
		$this->assertTrue(Utility::isPosted(), 'Post request yieled non-TRUE on isPosted');
		
		$_SERVER['REQUEST_METHOD'] = 'PUT';
		
		$this->assertFalse(Utility::isPosted(), 'Put request yieled TRUE on isPosted');
		
		$_SERVER['REQUEST_METHOD'] = 'HEAD';
		
		$this->assertFalse(Utility::isPosted(), 'Head request yieled TRUE on isPosted');
		
		$_SERVER['REQUEST_METHOD'] = 'DELETE';
		
		$this->assertFalse(Utility::isPosted(), 'Delete request yieled TRUE on isPosted');
		
		$_SERVER['REQUEST_METHOD'] = 'CONNECT';
		
		$this->assertFalse(Utility::isPosted(), 'Connect request yieled TRUE on isPosted');
		
		$_SERVER['REQUEST_METHOD'] = 'TRACE';
		
		$this->assertFalse(Utility::isPosted(), 'Trace request yieled TRUE on isPosted');
	}
	
	public function testCoalesce()
	{
		$this->assertEquals( 1, Utility::coalesce(1));
		$this->assertEquals( 1, Utility::coalesce(null,1));
		$this->assertEquals( 1, Utility::coalesce(null,null,1));
		$this->assertNull(Utility::coalesce(null));
		
		$this->assertEquals( 0, Utility::coalesce(0));
		$this->assertEquals( '', Utility::coalesce(''));
		$this->assertFalse( Utility::coalesce(null,false));
	}
	
	public function testArrayValueRecursive()
	{
		$this->assertEquals('sjaak', Utility::arrayValueRecursive(array('name' => 'sjaak'), 'name'), 'Could not use arrayValueRecursive as arrayValue');
		
		$arr = array(
			'users' => array(
				0 => array(
					'name' => 'sjaak'
				),
				1 => array(
					'name' => 'henk'
				)
			)
		);
		
		$this->assertEquals('sjaak', Utility::arrayValueRecursive($arr, array('users', 0, 'name')));
		$this->assertEquals('henk', Utility::arrayValueRecursive($arr, 'users', 1, 'name'));
		$this->assertNull(Utility::arrayValueRecursive($arr, 'users', 3, 'name'));
		
		$this->assertEquals('default', Utility::arrayValueRecursive(null, array('name'), 'default'));
		$this->assertEquals('default', Utility::arrayValueRecursive('name', array('name'), 'default'));
		$this->assertEquals('default', Utility::arrayValueRecursive(20, array('name'), 'default'));
		$this->assertEquals('default', Utility::arrayValueRecursive(true, array('name'), 'default'));
	}
	
	public function testSetArrayValueRecursive()
	{
		$arr = array('sjaak' => 'gek');
		$arr = Utility::setArrayValueRecursive($arr, true, array('namen', 'harry', 'gek'));
		
		$this->assertTrue(Utility::arrayValueRecursive($arr, array('namen', 'harry', 'gek')));
	}
	
	/**
	 * @covers Utility::replaceGet
	 */
	public function testReplaceGet()
	{
		$querystring = 'mod[WebArticleModule][aid]=113&mod[WebArticlemodule][page]=1';
		
		$this->assertEquals(
			'mod[WebArticleModule][aid]=113&mod[WebArticlemodule][page]=2',
			urldecode(Utility::replaceGet('mod[WebArticlemodule][page]', 2, $querystring))
		);
		
		$this->assertEquals(
			'mod[WebArticlemodule][page]=1',
			urldecode(Utility::replaceGet('mod[WebArticleModule][aid]', null, $querystring))
		);
		
		$this->assertEquals(
			'mod[WebArticleModule][aid]=113&mod[WebArticlemodule][page]=1&mod[Test][harry][sjaak]=1',
			urldecode(Utility::replaceGet(array('mod', 'Test', 'harry', 'sjaak'), 1, $querystring))
		);
	}
	
	public function testQuerySingleResult()
	{
		$this->markTestIncomplete();
	}
	
	public function testDebug()
	{
		$this->markTestIncomplete();
	}
	
	/**
	 * @covers Utility::truncate
	 */
	public function testTruncate()
	{
		$text = 'Arjan is gek!';
		
		$this->assertEquals(
			'Arjan is...',
			Utility::truncate($text, 11)
		);
		
		$this->assertEquals(
			'Arjan is,,,',
			Utility::truncate($text, 11, ',,,')
		);
		
		$this->assertEquals(
			'Arjan is g..',
			Utility::truncate($text, 12, '..', true)
		);
		
		$this->assertEquals(
			'Arja[..]gek!',
			Utility::truncate($text, 12, '[..]', false, true)
		);
	}
	
	/**
	 * @covers Utility::removeAccents
	 */
	public function testRemoveAccents()
	{
		$tests = array(
			array('&Agrave;&Aacute;&Acirc;&Auml;&Atilde;&Aring;&AElig;', 'AAAAAAA', 'Uppercase A\'s'),
			array('&agrave;&aacute;&acirc;&auml;&aelig;&atilde;&aring;', 'aaaaaaa', 'Lowercase a\'s'),
			array('&Egrave;&Eacute;&Ecirc;&Euml;', 'EEEE', 'Uppercase E\'s'),
			array('&egrave;&eacute;&ecirc;&euml;', 'eeee', 'Lowercase e\'s'),
			array('&Igrave;&Iacute;&Icirc;&Iuml;', 'IIII', 'Uppercase I\'s'),
			array('&igrave;&iacute;&icirc;&iuml;', 'iiii', 'Lowercase i\'s'),
			array('&Ograve;&Oacute;&Ocirc;&Ouml;&Oslash;', 'OOOOO', 'Uppercase O\'s'),
			array('&ograve;&oacute;&ocirc;&ouml;&oslash;', 'ooooo', 'Lowercase o\'s'),
			array('&Ugrave;&Uacute;&Ucirc;&Uuml;', 'UUUU', 'Uppercase U\'s'),
			array('&ugrave;&uacute;&ucirc;&uuml;', 'uuuu', 'Lowercase u\'s'),
			array('&Ccedil;', 'C', 'Uppercase C\'s'),
			array('&ccedil;&cent;', 'cc', 'Lowercase c\'s'),
			array('&ETH;', 'D', 'Uppercase D\'s'),
			array('&eth;', 'd', 'Lowercase d\'s'),
			array('&Ntilde;', 'N', 'Uppercase N\'s'),
			array('&ntilde;', 'n', 'Lowercase n\'s'),
			array('&Yacute;', 'Y', 'Uppercase Y\'s'),
			
			// &Yuml; is missing in the PHP trans table and acting really funky
			//array('&Yuml;', 'Y', 'Uppercase Y\'s'),
			array('&yacute;&yuml;&yen;', 'yyy', 'Lowercase y\'s'),
			array('&times;', 'x', 'Lowercase x\'s'),
			array('&szlig;', 'ss', 'Ringel s'),
		);
		
		foreach($tests as $test) {
			list($teststr, $expected, $msg) = $test;
			$this->assertEquals(
				Utility::removeAccents(html_entity_decode($teststr, ENT_COMPAT, 'UTF-8')),
				$expected,
				$msg.' UTF-8'
			);
			$this->assertEquals(
				Utility::removeAccents(html_entity_decode($teststr, ENT_COMPAT, 'ISO-8859-1')),
				$expected,
				$msg.' ISO-8859-1'
			);
		}
	}
	
	/**
	 * @covers Utility::toUri
	 */
	public function testToUri()
	{
		$tests = array(
			array('/&Agrave;rj&aacute;&ntilde;/&iuml;s gek.pdf', 'Arjan/is-gek.pdf', 'Test'),
		);
		
		foreach($tests as $test) {
			list($teststr, $expected, $msg) = $test;
			$this->assertEquals(
				Utility::toUri(html_entity_decode($teststr, ENT_COMPAT, 'UTF-8')),
				$expected,
				$msg.' UTF-8'
			);
			$this->assertEquals(
				Utility::toUri(html_entity_decode($teststr, ENT_COMPAT, 'ISO-8859-1')),
				$expected,
				$msg.' ISO-8859-1'
			);
		}
	}
	
	public function testmakeUrlsAbsolute()
	{
		$this->markTestIncomplete();
	}
	
	/**
	 * @covers Utility::autolinkUrls
	 */
	public function testAutolinkUrls()
	{
		$tests = array(
			'Test 123 christiaan.baartse@webpower.nl jaap' => array(
				'Test 123 <a href="mailto:christiaan.baartse@webpower.nl" target="_blank">christiaan.baartse@webpower.nl</a> jaap',
				'Test 123 <a href="mailto:christiaan.baartse@webpower.nl" target="_blank" rel="nofollow">christiaan.baartse@webpower.nl</a> jaap'
			),
			'http://www.webpower.nl jaap' => array(
				'<a href="http://www.webpower.nl" target="_blank">http://www.webpower.nl</a> jaap',
				'<a href="http://www.webpower.nl" target="_blank" rel="nofollow">http://www.webpower.nl</a> jaap'
			),
			'Test 123 ftp://www.webpower.nl' => array(
				'Test 123 <a href="ftp://www.webpower.nl" target="_blank">ftp://www.webpower.nl</a>',
				'Test 123 <a href="ftp://www.webpower.nl" target="_blank" rel="nofollow">ftp://www.webpower.nl</a>'
			),
			'     Test 123 www.webpower.nl jaap     ' => array(
				'     Test 123 <a href="http://www.webpower.nl" target="_blank">www.webpower.nl</a> jaap     ',
				'     Test 123 <a href="http://www.webpower.nl" target="_blank" rel="nofollow">www.webpower.nl</a> jaap     '
			),
			'Test 123      ftp.webpower.nl      jaap' => array(
				'Test 123      <a href="ftp://ftp.webpower.nl" target="_blank">ftp.webpower.nl</a>      jaap',
				'Test 123      <a href="ftp://ftp.webpower.nl" target="_blank" rel="nofollow">ftp.webpower.nl</a>      jaap'
			),
		);
		
		foreach ($tests as $text => $asserts) {
			$this->assertEquals(
				$asserts[0],
				Utility::autolinkUrls($text)
			);
			$this->assertEquals(
				$asserts[0],
				Utility::autolinkUrls($text, false)
			);		
			$this->assertEquals(
				$asserts[1],
				Utility::autolinkUrls($text, true)
			);
		}
	}
	
	public function testSweepDir()
	{
		$this->markTestIncomplete();
	}
	
	public function testIsUtf8Latin1()
	{
		$this->assertFalse(Utility::isUtf8(utf8_decode('áéóú')));
		$this->assertFalse(Utility::isUtf8(utf8_decode('huawerohawsoertoli')));
	}
	
	public function testIsUtf8withUtf8()
	{
		$this->assertTrue(Utility::isUtf8('Барт же сумасшедшей, как шляпник'));
		$this->assertTrue(Utility::isUtf8('巴特是一个疯狂的帽子'));
	}
}
