<?php
class Cache_DbTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @var Cache_Db
	 */
	private $cache;
	
	protected function setUp()
	{
		$this->cache = new Cache_Db(parse_url('db://1/brickwork_db_cache/somenamespace?timeout=10'));
	}
	
	function testNonExistingKey()
	{
		$this->assertFalse($this->cache->keyIsset('test'));
	}
	
	function testUnsettingNonExistingKey()
	{
		$this->assertFalse($this->cache->unsetKey('test'));
	}
	
	function testSettingValue()
	{
		$this->assertTrue(
			$this->cache->setValue('een_hele_lange_cache_key', 1234)
		);
	}
	
	/**
	 * @depends testSettingValue
	 */
	function testExistingKey()
	{
		$this->assertTrue(
			$this->cache->keyIsset('een_hele_lange_cache_key')
		);
	}
	
	/**
	 * @depends testSettingValue
	 */
	function testGettingSetValue()
	{
		$this->assertEquals(
			1234, $this->cache->getValue('een_hele_lange_cache_key')
		);
	}
	
	/**
	 * @depends testExistingKey
	 */
	function testUnsettingExistingKey()
	{
		$this->assertTrue(
			$this->cache->unsetKey('een_hele_lange_cache_key')
		);
		$this->assertFalse(
			$this->cache->keyIsset('een_hele_lange_cache_key')
		);
	}
	
	function testFlushingOfValues()
	{
		$this->cache->setValue('een_hele_lange_cache_key', 1234);
		$this->cache->flush();
		$this->assertFalse(
			$this->cache->keyIsset('een_hele_lange_cache_key')
		);
	}
}