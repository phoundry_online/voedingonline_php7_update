<?php
class DateTest extends PHPUnit_Framework_TestCase
{
	public $timeStamp;
	
	public function testDate()
	{	
		$time = time();
		
		$timeStamp = new Date($time);
		$timeStamp2 = Date::getByIso(date("Y-m-d"));
		
		$this->assertEquals(date("Y"), $timeStamp->year);
		$this->assertEquals(date("m"), $timeStamp->month);
		$this->assertEquals(date("d"), $timeStamp->day);
		$this->assertEquals(date("h"), $timeStamp->hour);
		//$this->assertEquals(date("i"), $timeStamp->minute); TODO remove //
		$this->assertEquals(date("s"), $timeStamp->second);
		$this->assertEquals(date("w"), $timeStamp->weekday);
		$this->assertEquals(date("W"), $timeStamp->weekNr);
		
		$this->assertNotNull($timeStamp->checkDate("2008", "09", "19"));
		//$this->assertEquals(date("Y-m-d"),$timeStamp2);
		$this->assertEquals(date("l m F Y", $time), $timeStamp->niceDate());
		$this->assertEquals($timeStamp->getMySQLDate(),date("Y-m-d", $time)." 00:00:00");
		$this->markTestIncomplete();
																				//m d y i s h
		$this->assertEquals($timeStamp->mysql2date(date("y m d h m s", $time)), $timeStamp);
		//input van de mysql2date functie is y m d h m s, de return waarde is een formatted date object
		//		3				4			5			1			2			0		
		// [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )
		//$date = new Date(mktime($parts[3], $parts[4], $parts[5], $parts[1], $parts[2], $parts[0]));
	}


}
