<?php
class UrlTest extends PHPUnit_Framework_TestCase
{
	public function testCreateValidUrl()
	{
		$str = 'http://www.webpower.nl/path/subpath/index.php?id=1&page=2';
		$url = new Url($str);
		$this->assertTrue( is_object($url) );
		$this->assertEquals( $str, (string)$url );
		$this->assertEquals( $str, $url->getUrl() );
	}
	
	public function testIsValidWithReturnTrue(){
		$str = 'http://www.webpower.nl/path/subpath/index.php?id=1&page=2';
		$url = Url::isURL($str);
		$this->assertTrue($url);
	}

	public function testIsValidBaseUrl(){
		$str = 'http://www.webpower.nl/path/subpath/index.php?id=1&page=2';
		$url = new Url($str);
		$this->assertTrue( is_object($url) );
		$this->assertEquals( 'http://www.webpower.nl/', $url->getBaseUrl() );
	}

	public function testIsValidRobotsTxtUrl(){
		$str = 'http://www.webpower.nl/path/subpath/index.php?id=1&page=2';
		$url = new Url($str);
		$this->assertTrue( is_object($url) );
		$this->assertEquals( 'http://www.webpower.nl/robots.txt', $url->getRobotsTxtUrl() );
        }

	public function testIsValidUrlDir(){
		$str = 'http://www.webpower.nl/path/subpath/index.php?id=1&page=2';
		$url = new Url($str);
		$this->assertTrue( is_object($url) );
		$this->assertEquals( 'http://www.webpower.nl/path/subpath/', $url->getUrlDir() );
        }

	public function testIsValidFilename(){
		$str = 'http://www.webpower.nl/path/subpath/index.php?id=1&page=2';
		$url = new Url($str);
		$this->assertTrue( is_object($url) );
		$this->assertEquals( '/path/subpath/index.php', $url->getFilename() );
        }

	
	// Function specification unclear, Test disabled
	public function testIsValidCompleteFileUrl(){
		$this->markTestIncomplete('API unclear');
		
		$str = 'http://www.webpower.nl/path/subpath/index.php?id=1&page=2';
		$url = new Url($str);
		$this->assertTrue( is_object($url) );
		$this->assertEquals( 'http://www.webpower.nl/path/subpath/index.php', $url->getCompleteFileUrl() );
        }

	// CODE ERROR, Test disabled
	public function testIsValidWithReturnFalse(){
		$this->markTestIncomplete('Code error, test disabled');
		
		$str = 'http://http//www.webpower.nl/path/subpath/index.php\?test?';
		$url = Url::isURL($str);
		$this->assertFalse($url);
	}

}
