<?php
class PageTypeTest extends PHPUnit_Framework_TestCase
{
	public function testCreateValidPageType()
	{
		$pageType = "news.tpl";
		$this->assertEquals("news.tpl", $pageType);
	}
}
