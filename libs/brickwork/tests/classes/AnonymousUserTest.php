<?php
class AnonymousUserTest extends PHPUnit_Framework_TestCase
{
	public function testAnonymousUser()
	{
		$anoUser = new AnonymousUser();
		
		// Methods of AnonymousUser
		$this->assertFalse($anoUser->login());
		$this->assertTrue($anoUser->isAnonymous());
		
		// Methods of AuthUser
		$this->assertFalse($anoUser->isLoggedIn());
		$this->assertFalse($anoUser->isValid());
		$this->assertEquals($anoUser->username, Auth::anonymous_user);
		$this->assertEquals($anoUser->password, Auth::anonymous_pw);
		
		// Methods of ACLUser
		$this->assertEquals($anoUser->getAuthName(), Auth::anonymous_user);
		$this->assertEquals($anoUser->getAuthId(), 0);
		
	}
}