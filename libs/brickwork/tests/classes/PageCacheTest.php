<?php
class PageCacheTest extends PHPUnit_Framework_TestCase
{
	public function testGetKey()
	{
		$instance = new PageCache('/home', 'peter_is_gek', array(), 600);
		$this->assertEquals($instance->getKey(), '/home/peter_is_gek');
	}
	
	public function testIsEnabledFalse()
	{
		$this->assertFalse(PageCache::isEnabled());
	}
	
	/**
	 * @depends testIsEnabledFalse
	 */
	public function testGetInstanceBeforeEnable()
	{
		global $WEBprefs;
		$_SERVER['PATH_INFO'] = '/home';
		$WEBprefs['pagecache']['filter'] = array(
			'allow' => array('/home' => 'text/html'),
			'block' => array('/page'),
		);
		
		$instance = PageCache::getInstance();
		$this->assertFalse($instance->cacheable());
		$this->assertFalse($instance->isCached());
		$this->assertFalse($instance->get());
		$this->assertFalse($instance->put('<html>peter is gek</html>'));
		$this->assertFalse($instance->clean());
		$this->assertFalse($instance->send());
	}
	
	/**
	 * @depends testGetInstanceBeforeEnable
	 */
	public function testEnable()
	{
		$this->assertTrue(PageCache::enable());
	}
	
	/**
	 * @depends testEnable
	 */
	public function testGetInstanceAfterEnable()
	{
		$instance = PageCache::getInstance();
		$this->assertTrue($instance->cacheable());
		$this->assertFalse($instance->isCached());
	}
	
	/**
	 * @depends testEnable
	 */
	public function testIsEnabledTrue()
	{
		$this->assertTrue(PageCache::isEnabled());
	}
	
	/**
	 * @depends testEnable
	 */
	public function testCachable()
	{
		$instance = new PageCache('/home', 'peter_is_gek', array('allow' => array('/home' => 'text/html')), 600);
		$this->assertTrue($instance->cacheable());
	}
	
	/**
	 * @depends testEnable
	 */
	public function testCachableFalse()
	{
		$instance = new PageCache('/home', 'peter_is_gek', array('allow' => array('/page' => 'text/html')), 600);
		$this->assertFalse($instance->cacheable());
	}
	
	/**
	 * @depends testEnable
	 */
	public function testCachableHome()
	{
		$instance = new PageCache('', 'peter_is_gek', array('allow' => array('/page' => 'text/html')), 600);
		$this->assertTrue($instance->cacheable());	
	}
	
	/**
	 * @depends testEnable
	 */
	public function testCachableBlock()
	{
		$instance = new PageCache('/page', 'peter_is_gek',
			array(
				'allow' => array('/page' => 'text/html'),
				'block' => array('/page')
			), 600);
		$this->assertFalse($instance->cacheable());	
	}
	
	/**
	 * @depends testCachable
	 */
	public function testCacheUsage()
	{
		$time = time();
		
		$mock = $this->getMock('Cache_Dir', array('keyIsset', 'setValue', 'getValue', 'unsetKey', 'modifiedTime'), array(array()));
		$mock->expects($this->once())->
			method('keyIsset')->
			with($this->equalTo('/home/peter_is_gek'))->
			will($this->returnValue(true));
		$mock->expects($this->once())->
			method('setValue')->
			with($this->equalTo('/home/peter_is_gek'),
			$this->equalTo('<html>peter is gek</html>'))->
			will($this->returnValue(true));
		$mock->expects($this->exactly(2))->
			method('getValue')->
			with($this->equalTo('/home/peter_is_gek'))->
			will($this->returnValue('<html>peter is gek</html>'));
		$mock->expects($this->once())->
			method('unsetKey')->
			with($this->equalTo('/home/peter_is_gek'))->
			will($this->returnValue(true));
		$mock->expects($this->once())->
			method('modifiedTime')->
			with($this->equalTo('/home/peter_is_gek'))->
			will($this->returnValue($time));
			
		$instance = new PageCache('/home', 'peter_is_gek', array('allow' => array('/home' => 'text/html')), 600, $mock);
		$this->assertTrue($instance->isCached());
		$this->assertTrue($instance->put('<html>peter is gek</html>'));
		$this->assertEquals($instance->get(), '<html>peter is gek</html>');
		$this->assertTrue($instance->clean());
		ob_start();
		// Yeah yeah headers already send.. so we surpress warnings
		$old = error_reporting(E_ALL ^ E_WARNING);
		$instance->send();
		error_reporting($old);
		$this->assertEquals(ob_get_clean(), '<html>peter is gek</html>');
	}
	
	/**
	 * @depends testEnable
	 */
	public function testCachableSubPage()
	{
		PageCache::enable();
		
		$filter = array(
			'allow' => array('/page/' => 'text/html'),
			'block' => array('/page/Zoek'),
		);
		
		$instance = new PageCache('/page/Zoeken', 'peter_is_gek', $filter, 600);
		$this->assertFalse($instance->cacheable());

		$instance = new PageCache('/page/Andere', 'christiaan_is_gaaf', $filter, 600);
		$this->assertTrue($instance->cacheable());
	}
	
	/**
	 * @depends testCachable
	 */
	public function testSendNotModifiedSince()
	{
		$mock = $this->getMock('Cache_Dir', array('getValue', 'modifiedTime'), array(array()));
		$mock->expects($this->never())->method('getValue');
		$mock->expects($this->exactly(2))->
			method('modifiedTime')->
			with($this->equalTo('/home/peter_is_gek'))->
			will($this->returnValue(strtotime('-3 hour')));
		
		$instance = new PageCache('/home', 'peter_is_gek', array('allow' => array('/home' => 'text/html')), 600, $mock);
		ob_start();
		// Yeah yeah headers already send.. so we surpress warnings
		$old = error_reporting(E_ALL ^ E_WARNING);
		$instance->send(array(
			'If-Modified-Since' => date(DATE_RFC1123, strtotime('-1 hour'))
		));
		error_reporting($old);
		$this->assertEquals(ob_get_clean(), '');
		
		ob_start();
		// Yeah yeah headers already send.. so we surpress warnings
		$old = error_reporting(E_ALL ^ E_WARNING);
		$instance->send(array(
			'If-Modified-Since' => date(DATE_RFC1123, strtotime('-1 hour')),
			'If-None-Match' => '"'.md5($instance->getKey()).'"'
		));
		error_reporting($old);
		$this->assertEquals(ob_get_clean(), '');
	}
}