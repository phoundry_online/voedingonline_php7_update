<?php
class MenuTest extends PHPUnit_Framework_TestCase 
{
	public function testConstruct()
	{
		$title="webpower link";
		$link='webpower.nl';
		$description='link naar webpower...';
		$nivo=0;
		$menu = new Menu($title, $link, $description, $nivo);
		$foutmenu = new Menu("foute link", "paarden.nl", "paarden website ofzoiets", 10);
		
		$this->assertNotNull($menu);
		$this->assertNotNull($foutmenu);
		$this->assertNotEquals($menu->link,$foutmenu->link);
		$this->assertNotEquals($menu,$foutmenu);
	}
}
