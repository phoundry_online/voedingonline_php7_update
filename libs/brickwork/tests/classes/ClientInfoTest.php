<?php
class ClientInfoTest extends PHPUnit_Framework_TestCase
{

	public function testClientInfoFacotry()
	{
		$params=null;
		$clientinfo1 = ClientInfo::factory('web', $params);
		$clientinfo2 = ClientInfo::factory('non-web', $params);

		$this->assertNotEquals($clientinfo1, $clientinfo2);
	}
	
	/**
	 * @dataProvider provider
     */
	public function testData($web, $params)
	{
		$expected = ClientInfo::factory('web', $params);
		$real = ClientInfo::factory($web, $params);
		
		$this->assertEquals($expected, $real);	
	}
	
	/*public function testNotEqual($web, $params)
	{
		$expected = ClientInfo::factory('non-web', $params);
		$real = ClientInfo::factory($web, $params);
		$this->assertNotEquals($expected, $real);
		
	}*/
	
	public function provider()
	{
		$capabilities_mobile = new Client_Capabilities();
		$capabilities_mobile->setDefaults(TRUE);
		
		$capabilities = new Client_Capabilities();
		$capabilities->setDefaults(FALSE);
		
		$web='web';
		return array(
			//data1 - represents a normal browser
			array($web, 
				array('http_accept'=>'text/xml','application/xml','application/xhtml+xml','text/html'),
				array('http_accept_charset'=>'ISO-8859-1'),
				array('http_accept_encoding'=>'gzip,deflate'),
				array('http_accept_language'=>'nl'),
				array('_preferredContent'=>'html'),
				array('preferences'=>'xhtml','html'),
				array('capabilities'=>$capabilities),
				array('_kindMapping'=>'application/xhtml+xml=>xhtml','text/html=>html')			
			),
			//data2 - represents a device browser
			array($web, 
				array('http_accept'=>'application/xhtml+xml','text/html'),
				array('http_accept_charset'=>'utf-8', 'ISO-8859-1'),
				array('http_accept_encoding'=>'gzip,deflate'),
				array('http_accept_language'=>'en-gb'),
				array('_preferredContent'=>'xhtml','html'),
				array('preferences'=>'wap', 'chtml', 'iphone', 'ihtml', 'html', 'xhtml'),
				array('capabilities'=>$capabilities_mobile),
				array('_mobileAgents'=>'w3c','wapp','mobi'),
				array('_kindMapping'=>'text/html=>html', 'text/plain', 'text/vnd.wap.wml=>wml')		
			),
			//data3 - represents something that is web... no params are given
			array($web, null)
			
		);
	}
	
	public function testWebClientInfo()
	{	
		$this->markTestIncomplete();
		$capabilities_mobile = new Client_Capabilities();
		$capabilities_mobile->setDefaults(TRUE);
		
		$client=ClientInfo::factory('web',
			array('http_accept'=>'text/html'),
			array('http_accept_charset'=>'ISO-8859-1'),
			array('http_accept_encoding'=>'gzip,deflate'),
			array('http_accept_language'=>'en-gb'),
			array('_preferredContent'=>'html'),
			array('preferences'=>'wap', 'chtml', 'iphone', 'ihtml', 'html'),
			array('capabilities'=>$capabilities_mobile),
			array('_mobileAgents'=>'wapp','mobi'),
			array('_kindMapping'=>'text/html=>html', 'text/plain', 'text/vnd.wap.wml=>wml')
		);
		
		
		$this->assertTrue($client->isMobile(), " I expected a mobile browser, but it isnt ");
		$this->assertFalse($client->isMobile(), " I expected a normal brwoser but it istn ");
		
		
		$_SERVER["HTTP_USER_AGENT"]="Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3 ";
	}
	
}