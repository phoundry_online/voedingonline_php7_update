<?php
class AddressTest extends PHPUnit_Framework_TestCase
{
	public function testCreateValidAddress()
	{
		// __construct($street, $housenr, $add='', $zip, $city, $country)
		$address = new Address('Anthonie Fokkerstraat', 5, 'bis', '3772 MP', 'Barneveld', new Country('NL', 'Nederland'));
		$this->assertTrue( is_object($address) );
		$this->assertEquals( "Anthonie Fokkerstraat 5bis\n3772 MP Barneveld\nNederland", (string)$address );
		$this->assertEquals( "Anthonie Fokkerstraat", $address->streetName );
		$this->assertEquals( "5", $address->houseNumber );
		$this->assertEquals( "bis", $address->houseNumberAddition );
		$this->assertEquals( "3772 MP", $address->zip->zip );
		$this->assertEquals( "Barneveld", $address->city );
		$this->assertEquals( "Nederland", $address->country->name );
	}
	
}
