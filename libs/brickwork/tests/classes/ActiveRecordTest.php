<?php
class ActiveRecordTest extends UnitTest_TestCase_Database
{
	protected function getDataSet()
	{
		return $this->createFlatXMLDataSet( PROJECT_DIR . '/tests/resources/ActiveRecord_BankAccount.xml');
	}
	
    protected function getSetUpOperation()
	{
		return $this->getOperations()->CLEAN_INSERT();
	}	
	
	public function testCreateExistingTable()
	{
		$this->markTestIncomplete("AR/Database not implemented");	
	}
	
	public function testCreateNonExistingTable()
	{
		$this->markTestIncomplete("AR/Database not implemented");
	}
}