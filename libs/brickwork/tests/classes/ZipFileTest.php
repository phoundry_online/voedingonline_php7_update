<?php
class ZipFileTest extends PHPUnit_Framework_TestCase
{
	public function testUnzipValidZip()
	{
		$this->markTestIncomplete();
	}
	
	public function testUnzipInvalidZip()
	{
		$this->markTestIncomplete();
	}
	
	public function testFileInValidZip()
	{
		$this->markTestIncomplete();
	}
	
	public function testFileInInvalidZip()
	{
		$this->markTestIncomplete();
	}
	
	public function testFileNotInValidZip()
	{
		$this->markTestIncomplete();
	}
	
	public function testListFilesInValidZip()
	{
		$this->markTestIncomplete();
	}
	
	public function testListFilesInNonValidZip()
	{
		$this->markTestIncomplete();
	}
	
	public function testNoUnzipAvailable()
	{
		$this->markTestIncomplete();
	}
}