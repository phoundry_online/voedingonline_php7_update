<?php
class ErrorTest extends PHPUnit_Framework_TestCase
{
	
	public function testErrorConstructor()
	{
		$errornr=1136;
		$errstr="string";
		$errfile="file.php";
		$errline="line";
		$backtrace=array();
		
		$error1 = new ErrorVoedingonline($errornr, $errstr, $errfile, $errline, $backtrace);
		$error2 = new ErrorVoedingonline($errornr, $errstr, $errfile, $errline, $backtrace);
				
		$this->assertNotNull($error1);
		$this->assertTrue($error1->isError($error1));
		
		$this->assertNotNull($error2);
		$this->assertTrue($error2->isError($error2));
		
		/*@todo exceptie testen (atribute niveau)3 */
		
		if($this->assertType("Error", $error1))
		{
			fail("error1 is not a instace of Error, in case of php5, else you can ignore this message");
		}
		if($this->assertType("Error", $error2))
		{
			fail("error2 is not a instace of Error, in case of php5, else you can ignore this message");
		}
	}

	public function testDBErrorFile()
	{
		
		$basedir="../";
		$this->assertFileExists($basedir.'classes/Error/MySQLErrorCodes.include.php');
		
	}
	
	public function testDBErrorConstruct()
	{
		$errno=1136;
		$errstr="Tabel bestaat niet";
		$errfile="file";
		$errline="line";
		$backtrace=array();
				
		$error1 = new ErrorVoedingonline($errno, $errstr, $errfile, $errline, $backtrace);
		$error2 = new ErrorVoedingonline($errno, $errstr, $errfile, $errline, $backtrace);
		
		$this->assertNotNull($error1);
		$this->assertTrue($error1->isError($error1));
		
		$this->assertNotNull($error2);
		$this->assertTrue($error1->isError($error2));
		
		if($this->assertType("Error", $error1))
		{
			fail("error1 is not a instace of Error, in case of php5, else you can ignore this message");
		}
		if($this->assertType("Error", $error2))
		{
			fail("error2 is not a instace of Error, in case of php5, else you can ignore this message");
		}
		
		$error = $error1->__get("errstr");
		$this->assertNotNull($error);
		$error=null;
		$error = $error1->__get("errno");
		$this->assertNotNull($error);
		
		/** @todo controleren, door jorgen of peter!!! */
		//$this->markTestIncomplete();
		
	}
	
	/** @todo controleren,,, klopt iets niet... */
	public function DISABLEDtestExceptions()
    {
    	$this->setExpectedException('AttributeSettingNotAllowedException');
        try
        {
        	throw new Exception('AttributeSettingNotAllowedException');
        }
        catch(AttributeSettingNotAllowedException $e)
        {
        	return;
        }
        fail("De exceptie wordt niet afgevangen...");
    }
	/** @todo fixen.
	*dit is uit de documentatie 
	*/
    public function DISABLEDtestException() 
    {
    	try
        {
        	
        }
        catch (AttributeSettingNotAllowedException $expected) {
            return;
        }
 
        $this->fail('An expected exception has not been raised.');
    }
    
}
?>