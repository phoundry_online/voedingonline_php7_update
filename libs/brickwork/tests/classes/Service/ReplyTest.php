<?php
class Service_ReplyTest extends PHPUnit_Framework_TestCase
{
	public function testActivateXML()
	{
		$reply = new Service_Reply(true,'xml');

		$this->assertTrue( is_object($reply) );
		$this->assertEquals( 'xml', (string)$reply );
		$this->assertTrue( $reply->setXMLContentOutput() );
		$this->assertXmlStringEqualsXmlString( '<?xml version="1.0" encoding="utf-8"?>' . "\n" . '<content>xml</content>' . "\n", (string)$reply );
	}
	
	public function testCreateWithStatusFalseOnly()
	{
		$reply = new Service_Reply(false);
		
		$this->assertFalse($reply->status);
		$this->assertNull($reply->content);
		$this->assertEmptyArray($reply->messages);
	}
	
	public function testCreateWithStatusTrueOnly()
	{
		$reply = new Service_Reply(true);
		
		$this->assertTrue($reply->status);
		$this->assertNull($reply->content);
		$this->assertEmptyArray($reply->messages);
	}

	public function testCreateStatusTrueWithStringContent()
	{
		$reply = new Service_Reply(true,'hello');
		
		$this->assertTrue($reply->status);
		$this->assertEquals( 'hello', $reply->content);
		$this->assertEquals( 'hello', (string)$reply );
		$this->assertEmptyArray($reply->messages);
	}
	
	public function testCreateStatusTrueWithArrayContent()
	{
		$test  = Array("hello" => "world" );
		
		$reply = new Service_Reply(true,$test);
		
		$this->assertTrue($reply->status);
		$this->assertEquals( $test, $reply->content);
		$this->assertEmptyArray($reply->messages);
		$this->assertNonEmptyArray( $reply->content );
		$this->assertEquals( 'Array', (string)$reply );
		
		$reply->setXMLContentOutput();
		
		$this->assertXmlStringEqualsXmlString('<?xml version="1.0" encoding="utf-8"?>' . "\n" . '<content type="Array"><hello>world</hello></content>' . "\n", (string)$reply );
		
		$test  = Array("hello","world" );
		
		$reply = new Service_Reply(true,$test);
		$reply->setXMLContentOutput();
		$this->assertXmlStringEqualsXmlString('<?xml version="1.0" encoding="utf-8"?>' . "\n" . '<content type="Array"><entry no="0">hello</entry><entry no="1">world</entry></content>' . "\n", (string)$reply );
	}

	public function testCreateStatusTrueWithObjectContent()
	{
		$test  = new Service_Reply(false);
		
		$reply = new Service_Reply(true,$test);
		
		$this->assertTrue($reply->status);
		$this->assertEquals( $test, $reply->content);
		$this->assertEmptyArray($reply->messages);
		$this->assertTrue( $reply->content instanceof Service_Reply );
		$this->assertEquals( '', (string)$reply );
		
		$reply->setXMLContentOutput();
		
		$xml = (string)$reply;
		
		$this->assertXmlStringEqualsXmlString('<?xml version="1.0" encoding="utf-8"?>' . "\n" . '<content type="Service_Reply"><status type="boolean">false</status><messages type="Array"/><content/><startNode>content</startNode></content>' . "\n",  $xml );
	}
	
	public function testCreateWithMessages()
	{
		$reply = new Service_Reply(true,null,'message');
		
		$this->assertNonEmptyArray( $reply->messages );
		$this->assertEquals(1, count($reply->messages) );
		$this->assertEquals('message', $reply->messages[0]);
		
		$reply = new Service_Reply(true,null,Array('message'));
		$this->assertNonEmptyArray( $reply->messages );
		$this->assertEquals(1, count($reply->messages) );
		$this->assertEquals('message', $reply->messages[0]);
		
		$reply = new Service_Reply(true,null,Array('message','message2'));
		$this->assertNonEmptyArray( $reply->messages );
		$this->assertEquals(2, count($reply->messages) );
		$this->assertEquals('message', $reply->messages[0]);
		$this->assertEquals('message2', $reply->messages[1]);
	}
	
	protected function assertEmptyArray( $a )
	{
		$this->assertTrue( is_array($a) );
		$this->assertEquals( 0, count($a) );
		$this->assertEquals( Array(), $a );
	}
	
	protected function assertNonEmptyArray( $a )
	{
		$this->assertTrue( is_array($a) );
		$this->assertGreaterThan( 0, count($a) );
	}
	
}