<?php
class Service_Representation_WsdlTest extends PHPUnit_Framework_TestCase
{
	protected	$_wsdl;
	
	public function setup()
	{
		$this->_wsdl = new Service_Representation_Wsdl();
	}
	
	public function teardown()
	{
		$this->_wsdl = null;
	}
	
	
	public function testWSDLNoMethods()
	{
		$this->markTestIncomplete();
		
		$this->_wsdl->setModule( new WSDLNoMethods() );
		
		$wsdl = '<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions
    name="WSDLNoMethodInfo"
    xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
    xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
    xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
    xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
    xmlns:tns="http://namespace.webpower.nl/WSDLNoMethod.wsdl"
    xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/"
    targetNamespace="http://namespace.webpower.nl/WSDLNoMethod.wsdl">
    <wsdl:types>
        <xs:schema targetNamespace="http://namespace.webpower.nl/WSDLNoMethod.xsd" elementFormDefault="qualified"/>
    </wsdl:types>
        <wsdl:portType name="WSDLNoMethodPortType">
        </wsdl:portType>
    <wsdl:binding name="WSDLNoMethodBinding" type="tns:WSDLNoMethodPortType">
        <soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
            </wsdl:binding>
    <wsdl:service name="WSDLNoMethodService">
        <wsdl:port name="WSDLNoMethodPort" binding="tns:WSDLNoMethodBinding">
            <soap:address location="http:///index.php/service/soap/WSDLNoMethod"/>
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>';
		$this->assertXMLStringEqualsXMLString( $wsdl, $this->_wsdl->out(true));
	}

	public function testWSDLPrivateMethod()
	{
		$this->markTestIncomplete();
		
		$this->_wsdl->setModule( new WSDLPrivateMethod() );
		
		$wsdl = '<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions
    name="WSDLPrivateMethodInfo"
    xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
    xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
    xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
    xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
    xmlns:tns="http://namespace.webpower.nl/WSDLPrivateMethod.wsdl"
    xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/"
    targetNamespace="http://namespace.webpower.nl/WSDLPrivateMethod.wsdl">
    <wsdl:types>
        <xs:schema targetNamespace="http://namespace.webpower.nl/WSDLPrivateMethod.xsd" elementFormDefault="qualified"/>
    </wsdl:types>
        <wsdl:portType name="WSDLPrivateMethodPortType">
        </wsdl:portType>
    <wsdl:binding name="WSDLPrivateMethodBinding" type="tns:WSDLPrivateMethodPortType">
        <soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
            </wsdl:binding>
    <wsdl:service name="WSDLPrivateMethodService">
        <wsdl:port name="WSDLPrivateMethodPort" binding="tns:WSDLPrivateMethodBinding">
            <soap:address location="http:///index.php/service/soap/WSDLPrivateMethod"/>
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>';
		$this->assertXMLStringEqualsXMLString( $wsdl, $this->_wsdl->out(true));
	}

	public function testWSDLProtectedMethod()
	{
		$this->markTestIncomplete();
		
		$this->_wsdl->setModule( new WSDLProtectedMethod() );
		
		$wsdl = '<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions
    name="WSDLProtectedMethodInfo"
    xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
    xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
    xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
    xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
    xmlns:tns="http://namespace.webpower.nl/WSDLProtectedMethod.wsdl"
    xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/"
    targetNamespace="http://namespace.webpower.nl/WSDLProtectedMethod.wsdl">
    <wsdl:types>
        <xs:schema targetNamespace="http://namespace.webpower.nl/WSDLProtectedMethod.xsd" elementFormDefault="qualified"/>
    </wsdl:types>
        <wsdl:portType name="WSDLProtectedMethodPortType">
        </wsdl:portType>
    <wsdl:binding name="WSDLProtectedMethodBinding" type="tns:WSDLProtectedMethodPortType">
        <soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
            </wsdl:binding>
    <wsdl:service name="WSDLProtectedMethodService">
        <wsdl:port name="WSDLProtectedMethodPort" binding="tns:WSDLProtectedMethodBinding">
            <soap:address location="http:///index.php/service/soap/WSDLProtectedMethod"/>
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>';
		
		$this->assertXMLStringEqualsXMLString( $wsdl, $this->_wsdl->out(true));
	}
	
	
	public function testAllOptions()
	{
		$this->markTestIncomplete();
		
		$this->_wsdl->setModule( new WSDLAllOptions() );
		//file_put_contents( dirname(__FILE__) . '/wsdlall.xml', $this->_wsdl->out(true) );
		$this->assertXMLStringEqualsXMLString( file_get_contents( dirname(__FILE__) . '/wsdlall.xml'), $this->_wsdl->out(true));
	}
}

class WSDLNoMethods implements Service_Module_Interface
{
	public	$code = 'WSDLNoMethod';
	
}

class WSDLPrivateMethod implements Service_Module_Interface
{
	public	$code = 'WSDLPrivateMethod';
	
	/**
	 * Enter description here...
	 *
	 * @param 	string $arguments
	 * @return	string
	 */
	private function WSDLtestRequest( $arguments )
	{
		
	}
}

class WSDLProtectedMethod implements Service_Module_Interface
{
	public	$code = 'WSDLProtectedMethod';
	
	protected function WSDLtestRequest( $arguments )
	{
		
	}
}

class WSDLAllOptions implements Service_Module_Interface
{
	public $code = 'all';
	
	
	public function noArgumentRequest()
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 */
	public function noArgumentEmptyDocBlockRequest()
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @return string
	 */
	public function noArgumentsDocBlockRequest()
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @param 	string $str
	 * @return	
	 */
	public function oneStringArgRequest( $str )
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @param Array $arr
	 */
	public function oneArrayArgRequest( $arr )
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @param Service_Reply $ob
	 */
	public function oneObjectArgRequest( $ob )
	{
		
	}
	
	/**
	 * 
	 * @param	bool	$bool
	 * @return	boolean
	 */
	public function oneBoolArgRequest( $bool )
	{
		
	}
	
	/**
	 * 
	 * @param	boolean	$bool
	 * @return	bool
	 */
	public function oneBooleanArgRequest( $bool )
	{
		
	}
	
	/**
	 * 
	 * @param	int	$i
	 * @return	integer
	 */
	public function oneIntArgRequest( $i )
	{
		
	}
	
	
	/**
	 * 
	 * @param	integer	$i
	 * @return	int
	 */
	public function oneIntegerArgRequest( $i )
	{
		
	}
	
}