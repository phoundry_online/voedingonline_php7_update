<?php
class Service_Representation_JsonTest extends PHPUnit_Framework_TestCase
{
	protected	$_rep;
	
	protected function setUp()
	{
		$this->_rep = new Service_Representation_Json();
	}
	
	protected function tearDown()
	{
		unset($this->_rep);
	}
	
	public function testFailedResponseNoContentStringMessage()
	{
		$reply = new Service_Reply(false,null,'Test string');
		
		$this->assertTrue( $this->_rep->setReply($reply) );
		$this->assertEquals( '{"status":false,"messages":["Test string"],"content":null,"startNode":"content"}', $this->_rep->out(true) );
		
		$output 	= $this->_rep->out(true);
		$converted	= json_decode($output);
		
		$this->assertTrue( is_object($converted) );
		$this->assertEquals( $reply->status , $converted->status );
		$this->assertEquals( $reply->messages , $converted->messages );
		$this->assertNull( $converted->content );
	}
	
	public function testNoReply()
	{
		$this->assertEquals('null',$this->_rep->out(true));
	}
}