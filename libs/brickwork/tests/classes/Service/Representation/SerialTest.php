<?php
class Service_Representation_SerialTest extends PHPUnit_Framework_TestCase
{
	protected	$_rep;
	
	protected function setUp()
	{
		$this->_rep = new Service_Representation_Serial();
	}
	
	protected function tearDown()
	{
		unset($this->_rep);
	}
	
	public function testNoReply()
	{
		$output = $this->_rep->out(true);
		$this->assertEquals("N;",$output);
		$this->assertNull( unserialize($output) );
	}
	
	public function testFaultReply()
	{
		$reply = new Service_Reply(false);
		
		$this->_rep->setReply($reply);
		$this->assertEquals('a:3:{s:6:"status";b:0;s:8:"messages";a:0:{}s:7:"content";N;}', $this->_rep->out(true));
		
		$response = $this->is_serialized($this->_rep->out(true)) ? @unserialize( $this->_rep->out(true) ) : $this->_rep->out(true);
		
		$this->assertValidResponse($response);
		$this->assertFalse($response['status']);
		$this->assertTrue(is_array($response['messages']));
		$this->assertNull( $response['content'] );
	}

	static public function is_serialized($data, $strict = true)
    {
        // if it isn't a string, it isn't serialized.
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace = strpos($data, '}');
            // Either ; or } must exist.
            if (false === $semicolon && false === $brace)
                return false;
            // But neither must be in the first X characters.
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            // or else fall through
            case 'a' :
            case 'O' :
                return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }
	
	public function testOkReply()
	{
		$reply = new Service_Reply(true);
		
		$this->_rep->setReply($reply);
		$this->assertEquals('a:3:{s:6:"status";b:1;s:8:"messages";a:0:{}s:7:"content";N;}', $this->_rep->out(true));
	}
	
	public function testArrayContentReply()
	{
		$test  = Array("a","b" => "c");
		$reply = new Service_Reply(true, $test );
		
		$this->_rep->setReply($reply);
		
		$response = $this->_rep->out(true);
		$this->assertEquals('a:3:{s:6:"status";b:1;s:8:"messages";a:0:{}s:7:"content";a:2:{i:0;s:1:"a";s:1:"b";s:1:"c";}}', $response);
		$out = $this->is_serialized($response) ? @unserialize($response) : $response;
		$this->assertValidResponse($out);
		$this->assertEquals($test,$out['content']);
		
	}
	
	public function assertValidResponse( $response )
	{
		$this->assertTrue( is_array($response) );
		$this->assertArrayHasKey( 'status', $response );
		$this->assertArrayHasKey( 'messages', $response );
		$this->assertArrayHasKey( 'content', $response );
	}
}
