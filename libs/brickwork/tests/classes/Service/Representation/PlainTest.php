<?php
class Service_Representation_PlainTest extends PHPUnit_Framework_TestCase
{
	protected	$_rep;
	
	protected function setUp()
	{
		$this->_rep = new Service_Representation_Plain();
	}
	
	protected function tearDown()
	{
		unset($this->_rep);
	}
	
	public function testNoReply()
	{
		$output = $this->_rep->out(true);
		$this->assertEquals("State: FAIL\n",$output);
	}
	
	public function testFaultReply()
	{
		$reply = new Service_Reply(false);
		
		$this->_rep->setReply($reply);
		$this->assertEquals("State: FAIL\nContent:\n", $this->_rep->out(true));
	}
	
	public function testOkReply()
	{
		$reply = new Service_Reply(true);
		
		$this->_rep->setReply($reply);
		$this->assertEquals("State: OK\nContent:\n", $this->_rep->out(true));
	}
	
	public function testArrayContentReply()
	{
		$test  = Array("a","b" => "c");
		$reply = new Service_Reply(true, $test );
		
		$this->_rep->setReply($reply);
		
		$response = $this->_rep->out(true);
		$this->assertEquals("State: OK\nContent:\nArray", $response);
		
	}
	
	public function testObjectContentReply()
	{
		$test = new Service_Reply(true);
		$reply = new Service_Reply(true, $test );
		
		$this->_rep->setReply($reply);
		
		$response = $this->_rep->out(true);
		$this->assertEquals("State: OK\nContent:\n", $response);
	}
	
	public function assertValidResponse( $response )
	{
		$this->assertTrue( is_array($response) );
		$this->assertArrayHasKey( 'status', $response );
		$this->assertArrayHasKey( 'messages', $response );
		$this->assertArrayHasKey( 'content', $response );
	}
}