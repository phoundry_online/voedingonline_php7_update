<?php
class Service_External_Youtube_VideoTest extends PHPUnit_Framework_TestCase
{
	public function testConstructor()
	{
		$video = new Service_External_Youtube_Video('http://www.youtube.com/v/zKoiWV9u4y0?autoplay=1&loop=0&rel=0');
		$this->assertEquals(
			'zKoiWV9u4y0',
			$video->__toString()
		);
		$video = new Service_External_Youtube_Video('http://www.youtube.com/watch?v=zKoiWV9u4y0&autoplay=0&loop=1&rel=1#t=1m30s');
		$this->assertEquals(
			'zKoiWV9u4y0',
			$video->__toString()
		);
		$video = new Service_External_Youtube_Video('http://img.youtube.com/vi/zKoiWV9u4y0/0.jpg');
		$this->assertEquals(
			'zKoiWV9u4y0',
			$video->__toString()
		);
	}
	
	public function testSwfUrl()
	{
		$video = new Service_External_Youtube_Video('zKoiWV9u4y0');
		
		$this->assertEquals(
			'http://www.youtube.com/v/zKoiWV9u4y0?autoplay=1&loop=0&rel=0',
			$video->swfUrl()
		);
		
		$video = new Service_External_Youtube_Video('zKoiWV9u4y0');
		$this->assertEquals(
			'http://www.youtube.com/v/zKoiWV9u4y0?autoplay=0&loop=1&rel=1&start=90',
			$video->swfUrl(false, true, true, 90)
		);
	}
	
	public function testVideoPageUrl()
	{
		$video = new Service_External_Youtube_Video('zKoiWV9u4y0');
		
		$this->assertEquals(
			'http://www.youtube.com/watch?v=zKoiWV9u4y0&autoplay=1&loop=0&rel=0',
			$video->videoPageUrl()
		);
		
		$video = new Service_External_Youtube_Video('zKoiWV9u4y0');
		$this->assertEquals(
			'http://www.youtube.com/watch?v=zKoiWV9u4y0&autoplay=0&loop=1&rel=1#t=1m30s',
			$video->videoPageUrl(false, true, true, 90)
		);
	}
	
	public function testGetImage()
	{
		$video = new Service_External_Youtube_Video('zKoiWV9u4y0');
		
		$this->assertEquals(
			'http://img.youtube.com/vi/zKoiWV9u4y0/0.jpg',
			$video->getImage()
		);
		
		$this->assertEquals(
			'http://img.youtube.com/vi/zKoiWV9u4y0/0.jpg',
			$video->getImage(0)
		);
		
		$this->assertEquals(
			'http://img.youtube.com/vi/zKoiWV9u4y0/1.jpg',
			$video->getImage(1)
		);
		
		$this->assertEquals(
			'http://img.youtube.com/vi/zKoiWV9u4y0/2.jpg',
			$video->getImage(2)
		);
		
		$this->assertEquals(
			'http://img.youtube.com/vi/zKoiWV9u4y0/3.jpg',
			$video->getImage(3)
		);
		
		$this->assertEquals(
			'http://img.youtube.com/vi/zKoiWV9u4y0/4.jpg',
			$video->getImage(4)
		);
	}
	
	public function testToString()
	{
		$video = new Service_External_Youtube_Video('zKoiWV9u4y0');

		$this->assertTrue('zKoiWV9u4y0' == $video);
		
		$this->assertEquals(
			'zKoiWV9u4y0',
			$video->__toString()
		);
	}
	
	public function testUrlToKey()
	{
		$video = new Service_External_Youtube_Video('zKoiWV9u4y0');
		
		$this->assertEquals(
			'zKoiWV9u4y0',
			Service_External_Youtube_Video::urlToKey($video->swfUrl())
		);
		$this->assertEquals(
			'zKoiWV9u4y0',
			Service_External_Youtube_Video::urlToKey($video->videoPageUrl())
		);
		$this->assertEquals(
			'zKoiWV9u4y0',
			Service_External_Youtube_Video::urlToKey($video->getImage())
		);
		
		$this->assertFalse(Service_External_Youtube_Video::urlToKey('http://www.webpower.nl/v/zKoiWV9u4y0'));
		$this->assertFalse(Service_External_Youtube_Video::urlToKey('/hoi'));
		$this->assertFalse(Service_External_Youtube_Video::urlToKey(''));
	}
}