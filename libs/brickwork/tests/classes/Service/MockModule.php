<?php
class MockModule implements Service_Module_Interface
{
	public $code = 'all';
	
	
	public function noArgumentRequest()
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 */
	public function noArgumentEmptyDocBlockRequest()
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @return string
	 */
	public function noArgumentsDocBlockRequest()
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @param 	string $str
	 * @return	
	 */
	public function oneStringArgRequest( $str )
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @param Array $arr
	 */
	public function oneArrayArgRequest( $arr )
	{
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @param Service_Reply $ob
	 */
	public function oneObjectArgRequest( $ob )
	{
		
	}
	
	/**
	 * 
	 * @param	bool	$bool
	 * @return	boolean
	 */
	public function oneBoolArgRequest( $bool )
	{
		
	}
	
	/**
	 * 
	 * @param	boolean	$bool
	 * @return	bool
	 */
	public function oneBooleanArgRequest( $bool )
	{
		
	}
	
	/**
	 * 
	 * @param	int	$i
	 * @return	integer
	 */
	public function oneIntArgRequest( $i )
	{
		
	}
	
	
	/**
	 * 
	 * @param	integer	$i
	 * @return	int
	 */
	public function oneIntegerArgRequest( $i )
	{
		
	}
}
