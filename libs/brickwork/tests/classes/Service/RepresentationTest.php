<?php
require_once('Framework.class.php'); // This file includes Representation which we need

class Service_RepresentationTest extends PHPUnit_Framework_TestCase
{
	protected function setup()
	{
		global $PHprefs;
		
		if ( !isset($PHprefs['baseDir']))
			$PHprefs['baseDir'] = '';
	}
	
	public function testFactoryExistingDisplay()
	{
		$f = Service_Representation::factory('plain');
		
		$this->assertTrue( is_object($f) );
		$this->assertTrue( is_subclass_of($f, 'Service_Representation') );
	}
	
	public function testFactoryNonExistingDisplay()
	{
		$f = Service_Representation::factory('hocuspocus');
		
		$this->assertNull($f);
	}
	
	public function testFactoryNullDisplay()
	{
		$f = Service_Representation::factory(null);
		
		$this->assertNull($f);
	}
	
	public function testFactoryEmptyDisplay()
	{
		$f = Service_Representation::factory('');
		
		$this->assertNull($f);
	}
	
}