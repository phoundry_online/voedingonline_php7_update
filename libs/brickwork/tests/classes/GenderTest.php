<?php
class GenderTest extends PHPUnit_Framework_TestCase
{
	
	protected	$_gender;
	
	protected function setUp()
	{
		$this->_gender = new Gender();
	}
	
	protected function tearDown()
	{
		$this->_gender = null;
	}
	
	public function testValueMapping()
	{
		//$this->assertEquals( Gender::unknown, $this->_gender->value2Key('bla'));
		$this->assertEquals( Gender::male, $this->_gender->value2key('g_male'));
		$this->assertEquals( Gender::female, $this->_gender->value2key('g_female'));
	}
}
