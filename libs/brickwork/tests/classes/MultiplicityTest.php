<?php
class MultiplicityTest extends PHPUnit_Framework_TestCase
{
	public function testAbove()
	{
		$i = 1;
		$j = 2;
		
		$ok= Multiplicity::greater($j,$i);
		$notOk= Multiplicity::greater($i,$j);
		 
		$this->assertTrue($ok);
		$this->assertFalse($notOk);
	}
	
	public function testBelow()
	{
		$i = 1;
		$j = 2;
				
		$notOk= Multiplicity::less($j,$i);
		$ok= Multiplicity::less($i,$j);
		 
		$this->assertFalse($notOk);
		$this->assertTrue($ok);
		
	}
}
