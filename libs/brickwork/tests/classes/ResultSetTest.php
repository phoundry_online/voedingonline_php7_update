<?php
class ResultSetTest extends UnitTest_TestCase_Database
{
	protected function getDataSet()
	{
		return $this->createFlatXMLDataSet( UNITTEST_RESOURCES . '/ActiveRecord_BankAccount.xml');
	}
	
	
	public function testGetAsArray()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$this->assertTrue($res instanceof ResultSet);
		$tmp = $res->getAsArray();
		$this->assertTrue( is_array($tmp) );
		$this->assertEquals(3, count($tmp));
		
		// Fetch with limit, also for the second time
		$tmp = $res->getAsArray(1);
		$this->assertTrue( is_array($tmp));
		$this->assertEquals( 1, count($tmp) );
		
		// Fetch with bogus limit
		$tmp = $res->getAsArray(10);
		$this->assertTrue( is_array($tmp) );
		$this->assertEquals(3, count($tmp));
		
		$this->assertTrue( is_array($tmp[0]));
		$this->assertTrue( isset( $tmp[0]['account_number'] ));
		$this->assertTrue( isset( $tmp[0]['balance'] ));
		$this->assertEquals( '15934903649620486', $tmp[0]['account_number'] );
		$this->assertEquals( 100.0, $tmp[0]['balance'] );
	}
	
	public function testResultAttributeIsFilled()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$this->assertTrue($res instanceof ResultSet);
		$this->assertEquals(3, count($res->result));
		$this->assertTrue( is_object($res->result[0]));
		
		$this->assertTrue( isset( $res->result[0]->account_number ));
		$this->assertTrue( isset( $res->result[0]->balance ));
		$this->assertEquals( '15934903649620486', $res->result[0]->account_number );
		$this->assertEquals( 100.0, $res->result[0]->balance );
	}
	
	public function testgetAsObjectArray()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$this->assertEquals( $res->result, $res->getAsObjectArray());
		$tmp = $res->getAsObjectArray();
	}
	
	public function testFirstWithFilledResultSet()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$row = $res->first();
		
		$this->assertTrue( is_object($row) );
		$this->assertTrue( isset($row->account_number) );
		$this->assertTrue( isset($row->balance));
		
		$this->assertEquals( '15934903649620486', $row->account_number );
	}
	
	public function testFirstWithEmptyResultSet()
	{
		$res = DB::iQuery('SELECT * FROM bank_account WHERE balance = -1');
		
		$row = $res->first();
		
		$this->assertFalse( $row );
	}
	
	public function testIteration()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$count = 0;
		
		foreach( $res as $row)
		{
			$count++;
			$this->assertTrue( is_object($row) );
		}
		
		$this->assertEquals( 3, $count );
		
		// Retry for second run (rewind functionality)
		$count = 0;
		
		foreach( $res as $row)
		{
			$count++;
			$this->assertTrue( is_object($row) );
		}
		
		$this->assertEquals( 3, $count );
	}
	
	public function testCountable()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$this->assertEquals(3, count($res) );
		$this->assertEquals( $res->numRows, count($res) );
		$this->assertEquals( $res->num_rows, count($res) );
		$this->assertEquals( $res->total_rows, count($res) );
	}
	
	public function testOffsets()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$this->assertTrue( isset($res[0]) );
		$this->assertTrue( isset($res[2]) );
		$this->assertFalse( isset($res[3]) );
		
		// Offset Get test
		$this->assertEquals( $res->result[0], $res[0]);
		$this->assertEquals( $res->result[1], $res[1]);
		$this->assertEquals( $res->result[2], $res[2]);
	}
	
	/**
	 * @expectedException BadMethodCallException
	 */
	public function testUnsetOffset()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		unset($res[0]); 
	}
	
	/**
	 * @expectedException BadMethodCallException
	 */
	public function testSetOffset()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		// Offset Set should not work
		$res[0] = 'test';
	}
}