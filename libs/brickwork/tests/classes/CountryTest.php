<?php
class CountryTest extends PHPUnit_Framework_TestCase
{
	public function testCountryConstruct()
	{
		$country = new Country('NL', "Netherlands");
		$this->assertTrue( is_object($country) );
		$this->assertEquals($country->name, "Netherlands");
		$this->assertEquals($country->iso, 'NL');
	}
		
	public function testCountryList()
	{
		$this->markTestIncomplete();
		
		$country = array('code' => "GB", 'name' => "Verenigd Koninkrijk");
		$country_list_2 = Country::getCountryList2();
		$wrong=true;
		
		$this->assertArrayHasKey("Verenigd Koninkrijk", $country_list_2);
		
		foreach($country_list_2 as $check)
		{
			if($check->iso === "GB")
			{
				$wrong=false;
			}
		}
		if($wrong)
		{
			$this->fail("Blijkbaar staat GB niet in de Country lijst.");
			
		}
	}
	
	public function testisObject()
	{
		$country = new Country('NL', "Netherlands");
		$country_fout = new Country('XX', "Boe");
		$this->assertTrue(True, Country::isCountry($country));
		$this->assertFalse(False, Country::isCountry("NL"));
		$this->assertFalse(False, Country::isCountry("Nederland"));
		$this->assertFalse(False, Country::isCountry($country_fout));
	}
	
	public function testGetCountryByBName()
	{
		$this->markTestIncomplete();
		
		$expect = new Country('DE', "Duitsland");
		$data = Country::getCountryList();
		$real = Country::getCountryByName("Duitsland");//hier komt null uit
		$this->assertEquals($expect->name, $real );
	}
	
	public function testCountryValidIso()
	{
		$this->assertTrue(TRUE, Country::validISO('NL'));
		$this->assertFalse(False, Country::validISO('XX'));
		$this->assertFalse(False, Country::validISO("Netherlands")); //must be iso code
	}
	/**
	@todo werkt niet to string name is niet public
	*/	
	public function testToString()
	{
		$this->markTestIncomplete();
		
		$this->assertFalse(False, Country::__toString());
		$country = new Country('NL', "Netherlands");
		$this->assertTrue(True, Country::__toString());
	}
}
?>