<?php
class EmailTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers Email::__construct
	 */
	public function testCreateValidEmail()
	{
		$email = new Email('jorgen@webpower.nl');
		$this->assertTrue( is_object($email) );
		$this->assertEquals( 'jorgen@webpower.nl', (string)$email );
		$this->assertEquals( 'jorgen@webpower.nl', $email->address );
	}
	
	/**
	 * Test 2 addresses for validation, one to be valid and one invalid
	 * @covers Email::isValid
	 */
	public function testIsValidWithReturnTrue(){
		$result = Email::isValid('jorgen@webpower.nl');
		$this->assertTrue($result);
		$result = Email::isValid('not a valid address');
		$this->assertFalse($result);

	}

	/**
	 * Test rot13 display of an address 
	 * @covers Email::rot13
	 */
	public function testRot13(){
		$email = new Email('jorgen@webpower.nl');
		$this->assertTrue( is_object($email) );
		$this->assertEquals( str_rot13('jorgen@webpower.nl'), $email->rot13() );
	}
}
