<?php
class CommandLineTest extends PHPUnit_Framework_TestCase
{
	public function testParseArgs()
	{
		$c = new CommandLine("test.php", array(
			'--x=Tieten',
			'-y',
			'Bier',
			'--henk="Sjaak trekhaak"'
		));
		
		$this->assertEquals("Tieten", $c->getArg('x'));
		$this->assertEquals("Bier", $c->getArg('y'));
		$this->assertEquals('"Sjaak trekhaak"', $c->getArg('henk'));
	}
}