<?php
class BrickWorkCryptTest extends PHPUnit_Framework_TestCase
{
	public function testEncode()
	{
		$string = "geheim";
		
		$this->assertTrue(is_string(BrickWorkCrypt::encode($string)), 'Encode method does not output a string.');
		$this->assertEquals(BrickWorkCrypt::decode(BrickWorkCrypt::encode($string)), $string, 'Decoding an encoded string does not equal the original string');
		
		$string = "testString";
		
		$this->assertFalse(BrickWorkCrypt::decode($string), 'Normal string is encoded');
	}
	
	public function testUrlEncode()
	{
		$array = array(
			'test'		=> 'test',
			'hello'		=> 'world',
			'success'	=> 1
		);
		
		$this->assertTrue(is_string(BrickWorkCrypt::urlEncode($array)), 'UrlEncode method does not output a string');
		$this->assertEquals(BrickWorkCrypt::urlDecode(BrickWorkCrypt::urlEncode($array)), $array, 'UrlDecoding an urlEncoded array does not equal the original array');
		
		$string = "testString";
		
		$this->assertFalse(BrickWorkCrypt::urlDecode($string), 'Normal string is encoded.');
	}
	
	public function testArrayEncode()
	{
		$array = array(
			'test'		=> 'test',
			'hello'		=> 'world',
			'success'	=> 1
		);
		
		$this->assertTrue(is_string(BrickWorkCrypt::encodeArray($array)), 'EncodeArray method does not output a string');
		$this->assertEquals(BrickWorkCrypt::decodeArray(BrickWorkCrypt::encodeArray($array)), $array, 'ArrayDecoding an arrayEncoded array does not equal the original array');
		
		$string = "testString";
		
		$this->assertEquals(BrickWorkCrypt::decodeArray($string), array(), 'EncodeArray method of a random string does not output an empty array.');
	}
}