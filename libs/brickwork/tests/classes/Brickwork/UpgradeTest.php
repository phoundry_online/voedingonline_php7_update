<?php
class Brickwork_UpgradeTest extends PHPUnit_Framework_TestCase
{
	private $upgrade;
	private $stub_db;
	private $stub_db_sync;
	private $stub_phoundry_sync;
	
	public function setUp()
	{
		$this->stub_db = $this->getMock('DB_Adapter');
		$this->stub_db_sync = $this->getMock('DB_Sync');
		$this->stub_phoundry_sync = $this->getMock('PhoundrySync');
		$this->upgrade = new Test_Brickwork_UpgradeTest($this->stub_db,
			$this->stub_db_sync, $this->stub_phoundry_sync);
	}
	
	public function testOutputter()
	{
		$upgrade = $this->upgrade;
		
		$outputter = new Test_Brickwork_Upgrade_Outputter();
		
		$upgrade->attachOutputter($outputter);
		$upgrade->prepare();
		
		$upgrade->detachOutputter($outputter);
		$upgrade->prepare();
		
		$upgrade->attachOutputter($outputter);
		$upgrade->run();
		$upgrade->rollback();
		
		$upgrade->detachOutputter($outputter);
		$upgrade->prepare();
		$upgrade->run();
		$upgrade->rollback();
		
		$this->assertEquals(3, count($outputter->outputted));
		$this->assertEquals('Prepare', $outputter->outputted[0]);
		$this->assertEquals('Run', $outputter->outputted[1]);
		$this->assertEquals('Rollback', $outputter->outputted[2]);
	}
}

class Test_Brickwork_UpgradeTest extends Brickwork_Upgrade_Abstract
{
	public function prepare()
	{
		$this->_out("Prepare");
		return array();
	}
	
	public function run(array $params = null)
	{
		$this->_out("Run");
		return true;
	}
	
	public function rollback(array $params = null)
	{
		$this->_out("Rollback");
		return true;
	}
}

class Test_Brickwork_Upgrade_Outputter implements Brickwork_Upgrade_Outputter
{
	public $outputted = array();
	
	public function output(Brickwork_Upgrade $upgrade, $message)
	{
		$this->outputted[] = $message;
	}
}