<?php
class DBTest extends UnitTest_TestCase_Database
{
	protected function getDataSet()
	{
		return $this->createFlatXMLDataSet( UNITTEST_RESOURCES . '/ActiveRecord_BankAccount.xml');
	}
	
	public function testQuote()
	{
		$this->assertEquals( 'test', DB::quote('test'));
		$this->assertEquals( "test\\'", DB::quote("test'"));
		$this->assertEquals( "test\\'\\'", DB::quote("test''"));
		$this->assertEquals( "test\\n", DB::quote("test\n"));
		$this->assertEquals( "\\r", DB::quote("\r"));
		$this->assertEquals( "	", DB::quote("\t"));
		$this->assertEquals( "%", DB::quote("%"));
		$this->assertEquals( "_", DB::quote("_"));
		$this->assertEquals( "", DB::quote(""));
		$this->assertEquals( "''", DB::quote("",true));
		$this->assertEquals( "'test\\n'", DB::quote("test\n",true));		
	}
	
	public function testValidQuery()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$this->assertTrue($res instanceof ResultSet);
		$this->assertEquals(3,count($res->result));
	}
	
	public function testInValidQuery()
	{
		$res = DB::iQuery('SELECT * FROM bank_account_test');
		
		$this->assertEquals('MySQLError', get_class($res));
		$this->assertGreaterThan( 0, DB::getQueryCount());
	}
	
	public function testquerySingleResult()
	{
		$res = DB::iQuery('SELECT * FROM bank_account');
		
		$this->assertTrue( is_object($res) );
		$this->assertNotEquals( 'MySQLError', get_class($res));
		$this->markTestIncomplete();
	}
	
	public function testUpdateQuery()
	{
		$res = DB::iQuery('UPDATE bank_account SET balance = 200 WHERE account_number = ' . DB::quote('15934903649620486',true));
		
		$this->assertTrue($res instanceof ResultSet);
		$this->assertEquals(1,$res->affectedRows);
		$this->assertEquals(1,$res->affected_rows);
		$this->assertEquals(1,$res->numRows);
		$this->assertEquals(1,$res->num_rows);
		
		$row = DB::iQuery('SELECT * FROM bank_account WHERE account_number = '. DB::quote('15934903649620486',true))->first();
		
		$this->assertTrue(is_object($row));
		$this->assertEquals(200, $row->balance);
	}
	
	public function testDeleteQuery()
	{
		$res = DB::iQuery('DELETE FROM bank_account WHERE account_number = ' . DB::quote('15934903649620486',true));
		
		$this->assertTrue($res instanceof ResultSet);
		$this->assertEquals(1,$res->affectedRows);
		$this->assertEquals(1,$res->affected_rows);
		$this->assertEquals(1,$res->numRows);
		$this->assertEquals(1,$res->num_rows);
		
		$res = DB::iQuery('SELECT COUNT(*) as rowcount FROM bank_account')->first();
		
		$this->assertTrue( is_object($res) );
		$this->assertEquals( 2, $res->rowcount );
	}
}