<?php
class BrickDateTimeTest extends PHPUnit_Framework_TestCase 
{
	public function testConstructor()
	{
		//__construct($datetime=NULL)
		$brd = new BrickDateTime();
		$date = Date('Now');
				
		$this->assertNotNull($brd);
		$this->assertNotNull($date);
		
		$this->assertTrue($brd->checkdatetime());
		$this->assertNotNull($brd->__toString());

		$timeStamp = new Date(time());
		
		$this->assertEquals($brd->__get('year'), 		(int)date('Y'));
		$this->assertEquals($brd->__get('month'), 		(int)date('m'));
		$this->assertEquals($brd->__get('day'), 		(int)date('d'));
		$this->assertEquals($brd->__get('hour'), 		(int)date('H'));
		$this->assertEquals($brd->__get('minute'), 		(int)date('i'));
		
		$this->markTestIncomplete();
		$this->assertEquals($brd->__get('seconds'), 	(int)date('s'));  //TODO nakijken + timestamp en now moet nog.
	}
	
}
?>
