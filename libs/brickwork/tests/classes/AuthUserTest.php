<?php
class TestUser extends AuthUser
{
	public function login()
	{
		return true;
	}
	
	public function setAuthId($id)
	{
		$this->_auth_id = (int) $id;
	}
	
	public function setIsLoggedIn($status)
	{
		$this->is_logged_in = $status;
	}
	
	public function setIsValid($status)
	{
		$this->is_valid = $status;
	}
}

class AuthUserTest extends PHPUnit_Framework_TestCase
{
	public function testIsAnonymous()
	{
		$user = new TestUser();
		$this->assertTrue(is_bool($user->isAnonymous()));
		$this->assertTrue($user->isAnonymous());
		
		$user = new TestUser(Auth::anonymous_user, Auth::anonymous_pw);
		$this->assertTrue($user->isAnonymous());
		
		$user = new TestUser('Christiaan', 'Password');
		$this->assertFalse($user->isAnonymous());
	}
	
	public function testIsLoggedIn()
	{
		$user = new TestUser();
		$this->assertTrue(is_bool($user->isLoggedIn()));
		$user->setIsLoggedIn(null);
		$this->assertTrue(is_bool($user->isLoggedIn()));
		$this->assertFalse($user->isLoggedIn());
		$user->setIsLoggedIn(true);
		$this->assertTrue($user->isLoggedIn());
	}
	
	public function testIsValid()
	{
		$user = new TestUser();
		$this->assertTrue(is_bool($user->isValid()));
		$user->setIsValid(null);
		$this->assertTrue(is_bool($user->isValid()));
		$this->assertFalse($user->isValid());
		$user->setIsValid(true);
		$this->assertTrue($user->isValid());
	}
	
	public function testEncryptPassword()
	{
		$org_webprefs = $GLOBALS['WEBprefs'];
		
		$GLOBALS['WEBprefs']['User.Encryption'] = null;
		if(!isset($GLOBALS['WEBprefs']['user'])) {
			$GLOBALS['WEBprefs']['user'] = array();
		}
		$GLOBALS['WEBprefs']['user']['encryption'] = null;
		
		$this->assertEquals('password', AuthUser::encryptPassword('password'));
		
		$GLOBALS['WEBprefs']['User.Encryption'] = 'md5';
		$this->assertEquals(md5('password'), AuthUser::encryptPassword('password'));
		
		$GLOBALS['WEBprefs']['User.Encryption'] = null;
		$GLOBALS['WEBprefs']['user']['encryption'] = 'md5';
		$this->assertEquals(md5('password'), AuthUser::encryptPassword('password'));
		
		$GLOBALS['WEBprefs']['user']['encryption'] = 'strtoupper';
		$this->assertEquals(strtoupper('password'), AuthUser::encryptPassword('password'));
		
		$GLOBALS['WEBprefs'] = $org_webprefs;
	}
	
	public function testMagicProperties()
	{
		$user = new TestUser('Christiaan', 'Password');
		$user->setAuthId(1337);
		$this->assertEquals(1337, $user->auth_id);
		$this->assertEquals('Christiaan', $user->username);
		$this->assertEquals('Password', $user->password);
	}
	
	public function testMagicPropertiesNonexistant()
	{
		$user = new TestUser('Christiaan', 'Password');
		$user->setAuthId(1337);
		$this->assertEquals(null, $user->paarden);
	}
}