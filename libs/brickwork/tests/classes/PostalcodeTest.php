<?php
class PostalcodeTest extends PHPUnit_Framework_TestCase
{
	public function testWithIllegalCountry()
	{
		$this->markTestIncomplete('API unclear');
		
		$z = new Postalcode('1016dp','nl');
	}
	
	public function testWithLegalZip()
	{
		$z = new Postalcode('1016dp', new Country('nl'));
		
		$this->assertEquals( '1016dp', $z->zip);
	}
	
	public function testWithIllegalZip()
	{
		$this->markTestIncomplete();
	}
	
	public function testWithInvalidCOuntry()
	{
		$this->markTestIncomplete();
	}
	
}
?>