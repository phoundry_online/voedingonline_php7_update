<?php
class SMTPTest extends PHPUnit_Framework_TestCase 
{
	public function testBasicSMTPMessage()
	{
		$params = array(
			'timeout'	=> "5",
			'host'		=> "localhost",
			'port'		=> "25"
		);
		
		$email = "mark.vandehaar@webpower.nl;"; //@TODO remove fixed email adress 
				
		$smtp = new SMTP($params);
		
		$this->assertNotNull($smtp);
		$smtp->connect($params);
		
		$this->assertNotNull($smtp);

		$smtp->send(
			$email, 
			$email, 
			array('Subject' => "UnitTest"),
			"Dit is een basis-test vanuit de Unit Test"
		);
		$this->assertNotNull($smtp);
		//$this->assertNotNull($smtp->get_data()); //@TODO get verwerken in de test...
	}
	
	public function testAdvancedSMTPMessage()
	{
		$params = array(
			'timeout'	=> "5",
			'host'		=> "localhost",
			'port'		=> "25"
		);
		
		$email = "mark.vandehaar@webpower.nl;"; //@TODO remove fixed email adress 
		
		$smtp = new SMTP($params);
		
		$this->assertNotNull($smtp);
		
		$smtp->connect($params);
		
		$this->assertNotNull($smtp);
		
		$smtp->send(
			$email, 
			$email,
			array('Subject' => "UnitTest",
				'MIME-Version' 	=> "1.0",
				'Content-type'	=> "text/plain"), 
			"Dit is een advanced-test vanuit de Unit Test;)"
		);
		
		$this->assertNotNull($smtp);
		
		//$this->assertNotNull($smtp->get_data()); //@TODO get verwerken in de test...
		
	}
	
}
?>