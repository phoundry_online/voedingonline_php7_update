<?php
class PaginationTest extends PHPUnit_Framework_TestCase 
{
	public function testConstruct()
	{
		$limit = 10;
		$curPage = 1;
		$totalItems = 20;
				
		$pagination = new Pagination($limit, $curPage, $totalItems);
		
		$this->assertEquals($totalItems, $pagination->total_items);
		$this->assertEquals($totalItems, $pagination->items);
		$this->assertEquals($curPage, $pagination->page);
		$this->assertEquals($curPage, $pagination->current_page);
		$this->assertEquals($limit, $pagination->items_per_page);
		$this->assertEquals($limit, $pagination->limit);
		$this->assertEquals($limit*$curPage, $pagination->offset);
		
		$this->assertEquals($curPage+1, $pagination->next_page);
		$this->assertEquals($curPage-1, $pagination->prev_page);
		
		$this->assertType("Pagination", $pagination->next_page_obj);
		$this->assertType("Pagination", $pagination->prev_page_obj);
		
		$this->assertEquals(0, $pagination->items_after_page);
		$this->assertEquals(10, $pagination->items_before_page);
		
		$amount = 100;
		$pagination->setTotalItems($amount);
		$this->assertNotEquals($totalItems, $pagination->total_items);
		$this->assertEquals($amount, $pagination->total_items);
		$this->assertEquals($amount, $pagination->items);
		
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testItemsException()
	{
		$pagination = new Pagination();
		$pagination->items;
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testTotalItemsException()
	{
		$pagination = new Pagination();
		$pagination->total_items;
	}
}
