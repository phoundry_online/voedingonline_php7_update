<?php
//
// Phoundry core settings:
//

$PHprefs = array(

// Other preferences.
// Charset for DB & web pages. Either 'iso-8859-1' or 'utf-8' (lowercase)
'charset'		=> 'utf-8',
'uploadDir'		=> dirname( dirname( __FILE__ ) ) . '/uploaded',
'uploadUrl'		=> '/uploaded',
'SMTPserver'	=> 'localhost',
'SMTPport'     => 25,
'SMTPfrom'		=> 'phoundry@webpower.nl',
'SMTPhelo'		=> php_uname('n'),
'SMTPheaders'	=> array('From: phoundry@webpower.nl'),
'errorRcpts'	=> Array('phoundry_admin@webpower.nl'),

'tinyMCEconfig' => "{theme_advanced_link_intern_dialog: '/phoundry/custom/brickwork/link_intern/'}",
'timezonePHP'	=> 'Europe/Amsterdam',
);

// Database preferences.
$PHprefs['DBtype']			= 'mysql';
$PHprefs['DBserver']		= '192.168.2.24';
$PHprefs['DBuserid']		= 'unittest';
$PHprefs['DBpasswd']		= 'tsettinu';
$PHprefs['DBname']			= 'brickwork_unittestdb';
$PHprefs['DBport']			= 3306;
$PHprefs['DBpersistent']	= false;
$PHprefs['DBoptions']		= array();
$PHprefs['debug']			= true;

// Settings specific for this website.
// Add settings or functions below.
//
$WEBprefs = array(
	'authorization' => array(
		'enable'		=> false, // Of het vinkje "beveiligd" getoont wordt voor een structure item
		'registration'	=> false, // Of nieuwe brickwork users kunnen registreren op de site (niet phoundry)
	),
	'acl'	=> Array(
		'enable'		=> false,	// Of ACL objecten zelfstandig aangemaakt moeten worden
		'autocreate'	=> true	// Of ACL validatie aan moet staan
	),
	'user'	=> Array(
		'encryption'	=> ''	// Of wachtwoorden beveiligd moeten worden (default nee, kan ook md5 zijn)
	),
	'reactions' => array(
		//'template' => 'modules/blocks/reactions.tpl',
		'imagedir' => 'IMAGES/Reaction',
		'reactiontid' => 349,
		'reactiongrouptid' => 353,
		'imagetid' => 350,
		'youtubetid' => 351,
		'urltid' => 352,
		/*'classes' => array(
			'reaction' => 'Reactions_Reaction',
			'group' => 'Reactions_Group',
			'media' => 'Reactions_Media',
			'relationConfig' => 'Reactions_Relationconfig',
		),*/
	),
	'Autoloader' => array(
		'cachetime' => 0,
		'paths' => array(),
	),
);

	define('PROJECT_DIR',    dirname( dirname( __FILE__ ) ) );
	define('PHOUNDRY_DIR',   PROJECT_DIR.'/phoundry/');
	define('UPLOAD_DIR',     PROJECT_DIR.'/uploaded/');
	define('TMP_DIR',        PROJECT_DIR . '/tmp/');
	define('TEMPLATE_DIR',   PROJECT_DIR . '/templates/');
	define('CACHE_DIR',      TMP_DIR . '/cache/');
	define('PHP_UPLOAD_DIR', TMP_DIR . '/php/');
	
	define('BRICKWORK_DIR',				realpath(dirname(__FILE__) . '/../') . '/');
	define('CUSTOM_INCLUDE_DIR',		BRICKWORK_DIR . '/tests/classes' );
	define('INCLUDE_DIR',				BRICKWORK_DIR);
	define('BRICKWORK_FUNCTION_DIR',	BRICKWORK_DIR . '/functions/');
	define('BRICKWORK_CLASS_DIR',		BRICKWORK_DIR . '/classes/');
	define('BRICKWORK_MODULE_DIR',		BRICKWORK_DIR . '/modules/');
	define('UNITTEST_RESOURCES',		BRICKWORK_DIR . '/tests/resources/');
	define("TESTMODE",true);
	define('USE_TEMPLATE_CACHE', TRUE);
	define('DB_LOG_QUERY', 0);
	define('DB_LOG_QUERY_SLOW', 0.01);
	define('PHP_TIMESTAMP', time() );
	define('BRICKWORK_BEAUTIFUL_URL_REDIRECT_OLD', TRUE);
	define('BRICKWORK_BEAUTIFUL_URL', TRUE);


if ( !isset($PHprefs['langDirs'] ))
	$PHprefs['langDirs'] = array();

if (!in_array( BRICKWORK_DIR . '/phoundry/lang', $PHprefs['langDirs'])  )
	$PHprefs['langDirs'][] =  BRICKWORK_DIR . '/phoundry/lang';

if (!isset($PHprefs['timezonePHP']))
	$PHprefs['timezonePHP'] = 'Europe/Amsterdam';
	
if ( !isset($WEBprefs['templateDirs']) )
	$WEBprefs['templateDirs'] = array();

if (!in_array( BRICKWORK_DIR . '/templates', $WEBprefs['templateDirs']) )
	$WEBprefs['templateDirs'][] = BRICKWORK_DIR . '/templates';
