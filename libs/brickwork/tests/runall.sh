#!/bin/bash

cd `dirname ${PWD}/${_}`
export BRICKWORK_TESTPATH=`pwd`

PROJECTDIR=`dirname ${BRICKWORK_TESTPATH}`
PROJECTDIR=`dirname ${PROJECTDIR}`

PATH=${PATH}:${PROJECTDIR}/phpunit:`pwd`

if [ -d results ]; then
	rm -rf results
	mkdir results
fi

#phpunit --coverage-html ../results/ --log-pmd ../results/pmd.xml --log-metrics ../results/metrics.xml AllTests
phpunit --bootstrap BrickworkSetup.php classes/AllTests $* 