<?php
/**
 * Iterate over a database Resultset returning ActiveRecord classes
 *
 */
class ActiveRecordIterator extends ResultSet implements OuterIterator
{
	/**
	 * The classname used to make the objects
	 *
	 * @var string
	 */
	protected $_class_name;
	
	/**
	 * The Database that was used and should be used by the ARs
	 * 
	 * @var DB_Adapter
	 */
	protected $_db;
	
	/**
	 * Constructor
	 *
	 * @param string $classname
	 * @param ResultSet $result_set
	 */
	public function __construct($classname, ResultSet $result_set, DB_Adapter $db = null)
	{
		if(is_object($classname)) {
			$classname = get_class($classname);
		}
		$this->_class_name = $classname;
		$this->_db = $db;
		$this->_count = $result_set->count();
		parent::__construct($result_set);
	}

	/**
	 * Override, because an ActiveRecord IS a object but we want to use array access
	 * @see ResultSet::getSelect()
	 */
	public function getSelect($key, $value = null)
	{
		$select_array = array();
		while($this->current()) {
            $row = $this->current();
			if(!is_null($value)) {
				$select_array[$row[$key]] = $row[$value];
			}
			else {
				$select_array[] = $row[$key];
			}
			$this->next();
		}
		$this->rewind();

		return $select_array;
	}

	/**
	 * Iterator: current
	 * 
	 * @return ActiveRecord
	 */
	public function current(): mixed
	{
		if (false !== ($row = $this->_source->current())) {
			$row = ActiveRecord::factory($this->_class_name, null, true,
				$this->_db)->loadResult($row);
		}
		
		return $row;
	}

	/**
	 * @see ResultSet::next()
	 */
	public function next(): void
	{
		$this->_source->next();
		parent::next();
	}

	/**
	 * Iterator: rewind
	 */
	public function rewind(): void
	{
		$this->_source->rewind();
		parent::rewind();
	}
	
	public function getInnerIterator(): ?Iterator
	{
		return $this->_source;
	}

	/**
	 * @see ResultSet::seek()
	 */
	public function seek($offset): void
	{
		$this->_source->seek($offset);
		parent::seek($offset);
	}
}
