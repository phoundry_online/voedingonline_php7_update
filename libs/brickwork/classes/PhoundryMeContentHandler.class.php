<?php
/**
 * Phoundry Me redirector
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @todo check on ipadress if a user should be redirected
 */
class PhoundryMeContentHandler extends ContentHandler
{
	public function init()
	{
		global $PHprefs;
		if (defined('TESTMODE') && true === TESTMODE) {
			$phoundry_url = (
				isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ?
				'https' : 'http'
			).'://'.$_SERVER['HTTP_HOST'].$GLOBALS['PHprefs']['url'];

		} else if (isset($GLOBALS['DMDprefs']['domain'])) {
			$phoundry_url = 'https://'.$GLOBALS['DMDprefs']['domain'] . $PHprefs['url'];
		} else if (isset($PHprefs['domain'])) {
			$phoundry_url = $PHprefs['domain'].$PHprefs['url'];
		} else {
			$protocols = explode(',', Framework::$site->protocol);
			$phoundry_url = in_array('https', $protocols) ? 'https' : first($protocols);
			$phoundry_url .= '://'.Framework::$site->host.$GLOBALS['PHprefs']['url'];
		}
		
		if ($this->path[0] == 'record' && isset($this->path[1])) {
			if (!is_numeric($this->path[1])) {
				$tid = PhoundryTools::getTableTID($this->path[1]);
			} else {
				$tid = (int) $this->path[1];
			}
			if (false === $tid) {
				exit(0);
			}
			
			$qs = array('TID' => $tid,'site_identifier' => Framework::$site->identifier);
			if (isset($this->path[2])) {
				$qs['RID'] = $this->path[2];
			}
			trigger_redirect(
				$phoundry_url.'/core/preview.php?'.http_build_query($qs)
			);
		} else if ($this->path[0] == 'structure' && isset($this->path[1]) && isset($_GET['url'])) {
			if ($plugin = PhoundryTools::getPluginUrl('PLUGIN_page_contents')) {
				$tid = PhoundryTools::getTableIdByString('PLUGIN_page_contents');
				list($site, $page) = explode(':', $this->path[1]);
				trigger_redirect(
					$phoundry_url.'/'.$plugin.'?'.http_build_query(
						array(
							'site_identifier' => $site,
							'TID' => $tid,
							'RID' => $page,
							'page_id' => $page
						)
					)
				);
			} else {
				trigger_redirect(
					$phoundry_url.'/custom/brickwork/phoundry_me.php?'.
					http_build_query(
						array('url' => $_GET['url'], 'page' => $this->path[1])
					)
				);
			}
		}
		
		exit(0);
	}	
	
}