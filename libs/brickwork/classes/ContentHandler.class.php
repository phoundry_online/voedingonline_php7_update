<?php
/**
 * Abstract content handler
 *
 * @package	ContentHandler
 * @version	1.0.1
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @last_modified 11-09-2008 CB
 */

abstract class ContentHandler
{
	/**
	 * Errors
	 * vanuit index.php worden errors in de contenthandler gezet
	 */
	public $errors = array();
	
	public $path;
	public $querystring;
	
	public function __construct($path, $query)
	{
		$this->path = $path;
		$this->querystring = $query;
	}

	public static function factory($type, $path, $query){
		global $WEBprefs;

		$contentHandler = NULL;

		/*
		 * haal contenthandler setup uit prefs file voor dynamisch toevoegen per installatie
		 * en sneller als reflection
		 */
		if (isset($WEBprefs) && isset($WEBprefs['contentHandlers']) && is_array($WEBprefs['contentHandlers'])){
			if (isset($WEBprefs['contentHandlers'][$type])){
					$contentHandler = new $WEBprefs['contentHandlers'][$type]($path, $query);
					return $contentHandler;
			}		
		}

		// try to find out with reflection
		try
		{
			$refl = new ReflectionClass( ucfirst($type).'ContentHandler');
			return $refl->newInstance($path, $query);
		}
		catch( Exception $e)
		{
			//return null;
			// nog even niks
		}

		// else, use hardcoded types
		/*
		** bepaal de basis types */
		switch($type){
			case 'page':
				$contentHandler = new PageContentHandler($path, $query);
				break;

			case 'majax':
				$contentHandler = new AjaxContentHandler($path, $query);
				break;

			case 'module':
				$contentHandler = new ModuleContentHandler($path, $query);
				break;

			case 'p3p':
				$contentHandler = new P3PContentHandler($path, $query);
				break;

			case 'googlesitemap':
				$contentHandler = new GoogleSitemapContentHandler($path, $query);
				break;

			default:
				return new ErrorVoedingonline(E_USER_NOTICE, sprintf('ContentHandler %s not found', $type));
		}

		return $contentHandler;
	}

	
	/**
	 * Checks what language to show the customer
	 * This method will check if the user has previously visited the site and if so,
	 * what language he preferred (was shown). If no previous visit information was
	 * found, it will just use the default Language.
	 * 
	 * @return Site_Language
	 */
	public function determineLanguage()
	{
		
		$c = Cookie::factory('preferences' ); //new Cookie('lang_' . Framework::$site->identifier);
		$l = false;

		if ( !isset($c->l) || is_null($c->l) )
		{
			foreach( Framework::$clientInfo->http_accept_language as $cl )
			{
				$cl = explode(';',$cl);
				$l  = Framework::$site->hasLanguage($cl[0]);

				if ( is_object($l) && $l instanceof Site_Language && $l->userSelectable === TRUE){
					break;
				} else {
					$l = false;
				}

			}
			
			if ( $l === false)
				$c->l = Framework::$site->defaultLang;
			else
				$c->l = $l;
			$c->update();
		}
		return $c->l;
	}
	
	abstract function init();
	
	/**
	 * This method should be abstract
	 * @abstract
	 * @param bool $return should the output be returned or outputted
	 * @return string|void
	 */
	public function out($return = false)
	{
		
	}
	
	public function print404()
	{
		$rep = Framework::$representation;
		$rep->cache_lifetime = -1;
		$rep->caching = false;

		error_log(sprintf("[%s] [error] [client %s] File not found: %s%s\n", date("D, d M Y H:i:s T"), $_SERVER['REMOTE_ADDR'], $_SERVER['REQUEST_URI'], !empty($_SERVER['HTTP_REFERER']) ? ' Referer: '.$_SERVER['HTTP_REFERER'] : '' ), 3, TMP_DIR.'/error.log');
		header('HTTP/1.1 404 Not found', true, 404);

		// if 404 found, then try to load structure item '404 Not found' otherwise load 404.tpl, if not exists print 404 message
		$page = false;
		$path = SiteStructure::getStructureByPath(array('404-Not-found'));
		
		if(!is_null($path)){
			$structure = SiteStructure::getStructureItem(
				Framework::$site->identifier, $path->site_structure_id);
			
			$rep->assign('STRUCTURE', $structure);
			
			$pf = new PageFactory(DB::getDb(), Framework::$site, $this,
				Framework::$clientInfo, Auth::singleton());
			
			$page = $pf->pageByStructureItem($structure);
		}
		
		if($page){
			header(sprintf('Content-type: %s; charset=%s',
				$rep->mimeType,
				Framework::$PHprefs['charset'])
			);
			$page->render($rep, false);
		} else if($rep->setTemplate('404.tpl')){
			$rep->assign('SITE', Framework::$site);
			$rep->assign('CLIENT', Framework::$clientInfo);
			$rep->out();
		} else {
			print '404 Not found';
		}
	}
}
