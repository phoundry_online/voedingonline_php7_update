<?php
require_once INCLUDE_DIR.'/classes/Menu.package.php';

class QuickMenu extends Menu {

	public function __construct ($title, $link='', $description='', $nivo=0) {
		parent::__construct ($title, $link='', $description='', $nivo=0);
	}

	static function getQuickMenu(Site $site){
		$retval = array();
		$sql = sprintf('
							SELECT
								ss.*,
								bqm.position
							FROM
								brickwork_quick_menu bqm
							INNER JOIN
								brickwork_site_structure ss ON ss.id = bqm.site_structure_id
							WHERE
								ss.site_identifier = "%s"
							ORDER BY
								bqm.prio ASC
							',
							// values
							$site->identifier
							);


		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);
		}

      if($res->current()){

			$stack = SiteStructure::getSiteStack(Framework::$site->identifier);;

			// collect all structure id's and determine whats connected
			$collected_ids = $structure_ids = array();
			while($res->current()){
                $item = $res->current();
                if(isset($stack[$item->id])){
					if(!isset($structure_ids[$item->position])){
						$structure_ids[$item->position] = array();
					}
					$structure_ids[$item->position][$item->id] = $stack[$item->id];
					$collected_ids[] = $item->id;
				}
				$res->next();
			}

			$pages = SiteStructure::getPages($collected_ids);
			$testpages = SiteStructure::getTestPages($collected_ids);
			$urls  = SiteStructure::getUrls($collected_ids);

			$menu = array();

//			if($res->num_rows > 0 ) {

				foreach($structure_ids as $position => $elements) {
					foreach($elements as $id => $item) {
						if(!empty($pages[$item->id])) {
							if (isset($testpages[$item->id]) && $testpages[$item->id] == $pages[$item->id]) {
								// All pages below this site_structure have staging status 'test'
								continue;
							}
							$url = SiteStructure::getFullPathByStructureId($item->id);
							$target = '_self';
						}
						else if(!empty($urls[$item->id])) {
							$url = $urls[$item->id]['link'];
							$target = $urls[$item->id]['target'];
						}
						else {
							continue;
						}
						$m = new Menu($item->name,$url,$item->name,0);
						$m->id = $item->id;
						$m->target	= $target;
						$m->selected = $item->isActive;
						$menu[$position][$item->id] = $m;
						$m->structure = $item;
					}
				}
				$retval = $menu;
//			}
		}
		return $retval;
	}

	public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

?>
