<?php
/**
 * Object Relation Mapper
 * 
 * @todo checking relation configurations should be done on set not on access
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
abstract class ORM extends ActiveRecord
{
	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
//		'foreign_objects' => array('foreign_classme', 'join_table', array('self_primary_key', 'self_primary_key'), array('foreign_PK')),
	);
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
//		'foreign_objects' => array('foreign_classname', array('foreign_fields', 'matching_own', 'primary_key')),
	);
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
	);
	
	/**
	 * Cache the many to many results
	 *
	 * @var array
	 */
	protected $_relation_items;
	
	/**
	 * Magic getter
	 *
	 * @throws Exception
	 * @param string $name
	 * @return mixedvar
	 */
	public function __get($name)
	{				
		$relation = $this->getRelation($name);
		if($relation !== false){
			return is_array($relation) ? (array) $relation : $relation;
		}
		
		throw new Exception('Relation '.$name.' doesn\'t exist for class '.get_class($this));
	}
	
	/**
	 * Magic setter
	 *
	 * @param string $name
	 * @param mixedvar $value
	 */
	public function __set($name, $value)
	{
		if(FALSE !== $this->add($name, $value, true)){
			return;
		}
		
		throw new Exception('Relation '.$name.' doesn\'t exist for class '.get_class($this));
	}
	
	public function __isset($name)
	{
		return (FALSE !== $this->getRelation($name));
	}
	
	/**
	 * Magic unsetter
	 *
	 * @param string $name
	 */
	public function __unset($name)
	{
		if(FALSE === $this->remove($name, null, true)){
			throw new Exception(get_class($this) . ' has no relation: '.$name);
		}
	}
	
	/**
	 * Get a relation as it is defined in either $_many_to_many,
	 * $_many_to_one or $_one_to_many
	 *
	 * @param string $key Key in one of the relation arrays
	 * @return object|array|bool
	 */
	public function getRelation($key)
	{
		if(!is_array($this->_relation_items)) {
			$this->_relation_items = array(
				'nn' => array(),
				'1n' => array(),
				'n1' => array(),
			);
		}
		
		if(isset($this->_many_to_many[$key])) {
			if(!isset($this->_relation_items['nn'][$key])) {
				// Initialize the variables
				if (count($this->_many_to_many[$key]) == 5) {
					list($foreign_class, $join_table, $this_pk, $foreign_pk, $order_by) = $this->_many_to_many[$key];
				}
				else {
					list($foreign_class, $join_table, $this_pk, $foreign_pk) = $this->_many_to_many[$key];
					$order_by = array();
				}
				
				$foreign_class = ActiveRecord::factory($foreign_class, null, true, $this->_db);
				$this_pk = (array) $this_pk;
				$foreign_pk = (array) $foreign_pk;
				$order_by = (array) $order_by;
								
				if(count($this->_localTable['primary_key']) != count($this_pk)) {
					throw new Exception(get_class($this)."'s Primary Key consists of ".count($this->getTableProperty('primary_key'))." columns. ".count($this_pk)." keys found.");
				}
				if(count($foreign_class->getTableProperty('primary_key')) != count($foreign_pk)) {
					throw new Exception(get_class($foreign_class)."'s Primary Key consists of ".count($foreign_class->getTableProperty('primary_key'))." columns. ".count($foreign_pk)." keys found.");
				}
				
				// Create the SQL to join the tables
				$join_on = '';
				$this_pk_copy = $this_pk;
				foreach($this->_localTable['primary_key'] as $col_name) {
					if($join_on != ''){
						$join_on .= ' AND ';
					}
					$join_on.= sprintf(
						"`join_table`.`%s` = '%s'\n",
						array_shift($this_pk_copy),
						$this->offsetGetReal($col_name)
					);
				}

				$foreign_pk_copy = $foreign_pk;
				foreach($foreign_class->getTableProperty('primary_key') as $col_name) {
					if($join_on != '') {
						$join_on.= ' AND ';
					}
					$join_on.= sprintf(
						"`join_table`.`%s` = `record_table`.`%s`\n",
						array_shift($foreign_pk_copy),
						$col_name
					);
				}
				
				$order_on = array();
				if ($order_by) {
					foreach($order_by as $ob) {
						if (is_array($ob)) {
							list($key, $order) = $ob;
							$order_on[] = sprintf("`%s` %s\n", $key, $order);
						}
						else {
							$order_on[] = sprintf("`%s` %s\n", $ob, 'ASC');
						}
					}
				}
				
				$sql = sprintf("
					SELECT
						%s
					FROM
						`%s` AS record_table
					INNER JOIN
						`%s` AS join_table
					ON
						%s
					%s",
					$foreign_class->getColumnsSql('record_table'),
					$foreign_class->getTableName(),
					$join_table,
					$join_on,
					$order_on ? 'ORDER BY '. implode(', ', $order_on) : ''
				);
				
				$res = $this->_db->iQuery($sql);
				$this->_relation_items['nn'][$key] = new ActiveRecordIterator($foreign_class, $res, $this->_db);
			}
			return $this->_relation_items['nn'][$key];
		}
		
		if(isset($this->_one_to_many[$key])) {
			if(!isset($this->_relation_items['1n'][$key])) {
				// Initialize the variables
				list($classname, $foreign_pk) = $this->_one_to_many[$key];
				$child_class = ActiveRecord::factory($classname, null, true, $this->_db);
				$foreign_pk = (array) $foreign_pk;
				if(count($this->_localTable['primary_key']) != count($foreign_pk)) {
					throw new Exception(get_class($this)."'s Primary Key consists of ".count($this->getTableProperty('primary_key'))." columns. ".count($foreign_pk)." keys found.");
				}
				
				$where = array();
				$foreign_pk_copy = $foreign_pk;
				foreach($this->_localTable['primary_key'] as $col_name) {
					$where[array_shift($foreign_pk_copy)] = $this->offsetGetReal($col_name);
				}
				
				$this->_relation_items['1n'][$key] = $child_class->
					where($where)->get();
			}
			return $this->_relation_items['1n'][$key];
		}
		
		if(isset($this->_many_to_one[$key])) {
			if(!isset($this->_relation_items['n1'][$key])) {
				list($classname, $match_fields) = $this->_many_to_one[$key];
				$match_fields = (array) $match_fields;				
				$pk = array();
				foreach($match_fields as $field) {
					$pk[] = $this->offsetGetReal($field);
				}
				
				$this->_relation_items['n1'][$key] = ActiveRecord::factory($classname, $pk, true, $this->_db);
			}
			return $this->_relation_items['n1'][$key];			
		}
		
		return false;
	}
	
	/**
	 * Sets the value of a given relation, or add items
	 * to a relation that can consist of multiple items
	 *
	 * @param string $key Key in one of the relation arrays
	 * @param object/array/number $items
	 * @return bool
	 */
	public function add($key, $items, $used_set = false)
	{
		if(isset($this->_many_to_many[$key]) && !$used_set) {
			list($foreign_class, $join_table, $this_pk, $foreign_pk) = $this->_many_to_many[$key];
			$foreign_class = ActiveRecord::factory($foreign_class, null, true, $this->_db);
			$this_pk = (array) $this_pk;
			$foreign_pk = (array) $foreign_pk;
			if(!is_array($items) && !($items instanceof ActiveRecordIterator)) {
				$items = array($items);
			}
			
			if(count($this->_localTable['primary_key']) != count($this_pk)) {
				throw new Exception(get_class($this)."'s Primary Key consists of ".count($this->getTableProperty('primary_key'))." columns. ".count($this_pk)." keys found.");
			}
			
			if(count($foreign_class->getTableProperty('primary_key')) != count($foreign_pk)) {
				throw new Exception(get_class($foreign_class)."'s Primary Key consists of ".count($foreign_class->getTableProperty('primary_key'))." columns. ".count($foreign_pk)." keys found.");
			}

				
			foreach($items as $item) {
				if(!($item instanceof ActiveRecord)) {
					throw new Exception(
						'Trying to add relation to '.get_class($this).
						' failed. Given items do not extend ActiveRecord');
				}
				
				$this_pk_copy = $this_pk;
				$foreign_pk_copy = $foreign_pk;
				
				$values = array();
				foreach($this->_localTable['primary_key'] as $col_name) {
					$values[array_shift($this_pk_copy)] = $this->_db->quoteValue($this->offsetGetReal($col_name));
				}
	
				foreach($foreign_class->getTableProperty('primary_key') as $col_name) {
					$values[array_shift($foreign_pk_copy)] = $this->_db->quoteValue($item->offsetGetReal($col_name));
				}
				
				$sql = sprintf("
					INSERT INTO `%s` (%s)
					VALUES (%s);
					",
					$join_table,
					'`'.implode('`, `', array_keys($values)).'`',
					implode(', ', $values)
				);
				$res = $this->_db->iQuery($sql);

				if(isset($this->_relation_items['nn'][$key])) {
					unset($this->_relation_items['nn'][$key]); // Flush cache if it is set
				}
			}
			
			return true;
		}
		
		if(isset($this->_one_to_many[$key]) && !$used_set)
		{
			list($classname, $foreign_pk) = $this->_one_to_many[$key];
			$foreign_pk = (array) $foreign_pk;
			if(!is_array($items) && !($items instanceof ActiveRecordIterator)) {
				$items = array($items);
			}
			if(count($this->_localTable['primary_key']) != count($foreign_pk)) {
				throw new Exception(get_class($this)."'s Primary Key consists of ".count($this->getTableProperty('primary_key'))." columns. ".count($foreign_pk)." keys found.");
			}
			
			foreach($items as $item)
			{
				$foreign_pk_copy = $foreign_pk;
				foreach($this->_localTable['primary_key'] as $col_name)
				{
					$item->offsetSetReal(array_shift($foreign_pk_copy), $this->offsetGetReal($col_name));
				}
				$item->save();				
			}
			
			
			if(isset($this->_relation_items['1n'][$key]))
			{
				unset($this->_relation_items['1n'][$key]); // Flush Cache if its set
			}
			return TRUE;			
		}
		
		if(isset($this->_many_to_one[$key]))
		{
			$foreign_class = $items;
			list($classname, $foreign_pk) = $this->_many_to_one[$key];
			if($foreign_class instanceof $classname){
				$foreign_pk = (array) $foreign_pk;
				if(count($foreign_class->getTableProperty('primary_key')) != count($foreign_pk)) {
					throw new Exception(get_class($foreign_class)."'s Primary Key consists of ".count($foreign_class->getTableProperty('primary_key'))." columns. ".count($foreign_pk)." keys found.");
				}
				
				$foreign_pk_copy = $foreign_pk;	
				
				foreach($foreign_class->getTableProperty('primary_key') as $col_name) {
					$this->offsetSetReal(array_shift($foreign_pk_copy), $foreign_class->offsetGetReal($col_name));
				}
				
				if(isset($this->_relation_items['n1'][$key])) {
					unset($this->_relation_items['n1'][$key]); // Flush Cache if its set
				}
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Remove child
	 *
	 * @param string $key Key in the _many_to_many array
	 * @param object/array/number $items
	 * @return bool
	 */
	public function remove($key, $items = null, $used_unset = false)
	{
		if(isset($this->_many_to_many[$key]) && !$used_unset)
		{
			list($foreign_class, $join_table, $this_pk, $foreign_pk) = $this->_many_to_many[$key];
			$foreign_class = ActiveRecord::factory($foreign_class, null, true, $this->_db);
			$this_pk = (array) $this_pk;
			$foreign_pk = (array) $foreign_pk;
			if(!is_array($items) && !($items instanceof ActiveRecordIterator)) {
				$items = array($items);
			}
			
			if(count($this->_localTable['primary_key']) != count($this_pk)) {
				throw new Exception(get_class($this)."'s Primary Key consists of ".count($this->getTableProperty('primary_key'))." columns. ".count($this_pk)." keys found.");
			}
			if(count($foreign_class->getTableProperty('primary_key')) != count($foreign_pk)) {
				throw new Exception(get_class($foreign_class)."'s Primary Key consists of ".count($foreign_class->getTableProperty('primary_key'))." columns. ".count($foreign_pk)." keys found.");
			}
				
			foreach($items as $item) {
				if(!($item instanceof ActiveRecord)) {
					throw new Exception('Trying to remove relation to '.get_class($this).' failed. Given items don\'t extend ActiveRecord');
				}
				
				$values = array();
				$this_pk_copy = $this_pk;
				$foreign_pk_copy = $foreign_pk;
				foreach($this->_localTable['primary_key'] as $col_name) {
					$values[array_shift($this_pk_copy)] = $this->_db->quoteValue($this->offsetGetReal($col_name));
				}
	
				foreach($foreign_class->getTableProperty('primary_key') as $col_name) {
					$values[array_shift($foreign_pk_copy)] = $this->_db->quoteValue($item[$col_name]);
				}
				
				$where_sql = array();
				foreach($values as $column => $value) {
					$where_sql[] = sprintf("`%s` = %s\n", $column, $value);
				}
				
				$sql = sprintf("
					DELETE FROM
						`%s`
					WHERE
						%s
					",
					$join_table,
					implode(' AND ', $where_sql)
				);
				
				$res = $this->_db->iQuery($sql);
				
				if(isset($this->_relation_items['nn'][$key])) {
					// Flush cache if its set
					unset($this->_relation_items['nn'][$key]);
				}
			}
			
			return true;
		}
		
		if(isset($this->_one_to_many[$key]) && !$used_unset)
		{
			list($classname, $foreign_pk) = $this->_one_to_many[$key];
			$foreign_pk = (array) $foreign_pk;
			if(!is_array($items) && !($items instanceof ActiveRecordIterator)) {
				$items = array($items);
			}
			if(count($this->_localTable['primary_key']) != count($foreign_pk)) {
				throw new Exception(get_class($this)."'s Primary Key consists of ".count($this->getTableProperty('primary_key'))." columns. ".count($foreign_pk)." keys found.");
			}
			
			foreach($items as $item) {
				foreach($foreign_pk as $col_name) {
					$item->offsetUnset($col_name);
				}
				$item->save();				
			}
			
			
			if(isset($this->_relation_items['1n'][$key])) {
				// Flush Cache if its set
				unset($this->_relation_items['1n'][$key]);
			}
			return true;			
		}
		
		if(isset($this->_many_to_one[$key])) {
			list($classname, $foreign_pk) = $this->_many_to_one[$key];
			
			$foreign_pk = (array) $foreign_pk;
			
			foreach($foreign_pk as $col_name) {
				$this->offsetUnset($col_name);
			}
			
			if(isset($this->_relation_items['1n'][$key])) {
				// Flush Cache if its set
				unset($this->_relation_items['n1'][$key]);
			}
			return true;			
		}
		
		return false;
	}
	
	/**
	 * Reset the relation cache
	 */
	public function resetRelationCache()
	{
		$this->_relation_items = null;
	}
}
