<?php
if (!class_exists('ACLUser')) {
	require_once('ACL/User.class.php');
}

/**
 * AuthUser
 *
 * Main class of wich all users must be inherited.
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 * @version 2.0.0
 * 
 * @property int $auth_id
 * @property string $username
 * @property string $password
 */
abstract class AuthUser extends ACLUser
{
	/**
	 * Keeps custom user data entries cached
	 * @var array
	 */
	protected $_attributeCache;

	/**
	 * The display name for this user (if any)
	 * @var string
	 */
	public	$displayname;
	
	/**
	 * The e-mail for this user
	 * @var string
	 */
	public	$email;
	
	/**
	 * Username
	 * @var String
	 */
	protected $username_ = Auth::anonymous_user;

	/**
	 * Password
	 * @var String
	 */
	protected $password_ = Auth::anonymous_pw;

	/**
	 * Flag if the user is anonymous
	 * @var bool
	 */
	protected $is_anonymous;

	/**
	 * Flag if the user is logged in
	 * @var bool
	 */
	protected $is_logged_in;

	/**
	 * Flag if the user is valid
	 * @var bool
	 */
	protected $is_valid;

	/**
	 * Constructor
	 * @param String Auth::anonymous_user $username
	 * @param String Auth::anonymous_pw $password
	 */
	public function __construct($username=Auth::anonymous_user,
		$password=Auth::anonymous_pw)
	{
		$this->username_ = empty($username) ? Auth::anonymous_user : $username;
		$this->password_ = empty($password) ? Auth::anonymous_pw   : $password;
		parent::__construct($this->username_);
		
		$this->is_anonymous = ($this->username_ == Auth::anonymous_user ||
			$this->password_ == Auth::anonymous_pw);
	}

	/**
	 * Check if user is anonymous
	 * @return bool
	 */
	public function isAnonymous()
	{
		return $this->is_anonymous;
	}

	/**
	 * Check if the user is logged in
	 * @return bool
	 */
	public function isLoggedIn()
	{
		return (bool) $this->is_logged_in;
	}

	/**
	 * Check if the user is valid
	 * @return bool
	 */
	public function isValid()
	{
		return (bool) $this->is_valid;
	}

	/**
	 * Login function
	 * @return bool
	 */
	abstract public function login();

	/**
	 * Generates the password in a storable format
	 * This function will encrypt (if required) the password and return it.
	 * The use of encryption depends on the preferences that were set for
	 * this site.
	 *
	 * @param 	string $password
	 * @return string
	 */
	public static function encryptPassword($password)
	{
		if(isset($GLOBALS['WEBprefs']['User.Encryption'])) {
			$encryption  = $GLOBALS['WEBprefs']['User.Encryption'];
		}
		else {
			$encryption  = isset($GLOBALS['WEBprefs']['user']['encryption']) ?
				$GLOBALS['WEBprefs']['user']['encryption'] : '';
		}
		
		return !empty($encryption) && is_callable($encryption) ?
			call_user_func($encryption, $password) : $password;
	}
	
	/**
	 * Allows for the fetching of custom site_user_data entries
	 * @param string $name
	 * @return mixedvar
	 */
	public function __get($name)
	{
		switch ($name) {
			case 'id':
				return $this->_auth_id;
			case 'auth_id':
				return $this->_auth_id;
			case 'username':
				return $this->username_;
			case 'password':
				return $this->password_;
		}
	}
}
