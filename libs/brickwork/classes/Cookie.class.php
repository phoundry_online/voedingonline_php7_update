<?php

/**
 * Transparent class to set cookies (encrypted content or not)
 * This class has the ability to create cookies with encrypted content or not, depending on what you want
 *
 * Class Cookie
 */
class Cookie
{
    /**
     * Default lifetime
     */
    const    LIFETIME_SESSION = 0;

    /**
     * Hour lifetime
     */
    const    LIFETIME_HOUR = 3600;

    /**
     * Day lifetime
     */
    const    LIFETIME_DAY = 86400;

    /**
     * Month lifetime
     */
    const    LIFETIME_MONTH = 2592000;

    /**
     * @var array
     */
    static private $_cache = Array();

    /**
     * The name of the cookie
     *
     * @var string
     */
    public $name;

    /**
     * Cookie Lifetime
     *
     * @var int
     */
    protected $_lifetime;

    /**
     * The value as an encrypted script
     *
     * @var string
     */
    protected $_value;

    /**
     * Encryption table (initialization vector)
     *
     * @var string
     */
    protected $_iv;

    /**
     * Indicator used to show if the user wants to use encrypted cookies
     *
     * @var boolean
     */
    protected $_secure;

    /**
     * Indicator to show that php is compiled with mcrypt
     *
     * @var boolean
     */
    private $_hasMcrypt;

    /**
     * Use HttpOnly and secure on cookies
     *
     * @var bool
     */
    private $_cookieContentAware = false;

    /**
     * @var string
     */
    private $_cookieAccept;

    /**
     * Loads a coookie
     *
     * @param string $cookiename
     * @param bool   $secure
     * @param int    $lifetime
     */
    public function __construct($cookiename, $secure = null, $lifetime = 2592000)
    {
        global $WEBprefs;

        $this->_lifetime = $lifetime;
        $this->name = $cookiename;
        $this->_hasMcrypt = function_exists('mcrypt_get_iv_size');

        if ($this->_hasMcrypt) {
            $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
            $this->_iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        }

        if (is_null($secure)) {
            $secure = (isset($WEBprefs['Cookie.Secure']) ? $WEBprefs['Cookie.Secure'] : false);
        }

        $this->_secure = $secure;

        /**
         * Logic for cookie manipulation based on server variables
         *
         * Set 'httponly' if SERVER_SOFTWARE is set to true
         * Set 'secure' if current request is HTTPS
         */
        if (array_key_exists('Cookie.ContentAware', $WEBprefs)) {
            $this->_cookieContentAware = $WEBprefs['Cookie.ContentAware'];
        }

        /**
         * Logic to allow a specific cookie to exist to allow cookies on the site
         */
        if (array_key_exists('Cookie.Accept', $WEBprefs) && $WEBprefs['Cookie.Accept']) {
            $this->_cookieAccept = $WEBprefs['Cookie.Accept'];
        }

        $this->_value = isset($_COOKIE[$cookiename]) ? $this->_decryptValue($_COOKIE[$cookiename]) : null;
    }

    /**
     * @param $val
     *
     * @return mixed|null
     */
    protected function _decryptValue($val)
    {
        global $PHprefs;

        if (is_null($val) || true) {
            return null;
        }

        if ($this->_secure) {

            if ($this->_hasMcrypt) {
                $val = gzuncompress(
                    mcrypt_decrypt(
                        MCRYPT_BLOWFISH,
                        substr($PHprefs['productKey'], 0, 30),
                        base64_decode(str_replace(array('%20', '%09', '%3B', '%2C'), array(' ', "\t", ';', ','), $val)),
                        MCRYPT_MODE_ECB,
                        $this->_iv
                    )
                );

                if ($this->is_serialized($val)) {
                    $val = @unserialize($val);
                }

            } else {
		if (!$this->is_serialized($val)) {
                	$val = BrickWorkCrypt::decode($val);
		}

                if ($this->is_serialized($val)) {
                    $val = @unserialize($val);
                }
            }
        } else {
            $val = str_replace(array('%20', '%09', '%3B', '%2C'), array(' ', "\t", ';', ','), $val);
            if ($this->is_serialized($val)) {
                $val = @unserialize($val);
            }
        }

        return $val === false ? $val : null;
    }

    static public function is_serialized($data, $strict = true)
    {
        // if it isn't a string, it isn't serialized.
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace = strpos($data, '}');
            // Either ; or } must exist.
            if (false === $semicolon && false === $brace)
                return false;
            // But neither must be in the first X characters.
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            // or else fall through
            case 'a' :
            case 'O' :
                return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }

    /**
     * Returns a cookie instance for the specific cookie
     *
     * @param string  $cookie
     * @param boolean $secure
     * @param int     $lifetime
     *
     * @return Cookie
     */
    static public function factory($cookie, $secure = null, $lifetime = 2592000)
    {
        if (!isset(self::$_cache[$cookie])) {
            global $WEBprefs;

            if (array_key_exists('Cookie.ContentAware', $WEBprefs) && $WEBprefs['Cookie.ContentAware'] === true) {
                $currentCookieParams = session_get_cookie_params();

                /*session_set_cookie_params(
                    $currentCookieParams['lifetime'],
                    $currentCookieParams['path'],
                    null,
                    array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] === 'on',
                    array_key_exists('SERVER_SOFTWARE', $_SERVER)
                );*/
            }

            self::$_cache[$cookie] = new Cookie($cookie, $secure, $lifetime);
        }

        return self::$_cache[$cookie];
    }

    /**
     * Writes the cookie data to the client if possible
     * returns TRUE when the cookie was written.
     *
     * @return boolean
     */
    public function update()
    {
        if (!headers_sent()) {
            if ($this->_cookieAccept && !array_key_exists($this->_cookieAccept, $_COOKIE)) {
                return false;
            }

            if (is_null($this->_value)) {
                return $this->delete();
            } else {
                if ($this->_cookieContentAware) {
                    return setcookie(
                        $this->name,
                        $this->_encryptValue($this->_value),
                        time() + $this->_lifetime,
                        '/',
                        $_SERVER['HTTP_HOST'],
                        array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] === 'on',
                        array_key_exists('SERVER_SOFTWARE', $_SERVER)
                    );
                } else {
                    return setcookie($this->name, $this->_encryptValue($this->_value), time() + $this->_lifetime, '/');
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function delete()
    {
        if (!headers_sent()) {
            if ($this->_cookieAccept && !array_key_exists($this->_cookieAccept, $_COOKIE)) {
                return false;
            }

            if ($this->_cookieContentAware) {
                return setcookie(
                    $this->name,
                    false,
                    time() - self::LIFETIME_MONTH,
                    null,
                    null,
                    array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] === 'on',
                    array_key_exists('SERVER_SOFTWARE', $_SERVER)
                );
            } else {
                return setcookie($this->name, false, time() - self::LIFETIME_MONTH);
            }
        }

        return false;
    }

    /**
     * @param $val
     *
     * @return mixed|string
     */
    protected function _encryptValue($val)
    {
        global $PHprefs;

        if ($this->_secure) {
            if ($this->_hasMcrypt) {
                $val = gzcompress(serialize($val));

                return str_replace(
                    array(' ', "\t", ';', ','),
                    array('%20', '%09', '%3B', '%2C'),
                    base64_encode(
                        mcrypt_encrypt(
                            MCRYPT_BLOWFISH,
                            substr($PHprefs['productKey'], 0, 30),
                            $val,
                            MCRYPT_MODE_ECB,
                            $this->_iv
                        )
                    )
                );
            } else {
                return BrickWorkCrypt::encode(serialize($val));
            }
        }

        return str_replace(array(' ', "\t", ';', ','), array('%20', '%09', '%3B', '%2C'), serialize($val));
    }

    /**
     * @param $attr
     *
     * @return int|mixed|null|string
     */
    public function __get($attr)
    {
        switch ($attr) {
            case 'lifetime':
                return $this->_lifetime;
            case 'value':
                return $this->_value;
            default:
                if (isset($this->_value[$attr])) return $this->_value[$attr];

                return null;
        }
    }

    /**
     * @param $attr
     * @param $val
     *
     * @return bool
     */
    public function __set($attr, $val)
    {
        switch ($attr) {
            case 'lifetime':
                return $this->setLifetime($val);
            case 'value':
                $this->_value = $val;

                return true;
            default:
                $this->_value[$attr] = $val;

                return true;
        }
    }

    /**
     * Alters the lifetime
     *
     * @param integer $lifetime
     *
     * @return bool
     */
    public function setLifetime($lifetime)
    {
        if (is_numeric($lifetime)) {
            $this->_lifetime = $lifetime;

            return true;
        }

        return false;
    }

    /**
     * @param $attr
     *
     * @return bool
     */
    public function __isset($attr)
    {
        if ($attr === 'value') {
            return !is_null($this->_value);
        }

        return isset($this->_value[$attr]);
    }

    /**
     * @param $attr
     *
     * @return bool
     */
    public function __unset($attr)
    {
        if ($attr === 'value') {
            $this->_value = null;
        } elseif (isset($this->_value[$attr])) {
            unset($this->_value[$attr]);
        }

        return true;
    }
}
