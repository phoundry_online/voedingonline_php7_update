<?php
/**
 * Reactions block
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @package Reactions
 * 
CREATE TABLE `brickwork_reaction` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `group_id` int(10) unsigned NOT NULL,
  `site_user_id` int(10) unsigned NOT NULL,
  `author` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `body` text,
  `ip` int(10) default NULL,
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created_on` datetime default NULL,
  `uid` char(32) default NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE `brickwork_reaction_group` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `relation_id` int(10) unsigned NOT NULL,
  `relation_name` varchar(50) NOT NULL default '',
  `closed` tinyint(1) default NULL,
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created_datetime` datetime NOT NULL,
  `reaction_count` int(10) unsigned NOT NULL,
  `title` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE `brickwork_reaction_media` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `reaction_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `mediatype` enum('audio','video','image','youtube','link') NOT NULL default 'image',
  `created_datetime` datetime NOT NULL,
  `original` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `reaction_id` (`reaction_id`)
);

CREATE TABLE `brickwork_reaction_site_config` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_identifier` varchar(20) NOT NULL default '',
  `relation_name` varchar(50) default '',
  `enabled` tinyint(1) unsigned NOT NULL default '0',
  `reaction_order_by_colom` varchar(50) NOT NULL default 'created_on',
  `reaction_order_by` enum('ASC','DESC') NOT NULL default 'ASC',
  `images` int(2) unsigned NOT NULL default '4',
  `youtube` int(2) unsigned NOT NULL default '1',
  `allow_html` tinyint(1) unsigned NOT NULL default '0',
  `allow_ubb` tinyint(1) unsigned NOT NULL default '0',
  `allow_smilies` tinyint(1) unsigned NOT NULL default '0',
  `auto_link` tinyint(1) unsigned NOT NULL default '1',
  `bad_words` tinyint(1) unsigned NOT NULL default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Uniek` (`site_identifier`,`relation_name`)
);
 */
final class Reactions
{
	public static $group_class		= 'Reactions_Group';
	public static $reaction_class	= 'Reactions_Reaction';
	public static $media_class		= 'Reactions_Media';
	public static $config_class		= 'Reactions_SiteConfig';
	public static $relationconfig_class = 'Reactions_Relationconfig';
	
	/**
	 * Reaction group factory
	 * @param int $id
	 * @param bool $load
	 * @return Reactions_Group
	 */
	public static function group($id = null, $load = true)
	{
		return ActiveRecord::factory(self::getClass('group'), $id, $load);
	}
	
	/**
	 * Get a reaction group by its relation
	 * @param string $relation_name
	 * @param int $relation_id
	 * @param string $title
	 * @param bool $auto_create
	 * @return Reactions_Group
	 */
	public static function groupByRelation($relation_name, $relation_id,
		$title = '', $auto_create = false)
	{
		$class = self::getClass('group');
		return call_user_func(array($class, 'getByRelation'), $relation_name,
			$relation_id, $title, $auto_create);
	}
	
	/**
	 * Get a reaction group by its hash
	 * @param string $hash
	 * @return Reactions_Group
	 */
	public static function groupByHash($hash)
	{
		$class = self::getClass('group');
		return call_user_func(array($class, 'getByHash'), $hash);		
	}
	
	/**
	 * Reaction factory
	 * @param int $id
	 * @param bool $load
	 * @return Reactions_Reaction
	 */
	public static function reaction($id = null, $load = true)
	{
		return ActiveRecord::factory(self::getClass('reaction'), $id, $load);
	}
	
	/**
	 * Media factory
	 * @param int $id
	 * @param bool $load
	 * @return Reactions_Media
	 */
	public static function media($id = null, $load = true)
	{
		return ActiveRecord::factory(self::getClass('media'), $id, $load);
	}
	
	/**
	 * Relation config factory
	 * @param int $id
	 * @param bool $load
	 * @return Reactions_Relationconfig
	 */
	public static function relationConfig($id = null, $load = true)
	{
		return ActiveRecord::factory(self::getClass('relationConfig'), $id, $load);
	}
	
	/**
	 * Relation config factory
	 * @param $name
	 * @return Reactions_Relationconfig
	 */
	public static function relationConfigByRelationName($relation_name)
	{
		$class = self::getClass('relationConfig');
		return call_user_func(array($class, 'getConfig'), $relation_name);
	}
	
	/**
	 * Config factory for old templates
	 * @deprecated
	 * @param int $id
	 * @param bool $load
	 * @return Reactions_SiteConfig
	 */
	public static function config($id = null, $load = true)
	{
		return ActiveRecord::factory(self::getClass('config'), $id, $load);
	}
	
	/**
	 * Config factory for old templates
	 * @deprecated
	 * @param string $site_identifier
	 * @param string $name
	 */
	public static function configByName($site_identifier, $name)
	{
		$class = self::getClass('config');
		return call_user_func(array($class, 'getConfig'), $site_identifier, $name);		
	}
	
	/**
	 * Activate a reaction after confirmation
	 * 
	 * @param Reactions_Reaction|int $reaction Either a reaction or a id
	 * @return Reactions_Reaction|bool
	 */
	public static function activate($reaction)
	{
		if(!$reaction instanceof Reactions_Reaction && $reaction) {
			$reaction = Reactions::reaction((int) $reaction);
		}
		
		if($reaction && $reaction->isLoaded() && $reaction->activate()) {
			return $reaction;
		}
		return false;
	}
	
	/**
	 * Tells the classname for the various types
	 * @param string $type
	 * @return string
	 */
	public static function getClass($type)
	{
		$class = Utility::arrayValueRecursive($GLOBALS, array('WEBprefs',
			'reactions', 'classes', $type));
		if($class) {
			return $class;
		}
		
		switch ($type) {
			case 'group':
				return self::$group_class;
			case 'reaction':
				return self::$reaction_class;
			case 'media':
				return self::$media_class;
			case 'relationConfig':
				return self::$relationconfig_class;
			case 'config':
				return self::$config_class;
		}
		
		throw new InvalidArgumentException('Unknown type');
	}
}
