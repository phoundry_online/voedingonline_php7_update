<?php
class Model_Site_Language extends ORM
{
	protected $_table_name = 'brickwork_site_language';
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('code', 'ASC');
	
	protected $_many_to_one = array(
		'site' => array('Model_Site', 'site_identifier'),
	);
	
	protected $_one_to_many = array(
		'pages'		=> array('Model_Page', 'lang_id'),
		'urls'		=> array('Model_Url', 'lang_id'),
	);

	/**
	 * List of ISO 8859-1 safe language codes
	 *
	 * @var string
	 */
	static protected $_iso88591 = array('af','sq','eu','ca','da','nl','en','fo','fi','fr','de','gl','is','ga','it','no','pt','gd','es','sv','se');

	/**
	 * Get the language part of the Locale and checks it against the list of all ISO8859-1 locales
	 *
	 * @return bool
	 */
	public function isISO8895()
	{
		$code = strtolower(substr($this['locale'], 0, 2));
		return in_array($code, self::$_iso88591);
	}
}