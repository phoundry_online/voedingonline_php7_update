<?php
/**
 * Model Site Structure Path
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class Model_Site_Structure_Path extends ORM
{
	protected $_table_name = 'brickwork_site_structure_path';

	protected $_many_to_one = array(
		'site' => array('model_Site', 'site_identifier'),
		'site_structure' => array('Model_Site_Structure', 'site_structure_id'),
		'site_language' => array('Model_Site_Language', 'lang_id'),
	);

	/**
	 * Check if a path already exists
	 * @param string $name
	 * @param Model_Site_Structure $structure
	 * @param int $lang_id
	 * @return bool
	 */
	protected static function pathInUse($name, Model_Site_Structure $structure, $lang_id)
	{
		$lang_sql = '';
		if (Features::brickwork('translation')) {
			$lang_sql = sprintf("AND lang_id = %s", DB::quoteValue($lang_id));
		}
		return (0 != DB::iQuery(sprintf("
			SELECT
				COUNT(*) AS cnt
			FROM
				brickwork_site_structure_path
			WHERE
				site_identifier = %s
			AND
				hash = %s
			AND
				site_structure_id != %s
			%s
			",
			DB::quoteValue($structure['site_identifier']),
			DB::quoteValue(md5($name)),
			DB::quoteValue($structure['id']),
			$lang_sql
		))->first()->cnt);
	}

	/**
	 * Postfix the name with a number untill
	 * the name is unique if neccesarry at all
	 *
	 * @param string $name
	 * @param int $structure_id
	 * @param int $lang_id
	 * @return string
	 */
	protected static function getUniqueName($name, $structure, $lang_id = null)
	{
		$lang_id = is_null($lang_id) ? 0 : $lang_id;

		while(self::pathInUse($name, $structure, $lang_id))
		{
			if(preg_match('/\-(\d)+$/', $name, $match))
			{
				$name = preg_replace('/\-(\d)+$/', '-'.++$match[1], $name);
			}
			else
			{
				$name.='-1';
			}
		}

		return $name;
	}

	/**
	 * Returns the path to use for the structure
	 * based on I18n records and the structure itself
	 * 
	 * @param Model_Site_Structure $structure
	 * @param Model_Site_Language $lang
	 * @return string
	 */
	protected static function getPathName(Model_Site_Structure $structure,
		Model_Site_Language $lang = null)
	{
		if(!$structure->isLoaded()) {
			throw new Exception('Structure does not exist');
		}

		$lang_id = 0;
		$name = $structure['name'];

		// See if we got a name for this specific language
		if(null !== $lang && $lang->isISO8895()) {
			$i18n = ActiveRecord::factory("Model_Site_Structure_I18n")->
				where(array(
					'site_structure_id' => $structure['id'],
					'lang_id' => $lang['id']
				))->get(true);
			
			if($i18n->isLoaded()) {
				$lang_id = $lang['id'];
				$name = $i18n['name'];
			}
		}

		$name = Utility::toUri($name);

		// Prepend with the path of its parent
		if($structure['parent_id'] && $structure->parent->isLoaded())
		{
			$name = self::getPathName($structure->parent, $lang).'/'.$name;
		}

		$name = self::getUniqueName($name, $structure, $lang_id);

		return $name;
	}

	/**
	 * Update the path to reflect the current values in the database
	 * 
	 * @param Model_Site_Structure $structure
	 * @param Model_Site_Language $lang
	 * @return Model_Site_Structure_Path
	 */
	public static function updatePath(Model_Site_Structure $structure, Model_Site_Structure_I18n $i18n = null)
	{
		if(!$structure->isLoaded())
		{
			throw new Exception('Structure does not exist');
		}
		if(!is_null($i18n) && !$i18n->isLoaded())
		{
			throw new Exception('I18n does not exist');
		}

		$lang_id = is_null($i18n) ? 0 : $i18n->site_language['id'];

		$path = ActiveRecord::factory(__CLASS__)->
			where('site_identifier', $structure['site_identifier'])->
			where('site_structure_id', $structure['id']);
		if (Features::brickwork('translation')) {
			$path = $path->where('lang_id', $lang_id);
		}
		$path = $path->get(true);

		if ($path) {
            $path['site_identifier'] = $structure['site_identifier'];
            $path['site_structure_id'] = $structure['id'];
            if (Features::brickwork('translation')) {
                $path['lang_id'] = $lang_id;
            }
            $path['name'] = self::getPathName($structure, $i18n ? $i18n->site_language : null);
            $path->save();
        }  else {
		    $name = self::getPathName($structure, $i18n ? $i18n->site_language : null);
		    
		    DB::iQuery(sprintf(
		        'INSERT INTO brickwork_site_structure_path 
                        (`site_identifier`, `hash`, `name`, `site_structure_id`, `lang_id`)
                        VALUES (%s, %s, %s, %s, %s)',
                DB::quoteValue($structure['site_identifier']),
                DB::quoteValue(md5($name)),
                DB::quoteValue($name),
                DB::quoteValue($structure['id']),
                DB::quoteValue(0)
            ));

            $path = ActiveRecord::factory(__CLASS__)->
            where('site_identifier', $structure['site_identifier'])->
            where('site_structure_id', $structure['id']);
            if (Features::brickwork('translation')) {
                $path = $path->where('lang_id', $lang_id);
            }
            $path = $path->get(true);
		    
        }
		return $path;
	}

	/**
	 * Archive the old path if this one get saved and is altered
	 * @see ActiveRecord#save()
	 */
	public function save()
	{
		$this['hash'] = md5($this['name']);

		if($this->isAltered())
		{
			if(Features::brickwork('translation'))
			{
				// @todo gebruik DB::insertInto();
				DB::iQuery(sprintf("
					INSERT IGNORE INTO
						brickwork_site_structure_path_archive
						(site_identifier, hash, name, site_structure_id, lang_id, archive_date)
					VALUES
						(%s, %s, %s, %s, %s, NOW())
					",
					DB::quoteValue($this['site_identifier']),
					DB::quoteValue($this['hash']),
					DB::quoteValue($this['name']),
					DB::quoteValue($this['site_structure_id']),
					DB::quoteValue($this['lang_id'])
				));
			}
			else
			{
				DB::iQuery(sprintf("
					INSERT IGNORE INTO
						brickwork_site_structure_path_archive
						(site_identifier, hash, name, site_structure_id, archive_date)
					VALUES
						(%s, %s, %s, %s, NOW())
					",
					DB::quoteValue($this['site_identifier']),
					DB::quoteValue($this['hash']),
					DB::quoteValue($this['name']),
					DB::quoteValue($this['site_structure_id'])
				));
			}
		}

		return parent::save();
	}

	/**
	 * Also delete all archive paths
	 * @return bool
	 */
	public function delete()
	{
		$site_identifier = $this['site_identifier'];
		$site_structure_id = $this['site_structure_id'];

		$lang_sql = '';
		if (Features::brickwork('translation')) {
			$lang_sql = 'AND lang_id = '.DB::quoteValue($this['lang_id']);
		}

		if(parent::delete()) {
			DB::iQuery(sprintf("
				DELETE FROM
					brickwork_site_structure_path_archive
				WHERE
					site_identifier = %s
				AND
					site_structure_id = %s
				%s
				",
				DB::quoteValue($site_identifier),
				DB::quoteValue($site_structure_id),
				$lang_sql
			));

			return true;
		}
		return false;
	}
}
