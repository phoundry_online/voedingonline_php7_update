<?php
class Model_Site_Structure_I18n extends ORM
{
	protected $_table_name = 'brickwork_site_structure_i18n';
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('name', 'ASC');
	
	protected $_many_to_one = array(
		'site_structure' => array('Model_Site_Structure', 'site_structure_id'),
		'site_language' => array('Model_Site_Language', 'lang_id'),
	);

	public function save()
	{
		if(parent::save())
		{
			Model_Site_Structure_Path::updatePath($this->site_structure, $this);
			return true;
		}
		return false;
	}

	public function delete($children = true)
	{
		if($this->isLoaded() && $children)
		{
			$path = ActiveRecord::factory('Model_Site_Structure_Path')->
				where('site_identifier', $this->site_structure['site_identifier'])->
				where('site_structure_id', $this['site_structure_id'])->
				where('lang_id', $this['lang_id'])->
			get(true);

			$path->delete();
		}
		return parent::delete();
	}

	public function copy($new_structure_id)
	{
		$new = ActiveRecord::factory(__CLASS__);
		foreach($this->getAsArray() as $k => $v)
		{
			switch ($k)
			{
				case 'id':
					$new[$k] = null;
				break;
				case 'site_structure_id':
					$new[$k] = $new_structure_id;
				break;
				default:
					$new[$k] = $v;
			}
		}
		$new->save();

		return $new;
	}
}