<?php
/**
 * Site User
 * 
 * @author Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @since 2009-06-23
 * @version 0.0.1 Beta
 * 
 * @property ActiveRecordIterator $login_activity Model_Site_User_Login items
 * @property ActiveRecordIterator $data Model_Site_User_Data items
 * @property ActiveRecordIterator $user_site Model_Site_User_Site items
 * @property ActiveRecordIterator $roles Model_Site_User_Role items
 */
class Model_Site_User extends ORM
{
	protected $_table_name = 'brickwork_site_user';

	protected $_one_to_many = array(
		'login_activity' => array('Model_Site_User_Login', 'site_user_id'),
		'data' => array('Model_Site_User_Data', 'site_user_id'),
		'user_site' => array('Model_Site_User_Site', 'site_user_id'),
	);
	
	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
		'roles' => array('Model_Site_User_Role', 'brickwork_site_user_auth_role', 'site_user_id', 'auth_role_id'),
	);

	/**
	 * (non-PHPdoc)
	 * @see ActiveRecord::delete()
	 */
	public function delete()
	{
		// delete child objects
		foreach($this->login_activity as $child) $child->delete();
		foreach($this->data as $child) $child->delete();
		foreach($this->user_site as $child) $child->delete();
		$this->remove('roles', $this->roles);
		return parent::delete();
	}

	public function dataGet($attr)
	{
		$res = DB::iQuery(sprintf("
			SELECT
				value
			FROM
				brickwork_site_user_data
			WHERE
				site_user_id = %s
			AND
				attribute = %s
			",
			DB::quoteValue($this['id']),
			DB::quoteValue($attr)
		))->first();

		return $res ? $res->value : null;
	}	

	public function dataSet($attr, $value)
	{
		$res = DB::iQuery(sprintf("
			INSERT INTO
				brickwork_site_user_data (site_user_id, attribute, value)
			VALUES
				(%s, %s, %s)
			ON DUPLICATE KEY UPDATE
				value = VALUES(value), modified_ts = NOW()
			",
			DB::quoteValue($this['id']),
			DB::quoteValue($attr),
			DB::quoteValue($value),
			DB::quoteValue($value)
		));
		
		return (!self::isError($res));
	}

	
	/**
	 * Generate a new random password
	 * 
	 * @param int $length
	 * @return string
	 */
	public static function generateNewPassword($length = 10)
	{
		$pass_chars = str_split('abcdefghjkmnpqrstuvwxyz0123456789@#$%*');
		$choosen_keys = array_rand($pass_chars, $length);
		$new_password = '';
		foreach($choosen_keys as $key)
		{
			$new_password .= (rand(0,1) == 1) ? strtoupper($pass_chars[$key]) : $pass_chars[$key];
		}
		return $new_password;
	}
	
	/**
	 * Get a Site User by its email and a site to which it should be bound
	 * 
	 * @param string $email
	 * @param Site $site site to which the user should be bound
	 * @param bool $auto_create create the user if it doesn't exist
	 * @return Model_Site_User|bool false if not found and no auto_create
	 */
	public static function getUserByEmailAndSite($email, Site $site,
		$auto_create = false)
	{
		$db = DB::getDb();
		$email = (string) $email;
		
		$user = new self(null, false, $db);

		$res = $db->iQuery(sprintf("
			SELECT
				%s
			FROM
				brickwork_site_user AS u
			INNER JOIN
				brickwork_site_user_site AS s
			ON
				s.site_user_id = u.id
			WHERE
				site_identifier = %s
			AND
				email = %s",
			$user->getColumnsSql('u'),
			$db->quote($site->identifier, true),
			$db->quote($email, true)
		));
		
		if($row = $res->first()) {
			return $user->loadResult($row);
		}
		
		if($auto_create) {
			$user['email'] = $email;
			$user->save();
			$site_user_site = ActiveRecord::factory('Model_Site_User_Site');
			$site_user_site['site_user_id'] = $user['id'];
			$site_user_site['site_identifier'] = $site->identifier;
			$site_user_site->save();
			return $user;
		}
		return false;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
