<?php
/**
 * Model Site Structure
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 * 
 * @property Model_Site $site
 * @property Model_Site_Structure $parent
 * @property ActiveRecordIterator $children
 * @property ActiveRecordIterator $translations
 * @property ActiveRecordIterator $i18ns
 * @property ActiveRecordIterator $pages
 * @property ActiveRecordIterator $urls
 * @property ActiveRecordIterator $paths
 */
class Model_Site_Structure extends ORM
{
	/**
	 * Tablename
	 * @var string
	 */
	protected $_table_name = 'brickwork_site_structure';

	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
		'children'		=> array('Model_Site_Structure', 'parent_id'),
		'translations'	=> array('Model_Site_Structure_I18n', 'site_structure_id'),
		'i18ns'			=> array('Model_Site_Structure_I18n', 'site_structure_id'),
		'pages'			=> array('Model_Page', 'site_structure_id'),
		'urls'			=> array('Model_Url', 'site_structure_id'),
		'paths'			=> array('Model_Site_Structure_Path', 'site_structure_id'),
	);

	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
		'parent'	=> array('Model_Site_Structure', 'parent_id'),
		'site'		=> array('Model_Site', 'site_identifier'),
	);

	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('prio', 'ASC');

	/**
	 * Cache the result of getACLMap
	 * @var array
	 */
	protected static $_ACLmap;

	/**
	 * Cache the SiteStructures
	 * @var array
	 */
	protected static $_registry;

	/**
	 * Registry
	 * @param int $id
	 * @param bool $load
	 * @return Model_Site_Structure
	 */
	public static function registry($id = null, $load = true)
	{
		if(!is_null($id))
		{
			$key = is_array($id) ? implode(',', $id) : $id;
			if(!isset(self::$_registry[$key]))
			{
				self::$_registry[$key] = new self($id, $load);
			}
			return self::$_registry[$key];
		}

		return new self();
	}

	/**
	 * Get the amount of active associated pages
	 * @return int
	 */
	public function getPageCount()
	{
		return (int) DB::iQuery(sprintf('
			SELECT
				count(*) AS cnt
			FROM
				brickwork_page
			WHERE
				site_structure_id = %d
			AND
				NOW() BETWEEN online_date AND offline_date
			',
			$this['id']
		))->first()->cnt;
	}

	/**
	 * Get the amount of active associated urls
	 * @return int
	 */
	public function getUrlCount()
	{
		return (int) DB::iQuery(sprintf('
			SELECT
				count(*) AS cnt
			FROM
				brickwork_url
			WHERE
				site_structure_id = %d
			',
			$this['id']
		))->first()->cnt;
	}

	/**
	 * Update the Structure's associated ACL object
	 */
	public function updateAcl()
	{
		if(Features::brickwork('ACL')) {
			self::initAcl($this['site_identifier']);
			$parent_id = self::getAcl($this['site_identifier'], is_null($this['parent_id']) ? 'page' : $this['parent_id']);
			self::addAcl(
				$this['site_identifier'],
				$this['id'],
				'StructureItem/'.$this['id'],
				$this['name'],
				'SiteStructure',
				$this['id'],
				$parent_id
			);
		}
	}
	
	/**
	 * Update the Path associated to the structure
	 */
	public function updatePath()
	{
		Model_Site_Structure_Path::updatePath($this);
	}

	/**
	 * Update associated I18n records
	 * @param array $translations
	 */
	public function updateTranslations($translations = array())
	{
		if(Features::brickwork('translation'))
		{
			try {
				$languages = ActiveRecord::factory('Model_Site_Language')->
					where('site_identifier', $this['site_identifier'])->
				get();
			} catch (Exception $e){}

			if($languages->current())
			{
				while($languages->current())
				{
				    $lang = $languages->current();
					$trans = ActiveRecord::factory('Model_Site_Structure_I18n')->
						where('site_structure_id', $this['id'])->
						where('lang_id', $lang['id'])->
					get(true);
					if ($trans) {
                        $trans['site_structure_id'] = $this['id'];
                        $trans['lang_id'] = $lang['id'];
                        $trans['name'] = Utility::arrayValue($translations, $lang['id']);
                        ($trans['name'] != '') ? $trans->save() : $trans->delete();
                        Translation::getByLanguageId($lang['id'])->flushCache();
                    }
					$languages->next();
				}
			}
		}
	}

	/**
	 * Extend the default delete to also delete the children
	 * @return bool
	 */
	public function delete($children = true)
	{
		if($this->isLoaded() && $children)
		{
			if(Features::brickwork('ACL')) {
				Model_ACL_Object::delete('StructureItem/'.$this['id']);
			}
			foreach($this->pages as $page) $page->delete();
			foreach($this->urls as $url) $url->delete();
			if(Features::brickwork('translation')) {
				foreach($this->i18ns as $i18n) $i18n->delete();
			}
			foreach($this->paths as $path) $path->delete();

			DB::iQuery(sprintf("
				DELETE FROM
					brickwork_custom_site_structure
				WHERE
					custom_id = %s
				",
					DB::quoteValue($this['id'])
			));

			foreach($this->children as $child)
			{
				$child['parent_id'] = $this['parent_id'];
				$child->save();
			}
		}
		return parent::delete();
	}

	/**
	 * Copy a structure item
	 * @param string $new_site_id New site_identifier
	 * @param bool $sub_structures Also copy substructures
	 * @return Model_Site_Structure The fresh copy
	 */
	public function copy($new_site_id)
	{
		if($this->isLoaded())
		{
			$new = ActiveRecord::factory(__CLASS__);
			foreach($this->getAsArray() as $k => $v)
			{
				$new[$k] = ('id' == $k) ? null : $v;
			}
			$new['site_identifier'] = $new_site_id;

			if($new->save())
			{
				foreach($this->pages as $page)
				{
					$page->copy($new['id']);
				}

				foreach($this->urls as $url)
				{
					$url->copy($new['id']);
				}

				if(Features::brickwork('translation')) {
					foreach($this->i18ns as $i18n)
					{
						$i18n->copy($new['id']);
					}
				}

				$res = DB::iQuery('DESC brickwork_custom_site_structure');

				if($res->num_rows > 1)
				{
					$res->setReturnType(DB_ResultSet::RETURNTYPE_ASSOC);
					$fields = array();
					while($res->current())
					{
					    $row = $res->current();
						if('custom_id' != $row['Field']) $fields[] = $row['Field'];
						$res->next();
					}
					
					DB::iQuery(sprintf("
						INSERT INTO
							brickwork_custom_site_structure
						(
						SELECT
							%s AS custom_id, 
							%s
						FROM
							brickwork_custom_site_structure
						WHERE
							custom_id = %s
						)
						",
						DB::quoteValue($new['id']),
						'`'.implode('`, `', $fields).'`',
						DB::quoteValue($this['id'])
					));
				}
			}

			return $new;
		}
	}
	
	/**
	 * Turns the structure in a json compatible array
	 * @return array|void
	 */
	public function getAsJsArray()
	{
		if($this->isLoaded())
		{
			$arr = $this->getAsArray();
			$arr['show_menu'] = (int) $arr['show_menu'];
			$arr['show_sitemap'] = (int) $arr['show_sitemap'];
			$arr['secure'] = (int) $arr['secure'];
			$arr['restricted'] = (int) $arr['restricted'];
			$arr['ssl'] = (int) $arr['ssl'];
			$arr['state'] = 'current';

			$custom_site_structure = PhoundryTools::getTableTID('brickwork_custom_site_structure');
			$custom_site_structure_rid = null;
			if($custom_site_structure)
			{
				$res = DB::iQuery("SELECT custom_id FROM brickwork_custom_site_structure WHERE custom_id = ".(int) $this['id']);
				if($res->current()) {
					// Has a strange side effect that this flags the tree dirty even tough this value aint used for saving the tree
					$arr['custom_rid'] = (int) $this['id'];
				}
			}

			$arr['translations'] = array();
			try
			{
				while ($this->translations->current())
				{
                    $translation = $this->translations->current();
					$arr['translations'][$translation['lang_id']] = $translation['name'];
					$this->translations->next();
				}
				$this->translations->rewind();
			}
			catch (Exception $e) {}
			
			$arr['pages'] = array();
			foreach ($this->pages as $page)
			{
				$arr['pages'][] = $page->getAsJsArray();
			}

			$arr['urls'] = array();
			foreach ($this->urls as $url)
			{
				$arr['urls'][] = $url->getAsJsArray();
			}

			$arr['children'] = array();
			foreach ($this->children as $child)
			{
				$arr['children'][] = $child->getAsJsArray();
			}

			return $arr;
		}
	}

	/**
	 * (Decrepated) Create the readable path records
	 *
	 * @param string $site_identifier
	 * @param int $structure_id
	 * @param string $name
	 * @param string $lang
	 * @return bool
	 */
	public static function createReadablePath($site_identifier, $structure_id, $name, Site_Language $lang = NULL)
	{
		// An 0 integer indicates multilangual is turned off
		$langId = is_null($lang) ? 0 : $lang->getId();

		// Build path in reverse order (starting with the actual page and work our way up)
		SiteStructure::getSiteStructure($site_identifier, $lang);
		$stack = SiteStructure::getSiteStack($site_identifier);

		$path = array($name);

		if(isset($stack[$structure_id]))
		{
			$parent_ids = array($structure_id);
			$item = $stack[$structure_id];

			while(!empty($item->parent) && !in_array($item->parent, $parent_ids) && isset($stack[$item->parent]))
			{
				$item = $stack[$item->parent];
				$parent_ids[] = $item->id;
				array_unshift($path, $item->name);
			}
		}

		$ssp = new Site_Structure_Path($site_identifier, $structure_id, $lang, implode('/', $path));
		return  $ssp->update();
	}

	/**
	 * Initializes the ACL
	 * @param string $site_identifier
	 */
	protected static function initAcl($site_identifier)
	{
		if(!isset(self::$_ACLmap)) self::$_ACLmap = array();
		if(!isset(self::$_ACLmap[$site_identifier]))
		{
			self::$_ACLmap[$site_identifier] = array();
			self::addAcl(
				$site_identifier,
				'site',
				'Site/' . $site_identifier,
				$site_identifier,
				'Site'
			);
			self::addAcl(
				$site_identifier,
				'page',
				'PageContentHandler',
				'Website page content handler',
				'Site',
				null,
				self::getAcl($site_identifier, 'site')
			);
		}
	}

	/**
	 * Add a ACL auth_object and add it to the ACLmap
	 * 
	 * @param string $site_identifier
	 * @param string $key
	 * @param string $instance_id
	 * @param string $description
	 * @param string $class
	 * @param int	 $foreign_id
	 * @param int	 $parent_id
	 */
	protected static function addAcl($site_identifier, $key, $instance_id, $description, $class, $foreign_id = null, $parent_id = null)
	{
		self::$_ACLmap[$site_identifier][ $key ] = Model_ACL_Object::add(
			$instance_id,
			$description,
			$class,
			$foreign_id,
			$parent_id
		);
	}

	/**
	 * Get a ACL auth_object wich was previously created by Model_Site_Structure::addAcl()
	 *
	 * @param string $site_identifier
	 * @param string $key
	 * @return int|bool
	 */
	protected static function getAcl($site_identifier, $key)
	{
		return Utility::arrayValueRecursive(self::$_ACLmap, $site_identifier, $key);
	}
}
