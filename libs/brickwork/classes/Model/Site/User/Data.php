<?php
/**
 * Site User Data
 * 
 * @author Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @since 2009-06-23
 * @version 0.0.1 Beta
 */
class Model_Site_User_Data extends ORM
{
	protected $_table_name = 'brickwork_site_user_data';
	
	protected $_many_to_one = array(
		'user' => array('Model_Site_User', 'site_user_id'),
	);
}
