<?php
/**
 * Role which Site_User's can have
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2011 Web Power BV, http://www.webpower.nl
 * 
 * @property ActiveRecordIterator $users Model_Site_User items 
 */
class Model_Site_User_Role extends ORM
{
	// Table was used by ACL in previous versions which is now deprecated
	protected $_table_name = 'brickwork_auth_role';
	
	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
		'users' => array('Model_Site_User', 'brickwork_site_user_auth_role', 'auth_role_id', 'site_user_id'),
	);
	
	/**
	 * (non-PHPdoc)
	 * @see ActiveRecord::delete()
	 */
	public function delete()
	{
		$this->remove('users', $this->users);
		return parent::delete();
	}
}