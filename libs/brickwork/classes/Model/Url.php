<?php
class Model_Url extends ORM
{
	protected $_table_name = 'brickwork_url';
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('link', 'ASC');
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
		'site_structure' => array('Model_Site_Structure', 'site_structure_id'),
		'site_language' => array('Model_Site_Language', 'lang_id'),
	);

	/**
	 * Copy the page to another structure
	 * @param int $new_structure_id
	 * @return Model_Url
	 */
	public function copy($new_structure_id)
	{
		$new = ActiveRecord::factory(__CLASS__);
		foreach($this->getAsArray() as $k => $v)
		{
			switch ($k)
			{
				case 'id':
					$new[$k] = null;
				break;
				case 'site_structure_id':
					$new[$k] = $new_structure_id;
				break;
				default:
					$new[$k] = $v;
			}
		}
		$new->save();

		return $new;
	}
	
	/**
	 * Converts the Model_Url to a json compatbile array with some extra attributes
	 * @return array|void
	 */
	public function getAsJsArray()
	{
		if($this->isLoaded())
		{
			$u = $this->getAsArray();
			$u['link_truncated'] = Utility::truncate($this['link'], 40, "[...]", false, true);
			try {
				$u['language_code'] = $this->site_language['code'];
			} catch(Exception $e){
				$u['language_code'] = null;
			}
			return $u;
		}
	}
}