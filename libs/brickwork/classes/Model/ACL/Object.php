<?php
/**
 * Object
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class Model_ACL_Object
{
	/**
	 * Create a new or update an existing auth_object
	 *
	 * @param string $instance_id
	 * @param string $description
	 * @param string $class
	 * @param int	 $foreign_id
	 * @param int	 $parent_id
	 * @return int|bool The id of the object that matches the instance_id
	 */
	public static function add($instance_id, $description, $class, $foreign_id = null, $parent_id = null)
	{
		$res = DB::iQuery(sprintf("
			INSERT INTO
				brickwork_auth_object
				(parent_id, instanceid, description, class, foreign_id )
			VALUES
				(%s,%s,%s,%s,%s)
			ON DUPLICATE KEY UPDATE
				parent_id = VALUES(parent_id),
				description = VALUES(description),
				class = VALUES(class),
				foreign_id = VALUES(foreign_id)
			",
			DB::quoteValue($parent_id),
			DB::quoteValue($instance_id),
			DB::quoteValue($description),
			DB::quoteValue($class),
			DB::quoteValue($foreign_id)
		));

		$res = DB::iQuery(sprintf("SELECT id FROM brickwork_auth_object WHERE instanceid = '%s'", DB::quote($instance_id)));
		return ($row = $res->first()) ? $row->id : false;
	}

	public static function delete($instance_id)
	{
		DB::iQuery(sprintf("DELETE FROM brickwork_auth_object WHERE instanceid = '%s'", $instance_id));
	}
}
