<?php
/**
 * BrickworkModule class
 *
 */
class Model_Module extends ORM
{
	protected $_table_name = 'brickwork_module';

	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
		'allowed_page_content_containers' => array('Module_Page_Content_Container', 'brickwork_content_container_allows_module', 'module_code', 'content_container_id'),
	);
	
	/**
	 * String representation (used by array_unique)
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this['code'];
	}

}