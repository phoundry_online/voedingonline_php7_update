<?php
class Model_Site extends ORM
{
	protected $_table_name = 'brickwork_site';
	
	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
	);
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
		'site_structures'	=> array('Model_Site_Structure', 'site_identifier'),
		'site_languages'	=> array('Model_Site_Language', 'site_identifier'),
	);

	/**
	 * Get the url for the site
	 * @param bool $secure should the url start with https
	 * @return string|bool
	 */
	public function getUrl($secure = false)
	{
		$protocols = explode(',', $this['protocol']);
		if ($secure && !in_array('https', $protocols)) {
			return false;
		}
		$preferred = $secure ? 'https' : 'http';
		if (in_array($preferred, $protocols)) {
			$protocol = $preferred;
		} else {
			$protocol = reset($protocols);
		}

		if ($protocol) {
			return $protocol.'://'.$this['domain'];
		}
		return false;
	}
}