<?php
/**
 * Class representing the pages that are bound to the structure items 
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 * 
 * @property ActiveRecordIterator $page_contents The contents of the page
 * @property Model_Page_Type $page_type The pagetype of the current page
 * @property Model_Site_Structure $site_structure the parent structure
 * @property Model_Site_Language $site_language the language the page is in
 */
class Model_Page extends ORM
{

	protected $_table_name = 'brickwork_page';

	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('title', 'ASC');
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
		'page_contents' => array('Model_Page_Content', 'page_id'),
	);
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
		'page_type' => array('Model_Page_Type', 'page_type_code'),
		'site_structure' => array('Model_Site_Structure', 'site_structure_id'),
		'site_language' => array('Model_Site_Language', 'lang_id'),
	);
	
	public function getUrl()
	{
		if (defined('TESTMODE') && true === TESTMODE) {
			$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http');
			$domain = $_SERVER['HTTP_HOST'];
		} else {
			$protocols = explode(',', $this->site_structure->site['protocol']);
			$protocol = in_array('http', $protocols) ? 'http' : reset($protocols);
			$domain = $this->site_structure->site['domain'];
		}

		return sprintf(
			'%s://%s%s?brickwork_test=%s',
			$protocol,
			$domain,
			$this->getFullPath(),
			BrickWorkCrypt::urlEncode(
				array(
					'pid' => $this['id'],
					'sid' => $this['site_structure_id'],
					'staging' => $this['staging_status']
				)
			)
		);	
	}
	
	public function getFullPath()
	{
		return SiteStructure::getFullPathByStructureId(
			$this['site_structure_id'],
			$this->site_structure['site_identifier'],
			$this->offsetExists('lang_id') ? $this['lang_id'] : null
		);
	}
	
	public function delete()
	{
		if ($this->isLoaded()) {
			foreach ($this->page_contents as $pcontent) {
				$pcontent->delete();
			}
		}
		return parent::delete();
	}

	/**
	 * Copy the page to another structure
	 * @param int $new_structure_id
	 * @return Model_Page
	 */
	public function copy($new_structure_id)
	{
		$new = ActiveRecord::factory(__CLASS__);
		foreach ($this->getAsArray() as $k => $v) {
			switch ($k) {
				case 'id':
					$new[$k] = null;
					break;
				case 'site_structure_id':
					$new[$k] = $new_structure_id;
					break;
				default:
					$new[$k] = $v;
			}
		}

		if ($new->save()) {
			foreach ($this->page_contents as $page_content) {
				$page_content->copy($new['id']);
			}
		}

		return $new;
	}
	
	/**
	 * Converts the Model_Page in an json compatible array with some extra attributes
	 * @return array|void
	 */
	public function getAsJsArray()
	{
		if ($this->isLoaded()) {
			$p = $this->getAsArray();
			$p['title'] = Utility::truncate($this['title'], 20);
			$p['preview_url'] = $this->getUrl();
			$p['date'] = $this['staging_status'] == 'test' ? 'test' :
				(date("d-m-'y", strtotime($this['online_date'])).
				(is_null($this['offline_date']) ? '' : ' t/m '.
				date("d-m-'y", strtotime($this['offline_date']))));
			
			try {
				$p['language_code'] = $this->site_language['code'];
			} catch(Exception $e){
				$p['language_code'] = null;
			}
			return $p;
		}
	}
}
