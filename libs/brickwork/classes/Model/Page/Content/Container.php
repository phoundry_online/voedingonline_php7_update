<?php
/**
 * Model for the Content Containers
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 * 
 * @property ActiveRecordIterator $allowed_modules
 * @property ActiveRecordIterator $default_modules
 * @property Model_Page_Type $page_type
 */
class Model_Page_Content_Container extends ORM
{
	protected $_default_order_by = array('prio', 'ASC');
	
	/**
	 * Name of the Table the AR operates on
	 *
	 * @var string
	 */
	protected $_table_name = 'brickwork_content_container';

	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
		'allowed_modules' => array('Model_Module', 'brickwork_content_container_allows_module', 'content_container_id', 'module_code'),
		'default_modules' => array('Model_Module', 'brickwork_content_container_default_module', 'content_container_id', 'module_code'),
	);
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
		'page_type' => array('Model_Page_Type', 'page_type_code'),
	);

}