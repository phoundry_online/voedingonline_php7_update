<?php
class Model_Page_Content extends ORM
{

	protected $_table_name = 'brickwork_page_content';
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('prio', 'ASC');
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
		'page_content_container' => array('Model_Page_Content_Container', 'content_container_code'),
		'page' => array('Model_Page', 'page_id'),
		'module' => array('Model_Module', 'module_code'),
	);
	
	public function delete($children = true)
	{
		if($children)
		{
			// Delete Config and content records
			if(class_exists($this->module['class'])) {
				call_user_func(array($this->module['class'], 'deleteConfig'), $this['id']);
				call_user_func(array($this->module['class'], 'deleteContent'), $this['id']);
			}
			else {
				Webmodule::deleteConfig($this['id']);
				Webmodule::deleteContent($this['id']);
			}

			if(Features::brickwork('ACL'))
			{
				$res = DB::iQuery(sprintf("
					SELECT
						id
					FROM
						brickwork_auth_object
					WHERE
						instanceid = %s
					",
					DB::quoteValue($this->module['class'].'/'.$this['id'])
				));

				if($row = $res->first())
				{
					DB::iQuery(sprintf("
						DELETE FROM
							brickwork_auth_object
						WHERE
							instanceid = %s
						",
						DB::quoteValue($this->module['class'].'/'.$this['id'])
					));
					DB::iQuery(sprintf("
						DELETE FROM
							brickwork_auth_right
						WHERE
							auth_object_id = %s
						",
						DB::quoteValue($row->id)
					));
				}
			}
		}
		
		parent::delete();
	}

	/**
	 * Copy the page content to a different page
	 * @param int $new_page_id
	 * @return Model_Page_Content
	 */
	public function copy($new_page_id)
	{
		$new = ActiveRecord::factory(__CLASS__);
		foreach($this->getAsArray() as $k => $v)
		{
			switch ($k)
			{
				case 'id':
					$new[$k] = null;
				break;
				case 'page_id':
					$new[$k] = $new_page_id;
				break;
				default:
					$new[$k] = $v;
			}
		}

		if($new->save())
		{
			// Copy Module Content
			if($this->module['ptid_content'])
			{
				if(in_array('PageContentCopyable', class_implements($this->module['class'])))
				{
					call_user_func(
						array($this->module['class'], 'copyContent'),
						$this['id'],
						$new['id'],
						$this->module['ptid_content'],
						$new->page->site_structure['site_identifier']
					);
				}
			}

			// Copy Module Config
			if($this->module['ptid_config'])
			{
				$table = PhoundryTools::getTableName($this->module['ptid_config']);
				if($table)
				{
					$res = DB::iQuery(sprintf('SELECT * FROM %s WHERE page_content_id = %d', $table, $this['id']));

					if(!self::isError($res) && count($res))
					{
						$config = get_object_vars($res->first());
						$config['page_content_id'] = $new['id'];
						DB::insertRows(array($config), $table);
					}
				}
			}
		}

		return $new;
	}
	
	/**
	 * Checks if the current Module has config
	 *
	 * @return bool
	 */
	public function checkConfig()
	{
		if(empty($this->module['ptid_config'])) return TRUE;
		$table_name = PhoundryTools::getTableName($this->module['ptid_config']);
		if($table_name == false) return TRUE;
		$sql = sprintf('SELECT COUNT(*) as config_records FROM %s WHERE page_content_id = %d', DB::quote($table_name), $this['id']);
		$res = DB::iQuery($sql);
		
		if(self::isError($res)){
			throw new Exception($res->errstr);
		}

		if($res->first()->config_records > 0){
			return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Checks if the given Module has content
	 *
	 * @return bool
	 */
	public function checkContent(){
		if(empty($this->module['ptid_content'])) return TRUE;
		$table_name = PhoundryTools::getTableName($this->module['ptid_content']);
		if($table_name == false) return TRUE;
		$sql = sprintf('SELECT COUNT(*) as content_records FROM %s WHERE page_content_id = %d', DB::quote($table_name), $this['id']);
		$res = DB::iQuery($sql);
		
		if(self::isError($res)){
			if($res->errno == 1054)
				return TRUE;

			throw new Exception($res->errstr);
		}

		if($res->first()->content_records > 0){
			return TRUE;
		}
		
		return FALSE;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
	
}