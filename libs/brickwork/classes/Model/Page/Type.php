<?php
/**
 * Page Type class
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 * 
 * @property ActiveRecordIterator $pages
 * @property ActiveRecordIterator $page_content_containers
 * @property Model_Template $template
 */
class Model_Page_Type extends ORM
{
	protected $_table_name = 'brickwork_page_type';
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
		'pages' => array('Model_Page', 'page_type_code'),
		'page_content_containers' => array('Model_Page_Content_Container', 'page_type_code'),
	);
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
		'template' => array('Model_Template', 'template_identifier'),
	);
	
	/**
	 * Get the contentSpecificiation for the pagetype in HTML
	 * 
	 * @return string
	 */
	public function getContentSpecification(Model_Site $site, $colorize = true)
	{
		if(defined("TESTMODE") && TESTMODE) {
			$protocol = "http".(!empty($_SERVER['HTTPS']) ? "s":"");
			$url = $protocol."://".$_SERVER['HTTP_HOST'];
		}
		else {
			if(false === $site) {
				throw new Exception("Template is not used by any site");
			}
			$url = in_array("https", explode(",", $site['protocol'])) ? "https" : "http";
			$url .= "://";
			$url .= $site['domain'];
		}
		
		$ccs = array();
		foreach($this->page_content_containers as $cc) {
			$ccs[] = $cc['code'];
		}
		$ccs_selector = "#".implode(", #", $ccs);
		
		$html = file_get_contents($url.
			"/index.php/emptyPage/".$this['code']);
		
		$contentspec = trim($this['contentspecification']);

		if ($colorize) {
			$styles =<<<EOT
{$ccs_selector} {
	background-color: green;
	min-height: 300px;
}
EOT;
		
			$html = str_replace("</head>", "<style type=\"text/css\">\n$styles\n\n</style>\n"."</head>", $html);
		}
		
		if(0 !== stripos($contentspec, "<")) {
			$html = str_replace("</head>", "<style type=\"text/css\">\n$contentspec\n</style>\n"."</head>", $html);
		}
		
		$html = preg_replace('/(<body.*>)/', '\\1<div class="page_type_thumb">', $html);
		$html = str_replace('</body>', '</div></body>', $html);
		
		$html = Utility::makeUrlsAbsolute($html, $url, '/');
		
		return $html;
	}
}