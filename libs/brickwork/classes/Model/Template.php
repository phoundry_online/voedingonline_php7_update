<?php
/**
 * 
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 * 
 * @property ActiveRecordIterator $sites
 */
class Model_Template extends ORM
{
	protected $_table_name = 'brickwork_template';
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
		'sites' => array('Model_Site', 'template'),
	);
}