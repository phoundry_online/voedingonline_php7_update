<?php
/**
* E-mail <<basetype>>
*
* Wrapper voor e-mailadressen. Deze wrapper heeft een check op de geldigheid van
* een e-mailadres en heeft een aantal basisfuncties waardoor het werken met
* e-mailadressen nog iets makkelijk wordt.
*
* @author Mick van der Most van Spijk
* @last_modified 7/10/2006
* @package None
*/
final class Email {
	/**
	* E-mailadres
	* @var String
	*/
	public $address;

	/**
	* Regular expression describing an emailaddress
	* @var String
	*/
	private static $pregex = '/^(([a-z0-9\-_\.\+!#\$%&\'\*\-\/=\?\^_`\{\|\}~]+)|("[a-z0-9\-_ \.\+!#\$%&@\'\*\-\/=\?\^_`\{\|\}~]+)")@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-z0-9\-]+\.)+([a-z]{2,6})))$/i';

	/**
	* Nieuw e-mailadres
	*
	* @param String
	*/
	public function __construct($a) {
		// preg
		$cnt = preg_match(self::$pregex, $a, $matches);

		if (FALSE === $cnt) {
			trigger_error('Controle op e-mailadres is fout gegaan', E_USER_ERROR);
			exit(1);
		}

		if (0 === $cnt) {
			// match kan niet worden gevonden, e-mailadres is fout, breng gebruiker op de hoogte
			//trigger_error('E-mailadres is niet correct', E_USER_NOTICE);
			return;
		}

		if (1 === $cnt) {
			$this->address = $a;
		}
	}

	/**
	* Presenteer e-mailadres
	*
	* @return String
	*/
	public function __toString() {
		return (isset($this->address) ? $this->address : "");
	}

	/**
	* ROT13
	*
	* Beveilig e-mailadres super simpel tegen harvesting
	* @return String
	*/
	public function rot13 () {
		return str_rot13($this->address);
	}


	static function isValid($str){
		$cnt = preg_match(self::$pregex, $str);
		if (FALSE === $cnt) {
			trigger_error('Controle op e-mailadres is fout gegaan', E_USER_ERROR);
			exit(1);
		}

		if ($cnt === 0) {
			return FALSE;
		}
		return TRUE;
	}
}
?>