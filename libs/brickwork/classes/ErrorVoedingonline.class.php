<?php
/**
 * Created by PhpStorm.
 * User: suroc1
 * Date: 7/17/19
 * Time: 5:07 PM
 */

/**
 * ErrorVoedingonline
 *
 * @package	ErrorVoedingonline
 * @version	1.1.0
 * @author	Mick van der Most van Spijk
 *
 * @property int $errno ErrorVoedingonline code
 * @property string $errstr ErrorVoedingonline description
 * @property string $errfile File in which the error occured
 * @property int $errline Line in the file on which the error occured
 * @property array $backtrace Full backtrace to where the error occured
 */
class ErrorVoedingonline {
    protected $errno_;
    protected $errstr_;
    protected $errfile_;
    protected $errline_;

    protected $backtrace_;
    protected $source_;

    public static $cnt = 0;

    public function __construct($errno, $errstr, $errfile='', $errline='', $backtrace=array()) {
        $this->errno_   = $errno;
        $this->errstr_  = $errstr;
        $this->errfile_ = $errfile;
        $this->errline_ = $errline;
        $this->backtrace_ = $backtrace;

        self::$cnt++;
    }

    /**
     * Test if an object is of type error
     *
     * @param ErrorVoedingonline
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }

    public function getType()
    {
        switch ($this->errno) {
            case 1: // E_ERROR
                $message = 'Run-time Error';
                break;

            case 2: // E_WARNING
                $message = 'Warning';
                break;

            case 4: // E_PARSE
                $message = 'Parse Error';
                break;

            case 8: // E_NOTICE
                $message = 'Notice';
                break;

            case 16: // E_CORE_ERROR
                $message = 'Core Error';
                break;

            case 32: // E_CORE_WARNING
                $message = 'Core Warning';
                break;

            case 64: // E_COMPILE_ERROR
                $message = 'Zend Compile Error';
                break;

            case 128: // E_COMPILE_WARNING
                $message = 'Compile-time Warnings';
                break;

            case 256: // E_USER_ERROR
                $message = 'User Error';
                break;

            case 512: // E_USER_WARNING
                $message = 'User Warning';
                break;

            case 1024; // E_USER_NOTICE
                $message = 'User Notice';
                break;

            case 2048: // E_STRICT
                $message = 'Strict Notice';
                break;

            case 4096: // E_RECOVERABLE_ERROR
                $message = 'Catchable Fatal Error';
                break;

            case 8192: // E_DEPRECATED
                $message = 'Deprecated Warning';
                break;

            case 16384: // E_USER_DEPRECATED
                $message = 'User Deprecated Warning';
                break;

            default:
                $message = sprintf('Unknown number: %s', $this->errno);
                break;
        }

        return $message;
    }

    public function __get($name) {
        switch ($name) {
            case 'errno':
                return $this->errno_;
                break;

            case 'errstr':
                return $this->errstr_;
                break;

            case 'errfile':
                return $this->errfile_;
                break;

            case 'errline':
                return $this->errline_;
                break;

            case 'backtrace':
                return (array) $this->backtrace_;
                break;

            case 'source':
                return $this->source_;
                break;

                switch ($name) {
                    default:
                        throw new AttributeNotFoundException (sprintf('Attribute %s not found', $name));
                        break;
                }
        }
    }

    public function __set($name, $value) {
        throw new AttributeSettingNotAllowedException (sprintf('Setting %s is not allowed', $name));
    }
}