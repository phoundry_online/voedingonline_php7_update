<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * Content Handler for Webservice related requests.
 * This handler handles all kinds of Webservice requests like SOAP/WSDL,
 * REST, JSON, etc. It consists of a few parts that need to be implemented
 * when introducing new types.
 * 
 * When writing a webservice, simply create a module that extends the
 * Service_Module class and you're set. make sure you use proper doc commenting
 * when using SOAP
 *
 * @author Peter Eussen
 * @version 1.0
 * @package Brickwork
 * @subpackage Service
 */
class ServiceContentHandler extends ContentHandler
{
	/**
	 * Time the request was started in microseconds
	 *
	 * @var float
	 */
	protected $_startTime;
	
	/**
	 * The query string (parsed)
	 *
	 * @var array
	 */
	protected $_query;
	
	/**
	 * Display handler for this RIA interface
	 *
	 * @var RIA_Display
	 */
	public $displayHandler;
	
	/**
	 * Module that is loaded
	 *
	 * @var RIA_Module
	 */
	public $module;	
	
	/**
	 * Initialises the (Web)Service content handler
	 *
	 * @param array $path
	 * @param string $query
	 */
	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
		
		$this->_query = ($query ? parse_str($query) : array());
		$this->displayHandler = null;
		$this->module = null;
		$this->_startTime = microtime(true);		
	}
	
	/**
	 * Initialises the contenthandler
	 * This class loads the display and handling object and sets their
	 * internal systems to the proper values.
	 *
	 * @return boolean
	 */
	public function init()
	{
		$display = Utility::arrayValue($this->path, 0);
		$module = Utility::arrayValue($this->path, 1);
		
		if ($display === 'version') {
			header("text/plain");
			echo "1.0";
			exit();
		}
		if (!is_null($display)) {
			$this->displayHandler = Service_Representation::factory($display);
		}
		
		if (!is_null($module)) {
			$this->module = Service_Module::factory($module);
		}
			
		return !is_null($this->module) && !is_null($this->displayHandler);
	}
	
	/**
	 * Returns the result of the called method
	 *
	 * @param bool $return
	 * @return string
	 */
	public function out($return = false)
	{
		if (is_null($this->displayHandler)) {
			if (defined('TESTMODE') && TESTMODE) {
				trigger_error('No display defined', E_USER_ERROR);
			} else {
				$this->print404();
				exit;
			}
		}
		
		if (is_null($this->module)) {
			$reply = new Service_Reply(false, null,
				sprintf('Fatal error: Unable to load interface %s',
					Utility::arrayValue($this->path, 1))
			);
		}
		else {	
			try {
				$this->module->path = array_slice( $this->path, 2);
				$this->module->type = strtolower(Utility::arrayValue($this->path, 0));
				
				$reply = $this->module->loadData();
				if ($reply === false) {
					return '';
				}
					
				if (!is_object($reply)) {
					$reply = new Service_Reply(
						($reply !== false && !is_null($reply)), $reply);
				}
				else if (!($reply instanceof Service_Reply)) {
					$reply = new Service_Reply(
						($reply !== false && !is_null($reply)), $reply);
				}
					
				$mod = $this->module->getModule();
				
				if (!is_null($mod)) {
					$this->displayHandler->setModule($mod);
				}
			}
			catch (Exception $e) {
				$reply = new Service_Reply(false, null, $e->getMessage());
			}
		}
		
		$this->displayHandler->setReply($reply);
		
		if (TESTMODE) {
			$reply->processTime = microtime(true) - $this->_startTime;
		}
			
		return $this->displayHandler->out($return);
	}
	
}
