<?php
/**
* ZipFile
*
* @author Remco Bos <remco.bos@webpower.nl>
* @author J�rgen Teunis <jorgen@webpower.nl>
* @last_modified 7/27/2006
* TODO generieke filehandler maken, zipfile moet die overerven en de juiste interface implementeren
*/

class ZipFile {
	public $file;
	public $path = NULL;

	/*
	* Build the zip object
	*
	* @param file	string	contains the zip file name
	* @param path	string	contains the path to the zip
	*/
	public function __construct($zip_file, $path) {
		$this->file	= $zip_file;
		$this->path	= $path;
	}

	/*
	* Get a list of the filenames in the zip file
	*/
	public function listFilenames() {
		$list = shell_exec('unzip -Z1 '.str_replace(' ','\ ', escapeshellcmd($this->path.$this->file)));
		return preg_split('/\s/', trim($list));
	}

	/*
	* Check for a specific file in the zip
	* @param filename	string name of the file to search for
	*/
	public function inArchive($filename) {
		// haal files op
		return (in_array($filename, $this->listFilenames()));	
	}

	/*
	* Extract the archive to specific folder
	* @param	dir string contains the directory where to extract the files om the archive
	*/
	public function extractArchive($dir='') {
		
		// If dir is not set, use home dir
		$dir = !empty($dir) ? $dir : '.';
		
		// Check if directory exists
		if($dir != '.') {
			if(!is_dir($dir)) {
				trigger_error('Directory does not exist', E_USER_ERROR);
			}
		}

		// Extract and copy files
		$out = shell_exec(sprintf('unzip %s/%s -d %s/', $this->path, str_replace(' ', '\ ', escapeshellcmd($this->file)), $dir));
		if ($out == NULL) {
			trigger_error('Zipfile cannot be extracted', E_USER_WARNING);
			return;
		}
	}
}
