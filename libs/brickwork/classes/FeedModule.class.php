<?php
/**
 * FeedModule
 *
 *
 * @abstract
 * @version 0.0.2
 * @author J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 */
abstract class FeedModule extends Module
{

	/**
	 * code
	 * @var string
	 */
	protected $_code;
	
	/**
	 * use cache
	 * @var Boolean
	 */
	protected $_use_cache_	= FALSE;

	/**
	 * seconds to cache the template
	 *
	 * @var Integer seconds
	 */
	protected $cache_lifetime	= 0;
	
	/**
	 * Non standart formats that this module supports
	 *
	 * @var array
	 */
	protected $_additionalFormats = array();
	
	/**
	 * Array holding the FeedItems
	 *
	 * @var array
	 */
	protected $_items = array();

	/**
	 * cacheparameters (2 dimensional array)
	 *
	 * @var Array
	 */
	public $cacheParams = array();
	
	/**
	 * Path parameters
	 *
	 * @var array
	 */
	public $params = array();

	/**
	 * module name
	 * 
	 * @var String
	 */
	public $name = '';

	/**
	 * The name of the content table
	 *
	 * @var string
	 */
	public $content_table;
	
	/**
	 * The name of the config table
	 *
	 * @var string
	 */
	public $config_table;

	/**
	 * Constructor
	 *
	 * @param string $code
	 */
	function __construct($code)
	{
		$this->_code = $code;
	}
	
	/**
	 * Magic Getter
	 *
	 * @param string $name
	 * @return mixedvar
	 */
	function __get($name)
	{
		switch($name)
		{
			case 'items':	return (array) $this->_items;
			case 'code':	return $this->_code;
		}
	}

	/**
	* Get cache parameters
	*
	* @access public
	* @return Array with cache parameters for the module
	*/
	public function getCacheParams()
	{
		return $this->cacheParams;
	}

	/**
	* Get cache lifetime in seconds
	*
	* PageContenthandler needs this lifetime
	*
	* @access public
	* @return Integer
	*/
	public function getCacheLifetime()
	{
		return $this->cache_lifetime;
	}

	/**
	* Mag deze module worden gecached
	*
	* @return Boolean
	* @access public
	*/
	//public function is_cachable(){ -> juiste scoopnaamgeving
	public function isCachable()
	{
		return ($this->_use_cache_ === TRUE && $this->cache_lifetime > 0);
	}


	/**
	* Set cache lifetime
	*
	* @param Integer
	* @access public
	*/
	public function setCacheLifetime($lt)
	{
		$this->cache_lifetime = (int)$lt;
		$this->_use_cache_ = ($lt > 0);
			
		return $this->_use_cache_;
	}

	/**
	 * Returns the additional formats this module supports
	 *
	 * @return array
	 */
	public function getAdditionalFormats()
	{
		return (array) $this->_additionalFormats;		
	}
	
	/**
	 * Factory
	 *
	 * @static
	 * @param string $class
	 * @param string $code
	 * @return FeedModule
	 */
	public static function factory($class, $code)
	{
		$module = NULL;

		if(class_exists($class)){
			$module = new $class($code);
		}

		if(!($module instanceof self)){
			trigger_error(sprintf('Module \'%s\' does not exists', $class), E_USER_NOTICE);
			return NULL;
		}

		return $module;
	}

	/**
	 * String representation of this module
	 *
	 * @return string classname
	 */
	public function __toString()
	{
		return get_class($this);
	}

	/**
	 * loadExtraCacheParams
	 *
	 * This method is loaded after the construct / loadconfg methods so you can set your extra cache
	 * parameters for the module.
	 *
	 */
	public function loadExtraCacheParams(){
		
	}
	
	public function loadConfig(){}
	
	public function fetch(){}

	/**
	* Get module by classname
	*
	* @static
	* @param string code
	* @return FeedModule
	*/
	public static function getModuleByCode($code)
	{
		$sql = sprintf("
			SELECT
				m.code,
				m.class,
				m.name,
				m.cachetime,
				pt1.name as content_table,
				pt2.name as config_table
			FROM
				brickwork_module m
			LEFT JOIN
				phoundry_table pt1 ON pt1.id = m.ptid_content
			LEFT JOIN
				phoundry_table pt2 ON pt2.id = m.ptid_config
			WHERE
				m.code = '%s'
			LIMIT 1
		",
			DB::quote($code)
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);
		}

		if($res->num_rows == 0) {
			// trigger_error(sprintf('No module found by this classname: %s', $code), E_USER_NOTICE);
			return NULL;
		}

		$mod = $res->first();

		$module = self::factory($mod->class, $mod->code);
		$module->setCacheLifetime($mod->cachetime);
		$module->content_table	= $mod->content_table;
		$module->config_table	= $mod->config_table;
		$module->name				= $mod->name;

		$module->loadConfig();

		return $module;
	}



}
