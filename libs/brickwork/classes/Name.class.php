<?php
/**
* Name
*
* Class to manage names of persons.
*
* @author Mick van der Most van Spijk (MMS)
* @author J�rgen Teunis
* @author Remco Bos
* @last_modified 8/15/2007 MMS
* @version 1.2.2
* @changes:
* - added baptismal name
* - added calling name
* - guess initials does nothing when there are no first names
* - guess initials check empty baptismal name
* - check if initials are set, if not; guess initials in __get
*/
class Name {
	/**
	* First name
	* @var String
	* @access protected
	*/
	protected $first_name;

	/**
	* Last name
	* @var String
	* @access protected
	*/
	protected $last_name;

	/**
	* Middle name
	* @var String
	* @access protected
	*/
	protected $middle_name;

	/**
	* Maiden name
	* @var String
	* @access protected
	*/
	protected $maiden_name;

	/**
	* Baptismal name
	* @var String
	* @access protected
	* @since 1.2.1
	*/
	protected $baptismal_name;

	/**
	* Calling name
	* @var String
	* @access protected
	* @since 1.2.1
	*/
	protected $calling_name;

	/**
	* Initials
	* @var String
	* @access protected
	*/
	protected $initials_;

	/**
	* Style for parsing initials
	* @var String
	* @static
	* @access public
	*/
	public static $initials_style = Name::initials_style_normal;

	/**
	* Initial parsing style normal
	* @var const
	*/
	const initials_style_normal = 'name_initials_style_normal';

	/**
	* Initial parsing style greek (th, ch and ph when appropriate)
	* @var const
	*/
	const initials_style_greek  = 'name_initials_style_greek';

	/**
	* Constructor
	*
	* @access public
	* @param String $first_name
	* @param String $last_name
	* @param String $middle_name
	*/
	public function __construct($first_name, $last_name, $middle_name='') {
		$this->first_name  = $first_name;
		$this->last_name   = $last_name;
		$this->middle_name = $middle_name;

		// guess initials
		$this->_guess_initials();

		// guess maiden name
		$this->_guess_maiden_name();
	}

	/**
	* Setter, return FALSE on failure (and raises a notice error)
	*
	* @access public
	* @param String $name Name of the attribute to set
	* @param Mixed $value Value of the attribute to set
	*/
	public function __set($name, $value) {
		$value = trim($value);

		switch ($name) {
			case 'firstName':
				$this->first_name = $value;
				break;

			case 'lastName':
				$this->last_name = $value;
				break;

			case 'middleName':
				$this->middle_name = $value;
				break;

			case 'initials':
				$this->initials_ = $value;
				break;

			case 'maidenName':
				$this->maiden_name = $value;
				break;

			case 'baptismalName':
				$this->baptismal_name = $value;
				break;

			case 'callingName':
				$this->calling_name = $value;
				break;

			default:
				trigger_error(sprintf('Attribute %s not found', $name), E_USER_NOTICE);
				return FALSE;
		}
	}

	/**
	* Getter
	*
	* @param String $name
	* @return Mixed
	* @access public
	*/
	public function __get($name) {
		switch ($name) {
			case 'firstName':
				return $this->first_name;

			case 'lastName':
				return $this->last_name;

			case 'middleName':
				return $this->middle_name;

			case 'initials':
				// check if initialals are set
				if ('' == $this->initials_) {
					$this->_guess_initials();
				}
				return $this->initials_;

			case 'maidenName':
				return $this->maiden_name;

			case 'baptismalName':
				return $this->baptismal_name;

			case 'callingName':
				return $this->calling_name;

			default:
				trigger_error(sprintf('Attribute %s not found', $name), E_USER_NOTICE);
				return FALSE;
		}
	}


	/**
	* Parse firstnames and guess the initials
	*
	* @access private
	*/
	private function _guess_initials() {
		// container
		$initials = array();

		if ('' != $this->first_name) {
			$names = explode(' ', $this->first_name);

			// brak up the first names by there spaces en retrieve the first letter
			foreach ($names as $name) {
				$initial = strtoupper(substr($name, 0, 1));

				// check for greek parsing style
				if (self::$initials_style == Name::initials_style_greek) {
					if ($initial == 'T' || $initial == 'C' || $initials == 'P') {
						if (substr($name, 1, 1) == 'h') {
							$initial = $initial . 'h';
						}
					}
				}

				$initials[] = $initial;
			}
		}

		// check for baptismal name
		if ('' != $this->baptismal_name) {
			$bap_names = explode(' ', $this->baptismal_name);
			foreach ($bap_names as $name) {
				$initial = strtoupper(substr($name, 0, 1));

				// check for greek parsing style
				if (self::$initials_style == Name::initials_style_greek) {
					if ($initial == 'T' || $initial == 'C' || $initials == 'P') {
						if (substr($name, 1, 1) == 'h') {
							$initial = $initial . 'h';
						}
					}
				}

				$initials[] = $initial;
			}
		}

		// glue first letters with a dot and set initials and add a dot at the end
		if (0 < count($initials)) { // dont make initials when there is nothink
			$this->initials_ = sprintf('%s.', implode(".", $initials));
		}
	}

	/**
	* Parse last name to find out if there's a maiden name
	*
	* @access private
	*/
	private function _guess_maiden_name() {
		$parts = explode('-', $this->last_name);

		// probably a maiden name
		if (count($parts) == 2) {
			$this->last_name = trim($parts[0]);
			$this->maiden_name = trim($parts[1]);
		}
	}

	/**
	* Format a name string
	*
	* Format options are:
	* - %i initials
	* - %m middlename
	* - %l lastname
	* - %f firstname(s)
	* - %g maidenname
	* - %b baptismalname(s)
	* - %c callingname
	*
	* @param String $format
	* @return String
	*/
	public function strfname($format="%l%g, %f %m") {
		// replace

		$str = str_replace('%l', $this->last_name, $format);
		$str = str_replace('%f', $this->first_name, $str);
		$str = str_replace('%m', $this->middle_name, $str);
		$str = str_replace('%i', $this->initials, $str);
		$str = str_replace('%b', $this->baptismal_name, $str);
		$str = str_replace('%c', $this->calling_name, $str);

		if (!empty($this->maiden_name)) {
			if(empty($this->last_name)) {
				$str = str_replace('%g', $this->maiden_name, $str);
			} else {
				$str = str_replace('%g', ' - '.$this->maiden_name, $str);
			}
		} else {
			$str = str_replace('%g', '', $str);
		}

		// strip whitespace
		$str = preg_replace("/ +/", " ", $str);

		// return name
		return $str;
	}

	/**
	* Return a string representation of this object
	*
	* @return String
	* @access public
	*/
	public function __toString() {
		return $this->strfname();
	}
}
?>