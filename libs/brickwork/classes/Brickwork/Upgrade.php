<?php
interface Brickwork_Upgrade
{
	/**
	 * Prepare the upgrade and determine what variables are needed for upgrade()
	 * 
	 * @return array Parameters that are expected for upgrade()
	 */
	public function prepare();
	
	/**
	 * Run the actual upgrade
	 * 
	 * @return array the rollback data
	 */
	public function run(array $params = null);
	
	/**
	 * Roll back a crashed upgrade() attempt
	 * 
	 * @return bool
	 */
	public function rollback(array $params = null);
	
	/**
	 * Returns the rollback data which can be used to roll back an upgrade
	 * @return array the rollback data
	 */
	public function getRollbackData();
	
	/**
	 * Sets the rollback data to a rollback state allowing ->rollback() to
	 * be succesfully called
	 * @param array $data
	 */
	public function setRollbackData(array $data);
	
	/**
	 * Attaches a outputter that processes output generated by the upgrade
	 * 
	 * @param Brickwork_Upgrade_Outputter $outputter
	 * @return void
	 */
	public function attachOutputter(Brickwork_Upgrade_Outputter $outputter);
	
	/**
	 * Detaches a outputter that processes output generated by the upgrade
	 * @param Brickwork_Upgrade_Outputter $outputter
	 * @return bool True if the outputter was found and detached
	 */
	public function detachOutputter(Brickwork_Upgrade_Outputter $outputter);
}
