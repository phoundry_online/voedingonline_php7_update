<?php
class Brickwork_Upgrade_1_7 extends Brickwork_Upgrade_Abstract
{
	/**
	 * Database structure as dumped by DB_Sync#dump() for the current db
	 * @var array
	 */
	protected $_current_db;
	
	/**
	 * Phoundry tables as dumped by PhoundrySync#exportAll() for the current db
	 * @var array
	 */
	protected $_current_phoundry;
	
	public function prepare()
	{
		$params = array('migrate_news');
		return $params;
	}
	
	public function run(array $params = null)
	{
		$this->_syncDbStructure();
		$this->_dropDuplicateKeys();
		$this->_upgradePhoundry();
		if ($params['migrate_news']) {
			$this->_migrateNewsArticles();
		}
		$this->_syncPhoundryInterfaces();
		$this->_setHtmlConfig();
		
		$this->_out("Upgrade succesfully applied");
		return true;
	}
	
	public function rollback(array $params = null)
	{
		$this->_out("Rolling back");
		$this->_rollbackSyncPhoundryInterfaces();
		if ($params['migrate_news']) {
			$this->_rollbackMigrateNewsArticles();
		}
		$this->_rollbackSyncDbStructure();
		return true;
	}
	
	protected function _upgradePhoundry()
	{
		global $PHprefs;
		if (!isset($PHprefs['distDir'])) {
			throw new Brickwork_Upgrade_Exception(
				"Phoundry distDir entry in ".
				"PHprefs missing"
			);
		} else if (!file_exists($PHprefs['distDir'])) {
			throw new Brickwork_Upgrade_Exception("Phoundry distDir not found");
		}
		
		if (file_exists(PROJECT_DIR."/config/PREFS.php")) {
			$prefs_path = PROJECT_DIR."/config/PREFS.php";
		} else if (file_exists(PROJECT_DIR."/admin/PREFS.php")) {
			$prefs_path = PROJECT_DIR."/admin/PREFS.php";
		} else {
			throw new Brickwork_Upgrade_Exception("PREFS.php not found!");
		}
		
		if (!$this->_db->columnExists("phoundry_table", "string_id")) {
			$this->_out("phoundry_table is missing string_id upgrade phoundry");
			
			$path = rtrim($PHprefs['distDir'], "/").
				"/skeleton/add_string_ids.php";
			
			if (!file_exists($path)) {
				throw new Brickwork_Upgrade_Exception(
					"add_string_ids.php not ".
					"found in distDir ".$PHprefs['distDir'].
					" Brickwork 1.7 needs at least Phoundry 5.8"
				);
			}
			
			$output = shell_exec("php {$path} {$prefs_path}");
			
			if ($output) {
				throw new Brickwork_Upgrade_Exception($output);
			}
			$this->_out("Phoundry succesfully upgraded");
		}
	}
	
	/**
	 * Sets the new Html module config table
	 */
	protected function _setHtmlConfig()
	{
		try {
			$row = $this->_db->iQuery(
				"SELECT ptid_config
				FROM brickwork_module
				WHERE code = 'htmlcontent'"
			)->first();
			
			if ($row && !$row->ptid_config) {
				$this->_out("Upgrading Html module to work with new config");
				
				if (isset($this->_new_ignored_interfaces['module_config_html'])) {
					$this->_phoundry_sync->import(
						$this->_new_ignored_interfaces['module_config_html']
					);
				}
				
				$row = $this->_db->iQuery(
					"SELECT id
					FROM phoundry_table
					WHERE string_id = 'module_config_html'"
				)->first();
				
				if ($row) {
					$this->_db->iQuery(
						sprintf(
							"UPDATE brickwork_module
							SET ptid_config = %d
							WHERE code = 'htmlcontent'",
							$row->id
						)
					);
				}
			}
		}
		catch (DB_Exception $e) {
			$this->_out(
				"Database Error occured when upgrading HTML module".
				$e->getMessage()
			);
			throw new Brickwork_Upgrade_Exception(
				$e->getMessage(), $e->getCode()
			);
		}
	}
	
	/**
	 * Drops the duplicate keys that may exist in the database
	 */
	protected function _dropDuplicateKeys()
	{
		$this->_db->query(
			"ALTER TABLE `brickwork_custom_site_structure`
			DROP KEY `id`"
		);
		$this->_db->query(
			"ALTER TABLE `brickwork_site_user_auth_role`
			DROP KEY `auth_username`"
		);
	}
	
	/**
	 * Migrates excisting news articles to the new structure
	 */
	protected function _migrateNewsArticles()
	{
		try {
			if ($this->_db->tableExists('module_news_article') &&
					!$this->_db->tableExists('module_news_article_category')) {
					
				$this->_out(
					'Upgrading news articles to allow multiple categories'
				);
				
				if (!isset($this->_new_ignored_tables['module_news_article_category'])) {
					throw new Brickwork_Upgrade_Exception(
						"module_news_article_category table missing from ".
						"create statements"
					);
				}
				
				// Create module_news_article_category
				$this->_db->iQuery(
					$this->_new_ignored_tables['module_news_article_category']
				);
				
				// Copy over the article category bindings
				$this->_db->iQuery(
					"INSERT INTO module_news_article_category 
					(article_id, category_id)
					(SELECT id, category_id FROM module_news_article)"
				);
				
				$this->_rollback_data['migrateNewsArticles'] = $this->_db->
					iQuery("SELECT id, category_id FROM module_news_article")->
					getSelect('id', 'category_id');
				
				$this->_db->iQuery(
					"UPDATE module_news_article SET category_id = NULL"
				);
				
				// Rename phoundry_table string_id to new id so it gets updated
				$this->_db->iQuery(
					"UPDATE phoundry_table
					SET string_id = 'brickwork_module_news_article'
					WHERE string_id = 'module_news_article'"
				);
			}
		}
		catch(DB_Exception $e) {
			$this->_out(
				"Database Error occured while migrating news articles".
				$e->getMessage()
			);
			throw new Brickwork_Upgrade_Exception(
				$e->getMessage(), $e->getCode()
			);
		}
	}
	
	/**
	 * Undoes the migration of the news articles
	 */
	protected function _rollbackMigrateNewsArticles()
	{
		if (!empty($this->_rollback_data['migrateNewsArticles'])) {
			$this->_out("Rolling back migrated news articles");
			foreach ($this->_rollback_data['migrateNewsArticles'] as $a => $c) {
				$this->_db->iQuery(
					sprintf(
						"UPDATE module_news_article 
						SET category_id = %d
						WHERE id = %d",
						$c, $a
					)
				);
			}
			
			// Rename phoundry_table string_id back
			$this->_db->iQuery(
				"UPDATE phoundry_table
				SET string_id = 'module_news_article'
				WHERE string_id = 'brickwork_module_news_article'"
			);
		}
	}
}