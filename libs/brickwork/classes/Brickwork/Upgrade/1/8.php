<?php
class Brickwork_Upgrade_1_8 extends Brickwork_Upgrade_Abstract
{
	public function prepare()
	{
		return array();
	}
	
	public function run(array $params = null)
	{
		$this->_syncDbStructure();
		$this->_syncPhoundryInterfaces();
		$this->_renameModules();
		$this->_out("Upgrade succesfully applied");
		return true;
	}
	
	public function rollback(array $params = null)
	{
		$this->_out("Rolling back");
		$this->_rollbackRenameModules();
		$this->_rollbackSyncPhoundryInterfaces();
		$this->_rollbackSyncDbStructure();
		$this->_out("Succesfully rolled back");
		return true;
	}
	
	protected function _renameModules()
	{
		$this->_out("Setting module names to translation numbers");
		
		$modules = array(
			'agenda' => 11201,
			'avoidform' => 11200,
			'banner' => 11202,
			'brickworknews' => 11203,
			'deeplink' => 11204,
			'faq' => 11205,
			'google_adsense' => 11206,
			'google_maps' => 11207,
			'htdig_search' => 11208,
			'htmlblock' => 11209,
			'htmlcontent' => 11210,
			'iframe' => 11211,
			'link' => 11212,
			'menu' => 11213,
			'photogallery' => 11214,
			'poll' => 11215,
			'reactions' => 11216,
			'sitemap' => 11217,
			'snippet' => 11218
		);
		
		$this->_rollback_data['renameModules'] = $this->_db->iQuery("
			SELECT code, name FROM brickwork_module")->
					getSelect('code', 'name');
		
		foreach ($modules as $code => $nr) {
			$this->_out("Setting translation nr to {$nr} for {$code}");
			$this->_db->iQuery(sprintf("
				UPDATE
					brickwork_module
				SET name = %d
				WHERE code = %s
				LIMIT 1",
				$nr,
				$this->_db->quote($code, true)
			));
		}
	}
	
	protected function _rollbackRenameModules()
	{
		if(isset($this->_rollback_data['renameModules'])) {
			$this->_out("Rolling back Rename Modules");
			
			foreach ($this->_rollback_data['renameModules'] as $code => $name) {
				$this->_db->iQuery(sprintf("
					UPDATE
						brickwork_module
					SET name = %s
					WHERE code = %s",
					$this->_db->quote($name, true),
					$this->_db->quote($code, true)
				));
			}
		}
	}
}
