<?php
/**
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
class Brickwork_Upgrade_Script_Outputter implements Brickwork_Upgrade_Outputter
{
	/**
	 * @var CommandLine
	 */
	protected $_c;
	
	public function __construct(CommandLine $commandline)
	{
		$this->_c = $commandline;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Brickwork/Upgrade/Brickwork_Upgrade_Outputter#output($message)
	 */
	public function output(Brickwork_Upgrade $upgrade, $message)
	{
		$this->_c->println(get_class($upgrade).": ".$message);
	}
}