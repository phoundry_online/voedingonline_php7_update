<?php
abstract class Brickwork_Upgrade_Abstract implements Brickwork_Upgrade
{
	/**
	 * @var array 
	 */
	protected $_outputters;
	
	/**
	 * @var DB_Adapter first database connection as configured in PHprefs
	 */
	protected $_db;
	
	/**
	 * @var DB_Sync
	 */
	protected $_db_sync;
	
	/**
	 * @var PhoundrySync
	 */
	protected $_phoundry_sync;
	
	/**
	 * @var array Data used for rollbacks
	 */
	protected $_rollback_data;
	
	/**
	 * @var array Tables that were not created by syncDbStructure but are new
	 */
	protected $_new_ignored_tables;
	
	/**
	 * @var array Table interfaces that were new but not imported
	 */
	protected $_new_ignored_interfaces;
	
	/**
	 * Initialize the upgrade setting defaults
	 * 
	 * @param DB_Adapter $db
	 * @param DB_Sync $db_sync
	 * @param PhoundrySync $phoundry_sync
	 */
	public function __construct(DB_Adapter $db, DB_Sync $db_sync,
		PhoundrySync $phoundry_sync)
	{
		$this->_outputters = array();
		$this->_db = $db;
		$this->_db_sync = $db_sync;
		$this->_phoundry_sync = $phoundry_sync;
		$this->_rollback_data = array();
	}
	
	/**
	 * Attaches a outputter that processes output generated by the upgrade
	 * 
	 * @param Brickwork_Upgrade_Outputter $outputter
	 * @return void
	 */
	public function attachOutputter(Brickwork_Upgrade_Outputter $outputter)
	{
		$this->_outputters[] = $outputter;
	}
	
	/**
	 * Detaches a outputter that processes output generated by the upgrade
	 * @param Brickwork_Upgrade_Outputter $outputter
	 * @return bool True if the outputter was found and detached
	 */
	public function detachOutputter(Brickwork_Upgrade_Outputter $outputter)
	{
		$i = array_search($outputter, $this->_outputters, true);
		if(false !== $i) {
			array_splice($this->_outputters, $i, 1);
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the rollback data which can be used to roll back an upgrade
	 * @return array the rollback data
	 */
	public function getRollbackData()
	{
		return $this->_rollback_data;
	}
	
	/**
	 * Sets the rollback data to a rollback state allowing ->rollback() to
	 * be succesfully called
	 * @param array $data
	 */
	public function setRollbackData(array $data)
	{
		$this->_rollback_data = $data;
	}
	
	/**
	 * Outputs the given message using the attached outputters
	 * 
	 * @param string $message
	 * @return void
	 */
	protected function _out($message)
	{
		foreach($this->_outputters as $out) {
			$out->output($this, $message);
		}
	}
	
	/**
	 * Syncs the database structure with the database_structure.sql file
	 * 
	 * @throws Brickwork_Upgrade_Exception on errors
	 * @return bool
	 */
	protected function _syncDbStructure()
	{
		if(!defined('BRICKWORK_DIR')) {
			throw new Brickwork_Upgrade_Exception("BRICKWORK_DIR not defined");
		}
		$to = BRICKWORK_DIR."/sql/database_structure.sql";
		if(!file_exists($to)) {
			throw new Brickwork_Upgrade_Exception($to.
				"could not be found in the sql folder in brickwork");
		}
		
		$this->_out("Database structure found, parsing");
		$to = explode(";\n\n", rtrim(file_get_contents($to), ";\n"));
		if(!$to) {
			throw new Brickwork_Upgrade_Exception("database_structure.sql does".
				" not contain any create statements or the file is malformed");
		}
		
		$this->_out("Parsing succesful compare structure to current structure");
		$this->_rollback_data['syncDbStructure'] =
			$from = $this->_db_sync->dump();
		$diff = $this->_db_sync->diffDumps($from, $to);
		
		try {
			// Add any missing tables that start with brickwork_
			if($diff['create']) {
				$this->_out("New tables found");
				foreach($diff['create'] as $table_name => $sql) {
					if(0 === stripos($table_name, 'brickwork_')) {
						$this->_out("Creating table ".$table_name);
						$this->_db->iQuery($sql);
					}
					else {
						$this->_new_ignored_tables[$table_name] = $sql;
					}
				}
			}
			
			// Update any changed tables with new columns
			// this does not include dropping of deleted columns
			if($diff['update']) {
				$this->_out("Updated tables found");
				foreach($diff['update'] as $table_name => $sql) {
					$this->_out("Updating table ".$table_name);
					$this->_db->iQuery($sql);
				}
			}
			
			// Echo any alters that might truncate the content
			if($diff['truncate']) {
				$this->_out(
					"Updated tables that involve truncating found you ".
					"might have to run the following queries by hand"
				);
				foreach ($diff['truncate'] as $table_name => $sql) {
					$this->_out($sql);
				}
			}
		}
		catch(DB_Exception $e) {
			$this->_out("Database Error occured while syncing db structure, ".
				"could you fix this by hand? Message: ".$e->getMessage());
			throw new Brickwork_Upgrade_Exception($e->getMessage(),
				$e->getCode());
		}
		
		return true;
	}
	
	/**
	 * Rolls back changes made by this#_syncDbStructure() if made any
	 * 
	 * @throws Brickwork_Upgrade_Exception on error
	 * @return bool
	 */
	protected function _rollbackSyncDbStructure()
	{
		if(isset($this->_rollback_data['syncDbStructure'])) {
			$this->_out("Rolling back the changes made to the database structure");
			
			$db_state = $this->_db_sync->dump();
			$diff = $this->_db_sync->diffDumps($db_state, 
				$this->_rollback_data['syncDbStructure']);
			try {
				// revert any changed tables with new columns
				foreach($diff['update'] as $sql) {
					$this->_db->iQuery($sql);
				}
				// delete any newly added columns
				foreach($diff['delete'] as $sql) {
					$this->_db->iQuery($sql);
				}
				
				// Remove any new tables that start with brickwork_
				foreach($diff['drop'] as $table_name => $sql) {
					if(0 === stripos($table_name, 'brickwork_')) {
						$this->_db->iQuery($sql);
					}
				}
			}
			catch(DB_Exception $e) {
				throw new Brickwork_Upgrade_Exception($e->getMessage(),
					$e->getCode());
			}
		}
		return true;
	}
	
	/**
	 * Syncs the phoundry table interfaces with table_interfaces.dat
	 * 
	 * @throws Brickwork_Upgrade_Exception on errors
	 * @return bool
	 */
	protected function _syncPhoundryInterfaces()
	{
		if(!defined('BRICKWORK_DIR')) {
			throw new Brickwork_Upgrade_Exception("BRICKWORK_DIR not defined");
		}
		$to = BRICKWORK_DIR."/sql/table_interfaces.dat";
		if(!file_exists($to)) {
			throw new Brickwork_Upgrade_Exception($to.
				"could not be found in the sql folder in brickwork");
		}
		
		$this->_out("Table interfaces found parsing");
		$to = include($to);
		if(!$to || !is_array($to)) {
			throw new Brickwork_Upgrade_Exception("table_interfaces.dat does".
				" not contain any phoundry tables or the file is malformed");
		}
		
		$this->_out("Compare table interfaces to current");
		$this->_rollback_data['syncPhoundryInterfaces'] =
			$from = $this->_phoundry_sync->exportAll();
		$diff = $this->_phoundry_sync->diffAll($from, $to);
		
		try {
			if($diff['import']) {
				$this->_out("New table interfaces found");
				foreach($diff['import'] as $table) {
					$name = $table['phoundry_table']['name'];
					$string_id = $table['phoundry_table']['string_id'];
					// Check if the table to be imported is even in our database
					if('' === $name || $this->_db->tableExists($name)) {
						$this->_out("New table interface for table ".$name);
						$this->_new_ignored_interfaces[$string_id] = $table;
					}
				}
			}
			 
			if($diff['update']) {
				$this->_out("Updated table interfaces found");
				foreach($diff['update'] as $string_id => $table) {
					$this->_out("Updating interface ".$string_id);
					$this->_phoundry_sync->update($string_id, $table, false);
				}
			}
		}
		catch(PDOException $e) {
			$this->_out("Database Error occured while syncing table interfaces ".
				$e->getMessage());
			throw new Brickwork_Upgrade_Exception($e->getMessage(),
				(int) $e->getCode());
		}
		
		return true;
	}
	
	/**
	 * Roll back the changes made by this#_syncPhoundryInterfaces() if made any
	 * 
	 * @throws Brickwork_Upgrade_Exception on errors
	 * @return bool
	 */
	protected function _rollbackSyncPhoundryInterfaces()
	{
		if(isset($this->_rollback_data['syncPhoundryInterfaces'])) {
			$this->_out("Rolling back the changes made to Phoundry interfaces");
			
			$phoundry_state = $this->_phoundry_sync->exportAll();
			$diff = $this->_phoundry_sync->diffAll($phoundry_state,
				$this->_rollback_data['syncPhoundryInterfaces']);
			try {
				foreach($diff['update'] as $string_id => $table) {
					$this->_phoundry_sync->update($string_id, $table, false);
				}
				
				foreach($diff['import'] as $table) {
					$this->_phoundry_sync->import($table);
				} 
			}
			catch(PDOException $e) {
				throw new Brickwork_Upgrade_Exception($e->getMessage(),
					(int) $e->getCode());
			}
		}
		return true;
	}
}
