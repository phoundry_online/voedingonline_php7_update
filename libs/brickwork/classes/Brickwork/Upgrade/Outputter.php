<?php
/**
 * Provides the interface to be used as Outputter by the Upgrades
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
interface Brickwork_Upgrade_Outputter
{
	public function output(Brickwork_Upgrade $upgrade, $message);
}