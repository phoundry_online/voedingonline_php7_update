<?php
/**
 * Commandline upgrade runner
 * 
 * @todo move rollback logic into its own class
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
class Brickwork_Upgrade_Script extends CommandLine_Script
{
	/**
	 * Upgrades to the specified version or if none given to the latest version
	 * 
	 * @param string $version backup directory, default prefs folder
	 * @return void
	 */
	public function upgrade($version = null, $backup = true)
	{
		if (null === $version) {
			$version = Framework::version();
		}
		list($major, $minor, $_) = sscanf($version, "%d.%dr%d");
		
		if(file_exists(PROJECT_DIR.'/config')) {
			$backup_dir = PROJECT_DIR.'/config';
		}
		else if(file_exists(PROJECT_DIR.'/admin')) {
			$backup_dir = PROJECT_DIR.'/admin';
		}
		else {
			throw new InvalidArgumentException("Backup_dir not given and both ".
				"config and admin dir could not be found in PROJECT_DIR"); 
		}
		
		global $PHprefs;
		
		$db = $this->_getDatabase();
		
		$db_sync = new DB_Sync($db->getResource());
		$phoundry_sync = new PhoundrySync($db->getResource(), $PHprefs['charset']);
		
		if ($backup && "false" !== $backup) {
			$this->backupPrefs($backup_dir.'/prefs_backup_'.time().'.tgz');
			$this->backupDatabase($backup_dir.'/database_backup_'.time().'.sql.gz');
		}
		
		$outputter = new Brickwork_Upgrade_Script_Outputter($this->_c);
		
		// Create the upgrade for the minor release version
		$upgrade = $this->_instantiateUpgrade(array($major, $minor), $db,
			$db_sync, $phoundry_sync);
		$maintenance = 0;
		
		while($upgrade instanceof Brickwork_Upgrade) {
			// Listen to output of the upgrade
			$upgrade->attachOutputter($outputter);
			
			// Execute the preparations and get the required params for the upgrade
			$this->_println("Preparing for Upgrade ".$major.'.'.$minor.'.'.$maintenance);
			$params = $upgrade->prepare();
			
			// Get our params from the CLI
			$this->_println("Getting params for Upgrade ".$major.'.'.$minor.'.'.$maintenance);
			$values = $this->_readParams($params);
			
			// Run the upgrade with our fetched params
			$this->_println("Apply Upgrade ".$major.'.'.$minor.'.'.$maintenance);
			$status = $this->_applyUpgrade($upgrade, $values);
			
			if($status === false) {
				break;
			}
			
			// Save the rollback data for the upgrade
			$this->_writeRollbackData(
				"rollback_{$major}.{$minor}.{$maintenance}".time().".php",
				$upgrade, $values);
			
			// Keep applying till the latest maintenance version
			$upgrade = $this->_instantiateUpgrade(
				array($major, $minor, ++$maintenance),
				$db,
				$db_sync,
				$phoundry_sync
			);
		}
	}
	
	/**
	 * Applies the upgrade
	 * 
	 * @param Brickwork_Upgrade $upgrade
	 * @param array $params the params as expected by the upgrade
	 * @return bool True if the upgrade was applied succesfully
	 */
	protected function _applyUpgrade(Brickwork_Upgrade $upgrade, array $params)
	{
		try {
			return $upgrade->run($params);
		}
		catch(Brickwork_Upgrade_Exception $e) {
			$this->_println("An error occured, rolling back. ".$e);
			$upgrade->rollback($params);
			return false;
		}
	}
	
	/**
	 * Instantiate a new Upgrade for the given version
	 * 
	 * @param int $major
	 * @param int $minor
	 * @return Brickwork_Upgrade|bool
	 */
	protected function _instantiateUpgrade($version, $db, $db_sync, $phoundry_sync)
	{
		$class = "Brickwork_Upgrade_".implode("_", $version);
		if(!class_exists($class, true)) {
			return false;
		}
		
		$upgrade = new $class($db, $db_sync, $phoundry_sync);
		if(!$upgrade instanceof Brickwork_Upgrade) {
			throw new UnexpectedValueException($class." should extend Brickwork_Upgrade");
		}
		return $upgrade;
	}
	
	/**
	 * Read in the needed params
	 * 
	 * @param array $params
	 * @return array
	 */
	protected function _readParams(array $params)
	{
		$values = array();
		foreach ($params as $param) {
			if (isset($this->_named_args[$param])) {
				$values[$param] = $this->_named_args[$param];
			} else {
				$values[$param] = null;
			}
		}
		return $values;
	}

	/**
	 * Backs up the Prefs and sandbox Prefs
	 * 
	 * @param string $file Filename of backup
	 */
	public function backupPrefs($file)
	{
		if(file_exists(PROJECT_DIR.'/config')) {
			$config_dir = PROJECT_DIR.'/config';
		}
		else if(file_exists(PROJECT_DIR.'/admin')) {
			$config_dir = PROJECT_DIR.'/admin';
		}
		else {
			throw new Exception("Config directory could not be found");
		}
		
		$this->_println("Backing up Prefs file as {$file}");
		shell_exec(sprintf("cd %s && tar -czf %s PREFS.php PREFS.sandbox.php",
			escapeshellarg($config_dir),
			escapeshellarg($file)
		));
	}
	
	/**
	 * Dumps the first database
	 * 
	 * @throws Exception
	 * @param string $file file to write the dump to
	 */
	public function backupDatabase($file)
	{
		if(file_exists(PROJECT_DIR.'/config')) {
			$config_dir = PROJECT_DIR.'/config';
		}
		else if(file_exists(PROJECT_DIR.'/admin')) {
			$config_dir = PROJECT_DIR.'/admin';
		}
		else {
			throw new Exception("Config directory could not be found");
		}
		
		global $PHprefs;
		if(!isset($PHprefs['DBserver'], $PHprefs['DBuserid'],
			$PHprefs['DBpasswd'], $PHprefs['DBport'])) {
				throw new Exception("Database connection not configured");
		}
		
		$this->_println("Backing up Database {$PHprefs['DBname']} as {$file}");
		shell_exec(sprintf("cd %s && mysqldump -h %s -u%s -p%s -P%d %s | gzip > %s",
			escapeshellarg($config_dir),
			escapeshellarg($PHprefs['DBserver']),
			escapeshellarg($PHprefs['DBuserid']),
			escapeshellarg($PHprefs['DBpasswd']),
			escapeshellarg($PHprefs['DBport']),
			escapeshellarg($PHprefs['DBname']),
			escapeshellarg($file)
		));
	}
	
	/**
	 * Rollback a previous upgrade
	 * @param string $file
	 * @throws Exception
	 */
	public function rollback($file = null)
	{
		global $PHprefs;
		
		if(!file_exists($file)) {
			throw new Exception("Rollback file could not be found");
		}
		
		$data = include $file;
		
		if(!isset($data['classname'], $data['rollback'], $data['params'])) {
			throw new Exception("Rollback file invalid");
		}
		
		$classname = $data['classname'];
		$rollback = $data['rollback'];
		$params = $data['params'];
		
		if(!class_exists($classname)) {
			throw new Exception("Upgrade class {$classname} does not exist!");
		}
		
		$outputter = new Brickwork_Upgrade_Script_Outputter($this->_c);
		$db = $this->_getDatabase();
		$pdo = $db->asPDO();
		$db_sync = new DB_Sync($pdo);
		$phoundry_sync = new PhoundrySync($pdo, $PHprefs['charset']);
		
		$upgrade = new $classname($db, $db_sync, $phoundry_sync);
		
		$upgrade->attachOutputter($outputter);
		$upgrade->setRollbackData($rollback);
		
		$this->_println("Rolling back {$classname}");
		$upgrade->rollback($params);
	}
	
	/**
	 * Writes the rollback data of an applied upgrade to disk
	 * @param string $file
	 * @param Brickwork_Upgrade $upgrade
	 * @param array $params
	 * @throws Exception
	 */
	protected function _writeRollbackData($file, Brickwork_Upgrade $upgrade, array $params)
	{
		if(file_exists(PROJECT_DIR.'/config')) {
			$config_dir = PROJECT_DIR.'/config';
		}
		else if(file_exists(PROJECT_DIR.'/admin')) {
			$config_dir = PROJECT_DIR.'/admin';
		}
		else {
			throw new Exception("Config directory could not be found");
		}
		
		$classname = get_class($upgrade);
		$rollback = $upgrade->getRollbackData();
		
		file_put_contents($config_dir."/{$file}", "<"."?php\n return ".
			var_export(compact('classname', 'rollback', 'params'), true).";");
	}
	
	/**
	 * Returns the DB_Adapter for the first configured connection
	 * @throws Exception
	 * @param int $connection
	 * @return DB_Adapter
	 */
	protected function _getDatabase($connection = 1)
	{
		global $PHprefs;
		$p = &$PHprefs;
		if(!isset($p['DBserver'], $p['DBname'], $p['DBuserid'], $p['DBpasswd'],
			$p['DBport'])) {
			throw new Exception("Database configuration incomplete cannot proceed");
		}
		
		return DB::getDb($connection);
	}
}