<?php
/**
 * Provides functionality to easen the pain of using various CLI commands
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class CommandLine
{
	/**
	 * @var Array Arguments as parsed by _parseArgs
	 */
	protected $_args;
	
	/**
	 * @var string the first argument in $argv normally t he scriptname
	 */
	protected $_scriptname;
	
	/**
	 * @var resource The resource that is used to write output to
	 */
	protected $_out;
	
	/**
	 * @var resource The resource that is used to read input from
	 */
	protected $_in;
	
	/**
	 * CommandLine uses by default the commandline arguments and in and output
	 * @param string $scriptname Path to the current script
	 * @param array $argv Commandline arguments
	 * @param resource $out Output resource used by println
	 * @param resource $in Input resource used by readln
	 */
	public function __construct($scriptname, array $argv, $out = null, $in = null)
	{
		if(!is_string($scriptname)) {
			throw new InvalidArgumentException("scriptname should be of type string");
		}
		if(null !== $out && !is_resource($out)) {
			throw new InvalidArgumentException("out should be a valid resource");
		}
		if(null !== $in && !is_resource($in)) {
			throw new InvalidArgumentException("in should be a valid resource");
		}
		
		$this->_scriptname = $scriptname;
		$this->_args = $this->_parseArgs($argv);
		$this->_out = null === $out ? STDOUT : $out;
		$this->_in = null === $in ? STDIN : $in;
	}
	
	/**
	 * Print some output to the commandline
	 * 
	 * @param string $msg
	 * @return int|bool Amount of bytes written, false on error
	 */
	public function println($msg = null)
	{
		return fwrite($this->_out, $msg."\r\n");
	}
	
	/**
	 * Read input from the commandline
	 * 
	 * @return string|bool The input, false on error
	 */
	public function readln()
	{
		return fread($this->_in);
	}
	
	/**
	 * Get a commandline argument by name
	 * 
	 * @param string $name
	 * @param mixedvar $default
	 * @return mixedvar
	 */
	public function getArg($name, $default = null)
	{
		return key_exists($name, $this->_args) ? $this->_args[$name] : $default;
	}
	
	/**
	 * Returns all given arguments as array
	 * 
	 * @return array
	 */
	public function getAllArgs()
	{
		return $this->_args;
	}
	
	/**
	 * Supports various ways of argument notations
	 * 
	 * --XYZ=waarde
	 * --XYZ
	 * -x=waarde
	 * -x waarde
	 * -xyz
	 * @param array $argv
	 * @return array
	 */
	protected function _parseArgs(array $argv)
	{
		// container for args
		$args = array();
		// --XYZ=waarde
		// --XYZ
		// -x=waarde
		// -x waarde
		// -xyz

		foreach($argv as $arg) {
			if ($arg[0] == '-') { // first char is -
				if ($arg[1] == '-') { // named --
					// --X=waarde
					if (strstr($arg, '=')) { // name value pair
						$parts = explode("=", $arg);
						$args[substr($parts[0], 2)] = $parts[1];
					}
					// --X
					else { // only a value, key and value are the same
						$args[substr($arg, 2)] = substr($arg, 2);
					}
				}
				else { // named -
					// -x=waarde
					if (strstr($arg, '=')) { // name value pair
						$parts = explode("=", $arg);
						$args[substr($parts[0], 1)] = $parts[1];
					}
					// -x waarde
					else if(strlen($arg) == 2) {
						$args[substr($arg, 1)] = NULL;
					}
					// -xyz (multiple flags)
					else {
						// each letter is an option
						foreach(str_split(substr($arg, 1)) as $arg) {
							$args[$arg] = TRUE;
						}
					}
				}
			}
			else { // value die hoort bij de vorige param

				// kijk naar laatst gezette waarde in de lijst
				end($args);

				// indien waarde null is, dan hebben we een `-x waarde` constructie en moet de waarde worden gezet
				if($args && is_null($args[key($args)])) {
					$args[key($args)] = $arg;
				}
				else {
					$args[] = $arg;
				}
			}
		}
		
		return $args;
	}
}