<?php
/**
* 404 content handler
*
* Handles the content selection and presentation for a page
* @todo even goed overna denken...
* @package ContentHandler
* @version 0.0.0
* @author Jorgen Teunis <jorgen.teunis@webpower.nl>
* @access public
*/
class HTTP404ContentHandler extends ContentHandler { 
	public	$id;
	
	/**
	 * Contenthandler Name
	 *
	 * @var String
	 */
	private $name = 'HTTP404ContentHandler';

	/**
	 * Mapping 
	 *
	 * @var Array
	 */
	protected $_mapping	= array();
	protected $_query	= '';

	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
		
		// later on we join it together, but this is to let it work like the other content handlers
		$this->path = explode('/', substr($_SERVER['REQUEST_URI'], 1));

		if(isset(Framework::$WEBprefs['http404_mapping'])){
			// custom set mapping
			$this->_mapping = $WEBprefs['http404_mapping'];
		} else {
			// default mapping
			$this->_mapping = array(
						'HTTP404_Image',
						'HTTP404_ReadableUrl',
						'HTTP404_BulkRedirect',
						'HTTP404_PageNotFound',
						'HTTP404_Template404',
						);
		}
	}

	public function __get($name) {

	}
	

	
	/**
	 * Initializes the content handler
	 * @return void
	 */
	public function init(){


	}

	/**
	 * Handles what output to generate
	 *
	 * @param boolean $return return the contents or output
	 * @return mixed string(html) or void
	 */
	public function out($return = FALSE){
		// glue path together
		
		$path = implode('/', $this->path);

		// walk through mapping object to handle requests
		foreach($this->_mapping as $mapping){
			$listener = new $mapping($path, $this->querystring); 
			$listener->fire();
		}
	}

}

?>
