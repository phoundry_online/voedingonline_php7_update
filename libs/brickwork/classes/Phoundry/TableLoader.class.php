<?php
require_once( $PHprefs['distDir'] . '/admin/sync/TableInterface.class.php');

/**
 * Wrapper class for the Phoundry Sync interface
 * This interface allows you to use the Phoundry TableInterface classes without having to
 * use Phoundry itself.
 * This script is used by the addmodule installer which allows you to add module packages
 * to a barebone brickwork/phoundry skeleton. This feature was introduced in version 1.4.1
 * 
 * @author			Peter Eussen
 * @package		Brickwork
 * @subpackage		Phoundry
 * @version		1.0
 */
class Phoundry_TableLoader
{
	/**
	 * Phoundry Table Interface
	 *
	 * @var TableInterface
	 */
	protected	$_ti;
	
	/**
	 * The definition filename
	 *
	 * @var string
	 */
	protected	$_definitionFile;

	/**
	 * The data in the definition file
	 *
	 * @var Array
	 */
	protected	$_definitions;
	
	public function __construct( )
	{
		$this->_ti 				= new TableInterface();
		$this->_definitionFile	= null;
		$this->_definitions		= null;
	}
	
	/**
	 * Loads a definition file into memory
	 * This function loads the file, but does not actually do anything (yet) other
	 * than checking if the definition file was loaded succesfully.
	 * 
	 * @param	string	$dataFile
	 * @return	boolean
	 */
	public function load( $dataFile )
	{
		$this->_definitionFile 	= $dataFile;
		
		try
		{
			$this->_definitions	= $this->_safeLoadFile($this->_definitionFile);
			
			if ( is_null($this->_definitions))
				return false;
		}
		catch ( Phoundry_Exception $e)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Retrieves a list of all target tables for all maintenance functions
	 * This function will return an array with all tables required for a succesful load.
	 * It will throw an Exception if no file was loaded.
	 * 
	 * @return	Array
	 * @throws	Phoundry_Exception
	 */
	public function getTargetTables()
	{
		if ( !is_null($this->_definitions) )
		{
			$tmp = Array();
			
			foreach($this->_definitions['table_trees'] as $tid => $table_tree)
			{
				if ( isset($table_tree['name']))
					array_push($tmp, $table_tree['name']);
			}
			
			$tmp = array_unique($tmp);
			
			return $tmp;
		}
		throw new Phoundry_Exception("No file loaded");
	}
	
	/**
	 * Gets a list of missing tables that are required for a succesful load
	 * 
	 * @return	Array
	 * @throws Phoundry_Exception
	 */
	public function getMissingTables()
	{
		$required = $this->getTargetTables();
		$missing  = Array();
		
		foreach( $required as $table )
		{
			try
			{
				// Try reflecting, if it succeeds, its there!
				$tr = new Reflection_Table($table);
			}
			catch (Exception $e )
			{
				array_push($missing, $table);
			}
		}
		
		return $missing;
	}
	
	/**
	 * Tries to load a definition file.
	 * 
	 * @return		boolean
	 * @throws		Phoundry_Exception
	 */
	public function import()
	{
		$missing = $this->getMissingTables();
		
		// Check if all tables are there
		if ( count($missing) )
			throw new Phoundry_Exception( sprintf("Missing tables for import: %s", implode(", ", $missing)));

		foreach( $this->_definitions['table_trees'] as $tid => $table_tree )
		{
			// Check if there is already a function for this table (probably a duplicate insert?)
			if ( !isset($table_tree['name']) || (isset($table_tree['name']) && PhoundryTools::getTableTID($table_tree['name']) === false) )
			{
				if( !$this->_ti->insert($table_tree) ) 
				{
					throw new Phoundry_Exception( sprintf("Failed to load '%s'",$table_tree['description']) );
				}
			}
		}
		return true;
	}
	
	
	/**
	 * Tries to load a file in a safe way
	 * Checks if the file really exists, and if it can be read by the current user.
	 * If the file could not be loaded, it will return NULL. If the file could
	 * be loaded it will return an Array containing the table definitions.
	 * 
	 * If the file does not contain a proper .dat format, it will throw a 
	 * Phoundry_Exception.
	 * 
	 * @param	string		$dataFile	The file to load
	 * @return	Array|null	
	 * @throws Phoundry_Exception
	 */
	protected function _safeLoadFile( $dataFile )
	{
		if ( file_exists($dataFile) && is_readable($dataFile) )
		{
			@include($dataFile);
			
			if ( !isset($varExported) )
				throw new Phoundry_Exception('Data file did not yield varExported, corrupted datafile?');
			
			return $varExported;
		}
		return null;
	}
}

