<?php

/**
 * Exception for Phoundry interface failures
 * 
 * @author			Peter Eussen
 * @package		Brickwork
 * @subpackage		Phoundry
 * @version		1.0
 */
class Phoundry_Exception extends Exception { }