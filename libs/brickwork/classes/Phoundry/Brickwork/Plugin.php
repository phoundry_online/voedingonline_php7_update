<?php
/**
 * Phoundry Brickwork Plugin
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
abstract class Phoundry_Brickwork_Plugin extends PhoundryPlugin
{
	/**
	 * The current site_identifier
	 * @var string|void
	 */
	protected $_site_identifier;

	/**
	 * Initialize variables and check access
	 */
	public function  __construct()
	{
		DB::setCollation('utf8');
		$this->_site_identifier = !empty($_GET['site_identifier']) ? $_GET['site_identifier'] : NULL;
		if(is_null($this->site_identifier))
		{
			$this->error('Site Identifier not found', true);
		}

		$class = 'BrickBootstrap';

		BrickBootstrap::run(array(
			30	=> array($class, 'initErrorHandler'),
			50 	=> function () { return Framework::loadSiteByIdentifier($this->_site_identifier); },
			70	=> array($class, 'initClient'),
			90 	=> array($class, 'initRepresentation'),
			110 => array($class, 'initFramework'),
		));

		parent::__construct();
	}

	/**
	 * Readonly access to plugin attributes
	 * @param string $name
	 * @return mixedvar
	 */
	public function  __get($name)
	{
		switch ($name)
		{
			case 'site_identifier':
				return $this->_site_identifier;

			default:
				return parent::__get($name);
		}
	}

	/**
	 * Render the plugin specific template
	 * @param string $template
	 * @return string
	 */
	protected function renderPlugin($template)
	{
		if('tpl' == substr($template, strrpos($template, '.')+1))
		{
			Framework::$representation->assign($this->_data);
			$template = realpath('./'.$template);
			return Framework::$representation->fetch('file:'.$template.'.tpl');
		}
		else
		{
			return parent::renderPlugin($template);
		}
	}
}

