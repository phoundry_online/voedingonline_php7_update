<?php

/**
 * Wrapper for Brickwork/Phoundry couplings
 * This class is used by the addmodule script to check if a module has already
 * been added to an installation and has the ability to add the module based
 * on a simple definitions xml file.
 * 
 * The definitions XML should describe the configuration that should be used
 * for the module. See the example below:
 * 
 * <definitions>
 * 	<module code="agenda">
 * 		<name>Agenda</name>
 * 		<class>AgendaModule</class>
 * 		<template>agenda.tpl</template>
 * 		<maxperpage />
 * 		<cachetime>0</cachetime>
 * 		<tables>
 * 			<configuration>module_config_agenda</configuration>
 * 			<content>module_content_agenda</content>
 * 		</tables>
 * 	</module>
 * </definitions>
 * 
 * - A definition file can define an arbitrary number of modules
 * - every module definition should be placed at the same level (below the definitions tag)
 * - every module should have a "code" attribute which should have a unique code for this
 *   module (alphanumeric)
 * - Every module needs at least a code, name and class in order for the import to succeed
 * - tables/configuration and tables/content should point to the database table that controls
 *   the specific content/config of the module
 * - maxperpage should be empty or a numeric character
 * - cachetime should be numeric (zero for no cache)
 * @author			Peter Eussen
 * @package		Brickwork
 * @subpackage		Phoundry
 * @version		1.0
 */
class Phoundry_Brickwork_Definitions
{
	protected $_definitionsFile;
	
	public function __construct( $file )
	{
		$this->_definitionsFile = $file;
	}
	
	public function moduleExists(  )
	{
		if ( !$this->needsLoad() )
			return false;
			
		$definitions = simplexml_load_file( $this->_definitionsFile );
		
		foreach( $definitions as $module )
		{
			if ( isset($module['code']))
			{
				$sql = sprintf("SELECT code FROM brickwork_module WHERE code = '%s'",DB::Quote($module['code']));
				$res = DB::iQuery($sql);
				
				if ( self::isError($res))
					throw new Exception("module check failed");
				elseif ( $res->current() )
					return true;
						
			}
		}
		return false;
	}
	
	public function needsLoad()
	{
		if ( file_exists( $this->_definitionsFile) )
			return true;
		return false;
	}
	
	public function load()
	{
		if ( !$this->needsLoad() )
			return false;
			
		$definitions = simplexml_load_file( $this->_definitionsFile );
		$sql 		 = 'INSERT INTO brickwork_module (code, class, name, description, template, max_per_page, cachetime, ptid_content, ptid_config )' .
				   	   " VALUES('%s','%s','%s','Default modulefor %s',%s, %d, %d, %s, %s)";
			
		
		foreach( $definitions as $module )
		{
			$name 		= (isset($module->name) ? $module->name : null);
			$code 		= (isset($module['code']) ? $module['code'] : null);
			$class		= (isset($module->class) ? $module->class : null );
			$template	= (isset($module->template) ? $module->template : null );
			$mpp		= (isset($module->maxperpage) ? (int)$module->maxperpage : null );
			$cache		= (isset($module->cachetime) ? (int)$module->classtime : 0 );
			
			if ( is_null($name)|| is_null($class) || is_null($code) )
				throw new Exception( sprintf("Module definition error: no name defined"));
			
			if ( isset($module->tables) )
			{
				if ( isset($module->tables->configuration) && $module->tables->configuration != '')
				{
					$ptconfig = PhoundryTools::getTableTID( (string)$module->tables->configuration );
					
					if ( $ptconfig === false )
						throw new Exception( sprintf("Could not locate configuration on table %s", $module->tables->configuration));
				}
				else
					$ptconfig = null;
					
					
				if ( isset($module->tables->content) && $module->tables->content != '')
				{
					$ptcontent = PhoundryTools::getTableTID( (string)$module->tables->content );
					
					if ( $ptcontent === false )
						throw new Exception( sprintf("Could not locate configuration on table %s", $module->tables->content));
				}
				else
					$ptcontent = null;
					
					
			}
			$res = DB::iQuery("SELECT code FROM brickwork_module WHERE code = '%s'", DB::quote($code) );
			
			if ( self::isError($res))
			{
				throw new Exception( "Could not check brickwork module, install not complete? (%s)", $res->errstr);
			}
			
			// Already there
			if ( $res->current() )
			{
				continue;
			}
			
			$tmp = sprintf($sql,DB::quote($code), 
								DB::quote($class), 
								DB::quote($name),
								DB::quote($name),  
								(is_null($template) ? 'NULL' : DB::quote($template,true)), 
								(int)$mpp, 
								(int)$cache, 
								(is_null($ptcontent) ? 'NULL' : (int)$ptcontent), 
								(is_null($ptconfig) ? 'NULL' : (int)$ptconfig) );
			
			$res = DB::iQuery( $tmp );
			
			if ( self::isError($res))
				throw new Exception( "Could not create module definition (%s)", $res->errstr );
		}
		return true;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

