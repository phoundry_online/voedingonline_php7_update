<?php
class Phoundry_Brickwork_ContentNodeService
{
	/**
	 * @var DB_Adapter
	 */
	private $db;
	
	/**
	 * @var Cache
	 */
	private $cache;
	
	public function __construct(DB_Adapter $db)
	{
		$this->db = $db;
	}
	
	public function setCache(Cache $cache)
	{
		$this->cache = $cache;
	}
	
	public function getLink(PhoundryContentNode $node, array $params = array())
	{
		if ($this->cache) {
			if ($link = $this->cache->getValue($node->id)) {
				return $this->formatLink($link, $params);
			}
		}
		
		$link = $this->getUnformattedLink($node);
		if ($link) {
			if ($this->cache) {
				$this->cache->setValue($node->id, $link);
			}
			return $this->formatLink($link, $params);
		}
			
		return false;
	}
	
	/**
	 * Get links for an array of nodes instead of just one node
	 * @param array $nodes
	 * @param array $params
	 * @return array
	 */
	public function getLinks(array $nodes, array $params = array())
	{
		$links = array();
		foreach ($nodes as $node) {
			if ($link = $this->getLink($node, $params)) {
				$links[] = $link;
			}
		}
		return $links;
	}
	
	private function getUnformattedLink(PhoundryContentNode $node)
	{
		if ($node->string_id === "brickwork_page_content" ||
				$this->isConfigTable($node->string_id)) {
			// This is a page content item so we just have to find the page
			// that this content item is displayed on
			$link = $this->getLinkForPageContent($node);
			if ($link) {
				return $link;
			}
			return false;
		}
		
		$link = $this->getLinkForContentNode($node);
		if ($link) {
			return $link;
		}
		return false;
	}
	
	private function isConfigTable($string_id)
	{
		$tid = PhoundryTools::getTableIdByString($string_id);
		$res = $this->db->iQuery(sprintf("
			SELECT
				ptid_config
			FROM
				brickwork_module
			WHERE
				ptid_config = %d
			LIMIT 1",
			$tid
		));
		
		return count($res) !== 0;
	}
	
	private function getLinkForPageContent(PhoundryContentNode $node)
	{
		$page_content_id = $node->rid;
		$row = $this->db->iQuery(sprintf("
			SELECT
				ss.id, ss.name, ss.site_identifier, p.lang_id, p.title
			FROM
				brickwork_page_content AS pc
			INNER JOIN
				brickwork_page AS p
			ON
				p.id = pc.page_id
			INNER JOIN
				brickwork_site_structure AS ss
			ON
				p.site_structure_id = ss.id
			WHERE
				pc.id = %d",
			$page_content_id
		))->first();
		
		if ($row) {
			$link = array();
			$link['content_id'] = $node->id;
			$link['structure_id'] = $row->id;
			$link['site_identifier'] = $row->site_identifier;
			$link['lang_id'] = (int) $row->lang_id;
			$link['url'] = SiteStructure::getFullPathByStructureId(
				$row->id, $row->site_identifier, $row->lang_id);
			$link['title'] = $row->title ? $row->title : $row->name;
			return $link;
		}
		return false;
	}
	
	private function getLinkForContentNode(PhoundryContentNode $node)
	{
		$link = array();
		$link['content_id'] = $node->id;
		
		$structure = $this->db->iQuery(sprintf("
			SELECT
				np.site_structure_id,
				np.lang_id,
				np.site_identifier,
				np.id_param,
				np.title_field,
				ss.name
			FROM
				brickwork_phoundry_content_node_page AS np
			INNER JOIN
				brickwork_site_structure AS ss
			ON
				np.site_structure_id = ss.id
			WHERE
				string_id = %s
			LIMIT 1",
			$this->db->quoteValue($node->string_id)
		))->first();
		
		if (!$structure) {
			return false;
		}
		$link['structure_id'] = $structure->site_structure_id;
		$link['site_identifier'] = $structure->site_identifier;
		$link['lang_id'] = (int) $structure->lang_id;
		$link['url'] = SiteStructure::getFullPathByStructureId(
			$structure->site_structure_id, $structure->site_identifier,
			(int) $structure->lang_id);
			
		if (!empty($structure->id_param)) {
			$link['url'] .= "?".$structure->id_param."=".$node->rid;
		}
		
		if (!empty($structure->title_field)) {
			$field = $structure->title_field;
			$row = $this->db->iQuery(sprintf("
				SELECT
					name
				FROM
					phoundry_table
				WHERE
					string_id = %s
				LIMIT 1",
				$this->db->quoteValue($node->string_id)
			))->first();
			
			if ($row) {
				$table = $row->name;
				$row = $this->db->iQuery(sprintf("
					SELECT
						%s AS title
					FROM
						%s
					WHERE
						id = %d",
					$field,
					$table,
					$node->rid
				))->first();
				if ($row) {
					$link['title'] = $row->title;
				}
			}
		}
		
		if (empty($link['title'])) {
			$page = SiteStructure::getLivePage($this->db,
				$structure->site_identifier,
				$structure->site_structure_id,
				$structure->lang_id ? $structure->lang_id : null);
			if (!$page) {
				return false;
			}
			$link['title'] = $page->title ? $page->title : $structure->name;
		}
		
		return $link;
	}
	
	private function formatLink(array $link, array $params = array())
	{
		$url = $link['url'];
		if ($params) {
			// Merge the given GET params with the configured ones
			$query = array();
			if (strpos($url, "?") !== false) {
				$expl = explode("?", $url, 2);
				$url = $expl[0];
				parse_str($expl[1], $query);
			}
			$query = array_merge_recursive($params, $query);
			$url = $url.($query ? "?".http_build_query($query) : "");
		}
		
		$link['url'] = $url;
		return $link;
	}
}