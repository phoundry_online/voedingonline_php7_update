<?php
class ClipEvents_Caption extends ORM
{
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('start', 'ASC');
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
		'video' => array('ClipEvents_Video', 'video_id'),
	);
	
	public function offsetSet($name, $value)
	{
		switch ($name) {
			case 'start':
			case 'end':
				$value = str_replace(',', '.', $value);
			break;
		}
		
		parent::offsetSet($name, $value);
	}
	
	public function getReadableTime($name)
	{
		if(is_numeric($this[$name])) {
			$seconds = $this[$name];
			$hours = intval($seconds / 3600);	
			$minutes = intval(($seconds - ($hours * 3600)) / 60);	
			$seconds = ($seconds - (($hours * 3600)+($minutes * 60)));
			
			return sprintf(
				"%s:%s:%s",
				str_pad($hours, 2, "0", STR_PAD_LEFT),
				str_pad($minutes, 2, "0", STR_PAD_LEFT),
				($seconds < 10 ? '0'.$seconds : $seconds)
			);
		}
	}
}
