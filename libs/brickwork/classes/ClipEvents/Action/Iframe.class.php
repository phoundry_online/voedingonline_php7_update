<?php
class ClipEvents_Action_Iframe extends ORM implements ClipEvents_ActionInterface
{
	public function getData()
	{
		return array('width' => $this['width'], 'heigth' => $this['height'], 'url' => $this['url']);
	}
	
	public function getJavascript($data = array('domtarget' => '', 'timename' => '', 'start' => '', 'end' => ''))
	{
		return (string) sprintf(
			'if(type == "TIME") {
				if(!this.active && (video.position >= %1$s && video.position <= %2$s)) {
					if(!this.jqueryTarget) this.jqueryTarget = $("%3$s");
					this.active = true;
					
					var ei_frame = $("<iframe frameborder=\"0\">"); /* Using attr() for frameborder doesnt work in IE */
					ei_frame.
						attr("src", this.data.url).
						attr("width", (this.data.width ? this.data.width : this.jqueryTarget.width())).
						attr("height", (this.data.height ? this.data.height : this.jqueryTarget.height()));					
					
					this.jqueryTarget.empty().append(ei_frame);
				} else if(this.active && !(video.position >= %1$s && video.position <= %2$s)) {
					this.active = false;
				}
			}',
			$data['start'],
			$data['end'],
			$data['domtarget']
		);
		
	}
	
}
