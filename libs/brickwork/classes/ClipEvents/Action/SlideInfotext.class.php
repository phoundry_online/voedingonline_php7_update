<?php
class ClipEvents_Action_SlideInfotext extends ORM
{
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
		'video' => array('ClipEvents_Video', 'video_id'),
		'domtarget' => array('ClipEvents_Domtarget', 'domtarget_id'),
	);
}
