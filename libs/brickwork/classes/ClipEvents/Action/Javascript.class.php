<?php
class ClipEvents_Action_Javascript extends ORM implements ClipEvents_ActionInterface
{
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
		'javascript_function' => array('ClipEvents_JavascriptFunction', 'javascript_function_id'),
	);
	
	public function getData()
	{
		null;
	}
	
	public function getJavascript($data = array())
	{
		$parameter_keys = explode(',', trim($this->javascript_function['parameters'], ','));
		$parameter_vals = explode(',', trim($this['parameters'], ','));
		$parameter_keys = array_map('trim', $parameter_keys);
		$parameter_vals = array_map('trim', $parameter_vals);
		foreach($parameter_keys as $k => $key){
			$data[$key] = !empty($parameter_vals[$k]) ? (string) $parameter_vals[$k] : '';
		}
		
		$find = array();
		foreach(array_keys($data) as $key) {
			$find[] = '{$'.$key.'}';
		}
		$replace = array_values($data);
		
		$javascript = str_replace($find, $replace, $this->javascript_function['body']);
		
		return (string) $javascript;
	}
	
}
