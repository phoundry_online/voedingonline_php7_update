<?php
class ClipEvents_Action_Captions extends ORM implements ClipEvents_ActionInterface
{
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
		'video' => array('ClipEvents_Video', 'video_id'),
	);
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
//		'foreign_objects' => array('foreign_classname', array('foreign_fields', 'matching_own', 'primary_key')),
	);
	
	public function getData()
	{
		$data = array();
		
		foreach($this->video->captions as $caption) {
			$data[] = array(
				'start' => $caption['start'],
				'end' => $caption['end'],
				'text' => $caption['text']
			);
		}
		
		
		return $data; 
	}
	
	public function getJavascript($data = array())
	{
		extract($data);
		
		switch ($this['effect']) {
			case 'slide':
				$open = 'slideDown';
				$close = 'slideUp';
			break;
			case 'fade':
			default:
				$open = 'fadeIn';
				$close = 'fadeOut';
			break;
		}
		
		
		return (string) sprintf(
			'if(type == "TIME") {
			
				if(!this.jqueryTarget){
					this.jqueryTarget = $("%s");
				}
				
				var activeCaption = ""; 
				$.each(this.data, function(i, caption){
					if(video.position >= caption.start && video.position <= caption.end) {
						activeCaption = caption.text;
					}
				});
				
				this.jqueryTarget.html(activeCaption);
			}',
			$domtarget
		);
		
	}
	
}
