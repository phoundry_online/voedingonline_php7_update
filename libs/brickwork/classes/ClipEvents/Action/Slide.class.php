<?php
class ClipEvents_Action_Slide extends ORM implements ClipEvents_ActionInterface
{
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
//		'foreign_objects' => array('foreign_classname', array('foreign_fields', 'matching_own', 'primary_key')),
	);
	
	public function getData()
	{
		$infotexts = array();
		foreach ($this->getInfoTexts() as $infotext) {
			$infotexts[$infotext['id']] = array(
				'id' => $infotext['id'],
				'domtarget' => $infotext->domtarget['jquery_selector'],
				'text' => Utility::makeUrlsAbsolute($infotext['text'], "http://".$_SERVER['HTTP_HOST']),
			);
		}
		
		$data = array(
			'body' => sprintf(
				"%s</h2>\n<div>%s</div>",
				($this['title'] !== null ? "<h2>".htmlspecialchars($this['title'])."</h2>" : ''),
				Utility::makeUrlsAbsolute($this['body'], "http://".$_SERVER['HTTP_HOST'])
			),
			'infotext'=> $infotexts,
			'insert_at' => $this['insert_at']
		);
		return $data;
	}
	
	public function getJavascript($data = array())
	{
		extract($data);
		
		switch ($this['effect']) {
			case 'slide':
				$open = 'slideDown';
				$close = 'slideUp';
			break;
			case 'fade':
			default:
				$open = 'fadeIn';
				$close = 'fadeOut';
			break;
		}
		
		
		return (string) sprintf(
			'if(type == "TIME") {
				if(!this.active && (video.position >= %1$s && video.position <= %2$s)) {
					var data = this.data;
					if(!this.jqueryTarget) this.jqueryTarget = $("%3$s");
					this.active = true;
					
					function slideLinkListener(e){
						var clicked = $(e.target);
						
						if(clicked.is(".CE_pause")) {
							video.flashPlayer.sendEvent("PLAY", "false");
						}
						
						if(clicked.is(".CE_play")) {
							video.flashPlayer.sendEvent("PLAY", "true");
						}
						
						if(clicked.is("a.infotext") || clicked.parents("a.infotext:first").length) {
							if(clicked.parents("a.infotext:first").length) {
								clicked = clicked.parents("a.infotext:first");
							}
							e.preventDefault();
							
							var infotext = clicked.attr("href").split("#")[1].replace("infotext_","");
							
							if(data.infotext[infotext] && data.infotext[infotext].text != "") {
								infotext = data.infotext[infotext];
								var clickCatcher = $("<div>").html(infotext.text).click(slideLinkListener);
								$((!infotext.domtarget || infotext.domtarget == "") ? "#infotext" : infotext.domtarget).
									empty().append(clickCatcher);
							}
							
							return false;
						}
						
						if(clicked.is("a")) {
							video.flashPlayer.sendEvent("PLAY", "false");
							
							if(clicked.attr("target") != "_self" && clicked.get(0).hostname != window.location.hostname) {
								window.open(clicked.attr("href"));
								return false;
							}
						}
					};
					
					var clickCatcher = $("<div>").html(data.body).click(slideLinkListener);
					if(data.insert_at == "replace") {
						this.jqueryTarget.%5$s("normal", function(){
							$(this).empty().append(clickCatcher).%4$s();
						});
					} else if(data.insert_at == "append") {
						this.jqueryTarget.append(clickCatcher.hide());
						clickCatcher.%4$s();
					} else if(data.insert_at == "prepend") {
						this.jqueryTarget.prepend(clickCatcher.hide());
						clickCatcher.%4$s();
					}
					
				} else if(this.active && !(video.position >= %1$s && video.position <= %2$s)) {
					this.active = false;
				}
			}',
			$start,
			$end,
			$domtarget,
			$open,
			$close
		);
		
	}
	
	public function getInfoTexts()
	{
		$stack = array();
		$infotexts = array();
		$infotext_ids = array();
		
		preg_match_all("/<a.+href=\"#infotext_(\d+)\".*>/iU", $this['body'], $matches);
		
		if(!empty($matches[1])) {
			$stack = $stack + ActiveRecord::factory('ClipEvents_Action_SlideInfotext')->where('id', '('.implode(',', $matches[1]).')', 'IN', false)->get()->getAsArray();
		}
		
		
		while(NULL !== ($info_text = array_shift($stack)))
		{
			// Make sure to parse infotexts only once!
			if(!in_array($info_text['id'], $infotext_ids))
			{
				$infotext_ids[] = $info_text['id'];
				$infotexts[] = $info_text;
				
				preg_match_all("/<a.+href=\"#infotext_(\d+)\".*>/iU", $info_text['text'], $matches);
				if(!empty($matches[1])) {
					$stack = $stack + ActiveRecord::factory('ClipEvents_Action_SlideInfotext')->where('id', '('.implode(',', $matches[1]).')', 'IN', false)->get()->getAsArray();
				}
			}
		}
		
		return $infotexts;
	}
	
}
