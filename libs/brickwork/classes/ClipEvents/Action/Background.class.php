<?php
class ClipEvents_Action_Background extends ORM implements ClipEvents_ActionInterface
{
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
	);
	
	public function getData()
	{
		return null;
	}
	
	public function getJavascript($data = array())
	{
		extract($data);
		
		$domtarget;
		$timename;
		$start;
		$end;
		
		if(!empty($this['value'])) {
			return (string) sprintf(
				'if(type == "TIME") {
					if(!this.active && (video.position >= %1$s && video.position <= %2$s)) {
						if(!this.jqueryTarget) {
							this.jqueryTarget = $("%3$s");
						}
						this.jqueryTarget.css("background-color", "%4$s");
						this.active = true;
				
					} else if(this.active && !(video.position >= %1$s && video.position <= %2$s)) {
						this.active = false;
					}
				}',
				$start,
				$end,
				$domtarget,
				$this['value']
			);
		
		}
		
		global $PHprefs;
		if(!empty($this['file'])) {
			return (string) sprintf(
				'if(type == "TIME") {
					if(!this.active && (video.position >= %1$s && video.position <= %2$s)) {
						if(!this.jqueryTarget) {
							this.jqueryTarget = $("%3$s");
						}
						this.jqueryTarget.css("background-image", "url(\'%4$s\')");
						this.active = true;
				
					} else if(this.active && !(video.position >= %1$s && video.position <= %2$s)) {
						this.jqueryTarget.css("background-image", "");
						this.active = false;
					}
				}',
				$start,
				$end,
				$domtarget,
				"http://".$_SERVER['HTTP_HOST'].$PHprefs['uploadUrl'].$this['file']
			);
		}
	}
	
}
