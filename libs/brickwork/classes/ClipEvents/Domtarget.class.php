<?php
class ClipEvents_Domtarget extends ORM
{
	/**
	 * Name of the Table the AR operates on
	 *
	 * @var string
	 */
	protected $_table_name;
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('name', 'ASC');
	
	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
//		'foreign_objects' => array('foreign_classme', 'join_table', array('self_primary_key', 'self_primary_key'), array('foreign_PK')),
	);
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
//		'foreign_objects' => array('foreign_classname', array('foreign_fields', 'matching_own', 'primary_key')),
	);
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
	);
}
