<?php
class ClipEvents_Action extends ORM
{
	/**
	 * Name of the Table the AR operates on
	 *
	 * @var string
	 */
	protected $_table_name;
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('name', 'ASC');
	
	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
//		'foreign_objects' => array('foreign_classme', 'join_table', array('self_primary_key', 'self_primary_key'), array('foreign_PK')),
	);
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
//		'foreign_objects' => array('foreign_classname', array('foreign_fields', 'matching_own', 'primary_key')),
	);
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
		'video' => array('ClipEvents_Video', 'video_id'),
		'time' => array('ClipEvents_Time', 'time_id'),
		'action_type' => array('ClipEvents_ActionType', 'action_type_id'),
		'domtarget' => array('ClipEvents_Domtarget', 'domtarget_id'),
	);
	
	protected $_real_action = 0;
	
	/**
	 * Get the real action
	 *
	 * @return ActiveRecord
	 */
	public function getRealAction()
	{
		if($this->_real_action === 0) {
			if($this->action_type->isLoaded()) {
				$realAction = ActiveRecord::factory($this->action_type['class_name'], $this['type_pk']);
			}
			if($realAction instanceof ClipEvents_ActionInterface && $realAction->isLoaded()) {
				$this->_real_action = $realAction;
			} else {
				$this->_real_action = null;
			}
		}
		return $this->_real_action;
	}
	
	public function getJavascript()
	{
		$real_action = $this->getRealAction();
		if($real_action instanceof ClipEvents_ActionInterface) {
			return (string) $real_action->getJavascript(array(
				'domtarget' => $this->domtarget['jquery_selector'],
				'timename' => $this->time['name'],
				'start' => $this->time['start'],
				'end' => $this->time['end']
			));
		}
		
		return '';
	}
	
	public function getData()
	{
		$real_action = $this->getRealAction();
		if($real_action instanceof ClipEvents_ActionInterface) {
			return $real_action->getData();
		}
		return null;
	}
	
	public function delete($childs = true)
	{
		if($childs) {
			$real_action = $this->getRealAction();
			if($real_action instanceof ClipEvents_ActionInterface) {
				$real_action->delete();
			}
		}
		
		return parent::delete();
	}
}
