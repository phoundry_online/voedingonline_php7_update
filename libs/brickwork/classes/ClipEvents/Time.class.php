<?php
class ClipEvents_Time extends ORM
{
	/**
	 * Name of the Table the AR operates on
	 *
	 * @var string
	 */
	protected $_table_name;
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('start', 'ASC');
	
	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
//		'foreign_objects' => array('foreign_classme', 'join_table', array('self_primary_key', 'self_primary_key'), array('foreign_PK')),
	);
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
//		'foreign_objects' => array('foreign_classname', array('foreign_fields', 'matching_own', 'primary_key')),
		'actions' => array('ClipEvents_Action', 'time_id'),
	);
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
		'video' => array('ClipEvents_Video', 'video_id'),
	);
	
	public function offsetSet($name, $value)
	{
		switch ($name) {
			case 'start':
			case 'end':
				$value = str_replace(',', '.', $value);
			break;
		}
		
		parent::offsetSet($name, $value);
	}
	
	public function getReadableTime($name)
	{
		if(is_numeric($this[$name])) {
			$seconds = $this[$name];
			$hours = intval($seconds / 3600);	
			$minutes = intval(($seconds - ($hours * 3600)) / 60);	
			$seconds = ($seconds - (($hours * 3600)+($minutes * 60)));
			
			return sprintf(
				"%s:%s:%s",
				str_pad($hours, 2, "0", STR_PAD_LEFT),
				str_pad($minutes, 2, "0", STR_PAD_LEFT),
				($seconds < 10 ? '0'.$seconds : $seconds)
			);
		}
	}
}
