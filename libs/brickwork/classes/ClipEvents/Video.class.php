<?php
class ClipEvents_Video extends ORM
{
	/**
	 * Name of the Table the AR operates on
	 *
	 * @var string
	 */
	protected $_table_name;
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('name', 'ASC');
	
	/**
	 * Many to many configuration (n<->n / lookuptable)
	 *
	 * @var array
	 */
	protected $_many_to_many = array(
//		'foreign_objects' => array('foreign_classme', 'join_table', array('self_primary_key', 'self_primary_key'), array('foreign_PK')),
	);
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
//		'foreign_objects' => array('foreign_classname', array('foreign_fields', 'matching_own', 'primary_key')),
		'captions' => array('ClipEvents_Caption', 'video_id'),
		'times' => array('ClipEvents_Time', 'video_id'),
		'actions' => array('ClipEvents_Action', 'video_id'),
		'domtargets' => array('ClipEvents_Domtarget', 'video_id'),
	);
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
	);
	
	public function delete($childs = true)
	{
		if($childs) {
			foreach($this->captions as $caption) $caption->delete();
			foreach($this->times as $time) $time->delete();
			foreach($this->actions as $action) $action->delete();
			foreach($this->domtargets as $domtarget) $domtarget->delete();
		}
		
		return parent::delete();
	}
}
