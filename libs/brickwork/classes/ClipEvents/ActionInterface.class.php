<?php
interface ClipEvents_ActionInterface
{
	public function getJavascript($data = array());

	public function getData();
}
