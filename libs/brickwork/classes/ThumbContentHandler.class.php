<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("ThumbContentHandler is deprecated in favor of ImageContentHandler", E_USER_ERROR);
}

if(!defined('THUMBCH_IMG_MAXWIDTH')) define('THUMBCH_IMG_MAXWIDTH', 500);
if(!defined('THUMBCH_IMG_MAXHEIGHT')) define('THUMBCH_IMG_MAXHEIGHT', 500);
if(!defined('THUMBCH_IMG_MINWIDTH')) define('THUMBCH_IMG_MINWIDTH', 20);
if(!defined('THUMBCH_IMG_MINHEIGHT')) define('THUMBCH_IMG_MINHEIGHT', 20);
if(!defined('THUMBCH_CACHE_OPTIONS')) define('THUMBCH_CACHE_OPTIONS', 'dir:///ThumbContentHandler/?timeout=2592000&raw=true&sweep=true');
/**
 * Thumb Content Handler
 * 
 * Create tumbnails from images in the configured directory
 * 
 * @version 0.0.1
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl> *
 */
class ThumbContentHandler extends ContentHandler
{
	/**
	 * Path to the image
	 *
	 * @var string
	 */
	protected $image_filename;
	
	/**
	 * Path to the image directory
	 *
	 * @var string
	 */
	protected $image_dir;
	
	/**
	 * Caching object
	 *
	 * @var Cache_Dir
	 */
	protected $cache;
	
	/**
	 * Key for the image in the cache
	 *
	 * @var string MD5 hash
	 */
	protected $cache_key;
	
	protected $width;
	protected $height;
	
	
	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
		
		$this->cache_time = 432000; // 5 days in seconds, browser cache time
		
		$this->width = (int) array_shift($this->path);
		$this->height = (int) array_shift($this->path);
		
		// Never smaller then 20px and never greather then 500px
		$this->width = max(THUMBCH_IMG_MINWIDTH, min(THUMBCH_IMG_MAXWIDTH, $this->width));
		$this->height = max(THUMBCH_IMG_MINHEIGHT, min(THUMBCH_IMG_MAXHEIGHT, $this->height));
		
		$this->cache = Cache::factory(THUMBCH_CACHE_OPTIONS);
		
		$this->image_dir = UPLOAD_DIR."IMAGES";
		$this->image_filename = implode('/', $this->path);
		if(!is_readable($this->image_dir.'/'.$this->image_filename))
		{
			header("HTTP/1.0 404 Not Found");
			die('404 not found');			
		}
		$this->image_mtime = filemtime($this->image_dir.'/'.$this->image_filename);
		$this->cache_key = $this->width."x".$this->height.'/'.$this->image_filename.'_'.$this->image_mtime;
	}
	
	public function init()
	{
		// Check if we already generated the thumbnail
		if(!isset($this->cache[$this->cache_key]))
		{
			$ext = strtolower(substr(strrchr($this->image_filename, '.'),1));
			$image_editor = new ImageEditor($this->image_dir.'/'.$this->image_filename, $ext);
			ini_set('memory_limit','100M');
			set_time_limit(130);
			$image_editor->resample($this->width, $this->height);
				
			ob_start();
			$image_editor->render();
			$this->cache[$this->cache_key] = ob_get_clean();
		}
	}
	
	public function out()
	{
		$headers = apache_request_headers();
		if(isset($headers["If-Modified-Since"]) && $this->image_mtime <= strtotime($headers["If-Modified-Since"])){
			header("HTTP/1.1 304 Not Modified");
			exit;
		}

		// set cache headers
		header('Date: '.gmdate('D, d M Y H:i:s \G\M\T', $this->image_mtime));
		header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', $this->image_mtime + $this->cache_time));
		header('Last-Modified: '.gmdate('D, d M Y H:i:s \G\M\T', $this->image_mtime ));
		header('Cache-Control: max-age='.($this->cache_time).', public, pre-check='.($this->cache_time) );
		header("ETag: \"".md5($this->image_filename)."\"");
		$ext = strtolower(substr(strrchr($this->image_filename, '.'),1));
		switch($ext) {
			case "jpg":
			case "jpeg": $ext =  image_type_to_mime_type(IMAGETYPE_JPEG); break;
			case "gif": $ext =  image_type_to_mime_type(IMAGETYPE_GIF); break;
			case "png": $ext =  image_type_to_mime_type(IMAGETYPE_PNG); break;
		}
		header('Content-type: '. $ext);
		
		$output = $this->cache[$this->cache_key];
		
//		if(strpos($_SERVER['HTTP_USER_AGENT'], 'W3C_Validator')=== false && strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')!== false)
//		{
//			$output = gzencode($output);
//			header("Content-Encoding: gzip");
//		}
//		header('Content-Length: ' . strlen($output));
		echo $output;
		exit(0);
	}
}
