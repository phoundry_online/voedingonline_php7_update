<?php 
/**
* Module content handler
*
* Module content handler wordt geladen om info van een enkele module te tonen.
* Dus zonder pageframes, subsets, contentcontainers, frames en sitestructures
*
* @version	2.0.0 Complete overhaul
* @author	Christiaan Baartse <christiaan.baartse@webpower.nl>
*/
class ModuleContentHandler extends ContentHandler
{
	protected $use_cache = FALSE;

	public $pageType = null;
	public $module;
	public $auth;
	
	/**
	 * Construct the Content Handler
	 *
	 * @param array $path
	 * @param string $query_string
	 */
	public function __construct($path, $query_string)
	{
		parent::__construct($path, $query_string);
		$this->module = null;
		$this->auth = null;
		Framework::startSession();
	}
	
	public function __get($name)
	{
		switch ($name) {
			case '_args': return (array) $this->path; // Backwards compat
		}
	}
	
	public function init()
	{
		// @todo hoort dit niet gewoon in de WebRepresentation?
		// Assign Basic brickwork variables to smarty template (copy from PageContentHandler!)
		Framework::$representation->assign('SITE', Framework::$site);
		Framework::$representation->assign('PAGE', $this);
		Framework::$representation->assign('SITEVERSION', trim(@file_get_contents(CUSTOM_INCLUDE_DIR.'/VERSION')));
		
		
		// $this->path[0] is a unique page_content id of the module OR the classname OR code
		if (is_numeric($this->path[0])) {
			$this->module = Webmodule::getModuleByPageContentId($this->path[0]);
		} 
		
		if(is_null($this->module) && !empty($this->path[0])) {
			$this->module = @Webmodule::getModuleByClassName($this->path[0]);
			
			if (false === $this->module) {
				$this->module = null;
			}
			
			if(is_null($this->module)) {
				$this->module = Webmodule::getFakeModuleByCode($this->path[0]);
			}
		}
		
		/*
		if (is_null($this->module) || !($this->module instanceof Webmodule)) {
			trigger_error('Module cannot be loaded', E_USER_ERROR);
		}
		*/
		
		//@todo slaat allemaal helemaal nergens meer op, getModuleByPageContentId verwerkt en renderd de module namelijk al
		/*
		$this->auth = Auth::singleton();

		// vertel module welke contenthandler actief is
		$this->module->contentHandlerName = get_class($this);
		
		// Damn easy to fetch the site structure id that is related to the module!
		$res = DB::iQuery(sprintf("
			SELECT brickwork_page.site_structure_id
			FROM brickwork_page
			INNER JOIN brickwork_page_content
			ON brickwork_page_content.page_id = brickwork_page.id AND brickwork_page_content.id = %d
			",
			$this->module->id
		));
		if(!self::isError($res) && count($res) != 0) {
			$pageStructureItem = SiteStructure::getStructureItem(Framework::$site->identifier, $res->first()->site_structure_id);
			Framework::$representation->assign('STRUCTURE', $pageStructureItem );
		}
		
		Framework::$representation->assign('SITEVERSION', trim(@file_get_contents(CUSTOM_INCLUDE_DIR.'/VERSION')));
		*/
	}
	
	public function out($return = FALSE)
	{
		if (is_null($this->module) || !($this->module instanceof Webmodule)) {
			return $return ? false : $this->print404();
		}
		
		if ( !$this->_isAllowed($this->module))
		{
			if ( method_exists( $this->module, 'print403'))
			{
				return $this->module->print403();
			}
			else
			{
				return '';
			}
		}
		
		if($return) {
			return $this->module->html;
		} else {
			header(sprintf('Content-type: %s; charset=%s', Framework::$representation->mimeType, Framework::$PHprefs['charset']));
			echo $this->module->html;
		}
		
		/* @todo nergens voor nodig, de template en html is namelijk al lang verwerkt
		if ($this->use_cache){
			Framework::$representation->caching = 2;
		}

		// make representation aware of the page type template again
		Framework::$representation->load_filter('output', 'stack_html');


		// make representation aware of the page type template again
		Framework::$representation->cache_lifetime = -1;
		Framework::$representation->caching        = FALSE;
		Framework::$representation->setTemplate($this->module->template);

		if($return){
			return Framework::$representation->out($return);
		} else {
			Framework::$representation->out();
		}
		*/
	}
	
	/**
	 * Checks if the user may see this module based on the location of the module in the site structure
	 *
	 * @param 	Webmodule $module
	 * @return boolean
	 */
	protected function _isAllowed(Webmodule $module )
	{
		$hasACL = Features::brickwork('ACL');
		
		// Find the site structure the module is placed on
		$sql = sprintf(	'SELECT s.id as sitestructureid, p.id as pageid, s.secure as secure, %s as restricted ' .
						'  FROM brickwork_page_content pc ' .
						' INNER JOIN brickwork_page p ' .
						'    ON (p.id = pc.page_id ) ' .
 						' INNER JOIN brickwork_site_structure s '.
						'    ON (s.id = p.site_structure_id) ' .
 						'WHERE pc.id = %s',
						( $hasACL ? 's.restricted' : '0'), 
						$module->id );

		$rs = DB::iQuery($sql);
		
		if($row = $rs->first())  {
			$secure 	= $row->secure;
			$restricted = $row->restricted;
			
			// Check if the site structure is secure
			if($secure) {
				$auth = Auth::singleton();
				$user = $auth->user;
				
				if ($user->isLoggedIn()) {
					// Check if the site structure is restricted
					if($hasACL && $restricted) {
						$acl = ACL::singleton();
						return $acl->hasAccess( $user, $module );
					}
					return true;
				}
				return false;
			}
		}
		
		if($hasACL) {
			// Fallthrough, we don't know, so we use the ACL default policy
			if(!isset($acl)) {
				$acl = ACL::singleton();
			}
			return $acl->getPolicy();
		}
		return true;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
