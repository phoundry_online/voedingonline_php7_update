<?php
/**
 * Image Editor
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @author Bart Lagerweij <bart.lagerweij@webpower.nl>
 * 
 * @package ImageEditor
 * @version 0.0.5
 * 
 * For use in a project with brickwork < 1.4 you must rename the driver files
 * because of the updated autoload function:
 * 	classes/ImageEditor/Driver/Gd.class.php -> custom_classes/ImageEditor.Driver.class.php
 * 	classes/ImageEditor/Driver.class.php -> custom_classes/ImageEditor.Driver.Gd.class.php
 * 
 */

class ImageEditor
{

	// Master Dimension
	const NONE = 1;
	const AUTO = 2;
	const HEIGHT = 3;
	const WIDTH = 4;
	// Flip Directions
	const HORIZONTAL = 5;
	const VERTICAL = 6;

	// Allowed image types
	public static $allowed_types = array
	(
		'gif' => 'gif',
		'jpg' => 'jpg',
		'jpeg' => 'jpg',
		'png' => 'png',
	);
	
	// Driver instance
	protected $driver;

	// Driver actions
	protected $actions = array();

	// Image filename
	protected $image = '';
	
	public $type = '';
	
	/**
	 * Constructor
	 *
	 * @throws Exception
	 * @param string $filename
	 * @param string $type One of the types defined in Image_Editor::$allowed_types
	 * @param string $driver One of the drivers. To use ImageEditor_Driver_Gd use 'Gd' 
	 */
	public function __construct($filename, $type, $driver = 'Gd')
	{
		// Check to make sure the image exists
		if ( ! file_exists($filename))
			throw new Exception('Bestand bestaat niet:'.$filename);
			

	
			// Check to make sure the image type is allowed
		if ( ! isset(ImageEditor::$allowed_types[$type])) {	
			throw new Exception('Dit image type ('.$type.') wordt niet ondersteunt');
				
		}
		$this->type = ImageEditor::$allowed_types[$type];
		$this->image = str_replace('\\', '/', realpath($filename));

		try
		{
			file_put_contents("/var/www/voedingonline/er.txt", print_r("before creted", true));
			$driver_class = new ReflectionClass('ImageEditor_Driver_'.$driver);
			file_put_contents("/var/www/voedingonline/refl.txt", print_r("reflection", true));

			
			// Initialize the driver
			$this->driver = $driver_class->newInstance($this);
		}
		catch (Exception $e)
		{
			// Driver was not found
			throw new Exception('Driver '.$driver.' kon niet worden geladen');
		}

		if ( ! ($this->driver instanceof ImageEditor_Driver))
			throw new Exception('Driver is geen instantie van ImageEditor_Driver');

	}

	/**
	 * Resize the image to fit in the given dimensions
	 *
	 * @throws Exception
	 * @param integer $width
	 * @param integer $height
	 * @return ImageEditor
	 */
	public function resize($width, $height)
	{
		if($width == 0 || $height == 0)
			throw new Exception('Cannot resize an image to nothing');

		$this->actions['resize'] = array
		(
			'width'  => $width,
			'height' => $height,
		);

		return $this;
	}
	
	/**
	 * Resample the image to fit in the given dimensions
	 *
	 * @throws Exception
	 * @param integer $width
	 * @param integer $height
	 * @return ImageEditor
	 */
	public function resample($width, $height)
	{
		if($width == 0 || $height == 0)
			throw new Exception('Cannot resample an image to nothing');

		$this->actions['resample'] = array
		(
			'width'  => $width,
			'height' => $height,
		);

		return $this;
	}
	
	/**
	 * Cut out a given area in the image.
	 *
	 * @throws Exception
	 * @param integer $width
	 * @param integer $height
	 * @param integer/string $left Can be set to 'left', 'right', 'center' or an leftoffset integer
	 * @param integer/string $top Can be set to 'top', 'bottom', 'center' or an topoffset integer
	 * @return ImageEditor
	 */
	public function crop($width, $height, $left, $top)
	{
		if($width == 0 || $height == 0)
			throw new Exception('Cannot crop an image to nothing');
				
		$this->actions['crop'] = array
		(
			'width'  => $width,
			'height' => $height,
			'top'    => $top,
			'left'   => $left,
		);

		return $this;
	}
		
	/**
	 * Convert the image to greyscale
	 *
	 * @param boolean $fast
	 * @return ImageEditor
	 */
	public function greyscale()
	{
		$this->actions['greyscale'] = null;
		return $this;
	}
	
	/**
	 * Birghten the image 
	 *
	 * @param boolean $brightness
	 * @return ImageEditor
	 */
	public function brightness($brightness = 20)
	{
		$this->actions['brightness'] = $brightness;
		return $this;
	}
	
	/**
	 * Convert the image to sepia
	 *
	 * @param integer $darkIt
	 * @return ImageEditor
	 */
	public function sepia($darkIt = 15)
	{
		$this->actions['sepia'] = $darkIt;
		return $this;
	}
	
	/**
	 * Sharpen the image by the given amount
	 *
	 * @param int $amount
	 * @return ImageEditor
	 */
	public function sharpen($amount)
	{
		$this->actions['sharpen'] = $amount;
		return $this;
	}
	
	/**
	 * Flip the image in the given direction
	 *
	 * @param int $direction
	 * @return ImageEditor
	 */
	public function flip($direction = self::VERTICAL)
	{
		$this->actions['flip'] = $direction;
		return $this;
	}
	
	/**
	 * Rotate the image by a given amount of degrees
	 *
	 * @param int $amount
	 * @return ImageEditor
	 */
	public function rotate($amount = 0)
	{
		$this->actions['rotate'] = $amount;
		return $this;
	}
	
	/**
	 * Set quality for jpg output (0-100%) 
	 *
	 * @param int $quality
	 * @return ImageEditor
	 */
	public function jpg_quality($quality = 85)
	{
		$this->actions['jpg_quality'] = $quality;
		return $this;
	}
	
	/**
	 * Set compresson for png output (0-9)
	 * 0= no compression 
	 *
	 * @param int $compr
	 * @return ImageEditor
	 */
	public function png_compression($compr = 3)
	{
		$this->actions['png_compression'] = $compr;
		return $this;
	}
	
	/**
	 * Watermark 
	 *
	 * @param string $image_file path to file
	 * @param string/int $left 
	 * @param string/int $top
	 * @return ImageEditor
	 */
	public function watermark($image_file, $left, $top)
	{
		$this->actions['watermark'] = array
		(
			'image_file'  => $image_file,
			'top'    => $top,
			'left'   => $left,
		);
		return $this;
	}
	
	/**
	 * Save the image to a new image or overwrite this image.
	 *
	 * @throws Exception
	 * @param   string  new image filename
	 * @return  boolean Status of the execution of the actions
	 */
	public function save($new_image = FALSE, $new_type = NULL)
	{
		// If no new image is defined, use the current image
		empty($new_image) and $new_image = $this->image;
		empty($new_type) and $new_type = $this->type;

		// Separate the directory and filename
		$dir  = dirname($new_image).'/';
		$file = basename($new_image);

		if ( ! is_writable($dir)){
			throw new Exception('Image '.$file.' kan niet weggeschreven worden. '.$dir.' niet schrijfbaar.');
		}
		// Process the image with the driver
		$status = $this->driver->process($this->image, $this->type, $this->actions, $new_type, $dir, $file);

		// Reset the actions
		$this->actions = array();

		return $status;
	}
	
	/**
	 * The same as save, but instead outputs the image to the browser.
	 *
	 * @param string Output type
	 * @return boolean Status of the execution of the actions
	 */
	public function render($new_type = NULL)
	{
		empty($new_type) and $new_type = $this->type;
		$image = $this->driver->process($this->image, $this->type, $this->actions, $new_type);
		$this->actions = array();
		return $image;
	}
	
	/**
	 * Calculate the factor by which to devide the dimensions to fit in the new dimensions
	 *
	 * @param numeric $old_width
	 * @param numeric $old_height
	 * @param numeric $new_width
	 * @param numeric $new_height
	 * @return float
	 */
	public static function calcFactor($old_width, $old_height, $new_width, $new_height)
	{
		$factor_width = ($old_width / $new_width);
		$factor_height = ($old_height / $new_height);
	
		$factor = ($factor_width > $factor_height) ? $factor_width : $factor_height;
		
		return (float) $factor;
	}

	/**
	 * Fit the image to exactly fit in the given dimensions
	 *
	 * @todo: add top/left params?
	 * 
	 * @throws Exception
	 * @param integer $width
	 * @param integer $height
	 * @param bool $use_resample
	 * @return ImageEditor
	 */
	public function fit($width, $height, $use_resample=true)
	{
		if($width == 0 || $height == 0)
			throw new Exception('Cannot fit an image to nothing');

		$this->actions['fit'] = array
		(
			'width'  => $width,
			'height' => $height,
			'use_resample' => $use_resample
		);

		return $this;
	}
	
	
}

