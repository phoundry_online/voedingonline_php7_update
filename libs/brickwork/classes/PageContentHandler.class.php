<?php
/**
* Page content handler
*
* Handles the content selection and presentation for a page
*
* @package ContentHandler
* @version 1.1.4
* @author J�rgen Teunis <jorgen.teunis@webpower.nl>
* @access public
*/
class PageContentHandler extends ContentHandler {

	/**
	 * Session
	 *
	 * @var Array
	 */
	static private $_session = array();

	/**
	 * @var Auth
	 */
	private $auth;

	/**
	 * @var Page
	 */
	public $page;

	/**
	 * @var string Path of the current active structure
	 */
	public $structurePath;

	/**
	 * @var PageFactory
	 */
	private $page_factory;

	/**
	 * @var int
	 */
	private $test_structure_id;

	/**
	 * @var int
	 */
	private $test_page_id;

	public function __construct($path, $query)
	{
		if (!is_array($path)) {
			$path = explode("/", $path);
		}
		$path = array_values(array_filter($path));

		parent::__construct($path, $query);

		// start session if not already started
		Framework::startSession();

		$this->auth = Auth::singleton();

		// indien de pagina in testmodules wordt aangeroepen de instellingen hiervoor laden
		if (!empty($_GET['brickwork_test'])) {
			$params = BrickWorkCrypt::urlDecode($_GET['brickwork_test'], true);
			if ($params && isset($params['pid'], $params['sid'])) {
				self::$_session['Testmode'] = $params;
				$_SESSION[get_class($this)] = self::$_session;
			}
		}

		if (isset($_SESSION[get_class($this)]['Testmode'])) {
			$params = $_SESSION[get_class($this)]['Testmode'];
			$this->test_page_id = (int) $params['pid'];
			$this->test_structure_id = (int) $params['sid'];
		}

		$this->page_factory = new PageFactory(DB::getDb(), Framework::$site, $this,
			Framework::$clientInfo, $this->auth);

		if(Features::brickwork('ACL')) {
			// JIT loading (JIT HACK)
			if (!isset(Framework::$acl)) {
				Framework::$acl = ACL::singleton();
			}
			$this->page_factory->setAcl(Framework::$acl);
		}
	}

	public function startSession()
	{
		return false;
	}


	/**
	 * Checks the language for the current site
	 *
	 * @return Site_Language
	 */
	public function determineLanguage()
	{
		if(Framework::$site->isMultiLingual()) {
			if(!empty($this->path[0])) {
				$pl = $this->path[0];

				foreach(Framework::$site->availableLang as $lang) {
					if($lang->code == $pl) {
						$c = Cookie::factory('preferences');
						$c->l = $lang;
						$c->update();
						array_shift($this->path);
						return $lang;
					}
				}
			}

			$c = Cookie::factory('preferences');
			if(isset($c->l) && $c->l instanceof Site_Language) {
				trigger_redirect('/page/'.$c->l->code.'/'.
					implode("/",$this->path));
			}
			else {
				$l = parent::determineLanguage();
				trigger_redirect('/page/'.$l->code.'/'.
					implode("/",$this->path));
			}
		}

		return parent::determineLanguage();
	}

	/**
	 * Initializes the content handler
	 * @return void
	 */
	public function init()
	{
		if (isset($this->path[0]) && $this->path[0] === '404-Not-found') {
			$this->print404();
		}

		$structure_id = $this->determineStructureId($this->path);

		try {
			$home = SiteStructure::getHomeStructure(Framework::$site->identifier);
			$home_id = $home->id;
		}
		catch (Exception $e) {
			$home_id = $home = false;
		}

		// Geen matche gevonden maar wel een home_id
		if($structure_id === -1 && $home_id) {
			$structure_id = $home_id;
		}

		$active_structure = SiteStructure::activateId(Framework::$site->identifier, $structure_id);
		$pageStructureItem = SiteStructure::getStructureItem(Framework::$site->identifier, $structure_id);

		if ($pageStructureItem && $pageStructureItem->ssl && $_SERVER['SERVER_PORT'] != 443 &&
				false !== strpos(Framework::$site->protocol, 'https')) {
			$url = "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
			trigger_redirect($url);
		}

		if($pageStructureItem) {
			if($pageStructureItem->id === $this->test_structure_id && $this->test_page_id) {
				$page = $this->page_factory->pageByStructureItem($pageStructureItem, $this->test_page_id);
			}
			else {
				$page = $this->page_factory->pageByStructureItem($pageStructureItem);
			}
		}
		else {
			$page = false;
		}

		// set path for the current page to use safely in module templates
		$langprefix = Framework::$site->isMultiLingual() ?
			Framework::$site->currentLang->code . '/' : '';

		if($active_structure) {
			if(defined('BRICKWORK_BEAUTIFUL_URL') && BRICKWORK_BEAUTIFUL_URL === true) {
				$this->structurePath = '/page/'. $langprefix  . SiteStructure::getPathByStructureId($structure_id, Framework::$site->identifier, Framework::$site->currentLang);
			}
			else {
				$this->structurePath = '/index.php/page/'. $langprefix  . $structure_id;
			}
		}

		if (!$page) {
			// Check if the structure item is linked to an URL
			$urls = SiteStructure::getUrls(array($structure_id));
			if (isset($urls[$structure_id]) && isset($urls[$structure_id]['link'])) {
				// Redirect to the URL
				trigger_redirect($urls[$structure_id]['link'], 301);
			}

			// Check if the structure item is linked to another structure item
			$row = DB::iQuery(
				sprintf(
					"SELECT
						linked_to
					FROM
						brickwork_site_structure
					WHERE
						id = %d",
					$structure_id
				)
			)->first();

			if ($row && !empty($row->linked_to)) {
				trigger_redirect(SiteStructure::getFullPathByStructureId($row->linked_to));
			}
		}

		//var_dump($pageStructureItem);
		// PE: 2008-05-06: Checken of pagina bekeken mag worden met de huidige rechten
		$user = $this->auth->user;

		// Check if the user may see the page, or should just be plainly denied
		if ( is_object($pageStructureItem) && $pageStructureItem->restricted )
		{
			if ( $user->isLoggedIn() && !Framework::$acl->hasAccess( $user, $pageStructureItem ) )
			{
				if ( file_exists( $_SERVER['DOCUMENT_ROOT'] . '/403.php') )
					trigger_redirect('/403.php');

				header('HTTP/1.1 403 Forbidden');
				header('Status: 403 Forbidden',null,403);
				exit();
			}
		}

		if(!$page)
		{
			// print pretty error page from template root directory : 404.tpl
			$this->print404();
		}

		$this->page = $page;
	}



	/**
	 * Outputs the page
	 * Gets all the html from the modules and load it into the page type template.
	 * Handles module caching and output of the contents to the screen
	 *
	 * @param boolean $return return the contents or output
	 * @return mixed string(html) or void
	 */
	public function out($return = false)
	{
		if($return) {
			return $this->page->render(Framework::$representation, true);
		}
		else {
			header(sprintf('Content-type: %s; charset=%s', Framework::$representation->mimeType, Framework::$PHprefs['charset']));
			$this->page->render(Framework::$representation, false);
		}
	}

	/**
	 * Generates and output a 404 page
	 * this function generates a templated based 404 page located in the templates base dir
	 * for example: /templates/example_template/html/404.tpl
	 * If no custom 404.tpl found, then it will print a simple 404 Page not found message
	 *
	 */
	public function print404(){
		// make representation aware of the page type template again
		Framework::$representation->cache_lifetime = -1;
		Framework::$representation->caching = false;

		# log error to project tmp/error.log
		error_log(sprintf("[%s] [error] [client %s] File not found: %s%s\n", date("D, d M Y H:i:s T"), $_SERVER['REMOTE_ADDR'], $_SERVER['REQUEST_URI'], !empty($_SERVER['HTTP_REFERER']) ? ' Referer: '.$_SERVER['HTTP_REFERER'] : '' ), 3, TMP_DIR.'/error.log');
		header('HTTP/1.1 404 Not found');

		// if 404 found, then try to load structure item '404 Not found' otherwise load 404.tpl, if not exists print 404 message
		$loaded = false;
		$pageStructureItem = SiteStructure::getStructureByPath(array('404-Not-found'));

		if(!is_null($pageStructureItem)){
			$this->structure = SiteStructure::getStructureItem(Framework::$site->identifier, $pageStructureItem->site_structure_id);
			Framework::$representation->assign('STRUCTURE', $this->structure );
			$loaded = $this->page_factory->pageByStructureItem($this->structure);
		}
		if($loaded){
			$this->page = $loaded;
			$this->out();
		} else {
			Framework::$representation->assign('SITE', Framework::$site);
			Framework::$representation->assign('CLIENT', Framework::$clientInfo);
			Framework::$representation->setTemplate('404.tpl');
			Framework::$representation->out();
		}
		exit;
	}

	/**
	 * Determine the structure id based on the $path
	 *
	 * The returned id can be one of the following
	 * 0     = we give up.. load the 404
	 * -1    = load the homepage
	 * > 0   = We have a page
	 *
	 * @param array $path
	 * @return int
	 */
	private function determineStructureId(array $path)
	{
		$first_param = $path ?
			$path[0] :
			((isset($_SERVER['REDIRECT_STATUS']) &&
				$_SERVER['REDIRECT_STATUS'] == 404) ? false : -1);

		// @todo fixen dat als je crap opgeeft dat je dan naar 404 gaat, en niet naar de homepage (/page/jfsdgfsdh moet niet naar de home gaan)
		if(is_numeric($first_param)) {
			$structure_id = (int)$first_param;

			if(defined('BRICKWORK_BEAUTIFUL_URL_REDIRECT_OLD') &&
				BRICKWORK_BEAUTIFUL_URL_REDIRECT_OLD === true) {
				$struc_path = SiteStructure::getPathByStructureId($structure_id);

				// check if the path is different than the structure id given, in that case try redirect
				if($struc_path != $structure_id) {
					$foo = explode('/', $_SERVER['PHP_SELF']);
					if(isset($foo[2])) {
						$url = '/'.$foo[2].'/'.$struc_path;
						if(!empty($_SERVER['QUERY_STRING'])) {
							$url.= '?'.$_SERVER['QUERY_STRING'];
						}
						trigger_redirect($url, 301);
					}
				}
			}
			return $structure_id;
		}


		if(is_string($first_param)) {
			if($path) {
				// find the structure id by path
				$structure = SiteStructure::getStructureByPath($path);

				if($structure) {
					if($structure->archive) {
						$url = '/page/'.SiteStructure::getPathByStructureId(
							$structure->site_structure_id
						);
						if(!empty($_SERVER['QUERY_STRING'])) {
							$url.= '?'.$_SERVER['QUERY_STRING'];
						}
						trigger_redirect($url, 301);
					}
					return $structure->site_structure_id;
				}
				return 0; // was -1
			}
			return -1;
		}
		return 0;
	}
}
