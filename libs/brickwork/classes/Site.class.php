<?php
/**
 * Site
 *
 * Site class to identify given server request
 *
 * @package	Site
 * @version	0.5.1
 * @last_modified 10-7-2007 (MMS)
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @author	Mick van der Most van Spijk (MMS)
 */
class Site {

	/**
	* @var String Unique identifier
	*/
	public $identifier;

	/**
	* @var String name
	*/
	public $name;

	 /**
	* @var String Protocol (http/https)
	*/
	public $protocol = 'http';

	/**
	* @var String Hostname
	*/
	public $host;

	/**
	* @var String Template name
	*/
	public $template = 'default';

	/**
	* @var Boolean test
	*/
	public $test = FALSE;

	/**
	* @var String Locale
	*/
	public $locale = 'nl_NL';

	/**
	 * The current Language
	 *
	 * @var Site_Language
	 */
	public $currentLang;

	/**
	 * The default Language for the website (if any)
	 *
	 * @var Site_Language
	 */
	public $defaultLang;

	/**
	 * Available languages
	 *
	 * @var	Array
	 */
	public	$availableLang;

	/**
	* @var Organisation Organsiation object
	*/
	public $organisation;

	/**
	* @var String default auth user classname
	*/
	public $auth_user_class = 'AuthUser';

	private	$_configAttribute;
	private $_configAltered;

	/**
	* Constructor
	*
	* @access	public
	* @param		String site identifier
	* @param		String host
	*/
	public function __construct($identifier, $host){
		$this->identifier		= $identifier;
		$this->host				= $host;
		$this->_configAttribute = $this->_loadCachedConfig();
		$this->_configAltered	= false;
		$this->defaultLang		= null;
		$this->currentLang		= null;
		$this->availableLang	= Array();
	}

	public function setLanguage( Site_Language $l )
	{
		$this->currentLang = $l;
		$this->setLocale( $l->locale );
		$GLOBALS['translations'] = Translation::getBySiteLanguage( $l );
	}

	/**
	* Locale settings of site
	* Set the locale sitewide according to locale definition: nl_NL, be_FR, be_NL
	*
	* @param String locale
	*/
	public function setLocale($l) {
	    if ($l == null)
	        $l = "nl_NL";
		setlocale(LC_ALL, $l);
		$this->locale = getlocale(LC_ALL);
	}

	public function setTemplate( $template )
	{
		$this->template 	= $template;
	}

	/**
	 *
	 */
	public function __destruct()
	{
		if ( defined('USE_CONFIG_CACHE') && USE_CONFIG_CACHE && $this->_configAltered )
		{
			Cache::factory('dir:///site_config')->setValue($this->identifier, $this->_configAttribute);
		}

	}

	/**
	 * Wrapper for the getConfig method
	 *
	 * @param 	string $var
	 * @return 	mixed
	 */
	public function __get( $var )
	{
		return $this->getConfig($var);
	}

	/**
 	 * Get Site specific configuration values
 	 * This functions allows you to access all configuration attributes that were
 	 * stored in the site_config table.
 	 *
 	 * @param 	string $var		The name of the config attribute you want to get
 	 * @param	mixed $default	The value thats returned if config is not found
 	 * @return 	mixed
 	 */
	public function getConfig($var, $default = null)
	{
		if(array_key_exists($var, $this->_configAttribute)) {
			return $this->_configAttribute[ $var ];
		}

		$value = self::getConfigForSite($this->identifier, $var);

		if($value !== FALSE) {
			$this->_configAttribute[$var] = $value;
			$this->_configAltered = true;
			return $this->_configAttribute[$var];
		}

		return $default;
	}

	/**
	 * Get a config attribute for a given site identifier
	 *
	 * @param String $site_identifier
	 * @param String $attribute
	 * @return Mixed value or FALSE in case attribute not exists
	 */
	public static function getConfigForSite($site_identifier, $attribute){
		$rs = DB::iQuery('SELECT value ' .
						'  FROM brickwork_site_config ' .
						" WHERE attribute = '" . DB::quote($attribute) .
						"'  AND site_identifier = '" . DB::quote($site_identifier) . "'");

		if ( self::isError($rs) ){
			ErrorHandler::singleton()->log(E_USER_WARNING, sprintf('site configuration error: %s', $rs->errstr), __FILE__, __LINE__ );
		}

		if ( $rs->num_rows > 0 )
		{
			return $rs->first()->value;
		} else {
			return FALSE;
		}
	}

	/**
	 * Checks if there is a language which matches the locale given
	 *
	 * @param 	string $langPart
	 * @return Site_Language|false
	 */
	public function hasLanguage( $langPart )
	{
		// brickwork system wide uses locale with underscore, make sure we check browser input the same:
		$langPart = str_replace('-','_', $langPart);
		if ( !is_null($this->defaultLang) )
		{
			foreach ( $this->availableLang as $lang )
			{
				if ($lang->locale && strtolower(substr($lang->locale ,0,strlen($langPart))) == strtolower($langPart) )
				{
					return $lang;
				}
			}
		}
		return false;
	}

	/**
	 * @return Site_Language
	 */
	public function getLang()
	{
		if (isset($this->currentLang)) {
			return $this->currentLang;
		}
		return $this->defaultLang;
	}

	/**
	 * Checks if the current site is a multilingual site
	 *
	 * @return boolean
	 */
	public function isMultiLingual()
	{
		return (Features::brickwork('translation') && !is_null($this->defaultLang) && count($this->availableLang) > 1);
	}

	public function __sleep()
	{
		$vars = get_object_vars( $this );
		unset($vars['_configAttribute']);
		unset($vars['_configAltered']);
		return array_keys($vars);
	}

	/**
	 * Restores object config variables
	 * Site objects are stored in a serialized manner for performance. But we do not
	 * want to rely on that for our config attributes.
	 *
	 */
	public function __wakeup()
	{
		$this->_configAttribute = $this->_loadCachedConfig();
		$this->_configAltered   = false;
	}

	/**
	 * Retrieves cached configuration values
	 * To increase performance, we built in an option to cache all configuration
	 * values. This option is switchable from within the PREFS file by setting
	 * the USE_CONFIG_CACHE define.
	 *
	 * @return Array
	 */
	private function _loadCachedConfig()
	{
		$configAttribute = array();

		if ( defined('USE_CONFIG_CACHE') && USE_CONFIG_CACHE )
		{
			$configAttribute = Cache::factory('dir:///site_config')->getValue($this->identifier);
			if(!is_array($configAttribute)) $configAttribute = array();
		}

		return $configAttribute;
	}

	/**
	 * Get the url for the site
	 * @param bool $secure should the url start with https
	 * @return string|bool
	 */
	public function getUrl($secure = false)
	{
		$protocols = explode(',', $this->protocol);
		if ($secure && !in_array('https', $protocols)) {
			return false;
		}
		$preferred = $secure ? 'https' : 'http';
		if (in_array($preferred, $protocols)) {
			$protocol = $preferred;
		}
		else {
			$protocol = reset($protocols);
		}

		if ($protocol) {
			return $protocol.'://'.$this->host;
		}
		return false;
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
