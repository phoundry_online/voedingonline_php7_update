<?php
/**
 * AvoidForm
 *
 * Koppeling van de Avoid Formbuilder in Phoundry met het Formulierscript in
 * de module structuur.
 *
 * @author			Jurriaan Nijkamp <jurriaan@webpower.nl>
 * @version			1.0.1
 * @last_modified	8-5-2007 Mick
 *
 * @package			Brickwork
 * @subpackage		AvoidForm
 */
class AvoidForm extends Form
{
	/**
	 * Title of the form
	 *
	 * @var	String
	 */
	protected $title_ = '';

	/**
	 * Constructor
	 *
	 * @param	String	$name
	 * @param	String	$action
	 * @param	String	$method
	 * @return	void
	 */
	public function __construct($name, $action, $method=FormMethod::post)
	{
		parent::__construct($name, $action, $method);
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Form#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'title':
				$this->title_ = $value;
				break;

			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Form#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'title':
				return $this->title_;
				break;

			default:
				try
				{
					$val = parent::__get($name);

					if ( is_array($val) )
						return (array)$val;
					return $val;
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}

	/**
	 * load form definitions and values
	 *
	 * @return	Boolean|void
	 */
	public function load()
	{
		// laad alle velden in
		$fields = $this->_get_avoid_fields();
		$this->fields = $this->_get_form_fields($fields);

		// on load form event(s)
		if(!$this->onLoad())
		{
			return FALSE;
		}
	}

	/**
	 * Initieer de formuliervelden vanuit de database
	 *
	 * @return	Array
	 */
	private function _get_avoid_fields()
	{
		$fields = array();

		$sql = sprintf("
			SELECT
				elements
			FROM
				brickwork_avoid_formbuilder
			WHERE
				form_code = '%s'
			LIMIT	1
		",
			DB::quote($this->name)
		);

		$result = DB::iQuery($sql);

		# geen formulier gevonden
		if (!$result->current())
		{
			return $fields;
		}

		$row = $result->first();
		$elements = unserialize($row->elements);

		for ($i = 0, $j = count($elements['text']); $i < $j; ++$i)
		{
			// Voeg alleen fileupload veld toe indien de upload map bestaat
			if ($elements['type'][$i] != 'fileupload' || ($elements['type'][$i] == 'fileupload' && ! is_null($this->upload_folder)))
			{
				// maak nieuwe velden aan voor formulier
				$af = new AvoidField($elements['text'][$i], $elements['name'][$i], $elements['type'][$i], $elements['syntax'][$i], (isset($elements['required'][$i]) && $elements['required'][$i] == 1), $elements['info'][$i], $elements['extra'][$i]);
				$af->form = $this;

				$fields[] = $af;
			}
		}

		return $fields;
	}

	/**
	 * Laadt formvelden op basis van de velddefinities
	 *
	 * @param	Array	$fields
	 * @return	Array
	 */
	private function _get_form_fields($fields)
	{
		/**
		* Return container
		*/
		$form_fields = array();

		foreach ($fields as $field)
		{
			if (class_exists($field->type))
			{
				$form_field = new $field->type($field);

				// inherit
				$form_field->disabled		= $field->disabled;
				$form_field->active			= $field->active;
				$form_field->label			= $field->label;
				$form_field->prio			= $field->prio;
				$form_field->required		= $field->required;
				$form_field->info			= $field->info;

				// indien extra is gezet dan graag default value zetten
				if (isset($field->extra['value']) && $field->extra['value'] != '')
				{
					if ($field->syntax == 'date')
					{
						$value = new BrickDateTime(); // @since 21-6-2007

						// veld is altijd ingevuld in de nederlandse syntax, maar presentatie mag in meerdere talen
						$parts = explode('-', $field->extra['value']);

						$value->day = $parts[0];
						$value->month = $parts[1];
						$value->year = $parts[2];
					}
					else
					{
						$value = $field->extra['value'];
					}

					$form_field->defaultValue = $value;
				}
				else
				{
					$form_field->defaultValue = '';
				}

				// Checkboxertje
				if ($form_field instanceof CheckboxField && isset($field->extra['value']))
				{
					$option = new FieldOption($field->extra['value']);
					if ($field->extra['checked'])
					{
						$option->selected = TRUE;
					}

					$form_field->options = array($option);

				// Options uit de extra-array toevoegen voor een SelectField
				}
				elseif ($form_field instanceof SelectField && isset($field->extra['options']))
				{
					$options = array();

					$tmp = $field->extra['options'];
					sort($tmp);
					foreach ($tmp as $name => $value)
					{
						$options[] = new FieldOption($name, $value);
					}

					$form_field->options = $options;
				}

				if (isset($field->extra['multiple'])
					&& $field->extra['multiple']!='false')
				{
					$form_field->multiplicity = '+';
				}

				$form_fields[] = $form_field;

			}
			else
			{
				trigger_error('Unknown field-type after check? Someone must have set us up the bomb!', E_USER_ERROR);
			}
		}
		return $form_fields;
	}

	/**
	 * Vang de submit af
	 *
	 * @param	Array	$request
	 * @return	Boolean
	 */
	public function handleRequest($request)
	{
		// before handle request events
		if(!$this->onBeforeHandleRequest())
		{
			return FALSE;
		}

		// handle request himself
		if(!parent::handleRequest($request))
		{
			return FALSE;
		}

		// after handle request events
		if(!$this->onAfterHandleRequest())
		{
			return FALSE;
		}

		// handle request AND events succeeded, so we are happy ;)
		return TRUE;
	}
}

/**
* Avoid field
*
* Base class for fields in an AvoidForm
*
* @package		Brickwork
* @subpackage	AvoidForm
*/
class AvoidField extends Field
{
	/**
	 * Type of the field
	 *
	 * @var	String
	 */
	private $_type;

	/**
	 * Syntax of the field (subtype)
	 *
	 * @var	String
	 */
	private $_syntax;

	/**
	 * Extra data related to the field
	 * @var	Array
	 */
	private $_extra = array();

	/**
	 * Constructor
	 *
	 * @param	String	$label
	 * @param	String	$name
	 * @param	String	$type
	 * @param	String	$syntax
	 * @param	Boolean	$required
	 * @param	String	$info
	 * @param	Array	$extra
	 * @return	void
	 */
	public function __construct($label, $name, $type, $syntax, $required, $info, $extra)
	{
		if (empty($label))
		{
			trigger_error('Label empty.', E_USER_ERROR);
		}

		if (empty($name))
		{
			trigger_error('Name empty.', E_USER_ERROR);
		}

		if (empty($type))
		{
			trigger_error('Type empty.', E_USER_ERROR);
		}

		$this->label	= $label;
		$this->name		= $name;
		$this->_syntax	= $syntax;
		$this->required	= (bool)$required;
		$this->info		= $info;

		if (!empty($extra))
		{
			eval('$extra = ' . $this->_js2php($extra) . ';');
			if(isset($extra['value']) && !is_null($extra['value']))
			{
				$extra['value'] = urldecode($extra['value']);
			}
			$this->_extra = $extra;
		}

		// indien in form instelling requestvars zijn gezet, eval de value dan zodat we prefil gegevens kunnen doorgeven
		if (isset($this->_extra['value']))
		{
			if (strstr($this->_extra['value'], '$_REQUEST'))
			{
				// indien waarde bestaat dan toekennen anders lege string
				eval('$value = isset('.$this->_extra['value'].') ? '.$this->_extra['value'].' : "";');
				$this->_extra['value'] = $value;
			}
		}

		$this->_type = $this->_find_type($type);
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Field#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			default:
				try
				{
					parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Field#__get()
	 */
	public function __get($name)
	{
		switch ($name) {
			case 'type' :
				return $this->_type;

			case 'syntax' :
				return $this->_syntax;

			case 'extra' :
				return (array)$this->_extra;

			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}

	/**
	 * Zet avoid-type om naar Form-type
	 *
	 * @param	String	$type
	 * @return	String
	 */
	private function _find_type($type)
	{
		switch ($type)
		{
			case 'submit':
			case 'reset':
				return 'ButtonField';

			case 'checkbox':
				return 'CheckboxField';

			case 'radio':
				return 'RadioField';

			case 'select':
				return 'SelectField';

			case 'mailselect':
				return 'MailSelectField';

			case 'password':
				return 'PasswordField';

			case 'hidden':
				return 'HiddenField';

			case 'textarea':
				return 'TextareaField';

			case 'freetext':
				return 'freetextField';

			case 'captcha':
				return 'CaptchaField';

			case 'dmd_subscribe':
				return 'DMDSubscribeField';

			case 'fileupload':
				return 'FileUploadField';

			case 'text':
				switch ($this->_syntax)
				{
					case 'email':
						return 'EmailField';

					case 'integer':
					case 'numeriek':
						return 'NumberField';

					case 'date':
						return 'DateField';

					case 'zipcode':
						return 'PostalCodeField';
					/*
						Activeren zodra CompositeField helemaal af is
						case 'zipcode' : return 'PostalCodeField';

						Nog aanpassen dat ie DD-MM-YYYY aan kan?
						case 'date' : return 'DateField';
					*/
				}

				return 'TextField';

			default:
				trigger_error(sprintf('Fieldtype %s not supported, defaulting to textfield', $type), E_USER_NOTICE);
		}
	}

	/**
	 * Zet extra avoid-gegevens om :/
	 *
	 * @param	String	$struct
	 * @return	String
	 */
	private function _js2php($struct) {
		$struct = str_replace('{', 'array(', $struct);
		$struct = str_replace('}', ')', $struct);

		// anders worden : in de values vervangen
//		$struct = str_replace(':', '=>', $struct);
		$struct = preg_replace('/\':/', '\' => ', $struct);

		return $struct;
	}
}
