<?php
/**
 * Auth_Session
 *
 * Session handling for authentication
 * This class is used by Auth for handling all the session stuff needed for
 * user authentication.
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @version 1.1.0
 */
class Auth_Session
{
	/**
	 * The logged in user
	 * @var AuthUser
	 */
	protected $_auth_user;

	/**
	 * Construct automatically loads the user back from the session
	 */
	public function __construct(AuthUser $user = null)
	{
		Framework::startSession();

		if(is_null($user)) {
			$user = isset($_SESSION['Auth']['User']) ? self::gzunserialize($_SESSION['Auth']['User']) : false;
		}

		$this->put(
			($user instanceof AuthUser)
			? $user
			: new AnonymousUser()
		);
	}

	/**
	 * When the object is destroyed, save the object in the session
	 * @deprecated
	 */
	public function __destruct()
	{

	}

	/**
	 * Set user in session
	 * @param AuthUser $user
	 */
	public function put(AuthUser $user)
	{
		$this->_auth_user = $user;
		if(!isset($_SESSION['Auth'])) {
			$_SESSION['Auth'] = array();
		}
		$_SESSION['Auth']['User'] = self::gzserialize($user);
	}

	/**
	 * Get user from session
	 * @return AuthUser

	 */
	public function get()
	{
		return $this->_auth_user;
	}

	/**
	 * Destroy session and user
	 * @return bool always true
	 */
	public function destroy()
	{
		$this->_auth_user = null;

		if (isset($_SESSION['Auth']['User'])) {
			unset($_SESSION['Auth']['User']);
		}

		return true;
	}

	/**
	 * Unpack user object by unzipping and unserializing it
	 * @param string
	 * @return AuthUser|null
	 */
	protected static function gzunserialize($gs_user)
	{
		// try uncompressing
		if (false !== ($s_user = gzuncompress($gs_user))) {
			if (Auth_Session::is_serialized($s_user) && false !== ($user = @unserialize($s_user))) {
				if($user instanceof AuthUser) {
					return $user;
				}
			}
		}
		return null;
	}

	static public function is_serialized($data, $strict = true)
    {
        // if it isn't a string, it isn't serialized.
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace = strpos($data, '}');
            // Either ; or } must exist.
            if (false === $semicolon && false === $brace)
                return false;
            // But neither must be in the first X characters.
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            // or else fall through
            case 'a' :
            case 'O' :
                return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }

	/**
	 * Pack user using gzip and serialize
	 * @param AuthUser $user
	 * @return string|false
	 */
	protected static function gzserialize(AuthUser $user)
	{
		if (false !== ($s_user = @serialize($user))) {
			if (false !== ($gs_user = gzcompress($s_user))) {
				return $gs_user;
			}
		}
		return false;
	}
}
