<?php
/**
 * Class for authentication errors
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 * @since 2009-07-15
 * @version 0.0.1
 */
class Auth_Exception extends Exception {}
