<?php
// Raare if else, maar zo werkt de search en replace nog
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
}
else {
	define ('HOST', $PHprefs['DBserver']);
	define ('USER', $PHprefs['DBuserid']);
	define ('PASS', $PHprefs['DBpasswd']);
	define ('DBNAME', $PHprefs['DBname']);
	define ('DBCOLLATION', $PHprefs['charset']);
}

if(!class_exists('ResultSet')) {
	require_once('ResultSet.class.php');
}
if(!class_exists('DB_ResultSet')) {
	require_once('DB/ResultSet.php');
}
if (!interface_exists('DB_Adapter')) {
	require_once 'DB/Adapter.php';
}
if (!class_exists('DB_Mysql')) {
	require_once 'DB/Mysql.php';
}
if(!class_exists('Error')) {
	require_once('Error.class.php');
}
if(!class_exists('Utility')) {
	require_once('Utility.class.php');
}

/**
 * DB Wrapper for MySQL
 *
 * @version 1.9.9
 * @author Mick <mick.vandermostvanspijk@webpower.nl>
 * @author Wilbert <wilbert.verplanke@webpower.nl>
 * @author J�rgen <jorgen.teunis@webpower.nl>
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @modified 2010-01-20 11:00
 * @package DB
 */
class DB
{
	const MYSQL_DATEFORMAT = 'Y-m-d H:i:s';
	const MYSQL_STRFFORMAT = 'Y-m-d H:i:s';

	/**
	 * Connection resources
	 * @var array
	 */
	protected static $_connections = array();

	/**
	 * Query count
	 * @var Integer
	 */
	protected static $_query_count = 0;

	/**
	 * Total query time elapsed
	 * @var float
	 */
	protected static $_total_time_elapsed = -1;

	/**
	 * Create static connection with a database
	 *
	 * @param Integer connection number
	 * @param String host
	 * @param String username
	 * @param String password
	 * @param String database name
	 * @param String collation
	 * @return DB_Mysql|Error
	 */
	public static function connect_db($connection = 1, $host = null, $user = null, $pass = null, $db = null, $collation = null, $port = null)
	{
		if (!isset(self::$_connections[$connection])) {
			global $PHprefs, $WEBprefs; // global is dirty, maar Framework::$PHprefs is not niet bekend bij db connectie
			$host		= Utility::coalesce($host, Utility::arrayValue($WEBprefs, 'DBserver'), Utility::arrayValue($PHprefs, 'DBserver'));
			$user		= Utility::coalesce($user, Utility::arrayValue($WEBprefs, 'DBuserid'), Utility::arrayValue($PHprefs, 'DBuserid'));
			$pass		= Utility::coalesce($pass, Utility::arrayValue($WEBprefs, 'DBpasswd'), Utility::arrayValue($PHprefs, 'DBpasswd'));
			$db			= Utility::coalesce($db, Utility::arrayValue($WEBprefs, 'DBname'), Utility::arrayValue($PHprefs, 'DBname'));
			$port		= Utility::coalesce($port, Utility::arrayValue($WEBprefs, 'DBport'), Utility::arrayValue($PHprefs, 'DBport'), 3306);
			$collation	= Utility::coalesce($collation, Utility::arrayValue($WEBprefs, 'charset'), Utility::arrayValue($PHprefs, 'charset'));

			// configurabel maken en dit als default
			$connect = new DB_Mysql();
			$status = $connect->connect($host, $user, $pass, $db, (int) $port);

			if($status instanceof Error) {
				return $status;
			}

			$connect->setCollation($collation);
			self::$_connections[$connection] = $connect;
		}

		return self::$_connections[$connection];
	}

	/**
	 * Create static connection with a database while using the Identifier name from the ExtraDBConnections array in the PREFS.php
	 *
	 * @param String Connection Identifier
	 * @param String DB Collation default DBCOLLATION (PREFS -> charset)
	 * @return DB_Mysql|ErrorVoedingonline
	 */
	public static function connect_db_by_id($connection, $collation = null)
	{
		global $PHprefs, $WEBprefs; // global is dirty, maar Framework::$PHprefs is not niet bekend bij db connectie
		$collation	= Utility::coalesce($collation, Utility::arrayValue($WEBprefs, 'charset'), Utility::arrayValue($PHprefs, 'charset'));

		// check for extra db connections
		if(!isset($PHprefs['ExtraDBConnections']) || count($PHprefs['ExtraDBConnections']) == 0){
			return new ErrorVoedingonline(E_USER_ERROR, 'ExtraDBConnections not found in PHprefs');
		}

		// check for connection info
		if(!isset($PHprefs['ExtraDBConnections'][$connection])){
			return new ErrorVoedingonline(E_USER_ERROR, 'Connection info for '.$connection.' not found in ExtraDBConnections');
		}

		if (!isset(self::$_connections[$connection])) {
			$host = $PHprefs['ExtraDBConnections'][$connection]['DBserver'];
			$user = $PHprefs['ExtraDBConnections'][$connection]['DBuserid'];
			$pass = $PHprefs['ExtraDBConnections'][$connection]['DBpasswd'];
			$db	  = $PHprefs['ExtraDBConnections'][$connection]['DBname'];
			$port = $PHprefs['ExtraDBConnections'][$connection]['DBport'];

		 	return self::connect_db($connection, $host, $user, $pass, $db, $collation, $port);
		}
		return self::$_connections[$connection];
	}

	/**
	 * Fetches a configured connection by its identifier if given null
	 * the default database connection is used
	 *
	 * @throws DB_Exception
	 * @param string $connection Default the default db connection
	 * @return DB_Adapter
	 */
	public static function getDb($id = 1)
	{
		$id = (int) $id;
		if(!array_key_exists($id, self::$_connections)) {
			global $PHprefs, $WEBprefs;
			// 1 is always the default db connection
			if(1 === $id) {
				$connect = self::connect_db();
			}
			else {
				if(!isset($PHprefs['ExtraDBConnections'][$id]) ||
					!is_array($PHprefs['ExtraDBConnections'][$id])) {
					throw new InvalidArgumentException('Extra database '.
						'connection '.$id.' is not declared in '.
						'$PHprefs["ExtraDBConnections"]');
				}
				$conf = $PHprefs['ExtraDBConnections'][$id];

				$host = Utility::arrayValue($conf, 'DBserver');
				$user = Utility::arrayValue($conf, 'DBuserid');
				$pass = Utility::arrayValue($conf, 'DBpasswd');
				$db = Utility::arrayValue($conf, 'DBname');
				$port = Utility::arrayValue($conf, 'DBport', 3306);

				if(!isset($host, $user, $pass, $db, $port)) {
					throw new InvalidArgumentException('Extra database '.
						'connection '.$id.' is missing some options');
				}

				$connect = self::connect_db($id, $host, $user, $pass, $db, null, $port);
			}

			if($connect instanceof Error) {
				throw new DB_Exception($connect->errstr, $connect->errno);
			}
		}

		return self::$_connections[$id];
	}

	/**
	 * Set correct collation
	 *
	 * @param String collation
	 * @param Integer connection number
	 * @return Mixed resource or ErrorVoedingonline
	 */
	public static function setCollation($collation, $connection = 1)
	{
		try {
			return self::getDb($connection)->setCollation($collation);
		}
		catch(InvalidArgumentException $e) {
			return new ErrorVoedingonline(sprintf('Collation %s not known', $collation));
		}
	}

	/**
	 * Query the database with INSERT, DELETE, SELECT and UPDATE statements
	 *
	 * @deprecated
	 * @param string 	$query
	 * @param integer 	$connection
	 * @return DB_ResultSet|MySQLError or ErrorVoedingonline
	 */
	public static function query($query, $connection = 1)
	{
		$connection = self::getDb($connection);
		if($connection instanceof DB_Mysql) {
			try {
				return $connection->query($query);
			}
			catch(DB_Exception $e) {
				// DB_Exception is thrown if the connection is invalid
				// fall trough to the last return
			}
		}
		return new ErrorVoedingonline(E_USER_ERROR,'Could not connect to the database in DB::Query');
	}

	/**
	 * Improved query
	 * @throws Exception
	 * @param string $sql
	 * @param int $connection
	 * @return DB_ResultSet
	 */
	static public function iQuery($sql, $connection = 1)
	{
		$connection = self::getDb($connection);
		if($connection instanceof DB_Mysql) {
			return $connection->iQuery($sql);
		}
		throw new DB_Exception("No database connection");
	}

	/**
	 * Fetches a single row from a query
	 * This method will run a query and obtain the first row if any were
	 * found. If the query yielded no results, it will return NULL.
	 * In case the query contained an error, it will return the Error object generated by self::query
	 *
	 * @param string 	$sSql
	 * @param integer 	$iConn
	 * @return stdObject or Error
	 */
	static public function querySingleResult( $sSql, $iConn = 1 )
	{
		$rs = self::query($sSql, $iConn );
		if ( self::isError($rs))
		{
			return $rs;
		}
		return ($rs->num_rows > 0) ? first($rs->result) : null;
	}

	/**
	 * Get total time used for all queries on all databases
	 *
	 * @return float
	 */
	public static function getTotalQueryTime()
	{
		$time = (float) 0;
		foreach(self::$_connections as $conn) {
			$time += $conn->getTotalQueryTime();
		}
		return $time;
	}

	/**
	 * Get number of queries executed on all databases on this run
	 *
	 * @return integer number of queries
	 */
	public static function getQueryCount()
	{
		$count = 0;
		foreach(self::$_connections as $conn) {
			$count += $conn->getQueryCount();
		}
		return $count;
	}

	/**
	 * Escape a SQL statement according to the first connections charset
	 *
	 * @param string $str Text to escape
	 * @param bool $quote Also quote the value
	 * @return string
	 */
	public static function quote($str, $quote = false)
	{
		return self::getDb()->quote($str, $quote);
	}

	/**
	 * Improved DB#quote
	 *
	 * @param mixedvar $value
	 * @return string|array
	 */
	public static function quoteValue($value)
	{
		return self::getDb()->quoteValue($value);
	}

	/**
	 * Create Insert Into statement
	 *
	 * @param array $rows Multi deminsional array
	 * @param string $table
	 * @return array Insert id's
	 */
	public static function insertRows(array $rows, $table)
	{
		return self::getDb()->insertRows($rows, $table);
	}

	/**
	 * Updates the given rows
	 *
	 * @param array $rows Multi deminsional array
	 * @param string $table
	 * @param array $match_colums Columns to match on
	 * @return bool
	 */
	public static function updateRows(array $rows, $table, array $match_colums = array('id'))
	{
		return self::getDb()->updateRows($rows, $table, $match_colums);
	}

	/**
	 * Gets a named lock
	 * @see http://dev.mysql.com/doc/refman/5.0/en/miscellaneous-functions.html#function_get-lock
	 * @param string $name Name of the lock
	 * @param int $timeout Timeout in seconds
	 * @return bool If the lock was succesfully obtained
	 */
	public static function getLock($name, $timeout = 10)
	{
		return self::getDb()->getLock($name, $timeout);
	}

	/**
	 * Releases a named lock
	 * @see http://dev.mysql.com/doc/refman/5.0/en/miscellaneous-functions.html#function_release-lock
	 * @param string $name Name of the lock
	 * @return bool Always true
	 */
	public static function releaseLock($name)
	{
		return self::getDb()->releaseLock($name);
	}

	/**
	 * Check if a lock is set and by whom
	 * @param string $name
	 * @return bool|int The connection identifier that has the lock
	 */
	public static function isLocked($name)
	{
		return self::getDb()->isLocked($name);
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
