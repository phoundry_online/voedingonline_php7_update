<?php
/**
 * Hash a mixed variable
 *
 * Hash multi-dimensional arrays, objects or just strings
 * @author: J�rgen Teunis
 */
class brickHash
{
	/**
	 * get a has from a mixed variable
	 *
	 * @param mixed $mixed
	 * @return string sha1 hashed
	 */
	public static function get($mixed)
	{
		$hash = array();
		$arr = Utility::arrayFlatten($mixed, 2);

		foreach($arr as $str)
		{
			if ($str) {
				$hash[] = md5($str);
			}
		}
		
		return sha1(implode('_',$hash));
	}
}
