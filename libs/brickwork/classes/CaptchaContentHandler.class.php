<?php
if(!defined('FONT_DIR')) {
	define('FONT_DIR', is_dir(PROJECT_DIR.'/fonts/') ? PROJECT_DIR.'/fonts/' : BRICKWORK_DIR.'/fonts/');
}
/**
 * Captcha Content Handler
 * 
 * Vervangt de oude captcha.php in de htdocs/scripts directory
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @author Adrie den Hartog <adrie@webpower.nl>
 *
 */
class CaptchaContentHandler extends ContentHandler
{
	
	private $captchaObj;

	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
		
		Framework::startSession();
		
		$type = (!empty($_GET['type'])) ? $_GET['type'] : 'CaptchaField';		
		switch($type){
			case 'CaptchaField':
				$field = !empty($_GET['field_id']) ? $_GET['field_id'] : '';
				if (isset($_GET['regenerate']) || empty($_SESSION[$type][$field])){
					$_SESSION[$type][$field] = Captcha::stringGen(6);
				}
				$string = $_SESSION[$type][$field];
				$width = 30;
			break;
			default:
				$string = 'invalid type';
		}
		//$_SESSION['CaptchaField'][$field] = base64_encode(serialize($captchaObj));
		
		$this->captchaObj = new Captcha($string, 6, 'png', 'trebuc.ttf', $width);
		$this->captchaObj->setImageType('png');
		
		if (isset($_GET['bg']))
			$this->captchaObj->setBackgroundColor($_GET['bg']);
		
		if (isset($_GET['c']))
			$this->captchaObj->setForegroundColor($_GET['c']);
	}
	
	public function init(){
		print $this->captchaObj->get();
		exit(0);
	}
	
}
