<?php
/**
* P3P content handler
*
* Handles the basic p3p policy certificate
*
* @package ContentHandler
* @version 1.0.0
* @author J�rgen Teunis <jorgen.teunis@webpower.nl>
* @access public
* @TODO make the policy and certificate file customizable for every site. This is just basic info to fool IE :)
*/
class P3PContentHandler extends ContentHandler
{
	protected $use_cache		= FALSE;
	protected $cache_string = '';

	private $name = 'P3PContentHandler';
	private $kind = 'policy';
	
	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
	}

	public function init()
	{
		$this->kind = isset($this->path[0]) ? $this->path[0] : 'policy';
		Framework::$representation->assign('SITE', Framework::$site);

	}

	public function out($return = FALSE)
	{
		switch($this->kind){
			case 'certificate':
				$xml = sprintf('<?xml version="1.0" encoding="UTF-8"?>
								<POLICY xmlns="http://www.w3.org/2000/12/P3Pv1" discuri="/privacy">
								 <ENTITY>
								  <DATA-GROUP>
								   <DATA ref="#business.name">%s</DATA>
								   <DATA ref="#business.contact-info.online.email">%s</DATA>
								   <DATA ref="#business.contact-info.online.uri">%s://%s</DATA>
								   <DATA ref="#business.contact-info.postal.street">%s</DATA>
								   <DATA ref="#business.contact-info.postal.city">%s</DATA>
								   <DATA ref="#business.contact-info.postal.stateprov">%s</DATA>
								   <DATA ref="#business.contact-info.postal.postalcode">%s</DATA>
								   <DATA ref="#business.contact-info.postal.country">%s</DATA>
								   <DATA ref="#business.contact-info.telecom.telephone.loccode">%s</DATA>
								   <DATA ref="#business.contact-info.telecom.telephone.number">%s</DATA>
								  </DATA-GROUP>
								 </ENTITY>
								 <ACCESS><all/></ACCESS>
								 <STATEMENT>
								  <EXTENSION optional="yes"><GROUP-INFO name=""/></EXTENSION>
								        <CONSEQUENCE>
								        </CONSEQUENCE>
								  <PURPOSE><admin/><customization/><pseudo-analysis/></PURPOSE>
								  <RECIPIENT><ours/></RECIPIENT>
								  <RETENTION><indefinitely/></RETENTION>
								   <DATA-GROUP>
								    <DATA ref="#dynamic.cookies"><CATEGORIES><navigation/></CATEGORIES></DATA>
								    <DATA ref="#dynamic.http.useragent"><CATEGORIES><computer/></CATEGORIES></DATA>
								   </DATA-GROUP>
								 </STATEMENT>
								</POLICY>
								', // values
								Framework::$site->organisation->name,
								Framework::$site->organisation->email_support->address,
								Framework::$site->protocol,
								Framework::$site->host,
								Framework::$site->organisation->street,
								Framework::$site->organisation->city,
								'',
								'',
								Framework::$site->organisation->country->iso,
								Framework::$site->organisation->phone_area_code,
								Framework::$site->organisation->phone_number
								);
			break;
			default:
				$xml = sprintf('<META xmlns="http://www.w3.org/2002/01/P3Pv1">
								  <POLICY-REFERENCES>
								    <POLICY-REF about="/index.php/p3p/certificate">
								        <INCLUDE>/*</INCLUDE>
								    </POLICY-REF>
								</POLICY-REFERENCES>
								</META>');
		}

		header('Content-type: text/xml; charset=utf-8');
		print utf8_encode( trim($xml) );
		exit;
	}

}
