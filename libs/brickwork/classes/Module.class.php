<?php

/**
 * Webmodule interface
 *
 * @package	Module
 * @version	0.1
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 */

interface iModule {
	public function fetch();
}

abstract class Module implements iModule 
{
	public $contentHandlerName = '';

	// every module needs to have a loadConfig or loadData method
	abstract public function loadConfig();
	
	/**
	 * Gets the ACL object name that we are trying to access
	 *
	 * @return	string
	 */
	abstract protected function loadData();
	
	
}

/**
* Module Config
*
* Deze class wordt dynamisch gevuld in Webmodule
*/
class ModuleConfig {
	public function __construct(){}
	
	public function get($name, $default = null)
	{
		return isset($this->{$name}) ? $this->{$name} : $default;
	}
}
?>
