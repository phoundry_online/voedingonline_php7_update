<?php
/**
 * Banner class
 *
 * Code copied from the Banner Class implementation of BDU Kranten
 *
 * @author		Stefan Grootscholten	<stefan.grootscholten@webpower.nl>
 * @copyright	Web Power
 * @created		20 jan 2009
 * @version		0.2
 *
 * @package		Brickwork
 * @subpackage	BannerModule
 *
 * @link		http://projectwiki.office.webpower.nl/index.php/Brickwork:standaard_modules:BannerModule
 */
/**
 * $Id: Banner.class.php 1192 2009-08-13 09:43:55Z christiaan.baartse $
 */
class Banner
{
	/**
	 * Google Adsense Client identifier
	 *
	 * @var	String
	 */
	public $adsense_client;

	/**
	 * Google Adsense Slot Number
	 *
	 * @var	Integer
	 */
	public $adsense_slot;

	/**
	 * Enable Google Analytics
	 *
	 * @var	Boolean
	 */
	public $analytics;

	/**
	 * Background color
	 *
	 * @var	String
	 */
	public $bgcolor;

	/**
	 * Custom Javascript code
	 *
	 * @var	String
	 */
	public $custom_javascript;

	/**
	 * External link
	 *
	 * @var	Boolean
	 */
	public $external;

	/**
	 * Filetype of the banner
	 *
	 * @var	String
	 */
	public $filetype;

	/**
	 * Variables for flash banners
	 *
	 * @var	Array
	 */
	public $flashvars;

	/**
	 * Height of the image/flash
	 *
	 * @var	Integer
	 */
	public $height;

	/**
	 * Banner ID
	 *
	 * @var	Integer
	 */
	public $id;

	/**
	 * Location of the banner
	 *
	 * @var	String
	 */
	public $image;

	/**
	 * Name of the banner
	 *
	 * @var	String
	 */
	public $name;

	/**
	 * Target of the banner link
	 *
	 * @var	String
	 */
	public $target;

	/**
	 * URL of the banner link
	 *
	 * @var	String
	 */
	public $url;

	/**
	 * Width of the banner/flash
	 *
	 * @var	Integer
	 */
	public $width;

	/**
	 * Extention of the image
	 * @var string
	 */
	public $ext;

	/**
	 * Constructor for a banner
	 *
	 * @param	Integer	$id
	 * @param	Integer	$name
	 * @return	void
	 */
	protected function __construct($id, $name)
	{
		$this->id			= (int) $id;
		$this->name			= $name;
		$this->analytics	= false;
		$this->external		= false;
		$this->flashvars	= array();
	}

	/**
	 * Magic Getter
	 *
	 * For backwards compatibility with the older (template) implementations of the banner module
	 *
	 * @param	String	$attr
	 * @return	MixedVar
	 *
	 * @throws
	 */
	public function __get( $attr )
	{
		switch ( $attr )
		{
			case "flashWidth":		return $this->width;
			case "flashHeight":		return $this->height;
			case "ga_enable":		return $this->analytics;
			case "netdirect_code":	return $this->custom_javascript;

			default:
				throw new AttributeNotFoundException("Could not 'get' " . $attr);
		}
	}

	/**
	 * Get the banners from the database
	 *
	 * @param	Integer	$max_banners
	 * @param	Integer	$banner_group
	 * @param	String	$content_table
	 * @param	Boolean	$adsense
	 * @return	Array
	 */
	public static function getBanners($max_banners, $banner_group = null,
		$content_table = 'module_content_banner', $orderby = 'RAND()', $adsense = true)
	{
		$sql = sprintf("SELECT
       b.id,
       b.name,
       b.image,
       b.image_url,
       b.width,
       b.height,
       b.bgcolor,
       b.file,
       b.url,
       b.custom_javascript,
       b.popup,
       b.adsense_client,
       b.adsense_slot,
       b.analytics
  FROM %s b
 INNER JOIN lookup_site_banner sb ON (b.id = sb.banner_id)
%s
 WHERE sb.site_identifier = %s
   AND ( ( ( b.image IS NOT NULL OR b.image_url IS NOT NULL ) )
       OR b.custom_javascript IS NOT NULL
       OR ( b.adsense_client IS NOT NULL AND b.adsense_slot IS NOT NULL ) )
   AND NOW() BETWEEN b.online AND COALESCE(b.offline, NOW())
%s
%s
 ORDER BY %s
 LIMIT %d",
			DB::quote($content_table),
			(! is_null($banner_group) ? " INNER JOIN lookup_module_banner_group bg ON (bg.banner_id = b.id)" : ""),
			DB::quote(Framework::$site->identifier, true),
			(! is_null($banner_group) ? "   AND bg.group_id = " . DB::quote($banner_group) : ""),
			(! $adsense ? "   AND COALESCE(b.adsense_client, '')='' AND COALESCE(b.adsense_slot, '') = ''" : ""),
			$orderby,
			DB::quote($max_banners)
		);

		$res = DB::iQuery($sql);

		$banners = array();

		if (self::isError($res))
		{
			throw new Exception("Query " . $sql . " failed. " . $res->errstr);
		}
		else if ($res->current())
		{
            while ($res->current())
			{
			    $item = $res->current();
				$banner = new Banner($item->id, $item->name);

				$banner->adsense_client		= $item->adsense_client;
				$banner->adsense_slot		= $item->adsense_slot;
				$banner->analytics			= (bool) $item->analytics;
				$banner->bgcolor			= $item->bgcolor;
				$banner->custom_javascript	= $item->custom_javascript;
				$banner->height				= (int) $item->height;
				$banner->width				= (int) $item->width;
				$banner->url				= $item->url;

				$banner->target			= $item->popup ? "_blank" : "_self";

				if (isset($item->image))
				{
					$banner->image = (isset($GLOBALS['PHprefs']['uploadUrl']) ? rtrim($GLOBALS['PHprefs']['uploadUrl'], '/').'/' : "/uploaded/") . $item->image;
					$banner->external = false;
				}
				else if (isset($item->image_url))
				{
					$banner->image = $item->image_url;
					$banner->external = true;
				}

				list($banner->filetype, $banner->ext) = Banner::getFileType($banner);

				if (isset($banner->filetype))
				{
					if ($banner->filetype == "movie")
					{
						if (isset($banner->file))
						{
							$banner->flashvars = array(
								'streamer'	=> $banner->image,
								'file'		=> $item->file,
								'token'		=> $item->token
							);
						}
						else
						{
							$banner->flashvars = array(
								'file'		=> $banner->image
							);
						}
					}
				}
				$banners[] = $banner;
				$res->next();
			}
			return $banners;
		}
		return null;
	}

	/**
	 * Get the filetype of the banner
	 *
	 * @param	Banner	$banner
	 * @return	String
	 */
	public static function getFileType(Banner $banner)
	{
		if (!empty($banner->custom_javascript))
		{
			return array("javascript", null);
		}
		else if (!empty($banner->image))
		{
			$file = $banner->image;
			$arr = explode("?", $file);
			$file = $arr[0];
			$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

			switch ($ext)
			{
				case "jpg":
				case "jpeg":
				case "png":
				case "gif":
					$type = "image";
					break;

				case "swf":
					$type = "flash";
					break;

				default:
					$type = 'movie';
			}
			return array($type, $ext);
		}
		else if (!empty($banner->adsence_client) && !empty($banner->adsense_slot))
		{
			return array("adsense", null);
		}
		return array(null, null);
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
