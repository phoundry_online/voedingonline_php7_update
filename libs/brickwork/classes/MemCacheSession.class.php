<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("Usage of MemCacheSession is deprecated", E_USER_ERROR);
}

/**
 * MemCached based session
 * Warning: this file is experimental as it has not been tested yet!
 * This class can be used if you have memcache, but not memcache sessions.
 * 
 * @author		Peter Eussen
 * @version		0.1
 * @copyright	Web Power	(http://www.webpower.nl)
 */
class MemCacheSession extends Session
{
	/**
	 * Server where the memcache service is located (configurable through 'sessionhost' pref)
	 *
	 * @var string
	 */
	private	$_host;
	
	/**
	 * Port of the memcache machine (configurable through the 'sessionport' pref)
	 *
	 * @var unknown_type
	 */
	private $_port;
	
	/**
	 * The session name (configurable through 'sessionname' pref)
	 *
	 * @var string
	 */
	private $_name;
	
	/**
	 * The Memcache object
	 *
	 * @var MemCache
	 */
	private $_mc;
	
	/**
	 * The maximum lifetime
	 *
	 * @var integer
	 */
	private $_maxLifeTime;
	
	/**
	 * The instance of the MemCacheSession
	 *
	 * @var MemCacheSession
	 */
	static private $_instance = null;
	 
	public function __construct( $memCacheUrl )
	{
		global $PHprefs;
		
		parent::__construct();
		
		$url = parse_url($sessionHandler);
		
		$this->_host = coalesce(Utility::arrayValue($url, 'host'),'localhost');
		$this->_port = (int)coalesce(Utility::arrayValue($url, 'port'),11211);
		$this->_name = coalesce(Utility::arrayValue($url, 'path'), session_name());
		$this->_mc   = new MemCache();
		
		if ($this->_mc->getServerStatus( $this->_host, $this->_port ) == 0 )
			trigger_error("Session server not available", E_USER_ERROR );
			 
		$this->_maxLifeTime = (int)ini_get('session.gc_maxlifetime');
	}
	
	public function start()
	{
		if ( session_set_save_handler(
			array($this, "phpSessionOpen"),
			array($this, "phpSessionClose"),
			array($this, "phpSessionRead"),
			array($this, "phpSessionWrite"),
			array($this, "phpSessionDestroy"),
			array($this, "phpSessionGarbageCollect") )
		  )
		{
			session_start();
			$this->_attribute 	= &$_SESSION;
			$this->_sessionOpen = true;
			return true;
		}
		return false;
	}
	
	public function write()
	{
		return true;
	}
	
	public function clean()
	{
		$this->_sessionOpen = false;
		return session_destroy();
	}
	
	/**
	 * Creates an instance of this object
	 *
	 * @return	DatabaseSession
	 */
	static public function singleton()
	{
		global $PHprefs;
		
		if ( !is_null( self::$_instance ) )
			return self::$_instance;
			
		self::$_instance = new self( Utility::arrayValue($PHprefs,'sessionHandler') );
		return self::$_instance;
	}

	public function phpSessionOpen( $savePath, $sessionName )
	{
		if ( $this->_mc->connect( $this->_host, $this->_port ) === false )
			trigger_error("Unable to connect to session server: " . $this->_host, E_USER_ERROR );
			
		return true;
	}
	
	public function phpSessionClose()
	{
		$this->_mc->close();
		return true;
	}
	
	public function phpSessionRead( $sessionId )
	{
		$data = $this->_mc->get( Array( $this->_name, $this->_sid ));
		
		if ($data === false)
			return '';
		return $data;
	}
	
	public function phpSessionWrite( $sessionId, $sessionData )
	{
		return $this->_mc->set( Array($this->_name, $sessionId), $sessionData, MEMCACHE_COMPRESSED,  $this->_maxLifeTime );
	}
	
	public function phpSessionDestroy( $sessionid )
	{
		return $this->_mc->delete(Array($this->_name, $sessionId));
	}
	
	public function phpSessionGarbageCollect( $maxLifeTime )
	{
		return true;
	}
	
}
?>
