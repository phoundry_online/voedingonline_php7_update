<?php
/**
 * Feed Content Handler
 * 
 * @author J�rgen Teunis <jorgen@webpower.nl>
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * 
 * @version 0.0.2 // A lot of refactoring
 */
class FeedContentHandler extends ContentHandler {

	/**
	 * The BrickworkFeedCreator
	 *
	 * @var BrickworkFeedCreator
	 */
	protected $feed;
	
	/**
	 * The format that was specified default rss2.0
	 *
	 * @var string
	 */
	protected $format;
	
	/**
	 * The code of the feed module that is being used
	 *
	 * @var string
	 */
	protected $feed_module_code;
	
	/**
	 * The actual module that is gonna give us our nice FeedItems
	 *
	 * @var FeedModule
	 */
	protected $feed_module;
	
	/**
	 * The cache dir that is being used (without trailing slash!)
	 *
	 * @var string
	 */
	protected $cache_dir;
	
	/**
	 * The cache file that is being used (an MD5 hash in our case)
	 *
	 * @var string
	 */
	protected $cache_file;
	
	/**
	 * Initialization of the configuration
	 * Set everything ready to render
	 * 
	 * @return void
	 */
	public function init()
	{
		if (!defined('HIDE_SHOW_ME_THE_DAMAGE')) {
			define('HIDE_SHOW_ME_THE_DAMAGE', true);
		}
		
		$this->feed = new BrickworkFeedCreator();
		
		// get type from url, default rss2.0
		$this->format = !empty($this->path[0]) ? strtoupper($this->path[0]) : 'RSS2.0';
		if(!empty($this->path[1]))
		{
			$this->feed_module_code	= strtoupper($this->path[1]);
			$this->feed_module = FeedModule::getModuleByCode($this->feed_module_code);
		}
	}

	/**
	 * Render the output
	 *
	 * @param bool $return
	 */
	public function out($return = FALSE)
	{
		if(!($this->feed_module instanceof FeedModule)) {
			return $return ? false : $this->print404();
		}
		
		// we've used the contenthandler parts of the path array only keep the remaining, and reindex
		array_splice($this->path, 0, 2);
		
		$this->feed_module->contentHandlerName = get_class($this);
		$this->feed_module->params = $this->path;
		
		// The module can add additional supported formats
		foreach($this->feed_module->getAdditionalFormats() as $code => $definition)
		{
			if(is_string($definition) && $this->feed->formatExists($definition))
			{
				$this->feed->addFormatAlias($code, $definition);
			}
			else  if(is_array($definition) && !empty($definition['class']) && !empty($definition['item']))
			{
				$this->feed->addFormat($code, $definition['class'], $definition['item']);
			}
			else
			{
				trigger_error('Unsupported format definition');
			}
		}
		
		// check if we support the requested format
		if(!in_array($this->format, $this->feed->supportedFormats)) {
			return $return ? false : $this->print404();
		}
		
		$this->cache_dir = CACHE_DIR.'/feeds/'.Framework::$site->identifier.'/'.$this->format.'/'.$this->feed_module_code;
		$this->cache_file = md5(implode('', $this->path));
		
		if(!is_dir($this->cache_dir)) {
			mkdir($this->cache_dir, 0777, true);
		}
		
		if(!is_writable($this->cache_dir)) {
			chmod($this->cache_dir, 0777);
		}
		
		
		// Declare Cache options
		$this->feed->useCached(
			$this->format,
			$this->cache_dir.'/'.$this->cache_file,
			$this->feed_module->getCacheLifetime()
		);
		
		// Kick the module so it generates its items
		$this->feed_module->loadData();
		$this->feed->addItem($this->feed_module->items);
		
		// Default values, can be overwritten in the loop that follows
		$this->feed->descriptionTruncSize = 500; // The trunc size is nowhere used thus far
		$this->feed->descriptionHtmlSyndicated = true;
		
		// The following properties of the module will be copied onto the feedcreator
		// Additional properties can be put in the additionalProperties array
		$properties = array(
			'title',
			'description',
			'descriptionTruncSize',
			'descriptionHtmlSyndicated',
			'feedImage',
			'link',
			'syndicationURL',
			'image',
			'language',
			'copyright',
			'pubDate',
			'lastBuildDate',
			'editor',
			'editorEmail',
			'webmaster',
			'category',
			'docs',
			'ttl',
			'rating',
			'skipHours',
			'skipDays',
			'additionalProperties'
		);
		
		foreach($properties as $prop) {
			if(isset($this->feed_module->$prop)) {
				$this->feed->$prop = $this->feed_module->$prop;
			}
		}
		
		return $this->feed->saveFeed($this->format, $this->cache_dir.'/'.$this->cache_file, true);
	}
	
	/**
	 * Delete old cache files
	 *
	 * @param string $dir
	 */
	protected function _garbageCollect($dir)
	{
		
	}
}
