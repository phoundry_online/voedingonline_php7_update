<?php
/**
* DateTime
*
* @author Mick van der Most van Spijk <mick@webpower.nl>
* @version 0.0.4
* @last_modified
*/

// main class
class BrickDateTime {
	// decorator pattern container of specialized datetime object // TODO is dit wel een decorator?
	private $_date_time = NULL;

	protected $hour_     = 0;
	protected $minute_   = 0;
	protected $second_   = 0;
	protected $day_      = 1;
	protected $month_    = 1;
	protected $year_     = 1900;
	protected $timezone_ = 'Europe/Berlin';

	public function __construct($datetime=NULL) {
		// zelf construeren
		if (is_null($datetime)) {
			$time = localtime(time(), TRUE);

			$this->hour_     = $time['tm_hour'];
			$this->minute_   = $time['tm_min'];
			$this->second_   = $time['tm_sec'];
			$this->day_      = $time['tm_mday'];
			$this->month_    = $time['tm_mon'] + 1;
			$this->year_     = $time['tm_year'] + 1900;
			$this->timezone_ = date_default_timezone_get();

		} elseif ($datetime instanceof BrickDateTime) {
			$this->_date_time = $datetime;

			$this->hour_     = $datetime->hour;
			$this->minute_   = $datetime->minute;
			$this->second_   = $datetime->second;
			$this->day_      = $datetime->day;
			$this->month_    = $datetime->month;
			$this->year_     = $datetime->year;
			$this->timezone_ = $datetime->timezone;
		} elseif ($datetime == 0) {
			// kale lege date time aanmaken
			$this->hour_     = 0;
			$this->minute_   = 0;
			$this->second_   = 0;
			$this->day_      = 1;
			$this->month_    = 1;
			$this->year_     = 1900;
			$this->timezone_ = date_default_timezone_get();
		}
	}

	public function __set($name, $value) {
		switch ($name) {
			case 'hour':
				$this->hour_ = intval($value);
				break;

			case 'minute':
				$this->minute_ = intval($value);
				break;

			case 'second':
				$this->second_ = intval($value);
				break;

			case 'day':
				// cannot be smaller than 1
				$this->day_ = intval($value) > 1 ? intval($value) : 1;
				break;

			case 'month':
				$this->month_ = intval($value);
				break;

			case 'year':
				$this->year_ = intval($value);
				break;

			case 'timezone':
				$this->timezone_ = $value;
				break;

			case 'timestamp':
				// set all attribs by timestamp
				$time = localtime($value, TRUE);

				$this->hour_     = $time['tm_hour'];
				$this->minute_   = $time['tm_min'];
				$this->second_   = $time['tm_sec'];
				$this->day_      = $time['tm_mday'];
				$this->month_    = $time['tm_mon'] + 1;
				$this->year_     = $time['tm_year'] + 1900;
				$this->timezone_ = date_default_timezone_get();
				break;

			default:
				trigger_error(sprintf('Attribute %s not found', $name), E_USER_NOTICE);
				return FALSE;
		}
	}

	public function __get($name) {
		switch ($name) {
			case 'hour':
				return $this->hour_;

			case 'minute':
				return $this->minute_;

			case 'second':
				return $this->second_;

			case 'day':
				return $this->day_;

			case 'month':
				return $this->month_;

			case 'year':
				return $this->year_;

			case 'timezone':
				return $this->timezone_;

			case 'timestamp':
				date_default_timezone_set($this->timezone_);
				$timestamp = mktime($this->hour_, $this->minute_, $this->second_, $this->month_, $this->day_, $this->year_);
				if ($timestamp == -1 || $timestamp == FALSE) {
					// trigger_error('Invalid time', E_USER_NOTICE);
					return FALSE;
					break;
				}

				return $timestamp;

			case 'now':
				return new BrickDateTime();

			default:
				trigger_error(sprintf('Attribute %s not found', $name), E_USER_NOTICE);
				return FALSE;
		}
	}

	public function date($format='d-m-Y') {
		if (FALSE !== $this->timestamp) {
			return date($format, $this->timestamp);
		} else {
			// zelf maken, is een tijdstip voor 1970
			$str = str_replace('Y', $this->year, $format);
			$str = str_replace('m', $this->month, $str);
			$str = str_replace('d', $this->day, $str);

			return $str;
		}
	}

	public function checkdatetime() {
		$r = TRUE;

		// time
		if ($this->hour < 0 || $this->hour >= 24)
			$r = FALSE;

		if ($this->minute < 0 || $this->minute >= 60)
			$r = FALSE;

		if ($this->second < 0 || $this->second >= 60)
			$r = FALSE;

		// date
		if (!checkdate($this->month, $this->day, $this->year))
			$r = FALSE;

		return $r;
	}

	public function __toString() {
		return $this->date('c');
	}
}

// decorator
class MySQLDateTime extends BrickDateTime {
	protected $mysql_date = '';

	public function __construct($mysql_date) {
		$this->mysql_date = $mysql_date;

		$time = $this->_parse();

		$this->hour     = $time['tm_hour'];
		$this->minute   = $time['tm_min'];
		$this->second   = $time['tm_sec'];
		$this->day      = $time['tm_mday'];

		// month starts at 0
		$this->month    = $time['tm_mon'] + 1;

		$this->year     = $time['tm_year'] + 1900;
		$this->timezone = date_default_timezone_get();
	}

	protected function _parse() {
		// 2006-10-18 12:01:51
		return date_parse_from_format('Y-m-d H:i:s', $this->mysql_date);
	}
}

class MySQLDate extends MySQLDateTime {
	public function __construct($mysql_date) {
		parent::__construct($mysql_date);

		$this->hour   = 0;
		$this->minute = 0;
		$this->second = 0;
	}

	protected function _parse() {
		// 2006-10-18
		return date_parse_from_format('Y-m-d', $this->mysql_date);
	}
}
?>
