<?php
require_once INCLUDE_DIR.'/classes/Mail/mime.class.php';
require_once INCLUDE_DIR.'/classes/Mail/mimepart.class.php';

/**
* Filename.......: SMTP.class.php
* Project........: SMTP Class
* Version........: 1.0
* Last Modified..: 10 oct 2006
*/

define('SMTP_STATUS_NOT_CONNECTED', 1);
define('SMTP_STATUS_CONNECTED', 2);

class SMTP {

	var $authenticated, $connection, $recipients, $headers, $timeout;
	var $errors, $status, $body, $from, $host, $port, $helo;
	var $auth, $user, $pass;

	/**
	 * Constructor function. Arguments:
	 * $params - An assoc array of parameters:
	 *
	 *   host    - The hostname of the smtp server		Default: localhost
	 *   port    - The port the smtp server runs on		Default: 25
	 *   helo    - What to send as the HELO command		Default: hostname
	 *             (typically the hostname of the
	 *             machine this script runs on)
	 *   auth    - Whether to use basic authentication	Default: FALSE
	 *   user    - Username for authentication			Default: <blank>
	 *   pass    - Password for authentication			Default: <blank>
	 *   timeout - The timeout in seconds for the call	Default: 5
	 *             to fsockopen()
	 */
	function __construct($params = array())
	{
		if(!defined('CRLF'))
			define('CRLF', "\r\n");

		$this->authenticated	= FALSE;
		$this->timeout			= 5;
		$this->status			= SMTP_STATUS_NOT_CONNECTED;
		$this->host				= 'localhost';
		$this->port				= 25;
		$this->helo				= php_uname('n');
		$this->auth				= FALSE;
		$this->user				= '';
		$this->pass				= '';
		$this->errors   		= array();

		foreach($params as $key => $value){
			$this->$key = $value;
		}
	}

	/**
	 * Connect function. This will, when called
	 * statically, create a new smtp object,
	 * call the connect function (ie this function)
	 * and return it. When not called statically,
	 * it will connect to the server and send
	 * the HELO command.
	 */

	function connect($params = array())
	{
		foreach($params as $key=>$value) {
			$this->$key = $value;
		}

		$this->connection = @fsockopen($this->host, $this->port, $errno, $errstr, $this->timeout);
		if(function_exists('socket_set_timeout')){
			@socket_set_timeout($this->connection, 5, 0);
		}

		$greeting = $this->get_data();
		if(is_resource($this->connection)){
			$this->status = SMTP_STATUS_CONNECTED;
			return $this->auth ? $this->ehlo() : $this->helo();
		}
		else {
			$this->errors[] = 'Failed to connect to server: '.$errstr;
			return FALSE;
		}
	}

	/**
	 * Function which handles sending the mail.
	 *
	 * @param $from(string) The return-path, needs to be a valid
	 * 							e-mail addres, like user@domain.com!
	 * @param $recipients(mixed)
	 *				Either a string containing 1 e-mail address, or
	 *          an array containing multiple adresses.
	 * @param array $headers The array of headers to send with the mail, in an
	 * 			associative array, where the array key is the
	 * 			header name (e.g., 'Subject'), and the array value
	 * 			is the header value (e.g., 'test'). The header
	 * 			produced from those values would be 'Subject: test'.
	 * @param string $body The full text of the message body, including any
	 * 			Mime parts, etc.
	 */
	function send($from, $recipients, $headers, $body)
	{
		$this->errors = array();

		if (!isset($headers['Return-Path']))
			$headers['Return-Path'] = $from;

		list($text_headers) = $this->prepareHeaders($headers);

		if($this->is_connected()){
			// Do we auth or not?
			// Note the distinction between the auth variable and auth() function
			if($this->auth && !$this->authenticated){
				if(!$this->auth())
					return FALSE;
			}

			$this->mail($from);

			if(is_array($recipients)) {
				foreach($recipients as $value) {
					$this->rcpt($value);
				}
			}
			else {
				$this->rcpt($recipients);
			}

			if(!$this->data()) {
				return FALSE;
			}
			
			// Fixes the possibility of a mailserver "error 550 - Maximum 
			// line length exceeded" by forcing a maximum length of 500 chars. 
			$body = wordwrap($body, 500, CRLF, false);

			$this->send_data($text_headers . CRLF . $body);
			$this->send_data('.');

			$result = (substr(trim($this->get_data()), 0, 3) === '250');
			return $result;
		}
		else {
			$this->errors[] = 'Not connected!';
			return FALSE;
		}
	}

	/**
	 * Function to implement HELO cmd
	 */
	function helo()
	{
		if(is_resource($this->connection)
			&& $this->send_data('HELO '.$this->helo)
			&& substr(trim($error = $this->get_data()), 0, 3) === '250' ) {
			return TRUE;
		}
		else {
			$this->errors[] = 'HELO command failed, output: ' . trim($error);
			return FALSE;
		}
	}

	/**
	 * Function to implement EHLO cmd
	 */
	function ehlo()
	{
		if(is_resource($this->connection)
			&& $this->send_data('EHLO '.$this->helo)
			&& substr(trim($error = $this->get_data()), 0, 3) === '250' ) {
			return TRUE;
		}
		else {
			$this->errors[] = 'EHLO command failed, output: ' . trim($error);
			return FALSE;
		}
	}

	/**
	 * Function to implement RSET cmd
	 */
	function rset()
	{
		if(is_resource($this->connection)
			&& $this->send_data('RSET')
			&& substr(trim($error = $this->get_data()), 0, 3) === '250' ) {
			return TRUE;
		}
		else {
			$this->errors[] = 'RSET command failed, output: ' . trim($error);
			return FALSE;
		}
	}

	/**
	 * Function to implement QUIT cmd
	 */
	function quit()
	{
		if(is_resource($this->connection)
			&& $this->send_data('QUIT')
			&& substr(trim($error = $this->get_data()), 0, 3) === '221' ){
			fclose($this->connection);
			$this->status = SMTP_STATUS_NOT_CONNECTED;
			return TRUE;
		}
		else {
			$this->errors[] = 'QUIT command failed, output: ' . trim($error);
			return FALSE;
		}
	}

	/**
	 * Function to implement AUTH cmd
	 */
	function auth()
	{
		if(is_resource($this->connection)
			&& $this->send_data('AUTH LOGIN')
			&& substr(trim($error = $this->get_data()), 0, 3) === '334'
			&& $this->send_data(base64_encode($this->user))			// Send username
			&& substr(trim($error = $this->get_data()),0,3) === '334'
			&& $this->send_data(base64_encode($this->pass))			// Send password
			&& substr(trim($error = $this->get_data()),0,3) === '235' ) {
			$this->authenticated = TRUE;
			return TRUE;
		}
		else {
			$this->errors[] = 'AUTH command failed: ' . trim($error);
			return FALSE;
		}
	}

	/**
	 * Function that handles the MAIL FROM: cmd
	 */
	function mail($from)
	{
		if($this->is_connected()
			&& $this->send_data('MAIL FROM:<'.$from.'>')
			&& substr(trim($this->get_data()), 0, 2) === '250' ) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

	/**
	 * Function that handles the RCPT TO: cmd
	 */
	function rcpt($to)
	{
		if($this->is_connected()
			&& $this->send_data('RCPT TO:<'.$to.'>')
			&& substr(trim($error = $this->get_data()), 0, 2) === '25' ) {
			return TRUE;
		}
		else {
			$this->errors[] = trim($error);
			return FALSE;
		}
	}

	/**
	 * Function that sends the DATA cmd
	 */
	function data()
	{
		if($this->is_connected()
			&& $this->send_data('DATA')
			&& substr(trim($error = $this->get_data()), 0, 3) === '354' ) {
			return TRUE;
		}
		else {
			$this->errors[] = trim($error);
			return FALSE;
		}
	}

	/**
	 * Function to determine if this object
	 * is connected to the server or not.
	 */
	function is_connected()
	{
		return (is_resource($this->connection) && ($this->status === SMTP_STATUS_CONNECTED));
	}

	/**
	 * Function to send a bit of data
	 */
	function send_data($data)
	{
		$this->quotedata($data);
		if(is_resource($this->connection))
			return fwrite($this->connection, $data.CRLF, strlen($data)+2);
		else
			return FALSE;
	}

	/**
	 * Function to get data.
	 */
	function get_data()
	{
		$return = '';
		$line   = '';
		$loops  = 0;

		if(is_resource($this->connection)) {
			while((strpos($return, CRLF) === FALSE || substr($line,3,1) !== ' ') && $loops < 100){
				$line    = fgets($this->connection, 512);
				$return .= $line;
				$loops++;
			}
			return $return;

		}
		else {
			return FALSE;
		}
	}

	/**
	 * Sets a variable
	 */
	function set($var, $value)
	{
		$this->$var = $value;
		return TRUE;
	}

	/**
	 * Take an array of mail headers and return a string containing
	 * text usable in sending a message.
	 *
	 * @param array $headers The array of headers to prepare, in an associative
	 *              array, where the array key is the header name (ie,
	 *              'Subject'), and the array value is the header
	 *              value (ie, 'test'). The header produced from those
	 *              values would be 'Subject: test'.
	 *
	 * @return mixed Returns false if it encounters a bad address,
	 *               otherwise returns an array containing two
	 *               elements: Any From: address found in the headers,
	 *               and the plain text version of the headers.
	 * @access private
	 */
	function prepareHeaders($headers)
	{
		$lines = array();

		foreach ($headers as $key => $value) {
			if ($key === 'Received') {
				// Put Received: headers at the top.  Spam detectors often
				// flag messages with Received: headers after the Subject:
				// as spam.
				array_unshift($lines, $key . ': ' . $value);
			}
			else {
				 if( strtolower($key) == 'to' && is_array($value) ){
				 	$value = implode(';', $value);
				 }
				$lines[] = $key . ': ' . $value;
			}
		}

		return array(join(CRLF, $lines) . CRLF);
	}


	/**
	 * Quote the data so that it meets SMTP standards.
	 *
	 * This is provided as a separate public function to facilitate easier
	 * overloading for the cases where it is desirable to customize the
	 * quoting behavior.
	 *
	 * @param string The message text to quote.  The string must be passed
	 *               by reference, and the text will be modified in place.
	 *
	 * @access public
	 * @since  1.2
	 */
	function quotedata(&$data)
	{
		/*
		 * Change Unix (\n) and Mac (\r) linefeeds into Internet-standard CRLF
		 * (\r\n) linefeeds.
		 */
		$data = preg_replace("/([^\r]{1})\n/", "\\1\r\n", $data);
		$data = preg_replace("/\n\n/", "\n\r\n", $data);

		/*
		 * Because a single leading period (.) signifies an end to the data,
		 * legitimate leading periods need to be "doubled" (e.g. '..').
		 */
		$data = preg_replace("/\n\./", "\n..", $data);
	}

}
?>