<?php
/**
 * Page Caching class
 *
 * @author 		Peter Eussen
 * @package		Brickwork
 * @subpackage	Core
 * @version		1.0
 */
class PageCache
{
	/**
	 * Indicator to see if page caching is enabled;
	 * @var bool
	 */
	static protected $_enabled = false;

	/**
	 * Singleton Instance
	 * @var PageCache
	 */
	static protected $_instance = null;

	/**
	 * The implementation of the cache
	 *
	 * @var Cache
	 */
	protected $_cache;

	/**
	 * The current path
	 * @var string
	 */
	protected $_path;
	
	/**
	 * The content type for the output
	 * @var	string
	 */
	protected $_contentType;

	/**
	 * The key for the current page in the cache
	 *
	 * @var string
	 */
	protected $_curKey;

	/**
	 * List of filters for requests
	 *
	 * @var Array
	 */
	protected $_filter;

	/**
	 * Cachetimeout
	 * @var	int
	 */
	protected $_timeout;

	/**
	 * Don't use this for normal use, use getInstance instead
	 * @param string $path
	 * @param string $hash
	 * @param array $filter
	 * @param int $timeout
	 */
	public function __construct($path, $hash, array $filter, $timeout, Cache_Dir $cache = null)
	{
		$this->_path = $path;
		$this->_filter = $filter;
		$this->_curKey = $this->_path.'/'.$hash;
		$this->_timeout = $timeout;
		$this->_cache = $cache;
	}

	/**
	 * Fetches the singleton instance with the configured config in WEBprefs
	 *
	 * @return PageCache
	 */
	static public function getInstance()
	{
		if(is_null(self::$_instance)) {
			global $WEBprefs;
			
			$constants = get_defined_constants(true);
			$constants = Utility::arrayValue($constants, 'user', array());
	
			// Die willen we _absoluut_ niet gebruiken voor cache
			if(isset($constants['PHP_TIMESTAMP'])) {
				unset($constants['PHP_TIMESTAMP']);
			}
			if(isset($constants['PHP_TIMESTAMP_FLOAT'])) {
				unset($constants['PHP_TIMESTAMP_FLOAT']);
			}
			
			$hash = brickHash::get(array(
				Utility::arrayValue($_SERVER, 'PATH_INFO'),
				Utility::arrayValue($_SERVER, 'QUERY_STRING'),
				$WEBprefs,
				$constants
			));
			
			$filter = Utility::arrayValueRecursive($WEBprefs, array('pagecache', 'filter'), array());
			$timeout = Utility::arrayValueRecursive($WEBprefs, array('pagecache', 'timeout'), 600);
			
			$cache = null;
			if($filter) {
				$cache = Cache::factory('dir://pages/'.
					(isset(Framework::$site->identifier) ? Framework::$site->identifier.'/':'').
					'?timeout='.$timeout.'&raw=true&sweep=false');
			}
			
			self::$_instance = new self(
				Utility::arrayValue($_SERVER, 'PATH_INFO'),
				$hash,
				$filter,
				$timeout, 
				$cache
			);
		}

		return self::$_instance;
	}

	/**
	 * Enables page caching
	 *
	 * @return bool
	 */
	static public function enable()
	{
		self::$_enabled = true;
		return true;
	}
	
	/**
	 * Tells if caching is currently enabled
	 * @return bool
	 */
	static public function isEnabled()
	{
		return self::$_enabled;
	}

	/**
	 * Checks if the page is cacheable
	 *
	 * @return bool
	 */
	public function cacheable()
	{
		if(!self::$_enabled || Utility::isPosted()) {
			return false;
		}

		// always allow caching of the homepage if caching is enabled
		if($this->_path == '') {
			$this->_contentType = 'text/html';
			return true;
		}

		$allow = false;
		if(isset($this->_filter['allow']) ) {
			foreach($this->_filter['allow'] as $path => $ct) {
				if(strpos($this->_path, $path) === 0) {
					$this->_contentType = $ct;
					$allow = true;
					break;
				}
			}
		}

		// If it is not allowed, dont try block
		if(!$allow) {
			return false;
		}

		if(isset($this->_filter['block'])) {
			foreach($this->_filter['block'] as $path) {
				if(strpos($this->_path, $path) === 0) {
					return false;
				}
			}
		}

		// Anything that is not blocked is allowed due to the !$allow filter above the block
		return true;
	}

	/**
	 * Checks if the page is cached
	 *
	 * @return bool
	 */
	public function isCached() {
		if(!self::$_enabled || !$this->_cache) {
			return false;
		}
		return $this->_cache->keyIsset($this->_curKey);
	}

	/**
	 * Store new content in the cache
	 *
	 * @param 	string	$data
	 * @return 	bool
	 */
	public function put($data)
	{
		return self::$_enabled && $this->_cache &&
			$this->_cache->setValue($this->_curKey, $data);
	}

	/**
	 * get the contents from the cache dir
	 *
	 * @return string
	 */
	public function get()
	{
		if(!self::$_enabled || !$this->_cache) {
			return false;
		}
		return $this->_cache->getValue($this->_curKey);
	}

	/**
	 * Flushes the page cache for the current page
	 *
	 * @return string
	 */
	public function clean()
	{
		if(!self::$_enabled || !$this->_cache) {
			return false;
		}
		return $this->_cache->unsetKey($this->_curKey);
	}

	/**
	 * Obtain the key for the current page
	 *
	 * @return string
	 */
	public function getKey()
	{
		return $this->_curKey;
	}

	/**
	 * Output the contents by setting the proper headers
	 *
	 * @return void
	 */
	public function send(array $headers = null)
	{
		if(!self::$_enabled || !$this->_cache) {
			return false;
		}

		if(!$headers) {
			$headers = function_exists('apache_request_headers') ?
				apache_request_headers() : null;
		}
		$modts = $this->_cache->modifiedTime($this->_curKey);

		// reset some
		header('Expires:');
		header('Pragma:');

		// set some
		header('Cache-Control: public, max-age='. $this->_timeout);
		// Dont base etag on content hash because then you always need the content which slows down the system
		$etag = md5($this->_curKey);
		header('Etag: "'.$etag.'"');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s \G\M\T', $modts));
		header(sprintf('P3P: policyref="http://%s/index.php/p3p", CP="NOI DSP COR NID PSA ADM OUR IND NAV COM"', Utility::arrayValue($_SERVER, 'HTTP_HOST', 'localhost')));

		$if_mod_since= isset($headers['If-Modified-Since']) ?
			strtotime($headers['If-Modified-Since']) : null;

		$if_none_match= isset($headers['If-None-Match']) ?
			trim($headers['If-None-Match'], '"') : null;

		if($modts <= $if_mod_since &&
			($if_none_match== null || $etag== $if_none_match)) {
			header("HTTP/1.1 304 Not Modified");
			return;
		}

		// set cache headers
		header('Date: '.gmdate('D, d M Y H:i:s \G\M\T', $modts));
		header('Content-type: '.$this->_contentType);

		$out = $this->get();
		header('Content-Length: '.strlen($out));
		echo $out;
	}
}