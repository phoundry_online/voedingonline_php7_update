<?php
require_once "{$PHprefs['distDir']}/core/include/PhoundryContentNodeService.php";

class DetailContentHandler extends ContentHandler
{
	public function init()
	{
		$db = DB::getDb(1);
		$content_service = new PhoundryContentNodeService($db);
		
		$path = $this->path;
		switch (count($path)) {
			case 1:
				$content_id = array_shift($path);
				$node = $content_service->getById($content_id);
			break;
			case 2:
				$string_id = array_shift($path);
				$rid = array_shift($path);
				$node = $content_service->getByStringIdAndRecordId($string_id, $rid);
			break;
			default:
				$this->send404();
		}
			
		if ($node) {
			$service = new Phoundry_Brickwork_ContentNodeService($db);
			$link = $service->getLink($node, $_GET);
			
			if ($link) {
				trigger_redirect($link['url']);
			}
		}
		
		$this->send404();
	}
	
	private function send404()
	{
		header("HTTP/1.0 404 Not Found");
		echo "Not Found";
		exit;
	}
}