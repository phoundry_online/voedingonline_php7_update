<?php
require_once INCLUDE_DIR.'functions/locale.php';

/**
 * Form
 *
 * General form class. Extend this class for your implementations.
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @version 0.0.3
 * @last_modified 2-7-2007
 * @note Velden worden onderaan geincluded
 * @changes:
 * - handleRequest: checkdatetime toegevoegd aan veld controle
 *
 * @package		Brickwork
 * @subpackage	Form
 */
abstract class Form
{
	/**
	 * Presentatietitel van het formulier
	 *
	 * @var	String
	 */
	protected $title_;

	/**
	 * Omschrijving van het formulier
	 *
	 * @var	String
	 */
	protected $description_;

	/**
	 * Header
	 * Inleidende tekst voor het formulier
	 *
	 * @var	String
	 */
	protected $header_;

	/**
	 * Footer
	 * Uitleidende tekst voor het formulier, meestal bedoeld om onder het formulier te zetten
	 *
	 * @var	String
	 */
	protected $footer_;

	/**
	 * Naam van het formulier
	 *
	 * @var	String
	 */
	protected $name_;

	/**
	 * Id van het formulier
	 *
	 * var	Integer
	 */
	protected $id_;

	/**
	 * URL waar het formulier naar wordt verzonden
	 *
	 * @var	String
	 */
	protected $action_;

	/**
	 * Methode van verzending
	 *
	 * @var	String
	 */
	protected $method_ = FormMethod::post;

	/**
	 * Doel van de verzending, voor gebruik bij frames
	 *
	 * @var	String
	 */
	protected $target_;

	/**
	 * Codering van het formulier
	 *
	 * Keuze uit:
	 * - application/x-www-form-urlencoded
	 * - multipart/form-data
	 *
	 * @var	String
	 */
	protected $encoding_ = 'application/x-www-form-urlencoded';

	/**
	 * Formulier velden
	 *
	 * @var	Array
	 */
	protected $fields_ = array();

	/**
	 * container voor requests
	 *
	 * @var	array
	 */
	protected static $request_ = array();

	/**
	 * form and fields are readonly
	 * upon setting this attrib all defined fields are set as well
	 *
	 * @var	Boolean
	 */
	protected $readonly_ = FALSE;

	/*
	 * multiplicity van het form, betekent of dit formulier meerder malen op de pagina mag worden ingevuld, zoals bijvoorbeeld kinderen van ouders, in een apart formulier kunnen deze eindeloos worden toegevoegd,
	 * de multipliciteit is:
	 * *: (0 of meer)
	 * +: (1 of meer)
	 * 1: precies 1
	 * n: precies n (unsigned int)
	 *
	 * @var	String
	 */
	protected $multiplicity_ = 1;

	/**
	 * Aantal data rijen voor dit formulier
	 * Indien dit een formulier is met een multipliciteit groter dan 1 dan
	 * moeten er meerdere rijen data worden gemanaged, deze teller geeft aan
	 * hoeveel rijen dat zijn
	 *
	 * @var	Integer
	 */
	protected $cnt_records = 0;

	/**
	 * Error container for form errors
	 *
	 * @var	Array
	 */
	protected $errors_ = array();

	/**
	 * Feedback mogelijkheid voor form
	 *
	 * @var	Boolean
	 */
	protected $feedbackable_ = FALSE;

	/**
	 * Default folder in which the uploaded files are saved
	 *
	 * @var	String
	 */
	protected $upload_folder = UPLOAD_DIR;

	/**
	 * List of emailadresses to send the results of this form to or a notification
	 *
	 * @var	E-mail[]
	 */
	protected $emailaddresses_ = array();

	/**
	 * Visibility
	 *
	 * Visibility for this form
	 * - public: visible to everybody
	 * - protected: visible to logged in users only
	 * - private: visible to specified user only
	 *
	 * inclusief controle array
	 *
	 * @var	String
	 */
	protected $visibility_ = 'public';

	/**
	 * Visibilities
	 *
	 * @var	Array
	 */
	private static $_visibilities = array(
		'private'   => FALSE,
		'protected' => FALSE,
		'public'    => TRUE
	);

	/**
	 * Visible flag
	 *
	 * @var	Boolean
	 */
	protected $visible_ = TRUE;

	/**
	 * Set visibilities
	 *
	 * @param	Array	$list
	 * @return	void
	 */
	public static function setVisibilities($list)
	{
		foreach ($list as $v => $flag)
		{
			switch($v)
			{
				case 'public':
				case 'protected':
				case 'private':
					self::$_visibilities[$v] = $flag;
			}
		}
	}

	public function addError($e) {
		$this->errors_[] = $e;
	}

	/**
	 * Construct a new form
	 *
	 * @param	String	$name
	 * @param	String	$action
	 * @param	String	$method
	 *
	 * @return	void
	 */
	public function __construct($name, $action, $method=FormMethod::post)
	{
		$this->name   = $name;
		$this->action = $action;
		$this->method = $method;
	}

	/**
	 * Magic setter
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'name':
				$this->name_ = $value;
				break;

			case "id":
				$this->id_ = $value;
				break;

			case 'action':
				// chk of url is
				$this->action_ = $value;
				break;

			case 'title':
				$this->title_ = $value;
				break;

			case 'readonly':
				$this->readonly_ = (bool)$value;
				$this->_set_readonly_fields((bool)$value);
				break;

			case 'method':
				if ((FormMethod::post != $value) && (FormMethod::get != $value))
				{
					throw new AttributeNotValidException(sprintf('Attribute %s not of type FormMethod', $name));
				}

				$this->method_ = $value;
				break;

			case 'fields':
				// ken parent toe
				if (is_array($value))
				{
					foreach ($value as $field)
					{
						$field->form = $this;
					}

					$this->fields_ = $value;
				}
				break;

			case 'multiplicity':
				$this->multiplicity_ = $value;
				break;

			case 'encoding':
				if ($this->_is_valid_encoding($value))
				{
					$this->encoding_ = $value;
				}
				break;

			case 'error':
				$this->errors_[] = $value;
				break;

			case 'feedbackable':
				$this->feedbackable_ = (bool)$value;
				break;

			case 'description':
				$this->description_ = $value;
				break;

			case 'header':
				$this->header_ = $value;
				break;

			case 'footer':
				$this->footer_ = $value;
				break;

			case 'emailaddresses':
				$this->emailaddresses_ = $value;
				break;

			case 'visibility':
				// bestaat deze zichtbaarheidsoptie
				if (in_array($value, self::$_visibilities))
				{
					if (self::$_visibilities[$value])
					{
						// bestaat en mag worden gezet
						$this->visibility_ = $value;
					}
					else
					{
						// bestaat maar mag niet worden gezet
						$this->visibility_ = FALSE;
					}
 				}
				break;

			case 'visible':
				$this->visible_ = (bool)$value;
				break;

			default:
				throw new AttributeNotFoundException(sprintf('Attribute %s not found', $name));
		}
	}

	/**
	 * Magic getter
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'name':
				return $this->name_;

			case "id":
				return $this->id_;

			case 'action':
				return $this->action_;

			case 'method':
				return $this->method_;

			case 'title':
				return coalesce($this->title_, $this->name_);

			case 'fields':
				return (is_array($this->fields_) ? (array)$this->fields_ : $this->fields_);

			case 'readonly':
				return $this->readonly_;

			case 'multiplicity':
				return $this->multiplicity_;

			case 'multiple':
				if (($this->multiplicity == '*' && $this->cnt_records > 0) ||
					($this->multiplicity == '+' && $this->cnt_records > 0) ||
					(($this->multiplicity > 1  && $this->cnt_records > 0)))
				{
					return TRUE;
				}
				return FALSE;

			case 'encoding':
				return $this->encoding_;

			case 'cntRecords':
				return $this->cnt_records;

			case 'errors':
				return $this->errors_;

			case 'removeable':
				if (($this->multiplicity == '*' && $this->cnt_records > 0) ||
					($this->multiplicity == '+' && $this->cnt_records > 0) ||
					(($this->multiplicity > 1  && $this->cnt_records > 0)))
				{
					return TRUE;
				}
				return FALSE;

			case 'feedbackable':
				return $this->feedbackable_;

			case 'description':
				return $this->description_;

			case 'header':
				return $this->header_;

			case 'footer':
				return $this->footer_;

			case 'emailaddresses':
				return $this->emailaddresses_;

			case 'visibility':
				return $this->visibility_;

			case 'visible':
				return $this->visible_;

			default:
				throw new AttributeNotFoundException(sprintf('Attribute %s not found', $name));
		}
	}

	/**
	 * Set all fields readonly
	 *
	 * Loop through fields and set each readonly
	 *
	 * @param	Boolean	$bool
	 */
	private function _set_readonly_fields($bool)
	{
		$fields = $this->fields;

		if (count($fields) > 0)
		{
			foreach ($fields as $field)
			{
				$field->readonly = $bool;
			}
		}
	}

	/**
	 * Check if encoding for form is valid
	 *
	 * @param	String	$enc
	 * @return	Boolean
	 */
	private function _is_valid_encoding($enc)
	{
		$valids = array(
			'application/x-www-form-urlencoded',
			'multipart/form-data'
		);
		return in_array($enc, $valids);
	}

	/**
	 * Return a field by its label
	 *
	 * Loop through fields and return the first field with the supplied label
	 *
	 * @param	String	$label
	 * @return	Field
	 */
	public function getFieldByLabel($label)
	{
		foreach ($this->fields as $i => $field)
		{
			if ($label == $field->label)
			{
				return $this->fields[$i];
			}
		}
	}

	/**
	 * Return a field by its name
	 *
	 * Loop through fields and return the first field with the supplied label
	 *
	 * @param	String	$name
	 * @return	Field
	 */
	public function getFieldByName($name)
	{
		foreach ($this->fields as $i => $field)
		{
			if ($name == $field->name)
			{
				return $this->fields[$i];
			}
		}
	}

	/**
	 * Handel de post of get van een formulier af
	 *
	 * na een post of get actie moet deze functie worden aangeroepen.
	 * Het formulier wordt uit de request gehaald en elk veld in het fomulier
	 * krijgt zijn requested waarde toegewezen
	 *
	 * @param	array	$request	post/get
	 * @return	Boolean	true on success and false if handle request failed
	 */
	public function handleRequest($request)
	{
		// zet velden op juiste waarde
		// interne container
		$_fields = array();

		// match requestvelden op formulier velden
		foreach ($request as $formname => $fields)
		{
			if ($formname == $this->name || $formname == str_replace(' ', '_', $this->name))
			{
				$_fields = $fields;
			}
		}

		// unsetten van foreach fields
		unset($fields);

		// match ook eventuele uploadfields
		foreach ($_FILES as $formname => $fields)
		{
			// loop door alle bestanden die in dit formulier worden geuploadeded
			if ($formname == $this->name || $formname == str_replace(' ', '_', $this->name))
			{
				// loop door upload filearray
				foreach ($fields as $key => $name_and_value)
				{
					// loop door gegevens van elk veld
					foreach ($name_and_value as $name => $value)
					{
						// maak eigen veld lijst aan, dus een lijst gegevens per veld ipv per key
						$_fields[$name][$key] = $value;
					}
				}
			}
		}

		// unsetten van foreach fields
		unset($fields);

		// Bepaal de multiple select arrays
		/*
		foreach ($_fields as $key=>$tmp_field) {
			if (is_array($tmp_field)) {
				$_array_fields[$key] = $tmp_field;
			}
		}
		*/

		// sleutels voor velden bepalen
		$_keys = array_keys($_fields);

		// alle data zit in fields
		$msg = '';
		foreach ( ($this->fields) as $field)
		{
			// key gevonden
			if (in_array($field->name, $_keys))
			{
				// zet nwe waarde
				$value = $_fields[$field->name];

				// strings trimmen
				if (is_string($value))
				{
					$value = trim($value);
				}

				if ($field instanceof SelectField && $value == '') {
					$value = 0;
				}

				// ken toe
				try
				{
					/*if ($field instanceOf MailSelectField) {
						if(!is_array($value)) {
							if(base64_decode($value) !== FALSE) {
								$value = base64_decode($value);
							}
						} else {
							$return = array();
							foreach($value as $val) {
								if(base64_decode($val) !== FALSE) {
									$return[] = base64_decode($val);
								} else {
									break;
								}
							}
							if (count($value) == count($return)) {
								$value = $return;
							}
						}cmVtY29Ad2VicG93ZXIubmw=
					}*/
					// var_dump($value);
					$field->value = $value;
				}
				catch (Exception $e)
				{
                    $error = new Form_Error(E_USER_NOTICE, $e->getMessage());
                    $error->setFormFieldName($field->formFieldName);
					$this->errors_[] = $error;
				}

				// verplicht dus checken op ingevuld
				if ($field->required)
				{

					// strings checken op leeg, objecten kunnen zichzelf valideren
					if (!is_object($field->value) && ($field->value === "" || $field->isEmpty()) || $field->value === -1 || $field->value === "-1")
					{
					                        $error = new Form_Error(E_USER_NOTICE, sprintf(_tr('%s is niet ingevuld',null,__FILE__,__LINE__), $field->label));
                        $error->setFormFieldName($field->formFieldName);
						$this->errors_[] = $error;
						continue;
					}
				}

				// ingevuld dus checken op syntax, kan alle soorten zijn dus ook array
				if (!is_object($field->value) && '' != $field->value)
				{
					$e = $field->chkValue();
					if (self::isError($e))
					{
                        $error = new Form_Error($e->errno, sprintf("%s: %s", $field->label, $e->errstr));
                        $error->setFormFieldName($field->formFieldName);
                        $this->errors_[] = $error;
						continue;
					}
				}

				if (is_array($field->value))
				{
					$value = commalize($field->value);
				}
				else
				{
					$value = $field->value;
				}	
				// fix voor datetime TODO klopt dit wel, moet dit wel door Form gebeuren? 12/19/2006
				if ($value instanceof BrickDateTime)
				{
					// valideer
					if (FALSE === $value->checkdatetime())
					{
                        $error = new Form_Error(E_USER_WARNING, sprintf(_tr("%s: geen correcte datum",null,__FILE__,__LINE__), $field->label));
                        $error->setFormFieldName($field->formFieldName);
                        $this->errors_[] = $error;
						continue;
					}

					$value = $value->date();
				}

				//checkbox afvangen
				if ($field instanceOf CheckboxField && !$field->value)
				{
					$field->value = "Checked"; // $field->value = "Checked";\
				}

				if ($field instanceof EmailField) {
					if (!Email::isValid($field->value)) {
						 $error = new Form_Error(E_USER_WARNING, sprintf("%s is geen geldig e-mailadres!", $field->value));
                          			 $error->setFormFieldName($field->formFieldName);
 			                         $this->errors_[] = $error;
					}
				}

				// file velden afvangen
				if ($field instanceOf FileField)
				{
					//if ($field->value == '') {
					//} else
					if (count($_FILES) > 0)
					{
						// check of wel is geuploadeded

						// verwerk upload
						$file = $this->_handle_file_upload($field);

						// file is FALSE als het is mislukt, anders is het een array

						if (self::isError($file))
						{
                            $error = new Form_Error(E_USER_NOTICE, 'File upload mislukt');
                            $error->setFormFieldName($field->formFieldName);
                            $this->errors_[] = $error;
							$this->errors_[] = $file;
						}
						elseif (NULL === $file)
						{
							if ($field->required) {
                                $error = new Form_Error(
                                    E_USER_NOTICE,
                                    sprintf(
                                        _tr(
                                            '%s is niet geupload',
                                            null,
                                            __FILE__,
                                            __LINE__
                                        ),
                                        $field->label
                                    )
                                );
                                $error->setFormFieldName($field->formFieldName);
                                $this->errors_[] = $error;
							}
							// geen file return -1
							continue; // return NULL; // return -1;
						}
						else
						{
							// jippie gelukt (filename of contents gaan erin)
							$field->value = $file;
							$value = $file['name'];
						}
					}
				}
			}
			else
			{
				//sommige velden komen niet terug in de keys, maar zijn wel verplicht. Als een veld dus niet gevonden is toch controleren of het verplicht is en foutmelding triggeren.
				if ($field->required)
				{
                    $error = new Form_Error(E_USER_NOTICE, sprintf( _tr('%s is niet ingevuld',null,__FILE__,__LINE__), $field->label));
                    $error->setFormFieldName($field->formFieldName);
                    $this->errors_[] = $error;
				}

				//checkbox afvangen
				if ($field instanceOf CheckboxField)
				{
					$field->value = "Unchecked";
				}
			} // end in_array
		}

		// do we have required fields and valid values errors?
		if (count($this->errors_) > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	/**
	 * Remove Field
	 *
	 * @param	Field	$removeField
	 * @return	Boolean
	 */
	public function removeField(Field $removeField)
	{
		// search for given field
		foreach ($this->fields_ as $index=>$field)
		{
			if($removeField->name == $field->name)
			{
				unset($this->fields_[$index]);
				return true;
			}
		}
		return false;
	}

	/**
	 * onLoad event
	 *
	 * @return	Boolean
	 */
	public function onLoad()
	{
		// 	trigger onLoad events
		foreach ($this->fields as $field)
		{
			$result = $field->onLoad();
			if($result !== TRUE)
			{
				$this->errors_[] = $result;
			}
		}
		if(count($this->errors_)==0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * onBeforeHandleRequest event
	 *
	 * @return	Boolean
	 */
	public function onBeforeHandleRequest()
	{
		// 	trigger onBeforeHandleRequest events
		$onBeforeHandleRequest_errors = array();

		foreach ($this->fields as $field)
		{
			$result = $field->onBeforeHandleRequest();
			if($result !== TRUE)
			{
				$onBeforeHandleRequest_errors[] = $result;
			}
		}

		if(count($onBeforeHandleRequest_errors)==0)
		{
			return TRUE;
		}
		else
		{
			$this->errors_ = array_merge($this->errors_, $onBeforeHandleRequest_errors);
			return FALSE;
		}
	}

	/**
	 * onAfterHandleRequest event
	 *
	 * @return	boolean
	 */
	public function onAfterHandleRequest()
	{
		// trigger onAfterHandleRequest events
		foreach ($this->fields as $field)
		{
			$result = $field->onAfterHandleRequest();
			if($result !== TRUE)
			{
				$this->errors_[] = $result;
			}
		}

		if(count($this->errors_)==0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * onSuccessHandleRequest event
	 *
	 * @return	Boolean
	 */
	public function onSuccessHandleRequest()
	{
		// 	trigger onSuccessHandleRequest events
		foreach ($this->fields as $field)
		{
			$result = $field->onSuccessHandleRequest();
			if($result !== TRUE)
			{
				$this->errors_[] = $result;
			}
		}

		if(count($this->errors_)==0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * dit is niet te standaardiseren dus dan maar via een generieke uitzondering files afhandelen
	 * TODO functie fixen
	 * @return NULL || -1 || file
	 */
	private function _handle_file_upload($field)
	{
		// interne container
		$_fields = array();

		// chk of er bestanden nar boven zijn geladen
		if (count($_FILES) == 0)
		{
			return NULL;
		}

		// iets anders dan de formulieren in elkaar geflanst
		foreach ($_FILES as $formname => $fields)
		{
			// juiste formulier bepalen
			if ($formname == $field->form->name)
			{
				if ($_FILES[$formname]['name'][$field->name] == '')
				{
					//continue;
					return NULL;
				}

				switch ($_FILES[$formname]['error'][$field->name] )
				{
					case UPLOAD_ERR_OK:
						// moeten naar bestand of naar db
						$uploaded_file = UploadedFile::getUploadedFile($_FILES[$formname], $field->name); // uploaded file geeft een assoc array van een bestand
						$uploaded_file['formname'] = $this->name;
						break;

					case UPLOAD_ERR_INI_SIZE:
					case UPLOAD_ERR_FORM_SIZE:
                        $error = new Form_Error(E_USER_NOTICE, _tr('Het bestand is te groot', null, __FILE__,__LINE__));
                        $error->setFormFieldName($field->formFieldName);
						return $error;

					case UPLOAD_ERR_PARTIAL:
                        $error = new Form_Error(E_USER_NOTICE, _tr('Het bestand is niet goed geuploadeted',null,__FILE__,__LINE__));
                        $error->setFormFieldName($field->formFieldName);
						return $error;

					case UPLOAD_ERR_NO_FILE:
						return NULL;

					case UPLOAD_ERR_NO_TMP_DIR:
					case UPLOAD_ERR_CANT_WRITE:
					case UPLOAD_ERR_EXTENSION:
                        $error = new Form_Error(E_USER_WARNING, _tr('Kan bestand niet wegschrijven',null,__FILE__,__LINE__));
                        $error->setFormFieldName($field->formFieldName);
						return $error;

					default:
                        $error = new Form_Error(E_USER_WARNING, _tr('Status bestands upload onbekend',null,__FILE__,__LINE__));
                        $error->setFormFieldName($field->formFieldName);
						return $error;
				}
			}
		}

		// move the files. this function is extended by db class to save files in db,
		// normally the form class saves the files to the uploaded folder
		if ($_FILES[$formname]['name'][$field->name] == '')
		{
			return NULL;
		}

		$file = $this->move_uploaded_file_($uploaded_file);
		return $file;
	}

	/**
	 * Move uploaded files to the specified upload folder, returns FALSE at failure
	 *
	 * @param	UploadedFile[]
	 * @return	UploadedFile[]|Boolean
	 */
	protected function move_uploaded_file_($file)
	{
		if (is_uploaded_file($file['tmp_name']))
		{
			// maak dir met instellingen
			if (move_uploaded_file($file['tmp_name'], $this->upload_folder.$file['name']))
			{
				return $file;
			}
		}
		return FALSE;
	}

	/**
	 * Set the folder in which the uploaded files are placed
	 * by default this is set to UPLOAD_DIR
	 *
	 * @param	String
	 */
	public function setUploadFolder($folder)
	{
		$this->upload_folder = $folder;
	}

	/**
	 * Magic string representation
	 */
	public function __toString()
	{
		return coalesce($this->title_, $this->name_);
	}

	/**
	 * Sla de super global request als statische var op
	 *
	 * @param	Array
	 */
	public static function setRequest($array)
	{
		self::$request_ = $array;
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

/**
 * Form methods
 *
 * @package		Brickwork
 * @subpackage	Form
 */
final class FormMethod
{
	const post = 'post';
	const get  = 'get';
}

require_once INCLUDE_DIR.'classes/FormField.package.php';
