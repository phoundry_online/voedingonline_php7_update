<?php
/**
 * Instances of this class are available as $PAGE in the templates
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class Page
{
	/**
	 * @var int Page Id
	 */
	public $id = 0;

	/**
 	 * @var StructureItem the parent of the page
 	 */
	public $structure;

	/**
	 * @var PageType Type of the page
	 */
	public $pageType;

	/**
	 * @var array Quickmenu items
	 */
	public $quick_menu = array();

	/**
	 * @var array Breadcrumb items
	 */
	public $breadcrumb = array();

	/**
	 * @var string Path for the current page
	 */
	public $structurePath = "";

	/**
	 * @var array
	 */
	public $switchlanguages = array();

	/**
	 * @var Auth
	 */
	public $auth;

	/**
	 * @var bool $needsAuthentication
	 */
	public $needsAuthentication = false;

	/**
	 * @var bool $isAuthenticated
	 */
	public $isAuthenticated = false;

	/**
	 * @var bool $isLoggedIn
	 */
	public $isLoggedIn = false;

	/**
	 * @var array
	 */
	public $errors = array();

	/**
	 * @var string The current page title as string
	 */
	public $title = "";

	/**
	 * @var array Extra titles added by modules
	 */
	public $title_array = array();

	/**
	 * @var string Meta discription tag contents
	 */
	public $description = "";

	/**
	 * @var string Meta keywords tag contents
	 */
	public $keywords = "";

	/**
	 * @var string language ISO code usable for setlocale
	 */
	public $lc = "";

	/**
	 * @var array $cContainers The content containers on the page
	 */
	public $cContainers = array();

	/**
	 * @var Site
	 */
	protected $site;

	/**
	 * @var ClientInfo
	 */
	protected $client_info;

	public function __construct($id, StructureItem $structure, PageType $type,
		Site $site, Client_Info $client_info, Auth $auth, $title, $description,
		$keywords, array $content_containers)
	{
		$this->id = (int) $id;
		$this->structure = $structure;
		$this->pageType = $type;
		$this->site = $site;
		$this->client_info = $client_info;
		$this->auth = $auth;
		$this->title = $title;
		$this->description = $description;
		$this->keywords = $keywords;
		$this->cContainers = $content_containers;

		$this->quick_menu = QuickMenu::getQuickMenu($site);
		$this->breadcrumb = BreadCrumb::getBreadCrumb($site, $structure->id);
		$this->switchlanguages = LanguageSwitch::getLanguageSwitch($site, $structure->id);

		$user = $auth->user;

		// Check if the user may see the page, but perhaps should authenticate
		// to get a full experience
		$this->isLoggedIn = $user->isLoggedIn();

		$this->needsAuthentication = $structure instanceof StructureItem &&
			$structure->secure;

		$this->isAuthenticated = $this->isLoggedIn &&
			$structure instanceof StructureItem &&
			$structure->checkAccess($user);
	}

	/**
	 * Renders the page using the WebRepresentation
	 * @param WebRepresentation $rep
	 * @param bool $return
	 */
	public function render(WebRepresentation $rep, $return = true)
	{
		try {
			$home = SiteStructure::getHomeStructure($this->site->identifier);
			$home_id = $home->id;
		}
		catch (Exception $e) {
			$home_id = false;
		}

		$rep->assign('SITE', $this->site);
		$rep->assign('PAGE', $this);
		$rep->assign('CLIENT', $this->client_info);
		$rep->assign('HOME_ID', $home_id);
		$rep->assign('STRUCTURE', $this->structure);
		$rep->assign('SITEVERSION', trim(@file_get_contents(CUSTOM_INCLUDE_DIR.'/VERSION')));

		// fetch module data
		foreach($this->cContainers as $cc_index => $modules) {
			foreach($modules as $m_index => $module) {
				if(!$module->fetch()){
					// detach module from container by id
					$this->cContainers[$cc_index]->detach($m_index);
				}
			}
		}


		// set page template for output
		$rep->setTemplate($this->pageType->identifier.'.tpl');

		if($return) {
			return $rep->out($return);
		}
		$rep->out($return);
	}
}
