<?php
class Loader_Script extends Loader_File
{
	/**
	 * Try find the resource and return its absolute filename
	 * @param string $resource
	 * @return string|bool
	 */
	public function find($resource)
	{
		if(Utility::endsWith($resource, array('.js', '.css', '.less'))) {
			return $this->findFile($resource);
		}
		return false;
	}
}