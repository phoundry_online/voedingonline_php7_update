<?php
interface Loader_Interface
{
	/**
	 * Try find the resource and return its absolute filename
	 * @param $resource
	 * @return string|bool
	 */
	public function find($resource);
}