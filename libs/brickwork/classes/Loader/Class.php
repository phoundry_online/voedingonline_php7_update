<?php
require_once INCLUDE_DIR.'/classes/Loader/File.php';
class Loader_Class extends Loader_File
{
	/**
	 * If the loader is spl_autoload_registered
	 * @var bool
	 */
	protected $_registered;

	/**
	 * Default path
	 * @var string
	 */
	protected $_old_path;

	protected $_allowed_extensions;
	
	/**
	 * @var array
	 */
	protected $_cache;
	
	/**
	 * @var string path to cachefile that should be used
	 */
	protected $_cache_file;
	
	/**
	 * @var bool Should the cache be saved
	 */
	protected $_cache_dirty;
	
	/**
	 * Constructs a new Class loader use an empty string as cachefile to disable
	 * caching the classes
	 * 
	 * @param string $cache_file [optional] location to cachefile
	 * @param int $cache_time [optional] lifetime of cache in seconds
	 */
	public function __construct($cache_file = null, $cache_time = null)
	{
		parent::__construct();
		
		if(null === $cache_file) {
			$cache_file = CACHE_DIR.'/autoloader.cache';
		}
		if(null === $cache_time) {
			$cache_time = isset($GLOBALS['WEBprefs']['Autoloader']['cachetime']) ?
				$GLOBALS['WEBprefs']['Autoloader']['cachetime'] : 600;
		}
		
		$this->_cache_file = $cache_file;
		
		$this->_registered = false;
		$this->_allowed_extensions = array();
		
		if($this->_cache_file && @is_readable($this->_cache_file) &&
				@filemtime($this->_cache_file) > time()-$cache_time) {
			$this->_cache = @include $this->_cache_file;
		}
		 
		if(!is_array($this->_cache)) {
			$this->_cache = array();
		}
			
		// Backwards compat
		if(!isset($GLOBALS['__autoload'])){
			$GLOBALS['__autoload'] = array();	
		}
		
		$this->addExtension('.php');
		$this->addExtension('.class.php');
		$this->addExtension('.package.php');

		$this->_old_path = get_include_path();
	}

	/**
	 * Bij destruct include pad ook weer terug zetten
	 */
	public function __destruct()
	{
		set_include_path($this->_old_path);
		
		if($this->_cache_file && $this->_cache_dirty) {
			$temp = tempnam(dirname($this->_cache_file),
				basename($this->_cache_file));
				
			file_put_contents($temp,
				'<'.'?php return ('.var_export($this->_cache, true).');');
			chmod($temp, 0777);
			rename($temp, $this->_cache_file);
		}
	}

	/**
	 * spl_autoload_register the BrickworkAutoloader
	 * @return bool
	 */
	public function register()
	{
		if(!$this->_registered)
		{
			spl_autoload_register(array($this, 'loadClass'));
			$this->_registered = true;
		}

		return $this->isRegistered();
	}

	/**
	 * spl_autoload_unregister the BrickworkAutoloader
	 * @return bool
	 */
	public function unregister()
	{
		if($this->_registered)
		{
			spl_autoload_unregister(array($this, 'loadClass'));
			$this->_registered = false;
		}

		return $this->isRegistered();
	}

	/**
	 * Return if the loader is currently registered
	 * @return bool
	 */
	public function isRegistered()
	{
		return $this->_registered;
	}

	/**
	 * Loads a class from a PHP file
	 * @param string $class
	 * @return bool
	 */
	public function loadClass($class)
	{
		if (class_exists($class, false) || interface_exists($class, false)) {
			return true;
		}
		
		if(!empty($GLOBALS['__autoload']) && is_array($GLOBALS['__autoload'])) {
			// Paden opslaan in de path array (ze zijn immers toegevoegd)
			$this->_paths = array_merge($GLOBALS['__autoload'], $this->_paths);

			// __autoload leegmaken om te voorkomen dat paden meerdere keren toegevoegd worden
			$GLOBALS['__autoload'] = Array();

			// En include pad opnieuw zetten
			set_include_path(implode(PATH_SEPARATOR, $this->_paths) . PATH_SEPARATOR . $this->_old_path);
		}
		
		if(!array_key_exists($class, $this->_cache)) {
			$this->_cache[$class] = $this->find($class);
			$this->_cache_dirty = true;
		}
		
		if($this->_cache[$class]) {
			// Backwards compat.. Sommige mensen verwachten dat $PHprefs altijd beschikbaar is tussen de begin en eind php tags
			global $PHprefs, $WEBprefs;
			if (is_object($class)) {
				$class = get_class($class);
			}
			include_once $this->_cache[$class];
			return true;
		}
		return false;
	}
	
	/**
	 * Find the file corresponding to the resource
	 * @param sting $resource
	 * @return string|bool
	 */
	public function find($resource)
	{
		if($filename = $this->getFilename($resource)) {
			foreach($this->_allowed_extensions as $ext) {
				if($file = $this->findFile($filename.$ext)){
					return $file;
				}
			}
		}
		return false;
	}
	
	
	/**
	 * Transform a class in the corresponding filename
	 * @param string $class
	 * @return string
	 */
	public function getFilename($class)
	{
		if(preg_match('/[a-zA-Z0-9_]+/', $class)) {
			return str_replace( '_', DIRECTORY_SEPARATOR, $class );
		}
		return false;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Loader/Loader_File#pushPath($path)
	 */
	public function pushPath($path)
	{
		parent::pushPath($path);
		set_include_path( implode(PATH_SEPARATOR, $this->getPaths()) . PATH_SEPARATOR . $this->_old_path );
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Loader/Loader_File#unShiftPath($path)
	 */
	public function unShiftPath($path)
	{
		parent::unShiftPath($path);
		set_include_path( implode(PATH_SEPARATOR, $this->getPaths()) . PATH_SEPARATOR . $this->_old_path );
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Loader/Loader_File#removePath($path)
	 */
	public function removePath($path)
	{
		$cnt = count($this->getPaths());
		parent::removePath($index);
		
		// there were actually paths removed so update the include path
		if($cnt != count($this->getPaths())) {
			set_include_path(implode(PATH_SEPARATOR, $this->getPaths()).
				PATH_SEPARATOR.$this->_old_path);
		}
		return $this;
	}
	
	/**
	 * Add a supported extension
	 * @param $ext
	 * @return bool
	 */
	public function addExtension($ext)
	{
		if(!in_array($ext, $this->_allowed_extensions)) {
			$this->_allowed_extensions[] = $ext;
			return true;
		}
		return false;
	}
	
	/**
	 * Remove a supported extension
	 * @param $ext
	 * @return bool
	 */
	public function removeExtension($ext)
	{
		if(-1 !== ($index = array_search($ext, $this->_allowed_extensions))) {
			unset($this->_allowed_extensions[$index]);
			return true;
		}
		return false;
	}
}
