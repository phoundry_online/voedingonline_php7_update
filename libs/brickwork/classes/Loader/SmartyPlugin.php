<?php
class Loader_SmartyPlugin extends Loader_File
{
	/**
	 * Try find the resource and return its absolute filename
	 * @param string $resource
	 * @return string|bool
	 */
	public function find($resource)
	{
		if($file = $this->getFilename($resource)) {
			return $this->findFile($file);
		}
		return false;
	}
	
	/**
	 * Transforms a resourcename to a relative filename
	 * @return string|bool
	 */
	public function getFilename($resource)
	{
		if('smarty_' == substr($resource, 0, 7)) {
			$filename = str_replace('_', '.', substr($resource, 7)).'.php';
			return $filename;
		}
		return false;
	}
}