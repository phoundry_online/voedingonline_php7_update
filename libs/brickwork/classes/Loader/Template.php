<?php
class Loader_Template extends Loader_File
{
	/**
	 * Try find the resource and return its absolute filename
	 * @param string $resource
	 * @return string|bool
	 */
	public function find($resource)
	{
		if('.tpl' == substr($resource, strrpos($resource, '.'))) {
			return $this->findFile($resource);
		}
		return false;
	}
	
	protected function _relativePath($path)
	{
		$paths = $this->getPaths();
	
		// Check if we have alternates, and if so, use them for matching
		foreach ($paths as $dir) {
			if (substr($path, 0, strlen($dir)) ==  $dir) {
				return substr($path, strlen($dir));
			}
		}
	
		// No matches found, just assume this file had a hard path to
		// a file outside any template_dirs
		return $path;
	}
}