<?php
require_once INCLUDE_DIR.'/classes/Loader/Interface.php';

abstract class Loader_File implements Loader_Interface
{
	/**
	 * Paths to include for the file search
	 * @var array
	 */
	protected $_paths;
	
	public function __construct()
	{
		$this->_paths = array();
	}
	
	/**
	 * Find the file in the configured paths
	 * @param $filename
	 * @return string|bool
	 */
	public function findFile($file)
	{
		foreach($this->_paths as $path)
		{
			if(file_exists($path . DIRECTORY_SEPARATOR . $file))
			{
				return $path.DIRECTORY_SEPARATOR.$file;
			}
		}
		
		return false;
	}
	
	/**
	 * Append path(s) to the search path
	 * @param int|array $index
	 * @param string $path
	 * @return void
	 */
	public function pushPath($path)
	{
		if(!is_array($path)) {
			$path = array($path);
		}
		
		foreach($path as $p) {
			array_push($this->_paths, rtrim($p, '/'));
		}
		
		return $this;
	}
	
	/**
	 * Add path(s) to beginning of the the search path
	 * @param int|array $index
	 * @param string $path
	 * @return void
	 */
	public function unShiftPath($path)
	{
		if(!is_array($path)) {
			$path = array($path);
		}
		
		foreach($path as $p) {
			array_unshift($this->_paths, rtrim($p, '/'));
		}
		
		return $this;		
	}
	
	/**
	 * Remove path(s) from the includepath
	 * @param int|array $index
	 * @return void
	 */
	public function removePath($path)
	{
		if(!is_array($path)) {
			$path = array($path);
		}
		
		foreach($path as $p) {
			if(false !== ($index = array_search(rtrim($p, '/'), $this->_paths, true))) {
				unset($this->_paths[$index]);
			}
		}
	}
	
	/**
	 * Returns the array with paths that have been set with addPath()
	 * @return array
	 */
	public function getPaths()
	{
		return $this->_paths;
	}
}