<?php
/**
* Country <<basetype>>
*
* All the countries in the world
*
* @author Mick van der Most van Spijk
* @last_modified 12/09/2006
* @package None
*/
class Country {
	/**
	* ISO code of the country
	* @var String
	*/
	public $iso		= 'NL';

	/**
	* Name of the country
	* @var String
	*/
	public $name	= '';

	/**
	* Internal list of supported countries
	* @var Array
	*/
	private static $_countries = FALSE;
	private static $_countries_2 = FALSE;

	/**
	* Make a new country object
	*
	* @param String iso
	* @access public
	*/
	public function __construct($iso, $name='') {
		// laad countries automagisch in
		self::_load_countries();
		self::_load_countries_2();

		// zeker weten dat het in kapitaal is
		$iso = strtoupper($iso);

		// checken
		if (strlen($iso) != 2) {
			trigger_error('Not supported ISO country code supplied: '.$iso, E_USER_ERROR);
			exit(1);
		}

		/*if (!isset(self::$_countries[$iso])) {
			trigger_error(sprintf('Unknown country code %s', $iso), E_USER_ERROR);
			exit(1);
		}*/

		// toekennen
		$this->iso	= $iso;
		$this->name = $name;
	}

	/**
	* Laad countries in
	*/
	private static function _load_countries() {
		if (FALSE === self::$_countries) {
			try {
				self::$_countries = i18n::getCountries();
			} catch (Exception $e) {
				// zodat niet opnieuw wordt opgehaald in dezelfde request
				self::$_countries = array();
			}
		}
	}

	/**
	* Laad countries in
	*/
	private static function _load_countries_2() {
		if (FALSE === self::$_countries_2) {
			try {
				self::$_countries_2 = i18n::getCountries2();

			} catch (Exception $e) {
				// zodat niet opnieuw wordt opgehaald in dezelfde request
				self::$_countries_2 = array();
			}
		}
	}

	/**
	* Overload all the countries
	*/
	public static function setCountries($countries) {
		self::$_countries = $countries;
	}

	/**
	* Functie om een lijst van alle landen op te halen
	*/
	public static function getCountries() {
		// zeker weten dat we ze hebben
		self::_load_countries();

		// geef lijst terug
		return self::$_countries;
	}

	/**
	* Haal een lijst met Countyry objecvten op
	*/
	public function getCountryList() {
		self::_load_countries();

		$countries = array();
		foreach (self::$_countries as $iso => $name) {
			$countries[] = new Country($iso, $name);
		}

		return $countries;
	}

	/**
	* Haal een lijst met Countyry objecvten op
	*/
	public function getCountryList2() {
		self::_load_countries_2();

		$countries = array();
		foreach (self::$_countries_2 as $iso => $name) {
			$countries[] = new Country($iso, $name);
		}

		return $countries;
	}

	/**
	* Make a new Country based on its name
	*
	* If the country is found a new Country object will be returned. Else NULL
	* will be returned, indicating that the is no object.
	*
	* @param String
	* @return Country
	* @access public
	* @static
	*/
	public static function getCountryByName($name){
		$iso = array_search($name, self::$_countries);

		if($iso)
			return new Country($iso, $name);

		return NULL;
	}

	/**
	* Check if country is a Country object
	*
	* @param Country
	* @return Boolean
	* @access public
	* @static
	*/
	public static function isCountry($c) {
		return ($c instanceof Country);
	}

	/**
	 * Check if the iso code is known
	 * @param String the iso code to check
	 * @return Boolean true if known
	 * @static
	 */
	public static function validISO($iso) {
		// zeker weten dat we ze hebben
		self::_load_countries();

		// zeker weten dat het in kapitaal is
		$iso = strtoupper($iso);

		// checken
		if (strlen($iso) != 2) {
			//trigger_error('Not supported ISO country code supplied: '.$iso, E_USER_ERROR);
			return false;
		}

		if (!isset(self::$_countries[$iso])) {
			//trigger_error(sprintf('Unknown country code %s', $iso), E_USER_ERROR);
			return false;
		}

		return true;
	}
	/**
	* String representation of this object
	*
	* @return String
	* @access public
	*/
	public function __toString() {
		return $this->name;
	}
}
?>
