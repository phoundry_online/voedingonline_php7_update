<?php
require_once 'Country.class.php';

/**
* Postalcode <<basetype>
*
* @author Mick van der Most van Spijk
* @author J�rgen Teunis
* @author Vincent Poulissen
* @last_modified 31-5-2007
* @package None
* @requires Country
* @version 1.0.1
* @changes:
* - __construct: errorsafhandeling $iso naar $c omgezet
*/
class Postalcode {
	/**
	* Value
	*
	* Valid value of the postalcode
	* @var String
	*/
	public $value;

	/**
	* Country
	*
	* Country object with a country code according to ISO standards
	* @var Counrty
	*/
	private $_country; // iso

	/**
	* ISO Country codes with there regex
	* @var Array
	*/
	private static $_zips = array (
		'AU' => '',
		'BE' => '^[1-9][0-9]{3,4}$',
		'CA' => '^[A-Z][0-9][A-Z] [0-9][A-Z][0-9]$',
		'DE' => '^[0-9]{5}$',
		'ES' => '',
		'FR' => '^(0[1-9]|[1-8][0-9]|9[0-8])[0-9]{3}$',
		'GB' => '^[A-Z]{1,2}[0-9][A-Z0-9]? [0-9][ABD-HJLNP-UW-Z]{2}$',
		'NL' => '^[1-9][0-9]{3}[ ]{0,1}[a-z]{2}$',
		'NO' => '',
		'CH' => '',
		'DK' => '',
		'FI' => '',
		'NZ' => '',
		'GR' => '',
		'IT' => '',
		'JP' => '',
		'MA' => '',
		'ZA' => '',
		'US' => '^[0-9]{5}-[0-9]{4}|[0-9]{5}$',
		'AN' => '',
		'AT' => '',
		'CN' => '',
		'CZ' => '',
		'IE' => '',
		'IL' => '',
		'LU' => '',
		'MX' => '',
		'PT' => '',
		'SE' => '',
		'SR' => '',
		'TR' => '',
	);

	public function __construct($zip, $c) {
		if (!Country::isCountry($c)) {
			trigger_error('Country is van een onjuist type', E_USER_ERROR);
			exit(1);
		}

		if (!isset(self::$_zips[$c->iso])) {
			trigger_error(sprintf('Onbekend land %s', $c->name), E_USER_ERROR);
			exit(1);
		}

		// sta landcodes zonder regex toe, maar check diegene waar wel een regex voor is gedefineerd
		if (!empty(self::$_zips[$c->iso]) && !eregi (self::$_zips[$c->iso], $zip)) {
			trigger_error(sprintf('Zip voldoet niet aan de zipcode specificaties van %s', $c->name), E_USER_ERROR);
			exit(1);
		}

		$this->zip		= $zip;
		$this->_country	= $c;
	}

	public static function isZip($z) {
		return ($z instanceof Postalcode);
	}

	/**
	 * Check if the zip code is valid
	 * @param String the zip code to check
	 * @return Boolean true if valid
	 * @static
	 */
	public static function validZip($zip, $c) {
		if (!Country::isCountry($c)) {
			trigger_error('Country is van een onjuist type', E_USER_ERROR);
			exit(1);
		}

		if (!isset(self::$_zips[$c->iso])) {
			//trigger_error(sprintf('Onbekend land %s', $c->name), E_USER_ERROR);
			return false;
		}

		// sta landcodes zonder regex toe, maar check diegene waar wel een regex voor is gedefineerd
		if (!empty(self::$_zips[$c->iso]) && !eregi (self::$_zips[$c->iso], $zip)) {
			//trigger_error(sprintf('Zip voldoet niet aan de zipcode specificaties van %s', $c->name), E_USER_WARNING);
			return false;
		}
		return true;
	}
	public function toString() {
		return $this->zip;
	}
	public function __toString() {
		return $this->zip;
	}
}
?>
