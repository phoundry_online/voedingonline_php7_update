<?php

class Mail_Content_HTML extends WebRepresentation
{
	protected $_host;
	private $_template;
	
	public function __construct( $template, $language = null )
	{
		if (!isset(Framework::$site) )
			throw new Mail_Content_Exception( "Mail requires a defined site in Framework to work");
			
		if ( is_null($language) )
		{
			if ( isset(Framework::$site->currentLang ) )
				$language = Framework::$site->currentLang->locale;
			else
				$language = setlocale(LC_ALL,"0");
		}
		
		parent::__construct( Framework::$site->template , $language);
		
		$this->_content_type = 'mail';
		$this->register_outputfilter( Array($this, 'smarty_outputfilter_makeAbsolute'));		
		$this->_host = ((Utility::arrayValue($_SERVER,'HTTPS') == 'on') ? 'https://' : 'http://') . Utility::arrayValue($_SERVER,'HTTP_HOST');
		$this->_template = $template;
	}

	public function setHost( $host, $secure = false )
	{
		if ( substr($host,0,7) != 'http://' && substr($host,0,8) != 'https://')
			$this->_host = ($secure ? 'https://' : 'http://') . $host;
		else
			$this->_host = $host;
		return true;
	}
	
	public function __toString()
	{
		return $this->fetch($this->_template);
	}
	
	/**
	 * Returns the parsed template
	 *
	 * @param boolean $return		Ignored, only for interface compatibility
	 * @param string  $cache_id
	 * @return string
	 */
	public function out( $return = true, $cache_id = null, $compile_id = null )
	{
		return $this->fetch($this->_template, $cache_id);
	}
	
	/**
	 * Make sure we have absolute links in our mail's
	 * 
	 * @param string 	$html
	 * @param Smarty 	&$smarty
	 * @return string
	 */
	public function smarty_outputfilter_makeAbsolute( $html, &$smarty )
	{
		return Utility::makeUrlsAbsolute($html, $this->_host);
	}
}
