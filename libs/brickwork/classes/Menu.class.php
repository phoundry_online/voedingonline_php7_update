<?php
require_once INCLUDE_DIR.'/classes/Menu.package.php';

class Menu extends RecursiveMenu {

	public $hasAccess;
	
	public function __construct ($title, $link='', $description='', $nivo=0) {
		$this->title = $title;
		$this->link = $link;
		$this->description = $description;
		$this->parent = NULL;
		$this->hasAccess = true;
		
		if(!isset(self::$_iteration[$nivo])){
			self::$_iteration[$nivo] = 1;
		} else {
			self::$_iteration[$nivo]++;
		}

		// reset onderliggende nivo's
		foreach (self::$_iteration as $iter_nivo => $tellert) {
			if ($iter_nivo > $nivo) {
				self::$_iteration[$iter_nivo] = 1;
			}
		}

		$this->iteration = self::$_iteration[$nivo];
	}


}

?>