<?php
/**
 * Media thats attached to reactions, either images or youtube movies
 *
CREATE TABLE `brickwork_reaction_media` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `reaction_id` int(10) unsigned NOT NULL,
  `mediatype` enum('audio','video','image','youtube','link') NOT NULL default 'image',
  `created_datetime` datetime NOT NULL,
  `url` varchar(255) NOT NULL,
  `original` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `reaction_id` (`reaction_id`)
) DEFAULT CHARSET=utf8
 */
class Reactions_Media extends ORM
{
	protected $_table_name = 'brickwork_reaction_media';
	
	protected $_default_order_by = array('created_datetime', 'ASC');
	
	public function youtubeUrl()
	{
		$video = new Service_External_Youtube_Video($this['url']);
		return $video->videoPageUrl(true);
	}
	
	public function youtubeThumb()
	{
		$video = new Service_External_Youtube_Video($this['url']);
		return $video->getImage(1);		
	}
	
	public function ext()
	{
		$i = strrpos($this['original'], '.');
		if (false !== $i) {
			return substr($this['original'], $i + 1);
		}
		return '';
	}
	
	public function delete($children = true)
	{
		if($children) {
			if('image' == $this['mediatype'] && '' != $this['url'] &&
				is_writable(rtrim(UPLOAD_DIR, '/').$this['url'])) {
					
				unlink(rtrim(UPLOAD_DIR, '/').$this['url']);
				@rmdir(dirname(rtrim(UPLOAD_DIR, '/').$this['url']));
			}
		}
		return parent::delete();
	}
}
