<?php
/**
 * The new config for reactions
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class Reactions_Relationconfig extends ORM
{
	protected $_table_name = 'brickwork_reaction_relationconfig';
	
	/**
	 * @var array Keep all our already loaded instances in here
	 */
	protected static $_instances = array();
	
	/**
	 * Check whenever the the $type is an allowed media type
	 * @param string $type
	 */
	public function mediaTypeAllowed($type)
	{
		$types = explode(',', strtolower($this['mediatypes']));
		return in_array(strtolower($type), $types);
	}
	
	/**
	 * Create singleton records for the config
	 *
	 * @param string $relation_name
	 * @return Reactions_Relationconfig
	 */
	public static function getConfig($relation_name)
	{
		if(!isset(self::$_instances[$relation_name]))
		{
			$config = Reactions::relationConfig()->
				whereIn('relation_name', array($relation_name, ''))->
				orderBy('relation_name', 'DESC')->
			get(true);
			
			self::$_instances[$relation_name] = $config;
		}
		return self::$_instances[$relation_name];
	}
}
