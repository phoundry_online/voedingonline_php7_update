<?php
/**
 * This class is deprecated don't use it anymore!
 * @deprecated
 */
class Reactions_SiteConfig extends ORM
{
	protected $_table_name = 'brickwork_reaction_site_config';
	
	// Keep all our already loaded instances in here
	protected static $_instances = array();
	
	/**
	 * Create singleton records for the config
	 *
	 * @param int $id
	 * @param bool $load
	 * @return BDU_ReactionConfig
	 */
	public static function getConfig($site_identifier, $relation_name = null)
	{
		$key = $site_identifier.$relation_name;
		if(!isset(self::$_instances[$key]))
		{
			$config = Reactions::config();
			$config->customWhere(sprintf(
				'site_identifier = "%s" AND (relation_name = "" OR relation_name IS NULL OR relation_name = "%s")',
				$site_identifier, $relation_name))->
				orderBy('relation_name', 'DESC')->
				limit(1)->
			get(true);
			
			if(!$config->isLoaded()) {
				return null;
			}
			self::$_instances[$key] = $config;
		}
		return self::$_instances[$key];
	}
}
