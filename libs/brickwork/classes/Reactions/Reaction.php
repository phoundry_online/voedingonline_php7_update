<?php
/**
 * Reactions used by the {reactions}{/reactions} smarty block
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 *
 * @property Reactions_Group $group The parent group this reaction belongs to
 * @property ActiveRecordIterator $media The media that belongs to this reaction
 */
class Reactions_Reaction extends ORM
{
	protected $_table_name = 'brickwork_reaction';
	
	/**
	 * Gets copied to the default order by of all the objects
	 * 
	 * @var array
	 */
	protected  $_default_order_by = array('created_on', 'DESC');
	
	/**
	 * Activate the Reaction so it shows up
	 * @return bool
	 */
	public function activate()
	{
		if($this->isLoaded()) {
			$this['active'] = 1;
			if ($this->save()) {
				$res = DB::iQuery(sprintf("
					SELECT
						count(*) AS cnt
					FROM
						`%s`
					WHERE
						group_id = %d
					AND
						active = 1
					",
					DB::quote($this->_table_name),
					$this['group_id']
				));
				
				if($row = $res->first()) {
					$this->group['reaction_count'] = $row->cnt;
					$this->group->save();
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Get a set of reactions for the given relation
	 * @param string $relation_name
	 * @param array $order_by
	 * @param int $limit
	 * @param int $offset
	 * @return ActiveRecordIterator
	 */
	public static function getByRelation($relation_name = array(),
		$order_by = array('created_on' => 'ASC'), $limit = null, $offset = null)
	{
		$relation_name = (array) $relation_name;
		$order_by = (array) $order_by;
		
		$query = Reactions::reaction();
		$query->where('active', 1);
		
		if($relation_name) {
			$relation_sql = array();
			foreach($relation_name as $rel) {
				$relation_sql[] = DB::quoteValue($rel);
			}
			$query->where('group_id','(SELECT id FROM brickwork_reaction_group
				WHERE relation_name IN ('.implode(', ', $relation_sql).'))',
				'IN', false);
		}
		
		foreach($order_by as $key => $order) {
			$query->orderBy($key, $order);
		}
		
		if(null !== $limit) {
			$query->limit($limit, $offset);
		}
		
		return $query->get();
	}
	
	public function __construct($id = null, $load = true)
	{
		// As the group and media class are variable we can't declare them
		$this->_many_to_one['group'] = array(Reactions::getClass('group'), 'group_id');
		$this->_one_to_many['media'] = array(Reactions::getClass('media'), 'reaction_id');
		
		parent::__construct($id, $load);
	}
	
	public function offsetSet($name, $value): void
	{
		switch ($name) {
			case 'ip':
				$value = ip2long($value);
			break;
		}
		
		parent::offsetSet($name, $value);
	}
	
	public function offsetGet($name): mixed
	{
		$value = parent::offsetGet($name);
		switch ($name) {
			case 'ip':
				$value = long2ip($value);
			break;
		}
		
		return $value;
	}
	
	/**
	 * Overload of the default Delete function to also delete all media within this reaction
	 *
	 * @param bool $children Also remove all media in the reaction, default true
	 * @return bool
	 */
	public function delete($children = true)
	{
		if($this->isLoaded() && $children) {
			foreach ($this->media as $child) {
				$child->delete();
			}
		}

		$group = $this->group;
		$active = $this['active'] == 1;
		
		
		// Delete self
		if(parent::delete()) {
			if($active) {
				$group['reaction_count'] = $group['reaction_count'] - 1;
				$group->save();
			}
			return true;
		}
		return false;
	}
}
