<?php
/**
 * Reaction Module
 * 
 * This module handles form submits to create new reactions
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class Reactions_Module extends Webmodule
{
	protected function loadData()
	{
		// Form submit either via ajax or normal
		if(Framework::$contentHandler instanceof ModuleContentHandler) {
			if (Utility::isPosted()) {
				$this->formResponse();
			}
			if (isset(Framework::$contentHandler->path[1])) {
				$this->fileResponse(Framework::$contentHandler->path[1]);
			}
		}
		// Page representation
		return $this->pageResponse();
	}
	
	public function formResponse()
	{
		try {
			$hash = '';
			
			if(isset($_GET['hash'])) {
				$hash = $_GET['hash'];
			}
			// else via path zou mooier zijn
			
			if(empty($hash)) {
				throw new Exception('No reactionHash found');
			}
			$group = Reactions::groupByHash($hash);
		}
		catch(Exception $e) {
			$this->send404();
		}
		
		$post = Utility::arrayValueRecursive($_POST, $group->qsPart(null, true));
		$files = UploadContentHandler_File::getFiles($group['id'], 'Reactionfiles');
		
		$post['email'] = Utility::arrayValue($post, 'imeel');
		$status = $group->postReaction(
			$post,
			$_SERVER['REMOTE_ADDR'],
			$files,
			Utility::arrayValue($_SERVER, 'HTTP_REFERER')
		);
		
		$errors = array();
		if(is_array($status)) {
			$errors = $status;
		}
		else if(false !== $status) {
			UploadContentHandler_File::setFiles(null, $group['id'], 'Reactionfiles');
			$status->activate();
		}
		
		if($errors) {
			$_SESSION['ReactionErrors_'.$group['id']] = $errors;
			$_SESSION['ReactionPosted_'.$group['id']] = $post;
		}
		else {
			$_SESSION['ReactionErrors_'.$group['id']] = array();
			$_SESSION['ReactionPosted_'.$group['id']] = array();			
		}
		
		$url = $_SERVER['HTTP_REFERER'];
		if(false !== ($index = strrpos($url, '#'))) {
			$url = substr($url, 0, $index);
		}
		trigger_redirect($url.'#reactiongroup_'.$group['id']);
	}
	
	public function fileResponse($id)
	{
		if (!$id || !is_numeric($id)) {
			$this->send404();
		}
		
		$file = Reactions::media($id);
		
		if (!$file->isLoaded() || !file_exists(UPLOAD_DIR.'/'.$file['url'])) {
			$this->send404();
		}
		
		if(!empty($file['mime'])) {
			header('Content-type: '.$file['mime']);
		}
		header('Content-Disposition: attachment; filename="'.$file['original'].'"');
		readfile(UPLOAD_DIR.'/'.$file['url']);
		exit;
	}
	
	public function pageResponse()
	{
		return true;
	}
	
	protected function send404()
	{
		header("HTTP/1.0 404 Not Found", true, 404);
		echo 'Not Found';
		exit;
	}
}