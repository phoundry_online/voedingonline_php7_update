<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("ACL is deprecated if you need ACL take a look at ".
		"http://framework.zend.com/manual/en/zend.acl.html instead", E_USER_ERROR);
}

require_once('ACL/Exception.class.php');
require_once('ACL/Object.if.php');
require_once('ACL/Object.class.php');
require_once('ACL/Role.class.php');
require_once('ACL/User.class.php');
require_once('ACL/Cache.class.php');

/**
 * Access Control List
 * An extended way of setting rights on a site. This function works in conjunction with
 * the "secure" method which was available in Brickwork 1.2 and earlier. It allows a more
 * granular access restriction through having an extendable access control list. You
 * can create your own access object which may have relation with normal object and thus
 * have Access control on a level lower than Module.
 *
 * @author		Peter Eussen
 * @package	Brickwork
 * @subpackage	ACL
 * @version	1.0
 * @todo		Build working ACL with default Policy DENY
 */
class ACL
{
	const ALLOW  	= 'allow';
	const DENY	 	= 'deny';
	const ROOT	 	= 'site';
	const SEPERATOR	= '.';

	/**
	 * Singleton var for storing the one and only ACL object
	 *
	 * @var ACL
	 */
	static	private $_instance = null;
	
	/**
	 * Default policy: can be either ACL::ALLOW or ACL::DENY
	 *
	 * @var string
	 */
	protected $_default_policy;
	
	/**
	 * Caching of the ACL Access rights
	 *
	 * @var ACLCache
	 */
	protected $_cache;

	/**
	 * 
	 */
	public function __construct( )
	{
		if ( isset( Framework::$site->identifier) )
			$this->_cache 			= new ACLCache( Framework::$site->identifier );
		else
			$this->_cache 			= new ACLCache( Utility::arrayValue($_GET,'site_identifier') );
			
		$this->_default_policy 	= self::ALLOW;
		$this->_current_user	= Utility::arrayValue($_SESSION,'currentUser');
	}

	public function setPolicy( $newPolicy )
	{
		if ( $newPolicy != self::ALLOW && $newPolicy != self::DENY )
			throw new ACLException( sprintf('invalid policy selected: %s', $newPolicy));
		$this->_default_policy = $newPolicy;
		return true;
	}
	
	public function getPolicy()
	{
		return $this->_default_policy;
	}
	
	/**
	 * Checks if a user has access to a specific object (with a specific role)
	 * With this method you can check if a user has access on a specific object. If the role
	 * is specified it will check only that specific role. If the role is not attached to the
	 * user, the function will always return FALSE.
	 * If one of the roles of the user has access to the specified object, this method will
	 * return TRUE, otherwise it will return FALSE
	 *
	 * @param ACLUser 				$user
	 * @param ACLObjectInterface 	$object
	 * @param ACLRole 				$role
	 * @return boolean
	 */
	public function hasAccess(ACLUser $user, ACLObjectInterface $object, ACLRole $role = null)
	{
		if ( ! is_null($role) )
		{
			if ( ! $user->hasRole($role) )
				return false;
		}

		if ( !$user->isLoggedIn() )
			return false;
			
		if ( ! $object instanceof ACLObject )
			$object = new ACLObject( $object->getInstanceId() );

//		Utility::debug( sprintf('hasAccess: %s on %s', $user->getAuthName(), $object->getInstanceId() ) );
		return $this->_hasAccess($object, $user, $role);
	}
	
	/**
	 * Check if the given role has access on the ACLObject
	 *
	 * @param ACLRole $role
	 * @param ACLObjectInterface $object
	 * @return bool
	 */
	public function roleHasAccess(ACLRole $role, ACLObjectInterface $object)
	{
		if ( ! $object instanceof ACLObject )
			$object = new ACLObject( $object->getInstanceId() );

		return $this->_hasAccess($object, null, $role);
	}

	/**
	 * Grants access to an object to a specific role
	 * Makes sure a user with a specific role can see the object.
	 * 
	 * @param ACLRole 	$role
	 * @param ACLObject $object
	 * @return boolean
	 */
	public function grant(ACLRole $role, ACLObject $object)
	{
		if ( ! $object instanceof ACLObject )
			$object = new ACLObject( $object->getInstanceId() );
		
		$sql = sprintf('
			INSERT INTO
				brickwork_auth_right
				(auth_role_id, auth_object_id, `grant`)
			VALUES
				("%s","%s","%s")
			ON DUPLICATE KEY UPDATE
				`grant` = "%s"
			', 
			DB::quote($role->getId()), 
			DB::quote($object->getAuthObjectId()), 
			DB::quote(self::ALLOW), 
			DB::quote(self::ALLOW)
		);
		
		$rs = DB::iQuery($sql);
				
		if (!self::isError($rs)) {
			$this->_cache->flush($role->getId(), $object->getAuthObjectId());
			return true;
		}
		return false;
	}

	/**
	 * Revokes access on a certain object for a certain role
	 * Makes sure that persons with a specific role will not be able to see the
	 * object and any objects under it.
	 * Will return TRUE if the revoke action succeeded or not.
	 * 
	 * @param ACLRole 				$role
	 * @param ACLObjectInterface 	$object
	 * @return boolean
	 */
	public function revoke(ACLRole $role, ACLObjectInterface $object)
	{
		if ( ! $object instanceof ACLObject )
			$object = new ACLObject( $object->getInstanceId() );
			
		$sql = sprintf('
			INSERT INTO
				brickwork_auth_right
				(auth_role_id, auth_object_id, `grant`)
			VALUES
				("%s", "%s", "%s")
			ON DUPLICATE KEY UPDATE 
				`grant` = "%s"
			', 
			DB::quote($role->getId()), 
			DB::quote($object->getAuthObjectId()), 
			DB::quote(self::DENY), 
			DB::quote(self::DENY)
		);
		
		$rs = DB::iQuery($sql);
		
		if (!self::isError($rs)) {
			$this->_cache->flush($role->getId(), $object->getAuthObjectId());
			return true;
		}
		return false;
	}

	/**
	 * Creates a relation between two objects
	 * When building your site you can build a relation between two classes with this function.
	 * The only restriction is that both should implement the ACLObjectInterface, but if they do
	 * you can use this function to create a child/patent relation.
	 * 
	 * This function will return TRUE if the relation was created, and FALSE if not.
	 * 
	 * @param 	ACLObjectInterface $parent
	 * @param 	ACLObjectInterface $child
	 * @return boolean
	 */
	public function setRelation( $parent, $child )
	{
		$parent = new ACLObject( $parent->getInstanceId(), get_class($parent) );
		$child  = new ACLObject( $child->getInstanceId(), get_class($child) );
		
		return $parent->addChild( $child );
	}
	
	/**
	 * protected function which does the actual checking of access.
	 * This function will do the actual checking of access for a specific user.
	 * If the 3rd argument was not set, it will use all the roles of the user and call
	 * itself with all those roles. If one role has access, then the user has access.
	 * 
	 * @param ACLObjectInterface $object
	 * @param ACLUser $user
	 * @param ACLRole $role
	 * @return boolean
	 * @access protected
	 */
	protected function _hasAccess(ACLObjectInterface $object, ACLUser $user = null, ACLRole $role = null)
	{
		if(!is_null($role)) {
			$roles = Array($role);
		} else if(!is_null($user)) {
			$roles = $user->getRoles(); 
		}
		
		if(empty($roles)) {
			return false;
		}
		
		$access = Array( 0 );
		
		foreach ( $roles as &$curRole )
		{
			$roleid   = $curRole->getId();
			$objectid = $object->getAuthObjectId();

			if ( !is_null($this->_cache) && $this->_cache->cached($roleid, $objectid) )
			{
				//Utility::debug( sprintf("Cache hit role %s on object %s", $roleid, $objectid));
				$hasAccess = $this->_cache->get( $roleid, $objectid );
				
				if ( $hasAccess )
					return true;
				else
				{
					array_push($access, $hasAccess);
					continue;
				}
			}
			//else
			//	Utility::debug( sprintf("Cache miss role %s on object %s", $roleid, $objectid));
			
			// Check of we een object hebben zonder access
			if ( is_null($objectid) )
			{
				logDebug( sprintf('Access check on non-existing object: %s, please add', $object->getInstanceId() ) );
				array_push( $access, 0);
				continue;				
			}
			
			//Utility::debug( sprintf('Check access for role %s on object %s', $curRole->getName(), $object->getInstanceId() ));
			$parent 	= $object->getParent();
			$hasAccess	= 0;

			if ( is_null($parent) )
				$parentAccess = ($this->_default_policy == self::ALLOW); 
			else
				$parentAccess = $this->_hasAccess($parent, $user, $curRole);
			
			if ( $parentAccess )
			{
				$res = DB::iQuery('SELECT `grant` FROM brickwork_auth_right WHERE auth_role_id = ' . (int) $roleid . '   AND auth_object_id = ' . (int) $objectid);
				
				if ( ! self::isError($res) )
				{
					$policy = $res->first();
					
					if ( $policy === false )
						$hasAccess = $parentAccess; 
					else
					{
						$hasAccess = (int) ($policy->grant == self::ALLOW);
						
						if ( !is_null($this->_cache))
						{
							$this->_cache->store( $roleid, $objectid, $hasAccess );
					//		Utility::debug( sprintf("Cache store role %s on object %s / %s", $roleid, $objectid, $hasAccess));
						}
					}
				} 
				else
					$hasAccess = $this->_default_policy;
			}
			
			//Utility::debug("Access for " . $curRole->getName() . ' on ' . $object->getInstanceId() . ' = ' . $hasAccess );
			if ( $hasAccess )
			{
				if ( !is_null( $this->_cache ))
					$this->_cache->store( $roleid, $objectid, $hasAccess );
				return 1;
			}
			array_push($access, $hasAccess);
		}
		
		//Utility::debug( var_export($access,true));
		return (max($access) == 1);
	}
	
	/**
	 * Singleton
	 *
	 * @return ACL
	 */
	public static function singleton()
	{
		if ( is_null( self::$_instance ))
			self::$_instance = new self();
		return self::$_instance;
	}
}
