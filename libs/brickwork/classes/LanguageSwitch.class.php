<?php
/**
 * Language Switch Module for Brickwork
 *
 * @author  Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 * @version 0.2
 * @package	Brickwork
 * @last_modified   17-12-2009
 *
 * @property string $code Code of the language
 * @property bool $default Language is the default site language
 * @property bool $current Language is the language currently in use
 * @property bool $available Language is available
 * @property string $name Name of the page
 */
class LanguageSwitch
{
	/**
	 * Defines the code of the language
	 *
	 * @var string
	 */
	protected $_code;

	/**
	 * Defines if the language is the default site language
	 *
	 * @var boolean
	 */
	protected $_default;

	/**
	 * Defines if the language is the language currently in use
	 *
	 * @var boolean
	 */
	protected $_current;

	/**
	 * Defines if the language is available
	 *
	 * @var boolean
	 */
	protected $_available;

	/**
	 * The name of the page
	 */
	protected $_name;

	public function __construct($code, $name, $default = false, $current = false, $available = false)
	{
		$this->_code = $code;
		$this->_name = $name;
		$this->_default = $default;
		$this->_current = $current;
		$this->_available = $available;
	}

	public function __get( $attr )
	{
		switch ($attr) {
			case "code":		return $this->_code;
			case "default":		return $this->_default;
			case "current":		return $this->_current;
			case "available":	return $this->_available;
			case "name":		return $this->_name;
			default:
				return null;
		}
	}

    /**
     * Get available languages for the current structure
     *
     * @param	Site	$site
     * @param	int		$structure_id
     * @return	array	Array with LanguageSwitch objects
     */
	static function getLanguageSwitch(Site $site, $structure_id)
	{
		$languages = array();

		if (!$site->isMultiLingual()) {

			return $languages;
		}

		$site_languages = Site_Language::getUserSelectableLanguages($site->identifier);

		$res = DB::iQuery(sprintf("
			SELECT
				lang_id
			FROM
				brickwork_page
			WHERE
				site_structure_id = %d
			",
			$structure_id
		));

		$page_lang_ids = array();
		while ($res->current()) {
            $page = $res->current();
			$page_lang_ids[] = is_null($page->lang_id) ? 0 : $page->lang_id;
		    $res->next();
		}

		foreach ($site_languages as $lang) {
			$lang_id = $lang->getId();
			$available = false;
			$name = "";
			if (in_array($lang_id, $page_lang_ids)) {
			    $name = SiteStructure::getPathByStructureId($structure_id, $site->identifier, $lang_id);
				$available = true;
			}
			else if (in_array(0, $page_lang_ids)) {
			    $name = SiteStructure::getPathByStructureId($structure_id, $site->identifier);
				$available = true;
			}
			$languages[] = new self(
				$lang->code,
				$name,
				$lang_id == $site->defaultLang->getId(),
				$lang_id == $site->currentLang->getId(),
				$available
			);
		}

		return $languages;
	}
}
