<?php
/**
 * Ajax content handler
 * 
 * to test if module is called by the AjaxContentHandler
 * use if(Framework::$contentHandler isntanceof AjaxContentHandler)
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 */
class AjaxContentHandler extends ModuleContentHandler 
{

}
