<?php
require_once INCLUDE_DIR.'/functions/common.php';

/**
* Error
*
* @package	Error
* @version	1.1.0
* @author	Mick van der Most van Spijk
*
* @property int $errno Error code
* @property string $errstr Error description
* @property string $errfile File in which the error occured
* @property int $errline Line in the file on which the error occured
* @property array $backtrace Full backtrace to where the error occured
*/
class Error {
	protected $errno_;
	protected $errstr_;
	protected $errfile_;
	protected $errline_;

	protected $backtrace_;
	protected $source_;

	public static $cnt = 0;

	public function __construct($errno, $errstr, $errfile='', $errline='', $backtrace=array()) {
		$this->errno_   = $errno;
		$this->errstr_  = $errstr;
		$this->errfile_ = $errfile;
		$this->errline_ = $errline;
		$this->backtrace_ = $backtrace;

		self::$cnt++;
	}

	/**
	* Test if an object is of type error
	*
	* @param Error
	* @return Boolean
	*/
	public static function isError(Exception &$e) {
		$v = PHP_VERSION;

		switch (TRUE) {
			// old php
			case ($v < 4.2):
				if ((get_class($e) == 'error')
				||	(is_subclass_of($e, 'error'))) {
					return TRUE;
				}
				break;

			// php 4.2 < 5
			case ($v >= 4.2 && $v < 5):
				return (is_a($e, 'Error'));
				break;

			// php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
		}

		// not an error
		return FALSE;
	}

	public function __get($name) {
		switch ($name) {
			case 'errno':
				return $this->errno_;
				break;

			case 'errstr':
				return $this->errstr_;
				break;

			case 'errfile':
				return $this->errfile_;
				break;

			case 'errline':
				return $this->errline_;
				break;

			case 'backtrace':
				return (array) $this->backtrace_;
				break;

			case 'source':
				return $this->source_;
				break;

			switch ($name) {
				default:
					throw new AttributeNotFoundException (sprintf('Attribute %s not found', $name));
					break;
			}
		}
	}

	public function __set($name, $value) {
		throw new AttributeSettingNotAllowedException (sprintf('Setting %s is not allowed', $name));
	}
}

class DBError extends Error {}

class MySQLError extends DBError {
	private static $_error_codes = array();

	public function __construct($errno, $errstr, $errfile='', $errline='', $backtrace=array()) {
		static $included = FALSE;

		if (!$included) {
			include_once INCLUDE_DIR . '/classes/Error/MySQLErrorCodes.include.php';
			self::$_error_codes = $mysql_server_error_codes_and_messages + $mysql_client_error_codes_and_messages;
			$included = TRUE;
		}

		parent::__construct($errno, $errstr, $errfile, $errline, $backtrace);
	}

	public function __get($name) {
		switch ($name) {
			case 'errstr':
				if ($this->errno_ == NULL || !in_array($this->errno_, array_keys(self::$_error_codes))) {
					$errstr = $this->errstr_;
				} else {
					$errstr = self::$_error_codes[$this->errno_];
				}

				// probeer mysql-melding te ont-nerden en return die
				return coalesce($errstr, $this->errstr_);
				break;

			default:
				try {
					return parent::__get($name);
				} catch (AttributeNotFoundException $e) {
					throw $e;
				}
				break;
		}
	}

	public function __set($name, $value) {
		switch ($name) {
			default:
				try {
					return parent::__set($name, $value);
				} catch (AttributeNotFoundException $e) {
					throw $e;
				}
				break;
		}
	}
}
?>
