<?php

class Client_Capabilities
{
	public	$frames;
	public	$iframes;
	public	$tables;
	public	$cookies;
	public	$javascript;
	public	$applets;
	public	$css;
	public	$cssVersion;
	public	$https;
	public	$fileUpload;
	public	$ajaxXHRType;
	public	$flashLite;
	public	$flashLiteStandalone;
	public	$jpg;
	public	$gif;
	public	$bmp;
	public	$wmbp;
	public	$png;
	public	$animatedGif;
	public	$width;
	public	$height;
	
	public function setDefaults( $mobile = false )
	{
		$this->frames 		= ($mobile ? 0 : 1);
		$this->iframes		= 1;
		$this->tables 		= 1;
		$this->cookies		= 1;
		$this->javascript	= ($mobile ? 0 : 1);
		$this->applets		= ($mobile ? 0 : 1);
		$this->css			= 1;
		$this->cssVersion	= 2;
		$this->https		= ($mobile ? 0 : 1);
		$this->fileUpload	= 0;
		$this->flashLite	= 1;
		$this->flashLiteStandalone = 1;
		$this->jpg			= 1;
		$this->gif			= ($mobile ? 0 : 1);
		$this->png			= 1;
		$this->animatedGif	= 1;
		$this->bmp			= $mobile ? 1 : 0;
		$this->wbmp			= 0;
		$this->width		= ($mobile ? 240 : 1024);
		$this->height		= ($mobile ? 400 : 768);
	}
}