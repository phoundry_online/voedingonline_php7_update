<?php

/**
 * Client info
 *
 * Info about the client that uses the framework
 *
 * @package	Framework
 * @version	1.0
 * @author	J�rgen Teunis
 * @abstract
 */

abstract class Client_Info {
	private $_is_test = FALSE;

	function __construct($data){

	}

	function isTestClient(){
		return $this->_is_test;
	}

	abstract function getType();

	static function factory($type, $params){
		switch($type){
			case 'web':
				return new Client_Info_Web($params);
			case 'cli':
				return new Client_Info_CLI($params);
			break;
		}
	}
}
