<?php

/**
 * Website Client info
 *
 * Info about the webclient accessing the framework
 *
 * @package	Framework
 * @version	1.0
 * @author		J�rgen Teunis
 * @author		Peter Eussen
 */

class Client_Info_Web extends Client_Info
{
	/**
	 * @example       "text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,;q=0.5"
	 * @var Array
	 */
	public $http_accept;

	/**
	 * @example       "ISO-8859-1,utf-8;q=0.7,*;q=0.7"
	 * @var Array
	 */
	public $http_accept_charset;

	/**
	 * @example       "gzip,deflate"
	 * @var Array
	 */
	public $http_accept_encoding;

	/**
	 * @example       "nl,en-gb;q=0.7,en;q=0.3"
	 * @var Array
	 */
	public $http_accept_language = array();

	/**
	 * A list of content types the client prefers
	 *
	 * @var Array
	 */
	public   $_preferredContent;

 	/**
 	 * Client mode is test client
 	 *
 	 * @var boolean
 	 */
	protected	$_isTest;

	/**
	 * Client is a mobile client
	 *
	 * @var boolean
	 */
	protected	$_isMobile;


	/**
	 * A list of known preferences
	 *
	 * @var Cookie
	 */
	public		$preferences;

	/**
	 * Capabilities of the client
	 *
	 * @var Client_Capabilities
	 */
	public		$capabilities;

	/**
	 * List of mobile user agent identifiers
	 *
	 * @var Array
	 */
	static	protected 	$_mobileAgents = array(
												'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
												'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
												'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
												'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
												'newt','noki','palm','pana','pant','phil','play','port','prox',
												'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
												'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
												'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
												'wapr','webc','winw','winw','xda','xda-');

	/**
	 * The mapping from content-type to a content kind
	 *
	 * @var	Array
	 */
	static protected 	$_kindMapping = Array(
												"application/xhtml+xml"=>'xhtml',
												"*/*"=>'xhtml',
												"text/plain"=>'plain',
												"text/html"=>'html',
												"text/xml"=>'xml',
												"application/xml"=>'xml',
												"text/vnd.wap.wml"=>'wml',
												"text/chtml" => "chtml"
											);

	function __construct($data)
	{
		global $WEBprefs;

		$this->http_accept = Array();
		$this->http_accept_charset = Array();
		$this->http_accept_encoding = Array();
		$this->http_accept_language = Array();
		$this->_preferredContent = Array();
		$this->_isTest = false;
		$this->_isMobile = false;
		$this->capabilities = null;

		if (isset($data['HTTP_ACCEPT'])) {
			$this->http_accept = explode(',', $data['HTTP_ACCEPT']);
		}

		if (isset($data['HTTP_ACCEPT_CHARSET'])) {
			$this->http_accept_charset = explode(',', $data['HTTP_ACCEPT_CHARSET']);
		}

		if (isset($data['HTTP_ACCEPT_LANGUAGE'])) {
			$this->http_accept_language = explode(',', $data['HTTP_ACCEPT_LANGUAGE']);
		}

		if (isset($data['HTTP_ACCEPT_ENCODING'])) {
			$this->http_accept_encoding = explode(',', $data['HTTP_ACCEPT_ENCODING']);
		}

		$this->_isTest = FALSE;


		if ( isset( $WEBprefs['Client.Preferences.Liftime'] ) )
			$this->preferences = Cookie::factory('preferences', true, (int)$WEBprefs['Client.Preferences.Liftime'] );
		else
			$this->preferences = Cookie::factory('preferences', true, Cookie::LIFETIME_MONTH );

		$this->_checkBrowserType( $data );
	}

	/**
	 * Returns a testmode indicator flag
	 *
	 * @param 	string $sid
	 * @return 	boolean
	 */
	function isTestClient($sid = NULL)
	{
		return $this->_isTest;
	}

	/**
	 * Returns a mobile device indicator flag
	 *
	 * @return boolean
	 */
	function isMobile()
	{
		return $this->_isMobile;
	}

	/**
	 * Returns an Array of preferred output content types
	 * The array should be read front to back, so first in array, is the one most preferred
	 * Possible options are: wap, chtml, iphone, ihtml, html. These should correspond to the
	 * directories in the template dir.
	 *
	 * @return Array
	 */
	public function getPreferredContentKind()
	{
		return $this->_preferredContent;
	}

	/**
	 * Checks what kind of browser we have
	 * Based on various sources on the internet. Basically, for mobile devices, do not
	 * trust the accept only, but also look at the agent. I introduced the mobile tag
	 * to indicate mobile (compact) versions of a html page (MHTML stands for multipart
	 * html). It also has the ability to find iphone and i-Mode browsers.
	 *
	 * @param 	Array $data
	 * @return	null
	 */
	protected function _checkBrowserType( $data )
	{
		global $WEBprefs;

		if ( !is_null( $this->preferences->isMobile) &&
			 !is_null( $this->preferences->preferredContent ) &&
			 !is_null( $this->preferences->capabilities) )
		{
			$this->_isMobile 		 = $this->preferences->isMobile;
			$this->_preferredContent = $this->preferences->preferredContent;
			$this->capabilities		 = $this->preferences->capabilities;
			//logDebug("Using cache");
			return true;
		}

		$userAgent 		 = (isset($data['HTTP_USER_AGENT']) ? $data['HTTP_USER_AGENT'] : '' );
		$mimeSupport 	 = false;
		$this->_isMobile = false;

		// Shortcut voor agents die zichzelf niet identificeren en ook geen accepts doorgeven
		if ( $userAgent == '' && count($this->http_accept) == 0 )
		{
			$this->_isMobile 		= false;
			$this->_preferredContent= Array('html','xhtml');
			$this->capabilities		= new Client_Capabilities();
			$this->capabilities->setDefaults();
			return true;
		}

		if ( isset($WEBprefs['Client.Service.UserAgent'] ) )
		{
			//logDebug("using service");
			$mimeSupport = $this->_loadInfoFromService($WEBprefs['Client.Service.UserAgent'],$userAgent);
		}

		if ( $mimeSupport === false )
		{
			// Altijd uitgaan dat we "iets" aka "niets" hebben
			$mimeSupport = Array();

			//logDebug("Using fallback");
			$q = 1;

			// Wap capable
			if ( isset( $data['HTTP_X_WAP_PROFILE'] ) ||
				 in_array( 'application/vnd.wap.wml', $this->http_accept ) ||
				 in_array( 'text/vnd.wap.wml', $this->http_accept )
			   )
			{
				$this->_isMobile = true;
				$mimeSupport['wml'] = $q;
				$q -= 0.1;
			}

			// iPhone browser, special case for a special device ;)
			// PE: 2010-02 - Added Android based system as "iphone" compat
			if ( $this->_isIphone($userAgent) ||
				 $this->_isAndroid($userAgent) ||
				 $this->_isOperaMobile($userAgent) )
			{
				$this->_isMobile       = true;
				$mimeSupport['iphone'] = $q;
				$q -= 0.1;
			}

			// Old iMode stuff, sill popular in japan
			if ( $this->_isImode($userAgent ) )
			{
				$this->_isMobile = true;
				$mimeSupport['chtml'] = $q;
				$q -= 0.1;
			}

			// All other mobile devices we can check for quickly
			$shortua = strtolower(substr($userAgent,0,4) );

			if ( isset($data['HTTP_PROFILE']) ||
				 ( $c = stristr( $userAgent, 'windows ce' )) ||
				 preg_match('/(up.browser|up.link|palm os|hiptop|xiino|blazer|iemobile|3g_t|mmp|symbian|smartphone|midp|wap|phone|blackberry)/i',$userAgent ) ||
				 in_array( $shortua, self::$_mobileAgents )
				)
			{
				$this->_isMobile = true;

				// Windows mobile has a nasty habbit of saying it does not understand html
				if ( isset($c) && $c )
				{
					$mimeSupport['mobile']	= $q;
					$mimeSupport['html'] 	= $q - 0.1;
					$q -= 0.2;
				}
			}

			if ( strpos( strtolower($userAgent), 'operamini') !== false )
			{
				$this->_isMobile = true;
			}

			$this->capabilities = new Client_Capabilities();
			$this->capabilities->setDefaults( $this->_isMobile );
		}
		else
			$q = 1 - (count($mimeSupport) * 0.1);


		foreach( $this->http_accept as $accept)
		{
			// split type with prio
			$tmp = explode(';', $accept);

			// we dont support images (yet)
			if(strpos($tmp[0],'image')!==FALSE) {
				continue;
			}

			// prioritize anything with no prio
			if ( $tmp[0] == 'text/xml' || $tmp[0] == 'application/xml' || $tmp[0] == 'text/plain' )
			{
				$mq = $q - 0.2;
			}
			else
			{
				$mq = ( !empty($tmp[1]) ? (float)str_replace('q=','',$tmp[1]) : $mq = $q );
				$q -= 0.1;
			}

			if(isset(self::$_kindMapping[$tmp[0]]) && !isset($mimeSupport[self::$_kindMapping[$tmp[0]]]))
			{
				if ( self::$_kindMapping[$tmp[0]] == 'html' && $this->_isMobile )
				{
					$mimeSupport['mobile'] = $mq;
					$mimeSupport['html']  = $mq - 0.1;
					$q -= 0.1;
				}
				else
					$mimeSupport[self::$_kindMapping[$tmp[0]]] = $mq;
			//	$cleanAcceptList[$tmp[0]] = 10*$q;
			}
		}

		arsort($mimeSupport);

		// No mobile device, use HTML
		$this->_preferredContent = array_keys($mimeSupport);
		$this->preferences->isMobile 		 = $this->_isMobile;
		$this->preferences->preferredContent = $this->_preferredContent;
		$this->preferences->capabilities	 = $this->capabilities;
		//logDebug( var_export($this->preferences,true) );
		$this->preferences->update();
		return true;
	}

	protected function _loadInfoFromService( $url, $agent )
	{
		try
		{
			$uaclient = new Service_Client( $url );
			$uainfo   = $uaclient->GetSimpleUserAgentInformation( $agent );

			if ( isset($uainfo['content']) )
			{
				$this->_isMobile = (bool)Utility::arrayValue($uainfo['content'], 'is_mobile_device' );

				$q 			 = 1;
				$mimeSupport = Array();

				if ( Utility::arrayValue($uainfo['content'], 'supports_wap' )  )
				{
					$mimeSupport['wml'] = $q;
					$q -= 0.1;
				}

				// iPhone browser, special case for a special device ;)
				if ( $this->_isIphone($agent) )
				{
					$mimeSupport['iphone'] = $q;
					$q -= 0.1;
				}

				// Old iMode stuff, sill popular in japan
				if ( $this->_isImode($agent ) )
				{
					$mimeSupport['chtml'] = $q;
					$q -= 0.1;
				}

				if ( $this->_isMobile )
				{
					$mimeSupport['mobile']	= $q;
					$mimeSupport['html'] 	= $q - 0.1;
				}

				//logDebug( var_export($uainfo,true) );

				$this->capabilities = new Client_Capabilities();
				$this->capabilities->frames 	= Utility::arrayValue($uainfo['content'],'supports_frames');
				$this->capabilities->iframes	= Utility::arrayValue($uainfo['content'],'supports_iframes');
				$this->capabilities->tables		= Utility::arrayValue($uainfo['content'],'supports_tables');
				$this->capabilities->cookies	= Utility::arrayValue($uainfo['content'],'supports_cookies');
				$this->capabilities->javascript	= Utility::arrayValue($uainfo['content'],'supports_javascript');
				$this->capabilities->applets	= Utility::arrayValue($uainfo['content'],'supports_javaapplets');
				$this->capabilities->jpg		= Utility::arrayValue($uainfo['content'],'supports_jpg');
				$this->capabilities->png		= Utility::arrayValue($uainfo['content'],'supports_png');
				$this->capabilities->gif		= Utility::arrayValue($uainfo['content'],'supports_gif');
				$this->capabilities->bmp		= Utility::arrayValue($uainfo['content'],'supports_bmp');
				$this->capabilities->wbmp		= Utility::arrayValue($uainfo['content'],'supports_wbmp');
				$this->capabilities->animatedGif= Utility::arrayValue($uainfo['content'],'supports_gif_animated');
				$this->capabilities->https		= Utility::arrayValue($uainfo['content'],'supports_https');
				$this->capabilities->css		= Utility::arrayValue($uainfo['content'],'supports_css');
				$this->capabilities->cssVersion	= Utility::arrayValue($uainfo['content'],'css_version');
				$this->capabilities->flashLite	= Utility::arrayValue($uainfo['content'],'supports_flashlite_browser');
				$this->capabilities->width		= Utility::arrayValue($uainfo['content'],'display_resolution_width');
				$this->capabilities->height		= Utility::arrayValue($uainfo['content'],'display_resolution_height');
				$this->capabilities->flashLiteStandalone	= Utility::arrayValue($uainfo['content'],'supports_flashlite_standalone');

				return $mimeSupport;

			}
			else
				logDebug("UAinfo did not return an array");
		}
		catch ( Exception $e )
		{
			logDebug( "UserAgent Service Error: " . $e->getMessage() );
			return false;
		}
		return false;
	}

	protected function _isIphone( $ua )
	{
		return (FALSE !== stripos( $ua, 'iphone') || FALSE !== stripos($ua,'ipod'));
	}

	protected function _isAndroid($ua)
	{
		return false !== stristr($ua, 'android');
	}

	protected function _isOperaMobile( $ua )
	{
		return (FALSE !== stripos( $ua , 'opera mobi'));
	}

	protected function _isImode( $ua )
	{
		return (substr( $ua ,0,7) == 'DoCoMo/');
	}

	public function getType()
	{
		return 'web';
	}
}
