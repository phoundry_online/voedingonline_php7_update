<?php

class Client_Info_CLI extends Client_Info
{
	function __construct($data)
	{
		parent::__construct( $data );
		$this->_is_test = TESTMODE;
	}

	public function getType()
	{
		return 'cli';
	}

}