<?php

require_once( BRICKWORK_DIR . '/libs/phpMailer/class.phpmailer.php' );

/**
 * Wrapper class for the SMTP mail sending class
 * This class serves as a wrapper for the commenly used
 * SMTP class. Though that one works fine, it is a bit
 * elaborate to use. Therefore I introduce this class,
 * which should make sending of mails a bit easier for
 * all non-SMTP guru's out there
 * This class also acts as a Subject for the Observer
 * pattern so you can attach your own logging system
 * to this class.
 *
 * @author			Peter Eussen
 * @package		Brickwork
 * @version		0.3
 * @copyright	Web Power (http://www.webpower.nl)
 */
class Mail extends PHPMailer implements splSubject
{
	const FAILED = false;
	const SUCCES = true;
	const UNKNOWN = null;

	public $status;

	protected $_observer;

	public function __construct()
	{
		global $WEBprefs, $PHprefs;

		$this->IsSMTP();
		$port = coalesce(
			Utility::arrayValue($WEBprefs, 'Mail.SMTPport'),
			Utility::arrayValue($PHprefs, 'SMTPport'),
			25
		);
		$this->Host = coalesce(
			Utility::arrayValue($WEBprefs, 'Mail.SMTPServer'),
			Utility::arrayValue($PHprefs, 'SMTPserver'),
			'localhost'
		) . ':' . $port;
		$this->Port = $port;

		$this->_observer = Array();
		$this->_vars	 = Array();
		$this->status	 = self::UNKNOWN;
		$this->Encoding  = 'quoted-printable';
		$this->CharSet = Utility::arrayValue($PHprefs, 'charset', 'utf-8');
	}

	public function setCredentials( $username, $password )
	{
		$this->SMTPAuth = true;
		$this->Username = $username;
		$this->Password = $password;
	}

	public function __get( $attr )
	{
		$attr = ucfirst($attr);

		if (isset($this->{$attr}))
			return $this->{$attr};
		return null;
	}

	public function __set( $attr, $val )
	{
		switch ($attr) {
		case 'html':
			if (!empty($this->Body))
				$this->AltBody = $this->Body;
			$this->Body = $val;
			$this->IsHTML(true);
			break;
		default:
			$attr = ucfirst($attr);
			$this->{$attr} = $val;
		}
		return true;
	}

	/**
	 * Sets the from name & email adress
	 *
	 * @param string 		$email
	 * @param string|NULL 	$name
	 */
	public function setFrom( $email, $name = null, $auto = true )
	{
		$this->From 	= $email;
		$this->FromName = $name;
		return true;
	}

	/**
	 * Attaches an observer for this object
	 * Note: there can be only one observer of a specific class. If you for
	 * example add two myObserver classes, the first attached observer will
	 * be replaced by the second one. This method will return the old observer
	 * if any was found, or NULL if none was set.
	 *
	 * @param 	splObserver 		$observer
	 * @return splObserver|NULL
	 */
	function attach(SplObserver $observer): void
	{
		$old = Utility::arrayValue($this->_observers, get_class($observer));
		$this->_observer[get_class($observer)] = $observer;
	}

	/**
	 * Removes an observer from the list
	 *
	 * @param 	splObserver $observer
	 * @return boolean
	 */
	function detach(SplObserver $observer): void
	{
		if (isset($this->_observer[get_class($observer)])) {
			unset($this->_observer[get_class($observer)]);
		}
	}

	/**
	 * Notifies the observers of an event
	 */
	public function notify(): void
	{
		foreach ($this->_observer as $observer) {
			$observer->update($this);
		}
	}

	public function Send()
	{
		$this->status = parent::Send();
		$this->notify();
		return $this->status;
	}


}
