<?php
/**
 * MailSelectField
 *
 * Build a mailselectfield
 *
 * @author Mark Veldhuizen
 * @version 1.0.5
 * @last_modified 18-12-2007
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: MailSelectField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class MailSelectField extends SelectField
{
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#__get()
	 */
	public function __get($name) {
		switch ($name)
		{
			case 'value':
				$retval = array();
				foreach($this->options_ as $option)
				{
					if($option->selected)
					{
						$retval[] = $option->name;
					}
				}
				return $retval;
				
			case 'selectedValue':
				$retval = array();
				foreach($this->options_ as $option)
				{
					if($option->selected && $option->value != '')
					{
						$retval[] = $option->value;
					}
				}
				return implode(', ', $retval);
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
		}
	}
}

