<?php
/**
 * Veld samengesteld uit meerdere velden
 *
 * @author		Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: CompositeField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class CompositeField extends FieldDecorator
{
	/**
	 * interne dingest waar alle velden in worden bijgehouden
	 *
	 * @var	Array
	 */
	protected $fields_ = array();
	
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
	}
	
	public function setData()
	{
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#chkValue()
	 */
	public function chkValue()
	{
		return TRUE;
	}
}
