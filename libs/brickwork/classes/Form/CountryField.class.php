<?php
/**
 * Country selectiefield
 *
 * @author		Mick van der Most van Spijk
 * @author		Mark Veldhuizen
 * @copyright	Web Power 2006
 * @version		1.0.1
 * @last_modified 18-12-2007
 *
 * @see Country.class.php
 *
 * @package		Brickwork
 * @subpackage	Form
 */
class CountryField extends SelectField
{
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		// roep parent aan, deze roept een setter aan en selecteerd de ingestelde waarde
		parent::__construct($field);
		
		// laad alle landen in
		$countries = Country::getCountryList2();
		
		// sorteer op name
		oksort($countries, 'name');
		
		$options = array();
		foreach ($countries as $country)
		{
			$options[] = new FieldOption($country->iso, $country->name);
		}
		
		$this->options = $options;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			/*case 'value':
				$this->value_ = $value;
				// $this->set_selected_index_by_value($value);
				break;*/
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#chkValue()
	 */
	public function chkValue()
	{
		return TRUE;
	}
}
