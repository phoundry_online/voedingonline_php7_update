<?php
/**
 * SelectField
 *
 * Build a selectfield
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @author Mark Veldhuizen
 * @version 1.0.5
 * @last_modified 18-12-2007
 *
 * @package		Brickwork
 * @subpackage	Form
 */
class SelectField extends FieldDecorator
{
	/**
	 * Options list
	 * @var	Array
	 */
	protected $options_ = array();
	
	/**
	 * Selected option index
	 * Index of array of which option is selected
	 * @var	Integer
	 */
	protected $selected_index;
	
	/**
	 * Multiple selects
	 */
	protected $multiple_; // @todo checken, want dit wordt waarschijnlijk niet gebruikt. Er is namelijk sprake van een attrib multiplicity

	/**
	 * Build a new select field
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
	}
	
	/*
	 * Als er geen waarde is wordt standaard de eerste van de opties geselecteerd.
	 */
	protected function set_selected_index()
	{
		$first = reset($this->options_);
		$this->selected_index = key($this->options_);
		$first->selected = TRUE;
	}
	
	/**
	 * Select a option by its value, if there are more options with the same value,
	 * the first one is selected
	 *
	 * @param	mixed	$value
	 * @return	Boolean
	 */
	protected function set_selected_index_by_value($value)
	{
		// De value komt meestal uit de post als string, bij een selectfield is
		// dit feitelijk het name gedeelte van de fieldoption
		if (count($this->options) == 0)
			return FALSE;

		// if single selection type, reset all
		if (!$this->multiple) {
			foreach ($this->options as $option) {
				$option->selected= false;
			}
		}
		
		// index of NULL
		$old_index = $this->selected_index;
		
		$selected = FALSE;
		foreach ($this->options as $tellert => $option)
		{
			// todo klopt niet dit is name en niet value
			// 6-12-2007 dit hebben wij (Mick en ik-? wie?) naar $option->value ipv $option->name gezet
			// nu ook goed werkend. levert dus het name gedeelte van de fieldoption op
			if ($option->name == $value)
			{
				$this->selected_index = $tellert;
				$option->selected = TRUE;
				$selected = TRUE;
			}
		}
		
		// no options found for this value, please reconsider your input!
		if (!$selected && $this->required)
		{
			$this->form->error = new ErrorVoedingonline(E_USER_ERROR, 'Geen keuze gemaakt bij '.$this->label);
			return FALSE;
		}
		
		return TRUE;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'length':
				return count($this->options_);
				break;
				
			case 'type':
				if ($this->multiple) {
					return 'select-multiple';
				}
				
				return 'select-one';
				break;
				
			case 'options':
				// bij een niet verplichte select een lege optie invoegen met NULL NULL
				// dit is in het tempalte te default:" -- maak een keuze --"
				if (!$this->field->required && isset($this->options_[0]) && !is_null($this->options_[0]->name))
				{
					array_unshift($this->options_, new FieldOption(NULL,NULL));
				}
				return (array)$this->options_;
				break;
				
			case 'value':
				if ($this->multiple)
				{
					$results = array();
					foreach ($this->options_ as $v)
					{
						if ($v->selected)
						{
							$results[] = $v->value;
						}
					}
					return (is_array($results) ? (array)$results : $results);
				}
				if ($this->readonly_)
				{
					if (!is_null($this->selected_index))
					{
						return $this->options_[$this->selected_index]->value;
					}
				}
				else
				{
					if (!is_null($this->selected_index))
					{
						return $this->options_[$this->selected_index]->name;
					}
				}
				break;
				
			case 'selectedIndex':
				return $this->selected_index;
				
			case 'multiple':
				if (($this->multiplicity == '*') ||
					($this->multiplicity == '+') ||
					($this->multiplicity > 1))
				{
					return TRUE;
				}
				return FALSE;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				$this->value_ = $value;
				if(is_array($value))
				{
					foreach($value as $val) {
						$this->set_selected_index_by_value($val);
					}
				}
				else
				{
					$this->set_selected_index_by_value($value);
				}
				
				break;
				
			case 'options':
				$this->options_ = $value;
				$this->selected_index = key($this->options_);
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#chkValue()
	 */
	public function chkValue()
	{
		return TRUE;
	}
}

/**
 * Field Option
 *
 * Option for selectfields and descendants
 *
 * @author Mick van der Most van Spijk
 * @version 1.0.1
 * @last_modified 12/19/2006
 *
 * @package		Brickwork
 * @subpackage	Form
 */
class FieldOption
{
	/**
	 * Name
	 *
	 * @var	String
	 */
	private $_name;
	
	/**
	 * Value
	 *
	 * @var	String
	 */
	private $_value;
	
	/**
	 * Selected
	 *
	 * @var	Boolean
	 */
	private $_selected = FALSE;
	
	/**
	 * Constructor
	 *
	 * @param	String	$name
	 * @param	String	$value
	 * @return	void
	 */
	public function __construct($name, $value='')
	{
		$this->_name  = $name;
		//$this->value = empty($value) ? $name : $value;
		$this->_value = ($value !== '') ? $value : ''; // indicator dat veld niet is ingevuld
	}
	
	/**
	 * Magic Getter
	 *
	 * @param	String	$name
	 * @return	Mixed
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'name':
				return $this->_name;
				
			case 'value':
				return $this->_value;
				
			case 'selected':
				return $this->_selected;
				
			default:
				throw new AttributeNotFoundException(sprintf('Attribute %s not found', $name));
		}
	}
	
	/**
	 * Magic Setter
	 *
	 * @param	String	$name
	 * @param	Mixed	$value
	 * @return	void
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'name':
				$this->_name = $value;
				break;
				
			case 'value':
				$this->_value = $value;
				break;
				
			case 'selected':
				$this->_selected = (bool)$value;
				break;
				
			default:
				throw new AttributeNotFoundException(sprintf('Attribute %s not found', $name));
				break;
		}
	}
	
	/**
	 * String representation of this class
	 *
	 * @return	void
	 */
	public function __toString()
	{
		return sprintf("%s: %s", $this->_name, $this->_value);
	}
}

