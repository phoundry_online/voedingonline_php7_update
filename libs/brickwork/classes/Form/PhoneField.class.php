<?php
/**
 * PhoneField
 *
 * Formulierveld voor telefoonnummers
 *
 * @version 1.1.0
 * @author Mick van der Most van Spijk
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: PhoneField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class PhoneField extends TextField
{
	/**
	 * Country
	 * @var	Country
	 */
	protected $country_ = NULL;
	
	/**
	 * Make a new PhoneField
	 *
	 * @param	Field	$field
	 */
	public function __construct($field)
	{
		parent::__construct($field);
	}
	
	/**
	 * Check the value of the field
	 * @return	ErrorVoedingonline or Bool
	 */
	public function chkValue()
	{
		// no country information available, then the phonenumber must probably be right
		if (NULL === $this->country_)
		{
			return TRUE;
		}
		
		// validate phone number according to locale
		$regexes = array (
			'NL' => '/^0(([0-9]{1}(\s|-)?[0-9]{8})|([0-9]{2}(\s|-)?[0-9]{7})|([0-9]{3}(\s|-)?[0-9]{6}))$/', // normale nummers
		);
		
		// it validated, hurray
		if (preg_match($regexes[$this->country_->iso], $this->value_) > 0)
		{
			return TRUE;
		}
		
		// oops
		return new ErrorVoedingonline(E_USER_NOTICE, _tr('Ongeldig telefoonnummer', null, __FILE__,__LINE__));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'country':
				$this->country_ = $value;
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'country':
				return $this->country_;
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
}
