<?php
/**
 * IBAN
 *
 * The IBAN identifies an account held by a financial institution. It facilitates the automated
 * processing of (cross-border) transactions. The IBAN is described in EBS 204 and its
 * implementation in SIG 203.
 * The IBAN is implemented without modification to the Basic Bank Account Number
 * (BBAN), being the domestic account number. This is done by adding a prefix to the domestic
 * account number. In some countries an additional bank identifier will be added.
 * The banking industry of each individual country has specified the country-specific length and
 * the composition of the IBAN.
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: IBANField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class IBANField extends TextField
{
	/**
	 * Country
	 * @var	Country
	 */
	private $_country;
	
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
		$this->_country = new Country('NL');
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/TextField#chkValue()
	 */
	public function chkValue() {
		// haal regex op
		if (i18n_IBAN::check($this->_country->iso, $this->value_))
		{
			return TRUE;
		}
		
		return new ErrorVoedingonline(E_USER_NOTICE, _tr('Ongeldig IBAN',null,__FILE__,__LINE__) );
	}
}
