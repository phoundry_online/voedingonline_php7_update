<?php
/**
 * YearField
 *
 * Field for years, default year is -1, which indicates no year is selected or set
 *
 * @version 1.0.1
 * @author Mick van der Most van Spijk
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: YearField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class YearField extends NumberField
{
	public function __construct($field)
	{
		$this->default_value = intval(date('Y'));
		//$this->default_value = -1;
		parent::__construct($field);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'value':
				//return coalesce($this->value_, $this->default_value);
				return $this->value_;
				break;
				
			// year wich to start the pull down in (oid)
			case 'startValue':
				return coalesce($this->value_, $this->default_value);
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				$this->value_ = intval($value);
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
}
