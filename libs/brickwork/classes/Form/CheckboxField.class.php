<?php
/**
 * CheckBox
 *
 * Presenteer meerdere selecties
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @copyright Web Power b.v.
 * @version 1.0.2
 * @access public
 * @last_modified 12/11/2006
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: CheckboxField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class CheckboxField extends SelectField
{
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
		
		$this->multiplicity = '*';
		
		// bij een checkbox veld zijn meerdere antwoorden mogelijk, dus moet de multipliciteit van het veld groter zijn dan 1
		/*if (!Multiplicity::greater($this->multiplicity, 1)) {
			throw new FieldMultiplicityIncorrectException(sprintf('Multiplicity for field %s (%s) is not correct, checkfields need a multiplicity greater than one', $this->label, $this->name));
		}*/
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'value':
				return (is_array($this->value_) ? (array)$this->value_ : $this->value_);
				break;
				
			case 'options':
				// select zet automagisch een lege optie indien niet verplicht. dat willen we niet bij checkboxen.
				return (array)$this->options_;
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				$this->value_ = $value;
				$this->set_selected_indexes_by_value($value);
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * Reset selected indexes
	 *
	 * @return	void
	 */
	protected function reset_selected_indexes()
	{
		foreach ($this->options as $option)
		{
			$option->selected = FALSE;
		}
	}
	
	/**
	 * Set selected indexes by value
	 *
	 * @param	Mixed	$value
	 * @return	void
	 */
	protected function set_selected_indexes_by_value($value)
	{
		if (count($this->options) == 0)
			return;
		
		if (!is_array($value))
			return;
		
		if (count($value) == 0 || empty($value))
		{
			// reset selections
			$this->reset_selected_indexes();
			return;
		}
		
		$selected = FALSE;
		foreach ($this->options as $index => $option)
		{
			foreach ($value as $option_key => $option_value)
			{
				if ($option->name == $option_key)
				{
					$option->selected = TRUE;
					$selected = TRUE;
				}
			}
		}
	}
}
