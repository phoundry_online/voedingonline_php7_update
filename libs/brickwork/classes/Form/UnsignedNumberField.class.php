<?php
/**
 * Numberfield for unsigned number, aka positive numbers
 *
 * Present unsigned numbers
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @version 1.0.0
 * @last_modified 11/23/2006
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: UnsignedNumberField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class UnsignedNumberField extends NumberField
{
	/**
	 * Build a new unsigned number field
	 *
	 * @param	Field	$field
	 */
	public function __construct($field)
	{
		parent::__construct($field);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				// make sure the value is always positive
				$this->value_ = abs($value);
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/NumberField#chkValue()
	 */
	public function chkValue()
	{
		if (is_numeric($this->value))
		{
			if ($this->value == abs($this->value))
			{
				return TRUE;
			}
		}
		
		return FALSE;
	}
}
