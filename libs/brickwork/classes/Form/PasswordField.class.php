<?php
/**
 * Password field
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @version 1.0.0
 * @last_modified 11/28/2006
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: PasswordField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class PasswordField extends HTMLField
{
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'value':
				if ($this->readonly_)
				{
					// genereerd dummy sterren
					return str_repeat('*', 8);
				}
				else
				{
					return $this->value_;
				}
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
}
