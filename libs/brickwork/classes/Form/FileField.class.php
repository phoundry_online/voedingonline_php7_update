<?php
/**
 * File Field
 *
 * @author		Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: FileField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class FileField extends FieldDecorator
{
	public static $maxFileSize = FALSE;
	
	public function __construct($field)
	{
		parent::__construct($field);
		
		// update encoding van formulier zodat bestanden kunnen worden gebruikt
		$this->form->encoding = 'multipart/form-data';
		
		// default max filesize, moet worden overschreven indien er meerder filefields per pagina zijn
		if (FALSE === self::$maxFileSize)
		{
			self::$maxFileSize = self::_convert(ini_get('upload_max_filesize'));
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'maxFileSize':
				return self::$maxFileSize;
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * Convert filesize string to number
	 *
	 * @param	String	$str
	 * @return	Integer
	 */
	private function _convert($str)
	{
		$int = intval($str);
		
		// map van K, M, G naar bytes
		if (stristr($str, 'K'))
		{
			$int = $int * 1024;
		}
		elseif (stristr($str, 'M'))
		{
			$int = $int * 1024 * 1024;
		}
		elseif (stristr($str, 'G'))
		{
			$int = $int * 1024 * 1024 * 1024;
		}
		
		return $int;
	}
	
	/**
	 * Get maximum file size
	 *
	 * @return	String
	 */
	public function getMaxFileSize()
	{
		$len = strlen(self::$maxFileSize);
		$size = self::$maxFileSize;
		
		if ($len > 9)
		{
			return self::$maxFileSize / 1024 / 1024 / 1024 ."Gb";
		}
		elseif ($len > 6)
		{
			return self::$maxFileSize / 1024 / 1024 ."Mb";
		}
		elseif ($len > 3)
		{
			return self::$maxFileSize / 1024 ."Kb";
		}
		else
		{
			return self::$maxFileSize ."b";
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#chkValue()
	 */
	public function chkValue()
	{
		return TRUE;
	}
}
