<?php
/**
 * PhotoUploadField
 *
 * Veld dat een foto toont en een uploadmogelijkheid voor een foto. Indien er
 * geen foto is is de value NULL en kan er een foto worden geuploaded. Indien er
 * wel een foto is, is de de value een verwijzing naar de foto. Er is dan de
 * mogelijkheid om de foto te overschrijven.
 *
 * @author Mick van der Most van Spijk
 * @version 1.1.0
 * @changes:
 * - function getAllowedMimeTypes toegevoegd
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: PhotoUploadField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class PhotoUploadField extends FileUploadField
{
	/**
	 * Get Allowed Mime Types
	 *
	 * @return	Array
	 */
	public static function getAllowedMimeTypes()
	{
		return array(
			'image/pjpeg',
			'image/jpg',
			'image/jpeg',
			'image/png',
			'image/jp2',
			'image/jpeg2000',
			'image/jpeg2000-image',
			'image/x-jpeg2000-image',
			'image/x-png'
		);
	}
}
