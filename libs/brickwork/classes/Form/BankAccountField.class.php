<?php
/**
 * BankAccountField
 *
 * Bank account number field, locate dependent, extend with rules for each
 * new country
 *
 * @author Mick van der Most van Spijk
 * @version 1.0.2
 * @last_modified 12/17/2007
 * @changes:
 * v1.0.2
 * - check bij postbank op mimimaal rekeningnummer 1
 * v1.0.1
 * - check op letters in chkValue
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: BankAccountField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class BankAccountField extends TextField
{
	/**
	 * Contry
	 *
	 * @var	Country
	 */
	private $_country; // Country
	
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
		$this->_country = new Country('NL');
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			// prepareer waarde voor juiste opslagwijze
			case 'value':
				// soms wordt bij postbankrekeningen een P meegegeven en er worden wel eens spaties meegegeven
				$value = preg_replace('/\s|p/i', '', $value);
				
				// pad naar 10 lengte
				$value = str_pad($value, 10, '0', STR_PAD_LEFT);
				
				// nu spatieloos en 10 lang
				$this->value_ = $value;
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/TextField#chkValue()
	 */
	public function chkValue()
	{
		switch ($this->_country->iso)
		{
			case 'NL':
				// check of nummer uit alleen getallen bestaat @since v1.0.1
				for ($i = 0; $i < strlen($this->value_); $i++)
				{
					$value = substr($this->value_, $i, $i+1);
					if (48 > ord($value) || 57 < ord ($value))
					{
						return new ErrorVoedingonline(E_USER_NOTICE, _tr('Bank- / girorekeningnummer bevat letters'));
					}
				}
				
				// indien niet 10 cijfers of drie voorloopnullen dan postbank en niet te checken
				if (strlen($this->value_) <= 7 || (strlen($this->value_) == 10 && substr($this->value_, 0, 3) == '000'))
				{
					if (0 >= intval($this->value_))
					{ // check op minimaal nummer 1 @since v1.0.2
						return new ErrorVoedingonline(E_USER_NOTICE, 'Het laagst bekende girorekeningnummer is 1 (\'s Rijks schatkist)');
					}
					return TRUE; // postbank - giro, no validation possible
				}
				
				// 11 check formule
				$sum = 0;
				for ($i = 0; $i < 10; $i++)
				{
					$sum += substr($this->value_, -($i + 1), 1) * ($i + 1);
				}
				
				// modulus moet nul zijn, dan is het nummer geldig
				if ($sum % 11 == 0)
				{
					return TRUE;
				}
				
				return new ErrorVoedingonline(E_USER_NOTICE, 'Ongeldig bank- / girorekeningnummer');
				
			default:
				return new ErrorVoedingonline(E_USER_NOTICE, 'Geldigheid kan niet worden gecontroleerd, regels voor het valideren zijn onbekend');
		}
	}
}
