<?php
/**
 * RadioField
 *
 * Enkele selectie uit een lijst van opties
 *
 * @author Mick van der Most van Spijk
 * @version 1.0.0
 * @last_modified 12/19/2006
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: RadioField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class RadioField extends SelectField
{
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'options':
				return (array)$this->options_;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#set_selected_index_by_value()
	 */
	protected function set_selected_index_by_value($value)
	{
		if (count($this->options) == 0)
			return FALSE;
		
		// index of NULL
		$old_index = $this->selected_index;
		
		$selected = FALSE;
		foreach ($this->options as $tellert => $option)
		{
			if ($option->value == $value)
			{
				$this->selected_index = $tellert;
				$option->selected = TRUE;
				$selected = TRUE;
			}
		}
		
		// niets geselecteerd, dan eerste element in lijst
		// TODO: temp even checken?
		if (!$selected && $this->required)
		{
			$first = reset($this->options_);
			$this->selected_index = key($this->options_);
			$first->selected = TRUE;
			$selected = TRUE;
		}
		
		// reset vorige selectie
		if ($selected && !is_null($old_index) && $old_index != $this->selected_index)
		{
			$this->options_[$old_index]->selected = FALSE;
		}
		
		return TRUE;
	}
	
	/**
	 * Select a option by its name, name is supposed to be unique
	 *
	 * @param	mixed	$value
	 * @return	Boolean
	 */
	protected function set_selected_index_by_name($name)
	{
		if (count($this->options) == 0)
			return FALSE;
		
		// index of NULL
		$old_index = $this->selected_index;
		
		$selected = FALSE;
		foreach ($this->options as $tellert => $option)
		{
			if ($option->name == $name)
			{
				$this->selected_index = $tellert;
				$option->selected = TRUE;
				$selected = TRUE;
			}
		}
		
		// niets geselecteerd, dan eerste element in lijst
		if (!$selected)
		{
			$first = reset($this->options_);
			$this->selected_index = key($this->options_);
			$first->selected = TRUE;
			$selected = TRUE;
		}
		
		// reset vorige selectie
		if ($selected && !is_null($old_index) && $old_index != $this->selected_index)
		{
			$this->options_[$old_index]->selected = FALSE;
		}
		
		return TRUE;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				$this->value_ = $value;
				$this->set_selected_index_by_value($value);
				//$this->selected_index = $value;
				break;
				
			case 'options':
				$this->options_ = $value;
				//$this->selected_index = key($this->options_);
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
}
