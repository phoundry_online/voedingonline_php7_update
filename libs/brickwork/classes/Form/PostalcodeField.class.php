<?php
/**
 * Postal Code Field
 * @author	Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: PostalcodeField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class PostalcodeField extends CompositeField
{
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/CompositeField#chkValue()
	 */
	public function chkValue()
	{
		return TRUE;
	}
}
