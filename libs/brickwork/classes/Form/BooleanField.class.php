<?php
/**
 * BooleanField
 *
 * @author Mick van der Most van Spijk
 * @version 1.0.0
 * @last_modified 11/23/2006
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: BooleanField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class BooleanField extends RadioField
{
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		// parent
		parent::__construct($field);
		
		// options
		$this->options = array(
			new FieldOption('1', 'Ja'),
			new FieldOption('0', 'Nee')
		);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/RadioField#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				$this->value_ = $value;
				$this->set_selected_index_by_value($value);
				break;
				
			case 'options':
				$this->options_ = $value;
				$this->selected_index = key($this->options_);
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
}
