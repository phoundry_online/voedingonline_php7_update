<?php
/**
 * Button Field
 *
 * @author	Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
class ButtonField extends HTMLField
{
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
	}
}
