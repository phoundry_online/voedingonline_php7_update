<?php
/**
 * File Upload Field
 *
 * @author		Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: FileUploadField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class FileUploadField extends FileField
{
	private static $error_codes = array(
		UPLOAD_ERR_OK			=> 'There is no error, the file uploaded with success.',
		UPLOAD_ERR_INI_SIZE		=> 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
		UPLOAD_ERR_FORM_SIZE	=> 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
		UPLOAD_ERR_PARTIAL		=> 'The uploaded file was only partially uploaded.',
		UPLOAD_ERR_NO_FILE		=> 'No file was uploaded.',
		UPLOAD_ERR_NO_TMP_DIR	=> 'Missing a temporary folder.',
		UPLOAD_ERR_CANT_WRITE	=> 'Failed to write file to disk.'
	);
	
	public static function getError($code)
	{
		return _tr(self::$error_codes[$code], null, __FILE__,__LINE__);
	}
}
