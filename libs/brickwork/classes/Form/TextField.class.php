<?php
/**
 * TextField
 *
 * Very basic implementation of a field.
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @version 1.0.0
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: TextField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class TextField extends FieldDecorator
{
	/**
	 * Construct
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#chkValue()
	 */
	public function chkValue()
	{
		return TRUE;
	}
}
