<?php
/**
 * Photo Field
 *
 * @author	Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: PhotoField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class PhotoField extends FileField
{
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/FileField#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/FileField#chkValue()
	 */
	public function chkValue() {
		return TRUE;
	}
}
