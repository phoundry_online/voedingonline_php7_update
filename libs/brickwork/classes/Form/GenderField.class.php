<?php
/**
 * Gender selectie veld
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @last_modified 11/23/2006
 * @see Gender.class.php
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: GenderField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class GenderField extends SelectField
{
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		// roep parent aan, deze roept een setter aan en selecteerd de ingestelde waarde
		parent::__construct($field);
		
		// maak opties aan
		$this->options = array(
			new FieldOption (Gender::male, 'Man'),
			new FieldOption (Gender::female, 'Vrouw')
		);
		
		// nu ook nog ff value vullen met juiste data enselected index zetten
		// $this->set_selected_index_by_value($this->value);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#chkValue()
	 */
	public function chkValue()
	{
		// return in_array($this->options_[$this->selectedIndex], array(Gender::male, Gender::female, Gender::unknown));
		return TRUE;
	}
/*
	public function __set($name, $value) {
		switch ($name) {
			case 'data':
				// $this->data_ = $value;

				// convert naar juiste value voor html gebruik
				$this->value_ = $value;

				// $this->set_selected_index_by_value($value);
				break;

			case 'value':
				$this->value_ = $value;
				// $this->set_selected_index_by_value($value);
				break;

			default:
				try {
					return parent::__set($name, $value);
				} catch (AttributeNotFoundException $e) {
					throw $e;
				}
				break;
		}
	}

	public function __get($name) {
		switch ($name) {
			case 'value':
				if ($this->readonly_)
				return $this->value_;
				break;

			default:
				try {
					return parent::__get($name);
				} catch (AttributeNotFoundException $e) {
					throw $e;
				}
				break;
		}
	}*/
}
