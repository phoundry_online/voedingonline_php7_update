<?php
/**
 * Email Field
 *
 * @author		Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: EmailField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class EmailField extends TextField implements FieldValidator
{
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				// verwijderd, anders nooit bekend dat veld leeg is gelaten
				/*if (trim($value) == '') {
					break;
				}*/

				/*if (!Email::isValid($value)) {
					//$this->form->error = new Error(E_USER_NOTICE, 'Ongeldig e-mailadres');
					return new Error(E_USER_NOTICE, 'Ongeldig e-mailadres');
				}*/
				$this->value_ = $value;
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/TextField#chkValue()
	 */
	public function chkValue()
	{
		if (empty($this->value_) || FALSE === Email::isValid($this->value))
		{
			return new ErrorVoedingonline(E_USER_NOTICE, _tr('Ongeldig e-mailadres', null, __FILE__, __LINE__));
		}
		
		return TRUE;
		
		// return preg_match('/^([a-z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-z0-9\-]+\.)+))([a-z]{2,4}|[0-9]{1,3})(\]?)$/i', $this->value);
	}
}
