<?php
/**
 * Cursor Field
 *
 * @author		Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: CursorField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class CursorField extends HiddenField
{
	public function __construct($field)
	{
		parent::__construct($field);
	}
}
