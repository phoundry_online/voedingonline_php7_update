<?php
/**
 * Date field
 *
 * Field for date selection. Uses the BrickDateTime class internally and can guess
 * datetime on setting an array from smarty. Conversion to DB-format is done
 * in DBForm
 *
 * @see			BrickDateTime.class.php
 * @author		Mick van der Most van Spijk <mick@webpower.nl>
 * @version		1.0.2
 * @last_modified	21-6-2007
 * @changes
 * - __set behandelt nu ook de default value
 * - _convert_value converteert nu ook timestamps naar BrickDateTime
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: DateField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class DateField extends FieldDecorator
{
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				$this->value_ = $this->_convert_value($value);
				if ($this->value_ == NULL)
				{
					$this->value_ = $this->default_value;
				}
				break;
				
			case 'defaultValue':
				$this->default_value = $this->_convert_value($value);
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * Convert value when setting
	 *
	 * @param	Mixed	$value
	 * @return	BrickDateTime
	 */
	private function _convert_value($value)
	{
		// is already date time
		if ($value instanceof BrickDateTime)
		{
			return $value;
		}
		// comming from smarty
		if (is_array($value))
		{
			if ($value['Day'] == ''
			&& $value['Month'] == ''
			&& $value['Year'] == '')
			{
				return NULL;
			}
			$formater = datefmt_create(
    				'nl_NL',
    				IntlDateFormatter::SHORT,
    				IntlDateFormatter::NONE,
    				'Europe/Amsterdam',
    				IntlDateFormatter::GREGORIAN,
    				'MMMM'
			);
			$date = new BrickDateTime();
			$date->day	= $value['Day'];
			$date->month	= date('m', $formater->parse($value['Month']));
			$date->year	= $value['Year'];
			return $date;
		}
		
		// misschien een timestamp?
		if ($value == intval($value))
		{
			return new BrickDateTime($value);
		}
		if ($value == '') {
			return new BrickDateTime($value);
		}

		// oops
		trigger_error('Value cannot be converted, unknown format, maybe they have set us up the bomb', E_USER_ERROR);
		return new BrickDateTime();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'value':
				/*if (empty($this->value_)) { removed 3/20/2007 mick
					$this->value_ = new BrickDateTime();
				}*/
				
				if ($this->readonly_ && !is_null($this->value_))
				{
					return $this->value_->date('d-m-Y');
				}
				else
				{
					return $this->value_;
				}
				break;
				
			// year wich to start the pull down in (oid)
			case 'startValue':
				if (!($this->value_ instanceof BrickDateTime))
				{
					return '--';
				}
				else
				{
					return $this->value_->date('Y-m-d');
				}
				
				break;
				
			case 'currentValue':
				if (is_null($this->value_) || $this->value_->timestamp == FALSE)
				{
					return $this->default_value;
				}
				else
				{
					return $this->value_;
				}
				
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#chkValue()
	 */
	public function chkValue()
	{
		if ($this->value_ instanceof BrickDateTime)
		{
			return $this->value_->checkdatetime();
		}
		
		return FALSE;
	}
}
