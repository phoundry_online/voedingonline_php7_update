<?php
/**
 * DMDSubscribe
 *
 * Presenteer aanvink box voor dmdelivery nieuwsbrief
 *
 * @author		Emiel Roelofsen <emiel.roelofsen@webpower.nl>
 * @copyright	Web Power b.v.
 * @version		1.0.0
 * @last_modified
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: DMDSubscribeField.class.php 1764 2010-05-31 14:33:22Z christiaan.baartse $
*/
class DMDSubscribeField extends SelectField
{
	/**
	 * Soap Campaigns
	 *
	 * @var	Array
	 */
	private $_soap_campaigns = array();
	
	/**
	 * Field Mapping
	 *
	 * @var	Array
	 */
	private $_field_mapping = array();
	
	/**
	 * @var string
	 */
	private $_form_name;
	
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
		
		if(is_array($this->field->extra))
		{
			if(!empty($this->field->extra['soapCampaigns']))
			{
				$this->_soap_campaigns = explode('|', $this->field->extra['soapCampaigns']);
				$this->_get_options();
			}
		}
		
		$this->_form_name = $this->form->name;
	}
	
	/**
	 * get options from database
	 *
	 * @return	void
	 */
	private function _get_options()
	{
		$sql = sprintf("
			SELECT	*
			FROM		brickwork_dmdelivery_soap_campaign
			WHERE		id IN(%s)
			AND		site_identifier = '%s'
		",
			implode(',', $this->_soap_campaigns),
			DB::quote(Framework::$site->identifier)
		);
		
		$res  = DB::iQuery($sql);
		
		if (self::isError($res))
		{
			trigger_error(sprintf('Could not execute query (%s) in line: %s File: %s %s', $sql, __LINE__, __FILE__, mysql_error()), E_USER_ERROR);
		}
		
		if ($res->current())
		{
			while($res->current())
			{
                $row = $res->current();
				$this->options_[$row->id] = new DMDSubscribeFieldOption($row->id, $row->campaign_title);
				$this->options_[$row->id]->brickworkDmdeliverySoapCampaignId = (int)$row->id;
				$this->options_[$row->id]->soapUrl = $row->soap_url;
				$this->options_[$row->id]->username = $row->username;
				$this->options_[$row->id]->password = $row->password;
				$this->options_[$row->id]->optinMailingId = !empty($row->optin_mailing_id) ? $row->optin_mailing_id : FALSE;
				$res->next();
			}
		}
	}
	
	/**
	 * Get Field Mappings
	 *
	 * @param	Integer	$id
	 * @return	void
	 */
	private function _get_field_mapping($id)
	{
		$this->_field_mapping = array();
		$sql = sprintf("
			SELECT	*
			FROM		brickwork_dmdelivery_soap_campaign_field_mapping
			WHERE		soap_id = %d
		",
			$id
		);
		
		$res  = DB::iQuery($sql);
		
		if (self::isError($res))
		{
			trigger_error(sprintf('Could not execute query (%s) in line: %s File: %s %s', $sql, __LINE__, __FILE__, mysql_error()), E_USER_ERROR);
		}
		
		if ($res->current())
		{
			while ($res->current())
			{
			    $row = $res->current();
				$this->_field_mapping[$row->user_fieldname] = $row->dmd_fieldname;
				$res->next();
			}
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#onLoad()
	 */
	public function onLoad()
	{
		if(count($this->_soap_campaigns) == 0)
		{
			$this->form->removeField($this);
		}
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'value':
				if(count($this->value_) > 0)
					return implode(',', $this->value_);
				return $this->value_;
				break;
				
			case 'options':
				// select zet automagisch een lege optie indien niet verplicht. dat willen we niet bij checkboxen.
				return $this->options_;
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#onAfterHandleRequest()
	 */
	public function onAfterHandleRequest()
	{
		if(count($this->value_) > 0)
		{
			foreach($this->value_ as $id => $campaign_title)
			{
				if(@!file_get_contents($this->options_[$id]->soapUrl))
				{
					return new ErrorVoedingonline(E_USER_WARNING, sprintf("Can't connect to soap wsdl: %s", $this->options_[$id]->soapUrl));
				}
			}
		}
		return TRUE;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#onSuccessHandleRequest()
	 */
	public function onSuccessHandleRequest()
	{
		if(count($this->value_) > 0)
		{
			$errors = array();
			foreach($this->value_ as $id => $campaign_title)
			{
				// Get field mapping.
				$this->_get_field_mapping($id);
				$SOAP = new DMD_SOAP($this->options_[$id]->soapUrl, $this->options_[$id]->username, $this->options_[$id]->password);
				$props = array();
				foreach($this->_field_mapping as $user_fieldname => $dmd_fieldname)
				{
					$props[$dmd_fieldname] = $_POST[$this->_form_name][$user_fieldname];
				}
				if(!$this->options_[$id]->optinMailingId)
				{
					$res = $SOAP->subscribe($props);
				}
				else
				{
					$res = $SOAP->optin($props, $this->options_[$id]->optinMailingId);
				}
				if(is_a($res, 'Error'))
				{
					return new ErrorVoedingonline(E_USER_WARNING, $res->errstr);
				}
			}
		}
		return TRUE;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/SelectField#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				$this->value_ = $value;
				$this->set_selected_indexes_by_value($value);
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * Reset selected indexes
	 *
	 * @return	void
	 */
	protected function reset_selected_indexes()
	{
		foreach ($this->options as $option)
		{
			$option->selected = FALSE;
		}
	}
	
	/**
	 * Set selected indexes
	 *
	 * @param	String	$value
	 * @return	void
	 */
	protected function set_selected_indexes_by_value($value)
	{
		if (count($this->options) == 0)
			return;
		
		if (!is_array($value))
			return;
		
		if (count($value) == 0 || empty($value))
		{
			// reset selections
			$this->reset_selected_indexes();
			return;
		}
		
		$selected = FALSE;
		
		$options = $this->options;
		foreach ($options as $index => $option)
		{
			foreach ($value as $option_key => $option_value)
			{
				if ($option->name == $option_key)
				{
					$option->selected = TRUE;
					$selected = TRUE;
				}
			}
		}
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

/**
 * DMD SubscribeField Option
 *
 * @package		Brickwork
 * @subpackage	Form
 */
class DMDSubscribeFieldOption extends FieldOption
{
	/**
	 * Campaign ID
	 *
	 * @var	Integer
	 */
	public $brickworkDmdeliverySoapCampaignId;
	
	/**
	 * Soap URL
	 *
	 * @var	String
	 */
	public $soapUrl;
	
	/**
	 * Username
	 *
	 * @var	String
	 */
	public $username;
	
	/**
	 * Password
	 *
	 * @var	String
	 */
	public $password;
	
	/**
	 * Opt-in Mailing ID
	 *
	 * @var	Integer
	 */
	public $optinMailingId;
	
	/**
	 * Constructor
	 *
	 * @param	String		$name
	 * @param	MixedVar	$value
	 * @return	void
	 */
	public function __construct($name, $value='')
	{
		parent::__construct($name, $value);
	}
}
