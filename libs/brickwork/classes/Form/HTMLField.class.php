<?php
/**
 * HTML Field
 *
 * Basic wrapper for HTML Fields
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @version 1.0.0
 * @last_modified 11/23/2006
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: HTMLField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class HTMLField extends FieldDecorator
{
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#chkValue()
	 */
	public function chkValue()
	{
		return TRUE;
	}
}
