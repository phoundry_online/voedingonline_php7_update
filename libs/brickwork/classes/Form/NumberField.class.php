<?php
/**
 * Numberfield
 *
 * Present numbers
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @version 1.0.0
 * @last_modified 11/23/2006
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: NumberField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class NumberField extends FieldDecorator
{
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		parent::__construct($field);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#chkValue()
	 */
	public function chkValue()
	{
		if (!is_numeric($this->value))
		{
			//return Webmodule::getErrorMsg('E_NOTNUMERIC', 'Ongeldig nummer', 2);
			return new ErrorVoedingonline(E_USER_NOTICE, _tr('Ongeldig nummer', null, __FILE__, __LINE__));
		}
		
		return TRUE;
	}
}
