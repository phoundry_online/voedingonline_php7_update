<?php
require_once 'HTMLField.class.php';

/**
 * Hidden Field
 *
 * @author	Unknown
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: HiddenField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class HiddenField extends HTMLField
{
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/HTMLField#chkValue()
	 */
	public function chkValue()
	{
		return TRUE;
	}
}
