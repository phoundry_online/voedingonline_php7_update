<?php
/**
 * Captcha
 *
 * Generate captcha code for human validation
 *
 * db changes:
 * insert into brickwork_db_form_field values (NULL, 'CaptchaField');
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: CaptchaField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class CaptchaField extends FieldDecorator
{
	/**
	 * Captcha
	 *
	 * @var	String
	 */
	static private $_captcha = NULL;
	
	/**
	 * Length of the captcha
	 *
	 * @var	integer
	 */
	private $_length = 5;
	
	/**
	 * When to ask for the captcha
	 *
	 * @var	String
	 */
	private $_ask = 'once';
	
	/**
	 * Captcha valid
	 *
	 * @var	Boolean
	 */
	private $_valid = FALSE;
	
	private $_hash;
	
	/**
	 * Constructor
	 *
	 * @param	Field	$field
	 * @return	void
	 */
	public function __construct($field)
	{
		$field->required = TRUE; // always true for captcha!
		parent::__construct($field);
		
		$this->_hash = md5($this->id);
		
		if(isset($field->extra))
		{
			$this->_length	= !empty($field->extra['value']) ? (int)$field->extra['value'] : 6;
			$this->_ask		= !empty($field->extra['ask']) ? $field->extra['ask'] : 'always';
		}
		
		if(isset($_SESSION['CaptchaField']) && isset($_SESSION['CaptchaField'][$this->_hash]))
		{
			self::$_captcha = $_SESSION['CaptchaField'][$this->_hash];
		}
		$_SESSION['CaptchaField'][$this->_hash] = Captcha::stringGen($this->_length);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'valid':
				return $this->_valid;
				break;
				
			case 'length':
				return $this->_length;
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Field#chkValue()
	 */
	public function chkValue()
	{
		if(!((self::$_captcha == $this->value_) && ($this->value_ != '')))
		{
			$this->value_ = '';
			$e = new ErrorVoedingonline(E_USER_NOTICE, _tr('De ingevulde code komt niet overeen met de afbeelding', null, __FILE__,__LINE__));
			return $e;
		}
		if($this->_ask == 'once')
		{
			$this->_valid = TRUE;
			$this->readonly = TRUE;
			$_SESSION['CaptchaField'][$this->_hash] = TRUE;
		}
		return TRUE;
	}
	
	public function onSuccessHandleRequest()
	{
		unset($_SESSION['CaptchaField'][$this->_hash]);
		return TRUE;
	}
}
