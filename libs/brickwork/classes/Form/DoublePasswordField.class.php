<?php
/**
 * DoublePassword field
 *
 * This is a special type of password field used to set new passwords and compare
 * the two given password, so the luser won't make a typo.
 *
 * Watch out! There are two labels in this field. Use them!
 * Seperate the two labels in the construct witdh ;
 *
 * @author Mick van der Most van Spijk <mick@webpower.nl>
 * @author Remco Bos <remco@webpower.nl> aka Remblok Bossi aka Brakepad Forrest
 *
 * @version 1.0.0
 * @last_modified 7/17/2007
 *
 * @package		Brickwork
 * @subpackage	Form
 */
/**
 * $Id: DoublePasswordField.class.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
class DoublePasswordField extends PasswordField
{
	/**
	 * Original Value
	 *
	 * @var	String
	 */
	private $_value_original;
	
	/**
	 * Confirmation Value
	 *
	 * @var	String
	 */
	private $_value_confirmation;
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/PasswordField#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'value':
				if (is_array($value))
				{
					$this->_value_original = reset($value);
					$this->_value_confirmation = end($value);
					
					if (!$this->_chk_value($value))
					{
						throw new Exception(_tr('Wachtwoorden zijn niet identiek', null, __FILE__,__LINE__) );
					}
					
					$this->value_ = $this->_value_original;
				}
				else
				{
					$this->value_ = $value;
				}
				break;
				
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Form/PasswordField#__get()
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'label':
			case 'label_original':
				return trim(reset(explode(";", $this->label_)));
				break;
				
			case 'label_confirmation':
				return trim(end(explode(";", $this->label_)));
				break;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
	
	/**
	 * check values
	 *
	 * @param	Array	$values
	 * @return	Boolean
	 */
	private function _chk_value($values)
	{
		if ($this->_value_original !== $this->_value_confirmation)
		{
			return FALSE;
		}
		
		return TRUE;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FieldDecorator#isEmpty()
	 */
	public function isEmpty()
	{
		if ($this->_value_original == '' || $this->_value_confirmation == '')
		{
			return TRUE;
		}
	}
}
