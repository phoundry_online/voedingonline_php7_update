<?php
/**
* Gender <<basetype>>
*
* @author Mick van der Most van Spijk
* @last_modified 7/10/2006
* @package None
*/
final class Gender {
	/**
	* Man
	*
	* @var
	* @access public
	*/
	const male = 'g_male';

	/**
	* Woman
	*
	* @var
	* @access public
	*/
	const female = 'g_female';

	/**
	* Unknown
	* @var String
	* @access public
	*/
	const unknown = 'g_unknown'; // Rammstein: Zwitter, ich bin so verliebt, Zwitter, ich bin in mich verliebt

	/**
	* Mapping of gender to db for instance
	*/
	public static $mapping = array (
		Gender::male    => Gender::male,
		Gender::female  => Gender::female,
		Gender::unknown => Gender::unknown
	);

	public function value2key($value) {
		return array_search($value, self::$mapping);
	}

	public function key2value($key) {
		// return array_keys(self::$mapping, $key);
		$keys = array_keys(self::$mapping);
		if (in_array($key, $keys)) {
			return self::$mapping[$key];
		}
	}

	/**
	* Very magic function to map keys and values
	*/
	public function map($thingy) {

		foreach (self::$mapping as $key => $value) {
			if ($key == $thingy)
				return $value;

			if ($value == $thingy)
				return $key;
		}
/*
		// zoek value
		$map = array_search($value, self::$mapping);

		// op value
		if (FALSE !== $map)
			return $map;

		// zoek dan op key
		// $map = array_search($value, array_keys(self::$mapping));
		$map = array_keys(self::$mapping, $value);
		print "<pre>\n";
		print_r(self::$mapping);
		print "</pre>\n";
print "<pre>\n";
print_r($map);
print "</pre>\n";
		// op keu
		if (FALSE !== $map)
			return $map;

		// jammer de bammer
		return FALSE;*/
	}

	/**
	* Parse string and return gender constant

	public static function parse($gender) {
		$males = array (
			Gender::male,
			'm',
			'male'
		);

		$females = array (
			Gender::female,
			'f',
			'v',
			'female'
		);

		if (in_array($gender, $males))
			return Gender::male;

		if (in_array($gender, $females))
			return Gender::female;

		// not found
		return FALSE;
	}*/
}
?>
