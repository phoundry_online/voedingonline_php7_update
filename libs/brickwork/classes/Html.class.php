<?php
/**
 * HTML helper functions
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @todo uitzoeken of dit nog steeds meerwaarde heeft boven smarties html_* functies
 */
class Html
{
	/**
	 * Create a <select>
	 *
	 * @param string $name
	 * @param array $options
	 * @param string $selected
	 * @param string $extra
	 * @return string
	 */
	public static function inputSelect($name, $options, $selected = null, $extra = '')
	{
		if(!is_array($name)) {
			$attribs = array('name' => $name);
		} else {
			$attribs = $name;
		}
		
		if(!is_array($selected)) $selected = array($selected);
		
		$input = '<select'.self::attributes($attribs).' '.$extra.'>';
		foreach ($options as $key => $val)
		{
			$key = (string) $key;

			if (is_array($val))
			{
				$input .= '<optgroup label="'.$key.'">'."\n";
				foreach ($val as $inner_key => $inner_val)
				{
					$inner_key = (string) $inner_key;

					$sel = ($selected === $inner_key) ? ' selected="selected"' : '';
					$input .= '<option value="'.$inner_key.'"'.$sel.'>'.$inner_val.'</option>'."\n";
				}
				$input .= '</optgroup>'."\n";
			}
			else
			{
				$sel = (in_array($key, $selected)) ? ' selected="selected"' : '';
				$input .= '<option value="'.$key.'"'.$sel.'>'.$val.'</option>'."\n";
			}
		}
		$input .= '</select>';
			
		return $input;
	}
	
	/**
	 * Create a <input type="radio"> set
	 *
	 * @param string $name
	 * @param array $options
	 * @param array $selected
	 * @param string $extra
	 * @return string
	 */
	public static function inputRadio($name, $options, $selected = null, $extra = '')
	{
		if(!is_array($name)) {
			$attribs = array('name' => $name);
		} else {
			$attribs = $name;
		}
		
		$attribs['type'] = 'radio';
		if(!is_array($selected)) $selected = array($selected);
		$input = '';
		foreach ($options as $key => $val)
		{
			$key = (string) $key;

			if (is_array($val))
			{
				throw new Exception("Input Radio doesn't support recursive options");
			}
			else
			{
				$sel = (in_array($key, $selected)) ? ' checked="checked"' : '';
				$input .= '<div>';
				$input .= '<label><input'.self::attributes($attribs).'" value="'.$key.'" '.$extra.' /> '.$val.'</label>';
				$input .= '</div>';
				$input .= "\n";
			}
		}
			
		return $input;
	}
	
	public static function inputMultiSelect($name, $options, $selected = null, $extra = '')
	{
		if(!is_array($name)) $name = array('name' => $name);
		$name['multiple'] = 'multiple';
		if(substr($name['name'], -2) != '[]') $name['name'] .= '[]';
		
		return self::inputSelect($name, $options, $selected, $extra);
	}
	
	public static function inputCheckbox($name, $options, $selected = null, $extra = '')
	{
		if(!is_array($name)) {
			$attribs = array('name' => $name);
		} else {
			$attribs = $name;
		}
		
		if(substr($attribs['name'], -2) != '[]') $attribs['name'] .= '[]';
		
		$attribs['type'] = 'checkbox';
		if(!is_array($selected)) $selected = array($selected);
		$input = '';
		foreach ($options as $key => $val)
		{
			$key = (string) $key;

			if (is_array($val))
			{
				throw new Exception("Input Checkbox doesn't support recursive options");
			}
			else
			{
				$sel = (in_array($key, $selected)) ? ' checked="checked"' : '';
				$input .= '<div>';
				$input .= '<label><input'.self::attributes($attribs).'" value="'.$key.'" '.$extra.' /> '.$val.'</label>';
				$input .= '</div>';
				$input .= "\n";
			}
		}
			
		return $input;
	}
	
	/**
	 * Compiles an array of HTML attributes into an attribute string.
	 *
	 * @param   array $attrs
	 * @return  string
	 */
	public static function attributes($attrs)
	{
		if (empty($attrs)) return '';

		if (is_string($attrs)) return ' '.$attrs;

		$compiled = '';
		foreach ($attrs as $key => $val)
		{
			$compiled .= ' '.$key.'="'.$val.'"';
		}

		return $compiled;
	}
	
}
