<?php

/**
 * Abstract class to output a data table into a specific format
 * This class is an abstract for all other formatters. Formatters can be
 * used to output an iterator as a tabled output. 
 * 
 * Every row in the iterator should yield a row in the output file. The row
 * can be an array or an object.
 * 
 * @author		Peter Eussen
 * @version	1.0
 * @package	Brickwork
 * @subpackage	DataTable
 */
abstract class DataTable_Formatter
{
	/**
	 * The source iterators
	 * The Datatable can concat several iterators in one file. This attribute holds all iterators
	 * 
	 * @var Array
	 */
	protected	$_source;
	
	/**
	 * The header column
	 * The header that should be printed as the first line of the file
	 * 
	 * @var Array
	 */
	protected	$_header;
	
	/**
	 * Initialises the DataTable Formatter
	 *
	 * @param Iterator $sourceTable
	 */
	public function __construct( Iterator $sourceTable = null )
	{
		$this->_source = Array();
		$this->_header = Array();
			
		if ( !is_null($sourceTable))
			$this->setDataSource( $sourceTable );
	}
	
	/**
	 * Adds another iterator as primary source
	 *
	 * @param 	Iterator $sourceTable
	 * @return boolean
	 */
	public function setDataSource( Iterator $sourceTable )
	{
		$this->_source = Array($sourceTable);
		return true;
	}
	
	/**
	 * Appends an iterator to the list of iterators to export
	 *
	 * @param 	Iterator $sourceTable
	 * @return boolean
	 */
	public function addDataSource( Iterator $sourceTable )
	{
		array_push( $this->_source, $sourceTable );
		return true;
	}
	
	/**
	 * Initialises the heading for the table
	 * if you want a heading above your table, you should call this method with the
	 * heading columns you want to have.
	 * 
	 * @param 	Array $header
	 * @return boolean
	 */
	public function setHeader( $header )
	{
		if ( !is_array($header) )
			$this->_header = Array($header);
		else
			$this->_header = $header;
		return true;
	}
	
	/**
	 * Retrieves the formatted text as a string
	 *
	 * @return string
	 */
	public function fetch()
	{
		$this->_startOutput();
		
		if (count($this->_header))
			$this->_addHeaderRow( $this->_header );
		foreach( $this->_source as $sourceTable )
		{
			foreach( $sourceTable as $row )
				$this->_addRow( $row );
		}
		
		$this->_endOutput();
		return $this->_getOutput();
	}
	
	/**
	 * Stores the formatted text as a file
	 *
	 * @param 	string $target		The destination file
	 * @return boolean
	 */
	public function save( $target )
	{
		return file_put_contents( $target, $this->fetch() );		
	}
	
	/**
	 * Outputs the formatted text directly to the browser
	 *
	 * @param 	string $targetfile	Optional filename to pass on
	 * @return boolean
	 */
	public function out( $targetfile = null )
	{
		if ( is_null($targetfile) )
			$targetfile = get_class($this);
			
		if ( !headers_sent() )
			$this->_setMimeHeaders($targetfile);
		echo $this->fetch();
		return true;
	}
	
	/**
	 * Use the array keys as heading for the table. 
	 * If you do not want to specify your own heading you can also use this
	 * method to retrieve the headings from the array keys (or object attribute
	 * names)
	 *
	 * @return boolean
	 */
	public function useKeysAsHeading()
	{
		$row = $this->_source[0]->current();
		
		if ( is_object($row) )
			return $this->setHeader(array_keys(get_object_vars($row)));
		else if ( is_array($row))
			return $this->setHeader(array_keys($row));
		else
			$this->setHeader(Array($row));
		return true;
	}
	
	/**
	 * Outputs the headers to the browsers
	 * The targefile should be used as destination if the format should be opened in an application
	 * 
	 * @param 	string 	$targetfile
	 * @return	boolean
	 */
	abstract protected function _setMimeHeaders($targetfile);
	
	/**
	 * Starts a new file generator
	 *
	 * @return boolean
	 */
	abstract protected function _startOutput();
	
	/**
	 * Closes the new file generator
	 *
	 * @return boolean
	 */
	abstract protected function _endOutput();
	
	/**
	 * Adds a heading row
	 *
	 * @param Array $headers
	 */
	abstract protected function _addHeaderRow( $headers );
	
	/**
	 * Adds a data row
	 *
	 * @param Array $columns
	 */
	abstract protected function _addRow( $columns );
	
	/**
	 * Returns the formatted text as string
	 *
	 * @return string
	 */
	abstract protected function _getOutput();
}