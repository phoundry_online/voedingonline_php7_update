<?php

/**
 * Outputs a file as Comma Seperated File
 *
 * @author		Peter Eussen
 * @version	1.0
 * @package	Brickwork
 * @subpackage	DataTable
 */
class DataTable_Formatter_CSV extends DataTable_Formatter
{
	const		DELIMITER	= '"';
	const		SEPARATOR	= ',';
	
	protected	$_delimiter;
	protected	$_separator;
	protected	$_buffer;

	public function __construct( Iterator $it )
	{
		parent::__construct( $it );
		$this->_delimiter = self::DELIMITER;
		$this->_separator = self::SEPARATOR;
	}
	
	/**
	 * Sets a new delimiter for the output
	 *
	 * @param 	string $delim
	 * @return boolean
	 */
	public function setDelimiter( $delim )
	{
		$this->_delimiter = $delim;
		return true;
	}
	
	/**
	 * Sets a new field seperator for the target file
	 * If you want Excel compatible output, use this one to set the seperator to
	 * a semi column (;)
	 * 
	 * @param 	string $sep
	 * @return boolean
	 */
	public function setSeparator( $sep )
	{
		$this->_separator = $sep;
		return true;
	}
	
	protected function _startOutput()
	{
		$this->_buffer 	  = '';
		return true;
	}
	
	protected function _endOutput()
	{
		return true;
	}
	
	protected function _addRow( $columns )
	{
		$line = '';
		
		if ( is_array($columns) || is_object($columns) )
		{
			foreach( $columns as $data )
			{
				if ( is_numeric($data) )
					$line .= ($line ? $this->_separator : '') . $data;
				else
					$line .= ($line ? $this->_separator : '') . $this->_delimiter . str_replace($this->_delimiter, str_repeat($this->_delimiter,2), $data ) . $this->_delimiter;
			}
		}
		else
			$line .= $columns;
			
		$this->_buffer .= $line . "\n";
		return true;
	}
	
	protected function _addHeaderRow( $columns )
	{
		return $this->_addRow($columns);
	}
	
	protected function _setMimeHeaders( $targetfile )
	{
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename=' . $targetfile . '.csv');
	}
	
	protected function _getOutput()
	{
		if ( !isset($this->_buffer) || is_null($this->_buffer) )
			throw new DataTable_Formatter_Exception('Can not output data before starting');
		return $this->_buffer;		
	}
}