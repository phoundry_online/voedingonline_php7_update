<?php

/**
 * Outputs the resultset as a table
 * Headers will be outputted as a THEAD section, the rest will be in a TBODY section.
 * If the rows have keys, the keys will be used as class names for the TD tags
 *
 * @author		Peter Eussen
 * @version	1.0
 * @package	Brickwork
 * @subpackage DataTable
 */
class DataTable_Formatter_HTML extends DataTable_Formatter
{
	protected	$_buffer;
	protected	$_bodyStarted;
	
	protected function _startOutput()
	{
		$this->_buffer 		= '<table class="datatable">' . "\n";
		$this->_bodyStarted = false;
		return true;
	}
	
	protected function _endOutput()
	{
		if ( $this->_bodyStarted )
			$this->_buffer .= "</tbody>";
		$this->_buffer .= "</table>";
		return true;
	}
	
	protected function _addRow( $columns )
	{
		if ( !$this->_bodyStarted)
		{
			$this->_buffer 		.= "<tbody>\n";
			$this->_bodyStarted = true;
		}
		
		$line = '<tr>';
		
		if ( is_array($columns) || is_object($columns) )
		{
			foreach( $columns as $key => $data )
			{
				if (!is_numeric($key))
					$line .= "<td class=\"$key\">" . htmlentities($data) . "</td>\n";
				else
					$line .= '<td>' . htmlentities($data) . "</td>\n";
			}
		}
		else
			$line .= "<td>" . htmlentities($columns) . "</td>\n";
					
		$this->_buffer .= $line . "</tr>\n";
		return true;
	}
	
	protected function _addHeaderRow( $columns )
	{
		$this->_buffer .= "<thead>\n<tr>\n";
		
		foreach( $columns as $key => $col )
		{
			if ( !is_numeric($key) )
				$this->_buffer .= "<td class=\"$key\">";
			else
				$this->_buffer .= "<td>";
				
			$this->_buffer .= htmlentities($col, ENT_QUOTES) . "</td>\n";
		}	
		$this->_buffer .= "</tr>\n</thead>\n";
		return true;
	}
	
	protected function _setMimeHeaders( $targetfile )
	{
		header('Content-Type: text/html');
		header('Content-Disposition: attachment; filename=' . $targetfile . '.csv');
	}
	
	protected function _getOutput()
	{
		if ( !isset($this->_buffer) || is_null($this->_buffer) )
			throw new DataTable_Formatter_Exception('Can not output data before starting');
		return $this->_buffer;
	}
}