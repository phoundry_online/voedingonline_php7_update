<?php

/**
 * Formats a file in Excel format
 * Inspired on theh ExcelFormatter from http://www.appservnetwork.com/modules.php?name=News&file=article&sid=8
 * Originally found by Stefan Grootscholten, converted to a brickwork class by me
 * 
 * @author		Peter Eussen
 * @version	1.0
 * @package	Brickwork
 * @subpackage	DataTable	
 */
class DataTable_Formatter_Excel extends DataTable_Formatter
{
	/**
	 * Internal buffer
	 *
	 * @var string
	 */
	protected	$_buffer;
	
	/**
	 * The current row we are writing
	 *
	 * @var integer
	 */
	protected	$_row;
	
	/**
	 * Initialises the excel file
	 *
	 * @return boolean
	 */
	protected function _startOutput()
	{
		$this->_buffer = pack("ssssss", 0x809, 0x8, 0x00, 0x10, 0x0, 0x0);
		$this->_row	   = 0; 
		return true;
	}
	
	protected function _endOutput()
	{
		$this->_buffer .= pack("ss", 0x0A, 0x00);
		return true;
	}
	
	protected function _getOutput()
	{
		return $this->_buffer;
	}
	
	protected function _addRow( $columns )
	{
		$col = 0;
		
		if ( is_array($columns) || is_object($columns) )
		{
			foreach ( $columns as $data )
			{
				if (is_numeric($data))
					$this->_buffer .= $this->_writeNumber( $this->_row, $col, $data );
				else
					$this->_buffer .= $this->_writeLabel( $this->_row, $col, $data );
				$col++;
			}
		}
		else
		{
			if ( is_numeric($columns) )
				$this->buffer .= $this->_writeNumber( $this->_row, $col, $columns );
			else
				$this->buffer .= $this->_writeLabel( $this->_row, $col, $columns );
		}
		$this->_row++;
		return true;
	}
	
	protected function _addHeaderRow( $columns )
	{
		return $this->_addRow($columns);
	}
	
	protected function _setMimeHeaders( $target )
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=' . $target . '.xls');
		return true;		
	}

    /**
     * Write Number in XLS file
     *
     * @param   int         $row
     * @param   int         $column
     * @param   int|float   $value
     * @return  void
     */
    private function _writeNumber($row, $col, $value)
    {
    	$ln = pack("sssss", 0x203, 14, $row, $col, 0x0);
        $ln .= pack("d", $value);
        return $ln;
    }
    
    /**
     * Write Label in XLS file
     *
     * @param   int     $row
     * @param   int     $column
     * @param   string  $value
     * @return  void
     */
    private function _writeLabel($row, $col, $value)
    {
    	if ($value == null)
        {
            return;
        }
        $len = strlen($value);
        $ln = pack("ssssss", 0x204, 8 + $len, $row, $col, 0x0, $len);
        $ln .= $value;
        return $ln;
    }	
}
