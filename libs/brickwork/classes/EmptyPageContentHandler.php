<?php
class EmptyPageContentHandler extends ContentHandler
{
	/**
	 * @var Page
	 */
	protected $page;

	/**
	 * Gets called to do initialization
	 */
	public function init()
	{
		if (empty($this->path[0])) {
			header("404 Not Found", true, 404);
			exit;
		}
		Framework::$representation->assign('SITE', Framework::$site);
		Framework::$representation->assign('PAGE', $this);
		Framework::$representation->assign('HOME_ID', false);
		
		class_exists("SiteStructure", true);
		
		$page_type = new PageType($this->path[0]);
		$this->page = new Page(
			0,
			new StructureItem(0, 'none', null),
			$page_type,
			Framework::$site,
			Framework::$clientInfo,
			Auth::singleton(),
			"snapshot",
			"",
			"",
			$this->_getContentContainers($page_type)
		);
	}

	/**
	 * Get the content containers for the current pageType
	 *
	 * @return array Array with empty content containers
	 */
	protected function _getContentContainers(PageType $page_type) 
	{
		$sql = sprintf("
			SELECT 
				code
			FROM 
				brickwork_content_container
			WHERE
				page_type_code = %s",
			DB::quote($page_type->identifier, true)
		);

		$keys = DB::iQuery($sql)->getSelect('code');
		
		if($keys) {
			return array_combine($keys, array_fill(0, count($keys), array()));
		}
		return array();
	}

	/**
	 * Outputs the page
	 * Gets all the html from the modules and load it into the page type template.
	 * Handles module caching and output of the contents to the screen
	 *
	 * @param boolean $return return the contents or output
	 * @return mixed string(html) or void
	 */
	public function out($return = false)
	{
		header(sprintf('Content-type: %s; charset=%s', Framework::$representation->mimeType, Framework::$PHprefs['charset']));
		$html = $this->page->render(Framework::$representation, true);

		if (count($this->page->cContainers)) {
			//$css_ids = "#".implode(",#", $cc);
			foreach ($this->page->cContainers as $cc2) {
				$html = str_replace(
					'<div id="'.$cc2.'" ', 
					'<div id="'.$cc2.'" style="background-color: #8FFFA9!important; display:block!important; min-height:50px!important; background-image:none!important;overflow:hidden!important;" ', 
					$html
				);
			}
		}
		echo $html;
	}
}