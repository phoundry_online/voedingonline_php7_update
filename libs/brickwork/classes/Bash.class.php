<?php
// the right shabang:
// #!/usr/local/php5/bin/php
// #!/usr/local/bin/php5

/**
* PHP wrapper om snel Bash classes te kunnen proggen met standaard help en
* arg parsing
*
* Argumenten kunnen net als in bash mee worden gegeven, een argument kan nooit
* de waarde NULL leveren. Dit duidt altijd op een fout
*
* @author Mick van der Most van Spijk
* @version 1.1.0
* @last_modified 2/21/2007
*/
class Bash {
	/**
	* Standard out attributes for use with writeln
	*
	* @var Array
	* @access private
	* @static
	*/
	private static $_attributes = array(
		"none"       => "00",
		"bold"       => "01",
		"underscore" => "04",
		"blink"      => "05",
		"reverse"    => "07",
		"concealed"  => "08"
	);

	/**
	* Standard out foreground colors for use with writeln
	*
	* @var Array
	* @access private
	* @static
	*/
	private static $_fgcolors = array(
		"black"        => "30",
		"red"          => "31",
		"green"        => "32",
		"yellow"       => "33",
		"blue"         => "34",
		"magenta"      => "35",
		"cyan"         => "36",
		"gray"         => "37",
		"default"      => "39",
		"darkgray"     => "30",
		"lightred"     => "31",
		"lightgreen"   => "32",
		"lightyellow"  => "33",
		"lightblue"    => "34",
		"lightmagenta" => "35",
		"lightcyan"    => "36",
		"white"        => "37",
	);

	/**
	* Standard out background color for use with writeln
	*
	* @var Array
	* @access private
	* @static
	*/
	private static $_bgcolors = array(
		"black"     => "40",
		"red"       => "41",
		"green"     => "42",
		"yellow"    => "43",
		"blue"      => "44",
		"magenta"   => "45",
		"cyan"      => "46",
		"lightgray" => "47",
		"default"   => "",
	);

	/**
	* Description
	* @var String
	*/
	protected $description_ = 'Generic bashscript';

	/**
	* Options
	* @var Array
	*/
	protected $options_ = array(
		'--help'    => 'this help',
		'--version',
		'--config' => 'config'
	);

	/**
	* Version
	* @var String (major:minor:bugfix)
	*/
	private $_version = "1.1.0";

	/**
	* Container for all passed arguments
	* @var Array
	*/
	protected $args_ = array();

	/**
	* Scriptname
	* @var String
	*/
	protected $scriptname_;

	/**
	* Make a new Bash class
	*/
	public function __construct() {
		// load args
		$this->args_       = self::_get_args();

		// check if a parameter is set in the commandlist which isnt supported by the script
		$this->_chk_args();

		// find scriptname, strip leading stuff (./)
		$this->scriptname_ = preg_replace("/^(\.\/)([.]*)/", "$2", $this->get_arg('scriptname'));

		// chk for help
		$help = $this->get_arg(array('h', 'help'));
		if (!is_null($help)) {
			$this->help_();
			exit(0);
		}

		$version = $this->get_arg('version');
		if (!is_null($version)) {
			$this->println(sprintf("%s - %s", $this->scriptname_, $this->_version));
			exit(0);
		}
	}

	/**
	* Get an commandline passed argument by its name
	* Returns NULL if nothing was found
	*
	* @param mixed
	* @return mixed
	*/
	protected function get_arg($name) {
		if (is_array($name)) {
			foreach ($name as $value) {
				if (in_array($value, array_keys($this->args_), TRUE)) {
					return $this->args_[$value];
				}
			}
		} else {
			//if (in_array($name, array_keys($this->args_))) {
			if (array_key_exists($name, $this->args_)) {
				return $this->args_[$name];
			}
		}

		// error, maar wel doorgaan, anders wordt script onbetrouwbaar
		//trigger_error(sprintf('No argument with the name %s found', $name), E_USER_NOTICE);
		return NULL;
	}

	/**
	* Get command line parameters
	*/
	private static function _get_args () {
		// container for args
		$args = array (
			'scriptname' => $_SERVER['argv'][0],
		);

		// --XYZ=waarde
		// --XYZ
		// -x=waarde
		// -x waarde
		// -xyz

		// get parameters
		for ($x = 1; $x < $_SERVER['argc']; $x++) {
			$arg = $_SERVER['argv'][$x];

			if ($arg[0] == '-') { // first char is -
				if ($arg[1] == '-') { // named --
					// --X=waarde
					if (strstr($arg, '=')) { // name value pair
						$parts = explode("=", $arg);
						$args[substr($parts[0], 2)] = $parts[1];
					} else { // only a value, key and value are the same
					// --X
						$args[substr($arg, 2)] = substr($arg, 2);
					}
				} else { // named -
					// -x=waarde
					if (strstr($arg, '=')) { // name value pair
						$parts = explode("=", $arg);
						$args[substr($parts[0], 1)] = $parts[1];

					// -x waarde
					} elseif (strlen($arg) == 2) {
						$args[substr($arg, 1)] = NULL;
// @todo -x als param werkt nog niet
					// -xyz (multiple flags)
					} else {
						$arg = substr($arg, 1);

						// each letter is an option
						for ($i = 0; $i < strlen($arg); $i++) {
							$args[$arg[$i]] = TRUE;
						}
					}
				}
			} else { // value die hoort bij de vorige param

				// kijk naar laatst gezette waarde in de lijst
				end($args);

				// indien waarde null is, dan hebben we een `-x waarde` constructie en moet de waarde worden gezet
				if (is_null($args[key($args)])) {
					$args[key($args)] = $arg;
				} else {
					// er is geen rare optie meegegeven, maar hoogstwaarschijnlijk is die het laatste element in de lijst
					$args[] = $arg;
				}
			}
		}

		// return list
		return $args;
	}

	/**
	* _chk_args
	*
	* check if a parameter is set in the commandlist which isnt supported by the script
	* look in option list for a key that is provided, if the key is not found, the script isnt supporting the param
	*
	* @access private
	*/
	private function _chk_args() {
		foreach ($this->args_ as $key => $value) {
			// scriptname is always ok
			if ('scriptname' == $key) {
				continue;
			}

			if (!in_array($key, $this->_get_options())) {
				$this->println(sprintf('Unknown argument %s', $key), 'red');
				exit(1);
			}
		}
	}

	/**
	* _get_options
	*
	* Get a list of all defined options
	* @param Array
	*/
	private function _get_options() {
		$_options = array();
		foreach ($this->options_ as $key => $value) {
			$_options[] = str_replace("-", "", $key);
		}

		return $_options;
	}

	/**
	* Print line to standard out
	*
	* @param String
	* @param String foreground color
	* @param String background color
	* @param String attribute
	*/
	public function println($str, $fgcolor='default', $bgcolor='default', $attribute='none') {
		// check existing foreground
		if (!in_array($fgcolor, array_keys(self::$_fgcolors))) {
			fwrite(STDOUT, sprintf("%s->writeln: invalid foreground color %s\n", get_class($this), $_fgcolor));
		}

		// check existing backgorund
		if (!in_array($bgcolor, array_keys(self::$_bgcolors))) {
			fwrite(STDOUT, sprintf("%s->writeln: invalid background color %s\n", get_class($this), $_bgcolor));
		}

		// check existing attributes
		if (!in_array($attribute, array_keys(self::$_attributes))) {
			fwrite(STDOUT, sprintf("%s->writeln: invalid attribute %s\n", get_class($this), $_attribute));
		}

		// write to standard out
		fwrite(STDOUT, sprintf("\033[%s;%s;%sm%s\033[0;39m\n", self::$_attributes[$attribute], self::$_bgcolors[$bgcolor], self::$_fgcolors[$fgcolor], trim($str)));
	}

	/**
	* Generic help function
	*
	*/
	protected function help_() {
		// calc max width
		$len = 0;
		foreach ($this->options_ as $key => $value) {
			if (strlen($key) + 2 > $len) {
				$len = strlen($key) + 2;
			}
		}

		$msg  = sprintf("Usage: %s [OPTION] FILE\n", $_SERVER['argv'][0]);
		$msg .= $this->description_."\n\n";

		$msg .= "Options:\n";
		foreach ($this->options_ as $key => $value) {
			$msg .= sprintf(" %s%s\n",str_pad($key, $len), $value);
		}

		$msg .= "\n";
		
		/*@todo volgens mijn moet dit een ander adres worden? */
		$msg .= "Report bugs to <mick@webpower.nl>.\n";

		fwrite(STDOUT, $msg);
		exit(0);
	}


	/*
	function x() {
		foreach ($args as $name => $value) {
			switch($name) {
				case 'conf':
				case 'conf':
				case 'c':
					$configFile = $value;
					break;

				case 'debug':
					$debug = TRUE;
					break;

				case 'version':
					fwrite(STDOUT, "Lotto import, version 0.0.1\nCopyright (C) 2006 Web Power\n");
					exit(0);
					break;

				case 'help':
				case 'h':
					fwrite(STDOUT,
						sprintf("Usage: %s [OPTION] -c=[FILE]\n", $_SERVER['argv'][0]).
						"Start het importscript voor de door de Lotto geuploadede bestanden\n\n".
						"Options:\n".
						"  -c=FILE, -conf, --conf=FILE      Laad een specifieke configfile\n".
						"  --version                        Toon versie informatie en exit\n".
						"  --debug                          Start in debugmode en toon alle stappen\n".
						"  -h, --help                       Toon dit helpscherm en exit\n\n".
						"Report bugs to <mick@webpower.nl>.\n"
					);
					exit(0);
					break;
			}
		}
	}
	*/

	/*
		if (strstr($arg, '=')) { // no eq allowed, value is next arg
						fwrite(STDOUT, sprintf("%s: invalid option -- =\n", $args['scriptname']));
						fwrite(STDOUT, sprintf("Try `%s --help` for more information.\n", $args['scriptname']));
						exit(1);
					}
					*/
}
?>
