<?php
/**
 * Album ORM class for the Photogallery
 *
 * @author		Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 * @copyright	Web Power
 * @since		13 nov 2008
 * @version	1.0
 *
 * <sql>
 CREATE TABLE `photogallery_album` (
 `id` int(10) unsigned NOT NULL auto_increment,
 `active` tinyint(1) default '1',
 `title` varchar(255) NOT NULL,
 `prio` float NOT NULL,
 PRIMARY KEY  (`id`)
 ) DEFAULT CHARSET=utf8;
 * </sql>
 */

class Photogallery_Album extends ORM
{
	/**
	 * Default table to use
	 *
	 * @var	String
	 */
	protected $_table_name = "brickwork_photogallery_album";
	
	/**
	 * Set the relation to the photos in this album
	 *
	 * @var	Array
	 */
	protected $_one_to_many = array(
		'photos' => array("Photogallery_Photo", "album_id"),
	);
	
	/**
	 * Set the default sort order
	 *
	 * @var	Array
	 */
	protected $_default_sort_order = array("prio", "DESC");
}