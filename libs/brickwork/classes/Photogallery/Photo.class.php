<?php
/**
 * Photo ORM class for the Photogallery
 *
 * @author		Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 * @copyright	Web Power
 * @since		13 nov 2008
 * @version	1.0
 *
 * <sql>
 CREATE TABLE `photogallery_photo` (
 `id` int(10) unsigned NOT NULL auto_increment,
 `album_id` int(10) unsigned NOT NULL,
 `title` varchar(255) default NULL,
 `description` varchar(255) default NULL,
 `location` varchar(255) NOT NULL,
 `pubdate` datetime NOT NULL default '0000-00-00 00:00:00',
 `prio` float NOT NULL,
 PRIMARY KEY (`id`),
 KEY `FK_photogallery_photo` (`album_id`)
 ) DEFAULT CHARSET=utf8
 * </sql>
 */

class Photogallery_Photo extends ORM
{
	/**
	 * Default table to use
	 *
	 * @var	String
	 */
	protected $_table_name = "brickwork_photogallery_photo";
	
	/**
	 * Many to one relation of the ORM
	 *
	 * @var	Array
	 */
	protected $_many_to_one = array(
		'album' => array('Photogallery_Album', 'album_id'),
	);
	
	/**
	 * Default order by for this table
	 *
	 * @var	Array
	 */
	protected $_default_order_by = array("prio", "DESC");
}