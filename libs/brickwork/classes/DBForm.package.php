<?php
/**
* DBForm
*
* Database driven form
*
* @author Mick van der Most van Spijk <mick@webpower.nl>
* @version 1.0.4
* @last_modified 6-12-2007
* @changes:
* - return Boolean bij functie load
* - radiofield toegevoegd bij srcScheme / selectfield
*
* Multipliciteit
* Bij een tabel-multipliciteit > 1 moet er een cursorfield worden aangemaakt om aan
* te geven welk veld een record uniek identificeerd (bijv id). Dit veld wordt
* vervolgens gebruikt om mee te kunnen bladeren.
*/
class DBForm extends Form {
	// indien een table wordt geedit moet er bekend zijn welke row er moet worden
	// aangepast, dus moet het formulier weten op welke veld en met welke waarde
	// de juiste rij moet worden geselecteerd uit welke tabel
	private static $_cursors = array();

	// lijst van schema en tabellen en velden
	private $_schemes = array();

	public $record_nr = 0;

	// build new form
	public function __construct($code, $action, $method=FormMethod::post) {
		parent::__construct($code, $action, $method);
	}

	// add a crusor, an indicator which row in the db is selected
	public function addCursor($formname, DBCursor $cursor) {
		self::$_cursors[$formname] = $cursor;
	}

	// set a list of cursors for this db form
	public function setCursors($cursors) {
		self::$_cursors = $cursors;
	}

	/**
	* load form definitions and values
	*
	* Laad alle velden uit de DB en map die op de Formulier velden
	* @return Boolean
	*/
	public function load() {
		// laad alle velden in
		$fields = $this->_get_db_fields();

		if (FALSE === $fields) {
			return FALSE;
		}

		$this->fields = $this->_get_form_fields($fields);

		// on load form event(s)
		if(!$this->onLoad()){
			return FALSE;
		}

		return TRUE;
	}

	// get cursor
	private static function _get_cursors($formname, $scheme, $table) {
		$_cursors = array();

		if (count(self::$_cursors) < 1) {
			return array();
		}

		foreach (self::$_cursors as $_formname => $cursors) {
			if ($formname == $_formname) {
				foreach ($cursors as $cursor) {
					if ($cursor->scheme == $scheme && $cursor->table == $table) {
						$_cursors[] = $cursor;
					}
				}
			}
		}

		// no cursor found
		return $_cursors;
	}

	private static function _set_cursor_value($formname, $scheme, $table, $field, $value) {
		foreach (self::$_cursors as $_formname => $cursors) {
			if ($formname == $_formname) {
				foreach ($cursors as $cursor) {
					if ($cursor->scheme == $scheme && $cursor->table == $table) {
						foreach ($cursor->conditions as $condition) {

							if ($condition->field == $field->name) {
								$condition->value = $value;
							}
						}
					}
				}
			}
		}
	}

	// laad velddefinities
	private function _get_db_fields($active=TRUE) {
		// active selector
		$active_sql = '';
		if (TRUE === $active || FALSE === $active) {
			$active_sql = sprintf("
				AND
					fm.active = %d
			",
				(bool)$active ? 1 : 0
			);
		}

		$sql = sprintf('
			SELECT
				fm.fieldtype,
				fm.dst_scheme,
				fm.dst_table,
				fm.dst_field,
				fm.src_scheme,
				fm.src_table,
				fm.src_field,
				fm.src_label,
				fm.src_constraint,
				fm.label,
				fm.prio,
				fm.multiplicity as src_multiplicity,
				fm.disabled,
				fm.active,
				fm.required,
				fm.info,
				bf.feedbackable,
				bf.multiplicity as dst_multiplicity,
				bf.title as title,
				bf.description as description,
				bf.header,
				bf.footer
			FROM
				brickwork_db_form_mapping fm
			RIGHT JOIN
				brickwork_form bf ON fm.brickwork_form_code = bf.code
			WHERE
				bf.code = \'%s\'
			AND
				bf.active = %d
			%s
		',
			DB::quote($this->name),
			(bool)$active ? 1 : 0,
			$active_sql
		);

		$result = DB::iQuery($sql);

		if (self::isError($result)) {
			// trigger_error('No result', E_USER_NOTICE);
			$this->errors_[] = $result;
			return FALSE;
		}

		// geen formulier gevonden, is ook niet erg, gewoon doorgaan
		if ($result->num_rows == 0) {
			$this->errors_[] = new ErrorVoedingonline(E_USER_ERROR, sprintf('No form with name %s found', $this->name));
			// trigger_error(sprintf('No form with name %s found', $this->name), E_USER_NOTICE);
			return FALSE;
		}

		// form dingen zetten
		if ($result->num_rows > 0) 
		{
			$row				= $result->first();
			
			$this->title        = $row->title;
			$this->description  = $row->description;
			$this->multiplicity = $row->dst_multiplicity;
			$this->feedbackable = $row->feedbackable;
			$this->header       = $row->header;
			$this->footer       = $row->footer;
		}

		// loop through result
		foreach ($result as $row) 
		{
			// indien formulier is aangemaakt maar er geen velden zijn ff checken
			if (empty($row->fieldtype)) {
				break;
			}

			$dbf = new DBField($row->fieldtype, $row->dst_scheme, $row->dst_table, $row->dst_field, $row->label, $row->prio);
			$dbf->srcScheme     = $row->src_scheme;
			$dbf->srcTable      = $row->src_table;
			$dbf->srcField      = $row->src_field;
			$dbf->srcLabel      = $row->src_label;
			$dbf->srcConstraint = $row->src_constraint;
			$dbf->multiplicity  = $row->src_multiplicity;

			$dbf->disabled      = $row->disabled;
			$dbf->active        = $row->active;
			$dbf->required      = $row->required;

			$dbf->info          = $row->info;
			$dbf->form          = $this;

			$fields[] = $dbf;
		}

		return $fields;
	}

	// laad formvelden op basis van de velddefinities en indien er een cursor bestaat ook de juiste data ophalen
	private function _get_form_fields($fields) {
		// return container
		$form_fields = array();

		// sort fields by scheme, table, unieke rij
		$schemes = array();

		foreach ($fields as $field) {
			$schemes[$field->dstScheme][$field->dstTable][] = $field;
		}

		// zet schemas en tabellen in de juiste volgorde
		foreach ($schemes as $scheme => $tables) {
			foreach ($tables as $table => $rows) {
				ksort($schemes[$scheme][$table]);
			}

			ksort($schemes[$scheme]);
		}

		ksort($schemes);

		// sla op
		$this->_schemes = $schemes;

		// indien er iets is gerequest dan kijken of er cursors instaan
		$controls = array();

		if (isset(self::$request_[$this->name]['controls'])) {
			$controls = self::$request_[$this->name]['controls'];
		}

		// teller voor totaal aantal records
		$records = 0;
		$record_nr = 0;
		$multi_select_fields = array();

		// foreach scheme and table set field en load data indien cursor bestaat voor die tabel
		foreach ($schemes as $scheme => $tables) {
			foreach ($tables as $table => $fields) {
				// container indien er data wordt geselecteerd
				$row = NULL;
				$sql = array();

				// chk for cursorfield
				$selector = array();
				foreach($fields as $field) {
					if ($field->fieldtype == 'CursorField') {
						//print $field->dstTable;
						// dit is het geselecteerde record binnen een form met meerdere records (multiplicity > 1)
						// $selector[$field->name] = $result->result[0]->{$field->name}; // current

						if (isset(self::$request_[$this->name][$field->name]) && !is_null(self::$request_[$this->name][$field->name])) {
							$selector[$field->name] = self::$request_[$this->name][$field->name];
						}

						// cursormeuk, ken toe aan cursors
						if (isset(self::$request_[$this->name][$field->name]) && !is_null(self::$request_[$this->name][$field->name])) {
							self::_set_cursor_value($this->name, $scheme, $table, $field, self::$request_[$this->name][$field->name]);
						}
					}
				}

				// haal data op voor alle velden, indien er een cursor voor bestaat
				if (NULL != ($cursors = self::_get_cursors($this->name, $scheme, $table))) {
					$select_fields = array();

					foreach ($fields as $field) {
						// in welk veld is data gestopt? dat moeten we uitlezen om value en selects te zetten
						$select_fields[] = $field->dstField;
					}

					// where dingest voor de cursors
					foreach ($cursors as $cursor) {
						foreach ($cursor->conditions as $condition) {
							$sql[] = $condition->sql();// sprintf (' %s %s \'%s\'', $condition->field, $condition->operator, $condition->value);
						}
					}

					if (count($sql) > 0) {
						$tellert = 0;
						foreach ($sql as $dingest) {
							if ($tellert++ == 0) {
								$yadda = 'WHERE ';
							} else {
								$yadda .= ' AND ';
							}

							$yadda .= $dingest;
						}
					}

					$sql = sprintf("
						SELECT SQL_CALC_FOUND_ROWS
							%s
						FROM
							%s.%s
						%s
					",
						commalize($select_fields),
						DB::quote($scheme),
						DB::quote($table),
						$yadda
					);

					$result = DB::iQuery($sql);
					if (self::isError($result)) {
						// trigger_error('No result', E_USER_NOTICE);
						$this->errors_[] = $result;
						return FALSE;
					}

					$rows = $result->total_rows;
					if ($rows > $records) {
						$records = $rows;
					}

					unset($select_fields);

					// bepaal de juiste rij met resultaten adhv selector en cursors
					if ($result->num_rows == 0) {
						// doe niets
					} 
					elseif ($result->num_rows == 1 || (count($selector) == 0 && count($controls) == 0) || array_key_exists('remove', $controls)) 
					{
						$row = $result->first();
						$record_nr = 1;

						// meerdere rijen gevonden, kan een multiple select zijn, dan zijn het alle geselecteerde velden, dus ff opslaan, we zien wel of we die gaan gebruiken
						$multi_select_fields[$scheme][$table] = $result->result;

					} else {
						// meerdere rijen met data gevonden, selecteer nu de juiste rij
						// foreach($result->result as $row) {
						//while (list($record_nr, $row) = each($result))
						$row= $result->first();
						foreach( $result as $record_nr => $record) 
						{
							// indien er een next cursor is dan willen we de volgende rij teruggeven
							if (isset($controls['next'])) {

								// indien eigenschap bestaat
								if (isset($record->{key($selector)})) 
								{
									if ($record->{key($selector)} == $selector[key($selector)]) 
									{
										// hou rekening met selector, indien verder dan de laatste, gewoon de laatste tonen
										if ($record_nr < $rows)
											$row= $result[$record_nr+1];
										else
											$row= $result[$record_nr];
										
										break;
									}
								}

							// indien er een previous cursor is dan willen we de vorige rij teruggeven
							} elseif (isset($controls['previous'])) {
								if (isset($record->{key($selector)})) {
									if ($record->{key($selector)} == $selector[key($selector)]) {
										// hou rekening met selector, indien verder dan de laatste, gewoon de laatste tonen
										if ($record_nr)
											$row= $result[$record_nr- 1];
										else
											$row= $result[0];
										break;
									}
								}
							// juiste rij bepalen op basis van selector
							} elseif (!empty($selector) && $record->{key($selector)} == $selector[key($selector)]) {
								$row= $record;
								break;
							} else {
								// meerdere rijen gevonden, kan een multiple select zijn, dan zijn het alle geselecteerde velden, dus ff opslaan, we zien wel of we die gaan gebruiken
								$multi_select_fields[$scheme][$table][] = $record;
							}
						}

						// tel de hoeveelste actief is (1 based)
						$teller = 0;
						foreach($result->result as $r) {
							if ($r== $row) {
								$record_nr = $teller + 1;
							}

							$teller++;
						}
					}
				}

				// loop door alle velden
				//$form_fields = array();
				foreach($fields as $field) {
					if (!class_exists($field->fieldtype)) { // TODO autoloader pakt dit af en triggert een error als de class niet is gevonden
						// in geval van een multipliciteits form (een formulier dat op de pagina meerder rijen kan hebben)
						// print 'setting: '.$field->name.": ".$row->{$field->name}."<br>";
						$field->fieldtype = 'TextField';
					}

					$form_field = new $field->fieldtype($field);

					// inherit
					$form_field->disabled = $field->disabled;
					$form_field->active   = $field->active;
					$form_field->label    = $field->label;
					$form_field->prio     = $field->prio;
					$form_field->required = $field->required;
					$form_field->info     = $field->info;

					// speciaal selectfield, wil namelijk selectiedata uit andere tabel hebben indien src velden zijn ingevuld
					//	if ($field->fieldtype == 'SelectField' || $field->fieldtype == 'CheckboxField' || $field->fieldtype == 'RadioField') {

					// 2007-12-06: aangepast: JT, aangepast naar instance of selectfield zodat niet elke afgeleide hierin geprogged hoeft te worden.
					if($form_field instanceof SelectField){

						// chk op src
						if ($field->srcScheme != '' && $field->srcTable != '' && $field->srcField != '') {
							$sql = array();
							$where = 'WHERE 1=1';

							$cursors = self::_get_cursors($this->name, $field->srcScheme, $field->srcTable);

							// where dingest voor de cursors
							foreach ($cursors as $cursor) {
								foreach ($cursor->conditions as $condition) {
									$where .= ' AND '.$condition->sql();// sprintf (' %s %s \'%s\'', $condition->field, $condition->operator, $condition->value);
								}
							}

							if ($field->srcConstraint!= '') {
								$where .= ' '.$field->srcConstraint;
							}

							// note: 2007-06-01 Jorgen: SELECT DISTINCT eruit gehaald vanwege een bug in mysql 4.1.20 waarbij de sortering mis gaat
							$sql = sprintf("
								SELECT
									%s as name,
									%s as value
								FROM
									%s.%s
								%s
							",
								DB::quote($field->srcField),
								coalesce($field->srcLabel, $field->srcField), /* niet quoten omdat er SQL in mag staan */
								DB::quote($field->srcScheme),
								DB::quote($field->srcTable),
								$where
							);

							$res2= DB::iQuery($sql);
							if (TESTMODE && self::isError($res2)) {
								throw new Exception("Error in query: ".$sql." in class: ".__CLASS__
									." on line: ".__LINE__." mysqlError:".$res2->errstr);
							}

							$options = array();
							if ($res2->num_rows > 0) {
								foreach ($res2->result as $row2) {
									$option = new FieldOption($row2->name, $row2->value);

									// bij multiselects zijn er een aantal options geselecteerd
									if (isset($multi_select_fields[$field->dstScheme][$field->dstTable])) {
										foreach ($multi_select_fields[$field->dstScheme][$field->dstTable] as $select_obj) {
											//foreach ($selecties as $select_obj) {
												if ($option->name == $select_obj->{$field->dstField}) {
													$option->selected = TRUE;
												}
											//}
										}
									}

									$options[] = $option;
								}
							}

							$form_field->options = $options;

						// misschien een ENUM
						} elseif ($field->srcField != '') {
							$_options = unserialize($field->srcField);
							$options = array();
							foreach ($_options as $key => $value) {
								$options[] = new FieldOption($key, $value);
							}

							$form_field->options = $options;
						}
					}

					// data zetten
					if (NULL != $row && isset($row->{$field->name})) {
						$form_field->value = self::_convert_from_db($field->fieldtype, $row->{$field->name});
					}

					$field->formField = $form_field;
					$form_fields[] = $form_field;
				}
			}
		}


		$this->cnt_records = $records;
		$this->record_nr   = $record_nr;

		// sort fields on prio
		oksort($form_fields, 'prio');

		// return fields
		return $form_fields;
	}

	/**
	* Converteer functie van DB naar Field
	* TODO dit zou in veld opgelost moeten worden denk ik. met bijv een setdata functie oid om verschil tussen ruwe data en value kenbaar te maken 12/19/2006
	*/
	private static function _convert_from_db($fieldtype, $data) {
		switch ($fieldtype) {
			case 'DateField':
				return new MySQLDate($data);
				break;

			case 'GenderField':
				switch (strtolower($data)) {
					case 'm':
						return Gender::male;
						break;

					case 'f':
					case 'v':
						return Gender::female;
						break;

					default:
						return Gender::unknown;
						break;
				}
				break;

			case 'BooleanField':
				switch ($data) {
					case 1:
						return 'Ja';
						break;

					case 0:
						return 'Nee';
						break;
				}
				break;

			default:
				return $data;
/*
			case 'PhotoUploadField':
			case 'FileUploadField':
				return strlen($data) > 0;
				break;*/
		}
	}

	private static function _convert_to_db($fieldtype, $data) {
		switch ($fieldtype) {
			case 'DateField':
				if ($data instanceof BrickDateTime) {
					return $data->date('Y-m-d');
				} else {
					return NULL;
				}

			case 'GenderField':
				switch ($data) {
					case Gender::unknown:
					case Gender::male:
						return 'm';
						break;

					case Gender::female:
						return 'f';
						break;
				}
				break;

			case 'BooleanField':
				switch (strtolower($data)) {
					case 1:
					case 'ja':
					case 'yes':
						return 1;
						break;

					case 0:
					case 'nee':
					case 'no':
						return 0;
						break;
				}
				break;
			case 'PhotoField':
			case 'PhotoUploadField':
				if(is_object($data)){
					$data = $data->toArray();
				}

				if (isset($data['del']) || $data == NULL) {
					return NULL;
				}

				if ($data['error'] == UPLOAD_ERR_NO_FILE) {
					return -1;
				}

				if (!is_array($data)) {
					$data = unserialize($data);
				}

				// photo's
				if (in_array($data['type'], PhotoUploadField::getAllowedMimeTypes())) {
					return serialize($data);
				} else {
					return new ErrorVoedingonline(E_USER_NOTICE, 'Bestand is geen foto');
				}

				return NULL;
				break;
			case 'FileField':
			case 'FileUploadField':
				if(is_object($data)){
					$data = $data->toArray();
				}

				if ($data['error'] != UPLOAD_ERR_NO_FILE) {
					return serialize($data);
				}

				return NULL;
				break;

			default:
				return $data;
		}
	}

	public function handleRequest($request) {

		// before handle request events
		if(!$this->onBeforeHandleRequest()){
			// if errors occur proceed! Errors will be handled later after storing the values in the fields
			// gewoon doorgaan dus
		}
		// indien er iets is gerequest dan kijken of er cursors instaan
		$controls = array();

		if (isset(self::$request_[str_replace(' ', '_', $this->name)]['controls'])) {
			$controls = self::$request_[str_replace(' ', '_', $this->name)]['controls'];
		}

		// niets te doen
		if (count($controls) == 0) {
			return FALSE;
		}

		// we hoeven niets te verwerken
		if (count(array_intersect(array('next', 'previous'), array_keys($controls))) > 0) {
			return FALSE;
		}

		// zet velden op juiste waarde
		if (!parent::handleRequest($request)) {
			return FALSE;
		}

		// nu hebben de velden wel de juiste waarde, maar nu willen we alle velden updaten of inserten in de juiste tabellen
		// daarom lopen we alle schemas en tabellen langs en bouwen we de queries op

		// foreach scheme and table set field en load data indien cursor bestaat voor die tabel
		foreach ($this->_schemes as $scheme => $tables) {
			foreach ($tables as $table => $fields) {

				// chk for cursorfield
				$selector = array();
				foreach($fields as $field) {
					
					if ($field->fieldtype == 'CursorField') {
						// dit is het geselecteerde record binnen een form met meerdere records (multiplicity > 1)
						// $selector[$field->name] = $result->result[0]->{$field->name}; // current

						if (isset(self::$request_[$this->name][$field->name]))
							$selector[$field->name] = self::$request_[$this->name][$field->name];
					}
				}

				// container indien er data wordt geselecteerd
				$row = NULL;

				$cursors = self::_get_cursors($this->name, $scheme, $table);

				// haal data op voor alle velden, indien er een cursor voor bestaat
				if (NULL != $cursors) {

					// loop door alle velden en maak save wijzigingen
					if (count(self::$_cursors) > 0) {
						$control = key($controls);

						// TODO onderstaande cases misschien in functies vangen, want dit wordt wel heel veel nu

						// chk de controls op wat we moeten gaan doen
						switch ($control) {
							// wijzigen
							case 'save':

								// alle werkdingen initten en leeg maken
								$sql = array();

								// updateten
								$update_fields  = array();

								// data voor velden ophalen
								foreach ($fields as $field) {
									if (Multiplicity::greater($field->multiplicity, 1)) {
										// veld zelf bijwerken
										$this->_save_lookup_table($scheme, $table, $field);
									} else {
										// converteer dataobj van field naar db snapbaar formaat
										$data = self::_convert_to_db($field->fieldtype, $field->formField->value);

										if (self::isError($data)) {
											$this->errors_[] = $data;
											return FALSE;
										}

										// bij bestanden die hetzelfde blijven moet er -1 worden doorgegeven

										// driemaal anders zijn lege strings ook NULL
										if (NULL === $data) {
											//continue;
											$update_fields[] = sprintf('%s = NULL', DB::quote($field->name));
										} elseif (-1 === $data) {
										} else {
											$update_fields[] = sprintf('%s = \'%s\'', DB::quote($field->name), DB::quote($data));
										}
									}
								}

								// chk of er nog te updateten velden zijn
								if (count($update_fields) > 0) {
									// where dingest voor de cursors
									foreach ($cursors as $cursor) {
										foreach ($cursor->conditions as $condition) {
											$sql[] = $condition->sql(); //sprintf (' %s %s \'%s\'', $condition->field, $condition->operator, $condition->value);
										}
									}

									// selecttor
									$selector_sql = '';
									if (count($selector) > 0) {
										$sql[] = sprintf('%s = \'%s\'', DB::quote(key($selector)), DB::quote($selector[key($selector)]));
									}

									// sql maken
									if (count($sql) > 0) {
										$tellert = 0;
										foreach ($sql as $dingest) {
											if ($tellert++ == 0) {
												$yadda = ' WHERE ';
											} else {
												$yadda .= ' AND ';
											}

											$yadda .= $dingest;
										}
									}

									// sql
									$sql = sprintf("
										UPDATE
											%s.%s
										SET
											%s
										%s
									",
										DB::quote($scheme),
										DB::quote($table),
										commalize($update_fields),
										$yadda
									);

									$result = DB::iQuery($sql);

									if (self::isError($result)) {
										$this->errors_[] = $result;
										return FALSE;
									}

									// chk of er maar 1 rij is geupdateted
									if ($result->num_rows > 1) {
										$this->errors_[] = new ErrorVoedingonline(E_USER_NOTICE, sprintf('%d rows updated, expected one updated row', $result->num_rows));
										return FALSE;
									}
								}
								break;

							// toevoegen
							case 'add':
								$inserts = array();
								$insert_fields = array();

								foreach ($fields as $field) {
									// skip cursorfield
									if ($field->fieldtype == 'CursorField') {
										$cursorfield = $field;
										continue;
									}

									// veld heeft meer values die in lookup moeten worden opgeslagen
									if (Multiplicity::greater($field->multiplicity, 1)) {
										// veld zelf bijwerken
										$this->_save_lookup_table($scheme, $table, $field);
									} else {

										// converteer dataobj van field naar db snapbaar formaat
										if ($field->fieldtype == 'PhotoField'){
											$data = self::_convert_to_db($field->fieldtype, $field->formField->value);

											$insert_fields[] = DB::quote($field->name);
											$insert_values[] = DB::quote($data);

										} elseif(is_array($field->formField->value)) {
											$teller = 0;
											foreach ($field->formField->value as $v => $label) {
												$data = self::_convert_to_db($field->fieldtype, $v);
												if (self::isError($data)) {
													$this->errors_[] = $data;
													return FALSE;
												}

												if ($teller == 0) {
													$insert_fields[] = DB::quote($field->name);
												}

												$inserts[] = DB::quote($data);

												$teller++;
											}
										} else {

											$data = self::_convert_to_db($field->fieldtype, $field->formField->value);
											if (self::isError($data)) {
												$this->errors_[] = $data;
												return FALSE;
											}

											$insert_fields[] = DB::quote($field->name);
											$insert_values[] = DB::quote($data);
										}
									}
								}

								if (count($insert_fields) > 0) {
									// cursor toevoegen
									foreach ($cursors as $cursor) {
										foreach ($cursor->conditions as $condition) {
											if ($condition->operator == '=') {
												if (NULL != $condition->value) {
													$insert_fields[] = DB::quote($condition->field);
													$insert_values[] = DB::quote($condition->value);
												}
											}
										}
									}

									$sql = sprintf("
										INSERT INTO
											%s.%s (
											%s
										) VALUES (
											%s
										)
									",
										DB::quote($scheme),
										DB::quote($table),
										commalize($insert_fields),
										commalize($insert_values, TRUE)
									);

									// voer uit
									$result = DB::iQuery($sql);
									if (self::isError($result)) {
										// trigger_error('No result', E_USER_NOTICE);
										$this->errors_[] = $result;
										return FALSE;
									}

									// chk of er maar 1 rij is geupdateted
									if (FALSE === $result->num_rows) {
										trigger_error(sprintf('%d rows inserted, expected one inserted row', $result->num_rows), E_USER_NOTICE);
										$this->errors_[] = sprintf('%d rows inserted, expected one inserted row', $result->num_rows);
										return FALSE;
									}

									// cursor zetten, naar net nieuw toegevoegd record
									
									// Bugfix: lost last inserted id after insert
									// Bart 07-08-2008 
									// if (!empty($result->last_insert_oid) && isset($cursorfield)) {
									if (!is_null($result->last_insert_oid) && isset($cursorfield)) {
										self::$request_[$this->name][$cursorfield->name] = $result->last_insert_oid;
									}
								}

								break;

							// verwijderen
							case 'remove':
								//print 'remove';

								// in ieder geval een cursor nodig en bij mulitpliciteit ook een selector
								foreach ($fields as $field) {
									// pak cursorfield
									if ($field->fieldtype == 'CursorField') {
										$cursorfield = $field;
										break;
									}
								}

								// where dingest voor de cursors
								foreach ($cursors as $cursor) {
									foreach ($cursor->conditions as $condition) {
										$sql[] = $condition->sql(); //sprintf (' %s %s \'%s\'', $condition->field, $condition->operator, $condition->value);
									}
								}

								// selecttor
								$selector_sql = '';
								if (count($selector) > 0) {
									$sql[] = sprintf('%s = \'%s\'', DB::quote(key($selector)), DB::quote($selector[key($selector)]));
								}

								if (count($sql) > 0) {
									$tellert = 0;
									foreach ($sql as $dingest) {
										if ($tellert++ == 0) {
											$yadda = ' WHERE ';
										} else {
											$yadda .= ' AND ';
										}

										$yadda .= $dingest;
									}
								}

								$sql = sprintf("
									DELETE FROM
										%s.%s
									%s
								",
									DB::quote($scheme),
									DB::quote($table),
									$yadda
								);

								$result = DB::iQuery($sql);
								if (self::isError($result)) {
									trigger_error('No result', E_USER_NOTICE);
									$this->errors_[] = $result;
									return FALSE;
								}

								// chk of er maar 1 rij is geupdateted
								if (FALSE === $result->num_rows || 1 < $result->num_rows) {
									trigger_error(sprintf('%d rows deleteted, expected one deleteted row', $result->num_rows), E_USER_NOTICE);
									$this->errors_[] = new ErrorVoedingonline(E_USER_NOTICE, sprintf('%d rows deleteted, expected one deleteted row', $result->num_rows));
									return FALSE;
								}

								// cursor weghalen
								// cursor zetten, naar net nieuw toegevoegd record
								// self::$request_[$this->name][$cursorfield->name] = $result->last_insert_oid;
								self::$request_[$this->name][$cursorfield->name] = NULL;
								self::$request_[$this->name]['controls']['previous'] = 'previous';
								break;
						}
					}
				}
			}
		}

		// do we have required fields and valid values errors?
		if (count($this->errors_) > 0) {

			return FALSE;
		}

		// after handle request events
		if(!$this->onAfterHandleRequest()){
			return FALSE;
		}

		// reload alle gegevens omdat het form is aangepast
		$this->load();

		// handle request AND events succeeded, so we are happy ;)
		return TRUE;
	}

	/**
	* Move uploaded files to the specified upload folder
	*
	* @param UploadedFile[]
	* @return Boolean
	*/
	protected function move_uploaded_file_($file) {
		if ($file['error'] == UPLOAD_ERR_NO_FILE) {
			return $file;
		}

		if (is_uploaded_file($file['tmp_name'])) {
			// default in blob opslaan
			$handle = fopen($file['tmp_name'], "rb");
			$file['contents'] = fread($handle, filesize($file['tmp_name']));
			fclose($handle);

			return $file;
		}

		return FALSE;
	}

	private function _save_lookup_table($scheme, $table, $field) {
		// moet in een transactieblok plaatsvinden,
		// als de insert namelijk niet lukt moet de delete worden terugedraaid
		
		$sql = array();
		$yadda = '';
		$cursors = self::_get_cursors($this->name, $scheme, $table);

		// selector
		foreach ($cursors as $cursor) {
			foreach ($cursor->conditions as $condition) {
				$sql[] = $condition->sql(); //sprintf (' %s %s \'%s\'', $condition->field, $condition->operator, $condition->value);
			}
		}

		if (count($sql) > 0) {
			$tellert = 0;
			foreach ($sql as $dingest) {
				if ($tellert++ == 0) {
					$yadda = ' WHERE ';
				} else {
					$yadda .= ' AND ';
				}

				$yadda .= $dingest;
			}
		}

		$sql = sprintf("
			DELETE FROM
				%s.%s
			%s
		",
			DB::quote($scheme),
			DB::quote($table),
			$yadda
		);

		$result = DB::iQuery($sql);
		if (self::isError($result)) {
			trigger_error('No result', E_USER_NOTICE);
			$this->errors_[] = $result;
			return FALSE;
		}

		// eerste is veldwaarde, tweede is cursor
		$replace_fields[] = DB::quote($field->name);

		foreach ($cursors as $cursor) {
			foreach ($cursor->conditions as $condition) {
				$replace_fields[] = DB::quote($condition->field);
				$cursor_fields[]  = DB::quote($condition->value);
			}
		}

		// data voor velden ophalen
		// converteer dataobj van field naar db snapbaar formaat
		$data = self::_convert_to_db($field->fieldtype, $field->formField->value);

		// niets geselecteerd, deleten was dus genoeg
		if (!is_array($data) || count($data) == 0) {
			return;
		}

		foreach ($data as $key => $label) {
			$replace_values[] = sprintf('(\'%s\', \'%s\')', DB::quote($key), commalize($cursor_fields));
		}

		$sql = sprintf("
			INSERT INTO
				%s.%s (
				%s
			) VALUES
				%s
		",
			DB::quote($scheme),
			DB::quote($table),
			commalize($replace_fields),
			commalize($replace_values)
		);

		$result = DB::iQuery($sql);
		if (self::isError($result)) {
			trigger_error('No result', E_USER_NOTICE);
			$this->errors_[] = $result;
			return FALSE;
		}

		return TRUE;
	}
}

/**
* Indicator welke rij is geselecteerd
*/
class DBCursor {
	public $scheme;
	public $table;
	public $conditions = array();

	public function __construct($scheme, $table, $conditions) {
		$this->scheme     = $scheme;
		$this->table      = $table;
		$this->conditions = $conditions;
	}
}

/**
* Veldcoditie voor rijselectie
*/
class DBCursorCondition {
	/**
	* Fieldname
	* @var String
	*/
	public $field;

	/**
	* Operator SQL
	* @var String
	*/
	public $operator;

	/**
	* Field value
	* @var String
	*/
	public $value;

	public function __construct($field, $operator, $value) {
		$this->field    = $field;
		$this->operator = $operator;
		$this->value    = $value;
	}

	public function sql() {
		if (is_numeric($this->value)) {
			return sprintf (' %s %s %d', DB::quote($this->field), DB::quote($this->operator), $this->value);
		} elseif ($this->value == 'NULL') {
			if ($this->operator == '=') {
				return sprintf (' ISNULL(%s)', DB::quote($this->field));
			} elseif ($this->operator == '!=') {
				return sprintf (' NOT ISNULL(%s)', DB::quote($this->field));
			}
		} else {
			return sprintf (' %s %s \'%s\'', DB::quote($this->field), DB::quote($this->operator), DB::quote($this->value));
		}
	}
}

/**
* Databasefield
*/
class DBField extends Field {
	private $_fieldtype;

	private $_dst_scheme;
	private $_dst_table;
	private $_dst_field;

	private $_src_scheme;
	private $_src_table;
	private $_src_field;
	private $_src_label;
	private $_src_constraint;

	private $_label;

	// id van record in db
	protected $id_;

	// wordt overgeerft van field
	// private $_prio;

	// container voor origineel veld (extended van FieldDecorator)
	private $_form_field;

	/**
	* Maak een nieuw databaseveld
	*
	* @param String fieldtype
	* @param String dstSchema
	* @param String dstTable
	* @param String dstField
	* @param String label
	* @param Float prio
	*/
	public function __construct ($fieldtype, $scheme, $table, $field, $label, $prio) {
		if (empty($fieldtype)) {
			trigger_error('Fieldtype empty', E_USER_ERROR);
		}

		if (empty($scheme)) {
			trigger_error('Scheme empty', E_USER_ERROR);
		}

		if (empty($table)) {
			trigger_error('Table empty', E_USER_ERROR);
		}

		if (empty($field)) {
			trigger_error('Field empty', E_USER_ERROR);
		}

		$this->fieldtype = $fieldtype;

		$this->_dst_scheme    = $scheme;
		$this->_dst_table     = $table;
		$this->_dst_field     = $field;

		$this->label     = $label;
		$this->prio      = $prio;

		// name, value
		parent::__construct($this->dstField);
	}

	public function __set($name, $value) {
		switch ($name) {
			case 'fieldtype':
				$this->_fieldtype = $value;
				break;

			case 'dstScheme':
				$this->_dst_scheme = $value;
				break;

			case 'dstTable':
				$this->_dst_table = $value;
				break;

			case 'dstField':
				$this->_dst_field = $value;
				break;

			case 'srcScheme':
				$this->_src_scheme = $value;
				break;

			case 'srcTable':
				$this->_src_table = $value;
				break;

			case 'srcField':
				$this->_src_field = $value;
				break;

			case 'srcLabel':
				$this->_src_label = $value;
				break;

			case 'srcConstraint':
				$this->_src_constraint = $value;
				break;

			case 'multiplicity':
				$this->multiplicity_ = $value;
				break;

			case 'id':
				$this->id_ = $value;
				break;

			// TODO juiste benaming
			case 'formField':
				$this->_form_field = $value;
				break;

			default:
				try {
					parent::__set($name, $value);
				} catch (AttributeNotFoundException $e) {
					throw $e;
				}
				break;
		}
	}

	public function __get($name) {
		switch ($name) {
			case 'fieldtype':
				return $this->_fieldtype;
				break;

			case 'dstScheme':
				return $this->_dst_scheme;
				break;

			case 'dstTable':
				return $this->_dst_table;
				break;

			case 'dstField':
				return $this->_dst_field;
				break;

			case 'srcScheme':
				return $this->_src_scheme;
				break;

			case 'srcTable':
				return $this->_src_table;
				break;

			case 'srcField':
				return $this->_src_field;
				break;

			case 'srcLabel':
				return $this->_src_label;
				break;

			case 'srcConstraint':
				return $this->_src_constraint;
				break;

			case 'multiplicity':
				return $this->multiplicity_;
				break;

			/*case 'prio':
				return $this->_prio;
				break;*/

			case 'formField':
				return $this->_form_field;
				break;

			case 'id':
				return $this->id_;
				break;

			default:
				try {
					return parent::__get($name);
				} catch (AttributeNotFoundException $e) {
					throw $e;
				}
				break;
		}
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
?>
