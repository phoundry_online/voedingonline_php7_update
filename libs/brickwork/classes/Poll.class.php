<?php

/*
** Poll
**
** @author Sven Pool <pool@rr-solutions.nl>
** @last_modified 21/11/2006 */

class Poll {

	/*
	* @var int
	* @acces public
	*/
	public $id;
	public $question;

	public $answers = array();

	/**
	 * Hier zet je in als er al gestemt is op deze poll
	 * @var boolean
	 */

	public $voted	= FALSE;

	/**
	 * instelling vanuit de poll. Als deze op true staat mag de bezoeker
	 * het resultaat zien anders een melding bedankt.
	 * @var boolean
	 */
	public $show_result = FALSE;

	/**
	 * Totaal aantal votes
	 * @var integer
	 */
	public $totalVote = 0;


	public function __construct($poll_id, $question){
		$this->id			= (int)$poll_id;
		$this->question	= $question;
	}

	/**
	 * getPoll
	 *
	 * Get a poll by id
	 *
	 * @access	public
	 * @param	integer Poll id
	 * @return	Poll */
	static function getPoll($poll_id = NULL) {

			$extra_sql = '';

			if(!is_null($poll_id)) {
				$extra_sql = sprintf('AND pq.id = %d', $poll_id);
			}

			/*
			** Laten we de POLL gegevens ophalen
			** die bij deze pagina hoort. */
			/* 1 rij ophalen */
			$sql = sprintf('
								SELECT
									pq.id as question_id,
									pq.question,
									pq.show_result
								FROM
									module_content_poll_question pq
								WHERE
									pq.site_identifier = "%s"
								%s
								AND
									pq.visible="yes"
								LIMIT 1',
								/* values */
								DB::quote(Framework::$site->identifier),
								$extra_sql
						);

			/*
			** Ophalen gegevens uit de database */
			$poll_res = DB::iQuery($sql);

			if (self::isError($poll_res)){
				trigger_error($poll_res, E_USER_ERROR);
			}

			// poll gevonden, dus nu samenstellen
			if($poll_res->num_rows === 1){
				// antwoorden ophalen anders heeft de poll geen bestaansrecht:
				$sql = sprintf('
					SELECT
						pa.id,
						pa.answer
					FROM
						module_content_poll_answer pa
					WHERE
						pa.poll_id = %d
					ORDER BY
						pa.prio DESC,
						pa.id ASC',
					/* values */
						$poll_id
				);
				/*
				** Ophalen gegevens uit de database */
				$answer_res = DB::iQuery($sql);
				if (self::isError($answer_res)){
					trigger_error($answer_res, E_USER_ERROR);
				}

				// Alleen als er meer dan 1 antwoord is dan laten we iets zien
				if($answer_res->num_rows > 1) {
					// indien maar 1 antwoord, dan geven we geen poll terug
					$poll_row = $poll_res->first();
					
					$poll = new Poll($poll_row->question_id, $poll_row->question);
					$poll->show_result = $poll_row->show_result=='yes' ? true : false;

					while($answer_res->current())
					{
						$record = $answer_res->current();
						$answer = new PollAnswer($record->id, $record->answer);
						$poll->addAnswer($answer);
						$answer_res->next();
					}

					return $poll;
				} // einde antwoorden
			} // einde poll

		// geen poll gevonden:
		return NULL;
	}

	/**
	 * loadStatisticsByPoll
	 *
	 * Add statistics to a Poll object
	 *
	 * @access	public
	 * @param	Poll (reference)
	 * @return	boolean */
	static function loadStatisticsByPoll(Poll $poll) {
		$sql = sprintf("
			SELECT
				pa.id as id,
				count(pv.id) as nrVote
			FROM
				module_content_poll_answer pa
			INNER JOIN
					module_content_poll_vote pv
				ON
					pa.id = pv.answer_id
			WHERE
				pa.poll_id = %d
			GROUP BY
				pa.id
			ORDER BY
				pa.prio DESC, pa.id ASC
		",
		/* values */
			$poll->id
		);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			trigger_error($res, E_USER_ERROR);
		}

		$tmpArray = array();
		$totalVotes = 0;
/*
*****************
Wat als er nog helemaal geen records zijn
de aller eerste keer
*****************
*/
		if($res->current()) {
			/*
			** Kleine array met waardes opgebouwen */
			while($res->current()){
				$record = $res->current();
				$tmpArray[$record->id] = $record->nrVote;
				$res->next();
			}

			/*
			** Array is afgerond nu het POLL object 'verrijken' */
			foreach($poll->answers as $tmpAnswers) {
				if (isset($tmpArray[$tmpAnswers->id])) {
					$tmpAnswers->nrVote = $tmpArray[$tmpAnswers->id];
					$totalVotes = $totalVotes + $tmpArray[$tmpAnswers->id];
				} else {
					$tmpAnswers->nrVote = 0;
				}
			}

			/*
			** Totaal bijhouden */
			$poll->totalVote = $totalVotes;
		} else {
			/*
			** Er zijn nog geen resultaten binnen */
			return FALSE;
		}
		return $poll;
	}

	public function addAnswer(PollAnswer $answer){
		//$answer->poll = $this; RECURSION?
		$answer->nrVote = 0;
		$this->answers[$answer->id] = $answer;
	}

	/**
	 * storeVote
	 *
	 * To store a vote
	 *
	 * @access	public
	 * @param	int
	 * @return	boolean */
	public function storeVote($answer_id){
  		// eerst kijken of de poll wel zichtbaar is en of het antwoord id wel bij de poll hoort
		$sql = sprintf("
									SELECT
										count(*)
									FROM
										module_content_poll_question pq,
										module_content_poll_answer pa
									WHERE
										pq.id = pa.poll_id
									AND
										pq.id = %d
									AND
										pq.visible = 'yes'
									AND
										pa.id = %d",
									/* values */
									$this->poll_id,
									$answer_id
								);

		$res = DB::iQuery($sql);

		if (self::isError($res)){
         trigger_error($res, E_USER_ERROR);
      }

		if($res->num_rows == 1) {
			// het antwoord opslaan.
			$uniq = isset($this->cookie_array[$this->poll_id]) ? $this->cookie_array[$this->poll_id] : '';
			$sql = sprintf("
								INSERT INTO module_content_poll_vote (
									id,
									poll_id,
									answer_id,
									ipaddress,
									browser_hash,
									uniqid,
									vote_date
								)
								VALUES (
									null,
									%d,
									%d,
									INET_ATON('%s'),
									'%s',
									'%s',
									NOW()
								)",
								/* Values */
									$this->poll_id,
									$answer_id,
									$_SERVER['REMOTE_ADDR'],
									md5($_SERVER['HTTP_USER_AGENT']),
									DB::quote($uniq)
							);
			$res = DB::iQuery($sql);

			if (self::isError($res)){
				trigger_error($res, E_USER_ERROR);
			}
			return TRUE;
		}
		return FALSE;
	}

	/*
	** Check if there is voted
	**
	** @access	private
	** @param	int, array, array
	** @return	boolean */
	public function isVoted($poll_id, $cookie_array, $cookie = FALSE) {
		/*
		** Cookie bestaat eigenlijk niet,
		** dus bye bye */
		if ($cookie === FALSE) {
			unset($cookie);
		}

		// checken of er gestemd is aan de hand van het cookie
		if(isset($cookie_array[$poll_id]) && $cookie_array[$poll_id]===TRUE) {
			/*
			** Als het cookie zegt dat er gestemd is dan return true */
			return TRUE;
		} elseif(isset($cookie) && isset($cookie_array[$poll_id])) {
			$sql = sprintf("
								SELECT
									count(*) as nrVoted
								FROM
									module_content_poll_vote
								WHERE
									poll_id = %d
								AND
									uniqid = '%s'",
								/* values */
									$poll_id,
									DB::quote($cookie_array[$poll_id])
								);

			$res = DB::iQuery($sql);

			if (self::isError($res)){
				trigger_error($res, E_USER_ERROR);
			}

			if($res->first()->nrVoted > 0){
				/*
				** Er is al gestemd */
				return TRUE;
			}

			return FALSE;
		} else {
			/* zo niet checken aan de hand van de poll_vote tabel en het ipadres,
			** als er meer dan 5 keer gestemd is vanaf dit ip met dezelfde browser md5 op deze poll dan ook return true
			** ipadres zet je zo om naar een int: INET_ATON('$_SERVER['REMOTE_ADDR']') */

			// als er wel een cookie meegestuurd is moet er een index voor hem aangemaakt zijn en mag hij hier niet komen dus fouteboel
			if(isset($cookie)){
				return TRUE;
			} else {
				// als er geen cookie is checken we op ipadres
				$sql = sprintf("
									SELECT
										count(*) as nrVoted
									FROM
										module_content_poll_vote
									WHERE
										poll_id = %d
									AND
										ipaddress = INET_ATON('%s')",
									/* values */
										$poll_id,
										$_SERVER['REMOTE_ADDR']
									);

				$res = DB::iQuery($sql);

				if (self::isError($res)){
					trigger_error($res, E_USER_ERROR);
				}
			}

			// @Jorgen nrVoted kan hier toch ook niet bestaan?
			if($res->first()->nrVoted >= POLL_MAX_IPCOUNT) {
				return TRUE;
			}
		}
		return FALSE;
	}

	/**
	* Get a random poll if no id is selected
	*
	* @access	public
	* @return	boolean
	*/
	public function getRandomPoll(){
		$poll_id = NULL;

		$sql = sprintf("
								SELECT
									id
								FROM
									module_content_poll_question pq
								WHERE
									pq.site_identifier = '%s'
								AND
									visible='yes'
								ORDER BY rand() LIMIT 1",
								/* values */
								DB::quote(Framework::$site->identifier)
							);

		$result = DB::iQuery($sql);

		if (self::isError($result)){
         trigger_error($result->errstr, E_USER_ERROR);
      }
		if($result->num_rows == 1){
			$poll_id = $result->first()->id;
		}

		return $poll_id;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

class PollAnswer {
	public $id;
	public $answer;
	public $poll = NULL;
	public $nrVote = NULL;

	public function __construct($id, $answer){
		$this->id = (int)$id;
		$this->answer = $answer;
	}

	/*
	** Deze functie heb ik niet gebruikt
	** de daadwerkelijke berekening zit in het template.
	*/
	public function getPercentage(){
		if(is_null($this->poll) || is_null($this->nrVote)){
			return false;
		}

		// Bereken het getal om de percentages uit te rekenen
		if($this->poll->totalVote>0){
			$mod = 100 / $this->poll->totalVote;
		} else {
			$mod = 0;
		}

		return round($this->nrVote * $mod, 1);
	}
}
?>
