<?php
/**
* File
*
*
*/
class File {
	public $name;
	public $type;
	public $src;

	// defineer bestandstypes
	const local 	= 'file_local';
	const external	= 'file_external';
	const db       = 'file_db';

	/**
	* Lijst met mogelijke bestandstypes
	*/
	protected static $_types = array (
		File::local,	// lokaal bestand in dit filesystem
		File::external,	// extern bestand, url naar ftp/http oid
		File::db
	);

	public function __construct($src, $ft=File::local, $name='') {
		if (!in_array($ft, self::$_types)) {
			trigger_error('Type van bestand is onbekend', E_USER_ERROR);
			exit(1);
		}

		if (empty($name)) {
			$name = current(explode('.', $src));
		}

		$this->src = $src;
		$this->name = $name;
		$this->type = $ft;
	}

	public function copy($dest) {
		return copy(realpath($this->src), $dest);
	}

	static function get($src, $create = FALSE){
		// check if file is external
		if(Url::isUrl($src)){
			// File::external
			$url = new Url($src);

			if(self::external_file_exists($url)){
				// bestaat, aanmaken
				return new File($src, File::external, basename($url->getFileName()));
			}
		} else {
			// File::local

			// always make sure of the realpath
			$src = realpath(dirname($src)).'/'.basename($src);

			// if file not exists and we need to create, make an attempt
			if($create && !file_exists($src)){
				if(!@touch($src)){
					// when touch did not succeed there is no need to check again.
					return NULL;
				}
			}

			// check if file exists
			if(file_exists($src)){
				return new File($src, File::local, basename($src));
			}
		}
		return NULL;
	}
	
	/**
	 * Check if an external file exists on an other URL
	 *
	 * @param Url $url
	 * @deprecated use externalFileExists
	 * @return boolean
	 */
	static function external_file_exists(Url $url){ // 
		$debug = @debug_backtrace();
		error_log("File::external_file_exists deprecated usage in: ".$debug[0]['file'].", please use File::externalFileExists \n", 3, TMP_DIR.'/error.log');
		return self::externalFileExists($url);
	}
	
	/**
	 * Check if an external file exists on an other URL
	 *
	 * @param Url $url
	 * @return boolean
	 */
	static function externalFileExists(Url $url){
		$file = fsockopen($url->host, $url->port, $errno, $errstr, 12);

		// url zonder file niet accepteren
		$name = basename($url->getFilename());
		if(empty($name)){
			return FALSE;
		}

		if($file){
			fputs($file, sprintf("GET %s HTTP/1.1\r\n", $url->getFilename()));
			fputs($file, "Host: {$url->host}\r\n");
			fputs($file, "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)\r\n\r\n");


		 	$buffer = fread($file, 1024);
		 	if(substr($buffer,9,3)=='200'){
				return TRUE;
			} else {
				return FALSE;
			}
		}
		return FALSE;
	}
}
?>
