<?php
include_once('ACL/Object.class.php');

/**
* ContentContainer
*
* @author Jørgen Teunis
* @version 1.0.0
*/
class ContentContainer extends ACLObject implements Iterator, Countable
{
	/**
	* Code
	* @var string
	*/
	public $code;

	/**
	 * Boolean flag to indicate if the user can see the content of this container
	 *
	 * @var boolean
	 */
	public $allowed;
	
	/**
	* contentHandler
	* @var ContentHandler
	*/
	public $contentHandler;

	/**
	* Modules
	* @var Array
	*/
	public $modules = array();

	/**
	 * Constructor
	 *
	 * @param string $code
	 * @param ContentHandler $contentHandler
	 * @param int $page_id
	 */
	public function __construct($code, ContentHandler $contentHandler,
		$page_id = null)
	{
		$this->code = $code;
		$this->contentHandler = $contentHandler;
		$this->allowed = true; // Handles by the PageContentHandler
		
		parent::__construct(
			get_class($contentHandler) . '/' .
			($page_id ? $page_id . '/' : '') .
			$this->code,
			get_class($this), $this->code
		);
	}

	public function attach(Module $module)
	{
		$this->modules[] = $module;
	}

	public function detach($id)
	{
		$this->modules[$id]->html = false;
	}

	function debug()
	{
		foreach ($this->modules as $index => $mod) {
			print "{$index} => {$mod}<br />";
		}
	}

	public function current(): mixed
	{
		return current($this->modules);
	}

	public function prev(): void
	{
		prev($this->modules);
	}

	public function next(): void
	{
		next($this->modules);
	}

	public function key(): mixed
	{
		return key($this->modules);
	}

	public function valid(): bool
	{
		return $this->current() !== false;
	}

	public function rewind(): void
	{
		reset($this->modules);
	}
	
	public function count(): int
	{
		return count($this->modules);
	}
}
