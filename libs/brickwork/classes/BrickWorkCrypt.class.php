<?php

/**
 * Crypt functions
 *
 * @author	Web Power
 */

class BrickWorkCrypt {
	/**
	 * Encode a string
	 *
	 * @param string $str
	 * @return string
	 */
	static function encode($str) {
		$let = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890-';
		$str = base64_encode(gzcompress($str,9));
		return rawurlencode(substr($str,0,4) . substr($let,mt_rand(0,62),1) . substr($str,4));
	}
	/**
	 * Decode a string
	 *
	 * @param string $str
	 * @param boolean $urldecode
	 * @return string
	 */
	static function decode($str, $urldecode = true) {
		if ($urldecode)
			$str = rawurldecode($str);

		// levert wel soms een warning als er verkeerde / broken input is
		return @gzuncompress(base64_decode(substr($str,0,4) . substr($str,5)));
	}
	/**
	 * urlEncode and encrypt a list of parameters
	 *
	 * @param array $params
	 * @return string
	 */
	static function urlEncode($params) {
		$ret = '';
		foreach($params as $name=>$value) {
			if ($ret != '')
				$ret .= chr(255);
			$ret .= $name . '=' . $value;
		}
		return self::encode($ret);
	}

	/**
	 * urlDecode an encrypted string back to its parameters
	 *
	 * @param string $str
	 * @param boolean $urldecode
	 * @return array
	 */
	static function urlDecode($str, $urldecode = true) {
		$params = array();
		$str = self::decode($str, $urldecode);
		if ($str === false)
			return false;
		$tmp1 = explode(chr(255), $str);
		foreach($tmp1 as $param) {
			$tmp2 = explode('=', $param, 2);
			$params[$tmp2[0]] = $tmp2[1];
		}
		return $params;
	}

	/**
	 * Encode a whole array to an encrypted BrickWorkCrypt string
	 *
	 * @param array $array
	 * @return string
	 */
	static public function encodeArray(array $array)
	{
		return self::encode(http_build_query($array));
	}

	/**
	 * Decode a BrickworkCrypt string to an array
	 *
	 * @param string $str
	 * @return array
	 */
	static public function decodeArray($str)
	{
		parse_str(self::decode($str), $temp);
		return $temp;
	}
}
