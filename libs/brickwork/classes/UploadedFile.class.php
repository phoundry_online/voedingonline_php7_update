<?php
/**
 * Class with sort functions for the $_FILES[] array
 * 
 * @deprecated
 * @author unknown
 * @author Stefan Grootscholten <stefan.grootscholten@webpower.nl>
 */
class UploadedFile
{
	/**
	 * @var array Cache for files
	 */
	private static $files;

	/**
	 * Sort uploaded file array
	 * This array is malsorted and not very usefull in the rest of the program,
	 * so sort this array and return a usable list of uploaded files
	 */
	public static function sortUploadedFiles($files, $cache_code)
	{
		if ($cache_code && isset(self::$files[$cache_code])) {
			return self::$files[$cache_code];
		}


		$uploaded_files = array();
		$file_names = array();

		if (!isset($files['name'])) {
			return false;
		}

		foreach (array_keys($files['name']) as $u_name) {
			$uploaded_files[$u_name] = array (
				'name' => $files['name'][$u_name],
				'type' => $files['type'][$u_name],
				'tmp_name' => $files['tmp_name'][$u_name],
				'error' => $files['error'][$u_name],
				'size' => $files['size'][$u_name]
			);
		}

		if ($cache_code) {
			self::$files[$cache_code] = $uploaded_files;
		}
		return $uploaded_files;
	}

	/**
	 * Sort uploaded file array
	 * This array is malsorted and not very usefull in the rest of the program,
	 * so sort this array and return a usable list of uploaded files
	 * @param array $files
	 * @param string $name
	 * @param string $cache_code
	 * @return array
	 */
	public static function getUploadedFile($files, $name, $cache_code = null)
	{
		$files = self::sortUploadedFiles($files, $cache_code);
		return $files[$name];
	}
}

