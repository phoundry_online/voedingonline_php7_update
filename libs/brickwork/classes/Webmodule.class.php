<?php
include_once('ACL/Object.class.php');

/**
 * Webmodule
 *
 * @abstract
 * @package Module
 * @version 1.1.0
 * @author Jorgen Teunis <jorgen.teunis@webpower.nl>
 * @access public
 *
 * $Id: Webmodule.class.php 1932 2010-09-14 08:15:20Z christiaan.baartse $
 */
abstract class Webmodule extends Module implements ACLObjectInterface
{

	/**
	 * Unique instance id
	 * @var int
	 */
	public $id;

	/**
	 * use cache
	 * @var bool
	 */
	protected $_use_cache_ = false;

	/**
	 * cache_lifetime
	 * seconds to cache the template
	 *
	 * @var int
	 */
	protected $cache_lifetime = 0;

	/**
	 * filename
	 * @var string
	 */
	public $template = null;
	
	/**
	 * Should the module obey page authentication
	 * @var bool
	 */
	public $respect_auth = true;

	/**
	 * prefix for query string params
	 *
	 * @var string
	 */
	protected static $qs_prefix = 'mod';

	/**
	 * cacheparameters (2 dimensional array)
	 * to store the parameters and values this module can be cached on
	 * gets filled default by PageContentHandler with, get, post, cookie,
	 * session, config and unique id hashed.
	 * That will cover the most importand parameters.
	 * Others can be added from within the loadExtraCacheParams method.
	 *
	 * @var array
	 */
	public $cacheParams = array();

	/**
	 * content table name
	 *
	 * @var string
	 */
	public $content_table = null;

	/**
	 * config table name
	 * @var string
	 */
	public $config_table = null;

	/**
	 * config object with settings
	 *
	 * @var ModuleConfig
	 */
	public $config = null;

	/**
	 * module name
	 * @var string
	 */
	public $name = '';
	
	/**
	 * module code
	 * @var string
	 */
	public $code = '';

	/**
	 * Generated html content
	 *
	 * @var string
	 */
	public $html = null;

	/**
	 * contentContainer
	 * @var ContentContainer
	 */
	public $contentContainer = null;

	/**
	 * Module get variables
	 * @var array
	 */
	public $get = array();

	/**
	 * Module post variables
	 * @var array
	 */
	public $post = array();

	/**
	 * Module request variables
	 * @var array
	 */
	public $request = array();

	/**
	 * Module cookie variables
	 * @var array
	 */
	public $cookie = array();

	/**
	* Module session variables
	* @deprecated Use the session directly instead!
	* @var array
	*/
	public $session = array();

	/**
	* Is NULL if the check isn't done. Sets TRUE or FALSE if check is done
	*
	* @var bool|void
	*/
	public static $errorTableExist = null;

	/**
	 * Constructor
	 *
	 * @param int $id
	 * @param ContentContainer $cc
	 * @param string $code
	 */
	public function __construct($id, ContentContainer $cc, $code = null)
	{
		$this->id = (int)$id;
		$this->contentContainer = $cc;
		$this->contentHandlerName = get_class($cc->contentHandler);
		$this->code = $code;

		// does not work with dynamic $_{$name} variables, thats why this is written out fully
		// GET
		$get1 = $get2 = array();
		if(isset($_GET[self::$qs_prefix][get_class($this)]) && is_array($_GET[self::$qs_prefix][get_class($this)])){
			$get1 = $_GET[self::$qs_prefix][get_class($this)];
		}

		if(isset($_GET[self::$qs_prefix][$this->id]) && is_array($_GET[self::$qs_prefix][$this->id])){
			$get2 = $_GET[self::$qs_prefix][$this->id];
		}
		$this->get = array_merge($get1, $get2);
		// /einde GET


		// POST
		$post1 = $post2 = array();
		if(isset($_POST[self::$qs_prefix][get_class($this)]) && is_array($_POST[self::$qs_prefix][get_class($this)])){
			$post1 = $_POST[self::$qs_prefix][get_class($this)];
		}

		if(isset($_POST[self::$qs_prefix][$this->id]) && is_array($_POST[self::$qs_prefix][$this->id])){
			$post2 = $_POST[self::$qs_prefix][$this->id];
		}
		$this->post = array_merge($post1, $post2);
		// /einde POST

		// REQUEST
		$request1 = $request2 = array();
		if(isset($_REQUEST[self::$qs_prefix][get_class($this)]) && is_array($_REQUEST[self::$qs_prefix][get_class($this)])){
			$request1 = $_REQUEST[self::$qs_prefix][get_class($this)];
		}

		if(isset($_REQUEST[self::$qs_prefix][$this->id]) && is_array($_REQUEST[self::$qs_prefix][$this->id])){
			$request2 = $_REQUEST[self::$qs_prefix][$this->id];
		}
		$this->request = array_merge($request1, $request2);
		// /einde REQUEST

		// COOKIE
		$cookie1 = $cookie2 = array();
		if(isset($_COOKIE[self::$qs_prefix][get_class($this)]) && is_array($_COOKIE[self::$qs_prefix][get_class($this)])){
			$cookie1 = $_COOKIE[self::$qs_prefix][get_class($this)];
		}

		if(isset($_COOKIE[self::$qs_prefix][$this->id]) && is_array($_COOKIE[self::$qs_prefix][$this->id])){
			$cookie2 = $_COOKIE[self::$qs_prefix][$this->id];
		}
		$this->cookie = array_merge($cookie1, $cookie2);
		// /einde COOKIE

		// SESSION
		$session = Session_Manager::factory();
		$session1 = $session2 = array();
		if(isset($session->{self::$qs_prefix})) {
			if(isset($session->{self::$qs_prefix}[get_class($this)]) && is_array($session->{self::$qs_prefix}[get_class($this)])){
				$session1 = $session->{self::$qs_prefix}[get_class($this)];
			}
	
			if(isset($session->{self::$qs_prefix}[$this->id]) && is_array($session->{self::$qs_prefix}[$this->id])){
				$session2 = $session->{self::$qs_prefix}[$this->id];
			}
		}
		$this->session = array_merge($session1, $session2);
		// /einde SESSION

	}
	
	/**
	 * Construct a new Webmodule instation
	 * 
	 * @todo maybe the construct should work like this...
	 * @param string $class
	 * @param int $content_id
	 * @param string|ContentContainer $container
	 * @param string $template
	 * @param string $content_table
	 * @param string $config_table
	 * @param string $name
	 * @param string $code
	 * @return Webmodule
	 */
	public static function constructModule($class, $content_id, $container,
		$template, $content_table, $config_table, $name, $code)
	{
		if(!$container instanceof ContentContainer) {
			$container = new ContentContainer($container,
				Framework::$contentHandler);
		}
		
		$module = self::factory($class, $content_id, $container, $code);
		$module->setTemplate($template);

		$module->content_table = $content_table;
		$module->config_table = $config_table;
		$module->name = $name;
		$module->code = $code;

		$module->loadConfig();
		$module->fetch();
		$module->unloadData();

		return $module;
	}

	/**
	 * Destructor
	 * @deprecated
	 */
	public function __destruct(){}
	
	public function unloadData()
	{
		if (!is_null($this->session)){
			$session = Session_Manager::factory();
			if(!isset($session->{self::$qs_prefix}) || !is_array($session->{self::$qs_prefix})) {
				$session->{self::$qs_prefix} = array();
			}
			$tmp = $session->{self::$qs_prefix};
			$tmp[$this->id] = $this->session;
			$session->{self::$qs_prefix} = $tmp;
		}

		// clear module cache once in a thousand times
		if(500 === rand(1,1000)){
			Framework::$representation->clear_cache(null,
				get_class($this->contentContainer->contentHandler).'|'.
					$this->contentContainer->contentHandler->id.'|'.
					$this->contentContainer->code.'|'.get_class($this),
				null,
				$this->cache_lifetime
			);
		}
	}

	/**
	 * @return array with cache parameters for the module
	 */
	public function getCacheParams()
	{
		return $this->cacheParams;
	}

	/**
	 * PageContenthandler needs this lifetime
	 *
	 * @return int lifetime in seconds
	 */
	public function getCacheLifetime()
	{
		return $this->cache_lifetime;
	}

	/**
	 * Should this module be cached
	 *
	 * @return bool|void
	 */
	public function isCachable()
	{
		if(!defined('USE_TEMPLATE_CACHE')) {
			return;
		}
		if(USE_TEMPLATE_CACHE === false) {
			return;
		}
		return ($this->_use_cache_ === true && $this->cache_lifetime > 0);
	}

	/**
	 * @todo live en ontwikkelserver gelijk trekken en dan deze opties eruit
	 * @deprecated
	 * @see Webmodule#isCacheble()
	 */
	public function is_cachable() { return $this->isCacheble(); }

	/**
	 * Set cache lifetime
	 *
	 * @param int $lt
	 * @return bool if cache will be used or not
	 */
	public function setCacheLifetime($lt)
	{
		$this->cache_lifetime = (int) $lt;
		if($lt > 0) {
			$this->_use_cache_ = true;
		}
		return $this->_use_cache_;
	}

	/**
	 * @return string template filename
	 */
	public function getTemplate()
	{
		return $this->template;
	}

	/**
	 * @param string template filename
	 * @return void
	 */
	public function setTemplate($filename)
	{
		$this->template = (string) $filename;
	}

	
	/**
	 * Laad config van een module
	 *
	 * Indien module wordt geextend en er is geen config tabel aanwezig maak
	 * dan een loadConfig functie aan die true terug geeft
	 *
	 * @access public
	 */
	public function loadConfig()
	{
		if($this->config_table && !$this->config) {
			$res = DB::iQuery(sprintf('
				SELECT
					*
				FROM
					%s
				WHERE
					page_content_id = %d',
				DB::quote($this->config_table),
				$this->id
			));
	
			if($config = $res->setReturnClass('ModuleConfig')->first()) {
				$this->config = $config;
				return true;
			}
			$this->config = new ModuleConfig();
			return false;
		}

		// geen config dus altijd true
		$this->config = new ModuleConfig();
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Module#loadData()
	 */
	protected function loadData()
	{
		return true;
	}

	/**
	 * Haal de HTML van deze module op en ken die toe aan this->html
	 *
	 * @return bool
	 */
	public function fetch()
	{
		$rep = Framework::$representation;
		if (!($rep instanceof WebRepresentation)) {
			trigger_error(sprintf('No representation layer available',
				$this->template, Framework::$site->identifier), E_USER_ERROR);
		}
		
		$rep->setTemplate("modules/{$this->template}");
		
		$cache = false;
		$cache_params = null;
		if ($this->_use_cache_ && $this->cache_lifetime > 0) {
			// Pipes are seen as directories for Smarty
			$cache_params =
				get_class($this->contentContainer->contentHandler).'|'.
				get_class($this).'|'.
				brickHash::get($this->getCacheParams());
				
			// http://www.smarty.net/manual/en/variable.caching.php
			$rep->caching = 2;
			$cache = $rep->setCaching($cache_params, $this->cache_lifetime);
			
		}
		else {
			$rep->cache_lifetime = 0;
		}

		if ($cache !== false) {
			$this->html = $cache;
		}
		else {
			// indien data geladen kan worden, het template laden en vullen met html
			// Webmodule heeft geen loadData functie, maar is abstract, dus prima
			if (false !== ($data = $this->loadData())) {
				// Load module into its template
				$rep->clear_assign('module');
				$rep->assign('module', $this);
				
				if (is_array($data)) {
					$old_values = array();
					foreach (array_keys($data) as $key) {
						$old_values[$key] = $rep->get_template_vars($key);
					}
					$rep->assign($data);
				}
				
				// Render the template and fetch the returned html
				$this->html = $rep->out(true, $cache_params);
				
				if (!empty($old_values)) {
					$rep->assign($old_values);
				}
				
				$rep->caching = 0;
			}
			else {
				// We couldn't find cache nor loadData
				return false;
			}
		}
		
		$this->postFetch();
		
		return true;
	}
	
	/**
	 * Template method that gets called after fetch completes succesfully
	 */
	public function postFetch() {}

	/**
	 * Factory
	 *
	 * @param string $class name of the webmodule class
	 * @param int $instance_id unique instance id from page_content table
	 * @param ContentContainer $cc
	 * @param string $code
	 * @return Webmodule|void constructed Module or null when class not found
	 */
	public static function factory($class, $instance_id, ContentContainer $cc,
		$code)
	{
		if(!class_exists($class, true)) {
			trigger_error(sprintf('Module \'%s\' does not exists', $class),
				E_USER_NOTICE);
			return null;
		}

		return new $class((int) $instance_id, $cc, (string) $code);
	}

	/**
	 * Creates a query string compatible name for use as the name attribute of a
	 * inputtype or for use in urls.
	 *
	 * @param string $name
	 * @param bool $global Module type wide
	 * @return string
	 */
	public function qsPart($name, $global = false)
	{
		return sprintf(
			'%s[%s][%s]',
			self::$qs_prefix ? self::$qs_prefix : 'undefined',
			$global ? get_class($this) : (int) $this->id,
			$name
		);
	}

	/**
	 * @return string classname
	 */
	public function __toString()
	{
		return get_class($this);
	}

	/**
	 * Implement this method in your Webmodule extend to specify extra values
	 * that will be used to create a cache hash.
	 * This method is called after the construct and loadconfig methods.
	 */
	public function loadExtraCacheParams() {}

	/**
	 * Fetch a module by its page content id
	 * 
	 * @param int $id
	 * @return Webmodule
	 */
	public static function getModuleByPageContentId($id)
	{
		$id = (int) $id;
		
		if($id) {
			$res = DB::iQuery(sprintf('
				SELECT
					pc.id as instance_id,
					pc.content_container_code,
					p.page_type_code,
					m.code,
					m.class,
					m.template,
					m.name,
					pt1.name as content_table,
					pt2.name as config_table
				FROM
					brickwork_page_content pc
				INNER JOIN
					brickwork_page p
				ON
					p.id = pc.page_id
				AND
					NOW() BETWEEN p.online_date
				AND
					p.offline_date
				INNER JOIN
					brickwork_module m ON m.code = pc.module_code
				LEFT JOIN
					phoundry_table pt1 ON pt1.id = m.ptid_content
				LEFT JOIN
					phoundry_table pt2 ON pt2.id = m.ptid_config
				WHERE
					pc.id = %d
				AND
					now() BETWEEN pc.online_date
				AND
					pc.offline_date
			',
				$id
			));
	
			if($m = $res->first()) {
				return self::constructModule($m->class, $m->instance_id,
					$m->content_container_code, $m->template, $m->content_table,
					$m->config_table, $m->name, $m->code);
			}
		}
		trigger_error('Geen module gevonden voor dit page_content id',
			E_USER_NOTICE);
		return false;
	}

	/**
	* Get module by classname
	* Hou rekening met de on en offline datae van zowel de pagina als de module
	*
	* @param String name
	* @return Module
	* @access public
	* @static
	*/
	public static function getModuleByClassName($name)
	{
		$name = (string) $name;
		if($name) {
			if($m = self::getModuleDataByClassName($name)) {
				return self::constructModule($m->class, $m->instance_id,
					$m->content_container_code, $m->template, $m->content_table,
					$m->config_table, $m->name, $m->code);
			}
		}
	//	trigger_error(sprintf('No module found by this classname: %s', $name),E_USER_NOTICE);
		return false;
	}
	
	/**
	 * Get the module data by either class or code
	 * 
	 * @param string $name
	 * @return stdClass|bool
	 */
	public static function getModuleDataByClassName($name)
	{
		$name = (string) $name;
		if (!$name) {
			return false;
		}
		
		$sql = sprintf("
			SELECT
				pc.id as instance_id,
				pc.content_container_code,
				p.page_type_code,
				m.code,
				m.class,
				m.template,
				m.name,
				pt1.name as content_table,
				pt2.name as config_table
			FROM
				brickwork_page_content pc
			INNER JOIN
				brickwork_page p ON p.id = pc.page_id
			AND
				'%s' BETWEEN p.online_date
			AND
				p.offline_date
			INNER JOIN
				brickwork_module m ON m.code = pc.module_code
			INNER JOIN
				brickwork_site_structure s ON s.id = p.site_structure_id
			AND
				s.site_identifier = '%s'
			LEFT JOIN
				phoundry_table pt1 ON pt1.id = m.ptid_content
			LEFT JOIN
				phoundry_table pt2 ON pt2.id = m.ptid_config
			WHERE
				(m.class = '%s' OR m.code = '%s')
			AND
				'%s' BETWEEN pc.online_date
			AND
				pc.offline_date
			LIMIT 1",
			date("Y-m-d H:i:s", PHP_TIMESTAMP),
			Framework::$site->identifier,
			DB::quote($name),
			DB::quote($name),
			date("Y-m-d H:i:s", PHP_TIMESTAMP)
		);
		return DB::iQuery($sql)->first();
	}
	
	/**
	 * Get a fake module instantiation by classname or code
	 * 
	 * @param string $code Code or classname
	 * @return Webmodule
	 */
	public static function getFakeModuleByCode($code)
	{
		$code = (string) $code;
		if($code) {
			$res = DB::iQuery(sprintf('
				SELECT
					m.code,
					m.class,
					m.template,
					m.name,
					pt1.name as content_table,
					pt2.name as config_table
				FROM
					brickwork_module m
				LEFT JOIN
					phoundry_table pt1 ON pt1.id = m.ptid_content
				LEFT JOIN
					phoundry_table pt2 ON pt2.id = m.ptid_config
				WHERE
					m.class = %1$s
				OR
					m.code = %1$s
				LIMIT 1
				',
				DB::quote($code, true)
			));

			if($m = $res->first()) {
				return self::constructModule($m->class, 0,
					"none", $m->template, $m->content_table,
					$m->config_table, $m->name, $m->code);
			}
		}
		// trigger_error(sprintf('No module found by this classname: %s', $code), E_USER_NOTICE);
		return false;
	}
	
	/**
	 * Delete content associated to a page content
	 * @param int $page_content_id
	 * @return bool
	 */
	public static function deleteContent($page_content_id)
	{
		$res = DB::iQuery(sprintf('
			SELECT
				pt.name as table_name
			FROM
				brickwork_page_content pc
			INNER JOIN
				brickwork_module m ON m.code = pc.module_code
			LEFT JOIN
				phoundry_table pt ON pt.id = m.ptid_content
			WHERE
				pc.id = %d
			', $page_content_id
		));
		
		if($row = $res->first()) {
			if ($row->table_name) {
				try {
					DB::iQuery(sprintf('
						DELETE FROM
							`%s`
						WHERE
							`page_content_id` = %d
						',
						$row->table_name,
						$page_content_id
					));
					
					return true;
					
				} catch(DB_Exception $e){}
			}
		}
		
		return false;
	}
	
	/**
	 * Delete config associated to a page content
	 * @param int $page_content_id
	 * @return bool
	 */
	public static function deleteConfig($page_content_id)
	{
		$res = DB::iQuery(sprintf('
			SELECT
				pt.name as table_name
			FROM
				brickwork_page_content pc
			INNER JOIN
				brickwork_module m ON m.code = pc.module_code
			LEFT JOIN
				phoundry_table pt ON pt.id = m.ptid_config
			WHERE
				pc.id = %d
			', $page_content_id
		));
		
		if($row = $res->first()) {
			if ($row->table_name) {
				try {
					DB::iQuery(sprintf('
						DELETE FROM
							`%s`
						WHERE
							`page_content_id` = %d
						',
						$row->table_name,
						$page_content_id
					));
					
					return true;
					
				} catch(DB_Exception $e){}
			}
		}
		
		return false;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/ACL/ACLObjectInterface#getInstanceId()
	 */
	public function getInstanceId()
	{
		return get_class($this).'/'.$this->id ;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/ACL/ACLObjectInterface#getInstanceName()
	 */
	public function getInstanceName()
	{
		return get_class($this).' table id ['.$this->id.']';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/ACL/ACLObjectInterface#getParent()
	 */
	public function getParent()
	{
		$object = new ACLObject($this->getInstanceId(), get_class($this));
		return $object->getParent();
	}
}
