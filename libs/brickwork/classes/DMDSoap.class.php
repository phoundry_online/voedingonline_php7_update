<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("Usage of DMDSoap is deprecated in favor of DMD_SOAP", E_USER_ERROR);
}

/*
TODO:

FF met Arjan overleggen of de errors anders kunnen
- Als een recipient/group niet bestaat dan gaat alles op zn bek, volgens mij willen we dan een status ERROR met een message terug krijgen.
- Wanneer je bij addGroup geen name property hebt gaat hij op zn bek, volgens mij willen we dan een status ERROR met een message terug krijgen.
- addRecipient geeft geen DUPLICATE  terug
- addRecipients geeft geen goede error melding wanneer er een error optreed
- addRecipients geeft geen DUPLICATE  terug
*/

if(!defined('FIRST_HARDBOUNCE_ID'))
	define('FIRST_HARDBOUNCE_ID', 10);

if(!defined('SECOND_HARDBOUNCE_ID'))
	define('SECOND_HARDBOUNCE_ID', 20);

if(!defined('HARDBOUNCE_ID'))
	define('HARDBOUNCE_ID', 30);

if(!defined('SOFTBOUNCE_ID'))
	define('SOFTBOUNCE_ID', 40);

if(!defined('UNSUBSCRIBERS_ID'))
	define('UNSUBSCRIBERS_ID', 50);

if(!defined('SPAMCOMPLAINT_ID'))
	define('SPAMCOMPLAINT_ID', 52);

if(!defined('OPTIN_ID'))
	define('OPTIN_ID', 60);

if(!defined('SUBSCRIBERS_ID'))
	define('SUBSCRIBERS_ID', 70);


class DMDSoap {
	/**
	 * SOAP
	 *
	 * @var SOAP
	 * @access private
	 */
	private $_soap;

	/**
	 * Login
	 *
	 * @var Array
	 * @access private
	 */
	private $_login;

	public function __construct($wsdl, $username, $password) {
		$this->_soap = new SoapClient($wsdl, array('encoding'=>'iso-8859-1'));
		$this->_login = array(
			'username'	=> $username,
			'password'	=> $password
		);

		return TRUE;
	}

	/**
	 * Add a new group to this DMdelivery campaign.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param group: The characteristics of the group to add
	 * @return ErrorVoedingonline or GroupId Object
	 */
	public function addGroup($group) {
		try {
			$result = @$this->_soap->addGroup($this->_login, $group);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}

		switch($result->status) {
			case 'OK' :
				return $result->id;
				break;
			case 'DUPLICATE' :
				return new ErrorVoedingonline(E_USER_WARNING, $result->statusMsg);
				break;
			case 'ERROR' :
				return new ErrorVoedingonline(E_USER_ERROR, $result->statusMsg);
				break;
		}
	}

	/**
	 * Add a new recipient to the DMdelivery campaign.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param groups: An array of groups (database ids) to make the recipients a member of
	 * @param recipientData: An associative array (key: name of field, value: value of field) containing recipient data
	 * @param addDuplisToGroup: Whether or not to add this recipient to the groups, when the recipient is in the database already
	 * @param overwrite: In case the recipient already exists, whether or not to overwrite the known recipient data with the new data provided
	 * @return ErrorVoedingonline or recipienID Object
	 */
	public function addRecipient($groups, $recipientData, $addDuplisToGroup, $overwrite) {
		try {
			$result = @$this->_soap->addRecipient(
				$this->_login,
				$groups,
				$recipientData,
				$addDuplisToGroup,
				$overwrite
			);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}

		switch($result->status) {
			case 'OK' :
				return $result->id;
				break;
			case 'DUPLICATE' : // DUPLICATE krijgen we nooit terug.
				return new ErrorVoedingonline(E_USER_WARNING, $result->statusMsg);
				break;
			case 'ERROR' :
				return new ErrorVoedingonline(E_USER_ERROR, $result->statusMsg);
				break;
		}
	}

	/**
	 * Add multiple new recipients to DMdelivery (max 500 at once).
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param groups: An array of groups (database ids) to make the recipients a member of
	 * @param recipientDatas: An array of associative arrays (key: name of field, value: value of field) containing recipient data
	 * @param addDuplisToGroup: Whether or not to add this recipient to the groups, when the recipient is in the database already
	 * @param overwrite: In case a recipient already exists, whether or not to overwrite the known recipient data with the new data provided
	 * @return ErrorVoedingonline or Bool Object
	 */
	public function addRecipients($groups, $recipientDatas, $addDuplisToGroup, $overwrite) {
		try {
			$result = @$this->_soap->addRecipients(
				$this->_login,
				$groups,
				$recipientDatas,
				$addDuplisToGroup,
				$overwrite
			);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}

		switch($result->status) {
			case 'OK' :
				return TRUE;
				break;
			case 'DUPLICATE' : // DUPLICATE krijgen we nooit terug.
				return new ErrorVoedingonline(E_USER_WARNING, $result->statusMsg);
				break;
			case 'ERROR' :
				return new ErrorVoedingonline(E_USER_ERROR, $result->statusMsg);
				break;
		}
	}

	/**
	 * Import recipients (max 500 at once), while sending a (definitive) mailing to them.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param mailing: The database id of the mailing to send
	 * @param groups: An array of groups (database ids) to make the recipients a member of
	 * @param recipientDatas: An array of associative arrays (key: name of field, value: value of field) containing recipient data
	 * @param addDuplisToGroup: Whether or not to add this recipient to the groups, when the recipient is in the database already
	 * @param overwrite: In case a recipient already exists, whether or not to overwrite the known recipient data with the new data provided
	 * @return ??
	public function addRecipientsSendMailing($mailing, $groups, $recipientDatas, $addDuplisToGroup, $overwrite) {} */

	/**
	 * Make a recipient a member of one or more groups.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param id: The database id of the recipient
	 * @param groups: An array of groups (database ids) to make the recipients a member of
	 * @returns: An array with all (database ids of) groups the recipient is now a member of
	 */
	public function addRecipientToGroups($recipientID, $groups) {
		try {
			return @$this->_soap->addRecipientToGroups(
				$this->_login,
				$recipientID,
				$groups
			);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}
	}

	/**
	 * Edit the data of an existing recipient.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param id: The database id of the recipient to edit
	 * @param recipientData: An associative array (key: name of field, value: value of field) containing updated recipient data
	 * @return ErrorVoedingonline or recipienID Object
	 */
	public function editRecipient($recipientID, $recipientData) {
		try {
			$result = @$this->_soap->editRecipient(
				$this->_login,
				$recipientID,
				$recipientData
			);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}

		switch($result->status) {
			case 'OK' :
				return $result->id;
				break;
			case 'DUPLICATE' :
				return new ErrorVoedingonline(E_USER_WARNING, $result->statusMsg);
				break;
			case 'ERROR' :
				return new ErrorVoedingonline(E_USER_ERROR, $result->statusMsg);
				break;
		}
	}

	/**
	 * Retrieve all groups from the DMdelivery campaign.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @return ErrorVoedingonline or Array
	 */
	public function getGroups() {
		try {
			return @$this->_soap->getGroups($this->_login);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}
	}

	/**
	 * Retrieve all mailings from the DMdelivery campaign. Mailings are returned from new to old (newest on top).
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param limit: indicates the number of mailings to retrieve. Use 0 to retrieve *all* mailings
	 * @param definitiveOnly: Whether or not to only return definitively sent mailings
	 * @returns: All mailings within the campaign
	 *
	public function getMailings($limit, $definitiveOnly) {} */


	/**
	 * Retrieve recipient fields for the DMdelivery campaign.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @return ErrorVoedingonline or Array
	 */
	public function getRecipientFields() {
		try {
			return @$this->_soap->getRecipientFields($this->_login);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}
	}

	/**
	 * Retreive the groups a recipient is member of.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param id: The database id of the recipient
	 * @returns: An array with all (database ids of) groups the recipient is now a member of
	 */
	public function getRecipientGroups($recipientID) {
		try {
			return @$this->_soap->getRecipientGroups(
				$this->_login,
				$recipientID
			);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}
	}

	/**
	 * Retrieve recipients from the DMdelivery campaign.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param fields: Array of fields to retrieve. Ex: ('email', 'firstname', 'lastname')
	 * @param in_groups: Array of (database ids of) groups the recipients should be member of
	 * @param not_in_groups: Array of (database ids of) groups the recipients should *not* be member of
	 * @param mailing: Array of (database ids of) mailings the recipients must have been sent
	 * @param filter: The database id of the filter to use for matching recipients. Use 0 for no filter
	public function getRecipients($fields, $in_groups, $not_in_groups, $mailing, $filter) {}*/

	/**
	 * Retrieve recipients that match certain criteria.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param recipientMatchData: An associative array of criteria to match recipients by. Ex: ('email'=>'user@example.com', 'firstname'=>'John')
	public function getRecipientsByMatch($recipientMatchData) {} */

	/**
	 * Retrieve recipients from a specific DMdelivery group.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param fields: Array of fields to retrieve. Ex: ('email', 'firstname', 'lastname')
	 * @param in_group: Database id of group the recipients should be member of
	 * @param from_date: Date since when recipient became a member of 'in_group'. Leave empty if it doesn't matter, otherwise use 'YYYY-MM-DD' format.
	 * @param mailings: Array of (database ids of) mailings the recipients must have been sent
	 * @param filter: The database id of the filter to use for matching recipients. Use 0 for no filter
	 *
	 public function getRecipientsFromGroup($fields, $in_groups, $from_date, $mailings, $filter) {} */

	/**
	 * Move all recipients from one group to another group.
	 *
	 * @param from_group: The database id of the source group
	 * @param to_group: The database id of the target group
	 */
	public function moveRecipientsToGroup($from_group, $to_group) {
		try {
			return @$this->_soap->moveRecipientsToGroup($this->_login, $from_group, $to_group);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}
	}

	/**
	 * Remove a recipient from one or more groups.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param id: The database id of the recipient
	 * @param groups: The (database id of) the groups to remove the recipient from
	 * @returns: An array with all (database ids of) groups the recipient is now a member of
	 */
	public function removeRecipientFromGroups($recipientID, $groups) {
		try {
			return @$this->_soap->removeRecipientFromGroups(
				$this->_login,
				$recipientID,
				$groups
			);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}
	}

	/**
	 * Send a mailing.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param mailing: The database id of the mailing to send
	 * @param isTest: Use true for test batch, false for final batch
	 * @param resultsEmail: The email address of the person who should receive logfile
	 * @param groups: Array of (database ids of) groups to send the mailing to
	 * @param filter: The database id of the filter to use for matching recipients. Use 0 for no filter
	 * @param lang: An array of languages to send the mailing to. Ex: ('en', 'nl')
	 * @return recipienID or Error Object
	public function sendMailing() {$mailing} */

	/**
	 * Send a mailing to a single recipient.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 *	@param mailing: The database id of the mailing to send
	 *	@param recipient: The database id of the recipient to send to
	 */
	public function sendSingleMailing($mailing, $recipientID) {
		try {
			$result = @$this->_soap->sendSingleMailing(
				$this->_login,
				$mailing,
				$recipientID
			);
		}
		catch (SoapFault $exception) {
			return new ErrorVoedingonline(E_USER_ERROR, $exception->getMessage());
		}
		if(!$result) {
			return new ErrorVoedingonline(E_USER_WARNING, sprintf('Could not send mailing(%s) to recipient(%s)', $mailing, $recipientID));
		}
	}

	/**
	 * Create a mailing from a URL.
	 *
	 * @param mailingName: The name to store the mailing as (must be unique!)
	 * @param lang: The ISO-lang code for the mailing. Ex: 'nl', 'en', etc.
	 * @param subject: The default subject of the mailing. Can be overwritten in the HTML to fetch
	 * @param fromName: The from name
	 * @param forward: The database id of the forward address
	 * @param url: The url where the mailing HTML can be fetched from
	 * @returns: The database id of the mailing that was created
	public function slurpMailing($mailingName, $lang, subject, $fromName, $forward, $url) {} */


/*****************************************************************************
 * BELOW THIS LINE SOME HANDY FUNCTIONS
 *****************************************************************************/


	/**
	 * Add a new recipient to the DMdelivery campaign for subscribe.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param recipientData: An associative array (key: name of field, value: value of field) containing recipient data
	 * @return recipienID or Error Object
	 */
	public function subscribe($recipientData) {
		$recipientID = $this->addRecipient(array(SUBSCRIBERS_ID), $recipientData, TRUE, FALSE);
		if(is_int($recipientID)) {
			$this->removeRecipientFromGroups(
				$recipientID,
				array(
					FIRST_HARDBOUNCE_ID,
					SECOND_HARDBOUNCE_ID,
					HARDBOUNCE_ID,
					SOFTBOUNCE_ID,
					OPTIN_ID,
					UNSUBSCRIBERS_ID
				)
			);
		}
		return $recipientID;
	}

	/**
	 * Add a new recipient to the DMdelivery campaign for optin.
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param recipientData: An associative array (key: name of field, value: value of field) containing recipient data
	 * @return recipienID or Error Object
	 */
	public function optin($recipientData, $mailing) {
		$recipientID = $this->addRecipient(array(OPTIN_ID), $recipientData, TRUE, FALSE);
		if(is_int($recipientID)) {
			$groups = $this->getRecipientGroups($recipientID);

			if(count($groups) > 0 && !in_array(SPAMCOMPLAINT_ID, $groups)) {
				$res = $this->sendSingleMailing($mailing, $recipientID);
				if(is_a($res, 'Error')) {
					return $res;
				}
			}
		}
		return $recipientID;
	}

	/**
	 * Mark a recipient as unsubscribed
	 *
	 * @author Emiel Roelofsen <emiel.roelofsen@webpower.nl>
	 * @access public
	 * @param id: The database id of the recipient
	 * @return ??
	 */
	public function unsubscribe($recipientID) {
		return $this->addRecipientToGroups($recipientID, array(UNSUBSCRIBERS_ID));
	}

}

?>