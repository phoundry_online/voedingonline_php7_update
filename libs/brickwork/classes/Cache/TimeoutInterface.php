<?php
interface Cache_TimeoutInterface
{
	/**
	 * Get the modified time of the given key
	 *
	 * @param string $key
	 * @return int timestamp returns FALSE on error
	 */
	public function modifiedTime($key);
	
	/**
	 * Try to make the lifetime of an (invalid) cache valid for some more seconds
	 * 
	 * @param string $key key of the cache entry
	 * @param int $secs amount of seconds the cache should be valid
	 */
	public function extendLifetime($key, $secs);
}