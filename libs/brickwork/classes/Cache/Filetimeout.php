<?php
/**
 * Caching using cache files with Timeouts for the values
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 0.0.1
 */
class Cache_Filetimeout extends Cache_File
{
	/**
	 * Set a value in the cache
	 *
	 * @param string $name
	 * @param mixedvar $value
	 * @param int $timeout Timeout in seconds
	 * @return bool
	 */
	public function setValue($name, $value, $timeout = null)
	{
		if($this->getValue($name) !== $value) {
			$this->_altered = true;			
		}
		
		if(!is_null($timeout)) {
			$this->_cacheData[$name] = array($value);
		} else {
			$this->_cacheData[$name] = array($value, time(), $timeout);
		}
		
		return true;		
	}
	
	/**
	 * Get a value from the cache
	 *
	 * @param string $name
	 * @return mixedvar Returns NULL if no cache is found
	 */
	public function getValue($name)
	{
		if($this->keyIsset($name))
		{
			return $this->_cacheData[$name][0];
		}
		return null;
	}
	
	/**
	 * Check if a key isset
	 *
	 * @param string $key
	 * @return bool
	 */
	public function keyIsset($key)
	{
		if(
			!isset($this->_cacheData[$name]) ||
			(
				count($this->_cacheData[$name]) == 3 &&
				(time() - $this->_cacheData[$name][2]) > $this->_cacheData[$name][1]
			)
		) {
			return false;
		}
		return true;
	}
}
