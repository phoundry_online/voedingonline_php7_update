<?php
/**
 * Caching using cache table and DB class
 * 
 * Simply create a table with a key column, a value column and a mod_date(time) column.
 * You can access it though the DB class with a connection string, table name and the column parameters as options
 * 
 * @example Cache::factory('db://1/table_name?timeout=3600&key=ua_hash&value=serialized_data&datetime=mod_datetime');
 * @author J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @version 1.0.0
 * @last_modified 2008-09-12 CB Bugfixes en stukken leesbaarder
 */
class Cache_Db extends Cache
{
	/**
	 * variable holding the cache data
	 *
	 * @var array
	 */
	private $cacheData;
	
	/**
	 * variable holding the modified time data
	 *
	 * @var array
	 */
	private $modifiedData;
	
	/**
	 * Connection
	 *
	 * @var DB_Adapter
	 */
	private $db;
	
	/**
	 * Schema.Table
	 *
	 * @var string
	 */
	private $table;
	
	/**
	 * @var string the Key prefix used in the table
	 */
	private $namespace;

	/**
	 * key column
	 *
	 * @var string
	 */
	private $keyColumn;
	
	/**
	 * value column
	 *
	 * @var string
	 */
	private $valueColumn;
		
	/**
	 * Datetime column
	 *
	 * @var string
	 */
	private $datetimeColumn;
	
	/**
	 * Prefix column
	 * 
	 * @var string
	 */
	private $namespaceColumn;

	/**
	 * Lifetime of the cacherecord in seconds
	 * 0 seconds is infinite
	 *
	 * @var int
	 */
	private $timeout;
	
	/**
	 * Magic Construct
	 *
	 * @param string $identifier
	 * @param array $options
	 */
	public function __construct($meta)
	{
		if(!empty($meta['query'])) {
			parse_str($meta['query'], $options);
		}
		
		$this->cacheData = array();
		$this->modifiedData = array();
		
		$this->db = DB::getDb(isset($meta['host']) ? $meta['host'] : 1);
		
		// get table from path
		if (isset($meta['path'])) {
			$path = explode('/', trim($meta['path'],'/'), 2);
		} else {
			$path = array();
		}
		
		$this->table = !empty($path[0]) ? $path[0] : 'brickwork_db_cache';
		$this->namespace = $this->md5((!empty($path[1]) ? $path[1] : 'default'));
				
		$this->timeout = isset($options['timeout']) && is_numeric($options['timeout']) ? (int) $options['timeout'] : 0; 
		$this->keyColumn = isset($options['key']) && preg_match('/^[a-z0-9][a-z0-9_]*[a-z0-9]$/i', $options['key'])>0 ? $options['key'] : 'key';
		$this->valueColumn = isset($options['value']) && preg_match('/^[a-z0-9][a-z0-9_]*[a-z0-9]$/i', $options['value'])>0 ? $options['value'] : 'value';
		$this->datetimeColumn = isset($options['datetime']) && preg_match('/^[a-z0-9][a-z0-9_]*[a-z0-9]$/i', $options['datetime'])>0 ? $options['datetime'] : 'mod_datetime';
		$this->namespaceColumn = isset($options['namespace']) && preg_match('/^[a-z0-9][a-z0-9_]*[a-z0-9]$/i', $options['namespace'])>0 ? $options['namespace'] : 'namespace';
		
		if(!$this->timeout) {
			$this->timeout = 3600;
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Cache#setValue($key, $value)
	 */
	public function setValue($key, $value)
	{
		$this->_query(sprintf('
			INSERT INTO
				`%1$s` (`%2$s`,`%3$s`,`%4$s`,`%5$s`)
			VALUES
				(%6$s, %7$s, %8$s, NOW())
			ON DUPLICATE KEY UPDATE
				`%4$s` = VALUES(`%4$s`),
				`%5$s` = VALUES(`%5$s`)
			',
			// schema.table
			$this->db->quote($this->table),
			// columns
			$this->db->quote($this->namespaceColumn),
			$this->db->quote($this->keyColumn),
			$this->db->quote($this->valueColumn),
			$this->db->quote($this->datetimeColumn),
			// values
			$this->db->quoteValue($this->namespace),
			$this->db->quoteValue($this->md5($key)),
			$this->db->quoteValue(serialize($value))
		));

		$this->cacheData[$key] = $value;
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Cache#getValue($key)
	 */
	public function getValue($key)
	{
		return $this->keyIsset($key) ? $this->cacheData[$key] : null;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Cache#unsetKey($key)
	 */
	public function unsetKey($key)
	{
		if ($this->keyIsset($key)) {
			$this->_query(
				sprintf(
					'DELETE
					FROM
						`%s`
					WHERE
						`%s` = %s
					AND 
						`%s` = %s',
					$this->db->quote($this->table),
					$this->db->quote($this->namespaceColumn),
					$this->db->quoteValue($this->namespace),
					$this->db->quote($this->keyColumn),
					$this->db->quoteValue($this->md5($key))
				)
			);
			unset($this->cacheData[$key]);
			return true;
		}

		return false;	
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Cache#keyIsset($key)
	 */
	public function keyIsset($key)
	{
		if (array_key_exists($key, $this->cacheData)) {
			return true;
		}

		$res = $this->_query(sprintf('
			SELECT 
				`%s` AS "value",
				UNIX_TIMESTAMP(`%s`) AS "mtime"
			FROM
				`%s`
			WHERE
				`%s` = %s
			AND
				`%s` = %s
			AND
				`%s`>= "%s"
			',
			$this->db->quote($this->valueColumn),
			$this->db->quote($this->datetimeColumn),
			$this->db->quote($this->table),
			$this->db->quote($this->namespaceColumn),
			$this->db->quoteValue($this->namespace),
			$this->db->quote($this->keyColumn),
			$this->db->quoteValue($this->md5($key)),
			$this->db->quote($this->datetimeColumn),
			date('Y-m-d H:i:s', (time()-$this->timeout))
		));
		
		if($row = $res->first()) {
			$this->cacheData[$key] = unserialize($row->value);
			$this->modifiedData[$key] = $row->mtime;
			return true;
		}
		return false;		
	}

	/**
	 * Get the modified time of the given key
	 *
	 * @param string $key
	 * @return int timestamp returns FALSE on error
	 */
	public function modifiedtime($key)
	{
		return $this->keyIsset($key) && isset($this->modifiedData[$key]) ?
			$this->modifiedData[$key] : false;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Cache#flush()
	 */
	public function flush()
	{
		$this->_query(
			sprintf(
				'DELETE LOW_PRIORITY IGNORE
				FROM
					`%s`
				WHERE
					`%s` = %s',
				$this->db->quote($this->table),
				$this->db->quote($this->namespaceColumn),
				$this->db->quoteValue($this->namespace)
			)
		);

		$this->cacheData = array();
		return true;
	}
	
	public function __destruct()
	{
		if(rand(1,100) == 50) {
			$this->flush();
		}		
	}
	
	/**
	 * Creates a md5 for a key but only when it is necessary
	 * 
	 * @param string $key
	 * @return string Never longer then 32 chars
	 */
	private function md5($key)
	{
		return strlen($key) > 32 ? md5($key) : $key;
	}
	
	/**
	 * Execute a query, on failure because the table misses it will be
	 * automatically created
	 * 
	 * @param string $sql
	 * @return DB_ResultSet
	 */
	private function _query($sql)
	{
		try {
			$res = $this->db->iQuery($sql);
		}
		catch(DB_Exception $e) {
			switch ($e->getCode()) {
				case DB_Exception::UNKNOWN_COLUMN:
					if ($this->table != 'brickwork_db_cache') {
						throw $e;
					}
					$this->db->iQuery(
						'DROP TABLE brickwork_db_cache;'
					);
				case DB_Exception::BAD_TABLE:
					$this->db->iQuery(sprintf('
						CREATE TABLE `%1$s` (
						  `%2$s` CHAR(32) NOT NULL,
						  `%3$s` CHAR(32) NOT NULL,
						  `%4$s` DATETIME NOT NULL,
						  `%5$s` BLOB NOT NULL,
						  PRIMARY KEY  (`%2$s`, `%3$s`)
						) DEFAULT CHARSET=latin1
						',
						$this->db->quote($this->table),
						$this->db->quote($this->namespaceColumn),
						$this->db->quote($this->keyColumn),
						$this->db->quote($this->datetimeColumn),
						$this->db->quote($this->valueColumn)
					));
					break;
				default:
					throw $e;
			}
			
			// Try again after creating the table
			$res = $this->db->iQuery($sql);
		}
		
		return $res;
	}
}
