<?php

/**
 * Caching using cache files in a specified directory
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 0.0.1
 */
class Cache_Dir extends Cache implements Cache_TimeoutInterface
{
	/**
	 * Internal cache to prevent continuous loading of files
	 * @var Array
	 */
	protected $_keyCache;

	/**
	 * Directory holding the files
	 *
	 * @var string
	 */
	protected $_cacheDir;
	
	/**
	 * Timeout for the cachefiles
	 *
	 * @var int
	 */
	protected $_timeout;
	
	/**
	 * provides direct access to the files contents
	 * @decrepated
	 * @var bool
	 */
	protected $_raw;
	
	
	/**
	 * How to export to the file
	 * 
	 * @var string
	 */
	protected $_export;

	/**
	 * Magic Construct
	 *
	 * @param string $identifier
	 * @param array $options
	 */
	public function __construct($meta)
	{
		if(isset($meta['query'])) {
			parse_str($meta['query'], $options);
		}
		
		$this->_cacheDir = rtrim(CACHE_DIR.'/'.
			(!empty($meta['host']) ? $meta['host'].'/' : '').
			Utility::arrayValue($meta, 'path'), '/');
			
		$this->_timeout = isset($options['timeout']) &&
			is_numeric($options['timeout']) ? (int) $options['timeout'] : 0;
		
		// Backward compat
		$this->_raw = isset($options['raw']) && $options['raw'] != 'false';
		
		$this->_export = isset($options['export']) ? $options['export'] : 'serialize';
		
		$this->_checkDirs( $this->_cacheDir );

		$this->sweep();	

		$this->_keyCache = Array();
	}
	
	/**
	 * Set a value in the cache
	 *
	 * @param string $name
	 * @param mixedvar $value
	 * @param int $timeout Timeout in seconds
	 * @return bool
	 */
	public function setValue($key, $value)
	{
		$filename = $this->_file($key);
		
		// Check if the key contains a directory seperator
		if(false !== strpos($key, '/') &&
		  !$this->_checkDirs(dirname($filename))) {
			throw new Exception('Directory could not be created for CacheKey: '.$key);	
		}
		
		// Keep unserialized data in _keyCache to prevent errors.
		$this->_keyCache[$key] = $value;
		
		$tmpnam = tempnam(dirname($filename), 'dircache');

		if(false === $tmpnam) {
			return false;
		}

		if($this->_export == 'var_export') {
			$file_put = (bool) file_put_contents($tmpnam, var_export($value, true));
		}
		else if($this->_export == 'raw' || $this->_raw) {
			$file_put = (bool) file_put_contents($tmpnam, $value);
		}
		else if($this->_export == 'json') {
			$file_put = (bool) file_put_contents($tmpnam, json_encode($value));
		}
		else {
			$file_put = (bool) file_put_contents($tmpnam, serialize($value));
		}

		if($file_put) {
			chmod($tmpnam, 0666);
			rename($tmpnam, $filename);
			return true;
		}

		return false;
	}
	
	/**
	 * Get a value from the cache
	 *
	 * @param string $key
	 * @return mixedvar Returns NULL if no cache is found
	 */
	public function getValue($key)
	{
		if($this->keyIsset($key)) {
			if(!isset($this->_keyCache[$key])) {
				$filename = $this->_file($key);
				
				if($this->_export == 'var_export') {
					$value = @eval('return('.file_get_contents($filename).');');
				}
				else if($this->_export == 'raw' || $this->_raw) {
					$value = file_get_contents($filename);
				}
				else if($this->_export == 'json') {
					$value = json_decode(file_get_contents($filename), true);
				}
				else {
					$value = unserialize(file_get_contents($filename));
				}
				
				$this->_keyCache[$key] = $value;
			}
			return $this->_keyCache[$key];
		}
		return null;
	}
	
	/**
	 * Unset a given key
	 *
	 * @param string $key
	 * @return bool
	 */
	public function unsetKey($key)
	{
		if(isset($this->_keyCache[$key])) {
			unset($this->_keyCache[$key]);
		}

		return $this->keyIsset($key) ?
			unlink($this->_cacheDir.'/'.$key.'.cache') : false;
	}
	
	/**
	 * Check if a key isset
	 *
	 * @param string $key
	 * @return bool
	 */
	public function keyIsset($key)
	{
		if(isset($this->_keyCache[$key])) {
			return true;
		}
		
		$file = $this->_file($key);

		
		if (is_file($file)) {
			if ($this->_timeout == 0) {
				return true;
			}
			$filemtime = @filemtime($file);
			if ($filemtime !== false && $filemtime > (time() - $this->_timeout)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get the modified time of the given key
	 *
	 * @param string $key
	 * @return int timestamp returns FALSE on error
	 */
	public function modifiedTime($key)
	{
		return $this->keyIsset($key) ?
			@filemtime($this->_file($key)) : false;
	}

	/**
	 * Try to make the lifetime of an (invalid) cache valid for some more seconds
	 * 
	 * @param string $key key of the cache entry
	 * @param int $secs amount of seconds the cache should be valid
	 */
	public function extendLifetime($key, $secs)
	{
		$file = $this->_file($key);
		if ($this->_timeout !== 0 && file_exists($file)) {
			touch($file, (time() - $this->_timeout) + $secs);
		}
	}
	
	/**
	 * Flush the cache
	 *
	 * @return bool
	 */
	public function flush($dir = false)
	{
		if(!$dir) {
			$dir = $this->_cacheDir;
		}
		
		foreach(new DirectoryIterator($dir) as $file) {
			if($file->isDir() && $file != '..' && $file != '.') {
				$this->flush($file->getPathname());
				rmdir($file->getPathname());
			}
			
			if($file->isFile()) {
				unlink($file->getPathname());
			}
		}

		$this->_keyCache = array();
		return true;
	}
	
	/**
	 * Sweep the cache dir clean
	 *
	 * @return bool
	 */
	public function sweep()
	{
		if($this->_timeout && mt_rand(0,10) == 10 && 
		  (!file_exists($this->_cacheDir.'/sweep') ||
		  filemtime($this->_cacheDir.'/sweep') < time()-86400)) {
			Utility::sweepDir($this->_cacheDir, $this->_timeout, true);
			touch($this->_cacheDir.'/sweep');
		}

		$this->_keyCache = array();
		return true;
	}

	/**
	 * Return the filename for the given key
	 * @param string $key
	 * @return string
	 */
	protected function _file($key)
	{
		return "{$this->_cacheDir}/{$key}.cache";
	}
	
	/**
	 * Recursively create all directories that are not there
	 *
	 * @param 	string $path
	 * @return boolean
	 */
	protected function _checkDirs($path)
	{
		$path = rtrim($path, '/');
		
		if($path == '') {
			return true;
		}
			
		if(file_exists($path) && is_dir($path)) {
			return true;
		}
			
		if($this->_checkDirs(dirname($path))) {
			@mkdir( $path );
			@chmod( $path, 0777 );
			return file_exists($path);
		}
		return false;
	}
}
