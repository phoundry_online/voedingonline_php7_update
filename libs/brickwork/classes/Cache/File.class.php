<?php
/**
 * Caching using cache files
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 0.0.2
 */
class Cache_File extends Cache
{
	/**
	 * variable holding the cache data
	 *
	 * @var array
	 */
	protected $_cacheData;
	
	/**
	 * Filename
	 *
	 * @var string
	 */
	protected $_cacheFile;
	
	/**
	 * Did the cache change?
	 *
	 * @var bool
	 */
	protected $_altered;
	
	/**
	 * Lifetime of the cachefile in seconds
	 * 0 seconds is infinite
	 *
	 * @var int
	 */
	protected $_timeout;
	
	/**
	 * How to export to the file
	 * 
	 * @var string
	 */
	protected $_export;
	
	/**
	 * Timestamp when the cache last changed
	 *
	 * @var int
	 */
	protected $_modifiedtime;
	
	/**
	 * Magic Construct
	 *
	 * @param string $identifier
	 * @param array $options
	 */
	public function __construct($meta)
	{
		if(isset($meta['query'])) {
			parse_str($meta['query'], $options);
		}
		
		$this->_altered = false;
		
		$this->_cacheFile = CACHE_DIR.'/'.$meta['path'].'.cache';
		
		$this->_export = isset($options['export']) ? $options['export'] : 'serialize';
		$this->_timeout = isset($options['timeout']) && is_numeric($options['timeout']) ? (int) $options['timeout'] : 0; 
		
		$this->_checkDirs( dirname($this->_cacheFile));
		$this->_loadCache();
	}
	
	/**
	 * Magic Destruct
	 */
	public function __destruct()
	{
		if ( $this->_altered ) {
			$this->_saveCache();
		}
	}

	/**
	 * Set a value in the cache
	 *
	 * @param string $key
	 * @param mixedvar $value
	 * @return bool
	 */
	public function setValue($key, $value)
	{
		if($this->getValue($key) !== $value)
		{
			$this->_altered = true;
			$this->_modifiedtime = time();
		}
		
		$this->_cacheData[$key] = $value;
		
		return true;		
	}
	
	/**
	 * Get a value from the cache
	 *
	 * @param string $key
	 * @return mixedvar Returns NULL if no cache is found
	 */
	public function getValue($key)
	{
		return isset($this->_cacheData[$key]) ? $this->_cacheData[$key] : null;
	}
	
	public function unsetKey($key)
	{
		if($this->keyIsset($key))
		{
			unset($this->_cacheData[$key]);
			return true;
		}
		return false;
	}
	
	/**
	 * Check if a key isset
	 *
	 * @param string $key
	 * @return bool
	 */
	public function keyIsset($key)
	{
		return array_key_exists($key, $this->_cacheData);
	}

	/**
	 * Flush the cache
	 *
	 * @return bool
	 */
	public function flush()
	{
		$this->_cacheData = array();
		$this->_altered = true;
		$this->_modifiedtime = time();
		return true;
	}
	
	/**
	 * Get last modified time of cache
	 * 
	 * @deprecated conflicts with the Cache_TimeoutInterface
	 * @return int timestamp
	 */
	public function modifiedtime()
	{
		return !empty($this->_modifiedtime) ? $this->_modifiedtime : false;
	}
	
	/**
	 * Load the cache array from the file
	 *
	 * @return bool
	 */
	protected function _loadCache()
	{
		if ( file_exists( $this->_cacheFile ) )
		{
			
			$this->_modifiedtime = @filemtime( $this->_cacheFile );
			
			if ($this->_modifiedtime !== false) {
				if ($this->_timeout === 0 || $this->_modifiedtime > time() - $this->_timeout)
				{
					if($this->_export == 'var_export')
					{
						$this->_cacheData = @eval('return('.file_get_contents($this->_cacheFile).');');
					}
					else if($this->_export == 'json')
					{
						$this->_cacheData = json_decode(file_get_contents($this->_cacheFile), true);
					}
					else
					{
						$this->_cacheData = unserialize(file_get_contents($this->_cacheFile));
					}
				}
				else
				{
					// Make the cache valid for all other requests so that when
					// we're regenerating the cache the others use the old cache
					if (false === @touch($this->_cacheFile, (time() - $this->_timeout) + 20)) {
						// Touching without specifying the time doesn't require
						// owner rights on the file
						@touch($this->_cacheFile);
					}
				}
			}
			
		}
		if (!is_array($this->_cacheData))
		{
			$this->flush();
		}
		
		return true;
	}
	
	/**
	 * Save the cache array serialized to file
	 *
	 */
	protected function _saveCache()
	{
		$temp = tempnam(dirname($this->_cacheFile), basename($this->_cacheFile));
		
		if($this->_export == 'var_export') {
			$file_put = (bool) file_put_contents($temp, var_export($this->_cacheData, true));
		}
		else if($this->_export == 'json') {
			$file_put = (bool) file_put_contents($temp, json_encode($this->_cacheData) );
		}
		else {
			$file_put = (bool) file_put_contents($temp, serialize($this->_cacheData) );
		}
		
		if($file_put) {
			chmod($temp, 0666);
			if (file_exists($this->_cacheFile)) {
				@unlink($this->_cacheFile);
				rename($temp, $this->_cacheFile);
			}
			$this->_modifiedtime = time();
			$this->_altered = false;
			return true;
		}
		return false;
	}
	
	/**
	 * Recursively create all directories that are not there
	 *
	 * @param 	string $path
	 * @return boolean
	 */
	protected function _checkDirs( $path )
	{
		$path = rtrim($path,"/");
		
		if ( $path == '' )
			return true;
			
		if ( file_exists($path) && is_dir($path) )
			return true;
			
		if ( $this->_checkDirs( dirname($path) ) )
		{
			@mkdir( $path );
			@chmod( $path, 0777 );
			return file_exists($path);
		}
		return false;
	}
		
}
