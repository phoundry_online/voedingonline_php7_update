<?php
/**
 * Generic Pressure Valve system
 * This class is a simple way to offload excess load in a generic way. If you suffer
 * from websites that have occassional high load you cannot predict, you can use this
 * class to filter that load to a base you can handle.
 *
 * This class will check the load on your webserver and will set defines and call methods
 * you specify for a specific load. You can use that to alter the appearance of your
 * website of to temporarily turn off features.
 * Remember: the methods will be called on EVERY request if the load is matched, so do not
 * do complex things or to save certain settings.  Also: there is no way of knowing when the
 * load is going down again. So don't change something you can not undo automatically
 *
 * The class is a Singleton to prevent duplicate loading of the load data.
 *
 * @author 		Peter Eussen
 * @version		1.0
 * @package		Brickwork
 * @subpackage	Core
 */
class PressureValve
{
	/**
	 * Singleton instance
	 * @var PressureValve
	 */
	static protected	$_instance = null;

	/**
	 * A list of rules that should be applied to loads
	 * @var Array
	 */
	protected	$_rules;

	/**
	 * Indicator showing it found some client rules
	 * @var bool
	 */
	protected	$_enabled;

	/**
	 * The current loads over 1, 5 and 15 minutes
	 * @var Array
	 */
	protected	$_load;

	/**
	 * For normal usage use getInstance instead
	 * @param array $rules
	 * @param array $load default uses sys_getloadavg to fetch the load
	 */
	public function __construct($rules = array(), array $load = null)
	{
		$this->_rules 	= $rules;
		$this->_enabled	= isset($this->_rules['web']) || isset($this->_rules['cli']);
		
		if(!$this->_enabled) {
			$this->_load = array(0, 0, 0);
		}
		else {
			$this->_load = ($load !== null) ? $load : self::_getLoad();
		}

		$this->_setDefines();
		$this->_doCalls();
	}

	/**
	 * Returns a single instance
	 * This class requires a singleton to prevent loading the data more than once, which would
	 * be a waste of resources.
	 *
	 * @return PressureValve
	 */
	public static function getInstance()
	{
		if(is_null(self::$_instance)) {
			global $WEBprefs;

			// Explicitly not using ArrayValue to prevent extra function call in high loads
			self::$_instance = new self(
				isset($WEBprefs['pressurevalve']) ? $WEBprefs['pressurevalve'] : array()
			);
		}
		return self::$_instance;
	}
	
	/**
	 * Shows if there are any client rules active
	 *
	 * @return boolean
	 */
	public function isEnabled()
	{
		return $this->_enabled;
	}
	
	/**
	 * Returns the current load
	 * 
	 * @return array
	 */
	public function getLoad()
	{
		return $this->_load;
	}

	/**
	 * Checks if the client is allowed to run
	 * Checks if the load isnt too high for the client.
	 *
	 * @param 	bool	$isWeb Is the client a web client (default TRUE)
	 * @return 	bool
	 */
	public function allowed( $isWeb = true )
	{
		if(!$this->_enabled) {
			return true;
		}

		return $isWeb ? $this->_webAllowed() : $this->_cliAllowed();
	}

	/**
	 * Checks if a webclient is allowed or not
	 *
	 * Warning: this method requires the ability to set cookies. So it should be called
	 * before any output is sent!
	 *
	 * @return bool
	 */
	protected function _webAllowed()
	{
		if(!isset($this->_rules['web'])) {
			return true;
		}
		
		// I wanted to give it the same name as the session. Unfortunately in Brickwork the
		// name of the session is set at a later stage. But for now it will give us a nice
		// Session name, so i will keep it like this
		$cookie = session_name() . '_mngr';
		// I use a hash to make sure we are allowed, so people have a harder time forcing their way in.
		// This is not foolproof, but it makes people wonder perhaps.
		$hash = $this->_getClientHash('allowed');
		// Assume the same lifetime as the gc of sessions
		$next = time() + ini_get('session.gc_maxlifetime');

		$headers_send = headers_sent();
		if($headers_send) {
			error_log("Warning: headers already sent before pressure valve could set it's information");
		}

		if((isset($_COOKIE[$cookie]) && $_COOKIE[$cookie] == $hash) ||
			!$this->_thresholdExceeded($this->_rules['web'])) {
				
			if(!$headers_send) {
				setCookie($cookie, $hash, $next, '/');
			}
			return true;
		}
		
		if(!$headers_send) {
			setCookie($cookie, $this->_getClientHash( 'denied'), $next, '/');
		}
		return false;
	}

	/**
	 * Checks if a Command Line Client is allowed to run
	 *
	 * @return bool
	 */
	protected function _cliAllowed()
	{
		if(isset($this->_rules['cli'])) {
			return !$this->_thresholdExceeded($this->_rules['cli']);
		}
		return true;
	}

	/**
	 * Loads system load averages
	 *
	 * @return Array
	 */
	protected static function _getLoad()
	{
		$data = sys_getloadavg();
		
		if(count($data) == 3) {
			return $data;
		}
		
		return array(0, 0, 0);
	}

	/**
	 * Activates defines based on a specific load value
	 *
	 * @return bool
	 */
	protected function _setDefines()
	{
		if(isset($this->_rules['define'])) {
			foreach($this->_rules['define'] as $def => $val) {
				// If it is already defined, then we don't have to check anymore
				if(defined($def)) {
					continue;
				}
				define($def, $this->_thresholdExceeded($val));
			}
		}
		return true;
	}

	/**
	 * Performs system calls on a specific load
	 *
	 * @return bool
	 */
	protected function _doCalls()
	{
		if(isset($this->_rules['call'])) {
			foreach($this->_rules['call'] as $callRule) {
				if(!isset($callRule['method']) || !isset($callRule['load']) ||
					!is_callable($callRule['method'])) {
					throw new LogicException("Load call rules require a callable 'method' and a 'load' setting");
				}

				if($this->_thresholdExceeded($callRule['load'])) {
					$call = call_user_func($callRule['method'], $this->_load);

					if($call === false) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Creates a has for the current client.
	 * The hash is used by the allowed method to check if the user is in or out. Thats also why
	 * i do it with a hash, so people can not forge their own cookies and get in anyways.
	 *
	 * @param 	string	$key
	 * @return 	string
	 */
	protected function _getClientHash($key)
	{
		return md5($key.Utility::arrayValue($_SERVER, 'HTTP_USER_AGENT', 'localhost').serialize($this->_rules));
	}

	/**
	 * Checks if a load threshold has been exceeded.
	 *
	 * @param $thresholds		Array or float
	 * @return bool
	 */
	protected function _thresholdExceeded( $thresholds )
	{
		// Safest: use all 3 load values
		if (is_array($thresholds) && count($thresholds) == 3) {
			return ( $thresholds[0] < $this->_load[0] && $thresholds[1] < $this->_load[1] && $thresholds[2] < $this->_load[2] );
		}
		else if ( is_array($thresholds)) {
			// No 3 values found, so assume a user error, but use the first
			return ((float) array_shift($thresholds) < $this->_load[1]);
		}
		else {
			// Single value, so check against one of the load values. I use the 5 minute average to prevent flagging from
			// the system by taking actions too quickly.
			return ((float) $thresholds < $this->_load[1]);
		}
	}
}