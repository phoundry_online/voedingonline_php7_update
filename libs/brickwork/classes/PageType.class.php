<?php

/**
 * PageType
 *
 * @package	ContentHandler
 * @version	0.5
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @access	public
 */

class PageType {
	 /**
	* @var String identifier
	*/
	public $identifier;

	/**
	* Constructor
	*
	* @access	public
	* @param	String Templatefile to load
	*/
	public function __construct($identifier){
		$this->identifier = $identifier;
	}
}

?>