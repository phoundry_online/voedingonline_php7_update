<?php
class Utility
{
	/**
	 * Fetches a value from an array in a safe way. 
	 * This method will check if a specific key exists in the array, and it it does,
	 * it will return that element's value. If no value was found, it will return NULL.
	 * The arguments of this function can be switched (decrepated).
	 * 
	 * @param 	Array $array
	 * @param 	string $key
	 * @param	mixed $default The value that is returned if the array key doesnt exist
	 * @return 	mixed
	 */
	static public function arrayValue($array, $key, $default = null)
	{
		if(!is_array($array) && is_array($key)) {
			$tmp = $array;
			$array = $key;
			$key = $tmp;
		}

		if(!is_array($array)) {
			throw new InvalidArgumentException('Invalid arguments, no array found');
		}
			
		if(is_array($key)) {
			throw new InvalidArgumentException('Key should not be an array');
		}
			
		return array_key_exists($key, $array) ? $array[$key] : $default;
	}
	
	/**
	 * Fetches a value from an object in a safe way. 
	 * This method will check if a specific key exists in the array, and it it does,
	 * it will return that element's value. If no value was found, it will return $default.
	 * 
	 * @param 	Object $array
	 * @param 	string $key
	 * @param	mixed $default The value that is returned if the array key doesnt exist
	 * @return 	mixed
	 */
	static public function objectValue($object, $key, $default = null)
	{
		if(!is_object($object)) {
			throw new InvalidArgumentException('Invalid arguments, $object no object');
		}
		return property_exists($object, $key) ? $object->{$key} : $default;
	}
	
	/**
	 * Fetch a value from an array in a safe way recursively.
	 * First parameter is the array to search in, the parameters thereafter specify keys.
	 * 
	 * @param array $array
	 * @param array $key
	 * @param mixedvar $default
	 * @return mixedvar
	 */
	static public function arrayValueRecursive($array, $keys, $default = null)
	{
		if (!is_array($array) && !is_object($array)) {
			return $default;
		}
		
		// Allow parameter overloading
		if (!is_array($keys)) {
			$keys = func_get_args();
			$array = array_shift($keys);
			$default = null;
		}
		
		if ($keys) {
			$key = array_shift($keys);
			$object_access = strpos($key, '->') === 0 &&
				is_object($array) && property_exists($array, substr($key, 2));
			
			if ($object_access || array_key_exists($key, $array)) {
				$value = $object_access ?
					$array->{substr($key, 2)} : $array[$key];
				
				if (!$keys) {
					return $value;
				} else if (is_object($value) || is_array($value)) {
					return self::arrayValueRecursive($value, $keys, $default);
				}
			}
		}
		return $default;
	}
	
	/**
	 * Set a value in an multidimentional array.
	 * If the keys aren't set they are created
	 *
	 * @param array $array
	 * @param mixedvar $value
	 * @param array $keys
	 * @return array The modified array
	 */
	static public function setArrayValueRecursive($array, $value, $keys)
	{
		if (is_object($array)) {
			$array = (array) $array;
		} else if (!is_array($array)) {
			$array = array();
		}
		
		// Allow parameter overloading
		if (!is_array($keys)) {
			$keys = func_get_args();
			// First entry is the array shift it off the keys array
			array_shift($keys);
			// The 2nd entry is the value that should be assigned
			$value = array_shift($keys);
		}
		
		if (count($keys) > 0) {
			$key = array_shift($keys);
			
			if (count($keys) == 0) {
				$array[$key] = $value;
			} else {
				if (!isset($array[$key]) || !is_array($array[$key])) {
					$array[$key] = array();
				}
				$array[$key] = self::setArrayValueRecursive(
					$array[$key], $value, $keys
				);
			}
		}
		return $array;
	}
	
	/**
	 * Sort an array by the given key
	 * 
	 * @param array $array
	 * @param array|string $key
	 * @param string $sorting
	 * @param bool $casesensitive
	 * @return array
	 */
	static public function sortArrayRecursive($array, $key, $sorting = 'ASC', $casesensitive = false)
	{
		$sortarr = array();
		foreach ($array as $k => $element)
		{
			$sortarr[$k] = Utility::arrayValueRecursive($element, $key);
		}
		
		$casesensitive ? natsort($sortarr) : natcasesort($sortarr);
		if($sorting == 'DESC')
		{
			$sortarr = array_reverse($sortarr, true);
		}
		
		$newarr = array();
		foreach ($sortarr as $k => $v)
		{
			$newarr[$k] = $array[$k];
		}
	
		return $newarr;
	}
	
	/**
	 * Creates a flat (single dimensional) array with the value(s) of the input 
	 * 
	 * @param mixedvar $value
	 * @param int $maxdepth maximum depth
	 * @param int $deep Current depth
	 * @return array
	 */
	static public function arrayFlatten($value, $maxdepth = 2, $deep = 0)
	{
		$retArr = array();

		if(is_array($value)) {
			if($deep < $maxdepth) {
				foreach ($value as $v) {
					$retArr = array_merge($retArr, self::arrayFlatten($v, $maxdepth, $deep+1));
				}
			}
		}
		else if (is_object($value)) {
			if($deep < $maxdepth) {
				$retArr = array_merge($retArr, self::arrayFlatten(get_object_vars($value), $maxdepth, $deep+1));
			}
		}
		else {
			$retArr[] = $value;
		}
		return $retArr;
	}
	
	/**
	 * Replace the given key in the querystring with the given value
	 *
	 * @param string/array $key
	 * @param mixedvar $value
	 * @param string/array $query_string
	 * @return string
	 */
	static public function replaceGet($key, $value, $query_string = null, $arg_serperator = '&')
	{
		if(is_null($query_string)) {
			$query_string = $_GET;
		}
		else if(is_string($query_string)) {
			parse_str($query_string, $query_string);
		}
		
		$keys = is_array($key) ? $key : explode('[', str_replace(']','', $key));
		$query_string = self::setArrayValueRecursive($query_string, $value, $keys);
		return http_build_query($query_string, '', $arg_serperator);
	}
	
	/**
	 * Checks if a form has been posted
	 *
	 * @return boolean
	 */
	static public function isPosted()
	{
		return (self::arrayValue($_SERVER,'REQUEST_METHOD') == 'POST');
	}

	/**
	 * Tests whether a text ends with the given string(s) or not.
	 *
	 * @param   string $haystack Haystack
	 * @param   string|array $needle Needle(s)
	 * @param	bool $insensitive Match case insensitive (default true)
	 * @return  bool
	 */
	public static function endsWith($haystack, $needle, $insensitive = true)
	{
		$needle = (array) $needle;
		if($insensitive) {
			$haystack = strtolower($haystack);
			$needle = array_map('strtolower', $needle);
		}
		
		foreach($needle as $n) {
			if(false !== ($i = strrpos($haystack, $n)) &&
			  $i === strlen($haystack) - strlen($n)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Fetches a single row from a query
	 * This method will run a query and obtain the first row if any were
	 * found. If the query yielded no results, it will return NULL.
	 * In case the query contained an error, it will throw an exception
	 * with the error that occurred
	 * 
	 * @param string 	$sSql
	 * @param integer 	$iConn
	 * @return stdObject
	 */
	static public function querySingleResult( $sSql, $iConn = 1 )
	{
		return DB::querySingleResult($sSql, $iConn);
	}
	
	/**
	 * Here for backward compat
	 *
	 * @deprecated
	 * @param string $sString
	 * @return string
	 */
	static public function quote( $sString )
	{
		return DB::quote( $sString, true );
	}
	
	/**
	 * Crude way to strip all MSOffice related codes
	 *
	 * @param unknown_type $sString
	 * @return unknown
	 */
	static public function stripWordTags( $sString )
	{
		// Deze zijn "veilig" te verwijderen
		$officeAttr = Array('style="TEXT-DECORATION: underline"',
							'class=MsoNormal',
							'style="mso-bidi-font-weight: normal"',
						    );
		// Je hebt ook nog style="MARGIN: 0cm 0cm 0pt", maar dan gaat alles gelijk helemaal uit elkaar
		return str_replace($officeAttr,'',strip_tags( $sString, '<p><a><i><strong><b><img><h1><h2><h3><h4><br><ul><ol><li><table><tr><td>'));
	}
	
	/**
	 * Returns the first non-NULL parameter
	 *
	 * @param	mixed	$value1
	 * @param	mixed	$value2
	 * @param  ...
	 * @return mixed
	 */
	static public function coalesce() 
	{
		$args = func_get_args();

		foreach($args as $arg) {
			if (is_array($arg) && count($arg) != 0) { 
				return $arg;
			}

			if (is_object($arg)) {
				return $arg;
			}

			if (!is_null($arg)) {
				return $arg;
			}
		}

		return NULL;
	}

	static public function debug($msg)
	{
		if (TESTMODE && defined('ACL_DEBUG') && ACL_DEBUG) {
			return error_log(
				date('Y-m-d H:i:s', mktime()).": ".$msg."\n",
				3,
				TMP_DIR.'/'.Utility::arrayValue($_SERVER, 'REMOTE_ADDR').
				  '_debug.log'
			);
		}
		return true;
	}
	
	/**
	 * Truncate the given string at the specified length See smarty truncate
	 *
	 * @param string $string
	 * @param int $length
	 * @param string $etc
	 * @param bool $break_words
	 * @param bool $middle
	 * @return string
	 */
	static public function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false)
	{
		if ($length == 0) {
			return '';
		}

		if (strlen($string) > $length) {
			$length -= strlen($etc);
			if (!$break_words && !$middle) {
				$string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length+1));
			}
			
			if(!$middle) {
				return substr($string, 0, $length).$etc;
			}
			return substr($string, 0, $length/2) . $etc . substr($string, -$length/2);
		}
		return $string;
	}
	
	/**
	 * Urlencode an url
	 * @param string $url
	 * @return string
	 */
	static public function urlencodeUrl($url)
	{
		return implode('/', array_map('rawurlencode', explode('/', $url)));
	}
	
	/**
	 * Remove accents
	 * 
	 * Remove accents from a string
	 *
	 * @param string $string
	 * @return string
	 */
	static public function removeAccents($string)
	{
		$encoding = self::isUTF8($string) ? 'UTF-8' : 'ISO-8859-1';
		
		$string = htmlentities(
	        $string,
			ENT_COMPAT,
			$encoding
		);
		
		$replace = array(
			'&Agrave;' => 'A',
			'&Aacute;' => 'A',
			'&Acirc;' => 'A',
			'&Auml;' => 'A',
			'&Atilde;' => 'A',
			'&Aring;' => 'A',
			'&AElig;' => 'A',
		
			'&agrave;' => 'a',
			'&aacute;' => 'a',
			'&acirc;' => 'a',
			'&auml;' => 'a',
			'&aelig;' => 'a',
			'&atilde;' => 'a',
			'&aring;' => 'a',
		
			'&Egrave;' => 'E',
			'&Eacute;' => 'E',
			'&Ecirc;' => 'E',
			'&Euml;' => 'E',
		
			'&egrave;' => 'e',
			'&eacute;' => 'e',
			'&ecirc;' => 'e',
			'&euml;' => 'e',
		
			'&Igrave;' => 'I',
			'&Iacute;' => 'I',
			'&Icirc;' => 'I',
			'&Iuml;' => 'I',
		
			'&igrave;' => 'i',
			'&iacute;' => 'i',
			'&icirc;' => 'i',
			'&iuml;' => 'i',
		
			'&Ograve;' => 'O',
			'&Oacute;' => 'O',
			'&Ocirc;' => 'O',
			'&Ouml;' => 'O',
			'&Oslash;' => 'O',
			'&Otilde;' => 'O',
		
			'&ograve;' => 'o',
			'&oacute;' => 'o',
			'&ocirc;' => 'o',
			'&ouml;' => 'o',
			'&oslash;' => 'o',
			'&otilde;' => 'o',
			
			'&Ugrave;' => 'U',
			'&Uacute;' => 'U',
			'&Ucirc;' => 'U',
			'&Uuml;' => 'U',
			
			'&ugrave;' => 'u',
			'&uacute;' => 'u',
			'&ucirc;' => 'u',
			'&uuml;' => 'u',
		
			'&Ccedil;' => 'C',
		
			'&ccedil;' => 'c',
			'&cent;' => 'c',
		
			'&ETH;' => 'D',
			'&eth;' => 'd',
		
			'&Ntilde;' => 'N',
			'&ntilde;' => 'n',
		
			'&Yacute;' => 'Y',
			'&Yuml;' => 'Y',
		
			'&yacute;' => 'y',
			'&yuml;' => 'y',
			'&yen;' => 'y',
		
			'&times;' => 'x',
		
			'&szlig;' => 'ss',
		);
		
		return html_entity_decode(strtr($string, $replace), ENT_COMPAT, $encoding);
	}
	
	/**
	 * Translates the string to a valid path string for in a url
	 * 
	 * @param string $str
	 * @return string
	 */
	static public function toUri($str)
	{
		return trim(
			preg_replace(
				array(
					'/[^a-z0-9\-\_\/\.]/i', // Replace everything that isn't alphanumeric or -_/. with a dash
					'/\/+/', // replace multiple slashes with just one
					'/-+/' // replace multiple dashes with just one
				),
				array('-', '/', '-'),
				self::removeAccents($str) // Change acented chars to their normal version
			), '-/' // Trim all - and /
		);
	}
	
	/**
	 * Check if the given string contains utf8 characters
	 * @param $string
	 * @return bool
	 */
	static public function isUtf8($string)
	{
		return (bool) preg_match(
			'%(?:
			[\xC2-\xDF][\x80-\xBF]              # non-overlong 2-byte
			|\xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
			|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
			|\xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
			|\xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
			|[\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
			|\xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
			)+%xs',
			$string
		);
	}
	
	/**
	 * 
	 * @param string $html
	 * @param string $base base domain and protocol http://example.com
	 * @param string $path path from the domain
	 */
	static public function makeUrlsAbsolute($html, $base, $path = '/')
	{
	   if (preg_match_all("/<\s*(a|area|body|form|img|table|td|script|link|object|embed|input)\s+[^>]+>/i", $html, $_matches, PREG_OFFSET_CAPTURE)) {
	      $_posd = 0;
	      $_chunks = array();
	      foreach($_matches[0] as $_match) {
	         $_posp  = $_match[1];
	         $_pos   = $_match[1] - $_posd;
	         $_tag   = $_oldTag = $_match[0];
	         $_tagl  = strtolower($_tag);
	
	         // Skip tags without 'href', 'action', 'src' or 'background' attribute:
	         if (!preg_match('/(href|action|src|background|data)\s*=\s*(?:"([^"]+)"|([^\'">\s]+)|\'([^\']+)\')/i', $_tag, $_reg)){
	            continue;
	         }
	
	         $_url  = $_reg[2] ? $_reg[2] : ($_reg[3] ? $_reg[3] : $_reg[4]);
	         $_urll = strtolower($_url);
	
	         // Continue when URL starts with #, mailto:, ftp: of javascript:
	         if (strpos($_urll, '#') === 0 || strpos($_urll, 'mailto:') === 0 || strpos($_urll, 'javascript:') === 0 || strpos($_urll, 'ftp:') === 0 || strpos($_urll, 'itpc:') === 0 || substr($_urll, 0, 10) == '/x/plugin/') {
	            continue;
	         }
	
	         if (substr($_urll, 0, 7) != 'http://' && substr($_urll, 0, 8) != 'https://' && substr($_urll,0,2) != '{$') {
	            // Make HREF absolute:
	            if ($_url[0] != '/') {
	               $_url = $path . $_url;
	            }
	            $_url = $base . str_replace('//','/',$_url);
	         }
	
	         $_chunks[] = substr($html, 0, $_pos);
	         $_chunks[] = str_replace($_reg[0], "{$_reg[1]}=\"{$_url}\"", $_tag); // New tag
	         $_len      = strlen($_oldTag);
	         $html      = substr($html, $_pos + $_len);
	         $_posd     = $_posp + $_len;
	      } // end foreach
	      $_chunks[] = $html;
	      $html = implode('', $_chunks);
	   }
	
	   // Substitute url() style tags:
	   if (preg_match_all('/url\(([^\)]+)\)/i', $html, $reg)) {
	      $i = 0;
	      foreach($reg[1] as $match) {
	         if (($match[0] == "'" && substr($match, -1) == "'") ||
	            ($match[0] == '"' && substr($match, -1) == '"')) {
	            // Remove quotes (they shouldn't be there in the 1st place!):
	            $match = substr($match, 1, strlen($match)-2);
	         }
	         if (preg_match('/^https?:\/\/|\/x\//', $match)) continue;
	
	         $match = ($match[0] == '/') ? $base . $match : $base . $path . $match;
	         $html = str_replace($reg[0][$i], 'url(' . $match . ')', $html);
	
	         $i++;
	      }
	   }
	   return $html;
	}
	
	/**
	 * Turns all urls in the text into <a> nodes
	 * @param string $text
	 * @param bool $nofollow
	 * @return string
	 */
	public static function autolinkUrls($text, $nofollow = false)
	{
		$nofollow = $nofollow ? ' rel="nofollow"' : '';
		
		$leader = "~(^|[ \n\r\t\.\,\;\:>])";
		
		$s = array();
		$r = array();
		
		// Emailadres. Werkt alleen niet met <b> tags etc eromheen, maar is momenteel even de beste oplossing
		$s[] = $leader."([a-z_-][\+a-z0-9\._-]*@[a-z0-9_-]+(\.[a-z0-9_-]+)+)([^ \n\r\t<]*)~i";
		$r[] = "\\1<a href=\"mailto:\\2\" target=\"_blank\"{$nofollow}>\\2</a>";
		
		//Hier worden alle http:// texten aanklikbare links.
		$s[] = $leader."((http(s?)://)(www\.)?([a-z0-9_-]+(\.[a-z0-9_-]+)+)(/[^/ \n\r<]*)*)~i";
		$r[] = "\\1<a href=\"\\2\" target=\"_blank\"{$nofollow}>\\2</a>";
			
		//Hier worden alle ftp:// texten aanklikbare links.
		$s[] = $leader."((ftp://)(www\.)?([a-z0-9_-]+(\.[a-z0-9_-]+)+)(/[^/ \n\r<]*)*)~i";
		$r[] = "\\1<a href=\"\\2\" target=\"_blank\"{$nofollow}>\\2</a>";
		
		//Als iemand de http:// vergeet en met www. begint wordt automatisch http:// toegevoegd en aanklikbaar gemaakt.
		$s[] = $leader."(www\.([a-z0-9_-]+(\.[a-z0-9_-]+)+)(/[^/ \n\r<]*)*)~i";
		$r[] = "\\1<a href=\"http://\\2\" target=\"_blank\"{$nofollow}>\\2</a>";
		
		//Als iemand de ftp:// vergeet en met ftp. begint wordt automatisch ftp:// toegevoegd en aanklikbaar gemaakt.
		$s[] = $leader."(ftp\.([a-z0-9_-]+(\.[a-z0-9_-]+)+)(/[^/ \n\r<]*)*)~i";
		$r[] = "\\1<a href=\"ftp://\\2\" target=\"_blank\"{$nofollow}>\\2</a>";
		
		return preg_replace($s, $r, $text);
	}
	
	/**
	 * Sweep an directory clean of exipired old files
	 *
	 * @param string $dir Directory to start in
	 * @param int $timeout Timout in seconds
	 * @param bool $recursive Should subdirectories also be sweeped
	 * @return bool
	 */
	public static function sweepDir($dir, $timeout = 86400, $recursive = true)
	{
		if ($timeout != 0) {
			try {
				foreach (@new DirectoryIterator($dir) as $file) {
					if ($recursive && $file->isDir() &&
					  $file != '..' && $file != '.') {
						self::sweepDir($file->getPathname(), $timeout, $recursive);
						@rmdir($file->getPathname());
					}
					
					if ($file->isFile() && $file->getCTime() < (time() - $timeout)) {
						@unlink($file->getPathname());
					}
				}
			} catch (Exception $e) {
				// Apparently the directory is already deleted
			}
		}
		return true;
	}
	
	/**
	 * Get a human readable notation for filesizes in bytes
	 * 
	 * @param int $bytes
	 * @param int $round
	 * @return string
	 */
	public static function formatFileSize($bytes, $round = 0)
	{
	    $sizes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	    $total = count($sizes);
	    for ($i = 0; $bytes > 1024 && $i < $total; $i++) $bytes /= 1024;
	    return round($bytes, $round).$sizes[$i];
	}
	
	/**
	 * Compacts a string containing CSS stripping all unnesesary markup
	 * 
	 * @param string $s The Css string
	 * @return string compacter string
	 */
	public static function compactCss($s)
	{
		$s = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!','', $s);
		$s = str_replace(array("\r\n","\r","\n","\t",'  ','    ','    '),'',$s);
		$s = str_replace('{ ', '{', $s);
		$s = str_replace(' }', '}', $s);
		$s = str_replace('; ', ';', $s);
		return $s;
	}
}
