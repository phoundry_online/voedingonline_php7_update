<?php
/**
 * Script Content Handler
 * 
 * Provides a way to cache pack and get multiple javascript/css files in one HTTP request
 * 
 * @version 1.0.0
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 */
class ScriptContentHandler extends ContentHandler
{
	
	protected $output = '';
	protected $script_type;
	
	protected $cache_key;
	
	/**
	 * @var Cache_Dir
	 */
	protected $cache;
	protected $cache_mtime;
	
	protected $pack = true;
	
	public function __construct($path, $query)
	{
		parent::__construct($path, $query);

		$this->script_type = array_shift($this->path);
		if(!$this->script_type) {
			echo "File not found";
			exit;
		}
		
		$this->pack = !isset($_GET['nopack']);
		
		$this->cache_key = md5(implode('.', $path));
		$this->cache = Cache::factory('dir:///'.get_class($this));
	}
	
	public function init()
	{
		if(!empty($_GET['refresh']) && isset($this->cache[$this->cache_key])) {
			unset($this->cache[$this->cache_key]);
		}
		
		if(!TESTMODE && $this->pack && isset($this->cache[$this->cache_key]))
		{
			$this->output = $this->cache[$this->cache_key];
			$this->cache_mtime = $this->cache->modifiedtime($this->cache_key);
		}
		else
		{
			$files = array();
			foreach ($this->path as $file) {
				$file = str_replace('|', '/', trim($file));
				if(!empty($file)) {
					$files[] = $file;
				}
			}
			
			if(empty($files)) {
				$this->_fileNotFound();
			}
			
			$service = new Script_Service(
				Loader::getInstance()->getLoader('script'),
				Framework::$site->identifier
			);
			
			try {
				$this->output = $service->fetch($this->script_type, $files, $this->pack);
			}
			catch (InvalidArgumentException $e) {
				$this->_fileNotFound();
			}
			
			$this->cache_mtime = time();
			
			if ($this->pack) {
				// Sla cache op
				$this->cache[$this->cache_key] = $this->output;
			}
		}
	}
	
	public function out($return = false)
	{
		//if($return) return $this->output;
		
		$headers = apache_request_headers();
		if(isset($headers["If-Modified-Since"]) &&
		  $this->cache_mtime <= strtotime($headers["If-Modified-Since"])) {
			header("HTTP/1.1 304 Not Modified");
			exit(0);
		}
	
		$cache_time = 432000; // 5days
		
		header('Date: '.gmdate('D, d M Y H:i:s \G\M\T', $this->cache_mtime));
		header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', $this->cache_mtime + $cache_time));
		header('Last-Modified: '.gmdate('D, d M Y H:i:s \G\M\T', $this->cache_mtime ));
		header('Cache-Control: max-age='.$cache_time.', public, pre-check='.$cache_time);
		
		switch ($this->script_type)
		{
			case 'less' :
			case 'css'	: header('Content-type: text/css'); break;
			case 'js'	: header('Content-type: text/javascript'); break;
		}
		
		//@todo Gzip compressie werkt nog niet goed
//		if( strstr($_SERVER['HTTP_USER_AGENT'], 'W3C_Validator')!==false || strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')===false ){
			//header('Content-Length: ' . strlen($this->output));
			echo $this->output;
//		}else{
//			$this->output = gzencode($this->output);
//			header("Content-Encoding: gzip");
//			header('Content-Length: ' . strlen($this->output));
//			echo $this->output;
//		}
		exit(0);
	}
	
	protected function _fileNotFound($file = null)
	{
		header("HTTP/1.0 404 Not Found");
		echo "File not found" . ($file ? ': '.$file : '');
		exit;
	}
}
