<?php
define('MAX_PRETTY_WEEKS_BACK',  52);

class Week {
	var $nr;
	var $nrWeeks;
	var $week_start;
	var $week_end;

	function Week($nr = null){
		if(is_null($nr)){
			$nr = date('YW');
		}
		$this->nr = $nr;
		$this->_loadWeekInfo();
	}

	public function getName($format = "%sd/%sm-%ed/%em Y%w"){

		// replace
		// technical 11/01 - 12-02 200645

		$str = str_replace('%sd', date('d',$this->week_start),	$format);
		$str = str_replace('%ed', date('d',$this->week_end),		$str);

		$str = str_replace('%sm', date('m',$this->week_start),	$str);
		$str = str_replace('%em', date('m',$this->week_end),		$str);

		$str = str_replace('%w', $this->week,	$str);
		$str = str_replace('Y', $this->year,	$str);

		$now = new Week();

		$difference = Week::weekDiff($now, $this);

		if($now->week == $this->week){
			$beautiful = 'deze week';
		} else if($now->week-1 == $this->week){
			$beautiful = 'vorige week';
		} else {

			if($difference == 1){
				$beautiful = '1 week terug';
			} else if($difference < MAX_PRETTY_WEEKS_BACK){
				$beautiful = $difference.' weken terug';
			} else {
				$beautiful = 'meer dan '.MAX_PRETTY_WEEKS_BACK.' weken terug';
			}
		}
		$str = str_replace('%b', $beautiful,	$str);

		// strip whitespace
		$str = preg_replace("/ +/", " ", $str);

		// return name
		return $str;
	}

	static function weekSub(Week $week, $substract = 1){
		die('weekSub not implemented '.__LINE__);

		if($substract <= 0){
			trigger_error('Substraction needs to be a valid integer', E_USER_ERROR);
		}


	}

	// count the difference in weeks
	static function weekDiff(Week $week1, Week $week2){
		if($week1->nr > $week2->nr){
			$first = $week1;
			$second = $week2;
		} else {
			$first = $week2;
			$second = $week1;
		}
		$week_cntr = 0;

		$check = $first;
		while(1==1){
			if($week_cntr >= MAX_PRETTY_WEEKS_BACK) {
				$week_cntr = MAX_PRETTY_WEEKS_BACK;
				break;
			}

			if($check->nr == $second->nr){
				break;
			}
			$week_cntr++;
			$last_checked = $check;
			$check = Week::getPrevious($last_checked);

		}
		return $week_cntr;
	}

	static function getNext(&$Week){
		if($Week->week+1 > $Week->nrWeeks){
			return new Week( ($Week->year+1).'01' );
		} else {
			return new Week( $Week->year.sprintf('%02d',$Week->week+1));
		}
	}

	static function getPrevious(&$Week){
		if($Week->week - 1 < 1 ){
			return new Week( ($Week->year-1). sprintf('%02d', Week::getMaxWeek($Week->year-1) ));
		} else {
			return new Week( $Week->year.sprintf('%02d',$Week->week-1));
		}
	}

	static function getMaxWeek($year){
		$nrWeeks = 0;
		for($i=-1;$i<=7;$i--){
			$nrWeeks = date('W', mktime(23,59,59,1,$i,$year+1 ));
			if($nrWeeks>1) break;
		}
		return (int)$nrWeeks;
	}

	private function _loadWeekInfo(){
		$this->year = (int)substr($this->nr,0,4);
		$this->week = substr($this->nr,-2);

		$start_ts 	= mktime(1, 1, 1, 1, 6, $this->year);
		$date = getdate($start_ts);
		$start_ts = mktime(0, 0, 0, $date['mon'], $date['mday']-(date('N', $start_ts)-1), $date['year']);

		$nrWeeks = $this->nrWeeks = Week::getMaxWeek($this->year);

		if($this->week>$nrWeeks){
			$this->week = $nrWeeks;
			$this->nr = $this->year.$this->week;
		}

		for ($x = 0; $x < $nrWeeks; $x++) {
			$ts = $start_ts + ( $x * 7 * 86400);
			if(date('W', $ts) == $this->week){
				$date = getdate($ts);
				$monday_ts = mktime(0, 0, 0, $date['mon'], $date['mday']-(date('N', $start_ts)-1), $date['year']);
				$this->week_start = $monday_ts;
				$this->week_end = $monday_ts+(4*86400);
				return true;
			}
		}
	}

}
?>
