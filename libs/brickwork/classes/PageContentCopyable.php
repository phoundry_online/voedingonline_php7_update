<?php
interface PageContentCopyable
{
	/**
	 * Copy PageContent specific content
	 * 
	 * @param int$page_content_id
	 * @param int $new_page_content_id
	 * @param int $page_content_tid Table Id of the content table
	 * @param string $site_identifier The new site Identifier
	 * @return bool
	 */
	public static function copyContent($page_content_id, $new_page_content_id, $page_content_tid, $site_identifier);
	
}