<?php
$GLOBALS['__autoload'] = array_merge(
	$GLOBALS['__autoload'],
	array(
		rtrim(CUSTOM_INCLUDE_DIR,'/').'/custom_classes/FeedCreator/',
		rtrim(BRICKWORK_DIR, '/').'/classes/FeedCreator/'									
	)
);
define('TIME_ZONE', '+1');
define('FEEDCREATOR_VERSION', 1.0);

/**
 * Brickwork Feed Creator
 * 
 * Wrapper around other FeedCreators
 * 
 * @package FeedContentHandler
 * @author J�rgen Teunis <jorgen@webpower.nl>
 * 
 */
class BrickworkFeedCreator extends FeedCreator
{
	
	/**
	 * Additional directories to look for Creator and Item classes
	 *
	 * @var array
	 */
	private $_plugin_dirs = array();

	/**
	 * Supported formats
	 *
	 * @var array
	 */
	private $_formats = array(
		'2.0'			=> array('class' => 'RSSCreator20', 'item' => 'FeedItem'),
		'RSS2.0'		=> '2.0',
		'1.0'			=> array('class' => 'RSSCreator10', 'item' => 'FeedItem'),
		'RSS1.0'		=> '1.0',
		'0.91'			=> array('class' => 'RSSCreator091', 'item' => 'FeedItem'),
		'RSS0.91'		=> '0.91',
		'PIE0.1'		=> array('class' => 'PIECreator01','item' => 'FeedItem'),
		'MBOX'			=> array('class' => 'MBOXCreator','item' => 'FeedItem'),
		'OPML'			=> array('class' => 'OPMLCreator','item' => 'FeedItem'),
		'KML'			=> array('class' => 'KMLCreator','item' => 'FeedItem'),
		'ATOM'			=> array('class' => 'AtomCreator03','item' => 'FeedItem'),
		'ATOM0.3'		=> 'ATOM',
		'HTML'			=> array('class' => 'HTMLCreator','item' => 'FeedItem'),
		'JAVASCRIPT'	=> array('class' => 'JSCreator', 'item' => 'FeedItem'),
		'JS'			=> array('class' => 'JSCreator', 'item' => 'FeedItem')
	);
	
	
	public function __construct(){
		
	}
	
	/**
	 * Add a supported format
	 *
	 * @param string $identifier Format
	 * @param string $creator ClassName
	 * @param string $item ClassName
	 * @return bool
	 */
	public function addFormat($identifier, $creator, $item = 'FeedItem')
	{
		$this->_formats[strtoupper($identifier)] = array('class'=>$creator, 'item'=>$item);
		return true;	 
	}

	/**
	 * Add a supported format wich just uses another format
	 *
	 * @param string $alias Format
	 * @param string $code Original format
	 * @return bool
	 */
	public function addFormatAlias($alias, $code)
	{
		if(isset($this->_formats[$code]))
		{
			$this->_formats[$alias]=$code;
			return true;
		}
		return false;
	}
	
	/**
	 * Add a directory to the autoloader
	 *
	 * @param string $dir Path to directory
	 * @return bool
	 */
	public function addPluginDir($dir){
		if(is_dir($dir))
		{
			if(!in_array($dir, $this->_plugin_dirs))
			{
				$this->_plugin_dirs[] = $dir;
				array_unshift($GLOBALS['__autoload'], $dir);
				array_unique($GLOBALS['__autoload']);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Create a FeedCreator
	 *
	 * @throws Exception
	 * @param string $format
	 * @return FeedCreator
	 */
	private function _getCreator($format)
	{
		$format = strtoupper($format);
		$class = 'RSSCreator091'; // default
		
		if(array_key_exists($format, $this->_formats)){
			if(is_array($this->_formats[$format])){
				$class = $this->_formats[$format]['class'];
			} elseif(is_string($this->_formats[$format]) && array_key_exists($this->_formats[$format], $this->_formats)){
				$class = $this->_formats[$this->_formats[$format]]['class'];
			}
		}
		
		$creator = new $class();
		
		if(!($creator instanceof FeedCreator))
		{
			throw new Exception($class." is no instance of FeedCreator");
		}

		$vars = get_object_vars($this);
		foreach ($vars as $key => $value) {
			// prevent overwriting of properties "contentType", "encoding"; do not copy "_feed" itself
			if (!in_array($key, array("_feed", "contentType", "encoding"))) {
				$creator->{$key} = $this->{$key};
			}
		}
		
		return $creator;
	}

	/**
	 * Returns either the format, or false if it is not found
	 *
	 * @param string $code
	 * @return bool/array
	 */
	public function formatExists($code)
	{
		 if(isset($this->_formats[$code])){
		 	return $this->_formats[$code];
		 } else {
		 	return false;
		 }
	}
	
	/**
	 * Magic getter
	 *
	 * @param string $name
	 * @return mixedvar
	 */
	public function __get($name)
	{
		switch($name)
		{
			case 'supportedFormats':	return (array) array_keys($this->_formats);
		}
	}
	
	/**
	 * Creates a syndication feed based on the items previously added.
	 *
	 * @param string $format format the feed should comply to.
	 * @return string Feed output
	 */
	function createFeed($format = "RSS0.91")
	{
		return $this->_getCreator($format)->createFeed();
	}

	/**
	 * Saves this feed as a file on the local disk. After the file is saved, an HTTP redirect
	 * header may be sent to redirect the use to the newly created file.
	 * @since 1.4
	 * 
	 * @param	string	format	format the feed should comply to. Valid values are:
	 *			"PIE0.1" (deprecated), "mbox", "RSS0.91", "RSS1.0", "RSS2.0", "OPML", "ATOM", "ATOM0.3", "HTML", "JS"
	 * @param	string	filename	optional	the filename where a recent version of the feed is saved. If not specified, the filename is $_SERVER["PHP_SELF"] with the extension changed to .xml (see _generateFilename()).
	 * @param	boolean	displayContents	optional	send the content of the file or not. If true, the file will be sent in the body of the response.
	 */
	function saveFeed($format="RSS0.91", $filename="", $displayContents=true)
	{
		$this->_getCreator($format)->saveFeed($filename, $displayContents);
	}

   /**
    * Turns on caching and checks if there is a recent version of this feed in the cache.
    * If there is, an HTTP redirect header is sent.
    * To effectively use caching, you should create the FeedCreator object and call this method
    * before anything else, especially before you do the time consuming task to build the feed
    * (web fetching, for example).
    *
    * @param   string   format   format the feed should comply to. Valid values are:
    *       "PIE0.1" (deprecated), "mbox", "RSS0.91", "RSS1.0", "RSS2.0", "OPML", "ATOM0.3".
    * @param filename   string   optional the filename where a recent version of the feed is saved. If not specified, the filename is $_SERVER["PHP_SELF"] with the extension changed to .xml (see _generateFilename()).
    * @param timeout int      optional the timeout in seconds before a cached version is refreshed (defaults to 3600 = 1 hour)
    */
   function useCached($format="RSS0.91", $filename="", $timeout=3600)
   {
      $this->_getCreator($format)->useCached($filename, $timeout);
   }	
   
}
	
