<?php
/**
 * ErrorObservers can be attached to an ErrorHandler and will be notified
 * of all errors that are being processed by the handler.
 *
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 */
interface ErrorObserver
{
	/**
	 * This method gets called by the ErrorHandler for each error that occurs
	 *
	 * @param ErrorVoedingonline $obj
	 * @param string $source
	 * @param array $backtrace
	 */
	public function notify(ErrorVoedingonline $obj, $source, $backtrace);
}

/**
 * @deprecated Lege abstracte classes, how usefull!
 */
abstract class Observer {
}

/**
 * ErrorScreener
 *
 * Writes error messages to screen
 *
 * @package	Error
 * @version	1.0
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 */
class ErrorScreener implements ErrorObserver
{
	/**
	 * (non-PHPdoc)
	 * @see classes/ErrorObserver#notify()
	 */
	public function notify(ErrorVoedingonline $obj, $source, $backtrace)
	{
		$source = (string) $source;
		$backtrace = (array) $backtrace;

		global $PHprefs;

		if(!defined('TESTMODE') || !TESTMODE) {
			$source = '';
			$backtrace = '';
		}

		if(!headers_sent() && isset($PHprefs['charset'])) {
			header('HTTP/1.1 500 Internal Server Error', true, 500);
			header('Content-Type: text/html; charset='.
				strtoupper($PHprefs['charset']));
		}

		if(defined('INCLUDE_DIR') &&
			is_file(INCLUDE_DIR."/templates/error.php")) {

			$msg = $this->_includeArgs(
				INCLUDE_DIR."/templates/error.php",
				array(
					'code' => $obj->errno,
					'message' => $obj->errstr,
					'file' => $obj->errfile,
					'line' => $obj->errline,
					'backtrace' => $backtrace,
					'source' => $source,
				)
			);
		}
		else {
			$msg = sprintf("%d: %s in file: %s at line: %d\n\n<br />%s\n%s",
				$obj->errno,
				$obj->errstr,
				basename($obj->errfile),
				$obj->errline,
				$source,
				nl2br(print_r($backtrace, true))
			);

		}

		print $msg;

		if(defined('TESTMODE') && TESTMODE) {
			exit(1);
		}
	}

	/**
	 * Used to include template files with global vars
	 *
	 * @param string $__includefile__ path to the template file being included
	 * @param array $args vars which will be in the scope of the template
	 * @param bool $return return the output or output it
	 * @return string|void
	 */
	protected function _includeArgs($__includefile__, $args, $return = true)
	{
		extract($args, EXTR_SKIP);
		ob_start();
		include($__includefile__);
		if($return) {
			return ob_get_clean();
		}

		ob_flush();
	}

	/**
	 * Get a syntax highlighted source code snippet
	 *
	 * @param string $file
	 * @param int $file_line
	 * @param int $topoffset
	 * @param int $botoffset
	 * @return string
	 */
	public function getSource($file, $file_line, $topoffset = 5, $botoffset = 5)
	{
		$source = '';
		if(file_exists($file)){
			if($file = file($file)) {
				$startrow = max(0, $file_line -5);
				$buffer = '<?php';
				for($line = $startrow; $line < $file_line + 5; $line++) {
					if(isset($file[$line])) {
						$row = $line + 1;
						$buffer .= "\n/*{$row}*/ ".trim($file[$line], "\n");
					}
				}
				$buffer .= "\n?>";
				$source = highlight_string($buffer, 1);
			}
			else {
				$source = 'No source found';
			}
		}

		return $source;
	}
}

/**
 * Logs error messages to logfile
 *
 * @package	Error
 * @version	1.0
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 */
class ErrorLogger implements ErrorObserver
{
	/**
	 * @var string The file where logs are being written to
	 */
	protected $_logfile;

	/**
	 * Creates a new ErrorLogger and validates and creates the logfile
	 * @param string $file path to logfile
	 */
	public function __construct($file = null)
	{
		$file = File::get((string) $file, true);
		if($file) {
			$this->_logfile = $file->src;
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/ErrorObserver#notify()
	 */
	public function notify(ErrorVoedingonline $obj, $source, $backtrace)
	{
		// syslog errors
		$msg = sprintf("%d: %s in file: %s at line: %d",
			$obj->errno,
			$obj->errstr,
			basename($obj->errfile),
			$obj->errline
		);

		if($this->_logfile) {
			error_log('['.date ("D d M Y H:i:s").'] [error] '.$msg."\n", 3,
				$this->_logfile);
		}
		else {
			error_log($msg);
		}
	}

}

/**
 * Handles errors within the PHP environment
 *
 * @package	Error
 * @version	1.0
 * @author	J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @access	public
 */

class ErrorHandler
{
	protected $observers;
	protected static $instance;

	/**
	 * Only allow one singleton instance
	 */
	protected function __construct()
	{
		$this->observers = array();
	}

	/**
	 * Returns the singleton instance
	 *
	 * @return ErrorHandler
	 */
	public static function singleton()
	{
		if(null === self::$instance) {
			self::$instance = new ErrorHandler();
			set_error_handler(array(self::$instance, 'logError'));
			set_exception_handler(array(self::$instance, 'logException'));
		}
		return self::$instance;
	}

	/**
	 * Attaches a observer to the singleton ErrorHandler
	 * @param ErrorObserver $observer
	 * @return bool
	 */
	public static function attach(ErrorObserver $observer)
	{
		return self::singleton()->attachObserver($observer);
	}

	/**
	 * Attaches a observer to this ErrorHandler
	 * @param ErrorObserver $observer
	 * @return bool
	 */
	public function attachObserver(ErrorObserver $observer)
	{
		if(!isset($this->observers[get_class($observer)])) {
			$this->observers[get_class($observer)] = $observer;
			return true;
		}
		return false;
	}

	/**
	 * The actual error logging, exceptions and normal errors are turned in
	 * Error objects which are handled by this method
	 *
	 * @param ErrorVoedingonline|int $error
	 * @param string $errstr
	 * @param string $errfile
	 * @param string $errline
	 * @return void
	 */
	public function log($error, $errstr = null, $errfile = null, $errline = null)
	{
		// Create our Error object using the extra params
		if(!$error instanceof Error) {
			return $this->logError($error, $errstr, $errfile, $errline);
		}

		// log errors
		$this->errors[] = $error;

		// skip notices in live omgeving
		if((!defined('TESTMODE') || !TESTMODE) &&
		($error->errno == E_NOTICE || $error->errno == E_USER_NOTICE)) {
			return;
		}

		$source = '';
		if(file_exists($error->errfile)) {
			if($file = file($error->errfile)) {
				$startrow = max(0, $error->errline -5);
				$buffer = '<?php';
				for($line = $startrow; $line < $error->errline + 5; $line++) {
					if(isset($file[$line])) {
						$row = $line + 1;
						$buffer .= "\n/*{$row}*/ ".trim($file[$line], "\n");
					}
				}
				$source = highlight_string($buffer, true);
			} else {
				$source = 'No source found';
			}
		}

		$i = 0;
		$backtrace = array();
		foreach ($error->backtrace as $er) {
			$bt = array();

			foreach($er as $k => $v) {
				if($k == 'args' && is_array($v)) {
					foreach($er['args'] as $argument) {
						if(is_object($argument)) {
							$bt[$k][] = get_class($argument).'()';
						}
						else if(is_array($argument)) {
							$aTmp = array();
							foreach($argument as $key => $val) {
								if(strstr(strtolower($key), 'passw')) {
									$aTmp[$key] = 'XXXXXXXXXXXXXXXX';
								}
								else if(is_array($val)) {
									$aTmp[$key] = 'Array( ... )';
								}
								else if(is_object($val)) {
									$aTmp[$key] = get_class($val) . '()';
								}
								else {
									$aTmp[$key] = var_export($val, true);
								}
							}
							$bt[$k][] = $aTmp;
						}
						else {
							$bt[$k][] = var_export($argument, true);
						}

					}
				}
				else {
					$bt[$k] = $v;
				}
			}

			$backtrace[] = $bt;
			$i++;
		}

		if($error->errno == E_USER_ERROR) {
			header('HTTP/1.1 500 Internal Server Error', true, 500);
		}

		foreach($this->observers as $observer) {
			$observer->notify($error, $source, $backtrace);
		}

		if($error->errno == E_USER_ERROR) {
			exit(1);
		}
	}

	/**
	 * Logs a PHP error, notice or warning
	 *
	 * @param int $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param int $errline
	 * @return void
	 */
	public function logError($errno, $errstr, $errfile, $errline)
	{
		// This happens when PHP5 is used and PHP4-functionality is
		// encountered: we need to continue as if nothing happened.
		/*if((int)PHP_VERSION >= 5 && $errno == E_STRICT) {
			return;
	}*/
		if (defined('E_DEPRACTED') && $errno == E_DEPRACTED) {
			return;
		}

		// error_reporting value of 0 means the @ construct is used
		if(error_reporting() === 0) {
			return;
		}

var_dump("Message: ". $errstr);
var_dump("File: ".$errfile);
var_dump("Line: ".$errline);die;
		/*$this->log(new ErrorVoedingonline($errno, $errstr, $errfile, $errline,
			array_slice(debug_backtrace(), 1)));*/
		debug_print_backtrace();die;

		// We never want the normal error handler to handle errors
		// @see http://php.net/set_error_handler
		return true;
  }

	/**
	 * Logs a caught exception
	 *
	 * @param Exception $exception
	 * @return void
	 */
	public function logException($exception)
	{
	    var_dump($exception);
	    die;
		return $this->log(new ErrorVoedingonline(
			E_USER_ERROR,
			$exception->getMessage(),
			$exception->getFile(),
			$exception->getLine(),
			$exception->getTrace()
		));
	}

}

// Extra gratis nog meer Exceptions
class AttributeNotFoundException extends Exception {}
class AttributeNotValidException extends Exception {}
class IncorrectObjectException extends Exception {}
class AttributeSettingNotAllowedException extends Exception {}
class FieldMultiplicityIncorrectException extends Exception {}
