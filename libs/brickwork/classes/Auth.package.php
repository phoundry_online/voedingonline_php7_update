<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("Usage of Auth.package.php is deprecated", E_USER_ERROR);
}

require_once INCLUDE_DIR . '/classes/Auth.php';
require_once INCLUDE_DIR . '/classes/Auth/Session.php';

class AuthSession extends Auth_Session {}
class AuthLevel {
	const None    = 'auth_level_none';
	const Session = 'auth_level_session';
	const Visitor = 'auth_level_visitor';
	const User    = 'auth_level_user';
}
