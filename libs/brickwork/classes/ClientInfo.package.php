<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("Usage of ClientInfo.package.php is deprecated", E_USER_ERROR);
}

/**
 * Stubs for backward compatibility
 *
 * @author 	Peter Eussen
 * @package	Brickwork
 */

abstract class ClientInfo extends Client_Info
{

}

class WebClientInfo extends Client_Info_Web
{

}


