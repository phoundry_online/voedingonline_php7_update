<?php
class Script_Service
{
	/**
	 * @var Loader_Script
	 */
	private $loader;
	
	/**
	 * @var string
	 */
	private $site_identifier;
	
	/**
	 * @var File_storageInterface
	 */
	private $storage;
	
	public function __construct(Loader_Script $loader, $site_identifier)
	{
		$this->loader = $loader;
		$this->site_identifier = $site_identifier;
	}
	
	public function setStorage(File_StorageInterface $storage)
	{
		$this->storage = $storage;
	}
	
	public function getStorage()
	{
		if (null === $this->storage) {
			throw new RuntimeException(
				"Storage has not been set using setStorage()"
			);
		}
		return $this->storage;
	}
	
	public function replaceScriptUrls($source)
	{
		return preg_replace_callback(
			"/\/index.php\/script\/(\w*)\/([\w\.\|\/\?\=\-]*)/",
			array($this, "replaceCallback"),
			$source
		);
	}
	
	private function replaceCallback($matches)
	{
		$pack = true;
		
		$exploded = explode("?", $matches[2], 2);
		if (count($exploded) === 2) {
			$parsed = array();
			parse_str($exploded[1], $parsed);
			$pack = !isset($parsed['nopack']);
		}
		
		$files = explode("/", $exploded[0]);
		$files = array_map(array($this, "arrayMap"), $files);
	
		return $this->getStorageUrl($matches[1], $files, $pack, false);
	}
	
	private function arrayMap($value)
	{
		return str_replace("|", "/", $value);
	}
	
	/**
	 * Get the public url for a set of urls
	 * 
	 * @param string $type
	 * @param array $files
	 * @param string $pack
	 * @param bool $cache
	 * @return string
	 */
	public function getStorageUrl($type, array $files, $pack = true, $cache = true)
	{
		$ext = $type;
		if ($ext === 'less') {
			$ext = 'less.css';
		}
		
		$name = sha1($this->site_identifier.implode(",", $files)).".".$ext;
		$storage = $this->getStorage();
		$url = $cache ? $storage->getFileUrl($name) : false;
		if (!$url) {
			$data = $this->fetch($type, $files, $pack);
			$url = $storage->putString($data, $name);
		}
		return $url;
	}
	
	/**
	 * Get the data for various script files as one big string
	 * 
	 * @param string $type js, css of less
	 * @param array $files
	 * @param bool $pack should the output be packed
	 * @return string
	 */
	public function fetch($type, array $files, $pack = true)
	{
		$contents = array();
		foreach ($files as $file) {
			$contents[] = "/** ".basename($file)." **/";
			try {
				$file_contents = file_get_contents($this->findFile($type, $file));
				if (!Utility::isUtf8($file_contents)) {
					$file_contents = utf8_encode($file_contents);
				}
				
				if ($type === 'less') {
					require_once BRICKWORK_DIR. "/libs/lessphp/lessc.inc.php";
					$less = new lessc();
					$file_contents = $less->parse($file_contents);
				}
				
				if ($pack) {
					$file_contents = $this->pack($type, $file_contents);
				}
				
				$contents[] = $file_contents;
			} catch (InvalidArgumentException $e) {
				if ($type === 'js') {
					$contents[] = ';console && console.log("'.__CLASS__.': '.basename($file).' not found");';
				} else {
					throw $e;
				}
			}
			$contents[] = '';
		}
		
		$data = implode("\n", $contents);
		
		return $data;
	}
	
	/**
	 * Fetch the absolute path for the file
	 * 
	 * @throws InvalidArgumentException
	 * @param string $type
	 * @param string $file
	 * @return string absolute path to the requested file
	 */
	private function findFile($type, $file)
	{
		if ($path = $this->loader->find($type.'/'.$this->site_identifier.'/'.$file)) {
			return $path;
		}
		
		if ($path = $this->loader->find($type.'/'.$file)) {
			return $path;
		}

		throw new InvalidArgumentException(
			"File {$file} of type {$type} could not be found!"
		);
	}
	
	/**
	 * Pack the data
	 * @param string $type
	 * @param string $data
	 */
	private function pack($type, $data)
	{
		switch ($type) {
			case 'css' :
			case 'less' :
				return Utility::compactCss($data);
			case 'js' :
				$myPacker = new JavaScriptPacker($data, 0);
				return $myPacker->pack();
		}
		
		return $data;
	}
}