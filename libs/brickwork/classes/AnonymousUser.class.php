<?php
/**
* Anonymous user
*
* When a user must be present in an app, use this user
*
* @author Mick van der Most van Spijk <mick@webpower.nl>
* @version 0.0.1
*/
class AnonymousUser extends AuthUser {
	/**
	* Make a new anonymous user
	*/
	public function __construct() {
		parent::__construct();

		// an anonymous user is never logged in
		$this->is_logged_in = FALSE;

		// an anonymous user is never valid
		$this->is_valid = FALSE;
	}

	/**
	* Login
	*
	* Anonymous user cannot log in
	* @return Boolean FALSE
	*/
	public function login() {
		return FALSE;
	}

	/**
	* isAnonymous
	*
	* An anonymous user is always anonymous
	* @return Boolean TRUE
	*/
	public function isAnonymous() {
		return TRUE;
	}
}
?>