<?php
/**
 * Mailtemplates that are manageable in phoundry
 * 
 * @author Christiaan Baartse <christaan.baartse@webpower.nl>
 * @copyright 2011 Web Power BV, http://www.webpower.nl
 */
class Mailtemplate extends ActiveRecord
{
	protected $_table_name = 'brickwork_mailtemplate';
	
	protected static $_templates = array();
	
	/**
	 * Used by ActiveRecord::factory
	 * 
	 * @see ActiveRecord#factory()
	 * @param mixed $id
	 * @param bool $load
	 * @return Mailtemplate
	 */
	public static function registry($id = null, $load = true)
	{
		if(!is_null($id) && $load) {
			$key = is_array($id) ? implode(',', $id) : $id;
			
			if(!isset(self::$_templates[$key])) {
				$template = new self();
				$template->
						where('code', $id[0])->
						where('site_identifier', $id[1])->
						orWhere('code', $id[0])->
						where('site_identifier', '')->
						orderBy('site_identifier', 'DESC')->
						get(true);
				
				if(!$template->isLoaded()) {
					$template['code'] = $id[0];
					$template['site_identifier'] = ''; //$id[1];
					$template->save();
				}
				
				self::$_templates[$key] = $template;
			}
			
			return self::$_templates[$key];
		}
		
		return new self($id, $load);
	}
	
	/**
	 * Get a template by its code
	 * @param string $code
	 * @param string|null $site_identifier
	 * @return Mailtemplate
	 */
	public static function getByCode($code, $site_identifier = null)
	{
		if(is_null($site_identifier)) {
			$site_identifier = Framework::$site->identifier;
		}
		
		return self::registry(array($code, $site_identifier), true);
	}
	
	/**
	 * Get the bcc email adresses as array
	 *
	 * @return array
	 */
	public function getBcc()
	{
		return preg_split("/,|;|\s/is", $this['bcc']);	
	}
	
	/**
	 * Send the mailtemplate to an array of recipients
	 * 
	 * Recipients uses an arraystructure like:
	 * $recipients = array(
	 * 		array(
	 * 			'to_name' => 'Christiaan Baartse',
	 * 			'to_email' => 'christiaan.baartse@webpower.nl',
 	 * 			'reactiongroup' => $reactiongroup,
 	 * 			'reaction' => $reaction_id
	 * 		),
	 *	 	array( 'name' => ... )
	 * );
	 * All key value parts get assigned to the template.
	 * to_name and to_email are used for emailing, name is optional.
	 * 
	 * @param array $recipients Array of recipient arrays
	 * @param array $global Array of wich the key value pairs get assigned to each mail
	 * @param bool $dry_run Do we really mail or want to view the data first?
	 * @return array On dry_run multidimensional array with maildata. When not dry_run indexes of succesfully send recipients
	 */
	public function send(array $recipients, $global= array(), $dry_run = false)
	{
		$recipients_send = array();
		if (!$recipients) {
			return $recipients_send;
		}
		
		if (!is_array(reset($recipients))) {
			throw new InvalidArgumentException("recipients does not contain an array of arrays");
		}
		
		if(!empty($this['body'])) {
			$mailHtml = new Mail_Content_HTML(
				"mailtemplate:body:".$this['code'].":".$this['site_identifier']);

			// aantal vaste variabelen in alle templates!
			
			$global = array_merge(array(
				'host' => $_SERVER['HTTP_HOST'],
				'remote_addr' => $_SERVER['REMOTE_ADDR'],
				'site_name' => Framework::$site->name,
				'site_url' => Framework::$site->host,
			), $global);

			$mailHtml->assign($global);

			foreach ($recipients as $rid => $recipient) {
				if(!array_key_exists('to_email', $recipient)) {
					continue;
				}
				
				$mailHtml->assign($recipient);
				
				$mailArr = array();
				$mailArr['body'] = (string) $mailHtml;
				$mailArr['subject'] = (string) $mailHtml->fetch(
					"mailtemplate:subject:".$this['code'].":".$this['site_identifier']);
				
				$mailArr['from'] = array(
					'email'	=> Utility::coalesce(
						Utility::arrayValue($recipient, 'from_email'),
						Utility::arrayValue($global, 'from_email'),
						$this['from_email']
					),
				 	'name' => Utility::coalesce(
						Utility::arrayValue($recipient, 'from_name'),
						Utility::arrayValue($global, 'from_name'),
						$this['from_name']
					),
				);
				
				$mailArr['to'] = array(
					'email' => $recipient['to_email'],
					'name' => Utility::arrayValue($recipient, 'to_name', '')
				);
				
				$mailArr['encoding'] = 'quoted-printable';
				$mailArr['send'] = false;
				
				if(!$dry_run) {
					$mail = new Mail();
					$mail->html = $mailArr['body'];
					$mail->subject = $mailArr['subject'];
					$mail->setFrom($mailArr['from']['email'], $mailArr['from']['name']);
					$mail->AddAddress($mailArr['to']['email'], $mailArr['to']['name']);
					$mail->Encoding = $mailArr['encoding'];
					$mailArr['send'] = $mail->Send();
				}
				
				$recipients_send[$rid] = $mailArr;
			}
		}
		
		return $recipients_send;
	}
}
