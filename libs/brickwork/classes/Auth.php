<?php
/**
 * Auth
 *
 * Main authentication class. Use this class to start new authentication.
 * Instanciate Auth and use your own created user object, wich is inherited from
 * AuthUser to log in.
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 2.0.0
 */
class Auth implements SplSubject
{
	/**
	 * Anonymous user
	 * @var string
	 */
	const anonymous_user = 'auth_anonymous_user';

	/**
	 * Anonymous password
	 * @var string
	 */
	const anonymous_pw   = 'auth_anonymous_pw';

	/**
	 * Errors concerning authentication
	 * @var array
	 */
	public $errors;

	/**
	 * Authentication session
	 * @var Auth_Session
	 */
	protected $_auth_session;

	/**
	 * Instance for singleton
	 * @var Auth
	 */
	protected static $_instance;

	/**
	 * Array used to save the observers
	 * attached by the attach method
	 * @var array
	 */
	protected $_observers;

	/**
	 * Used by the Singleton() method. Use that to fetch a Auth instance instead.
	 * @see Auth#singleton()
	 */
	protected function __construct()
	{
		$this->errors = array();
		$this->_observers = array();
		$this->_auth_session = new Auth_Session();
	}

	/**
	 * Function to return the singleton instance
	 * @return Auth
	 */
	public static function singleton()
	{
		if (!isset(self::$_instance)) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Get some indirect properties like the current user object
	 * @param $name
	 * @return mixedvar
	 */
	public function __get($name)
	{
		switch($name) {
			case 'user':
				return $this->getUser();
			default:
				throw new Exception('The requested property ('.$name.') does not exist.');
		}
	}

	/**
	 * Get the current user for this session
	 * @return AuthUser
	 */
	public function getUser()
	{
		return $this->_auth_session->get();
	}

	/**
	 * Use the users login method to log the user in
	 * @param AuthUser|string		$user Either the user object to login or the username
	 * @param string					$password Password or empty when using a AuthUser object
	 * @return bool Did we log in succesfully
	 */
	public function login($user = null, $password = null)
	{
		if($user instanceof AuthUser) {
			if($user->login(Framework::$site)) {
				$this->_auth_session->put($user);
				$this->notify('login', array('success' => true, 'user' => $user));
				return true;
			}

			$this->notify('login', array('success' => false, 'user' => $user));
			return false;
		}
		else {
			$errors = array();
			$loggedin = false;

			if (empty($user)) {
				$errors['user']= 'Verplicht veld is leeg';
			}
			if (empty($password)) {
				$errors['password']= 'Verplicht veld is leeg';
			}

			if (!count($errors))
			{
				$auth_user_class = Framework::$site->auth_user_class;
				$site_user = new $auth_user_class($user, $password);

				$loggedin = $this->login($site_user);

				if(!$loggedin) {
					$errors[] = 'Login mislukt';
				}
			}

			$this->errors = array_merge($this->errors, $errors);

			return $loggedin;
		}
	}

	/**
	 * Destroy session and logged in user
	 */
	public function logout()
	{
		// trip double logout
		static $logged_out = false;

		if (!$logged_out) {
			$site_user = $this->getUser();
			$this->_auth_session->destroy();
			$this->_auth_session = new Auth_Session();

			$this->notify('logout', array('success' => true, 'user' => $site_user));
			$logged_out = true;
		}
	}

	/**
	 * Attaches an observer for this object
	 * Note: there can be only one observer of a specific class. If you for
	 * example add two myObserver classes, the first attached observer will
	 * be replaced by the second one. This method will return the old observer
	 * if any was found, or null if none was set.
	 *
	 * @param 	splObserver 		$observer
	 * @return splObserver|null
	 */
	function attach(SplObserver $observer): void
	{
		$old = Utility::arrayValue($this->_observers, get_class($observer));
		$this->_observers[ get_class($observer) ] = $observer;
	}

	/**
	 * Removes an observer from the list
	 * @param 	splObserver $observer
	 */
	function detach(SplObserver $observer): void
	{
		if(isset($this->_observers[get_class($observer)])) {
			unset($this->_observers[get_class($observer)]);
		}
	}

	/**
	 * Notifies the observers of an event
	 * @param string $name Name of the event that occured
	 * @param array	$data	Event data
	 */
	public function notify($name = '', $data = array()): void
	{
		foreach($this->_observers as $o) {
			$o->update($this, $name, $data);
		}
	}

	/**
	 * @todo Decrepated clean up We put documentation on the wiki nowadays not as php class methods
	 * @return string HTML
	 */
	public static function WPHelp() { return ''; }
}
