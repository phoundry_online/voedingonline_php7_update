<?php
/**
 * Active Record
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
abstract class ActiveRecord implements ArrayAccess, Serializable
{
	/**
	 * @var DB_Adapter Database connection being used
	 */
	protected $_db;

	/**
	 * Array holding the values of the current record
	 *
	 * @var array
	 */
	protected $_values;

	/**
	 * MD5 has of the current values
	 *
	 * @var array
	 */
	protected $_values_hash;

	/**
	 * The pk of the currently loaded record
	 *
	 * @var array
	 */
	protected $_loaded_pk;

	/**
	 * Name of the Table the AR operates on
	 *
	 * @var string
	 */
	protected $_table_name;

	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by;

	/**
	 * Array used for storing the query parameters
	 *
	 * @var array
	 */
	protected $_query_parts = array();

	/**
	 * The last query that ran
	 *
	 * @var string
	 */
	protected $_last_query;

	/**
	 * Caching of the table properties
	 *
	 * @var Cache
	 */
	protected static $_tables_properties_cache;

	/**
	 * Local version of the table definition
	 *
	 * @var array
	 */
	protected $_localTable;

	/**
	 * Factory method
	 *
	 * @throws Exception when the $classname is no ActiveRecord
	 * @param array $id
	 * @param boolean $load Auto load the record in the object
	 * @param DB_Adapter $db Database connection to use
	 * @param [mixedvar]	instead of using an array for the primary key
	 * 						the paramaters can also be overloaded
	 * @return ActiveRecord
	 */
	public static function factory($classname, $id = null, $load = true, $db = null)
	{
		if(null !== $id && !is_array($id) && !is_bool($load)) {
			$id = func_get_args();
			$classname = array_shift($id);
			$load = true;
			$db = null;
		}

		if(!class_exists($classname, true)) {
			throw new Exception($classname.' does not exist');
		}

		if(!is_subclass_of($classname, __CLASS__)) {
			throw new Exception($classname.' is no '.__CLASS__);
		}

		// If the class has a registry method use that to instantiate a new object
		// so we can implement a registry for some classes
		if(method_exists($classname, 'registry')) {
			return call_user_func(array($classname, 'registry'), $id, $load, $db);
		}

		return new $classname($id, $load, $db);
	}

	public function __serialize() {}
	public function __unserialize($arg) {}

	/**
	 * The construct allows overloading of the parameters for records
	 * with more then one column primary keys
	 *
	 * @param string $id
	 * @param bool $load
	 */
	public function __construct($id = null, $load = true, $db = null)
	{
		$this->_db = ($db instanceof DB_Adapter) ? $db : DB::getDb();

		$this->_loadTableDefinition();
		$this->getTableName();
		$this->flushValues();

		if($load !== false && !is_null($id)) {
			if(!is_array($id) && !is_bool($load)) {
				// the parameters were overloaded
				$id = func_get_args();
			}
			$this->get($id);
		}
	}

	/**
	 * Extending classes can overwrite this to do some automagic conversion of
	 * certain columns without breaking the inner workings of this class
	 *
	 * @throws Exception
	 * @param string $name
	 * @return mixedvar
	 */
	public function offsetGet($name): mixed
	{
		return $this->offsetGetReal($name);
	}

	/**
	 * This function should always return the real value as in the database
	 *
	 * @throws Exception
	 * @param string $name
	 * @return mixedvar
	 */
	public function offsetGetReal($name)
	{
		if(!isset($this->_localTable['columns'][$name])) {
			throw new Exception(get_class($this)." has no column named ".$name."!");
		}

		return array_key_exists($name, $this->_values) ? $this->_values[$name] : null;
	}

	/**
	 * Extending classes can overwrite this to do some automagic conversion of
	 * certain columns without breaking the inner workings of this class
	 *
	 * @param string $name
	 * @param mixedvar $value
	 */
	public function offsetSet($name, $value): void
	{
		$this->offsetSetReal($name, $value);
	}

	/**
	 * This function should always be set to set the real db value
	 *
	 * @param string $name
	 * @param mixedvar $value
	 */
	public function offsetSetReal($name, $value)
	{
		if(!isset($this->_localTable['columns'][$name])) {
			throw new Exception(get_class($this)." has no column named ".$name."!");
		}

		$this->_values[$name] = $value;
	}

	/**
	 * Magic isset for the record values
	 *
	 * @param string $name
	 * @return bool
	 */
	public function offsetExists($name): bool
	{
		return isset($this->_localTable['columns'][$name]) &&
			array_key_exists($name, $this->_values);
	}

	/**
	 * Magic unset for the record values
	 *
	 * @param string $name
	 */
	public function offsetUnset($name): void
	{
		if($this->offsetExists($name)) {
			unset($this->_values[$name]);
		}
	}

	/**
	 * Serialize the Active Record
	 * @return string
	 */
	public function serialize()
	{
		return serialize($this->getAsArray());
	}

	/**
	 * Recover from an unserialize
	 * @param array $serialized
	 */
	public function unserialize($serialized)
	{
		// Initialize as an empty active record
		$this->__construct();
		// Fill it with the serialized values
		$this->loadResult(unserialize($serialized));
	}

	/**
	 * Reset the record to that of an unloaded one
	 */
	public function flushValues()
	{
		$this->_loaded_pk = array();
		$this->_values = array();
	}

	/**
	 * Used internaly by ActiveRecord and ORM DON'T USE.
	 * Use ActiveRecordIterator instead!
	 *
	 * @param object|array $result
	 * @param string $className Decrepated
	 * @return ActiveRecord|array
	 */
	public function loadResult($result, $className = null)
	{
		if(is_array($result) &&
			(is_object(current($result)) || is_array(current($result)))) {
			// Record Set
			if(is_null($className)){
				$className = get_class($this);
			}

			$objects = array();
			foreach($result as $row) {
				$object = ActiveRecord::factory($className, null, true,
					$this->_db);
				$object->loadResult($row);
				$objects[] = $object;
			}
			// Workaround for PHP bug which didnt allow foreaching
			// over returned arrays values
			return (array) $objects;
		}

		// Single Record
		$this->flushValues(); // Clean up the old variables
		if(is_object($result)) {
			$result = get_object_vars($result);
		}

		if(!empty($result)) {
			foreach($result as $key => $value) {
				$this->offsetSetReal($key, $value);
			}
			$this->_loaded_pk = $this->getPk();
			$this->_values_hash = $this->getCurrentValuesHash();
			return $this;
		}

		// Before 1.6 loadResult didnt return anything due to bad code
		// now we implicit return it as it is already being used in production
		return null;
	}

	/**
	 * Fire off the query and fetch the records
	 * @return ActiveRecord|ActiveRecordIterator self
	 */
	public function get($id = null)
	{
		$columns = $this->getColumnsSql();
		if(!empty($columns)) {
			if(null === $id || true === $id) {
				if(true === $id) {
					$this->limit(1);
				}

				// Save the queryparts before we use them, this way we can do a count(*) afterwards
				$this->_last_query = $this->_query_parts;

				// TODO Check if SQL_CALC_FOUND_ROWS doesnt cost to
				// much reducing following getCount() calls
				$sql = sprintf("
					SELECT
						%s
					FROM
						`%s`
					%s
					%s
					%s
				",
				$columns,
				$this->_db->quote($this->_table_name),
				$this->sqlWhere(),
				$this->sqlOrderBy(),
				$this->sqlLimit()
				);

				$res = $this->_db->iQuery($sql);
				if(true === $id) {
					return ($res->current() !== null) ? $this->loadResult($res->first()) : $this;
				}
				return new ActiveRecordIterator(get_class($this), $res, $this->_db);
			}

			// Fetch a record by PK
			if(!is_array($id)) {
				$id = func_get_args();
			}
			return $this->wherePk($id)->get(true);
		}
	}

	/**
	 * Same as get() only gives back nr of rows
	 *
	 * @param bool $last_query Count the results of the last query that ran
	 * @return integer
	 */
	public function getCount($last_query = true)
	{
		if($last_query && !empty($this->_last_query)) {
			$this->_query_parts = $this->_last_query;
		}
		$sql = sprintf("
			SELECT
				COUNT(*) as cnt
			FROM (
				SELECT
					%s
				FROM
					%s
				%s
			) as lastQuery
			",
			$this->getColumnsSql(),
			$this->_db->quote($this->_table_name),
			$this->sqlWhere()
		);

		$res = $this->_db->iQuery($sql);

		return (int) $res->first()->cnt;
	}

	/**
	 * Save the record
	 *
	 * @throws Exception
	 * @return boolean Successfully saved
	 */
	public function save()
	{
		$table_name = $this->_table_name;
		$new_values_hash = $this->getCurrentValuesHash();
		$values = array();
		$auto_increment_column = '';
		foreach($this->_localTable['columns'] as $col_name => $column) {
			// If its an updatable field and its changed add it it to the update arrays
			if($column['auto_increment'] && null === $this->offsetGetReal($col_name)) {
				$auto_increment_column = $col_name;
			}

			if(!isset($this->_values_hash[$col_name]) ||
			  (isset($this->_values_hash[$col_name]) &&
			  $new_values_hash[$col_name] != $this->_values_hash[$col_name])) {

				if(null === $this->offsetGetReal($col_name)) {
					$values[$col_name] = $column['null'] ? 'NULL' : "''";
				}
				else {
					$values[$col_name] = $this->_db->quoteValue($this->offsetGetReal($col_name));
				}
			}
		}

		if(!$values) {
			return false;
		}

		if($this->isLoaded()) {
			// Updating an existing record
			$update_sql = '';
			foreach($values as $column => $value) {
				if($update_sql != '') {
					$update_sql .= ', ';
				}
				$update_sql .= sprintf("`%s` = %s", $column, $value);
			}

			$this->sqlWhere(); //Flush the wherepart
			$this->wherePk(); //Always match on the current PK

			$sql = sprintf(
				"UPDATE
				`%s`
				SET
				%s
				%s
				LIMIT 1;",
				$this->_db->quote($table_name),
				$update_sql,
				$this->sqlWhere()
			);
			$res = $this->_db->iQuery($sql);

			// If we updated the pk we want to reflect this
			$this->_loaded_pk = $this->getPk();
			$this->_values_hash = $this->getCurrentValuesHash();
		}
		else {
			// Insert
			$insert_columns = "`".implode("`, `", array_keys($values))."`";
			$insert_values = implode(", ", $values);
			$sql = sprintf("
				INSERT INTO
					`%s`
					(%s)
				VALUES
					(%s)
				",
				$this->_db->quote($table_name),
				$insert_columns,
				$insert_values
			);
			$res = $this->_db->iQuery($sql);

			if(!empty($auto_increment_column)) {
				$this->offsetSetReal($auto_increment_column, $res->last_insert_id);
			}

			// If we updated the pk we want to reflect this
			$this->_loaded_pk = $this->getPk();
			$this->_values_hash = $this->getCurrentValuesHash();
		}
		return true;
	}

	/**
	 * Reloads the ActiveRecord refetching the data from the database
	 *
	 * @return ActiveRecord self
	 */
	public function reload()
	{
		return $this->get($this->getPk());
	}

	/**
	 * Delete the current record
	 *
	 * @throws Exception
	 * @return boolean Successfully deleted
	 */
	public function delete()
	{
		if($this->isLoaded())
		{
			$this->sqlWhere(); //Flush the wherepart
			$this->wherePk()->limit(1);

			$sql = sprintf("
					DELETE
					FROM `%s`
					%s
					%s",
				$this->_db->quote($this->_table_name),
				$this->sqlWhere(),
				$this->sqlLimit()
			);

			$res = $this->_db->iQuery($sql);

			$this->flushValues();
			return TRUE;
		}
		return false;
	}

	/**
	 * Get the values of the current Primary Key
	 *
	 * @return array
	 */
	public function getPk()
	{
		$pk = array();
		foreach($this->_localTable['primary_key'] as $col_name) {
			$pk[] = $this->offsetGetReal($col_name);
		}
		return $pk;
	}

	/**
	 * Get the where clausule to match the PK
	 *
	 * @param string $id If none is given uses the current PK
	 * @return ActiveRecord Self
	 */
	public function wherePk($id = null)
	{
		if(is_null($id)) {
			$id = !empty($this->_loaded_pk) ? $this->_loaded_pk : $this->getPk();
		} else if(!is_array($id)) {
			$id = func_get_args();
		}
		$where = array();
		$pk    = $this->_localTable['primary_key'];

		if(count($pk) != count($id)) {
			throw new Exception(get_class($this)."'s Primary Key consists of ".count($pk)." columns. ".count($id)." keys found.");
		}
		foreach($pk as $col_name) {
			$where[$col_name] = array_shift($id);
		}
		return $this->where($where);
	}

	/**
	 * Generate a unique hash for the current columns
	 * and values so we can see if the record has changed
	 *
	 * @return string
	 */
	public function getCurrentValuesHash()
	{
		$keys = array_keys($this->_values);
		$res = array();
		foreach ($keys as $key) {
			$res[$key] = md5(strval($this->_values[$key]));
		}

		return $res;
	}

	/**
	 * Did the record change since the last loadResult() or instantiation
	 *
	 * @return bool
	 */
	public function isAltered()
	{
		return ($this->_values_hash != $this->getCurrentValuesHash());
	}

	/**
	 * Do we got a record
	 *
	 * @return bool
	 */
	public function isLoaded()
	{
		return (count($this->_loaded_pk) != 0 && !is_null(max($this->_loaded_pk)));
	}

	/**
	 * Get the current column values
	 *
	 * @return array
	 */
	public function getAsArray()
	{
		$values = array();
		foreach($this->_localTable['columns'] as $col_name => $column) {
			$values[$col_name] = $this->offsetGetReal($col_name);
		}
		return $values;
	}

	/**
	 * Add where statement
	 *
	 * @param string $key Colum name
	 * @param string $value
	 * @param string $operator
	 * @param bool $dbquote Quote the value
	 * @return ActiveRecord
	 */
	public function where($key, $value = null, $operator = '=', $dbquote = TRUE)
	{
		$keys = is_array($key) ? $key : array($key => $value);

		if(!isset($this->_query_parts['where']) ||
			!is_array($this->_query_parts['where'])) {
			$this->_query_parts['where'] = array();
		}

		foreach($keys as $key => $value) {
			$this->_query_parts['where'][] = array($key, $value, 'AND', $operator, $dbquote);
		}

		return $this;
	}

	/**
	 * Add where statement
	 *
	 * @param string $key Colum name
	 * @param string $value
	 * @param string $operator
	 * @param bool $dbquote Quote the value
	 * @return ActiveRecord
	 */
	public function orWhere($key, $value = null, $operator = '=', $dbquote = TRUE)
	{
		$keys = is_array($key) ? $key : array($key => $value);

		if(!isset($this->_query_parts['where']) ||
			!is_array($this->_query_parts['where'])) {
			$this->_query_parts['where'] = array();
		}

		foreach($keys as $key => $value) {
			$this->_query_parts['where'][] = array($key, $value, 'OR', $operator, $dbquote);
		}

		return $this;
	}
	/**
	 * Where the given column matches one of the array values
	 *
	 * @param string $key Colum name
	 * @param array $values
	 * @param bool $not_in use NOT IN instead of IN
	 * @return ActiveRecord
	 */
	public function whereIn($key, array $values, $not_in = false)
	{
		$vals = array();
		foreach ($values as $val)
		{
			$vals[] = $this->_db->quoteValue($val);
		}

		return empty($values) ? $this : $this->where($key, '('.implode(', ', $vals).')', $not_in ? 'NOT IN' : 'IN', false);
	}

	/**
	 * Where the given column does NOT matches one of the array values
	 *
	 * @param string $key Colum name
	 * @param array $values
	 * @return ActiveRecord
	 */
	public function whereNotIn($key, array $values)
	{
		return $this->whereIn($key, $values, true);
	}

	/**
	 * Where the given column matches null or not null
	 * @param string	$key	Column name
	 * @param bool		$is		If the value should match null or not
	 * @return ActiveRecord
	 */
	public function whereNull($key, $is = true)
	{
		return $this->where($key, 'NULL', $is ? 'IS' : 'IS NOT', false);
	}

	/**
	 * Custom Where clausule
	 *
	 * @param string $sql
	 * @return ActiveRecord
	 */
	public function customWhere($sql = null) {
		if(is_null($sql)){
			return $this->_where_custom;
		}
		$this->_query_parts['where_custom'] = $sql;
		return $this;
	}

	/**
	 * Create the sql for the where statements
	 *
	 * @param bool $flush Reset the query parts
	 * @return string
	 */
	protected function sqlWhere($flush = true)
	{
		$clausule = '';
		$clausule.= (!empty($this->_query_parts['where_custom']))? $this->_query_parts['where_custom'] : '';

		if(!empty($this->_query_parts['where'])){
			foreach($this->_query_parts['where'] as $where){
				list($key, $value, $and_or, $operator, $dbquote) = $where;
				if($clausule != '') {
					$clausule.= ' '.$and_or.' ';
				}

				if($dbquote) {
					$key = '`'.$key.'`';
					$value = $this->_db->quoteValue($value);
				}
				if ($value === 'NULL' && $operator === '=') {
					$operator = 'IS';
				}
				if ($value === 'NULL' && $operator === '!=') {
					$operator = 'IS NOT';
				}

				$clausule .= $key.' '.$operator.' '.$value;
			}
		}
		if($flush) {
			$this->_query_parts['where_custom'] = '';
			$this->_query_parts['where'] = array();
		}

		return $clausule ? "WHERE\n".$clausule : '';
	}

	/**
	 * Limit the query
	 *
	 * @param integer $items
	 * @param integer $offset
	 * @return ActiveRecord
	 */
	public function limit($items = null, $offset = null){
		if(is_null($items) && is_null($offset)){
			$this->_query_parts['limit'] = '';
			return $this;
		}
		if(!is_null($items) && is_null($offset)){
			$this->_query_parts['limit'] = "LIMIT {$items}";
			return $this;
		}

		$this->_query_parts['limit'] = "LIMIT {$offset}, {$items}";
		return $this;
	}

	/**
	 * Output the sql for the limit
	 *
	 * @param bool $flush Reset the query parts
	 * @return string
	 */
	protected function sqlLimit($flush = true){
		$limit = !empty($this->_query_parts['limit']) ? $this->_query_parts['limit'] : '';
		if($flush) {
			$this->_query_parts['limit'] = '';
		}
		return $limit;
	}

	/**
	 * Order by
	 *
	 * @param string $key
	 * @param string $order
	 * @return ActiveRecord
	 */
	public function orderBy($key, $order = 'ASC'){
		if(is_array($key)) {
			foreach($key as $k => $v) {
				$this->orderBy($k, $v);
			}
			return $this;
		}

		if(!is_string($key) || !is_string($order)) {
			throw new Exception(__METHOD__." expects a string as parameter");
		}
		if(!isset($this->_query_parts['order_by']) ||
			!is_array($this->_query_parts['order_by'])) {
			$this->_query_parts['order_by'] = array();
		}
		$this->_query_parts['order_by'][] = array($key, $order);
		return $this;
	}

	/**
	 * Order Randomly
	 *
	 * @return ActiveRecord self
	 */
	public function orderByRand()
	{
		if(!isset($this->_query_parts['order_by']) ||
		!is_array($this->_query_parts['order_by'])) {
			$this->_query_parts['order_by'] = array();
		}
		$this->_query_parts['order_by'][] = "RAND()";
		return $this;
	}

	/**
	 * Output the sql for the Order By
	 *
	 * @param bool $flush Reset the query parts
	 * @return string
	 */
	protected function sqlOrderBy($flush = true){
		$order_by = array();
		$order_by_array = array();
		if(!empty($this->_query_parts['order_by'])) {
			$order_by_array = $this->_query_parts['order_by'];
		} else if(!empty($this->_default_order_by) && is_array($this->_default_order_by)) {
			if(!is_array(current($this->_default_order_by))) {
				$order_by_array = array($this->_default_order_by);
			}
			else {
				$order_by_array = $this->_default_order_by;
			}
		}


		foreach($order_by_array as $ob) {
			if(is_array($ob)) {
				list($key, $order) = $ob;
				$order_by[] = sprintf("`%s` %s\n", $key, $order);
			}
			else if("RAND()" === $ob) {
				$order_by[] = "RAND()\n";
			}
		}

		if($flush) {
			$this->_query_parts['order_by'] = array();
		}

		return $order_by ? "ORDER BY\n".implode(', ', $order_by) : '';
	}

	/**
	 * The columns escaped as SQL select part
	 *
	 * @return string
	 */
	public function getColumnsSql($table_name = null)
	{
		$columns = $this->_localTable['columns'];
		if(is_array($columns)) {
			if(!is_null($table_name)) {
				$table_name = $this->_db->quote($table_name);
				return "`{$table_name}`.`".implode("`,\n `{$table_name}`.`", array_keys($columns))."`";
			} else {
				return "`".implode("`,\n `", array_keys($columns))."`";
			}
		}
	}

	/**
	 * Get the table properties
	 *
	 * @return array/array_key
	 */
	public function getTableProperty($property = null)
	{
		if (null === $property) {
			return $this->_localTable;
		}
		else if(array_key_exists($property, $this->_localTable)) {
			return $this->_localTable[$property];
		}

		throw new InvalidArgumentException( sprintf("Illegal argument for call: %s",$property));
	}

	/**
	 * Get the Tablename for the current class
	 *
	 * @return string
	 */
	public function getTableName()
	{
		if (!isset($this->_table_name)) {
			// Camelcase to underscored
			$this->_table_name = strtolower(
				trim(
					preg_replace(
						'/[A-Z]/',
						'_$0',
						str_replace('_', '', get_class($this))
					),
					'_'
				)
			);
		}
		return $this->_table_name;
	}

	protected function _loadTableDefinition()
	{
		$table_name = $this->getTableName();

		// Singleton cache object
		if(! self::$_tables_properties_cache instanceof Cache)
		{
			self::$_tables_properties_cache = Cache::factory(
				defined('ACTIVERECORD_TABLE_CACHE') ?
				ACTIVERECORD_TABLE_CACHE :
				'file:///activerecord_tables?timeout=3600'
			);
		}

		if(!isset(self::$_tables_properties_cache[$table_name]))
		{
			$res = $this->_db->iQuery(sprintf("DESC `%s`", $this->_db->quote($table_name)));
			$primary_key = array();
			$columns = array();
			while ($res->current()) {
                $column = $res->current();
				if (strpos($column->Field, 'PK_') === 0) {
					$primary_key[] = $column->Field;
				}

				if($column->Key == 'PRI') {
					$primary_key[] = $column->Field;
				}

				$columns[$column->Field] = array(
					'type' => sscanf(str_replace(str_split("()"), " ", $column->Type), '%s %s %s'),
					'null' => ($column->Null == 'YES'),
					'default' => $column->Default,
					'auto_increment' => ($column->Extra == 'auto_increment'),
					'primary_key' => $column->Key == 'PRI',
					'unique' => $column->Key == 'UNI'
				);
				$res->next();
			}

			self::$_tables_properties_cache[$table_name] = array(
				'primary_key' => $primary_key,
				'columns' => $columns
			);
		}

		$this->_localTable = self::$_tables_properties_cache[$table_name];
		return true;
	}
}
