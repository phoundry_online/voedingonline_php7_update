<?php
/**
 * Generic Cache object
 *
 */
abstract class Cache implements ArrayAccess
{
	/**
	 * @var array Instantiated cache objects
	 */
	protected static $_instances = array();
	
	/**
	 * Cache Factory
	 *
	 * @param string $url
	 * @return Cache
	 */
	public static function factory($url)
	{
		if(!isset(self::$_instances[$url])) {
			$meta = parse_url(str_replace(':///', ':// /', $url));
			$meta['host'] = trim($meta['host']);
			$type = $meta['scheme'];
			
			if(!empty($GLOBALS['WEBprefs']['wipe_cache'])) {
				if(isset($meta['query'])) {
					$meta['query'] .= "&timeout=15";
				}
				else {
					$meta['query'] = "timeout=15";
				}
			}
			
			$class = 'Cache_'.ucfirst($type);
			if(!class_exists($class)) {
				throw new Exception('Driver '.$type.' does not exist.');
			}
			
			$driver = new $class($meta);
			if(!$driver instanceof self) {
				throw new Exception('Driver '.$type.' isn\'t an instance of '.__CLASS__);
			}
			self::$_instances[$url] = $driver;
		}
		
		return self::$_instances[$url];
	}
	
	/**
	 * Magic ArrayAccess function
	 *
	 * @param string $name
	 * @return bool
	 */
	public function offsetExists($name): bool
	{
		return $this->keyIsset($name) && $this->getValue($name) !== null;
	}
	
	/**
	 * Magic ArrayAccess function
	 *
	 * @param string $offset
	 * @return mixedvar
	 */
	public function offsetGet($offset): mixed
	{
		return $this->getValue($offset);
	}
	
	/**
	 * Magic ArrayAccess function
	 *
	 * @param string $offset
	 * @param mixedvar $value
	 */
	public function offsetSet($offset, $value): void
	{
		$this->setValue($offset, $value);
	}
	
	/**
	 * Magic ArrayAccess function
	 *
	 * @param string $offset
	 */
	public function offsetUnset($offset): void
	{
		$this->unsetKey($offset);
	}
	
	/**
	 * Magic ObjectAcess function
	 * 
	 * @param string $name
	 * @return mixedvar
	 */
	public function __get($name)
	{
		return $this->getValue($name);
	}
	
	/**
	 * Magic ObjectAcess function
	 * 
	 * @param string $name
	 * @param mixedvar $value
	 */
	public function __set($name, $value)
	{
		$this->setValue($name, $value);
	}
	
	/**
	 * Magic ObjectAcess function
	 * 
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		return $this->keyIsset($name) && $this->getValue($name) !== null;
	}
	
	/**
	 * Magic ObjectAcess function
	 * 
	 * @param string $name
	 */
	public function __unset($name)
	{
		$this->unsetKey($name);
	}
	
	abstract public function setValue($key, $value);
	
	abstract public function getValue($key);
	
	abstract public function keyIsset($key);
	
	abstract public function unsetKey($key);
	
	abstract public function flush();
}
