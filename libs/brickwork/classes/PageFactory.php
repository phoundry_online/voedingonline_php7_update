<?php
/**
 * Factory for creating pages
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class PageFactory
{
	/**
	 * @var DB_Adapter
	 */
	private $db;

	/**
	 * @var Site
	 */
	private $site;

	/**
	 * @var ContentHandler
	 */
	private $content_handler;

	/**
	 * @var ClientInfo
	 */
	private $client_info;

	/**
	 * @var Auth
	 */
	private $auth;

	/**
	 * @var ACL
	 */
	private $acl;

	/**
	 * Create a Factory with the needed dependencies
	 *
	 * @param DB_Adapter $db
	 * @param Site $site
	 * @param ContentHandler $content_handler
	 * @param Client_Info $client_info
	 * @param Auth $auth
	 */
	public function __construct(DB_Adapter $db, Site $site,
		ContentHandler $content_handler, Client_Info $client_info, Auth $auth)
	{
		$this->db = $db;
		$this->site = $site;
		$this->content_handler = $content_handler;
		$this->client_info = $client_info;
		$this->auth = $auth;
	}

	/**
	 * Enables ACL
	 * @param ACL $acl
	 * @return PageFactory self
	 */
	public function setAcl(ACL $acl)
	{
		$this->acl = $acl;
		return $this;
	}

	/**
	 * Get the page associated to the structure
	 *
	 * @param StructureItem $structure
	 * @return Page|bool
	 */
	public function pageByStructureItem(StructureItem $structure, $page_id = null)
	{
		$lang_id = isset($this->site->currentLang) ?
			$this->site->currentLang->getId() : null;
		if ($page_id === null) {
			$page_data = SiteStructure::getLivePage(
				$this->db,
				$this->site->identifier,
				$structure->id,
				$lang_id,
				$page_id
			);
		} else {
			$page_data = $this->getPageData($page_id);
		}


		// When no page could be loaded, or the one we requested couldn't
		if (!$page_data) {
			return false;
		}

		// load page containers
		$staging_status = array('live');

		if ($page_id !== null) {
			$staging_status[] = 'test';
		}

		$online_sql = '
			AND
				(
				(NOW() BETWEEN pc.online_date AND pc.offline_date)
				OR
				(pc.online_date <= NOW() AND pc.offline_date IS NULL)
				OR
				(pc.offline_date > NOW() AND pc.online_date IS NULL)
				OR
				(pc.online_date IS NULL AND pc.offline_date IS NULL)
				)';

		$sql = sprintf(
			'SELECT DISTINCT
				pc.id as instance_id,
				pc.content_container_code,
				pc.respect_auth,
				m.class as class,
				m.name as name,
				pc.module_code,
			
				m.cachetime as cachetime,
				m.template as template,
				pt1.name as config_table,
				pt2.name as content_table
			FROM
				brickwork_page p
			INNER JOIN
				brickwork_page_content pc
			ON
				pc.page_id = p.id
			INNER JOIN
				brickwork_module m
			ON
				m.code = pc.module_code
			INNER JOIN
				brickwork_content_container_allows_module lcam
			ON
				lcam.module_code = m.code
			INNER JOIN
				brickwork_content_container cc
			ON
				cc.id = lcam.content_container_id
			AND
				pc.content_container_code = cc.code
			AND
				cc.page_type_code = p.page_type_code
			LEFT JOIN
				phoundry_table pt1
			ON
				pt1.id = m.ptid_config
			LEFT JOIN
				phoundry_table pt2
			ON
				pt2.id = m.ptid_content
			WHERE
				pc.page_id = %d
			AND
				pc.staging_status IN ("%s")
			AND
				pc.state IN ("delete","ok")
			%s
			ORDER BY
				cc.prio ASC, pc.prio ASC',
			// values
			$page_data->id,
			implode('","', $staging_status),
			$page_id === null ? $online_sql : ''
		);

		$res = $this->db->iQuery($sql);

		$content_containers = array();

		// content vlakken maken
		while ($res->current()) {
            $mod = $res->current();
         	if (!isset($content_containers[$mod->content_container_code])) {
				$container = new ContentContainer(
					$mod->content_container_code,
					$this->content_handler,
					(int) $page_data->id
				);

				if (null !== $this->acl && $structure->secure &&
						$structure->restricted) {
					$container->allowed = $this->acl->hasAccess(
						$this->auth->user, $container
					);
				}
				$content_containers[$mod->content_container_code] = $container;
			} else {
				$container = $content_containers[$mod->content_container_code];
			}

			$module = $this->createModule(
				(int) $mod->instance_id,
				$mod->class,
				$mod->module_code,
				$mod->name,
				$container,
				$mod->template,
				$mod->content_table, $mod->config_table,
				$mod->cachetime,
				(bool) $mod->respect_auth
			);

			if ($this->isModuleShown($container, $structure, $module)) {
				$container->attach($module);
			}
			$res->next();
		}

		return new Page(
			(int) $page_data->id,
			$structure,
			new PageType($page_data->page_type_code),
			$this->site,
			$this->client_info,
			$this->auth,
			$page_data->title,
			$page_data->description,
			$page_data->keywords,
			$content_containers
		);
	}

	/**
	 * Gets the page data for a specific page
	 * @param int $page_id
	 * @return object|bool
	 */
	private function getPageData($page_id)
	{
		$res = $this->db->iQuery(
			sprintf(
				"SELECT * FROM brickwork_page WHERE id = %d LIMIT 1",
				$page_id
			)
		);

		if ($row = $res->first()) {
			return $row;
		}
		return false;
	}

	/**
	 * Creates a module based on a database result
	 *
	 * @param int $id
	 * @param string $class
	 * @param string $code
	 * @param string$name
	 * @param ContentContainer $cc
	 * @param string $template
	 * @param string $content_table
	 * @param string $config_table
	 * @param string $cachetime
	 * @param bool $respect_auth
	 * @return Webmodule
	 */
	private function createModule($id, $class, $code, $name, ContentContainer $cc,
		$template, $content_table, $config_table, $cachetime, $respect_auth)
	{
		$module = Webmodule::factory($class, $id, $cc, $code);

		// if we could not create the module, continue with the next one
		if(is_null($module)) {
			throw new Exception("Module could not be created by factory");
		}

		$module->setTemplate($template);
		$module->content_table = $content_table;
		$module->config_table = $config_table;
		$module->name = $name;
		$module->code = $code;
		$module->respect_auth = $respect_auth;
		$module->setCacheLifetime($cachetime);

		// load configuration
		if($module->config_table){
			$module->loadConfig();
		}

		if($module->isCachable()){
			$module->cacheParams = array(
				'get' => brickHash::get($module->get),
				'cookie' => brickHash::get($module->cookie),
				'post' => brickHash::get($module->post),
				'session' => brickHash::get($module->session),
				'config' => brickHash::get($module->config),
				'id' => (int)$module->id
			);

			$module->loadExtraCacheParams();
		}

		return $module;
	}

	/**
	 * Tells if the module should be shown
	 *
	 * @param ContentContainer $cc
	 * @param StructureItem $structure
	 * @param Webmodule $mod
	 * @return bool
	 */
	private function isModuleShown(ContentContainer $cc,
		StructureItem $structure, Webmodule $module)
	{
		// Module or structure doesn't care about authentication
		if(!$structure->secure || !$module->respect_auth) {
			return true;
		}

		if (!$cc->allowed) {
			return false;
		}

		$user = $this->auth->user;
		if ($this->acl && $structure->restricted &&
				$user instanceOf ACLUser) {
			return $user->isLoggedIn() && $structure->checkAccess($user) &&
					$this->acl->hasAccess($user, $module);
		}

		return $user->isLoggedIn() && $structure->checkAccess($user);
	}
}
