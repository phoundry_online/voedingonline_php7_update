<?php
/**
 * Wrapper on the Cache class
 *
 */
class ACLCache
{
	/**
	 * Cache Object
	 *
	 * @var Cache
	 */
	private $_cache;
	
	public function __construct( $site, $tmpDir = CACHE_DIR, $iTimeout = 3600 )
	{
		global $WEBprefs;
		
		if ( isset($WEBprefs['ACL.Cache.Target']) )
			$this->_cache = Cache::factory( sprintf( $WEBprefs['ACL.Cache.Target'], $site, $iTimeout ) );
		else
			$this->_cache = Cache::factory('file:///'.$site.'?timeout='.$iTimeout);
			
		if(!isset($this->_cache->acl)) {
			$this->_cache->acl = array();
		}
	}
	
	public function cached( $role, $object )
	{
		return isset($this->_cache->acl[ $role ][ $object ]);	
	}
	
	public function flush( $role = null , $object = null )
	{
		$this->_cache->flush();
		return true;
	}
	
	public function store( $role, $object, $grant )
	{
		// Otherwise we get errors about trying to change an overloaded property wich in fact ofc is true
		$acl = $this->_cache->acl;
		$acl[ $role ][ $object ] = $grant;
		$this->_cache->acl = $acl;
		
		$this->_altered = true;
		return true;
	}
	
	public function get( $role, $object )
	{
		if ( $this->cached($role,$object) ) {
			return $this->_cache->acl[ $role ][ $object ];
		}
		return null;
	}
}
