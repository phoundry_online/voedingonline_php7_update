<?php
/**
 * Access Control List Role
 * A user in the ACL list can have multiple roles. A role can even be a child of another
 * role if need be. This class will handle all role retrieval and role coupling for you
 * 
 * @author			Peter Eussen
 * @package		Brickwork
 * @subpackage		ACL
 * @version		1.0
 *
 */
class ACLRole
{

	private $_auth_name;

	private $_auth_id;

	function __construct($name = null, $id = null)
	{
		$this->_auth_name = $name;
		$this->_auth_id = $id;
		
		if(!is_null($id)) {
			$this->_loadById();			
		} else
		if(!is_null($name)) {
			$this->_loadByName();
		}
		
	}

	public function getId()
	{
		return $this->_auth_id;
	}

	/**
	 * Returns the name of the role
	 *
	 * @return              string
	 */
	public function getName()
	{
		return $this->_auth_name;
	}

	public function __toString()
	{
		return $this->getName();
	}
	
	/**
	 * Returns a list of all roles that were set as children of this role
	 * This method returns all objects that are set as children of this role. The retrieval works
	 * recursively so all underlying roles will be returned. At the moment the method will not
	 * filter the output. So it may be possible that the returning set has the same role twice or more.
	 *
	 * @return              Array
	 */
	public function getChildRoles(  )
	{
		$res = DB::iQuery('SELECT name FROM brickwork_auth_role WHERE parent_id = ' . (int) $this->_auth_id);
		
		$tmp = Array();
		
		if ( !self::isError( $res ) )
		{
			while ( $res->current())
			{
			    $row = $res->current();
				$new = new ACLRole($row->name);
				array_push($tmp, $new);
				$tmp = array_merge($tmp, $new->getChildRoles());
				$res->next();
			}
		}
		return $tmp;
	}

	/**
	 * Adds another role as child of this one
	 *
	 * @return boolean
	 */
	public function addChild(AppACLRole $oRole)
	{
		if ( is_null($oRole) )
			return true;
		
		return $oRole->setParent($this);
	}

	/**
	 * Sets a specific role as the parent of the current role
	 *
	 * @param ACLRole $oRole
	 * @return boolean
	 */
	public function setParent( ACLRole $oRole)
	{
		if ( is_null($oRole) )
			DB::iQuery('UPDATE brickwork_auth_role SET parent_id = NULL  WHERE id = ' . (int) $this->_auth_id);
		else
			DB::iQuery('UPDATE brickwork_auth_role SET parent_id = ' . (int) $oRole->getId() . ' WHERE id = ' . (int) $this->_auth_id);
		return true;
	}

	/**
	 * Retrieves role by id
	 *
	 * @return boolean
	 */
	private function _loadById()
	{
		$res = DB::iQuery( sprintf("SELECT name, parent_id FROM brickwork_auth_role WHERE id = '%s'", DB::quote($this->_auth_id) ) );

		if ( self::isError($res) )
			throw new ACLException( $res->errstr, $res->errno );
			
		$row = $res->first();
		
		if(is_null($row)) {
			throw new ACLException( sprintf('No such role: %s', $this->_auth_id) );
		} else {
			$this->_auth_name = $row->name;
		}
		return true;
	}
	
	/**
	 * Retrieves role information for a specific role (used on construction)
	 *
	 * @return boolean
	 */
	private function _loadByName()
	{
		$res = DB::iQuery( sprintf("SELECT id, parent_id FROM brickwork_auth_role WHERE name = '%s'", DB::quote($this->_auth_name) ) );

		if ( self::isError($res) )
			throw new ACLException( $res->errstr, $res->errno );
			
		$row = $res->first();
		
		if ( is_null($row) )
		{
			if ( ( isset($WEBprefs['acl']['autocreate']) && $WEBprefs['acl']['autocreate'] ) ||
				 ( isset($WEBprefs['ACL.AutoCreate']) && $WEBprefs['ACLAutoCreate'] )
			   )
			{
				$sql = sprintf("INSERT INTO brickwork_auth_role ( name ) VALUES('%s')", DB::quote( $this->_auth_name));
				
				$res = DB::iQuery($sql);
	
				if ( self::isError( $res )) {
					return false;
				}
					
				$this->_auth_id = $res->lastInsertId;
				return true;
			}
			throw new ACLException( sprintf('No such role: %s', $this->_auth_name) );
		} 
		else
		{
			$this->_auth_id = $row->id;
		}
		return true;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError( &$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }

}
