<?php

/**
 * Access Control List Object Interface
 * You can make any object an object that is compatible with the ACL restriction
 * model by implementing this interface. The interface is required for use of the
 * ACL.
 *
 * @author		Peter Eussen
 * @package	Brickwork
 * @subpackage	ACL
 * @version	1.0
 */
interface ACLObjectInterface
{
	/**
	 * Returns a unique instance ID. This should be a UNIQUE string
	 *
	 * @return	string
	 */
	public function getInstanceId();
	
	/**
	 * Returns a name for the Instance. This basically can be a descriptive text
	 * 
	 * @return string
	 */
	public function getInstanceName();
	
	/**
	 * Returns the parent ACL object for this object.
	 * You can determine yourself how you want to do this. But you should implement it!
	 * It should return an ACLObjectInterface implementing object.
	 *
	 * @return ACLObjectInterface
	 */
	public function getParent();
}

