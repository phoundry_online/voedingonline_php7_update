<?php
if (!class_exists('ACLRole')) {
	require_once('Role.class.php');
}

/**
 * Abstract Access Controlled User
 * This base class is used to check if someone has access to a specific object in Brickwork.
 * It is one of the base classes for all User (derived) classes, and is primarily used in 
 * conjunction with the SiteUser class.
 * AuthUser extends this class to ensure maximum compatibility.
 * 
 * @author		Peter Eussen
 * @package	Brickwork
 * @subpackage ACL
 * @version	1.0
 */
abstract class ACLUser
{

	private $_auth_name;

	private $_auth_roles;

	private $_auth_type;

	protected	$_auth_id;
	
	/**
	 * 
	 */
	function __construct($name = null)
	{
		$this->_auth_name = $name;
		$this->_auth_roles = null;
		$this->_auth_type = get_class($this);
		$this->_auth_id	  = 0;
	}

	public function getAuthName()
	{
		return $this->_auth_name;
	}

	public function getAuthId()
	{
		return $this->_auth_id;
	}
	
	/**
	 * Returns an Array of ACLRole objects which are assigned for this user
	 *
	 * @return Array
	 */
	public function getRoles()
	{
		if(!is_array($this->_auth_roles))
		{
			$this->_auth_roles = Array();
	
			$sql = sprintf('SELECT r.name ' . 
							 '  FROM brickwork_auth_role r ' . 
							 ' INNER JOIN brickwork_site_user_auth_role ur' . 
							 '    ON (ur.auth_role_id = r.id) ' . 
							 " WHERE ur.site_user_id = '%s'", DB::quote($this->_auth_id));
	
			$res = DB::iQuery($sql);
	
			if (self::isError($res) )
				return $this->_auth_roles;
				

			while ($res->current()) {
				$row = $res->current();
				$role = new ACLRole( $row->name );
				$this->_auth_roles[] = $role;
				$this->_auth_roles = array_merge($this->_auth_roles, $role->getChildRoles());
				$res->next();
			}
		}
		return $this->_auth_roles;
	
	}

	/**
	 * Adds a role for a specific user
	 *
	 * @param ACLRole $role
	 * @return boolean
	 */
	public function assignRole( ACLRole $role )
	{
		$sql = 'INSERT INTO brickwork_auth_user_role ( auth_role_id, auth_username ) VALUES(%s,%s)';
		
		$res = DB::iQuery( sprintf( $sql, DB::quote($role->getId()), DB::quote($this->_auth_name) ) );
		
		if ( self::isError($res))
			return false;
		return true;
	}

	/**
	 * Checks if a user has a specific role assigned to him
	 *
	 * @param ACLRole $currole
	 * @return boolean
	 */
	public function hasRole( ACLRole $currole )
	{
		if ( is_null($this->_auth_roles) )
			$this->getRoles();
		
		foreach ( $this->_auth_roles as $role )
		{
			if ( $role->getId() == $currole->getId() )
				return true;
		}
		return false;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }

}

?>
