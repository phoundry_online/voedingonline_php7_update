<?php
require_once('Exception.class.php');
require_once('Object.if.php');

class ACLObject implements ACLObjectInterface 
{

	private $_auth_id;

	private $_auth_parent_id;

	private $_auth_instance;
	
	private $_auth_class;
	
	private $_allow_create;
	
	private $_auth_description;
	
	/**
	 * Constructs the object
	 *
	 * @param string 		$instance		The name for this instance
	 * @param string|null 	$class			The class that is being used
	 * @param string|null 	$description	A description for the object
	 * @param boolean|null	$allowCreate	Indicator if we are allowed to create new objects
	 * @return void
	 */
	public function __construct($instance, $class = null, $description = null, $allowCreate = null )
	{
		global $WEBprefs;
		
		if ( is_null($class) )
			$this->_auth_class = get_class($this);
		else
			$this->_auth_class = $class;

		if ( is_null( $allowCreate ) ) 
		{
			if ( isset( $WEBprefs['ACL.AutoCreate']) )
				$this->_allow_create = $WEBprefs['ACL.AutoCreate'];
			else 
				$this->_allow_create = ( isset($WEBprefs['acl']['autocreate']) ? $WEBprefs['acl']['autocreate'] : false);
		}
		else
			$this->_allow_create = $allowCreate;
			
		$this->_auth_parent_id 	= null;
		$this->_auth_description= $description;
		
		if ( is_numeric($instance) )
		{
			$this->_auth_instance 	= null;
			$this->_auth_id 		= (int) $instance;
			$this->_loadByID();
		} else
		{
			$this->_auth_instance 	= $instance;
			$this->_auth_id 		= null;
			$this->_loadByInstance();
		}
	}

	/**
	 * Maakt een nieuwe instantie aan indien deze nog niet bestaat
	 *
	 * @param ACLObjectInterface 	$parent
	 * @param string 				$name
	 * @param string 				$description
	 * @return ACLObject
	 */
	static public function newInstance( $parent, $name, $description = '')
	{
		if ( is_null($parent) )
			$parentid = null;
		else
			$parentid = $parent->getInstanceId();
			
		if ( $description == '' )
			$description = (is_null($parent) ? $name : "$name on " . $parentid);
			
		$id 	= $name . '/' . $parentid;
		$child  = new ACLObject( $id, get_class($parent), $description, true );
		
		if ( !is_null($parent) && is_null($child->getParent() ) )
		{
			$tmp = new ACLObject( $parentid );
			$tmp->addChild( $child );
			
			// Refetch to make sure the parent ID is set
			$child = new ACLObject( $id, get_class($parent), $description );
		}
		return $child;
	}
	
	/**
	 * Returns the parent object for this authorisation object
	 *
	 * @return ACLObject
	 */
	public function getParent()
	{
		if ( is_null($this->_auth_parent_id) )
			return null;
		return new ACLObject($this->_auth_parent_id);
	}

	/**
	 * Returns the authorisation object ID for this object
	 *
	 * @return integer
	 */
	public function getInstanceId()
	{
		return $this->_auth_instance;
	}

	/**
	 * Returns the authorisation name for this object
	 *
	 * @return string
	 */
	public function getInstanceName()
	{
		return get_class($this);
	}

	/**
	 * Returns the Authentication ID for this object
	 *
	 * @return integer
	 */
	public function getAuthObjectId()
	{
		return $this->_auth_id;
	}
	
	/**
	 * Add's a ACLObject as a child of this one
	 *
	 * @param 	ACLObject $oObject
	 * @return 	boolean
	 * @throws	ACLException
	 */
	public function addChild(ACLObject $oObject)
	{
		$sql = sprintf('UPDATE brickwork_auth_object SET parent_id = %d WHERE id = %d', $this->_auth_id, $oObject->getAuthObjectId());
		$res = DB::iQuery($sql);
		
		if ( self::isError($res) )
		{
			throw new ACLException($res->errstr, $res->errno);
		}
		return true;
	}

	private function _loadByInstance()
	{
		global $WEBprefs;
		
		if ( (isset($WEBprefs['acl']['enable']) && $WEBprefs['acl']['enable'] === false) ||
			 (isset($WEBprefs['ACL.Enable']) && $WEBprefs['ACL.enable'] === false)
		   )
		{
			$this->_auth_id = $this->_auth_parent_id = null;
			return false;	
		}
		
		$sql = sprintf("SELECT id, parent_id FROM brickwork_auth_object WHERE instanceid = '%s'",
						DB::quote($this->_auth_instance));

		$res = DB::iQuery($sql);
		
		if ( self::isError($res) )
		{
			throw new ACLException( $res->errstr, $res->errno );
		}
		
		$row = $res->first();
		
		if ( $row === false )
		{
			if ( $this->_allow_create )
			{
				// Then insert
				$sql = sprintf("INSERT INTO brickwork_auth_object ( class, instanceid, description ) VALUES('%s','%s','%s')", 
								DB::quote( $this->_auth_class ), DB::quote( $this->_auth_instance), DB::quote($this->_auth_description) );
				
				$res = DB::iQuery($sql);
	
				if ( self::isError( $res ))
					throw new ACLException( $res->errstr, $res->errno );
				
				$this->_auth_id 		= $res->last_insert_id;
				$this->_auth_parent_id	= null;
			}
			else
			{
				$this->_auth_id 		= null;
				$this->_auth_parent_id	= null;
				logDebug( sprintf('No such object: %s', $this->_auth_instance) );
				//throw new ACLException( sprintf('No such object: %s', $this->_auth_instance) );
			}
		} 
		else
		{
			$this->_auth_id 		= $row->id;
			$this->_auth_parent_id	= $row->parent_id;
		}
		return true;
	}
	
	private function _loadById()
	{
		$sql = sprintf("SELECT id, parent_id, instanceid  FROM brickwork_auth_object WHERE id = '%s'",
						DB::quote($this->_auth_id));

		$res = DB::iQuery($sql);
		
		if ( self::isError($res) )
		{
			throw new ACLException( $res->errstr, $res->errno );
		}
		
		$row = $res->first();
		
		if ( $row !== false )
		{
			$this->_auth_id 		= $row->id;
			$this->_auth_instance	= $row->instanceid;
			$this->_auth_parent_id	= $row->parent_id;
			return true;
		}
		return false;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

?>
