<?php

/**
 * Organisation
 *
 * Organisation where the site belongs to.
 *
 * @author J�rgen Teunis <jorgen.teunis@webpower.nl>
 * @todo maybe fix this to a generic organisation object not depending on brickwork organisation table OR name BrickworkOrganisation
 * @access public
 */
class Organisation {

	public $id;
	public $name;
	public $street;
	public $house_nr;
	public $house_nr_add;
	public $city;
	public $country = NULL;
	public $phone_area_code;
	public $phone_number;
	public $email_info;
	public $email_privacy;
	public $email_support;

	public function __construct($id, $name){
		$this->id		= $id;
		$this->name		= $name;
	}

	/**
	 * Get brickwork organisation from DB
	 *
	 * @param integer $id
	 * @return Organisation
	 */
	static function get($id){
		$sql = sprintf('
			SELECT
				*
			FROM
				brickwork_organisation
			WHERE
				id = %d
			LIMIT 1
		',
			$id
		);


        try {
            $res = DB::iQuery($sql);
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return null;
        }

		if($res->first()){
			$res = $res->first();
			$organisation = new Organisation($res->id, $res->name);
			$organisation->street = $res->street;
			$organisation->house_nr = $res->house_nr;
			$organisation->house_nr_add = $res->house_nr_add;
			$organisation->city = $res->city;

			// edit 11/16/2006 Mick naar Country omdat Country veranderd is
			if (!empty($res->country))
				$organisation->country = new Country($res->country);

            $organisation->phone_area_code = $res->phone_area_code;
			$organisation->phone_number = $res->phone_number;

			if(!empty($res->email_info)){
				$organisation->email_info = new Email($res->email_info);
			}
			if(!empty($res->email_privacy)){
				$organisation->email_privacy = new Email($res->email_privacy);
			}

			if(!empty($res->email_support)){
				$organisation->email_support = new Email($res->email_support);
			}

			return $organisation;
		}
		return NULL;
	}
}

?>
