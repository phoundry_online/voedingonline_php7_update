<?php
final class News
{
	const RECENT = 1;
	const ARCHIVED = 2;
	const MIXED = 3;
	
	public static $article_class	= 'News_Article';
	public static $category_class	= 'News_Category';
	public static $reaction_class	= 'News_Reaction';
	
	/**
	 * Get a category using the given category_class
	 *
	 * @param int $id
	 * @param bool $load
	 * @return News_Category
	 */
	public static function categoryFactory($id = 0, $load = true)
	{
		return ActiveRecord::factory(self::$category_class, $id, $load);
	}
	
	/**
	 * Get a article using the given article_class
	 *
	 * @param int $id
	 * @param bool $load
	 * @return News_Article
	 */
	public static function articleFactory($id = 0, $load = true)
	{
		return ActiveRecord::factory(self::$article_class, $id, $load);
	}
}
