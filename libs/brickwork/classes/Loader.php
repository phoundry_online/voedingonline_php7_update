<?php
/**
 * Brickwork Loader
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @since 2009-08-19
 */
class Loader
{
	/**
	 * Singleton instance
	 * @var Loader
	 */
	protected static $_instance;

	/**
	 * Registered loaders
	 * @var array
	 */
	protected $_loaders;

	protected function __construct()
	{
		$this->_loaders = array();
	}
	
	/**
	 * Return a singleton instance of itself
	 * @return Loader
	 */
	public static function getInstance()
	{
		if(null === self::$_instance)
			self::$_instance = new self();
		
		return self::$_instance;
	}
	
	/**
	 * Find the given resource trying the registered loaders
	 * @param string $resource
	 * @return string|bool
	 */
	public function find($resource)
	{
		foreach($this->_loaders as $loader) {
			if($loader instanceof Loader_Interface 
			&& ($file = $loader->find($resource))) {
				return $file;
			}
		}
		return false;
	}
	
	/**
	 * Add a new loader
	 * @param string $name
	 * @param Loader_Interface $loader
	 * @return Loader self
	 */
	public function addLoader($name, Loader_Interface $loader)
	{
		$this->_loaders[$name] = $loader;
		return $this;
	}
	
	/**
	 * Remove a registered loader
	 * @param string $name
	 * @return bool
	 */
	public function removeLoader($name)
	{
		if(key_exists($name, $this->_loaders)) {
			unset($this->_loaders[$name]);
			return true;
		}
		return false;
	}
	
	/**
	 * Get a registered loader by its name
	 * @param string $name
	 * @return Loader_Interface
	 */
	public function getLoader($name)
	{
		return key_exists($name, $this->_loaders) ? $this->_loaders[$name] : false;
	}
}
