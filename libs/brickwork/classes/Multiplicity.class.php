<?php
/**
* Multiplicity
*
* Deze class herbergt een aantal functies om makkelijk multipliciteit mee te
* controleren.
* Multiplicity is: + (groter dan 1), * (0 of meer) of een getal
*
* @author Mick van der Most van Spijk <mick@webpower.nl>
* @copyright 2006 Web Power b.v.
* @version 1.0.0
* @last_modified 12/5/2006
*/
class Multiplicity {
	/**
	* Check if a value is greater than the multiplicity
	*
	* @param m multiplicity to check
	* @param v value to check againts multiplicity
	* @access public
	* @static
	*/
	public static function greater($m, $v) {
		// multiplicity is bigger than anything
		if ($m == '*' || $m == '+') {
			return TRUE;
		}

		// bigger than value
		if ($m > $v) {
			return TRUE;
		}

		// not bigger
		return FALSE;
	}

	/**
	* Check if a value is less than a multiplicity
	*
	* @param m multiplicity to check
	* @param v value to check against multiplicity
	* @access public
	* @static
	*/
	public static function less($m, $v) {
		// bigger than zero, never less
		if ($m == '*')
			return FALSE;

		// only less if less than 1
		if ($m == '+') {
			if ($v > 1) {
				return FALSE;
			} else {
				return TRUE;
			}
		}

		// less if less
		if ($m < $v) {
			return TRUE;
		}

		// not less
		return FALSE;
	}
}
?>
