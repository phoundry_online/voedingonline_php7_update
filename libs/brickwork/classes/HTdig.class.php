<?php
/*
 * For info about tags available for use in FORM's:
 * http://www.htdig.org/hts_form.html
 *
 * For info about search templates:
 * http://www.htdig.org/hts_templates.html
 *
 * For info about configuration options:
 * http://www.htdig.org/attrs.html
 *
 * In order to skip indexing some parts of your documents,
 * use <!--htdig_noindex--> and <!--/htdig_noindex-->.
 *
 */

//define('HTSEARCH', );

/*
* HTdigResult
*/
class HTdigResult {

	/*
	*
	*/
	public $index;

 
	/*
	*
	*/
	public $url;

	/*
	* @var string
	* @acces public
	*/
	public $title = '';

	/*
	* @var
	* @acces public
	*/
	public $modified;

	/*
	*
	*/
	public $size;

	/*
	*
	*/
	public $score;

	/*
	*
	*/
	public $percent;

	/*
	*
	*/
	public $excerpt;

	/*
	* Contructor
	*/
	public function __construct($index, $result) {

		$this->index = $index;

		$tags = array('url', 'title', 'modified', 'size', 'score', 'percent', 'excerpt');

		
		foreach ($tags as $tag) {
			$tagU = strtoupper($tag);
			if (preg_match("'<$tagU>(.*)</$tagU>'s", $result, $reg))
			{
				if('excerpt' == $tag)
				{
					$this->{$tag} = trim(str_replace('*', '', $reg[1])); 
				}
				else
				{
					$this->{$tag} = trim($reg[1]);
				}
			}
		}
		
	}
}

/*
* HTdig
*/
class HTdig {

	/*
	*
	*/
	public $first_match	=	0;

	/*
	*
	*/
	public $last_match	=	0;

	/*
	*
	*/
	public $args		= array();

	/*
	*
	*/
	public $config		= 'daily';

	/*
	* @var string
	* @acces public
	*/
	public $method = 'and';		// and, boolean, or

	/*
	* @var string
	* @acces public
	*/
	public $sort = 'score';

	/*
	* @var integer
	* @acces public
	*/
	public $matchesperpage = 10;

	/*
	*
	*/
	public $restrict;

	/*
	*
	*/
	public $exclude;

	/*
	*
	*/
	protected $syntaxerror	=	FALSE;

	/*
	*
	*/
	public $matches;

	/*
	*
	*/
	public $nr_pages;

	/*
	*
	*/
	public $cur_page;

	/*
	*
	*/
	public $words			= '';

	/*
	*
	*/
	public $results		= array();
	static protected $HTSEARCH;
	
	/*
	* Constructor
	*
	* @param config			string
	* @param method			string
	* @param sort				string
	* @param matcherperpage integer
	* @param restrict
	* @param exlude
	*
	* @acces public
	*/
	public function __construct($config = '', $method='and', $matchesperpage=2, $sort='score',  $restrict='', $exclude='') {

		
		// get htsearch binary
		if(isset($GLOBALS['WEBprefs']['htsearch_bin'])){
			self::$HTSEARCH = $GLOBALS['WEBprefs']['htsearch_bin'];
		} else {
			self::$HTSEARCH = '/usr/local/bin/htsearch';
		}
		

		$this->config				= $config;
		$this->method				= $method;
		$this->sort					= $sort;
		$this->matchesperpage	= $matchesperpage;
		$this->restrict			= $restrict;
		$this->exclude				= $exclude;
		
		$this->args['config'] = $this->config;
		$this->args['method'] = $this->method;
		$this->args['sort'] = $this->sort;
		$this->args['matchesperpage'] = $this->matchesperpage;
		$this->args['restrict'] = $this->restrict;
		$this->args['exclude'] = $this->exclude;
		$this->args['words'] = '';
		$this->args['page'] = 1;
	}

	/*
	* init_header
	* @param String
	* @acces protected
	*/
	protected function init_header($header) {
		$tags = array('matches', 'nr_pages', 'cur_page', 'words', 'syntaxerror');
		foreach ($tags as $tag) {
			$tagU = strtoupper($tag);
			if (preg_match("'<$tagU>(.*)</$tagU>'s", $header, $reg))
				$this->$tag = $reg[1];
		}
	}

	/*
	* getResults
	* @param String
	* @acces protected
	*/
	protected function getResults($results_str) {
		$results = array();

		if (preg_match_all("'<RESULT NR=\"([0-9]+)\">(.*?)</RESULT>'s", $results_str, $reg)) {
			for ($x = 0; $x < count($reg[1]); $x++) {
				if ($x == 0){
					$this->first_match = $reg[1][$x];
				}

				$results[] = new HTdigResult($reg[1][$x],$reg[2][$x]);
				if ($x == count($reg[1])-1)
					$this->last_match = $reg[1][$x];
			}
		}
		
		return $results;
	}

	/*
	* getQS
	* @param
	* @acces public
	*/
	public function getQSarray($page) {
		$args = array();
		$this->args['page'] = $page;

		foreach($this->args as $name=>$value) {
			if ($name == 'config' || empty($value))
				continue;
			$args[$name]=$value;
		}
		return $args;
	}

	/*
	* getQS
	* @param
	* @acces public
	*/
	public function getQS($page) {

		$this->args['page'] = $page;
		$args = '';	$x = 0;
		foreach($this->args as $name=>$value) {
			if ($name == 'config' || empty($value))
				continue;
			if ($x++ != 0)	$args .= '&amp;';
			$args .=  $name . '=' . urlencode(stripslashes($value));
		}
		return $args;
	}

	/*
	* doDig
	* @param
	* @acces public
	*/
	public function doDig($words, $page=NULL) {
		$this->words = $words;
		$this->args['page'] = (is_null($page)) ? 1 : $page;
		$this->args['words'] = $this->words;

		$args = '';	$x = 0;
		foreach($this->args as $name=>$value) {
			if ($x++ != 0)	$args .= ';';
			$args .= $name . '=' . escapeshellcmd($value);
		}
		//$command = HTSEARCH." \"format=php;$args\"";
		$command = self::$HTSEARCH." -c ".getenv('CONFIG_DIR')."/htdig.{$this->config}.conf \"format=php;$args\"";
		$cnt = `$command`;
		
		$retval = NULL;

		$cnt = preg_replace("'Content-type: text/html\r\n\r\n'", '', $cnt);
		if (strstr($cnt, '<NOMATCH>')){
			$this->matches = 0;
		} elseif (strstr($cnt, '<SYNTAXERROR>')) {
			$this->matches = 0;
			$this->syntaxerror = TRUE;
			// syntax error
			$retval = new ErrorVoedingonline(E_USER_NOTICE, 'HTdig syntax error', __FILE__, __LINE__);
		}
		elseif (preg_match('/Unable to read configuration file/i', $cnt))
		{

			// configuratie error
			$retval = new ErrorVoedingonline(E_USER_WARNING, 'HTdig config error', __FILE__, __LINE__);
		}
		else
		{
			if (preg_match("'<HEADER>(.*)</HEADER>'s", $cnt, $reg)){
				$this->init_header($reg[1]);
			}
			
			if (preg_match("'<RESULTSET>(.*)</RESULTSET>'s", $cnt, $reg)){
				$retval = $this->getResults($reg[1]);
			}
		}

		return $retval;
	}
}
