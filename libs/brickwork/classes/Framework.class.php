<?php

/**
* Representation interface
*
* @author Jorgen Teunis
* @last_modified 2007-09-11
* @package Brickwork
*/
interface Representation {
	public function out();
}

/**
* Framework
*
* @author Jorgen Teunis
* @author Mick van der Most van Spijk (MMS)
* @last_modified 19-7-2007
* @package Framework
* @changes:
* - Caching van sitehostprut uitgeschakeld in testmode
*/
class Framework {
	/**
	 * Site
	 * @var Site
	 */
	public static $site				   = NULL;

	/**
	* contentHandler
	*
	* Handles the content generation. For example: PageContentHandler, MailingContentHandler, AjaxContentHandler.
	*
	* @var ContentHandler
	*/
	public static $contentHandler	   = NULL;

	/**
	* clientInfo
	* @var Client_Info
	*/
	public static $clientInfo		   = NULL;

	/**
	* Representation
	* @var Representation
	*/
	public static $representation	   = NULL;

	/**
	* siteHostAliasTable
	*
	* @var Array
	*/
	public static $SiteHostAliasTable = NULL;

	/**
	* PHprefs
	* @var Array
	*/
	public static $PHprefs = array();

	/**
	* WEBprefs
	* @var Array
	*/
	public static $WEBprefs = array();


	/**
	 * Session object
	 *
	 * @var Session
	 */
	public static 	$session = null;

	/**
	 * Access Control List methods
	 *
	 * @var ACL
	 */
	public static $acl;

	/**
	 * Prepare the session
	 * @return bool
	 */
	public static function startSession()
	{
		if (!is_object(self::$session)) {
			self::$session = Session_Manager::factory();
			self::$session->start();
		}
		return true;
	}

	/**
	 * Haal een enkel PHpref op uit de PHprefs lijst
	 *
	 * @deprecated
	 * @param String
	 * @return Mixed || FALSE
	 */
	public static function getPHprefs($key) {
		if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
			trigger_error("Usage of Framework::getPHprefs is deprecated", E_USER_ERROR);
		}
		if (array_key_exists($key, self::$PHprefs)){
			return self::$PHprefs[$key];
		}
		return FALSE;
	}

	/**
	* loadContentHandler
	*
	* @param String path
	* @param String query string
	* @return Boolean
	*/
	public static function loadContentHandler($path, $query){
		if(!empty($path)){
			$path = explode("/", $path);
			unset($path[0]);

			// trailing slash nu genegeerd dd 10/20/2006 Mick
			$_content_handler_name = (isset($path[1]) && (!empty($path[1]))) ? $path[1] : 'page';
			unset($path[1]);
		} else {
			$path = array();
			// default page
			$_content_handler_name = 'page';
		}

		// zorgen dat het netjes bij 0 begint
		$new_path = array_merge($path, array());

		$contentHandler = ContentHandler::factory($_content_handler_name, $new_path, $query);

		if(self::isError($contentHandler)){
			// set 404 in web environment
			if(Framework::$representation instanceof WebRepresentation){
				header('HTTP/1.1 404 Not found');
			}
			if(TESTMODE){
				$errHandler = ErrorHandler::singleton();
				$errHandler->log(new ErrorVoedingonline(
					E_USER_NOTICE,
					sprintf('Content Handler %s not found', $_content_handler_name),
					__FILE__,
					__LINE__
				));
			} else {
				die(sprintf('Content Handler %s not found', $_content_handler_name));
			}
			exit;
		}

		if (!is_null($contentHandler)){

			self::$contentHandler = $contentHandler;

			// PE: Load locales
			if ( !is_null(Framework::$site->defaultLang) )
			{
				$curLang = self::$contentHandler->determineLanguage();
				self::$site->setLanguage($curLang);
				self::$representation->compile_id = $curLang->code;
			}
			// start initialization
			self::$contentHandler->init();
			return TRUE;

		// contenthandler doesnt exist
		}


		return FALSE;
	}

	/**
	* loadClientInfo
	*
	* @param String type
	* @return Boolean
	*/
	public static function loadClientInfo($type){
		switch($type){
			case 'cli':
			case 'web':
				self::$clientInfo = Client_Info::factory($type, $_SERVER);
				break;

			default:
				trigger_error(sprintf('Client %s not yet supported by this framework', $type), E_USER_ERROR);
		}

		return (!is_null(self::$clientInfo));
	}

	/**
	* loadRepresentation
	*
	* @param String type
	* @return Boolean
	*/
	public static function loadRepresentation($type){

		switch($type){
			case 'web':
				self::$representation = new WebRepresentation(
					Framework::$site->template,
					Framework::$site->locale,
					self::$clientInfo
				);
				break;

			default:
				trigger_error(sprintf('Client %s not yet supported by this framework', $type), E_USER_ERROR);
		}

		// Version numbering
		self::$representation->assign(
			'SITEVERSION',
			trim(@file_get_contents(CUSTOM_INCLUDE_DIR .'VERSION'))
		);

		self::$representation->assign('BRICKWORKVERSION', self::version());
		self::$representation->assign('PHprefs', $GLOBALS['PHprefs']);
		self::$representation->assign('WEBprefs', $GLOBALS['WEBprefs']);

		return (!is_null(self::$clientInfo));
	}

	/**
	* loadSiteByHost
	*
	* @param String host
	* @param Integer port
	* @return Boolean
	*/
	public static function loadSiteByHost($host, $port){
		// remove port number from host
		$host = preg_replace('/(.*):\d+$/','\1', $host);
		$host = trim($host,"\n\r\\.:");

		$cache_key = 'site_'.$port.'_'.escapeshellcmd($host);
		$cache = Cache::factory('dir:///site_host');

		$site = $cache[$cache_key];

		if (is_null($site))
		{
			$site = self::getSiteByHost($host, $port);

			if (is_null($site))
			{
				$site = self::getSiteByAlias($host, $port);
			}

			// Cache the site
			$cache[$cache_key] =  $site;
		}

		if(isset($site->defaultLang)) {
			$GLOBALS['translations'] = Translation::getBySiteLanguage( $site->defaultLang );
		}

		if(is_object($site)){
			self::$site = $site;
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * loadSiteByIdentifier
	 *
	 * Load site by given identifier.
	 *
	 * @param String site_identifier
	 * @return Boolean
	 */
	public static function loadSiteByIdentifier($identifier)
	{
		$site = self::getSiteByIdentifier($identifier);

		if(isset($site->defaultLang)) {
		    $GLOBALS['translations'] = Translation::getBySiteLanguage( $site->defaultLang );
		}

		if(is_object($site)){
		    self::$site = $site;
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Get the site by host and port
	 *
	 * @param String host
	 * @param String port
	 * @return Site || NULL
	 */
	public static function getSiteByHost($host, $port){
		// determine http protocol
		$protocol =  ($port == '443') ? 'https' : 'http';
		$host = strtolower($host);

		$sql = sprintf('
			SELECT
				s.*
			FROM
				brickwork_site s
			WHERE
				LOWER(domain) = "%s"
			AND
				FIND_IN_SET("%s", protocol)
			LIMIT 1
		',
			// values
			DB::quote($host),
			DB::quote($protocol)
		);

		$res = DB::iQuery($sql);
		if (self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);
		}

		return self::getSiteByDbresult($res);
	}

	/**
	 * Haal een site op aan de hand van zijn alias
	 *
	 * @param String alias
	 * @param Integer port
	 */
	public static function getSiteByAlias($alias, $port){
		// remove portnumber, trailing stuff and make lowercase
		$host = trim(preg_replace('/(.*):\d+$/','\1', strtolower($alias)), "\n\r\\.:");
		$protocol =  ($port == '443') ? 'https' : 'http';

		// Fill the self::$SiteHostAliasTable array
		if (self::loadHostAliasTable($port))
		{
			$site_info = null;

			if(isset(self::$SiteHostAliasTable[$alias]))
			{
				$site_info = self::$SiteHostAliasTable[$alias];
			}
			else
			{
			    foreach( self::$SiteHostAliasTable as $alias => $entry )
				{
				 	if ( preg_match('/' . str_replace('*','.*',$alias) . '/', $host))
					{
						$site_info = $entry;
						break;
					}
				}
			}

			if(!is_null($site_info))
			{
				if($site_info->redirect)
				{
					// check if current requested protocol is available in site
					// then redirect to this protocol, otherwise redirect to first protocol from list.
					$protocols = explode(',', $site_info->protocol);
					if(!in_array($protocol, $protocols)){
						$protocol = first($protocols);
					}
					if(empty($protocol)){
						$protocol = 'http';
					}

					trigger_redirect(sprintf(
						'%s://%s%s',
						$protocol,
						$site_info->domain,
                        $_SERVER['REQUEST_URI']
					));
				}

				return self::getSiteByIdentifier($site_info->site_identifier,
					$site_info->default_lang_id);
			}
		}

		return null;
	}

	/**
	 * Gets a site by its identifier
	 *
	 * @param String $identifier
	 * @return Site|void
	 */
	public static function getSiteByIdentifier($identifier, $lang_id = null)
	{
		$sql = sprintf('
			SELECT
				s.*
			FROM
				brickwork_site s
			WHERE
				identifier = "%s"
			LIMIT 1
		',
			// values
			DB::quote($identifier)
		);

		$res = DB::iQuery($sql);

		return self::getSiteByDbresult($res, $lang_id);
	}

	/**
	 * Turns a database result in a Site object
	 * used by getSiteByIdentifier and getSiteByHost
	 * @param $res
	 * @return Site|void
	 */
	protected static function getSiteByDbresult($res, $lang_id = null)
	{
		// een site gevonden
		if($row = $res->first())
		{
			// indien er een organisatie bekend is, deze laden
			if (!empty($row->organisation_id)){
				$organisation = Organisation::get($row->organisation_id);
			}

			// nwe site
			$site = new Site($row->identifier, $row->domain);
			$site->protocol = $row->protocol;

			// organisatie koppelen
			if (isset($organisation)){
				$site->organisation = $organisation;
			}

			// set template.
			//$site->template = $row->template;
			$site->name = $row->name;
			$site->auth_user_class = $row->auth_user;
			$site->test = ($row->status == 'test') ? TRUE : FALSE;
			$site->setTemplate( $row->template );

			if(!$lang_id && isset($row->default_lang_id)) {
				$lang_id = $row->default_lang_id;
			}

			// Backward compatibility check met 1.3.x (geen default_locale
			if($lang_id) {
				$site->availableLang = Site_Language::getAllSiteLanguages(
					$site->identifier);

				if(isset($site->availableLang[$lang_id])) {
					$site->defaultLang 	= $site->availableLang[$lang_id];
				}
				else {
					throw new Language_Exception(sprintf(
						"Default language (%s) is not part of the available ".
						"languages for this site (%s).",
						$lang_id, $site->identifier));
				}
			}
			else {
				$site->defaultLang = null;
			}

			return $site;
		}

		// niets gevonden
		return NULL;
	}

	/**
	* loadHostAliasTable
	*
	* Load host by a host alias and redirect or show site according to settings
	*
	* @param Integer port
	* @return Boolean
	*/
	public static function loadHostAliasTable($port){
		$protocol =  ($port == '443') ? 'https' : 'http';

		$cache = Cache::factory('file:///host_alias_table_'.$port);

		if(isset($cache['site_host_alias_table']) && is_array($cache['site_host_alias_table']))
		{
			$SiteHostAliasTable = $cache['site_host_alias_table'];

		}
		else
		{
			$sql = sprintf('
				SELECT
					sha.host,
					s.protocol,
					LOWER(s.domain) AS domain,
					s.identifier AS site_identifier,
					sha.protocol AS alias_protocol,
					sha.redirect,
					sha.default_lang_id
				FROM
					brickwork_site_host_alias sha
				INNER JOIN
					brickwork_site s ON s.identifier = sha.site_identifier
				WHERE
					FIND_IN_SET("%1$s", sha.protocol)
				ORDER BY
					sha.prio DESC, id ASC
			',
				// values
				DB::quote($protocol)
			);

			$res = DB::iQuery($sql);
			if (self::isError($res)){
				trigger_error($res->errstr, E_USER_ERROR);
			}

			$SiteHostAliasTable = array();
			if($res->first()) {
			    while ($res->current()) {
                    $host = $res->current();
					$SiteHostAliasTable[$host->host] = $host;
				    $res->next();
			    }
				// write site to cachefile
				$cache['site_host_alias_table'] = $SiteHostAliasTable;
			}
		}

		self::$SiteHostAliasTable = $SiteHostAliasTable;
		return TRUE;
	}

	/**
	 * Returns the version number based on the subversion location/revision
	 *
	 * @return string
	 */
	public static function version()
	{
		return '1.8.48';
	}

	/**
	 * Determines the language to use based on the set cookie or tries to
	 * determine what language to used based on the visitors browser languages
	 *
	 * @return Site_Language|bool
	 */
	public static function determineLanguage()
	{
		$c = Cookie::factory('preferences' );
		$l = false;

		if (!isset($c->l)) {
			if (self::$clientInfo) {
				foreach (self::$clientInfo->http_accept_language as $cl) {
					$cl = explode(';', $cl);
					$l  = self::$site->hasLanguage($cl[0]);

					if (is_object($l) && $l instanceof Site_Language &&
						$l->userSelectable === true) {
						break;
					}
					$l = false;
				}
			}

			if (!$l && self::$site && self::$site->defaultLang) {
				$l = self::$site->defaultLang;
			}
			$c->l = $l ? $l : false;
			$c->update();
		}
		return $c->l;
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
