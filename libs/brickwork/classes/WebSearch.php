<?php
/**
 * Search wrapper which takes any WebSearch_Engine and handles a search request.
 *
 * @author Adrie den Hartog <adrie.denhartog@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 * @version 1.0.0
 */
class WebSearch
{
	/**
	 * @var WebSearch_Engine Search engine
	 */
	protected $_search_engine;

	/**
	 * Takes a search engine as a construction param
	 *
	 * @param WebSearch_Engine
	 */
	public function __construct(WebSearch_Engine $search_engine)
	{
		$this->_search_engine = $search_engine;
	}

	/**
	 * Pass along the search request to the given search engine
	 *
	 * @param WebSearch_Query
	 * @return WebSearch_ResultSet
	 */
	public function search(WebSearch_Query $search_query) 
	{
		return $this->_search_engine->search($search_query);
	}
}