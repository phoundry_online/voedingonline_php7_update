<?


class HTTP404_ReadableUrl {


	public function __construct($path, $query){

	}

	public function fire(){
		if(!empty($_SERVER['REQUEST_URI'])){
			$subdir = substr($_SERVER['REQUEST_URI'],1);
		
		if(preg_match('/^([a-z0-9_\/|-]+)\?{0,1}(.*)$/i', $subdir, $matches)>0){
		
				$search	= !empty($matches[1]) ? $matches[1] : '';
				$search  = trim($search,'/');
				$query	= !empty($matches[2]) ? '?'.$matches[2] : '';
		
				$sql = sprintf('
								SELECT site_structure_id FROM brickwork_readable_links WHERE site_identifier = "%s" AND subdir = "%s"
								',
								DB::quote(Framework::$site->identifier),
								DB::quote($search)
								);

				$res = DB::iQuery($sql);

				// @todo indien meertalig naar de juiste page redirecten
				if($res->result){
					header('Status: 302 Moved');
					header('Location: /page/'.SiteStructure::getPathByStructureId($res->result[0]->site_structure_id).$query);
					exit;
				}
		
			}
		}
		return false;		
	}
}
