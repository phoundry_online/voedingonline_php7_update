<?

// @todo, fixen incl page contenthandler zodat pagina's geladen kunnen worden zonder de stuff eromheen (mogelijk?)
class HTTP404_Template404 extends PageContentHandler {

	public function fire(){
                // rechtstreeks uit page contenthandler
                // Assign Basic brickwork variables to smarty template
                Framework::$representation->assign('SITE', Framework::$site);
                Framework::$representation->assign('PAGE', $this);

                if ( isset(Framework::$clientInfo)){
                        Framework::$representation->assign('CLIENT', Framework::$clientInfo);
                }
                Framework::$representation->assign('STRUCTURE', NULL );
                Framework::$representation->assign('SITEVERSION', trim(@file_get_contents(CUSTOM_INCLUDE_DIR.'/VERSION')));
	
		if(Framework::$representation->setTemplate('404.tpl')){
			Framework::$representation->out();
			exit;
		}

		return false;		
	}
}
