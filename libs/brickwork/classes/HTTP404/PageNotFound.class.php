<?php


class HTTP404_PageNotFound {


	public function __construct($path, $query){

	}

        /**
         * Generates and output a 404 page
         * this function generates a templated based 404 page located in the templates base dir
         * for example: /templates/example_template/html/404.tpl
         * If no custom 404.tpl found, then it will print a simple 404 Page not found message
         *
         */
        public function print404(){
                // rechtstreeks uit page contenthandler
		// Assign Basic brickwork variables to smarty template
                Framework::$representation->assign('SITE', Framework::$site);
                Framework::$representation->assign('PAGE', $this);

                if ( isset(Framework::$clientInfo)){
                        Framework::$representation->assign('CLIENT', Framework::$clientInfo);
                }
                Framework::$representation->assign('STRUCTURE', $pageStructureItem );
                $this->structure = $pageStructureItem;
                Framework::$representation->assign('SITEVERSION', trim(@file_get_contents(CUSTOM_INCLUDE_DIR.'/VERSION')));

		// print 404 vanuit page content handler
				
                // make representation aware of the page type template again
                Framework::$representation->cache_lifetime = -1;
                Framework::$representation->caching = false;

                # log error to project tmp/error.log
                error_log(sprintf("[%s] [error] [client %s] File not found: %s%s\n", date("D, d M Y H:i:s T"), $_SERVER['REMOTE_ADDR'], $_SERVER['REQUEST_URI'], !empty($_SERVER['HTTP_REFERER']) ? ' Referer: '.$_SERVER['HTTP_REFERER'] : '' ), 3, TMP_DIR.'/error.log');
                header('HTTP/1.1 404 Not found');

                // if 404 found, then try to load structure item '404 Not found' otherwise load 404.tpl, if not exists print 404 message
                $loaded = false;
                $pageStructureItem = SiteStructure::getStructureByPath(array('404-Not-found'));
                if(!is_null($pageStructureItem)){
                        $loaded = $this->loadPageByStructureId($pageStructureItem->site_structure_id);
                }
                if($loaded){
                        $this->out();
			return true;
                }
		return false;
        }

	public function fire(){
	return false;
		if($this->print404()){
			exit;
		}

		return false;		
	}
}
