<?php
require_once INCLUDE_DIR.'/classes/Menu.package.php';

class BreadCrumb extends Menu
{
	public function __construct ($title, $link='', $description='', $nivo=0)
	{
		parent::__construct ($title, $link='', $description='', $nivo=0);
	}
	
	static function getBreadCrumb(Site $site, $active_structure_id)
	{
		
		$retval = array();
		
		// Load Site stack
		$stack = SiteStructure::getSiteStack(Framework::$site->identifier);
		
		// We need some stack
		if(count($stack) == 0)
		{
			return $retval;
		}
		
		$breadStack = array();
		
		if(isset($stack[$active_structure_id]))
		{
			$stackItem = $stack[$active_structure_id];
			
			while($stackItem)
			{
				$breadStack[$stackItem->id] = $stackItem;
				if(empty($stackItem->parent)) break;
				$stackItem = $stack[$stackItem->parent];
			}
		}
		
		// collect all structure id's and determine whats connected
		$structure_ids = array();
		foreach($breadStack as $item)
		{
			$structure_ids[] = (int)$item->id;
		}
		
		// Get pages
		$pages = SiteStructure::getPages($structure_ids);
		
		// Get urls
		$urls  = SiteStructure::getUrls($structure_ids);
		
		// Create shorcuts array
		$menu_shortcuts = array();
		
		// Creat menu array
		$menu = array();
		
		// Create invisible parent array
		$invisible_parents = array();
		
		// Loop trough the stack and add valid items
		foreach($stack as $item)
		{
			// Check for url id and page id
			if(!isset($urls[$item->id]) && !isset($pages[$item->id]))
			{
				continue;
			}
			
			// Create the url
			if(!empty($pages[$item->id]))
			{
				$langprefix = (Framework::$site->isMultiLingual() ? Framework::$site->currentLang->code . '/' : '' );
				
				if(defined('BRICKWORK_BEAUTIFUL_URL') && BRICKWORK_BEAUTIFUL_URL === TRUE)
				{
					$url = '/page/'. $langprefix  . $item->path;
				}
				else
				{
					$url = '/index.php/page/'. $langprefix  . $item->id;
				}
				$target = '_self';
			}
			else if(!empty($urls[$item->id]))
			{
				$url = $urls[$item->id]['link'];
				$target = $urls[$item->id]['target'];
			}
			
			// Create menu object
			if(empty($item->parent))
			{
				$m = new Menu($item->name,$url,$item->name,0);
				$m->id = $item->id;
				$m->target = $target;
				$m->selected = $item->isActive;
				$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
				$m->structure = $item;
				$menu[$item->id] = $m;
				$menu_shortcuts[$item->id] = $m;
			}
			else
			{
				$m = new Menu($item->name,$url,$item->name,1);
				$m->id = $item->id;
				$m->target = $target;
				$m->selected = $item->isActive;
				$m->parent = isset($menu_shortcuts[$item->parent]) ? $menu_shortcuts[$item->parent] : NULL;
				$m->structure = $item;
				if(isset($menu_shortcuts[$item->parent]))
				{
					$menu_shortcuts[$item->parent]->items[] = $m;
				}
				$menu_shortcuts[$item->id] = $m;
			}
		}
		
		if(isset($menu_shortcuts[$active_structure_id]))
		{
			$structure_ids = array();
			
			$_foo = $stack[$active_structure_id];
			
			$safety_counter = 0;
			while (true)
			{
				$structure_ids[$_foo->id] = $_foo;
				if(empty($stack[$_foo->parent]) || $safety_counter>90)
				{
					break;
				}
				$_foo = $stack[$_foo->parent];
				$safety_counter++;
			}
			
			$structure_ids = array_reverse($structure_ids, TRUE);
			
			$breadcrumbs = array();
			
			foreach($structure_ids as $id => $item)
			{
				if(isset($menu_shortcuts[$item->id]))
				{
					$breadcrumbs[$item->id] = $menu_shortcuts[$item->id];
				}
			}
			$retval = $breadcrumbs;
		}
		return $retval;
	}
}
