<?php
/**
 * Feature Check
 * Checks which features are available for the system. Now improved with extendable structure.
 * You can now add feature checks through Webprefs['Features']. You can either set a method/function
 * which will perform the actual check, or you can just say if the feature is available.
 * 
 * You can now also set the feature on runtime using the setXXXFeature methods
 * 
 * @author 	Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @author	Peter Eussen <peter.eussen@webpower.nl>
 * @version	1.1
 * @package	Brickwork
 * @subpackage	Features
 */
class Features
{
	protected static $one_cache = array();
	
	/**
	 * Caching for the features
	 *
	 * @var Cache_File
	 */
	protected static $cache = null;
	
	/**
	 * Check for a brickwork feature
	 *
	 * @param string $name
	 * @return bool
	 */
	public static function brickwork($name)
	{
		return self::featureCheck('brickwork', $name);
	}
	
	/**
	 * Check for a phoundry feature
	 *
	 * @param string $name
	 * @return bool
	 */
	public static function phoundry($name)
	{
		return self::featureCheck('phoundry', $name);
	}
	
	/**
	 * Check for a DMdelivery feature
	 * @param string $name
	 * @return bool
	 */
	public static function DMD($name)
	{
		return self::featureCheck('DMD', $name);
	}
	
	/**
	 * Adds a new feature to the feature list for brickwork.
	 * See @Features::_setFeature for more information
	 *
	 * @param string 	$feature	The feature you want to set
	 * @param boolean 	$enabled	The value you want to set it to
	 * @return boolean
	 */
	public static function setBrickworkFeature( $feature, $enabled = true )
	{
		return self::_setFeature('brickwork', $feature, $enabled );
	}
	
	/**
	 * Adds a new feature to the feature list for phoundry.
	 * See @Features::_setFeature for more information
	 *
	 * @param string 	$feature	The feature you want to set
	 * @param boolean 	$enabled	The value you want to set it to
	 * @return boolean
	 */
	public static function setPhoundryFeature( $feature, $enabled = true )
	{
		return self::_setFeature('phoundry', $feature, $enabled );
	}
	
	/**
	 * Adds a new feature to the feature list for DMdelivery.
	 * See @Features::_setFeature for more information
	 *
	 * @param string 	$feature	The feature you want to set
	 * @param boolean 	$enabled	The value you want to set it to
	 * @return boolean
	 */
	public static function setDMDFeature( $feature, $enabled = true )
	{
		return self::_setFeature('DMD', $feature, $enabled );
	}
	

	/**
	 * Adds a new feature to the feature list.
	 * This method actually stores a specific feature in cache so you will not have to 
	 * check for it again.
	 * This may be handy for modules which have their own checks which not really are
	 * brickwork or phoundry features but you may have to check. For example: 
	 * a check to see which tables you need to use.
	 *
	 * @param string 	$prefix
	 * @param string 	$name
	 * @param boolean 	$value
	 * @return boolean
	 */
	protected static function _setFeature( $prefix, $name, $value )
	{
		if(is_null(self::$cache)) self::$cache = Cache::factory('file:///features');
		
		self::$cache[$prefix.$name] = $value == true;
		return true;
	}
	
	/**
	 * The real feature checker
	 *
	 * @param string $prefix
	 * @param string $name
	 * @return bool
	 */
	protected static function featureCheck($prefix, $name)
	{
		if (isset(self::$one_cache[$prefix.$name])) {
			return self::$one_cache[$prefix.$name];
		}
		
		global $WEBprefs;
		
		if(is_null(self::$cache)) self::$cache = Cache::factory('file:///features');
		
		if(!isset(self::$cache[$prefix.$name]))
		{
			if ( class_exists('Features_'.ucfirst($prefix)) && is_callable(array('Features_'.ucfirst($prefix), $name)) )
			{
				self::$cache[$prefix.$name] =  call_user_func(array('Features_'.ucfirst($prefix), $name));
			}
			else if ( isset( $WEBprefs['Features'] ) && isset($WEBprefs['Features'][$prefix.ucfirst($name)]) )
			{
				self::$cache[$prefix.$name] = (is_callable($WEBprefs['Features'][$prefix.ucfirst($name)]) ? call_user_func( $WEBprefs['Features'][$prefix.ucfirst($name)] ) : $WEBprefs['Features'][$prefix.ucfirst($name)]);
			}
			else if(TESTMODE)
			{
				throw new Exception('Feature check for '.$prefix.$name.' does not exist!');
			}
			else
				self::$cache[$prefix.$name] = false;
		}
		
		self::$one_cache[$prefix.$name] = self::$cache[$prefix.$name];
		
		return self::$cache[$prefix.$name];
	}
}
