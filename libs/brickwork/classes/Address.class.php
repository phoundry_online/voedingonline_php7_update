<?php
require_once INCLUDE_DIR . '/classes/Postalcode.class.php';
require_once INCLUDE_DIR . '/classes/Country.class.php';

/**
* Address
*
* Adresgegevens
*
* @author Mick van der Most van Spijk
* @last_modified 7/10/2006
* @package None
*/
class Address {
	/**
	*
	* @var String
	* @access public
	*/
	public $streetName;

	/**
	*
	* @var Integer
	* @access public
	*/
	public $houseNumber;

	/**
	*
	* @var String
	* @access public
	*/
	public $houseNumberAddition = '';

	/**
	*
	* @var Zip
	* @access public
	*/
	public $zipCode = NULL;

	/**
	*
	* @var String
	* @access public
	*/
	public $city = '';

	/**
	*
	* @var Country
	* @access public
	*/
	public $country = NULL;

	/**
	* Make a new address
	*
	* @param String
	* @param Integer
	* @param String
	* @param Postalcode
	* @param String
	* @param Country
	*/
	public function __construct($street, $housenr, $add='', $zip, $city, $country) {
		// chk numeric housenummer
		if (!is_numeric($housenr)) {
			trigger_error('Ongeldig huisnummer', E_USER_ERROR);
			exit(1);
		}

		// if country is country code then create it
		if(is_string($country)){
			$country = new Country($country);
		}

		// check land
		if (!Country::isCountry($country)) {
			trigger_error('Ongeldig land', E_USER_ERROR);
			exit(1);
		}
		// if zip is string then create it
		if(is_string($zip)){
			$zip = new Postalcode($zip, $country);
		}

		// chk postcode
		if (!Postalcode::isZip($zip, $country)) {
			trigger_error('Ongeldige postcode', E_USER_ERROR);
			exit(1);
		}

		// ken attr toe
		$this->streetName          = $street;
		$this->houseNumber         = $housenr;
		$this->houseNumberAddition = $add;
		$this->zip                 = $zip;
		$this->city                = $city;
		$this->country             = $country;
	}
	
	public function __toString(){
		$retval = array();
		$retval[]=$this->streetName.' '.$this->houseNumber.$this->houseNumberAddition;
		$retval[]=$this->zip.' '.$this->city;
		$retval[]=$this->country;
		return preg_replace('/ {2,}/','', implode("\n", $retval));
	}
}
?>
