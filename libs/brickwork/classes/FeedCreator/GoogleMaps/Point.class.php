<?php

class Point {

	/**
	 * Set variables for this placemark point
	 */
	var $coordinates;

	public function __construct($coordinates)	{
		$this->coordinates = $coordinates;
	}

	public function __toString() {
		return $this->coordinates;
	}

}

?>