<?php

class PlaceMark extends KMLPlaceMarkItem {

	/**
	 * Set variables for this placemark
	 */
	var $icon;
	var $point;

	public function __construct()	{
		
	}

	public function setIcon ($url, $scale = 1) {

		if (!empty($url) && is_numeric($scale)) {
			$newIcon->iconUrl = $url;
			$newIcon->iconScale = $scale;
			$newIcon->iconHash = hash('md5', $url.$scale);
			$this->icon = $newIcon;
			return true;
		} else {
			return false;
		}
		
	}

	public function setCoordinates ($coordinates) {
		
		$this->point = new Point($coordinates);

	}

}

?>