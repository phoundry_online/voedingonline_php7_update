<?PHP

/**
 * KMLCreator is a FeedCreator that implements KML 2.0.
 * 
 * @see http://code.google.com/apis/kml/documentation/kmlreference.html
 * @author Adrie den Hartog
 */
class KMLCreator extends FeedCreator {

	function KMLCreator() {
		$this->encoding = "utf-8";
	}
    
	static function groupSort($a, $b) {
		if ($a->group == $b->group) {
			return 0;
		}
		return ($a->group < $b->group) ? -1 : 1;
	}

	function createFeed() {     

		$feed = "<?xml version=\"1.0\" encoding=\"".$this->encoding."\"?>\n";
		$feed.= $this->_createGeneratorComment();
		$feed.= "<kml xmlns=\"http://earth.google.com/kml/2.0\">\n";
		$feed.= "    <Document>\n";
		$feed.= "        <name>".htmlspecialchars($this->title)."</name>\n";

		// Filter unique styles
		foreach ($this->items as $item) {
			$uniqueIcons[$item->placeMark->icon->iconHash]->iconUrl = $item->placeMark->icon->iconUrl;
			$uniqueIcons[$item->placeMark->icon->iconHash]->iconScale = $item->placeMark->icon->iconScale;
		}

		if (isset($uniqueIcons) && is_array($uniqueIcons)) {
			$feed.= "	<!-- Begin Style Definitions -->\n";
			foreach ($uniqueIcons as $hash => $iconStyle) {
				$feed.= "	<Style id=\"a".$hash."\">\n";
				$feed.= "	<IconStyle>\n";
				$feed.= "		<scale>".$iconStyle->iconScale."</scale>\n";
				$feed.= "		<Icon><href>".$iconStyle->iconUrl."</href></Icon>\n";
				$feed.= "	</IconStyle>\n";
				$feed.= "	</Style>\n";
			}
			$feed.= "	<!-- End Style Definitions -->\n";
		}

		$lastGroup = "";

		// Detect whether we're using grouping of markers
		if (isset($this->items[0]->group)) {
			
			// Sort by group
			usort($this->items, array(&$this, "groupSort"));

		}	

		foreach ($this->items as $key=>$item) {

			if (isset($item->group) && $item->group != $lastGroup) {
				$feed.= "	<Folder id=\"".$item->group."\">\n";
			}

			$feed.= "	<!-- Placemark ".$item->id." -->\n";
			$feed.= "	<Placemark id=\"".$item->id."\">\n";
			$feed.= "		<name>".$item->placeMark->title."</name>\n";
			$feed.= "		<description><![CDATA[".$item->placeMark->description."]]></description>\n";
			$feed.= "		<styleUrl>#a".$item->placeMark->icon->iconHash."</styleUrl>\n";
			$feed.= "		<Point>\n";
			$feed.= "			<coordinates>".$item->placeMark->point."</coordinates>\n";
			$feed.= "		</Point>\n";
			$feed.= "	</Placemark>\n";
			$feed.= "	<!-- / Placemark ".$item->id." -->\n";

			if (isset($item->group) && isset($this->items[$key+1]->group) && $this->items[$key+1]->group != $item->group) {
				$feed.= "	</Folder>\n";				
			} elseif (isset($item->group) && !isset($this->items[$key+1]->group)) {
				$feed.= "	</Folder>\n";
			}

			if (isset($item->group)) {
				$lastGroup = $item->group;
			}

		}

		$feed.= "    </Document>\n";
		$feed.= "</kml>\n";
		return $feed;
	}
}



?>