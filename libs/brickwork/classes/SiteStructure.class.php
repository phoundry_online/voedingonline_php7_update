<?php
if (!interface_exists('ACLObjectInterface')) {
	require_once 'ACL/Object.if.php';
}

/**
 * Site Structure
 *
 * @author Jørgen Teunis
 * @last_modified 28/12/2008
 * @version 1.0.1
 * @abstract
 *
 * $Id: SiteStructure.class.php 2005 2010-11-01 12:05:05Z christiaan.baartse $
 */
abstract class SiteStructure
{
	/**
	 * Flat list of site structure items for a given site_identifier
	 * This references directly to items in the recursive structure id's
	 * This is done to spead up and simplify recursive loops
	 *
	 * @var array of site structure items
	 */
	public static $stack = null;

	/**
	 * Recursive list of site structure items for a given site_identifier
	 *
	 * @var array
	 */
	public static $structure = array();

	/**
	 * Paths array cache for the getPathByStructureId function
	 */
	protected static $paths = array();

	/**
	 * Gives back the site structure stacked array for the given site_identifier
	 *
	 * @param string site_identifier
	 * @return array|void
	 */
	public static function getSiteStack($site_identifier)
	{
		// check params
		if (!is_string($site_identifier)) {
			trigger_error(
				'Site identifier is van het verkeerde type',
				E_USER_WARNING
			);
			return null;
		}

		// return item uit de stack indien de stack een array is
		if (isset(self::$stack[$site_identifier]) &&
				is_array(self::$stack[$site_identifier])) {
			return self::$stack[$site_identifier];
		}

		// geen sitestack
		return null;
	}

	/**
	 * Get and loads the recursive site structure for a given site_identifier
	 *
	 * @param string site_identifier
	 * @param Site_Language lang
	 * @return array|void
	 */
	public static function getSiteStructure($site_identifier,
		Site_Language $lang = null)
	{
		global $WEBprefs;

		$useACL = Features::brickwork('ACL');

		// check params
		if (!is_string($site_identifier)) {
			trigger_error(
				'Site identifier is van het verkeerde type',
				E_USER_WARNING
			);
			return null;
		}

		// get the structure the given site_identifier from the internal list
		// if it exists and return it.
		if (isset(self::$structure[$site_identifier]) &&
				is_array(self::$structure[$site_identifier])) {
			return self::$structure[$site_identifier];
		}

		// instanciate empty lists
		$structure = array();
		$stack = array();

		if ( is_null($lang) ) {
			$langcode = isset(
				Framework::$site->defaultLang,
				Framework::$site->currentLang->code
			) ? Framework::$site->currentLang->code : '';
		} else {
			$langcode = $lang->code;
		}

		$cache_key = $site_identifier.( $langcode == '' ? '' : '_' . $langcode);
		$cache = Cache::factory(
			isset($WEBprefs['SiteStructure.Caching.Target'])
			? $WEBprefs['SiteStructure.Caching.Target']
			: 'dir:///site_structure?timeout=3600'
		);
		$stack = (!is_null($cache[$cache_key]) && !is_null($cache[$cache_key]['stack'])) ?
			$cache[$cache_key]['stack'] : array();

		$structure = (!is_null($cache[$cache_key]) && !is_null($cache[$cache_key]['structure'])) ?
			$cache[$cache_key]['structure'] : array();

		if (!is_array($stack) || count($stack) == 0) {
			/*
			 *
			 * It is possible to add custom fields to the site structure.
			 * To make sure that no harm is done by all custom made work
			 * against the standard Brickwork site_structure table, you
			 * can create a custom_site_structure table with custom fields to
			 * extend a structure item.
			 *
			 * Here we check of the table exists and if so we add the fields to
			 * the result. All fields in the custom_site_structure table need
			 * to be prefixed with custom_YOUR_FIELD_NAME
			 *
			 */
			$sql_custom = '';
			$sql_custom_field = '';

			$sql = 'SHOW TABLES LIKE "brickwork_custom_site_structure"';
			$res = DB::iQuery($sql);

			// custom table exists, glue it to our query
			if ($res instanceof ResultSet) {
				$sql_custom_field = 'css.*,';
				$sql_custom = ' LEFT JOIN brickwork_custom_site_structure css '.
					'ON css.custom_id = s.id';
			}

			// Backward compatibility check voor 1.3.x and prior releases.
			// If no language => no language check

			if (!Features::brickwork('translation') &&
				(
					!isset(Framework::$site->defaultLang) ||
					is_null(Framework::$site->defaultLang)
				)
			) {
				$sql = sprintf(
					"SELECT
						s.id as id,
						s.parent_id as parent,
						s.name as name,
						s.stype as type,
						s.secure as secure,
						s.ssl as `ssl`,
						%s
						s.show_sitemap,
						s.show_menu,
						ssp.name as path,
						%s
						s.prio
					FROM
						brickwork_site_structure s
					LEFT JOIN
						brickwork_site_structure_path ssp
					ON
						ssp.site_structure_id = s.id
						/* custom mee selecteren ja of nee ?*/
						%s
					WHERE
						s.site_identifier = '%s'
					ORDER BY
						s.parent_id IS NULL DESC, s.prio ASC
					",
					( $useACL ? 's.restricted as restricted,' : ''),
					// values
					$sql_custom_field,
					$sql_custom,
					DB::quote($site_identifier)
				);
			} else {
				if (is_null($lang)) {
					$langId = (isset(Framework::$site->currentLang) ?
						Framework::$site->currentLang->getId() : 0);
				} else {
					$langId = $lang->getId();
				}

				$sql = sprintf(
					"SELECT
						s.id as id,
						s.parent_id as parent,
						IF(si.name IS NULL, s.name, si.name) as name,
						s.stype as type,
						s.secure as secure,
						s.ssl as `ssl`,
						%s
						s.show_sitemap,
						s.show_menu,
						ssp.name as path,
						%s
						s.prio
					FROM
						brickwork_site_structure s
					LEFT OUTER JOIN brickwork_site_structure_i18n si
					ON (s.id = si.site_structure_id AND si.lang_id = %d)
					LEFT JOIN
						brickwork_site_structure_path ssp 
					ON
						(
						ssp.site_structure_id = s.id AND
						(ssp.lang_id = 0 OR ssp.lang_id = %d)
						)
						/* custom mee selecteren ja of nee ?*/
						%s
					WHERE
						s.site_identifier = '%s'
					ORDER BY
						s.parent_id IS NULL DESC, s.prio ASC
					",
					( $useACL ? 's.restricted as restricted,' : ''),
					$sql_custom_field,
					// values
					$langId,
					$langId,
					$sql_custom,
					DB::quote($site_identifier)
				);
			}

			$res = DB::iQuery($sql);
			if (self::isError($res)) {
				trigger_error($res->errstr, E_USER_ERROR);
			}

			// first always try to add custom fields
			$addCustomFields = true;

			if (!is_array($structure))
				$structure = array();

			// De resultaten doorlopen
			while ($res->current()) {
                $item = $res->current();
				// Toevoegen aan de output array
				$sItem = new StructureItem(
					$item->id,
					$item->name,
					$item->parent,
					false,
					$item->secure,
					$item->show_menu,
					$item->show_sitemap,
					$item->path,
					$item->ssl
				);

				if ($useACL) {
					$sItem->restricted = $item->restricted == 1;
					$sItem->secure = $item->secure || $item->restricted;
				} else {
					$sItem->secure		= $item->secure;
				}

				// add custom fields to basic structure item,
				// in case no custom fields found for the first item,
				// ignore it for the rest of the items as well.
				if ($addCustomFields === true) {
					$addCustomFields = $sItem->addCustomFields($item);
				}

				// empty parent so we are a parent ourself
				if (empty($item->parent) || !isset($stack[$item->parent])) {
					$structure[] = $sItem;
					$stack[$sItem->id] = $sItem;
					// structure item has a parent,
					// so we hang it against his parent
				} else {
					// element-array in de "Symlink" variabele aanroepen en
					// hier een data-array aanhangen
					$stack[$item->parent]->addElement($sItem);
					$stack[$sItem->id] = $sItem;
				}
				$res->next();
			}

			$cache[$cache_key] = array(
				'stack' => $stack,
				'structure' => $structure
			);
		}

		// store stack and structure for given site_identifier
		self::$stack[$site_identifier]		= $stack;
		self::$structure[$site_identifier]	= $structure;

		// geef terug
		return self::$structure[$site_identifier];
	}

	/**
	 * Remove structure cache file
	 *
	 * Used to keep all the file location/handling stuff in oen file
	 * used from within the Phoundry site structure plugin
	 *
	 * @param string $site_identifier
	 * @return bool
	 */
	public static function removeCacheFile()
	{
		global $WEBprefs;
		return Cache::factory(
			isset($WEBprefs['SiteStructure.Caching.Target'])
			? $WEBprefs['SiteStructure.Caching.Target']
			: 'dir:///site_structure?timeout=3600'
		)->flush();
	}

	/**
	 * Returns a structure item for a given site_identifier.
	 * For example if you determined the active id, you call this method
	 * for the site and the active site structure id.
	 *
	 * @param string
	 * @param int
	 * @return StructureItem|bool
	 */
	public static function getStructureItem($site_identifier, $active_id)
	{
		if (!isset(self::$stack[$site_identifier])) {
			self::getSiteStructure($site_identifier);
		}

		if (isset(self::$stack[$site_identifier][$active_id])) {
			return self::$stack[$site_identifier][$active_id];
		}

		// asked item not found
		return false;
	}

	/**
	 * Return the first StructureItem in the StructureItem tree that is active
	 *
	 * @param array structure_items
	 * @return StructureItem|void
	 */
	private static function _get_active_structure_item($structure_items)
	{
		/* @var $structure StructureItem */
		foreach ($structure_items as $structure) {
			if ($structure->active) {
				$child = self::_get_active_structure_item($structure->element);
				if ($child) {
					return $child;
				}
				return $structure;
			}
		}

		return null;
	}

	/**
	 * Controls the recursive function "_get_active_structure_item"
	 *
	 * @param int site_identifier
	 * @return StructureItem|null
	 */
	public static function getActiveStructureItem($site_identifier)
	{
		return self::_get_active_structure_item(
			self::getSiteStructure($site_identifier)
		);
	}


	/**
	 * Set a recursive structure item PATH active if exists
	 *
	 * @param	string Site identifier code
	 * @param	int ID to activate
	 * @return	StructureItem|bool
	 */
	public static function activateId($site_identifier, $active_id)
	{
		// is the site structure is not loaded yet, try to load it.
		if (!isset(self::$stack[$site_identifier])) {
			if (!self::getSiteStructure($site_identifier)) {
				trigger_error('No stack for site structure', E_USER_ERROR);
			}
		}

		// try to find voodoo
		$teller = 0;
		while ($teller < count(self::$stack[$site_identifier])) {
			if (isset(self::$stack[$site_identifier][$active_id])) {
				self::$stack[$site_identifier][$active_id]->active = true;

				if (!empty(self::$stack[$site_identifier][$active_id]->parent)) {
					$active_id = self::$stack[$site_identifier][$active_id]->parent;
				} else {
					break;
				}
			}
			$teller++;
		}

		if (!empty(self::$stack[$site_identifier][$active_id])) {
			return self::$stack[$site_identifier][$active_id];
		}

		return false;
	}

	/**
	 * Get the current live page data for a given structure_id
	 * @param DB_Adapter $db
	 * @param Site $site
	 * @param int $structure_id
	 * @param int $page_id a specific page we want to load (test page)
	 * @return array|bool
	 */
	public static function getLivePage(DB_Adapter $db, $site_identifier,
		$structure_id, $lang_id = null, $page_id = null)
	{
		// Backward compatibility check voor 1.3.x and prior releases.
		// If no language => no language check
		if (!is_null($lang_id)) {
			$sql = sprintf(
				'SELECT
					p.*
				FROM
					brickwork_site_structure AS s
				INNER JOIN
					brickwork_page AS p
				ON
					p.site_structure_id = s.id
				AND
					( p.lang_id = "%d" OR p.lang_id IS NULL)
				AND (
					(
						"%s" BETWEEN online_date AND offline_date 
						AND staging_status="live"
					)
					OR
					staging_status = "test"
				)
				WHERE
					s.id = %d
				AND
					s.site_identifier = "%s"
				ORDER BY
					p.lang_id DESC,
					p.online_date ASC
				',
				// values
				$db->quote($lang_id),
				date('Y-m-d H:i:00', PHP_TIMESTAMP),
				$structure_id,
				$db->quote($site_identifier)
			);
		} else {
			// Original 1.3.x query
			$sql = sprintf(
				'SELECT
					p.*
				FROM
					brickwork_site_structure AS s
				INNER JOIN
					brickwork_page AS p
				ON
					p.site_structure_id = s.id
				AND (
					(
						"%s" BETWEEN online_date AND offline_date
						AND staging_status="live"
					)
					OR
					staging_status = "test"
				)
				WHERE
					s.id = %d
				AND
					s.site_identifier = "%s"
				ORDER BY
					p.online_date ASC
				',
				// values
				date('Y-m-d H:i:00', PHP_TIMESTAMP),
				$structure_id,
				$db->quote($site_identifier)
			);
		}

		$res = $db->iQuery($sql);

		if (!$res->current()) {
			return false;
		}

		// We want a specific page as requested by the page_id param
		if (null !== $page_id) {
			while ($res->current()) {
                $record = $res->current();
				if ($record->id == $page_id) {
					return $record;
				}
				$res->next();
			}
			return false;
		}

		// Otherwise we want the first page that is live
		while ($res->current()) {
            $record = $res->current();
			if ($record->staging_status == 'live') {
				// Return the eerste de beste live pagina
				return $record;
			}
			$res->next();
		}
		return false;
	}

	/**
	 * Get the count of pages per structure ID that have test
	 *
	 * @param	array	$struct_ids
	 * @return	array
	 */
	public static function getTestPages($struct_ids)
	{
		$retval = array();
		if ($struct_ids) {
			// Backward compatibility check voor 1.3.x and prior releases.
			// If no language => no language check
			if (!Features::brickwork('translation') ||
				!isset(Framework::$site) ||
				!isset(Framework::$site->currentLang)) {
				$sql = sprintf(
					'SELECT
						s.id,
						count(p.id) as pages
					FROM
						brickwork_site_structure AS s
					INNER JOIN
						brickwork_page AS p
					ON
						(
							p.site_structure_id = s.id
							OR p.site_structure_id = s.linked_to
						)
					AND 
						p.staging_status = "test"
					WHERE
						s.id IN ( %s )
					GROUP BY
						s.id
					',
					// values
					implode(',', $struct_ids)
				);
			} else {
				$sql = sprintf(
					'SELECT
						s.id,
						count(p.id) as pages
					FROM
						brickwork_site_structure AS s
					INNER JOIN
						brickwork_page AS p
					ON
						(
							p.site_structure_id = s.id
							OR p.site_structure_id = s.linked_to
						)
					AND 
						p.staging_status = "test"
					AND
						( p.lang_id IS NULL OR p.lang_id = "%d" ) 
					WHERE
						s.id IN ( %s )
					GROUP BY
						s.id
					',
					// values
					Framework::$site->currentLang->getId(),
					implode(',', $struct_ids)
				);
			}

			$res = DB::iQuery($sql);

			while ($res->current()) {
                $s = $res->current();
				$retval[$s->id] = $s->pages;
				$res->next();
			}
		}
		return $retval;
	}

	/**
	 * Get number of pages connected to a list of site structure id's
	 *
	 * @param array structure id's
	 */
	public static function getPages($struct_ids)
	{
		$retval = array();

		if ($struct_ids) {
			// Backward compatibility check voor 1.3.x and prior releases.
			// If no language => no language check
			if (!Features::brickwork('translation') ||
				!isset(Framework::$site) ||
				!isset(Framework::$site->currentLang)) {
				$sql = sprintf(
					'SELECT
						s.id,
						count(p.id) as pages
					FROM
						brickwork_site_structure AS s
					INNER JOIN
						brickwork_page AS p
					ON
						(
							p.site_structure_id = s.id
							OR p.site_structure_id = s.linked_to
						)
					AND (
						(
							"%s" BETWEEN online_date AND offline_date
							AND staging_status="live"
						)
						OR
						staging_status = "test"
					)
					WHERE
						s.id IN(%s)
					GROUP BY
						s.id
					',
					// values
					date('Y-m-d H:i:00'),
					implode(',', $struct_ids)
				);
			} else {
				$sql = sprintf(
					'SELECT
						s.id,
						count(p.id) as pages
					FROM
						brickwork_site_structure AS s
					INNER JOIN
						brickwork_page AS p
					ON
						(
							p.site_structure_id = s.id OR
							p.site_structure_id = s.linked_to
						)
					AND (
						(
							"%s" BETWEEN online_date AND offline_date
							AND staging_status="live"
						)
						OR
						staging_status = "test"
					)
					AND
						( p.lang_id IS NULL OR p.lang_id = "%d" ) 
					WHERE
						s.id IN(%s)
					GROUP BY
						s.id
					',
					// values
					date('Y-m-d H:i:00'),
					Framework::$site->getLang()->getId(),
					implode(',', $struct_ids)
				);
			}

			$res = DB::iQuery($sql);

			while ($res->current()) {
                $s = $res->current();
				$retval[$s->id] = $s->pages;
				$res->next();
			}
		}

		return $retval;
	}

	/**
	 * Get path by a structure id
	 *
	 * @param int $id
	 * @return string
	 */
	public static function getPathByStructureId($id, $identifier = null,
		$lang = null)
	{
		if (is_null($identifier)) {
			$identifier = Framework::$site->identifier;
		}

		if (!is_null($lang)) {
			$langcode = is_object($lang) ? $lang->getId() : $lang;
			$mlwhere = sprintf(" AND (lang_id = %d OR lang_id = 0)", $langcode);
			$mlorder = 'ORDER BY lang_id DESC';
		} else {
			$mlwhere = '';
			$mlorder = '';
			$langcode = 'default';
		}

		if (!isset(self::$paths[$identifier][$id][$langcode])) {
			if (!isset(self::$paths[$identifier])) {
				self::$paths[$identifier] = array();
			}

			if ( !isset(self::$paths[$identifier][$id])) {
				self::$paths[$identifier][$id] = array();
			}

			if (!isset(self::$paths[$identifier][$id][$langcode])) {
				$sql = sprintf(
					'SELECT 
					name 
					FROM 
					brickwork_site_structure_path 
					WHERE 
					site_identifier ="%s" 
					AND 
					site_structure_id=%d %s %s LIMIT 1',
					DB::quote($identifier),
					$id,
					$mlwhere,
					$mlorder
				);

				$result = DB::iQuery($sql);

				if (self::isError($result)) {
					trigger_error($result->errstr, E_USER_ERROR);
				}

				if ($row = $result->first()) {
					self::$paths[$identifier][$id][$langcode] = $row->name;
				} else {
					self::$paths[$identifier][$id][$langcode] = $id;
				}
			}
		}
		return self::$paths[$identifier][$id][$langcode];
	}

	/**
	 * Get the whole path including pagecontenthandler and language parameter
	 * @param int $id
	 * @param string $identifier Site Identifier
	 * @param int|Site_Language $lang
	 * @return string
	 */
	public static function getFullPathByStructureId( $id, $identifier = null,
		$lang = null)
	{
		$langcode = '';
		if(is_null($identifier)) {
			$identifier = Framework::$site->identifier;
			$site = Framework::$site;
		} else {
			$site = Framework::getSiteByIdentifier($identifier, $lang);
		}

		if (is_null($lang)) {
			$lang = Framework::determineLanguage();
		}


		if ($site->isMultiLingual() && $lang) {
			if ($lang instanceof Site_Language) {
				$langcode = $lang->code.'/';
			} else {
				$res = DB::iQuery(
					sprintf(
						'SELECT code
						FROM brickwork_site_language
						WHERE id = %d
						AND site_identifier = %s
						',
						$lang,
						DB::quoteValue($identifier)
					)
				);

				if ($res instanceof ResultSet && count($res)) {
					$langcode = $res->first()->code.'/';
				}
			}
		}
		return '/page/'.$langcode.self::getPathByStructureId(
			$id,
			$identifier,
			$lang
		);
	}

	/**
	 * Get Structure
	 *
	 * @param array $path
	 * @return SiteStructurePathResult
	 */
	public static function getStructureByPath(array $path, $identifier = null)
	{
		if (is_null($identifier)) {
			$identifier = Framework::$site->identifier;
		}

		// remove empty value in the end caused by explode
		if (empty($path[count($path) - 1])) {
			unset($path[count($path) - 1]);
		}

		// TODO 0 naar NULL omschrijven en order by NULL DESC
		// (of asc, iig een taal voorang geven op geen taal
		if (isset(Framework::$site) && Framework::$site->isMultiLingual()) {
			$mlwhere = sprintf(
				"AND (lang_id = %d OR lang_id = 0) ORDER BY lang_id DESC",
				isset(Framework::$site, Framework::$site->currentLang) ?
				Framework::$site->currentLang->getId() : 0
			);
		} else {
			$mlwhere = '';
		}

		while (count($path) > 0) {
			$foo = implode('/', $path);

			$sql = sprintf(
				'SELECT site_structure_id, prio
				FROM (
					SELECT site_structure_id, 0 as prio 
					FROM brickwork_site_structure_path 
					WHERE site_identifier="%s" 
					AND hash="%s" 
					%s
				) p
				UNION
				SELECT site_structure_id, prio
				FROM (
					SELECT site_structure_id, 1 as prio 
					FROM brickwork_site_structure_path_archive 
					WHERE site_identifier="%s" 
					AND hash="%s" 
					%s
				) pa
				LIMIT 1',
				DB::quote($identifier),
				md5($foo),
				$mlwhere,
				DB::quote($identifier),
				md5($foo),
				$mlwhere
			);
			/*
			$sql = sprintf('
						SELECT site_structure_id, prio FROM
							(
							SELECT site_structure_id, 0 as prio
							  FROM brickwork_site_structure_path
							 WHERE site_identifier="%s"
							   AND hash="%s"
							   %s
							UNION
							SELECT site_structure_id, 1 as prio
							  FROM brickwork_site_structure_path_archive
							 WHERE site_identifier="%s"
							   AND hash="%s"
							   %s
							) rp LIMIT 1
						', // values
						DB::quote($identifier),
						md5($foo),
						$mlwhere,
						DB::quote($identifier),
						md5($foo),
						$mlwhere
						);
			*/
			$res = DB::iQuery($sql);
			if (self::isError($res)) {
				trigger_error($res->errstr, E_USER_ERROR);
			}

			if ($row = $res->first()) {
				return new SiteStructurePathResult(
					$row->site_structure_id,
					$row->prio
				);
			}

			array_pop($path);
		}

		// NO RESULT
		return null;
	}


	/**
	 * Get number of URLs connected to a list of site structure id's
	 *
	 * @param array
	 */
	public static function getUrls($struct_ids)
	{
		$retval = array();

		// no list, no result, bye
		if (count($struct_ids) == 0) {
			return $retval;
		}

		// Backward compatibility check voor 1.3.x and prior releases.
		// If no language => no language check
		if (!isset(Framework::$site, Framework::$site->defaultLang)) {
			$sql = sprintf(
				'SELECT
					s.id,
					u.link,
					u.target
				FROM
					brickwork_site_structure AS s
				INNER JOIN
					brickwork_url AS u
				ON
					(
						u.site_structure_id = s.id
						OR u.site_structure_id = s.linked_to
					)
				WHERE
					s.id IN(%s)
				GROUP BY
					s.id
				',
				implode(',', $struct_ids)
			);
		} else {
			$sql = sprintf(
				'SELECT
					s.id,
					u.link,
					u.target
				FROM
					brickwork_site_structure AS s
				INNER JOIN
					brickwork_url AS u
				ON
					(
						u.site_structure_id = s.id
						OR u.site_structure_id = s.linked_to
					)
				AND
					( u.lang_id IS NULL OR u.lang_id = "%d" ) 
				WHERE
					s.id IN(%s)
				GROUP BY
					s.id
				',
				Framework::$site->getLang()->getId(),
				implode(',', $struct_ids)
			);
		}

		$res = DB::iQuery($sql);
		if ($res->current()) {
			while ($res->current()) {
                $s = $res->current();
				$retval[$s->id] = array('link'=>$s->link, 'target'=>$s->target);
			    $res->next();
			}
		}

		return $retval;
	}

	/**
	 * Get the StructureItem which is the home for the site
	 *
	 * @throws Exception if site has no structure items
	 * @param string $site_identifier
	 * @return StructureItem
	 */
	public static function getHomeStructure($site_identifier)
	{
		$structure = SiteStructure::getSiteStructure($site_identifier);
		if (!$structure) {
			throw new Exception("No home structure found for site ".
				$site_identifier);
		}
		return $structure[0];
	}

	/**
	 * Get full path by readable link
	 *
	 * @param stirng $subdir
	 * @param string $site_identifier
	 * @return string url
	 */
	static function getFullPathByReadableLink($subdir, $site_identifier = null)
	{
		$res = DB::iQuery(
			sprintf(
				"SELECT
					site_structure_id, site_identifier
				FROM
					brickwork_readable_links
				WHERE
					subdir = '%s'
					%s",
				DB::quote($subdir),
				$site_identifier ?
				' AND site_identifier=\''.$site_identifier.'\'' : ''
			)
		);

		if ($row = $res->first()) {
			return SiteStructure::getFullPathByStructureId($row->site_structure_id, $row->site_identifier);
		}
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

class StructureItem implements ACLObjectInterface
{
	/**
	 * Id
	 * @var int
	 */
	public $id;

	/**
	 * Naam
	 * @var string
	 */
	public $name;

	/**
	 * Parent
	 * @var int
	 */
	public $parent;

	/**
	 * This flag indicates if a "page" and its contents should be checked
	 * for security. It does not mean that the page itself is secure,
	 * or restricted in any way.
	 * To restrict access to a page use the $_restricted option
	 *
	 * @var bool
	 */
	private $_secure = false;

	/**
	 * Indicator to make sure only authorised users can see the page
	 * If this option is on, only authorised users will be able to see the page.
	 * If the user does not have access,
	 * then the page should throw a 403: not authorised error.
	 *
	 * @var 	bool
	 */
	private $_restricted = false;

	/**
	 * Active
	 *
	 * @var bool
	 * @defailt false
	 * @access private
	 */
	private $_active = false;

	/**
	 * showMenu
	 *
	 * Menu module uses this variable to determine if it needs to show it
	 *
	 * @var bool
	 * @default true
	 */
	public $showMenu = true;

	/**
	 * showSitemap
	 *
	 * @var bool
	 * @default false
	 */
	public $showSitemap = false;

	/**
	 * If this sitestructure should only be reached by https
	 * @var bool
	 */
	public $ssl = false;

	/**
	 * Child elements
	 * This is a single name for a recursive list of elements.
	 * But because there is only 1 main element and its used recursive in smarty
	 * its more simple to have it just as one.
	 *
	 * @var array
	 */
	public $element = array();

	/**
	 * Path of the structure item
	 *
	 * @var string
	 */
	protected $_path;

	/**
	 * Constructor
	 *
	 * @param int id
	 * @param string name
	 * @param int parent_id
	 * @param bool is_active
	 * @param bool is_secure
	 * @param bool show_menu
	 * @param bool show_sitemap
	 * @param string path (/page/foo/bar)
	 */
	public function __construct($id, $name, $parent, $is_active = false,
		$secure = false, $showMenu = false, $showSitemap = false,
		$path = false, $ssl = false)
	{
		$this->id = (int) $id;
		$this->name = $name;
		$this->parent = $parent;

		$this->active = $is_active;
		$this->showMenu = (bool) $showMenu;
		$this->showSitemap = (bool) $showSitemap;
		$this->_secure = (bool) $secure;
		$this->_path = !empty($path) ? $path : null;
		$this->ssl = (bool) $ssl;
		$this->_restricted = false;
	}

	/**
	 * Add fields from custom_site_structure table to the structure item
	 *
	 * @param mixed array or stdClass Object
	 * @return bool true in case custom fields where added
	 */
	public function addCustomFields($custom_fields_array)
	{
		$foundFields = false;
		if (is_object($custom_fields_array)) {
			// converteren naar lijst
			$custom_fields_array = get_object_vars($custom_fields_array);
		}

		// check op param
		if (!is_array($custom_fields_array)) {
			trigger_error(
				'Parameter is niet van het juiste type',
				E_USER_NOTICE
			);
		}

		// loop en plak de custom velden als attrib aan deze klasse
		foreach ($custom_fields_array as $index => $value) {
			if (substr($index, 0, 7) == 'custom_' && $index != 'custom_id') {
				$this->{$index} = $value;
				$foundFields = true;
			}
		}
		return $foundFields;
	}

	/**
	 * Magic setter Set object attribute
	 *
	 * @param string name
	 * @param mixed value
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'restricted':
				$this->_restricted = (bool) $value;
				break;
			case 'secure':
				$this->_secure = (bool) $value;
				break;
			case 'isActive':
			case 'active':
				$this->_active = (bool) $value;
				break;
			case 'path':
				if (!empty($value)) {
					$this->_path = $value;
				}
				break;
			default:
				if (substr($name, 0, 7) == 'custom_') {
					$this->{$name} = $value;
				}
		}
	}

	/**
	 * Magic getter Get object attribute
	 *
	 * @param string name
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'secure':
				return $this->_secure;
				break;
			case 'restricted':
				return $this->_restricted;
			case 'isActive':
			case 'active':
				return $this->_active;
				break;
			case 'path':
				if (!empty($this->_path)) {
					return $this->_path;
				} else {
					return SiteStructure::getPathByStructureId($this->id);
				}
				break;
		}

		// attribute niet gevonden
		trigger_error(sprintf('Attribuut %s not found', $name), E_USER_ERROR);
	}

	/**
	 * Adds a child structure element to itself
	 *
	 * @param StructureItem
	 */
	public function addElement(StructureItem $el)
	{
		$this->element[] = $el;
	}

	public function getInstanceId()
	{
		return get_class($this) . '/' . $this->id;
	}

	public function getInstanceName()
	{
		return $this->name;
	}

	public function getParent()
	{
		return SiteStructure::getStructureItem(
			Framework::$site->identifier,
			$this->parent
		);
	}

	/**
	 * Get sub element id's from the structure item
	 *
	 * @param bool $recursive
	 * @return array id's of child elements
	 */
	public function getElements($recursive = false)
	{
		$retval = array();
		foreach ($this->element as $structure_item) {
			$retval[] = $structure_item->id;
			if ($recursive) {
				$retval = array_merge(
					$retval,
					$structure_item->getElements(true)
				);
			}
		}
		return $retval;
	}

	/**
	 * Check if the user has access to the structure item
	 *
	 * @param AuthUser $user
	 * @return bool
	 */
	public function checkAccess(AuthUser $user)
	{
		if ($this->secure) {
			if ($user instanceof AnonymousUser) {
				return false;
			}

			if ($user->getSiteUserRole() == 'cursisten') { 
				if (!$this->getLimitedAccessItems($user->auth_id, $_SERVER['REQUEST_URI'])) {
					return false;
				} else {
					return true;
				}
			}

			try {
				$roles = DB::iQuery(
					sprintf(
						"SELECT role_id
						FROM brickwork_site_structure_role
						WHERE site_structure_id = %d",
						$this->id
					)
				)->getAsArray();
				$tmp = array();
				foreach ($roles as $role) {
				    $tmp[] = $role['role_id'];
                }
                $roles = $tmp;
			}
			catch(DB_Exception $e) {
				// Only check roles if we have the
				// brickwork_site_structure_role table
				if ($e->getCode() !== DB_Exception::BAD_TABLE) {
					throw $e;
				}
				$roles = false;
			}

			// Only check for role access if there are actually roles assigned
			if ($roles) {
				$user_roles = DB::iQuery(
					sprintf(
						"SELECT auth_role_id
						FROM brickwork_site_user_auth_role
						WHERE site_user_id = %d",
						$user->auth_id
					)
				)->getAsArray();

                $tmp = array();
                foreach ($user_roles as $role) {
                    $tmp[] = $role['auth_role_id'];
                }
                $user_roles = $tmp;
				// Roles that grant access as the user has them
				$access = array_intersect($roles, $user_roles);
				if (!$access) {
					return false;
				}
			}
		}

		return true;
	}

	private function getLimitedAccessItems($user_id, $link) {
		if (str_contains($link, 'E-book/')) {
			$curr_nodes = explode('/', $link);
			$base_url = implode('/', array_slice($curr_nodes, 0, array_search('hoofdstuk', $curr_nodes)));

        		if (empty($base_url)) {
            			$base_url = implode('/', $curr_nodes);
        		}
			$link = $base_url;
		}
		$sql = sprintf('
			select vo_abonnementsoort from brickwork_site_user where id = %d
		', $user_id);
		$role = DB::iQuery($sql)->getAsArray();

		if ($role[0]['vo_abonnementsoort'] != 'cursisten') {
			return false;
		} else if ($link == '/page/Persoonlijke-leeromgeving') {
			return true;
		}

		$sql = sprintf('
			select el.id from sse_e_learning_to_user as eltu
			left join sse_e_learning as el on eltu.accessed_e_learning = el.id
			left join sse_e_learning_access as ela on el.id = ela.e_learning_id
				where ela.link like "%%%s%%"
				and eltu.user_id = "%d"
		', substr($link, 0, -1), $user_id);
		$result = DB::iQuery($sql)->getAsArray();
		if (count($result) == 0) {
			return false;
		}
		
		return true;
	}

	private function redirectToOnlineLearningPage() {
		$sql = sprintf('select ssp.name from brickwork_site_structure_path as ssp
		left join brickwork_page as bp on ssp.site_structure_id = bp.site_structure_id
		left join brickwork_page_content as bpc on bpc.page_id = bp.id
		where bpc.module_code = "elearning_online"');
		$res = DB::iQuery($sql);

		if ($res->current()) {
			trigger_redirect('/page'.'/'.$res->current()->name);
		}
	}
}

/**
 * Result by getting the structure item by its path
 *
 * @param int $id site_structure id
 * @param int $archive 1/0
 */
class SiteStructurePathResult
{
	/**
	 * Site structure id
	 *
	 * @var int
	 */
	public $site_structure_id = 0;

	/**
	 * From archive
	 *
	 * @var bool
	 */
	public $archive = false;

	public function __construct($id, $archive)
	{
		$this->site_structure_id = (int) $id;
		$this->archive = (bool) $archive;
	}
}
