<?php
/**
* CAPTCHA
*
* This class generates an CAPTCHA image
* A CAPTCHA is a test that:
* - Most humans can pass.
* - Current computer programs can't pass.
*
* @author Mick van der Most van Spijk
* @copyright 2005 Web Power
*/

class Captcha {
	/**
	* number of captcha-chars
	* @var int
	*/
	private $length = 6;

	/**
	* generated string
	* @var string
	*/
	private $string;

	/**
	* image type to generate
	* @var string png|gif|jpg
	*/
	private $image_type = 'png';

	/**
	* font to use
	* @var string
	*/
	private $font;

	/**
	* image width
	* @var int
	*/
	private $width;

	/**
	* image height
	* @var int
	*/
	private $height = 36;

	/**
	* backgroundcolor
	* @var string
	*/
	private $backgroundcolor;

	/**
	* foregroundcolor
	* @var string
	*/
	private $foregroundcolor;

	/**
	* make a new captcha image
	* @param string letters to use
	* @param int length
	* @param string type
	*/
	public function __construct ($letter = '', $length = 6, $type = 'png', $font = 'ProggyClean.ttf', $width = NULL) {
		$this->length			= $length;
		$this->image_type		= $type;
		$this->width			= is_null($width) || $width < 50 ? 50 : (int)$width;
		$this->width = 50;

		//$this->font = FONT_DIR . 'bluehigh.ttf';
		$this->font = FONT_DIR . $font; // good fonts to see differences in 0 and O, and 1 and l

		// generate string
		if (empty($letter)) {
			$this->string_gen();
		} else {
			$this->length = strlen($letter);
			$this->string = $letter;
		}
	}

	public function setImageType($t) {
		$this->image_type = $t;
	}

	public function setBackgroundColor($c) {
		if (strlen($c) != 6)
			return;

		$this->backgroundcolor = $c;
	}

	public function setForegroundColor($c) {
		if (strlen($c) != 6)
			return;

		$this->foregroundcolor = $c;
	}

	/**
	* generate string
	*/
	private function string_gen () {
		$this->string = self::stringGen($this->length);
	}

	public static function stringGen ($length = 6) {
		
		if ( defined('TESTMODE') && TESTMODE )
			return 'abc123';
			
			
		$uppercase		= array('a','b','c','d','e','f','g','h','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z','2','3','4','5','6','7','8','9');
		$numeric			= range(2, 9);

		$char_pool		= array_merge($uppercase, $numeric);
		$pool_length	= count ($char_pool) - 1;

		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= $char_pool[mt_rand(0, $pool_length)];
		}

		return $string;
	}

	/**
	* send image headers
	*/
	private function send_header () {
		switch ($this->image_type) {
			case 'gif':
				header('Content-type: image/gif');
				break;

			case 'jpeg':
				header('Content-type: image/jpeg');
				break;

			case 'png':
				header('Content-type: image/png');
				break;

			default:
				header('Content-type: image/png');
				break;
		}
		header('Cache-control: no-store, no-cache, must-revalidate, proxy-revalidate');
		header('Cache-Control: max-age=-1, private, pre-check=-1');
		header('Pragma: no-cache');
	}

	/**
	* build image
	*/
	private function make_captcha () {
		$font = @imageloadfont($this->font);
		// determine font-char width, in case font can not be loaded, use 18 (pretty safe)
		$width = !$font ? 18 : imagefontwidth($font);
		$imagelength = $width*$this->length;
		$imageheight = $this->height;

		$image       = imagecreate($imagelength, $imageheight);

		if (!empty($this->backgroundcolor)) {
			$bgcolor     = imagecolorallocate($image, hexdec(substr($this->backgroundcolor, 0, 2)), hexdec(substr($this->backgroundcolor, 2, 2)), hexdec(substr($this->backgroundcolor, 4, 2)));
		} else {
			$bgcolor     = imagecolorallocate($image, 240, 240, 120);
		}

		if (!empty($this->backgroundcolor)) {
			$stringcolor = imagecolorallocate($image, hexdec(substr($this->foregroundcolor, 0, 2)), hexdec(substr($this->foregroundcolor, 2, 2)), hexdec(substr($this->foregroundcolor, 4, 2)));
			$linecolor   = imagecolorallocate($image, hexdec(substr($this->foregroundcolor, 0, 2)), hexdec(substr($this->foregroundcolor, 2, 2)), hexdec(substr($this->foregroundcolor, 4, 2)));
		} else {
			$stringcolor = imagecolorallocate($image, 0, 0, 0);
			$linecolor   = imagecolorallocate($image, 0, 0, 0);
		}

		if (function_exists('imagettftext')){
			imagettftext($image, ($imageheight - 16), rand(1,10), 8, ($imageheight - 4),
						 $stringcolor,
						 $this->font,
						 $this->string
			);
			/* imagettftext ( resource image, float size, float angle, int x, int y, int color, string fontfile, string text )*/
		} else {
			imagestring ($image, 9, rand(1,25), rand(1,15),	 $this->string, $stringcolor);
		}

		switch ($this->image_type) {
			case 'gif':
				imagegif($image);
				break;

			case 'jpeg':
				imagejpeg($image);
				break;

			case 'png':
				imagepng($image);
				break;

			default:
				imagepng($image);
				break;
		}
	}

	/**
	* return image
	*/
	public function get () {
		$this->send_header();
		$this->make_captcha();
	}

	/**
	* return captcha string
	*/
	public function getString() {
		return $this->string;
	}
}
?>
