<?php
/**
 * File Cabinet
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @package FileCabinet
 * 
 * @uses FileCabinet_Folder
 * @uses FileCabinet_File
 */
final class FileCabinet
{
	public static $folder_class = 'FileCabinet_Folder';
	public static $file_class = 'FileCabinet_File';
	
	public static $file_rootdir;
	
	private static $user;
	
	public static function setUser(AuthUser $user)
	{
		self::$user = $user;
	}
	
	/**
	 * Get the user to authenticate the rights with
	 *
	 * @return AuthUser
	 */
	public static function getUser()
	{
		if(null === self::$user) {
			self::setUser(Auth::singleton()->user);
		}
		return self::$user;
	}
	
	/**
	 * Get a folder using the given folder_class
	 *
	 * @param int $id
	 * @param bool $load
	 * @return FileCabinet_Folder
	 */
	public static function folderFactory($id = 0, $load = true)
	{
		return ActiveRecord::factory(self::$folder_class, $id, $load);
	}
	
	/**
	 * Get a file using the given file_class
	 *
	 * @param int $id
	 * @param bool $load
	 * @return FileCabinet_File
	 */
	public static function fileFactory($id = 0, $load = true)
	{
		return ActiveRecord::factory(self::$file_class, $id, $load);
	}
}
