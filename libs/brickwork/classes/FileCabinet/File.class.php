<?php
/**
 * FileCabinet File
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @package FileCabinet
 */
class FileCabinet_File extends ActiveRecord implements FileCabinet_FileInterface
{
	protected $_table_name = 'file_cabinet_file';

	/**
	 * Cached user database row
	 * @var object
	 */
	protected $_user;
	
	/**
	 * Custom hasAccess function
	 *
	 * @param string $right
	 * @param bool $recursive
	 * @param AuthUser|int $user_or_role
	 * @return bool
	 */
	public function hasAccess($right = 'read', $recursive = true, $user_or_role = null)
	{
		if($parent = $this->getParent()) {
			return $parent->hasAccess($right, $recursive, $user_or_role);
		}
		// If there is no parent folder there are no rights to be configured
		return true;
	}
	
	/**
	 * Delete folder
	 *
	 * @param bool $delete_file
	 * @return bool
	 */
	public function delete($delete_file = true)
	{
		if(ActiveRecord::factory(get_class($this))->where('filename', $this['filename'])->getCount() < 2) {
			unlink(FileCabinet::$file_rootdir.'/'.$this['filename']);
			
			@rmdir(dirname(FileCabinet::$file_rootdir.'/'.$this['filename']));
		}
		
		return parent::delete();
	}
	
	/**
	 * Get the user as a standard object with id, username, displayname and email property
	 * @param string $property
	 * @return stdobject
	 */
	public function getUser($property = null)
	{
		if(is_null($this->_user)) {
			$res = DB::iQuery(sprintf("
				SELECT
					id, username, displayname, email
				FROM
					brickwork_site_user
				WHERE
					id = %d
				LIMIT 1
				",
				$this['user_id']
			));
			
			$this->_user = $res->first();
		}
		
		if(null === $property) {
			return $this->_user;
		}
		
		return isset($this->_user->{$property}) ?
			$this->_user->{$property} : null;
	}
	
	/**
	 * Returns the parent folder or null if there is no parent
	 *
	 * @return FileCabinet_FolderInterface|null
	 */
	public function getParent()
	{
		$parent = FileCabinet::folderFactory($this['folder_id']);
		return $parent->isLoaded() ? $parent : null;
	}
	
}
