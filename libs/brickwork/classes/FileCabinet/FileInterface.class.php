<?php
/**
 * Files used by the FileCabinet
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @package FileCabinet
 */
interface FileCabinet_FileInterface
{
	public function hasAccess($right = 'read', $recursive = true, $user_or_role = null);
}