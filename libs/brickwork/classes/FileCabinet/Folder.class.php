<?php
/**
 * Default folderclass used by FileCabinet
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @package FileCabinet
 */
class FileCabinet_Folder extends ActiveRecord implements FileCabinet_FolderInterface
{
	protected $_table_name = 'file_cabinet_folder';

	// cache variables
	private $_children;
	private $_files;
	private $_path;
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FileCabinet/FileCabinet_FolderInterface#hasAccess($right, $recursive, $user_or_role)
	 */
	public function hasAccess($right = 'read', $recursive = true, $user_or_role = null)
	{
		if($user_or_role !== null && !$user_or_role instanceof SiteUser && !is_int($user_or_role)) {
			throw new InvalidArgumentException('user_or_role should either be '.
				'of type SiteUser or integer');
		}
		
		if(!$this->isLoaded()) {
			throw new Exception("Cannot check acces on a nonexisting folder");
		}
		
		if(null === $user_or_role) {
			$user_or_role = FileCabinet::getUser();
			if(!$user_or_role instanceof SiteUser) {
				$user_or_role = 0;
			}
		}
		
		$has_access = false;
		$role_ids = array();
		if($user_or_role instanceof SiteUser) {
			$res = DB::iQuery(sprintf("
				SELECT
					auth_role_id
				FROM
					brickwork_site_user_auth_role
				WHERE
					site_user_id = %d
				",
				$user_or_role->auth_id
			));
			
			foreach($res as $row) {
				$role_ids[] = $row->auth_role_id;
			}
		}
		else {
			$role_ids[]= (int) $user_or_role;
		}
		
		if($role_ids) {
			$res = DB::iQuery(sprintf("
				SELECT
					rights.role_id
				FROM
					file_cabinet_right AS rights
				WHERE
					rights.role_id IN (%s)
				AND
					rights.folder_id = %d
				AND
					FIND_IN_SET(%s, rights.rights)
				LIMIT 1",
				implode(', ', $role_ids),
				$this['id'],
				DB::quoteValue($right)
			));
			
			$has_access = (1 === count($res));
		}
		
		return $has_access;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FileCabinet/FileCabinet_FolderInterface#setAccess($rights, $role_id)
	 */
	public function setAccess(array $rights, $role_id)
	{
		DB::iQuery(sprintf("
			INSERT INTO
				file_cabinet_right (folder_id, role_id, rights)
			VALUES
				(%d, %d, %s)
			ON DUPLICATE KEY UPDATE
				rights = VALUES(rights)
			",
			$this['id'],
			$role_id,
			DB::quoteValue($rights ? implode(',', $rights) : null)
		));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/FileCabinet/FileCabinet_FolderInterface#getParent()
	 */
	public function getParent()
	{
		if($this['parent_id']) {
			return ActiveRecord::factory(get_class($this), $this['parent_id']);
		}
		return null;
	}
	
	/**
	 * Get the children folders sorted by prio
	 *
	 * @return ActiveRecordIterator
	 */
	public function getChildren($limit = null, $offset = null)
	{
		if(is_null($this->_children)) {
			if(!isset($this['id'])) {
				$this->_children = ActiveRecord::factory(get_class($this))->
					where('parent_id', 'NULL', 'IS', false)->
					orderBy('prio', 'DESC')->
					orderBy('create_datetime', 'DESC')->
					limit($limit, $offset)->
				get();
			} else {
				$this->_children = ActiveRecord::factory(get_class($this))->
					where('parent_id', $this['id'])->
					orderBy('prio', 'DESC')->
					orderBy('create_datetime', 'DESC')->
					limit($limit, $offset)->
					get();
			}
		}
		return $this->_children;
	}
	
	/**
	 * Get the files sorted by prio
	 *
	 * @return array
	 */
	public function getFiles($limit = null, $offset = null)
	{
		if(is_null($this->_files)) {
			$this->_files = FileCabinet::fileFactory()->
				where('folder_id', $this['id'])->
				orderBy('prio', 'DESC')->
				orderBy('create_datetime', 'DESC')->
				limit($limit, $offset)->
				get();
		}
		return $this->_files;
	}
	
	/**
	 * Get the folder path up untill the root folder
	 * 
	 * @throws Exception
	 * @return array
	 */
	public function getPath()
	{
		if(is_null($this->_path)) {
			$path = array($this);
			$folder = $this;
			$ids = array($this['id']);
			while($folder->getParent() !== null) {
				$path[] = $folder = $folder->getParent();
				
				// Recursion Detection
				if(in_array($folder['id'], $ids)) {
					throw new Exception('Recursion detected on folder: '.$folder['name']);
				}
				$ids[] = $folder['id'];
			}
			$this->_path = array_reverse($path);
		}
		return $this->_path;
	}
	
	/**
	 * Get the ids of the path folders
	 *
	 * @return array
	 */
	public function getPathIds()
	{
		$folders = $this->getPath();
		$path_ids = array();
		foreach($folders as $folder) {
			$path_ids[] = $folder['id'];
		}
		return $path_ids;
	}
	
	/**
	 * Delete folder
	 *
	 * @param int $new_parent_id
	 * @return bool
	 */
	public function delete($new_parent_id = null)
	{
		if($this->isLoaded()) {
			$files = $this->getFiles();
			$children = $this->getChildren();
			
			foreach ($children as $child) {
				$child['parent_id'] = $new_parent_id;
				$child->save();
			}
			foreach ($files as $file) {
				$file['folder_id'] = $new_parent_id;
				$file->save();
			}
			
			$id = $this['id'];
			
			if(parent::delete()) {
				DB::iQuery(sprintf("
					DELETE FROM
						file_cabinet_right
					WHERE
						folder_id = %d",
					$id
				));
				return true;
			}
		}
		return false;
	}
}
