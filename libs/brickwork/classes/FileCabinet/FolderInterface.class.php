<?php
/**
 * Folder used by the FileCabinet
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @package FileCabinet
 */
interface FileCabinet_FolderInterface
{
	/**
	 * Get the parent folder
	 *
	 * @return FileCabinet_Folder
	 */
	public function getParent();
	
	/**
	 * Get the children folders sorted by prio
	 *
	 * @return array
	 */
	public function getChildren($limit = null, $offset = null);

	/**
	 * Get the files sorted by prio
	 *
	 * @return array
	 */
	public function getFiles($limit = null, $offset = null);

	/**
	 * Get the folders up untill the root folder
	 *
	 * @return array
	 */
	public function getPath();

	/**
	 * Get the ids of the path folders
	 *
	 * @return array
	 */
	public function getPathIds();

	/**
	 * Delete folder and give the underlying folders a new parent or delete them.
	 *
	 * @param int $new_parent_id if null just delete the child folders
	 * @return bool
	 */
	public function delete($new_parent_id = null);
	
	/**
	 * Checks for access on the folder
	 *
	 * @param string $right
	 * @param bool $recursive
	 * @param SiteUser|int $user_or_role
	 * @return bool
	 */
	public function hasAccess($right = 'read', $recursive = true, $user_or_role = null);
	
	/**
	 * Sets the acces on the folder for the given role
	 * 
	 * @param array $rights
	 * @param int $role_id
	 * @return void
	 */
	public function setAccess(array $rights, $role_id);
}