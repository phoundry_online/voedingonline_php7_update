<?php
/**
 * URL type wrapper class
 *
 * Class om URL's te kunnen maken. Op deze manier zin url's als type
 * geimplementeerd en kunnen we makkelijk validators maken en andere leuke
 * onzin, zoals dooie linkcheckers enzovoorts
 *
 * @TODO PHP Doc above functions and private/public declarations
 * @author Jørgen Teunis
 * @last_modified 2008/08/26
 * @package Web
 */
class Domain
{
	
	public $domain;
	
	function Domain($host)
	{
		$this->domain = strtolower(preg_replace('/.*?(\w+\.[a-z]+)$/i', '\1', $host));
	}
	
	function __toString()
	{
		return $this->domain;
	}
}


class Url {
var $scheme;
var $host;
var $port;
var $user;
var $pass;
var $path;
var $query;
var $fragment;
var $method = 'GET';
var $Domain = NULL;

   function __construct($url=NULL){
      if(!is_null($url)){
         $url = substr($url, 0, 2024);
         $nibbles = parse_url($url);
         $this->scheme  = &$nibbles['scheme'];
         $this->host    = &$nibbles['host'];
         $this->port    = !isset($nibbles['port']) ? 80 : $nibbles['port'];
         $this->user    = &$nibbles['user'];
         $this->pass    = &$nibbles['pass'];
         $this->path    = &$nibbles['path'];
         $this->query   = !empty($nibbles['query']) ? explode('&',$nibbles['query']) : array();
         $this->fragment= &$nibbles['fragment'];
         $this->Domain  = new Domain($this->host);
      }
   }

   public function getScheme(){
      return $this->scheme;
   }

   public function getUrl(){
      $newUrl = array();
      $newUrl[]=(empty($this->scheme) ? 'http' : $this->scheme ).'://';
      $newUrl[]=$this->host;
      if($this->port!=80){
         $newUrl[]=!empty($this->port) ? ":{$this->port}" : '';
      }
      $newUrl[]=$this->path;
      if(count($this->query)>0){
         $newUrl[]='?'.implode('&',$this->query);
      }
      return implode($newUrl);
   }

   public function __toString(){
	return $this->getUrl();
   }

   public function getBaseUrl(){
      $newUrl = array();
      $newUrl[]=(empty($this->scheme) ? 'http' : $this->scheme ).'://';
      $newUrl[]=$this->host;
      if($this->port!=80){
         $newUrl[]=!empty($this->port) ? ":{$this->port}" : '';
      }
      $newUrl[]='/';
      return implode($newUrl);
   }

   public function getRobotsTxtUrl(){
      $newUrl = array();
      $newUrl[]=(empty($this->scheme) ? 'http' : $this->scheme ).'://';
      $newUrl[]=$this->host;
      if($this->port!=80){
         $newUrl[]=!empty($this->port) ? ":{$this->port}" : '';
      }
      $newUrl[]='/robots.txt';
      return implode($newUrl);
   }

	public function getUrlDir(){
      $newUrl = array();
      $newUrl[]=(empty($this->scheme) ? 'http' : $this->scheme ).'://';
      $newUrl[]=$this->host;
      if($this->port!=80){
         $newUrl[]=!empty($this->port) ? ":{$this->port}" : '';
      }
      $path = dirname($this->path).'/';
      $newUrl[]=empty($path) ? '/' : $path;
      return implode($newUrl);
   }


   public function getFilename(){
      $newUrl = array();
      $path = $this->path;
      $newUrl[]=empty($path) ? '/' : $path;
      return implode($newUrl);
   }

   // @TODO waszige functie: even nadenken wat dit precies moet doen :)
   public function getCompleteFileUrl($Url,$file){
      if(!Url::isUrl($file)){
         if(substr($file,0,1)=='/'){
            $file = substr($file,1);
            return new Url( $Url->getBaseUrl().$file );
         }
         return new Url( $Url->getUrlDir().$file );
      } else {
         return new Url( $file );
      }
   }

   public function setMethod($method){
      $valid_methods = array('GET','POST','PUT','HEAD');
      if(in_array(strtoupper($method),$valid_methods)){
         $this->method = strtoupper($method);
         return TRUE;
      } else {
         return FALSE;
      }
   }
   
   /**
    * Tests if a string is a URL.
    *
    * @param string $URL The string that is to be tested
    * @return bool TRUE if the string is a URL
    */
   static function isURL($url) {
      return (bool) filter_var($url, FILTER_VALIDATE_URL);
   }
}
