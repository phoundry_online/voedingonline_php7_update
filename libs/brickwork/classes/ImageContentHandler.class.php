<?php
if(!defined('IMAGECH_CACHE_OPTIONS')) define('IMAGECH_CACHE_OPTIONS', 'dir:///ImageContentHandler/?timeout=2592000&raw=true&sweep=true');

/**
 * Image Content Handler
 * 
 * Create images from images
 * 
 * @author Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @version 0.0.5
 * @uses ImageEditor
 * 
 * v0.0.5
 * - Improved error handing
 *
 */

class ImageContentHandler extends ContentHandler
{
	/**
	 * Path to the image
	 *
	 * @var string
	 */
	protected $image_filename;
	
	/**
	 * Path to the image directory
	 *
	 * @var string
	 */
	protected $image_dir;
	
	/**
	 * Caching object
	 *
	 * @var Cache_Dir
	 */
	protected $cache;
	
	/**
	 * Key for the image in the cache
	 *
	 * @var string MD5 hash
	 */
	protected $cache_key;
	
	/**
	 * name used in $WEBprefs array
	 *
	 * @var string
	 */
	protected $name;
	
	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
		
		$this->cache_time = 432000; // 5 days in seconds, browser cache time
		
		$this->name = array_shift($this->path);
		$this->image_filename = implode('/', $this->path);
		
		global $WEBprefs;
		
		if (isset($WEBprefs['Image']) && isset($WEBprefs['Image'][$this->name])) {
			foreach ($WEBprefs['Image'][$this->name] as $key => $value) {
				switch ($key) {
					case 'base':
						$this->image_dir = rtrim($value, '/');
						break;
				}
			}
		} else {
			if (TESTMODE) {
				header("HTTP/1.0 404 Not Found");
				die("WEBprefs['Image'] not found in class: ".__CLASS__." on line:".__LINE__);
			} else {
				$this->print404();
				exit;
			}			
		}
		
		$this->cache = Cache::factory(IMAGECH_CACHE_OPTIONS);
		
		if (!is_file($this->image_dir.'/'.$this->image_filename)) {
			if (TESTMODE) {
				header("HTTP/1.0 404 Not Found");
				die(
					"File not readable: ".$this->image_dir.'/'.
					$this->image_filename." in class: ".
					__CLASS__." on line:".__LINE__
				);
			} else {
				$this->print404();
				exit;
			}			
		}
		$this->image_mtime = filemtime(
			$this->image_dir.'/'.$this->image_filename
		);
		
		$dirname = dirname($this->image_filename);
		$dirname = empty($dirname) ? '' : $dirname.'/';
		$this->cache_key = $this->name.'/'.$dirname.md5(
			$this->image_dir.'/'.$this->image_filename.
			'|' .$this->image_mtime.
			'|'. serialize($WEBprefs['Image'][$this->name])
		);
	}
	
	public function init()
	{
		// Check if we already generated the thumbnail
		if (isset($this->cache[$this->cache_key])) {
			return true;
		}
		if (!$this->_getLock(__CLASS__.$this->cache_key, 2)) {
			return false;
		}
		if (isset($this->cache[$this->cache_key])) {
			$this->_releaseLock(__CLASS__.$this->cache_key);
			return true;
		}
		
		$ext = strtolower(substr(strrchr($this->image_filename, '.'), 1));
		try {
		$image_editor = new ImageEditor($this->image_dir.'/'.$this->image_filename, $ext);
		} catch(Exception $e) {
			$this->print404();
			return false;
		}
		set_time_limit(130);
		
		global $WEBprefs;
		if (isset($WEBprefs['Image']) && isset($WEBprefs['Image'][$this->name])) {
			foreach ($WEBprefs['Image'][$this->name] as $key => $value) {
				switch ($key) {
					case 'resize':
						if (is_array($value)) {
							list($width, $height) = $value;
						} else {
							$width = $height = $value;
						} 
						$image_editor->resize($width, $height);
						break;
					case 'resample':
						if (is_array($value)) {
							list($width, $height) = $value;
						} else {
							$width = $height = $value;
						}
						$image_editor->resample($width, $height);
						break;
					case 'fit':
						$width = $height = null;
						$use_resample = 1;
						if (is_array($value)) {
							foreach ($value as $key => $item) {
								switch($key) {
									case 0:
										$width = $item;
										break;
									case 1:
										$height = $item;
										break;
									case 2:
										$use_resample = $item;
										break;
								}
							}
						} else {
							$width = $height = $value;
						}
						if (!$height && $width) {
							$height = $width;
						}
						$image_editor->fit($width, $height, $use_resample);
						break;
					case 'crop':
						$width = $height = $left = $top = null;
						if (is_array($value)) {
							foreach ($value as $key => $item) {
								switch($key) {
									case 0:
										$width = $item;
										break;
									case 1:
										$height = $item;
										break;
									case 2:
										$left = $item;
										break;
									case 3:
										$top = $item;
										break;
								}
							}
						} else {
							$width = $height = $value;
							$top = $left = 0;
						}
						$image_editor->crop($width, $height, $left, $top);
						break;
					case 'flip':
						$image_editor->flip($value);
						break;
					case 'grayscale':
						$image_editor->greyscale();
						break;
					case 'brightness':
						$image_editor->brightness($value);
						break;
					case 'sepia':
						$image_editor->sepia($value);
						break;
					case 'sharpen':
						$image_editor->sharpen($value);
						break;
					case 'rotate':
						$image_editor->rotate($value);
						break;
					case 'jpg_quality':
						$image_editor->jpg_quality($value);
						break;
					case 'png_compression':
						$image_editor->png_compression($value);
						break;
					case 'watermark':
						$pos = '';
						$left = $top = null;
						if (is_array($value)) {
							foreach ($value as $key => $item) {
								switch($key) {
									case 0:
										$file = $item;
										break;
									case 1:
										$left = $item;
										break;
									case 2:
										$top = $item;
										break;
								}
							}
						} else {
							$file = $value;
						}
						$image_editor->watermark($file, $left, $top);
						break;
				}
			}
		}
		
		ob_start();
		try {
			$rv = $image_editor->render();
		}
		catch(Exception $e) {
			$rv = false;
		}
		
		
		if ($rv) {
			$this->cache[$this->cache_key] = ob_get_clean();
			$this->_releaseLock(__CLASS__.$this->cache_key);
			return true;
		} else {
			if (TESTMODE) {
				echo 'Image rendering failed for file: '.
					$this->image_dir.'/'.$this->image_filename.' in class: '.
					__CLASS__.' on line:'.__LINE__;
			}
			$this->_releaseLock(__CLASS__.$this->cache_key);
			return false;
		}
	}
	
	public function out()
	{
		$headers = apache_request_headers();
		
		// reset some
		// TODO reset param is not set for header calls
		header('Expires:');
		header('Pragma:');
		header('Set-Cookie:');
		header('P3P:');
		
		// set some
		header('Cache-Control: public, max-age='. $this->cache_time);
		$etag = md5($this->cache_key);
		header('Etag: "'.$etag.'"');
		header(
			'Last-Modified: '.
			gmdate('D, d M Y H:i:s \G\M\T', $this->image_mtime)
		);
		
		$if_mod_since = isset($headers['If-Modified-Since'])
			? strtotime($headers['If-Modified-Since']) : null;

		$if_none_match = isset($headers['If-None-Match'])
			? trim($headers['If-None-Match'], '"') : null;
		
		if ($this->image_mtime <= $if_mod_since
			&& ($if_none_match == null || $etag == $if_none_match)) {
			header("HTTP/1.1 304 Not Modified");
			exit;
		}
		
		// have thumb in cache?
		$output = $this->cache[$this->cache_key];
		if (!$output) {
			$this->print404();
			return;
		}

		// set cache headers
		header('Date: '.gmdate('D, d M Y H:i:s \G\M\T', $this->image_mtime));
		$ext = strtolower(substr(strrchr($this->image_filename, '.'), 1));
		switch($ext) {
			case "jpg":
			case "jpeg":
				$ext = image_type_to_mime_type(IMAGETYPE_JPEG);
				break;
			case "gif":
				$ext = image_type_to_mime_type(IMAGETYPE_GIF);
				break;
			case "png":
				$ext = image_type_to_mime_type(IMAGETYPE_PNG);
				break;
		}
		header('Content-type: '. $ext);
		
		header('Content-Length: ' . strlen($output));
		echo $output;
		exit(0);
	}
	

	/**
	 * Get a named lock
	 * @param string $name
	 * @param int $timeout
	 * @return bool
	 */
	protected function _getLock($name, $timeout = 30)
	{
		$res = DB::iQuery(
			sprintf(
				"SELECT get_lock('%s', %d) AS resultaat",
				DB::quote($name),
				$timeout
			)
		);
		
		if (!count($res)) {
			throw new Exception('The getLock query returned an empty resultset');
		}
		$res = $res->first()->resultaat;
		if (is_null($res)) {
			throw new Exception('Could not get a lock due to thread being killed');
		}
		return 1 == $res;
	}

	/**
	 * Release a named lock
	 * @param string $name
	 * @return bool
	 */
	protected function _releaseLock($name)
	{
		DB::iQuery(
			sprintf("DO release_lock('%s')", DB::quote($name))
		);
		
		return true;
	}
}
