<?php
/**
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @author Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @version 0.0.3
 * 
 */
abstract class ImageEditor_Driver
{
	
	// Reference back to the image object
	protected $image;

	// Temporary image
	protected $tmp_image;
	
	// Temporary image type
	protected $tmp_image_type;
	
	// Processing errors
	protected $errors = array();
	
	public function execute($actions)
	{
		foreach($actions as $func => $args)
		{
			if ( ! $this->$func($args))
				return FALSE;
		}

		return TRUE;
	}
	
	/**
	 * Process an image with a set of actions.
	 *
	 * @param   string   image filename
	 * @param   array    actions to execute
	 * @param   string   type	 
	 * @param   string   destination directory path
	 * @param   string   destination filename
	 * @return  boolean
	 */
	abstract public function process($image, $image_type, $actions, $type = NULL, $dir = NULL, $file = NULL);

	/**
	 * Flip an image. Valid directions are horizontal and vertical.
	 *
	 * @param   integer   direction to flip
	 * @return  boolean
	 */
	abstract function flip($direction);

	/**
	 * Crop an image. Valid properties are: width, height, top, left, sanitize_geometry.
	 *
	 * @param   array     new properties
	 * @return  boolean
	 */
	abstract function crop($properties);

	/**
	 * Resize an image. Valid properties are: width, height, and master.
	 *
	 * @param   array     new properties
	 * @return  boolean
	 */
	abstract public function resize($properties);
	
	/**
	 * Resample an image. Valid properties are: width, height, and master.
	 *
	 * @param   array     new properties
	 * @return  boolean
	 */
	abstract public function resample($properties);

	/**
	 * Rotate an image. Validate amounts are -180 to 180.
	 *
	 * @param   integer   amount to rotate
	 * @return  boolean
	 */
	abstract public function rotate($amount);

	/**
	 * Watermark 
	 *
	 * @param   array     new properties
	 * @return  boolean
	 */
	abstract public function watermark($properties);
	
	/**
	 * Sharpen and image. Valid amounts are 1 to 100.
	 *
	 * @param   integer  amount to sharpen
	 * @return  boolean
	 */
	abstract public function sharpen($amount);
	
	/**
	 * Convert an image to Greyscale
	 *
	 * @return boolean
	 */
	abstract public function greyscale();
		
	/**
	 * Convert an image to brightness. Valid amounts 1 to 100
	 *
	 * @param   integer  amount to brighten
	 * @return boolean
	 */
	abstract public function brightness($brightness);

	/**
	 * Convert an image to Sepia.
	 *
	 * @param integer amount to darken
	 * @return boolean
	 */
	abstract public function sepia($dark_it);
	
	/**
	 * Resize/crop image to "fit" exact height and width
 	 *
	 * @param   array properties
	 * 
	 * width, height, use_resample
	 * use_resample is optional, default=true
	 * 
	 * @return  boolean
	 */
	abstract public function fit($properties);
	
}
