<?php
/**
 * Class: ImageEditor_Driver_Gd
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @author Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @version 0.0.5
 * @uses GD library
 * @package ImageEditor
 * 
 * v0.0.5
 * - Changed setting the memory limit
 * - added constantes IMAGE_EDITOR_MAX_PIXELS, IMAGE_EDITOR_MAX_MEMORY
 * - added setting jpg_quality
 * - added setting png_compression
 * 
 */

if(!defined('IMAGE_EDITOR_MAX_PIXELS')) define('IMAGE_EDITOR_MAX_PIXELS', 5000);
if(!defined('IMAGE_EDITOR_MAX_MEMORY')) define('IMAGE_EDITOR_MAX_MEMORY', '256M');

class ImageEditor_Driver_Gd extends ImageEditor_Driver
{
	
	public function __construct()
	{
	}	
	
	/**
	 * Creates a temporary image and executes the given actions.
	 *
	 * @param string $image Filename
	 * @param string $image_type Imagetype
	 * @param array $actions To be executed methods
	 * @param string $type Output type
	 * @param string $dir Directory to save in (optional)
	 * @param string $file Filename to save to (optional)
	 * @return boolean
	 */
	public function process($image, $image_type, $actions, $type = NULL, $dir = NULL, $file = NULL)
	{
		$orginal_type = $this->getType($image_type);
		$typeprops = $this->getType(is_null($type) ? $image_type : $type); 
		
		// Temporary image resource
		if(!function_exists($orginal_type['inputFunc'])){
			throw new Exception("Input function: ".$orginal_type['inputFunc']." doesn't exist!");
		}
		if(!function_exists($orginal_type['outputFunc'])){
			throw new Exception("Output function: ".$orginal_type['outputFunc']." doesn't exist!");
		}
		$memory_limit_old= $this->set_memory_for_image($image);
		if (!$memory_limit_old) {
			return false;
		}
		$this->tmp_image= @$orginal_type['inputFunc']($image);
		if (!$this->tmp_image) {
			return false;
		}
		$this->tmp_image_type = ($type === NULL) ? $image_type : $type;


		// lets do some gif magic here
		if ($image_type == 'gif') {
			$tempFile = fread(fopen($image, "rb"), filesize($image));
			$gifDecoder = new ImageEditor_Driver_Gd_GIFDecoder($tempFile);
			$delay = $gifDecoder->GIFGetDelays() ? : array(0); // get the delay between each frame
			
			$editedFrames = array();
			
			foreach ($gifDecoder->GIFGetFrames() as $frame) {
				//var_dump($this->tmp_image, $frame); exit;
				$tmpFileName = tempnam(TMP_DIR, "gif_");
				file_put_contents($tmpFileName, $frame);
				$this->tmp_image = @$orginal_type['inputFunc']($tmpFileName);
				
				$status = $this->execute($actions);
				
				if ($status === false) {
					return false;
				}
				
				$tmpFileName = tempnam(TMP_DIR, "gif_thumb_");
				imagegif($this->tmp_image, $tmpFileName);
				
				$editedFrames[] = $tmpFileName;
			}
			
			// LETS MORF BACK
			$gifEncoder = new ImageEditor_Driver_Gd_GIFEncoder(
				$editedFrames,
				$delay,
				0,
				2,
				255, 255, 255,
				"url"
			);
			$this->tmp_image = $gifEncoder->GetAnimation();
			
			foreach ($editedFrames as $frameFile) {
				unlink($frameFile);
			}
		} else {
			// jpg_quality
			$jpg_quality = 85;
			if (array_key_exists('jpg_quality', $actions)) {
				$jpg_quality = (int) $actions['jpg_quality'];
				unset($actions['jpg_quality']);
			}
			
			// png_compression
			$png_compression = 3;
			if (array_key_exists('png_compression', $actions)) {
				$png_compression = (int) $actions['png_compression'];
				if ($png_compression < 0) $png_compression = 0;
				if ($png_compression > 9) $png_compression = 9;
				unset($actions['png_compression']);
			}
	
	
			$status = $this->execute($actions);
			
			// If the execute failed at some point return false
			if ($status === false) return false;
		}
		
		// If image has no file destination render the image
		if ($dir === null && $file === null) {
			header('Content-type: '. $typeprops['mime_type']);
			switch($image_type) {
				case "jpg":
				case "jpeg":
					$typeprops['outputFunc']($this->tmp_image, null, $jpg_quality);
					break;
				case "gif":
					//$typeprops['outputFunc']($this->tmp_image);
					print $this->tmp_image;
					break;
				case "png":
					$typeprops['outputFunc']($this->tmp_image, null, $png_compression);
					break;
			}	
		} else {
			switch($image_type) {
				case "jpg":
				case "jpeg":
					$typeprops['outputFunc']($this->tmp_image, $dir.$file, $jpg_quality);
					break;
				case "gif":
					//$typeprops['outputFunc']($this->tmp_image, $dir.$file);
					file_put_contents($dir.$file, $this->tmp_image); 
					break;
				case "png":
					$typeprops['outputFunc']($this->tmp_image, $dir.$file, $png_compression );
					break;
			}	
		}

		// destroy gd handle to free up some memory
		if (is_resource($this->tmp_image)) {
			imagedestroy($this->tmp_image);
		}
		unset($this->tmp_image);
		
		// restore memory limit to previous value
		if ($memory_limit_old) {
			ini_set('memory_limit', $memory_limit_old );
		}
		return $status;
	}
	
	/**
	 * Cuts out the given area from the image
	 *
	 * @param array $prop left, top, width, height, sanitize_geometry
	 * @return boolean
	 */
	public function crop($prop)
	{
		// Sanitize and normalize the properties into geometry
		if ($prop['sanitize_geometry']) {
			$prop = $this->sanitize_geometry($prop);
		}
		
		$new_resource = $this->createImage($prop['width'], $prop['height']);
		
		imagecopyresized($new_resource, $this->tmp_image, 0, 0, $prop['left'], $prop['top'], $prop['width'], $prop['height'], $prop['width'], $prop['height']);
		imagedestroy($this->tmp_image);
		
		$this->tmp_image = $new_resource;
		
		return TRUE;
	}
	
	/**
	 * Resize the image
	 *
	 * @param array $prop width, height
	 * @return boolean
	 */
	public function resize($prop)
	{
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);
		
		$factor = ImageEditor::calcFactor($width, $height, (float) $prop['width'], (float) $prop['height']);
		
		$new_width = (int)($width / $factor);
		$new_height = (int)($height / $factor);	
		$new_resource = $this->createImage($new_width, $new_height);
		
		imagecopyresized($new_resource, $this->tmp_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		imagedestroy($this->tmp_image);
		
		$this->tmp_image = $new_resource;
		
		return TRUE;
	}
	
	/**
	 * Resample the image
	 *
	 * @param array $prop width, height
	 * @return boolean
	 */
	public function resample($prop)
	{
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);
		
		$factor = ImageEditor::calcFactor($width, $height, (float) $prop['width'], (float) $prop['height']);
		
		$new_width = (int)($width / $factor);
		$new_height = (int)($height / $factor);	
		$new_resource = $this->createImage($new_width, $new_height);
				                
		imagecopyresampled($new_resource, $this->tmp_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		imagedestroy($this->tmp_image);
		
		$this->tmp_image = $new_resource;
		
		return TRUE;
	}
	
	/**
	 * Flips the image, either horizontally or vertically.
	 *
	 * @param int $direction
	 * @return boolean
	 */
	public function flip($direction)
	{
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);
		$new_image = $this->createImage($width, $height);
				
		if($direction == ImageEditor::VERTICAL)
			for ($y = 0; $y < $height; ++$y) {
				imagecopy(
					$new_image, $this->tmp_image,
					0, $y,
					0, $height - $y - 1,
					$width, 1
					);
				}
		
		if($direction == ImageEditor::HORIZONTAL){
			for ($x = 0; $x < $width; ++$x) {
			imagecopy(
				$new_image, $this->tmp_image,
				$x, 0,
                $width - $x - 1, 0,
                1, $height
                );
			}
		}
		imagedestroy($this->tmp_image);
		
		$this->tmp_image = $new_image;		
		return TRUE;
	}
	
	/**
	 * Rotate the image by the given degrees
	 *
	 * @param string $amount
	 * @return boolean
	 */
	public function rotate($amount)
	{
		$this->tmp_image = imagerotate ($this->tmp_image, $amount, 0);
		return TRUE;
	}
	
	/**
	 * Watermark image
	 *
	 * @param array $prop image_file, postion, left, top
	 * @return boolean
	 */
	public function watermark($prop)
	{
//		$ext= strtolower(pathinfo($prop['image_file'], PATHINFO_EXTENSION));
		$ext = strtolower(substr(strrchr($prop['image_file'], '.'),1));
		if ($ext== 'gif') {
			if (!function_exists('imagecreatefromgif')) {
				return(false);
			} else {
				$filter = @imagecreatefromgif($prop['image_file']);
				if (!$filter) {
					return(false);
				}
			}
		}
		else if ($ext== 'jpg') {
			if (!function_exists('imagecreatefromjpeg')) {
				return(false);
			} else {
				$filter = @imagecreatefromjpeg($prop['image_file']);
				if (!$filter) {
					return(false);
				}
			}
		}
		else if ($ext== 'png') {
			if (!function_exists('imagecreatefrompng')) {
				return(false);
			} else {
				$filter = @imagecreatefrompng($prop['image_file']);
				if (!$filter) {
					return(false);
				}
			}
		}

		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);
		$watermark_width  = imagesx($filter);
		$watermark_height = imagesy($filter);
		$watermark_x = 0;
		$watermark_y = 0;
		if (is_numeric($prop['left'])) {
			if ($prop['left'] < 0) {
				$watermark_x = $width - $watermark_width + $prop['left'];
			} else {
				$watermark_x = $prop['left'];
			}
		} else {
			if ($prop['left'][0]== 'r') {
				$watermark_x = $width - $watermark_width;
			} else if ($prop['left'][0]== 'l') {
				$watermark_x = 0;
			} else {
				$watermark_x = ($width - $watermark_width) / 2;
			}
		}
		if (is_numeric($prop['top'])) {
			if ($prop['top'] < 0) {
				$watermark_y = $height - $watermark_height + $prop['top'];
			} else {
				$watermark_y = $prop['top'];
			}
		} else {
			if ($prop['top'][0]== 'b') {
				$watermark_y = $height - $watermark_height;
			} else if ($prop['left'][0]== 't') {
				$watermark_y = 0;
			} else {
				$watermark_y = ($height - $watermark_height) / 2;
			}
		}
		imagecopyresampled ($this->tmp_image, $filter, $watermark_x, $watermark_y, 0, 0, $watermark_width, $watermark_height, $watermark_width, $watermark_height);
		
		return TRUE;
	}
	
	/**
	 * Sharpen the image by the given amount
	 *
	 * @param integer $amount
	 * @return boolean
	 */
	public function sharpen($amount)
	{
		$img =& $this->tmp_image;
		$radius	= 0.5;
		$threshold = 3;	
	    // $img is an image that is already created within php using 
	    // imgcreatetruecolor. No url! $img must be a truecolor image. 
	
	    // Attempt to calibrate the parameters to Photoshop: 
	    if ($amount > 500)    $amount = 500; 
	    $amount = $amount * 0.016; 
	    if ($radius > 50)    $radius = 50; 
	    $radius = $radius * 2; 
	    if ($threshold > 255)    $threshold = 255; 
	     
	    $radius = abs(round($radius));     // Only integers make sense. 
	    if ($radius == 0) { 
	        return $img; imagedestroy($img);        } 
	    $w = imagesx($img); $h = imagesy($img); 
	    $imgCanvas = $this->createImage($w, $h); 
	    $imgBlur = $this->createImage($w, $h); 
	     
	
	    // Gaussian blur matrix: 
	    //                         
	    //    1    2    1         
	    //    2    4    2         
	    //    1    2    1         
	    //                         
	    ////////////////////////////////////////////////// 
	         
	
	    if (function_exists('imageconvolution')) { // PHP >= 5.1  
	            $matrix = array(  
	            array( 1, 2, 1 ),  
	            array( 2, 4, 2 ),  
	            array( 1, 2, 1 )  
	        );  
	        imagecopy ($imgBlur, $img, 0, 0, 0, 0, $w, $h); 
	        imageconvolution($imgBlur, $matrix, 16, 0);  
	    }  
	    else {  
	
	    // Move copies of the image around one pixel at the time and merge them with weight 
	    // according to the matrix. The same matrix is simply repeated for higher radii. 
	        for ($i = 0; $i < $radius; $i++)    { 
	            imagecopy ($imgBlur, $img, 0, 0, 1, 0, $w - 1, $h); // left 
	            imagecopymerge ($imgBlur, $img, 1, 0, 0, 0, $w, $h, 50); // right 
	            imagecopymerge ($imgBlur, $img, 0, 0, 0, 0, $w, $h, 50); // center 
	            imagecopy ($imgCanvas, $imgBlur, 0, 0, 0, 0, $w, $h); 
	
	            imagecopymerge ($imgBlur, $imgCanvas, 0, 0, 0, 1, $w, $h - 1, 33.33333 ); // up 
	            imagecopymerge ($imgBlur, $imgCanvas, 0, 1, 0, 0, $w, $h, 25); // down 
	        } 
	    } 
	
	    if($threshold>0){ 
	        // Calculate the difference between the blurred pixels and the original 
	        // and set the pixels 
	        for ($x = 0; $x < $w-1; $x++)    { // each row
	            for ($y = 0; $y < $h; $y++)    { // each pixel 
	                     
	                $rgbOrig = ImageColorAt($img, $x, $y); 
	                $rOrig = (($rgbOrig >> 16) & 0xFF); 
	                $gOrig = (($rgbOrig >> 8) & 0xFF); 
	                $bOrig = ($rgbOrig & 0xFF); 
	                 
	                $rgbBlur = ImageColorAt($imgBlur, $x, $y); 
	                 
	                $rBlur = (($rgbBlur >> 16) & 0xFF); 
	                $gBlur = (($rgbBlur >> 8) & 0xFF); 
	                $bBlur = ($rgbBlur & 0xFF); 
	                 
	                // When the masked pixels differ less from the original 
	                // than the threshold specifies, they are set to their original value. 
	                $rNew = (abs($rOrig - $rBlur) >= $threshold)  
	                    ? max(0, min(255, ($amount * ($rOrig - $rBlur)) + $rOrig))  
	                    : $rOrig; 
	                $gNew = (abs($gOrig - $gBlur) >= $threshold)  
	                    ? max(0, min(255, ($amount * ($gOrig - $gBlur)) + $gOrig))  
	                    : $gOrig; 
	                $bNew = (abs($bOrig - $bBlur) >= $threshold)  
	                    ? max(0, min(255, ($amount * ($bOrig - $bBlur)) + $bOrig))  
	                    : $bOrig; 
	                 
	                 
	                             
	                if (($rOrig != $rNew) || ($gOrig != $gNew) || ($bOrig != $bNew)) { 
	                        $pixCol = ImageColorAllocate($img, $rNew, $gNew, $bNew); 
	                        ImageSetPixel($img, $x, $y, $pixCol); 
	                    } 
	            } 
	        } 
	    } 
	    else{ 
	        for ($x = 0; $x < $w; $x++)    { // each row 
	            for ($y = 0; $y < $h; $y++)    { // each pixel 
	                $rgbOrig = ImageColorAt($img, $x, $y); 
	                $rOrig = (($rgbOrig >> 16) & 0xFF); 
	                $gOrig = (($rgbOrig >> 8) & 0xFF); 
	                $bOrig = ($rgbOrig & 0xFF); 
	                 
	                $rgbBlur = ImageColorAt($imgBlur, $x, $y); 
	                 
	                $rBlur = (($rgbBlur >> 16) & 0xFF); 
	                $gBlur = (($rgbBlur >> 8) & 0xFF); 
	                $bBlur = ($rgbBlur & 0xFF); 
	                 
	                $rNew = ($amount * ($rOrig - $rBlur)) + $rOrig; 
	                    if($rNew>255){$rNew=255;} 
	                    elseif($rNew<0){$rNew=0;} 
	                $gNew = ($amount * ($gOrig - $gBlur)) + $gOrig; 
	                    if($gNew>255){$gNew=255;} 
	                    elseif($gNew<0){$gNew=0;} 
	                $bNew = ($amount * ($bOrig - $bBlur)) + $bOrig; 
	                    if($bNew>255){$bNew=255;} 
	                    elseif($bNew<0){$bNew=0;} 
	                $rgbNew = ($rNew << 16) + ($gNew <<8) + $bNew; 
	                    ImageSetPixel($img, $x, $y, $rgbNew); 
	            } 
	        } 
	    } 
	    imagedestroy($imgCanvas); 
	    imagedestroy($imgBlur); 

		return TRUE;
	}
	
	/**
	 * Converts the image to greyscale.
	 * 
	 * @param boolean $palette
	 * @return boolean
	 */
	public function greyscale()
	{
		
		
		if($this->tmp_image_type == 'gif'){
			
			// if we don't have a palette, we convert it
			if( !($t = imagecolorstotal($this->tmp_image)) )
			{
				$t = 256;
				imagetruecolortopalette($this->tmp_image, true, $t);
			}
			
			for($c=0;$c<$t;$c++)
			{
				$dstRGB = imagecolorsforindex( $this->tmp_image, $c );
				
				//$dstA = ($dstRGB >> 24) << 1;
				$dstR = $dstRGB["red"];// >> 16 & 0xFF;
				$dstG = $dstRGB["green"];// >> 8 & 0xFF;
				$dstB = $dstRGB["blue"];// & 0xFF;
					
				$newC = ( $dstR + $dstG + $dstB )/3;
				
				imagecolorset($this->tmp_image, $c, $newC, $newC, $newC );
			}
			
			return true;
		}
		/*else{
			$imgW = imagesx( $this->tmp_image );
			$imgH = imagesy( $this->tmp_image );
			
			for($y=0;$y<$imgH;$y++)
			{
				for($x=0;$x<$imgW;$x++)
				{
					$dstRGB = imagecolorat($this->tmp_image, $x, $y);
		
					//$dstA = ($dstRGB >> 24) << 1;
					$dstR = $dstRGB >> 16 & 0xFF;
					$dstG = $dstRGB >> 8 & 0xFF;
					$dstB = $dstRGB & 0xFF;
					
					$newC = ( $dstR + $dstG + $dstB )/3;
						
					$newRGB = imagecolorallocate($this->tmp_image, $newC, $newC, $newC );
					imagesetpixel($this->tmp_image, $x, $y, $newRGB );
				}
			}
		}*/
		return imagefilter ($this->tmp_image, IMG_FILTER_GRAYSCALE);
	}
	
	/**
	 * Change the image's brightness
	 * 
	 * @param integer brightness value from 1 to 100 
	 * @return boolean
	 */
	public function brightness($brightness)
	{
		return imagefilter ($this->tmp_image, IMG_FILTER_BRIGHTNESS, (int)$brightness);
	}

	/**
	 * Converts the image to sepia
	 *
	 * @param int $dark_it by how much it should be darked
	 * @return boolean
	 */
	public function sepia($dark_it)
	{
		if($this->tmp_image_type == 'gif'){
			return FALSE;
		}
		
		$imgW = imagesx( $this->tmp_image );
		$imgH = imagesy( $this->tmp_image );
		
		for($y=0;$y<$imgH;$y++)
		{
			for($x=0;$x<$imgW;$x++)
			{
				$dstRGB = imagecolorat($this->tmp_image, $x, $y);
	
				//$dstA = ($dstRGB >> 24) << 1;
				$dstR = $dstRGB >> 16 & 0xFF;
				$dstG = $dstRGB >> 8 & 0xFF;
				$dstB = $dstRGB & 0xFF;
				
				$newR = ($dstR * 0.393 + $dstG * 0.769 + $dstB * 0.189 ) - $dark_it;
				$newG = ($dstR * 0.349 + $dstG * 0.686 + $dstB * 0.168 ) - $dark_it;
				$newB = ($dstR * 0.272 + $dstG * 0.534 + $dstB * 0.131 ) - $dark_it;
				
				$newR = ($newR>255) ? 255 : ( ($newR<0) ? 0 : $newR );
				$newG = ($newG>255) ? 255 : ( ($newG<0) ? 0 : $newG );
				$newB = ($newB>255) ? 255 : ( ($newB<0) ? 0 : $newB );
				
				//echo "{$newR}, {$newG}, {$newB}<br>";
					
				$newRGB = imagecolorallocate($this->tmp_image, $newR, $newG, $newB );
				imagesetpixel($this->tmp_image, $x, $y, $newRGB );
			}
		}
		
		return TRUE;
	}
	
	/**
	 * Sanitizes the geometry used in crop()
	 *
	 * @param array $geometry
	 * @return array
	 */
	protected function sanitize_geometry($geometry)
	{
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);

		// Width and height cannot exceed current image size
		$maxWidth  = min($geometry['width'], $width);
		$maxHeight = min($geometry['height'], $height);
		
		$factor = ImageEditor::calcFactor($geometry['width'], $geometry['height'], $maxWidth, $maxHeight);
		$geometry['width'] = $geometry['width'] / $factor;
		$geometry['height'] = $geometry['height'] / $factor;
		
		
		if(is_string($geometry['top'])) {
			switch($geometry['top'])
			{
				case 'center':
					$geometry['top'] = floor(($height / 2) - ($geometry['height'] / 2));
				break;
				case 'top':
					$geometry['top'] = 0;
				break;
				case 'bottom':
					$geometry['top'] = $height - $geometry['height'];
				break;
			}
		}

		if(is_string($geometry['left'])) {
			switch($geometry['left'])
			{
				case 'center':
					$geometry['left'] = floor(($width / 2) - ($geometry['width'] / 2));
				break;
				case 'left':
					$geometry['left'] = 0;
				break;
				case 'right':
					$geometry['left'] = $width - $geometry['height'];
				break;
			}
		}
		
		return $geometry;
	}
	
	/**
	 * Gets the saveFunc, outputFunc, inputFunc and mime_type of the given type
	 *
	 * @param string $type
	 * @return array
	 */
	protected function getType($type)
	{
		$r = array();
		
		switch($type) {
			case "jpg":
			case "jpeg":
				$r['saveFunc']	= 'imagejpeg';
				$r['outputFunc']= 'imagejpeg';
				$r['inputFunc'] = 'imagecreatefromjpeg';
				$r['mime_type'] =  image_type_to_mime_type(IMAGETYPE_JPEG);
			break;
			case "gif":
				$r['saveFunc']	= 'imagegif';
				$r['outputFunc']= 'imagegif';
				$r['inputFunc'] = 'imagecreatefromgif';
				$r['mime_type'] =  image_type_to_mime_type(IMAGETYPE_GIF);
			break;
			case "png":
				$r['saveFunc']	= 'imagepng';
				$r['outputFunc']= 'imagepng';
				$r['inputFunc']	= 'imagecreatefrompng';
				$r['mime_type']	=  image_type_to_mime_type(IMAGETYPE_PNG);
			break;
		}
		
		return $r;
		
	}

	/**
	 * Resize/sample and crop the image to "fit" width/height
	 *
	 * @param array $prop width, height, use_resample
	 * @return boolean
	 */
	public function fit($prop)
	{
		$use_resample= isset($prop['use_resample']) ? $prop['use_resample'] : true; 
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);
		
		$factor_width = ($width / (float)$prop['width']);
		$factor_height = ($height / (float)$prop['height']);
		$factor = ($factor_width < $factor_height) ? $factor_width : $factor_height;
				
		$new_width= (int)($width / $factor);
		$new_height= (int)($height / $factor);	
		if ($new_width < $prop['width']) {
			$new_width= $prop['width'];
		}
		if ($new_height < $prop['height']) {
			$new_height= $prop['height'];
		}
		$new_resource = $this->createImage($new_width, $new_height);
		
		if ($use_resample) {
			imagecopyresampled($new_resource, $this->tmp_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		}
		else {
			imagecopyresized($new_resource, $this->tmp_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		}
		imagedestroy($this->tmp_image);
		
		$this->tmp_image = $new_resource;
		
		$new_resource = $this->createImage($prop['width'], $prop['height']);
		
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);
		$top = floor(($height / 2) - ($prop['height'] / 2));
		$left = floor(($width / 2) - ($prop['width'] / 2));
		
		imagecopyresized($new_resource, $this->tmp_image, 0, 0, $left, $top, $prop['width'], $prop['height'], $prop['width'], $prop['height']);
		imagedestroy($this->tmp_image);
		
		$this->tmp_image = $new_resource;
		return TRUE;
	}

	/**
	 * Create an image with a transparent background
	 * 
	 * @param int $width
	 * @param int $height
	 * @return resource a valid GD resource
	 */
	private function createImage($width, $height)
	{
		$resource = imagecreatetruecolor($width, $height);
		imagealphablending($resource, true);
		imagesavealpha($resource, true);
		$bg = imagecolorallocatealpha($resource, 255, 255, 255, 127);
		imagefill($resource, 0, 0, $bg);
		if ($this->tmp_image_type !== 'gif') {
			imagealphablending($resource, false);
		} else {
			imagecolortransparent($resource, $bg);
		}
		return $resource;
	}
	
	/**
	 * check image width and height
	 * if larger that IMAGE_EDITOR_MAX_PIXELS 
	 * or when image info is not returned then fail
	 * 
	 * when size is OK than set memory limit to IMAGE_EDITOR_MAX_MEMORY
	 *
	 * @param string $filename
	 * @return string with memory limit or null when failed
	 */
	private function set_memory_for_image( $filename ) {
	    $imageInfo= getimagesize($filename);
	    if ($imageInfo) {
		    $pix= max($imageInfo[0], $imageInfo[1]);
		    if ($pix >= IMAGE_EDITOR_MAX_PIXELS) {
		    	return null;
		    }
	    }
	    else {
	    	return null;
	    }
		$memoryLimitMB= ini_get('memory_limit');
        ini_set('memory_limit', IMAGE_EDITOR_MAX_MEMORY);
        
        return $memoryLimitMB;
	}
}
