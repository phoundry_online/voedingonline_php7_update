<?php
/**
 * Image Formatter class which uses config options in $WEBprefs['Image']
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 */
class ImageEditor_Formatter
{
	/**
	 * @var string The name of the key in $WEBprefs['Image']
	 */
	protected $_name;
	
	/**
	 * @var array The contents of $WEBprefs['Image'][$name]
	 */
	protected $_options;
	
	/**
	 * @var string  The contents of $WEBprefs['Image'][$name]['base']
	 */
	protected $_image_dir;
	
	/**
	 * @var string path to the directory where the cache files are stored
	 */
	protected $_cache_dir;
	
	/**
	 * Creates a new formatter
	 * @throws Exception
	 * @param string $name
	 * @param string $cache_dir
	 * @param string $cache_url
	 * @param array $options Leave empty to fetch from $WEBprefs automagically
	 */
	public function __construct($name, $cache_dir, $options = null)
	{
		$this->_name = (string) $name;
		$cache_dir = rtrim($cache_dir, '/');
		
		if(!is_dir($cache_dir) && !@mkdir($cache_dir, 0777, true)) {
			throw new Exception('Unable to create '.$cache_dir);
		}
		
		$this->_cache_dir = $cache_dir;
		
		if(null === $options) {
			global $WEBprefs;
			if(!isset($WEBprefs['Image'], $WEBprefs['Image'][$this->_name])) {
				throw new Exception($name.' could not be found in $WEBprefs["Image"]');
			}
			$options = $WEBprefs['Image'][$name];
		}
		
		if(!isset($options['base'])) {
			throw new Exception('Options is missing \'base\' key');
		}
		
		$this->_options = $options;
		$this->_image_dir = rtrim($this->_options['base'], '/');
		
		if(!file_exists($this->_image_dir)) {
			throw new Exception("Base image directory does not exist");
		}
	}
	
	/**
	 * Formats the image to the configured format and returns its cacheName
	 * @param string $image
	 * @param int $timeout Amount of seconds to wait for the lock
	 * @return bool|string On success it returns the result from getCacheName
	 */
	public function format($image, $timeout = 0)
	{
		if(!$this->_imageExists($image)) {
			throw new Exception("Image ".$image." could not be found");
		}
		
		// Check if we already generated the image
		if($cache_name = $this->_cacheExists($image)) {
			return $cache_name;
		}
		
		if(!$this->_getLock(__CLASS__.$cache_name, $timeout)) {
			return false;
		}
		
		if(!($cache_name = $this->_cacheExists($image))) {
			$cache_name = $this->_createCache($image);
		}
		$this->_releaseLock(__CLASS__.$cache_name);
		return $cache_name;
	}
	
	/**
	 * Checks if the image can be found in the configured image directory
	 * @param string $image
	 * @return bool
	 */
	protected function _imageExists($image)
	{
		return is_readable($this->_image_dir.'/'.$image);
	}
	
	/**
	 * Checks if the image already has a valid cachefile for this format
	 * Make sure that $image exists
	 * @param string $image
	 * @return bool|string On success it returns the result from getCacheName
	 */
	protected function _cacheExists($image)
	{
		$cache_name = $this->_getCacheName($image);
		$image_mtime = filemtime($this->_image_dir.'/'.$image);
		return (file_exists($this->_cache_dir.'/'.$cache_name) &&
			filemtime($this->_cache_dir.'/'.$cache_name) > $image_mtime) ? 
			$cache_name : false;
	}
	
	/**
	 * Creates the name that will be used for caching the image
	 * Make sure that $image exists
	 * @param string $image
	 * @return string
	 */
	protected function _getCacheName($image)
	{
		$image_mtime = filemtime($this->_image_dir.'/'.$image);
		$dirname = dirname($image);
		$dirname = empty($dirname) ? '' : $dirname.'/';
		$ext = strtolower(substr(strrchr($image, '.'), 1));
		
		return $this->_name.'/'.$dirname.md5($this->_image_dir.'/'.$image.'|'.$image_mtime.'|'.
			serialize($this->_options)).'.'.$ext;
	} 
	
	/**
	 * Creates a cachefile for the image even tough it may already exist
	 * @param string $image
	 * @return bool|string On success it returns the result from getCacheName
	 */
	protected function _createCache($image)
	{
		if($this->_imageExists($image)) {
			$cache_name = $this->_getCacheName($image);
			$cache_file = $this->_cache_dir.'/'.$cache_name;
			$dirname = dirname($cache_file);
			
			if(!is_dir($dirname) && !@mkdir($dirname, 0777, true)) {
				throw new Exception('Cannot create '.$dirname);
			}
			
			$ext = strtolower(substr(strrchr($image, '.'), 1));
			$image_editor = new ImageEditor($this->_image_dir.'/'.$image, $ext);
			
			foreach($this->_options as $key => $value) {
				switch ($key) {
					case 'resize':
					case 'resample':
						if(is_array($value)) {
							list($width, $height)= $value;
						}
						else {
							$width = $height = $value;
						}
						$image_editor->{$key}($width, $height);
						break;
					case 'fit':
						$width= $height= null;
						$use_resample= 1;
						if (is_array($value)) {
							foreach($value as $key => $item) {
								switch($key) {
									case 0: $width= $item; break;																		
									case 1: $height= $item; break;																		
									case 2: $use_resample= $item; break;																		
								}
							}
						}
						else {
							$width= $height= $value;
						}
						if (!$height && $width) {
							$height= $width;
						}
						$image_editor->fit($width, $height, $use_resample);
						break;
					case 'crop':
						$width= $height= $left= $top= null;
						if (is_array($value)) {
							foreach($value as $key => $item) {
								switch($key) {
									case 0: $width= $item; break;																		
									case 1: $height= $item; break;																		
									case 2: $left= $item; break;																		
									case 3: $top= $item; break;																		
								}
							}
						}
						else {
							$width= $height= $value;
							$top= $left= 0;
						}
						$image_editor->crop($width, $height, $left, $top);
						break;
					case 'grayscale':
						$image_editor->greyscale();
						break;
					case 'flip':
					case 'brightness':
					case 'sepia':
					case 'sharpen':
					case 'rotate':
					case 'jpg_quality':
					case 'png_compression':
						$image_editor->{$key}($value);
						break;
					case 'watermark':
						$pos= '';
						$left= $top= null;
						if (is_array($value)) {
							foreach($value as $key => $item) {
								switch($key) {
									case 0: $file= $item; break;																		
									case 1: $left= $item; break;																		
									case 2: $top= $item; break;																		
								}
							}
						}
						else {
							$file= $value;
						}
						$image_editor->watermark($file, $left, $top);
						break;
				}
			}
			
			if(!$image_editor->save($cache_file)) {
				throw new Exception('Image rendering failed for file: '.$cache_file);
			}
			
			return $cache_name;
		}
		return null;
	}

	/**
	 * Get a named lock
	 * @param string $name
	 * @param int $timeout
	 * @return bool
	 */
	protected function _getLock($name, $timeout = 30)
	{
		return DB::getLock($name, $timeout);
	}

	/**
	 * Release a named lock
	 * @param string $name
	 * @return bool
	 */
	protected function _releaseLock($name)
	{
		return DB::releaseLock($name);
	}
}
