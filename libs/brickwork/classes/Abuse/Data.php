<?php

/**
 * Abuse Report Data class
 * 
 * @author		Bart Lagerweij <bart.lagerweij>
 * @copyright	Web Power
 * @version		0.0.1
 * @package		ReportAbuse
 * 
 * @uses: locale.php, PH.class.php
 *
 */

class Abuse_Data extends ORM
{
	protected $_table_name= 'brickwork_abuse_report';

	/**
	 * Notify users of new record using phoundry notify table
	 */
	public function notifyUsers($tid= null) {
		require_once INCLUDE_DIR.'functions/locale.php';
		
		if (!$tid) {
			$tid= PhoundryTools::getTableTID('brickwork_abuse_report');	
		}
		if (!$tid) {
			return false;
		}
		
		$sql= sprintf("
			SELECT u.e_mail FROM phoundry_user u
			INNER JOIN phoundry_notify n ON n.user_id=u.id
			WHERE n.keyval='any' AND table_id=%d
		
		", (int)$tid
		);
		$res= DB::iQuery($sql);
		if ($res) {
			$rcpt= $res->getSelect('e_mail');
		}
		
		$phoundry_url= 'http://'.$_SERVER['HTTP_HOST'].$GLOBALS['PHprefs']['url'].'/core/preview.php?TID='.$tid
			.'&site_identifier='.(Framework::$site->identifier).'&RID='.$this['id'];
		 
		$body= sprintf("
%s %s<br />
<br />
\"%s\" %s:<br />
<div style=\"border: 1px solid #ccc\">
%s
</div>
<br />
%s:<br />
<a href=\"%s\">%s</a><br />
<br />
%s:<br />
<a href=\"%s\">%s</a>

		",
		_tr('Er is een melding van misbruik gedaan op'),
		htmlspecialchars(Framework::$site->name),
		htmlspecialchars($this['name']),
		_tr('zegt'),
		nl2br(htmlspecialchars($this['message'])),
		_tr('Klik hier om de web pagina te zien'),
		htmlspecialchars($this['url']),
		htmlspecialchars($this['url']),
		_tr('Klik hier om de melding in phoundry te zien'),
		htmlspecialchars($phoundry_url),
		htmlspecialchars($phoundry_url)
		);
		
		global $PHprefs;
		require_once $PHprefs['distDir']. '/core/include/PH.class.php';
		if (class_exists('PH', false)) {
			return PH::sendHTMLemail($rcpt,
				'Abuse report for '. Framework::$site->name,
				$body);
		}
	}
} 
