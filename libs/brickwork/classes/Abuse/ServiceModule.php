<?php
/**
 * Report Abuse Service content handler
 *
 * @author		Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @copyright	Web Power
 * @create_date	02-10-2009
 * @package		ReportAbuse
 *
 */

class Abuse_ServiceModule extends Service_Module
{
	protected function _saveData($hash, $post) {
		
		$d= ActiveRecord::factory('Abuse_Data');
		if ($d) {
			$d['url']= str_replace('/index.php/page', '/page', $_SERVER['HTTP_REFERER']);
			if (!empty($hash)) {
				$d['url'].= '#'. $hash;
			}
			$d['hash']= $hash;
			$d['name']= $post['name'];
			$d['email']= $post['email'];
			$d['message']= $post['message'];
			$d['status']= 'open';
			$d['ip']= ip2long(Utility::arrayValue($_SERVER,'REMOTE_ADDR'));
			$d['created_on']= date('Y-m-d H-i-s');
			$d['site_identifier']= Framework::$site->identifier;
			
			if ($d->save()) {
				$d->notifyUsers();
				return true;
			}
		}
		return false;
	} 

	public function formRequest($hash) {
		
		$td= array();
		$td['show_form']= true;
		$error= array();
		$info= array();
		if (!empty($_POST['abuse'])) {
			$abuse= $_POST['abuse'];
			$abuse['name']= trim($abuse['name']);
			if (empty($abuse['name'])) {
				$error['name']= 'Verplicht veld is leeg';
			}
			$abuse['email']= trim($abuse['email']);
			if (empty($abuse['email'])) {
				$error['email']= 'Verplicht veld is leeg';
			}
			else if (!email::isValid($abuse['email'])) {
				$error['email']= 'E-mail adres is ongeldig';
			}
			$abuse['message']= trim($abuse['message']);
			if (empty($abuse['message'])) {
				$error['message']= 'Verplicht veld is leeg';
			}
			elseif (strlen($abuse['message']) < 5) {
				$error['message']= 'Uw bericht is te klein (minimaal 5 tekens)';
			}
			elseif (strlen($abuse['message']) > 1024) {
				$error['message']= 'Uw bericht is te groot ('. strlen($abuse['message']). ' tekens)';
			}
			if (!empty($abuse['captcha'])) {
				$error[]= 'Uw melding is geweigerd. Het lijkt op geautomatiseerde invoer.';
			}
			$td['abuse']= $abuse;
			
			if (empty($error)) {
				if ($this->_saveData($hash, $abuse)) {
					$info[]= 'Bedankt! Uw melding is ontvangen. Het zal door onze medewerkers worden behandeld.';
					$td['show_form']= false;
				}
				else {
					$error[]= 'Helaas kunnen we uw melding op dit moment niet ontvangen';
				}
			}
		}
		$td['error']= $error;
		$td['info']= $info;
		$td['hash']= $hash;
		$smarty= Framework::$representation;
		if ($smarty) {
			$smarty->assign('td', $td);
			$data= $smarty->fetch('modules/blocks/abuse/form.tpl');
			if ($data) {
				return new Service_Reply(true, $data);
			}
		}
		return new Service_Reply(false);
	}
}
