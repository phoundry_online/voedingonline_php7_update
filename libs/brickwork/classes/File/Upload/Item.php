<?php
/**
 * Class representing a uploaded file
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @author Peter Eussen <peter.eussen@webpower.nl>
 * @author Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
class File_Upload_Item
{
	/**
	 * @var string original filename
	 */
	protected $_name;
	
	/**
	 * @var int size of the file measured in bytes
	 */
	protected $_size;
	
	/**
	 * @var string mimetype of the file as reported by the browser
	 */
	protected $_type;
	
	/**
	 * @var string path to the temp file where the data is stored
	 */
	protected $_tmp_name;
	
	/**
	 * @var int error code as reported by PHP matches the UPLOAD_ERR_* defines
	 */
	protected $_error;
	
	/**
	 * @var string the extension of the file being uploaded
	 */
	protected $_extension;
	
	/**
	 * Error code to message transition table
	 * @var array
	 */
	public static $error_messages = array(
		UPLOAD_ERR_OK => "",
		UPLOAD_ERR_INI_SIZE => 'bestand is groter dan is toegestaan',
		UPLOAD_ERR_FORM_SIZE => 'bestand is groter dan is toegestaan door formulier',
		UPLOAD_ERR_PARTIAL => 'uploaden halverwege onderbroken',
		UPLOAD_ERR_NO_FILE => 'geen bestand geupload',
		UPLOAD_ERR_NO_TMP_DIR => 'server fout: Missing a temporary folder',
		UPLOAD_ERR_CANT_WRITE => 'server fout: Failed to write file to disk',
		UPLOAD_ERR_EXTENSION => 'server fout: A extension stopped the upload'
	);
	
	/**
	 * Creates the new uploaded file class. The $file param should be an array
	 * with name, size, type, tmp_name and error entries.
	 *  
	 * @param array $file
	 */
	public function __construct(array $file)
	{
		if(!isset($file['name'], $file['size'], $file['type'],
			$file['tmp_name'], $file['error'])) {
			throw new InvalidArgumentException("invalid uploaded file");
		}
		
		$this->_name = (string) $file['name'];
		$this->_size = (int) $file['size'];
		$this->_type = (string) $file['type'];
		$this->_tmp_name = (string) $file['tmp_name'];
		$this->_error = (int) $file['error'];
		$this->_extension = pathinfo($this->_name, PATHINFO_EXTENSION);
	}

	/**
	 * Checks if the file matches one of the extensions
	 * 
	 * @param array|string $extensions single extension or array of extensions
	 * @return bool
	 */
	public function allowedExtension($extensions)
	{
		$extensions = array_map('strtolower', (array) $extensions);
		return in_array(strtolower($this->_extension), $extensions);
	}

	/**
	 * Check if the file exeeds the maximum filesize
	 * 
	 * @param int $max filesize in MiB
	 */
	public function allowedSize($max)
	{
		return ($this->_size <= (int) $max * 1024 * 1024);
	}

	/**
	 * Tells if the file was succesfully uploaded (errorcode == 0) and
	 * optionally can check if the filesize and extensions are ok as well
	 * 
	 * @param int $size [optional] also check on filesize
	 * @param array|string $exts [optional] also check on extensions
	 * @return bool
	 */
	public function ok($size = null, $exts = null)
	{
		$size = null !== $size ? $this->allowedSize($size) : true;
		$exts = null !== $exts ? $this->allowedExtension($exts) : true;
		return $size && $exts && $this->_error === 0 &&
			file_exists($this->_tmp_name);
	}

	/**
	 * Returns the original filename
	 * 
	 * @return string
	 */
	public function __toString() {
		return $this->_name;	
	}
	
	/**
	 * Get the filesize of the uploaded file
	 * 
	 * @return int bytes
	 */
	public function getSize()
	{
		return $this->_size;
	}
	
	/**
	 * Get the mimetype as it is reported by the users browser when the file
	 * was being uploaded.
	 * 
	 * @return string
	 */
	public function getMimeType()
	{
		return $this->_type;
	}
	
	/**
	 * Error code as reported by PHP matches the UPLOAD_ERR_* defines
	 * 
	 * @see http://www.php.net/manual/en/features.file-upload.errors.php
	 * @return int
	 */
	public function getError()
	{
		return $this->_error;
	}
	
	/**
	 * The path to the temporary file
	 * 
	 * @return string
	 */
	public function getTmpName()
	{
		return $this->_tmp_name;
	}

	/**
	 * Returns a textual representation of the error code
	 * 
	 * @see File_Upload_Item::$error_messages
	 * @return string
	 */
	public function getErrorMessage()
	{
		if(isset(self::$error_messages[$this->_error])) {
			return self::$error_messages[$this->_error];
		}
		return 'unspecified error ('.$this->_error.')';
	}

	/**
	 * Save the uploaded file to a permanent location.
	 * 
	 * If there already is a file at the target location the new file will
	 * either overwrite it or the fill will be suffixed with a number depending
	 * on the overwrite param.
	 * 
	 * @throws File_Upload_Exception if unable to overwrite or file already
	 * 	exists with the same content
	 * @param string $target Path to the target location
	 * @param bool $overwrite Overwrite or suffix if file already exists
	 * @return string the filepath to the saved path, same as $target for overwrite
	 */
	public function save($target, $overwrite = false)
	{
		if(!$this->ok()) {
			throw new File_Upload_Exception("Invalid file",
				File_Upload_Exception::INVALID);
		}
		
		$target = (string) $target;
		$overwrite = (bool) $overwrite;

		if(file_exists($target)) {
			if(md5_file($this->_tmp_name) == md5_file($target)) {
				throw new File_Upload_Exception("File with same content ".
					"already exists", File_Upload_Exception::DUPLICATE);
			}
			
			if($overwrite) { 
				if(!@unlink($target)) {
					throw new File_Upload_Exception("Unable to overwrite file",
						File_Upload_Exception::UNABLE_TO_OVERWRITE);
				}
			}
			else {
				$target = $this->_newFilename($target);
			}
		}

		if(!@copy($this->_tmp_name, $target)) {
			throw new File_Upload_Exception("Unable to copy uploaded file to ".
				$target, File_Upload_Exception::UNABLE_TO_MOVE);
		}
		
		@chmod($target, 0666);
		return $target;
	}
	
	/**
	 * Make new filename like '/path/file-xx.ext' where xx will autonumber
	 * 
	 * @param string $file
	 * @return string
	 */
	protected function _newFilename($file)
	{
		$p = pathinfo($file);
		if(isset($p['dirname'], $p['filename'], $p['extension'])) {
			for($n = 1; $n < 99; $n++) {
				$new = $p['dirname'].'/'.
					$p['filename'].'-'.$n.'.'.$p['extension'];
				if(!file_exists($new)) {
					return $new;
				}
			}
		}
		
		return false;
	}
}