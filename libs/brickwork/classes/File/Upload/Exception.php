<?php
/**
 * Exception class for the File Upload functionality
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
class File_Upload_Exception extends Exception
{
	const INVALID = 1;
	const DUPLICATE = 2;
	const UNABLE_TO_OVERWRITE = 3;
	const UNABLE_TO_MOVE = 4;
}