<?php
/**
 * Collection class for the $_FILES array which is funky wonky sucky
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
class File_Upload_Collection implements Countable, Iterator, ArrayAccess
{
	/**
	 * @var int The key of the currently cached entry
	 */
	protected $_index;
	
	/**
	 * @var File_Upload_Item|File_Upload_Collection Currently cached entry
	 */
	protected $_current;
	
	/**
	 * @var array
	 */
	protected $_keys;
	
	/**
	 * @var array The array with files
	 */
	protected $_values;
	
	public function __construct(array $files)
	{
		$this->_keys = array_keys($files);
		$this->_values = array_values($files);
	}
	
	public function count()
	{
		return count($this->_keys);
	}
	
	/**
	 * Magic iterator method
	 * 
	 * @return File_Upload_Item|File_Upload_Collection
	 */
	public function current()
	{
		return $this->_getEntry($this->_index);
	}

	public function next()
	{
		$this->_index += 1;
	}

	public function key()
	{
		return $this->valid() ?
			$this->_keys[$this->_index] : false;
	}

	public function valid()
	{
		return array_key_exists($this->_index, $this->_keys);
	}

	public function rewind()
	{
		$this->_index = 0;
	}
	
	/**
	 * Magic array access method
	 * 
	 * @return File_Upload_Item|File_Upload_Collection
	 */
	public function offsetGet($offset)
	{
		$index = array_search($offset, $this->_keys);
		if (false === $index) {
			throw new OutOfBoundsException("Invalid offset: {$offset}");
		}
		return $this->_getEntry($index);
	}
	
	public function offsetExists($offset)
	{
		return in_array($offset, $this->_keys);
	}
	
	public function offsetSet($offset, $value)
	{
		throw new BadMethodCallException(__CLASS__.' is read-only');
	}
	
	public function offsetUnset($offset)
	{
		throw new BadMethodCallException(__CLASS__.' is read-only');
	}
	
	/**
	 * Normalizes the wonky array entries for the $_FILES array on access
	 * 
	 * @param array $entry
	 * @return File_Upload_Item|File_Upload_Collection
	 */
	protected function _getEntry($index)
	{
		if (!array_key_exists($index, $this->_values)) {
			return false;
		}
		
		$entry = $this->_values[$index];
		if(isset($entry['name']) && is_array($entry['name'])) {
			$files = array();
			foreach($entry['name'] as $k => $name) {
				$files[$k] = array(
					'name' => $name,
					'tmp_name' => $entry['tmp_name'][$k],
					'size' => $entry['size'][$k],
					'type' => $entry['type'][$k],
					'error' => $entry['error'][$k]
				);
			}
			return new self($files);
		}
		else {
			return new File_Upload_Item($entry);
		}
	}
}
