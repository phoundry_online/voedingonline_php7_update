<?php
interface File_StorageInterface
{
	public function getFileUrl($name);
	public function putFile($file, $name);
	public function putString($data, $name);
}