<?php
/**
 * Creates a public url for the given file which is accessible over HTTP
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
class File_Storage_Local implements File_StorageInterface
{
	private $dir;
	private $url;
	
	public function __construct($dir, $url)
	{
		$this->dir = $dir;
		$this->url = $url;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/File/File_StorageInterface#hasFile()
	 */
	public function getFileUrl($name)
	{
		return file_exists($this->dir."/".$name) ? $this->url."/".$name : null;
	}
	
	/**
	 * Creates a copy of the file in a folder within the htdocs
	 * 
	 * @param string $file
	 * @param string $name
	 * @return string the url to the file
	 */
	public function putFile($file, $name)
	{
		$filename = basename($file);
		copy($file, $this->dir."/".$filename);
		return $this->url."/".$filename;
	}
	
	/**
	 * Makes the string available as a file in the htdocs folder
	 * 
	 * @param string $data
	 * @param string $name
	 * @return string the url to the file
	 */
	public function putString($data, $name)
	{
		file_put_contents($this->dir."/".$name, $data);
		return $this->url."/".$name;
	}
}