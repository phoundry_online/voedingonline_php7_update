<?php
require_once(BRICKWORK_DIR . '/functions/locale.php');

/**
 * Simple Translation class based on a single table and Locale settings
 * This class can serve as a base for many translation related things
 *
 * @package 	 Brickwork
 * @subpackage Translation
 * @author		 Peter Eussen
 * @todo		 Make multi site compatible, and rewrite to language code as primary key
 */
class Translation implements ArrayAccess
{
	const	CACHE_MODIFIED_KEY = '_translation_modifiedts';

	/**
	 * Instance of a locale namespace translation
	 *
	 * @var Array
	 */
	private static 	$_instance = Array();

	/**
	 * The site identifier
	 *
	 * @var string
	 */
	protected	$_siteIdentifier;

	/**
	 * The code for this locale
	 *
	 * @var string
	 */
	public		$code;

	/**
	 * The current local
	 *
	 * @var string
	 */
	public		$locale;

	/**
	 * The namespace of the language file
	 *
	 * @var string
	 */
	public		$namespace;

	/**
	 * Automaticlaly create an entry for an unexisting label
	 *
	 * @var boolean
	 */
	protected	$_autocreate;

	/**
	 * Cache object for translation caching
	 *
	 * @var Cache
	 */
	protected	$_cache;

	/**
	 * Initialises the Translation set
	 *
	 * @param 	string $locale
	 * @return	void
	 */
	public function __construct( $langcode,  $site_identifier, $autocreate = null )
	{
		global $WEBprefs;

		$savePath 		= (isset($WEBprefs['Language.Cache.Target']) ? $WEBprefs['Language.Cache.Target'] : 'file:///lang_%s_%s.words' );
		$prefsCreate	= (isset($WEBprefs['Language.Create']) ? $WEBprefs['Language.Create'] : TESTMODE );

		$this->_getLanguageDefinitions( $langcode, $site_identifier );

		$this->_cache 		= Cache::factory( sprintf($savePath, $site_identifier, $this->code ) );

		$this->_autocreate	= (is_null($autocreate) ? $prefsCreate : $autocreate);
	}

	/**
	 * Returns the time the dataset was last modified
	 *
	 * @return integer
	 */
	public function lastModified()
	{
		// Je zou verwachten dat hier time() zou staan, maar dat doe ik niet omdat dan voor sites
		// Zonder translations de template altijd gecompiled zou worden
		return (int) $this->_cache[ self::CACHE_MODIFIED_KEY ];
	}

	/**
	 * Flushes a cache file
	 *
	 * @return boolean
	 */
	public function flushCache()
	{
		return $this->_cache->flush();
	}

	/**
	 * Translates a string
	 *
	 * @param string $label
	 * @return string
	 */
	public function tr( $label, $file = null, $line = 0 )
	{
		$local = $this->_fetch( $label, $file, $line );

		return is_null($local) ? $label : $local;
	}

	/**
	 * Checks if a translation exists
	 *
	 * @param  string $label
	 * @return boolean
	 */
	public function offsetExists( $label ): bool
	{
		return !is_null($this->_fetch( $label ) );
	}

	/**
	 * gets the translation for a specific label
	 *
	 * @param 	string $label
	 * @return string|null
	 */
	public function offsetGet( $label ): mixed
	{
		$local = $this->_fetch( $label );

		return is_null($local) ? $label : $local;
	}

	/**
	 * Stores a new translation in the translation table
	 *
	 * @param string $label
	 * @param string $translation
	 * @return boolean
	 */
	public function offsetSet( $label, $translation ): void
	{
		$this->_add( $label, $translation );
	}

	/**
	 * Removes a translation from the translation table
	 *
	 * @param 	string $label
	 * @return boolean
	 */
	public function offsetUnset( $label ): void
	{
		$this->_remove( $label );
	}


	/**
	 * Creates an instance of a specific language/namespace file
	 *
	 * @param string $language
	 * @param string $namespace
	 * @return Translation
	 */
	public static function factory( $code, $site_identifier = null, $autocreate = null )
	{
		if ( is_null($site_identifier) )
		{
			if ( !isset(Framework::$site) )
				throw new Language_Exception('Call to translations without a site identifier');
			$site_identifier = Framework::$site->identifier;
		}

		if ( !isset(self::$_instance[ $site_identifier ][ $code ]) )
		{
			self::$_instance[ $site_identifier ][ $code ] = new Translation( $code, $site_identifier, $autocreate );
		}


		return self::$_instance[ $site_identifier ][ $code ];
	}

	/**
	 * Returns the translation file based on a site language
	 *
	 * @param Site_Language $sl
	 */
	public static function getBySiteLanguage( Site_Language $sl, $autocreate = null )
	{
		return self::factory( $sl->code, $sl->site_identifier, $autocreate );
	}

	/**
	 * Loads a translation based on its DB id
	 *
	 * @param 	integer $id
	 * @return Translation|NULL
	 */
	public static function getByLanguageId( $id )
	{
		$res = DB::iQuery( sprintf('SELECT code, site_identifier FROM brickwork_site_language WHERE id = %d', $id) );

		if ( self::isError($res) )
	 	{
			ErrorHandler::singleton()->log(new ErrorVoedingonline(E_USER_ERROR, sprintf('Translation failure: %s', $res->errstr), __FILE__, __LINE__ ) );
	 	}
	 	else if ( $res->first() )
	 	{
	 		$row = $res->first();
	 		return self::factory( $row->code, $row->site_identifier );
	 	}
	 	return null;
	}

	/**
	 * Obtains a translation by checking cache and database
	 *
	 * @param 	string $label
	 * @return string|null
	 */
	protected function _fetch( $label, $file = null, $line = 0 )
	{
		 $hash = $this->_getLabelhash( $label );

		 if ( isset($this->_cache[$hash]) )
		 	return $this->_cache[ $hash ];
		 else
		 {
		 	$sql = sprintf("SELECT localstring " .
		 					 "  FROM brickwork_translation " .
		 					 " WHERE labelhash = '%s'" .
		 					 "   AND lang = '%s'" .
		 					 "   AND site_identifier = '%s'",
		 					DB::quote($hash),
		 					DB::quote($this->code),
		 					DB::quote($this->_siteIdentifier));
		 	$res = DB::iQuery( $sql );

		 	if ( self::isError( $res ))
		 	{
				ErrorHandler::singleton()->log(new ErrorVoedingonline(E_USER_ERROR, sprintf('Translation failure: %s', $res->errstr), __FILE__, __LINE__ ) );
		 	}
		 	else
		 	{
		 		$row = $res->first();

		 		if ( $row !== null )
		 		{
		 			$this->_cache[ self::CACHE_MODIFIED_KEY ] = time();
		 			$this->_cache[ $hash ] = $row->localstring;
		 			return $row->localstring;
		 		}
		 		else if ($this->_autocreate )
		 			$this->_add( $label, null, $file, $line );
		 	}
		 }

		 return null;
	}

	/**
	 * Removes a translation from the system
	 *
	 * @param string $attr
	 * @return boolean
	 */
	protected function _remove( $attr )
	{
		$hash = $this->_getLabelhash( $label );

		if ( isset($this->_cache[ $hash ]))
		{
			$this->_cache[ self::CACHE_MODIFIED_KEY ] = time();
			unset( $this->_cache[ $hash ] );
		}

		$sql = sprintf("DELETE FROM brickwork_translation WHERE labelhash = '%s' AND lang = '%s' AND site_identifier = '%s'", DB::quote($hash), DB::quote($this->code), DB::Quote($this->_siteIdentifier) );
		$res = DB::iQuery( $sql );

		if ( self::isError($res) )
	 	{
			ErrorHandler::singleton()->log(new ErrorVoedingonline(E_USER_ERROR, sprintf('Translation failure: %s', $res->errstr), __FILE__, __LINE__ ));
	 	}

	 	return true;
	}

	/**
	 * Adds a new item to the translation table
	 * If the item already exists, it will be replaced by the current value.
	 *
	 * @param string $label
	 * @param string $translation
	 * @return boolean
	 */
	protected function _add( $label, $translation, $sourcefile = null, $sourceline = 0 )
	{
		$hash 		= $this->_getLabelhash( $label );

		if ( substr($sourcefile,0,strlen(realpath(PROJECT_DIR)) ) == PROJECT_DIR)
			$sourcefile = substr($sourcefile,strlen(realpath(PROJECT_DIR)));

		$sql = sprintf("INSERT INTO brickwork_translation ( labelhash, lang, site_identifier, localstring, systemstring, template, line) " .
						 "VALUES('%s','%s','%s',%s,'%s',%s,%d) ON DUPLICATE KEY UPDATE localstring = '%s'",
						 DB::quote($hash),
						 DB::quote($this->code),
						 DB::quote($this->_siteIdentifier),
						 (is_null($translation) ? 'NULL' : "'" . DB::quote($translation) . "'"),
						 DB::quote($label),
						 (!is_null($sourcefile) ? "'" . DB::quote($sourcefile) . "'" : 'NULL'),
						 (int)$sourceline,
						 DB::quote($translation));

		$res = DB::iQuery( $sql );

		if ( self::isError($res) )
	 	{
			ErrorHandler::singleton()->log(new ErrorVoedingonline(E_USER_ERROR, sprintf('Translation failure: %s', $res->errstr), __FILE__, __LINE__ ));
			return false;
	 	}

	 	if ( isset($this->_cache[ $hash ]))
	 	{
			$this->_cache[ $hash ] = $translation;
			$this->_cache[ self::CACHE_MODIFIED_KEY ] = time();
	 	}
		return true;
	}

	/**
	 * Gets a hash for a specific system label
	 *
	 * @param string $label
	 * @return string
	 */
	protected function _getLabelhash( $label )
	{
		return sha1( $label );
	}

	protected function _getLanguageDefinitions( $code, $site_identifier = null )
	{
		if ( is_null($site_identifier) )
			$site_identifier = ( isset(Framework::$site) ? Framework::$site->identifier : null);

		if ( is_null($site_identifier ) )
		{
			throw new Language_Exception( sprintf("Construction of translations without site_identifier"));
		}

		$res = DB::iQuery( sprintf("SELECT code, locale, namespace FROM brickwork_site_language WHERE code = '%s' AND site_identifier = '%s'", DB::quote($code), DB::quote($site_identifier) ) );

        try {
            $res = DB::iQuery( sprintf("SELECT code, locale, namespace FROM brickwork_site_language WHERE code = '%s' AND site_identifier = '%s'", DB::quote($code), DB::quote($site_identifier) ) );
        } catch (Exception $e) {
            ErrorHandler::singleton()->log(new ErrorVoedingonline(E_USER_ERROR, sprintf('Translation failure: %s', $e->getMessage()), __FILE__, __LINE__) );
            return false;
        }

        if ( $res->numRows > 0)
		{
		    $row = $res->first();
			$this->locale 	 		= $row->locale;
			$this->namespace 		= $row->namespace;
			$this->code		 		= $row->code;
			$this->_siteIdentifier  = $site_identifier;
			return true;
		}
		return false;
	}

    public static function isError($e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

