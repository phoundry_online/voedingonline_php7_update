<?php
/**
 * Interface die aangeeft dat een plugin/module gebruik kan/wil maken van confirmatie
 * Dit is een poging om confirmatie via e-mail op een generieke manier te laten plaatsvinden.
 * Indien een module/plugin gebruik wil maken van een confirmatie via e-mail, moet hij
 * deze interface implementeren. Deze interface definieert een aantal functies die nodig
 * zijn om op een "abstracte" manier informatie uit te wisselen met de "moeder" module.
 * 
 * @author 		Peter Eussen	<peter.eussen@webpower.nl>
 * @version		1.0
 * @package		Brickwork
 * @subpackage	Confirmation
 */
interface Module_ConfirmableInterface
{
	/**
	 * Retourneerd data die nodig is voor het succesvol versturen van de e-mail.
	 * Deze methode kan gebruikt worden om data met betrekking tot de confirmatie door te sturen naar
	 * de confirm plugin. Deze methode moet een array teruggeven met de volgende gegevens:
	 * - name		: de naam van de persoon die moet bevestigen
	 * - email		: de e-mail van de persoon die moet bevestigen
	 * - url		: de URL die gebruikt moet worden als "target" link. Dit moet de URL zijn waar
	 *                de uiteindelijke contributie op te zien is en moet dan ook een instance van
	 *                de moeder module bevatten
	 * - type       : De type confirmatie (gebruikt voor het selecteren van de mailtemplate e.d.)              
	 * 
	 * @return array
	 */
	public function getConfirmData();
	
	/**
	 * Method die aangeroepen wordt op het moment dat iemand een confirmatie link aanroept
	 * 
	 * @param 	Confirmation $confirm
	 * @return 	bool
	 */
	public function confirm(Confirmation $confirm);
}