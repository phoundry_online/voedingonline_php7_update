<?php
/**
 * Denotes that a class is pluggable (aka, can load other plugins )
 *
 * @author 		Peter Eussen <peter.eussen@webpower.nl>
 * @version		1.0
 * @package		Brickwork
 * @subpackage	Module
 */
interface Module_PluggableInterface
{
	/**
	 * This should return the id of the "active content"
	 * This ID can be used by other classes to refer to the content that is currently being viewed. In
	 * a forum for example this may be the topic id, but it can also refer to the module id if you want
	 * to. It leaves the deciding up to the module which handles the displaying of the content.
	 * Plugins depend on it to put their content alongside the required content of the module.
	 *
	 * @return	int
	 */
	public function getContentId();
}