<?php
/**
 * Makes a module able to copy plugin data
 *
 * @author		Bart Lagerweij <bart.lagerweij@webpower.nl>
 * @copyright	Web Power BV, http://www.webpower.nl
 */
interface Module_MutableInterface
{
	/**
	 * This should return the data needed to perform the copy operation
	 * First element in the array is the source
	 * Second element in the array is the destination
	 * 
	 * @return Array with Module_ContentItem objects
	 */
	public function getMutableData();
	
}
