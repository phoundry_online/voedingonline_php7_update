<?php
/**
 * Module Plugin abstract
 * This abstract class implements some key features for plugin development. It is wise to extend this
 * class when you start developing your own plugins, though it is not required.
 *
 * This class initialises the plugin and implements some observer methods for easy handling of different
 * events. In this case you can catch events by implementing the following functions in your code:
 *
 * initEvent		=> called at the end of the __construct of WebModule
 * preLoadEvent 	=> called before loadData of the module
 * postLoadEvent	=> called after the loadData of the module
 * preFetch			=> called before the fetch in the module
 * postFetch		=> called after the fetch of the module
 * copyEvent		=> called when a module want's a plugin to copy data
 * deleteEvent		=> called when a module want's a plugin to delete data
 * destroyEvent		=> called when the module is beging destroyed
 *
 * Also: Plugins can contain other plugins! In such cases you can also receive a switchContent event,
 * which can be used in your module to obtain the new contentId.
 *
 * @author 		Peter Eussen <peter.eussen@webpower.nl>
 * @version		1.0
 * @package		Brickwork
 * @subpackage	Module
 *
 */
abstract class Module_PluginAbstract implements Module_PluginInterface,
	Module_PluggableInterface
{
	const METHOD_POSTFIX	= 'Event';

	/**
	 * Link back to the Webmodule
	 * @var Webmodule
	 */
	public $module;
	
	/**
	 * Link to the parent, this is either the Webmodule or another Plugin
	 * @var Module_PluggableInterface
	 */
	public $parent;

	/**
	 * name the plugin will be known as in the Webmodule. Can be used to make the plugin "unique"
	 *
	 * @var string
	 */
	public $identifier;
	
	/**
	 * Sub Plugins attached to this Plugin
	 * 
	 * @var Module_PluginCollection
	 */
	public $plugins;

	/**
	 * Get vars for this plugin (use qsPart in your template)
	 *
	 * @var Array
	 */
	public $get;

	/**
	 * Post vars for this plugin
	 *
	 * @var Array
	 */
	public $post;

	/**
	 * Private key used for forms
	 *
	 * @var string
	 */
	protected $_pluginUid;

	/**
	 * Indicator to show that the processing on the parent failed
	 *
	 * @var bool
	 */
	protected $_parentFailed;

	/**
	 * (non-PHPdoc)
	 * @see Module_Plugin#__construct()
	 */
	public function __construct(Webmodule $module, $identifier,
		Module_PluggableInterface $parent = null)
	{
		$this->module = $module;
		$this->identifier = $identifier;
		$this->parent = null !== $parent ? $parent : $module;
		
		$this->_parentFailed = false;

		$uid = $this->_getPluginUUID();

		// Fetch Plugin related get/post vars
		$this->get = Utility::arrayValueRecursive($_GET, array('pl', $uid), array());
		$this->post = Utility::arrayValueRecursive($_POST, array('pl', $uid), array());

		// Load the sub plugins setting $this->plugins
		$this->_initializePlugins();
		
		// Determine to which events we as a plugin should subscribe
		$events = array();
		$methods = get_class_methods(get_class($this));
		if($methods) {
			foreach($methods as $method) {
				if(Utility::endsWith($method, self::METHOD_POSTFIX, false)) {
					// Cut off Event at the end and we got the event name
					$events[] = substr($method, 0, -strlen(self::METHOD_POSTFIX));
				}
			}
		}
		
		$this->_events = array_unique(array_merge(
			$events, $this->plugins->getEvents()
		));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Module_Plugin#getEvents()
	 */
	public function getEvents()
	{
		return $this->_events;
	}

	/**
	 * Handles notifications of the module
	 * This method is to comply with the interface. Underwater it will use
	 * seperate event methods per possible event.
	 * Please see class documentation for usage.
	 *
	 * @param string $event
	 */
	public function update($event)
	{
		$e = strtoupper($event);
		$method = $e.self::METHOD_POSTFIX;

		// If it is a PRE event, we probably want to fire it before the event in this plugin
		if(substr($e, 0, 3) == 'PRE') {
			if($this->plugins->notify($event) === false) {
				return false;
			}
		}

		// After all pre-events plugin-plugins we process the event ourselves
		if (method_exists($this, $method)) {
			if ($this->$method() === false) {
				return false;
			}
		}

		// Everything that is not a pre-event, is a post-event.
		// so call it after the plugin itself processed it
		if (substr($event, 0, 3) != 'PRE') {
			if ($this->plugins->notify($event) === false) {
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Return default the parent's content id
	 */
	public function getContentId()
	{
		return $this->parent->getContentId();
	}

	/**
	 * Plugin version of qspart
	 *
	 *
	 * @param unknown_type $nameModule_Plugin
	 */
	public function qsPart($name)
	{
		//return 'pl[' . $this->identifier . '][' . $this->module->id . '][' . $name . ']';
		// Simplified form code which uses a hash instead of a nested nested set of arrays
		return 'pl['.$this->_getPluginUUID().']['.$name.']';
	}

	/**
	 * Helper method for requesting refering link info of another object/method
	 *
	 * @param string $code
	 * @param int $id
	 * @return array
	 */
	protected function _getRefererInfo($code, $id)
	{
		$class = Webmodule::getClassForCode($code);

		if ($class === null) {
			return null;
		}

		// Only find info for classes that say they are referable
		if (in_array('Module_ReferableInterface', class_implements($class))) {
			return call_user_func(array($class, 'getLinkInfo'), $id, $code);
		}
		return false;
	}

	/**
	 * For compatibility with "module's".
	 * We "borrow" the code and id from a Webmodule to store our data, so make
	 * it available for a plugin as well, even though we do not have such a
	 * thing in our set
	 *
	 * @param string $attr
	 * @return mixed
	 * @throws	BadArgumentException
	 */
	public function __get( $attr )
	{
		switch ($attr) {
			case 'code':
				// This works recursively, the code of a plugin in a plugin
				// will have a lot of [] in it!
				return sprintf("%s[%d]%s",
					$this->parent->code,
					$this->parent->getContentId(),
					$this->identifier
				);
			case 'id':
				return $this->module->id;
			case 'plugin_uid'  : return $this->_pluginUid;
			default:
				if(isset($this->plugins->{$attr})) {
					return $this->plugins->{$attr};
				}
				throw new InvalidArgumentException("No such attribute: $attr");
		}
	}
	
	public function __isset($attr) {
		return isset($this->plugins->{$attr});
	}

	/**
	 * Retrieve a list of plugins that should be loaded within this plugin
	 * This is a tricky one, so be careful when configuring it in your prefs.
	 * Avoid circular plugin calls! What you should do is make a new WEBprefs
	 * entry called 'plugins'. In that array put the class  that should contain
	 * the plugin and the name the plugin should be known with and then the
	 * class of the plugin it should load.
	 *
	 * @return void
	 */
	protected function _initializePlugins()
	{
		global $WEBprefs;

		$res = Utility::arrayValueRecursive($WEBprefs,
			array('plugins', get_class($this), $this->identifier));
			
		$plugins = array();
		if(is_array($res) && $res) {
			foreach($res as $id => $plugin) {
				$plugins[$id] = new $plugin($this->module, $id, $this);
			}
		}
		
		$this->plugins = new Module_PluginCollection($plugins);
	}

	/**
	 * Stores the failure in the $_parentFailed attribute so other methods can check for it
	 *
	 * @return bool
	 */
	public function processFailedEvent()
	{
		$this->_parentFailed = true;
		return true;
	}

	/**
	 * Create a unique plugin Id for this plugin
	 * This makes a "location independant" code for the plugin. Which can be used
	 * in qspart to point to this specific plugin within the structure it is in.
	 * Warning: the hash is based on crc32, so collisions are possible! (though
	 * unlikely with the limited amount of plugins on a page)
	 *
	 * @return string
	 */
	protected function _getPluginUUID()
	{
		if (!isset($this->_pluginUid)) {
			$hashString = sprintf("%s[%s]%s[%s]", $this->module->code, $this->module->id, $this->identifier, get_class($this));
			$this->_pluginUid = str_pad(dechex(crc32($hashString)), 8, '0', STR_PAD_LEFT);
		}
		return $this->_pluginUid;
	}

	/**
	 * Returns the element for the current content id
	 *
	 * @return Module_ContentItem
	 */
	protected function _getContentElement($contentId)
	{
		return Module_ContentItem::getByCodeAndContentId($this->module->code,
			$contentId, Framework::$site->identifier);
	}
}