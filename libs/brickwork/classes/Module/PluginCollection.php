<?php
/**
 * This class represends a collection of Plugins which can be bound to
 * either a module or another plugin.
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 */
class Module_PluginCollection
{
	/**
	 * @var array Containing all instantiated plugins
	 */
	protected $_plugins;
	
	/**
	 * @var array Contains plugins subscribed to all events
	 */
	protected $_all_events;
	
	/**
	 * @var array Contains plugins subscribed to various events
	 */
	protected $_events;
	
	/**
	 * Construct the Module_Plugins setting up the provided plugins. The plugins
	 * array should consist of valid key value pairs where the key will be used
	 * as the identifier of the plugin and the value the classname.
	 * 
	 * @param array		$plugins List of plugins
	 */
	public function __construct(array $plugins)
	{
		$this->_plugins = array();
		$this->_events = array();
		$this->_all_events = array();
		
		foreach($plugins as $identifier => $plugin) {
			if(!is_string($identifier) || "" === $identifier) {
				throw new InvalidArgumentException('Invalid identifier: '.
					$identifier.' for Plugin: '.$class);
			}
			if(!$plugin instanceof Module_PluginInterface) {
				throw new InvalidArgumentException('Plugin '.get_class($plugin).
					' does not implement the Module_PluginInterface');
			}
			
			$this->_plugins[$identifier] = $plugin;
			$this->_registerPlugin($plugin);
		}
	}
	
	/**
	 * This method should register which plugin wants to receive which events.
	 * As input it requires the plugin and an array of events it
	 * wants to listen for.
	 *
	 * @param Module_PluginInterface $plugin
	 * @param string|array $events
	 * @return Module_Plugins self
	 */
	protected function _registerPlugin(Module_PluginInterface $plugin)
	{
		$events = $plugin->getEvents();
		
		if(null === $events) {
			$this->_all_events[] = $plugin;
		}
		else {
			foreach($events as $event) {
				$event = strtoupper($event); // Events are case insensitive
				if(!isset($this->_events[$event])) {
					$this->_events[$event] = array();
				}
				$this->_events[$event][] = $plugin;
			}
		}
		return $this;
	}
	
	/**
	 * Fires the given event
	 * 
	 * All plugins will be notified of the event. A plugin can stop a event
	 * prematurely by returning false.
	 *
	 * @param 	string	$e Event that should be fired
	 * @return	bool false if event was stopped prematurely else true
	 */
	public function notify($event)
	{
		$e = strtoupper($event);
		if(isset($this->_events[$e])) {
			foreach($this->_events[$e] as $p) {
				if (false === $p->update($event)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Returns an array containing all events that are being listened to
	 * 
	 * @return array
	 */
	public function getEvents()
	{
		return array_keys($this->_events);
	}
	
	/**
	 * Checks if there is a Plugin exposed as $name
	 * 
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		return array_key_exists($name, $this->_plugins);
	}
	
	/**
	 * Returns the Plugin as exposed by $name
	 * 
	 * @throws InvalidArgumentException if there is no Plugin exposed as $name
	 * @param string $name
	 * @return Module_PluginInterface
	 */
	public function __get($name)
	{
		if(!$this->__isset($name)) {
			throw new InvalidArgumentException('No plugin exposed as '.$name);
		}
		return $this->_plugins[$name];
	}
}