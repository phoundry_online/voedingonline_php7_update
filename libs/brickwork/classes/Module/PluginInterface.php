<?php
/**
 * Denotes that a class can be used as a Module plugin
 *
 * @author 		Peter Eussen <peter.eussen@webpower.nl>
 * @version 	1.0
 * @package		Brickwork
 * @subpackage	Module
 */
interface Module_PluginInterface
{
	/**
	 * Creates the new Plugin if there is no parent the module will be the
	 * parent for sub plugins the parent will be the parent plugin.
	 *
	 * @param Webmodule	$module
	 * @param string	$identifier
	 * @param Module_PluggableInterface $parent Parent plugin
	 */
	public function __construct(Webmodule $module, $identifier,
		Module_PluggableInterface $parent = null);

	/**
	 * Called when a subscribed event is being fired
	 * 
	 * @param string $event
	 * @return bool
	 */
	public function update($event);
	
	/**
	 * The events to which the Plugin should be registered to
	 * 
	 * The magic value null can be used to register to all available events this
	 * can be used in combination with __call to handle all events with one
	 * single method
	 * 
	 * @return array|null
	 */
	public function getEvents();
}