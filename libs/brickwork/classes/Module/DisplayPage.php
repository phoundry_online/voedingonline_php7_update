<?php
/**
 * Mapping for modules and their detail page
 *
 * @author Peter Eussen	<peter.eussen@webpower.nl>
 * @version 1.0
 * @package Brickwork
 * @subpackage Module
 *
 */
class Module_DisplayPage
{
	/**
	 * returns a path to a detail page based on a module and a site
	 *
	 * @param Webmodule $module
	 * @param string $site Site identifier
	 * @param Site_Language	$lang
	 * @return string
	 */
	static public function getByModule(Webmodule $module, $site = null,
		Site_Language $lang = null)
	{
		if($site === null && isset(Framework::$site)) {
			$site = Framework::$site->identifier;
		}

		if($lang === null && isset(Framework::$site->currentLang)) {
			$lang = Framework::$site->currentLang;
		}

		return self::getByCode($module->code, $site, $lang);
	}

	/**
	 * returns a path to a detail page based on a module class name and a site
	 * Warning: this may yield the wrong page if you have more than one "modules" pointing
	 * to the same class!
	 *
	 * @param string $class
	 * @param string $site Site identifier
	 * @param Site_Language $lang
	 * @return string
	 */
	static public function getByClass( $class, $site = null, $lang = null )
	{
		if($site === null && isset(Framework::$site)) {
			$site = Framework::$site->identifier;
		}

		if($lang === null && isset(Framework::$site->currentLang)) {
			$lang = Framework::$site->currentLang;
		}

		$res = DB::iQuery(sprintf('
			SELECT
				code
			FROM
				brickwork_module
			WHERE
				class = %s',
			DB::quoteValue($class)
		));
		
		if($row = $res->first()) {
			return self::getByCode($row->code, $site, $lang);
		}
	}


	/**
	 * returns a path to a detail page based on a module code and a site
	 *
	 * @param Webmodule $module
	 * @param string $site Site identifer
	 * @param Site_Language $lang
	 * @return string|void path or void when nothing is found
	 */
	static public function getByCode($code, $site, Site_Language $lang = null)
	{
		$res = DB::iQuery(sprintf('
			SELECT
				site_structure_id
			FROM
				brickwork_module_show_page
			WHERE
				module_code = %s
			AND
				site_identifier = %s
			AND
				lang_id %s',
			DB::quoteValue($code),
			DB::quoteValue($site),
			$lang === null ? 'IS NULL' : ('= '.(int) $lang->getId())
		));
			
		if($row = $res->first()) {
			return SiteStructure::getFullPathByStructureId(
				$row->site_structure_id, $site, $lang);
		}
	}
}