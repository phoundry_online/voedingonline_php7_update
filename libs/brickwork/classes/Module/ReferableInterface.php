<?php
/**
 * Denotes that a module can be linked through by other modules
 *
 * @author 		Peter Eussen <peter.eussen@webpower.nl>
 * @version		1.0
 * @package		Brickwork
 * @subpackage	Module
 */
interface Module_ReferableInterface
{
	/**
	 * Returns a link for viewing the specified content
	 * This method is static so it can be called without needing to create the whole module.
	 * It is here to let the module decide the URL that should be used for links to this article.
	 *
	 * It has one parameter: $id which denotes the "contentid" as it was obtained through "getContentId".
	 * It will return an array with _at_least_ two key values: url and title. If the id is invalid or can
	 * not be used, this function should return FALSE
	 *
	 * @param int $id
	 * @param string $code
	 * @return array|false
	 */
	static public function getLinkInfo($id, $code);
}