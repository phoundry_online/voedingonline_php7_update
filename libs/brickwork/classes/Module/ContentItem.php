<?php
/**
 * All Items on which Plugins can react
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
class Module_ContentItem extends ORM
{
	static protected $_item_cache = array();

	protected $_table_name = 'brickwork_module_content_item';

	/**
	 * Get Content_Item object
	 * 
	 * @param string $module_code
	 * @param int $content_id
	 * @param string $site_identifier (optional)
	 */
	static public function getByCodeAndContentId($module_code, $content_id,
		$site_identifier = null)
	{
		$content_id = (int) $content_id;
		
		if($site_identifier === null && isset(Framework::$site)) {
			$site_identifier = Framework::$site->identifier;
		}

		$hash = $module_code.'$$'.$content_id.'$$'.$site_identifier;

		if(isset(self::$_item_cache[$hash])) {
			return self::$_item_cache[$hash];
		}

		$item = new self;
		
		$item->where(array(
			'module_code' => $module_code,
			'content_id' => $content_id,
			'site_identifier' => $site_identifier
		))->get(true);
		
		if(!$item->isLoaded()) {
			$item['module_code'] = $module_code;
			$item['site_identifier'] = $site_identifier;
			$item['content_id'] = $content_id;
			$item->save();
		}
		
		self::$_item_cache[$hash] = $item;
		return $item;
	}
}
