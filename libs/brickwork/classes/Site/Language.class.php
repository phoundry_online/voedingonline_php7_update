<?php
/**
 * Language configuration
 *
 * @author			Peter Eussen
 * @package		Brickwork
 * @subpackage		Language
 *  */

/**
 * Site specific language configurations
 *
 * @author			Peter Eussen
 * @package		Brickwork
 * @subpackage		Language
 *
 * @property string $country
 * @property string $language
 * @property string $site_identifier
 */
class Site_Language
{
	/**
	 * List of ISO 8859-1 safe language codes
	 *
	 * @var string
	 */
	static protected	$_iso88591 = Array('af','sq','eu','ca','da','nl','en','fo','fi','fr','de','gl','is','ga','it','no','pt','gd','es','sv','se');

	/**
	 * The id of the record in the database
	 *
	 * @var integer
	 */
	protected	$_id;

	/**
	 * Site identifier string
	 *
	 * @var string
	 */
	protected	$_siteIdentifier;

	/**
	 * The code of the language
	 *
	 * @var string
	 */
	public	$code;

	/**
	 * The locale for the code
	 *
	 * @var string
	 */
	public	$locale;

	/**
	 * The namespace within the locale
	 *
	 * @var string
	 */
	public	$namespace;

	/*
	 * User selectable
	 *
	 * When true, the language will be used during the auto detection and it will show up in language selection lists
	 *
	 * @var boolean
	 */
	public $userSelectable;

	/*
	 * Manual selectable
	 *
	 * This language can be selected by changing the code in the url
	 *
	 * @var boolean
	 */
	public $manualSelectable;

	/**
	 * The __construct
	 *
	 * @param string 	$site
	 * @param string 	$code
	 * @param string 	$locale
	 * @param string 	$namespace
	 * @param integer 	$id
	 * @throws	Language_Exception
	 */
	public function __construct( $site, $code, $locale = null, $namespace = null, $id = null, $userSelectable = true, $manualSelectable = true )
	{
		$this->code 			= $code;
		$this->_id				= $id;
		$this->_siteIdentifier	= $site;
		$this->locale			= str_replace('-','_', $locale);
		$this->namespace		= $namespace;
		$this->userSelectable	= ($userSelectable == 1 || $userSelectable === TRUE) ? TRUE : FALSE;
		$this->manualSelectable = $manualSelectable;

		if ( is_null($this->_id) )
		{
			if ( !$this->_loadBySiteCode($site,$code) )
				throw new Language_Exception( sprintf("Undefined language: %s-%s", $site, $code));
		}
	}

	public function __get( $attr )
	{
		switch ($attr) {
			case "country":	return substr($this->locale,0,2);
			case 'language':return substr($this->locale,-2);
			case "site_identifier":
				return $this->_siteIdentifier;
			default:
				return null;
		}
	}

	/**
	 * Get the language part of the Locale and checks it against the list of all ISO8859-1 locales
	 *
	 * @return boolean
	 */
	public function isISO8895()
	{
		$code = substr($this->locale,0,2);

		return in_array( strtolower($code), self::$_iso88591 );
	}

	/**
	 * Returns the ID of the language
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->_id;
	}

	/**
	 * Returns a site language by using it's ID
	 *
	 * @param 	integer $langId
	 */
	static public function getSiteLanguageById( $langId )
	{
		$sql = sprintf("SELECT id, site_identifier, code, locale, namespace, user_selectable FROM brickwork_site_language WHERE id = %d", (int)$langId );

		$res = DB::iQuery($sql);

		if ( self::isError($res) )
			trigger_error('Error in SQL statement: ' . $res->errstr, E_USER_ERROR);

		$row = $res->first();

		if ( $row )
		{
			return new Site_Language( $row->site_identifier, $row->code, $row->locale, $row->namespace, $row->id, $row->user_selectable );
		}
		return null;
	}

	/**
	 * Get all language from a site
	 *
	 * @param 	string $site
	 * @return Array
	 */
	public static function getAllSiteLanguages( $site )
	{
		$sql = sprintf("SELECT id, code, locale, namespace, user_selectable FROM brickwork_site_language WHERE site_identifier = '%s'", DB::quote($site) );

		$res = DB::iQuery($sql);
		if ( self::isError($res) )
			trigger_error('Error in SQL statement: ' . $res->errstr, E_USER_ERROR);

		$tmp = Array();
        while ($res->current()) {
            $row = $res->current();
            $sitelang = new Site_Language($site, $row->code, $row->locale, $row->namespace, $row->id, $row->user_selectable);
            $tmp[ $row->id ] = clone $sitelang;
            $res->next();
        }
		return $tmp;
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }

	/*
	 * Get all user selectable languages
	 *
	 * @param string $site_identifier
	 * @return Array
	 */
	public static function getUserSelectableLanguages($site_identifier){
		$allLanguages = Site_Language::getAllSiteLanguages($site_identifier);
		$tmp = array();
		foreach($allLanguages as $l){
			if($l->userSelectable === TRUE){
				$tmp[] = $l;
			}
		}
		return $tmp;
	}

	/**
	 * loads a site language based on the code and a site identfiier
	 *
	 * @param 	string $site
	 * @param 	string $code
	 * @return boolean
	 */
	protected function _loadBySiteCode( $site, $code )
	{
		$sql = sprintf("SELECT id, code, locale, namespace, user_selectable FROM brickwork_site_language WHERE site_identifier = '%s' AND code = '%s'", DB::quote($site), DB::quote($code) );

		$res = DB::iQuery($sql);

		if ( self::isError($res) )
			trigger_error('Error in SQL statement: ' . $res->errstr, E_USER_ERROR);

		if ( count($res) )
		{
			$row = $res->first();

			if ( is_object($row) )
			{
				$this->_id 		= $row->id;
				$this->locale	= $row->locale;
				$this->namespace= $row->namespace;
				$this->code		= $row->code;
				$this->userSelectable = ($row->user_selectable == 1 || $row->user_selectable === TRUE) ? TRUE : FALSE;
				return true;
			}
		}
		return false;
	}

	public function __sleep()
	{
		return array('code','_siteIdentifier');
	}

	public function __wakeup()
	{
		$this->_loadBySiteCode( $this->_siteIdentifier, $this->code);
	}
}
