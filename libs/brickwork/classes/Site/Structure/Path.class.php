<?php
/**
 * Readable paths for the site structure
 * 
 * @author		Peter Eussen
 * @package	Brickwork
 * @subpackage	SiteStructure
 * @version	1.0
 */

/**
 * Site Structure readable Path functionality
 * 
 * 
 * @author		Peter Eussen
 * @package	Brickwork
 * @subpackage	SiteStructure
 * @version	1.0
 * @todo		clean up code, move get sitestructurebypath to here
 * @todo		make the path creation cleaner
 */
class Site_Structure_Path
{
	/**
	 * Indicator set to TRUE if the site_structure_path has a lang_id field
	 *
	 * @var boolean
	 */
	static protected	$_hasLanguage    = null;
	
	/**
	 * The Structure
	 *
	 * @var int
	 */
	public	$structureId;
	
	/**
	 * The path name that should be user
	 *
	 * @var string
	 */
	public	$name;
	
	/**
	 * The language it is valid for
	 *
	 * @var Site_Language
	 */
	public	$language;
	
	/**
	 * The site we are looking at
	 *
	 * @var string
	 */
	public	$siteIdentifier;
	
	/**
	 * The hash for the path
	 *
	 * @var string
	 */
	public	$hash;
	
	/**
	 * Indicator to show if it came from the archives
	 *
	 * @var boolean
	 */
	public	$isArchive;
	
	/**
	 * Construct, all params are optional
	 * @param string			$site_identifier
	 * @param int				$structure_id
	 * @param Site_Language		$lang
	 * @param string			$name
	 * @param string			$hash
	 * @param bool 				$archive
	 */
	public function __construct($site_identifier = null, $structure_id = null, Site_Language $lang = null, $name = null, $hash = null, $archive = false)
	{
		if ( is_null(self::$_hasLanguage) )
		{
			self::$_hasLanguage = Features::brickwork('translation');
		}
		
		$this->hash		 	 = $hash;
		
		$this->isArchive 	 = $archive;
		is_null($name) ? $this->name = null : $this->safeSetName($name);
		if(!is_null($lang)) $this->language = $lang;
		$this->siteIdentifier= $site_identifier;
		$this->structureId	 = $structure_id;
	}

	/**
	 * Searches a path based on the site_structure id (and language)
	 *
	 * @param string 	$siteIdentifier
	 * @param integer 	$siteStructureId
	 * @param Site_Language $lang
	 * @return Site_Structure_Path
	 */
	static public function getPathBySiteStructureId( $siteIdentifier, $siteStructureId, Site_Language $lang = null )
	{
		if ( self::$_hasLanguage )
		{
			$sql = sprintf('SELECT site_identifier, hash, site_structure_id, lang_id, name ' .
						   '  FROM brickwork_site_structure_path ' .
						   ' WHERE site_identifier = %s ' .
						   '   AND site_structure_id = %d ' . 
						   '   AND lang_id = %d ',
							Utility::quote( $siteIdentifier ),
							(int) $siteStructureId,
							(int) (is_null($lang) ? 0 : $lang->getId() )
						  );
		}
		else
		{
			$sql = sprintf('SELECT site_identifier, hash, site_structure_id, lang_id, name ' .
						   '  FROM brickwork_site_structure_path ' .
						   ' WHERE site_identifier = %s ' .
						   '   AND site_structure_id = %d ', 
							Utility::quote( $siteIdentifier ),
							(int) $siteStructureId
						  );
		}
		
		$res = DB::iQuery($sql);

		if ( self::isError($res) )
			throw new Exception('Query error: ' . $res->errstr );
			
		$row = $res->first();
		
		if ( $row )
		{
			$ssp = new Site_Structure_Path();
			$ssp->structureId 	= $row->site_structure_id;
			$ssp->hash        	= $row->hash;
			
			if ( self::$_hasLanguage )
				$ssp->language	= Site_Language::getSiteLanguageById( $row->lang_id );
			else
				$ssp->langauage = null;
				
			$ssp->name		  	= $row->name;
			$ssp->siteIdentifier= $row->site_identifier;
			return $ssp;
		}
		return null;
	}
	
	/**
	 * Changes the name of the path, but does it safely
	 * This method will alter the $name and $hash attributes, but will do so and make sure it is
	 * a readable URL and it's unique for the site/language combination.
	 * 
	 * @param 	string $name
	 * @return boolean
	 */
	public function safeSetName( $name )
	{
		if ( $this->localNameAllowed() )
		{
			$name 		= trim(preg_replace('/-+/','-', preg_replace('/[^a-z0-9àáâäæèéêëìíîïòóôöøðùúûüçñýÿ\-\_\/]/i','-',$name)),"-/");		
			$this->name = Utility::removeaccents(preg_replace('/\/+/','/', $name) );
			$this->makeUnique();
			
			$this->hash = md5($this->name);
		}
		else
		{
			// Language is not safe, assume the "safe" default
			$ssp = Site_Structure_Path::getPathBySiteStructureId( $this->siteIdentifier, $this->structureId );
			if ( is_null($ssp) )
			{
				trigger_error("Site structure does not have a base structure!", E_USER_ERROR);
			}
			$this->name = $ssp->name;
			$this->makeUnique();
			$this->hash = md5($this->name);
		}
		return true;
	}
	
	/**
	 * Checks if the name is unique and if not makes it unique
	 *
	 * @return boolean
	 */
	public function makeUnique()
	{
		$valid = false;
		$name  = $this->name;
		
		while (  !$valid )
		{
			if ( self::$_hasLanguage )
			{
				$sql = sprintf( 'SELECT count(*) as cnt ' .
								'  FROM brickwork_site_structure_path ' .
								' WHERE hash ="%s" ' .
								'   AND site_structure_id!=%d ' . 
								'   AND site_identifier="%s" '.
								'   AND lang_id = %d', 
								md5($name), 
								$this->structureId, 
								DB::quote($this->siteIdentifier),
								(is_null($this->language) ? 0 : $this->language->getId() )
							  );
			}
			else
			{
				$sql = sprintf( 'SELECT count(*) as cnt ' .
								'  FROM brickwork_site_structure_path ' .
								' WHERE hash ="%s" ' .
								'   AND site_structure_id!=%d ' . 
								'   AND site_identifier="%s"', 
								md5($name), $this->structureId, DB::quote($this->siteIdentifier));
				
			}
			
			$res = DB::iQuery($sql);
			
			if(self::isError($res)){
				$errorHandler->log($res);
			}

			$row = $res->first();
			if($row->cnt)
			{
				if(preg_match('/\-(\d)+$/', $name, $match)>0){
					$name = preg_replace('/\-(\d)+$/', '-'.++$match[1], $name);
				} else {
					$name.='-1';
				}
			} 
			else 
			{
				// No matches found
				$valid = true;
			}
		}
		
		$this->name = $name;
		return true;
	}

	/**
	 * Checks if for the current language we will accept the localized name
	 *
	 * @return boolean
	 */
	public function localNameAllowed()
	{
		if ( !is_object($this->language))
			return true;
			
		return $this->language->isISO8895();
	}
	
	/**
	 * Stores the new name, and moves the old to archive
	 *
	 * @return boolean
	 */
	public function update()
	{
		if ( empty($this->name) )
			throw new Exception("Name should not be empty!");
			
		// First check if the current one differs, because if it does we need to 
		// move it to the archive
		if ( self::$_hasLanguage )
		{
			$sql = sprintf("SELECT COUNT(site_identifier) as hasold " .
						   "  FROM brickwork_site_structure_path " .
						   " WHERE site_identifier = %s " .
						   "   AND site_structure_id = %d " .
						   "   AND hash != %s" .
						   "   AND lang_id = %d",
							Utility::quote($this->siteIdentifier),
							$this->structureId,
							Utility::quote($this->hash),
							(is_object($this->language) ? $this->language->getId() : 0 )
						  );
		}
		else
		{
			$sql = sprintf("SELECT COUNT(site_identifier) as hasold " .
						   "  FROM brickwork_site_structure_path " .
						   " WHERE site_identifier = %s " .
						   "   AND site_structure_id = %d " .
						   "   AND hash != %s",
							Utility::quote($this->siteIdentifier),
							$this->structureId,
							Utility::quote($this->hash)
						  );
		}
		
		$res = DB::iQuery($sql);
		
		if ( self::isError($res))
		{
			echo $res->errstr;
			trigger_error($res->errstr, E_USER_ERROR);
		}
		
		if (($row = $res->first()) && $row->hasold )
			$this->_moveOldToArchive();
			
		// brickwork_site_structure is "clean" for this path add a new entry
		if ( self::$_hasLanguage )
		{
			$sql = sprintf('INSERT IGNORE INTO brickwork_site_structure_path (site_identifier, hash, name, site_structure_id, lang_id) ' . 
						   ' VALUES (%s,%s, %s, %d, %d)', 
							Utility::quote($this->siteIdentifier), 
							Utility::quote($this->hash), 
							Utility::quote($this->name), 
							$this->structureId,
							(is_null($this->language) ? 0 : $this->language->getId() )
						   );
		}
		else
		{
			$sql = sprintf('INSERT IGNORE INTO brickwork_site_structure_path (site_identifier, hash, name, site_structure_id) ' . 
						   ' VALUES (%s,%s, %s, %d)', 
							Utility::quote($this->siteIdentifier), 
							Utility::quote($this->hash), 
							Utility::quote($this->name), 
							$this->structureId);
		}
		
		$res = DB::iQuery($sql);
			
		if(self::isError($res)){
			trigger_error($res->errstr, E_USER_ERROR);		
		}

		return true;
	}
	
	/**
	 * Moves the old path record to archive 
	 *
	 * @return boolean
	 */
	protected function _moveOldToArchive()
	{
		// First add to archive if not already in there
		if ( self::$_hasLanguage )
		{
			$sql = sprintf("INSERT IGNORE INTO brickwork_site_structure_path_archive (site_identifier, hash, name, site_structure_id, lang_id, archive_date) " .
						   " SELECT site_identifier, hash, name, site_structure_id, lang_id, NOW() ".
						   "   FROM brickwork_site_structure_path " .
					       "  WHERE site_identifier = %s " .
					       "    AND site_structure_id = %d " .
						   "    AND lang_id = %d ",
							Utility::quote($this->siteIdentifier),
							$this->structureId,
							(is_object($this->language) ? $this->language->getId() : 0 ) );
		}
		else
		{
			$sql = sprintf("INSERT IGNORE INTO brickwork_site_structure_path_archive (site_identifier, hash, site_structure_id, archive_date) " .
						   " SELECT site_identifier, hash, site_structure_id, NOW() ".
						   "   FROM brickwork_site_structure_path " .
					       "  WHERE site_identifier = %s " .
					       "    AND site_structure_id = %d ",
							Utility::quote($this->siteIdentifier),
							$this->structureId
						  );
		}

		$res = DB::iQuery($sql);
		
		if ( self::isError($sql))
			trigger_error('Query error: ' . $res->errstr, E_USER_ERROR);
			
		// Then delete
		if ( self::$_hasLanguage )
		{
			$sql = sprintf(" DELETE FROM brickwork_site_structure_path " .
					       "  WHERE site_identifier = %s " .
					       "    AND site_structure_id = %d " .
						   "    AND lang_id = %d ",
							Utility::quote($this->siteIdentifier),
							$this->structureId,
							(is_object($this->language) ? $this->language->getId() : 0 ) );
		}
		else
		{
			$sql = sprintf(" DELETE FROM brickwork_site_structure_path " .
					       "  WHERE site_identifier = %s " .
					       "    AND site_structure_id = %d ",
							Utility::quote($this->siteIdentifier),
							$this->structureId
						   );
		}

		$res = DB::iQuery($sql);
		
		if ( self::isError($res))
			trigger_error('Query error: ' . $res->errstr, E_USER_ERROR);
		return true;
	}
}
