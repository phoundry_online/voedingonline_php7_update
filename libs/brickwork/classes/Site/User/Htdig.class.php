<?php

/**
 * User object for the HTDig crawler, with all access that may be required
 *
 */
class Site_User_Htdig extends SiteUser
{
	private $_auth_roles;
	
	public function __construct( $username = 'HTdigCrawler', $password = '')
	{
		parent::__construct($username, $password);
		
		// set attribs
		$this->_name          = new Name('HTdig', 'Crawler');
		$this->_email         = null;
		$this->is_valid       = true;
		$this->is_anonymous   = false;
		$this->is_logged_in   = true;
		$this->_auth_id		  = 0;
		$this->_auth_roles 	  = null;
		
		global $WEBprefs;
		if (isset($WEBprefs['HTdig.Roles'])) {
			$this->_initRoles($WEBprefs['HTdig.Roles']);
		}
	}
	
	/**
	 * Always say we are logged in
	 *
	 * @return boolean
	 */
	public function login()
	{
		return true;
	}
	
	/**
	 * HtDig may access anything
	 *
	 * @param ACLRole $currole
	 * @return unknown
	 */
	public function hasRole( ACLRole $currole )
	{
		return true;
	}
	
	/**
	 * Return all possible rows
	 *
	 * @return Array
	 */
	public function getRoles()
	{
		if ( is_array($this->_auth_roles))
			return $this->_auth_roles;
			
		$this->_auth_roles = Array();
		$sql = sprintf('SELECT r.name ' . 
						 '  FROM brickwork_auth_role r ');

		$res = DB::iQuery($sql);

		if (self::isError($res) )
		{
			return $this->_auth_roles;
		}	

		while ( $res->current() )
		{
		    $row = $res->current();
			$role = new ACLRole( $row->name );
			array_push($this->_auth_roles, $role);
			$this->_auth_roles = array_merge($this->_auth_roles, $role->getChildRoles());
			$res->next();
		}
		return $this->_auth_roles;
	
	}
	
	/**
	 * Checks if the current user is a HTDig user and sets it when it is
	 *
	 * @return	boolean
	 */
	static public function checkHTDigUser()
	{
		$remote = Utility::arrayValue($_SERVER,'REMOTE_ADDR');
		$local  = Utility::arrayValue($_SERVER,'SERVER_ADDR');
		$agent = Utility::arrayValue($_SERVER, 'HTTP_USER_AGENT');
		 
		if ( stripos($agent,'HTdig') !== false && ($remote == '127.0.0.1' || $remote == $local ) )
		{
			$auth = Auth::singleton();
			$auth->login( new Site_User_Htdig() );
		}
		return true;
	}
	
	/**
	 * Restricts access to only specific roles
	 *
	 * @param 	string $roles		semi-column seperated role names
	 * @return boolean
	 */
	protected function _initRoles( $roles )
	{
		$roleArray = explode(";", $roles );
		
		for ($i = 0, $m = count($roleArray); $i < $m; $i++ )
			$roleArray[$i] = Utility::quote($roleArray[$i]);
		
		$this->_auth_roles = Array();
		$sql = sprintf('SELECT r.name ' . 
					   '  FROM brickwork_auth_role r WHERE r.name IN (%s) ', implode(",", $roleArray));

		$res = DB::iQuery($sql);

		if ( !self::isError($res) )
		{
			while ( $res->current() )
			{
			    $row = $res->current();
				$role = new ACLRole( $row->name );
				array_push($this->_auth_roles, $role);
				$this->_auth_roles = array_merge($this->_auth_roles, $role->getChildRoles());
				$res->next();
			}
			return true;	
		}
		return false;
	}
}