<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("Usage of ORMWrapper is deprecated", E_USER_ERROR);
}

/**
 * Simulates pre Brickwork 1.4 ActiveRecord Behaviour
 *
 */
class ORMWrapper extends ORM
{
	/**
	 * Field name that is used as primary key
	 *
	 * @var string
	 */
	protected $id_field_name_ = 'id';
	
	/**
	 * Id used to get filled no matter if the record got loaded or not
	 * 
	 * @param mixed $id
	 * @param bool $load
	 */
	public function __construct($id = null, $load = true)
	{
		parent::__construct($id, $load);
		if(!$this->isLoaded() && !is_null($id))
		{
			$this->id = $id;
		}
	}
	
	/**
	 * Get always returned an array if it was a multiple records result
	 *
	 * @param mixed $id
	 * @return array
	 */
	public function get($id = null)
	{
		$return = parent::get($id);
		if($return instanceof ActiveRecordIterator)
		{
			$temp = array();
			foreach($return as $val)
			{
				$temp[(is_numeric($val->id)?(int)$val->id:$val->id)] = $val;
			}
			$return = $temp;
		}
		return  $return;
	}
	
	/**
	 * There was no option to get the count for the last query so we always want the current query
	 *
	 * @return int
	 */
	public function getCount()
	{
		return parent::getCount(false);
	}
	
	/**
	 * Save always returned true, or it threw an exception
	 *
	 * @throws Exception
	 * @return bool True
	 */
	public function save()
	{
		parent::save();
		return true;
	}
	
	/**
	 * Make getting column values trough -> possible
	 *
	 * @throws Exception
	 * @param string $name
	 * @return mixedvar
	 */
	public function __get($name)
	{				
		$relation = $this->getRelation($name);
		if($relation !== false){
			return is_array($relation) ? (array) $relation : $relation;
		}
		
		return self::offsetGet($name);
	}
	
	/**
	 * Make setting column values trough -> possible
	 *
	 * @param string $name
	 * @param mixedvar $value
	 */
	public function __set($name, $value)
	{
		if($this->add($name, $value, true) !== false){
			return;
		}
		
		return self::offsetSet($name, $value);
	}
	
	/**
	 * Makes checking on column values trough -> possible
	 *
	 * @param unknown_type $name
	 * @return unknown
	 */
	public function __isset($name)
	{
		return self::offsetExists($name);
	}
	
	/**
	 * Makes unsetting column values trough -> possible
	 *
	 * @param string $name
	 */
	public function __unset($name)
	{
		switch ($name) {
			default:
				if($this->remove($name, null, true) !== false){
					break;
				}
				
				if(parent::offsetExists($name)){
					parent::offsetUnset($name);
					break;
				}
				
				throw new Exception(get_class($this) . ' heeft geen propery: '.$name);
			break;
		}		
	}
	
	/**
	 * Translations for the universal ->id and ->_id
	 */
	public function offsetGetReal($name)
	{
		if(($name == 'id' || $name == '_id' || $name == 'id_') && !empty($this->id_field_name_)) {
			$name = $this->id_field_name_;
		}
		return parent::offsetGetReal($name);
	}
	
	public function offsetSetReal($name, $value)
	{
		if(($name == 'id' || $name == '_id' || $name == 'id_') && !empty($this->id_field_name_)) {
			$name = $this->id_field_name_;
		}
		return parent::offsetSetReal($name, $value);
	}

	public function offsetExists($name)
	{
		if(($name == 'id' || $name == '_id' || $name == 'id_') && !empty($this->id_field_name_)) {
			$name = $this->id_field_name_;
		}
		
		return parent::offsetExists($name);
	}
	
	public function offsetUnset($name)
	{
		if(($name == 'id' || $name == '_id' || $name == 'id_') && !empty($this->id_field_name_)) {
			$name = $this->id_field_name_;
		}
		
		return parent::offsetUnset($name);
	}
	
	public function __toString()
	{
		return (string) $this->id;
	}
	
	/**
	 * The old Activerecord had some 1337 function that prefilled the primarykey for many to one relations
	 *
	 * @param string $key Key in one of the relation arrays
	 * @return object/array
	 */
	public function getRelation($key)
	{
		$value = parent::getRelation($key);
		
		if(FALSE !== $value && isset($this->_many_to_one[$key]) && (!($value instanceof ActiveRecord) || !$value->isLoaded()))
		{
			list($classname, $match_field) = $this->_many_to_one[$key];
			if(!is_array($match_field))
			{
				$match_field = array($match_field);				
			}
			$pk = array();
			foreach($match_field as $match_f) {
				$pk[] = $this->offsetGetReal($match_f);
			}
			
			if(!($value instanceof ActiveRecord)) {
				$value = ActiveRecord::factory($classname);
			}
			
			foreach($value->getTableProperty('primary_key') as $col_name) {
				$value->offsetSetReal($col_name, array_shift($pk));
			}
		}
		if($value instanceof ActiveRecordIterator)
		{
			$values = array();
			foreach($value as $val)
			{
				$values[(is_numeric($val->id)?(int)$val->id:$val->id)] = $val;
			}
			$value = $values;
		}
		return $value;
	}
	
	/**
	 * The old Activerecord accepted arrays filled with PKs for relations
	 * 
	 * @return bool
	 */
	public function add($key, $items, $used_set = false)
	{
		if(isset($this->_many_to_many[$key]) && !$used_set && is_array($items) && !is_object(first($items)))
		{
			list($foreign_class, $join_table, $this_pk, $foreign_pk) = $this->_many_to_many[$key];
			$foreign_class = ActiveRecord::factory($foreign_class);
			$pk = $foreign_class->getTableProperty('primary_key');
			$items = $foreign_class->where(first($pk), '("'.implode('", "', $items).'")', 'IN', false)->get();
		}
		
		if(isset($this->_one_to_many[$key]) && !$used_set && is_array($items) && !is_object(first($items)))
		{
			list($foreign_class, $foreign_pk) = $this->_one_to_many[$key];
			$foreign_class = ActiveRecord::factory($foreign_class);
			$pk = $foreign_class->getTableProperty('primary_key');
			$items = $foreign_class->where(first($pk), '("'.implode('", "', $items).'")', 'IN', false)->get();
		}
		
		if(isset($this->_many_to_one[$key]) && !($items instanceof ActiveRecord))
		{
			$foreign_class = $items;
			list($classname, $foreign_pk) = $this->_many_to_one[$key];
			$items = ActiveRecord::factory($classname, $items);
		}
		
		return parent::add($key, $items, $used_set);
	}
	
	/**
	 * The old Activerecord accepted arrays filled with PKs for relations
	 * 
	 */
	public function remove($key, $items, $used_unset = false)
	{
		if(isset($this->_many_to_many[$key]) && !$used_unset && is_array($items) && !is_object(first($items)))
		{
			list($foreign_class, $join_table, $this_pk, $foreign_pk) = $this->_many_to_many[$key];
			$foreign_class = ActiveRecord::factory($foreign_class);
			$pk = $foreign_class->getTableProperty('primary_key');
			$items = $foreign_class->where(first($pk), '("'.implode('", "', $items).'")', 'IN', false)->get();
		}
		
		if(isset($this->_one_to_many[$key]) && !$used_unset && is_array($items) && !is_object(first($items)))
		{
			list($foreign_class, $foreign_pk) = $this->_one_to_many[$key];
			$foreign_class = ActiveRecord::factory($foreign_class);
			$pk = $foreign_class->getTableProperty('primary_key');
			$items = $foreign_class->where(first($pk), '("'.implode('", "', $items).'")', 'IN', false)->get();
		}
		
		if(isset($this->_many_to_one[$key]) && !($items instanceof ActiveRecord))
		{
			$foreign_class = $items;
			list($classname, $foreign_pk) = $this->_many_to_one[$key];
			$items = ActiveRecord::factory($classname, $items);
		}
		
		return parent::remove($key, $items, $used_unset);
	}
	
	public function get_table_name()
	{
		return $this->getTableName();
	}
	
	/**
	 * Construct a fieldnames array used in queries
	 *
	 * @return array
	 */
	public function get_field_names()
	{
		$field_names = array();
		foreach($this->_localTable['columns'] as $col_name){
			if($col_name == 'id' || (!empty($this->id_field_name_) && $col_name == $this->id_field_name_)) continue;
				$field_names[] = $col_name;
		}
		
		return $field_names;
	}
	
	public function load_result($result, $classname = null)
	{
		$return = $this->loadResult($result, $classname);
		if(is_array($return))
		{
			$temp = array();
			foreach ($return as $val) {
				$temp[(is_numeric($val->id)?(int)$val->id:$val->id)] = $val;
			}
			$return = $temp;
		}
		return $return;
	}
}
