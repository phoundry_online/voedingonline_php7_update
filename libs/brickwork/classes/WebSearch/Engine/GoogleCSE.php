<?php
/**
 * Search engine, Google CSE 
 * 
 * @see http://www.google.com/cse/
 * @author Adrie den Hartog <adrie.denhartog@webpower.nl>
 * @version 1.0.0
 */
class WebSearch_Engine_GoogleCSE implements WebSearch_Engine
{
	/**
	 * @var Site_Language|void if searching in a specific site language
	 */
	private $lang;
	
	public function __construct(Site_Language $lang = null)
	{
		$this->lang = $lang;
	}
	
	/**
	 * Perform the specific search tasks using the given query object and
	 * return a WebSearch_ResultSet
	 *
	 * @param	WebSearch_Query
	 * @return	WebSearch_ResultSet
	 */
	public function search(WebSearch_Query $search_query)
	{
		$url = $this->createUrl($search_query);
		$res = $this->executeSearch($url, $search_query->getDomain());
		
		if (!empty($res)) {
			$results = array();
			// $res->R is a Iterator so we can't use array_map :(
			foreach ($res->R as $result) {
				$results[] = $this->transformResult($result);
			}
			
			// Prepare a filled websearch resultset and pass along the total
			// guesstimate google gave.
			$resultset = new WebSearch_ResultSet($results, $res->M);
			$resultset->first = (string) $res['SN'];
			$resultset->last = (string) $res['EN'];
			
			return $resultset;
		}
		
		// Prepare an empty Resultset, the show must go on..
		return new WebSearch_ResultSet(array(), 0);
	}
	
	/**
	 * Create the URL to call the google API with
	 * @param WebSearch_Query $search_query
	 * @return string
	 */
	private function createUrl(WebSearch_Query $search_query)
	{
		global $PHprefs;
		$charset = 'latin1';
		if ($PHprefs['charset'] === 'utf-8') {
			$charset = 'utf8';
		}
		
		$get = array(
			'cx' => $this->getApiKey($search_query->getExtraParams()),
			'client' => 'google-csbe',
			'output' => 'xml_no_dtd',
			'q' => $search_query->getSearchString(),
			'ie' => $charset,
			'oe' => $charset,
		);
		
		if ($domain = $search_query->getDomain()) {
			$get['as_dt'] = 1;
			$get['as_sitesearch'] = $domain;
		}
		
		if ($offset = $search_query->getOffset()) {
			$get['start'] = $offset;
		}
		
		if ($limit = $search_query->getLimit()) {
			$get['num'] = $limit;
		}
		
		// Multilanguage options
		if ($this->lang) {
			switch ($this->lang->code) {
				case 'cn':
					$get['lr'] = 'lang_zh-CN';
					$get['gl'] = 'cn';
					break;
				default:
					$get['lr'] = 'lang_'.$this->lang->code;
					$get['gl'] = $this->lang->code;
			}
		}
		
		return "http://www.google.com/cse?".http_build_query($get);
	}
	
	/**
	 * Execute the created search url
	 * 
	 * @param string $url
	 * @param string|void $domain
	 * @return SimpleXMLElement|void
	 */
	private function executeSearch($url, $domain = null)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if ($domain) {
			curl_setopt($ch, CURLOPT_REFERER, $domain);
		}
		$res = curl_exec($ch);
		curl_close($ch);
		
		if (!empty($res)) {
			// Retrieve information from the response
			$xml = simplexml_load_string($res);
			if (isset($xml->RES->R)) {
				return $xml->RES;
			}
		}
		return null;
	}

	/**
	 * Retreive an API key, first by extra params, secondly by WEBprefs
	 *
	 * @throws Exception when no api key is found
	 * @return string api_key
	 */
	private function getApiKey($extra_params)
	{
		if (!empty($extra_params['api_key'])) {
			return $extra_params['api_key'];
		}
		
		if (!empty(Framework::$WEBprefs['google_cse_api_key'])) {
			return Framework::$WEBprefs['google_cse_api_key'];
		}
		
		throw new Exception(__CLASS__.' failed to obtain an api key.');
	}
	
	/**
	 * Turn a result as returned by the CSE API into a WebSearch_Result
	 * 
	 * @param SimpleXMLElement $result
	 * @return WebSearch_Result
	 */
	private function transformResult(SimpleXMLElement $result)
	{
		return new WebSearch_Result(
			(string) $result->U, 
			(string) $result->T,
			(string) $result->S,
			array(
				"mime" => (
					!empty($result['MIME']) ? (string) $result['MIME'] : ''
				),
				"language" => (
					!empty($result->LANG) ? (string) $result->LANG : ''
				)
			)
		);
	}
}