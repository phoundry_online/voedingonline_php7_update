<?php
/**
 * Search engine, Google API: 
 * http://code.google.com/intl/nl/apis/ajaxsearch/
 *
 * @author Adrie den Hartog <adrie.denhartog@webpower.nl>
 * @version 1.0.0
 */
class WebSearch_Engine_GoogleAPI implements WebSearch_Engine
{
	/**
	 * @var bool if the values should be decoded from utf8 to latin
	 */
	private $decode_utf8;
	
	public function __construct()
	{
		global $PHprefs;
		$this->decode_utf8 = isset($PHprefs['charset']) &&
			$PHprefs['charset'] !== 'utf8' && 
			$PHprefs['charset'] !== 'utf-8'; 
	}
	
	/**
	 * Perform the specific search tasks using the given query object and
	 * return a WebSearch_ResultSet
	 *
	 * @param $search_query WebSearch_Query
	 * @return WebSearch_ResultSet
	 */
	public function search(WebSearch_Query $search_query)
	{
		$url = $this->creatUrl($search_query);
		$data = $this->executeSearch($url);
	
		if (!empty($data['responseData']) &&
				!empty($data['responseData']['results'])) {

			// Turn the results from the API in WebSearch_Result objects
			$results = array_map(
				array($this, 'transformResult'),
				$data['responseData']['results']
			);
				
			return new WebSearch_ResultSet(
				$results,
				// The guesstimate as returned by google
				$data['responseData']['cursor']['estimatedResultCount']
			);			

		}
		
		// Prepare an empty Resultset, the show must go on..
		return new WebSearch_ResultSet(array(), 0);			
	}

	/**
	 * Prepare a search through Google REST api interface
	 * 
	 * @param WebSearch_Query $search_query
	 * @return string
	 */
	private function creatUrl(WebSearch_Query $search_query)
	{
		$get = array(
			'v' => '1.0',
			'rsz' => 'large'
		);
		
		$offset = $search_query->getOffset();
		$limit = $search_query->getLimit();
		
		if ($offset) {
			$get['start'] = $offset;
		}
		
		if ($limit) {
			$get['rsz'] = $limit;
		}
		
		$q = array();
		if ($search_query->getDomain()) {
			$q[] = "site:".$search_query->getDomain()." ";
		}
		$q[] = $search_query->getSearchString();
		
		$get['q'] = join("", $q);

		return 'http://ajax.googleapis.com/ajax/services/search/web?'.
			http_build_query($get);
	}
	
	/**
	 * Execute the search against the ajax API
	 * 
	 * @param string $url
	 * @return array
	 */
	private function executeSearch($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt(
			$ch, CURLOPT_REFERER,
			(!empty($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '')
		);
		$res = curl_exec($ch);
		curl_close($ch);

		return !empty($res) ? json_decode($res, true) : array();
	}
	
	/**
	 * Decodes the retrieved values if neccesary
	 * @param string $val
	 */
	private function decode($val)
	{
		return $this->decode_utf8 ? utf8_decode($val) : $val;
	}
	
	/**
	 * Turn a result as recieved from Google into a WebSearch_Result
	 * 
	 * @param array $result
	 * @return WebSearch_Result
	 */
	private function transformResult(array $result)
	{
		return new WebSearch_Result(
			$result['unescapedUrl'],
			$this->decode($result['title']), 
			$this->decode($result['content']), 
			array(
				"escapedUrl" => $result['url'],
				"cacheUrl" => $result['cacheUrl'],
				"titleNoFormatting" => $result['titleNoFormatting']
			)
		);
	}
}