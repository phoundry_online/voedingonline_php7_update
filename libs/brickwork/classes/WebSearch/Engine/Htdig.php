<?php
/**
 * HTdig search engine for the WebSearch functionality
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class WebSearch_Engine_Htdig implements WebSearch_Engine
{
	/**
	 * (non-PHPdoc)
	 * @see classes/WebSearch/WebSearch_Engine::search()
	 */
	public function search(WebSearch_Query $search_query)
	{
		$limit = $search_query->getLimit();
		if (!$limit) {
			$limit = 15;
		}
		
		$offset = $search_query->getOffset();
		if (!$offset) {
			$page = 1;
		} else {
			$page = ($offset + $limit) / $limit;
		}
		
		$htdig = new HTdig('daily', 'and', $limit);
		$res = $htdig->doDig($search_query->getSearchString(), $page);

		if (!empty($res)) {
			// Retreive information from the response
			$results = array_map(array($this, 'transformResult'), $res);
			return new WebSearch_ResultSet($results, $htdig->matches);
		}
		
		return new WebSearch_ResultSet(array(), 0);
	}
	
	/**
	 * Turn a HTdigResult into a WebSearch_Result
	 * 
	 * @param HTdigResult $result
	 * @return WebSearch_Result
	 */
	private function transformResult(HTdigResult $result)
	{
		return new WebSearch_Result(
			$result->url,
			$result->title,
			$result->excerpt,
			array(
				"modified" => $result->modified,
				"size" => $result->size,
				"score" => $result->score,
				"percent" => $result->percent			
			)
		);
	}
}
