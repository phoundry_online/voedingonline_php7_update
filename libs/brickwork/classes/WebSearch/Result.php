<?php
/**
 * Search Result object.
 *
 * @author Adrie den Hartog <adrie.denhartog@webpower.nl>
 * @version 1.0.0
 */
class WebSearch_Result
{
	/**
	 * @var string Url
	 */
	protected $_url;

	/**
	 * @var string Title
	 */
	protected $_title;

	/**
	 * @var string Description
	 */
	protected $_description;

	/**
	 * @var array Extra (SearchEngine specific) search result data
	 */
	protected $_extras;

	/**
	 * @param string $url	
	 * @param string $title	
	 * @param string $description	
	 * @param array $extras			
	 */
	public function __construct($url, $title, $description,
		array $extras = array())
	{
		$this->_url = (string) $url;
		$this->_title = (string) $title;
		$this->_description = (string) $description;
		$this->_extras = $extras;
	}

	/**
	 * Retreive the url
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return $this->_url;
	}

	/**
	 * Retreive the title
	 *
	 * @return string
	 */
	public function getTitle ()
	{
		return $this->_title;
	}

	/**
	 * Retreive the description
	 *
	 * @param	none
	 * @return	string
	 */
	public function getDescription ()
	{
		return $this->_description;
	}

	/**
	 * Retreive the extras
	 *
	 * @param	none
	 * @return	array
	 */
	public function getExtras ()
	{
		return $this->_extras;
	}
}