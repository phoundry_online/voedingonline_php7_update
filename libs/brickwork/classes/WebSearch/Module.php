<?php
/**
 * SemiStandard version of the WebSearchModule
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class WebSearch_Module extends Webmodule
{
	public $searchresults;
	
	/**
	 * @var Pagination
	 */
	public $pages;
	
	public $totalresults;
	public $searchstring;
	
	public function loadData()
	{
		$pages_per_search = (int) min(8, $this->config->get('resultcount', 10));
		
		$this->searchstring = Utility::arrayValue($_REQUEST, 'search');
		$this->pages = new Pagination(
			$pages_per_search,
			Utility::arrayValue($this->get, 'page', 0),
			null,
			$this->qsPart('page')
		);

		if (trim($this->searchstring) !== '') {
			$lang = null;
			$site = Framework::$site;
			if ($site && $site->isMultiLingual()) {
				$lang = isset($site->currentLang) ?
					$site->currentLang : $site->defaultLang;
			}
			
			if (defined('TESTMODE') && TESTMODE) {
				$domain = 'www.webpower.nl';
			} else {
				$domain = Framework::$site->host;
			}
			
			$query_object = new WebSearch_Query($this->searchstring,
				$domain, $this->pages->limit, $this->pages->offset);
			
			/* Any engine specific extra params
			$extra_params = array();
			$query_object->setExtraParams($extra_params);
			//*/
			
			$service = new WebSearch(new WebSearch_Engine_GoogleAPI());
			$this->searchresults = $service->search($query_object);
			$this->totalresults = $this->searchresults->getTotalResultCount();
			$this->pages->setTotalItems($this->totalresults);
		} else {
			$this->pages->setTotalItems(0);
		}
		
		return true;
	}
}
