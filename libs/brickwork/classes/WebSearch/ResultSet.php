<?php
/**
 * Contains WebSearch_Result objects
 *
 * @author Adrie den Hartog <adrie.denhartog@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 * @version 1.0.0
 */
class WebSearch_ResultSet extends ResultSet
{
	/**
	 * @var int first result on the page
	 */
	public $first;
	
	/**
	 * @var int last result on the page
	 */
	public $last;
	
	/**
	 * Total amount of possible results
	 * Be aware that this is a guesstimate
	 *
	 * @var int
	 */
	protected $_total_result_count;
	
	/**
	 * Constructor
	 *
	 * @param array $source
	 * @param int $total_result_count
	 */
	public function __construct(array $source, $total_result_count)
	{
		$this->_total_result_count = (int) $total_result_count;
		
		$this->_count = count($source);
		parent::__construct($source);
	}

	/**
	 * @see Resultset::current()
	 */
	public function current()
	{		
		
		if (!($this->_source[$this->_current_index] instanceof WebSearch_Result)) {
			return false;
		} 

		return $this->_source[$this->_current_index];

	}

	/**
	 * Gets the total amount of possible results
	 *
	 * @return	$total_result_count	int
	 */
	public function getTotalResultCount ()
	{
		return $this->_total_result_count;
	}

}