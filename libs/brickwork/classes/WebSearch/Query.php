<?php
/**
 * Search query object.
 * Also holds all parameters and settings for the query at hand.
 *
 * @author Adrie den Hartog <adrie.denhartog@webpower.nl>
 * @version 1.0.0
 */
class WebSearch_Query
{
	/**
	 * @var string The unparsed query string 
	 */
	protected $_search_string;

	/**
	 * @var string The domain to limit the search to
	 */
	protected $_domain;

	/**
	 * @var int Result limit
	 */
	protected $_limit;

	/**
	 * @var int Pagination offset
	 */
	protected $_offset;

	/**
	 * @var array Extra search parameters
	 */
	protected $_extra_params;

	/**
	 * @param string $search_string	
	 * @param string $domain	
	 * @param string $limit	
	 * @param string $offset	
	 */
	public function __construct($search_string, $domain = null, $limit = null,
		$offset = null, array $extra_params = array())
	{
		$this->_search_string = (string) $search_string;
		$this->_domain = (string) $domain;
		$this->_limit = $limit ? (int) $limit : null;
		$this->_offset = $offset ? (int) $offset : null;		
		$this->_extra_params = $extra_params;
	}

	/**
	 * Retreives an array of "extra params", if available
	 *
	 * @return array
	 */
	public function getExtraParams()
	{
		return $this->_extra_params;
	}

	/**
	 * Sets an array of "extra params"
	 *
	 * @param $extra_params array
	 */
	public function setExtraParams(array $extra_params)
	{
		$this->_extra_params = $extra_params;
	}

	/**
	 * Retreive the searchstring
	 *
	 * @return string
	 */
	public function getSearchString()
	{
		return $this->_search_string;
	}

	/**
	 * Retreive the domain
	 *
	 * @return string
	 */
	public function getDomain()
	{
		return $this->_domain;
	}

	/**
	 * Retreive the search limit
	 *
	 * @return int
	 */
	public function getLimit()
	{
		return $this->_limit;
	}

	/**
	 * Retreive the search offset
	 *
	 * @return int
	 */
	public function getOffset()
	{
		return $this->_offset;
	}
}