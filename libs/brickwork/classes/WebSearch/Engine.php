<?php
/**
 * WebSearch_Engine
 *
 * Search engine base.
 *
 * @author Adrie den Hartog <adrie.denhartog@webpower.nl>
 * @version 1.0.0
 */
interface WebSearch_Engine
{
	/**
	 * Perform a search query
	 *
	 * @param	WebSearch_Query
	 * @return	WebSearch_ResultSet
	 */
	public function search(WebSearch_Query $search_query);
}
