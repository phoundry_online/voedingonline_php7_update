<?php
/**
 * Base resultset class
 * Resultsets always consist of zero indexed rowsets
 */
abstract class ResultSet implements SeekableIterator, ArrayAccess, Countable
{
	/**
	 * The source of the rows
	 * @var resource
	 */
	protected $_source;

	/**
	 * The index of the current row
	 * @var int
	 */
	protected $_current_index;

	/**
	 * The amount of rows in the source
	 * @var int
	 */
	protected $_count;

	public function __construct($source)
	{
		$this->_source = $source;
	}

	/**
	 * Get the resultset as array
	 *
	 * @param $limit
	 * @return array
	 */
	public function getAsArray($limit = null)
	{
		$res = array();

		$this->rewind();

		// Only do this slow loop if we actually have to limit
		if(!is_null($limit))
		{
			for ($i = 0; $this->valid() && $i < $limit; $i++)
			{
				$res[] = $this->current();
				$this->next();
			}
		}
		else
		{
			while($this->current())
			{
				$res[] = $this->current();
				$this->next();
			}
		}

		return $res;
	}

	/**
	 * Get a array with key value pairs
	 *
	 * @param string $key Column used as key or as value if value is not set
	 * @param string $value Column used as value
	 * @return array
	 */
	public function getSelect($key, $value = null)
	{
		$select_array = array();
		foreach($this as $row)
		{
			if(is_array($row))
			{
				if(!is_null($value))
				{
					$select_array[$row[$key]] = $row[$value];
				}
				else
				{
					$select_array[] = $row[$key];
				}
			}
			else if(is_object($row))
			{
				if(!is_null($value))
				{
					$select_array[$row->{$key}] = $row->{$value};
				}
				else
				{
					$select_array[] = $row->{$key};
				}
			}
		}
		return $select_array;
	}

	/**
	 * Set the current index to value, if true is passed as the value increment
	 * @param int|bool $value
	 */
	protected function _setCurrentIndex($value)
	{
		(true === $value) ? $this->_current_index++ : $this->_current_index = (int) $value;
	}

	/**
	 * Return the first row of the resultset
	 * @return mixedvar
	 */
	public function first()
	{
		if ($this->_current_index != 0)
		{
			$this->rewind();
		}

		return $this->current();
	}

	/**
	 * SeekableIterator
	 * @return mixedvar
	 */
	public function current(): mixed {}

	/**
	 * SeekableIterator
	 * @return bool
	 */
	public function next (): void
	{
		$this->_setCurrentIndex(true);
	}

	/**
	 * SeekableIterator
	 * @return int
	 */
	public function key(): mixed
	{
		return $this->_current_index;
	}

	/**
	 * SeekableIterator
	 * @return bool
	 */
	public function valid (): bool
	{
		return $this->offsetExists($this->_current_index);
	}

	/**
	 * SeekableIterator
	 * @return bool
	 */
	public function rewind (): void
	{
		$this->_setCurrentIndex(0);
	}

	/**
	 * ArrayAccess
	 * @param offset
	 */
	public function offsetExists ($offset): bool
	{
		if (!is_numeric($offset))
			return false;

		$offset = (int)$offset;

		return ( $offset >= 0 && $offset < $this->_count );
	}

	/**
	 * ArrayAccess
	 * (ATTENTION: resets the internal pointer)
	 * @param offset
	 */
	public function offsetGet( $offset ): mixed
	{
		if (is_numeric($offset))
		{
			$offset = (int) $offset;

			if($this->offsetExists($offset))
			{
				$this->seek($offset);
				$ret = $this->current();
				$this->rewind();
				return $ret;
			}
		}
		return false;
	}

	/**
	 * ArrayAccess
	 * @param offset
	 * @param value
	 */
	public function offsetSet ($offset, $value): void
	{
		throw new BadMethodCallException('Resultset is readonly');
	}

	/**
	 * ArrayAccess
	 * @param offset
	 */
	public function offsetUnset ($offset): void
	{
		throw new BadMethodCallException('Resultset is readonly');
	}

	/**
	 * SeekableIterator
	 * @param <type> $offset
	 */
	public function seek (int $offset): void
	{
		$this->_setCurrentIndex($offset);
	}

	/**
	 * Countable
	 * @return int
	 */
	public function count (): int
	{
		return $this->_count ? $this->_count : 0;
	}
}
