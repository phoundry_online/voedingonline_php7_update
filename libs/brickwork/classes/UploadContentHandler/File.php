<?php
class UploadContentHandler_File implements Serializable
{
	/**
	 * @var string The filename as it was uploaded
	 */
	protected $_filename;
	
	/**
	 * @var string The filename as it is on disk
	 */
	protected $_tmpname;
	
	/**
	 * @var string mimetype as reported by the browser that uploaded the file
	 */
	protected $_mime;
	
	/**
	 * @var string The folder where all uploaded files are kept
	 */
	protected $_upload_path;
	
	/**
	 * @var string Url prefix to show the uploaded files as thumbnail
	 */
	protected $_thumb_url;
	
	/**
	 * @var string Url prefix to show the uploaded files as zoomed
	 */
	protected $_zoom_url;
	
	/**
	 * Instantiates a new File
	 * @param string $filename
	 * @param string $tmpname
	 * @param string $mime the mimetype as reported by the browser
	 * @param string $upload_path default the directory as configured in $WEBprefs
	 * @param string $thumb_url default the url as configured in $WEBprefs
	 * @param string $zoom_url default the url as configured in $WEBprefs
	 */
	public function __construct($filename, $tmpname, $mime, $upload_path = null,
		$thumb_url = null, $zoom_url = null)
	{
		global $WEBprefs;
		
		$this->_filename = $filename;
		$this->_tmpname = $tmpname;
		$this->_mime = $mime;
		$this->_upload_path = $upload_path;
		$this->_thumb_url = $thumb_url;
		$this->_zoom_url = $zoom_url;
		if(null === $this->_upload_path) {
			$this->_upload_path = $WEBprefs['UploadContentHandler']['directory'];
		}
		if(null === $this->_thumb_url) {
			$this->_thumb_url = $WEBprefs['UploadContentHandler']['url_thumb'];
		}
		if(null === $this->_zoom_url) {
			$this->_zoom_url = $WEBprefs['UploadContentHandler']['url_zoom'];
		}
	}
	
	public function moveUploadedFile($file)
	{
		move_uploaded_file($file, $this->localFile());
	}

	public function __serialize(): array {
		return array(
			'_filename' => $this->_filename,
			'_tmpname' => $this->_tmpname,
			'_mime' => $this->_mime,
			'_upload_path' => $this->_upload_path,
			'_thumb_url' => $this->_thumb_url,
			'_zoom_url' => $this->_zoom_url
		);
	}

	public function __unserialize(array $data): void {

	}
	
	public function filename()
	{
		return $this->_filename;
	}
	
	public function mime()
	{
		return $this->_mime;
	}
	
	public function localFile()
	{
		return $this->_upload_path.'/'.$this->_tmpname;
	}
	
	public function thumbUrl()
	{
		return $this->_thumb_url.'/'.$this->_tmpname;
	}
	
	public function zoomUrl()
	{
		return $this->_zoom_url.'/'.$this->_tmpname;
	}
	
	public function size()
	{
		return $this->exists() ? filesize($this->localFile()) : false;
	}
	
	public function getAsArray()
	{
		return array(
			'name' => $this->_filename,
			'tmp_name' => $this->_tmpname,
			'mime' => $this->_mime,
			'thumb_url' => $this->thumbUrl(),
			'zoom_url' => $this->zoomUrl(),
			'size' => $this->size(),
		);
	}
	
	/**
	 * Tells if the file exists on disk
	 * @return bool
	 */
	public function exists()
	{
		if(null !== $this->_filename && null !== $this->_tmpname) {
			return file_exists($this->localFile());
		}
		return false;
	}
	
	public function delete()
	{
		if($this->exists()) {
			unlink($this->localFile());
		}
		$this->_filename = null;
		$this->_tmpname = null;
		$this->_mime = null;
	}
	
	public function serialize()
	{
		return serialize(array(
			'filename' => $this->_filename,
			'tmpname' => $this->_tmpname,
			'mime' => $this->_mime,
			'upload_path' => $this->_upload_path,
			'thumb_url' => $this->_thumb_url,
			'zoom_url' => $this->_zoom_url
		));
	}
	
	public function unserialize($serialized)
	{
		$arr = unserialize($serialized);
		$this->_filename = $arr['filename'];
		$this->_tmpname = $arr['tmpname'];
		$this->_mime = $arr['mime'];
		$this->_upload_path = $arr['upload_path'];
		$this->_thumb_url = $arr['thumb_url'];
		$this->_zoom_url = $arr['zoom_url'];
	}
	
	/**
	 * Get the files for a given type and key
	 * @param string $upload_key
	 * @param string $upload_type
	 * @return array An array containing UploadContentHandler_File objects
	 */
	public static function getFiles($upload_key, $upload_type = 'uploads')
	{
		Framework::startSession();
		
		// When google requests the UploadContentHandler the SESSION is NULL
		if (empty($_SESSION)) {
			return array();
		}
		
		return Utility::arrayValueRecursive($_SESSION, array($upload_type, $upload_key), array());
	}
	
	/**
	 * Sets the files for a given type and key
	 * @param array $files
	 * @param string $upload_key
	 * @param string $upload_type
	 */
	public static function setFiles($files, $upload_key, $upload_type = 'uploads')
	{
		Framework::startSession();
		if(!isset($_SESSION[$upload_type])) {
			$_SESSION[$upload_type] = array();
		}
		
		if($files) {
			$_SESSION[$upload_type][$upload_key] = $files;
		}
		else if(isset($_SESSION[$upload_type][$upload_key])) {
			unset($_SESSION[$upload_type][$upload_key]);
		}
	}
	
	/**
	 * Unsets the files for a given type and key
	 * @param string $upload_key
	 * @param string $upload_type
	 */
	public static function unsetFiles($upload_key, $upload_type = 'uploads')
	{
		self::setFiles(null, $upload_key, $upload_type);
	}
	
	public static function getFilesAsArray($upload_key, $upload_type = 'uploads')
	{
		$files = self::getFiles($upload_key, $upload_type);
		$files_arr = array();
		foreach($files as $file) {
			$files_arr[] = $file->getAsArray();
		}
		return $files_arr;
	}
}
