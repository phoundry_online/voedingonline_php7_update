<?php

/**
 * Default Site User obejct
 * This object can be used for any site and contains the most common methods
 * for working with site users. It allows a user to log in and to logout again
 * but not much more. 
 * It also has an option to store basic attributes for a user. Complex attributes
 * as arrays and objects are currently not supported.
 *
 * @author		Peter Eussen
 * @package	Brickwork
 * @subpackage	Auth
 */
class SiteUser extends AuthUser 
{
	/**
	 * The username (the name with which the user logs in)
	 * @var string
	 * @todo Why 2 username properties? we also got username_ in the AuthUser class
	 */
	public	$username;
	
	public function __construct($username, $password)
	{
		parent::__construct( $username, $password );
		
		$this->is_anonymous 	= false;
		$this->_attributeCache	= array();
			
		$this->username = $this->email = $this->displayname = null;
		// plain or encoded password
		$this->password_ = self::encryptPassword($password);
	}
	
	public function updateUser($name, $displayname, $email, $pass_key= null) {
		$this->username= $name;
		$this->displayname= $displayname;
		$this->email= $email;
		if ($pass_key) {
			$this->password_= $pass_key;
		}
		return true;
	}
	
	/**
	 * Login the current SiteUser
	 * @return bool Did we login succesfully
	 */
	public function login(Site $site = null)
	{
		if(null === $site) {
			$site = Framework::$site;
		}
		
		$use_email= Email::isValid($this->username_);
		$sql = sprintf("
			SELECT
				u.id,
				username,
				password,
				site_identifier,
				email,
				displayname
			FROM
				brickwork_site_user AS u
		  	INNER JOIN
				brickwork_site_user_site AS s
			ON
				s.site_user_id = u.id
			AND
				s.site_identifier = %s
			LEFT JOIN
				`brickwork_site_user_data` AS d
			ON
				d.site_user_id = u.id
			AND
				d.attribute = 'verified_hash'
		 	WHERE
				u.`%s` = %s
			AND
				u.password = %s
			AND
				(d.value = 'OK' OR d.value IS NULL)
			",
			DB::quoteValue($site->identifier),
			$use_email ? 'email' : 'username',
			DB::quoteValue($this->username_),
			DB::quoteValue($this->password_)
		);
		
		$row = DB::iQuery($sql)->first();

		// be safe double check found user data
		if ($row && $row->password == $this->password_){
			$this->is_logged_in	= true;
			$this->is_valid		= true;
			$this->is_anonymous	= false;
			$this->_auth_id		= $row->id;
			$this->username		= $row->username;
			$this->email			= new Email($row->email);
			$this->displayname	= $row->displayname;
			$this->logLogin();
			return true;
		}
		return false;
	}

	/**
	 * (DEPRECATED)
	 * @see BrickBootstrap#initAuthorization()
	 * @todo check if this method can be removed
	 */
	static public function initUserAuthentication()
	{
		if ( Utility::isPosted() ) 
		{
			if ( isset($_POST['LoginModule']['username'], $_POST['LoginModule']['password'])) 
			{
				$user = new Framework::$site->auth_user_class( $_POST['LoginModule']['username'], $_POST['LoginModule']['password'] );
				define("LOGIN_SUCCESS", Auth::singleton()->login( $user ));
			}
		}
		
		// check for logout
		if (isset($_REQUEST['logout'])) {
			$auth = Auth::singleton();
			$auth->logout();
			trigger_redirect('/');
		}
	}
	
	/**
	 * Decrepated
	 * @todo clean up if not used anymore
	 */
	public function generatePasswordKey($password)
	{
		return self::encryptPassword($password);
	}
	
	/**
	 * Get previous login entries for user
	 * @param $max (int) number of rows to return
	 * @return array
	 */
	public function previousLogin($max= 20)
	{
		$ret = array();

		if($this->_auth_id > 0) {
			// Skip the first entry as that is our current login
			$res = DB::iQuery(sprintf("
				SELECT
					id,
					site_user_id,
					modified_ts,
					ip
				FROM
					brickwork_site_user_login 
				WHERE
					site_user_id = %d
				ORDER BY
					id DESC
				LIMIT
					1, %d;
				",
				$this->_auth_id,
				$max
			));
				
			$res->setReturnType(DB_ResultSet::RETURNTYPE_ASSOC);
			foreach($res as $row) {
				if ($row['ip']) {
					$row['ip_address'] = long2ip($row['ip']);
				}
				$ret[] = $row;
			}
		}
		return $ret;
	}

	/**
	 * Logs a login for this user
	 * @return bool Did the log succeed
	 */
	public function logLogin()
	{
		if($this->_auth_id > 0) {
			DB::iQuery(sprintf("
				INSERT INTO
					brickwork_site_user_login 
					(site_user_id, modified_ts, ip)
				VALUES
					(%d, %s, %s);
				",
			$this->_auth_id,
			DB::quoteValue(date(DB::MYSQL_DATEFORMAT)),
			DB::quoteValue(ip2long($_SERVER['REMOTE_ADDR']))
			));
				
			return true;
		}

		return false;
	}
	
	/**
	 * Allows for custom properties stored in brickwork_site_user_data
	 * @see classes/AuthUser#__get($name)
	 */
	public function __get($name)
	{
		if(null !== ($parent = parent::__get($name))) {
			return $parent;
		}
		
		if($this->_auth_id > 0) {
			if(!isset($this->_attributeCache[$name])) {
				$value = null;
				try {
				$row = DB::iQuery(sprintf("
					SELECT
						value
					FROM
						brickwork_site_user_data
					WHERE
						site_user_id = %d
					AND
						attribute = %s
					",
					$this->_auth_id,
					DB::quoteValue($name)
				))->first();
				}
				catch(DB_Exception $e) {
					$row = false;
				}

				$this->_attributeCache[$name] = $row ? $row->value : null;
			}
			return $this->_attributeCache[$name];
		}
	}
	
	/**
	 * Allows for custom properties stored in brickwork_site_user_data
	 * @param string $name
	 * @param m $value
	 * @return unknown_type
	 */
	public function __set($name, $value)
	{
		if($this->_auth_id > 0) {
			try {
				if(null === $value) {
					DB::iQuery(sprintf("
						DELETE FROM
							brickwork_site_user_data
						WHERE
							site_user_id = %d
						AND
							attribute = %s
						",
						$this->_auth_id,
						DB::quote($name, true)
					));
				}
				else {
					DB::iQuery(sprintf("
						INSERT INTO
							brickwork_site_user_data
							(site_user_id, attribute, value)
						VALUES
							(%d, %s, %s)
						ON DUPLICATE KEY UPDATE
							value = VALUES(value),
							modified_ts = NOW()
						",
						$this->_auth_id,
						DB::quote($name, true),
						DB::quoteValue($value)
					));
				}
				
				$this->_attributeCache[$name] = $value;
				return true;
			}
			catch(DB_Exception $e) {
			}
		}
		
		return false;
	}
}
