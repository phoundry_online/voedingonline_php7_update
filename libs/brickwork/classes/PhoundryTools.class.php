<?php
/**
 * Phoundry Api Tools
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 */
class PhoundryTools
{
	/**
	 * Cache arrays so we dont fetch data twice from the db
	 */	
	protected static $tables_id_by_string = array();
	protected static $tables_by_id = array();
	protected static $tables_by_name = array();

	/**
	 * Get phoundry table id by table name
	 *
	 * @param string $table_name
	 * @param bool $all If set to true returns all found TIDs otherwise only the first
	 * @return int
	 */
	public static function getTableTID($table_name, $all = false)
	{
		if(!isset(self::$tables_by_name[$table_name]) || $all){
			$sql = sprintf('select id from phoundry_table where name="%s"', DB::quote($table_name));
	        $res = DB::iQuery($sql);
			
			if(count($res))
			{
				if($all)
				{
					$ids = array();
					foreach($res as $row) $ids[] = (int) $row->id;
					return $ids;
				}
				$id = (int) $res->first()->id;
				self::$tables_by_name[$table_name] = $id;
				self::$tables_by_id[$id] = $table_name;
			}
			else
			{
				self::$tables_by_name[$table_name] = false;
			}
		}
		return self::$tables_by_name[$table_name];
	}
	
	/**
	 * Get the Table ID by the table's String ID
	 * 
	 * @param $string_id
	 * @return int|bool false if id is not found
	 */
	public static function getTableIdByString($string_id)
	{
		if(!isset(self::$tables_id_by_string[$string_id])) {
			$row = DB::iQuery(sprintf("
				SELECT id
				FROM phoundry_table
				WHERE string_id=%s",
				DB::quote($string_id, true)
			))->first();
			
			self::$tables_id_by_string[$string_id] = $row ? $row->id : false;
		}
		return self::$tables_id_by_string[$string_id];
	}
	
	/**
	 * Get table name for a given TID
	 *
	 * @param int $tid Table Id
	 * @return string Table name
	 */
	public static function getTableName($tid)
	{
		if(!is_numeric($tid)) throw new InvalidArgumentException('the given $tid aint numeric');
		$tid = (int) $tid;

		if(!isset(self::$tables_by_id[$tid]))
		{
	        $res = DB::iQuery(sprintf('select name from phoundry_table where id="%d"', $tid));
			if($row = $res->first())
			{
				self::$tables_by_id[$tid] = $row->name;
				self::$tables_by_name[$row->name] = $tid;
			}
			else
			{
				self::$tables_by_id[$tid] = false;
			}
		}
		return self::$tables_by_id[$tid];
	}
	
	/**
	 * Get table column properties
	 *
	 * @param int $tid Table id
	 * @param string $name Column name
	 * @return stdObj|bool Object containing all fields from phoundry_column
	 */
	public static function getTableColumn($tid, $name)
	{
		return DB::iQuery(
			sprintf(
				"SELECT
					*
				FROM
					phoundry_column
				WHERE
					table_id = %d
				AND
					name = %s
				LIMIT 1",
				$tid,
				DB::quoteValue($name)
			)
		)->first();
	}
	
	/**
	 * Get table user/system filter data
	 *
	 * @param int $tid Table id
	 * @param string $string Filter name
	 * @return stdObj|bool containing rowdata from phoundry_filter
	 */
	public static function getTableFilter($tid, $string)
	{
		return DB::iQuery(
			sprintf(
				"SELECT
					*
				FROM
					phoundry_filter
				WHERE
					table_id = %d
				AND
					string_id = %s
				",
				$tid,
				DB::quoteValue($string)
			)
		)->first();		
	}
	
	/**
	 * Fetch the column name used by phoundry as the record identifier
	 *
	 * @param string $table_name Table name
	 * @return string Column used as record identifier by phoundry
	 */
	public static function getRidColumn($table_name)
	{
		$sql = sprintf("SHOW KEYS FROM `%s` WHERE Key_name = 'PRIMARY'", DB::quote($table_name));
		$res = DB::iQuery($sql);
		if(self::isError($res)) throw new Exception(sprintf("Query %s failed! MySQL returned %s ", $sql, $res->errstr));
		if($res->num_rows > 0)
		{
			return $res->first()->Column_name;
		}
		
		return 'id'; // Gevaarlijke Arjan aanname
	}
	
	/**
	 * Get the configured url for a phoundry plugin
	 * @param string $string_id
	 * @return string|bool false if not found
	 */
	public static function getPluginUrl($string_id)
	{
		$row = DB::iQuery(sprintf("
			SELECT extra
			FROM phoundry_table
			WHERE string_id = %s
			AND is_plugin = 1
			LIMIT 1",
			DB::quoteValue($string_id)
		))->first();
		
		if ($row) {
			$unserialized = unserialize($row->extra);
			if (isset($unserialized['sUrl'])) {
				return $unserialized['sUrl'];
			}
		}
		return false;
	}

	/**
	 * Encode a string using the Web Power encoding.
	 *
	 * @author Arjan Haverkamp
	 * @param $instr(string) The string to decode.
	 * @return A string containing the encoded value of $instr.
	 * @returns string
	 */
	public static function WPencode($str, $editor = false) {
		if ($editor)
			$str .= chr(255) . crc32('u' . $str . 'l');
		else
			$str .= chr(255) . crc32('w' . $str . 'p');
		$ret = '';
		for ($x = 0; $x < strlen($str); $x++)
			$ret .= bin2hex($str[$x]);
		return strrev($ret);
	}

	/**
	 * Decode a string using the Web Power encoding.
	 *
	 * @author Arjan Haverkamp
	 * @param $instr(string) The string to decode.
	 * @return A string containing the decoded value of $instr.
	 * @returns string
	 */
	public static function WPdecode($instr) {
		$instr = strrev($instr);
		$orig = '';
		if (!strlen($instr) || strlen($instr) % 2 != 0)
			return false;
		for ($i = 0; $i < strlen($instr); $i += 2)
			$orig .= chr(hexdec($instr[$i]) * 16 + hexdec($instr[$i+1]));
		$tmp = explode(chr(255), $orig);
		if (!isset($tmp[1]) || crc32('w' . $tmp[0] . 'p') != $tmp[1])
			return false;
		return $tmp[0];
	}
	
	/**
	 * Check access for a user on a table
	 *
	 * @param int $tid Table id
	 * @param int $user_id User id
	 * @param string $right Rights as defined in phoundry
	 * @return bool
	 */
	public static function checkTableAccess($tid, $user_id, $right = '')
	{
		// First user has all rights
		if($user_id == 1) {
			return true;
		}
		
		$row = DB::iQuery(sprintf("
			SELECT
				admin
			FROM
				phoundry_user
			WHERE
				id = %d
			LIMIT 1",
			$user_id
		))->first();
		
		if($row && $row->admin == self::WPencode($user_id.'=admin')) {
			 return true;
		}

		$rights = array('insert'=>'i', 'update'=>'u', 'delete'=>'d',
		'view'=>'p', 'notify'=>'n', 'import'=>'m', 'export'=>'x', 'copy'=>'c');

		
		$res = DB::iQuery(sprintf("
			SELECT rights
			FROM phoundry_user_group AS pug
			INNER JOIN phoundry_group_table AS pgt
			ON pgt.group_id = pug.group_id AND pgt.table_id = %d
			WHERE pug.user_id = %d
			",
			$tid,
			$user_id
		));
		
		if(count($res)) {
			if($right === "") {
				return true;
			}
			foreach($res as $row) {
				if(stripos($row->rights, $rights[$right])) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Check if a user created a shortcut to the given interface
	 *
	 * @param int $tid
	 * @param int $user_id
	 * @return bool
	 */
	public static function isShortcut($tid, $user_id = null)
	{
		if(class_exists('PH', false) && method_exists('PH', 'isShortcut'))
		{
			return PH::isShortcut($tid, $user_id);
		}
		if(is_null($user_id)) $user_id = $GLOBALS['PHSes']->userId;
		
		$res = DB::iQuery(sprintf("
			SELECT COUNT(*) as cnt
			FROM phoundry_user_shortcut
			WHERE user_id = %d AND table_id = %d
			",
			$user_id,
			$tid
		));
		
		return ($res->first()->cnt != 0);
	}
	
	/**
	 * Get notes for the given interface
	 *
	 * @param int $tid
	 * @param int $user_id
	 * @return array
	 */
	public static function getNotes($tid, $user_id = null)
	{
		if(is_null($user_id)) $user_id = $GLOBALS['PHSes']->userId;
		return getNotes($tid, $user_id);
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
