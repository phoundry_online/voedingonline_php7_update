<?php

/**
 * Reflection of an Table index
 *
 * @author		Peter Eussen
 * @version	1.0
 */
class Reflection_Index
{
	/**
	 * The name of the index
	 *
	 * @var string
	 */
	protected	$_indexName;
	
	/**
	 * The table the index refers to
	 *
	 * @var string
	 */
	protected	$_table;
	
	/**
	 * The schema we are in
	 *
	 * @var integer
	 */
	protected	$_schema;
	
	/**
	 * If the index is unique
	 *
	 * @var boolean
	 */
	protected	$_isUnique;
	
	/**
	 * All columns that make up the index
	 *
	 * @var Array
	 */
	protected	$_columns;
	
	/**
	 * An estimate number of unique values in the index
	 *
	 * @var int
	 */
	protected	$_cardinality;
	
	/**
	 * Whether the index is sorted or not
	 *
	 * @var boolean
	 */
	protected	$_sorted;

	/**
	 * Indicator how the key is packed if any
	 *
	 * @var string
	 */
	protected	$_packed;
	
	/**
	 * Indicator to show that the index may contain NULL values
	 *
	 * @var boolean
	 */
	protected	$_containsNull;
	
	/**
	 * The type of index that is being used
	 *
	 * @var string
	 */
	protected	$_type;
	
	/**
	 * The comment of the column/index
	 *
	 * @var string
	 */
	protected	$_comment;
	
	/**
	 * Reflection of indexes
	 *
	 * @param string 	$indexname
	 * @param string 	$table
	 * @param integer 	$schema
	 */
	public function __construct( $indexname, $table, $schema = 1 )
	{
		$this->_indexName = $indexname;
		$this->_table	  = $table;
		$this->_schema	  = $schema;
		
		$this->_isUnique	= false;
		$this->_columns		= Array();
		$this->_sorted		= true;
		$this->_cardinality = 1;
		$this->_packed		= false;
		$this->_containsNull= true;
		$this->_type		= null;
		$this->_getIndexDefinition();
	}
	
	/**
	 * Returns an Array of fields that make up the index
	 *
	 * @return unknown
	 */
	public function getColumns()
	{
		return $this->_columns;
	}
	
	/**
	 * Whether the index is unique or not
	 *
	 * @return boolean
	 */
	public function isUnique()
	{
		return $this->_isUnique;
	}
	
	/**
	 * Returns the name of the index
	 *
	 * @return string
	 */
	public function getIndexName()
	{
		return $this->_indexName;
	}
	
	/**
	 * Returns the type of the index
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->_type;
	}
	
	/**
	 * Returns the cardinality of the index
	 *
	 * @return float
	 */
	public function getCardinality()
	{
		return $this->_cardinality;
	}
	
	/**
	 * Returns the comment section of the index
	 *
	 * @return string
	 */
	public function getComment()
	{
		return $this->_comment;
	}
	
	/**
	 * Reads the index definition from the database
	 *
	 * @return boolean
	 */
	protected function _getIndexDefinition()
	{
		$res = DB::iQuery( sprintf('SHOW INDEX FROM `%s`', $this->_table ), $this->_schema );
		
		if ( self::isError( $res) )
			throw new Reflection_Exception_Table( $res->errstr, $res->errno );
		
		if ( $res->numRows > 0)
		{
			foreach( $res as $row )
			{
				if ( $row->Key_name == $this->_indexName )
				{
					$this->_isUnique = ( $row->Non_unique == 0);
					$this->_columns[ $row->Seq_in_index ] = new Reflection_Column( $row->Column_name, $row->Table, $this->_schema );
					$this->_type	 	= $row->Index_type;
					$this->_containsNull= ($row->Null == 'YES');
					$this->_packed		= (!is_null($row->Packed));
					$this->_cardinality	= $row->Cardinality;
					$this->_sorted		= ($row->Collation == 'A');
				}
			}
			
		}
		return true;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}