<?php

/**
 * Read-only view on a table in the database
 * Allows you to check out a table in a more general way
 *
 * @author		Peter Eussen
 * @version	1.0
 */
class Reflection_Table
{
	/**
	 * The name of the table you are trying to reflect
	 *
	 * @var string
	 */
	protected	$_tableName;

	/**
	 * The create statement that should be executed when you want to create this table;
	 *
	 * @var unknown_type
	 */
	protected	$_createStatement;

	/**
	 * The connection ID for the schema we are trying to reflect upon
	 *
	 * @var unknown_type
	 */
	protected	$_schema;

	/**
	 * List of all fields in the table
	 *
	 * @var unknown_type
	 */
	protected	$_field;

	/**
	 * A list of all indexes
	 *
	 * @var Array
	 */
	protected	$_index;

	/**
	 * Enter description here...
	 *
	 * @param string $table
	 */
	public function __construct( $table, $schema = 1 )
	{
		$this->_tableName 		= $table;
		$this->_schema	  		= $schema;
		$this->_createStatement = '-- No Create statement available';
		$this->_field			= Array();
		$this->_index			= Array();
		$this->_getMetaData();
	}

	static public function exists( $table, $schema = 1 )
	{
		$res = DB::iQuery( sprintf("SHOW TABLES LIKE '%s'", $table), $schema );

		if ( self::isError( $res) )
			throw new Reflection_Exception_Table( $res->errstr, $res->errno );

		if ( !$res->current() )
			return false;
		return true;
	}

	/**
	 * Displays the table name
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->getTableName();
	}

	/**
	 * Returns the name of the table
	 *
	 * @return unknown
	 */
	public function getTableName()
	{
		return $this->_tableName;
	}

	/**
	 * Checks if a field exists in this table
	 *
	 * @param 	string 	$field
	 * @return boolean
	 */
	public function __isset( $field )
	{
		return isset($this->_field[ $field ]);
	}

	/**
	 * Obtains a field from the table (if exists)
	 *
	 * @param 	string					$field
	 * @return ReflectionField|null
	 */
	public function __get( $field )
	{
		if ( isset( $this->_field[$field]) )
			return $this->_field[$field];
		return null;
	}

	/**
	 * Returns an array of all fields
	 *
	 * @return Array
	 */
	public function getColumns()
	{
		return $this->_field;
	}

	/**
	 * Returns a list of all indexes on the table
	 *
	 * @return		Array
	 */
	public function getIndexes()
	{
		return $this->_index;
	}

	/**
	 * Returns a table index by name
	 *
	 * @param 	string $name
	 * @return	ReflectionIndex
	 */
	public function getIndex( $name )
	{
		if ( isset( $this->_index[$name]) )
			return $this->_index[$name];
		return null;
	}

	/**
	 * Returns the primary key of a table
	 *
	 * return Array
	 */
	public function getPrimaryKey()
	{
		$tmp = Array();

		foreach( $this->_field as $field )
		{
			if ($field->isPrimaryKey() )
				array_push( $tmp, clone $field );
		}
		return $tmp;
	}

	/**
	 * Tries to obtain the table definition from the system
	 *
	 * @return boolean
	 * @throws TableReflectionException
	 */
	private function _getMetaData()
	{
		if ( $this->_tableExists() )
		{
			if ( $this->_getCreateStatement() )
			{
				$res = DB::iQuery( sprintf('DESCRIBE `%s`', $this->_tableName ), $this->_schema );

				if ( self::isError( $res) )
					throw new Reflection_Exception_Table( $res->errstr, $res->errno );

				if ( $res->numRows > 0)
				{
					foreach( $res as $row )
					{
						$this->_field[ $row->Field ] = new Reflection_Column( $row->Field, $this->getTableName(), $this->_schema );
					}

					$this->_index = $this->_getIndexes();
					return true;
				}
			}
		}
		else
			throw new Reflection_Exception_Table( sprintf('Table %s not found', $this->_tableName) );
		return false;
	}

	/**
	 * Checks if the table exists
	 *
	 * @return boolean
	 */
	private function _tableExists()
	{
		$res = DB::iQuery( sprintf("SHOW TABLES LIKE '%s'", $this->_tableName), $this->_schema );

		if ( self::isError( $res) )
			throw new Reflection_Exception_Table( $res->errstr, $res->errno );

		if ( $res->numRows == 0)
			throw new Reflection_Exception_Table( sprintf('Table %s does not exist in the current schema', $this->_tableName) );
		return true;
	}

	/**
	 * Tries to set the create statement attribute
	 *
	 * @return boolean
	 */
	private function _getCreateStatement()
	{
		$res = DB::iQuery( sprintf('SHOW CREATE TABLE `%s`', $this->_tableName), $this->_schema );

		if ( self::isError( $res) )
			throw new Reflection_Exception_Table( $res->errstr, $res->errno );

		if ( $res->numRows > 0)
		{
			$row = $res->first();
			$vars = get_object_vars($row);
			$this->_createStatement = $vars['Create Table'];
			return true;
		}
		return false;
	}

	/**
	 * Retrieves all indexes for  the table
	 *
	 * @return		Array()
	 */
	public function _getIndexes()
	{
		$res = DB::iQuery( sprintf('SHOW INDEX FROM `%s`', $this->_tableName ), $this->_schema );

		if ( self::isError( $res) )
			throw new Reflection_Exception_Table( $res->errstr, $res->errno );

		if ( $res->numRows > 0)
		{
			$indexes = Array();

			foreach( $res as $row )
			{
				if ( !isset( $indexes[ $row->Key_name ] ) )
					$indexes[ $row->Key_name] = new Reflection_Index( $row->Key_name, $this->getTableName(), $this->_schema );
			}
			return $indexes;
		}
		return Array();
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
