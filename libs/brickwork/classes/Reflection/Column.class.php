<?php

/**
 * Reflects a table field in MYSQL
 * Readonly for now
 *
 * @author		Peter Eussen
 * @version	1.0
 */
class Reflection_Column
{
	protected $_isPrimary;
	protected $_isNullable;
	protected $_fieldName;
	protected $_type;
	protected $_default;
	protected $_extra;
	protected $_schema;

	/**
	 * Constructor of the field, usually called from the TableReflection Class
	 *
	 * @param string $name			Field name
	 * @param string $table			The table the field is member of
	 */
	public function __construct( $name, $table, $schema = 1 )
	{
		$this->_default		= null;
		$this->_extra       = Array(); //explode(",", strtolower($extra) );
		$this->_isPrimary	= false; //( $key == 'PRI');
		$this->_isNullable	= false; // ( $nullable = 'YES');
		$this->_fieldName	= $name;
		$this->_type		= Array('datatype' => null, 'format' => null ); //$this->_parseType( $type );
		$this->_table		= $table;
		$this->_schema		= $schema;

		if ( !$this->_getColumnDefinition() )
			throw new Reflection_Exception_Column( sprintf("Column %s does not exists in table %s", $name, $table ) );
	}

	static public function exists( $name, $table, $schema = 1 )
	{
		try {
			new self($name, $table, $schema);
			return true;
		}
		catch(Reflection_Exception_Column $e) {
			return false;
		}
	}

	/**
	 * Returns the name of the field
	 *
	 * @return unknown
	 */
	public function __toString()
	{
		return $this->getColumnName();
	}

	/**
	 * Returns the name of the field
	 *
	 * @return string
	 * @deprecated
	 */
	public function getFieldName()
	{
		return $this->_fieldName;
	}

	/**
	 * Returns the column name
	 *
	 * @return unknown
	 */
	public function getColumnName()
	{
		return $this->_fieldName;
	}

	/**
	 * Returns the datatype (VARCHAR, INTEGER etc) part of the field type
	 *
	 * @return string
	 */
	public function getDataType()
	{
		return $this->_type['datatype'];
	}

	/**
	 * Returns the format type of the field
	 *
	 * @return string
	 */
	public function getTypeFormat()
	{
		if ( count($this->_type['format']) == 1 )
			return $this->_type['format'];
		return $this->_type['format'];
	}

	/**
	 * Returns the full type definition (datatype and format)
	 *
	 * @return string
	 */
	public function getType()
	{
		if ( is_null($this->_type['format']) )
			return $this->_type['datatype'];
		return sprintf("%s( %s )", $this->_type['datatype'], implode(",",$this->_type['format']) );
	}

	/**
	 * Checks if the field is Nullable
	 *
	 * @return boolean
	 */
	public function isNullable()
	{
		return $this->_isNullable;
	}

	/**
	 * Checks if the field is a primary key field
	 *
	 * @return boolean
	 */
	public function isPrimaryKey()
	{
		return $this->_isPrimary;
	}

	/**
	 * Checks if the field has a certain "extra"
	 * Extra's are ZEROFILL and auto_increment etc
	 *
	 * @param 	string $extra
	 * @return boolean
	 */
	public function hasExtra( $extra )
	{
		return in_array(strtolower($extra), $this->_extra );
	}

	/**
	 * Return sthe default value of the field
	 *
	 * @return string
	 */
	public function getDefaultValue()
	{
		return $this->_default;
	}

	/**
	 * Parses the type format to a datatype and the format
	 *
	 * @param 	string $type
	 * @return Array
	 */
	private function _parseType( $type )
	{
		if (preg_match("/(\w+)\(([a-z0-9\,\'_]+)\)/i", $type, $matches ) )
		{
			return Array('datatype'	=> strtoupper($matches[1]),
					  	 'format'	=> explode(",",$matches[2])
						);
		}
		return Array('datatype' => strtoupper($type), 'format' => null);
	}

	/**
	 * Reads the column information
	 *
	 * @return boolean
	 */
	private function _getColumnDefinition()
	{
		$res = DB::iQuery( sprintf('DESCRIBE `%s`', $this->_table ), $this->_schema );

		if ( self::isError( $res) )
			throw new Reflection_Exception_Table( $res->errstr, $res->errno );

		if ( $res->numRows > 0)
		{
			foreach( $res as $row )
			{
				if ( $row->Field == $this->_fieldName )
				{
					$this->_default		= $row->Extra;
					$this->_extra       = Array(); //explode(",", strtolower($extra) );
					$this->_isPrimary	= ($row->Key == 'PRI');
					$this->_isNullable	= ($row->Null == 'YES');
					$this->_type		= $this->_parseType( $row->Type );
					return true;
				}
			}
			return false;
		}
		return true;
	}

    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||  (is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
