<?php

/**
 * Exception for table reflections
 * 
 * @author 	Peter Eussen
 * @version 	1.0
 */
class Reflection_Exception_Column extends Reflection_Exception_Table {}