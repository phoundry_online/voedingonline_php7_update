<?php
require_once 'WebPower/Smarty/Smarty.class.php';
require_once 'WebPower/Smarty/plugins/compiler.fun.php';

/**
 * You shouldn't create this class yourself use
 * Framework::$representation instead!
 *
 * Ugly wrapper that extends Smarty instead of wraps it
 */
class WebRepresentation extends Smarty implements Representation
{
	/**
	 * @var bool If caching should be used or not
	 */
	public $caching;

	/**
	 * @var string template directory that should be used
	 */
	public $templateName;

	/**
	 * @var string Template that is used when out() is called
	 */
	public $templateFile;

	/**
	 * @var string The content type that should be used (based off Client_Info)
	 */
	protected $_content_type;

	/**
	 * @var string The mime type based off Client_Info
	 */
	protected $_mime_type;

	/**
	 * @var array
	 * @deprecated Just use $PHprefs in smarty templates
	 */
	public $PHprefs;

	/**
	 * @var WebRepresentation first instantiation of WebRepresentation
	 * @deprecated use Framework::$representation
	 */
	public static $instance;

	/**
	 * You shouldn't create this class yourself use
	 * Framework::$representation instead!
	 *
	 * Creates a new WebRepresentation for a given template
	 * language and client info.
	 *
	 * @param string $template
	 * @param void $language not used for anything
	 * @param Client_Info $client_info
	 */
	public function __construct($template, $language = null,
		Client_Info $client_info = null)
	{
		// We'll be doing a bunch of overrides so init Smarty first
		parent::Smarty();

		global $PHprefs, $WEBprefs;
		$this->PHprefs = $PHprefs;

		$this->templateName = $template;
		$this->template_dir = realpath(TEMPLATE_DIR.$this->templateName.'/');

		// If there is a template_dir for the template add it to the loader
		// so the bwfile smarty resource type works as it should
		if($this->template_dir) {
			$templateloader = Loader::getInstance()->getLoader('template');
			$templateloader->unShiftPath($this->template_dir);
		}

		// detect media type AND content type
		$this->detectHTTPAccept($client_info);

		$this->compile_dir = TMP_DIR.'templates_c/'.$this->templateName.
			'/'.$this->_content_type;

		$this->cache_dir = CACHE_DIR.'/'.$this->templateName.
			'/'.$this->_content_type;

		if(!is_dir($this->compile_dir)) {
			mkdir($this->compile_dir, 0777, true);
		}

		if(!is_dir($this->cache_dir)) {
			mkdir($this->cache_dir, 0777, true);
		}

		$this->config_dir = getenv('CONFIG_DIR');
		$this->caching = false;
		$this->cache_modified_check = false;
		$this->use_sub_dirs	= true;
		$this->compile_check = !empty($WEBprefs['compile_check']) ? $WEBprefs['compile_check'] : true;
		$this->force_compile = false;
		$this->debugging     = false;
		$this->default_resource_type = 'bwfile';

		$this->plugins_dir = Loader::getInstance()->
			getLoader('template_plugin')->getPaths();

        $this->compile_id = md5(\Locale::getDefault() /*getlocale()*/);
		$this->load_filter("pre", "script");

		// postfilter om jssen in blokken in head van pagina te kunnen zetten
		$this->load_filter('output', 'stack');
		$this->load_filter('output', 'stack_html');

		// outputfilter to put google tag manager on every page if site config 'google_tag_manager' is set
		$this->load_filter('output', 'google_tag_manager');

		// outputfilter to put the google analytics pagetracker on every page
		$this->load_filter('output', 'google_analytics');

		if(isset($WEBprefs['smarty_filters']) && is_array($WEBprefs['smarty_filters'])) {
			foreach($WEBprefs['smarty_filters'] as $filter => $type) {
				if(!in_array($type, array('output', 'pre', 'post'))) {
					throw new InvalidArgumentException(
						"WEBprefs['smartyFilters'] contains a filter of an".
						" unknown type. Available types are post, pre and ".
						"output, ".$type." given.");
				}
				else {
					$this->load_filter($type, $filter);
				}
			}
		}

		$this->register_modifier('pathByStructureId', 'SiteStructure::getPathByStructureId');
		$this->register_modifier('qsmod', array($this, 'template_modifier_qsmod'));
		$this->register_modifier('urlencodeUrl', array($this, 'template_modifier_urlencodeUrl'));
		$this->register_modifier('pageUrl', array($this, 'template_modifier_pageUrl'));

		// defun, recursive functions
		$this->register_compiler_function('fun', 'smarty_compiler_fun');
		$this->register_compiler_function('/defun', 'smarty_compiler_defun_close');
		$this->register_postfilter('smarty_postfilter_defun');

		// Extra filters and modifiers for translations
		$this->register_prefilter( Array($this, 'template_translate_prefilter') );
		$this->register_modifier( '_', Array($this, 'template_modifier_translate') );
		$this->register_modifier( 'tr', Array($this, 'template_modifier_translate') );
		$this->register_modifier('trParams', array($this, 'template_modifier_trParams'));
		$this->register_modifier( 'sprintf', Array($this, 'template_modifier_sprintf') );

		// PE 2008-11-10
		// Voor nu maar even zo opgelost. Alhoewel instance volgens mij nergens meer gebruikt
		// wordt, gooit het nog wel roet in het eten als je meerdere webrepresentations wil
		// aanmaken, wat we soms wel willen. (voor parsen mail templates bijvoorbeeld)
		// @todo weet nog iemand waarom we dit ding zetten???
		if(null === self::$instance) {
			self::$instance = $this;
		}
	}

	public function __get($name) {
		switch ($name) {
			case 'mimeType':		return $this->_mime_type;
			case 'contentType':	return $this->_content_type;
			default:	throw new AttributeNotFoundException($name . ' not found');
		}
	}

	/*public function setLanguage($l) {
		setlocale(LC_TIME, $l);
		$this->lc = getlocale(LC_TIME);
	} @depricated verhuist naar Site */

	// @see trigger_redirect functie in globals
	function redirect($url){
		trigger_redirect($url);
	}

	/**
	 * $WEBprefs['disable_caching'] Removed
	 * by Bart Lagerweij, 28-07-2008
	 */
	function setCaching($cache_id,$lifetime=60){
		if(is_null($cache_id)){
			return false;
		}

		if($lifetime>0){
			$this->caching = true;
			$this->cache_lifetime = $lifetime;

			if($this->is_cached($this->templateFile, $cache_id)){
				return $this->fetch($this->templateFile, $cache_id);
			}
		}
		return FALSE;
	}

	public function setTemplate($filename){
		$this->templateFile = $filename;
		return TRUE;
	}

	public function out($return = false, $cache_id = null, $compile_id = null)
	{
		if(null === $this->templateFile) {
			trigger_error('No template selected', E_USER_ERROR);
		}

		if(null === $compile_id && isset(Framework::$site->currentLang)) {
			$compile_id =  Framework::$site->currentLang->code;
		}

		if(defined('USE_TEMPLATE_CACHE') && USE_TEMPLATE_CACHE === true &&
			$this->caching && !is_null($cache_id))
		{
			$html = $this->fetch($this->templateFile, $cache_id, $compile_id);
		}
		else {
			$this->caching = 0; // make sure caching is off
			$html = $this->fetch($this->templateFile, null, $compile_id);
		}

		if($return) {
			return $html;
		}

		print $html;
	}


	protected function detectHTTPAccept(Client_Info $ci = null) {
		$mime_type_mapping = array(
			'xhtml'=>"text/html",
			'plain'=>"text/plain",
			'html'=>"text/html",
			'xml'=>"text/xml",
			'wml'=>"text/vnd.wap.wml",
			'xhtml'=>'*/*',
			'iphone' => 'text/html',
			'chtml'	=> 'text/html',
			'mobile'=> 'text/html'
		);

		$this->_content_type = 'html';
		$this->_mime_type = $mime_type_mapping['html'];

		if($ci && $ci->http_accept) {
			$mime_support = $ci->getPreferredContentKind();

			foreach($mime_support as $ct) {
				if(is_dir(TEMPLATE_DIR.$this->templateName.'/'.$ct)) {
					$this->_content_type = $ct;
					$this->_mime_type = $mime_type_mapping[$ct];
					break;
				}
			}
		}
	}

	public function __destruct(){

		// garbage collect all cache older than a day
		$r = mt_rand(1,10000);
		if($r === 5000){
			$this->clear_all_cache(86400);
		}
	}

	/**
	 * Translation modiftier which will be triggered whenever there is a translate which could not be preparsed
	 *
	 * @todo Worden deze parameters ooit gebruikt?!
	 * @param string 	$string
	 * @param string	$lang		defaults to Framework::$site->locale
	 * @param string	$template	The original template
	 * @param integer	$line		The line number
	 * @return string
	 */
	public function template_modifier_translate( $string, $lang = null, $template = null, $line = 0 )
	{
		if ( is_null($lang) )
		{
			if ( isset(Framework::$site->currentLang))
				$lang = Translation::getBySiteLanguage(Framework::$site->currentLang );
			else
				return $string;
		}
		else
			$lang = Translation::factory( $lang );

		return $lang->tr( $string, $template, $line );
	}

	/**
	 * Translation modifier wich can be used with params to substitute in the string
	 * @return string
	 */
	public function template_modifier_trParams()
	{
		$string = '';
		$na = func_num_args();
		if($na)
		{
			$string = func_get_arg(0);
			if(isset(Framework::$site->currentLang))
			{
				$lang = Translation::getBySiteLanguage(Framework::$site->currentLang );
				$string = $lang->tr($string, null, 0);
			}

			for ($x = 1; $x < $na; $x++)
			{
				$tmp = func_get_arg($x);
				$string = str_replace('#'.$x, $tmp, $string);
			}
		}
		return $string;
	}

	/**
	 * sprintf modifier
	 *
	 * @param  string $format
	 * @return string
	 */
	public function template_modifier_sprintf($format/*, $arg1, $argN*/)
	{
		$args = func_get_args();
		array_shift($args);

		return vsprintf($format, $args);
	}

	/**
	 * Smarty Template translation prefilter
	 * This method will prefilter smarty templates and searches for any string that will need translations
	 * which can be denoted by using a |tr or a {_ syntax. These will then be replaced BEFORE smarty does
	 * it's jobs, which will ensure that we have compiled templates which will still run quickly.
	 *
	 * @param string $source
	 * @param Smarty $smarty
	 * @return string
	 */
	public function template_translate_prefilter( $source, &$smarty )
	{
		$template = $smarty->_current_file;

		// Match all translate tags BEHIND the string
		if ( preg_match_all("/\{\"([^\"\\\\]*(?:\\\\.[^\"\\\\]*)*)\"\|(_)(:([a-z0-9_]+)(:([a-z_]+))?)?/i", $source, $matches ) )
		{
			for( $i = 0, $m = count($matches[0]); $i < $m; $i++)
			{
				$content = str_replace('\"','"', $matches[1][$i]);
				$langcode= ( isset($matches[4][$i]) && !empty($matches[4][$i]) ? $matches[4][$i] : Framework::$site->currentLang->code );

				$line    = count(explode("\n",substr( $source, 0, strpos($source, $matches[1][$i]))));
				$content = Translation::factory( $langcode )->tr( $content, $template, $line );
				//$content = "(translated to)$content(using language $lang and namespace $ns)";
				$source = str_replace( $matches[0][$i], '{"' . str_replace('"','\"',$content ) . '"', $source );
			}
		}

		// Match all remaining shorthand notations
		if ( preg_match_all("/\{_\"([^\"\\\\]*(?:\\\\.[^\"\\\\]*)*)\"/i", $source, $matches ) )
		{
			for( $i = 0, $m = count($matches[0]); $i < $m; $i++)
			{
				$line   = count(explode("\n",substr( $source, 0, strpos($source, $matches[1][$i]))));
				if ( !is_null(Framework::$site->currentLang))
					$content = Translation::getBySiteLanguage( Framework::$site->currentLang )->tr( str_replace('\"','"', $matches[1][$i]), $template, $line);
				else
					$content = str_replace('\"','"', $matches[1][$i]);

				//$content = "(shorthand translated to)$content(using language $lang and namespace $ns)";
				$source = str_replace( $matches[0][$i], '{"' . str_replace('"','\"',$content ) . '"', $source );
			}
		}

		// Truuk om ook de filename en line (sort of) mee te krijgen bij runtime vertalingen
		$source = preg_replace('/\|tr(\||\})/i','|tr:null:$smarty.template:$smarty.const.__LINE__${1}',$source);
		return $source;
	}

	public function template_modifier_qsmod($key, $value, $query_string = null, $arg_seperator = '&amp;')
	{
		 return Utility::replaceGet($key, $value, $query_string, $arg_seperator);
	}

	public function template_modifier_urlencodeUrl($url)
	{
		return Utility::urlencodeUrl($url);
	}

	public function template_modifier_pageUrl($structure_id, $lang = null, $site_identifier = null)
	{
		if(!empty($lang))
		{
			$lang = new Site_Language(Framework::$site->identifier, $lang);
		}
		return SiteStructure::getFullPathByStructureId($structure_id, $site_identifier, $lang);
	}

   /**
     * test if resource needs compiling
     *
     * @param string $resource_name
     * @param string $compile_path
     * @return boolean
     */
    function _is_compiled($resource_name, $compile_path)
    {
    	if ( parent::_is_compiled( $resource_name, $compile_path ) && $this->compile_check)
    	{
    		$cts = filemtime($compile_path);

    		if ( isset(Framework::$site) && !is_null( Framework::$site->defaultLang) )
    		{
    			$tr = Translation::getBySiteLanguage( isset( Framework::$site->currentLang ) ? Framework::$site->currentLang : Framework::$site->defaultLang );

    			return ($tr->lastModified() <= $cts );
    		}
    		return true;
    	}

    	return false;
    }
}
