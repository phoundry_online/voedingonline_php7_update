<?php
/**
 * Error observer which sends an sentry log to the central server
 *
 * @author Martijn van Maurik <martijn.van.maurik@phoundry.nl>
 * @version 0.1
 */
class Error_Observer_LogSentry implements ErrorObserver
{
	/**
	 * @var Raven_Client
	 */
	private $client;

	/**
	 * @var
	 */
	private $error_level;

	/**
	 * @param string $raven_dsn
	 * @param $error_level
	 */
	public function __construct($raven_dsn, $error_level)
	{
		$this->client = new Raven_Client($raven_dsn, array(
			'release' => Framework::version(),
		));

		$this->error_level = $error_level;
	}

	/**
	 * This method gets called by the ErrorHandler for each error that occurs
	 *
	 * @param \ErrorVoedingonline $obj
	 * @param string $source
	 * @param array $backtrace
	 */
	public function notify(ErrorVoedingonline $obj, $source, $backtrace)
	{
		if (($this->error_level & $obj->errno) === $obj->errno) {
			$data['message'] = $obj->errstr;
			$data['level'] = $this->client->translateSeverity($obj->errno);
			$data['tags']['php_version'] = phpversion();
			$this->client->capture($data, $backtrace);
		}
	}
}