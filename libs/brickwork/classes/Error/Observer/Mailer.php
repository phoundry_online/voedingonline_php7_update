<?php
/**
 * Error observer which sends an email on error to the configured emailadresses
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 0.1
 */
class Error_Observer_Mailer implements ErrorObserver
{
	/**
	 * @var array
	 */
	protected $_recipients;
	
	/**
	 * @var string
	 */
	protected $_from_email;
	
	/**
	 * @var string
	 */
	protected $_from_name;
	
	/**
	 * Creates a new Error observer that mails the configured
	 * recipients on errors
	 * 
	 * @param array $recipients list of emailadresses that get the errors mailed
	 * @param string $from_email [optional] Adress used as from email
	 * @param string $from_name [optional] Name used as from name
	 */
	public function __construct(array $recipients = null, $from_email = null,
		$from_name = null)
	{
		foreach($recipients as $r) {
			if(!Email::isValid($r)) {
				throw new InvalidArgumentException("Recipients contains a ".
					"invalid emailadress");
			}
		}
		if(null !== $from_email && !Email::isValid($from_email)) {
			throw new InvalidArgumentException("From_email should contain a ".
				"valid emailadress");
		}
		if(null !== $from_name && (!is_string($from_name) || !$from_name)) {
			throw new InvalidArgumentException("From_name should contain a ".
				"valid (non empty) string");
		}
		
		$this->_recipients = $recipients;
		$this->_from_email = $from_email ? $from_email : "errormailer@webpower.nl";
		$this->_from_name = $from_name ? $from_name : "Error Mailer";
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/ErrorObserver#notify($obj, $source, $backtrace)
	 */
	public function notify(ErrorVoedingonline $obj, $source, $backtrace)
	{
		$url = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://').
			$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			
		$referer = isset($_SERVER['HTTP_REFERER']) ?
			'Referer: '.$_SERVER['HTTP_REFERER'] . "<br>\n" : '';
		
		$this->_sendMail(
			"Error Mailer: ".$obj->errstr,
			sprintf(
				"%d: %s in file: %s at line: %d<br>
				URL: %s<br>
				UA: %s<br>
				%s%s
				<pre>%s</pre>",
				$obj->errno,
				$obj->errstr,
				$obj->errfile,
				$obj->errline,
				$url,
				isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
				$referer,
				$source,
				print_r(array_slice($backtrace, 0, 10), true)
			)
		);
	}
	
	/**
	 * Sends the mail to the configured recipients
	 * 
	 * @param string $subject
	 * @param string $body
	 * @return bool
	 */
	protected function _sendMail($subject, $body)
	{
		$m = new Mail();
		$m->Encoding = "quoted-printable";
		$m->html = $body;
		$m->subject = $subject;
		$m->setFrom($this->_from_email, $this->_from_name);
		
		foreach($this->_recipients as $r) {
			$m->AddAddress($r);
		}
		
		return $m->Send();
	}
}