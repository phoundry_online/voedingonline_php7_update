<?php

/**
 * Automatic XML sitemap generator
 * This class automatically generates a sitemap in XML format so it can be used
 * by searchengines for crawling the website.
 * 
 * @author 	Peter Eussen
 * @version	2.1
 * @package	Brickwork
 * @subpackage	ContentHandlers
 */
class GoogleSitemapContentHandler extends ContentHandler 
{
	protected $use_cache		= TRUE;
	protected $page_cache_time = -1;

	private $items		= array();

	private $name = 'GoogleSitemapContentHandler';

	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
		
	}

	public function init()
	{
		$feed = !empty($this->path[0]) ? $this->path[0] : '';

		$cache_id = md5($feed);
		$lifetime = 3600;

		// Check if the feed string holds extra information (the only we would expect is the language)
		if ( !empty($feed) && Features::brickwork('translation') )
		{
			try
			{
				// We only need the first section so make sure we have that and then try setting it
				$lang = array_shift(explode('/',$feed));
				$l    = new Site_Language( Framework::$site->identifier, $lang );
				Framework::$site->setLanguage( $l );
			}
			catch (Language_Exception $e)
			{
				// Niets doen.. gewoon de default aanhouden
			}
		}
	}


	public function out($return = FALSE)
	{
		$res = $this->_generate_sitemap_xml();

		header('Content-type: text/xml; charset=iso-8859-1');
		if($return){
			return $res;
		} else {
			print $res;
		}
	}

	private function _generate_sitemap_xml()
	{
		$stack = SitemapModule::getSitemap();
		$retval ='<?xml version="1.0" encoding="iso-8859-1"?>
		<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">';

		$retval .= $this->_generateURLList( $stack );
		$retval .= "\n</urlset>";	
		return $retval;
											   
	}
	
	private function _generateURLList( $itemlist )
	{
		$retval = '';
		
		foreach ($itemlist as $item )
		{
			if ($item->link[0]== '/') {
				// make relative url absolute
				$protocol= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http');
				$url= $protocol. '://'. $_SERVER['HTTP_HOST']. $item->link;
			}
			else {
				$url= $item->link;
			}
			$lastmod= date('Y-m-d');
			$prio= 1.0 ;
		
			$retval .= sprintf('
		   <url>
			      <loc>%s</loc>
					<lastmod>%s</lastmod>
					<changefreq>%s</changefreq>
					<priority>%d</priority>
			</url>', $url, $lastmod, 'daily', $prio);

			// Get children as well
			if ( count($item->items) )
			{
				$retval .= $this->_generateURLList( $item->items );
			}
		}
		return $retval;
	}
}
