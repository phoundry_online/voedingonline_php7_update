<?php
/**
 * $Id: FormField.package.php 837 2009-02-27 13:45:57Z christiaan.baartse $
 */
/**
 * Interface om velden controleerbaar te maken
 *
 * @package		Brickwork
 * @subpackage	Form
 */
interface FieldValidator
{
	public function chkValue();
}

/**
 * Field
 *
 * @version 0.1.0
 * @last_modified 21-6-2007
 * @changes:
 * - currentValue return value or if value is NULL return defaultvalue
 *
 * @package		Brickwork
 * @subpackage	Form
 */
abstract class Field implements FieldValidator
{
	/**
	 * Name
	 *
	 * @var	String
	 */
	protected $name_;
	
	/**
	 * Value for HTML
	 *
	 * @var	String
	 */
	protected $value_;
	
	/**
	 * Default value
	 *
	 * @var	String
	 */
	protected $default_value;
	
	/**
	 * Title or label for the HTML field
	 *
	 * @var	String
	 */
	protected $label_;
	
	/**
	 * Order
	 *
	 * @var	Float
	 */
	protected $prio_;
	
	/**
	 * Readonly flag, the field will be presented as text,
	 * for disabling use disabled
	 *
	 * @var	Boolean
	 */
	protected $readonly_;
	
	/**
	 * This field can have more than one selection
	 * TODO Moet dat wel hier is dit niet alleen voor selects?
	 *
	 * @var	String
	 */
	protected $multiplicity_;
	
	/**
	 * Mandatory field
	 *
	 * @var	Boolean
	 */
	protected $required_ = FALSE;
	
	/**
	 * Additional field info
	 *
	 * @var	String
	 */
	protected $info_;
	
	/**
	 * Form
	 * Backwards reference to form in which this field is set
	 *
	 * @var	Form
	 */
	protected $form_;
	
	/**
	 * Disabled
	 * The field is presented as a disabled field
	 *
	 * @var	Boolean
	 */
	protected $disabled_ = FALSE;
	
	/**
	 * Active
	 * Is this field active
	 *
	 * @var	Boolean
	 */
	protected $active_ = TRUE;
	
	/**
	 * Make a new field
	 *
	 * @param	String	$name
	 * @param	Mixed	$value	or selected option value
	 */
	public function __construct($name, $value=NULL)
	{
		$this->name  = $name;
		$this->value = $value;
	}
	
	/**
	 * Check the value of the field
	 *
	 * This function needs to be implemented for each Field so that we can
	 * check if the set value for the vield is a valid value
	 */
	public function chkValue()
	{
		trigger_error(sprintf('Validator not implemented for this class %s, be gone you very evil code!', get_class($this)), E_USER_NOTICE);
		return FALSE;
	}
	
	/**
	 * onLoad event triggerd by Form::load
	 *
	 * @return	mixed	TRUE or Error
	 */
	public function onLoad()
	{
		return TRUE;
	}
	
	/**
	 * onBeforeHandleRequest event triggerd by Form::handleRequest
	 *
	 * @return mixed TRUE or Error
	 */
	public function onBeforeHandleRequest()
	{
		return TRUE;
	}
	
	/**
	 * onAfterHandleRequest event triggerd by Form::handleRequest
	 *
	 * @return mixed TRUE or Error
	 */
	public function onAfterHandleRequest()
	{
		return TRUE;
	}
	
	/**
	 * onSuccessHandleRequest event triggerd by Form::handleRequest
	 *
	 * @return mixed TRUE or Error
	 */
	public function onSuccessHandleRequest()
	{
		return TRUE;
	}
	
/*
onLoad

onBeforeHandleRequest
onSuccessHandleRequest
*/
	/**
	* Magic settert
	*/
	public function __set($name, $value)
	{
		switch ($name)
		{
			case 'name':
				$this->name_ = $value;
				break;
				
			case 'value':
				$this->value_ = $value;
				break;
				
			case 'defaultValue':
				$this->default_value = $value;
				break;
				
			case 'label':
				$this->label_ = $value;
				break;
				
			case 'prio':
				$this->prio_ = $value;
				break;
				
			case 'readonly':
				$this->readonly_ = (bool) $value;
				break;
				
			case 'form':
				$this->form_ = $value;
				break;
				
			case 'multiplicity':
				$this->multiplicity_ = $value;
				break;
				
			case 'required':
				$this->required_ = (bool)$value;
				break;
				
			case 'info':
				$this->info_ = $value;
				break;
				
			case 'disabled':
				$this->disabled_ = (bool)$value;
				break;
				
			case 'active':
				$this->active_ = (bool)$value;
				break;
				
			default:
				throw new AttributeNotFoundException(sprintf('Attribute %s not found', $name));
		}
	}
	
	/**
	 * Magic gettert
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'name':
				return $this->name_;
				
			case 'value':
				return $this->value_;
				
			case 'defaultValue':
				return $this->default_value;
				
			case 'currentValue':
				if ($this->value_ == NULL)
				{
					return $this->default_value;
				}
				else
				{
					return $this->value_;
				}
				break;
				
			case 'label':
				return $this->label_;
				
			case 'prio':
				return $this->prio_;
				
			case 'readonly':
				return $this->readonly_;
				
			case 'form':
				return $this->form_;
				
			case 'id':
				return sprintf("%s_%s", $this->form_->name, $this->name_);
				
			case 'required':
				return $this->required_;
				
			case 'multiplicity':
				return $this->multiplicity_;
				
			case 'info':
				return $this->info_;
				
			case 'disabled':
				return $this->disabled_;
				
			case 'active':
				return $this->active_;
				
			default:
				throw new AttributeNotFoundException(sprintf('Attribute %s not found', $name));
		}
	}
	
	/**
	 * Handle Request
	 *
	 * @param	Array	$value
	 * @return	void
	 */
	public function handleRequest($value)
	{
		$this->value = $value;
	}
	
	public function __sleep()
	{
		$this->form_ = null;
		return array_keys(get_object_vars($this));
	}
}

/**
 * A field decorator is the parent of all form fields. Each field from a form
 * system like avoid-form of db-form is wrapped with a decorator to represent
 * itself in a webapplication. Each of those wrapped fields is extended from
 * the decorator.
 *
 * @author Mick van der Most van Spijk
 * @version 1.1.0
 * @changes:
 * - isEmpty()
 *
 * @package		Brickwork
 * @subpackage	Form
 */
class FieldDecorator extends Field
{
	/**
	 * Container for original wrapped field
	 * aka avoid-field or db-field
	 *
	 * @var	Field
	 */
	protected $_field;
	
	/**
	 * Wrap a field with the decorator
	 * @param	Field
	 */
	public function __construct($field)
	{
		// save original field
		$this->_field = $field;
		
		// inherit
		$this->name  = $field->name;
		$this->label = $field->label;
		$this->prio  = $field->prio;
		$this->form  = $field->form;
		
		$this->multiplicity = $field->multiplicity;
	}
	
	/**
	 * Check if field is empty
	 * @return unknown_type
	 */
	public function isEmpty()
	{
		return FALSE;
	}
	
	/**
	 * Get data from field
	 * Most of the properties are inherited from the wrapped field
	 * Some attributes as for instance value must be properly set with a setter
	 */
	public function __get($name)
	{
		switch ($name)
		{
			case 'field':
				return $this->_field;
				
			case 'label':
				// return $this->_field->label;
				return $this->label_;
				
			case 'prio':
				// return $this->_field->prio;
				return $this->prio_;
				
			case 'formFieldName':
				return sprintf("%s[%s]", $this->_field->form->name, $this->_field->name);
				
			case 'id':
				return sprintf("%s_%s", $this->_field->form->name, $this->_field->name);
				
			case 'value':
				return $this->value_;
				
			default:
				try
				{
					return parent::__get($name);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Field#__set()
	 */
	public function __set($name, $value)
	{
		switch ($name)
		{
			default:
				try
				{
					return parent::__set($name, $value);
				}
				catch (AttributeNotFoundException $e)
				{
					throw $e;
				}
				break;
		}
	}
}

require_once 'BrickDateTime.class.php';

/*
 * Velden
 */
require_once 'Form/TextField.class.php';
require_once 'Form/SelectField.class.php';
require_once 'Form/MailSelectField.class.php';
require_once 'Form/RadioField.class.php';
require_once 'Form/CheckboxField.class.php';
// require_once 'Form/SingleCheckboxField.class.php';
require_once 'Form/GenderField.class.php';
require_once 'Form/NumberField.class.php';
require_once 'Form/UnsignedNumberField.class.php';
require_once 'Form/DateField.class.php';
require_once 'Form/EmailField.class.php';
require_once 'Form/PhoneField.class.php';
require_once 'Form/FileField.class.php';
require_once 'Form/FileUploadField.class.php';
require_once 'Form/PhotoField.class.php';
require_once 'Form/PhotoUploadField.class.php';
require_once 'Form/CountryField.class.php';
require_once 'Form/HiddenField.class.php';
require_once 'Form/CursorField.class.php';
require_once 'Form/CompositeField.class.php';
require_once 'Form/BankAccountField.class.php';
require_once 'Form/IBANField.class.php';
require_once 'Form/YearField.class.php';
require_once 'Form/URLField.class.php';
require_once 'Form/BooleanField.class.php';
require_once 'Form/HouseNumberField.class.php';
require_once 'Form/PostalcodeField.class.php';
require_once 'Form/HTMLField.class.php';
require_once 'Form/ButtonField.class.php';
require_once 'Form/PasswordField.class.php';
require_once 'Form/DoublePasswordField.class.php';
require_once 'Form/RadioField.class.php';
require_once 'Form/TextareaField.class.php';
require_once 'Form/FreetextField.class.php';
require_once 'Form/CaptchaField.class.php';
require_once 'Form/DMDSubscribeField.class.php';