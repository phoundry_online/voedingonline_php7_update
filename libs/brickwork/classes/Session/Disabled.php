<?php
/**
 * This session handler makes it possible to use sessions without using them
 * so that functions that rely on $_SESSION still work for single requests
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class Session_Disabled implements Session_Handler
{
	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionOpen($save_path, $session_name)
	 */
	public function sessionOpen($save_path, $session_name)
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionClose()
	 */
	public function sessionClose()
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionRead($sessionid)
	 */
	public function sessionRead($sessionid)
	{
		return '';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionWrite($sessionid, $data)
	 */
	public function sessionWrite($sessionid, $data)
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionDestroy($sessionid)
	 */
	public function sessionDestroy($sessionid)
	{
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionGarbageCollect($max_life_time)
	 */
	public function sessionGarbageCollect($max_life_time)
	{
		return true;
	}
}