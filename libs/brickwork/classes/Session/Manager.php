<?php
/**
 * Class that manages the loading/initialisation of sessions
 * This class should be used whenever you need a link to a session.
 * It is recommended that you do not use the $_SESSION variable
 * and that you use this class instead of calling session_start
 * yourself!
 *
 * @author		Peter Eussen
 * @copyright	Web Power (http://www.webpower.nl)
 * @package		Brickwork
 * @version		1.0
 */
abstract class Session_Manager
{
	/**
	 * Local variable for the instance you want to have
	 *
	 * @var Session_Abstract
	 */
	protected static $_instance;

	/**
	 * Creates a session object for you
	 * This method will check it's internal cache for a previously created
	 * session, if it is found, it will use that, otherwise it will create
	 * a new session object based on the settings in the PREFS.php.
	 * You can influence the session creation process by setting these two vars:
	 * - sessiontype		: can be Default or Database (MemCache in the make)
	 * - sessionautostart	: Defaults to true, for automatic creation of session
	 *
	 * @throws RuntimeException if the Framework is not initialized
	 * @return Session
	 */
	public static function factory()
	{
		if(null === self::$_instance) {
			if(!isset(Framework::$site->identifier)) {
				throw new RuntimeException("Framework should be initialized ".
					"before trying to use Session_Manager#factory()");
			}

			global $PHprefs, $WEBprefs;
			$sessionHandler	= Utility::coalesce(
				Utility::arrayValue($WEBprefs, 'sessionHandler'),
				Utility::arrayValue($WEBprefs, 'Session.Handler'),
				Utility::arrayValue($PHprefs, 'sessionHandler')
			);

			if($sessionHandler && $sessionHandler != 'file') {
				$url = parse_url($sessionHandler);
				if(isset($url['schema'])) {
					$sessionHandler = 'Session_'.ucfirst($url['schema']);
					$sessionHandler = new $sessionHandler($url);
				}
				else {
					$sessionHandler = 'Session_'.ucfirst($sessionHandler);
					$sessionHandler = new $sessionHandler();
				}

				// Debian does never call sessionGarbageCollect so we
				// have to call it by hand for custom session handlers
				$prob = (int) ini_get('session.gc_probability');
				$prob = $prob ? $prob : 1;
				$divisor = (int) ini_get('session.gc_divisor');
				$divisor = $divisor ? $divisor : 100;
				if($prob === $divisor || rand($prob, $divisor) === $divisor) {
					$lt = (int) ini_get('session.gc_maxlifetime');
					$sessionHandler->sessionGarbageCollect($lt ? $lt : 24);
				}
			}
			else {
				$sessionHandler = null;
				$options = array();
			}

			self::$_instance = new Session(Framework::$site->identifier, $sessionHandler);
		}
		return self::$_instance;
	}
}
