<?php
/**
 * Database managed session
 * When using this class all sessions will be stored in the database.
 * This is the recommened session handler for all websites that run
 * on more than one machine and has little session data.
 *
 * @author		Peter Eussen
 * @version		1.0
 * @package		Brickwork
 * @copyright	Web Power (http://www.webpower.nl)
 */
class Session_Database implements Session_Handler
{
	/**
	 * @var string The current session name
	 */
	protected $_session_name;

	/**
	 * @var string the Site_identifier extracted from $_session_name
	 */
	protected $_site_identifier;

	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionOpen($save_path, $session_name)
	 */
	public function sessionOpen($save_path, $session_name)
	{
		$this->_session_name = $session_name;

		// reverse engineer $session_name to $site_identifier
		// Handler's shouldn't care about site_identifiers etc but being
		// able to query on site_identifier is nijs
		$this->_site_identifier = str_replace('_', '.',
			substr($this->_session_name, 5));

		return true;
	}

	/**
	 * There is no need to specifically close the session
	 * as the database connection is closed on __destruct anyway
	 * @see classes/Session/Session_Handler#sessionClose()
	 * @return bool
	 */
	public function sessionClose()
	{
		return true;
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionRead($sessionid)
	 */
	public function sessionRead($sessionid)
	{
		$maxlifetime = (int) ini_get('session.gc_maxlifetime');
		if ($maxlifetime === 0) {
			$maxlifetime = 24;
		}

		try {
			$res = DB::iQuery(sprintf('
				SELECT
					data
				FROM
					brickwork_session
				WHERE
					identifier = %s
				AND
					site_identifier = %s
				AND
					lastaccess > DATE_SUB(NOW(), INTERVAL %d MINUTE)
				',
				DB::quoteValue($sessionid),
				DB::quoteValue($this->_site_identifier),
				$maxlifetime
			));
		}
		catch(DB_Exception $e) {
			trigger_error('Session read error: '.$e, E_USER_ERROR );
		}

		if ($row = $res->first()) {
			return $row->data;
		}
		return '';
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionWrite($sessionid, $data)
	 */
	public function sessionWrite($sessionid, $data)
	{
		try {
			$res = DB::iQuery(sprintf('
				INSERT INTO
					brickwork_session
					(identifier, site_identifier, data, lastaccess,
					ip, createdon)
				VALUES
					(%s, %s, %s, NOW(), INET_ATON(%s), NOW())
				ON DUPLICATE KEY UPDATE
					data = VALUES(data),
					lastaccess = VALUES(lastaccess)
				',
				DB::quoteValue($sessionid),
				DB::quoteValue($this->_site_identifier),
				DB::quoteValue($data),
				DB::quoteValue(Utility::arrayValue($_SERVER, 'REMOTE_ADDR'))
			));
		}
		catch(DB_Exception $e) {
			trigger_error('Session write error: '.$e, E_USER_ERROR );
		}
		return true;
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionDestroy($sessionid)
	 */
	public function sessionDestroy($sessionid)
	{
		try {
			$res = DB::iQuery(sprintf('
				DELETE FROM
					brickwork_session
				WHERE 
					identifier = %s
				AND
					site_identifier = %s
				',
				DB::quoteValue($sessionid)
			));
		}
		catch(DB_Exception $e) {
			trigger_error('Session delete error: '.$e, E_USER_ERROR );
		}
		return ($res->num_rows == 1);
	}

	/**
	 * (non-PHPdoc)
	 * @see classes/Session/Session_Handler#sessionGarbageCollect($max_life_time)
	 */
	public function sessionGarbageCollect($max_life_time)
	{
		try {
			$res = DB::iQuery(sprintf('
				DELETE FROM
					brickwork_session
				WHERE
					lastaccess < DATE_SUB(NOW(), INTERVAL %d MINUTE)
				',
				$max_life_time
			));
		}
		catch(DB_Exception $e) {
			trigger_error('Session delete error: '.$e, E_USER_ERROR );
		}

		return true;
	}
}
