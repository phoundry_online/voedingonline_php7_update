<?php
/**
 * This interface needs to be implemented if you want to use
 * a class as session handler
 * @author		Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright	Web Power, http://www.webpower.nl
 */
interface Session_Handler
{
	/**
	 * Open function, this works like a constructor in classes and is
	 * executed when the session is being opened. The open function expects
	 * two parameters, where the first is the save path and
	 * the second is the session name.
	 *  
	 * @param string $save_path
	 * @param string $session_name
	 * @return bool
	 */
	public function sessionOpen($save_path, $session_name);
	
	/**
	 * Close function, this works like a destructor in classes and is
	 * executed when the session operation is done.
	 *  
	 * @return bool
	 */
	public function sessionClose();
	
	/**
	 * Read function must return string value always to make save handler
	 * work as expected. Return empty string if there is no data to read.
	 * Return values from other handlers are converted to boolean expression.
	 * TRUE for success, FALSE for failure.
	 *  
	 * @param string $sessionid
	 * @return string
	 */
	public function sessionRead($sessionid);
	
	/**
	 * Writes the session data to the array when the request is finished
	 * @param string $sessionid
	 * @param string $data
	 * @return bool
	 */
	public function sessionWrite($sessionid, $data);
	
	/**
	 * Deletes a given session by id
	 * @param string $sessionid
	 * @return bool
	 */
	public function sessionDestroy($sessionid);
	
	/**
	 * Deletes expired sessions
	 * @param int $maxLifeTime
	 * @return bool
	 */
	public function sessionGarbageCollect($max_life_time);
}
