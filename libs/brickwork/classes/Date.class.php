<?php
/**
* Date 
*
* @author Jorgen Teunis
* @version 1.0.0
* @last_modified 2/15/2006 Mick van der Most van Spijk
*/

class Date {
	public $isoDate;
	public $timeStamp;
	public $year;
	public $month;
	public $day;

	public $hour;
	public $minute;
	public $second;

	public $weekday;
	public $weekNr;

	public function __construct($timestamp=NULL) {
		$this->timeStamp = is_null($timestamp) ? time() : $timestamp;
		$this->isoDate = date('Y-m-d',$this->timeStamp);
		$this->year		= (int)date('Y',$this->timeStamp);
		$this->month	= (int)date('m',$this->timeStamp);
		$this->day		= (int)date('d',$this->timeStamp);
		$this->weekday	= (int)date('w',$this->timeStamp);
		$this->weekNr	= (int)date('W',$this->timeStamp);
		$this->hour		= (int)date('h', $this->timeStamp);
		$this->minute	= (int)date('i', $this->timeStamp);
		$this->second	= (int)date('s', $this->timeStamp);
	}

	static function checkDate($year, $month, $day){
		return checkdate($month, $day, $year);
	}

	/** @todo is deze nodig? en waarvoor? */
	static function getByIso($iso){
		$tmp = explode('-', $iso);
		$ts = mktime(0, 0, 0, $tmp[1], $tmp[2], $tmp[0]);
		return new Date($ts);
	}

	public function niceDate(){
		return date("l m F Y",$this->timeStamp);
	}
	
	public function getTimestamp(){
		return $this->timeStamp;
	}

	public function getMySQLDate() {
		return date("Y-m-d 00:00:00", $this->timeStamp);
	}

	public static function mysql2date($mysql) {
		if (empty($mysql))
			return NULL;

		// 2006-03-12 09:12:34
		$parts = preg_split('/\D/', $mysql);
		$parts[3] = !empty($parts[3]) ? $parts[3] : 0;
		$parts[4] = !empty($parts[4]) ? $parts[4] : 0;
		$parts[5] = !empty($parts[5]) ? $parts[5] : 0;

		// [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )
		$date = new Date(mktime($parts[3], $parts[4], $parts[5], $parts[1], $parts[2], $parts[0]));
		return $date;
	}
}
