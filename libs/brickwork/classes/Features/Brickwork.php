<?php
class Features_Brickwork
{
	/**
	 * Brickwork Translation
	 *
	 * @return bool
	 */
	public static function translation()
	{
		return Reflection_Table::exists('brickwork_site_language') &&
			Reflection_Table::exists('brickwork_site_structure_i18n') &&
			Reflection_Column::exists('lang_id','brickwork_site_structure_path') &&
			Reflection_Column::exists('lang_id','brickwork_site_structure_path_archive');
	}
	
	public static function ACL()
	{
		if (!Reflection_Table::exists('brickwork_auth_object') ||
			!Reflection_Table::exists('brickwork_auth_right') ||
			!Reflection_Table::exists('brickwork_auth_role')) {
			return false;
		}

		if(!Features::brickwork('authorization')) {
			return false;
		}

		if(isset($GLOBALS['WEBprefs']['acl']['enable'])) {
			return $GLOBALS['WEBprefs']['acl']['enable'];
		}

		return true;
	}

	public static function sitemap()
	{
		if(class_exists('SitemapModule', true))
		{
			$res = DB::iQuery('SELECT `class` FROM `brickwork_module`');
			foreach($res as $row)
			{
				if(strtolower($row->class) == 'sitemapmodule' || 
				  (class_exists($row->class) &&
				  is_subclass_of($row->class, 'SitemapModule'))) return true;
			}
		}
		return false;
	}

	public static function authorization()
	{
		if(isset($GLOBALS['WEBprefs']['authorization']['enable']))
		{
			return $GLOBALS['WEBprefs']['authorization']['enable'];
		}
		return true;
	}
	
	public static function authorization_registration()
	{
		$registration = true;
		if(isset($GLOBALS['WEBprefs']['authorization']['registration']))
		{
			$registration = $GLOBALS['WEBprefs']['authorization']['registration'];
		}
		
		return $registration && Features::brickwork('authorization');
	}
}