<?php
/**
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
abstract class CommandLine_Script
{
	/**
	 * @var CommandLine
	 */
	protected $_c;
	
	/**
	 * @var array The passed named arguments
	 */
	protected $_named_args;
	
	public function __construct($scriptname, $argv = null, $in = null, $out = null)
	{
		$this->_c = new CommandLine($scriptname, $argv, $in, $out);
	}
	
	/**
	 * Runs the actual script, this method determines which method to call
	 * depending on the given arguments and finally calls it returning the
	 * methods output
	 * 
	 * @return mixedvar
	 */
	public function __invoke($default = 'run')
	{
		$args = array();
		$this->_named_args = array();
		foreach($this->_c->getAllArgs() as $k => $v) {
			if(is_int($k)) {
				$args[] = $v;
			}
			else {
				$this->_named_args[$k] = $v;
			}
		}
		
		$method = array_shift($args);
		if(false === $method) {
			$method = $default;
		}
		
		if(array_key_exists('help', $this->_named_args)) {
			$method = 'help';
		}
		
		try {
			$method = new ReflectionMethod($this, $method);
		}
		catch(ReflectionException $e) {
			$method = new ReflectionMethod($this, $default);
		}
		
		$class = new ReflectionClass($this);
		$this->_printDocComment($class->getDocComment());
		$this->_println();
		
		return $method->invokeArgs($this, $args);
	}
	
	/**
	 * Shows help for the specified method
	 * @param string $method If not specified lists all available methods
	 */
	public function help($method = null)
	{
		if(null === $method) {
			// Default help function list all available methods
			$this->_println("Listing available methods:");
			$class = new ReflectionClass($this);
			foreach($class->getMethods(ReflectionMethod::IS_PUBLIC) as $m) {
				$name = $m->getName();
				
				// Dont show php's magic functions
				if(0 !== strpos($name, "_")) {
					$params = array();
					foreach($m->getParameters() as $p) {
						if($p->isDefaultValueAvailable()) {
							$params[] = "$".$p->getName()." = ".var_export($p->getDefaultValue(), true);
						}
						else {
							$params[] = "$".$p->getName();
						}
					}
					$this->_println('  '.$name.'('.implode(', ', $params).')');
				}
			}
		}
		else {
			try {
				$method = new ReflectionMethod($this, $method);
				$this->_printDocComment($method->getDocComment());
			}
			catch(ReflectionException $e) {
				$this->_println("There is no such method run help without any ".
					"params to see all available methods");
			}
		}
	}
	
	/**
	 * Shorthand to CommandLine#println()
	 * 
	 * @param string $msg
	 * @return int|bool
	 */
	protected function _println($msg = null) { return $this->_c->println($msg); }
	
	/**
	 * Shorthand to CommandLine#readln()
	 * 
	 * @return string|bool
	 */
	protected function _readln() { return $this->_c->readln(); }
	
	/**
	 * Prints a phpDocBlock nicely formatted
	 * @param string $doc
	 */
	private function _printDocComment($doc)
	{
		foreach(explode("\n", $doc) as $l) {
			if($l = substr($l, 3)) {
				$this->_println($l);
			}
		}
	}
}