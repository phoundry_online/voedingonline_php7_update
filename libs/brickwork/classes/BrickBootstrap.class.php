<?php
/**
 * Brickwork Bootstrap class
 * Class to wrap all the bootstrap calls so that you do not have to modify
 * "standard" files but can just extend the functionality and add your own.
 *
 * The way this works is as follows: Just remove all but the basic includes
 * from your index.php and replace it with this:
 *
 * BrickBootstrap::run();
 *
 * run() takes one argument and that is the loadChain.
 * With this you can manipulate the way things are loaded. If you want some
 * custom loading you just modify this chain. You could also extend
 * BrickBootstrap with your own and modify the initLoadChain method.
 *
 * @author Peter Eussen
 * @package Brickwork
 * @version 1.0
 */
class BrickBootstrap
{
	static protected $_customChain = array();

	/**
	 * Bootstraps the system by callling all methods in the loadChain
	 * This method boots the system by calling all initializing functions,
	 * and when those don't fail, it will call the handleRequest method.
	 * The optional parameter may contain a loadChain:
	 * an array of functions that should be called in order of occurance.
	 * Exceptions thrown in these functions will result in a DIE of the website
	 *
	 * @param array $loadChain Array of callables
	 * @return void
	 */
	static public function run(array $loadChain = null)
	{
		if (!$loadChain) {
			$loadChain = self::initLoadChain();
		}

		ksort($loadChain, SORT_NUMERIC);
		foreach ($loadChain as $chain) {

		    if ($chain instanceof Closure) {
                $chain();
            } else {
                if (isset($chain[0]) && $chain[0] == __CLASS__) {
                    // Less expensive as below and used most often
                    self::{$chain[1]}();
                } else {
                    // Expensive call
                    call_user_func($chain);
                }
            }
		}
	}

	/**
	 * Adds a chain to the bootstrap load chain
	 *
	 * @param string|array $chain
	 * @param int $offset
	 * @return boolean
	 */
	static public function addChain($chain, $offset = null)
	{
		if (!is_callable($chain, true)) {
			return false;
		}

		if (is_null($offset)) {
			if (count(self::$_customChain)) {
				$offset = max(max(array_keys(self::$_customChain)), 130) + 1;
			} else {
				$offset = 131;
			}
		}

		self::$_customChain[$offset] = $chain;
		return true;
	}

	/**
	 * Determines the sequence of loading that needs to be done
	 *
	 * @return array
	 */
	static public function initLoadChain()
	{
		$chain = array(
			10	=> array(__CLASS__, 'initAutoloader'),
			30	=> array(__CLASS__, 'initErrorHandler'),
			40	=> array(__CLASS__, 'initTimezone'),
			50 	=> array(__CLASS__, 'initSite'),
			70	=> array(__CLASS__, 'initClient'),
			90 	=> array(__CLASS__, 'initRepresentation'),
			110	=> array(__CLASS__, 'initFramework'),
			115 => array(__CLASS__, 'checkServerLoad'),
			120	=> array(__CLASS__, 'initAuthorization'),
			130	=> array(__CLASS__, 'handleRequest'),
		);

		return (self::$_customChain + $chain);
	}

	static public function initAutoloader()
	{
		self::initSystemPaths();
		self::initExtensions();
		self::initCustomPaths();
		self::initComposerAutoloader();
	}

	static public function initSystemPaths()
	{
		static $run;
		if ($run === true) return false;

		global $WEBprefs;

		// Classes loader
		require_once INCLUDE_DIR.'/classes/Loader/Class.php';
		$classloader = new Loader_Class(
			(!defined('TESTMODE') || !TESTMODE) ?
				CACHE_DIR.'/autoloader.cache' : "", // empty file disables cache
			isset($GLOBALS['WEBprefs']['Autoloader']['cachetime']) ?
	    		$GLOBALS['WEBprefs']['Autoloader']['cachetime'] : 600
		);
		$classloader->unShiftPath(
			array(
				rtrim(INCLUDE_DIR, '/') . '/modules/',
				rtrim(INCLUDE_DIR, '/') . '/classes/',
			)
		);
		$classloader->register();

		// Smarty plugin loader
		$paths = isset($WEBprefs['smarty_plugins']) ?
			$WEBprefs['smarty_plugins'] : array(
			// the default under SMARTY_DIR
			'plugins',
			// custom smarty plugins
			rtrim(INCLUDE_DIR, '/').'/smarty_plugins/'
		);

		$smarty_plugins = new Loader_SmartyPlugin();
		$smarty_plugins->unShiftPath($paths);

		// Template loader
		$templateloader = new Loader_Template();
		$templateloader->unShiftPath(BRICKWORK_DIR . '/templates');

		// Script loader
		$scriptloader = new Loader_Script();
		$scriptloader->unShiftPath(rtrim(BRICKWORK_DIR, '/').'/public');

		$loader = Loader::getInstance();
		$loader->addLoader('class', $classloader);
		$loader->addLoader('template_plugin', $smarty_plugins);
		$loader->addLoader('template', $templateloader);
		$loader->addLoader('script', $scriptloader);

		$run = true;
		return true;
	}

	static public function initExtensions()
	{
		static $run;
		if ($run === true) return false;

		global $WEBprefs;
		if (!empty($WEBprefs['extensions']) &&
				is_array($WEBprefs['extensions'])) {
			$extensions_path = !empty($WEBprefs['extensions.Path'])
				? $WEBprefs['extensions.Path']
				: rtrim(PROJECT_DIR, '/').'/libs';

			$class_paths = array();
			$smarty_plugin_paths = array();
			$template_paths = array();
			$script_paths = array();

			foreach ($WEBprefs['extensions'] as $extension) {
				$extension_path = $extensions_path.'/'.$extension;

				if (!is_dir($extension_path)) {
					throw new Exception("Extension ".$extension.
						" could not be found");
				}

				// The include path to the extension's classes folder
				if (is_dir($extension_path.'/classes/')) {
					$class_paths[] = $extension_path.'/classes/';
				}

				if (is_dir($extension_path.'/smarty_plugins/')) {
					$smarty_plugin_paths[] = $extension_path.'/smarty_plugins/';
				}

				if (is_dir($extension_path.'/templates')) {
					$template_paths[] = $extension_path.'/templates';
				}

				if (is_dir($extension_path.'/public')) {
					$script_paths[] = $extension_path.'/public';
				}
			}

			Loader::getInstance()->getLoader('class')->unShiftPath(
				$class_paths
			);
			Loader::getInstance()->getLoader('template_plugin')->unShiftPath(
				$smarty_plugin_paths
			);
			Loader::getInstance()->getLoader('template')->unShiftPath(
				$template_paths
			);
			Loader::getInstance()->getLoader('script')->unShiftPath(
				$script_paths
			);
		}

		$run = true;
		return true;
	}

	static public function initCustomPaths()
	{
		static $run;
		if ($run === true) return false;

		global $WEBprefs;
		$class_loader = Loader::getInstance()->getLoader('class');

		// Controleren of er in de WEBprefs extra paden gedefinieerd zijn
		// die gebruikt moeten worden
		// Zo ja, deze toevoegen aan de array van autoload paden en in de
		// juiste volgorde zetten.
		if (isset($WEBprefs['Autoloader']['paths']) &&
				is_array($WEBprefs['Autoloader']['paths'])) {
			$paths = $WEBprefs['Autoloader']['paths'];
			array_reverse($paths);
			$class_loader->unShiftPath($paths);
		}

		$class_loader->unShiftPath(
			array(
				rtrim(CUSTOM_INCLUDE_DIR, '/') . '/custom_modules/',
				rtrim(CUSTOM_INCLUDE_DIR, '/') . '/custom_classes/',
			)
		);

		// Additional custom template paths
		// The site specific template path gets pushed in
		// WebRepresentation#__construct()
		if (isset($WEBprefs['templateDirs']) &&
				is_array($WEBprefs['templateDirs'])) {
			$template_loader = Loader::getInstance()->getLoader('template');
			$template_paths = $template_loader->getPaths();
			$paths = array();

			foreach ($WEBprefs['templateDirs'] as $dir) {
				$dir = rtrim($dir, '/');
				if (!in_array($dir, $template_paths)) {
					$paths[] = $dir;
				}
			}

			if ($paths) {
				array_reverse($paths);
				$template_loader->unShiftPath($paths);
			}
		}

		Loader::getInstance()->getLoader('template_plugin')->unShiftPath(
			rtrim(CUSTOM_INCLUDE_DIR, '/').'/smarty_plugins/'
		);

		$scriptloader = Loader::getInstance()->getLoader('script');
		$scriptloader->unShiftPath(rtrim(PROJECT_DIR, '/').'/htdocs');

		$run = true;
		return true;
	}

	public static function initComposerAutoloader()
	{
		/* First load brickwork's composer autoload */
		$brickwork_composer = INCLUDE_DIR . 'vendor/autoload.php';
		if (is_file($brickwork_composer)) {
			include_once $brickwork_composer;
		}

		/* Second load project's composer autoload */
		$project_composer = PROJECT_DIR . '/vendor/autoload.php';
		if (is_file($project_composer)) {
			include_once $project_composer;
		}
	}

	static public function initTimezone()
	{
		global $PHprefs;
		if (!isset($PHprefs['timezonePHP']) ||
				!date_default_timezone_set($PHprefs['timezonePHP'])) {
			throw new Exception(
				'No valid timezone set in $PHprefs[\'timezonePHP\']'
			);
		}

		$mysql = Utility::arrayValue($PHprefs, 'timezoneMySQL');
		if (is_null($mysql)) {
			$dtz = new DateTimeZone($PHprefs['timezonePHP']);
			$now = new DateTime('now', new DateTimeZone('UTC'));
			$mysql = (string) ($dtz->getOffset($now) / 3600).':00';

			// Mysql wants to have a + in front of timezones ahead
			if('-' != substr($mysql, 0, 1)) $mysql = '+'.$mysql;
			$PHprefs['timezoneMySQL'] = $mysql;
		}

		DB::iQuery("SET time_zone = '".$mysql."'");
		return true;
	}

	static public function initErrorHandler()
	{
		ErrorHandler::singleton();

		if (!empty($GLOBALS["PHprefs"]['errorRcpts'])) {
            if (substr($_SERVER['HTTP_HOST'], 0, 4) == 'www.') {
                $domain = substr($_SERVER['HTTP_HOST'], 4);
            } else {
                $domain = $_SERVER['HTTP_HOST'];
            }
			ErrorHandler::attach(
				new Error_Observer_Mailer(
					$GLOBALS["PHprefs"]['errorRcpts'],
					"errormailer@".$domain,
                    $domain
				)
			);
		}

		/**
		 * Stack on the Raven > Sentry errorhandler when exists and not in TESTMODE
		 */
		if (class_exists('Raven_Client') && defined('TESTMODE') && !TESTMODE) {
			if (isset($GLOBALS["PHprefs"]['raven']['dsn'])) {
				$raven_dsn = $GLOBALS["PHprefs"]['raven']['dsn'];
			} else {
				$raven_dsn = 'https://0b241abe83a84112adef0d0cc52546c3:5c035f50160d4ad1b7c44f495daa0ab2@logsentry.phoundry.nl/2';
			}
			if (isset($GLOBALS["PHprefs"]['raven']['error_level'])) {
				$error_level = $GLOBALS["PHprefs"]['raven']['error_level'];
			} else {
				$error_level = E_ERROR | E_USER_ERROR;
			}
			ErrorHandler::attach(new Error_Observer_LogSentry($raven_dsn, $error_level));
		}

		// attach screen error observer
		if (TESTMODE) {
			$observerScreen	= new ErrorScreener();
			$observerLog = new ErrorLogger(
				PROJECT_DIR.'/tmp/'.$_SERVER['REMOTE_ADDR'].'.log'
			);
			ErrorHandler::attach($observerScreen);
		} else {
			$observerLog = new ErrorLogger(PROJECT_DIR.'/tmp/error.log');
		}

		ErrorHandler::attach($observerLog);

		return true;
	}

	static public function initSite()
	{
		// load site
		if (!Framework::loadSiteByHost(
			$_SERVER['HTTP_HOST'],
			$_SERVER['SERVER_PORT']
		)) {
			throw new Exception(
				'Site not loaded, '.$_SERVER['HTTP_HOST'].':'.
				$_SERVER['SERVER_PORT']
			);
		}
		return true;
	}

	static public function initClient()
	{
		// load client info
		if (!Framework::loadClientInfo('web')) {
			die('Client not supported');
		}
		return true;
	}

	static public function initRepresentation()
	{
		// load web representation
		if (!Framework::loadRepresentation('web')) {
			die('Could not load representation');
		}

		return true;
	}

	static public function initFramework()
	{
		global $PHprefs, $WEBprefs;

		Framework::$PHprefs = $PHprefs;
		Framework::$WEBprefs = $WEBprefs;
		return true;
	}

	static public function initAuthorization()
	{
		if (Features::brickwork('authorization')) {
			// Login
			if (isset($_POST['authorization']['user'])) {
				$auth = Auth::singleton();
				$user = $_POST['authorization']['user'];
				$password = Utility::arrayValue(
					$_POST['authorization'],
					'password'
				);

				//if (!empty($_POST['authorization']['captcha_'])) {
				//	$auth->errors[] = 'You don\'t seem to be human!';
				//} else {
					$auth->login(trim($user), trim($password));
				//}
			}

			// Logout
			if (isset($_REQUEST['authorization']['logout'])) {
				Auth::singleton()->logout();
			}
		}
		return false;
	}

	static public function getRequestPath()
	{
		$path_info = isset($_SERVER['PATH_INFO']) ?
			$_SERVER['PATH_INFO'] : array();
		return $path_info;
	}

	/**
	 * Initialises the content handler (called from handleRequest)
	 *
	 * @param 	string $path
	 * @return boolean
	 */
	static public function initContentHandler( $path = null )
	{
		if (is_null($path)) {
			$path = self::getRequestPath();
		}

		// Load contenthandler
		if (Framework::loadContentHandler($path, $_SERVER['QUERY_STRING'])) {
			Framework::$contentHandler->errors = array();
			return true;
		}
		return false;
	}

	/**
	 * Parses a 404 request
	 *
	 */
	static public function parse404Request()
	{
		if (!empty($_SERVER['REQUEST_URI'])) {
			$subdir = substr($_SERVER['REQUEST_URI'], 1);

			if (preg_match(
				'/^([a-z0-9_\/|-]+)\?{0,1}(.*)$/i', $subdir, $matches
			) > 0) {

				$search	= !empty($matches[1]) ? $matches[1] : '';
				$search  = trim($search, '/');
				$query	= !empty($matches[2]) ? '?'.$matches[2] : '';

				$sql = sprintf(
					'SELECT
						site_structure_id
					FROM
						brickwork_readable_links
					WHERE
						site_identifier = "%s"
					AND
						subdir = "%s"',
					DB::quote(Framework::$site->identifier),
					DB::quote($search)
				);

				$res = DB::iQuery($sql);

				if ($row = $res->first()) {
					header('Status: 302 Moved');
					// Move to /page/name or /page/number URL
					if (defined('BRICKWORK_BEAUTIFUL_URL') &&
							BRICKWORK_BEAUTIFUL_URL === true) {
						header(
							'Location: /page/'.
							SiteStructure::getPathByStructureId(
								$row->site_structure_id
							).$query
						);
					} else {
						header(
							'Location: /page/'.$row->site_structure_id.$query
						);
					}
					exit;
				}
			}
		}
	}

	/**
	 * Handles a normal 404 request
	 * Initialises the contenthandler and prints the 404
	 */
	static public function handle404Request()
	{
		// Load contenthandler
		Framework::loadContentHandler(null, '');

		// print pretty error page from template root directory : 404.tpl
		Framework::$contentHandler->print404();
	}

	/**
	 * Loads the content handler and will start processing the request
	 *
	 * @return	void
	 */
	static public function handleRequest()
	{
		// Check if the page should be cached
		$pagecache = false;
		if (PageCache::isEnabled()) {
			$pagecache = PageCache::getInstance();
		}

		$cache = $pagecache && $pagecache->cacheable();

		if ($cache) {
			if ($pagecache->isCached()) {
				// Have a cache, use it
				$pagecache->send();
			}

			// Otherwise capture output so we can use that as cache
			ob_start();
		}

		if (self::initContentHandler()) {
			header(
				'P3P: policyref="http://'.$_SERVER['HTTP_HOST'].
				'/index.php/p3p", CP="NOI DSP COR NID PSA ADM OUR IND NAV COM"'
			);
			Framework::$contentHandler->out(false);
		}

		if ($cache) {
			$pagecache->put(ob_get_clean());
			// Maybe not the quickest way,
			// but this ensures we set things like etag etc
			$pagecache->send();
		}
	}

	/**
	 * checks if the server is under stress and should be closing things off
	 *
	 * @return void
	 */
	static public function checkServerLoad()
	{
		global $WEBprefs;
		if (isset($WEBprefs['pressurevalve'])) {
			$pv = PressureValve::getInstance();

			if (!$pv->allowed(true)) {
				if (!headers_sent()) {
					if (isset($WEBprefs['pressurevalve']['busyurl'])) {
						header(
							"Location: ".$WEBprefs['pressurevalve']['busyurl']
						);
						exit(0);
					}
					header("HTTP/1.1 503 Service Temporarirly Overloaded");
					exit(0);
				}
				echo "503: Service Temporarily Overloaded,",
					"please try again later";
				exit(0);
			}
		}
	}

}
