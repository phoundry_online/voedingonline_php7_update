<?php

/**
 * Client helper routines
 * This client allows you to quickly create a client side version of your
 * Brickwork webservice. It requires a couple of things but then gives you 
 * some nice features which I will explain below.
 * 
 * The Class requires two (and optionally a third) argument to construct.
 * The first one is the URL to the Brickwork webservice. This is the URL
 * UP TO the format. Typically this will end on index.php/service
 * 
 * The second argument is the service (or module) you want to call. The
 * final argument is optional and indicates which transfer method should
 * be used. By default it will use 'serial', but you can opt for using soap
 * or XML when that suits you most.
 * 
 * After creation you can call your API by simply calling a function in the
 * constructed class. For example:
 * 
 * Webservice we want to call:
 * http://myservice/index.php/service/xml/mymodule/mymethod
 * 
 * Code:
 * 
 * $client = new Service_Client('http://myservice/index.php/service','mymodule');
 * $reply  = $client->mymethod();
 * 
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.1
 * @todo Test with Classes
 */
class Service_Client
{
	const AUTH_BASIC = CURLAUTH_BASIC;
	const AUTH_NTLM = CURLAUTH_NTLM;
	const AUTH_ANY = CURLAUTH_ANY;
	
	/**
	 * URL to the service content handler
	 *
	 * @var string
	 */
	public $url;
	
	/**
	 * The service you want to call
	 *
	 * @var string
	 */
	public $service;
	
	/**
	 * The preferred api call you want to use
	 *
	 * @var string
	 */
	public $type;
	
	/**
	 * The username to use when authenticating
	 * 
	 * @var string
	 */
	protected $_authUser;
	
	/**
	 * The password to use when authenticating
	 * 
	 * @var string
	 */
	protected $_authPassword;
	
	/**
	 * Authentication method
	 * 
	 * @var string
	 */
	protected $_authMethod;
	
	/**
	 * Initialises the client
	 *
	 * @param string $url
	 * @param string $service
	 */
	public function __construct($url, $service = null, $type = 'serial')
	{
		$this->_authUser = $this->_authPassword = null;
		
		if (!is_null($service))	{
			$this->url = $url;
			$this->service = $service;
		}
		else {
			$path = explode('/', $url);
			$this->service = array_pop($path);
			// remove type
			array_pop($path);
			$this->url = implode('/', $path);
		}
		$this->type = strtolower($type);
	}
	
	/**
	 * Sets the username to use for authentication
	 * 
	 * @param string $username
	 * @param string $password
	 * @return bool
	 */
	public function setAuthentication($username, $password,
		$method = self::AUTH_ANY)
	{
		$this->_authUser 		= $username;
		$this->_authPassword 	= $password;
		$this->_authMethod		= $method;
		return true;		
	}
	
	/**
	 * Wrapper for submit methods
	 * You can use this wrapper when you need to POST data to the webservice. If you want to
	 * submit a file, put a '@' in front of the filename in the array value.
	 *
	 * @param string $method
	 * @param array $args
	 * @return mixed
	 */
	public function __call($method, array $args)
	{
		
		if ($this->type === 'soap') {
			$soapClient = new SoapClient($this->_getWsdlUrl(),
				array("encoding" => "utf-8"));
			
			$data = $soapClient->__call($method, $args);

			try {
				return self::XML2PHP($data);
			}
			catch (Exception $e) {
				return $data;
			}
		}
		
		$handle = curl_init();
		$url = implode('/',
			array($this->url, $this->type, $this->service, $method));
		
		$query = array();
		if ($args) {
			
			foreach($args as $id => $param) {
				if (is_int($id)) {
					$query['arg'.$id] = $param;
				}
				else {
					$query[$id] = $param;
				}
			}
			
		}
		$args = http_build_query($query);
			
		//logDebug("Client.Service::__call => $url ");
		
        //curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($handle, CURLOPT_HEADER, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $args);
        //curl_setopt($handle, CURLOPT_REFERER, Utility::arrayValue($_SERVER,'HTTP_REFERER'));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_USERAGENT, 'Brickwork Service Client 1.0');
        curl_setopt($handle, CURLOPT_POST, true);

        $this->_addAuthentication($handle);
        
        $response = curl_exec($handle);
 		
        if (false === $response) {
        	throw new Exception("Request failed: ".curl_error($handle), curl_errno($handle));
        }
        
 		switch ($this->type) {
 			case 'serial':
				$data = $this->is_serialized($response) ? @unserialize($response) : $response;
				break;
 			case 'xml':
 				$data = new SimpleXMLElement($response);
 				$data = self::XML2PHP($data->content);
 				break;
 			case 'json':
 				$data = @json_decode($response);
 				break;
 			default: 					
 		}
 		
 		
 		if ($data === false) {
 			throw new Exception("Malformed data received".(TESTMODE ? ": " . $response : ""));
 		}
 			
 		return $data;
	}

	static public function is_serialized($data, $strict = true)
    {
        // if it isn't a string, it isn't serialized.
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace = strpos($data, '}');
            // Either ; or } must exist.
            if (false === $semicolon && false === $brace)
                return false;
            // But neither must be in the first X characters.
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            // or else fall through
            case 'a' :
            case 'O' :
                return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }
	
	/**
	 * Wrapper for submit methods
	 * You can use this wrapper when you need to POST data to the webservice. If you want to
	 * submit a file, put a '@' in front of the filename in the array value.
	 * 
	 * @deprecated
	 * @param string $method
	 * @param array $args
	 * @return mixed
	 */
	protected function _submit($method, array $args)
	{
		return $this->__call($method, $args);
	}
	
	/**
	 * Converts an XML reply content XML string into PHP variables.
	 *
	 * @throws InvalidArgumentException
	 * @param string $data
	 * @return mixed
	 */
	static public function XML2PHP($data)
	{
		try {
			return self::_toPHP($data instanceof SimpleXMLElement ?
				$data : new SimpleXMLElement($data));
		}
		catch (Exception $e) {
			throw new InvalidArgumentException("Unable to parse xml: ".$e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Converts all elements below the specified element into PHP variables
	 *
	 * @param SimpleXMLelement $element
	 * @return mixed
	 */
	static protected function _toPHP(SimpleXMLelement $element)
	{
		if (isset($element['type'])) {
			if (strtolower($element['type']) === 'boolean') {
				return (bool) $element;
			}
				
			if (strtolower($element['type']) !== 'array') {
				$class = $element['type'];
				if(class_exists($class, true)) {
					$o = new $class();
					
					foreach($element->children() as $child) {
						$attr = $child->getName();
						$o->{$attr} = self::_toPHP($child);
					}
				}
				else {
					//logDebug("Class not found: " . $e->getMessage() );
				}
			}
			
			$tmp = array();
			
			foreach($element->children() as $child) {
				if ($child->getName() === 'entry' && isset($child['no'])) {
					$tmp[(int)$child['no']] = self::_toPHP($child);
				}
				else {
					$tmp[$child->getName()] = self::_toPHP($child);
				}
			}
			
			return $tmp;
		}
		return (string) $element;
	}
	
	protected function _addAuthentication(&$curl)
	{
        if (!is_null($this->_authUser)) {
        	// CURLAUTH_NTLM
        	curl_setopt($curl, CURLOPT_HTTPAUTH, $this->_authMethod );
        	curl_setopt($curl, CURLOPT_USERPWD,
        		$this->_authUser.':'.$this->_authPassword);
        }
	}
	
	protected function _getWsdlUrl()
	{
		return rtrim($this->url,"/").'/wsdl/'.$this->service;
	}
}

/*
$uc = new Service_Client('http://portal.webpower.peter.eussen.dev.local/index.php/service/','me','soap');
var_dump($uc->version(''));  
*/
