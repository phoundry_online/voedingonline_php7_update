<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * Default Exception for Service Errors
 *
 * @author Peter Eussen
 * @package	Brickwork
 * @subpackage Service
 * @version	1.0
 */
class Service_Exception extends Exception {}
