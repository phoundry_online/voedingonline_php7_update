<?php
/**
 * Service package
 * 
 * @package	 Brickwork
 * @subpackage Service
 * @author		 Peter Eussen
 */

/**
 * Interfaces can not be autoloaded
 */
require_once(dirname(__FILE__).'/Module/Interface.php');

/**
 * Abstract (Web)Service module
 * All service modules should extend this class in order to make it work
 * properly.
 *
 * Golden rules for making your own services:
 * 1) All functions you want to expose should be PUBLIC
 * 2) All functions you want to expose should end with Request
 * 3) All functions you want to expose should be in the same class as that 
 *    is loaded (so not in parent classes)
 * 4) PHP-doc your functions!
 * 5) Avoid complex datatypes like arrays and objects for in and output as 
 *    they are not universally supported at the moment
 * 
 * @author		Peter Eussen
 * @version	1.0
 * @package	Brickwork
 * @subpackage	Service
 */
abstract class Service_Module extends Module implements Service_Module_Interface
{
	/**
	 * The name of the configuration table
	 *
	 * @var string
	 */
	protected $_configTable;
	
	/**
	 * The name of the content Table
	 *
	 * @var string
	 */
	protected $_contentTable;
	
	/**
	 * Indicator to tell if the module is a restricted module
	 *
	 * @var boolean
	 */
	protected $_restricted;
	
	/**
	 * How the module is restricted
	 *
	 * @var string
	 */
	protected $_restrictionMethod;
	
	/**
	 * List of IP restrictions
	 *
	 * @var array
	 */
	protected $_ipRestrictions;

	/**
	 * The internal ID for this module
	 *
	 * @var int
	 */
	public $id;
	
	/**
	 * The code of the module (this is the short alias that you assign in Phoundry)
	 *
	 * @var string
	 */
	public $code;
	
	/**
	 * The type of request that is being done
	 * This can be soap, wsdl, xml or anything else, and depicts the kind of output
	 * the user is requesting. In case you need some special output for some special
	 * method.
	 * 
	 * @var string
	 */
	public $type;
	
	/**
	 * The full name of the module as defined in Phoundry
	 *
	 * @var string
	 */
	public $moduleName;
	
	/**
	 * The cache time that should be used for this request
	 * 
	 * @todo implement caching
	 * @var integer
	 */
	public $cachetime;
	
	/**
	 * All post parameters
	 *
	 * @var array
	 */
	public 	$post;
	
	/**
	 * All get parameters
	 *
	 * @var array
	 */
	public $get;
	
	/**
	 * All configuration variables
	 *
	 * @todo load configuration data from table when set 
	 * @var array
	 */
	public $config;
	
	/**
	 * The headers for the current request
	 *
	 * @var array
	 */
	public $header;
	
	/**
	 * Additional path parameters
	 *
	 * @var array
	 */
	public $path;
	
	
	/**
	 * Indicator to show if debugging should be turned on
	 *
	 * @var boolean
	 */
	public $debug;
	
	/**
	 * Initialises the module
	 *
	 * @param string $code
	 * @param string $type defaults to soap
	 */
	public function __construct($code, $type = 'soap')
	{
		$this->_loadModuleByCode($code);
		
		$this->post = &$_POST;
		$this->get = &$_GET;
		$this->type = $type;	// Assume SOAP, because that is the only one who wants to create its own class
		$this->config = array();
		$this->path = array();
		
		if (function_exists('apache_response_headers')) {
			$this->header = apache_response_headers();
		}
		else {
			$this->header = array();
		}
	}
	
	/**
	 * Factory method to initialise your own module class
	 *
	 * @param string $module
	 * @return Service_Module
	 */
	static public function Factory( $module )
	{
		$sql = sprintf("
			SELECT
				m.code,
				m.class
			FROM
				brickwork_service_module m
			WHERE
				m.code = %s
			LIMIT 1",
			DB::quote($module, true)
		);
						
		$res = DB::iQuery($sql);
		
		
		if (self::isError($res)) {
			throw new Service_Exception( $res->errstr );
		}
		else if ($res->numRows > 0) {
			$row = $res->first();
			$class = $row->class;
			$mod = new $class($row->code);
			return $mod;
		}
		return null;
	}

	/**
	 * Returns reference to the module (which could be another than the one loaded)
	 *
	 * @return Service_Module
	 */
	public function getModule()
	{
		return $this;
	}

	/**
	 * Returns the output as a string
	 *
	 * @return string
	 */
	public function fetch()
	{
		return $this->out(true);
	}
	
	/**
	 * Sets the template to the file specified
	 *
	 * @param string $template
	 * @return bool
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
		return true;
	}
	
	/**
	 * Set the caching time
	 *
	 * @param int $cachetime
	 * @return bool
	 */
	public function setCaching($cachetime)
	{
		$this->cachetime = (int) $cachetime;
		return true;
	}
	
	public function setRestrictions($restrictions, $secure = false,
		$auth_method = null)
	{
		$this->_ipRestrictions = $restrictions;
		$this->_restricted = $secure;
		$this->_restrictionMethod = $auth_method;
		return true;
	}
	
	/**
	 * Loads the configuration from the configuration table
	 */
	public function loadConfig()
	{
		return true;
	}
	
	/**
	 * Maps a request to an internal function and writes the output back 
	 *
	 * @return string|Service_Reply
	 */
	public function loadData()
	{
		global $WEBprefs;

		if (!$this->_checkIPRestrictions($this->_ipRestrictions)) {
			header('HTTP/1.1 403 Forbidden');
			logDebug(sprintf("Unauthorized access to %s",get_class($this)));
			return false;			
		}
		
		switch (strtolower($this->type)) {
			case 'soap':
				if (isset($WEBprefs['Service.WSDL.Caching'])) {
					ini_set("soap.wsdl_cache_enabled", $WEBprefs['Service.WSDL.Caching'] ? "0" : "1" );
				}
				else {
					ini_set("soap.wsdl_cache_enabled", TESTMODE ? "0" : "1" );
				}
					
				$url = (Utility::arrayValue($_SERVER,'HTTPS') === 'on' ?
					'https://' : 'http://').
					Utility::arrayValue($_SERVER,'HTTP_HOST').
					'/index.php/service/wsdl/'.$this->code;
							
				$soapServer = new SoapServer($url, array("encoding" => "utf-8"));
				$soapServer->setObject($this);
//				$soapServer->setClass(get_class($this), $this->code, $this->type);
				ob_start();
				$soapServer->handle();
				return ob_get_clean();
			default:
				try {
					$reply = $this->_checkAuthorization();
					if ($reply === true) {
						$class = 'Service_Request_'.ucfirst($this->type);
						
						if (class_exists($class, true)) {
							$req = new $class($this->getModule());
						}
						else {
							$req = new Service_Request_Default($this->getModule());
						}
					}
					else {
						if ($this->debug) {
							logDebug("Auth failed to return TRUE: it returned ".
								var_export($reply, true));
						}
						return $reply;
					}
				}
				catch (Exception $e) {
					$req = new Service_Request_Default($this->getModule());
				}
				
				try {
					$req->parse($this->path);
					$mod = $this->getModule();
					
					if (method_exists($mod, $req->method)) {
						if ($req->hasArguments()) {
							return call_user_func_array(
								array($mod, $req->method), $req->arg);
						}
						return $mod->{$req->method}();
					}
				}
				catch (Exception $e) {
					return new Service_Reply(false, null, $e->getMessage());
				}
				
				return new Service_Reply(false, null, 'Invalid request call');
		}
	}
	
	/**
	 * Maps "Normal" Method to the special "Request methods that All other interfaces use
	 *
	 * @param string $method
	 * @param array $arg
	 * @return mixed
	 */
	public function __call($method, array $arg)
	{
		if ($this->type !== 'soap') {
			throw new Service_Exception("Invalid method call");
		}
		
		if ($this->debug) {
			logDebug(sprintf("Request for SOAP method %s from %s", $method,
				$_SERVER['REMOTE_ADDR']));
		}
			
		$reply = $this->_checkAuthorization();
		
		if ($reply !== true) {
			if ($this->debug) {
				logDebug($reply->messages[0]);
			}
			throw new SOAPFault('client', $reply->messages[0]);
		}
		
		$mod = $this->getModule();
		$method = "{$method}Request";
		if (method_exists($mod, $method)) {
			try {
				if ($arg) {
					$val = call_user_func_array(array($mod, $method), $arg);
					if ($this->debug) {
						logDebug("Method returned ".var_export($val, true));
					}
				}
				else {
					$val = $mod->{$method}();
				}
				
				if (is_array($val) || (is_object($val) &&
						!($val instanceof Service_Reply))) {
					$val = new Service_Reply(true, $val);
					$val->setXMLContentOutput();
					return (string) $val;
				}
				return $val;
			}
			catch (Exception $e) {
				// Fall down to the SOAPFault
			}
		}
		
		throw new SOAPFault('client', 'Invalid request call');
	}
	
	/**
	 * Checks if the current requestor has access based on IP
	 * This function will return FALSE if the user has no access, or TRUE if he has.
	 * The ip list may contain wildcard patterns or even regular expressions.
	 * Default is no access, so if for some reason (no REMOTE_ADDR) the method 
	 * could not verify your location, you will not have access.
	 * 
	 * @param array $ip
	 * @return boolean
	 */
	protected function _checkIPRestrictions($ip)
	{
		if ($this->_isLocalClient()) {
			return true;
		}
			
		if (is_array($ip)) {
			$remote = Utility::arrayValue($_SERVER, 'REMOTE_ADDR');
			foreach($ip as $pattern) {
				$pattern = "/".str_replace(array(".", "*"),
					array("\\.", "[0-9\.]*"), trim($pattern))."/";
				
				if (preg_match($pattern, $remote)) {
					//logDebug("Match $remote with $pattern");
					return true;
				}
			}
		}
		
		if ($this->debug) {
			logDebug(sprintf("No match found for %s on module %s", $ip,
				$this->code));
		}
		return false;
	}
	
	/**
	 * Checks if the user has access to this module
	 * This method uses an Service_Authorization_* object to do its Authorization. You can
	 * specify which by changing the auth_method in the database.
	 * If this method returns a service reply, it means the authorization failed, if it
	 * returns TRUE it means authorization was succesful.
	 *
	 * @return Service_Reply|bool
	 */
	protected function _checkAuthorization()
	{
		if ($this->_restricted) {
			// Nobody knows why we don't require a PHP_AUTH_USER for SOAP
			// For local requests we require a username to be given so we
			// can use the username for like foldernames and such
			if ($this->type !== 'soap' && $this->_isLocalClient()) {
				if (!isset($_SERVER['PHP_AUTH_USER'])) {
					return new Service_Reply(false, null, "This service requires an authentication user");
				}
					
				if ($this->debug) {
					logDebug("_checkAuthorization {$this->type} for call from own server, ignoring");
				}
					
				return true;
			}
			
			$class = 'Service_Authorization_'.ucfirst($this->_restrictionMethod);
			
			try	{
				if (class_exists($class)) {
					$authObject = new $class($this);
					$authObject->setDebugging($this->debug);
					
					$credentials = $authObject->getCredentials();
					
					if ($credentials === false) {
						if ($this->debug) {
							logDebug("No Authorization found, requesting authorization from client");
						}
							
						return $authObject->requestCredentials();
					}
					else if ($this->debug) {
						logDebug("Authorization found, checking validity");
					}
						
					return $authObject->validate($credentials);
				}
				else if ($this->debug) {
					logDebug("Authorization requires class {$class} which was not found");
				}
					
				return new Service_Reply(false, null, 'Server configuration error, invalid authorisation model');
			}
			catch (Exception $e) {
				logDebug(sprintf("Auth method %s failed: %s", $this->_restrictionMethod, $e->getMessage()));
				return new Service_Reply(false, null, 'Server configuration error, invalid authorization model');
			}
		}
		else if ($this->debug) {
			logDebug("Module is not restricted, allowing access");
		}
		return true;
	}
	
	/**
	 * Tells if the remote client is the same as the current host
	 * @return bool
	 */
	protected function _isLocalClient()
	{
		$remote = Utility::arrayValue($_SERVER, 'REMOTE_ADDR');
		$server = Utility::arrayValue($_SERVER, 'SERVER_ADDR');
		return ($remote === $server || $remote === '127.0.0.1');
	}
	
	/**
	 * Load the module data by the given code 
	 *
	 * @param string $code
	 * @return bool
	 */
	protected function _loadModuleByCode($code)
	{
		$sql = sprintf("
			SELECT
				m.id,
				m.code,
				m.class,
				m.name,
				m.cachetime, 
				m.secure,
				m.restrict_ip,
				m.auth_method,
				m.debug,
				pt1.name as content_table,
				pt2.name as config_table
			FROM
				brickwork_service_module m
			LEFT JOIN
				phoundry_table pt1 ON pt1.id = m.ptid_content
			LEFT JOIN
				phoundry_table pt2 ON pt2.id = m.ptid_config
			WHERE
				m.code = %s
			LIMIT 1",
			DB::quote($code, true)
		);
						
		$res = DB::iQuery($sql);
		
		if (self::isError($res)) {
			throw new Service_Exception($res->errstr);
		}
		else if ($res->num_rows === 1)	{
			$row = $res->first();
			
			$class = $row->class;
			
			if ($row->cachetime) {
				$this->setCaching($row->cachetime);
			}

			$this->setRestrictions(explode("\n", $row->restrict_ip),
				$row->secure, $row->auth_method);

			$this->id			= $row->id;
			$this->code			= $row->code;
			$this->_configTable	= $row->config_table;
			$this->_contentTable= $row->content_table;
			$this->moduleName 	= $row->name;
			$this->debug		= $row->debug;
			return true;
		}
		return false;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
