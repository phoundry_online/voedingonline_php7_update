<?php
/**
 * Abstract authorization class
 * This class is an abstract for all authorization methods. It specifies the interface and
 * supplies some helper functions if needed.
 *
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.0
 */
abstract class Service_Authorization
{
	/**
	 * Reference to the module that is being requested
	 *
	 * @var Service_Module
	 */
	protected $_module;
	
	/**
	 * Enables debugging
	 *
	 * @var boolean
	 */
	protected $debug;
	
	/**
	 * Constructor
	 *
	 * @param Service_Module $module
	 */
	public function __construct( Service_Module $module )
	{
		$this->_module = $module;
		$this->debug = false;
	}
	
	/**
	 * Checks the given credentials agains whatever you want
	 * This method should return a Service_Reply with an error message on error and
	 * TRUE when validation was succesful.
	 * 
	 * @param 	mixed $auth
	 * @return	Service_Reply|boolean
	 */
	abstract public function validate( $auth );
	
	/**
	 * Requests the client for credentials
	 * This method should send whatever headers required for requesting authorization.
	 * If should also return a Service_Reply with some message in case authorization fails.
	 * 
	 * @return	Service_Reply
	 */
	abstract public function requestCredentials();
	
	/**
	 * Obtains the credentials for this request
	 * This method should return FALSE if the credentials could not be found and the 
	 * service should do a requestCredentials challenge. Any other value will
	 * mean the credentials were found.
	 * 
	 * @return mixed
	 */
	abstract public function getCredentials();

	/**
	 * Enable or disable debugging
	 * @param bool $enable
	 */
	public function setDebugging($enable)
	{
		$this->debug = (bool) $enable;
	}
	
	/**
	 * Checks if the current requestor has access based on IP
	 * This function will return FALSE if the user has no access, or TRUE if he has.
	 * The ip list may contain wildcard patterns or even regular expressions.
	 * Default is no access, so if for some reason (no REMOTE_ADDR) the method 
	 * could not verify your location, you will not have access.
	 * 
	 * @param 	Array $ip
	 * @return boolean
	 */
	protected function _checkIPRestrictions( $ip )
	{
		$remote = Utility::arrayValue($_SERVER,'REMOTE_ADDR');
		
		if ( is_array($ip) )
		{
			foreach( $ip as $pattern )
			{
				$pattern = "/" . str_replace( Array(".","*"),Array("\\.","[0-9\.]*"),trim($pattern)) . "/";
				
				if ( preg_match($pattern,$remote) )
				{
					//logDebug("Match $remote with $pattern");
					return true;
				}
			}
		}
		return false;
	}
	
}
