<?php
/**
 * Delegates requests to a webmodule
 * This module loads a webModule based on it's ID or name. All subsequent calls
 * to this module will then be redirected to the loaded webmodule.
 * 
 * @author		Peter Eussen
 * @version	1.0
 * @package	Brickwork
 * @subpackage Service
 */
class Service_Module_Webmodule extends Service_Module
{
	/**
	 * The webmodule the user requested
	 *
	 * @var Webmodule
	 */
	protected $_webmodule;

	/**
	 * Returns a reference to the webmodule
	 *
	 * @return Webmodule
	 */
	public function getModule()
	{
		return $this->_webmodule;
	}

	/**
	 * Loads the Webmodule and relays the request
	 *
	 * @return mixed
	 */
	public function loadData()
	{
		$moduleId = array_shift($this->path);

		if ($moduleId === false)
			return new Service_Reply(false, null, 'Missing module parameter');

		try {
			if ($this->_loadWebmodule($moduleId)) {
				return parent::loadData();
			}
		}
		catch (Exception $e) {
			return new Service_Reply(false, null, $e->getmessage());
		}
		return new Service_Reply(false, null, 'No such module');
	}

	/**
	 * Tries to load the webmodule based on the code/id
	 *
	 * @param int|string $moduleId
	 * @return bool
	 */
	public function _loadWebmodule($moduleId)
	{
		$oldErrorHandler = set_error_handler(array($this, 'errorHandler'));
		
		if (is_numeric($moduleId)) {
			$this->_webmodule = Webmodule::getModuleByPageContentId($moduleId);
		} 
		else {
			$this->_webmodule = Webmodule::getModuleByClassName($moduleId);
		}

		if ($this->_webmodule instanceof Service_Module_Interface) {
			if (!empty($this->path[0]) && preg_match('/^[a-z0-9]+$/', $this->path[0]) > 0) {
				$this->template = $this->path[0];
			}
			$this->_webmodule->contentHandlerName = 'ServiceContentHandler';
			$this->_webmodule->code = $this->code.'/'.$moduleId;
			return true;
		}
		
		$this->_webmodule = null;
		set_error_handler($oldErrorHandler);
		return false;
	}
	
	public function errorHandler($errno, $errstr, $errfile, $errline)
	{
		if ((int) PHP_VERSION >= 5 && $errno == E_STRICT) {
			return;
		}
		throw new Service_Exception("$errstr\nFile: $errfile:$errline\n", $errno);
	}
}
