<?php
/**
 * Interface for all Modules that implement the Service interface
 *
 * @author Peter Eussen
 * @version 1.0
 * @package Brickwork
 * @subpackage Service
 */
interface Service_Module_Interface
{
}

