<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * WebService Representation class
 * A simple wrapper to outputting the text in a specific format
 *
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.0
 */
abstract class Service_Representation implements Representation
{
	/**
	 * The response we want
	 *
	 * @var Service_Reply
	 */
	protected $_reply;
	
	/**
	 * The module that handled the request
	 *
	 * @var Service_Module
	 */
	protected $_module;
	
	/**
	 * Retrieves the representation class for this service
	 *
	 * @param string $display
	 * @return Service_Representation
	 */
	static public function factory($display)
	{
		try {
			$class = 'Service_Representation_'.ucfirst($display);
			
			if (class_exists($class) &&
					is_subclass_of($class, 'Service_Representation')) {
				return new $class();
			}
		}
		catch (Exception $e) {
			logDebug( sprintf("Invalid service request %s", $display));		
		}
		return null;
	}
	
	/**
	 * Replaces/sets the module that handled the request
	 *
	 * @param Service_Module_Interface $module
	 * @return bool
	 */
	public function setModule(Service_Module_Interface $module)
	{
		$this->_module = $module;
		return true;
	}
	
	/**
	 * Adds the reply from the handling method/module to the representation
	 *
	 * @param Service_Reply $reply
	 * @return bool
	 */
	public function setReply(Service_Reply $reply)
	{
		$this->_reply = $reply;
		return true;
	}
	
	/**
	 * Writes the result to the screen/string
	 * Optionally writes the output to a string if the $return parameter was set to TRUE
	 * 
	 * @param bool $return
	 * @return string|void
	 */
	public function out($return = false)
	{
		if ($return) {
			return '';
		}
		return;
	}
}
