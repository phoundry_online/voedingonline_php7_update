<?php
/**
 * Service package
 * 
 * @package	 Brickwork
 * @subpackage Service
 * @author		 Peter Eussen
 */

/**
 * Abstract Request class
 * This class handles the parsing of parameters for a request ( if required). you can
 * create your own handler for any request method that is currently not supported.
 * Just make sure it corresponds to the service method chosen in the URL
 *
 * @author		Peter Eussen
 * @version	1.0
 * @package	Brickwork
 * @subpackage Service
 */
abstract class Service_Request
{
	/**
	 * The current module instance
	 *
	 * @var Service_Module
	 */
	public $object;
	
	/**
	 * The class of the module that is loaded
	 *
	 * @var string
	 */
	public $class;
	
	/**
	 * The method that is being called
	 *
	 * @var string
	 */
	public $method;
	
	/**
	 * The arguments that should be passed to the method
	 *
	 * @var array
	 */
	public $arg;
	
	/**
	 * Your own request parsing function
	 *
	 * @param array $args
	 */
	abstract public function parse(array $args);
	
	/**
	 * Initialises the base class
	 *
	 * @param Service_Module $module
	 */
	public function __construct($module)
	{
		$this->class = get_class($module);
		$this->object = $module;
		$this->method = null;
		$this->arg = array();
	}
	
	/**
	 * Returns TRUE if the method requires parameters
	 *
	 * @return boolean
	 */
	public function hasArguments()
	{
		return count($this->arg) !== 0;
	}
	
	/**
	 * Reflects the method to check if it requires arguments
	 * Checks if there are argumenst and populates the arg attribute accordingly
	 * 
	 * @return boolean
	 * @throws	Service_Exception
	 */
	protected function _parseFunctionParam()
	{
		try {
			$m = new ReflectionMethod($this->class, $this->method);
			
			$params = $m->getParameters();
			
			foreach($params as $i => $param) {
				if (!isset($this->arg[$i])) {
					$name = $param->getName();
					$v = $this->_param($name);
					
					if (is_null($v)) {
						$v = $this->_param('arg'.$i);
					}
						
					if (is_null($v) && $param->isDefaultValueAvailable()) {
						$this->arg[$i] = $param->getDefaultValue();
					}
					else {
						$this->arg[$i] = $v;
					}
				}
			}
			return true;
		}
		catch (ReflectionException $e) {
			throw new Service_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	/**
	 * Tries to fetch a POST/Get parameters with a specific name
	 * Depending on wether the request was a POST or GET request, this will
	 * try to load it from the POST array first, and from the GET option alternatively
	 * 
	 * @param 	string $name
	 * @return string
	 */
	protected function _param($name)
	{
		if (Utility::isPosted() && isset($_POST[$name])) {
			return $_POST[$name];
		}
		return Utility::arrayValue($_GET, $name);
	}
}