<?php
/**
 * This class represents a movie on youtube
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power, http://www.webpower.nl
 */
class Service_External_Youtube_Video
{
	protected $_key;
	
	/**
	 * Construct a new video with various options
	 * @param key string The unique video identifier
	 */
	public function __construct($key)
	{
		if($parsed_key = self::urlToKey($key)) {
			$this->_key = $parsed_key;
		}
		else {
			$this->_key = $key;
		}
	}
	
	/**
	 * Constructs the direct url to the video's swf
	 * @param bool $autoplay Start the video directly when its loaded
	 * @param bool $loop Keep repeating the video
	 * @param bool $show_related Shows related videos when the video finishes playing
	 * @param int $start_offset Seconds from the start at which the video starts playing
	 * @return string
	 */
	public function swfUrl($autoplay = true, $loop = false,
		$show_related = false, $start_offset = 0)
	{
		$url = 'http://www.youtube.com/v/'.$this->_key.'?'.
			$this->_getQuerystring($autoplay, $loop, $show_related, $start_offset);
		
		if($start_offset) {
			$url.= '&start='.$start_offset;
		}
		
		return $url;
	}
	
	/**
	 * Constructs the url to the page containing the video
	 * @param bool $autoplay Start the video directly when its loaded
	 * @param bool $loop Keep repeating the video
	 * @param bool $show_related Shows related videos when the video finishes playing
	 * @param int $start_offset Seconds from the start at which the video starts playing
	 * @return string
	 */
	public function videoPageUrl($autoplay = true, $loop = false,
		$show_related = false, $start_offset = 0)
	{
		$url = 'http://www.youtube.com/watch?v='.$this->_key.'&'.
			$this->_getQuerystring($autoplay, $loop, $show_related);
		
		if($start_offset) {
			$minutes = floor($start_offset/60);
			$seconds = $start_offset%60;
			$url.= '#t='.$minutes.'m'.$seconds.'s';
		}
		
		return $url;
	}
	
	/**
	 * Constructs the url to the various thumbnails for the video
	 * @param int $index Integer between 0 and 4 are currently available on yt
	 * @return url
	 */
	public function getImage($index = 0)
	{
		return 'http://img.youtube.com/vi/'.$this->_key.'/'.$index.'.jpg';
	}
	
	public function __toString()
	{
		return (string) $this->_key;
	}
	
	/**
	 * Turns a url to a youtube video into just its key
	 * @param string $url
	 * @return string|bool The key or false on failure
	 */
	public static function urlToKey($url)
	{
		$url = parse_url($url);
		
		if(isset($url['host']) && false !== stripos($url['host'], 'youtube')) {
			
			if(isset($url['query'])) {
				parse_str($url['query'], $query);
				if(isset($query['v'])) {
					$key = $query['v'];
				}
			}
			
			if(empty($key) && !empty($url['path'])) {
				$path = explode('/', trim($url['path'], '/'));
				
				if(isset($path[0], $path[1]) &&
					($path[0] == 'v' || $path[0] == 'vi') && !empty($path[1])) {
					$key = $path[1];
				}
			}
			
			return $key;
		}
		return false;
	}
	
	protected function _getQuerystring($autoplay, $loop, $show_related)
	{
		$qs = '';
		
		$query = array();
		$query['autoplay'] = $autoplay ? 1 : 0;
		$query['loop'] = $loop ? 1 : 0;
		$query['rel'] = $show_related? 1 : 0;
		
		if($query) {
			$qs.= http_build_query($query);
		}
		
		return $qs;
	}
}