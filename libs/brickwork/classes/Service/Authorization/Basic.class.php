<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * Basic authorization
 * Basic authorization uses the HTTP authorization request challenge and 
 * checks the given username and password against the table 
 * brickwork_service_user_module and the IP of the request against the
 * allowed IP list for that user.
 *
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 * @version 1.0
 */
class Service_Authorization_Basic extends Service_Authorization
{
	/**
	 * Checks for validity of the supplied username/password
	 *
	 * @param array $auth
	 * @return Service_Reply|boolean
	 */
	public function validate($auth)
	{
		$user = Utility::arrayValue($auth, 'user');
		$pass = Utility::arrayValue($auth, 'pass');
		
		$sql = sprintf("
			SELECT
				id,
				disabled,
				restrict_ip,
				sum.service_module_id
			FROM brickwork_service_user u
			LEFT OUTER JOIN  brickwork_service_user_module sum
			ON (sum.service_user_id = u.id AND sum.service_module_id = %d)
			WHERE `username` = %s
			AND `password` = %s
			",
			$this->_module->id,
			DB::quote($user, true),
			DB::quote($pass, true)
		);
				
		$res = DB::iQuery($sql);
		
		if (self::isError($res)) {
			if ($this->debug) {
				logDebug($res->errstr);
			}
			return new Service_Reply(false, null, 'Server configuration error');
		}
		
		$row = $res->first();
		
		if ($row === false)
		{
			if ($this->debug) {
				logDebug(sprintf("Unable to find the user with specified credentials (%s)", $user));
			}
				
			return new Service_Reply(false, null, 'Invalid credentials supplied');
		}
		
		if ($row->disabled) {
			if ($this->debug) {
				logDebug("User found, but disabled");
			}
				
			return new Service_Reply(false, null, 'Your account has been suspended');
		}
		
		$ips = explode("\n", $row->restrict_ip);
		
		if ($ips && !$this->_checkIPRestrictions($ips)) {
			if ($this->debug) {
				logDebug("User is not allowed from his ip");
			}
			return new Service_Reply(false, null, 'Request denied, requesting client ip is unauthorized');
		}
		
		if (!is_null($row->service_module_id)) {
			if ($this->debug) {
				logDebug("User has access to the module");
			}
			return true;
		}
		
		if ($this->debug) {
			logDebug("User has no access to the module");
		}
		return new Service_Reply(false, null, 'You do not have access to this service');
	}
	
	/**
	 * Sends the 401 Authorization request headers
	 *
	 * @return Service_Reply
	 */
	public function requestCredentials()
	{
		header('WWW-authenticate: basic realm="'.$this->_module->code.' webservice"');
		header("HTTP/1.0 401 Unauthorized");
		
		if ($this->_module->type === 'soap') {
			throw new SOAPFault('client', 'Please supply credentials');
		}

		return new Service_Reply(false, null, "Please supply your credentials");
	}
	
	/**
	 * Retrieves the given credentials
	 * Checks if PHP_AUTH_USER and PHP_AUTH_PW are set, if so it will return an array 
	 * with the username and password, otherwise it will return false
	 * 
	 * @return array|boolean
	 */
	public function getCredentials()
	{
		$user = Utility::arrayValue($_SERVER, 'PHP_AUTH_USER');
		$pass = Utility::arrayValue($_SERVER, 'PHP_AUTH_PW');
		
		if (is_null($user) || is_null($pass)) {
			return false;
		}
			
		return array("user" => $user, "pass" => $pass);		
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}
