<?php
/**
 * Handles XML requests
 *
 * @author Peter Eussen
 * @package	Brickwork
 * @subpackage Service
 * @version	1.0
 */
class Service_Request_Xml extends Service_Request
{
	/**
	 * Implement the abstract parse
	 * (non-PHPdoc)
	 * @see classes/Service/Service_Request#parse()
	 */
	public function parse(array $args)
	{
		if ($args) {
			$req = array_shift($args);
		}
		else if (Utility::isPosted()) {
			$req = 'post';
		}
		else {
			$req = 'get';
		}
			
		$this->method = "{$req}Request";
		$this->arg = $args;

		if (!$this->_parseXMLRequest()) {
			$this->_parseFunctionParam();
		}
		return true;
	}
	
	/**
	 * Parses the XML post data into parameters
	 * This method will parse an XML request when the opening tag is 'request'.
	 * All underlying elements will be parsed as parameters that must be passed
	 * on to the method. It will use the parameter names as elements that it
	 * needs to find.
	 * 
	 * If you need to pass binary or XML unsafe data you can use encoding of the
	 * type base64 or url
	 * 
	 * @return bool|void
	 */
	protected function _parseXMLRequest()
	{
		$xml = $this->_getRawPostData();
		
		if (!is_object($xml)) {
			return false;
		}
	
		if ($xml->getName() !== 'request') {
			return false;
		}
			
		if (isset($xml['method'])) {
			$this->method = $xml['method'].'Request';
		}
		
		try {
			$m = new ReflectionMethod($this->class, $this->method);
			
			$params = $m->getParameters();
			
			foreach($params as $i => $param) {
				$name = $param->getName();
				
				if (isset($xml->{$name})) {
					$element = $xml->{$name};
					if (isset($element['encoding'])) {
						switch ($element['encoding']) {
							case 'base64':
								$val = base64_decode($element);
								break;
							case 'url':
								$val = rawurldecode($element);
								break;
							default:
								$val = $element; 
						}
					}
					else {
						$val = $element;
					}
					
					$this->arg[$i] = $val;					
				}
				else if ($param->isDefaultValueAvailable()) {
					$this->arg[$i] = $param->getDefaultValue();
				}
			}
			return true;
		}
		catch (ReflectionException $e) {
			throw new Service_Exception($e->getMessage(), $e->getCode());
		}
		
		/* Unreachable Code
		foreach( $xml->children() as $element )
		{
			if ( isset($element['encoding']) )
			{
				switch ($element['encoding']) {
					case 'base64': 	$val = base64_decode( $element ); break;
					case 'url':		$val = rawurldecode($element); break;
					default:
						$val = $element; 
				}
				
			}
			else
				$val = $element;
				
			
		}
		*/
	}
	
	/**
	 * Tries to load the POST data as XML element
	 * This method will try to load the POST data as XML element. If that works,
	 * it will return a SimpleXMLElement, otherwise it will return a boolean
	 *
	 * @return SimpleXMLElement|boolean
	 */
	protected function _getRawPostData()
	{
		global $HTTP_RAW_POST_DATA;
		
		if (Utility::isPosted()) {
			if (empty($HTTP_RAW_POST_DATA)) {
				// ini setting always_populate_raw_post_data is turned off
				$data = file_get_contents("php://input");
			}
			else {
				$data = $HTTP_RAW_POST_DATA;
			}
				
			if (empty($data)) {
				return false;
			}
				
			try {
				$xml = new SimpleXMLElement($data);
				return $xml;
				
			}
			catch (Exception $e) {
			}
		}
		return false;
	}
}