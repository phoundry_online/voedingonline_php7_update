<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * Default handler for basic GET/POST requests to service methods
 * 
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.0
 */
class Service_Request_Default extends Service_Request
{
	/**
	 * Parses the request
	 *
	 * @param 	array	 $args	Arguments given in the URL
	 * @return boolean
	 */
	public function parse(array $args)
	{
		if (count($args)) {
			$req = array_shift($args);
		}
		else if (Utility::isPosted()) {
			$req = 'post';
		}
		else {
			$req = 'get';
		}
			
		$this->method = "{$req}Request";
		$this->arg = $args;

		$this->_parseFunctionParam();
		return true;
	}
}
