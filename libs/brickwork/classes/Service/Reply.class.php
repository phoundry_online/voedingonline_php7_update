<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * The default Response wrapper for all Service request responses
 *
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.0
 */
class Service_Reply
{
	/**
	 * Indicator if output should be XML
	 *
	 * @var bool
	 */
	protected $_useXml;
	
	/**
	 * Base DOMDocument if using XML
	 *
	 * @var DOMDocument
	 */
	protected $_xml;
		
	/**
	 * Whether the reply was normal or a failur
	 *
	 * @var bool
	 */
	public $status;
	
	/**
	 * Possible messages in case of errors
	 *
	 * @var array
	 */
	public $messages;
	
	/**
	 * Additional content
	 *
	 * @var string
	 */
	public $content;
	
	/**
	 * startNode when using XML
	 *
	 * @var string
	 */
	public $startNode;
	
	/**
	 * Initialises an Reply object
	 *
	 * @param bool $status
	 * @param mixed $content
	 * @param array $messages
	 */
	public function __construct( $status = true, $content = null, $messages = array()  )
	{
		$this->status = $status;
		$this->messages = (array) $messages;
		$this->content = $content;
		$this->_useXml = false;
		$this->_xml = null;
		$this->startNode = 'content';
	}

	/**
	 * Converts the reply to a readable string
	 *
	 * @return string
	 */
	public function __toString()
	{
		if ($this->_useXml) {
        	$this->_xml = new DOMDocument("1.0", "utf-8");
        	$elm = $this->_xml->createElement($this->startNode);
        	$this->_contentAsXML($elm, $this->content);
        	$this->_xml->appendChild($elm);
			//if ( TESTMODE )
			//	$this->_xml->formatOutput = true;
			return $this->_xml->saveXML();
		}
		
		if (is_object($this->content)) {
			if (method_exists($this->content, "__toString")) {
				return (string) $this->content;
			}
			return 'Object';
		}
		return (string) $this->content;
	}
	
	/**
	 * Toggles XML generation mode 
	 *
	 * @param string $startNode
	 */
	public function	setXMLContentOutput($startNode = 'content')
	{
		$this->_useXml = true;
		$this->startNode = $startNode;
		return true;
	}

	/**
	 * Converts a complex datatype as Object/array to an XML tree
	 * All xml will be appended to the DOMelement $content node
	 * 
	 * @param DOMElement $content
	 * @param mixed $data
	 */
	protected function _contentAsXML(DOMElement $content, $data)
	{
		if (is_array($data)) {
			$attr = $this->_xml->createAttribute('type');
			$attr->appendChild( $this->_xml->createTextNode("Array"));
			$content->appendChild($attr);
			
			foreach($data as $key => $sub)
			{
				if (is_numeric($key)) {
					$elm = $this->_xml->createElement('entry');
					$attr = $this->_xml->createAttribute('no');
					$attr->appendChild($this->_xml->createTextNode($key));
					$elm->appendChild($attr);
				}
				else {
					$elm = $this->_xml->createElement($key);
				}
					
				$this->_contentAsXML($elm, $sub);
				$content->appendChild($elm);
			}
			
		}
		else if (is_object($data)) {
			//$elm = $this->_xml->createElement( get_class($data) );
			$elm = &$content;
			$attr = $this->_xml->createAttribute('type');
			$attr->appendChild( $this->_xml->createTextNode(get_class($data)));
			$elm->appendChild($attr);
			
			$vars = get_object_vars($data);
			
			foreach($vars as $label => $value) {
				if ($label[0] !== '_' && substr($label,-1) !== '_') {
					$node = $this->_xml->createElement($label);
					$this->_contentAsXML($node,$value);
					$elm->appendChild($node);
				}
			}
			//$content->appendChild($elm);
		}
		else if (!is_null($data)) {
			if (is_bool($data)) {
				$txt = $this->_xml->createTextNode($data ? 'true' : 'false');
				$attr = $this->_xml->createAttribute('type');
				$attr->appendChild( $this->_xml->createTextNode('boolean'));
				$content->appendChild($attr);
			}
			else {
				$txt = $this->_xml->createTextNode(utf8_encode($data));
				//$attr->appendChild( $this->_xml->createTextNode( is_numeric($data) ? 'number' : 'string'));
			}
			$content->appendChild($txt);
		}
	}
}
