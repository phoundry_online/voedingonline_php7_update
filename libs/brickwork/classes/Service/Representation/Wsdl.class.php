<?php
/**
 * Service package
 * 
 * @package	 Brickwork
 * @subpackage Service
 * @author		 Peter Eussen
 */

/**
 * WSDL specification representation 
 * This class is a subpart of the SOAP interface. It handles basic representation
 * of methods in service classes. It can as of now not handle complex requests
 * so try to keep your requests as simple as possible
 * 
 * @package	 Brickwork
 * @subpackage Service
 * @author		 Peter Eussen
 * @version	 1.0
 */
class Service_Representation_Wsdl extends Service_Representation
{
	/**
	 * The cache file
	 *
	 * @var Cache
	 */
	protected $_cache;
	
	/**
	 * Creates the WSDL file
	 *
	 * @param 	boolean $return
	 * @return	string
	 */
	public function out($return = false)
	{
		if (is_null($this->_module)) {
			throw new Service_Exception("WSDL without a module called");
		}

			
		if ($return) {
			return $this->_introspectClass($this->_module);
		}
		
		if (!headers_sent()) {
			header('Content-Type: text/xml; charset=utf-8');
		}
		echo $this->_introspectClass($this->_module);
	}
	
	/**
	 * Analyses the module, it's methods and documentation and generates the WSDL
	 *
	 * @param 	string $module
	 * @return string
	 */
	protected function _introspectClass( $module )
	{
		global $WEBprefs;
		
		$cl= get_class($module);

		// Caching enabled?
		if ( isset($this->_module->cachetime) && $this->_module->cachetime > 0 )
		{
			if ( isset($WEBprefs['Service.WSDL.Caching.Target']) )
				$this->_cache = Cache::factory($WEBprefs['Service.WSDL.Caching.Target'] . '?timeout=' . (int)$this->_module->cachetime);
			else
				$this->_cache = Cache::factory('file:///wsdl?timeout=' . (int)$this->_module->cachetime);

			$wsdl = $this->_isCached($cl);
			
			if ( $wsdl !== false )
			{
				return $wsdl;
			}
		}
			
		$c = new ReflectionClass( $cl );
		$m = $c->getMethods(ReflectionMethod::IS_PUBLIC);
				
		$url 	 = (Utility::arrayValue($_SERVER,'HTTPS') == 'on' ? 'https://' : 'http://' ) . 
					Utility::arrayValue($_SERVER,'HTTP_HOST') . '/index.php/service/soap/' . $module->code;

		if ( isset($module->code) )
			$service = str_replace('/','_',$module->code);
		else
			$service = str_replace('/','_',get_class($module));
		
		$operation = Array();
		
		foreach( $m as $method )
		{
			if ( $method->isUserDefined() && $method->getFileName() == $c->getFileName() && substr($method->getName(),-7) == 'Request' )
			{
				$doc   = $method->getDocComment();			// Doc content
				$dp    = $this->_getDocParams($doc);		// DocParameters
				$op    = substr($method->getName(),0,-7);	// Operation
				
				if ( is_null($dp['return']) )
				{
					if ( TESTMODE )
					{
						$this->_flush($cl);
						if ( isset($this->_cache) && is_object($this->_cache))
							unset($this->_cache);
						trigger_error( sprintf("Invalid return type for %s:%s(), check DOCblock for valid information", $cl, $method->getName()));
					}
					$dp['return']['name'] = 'undefinedReturnName';
					$dp['return']['type'] = 'string';
				}
				
				$operation[ $op ]['request'] 	= $dp['param'];
				$operation[ $op ]['response']	= $dp['return'];
				
				if ( !empty($dp['doc']) )
					$operation[ $op ]['doc']	= $dp['doc'];
				else if ( isset($dp['shortdoc']) )
					$operation[ $op ]['doc']	= $dp['shortdoc'];
					
				// Checken of alle params beschreven zijn
				if ( $method->getNumberOfParameters() > count($dp['param']) )
				{
					$p = $method->getParameters();	// Parameters
					
					for ( $i = 0, $m = count($p); $i < $m; $i++)
					{
						if ( !isset( $operation[ $op ]['request'][$i]) )
						{
							$name = $p[$i]->getName();
							$operation[ $op ]['request'][$i] = array('type' => 'string', 'var' => $name );
						}
						
					}
				}
				
			}
		}
		
		ob_start();
		include('wsdl.tpl.php');
		$wsdl = '<?xml version="1.0" encoding="UTF-8"?>' . "\n"  .'<?xml-stylesheet type="text/xsl" href="/brickwork/xsl/wsdl-viewer.xsl"?>
		' . "\n" . ob_get_clean();
		
		$this->_cache( $cl, $wsdl );
		return $wsdl;
	}
	
	/**
	 * Parses docblocks for @param and @return options
	 *
	 * @param 	string $doc
	 * @return Array
	 */
	protected function _getDocParams( $doc )
	{
		$all = Array('param' => Array(), 'return' => null);
		
		if ( empty($doc) )
			return $all;

		$ct = preg_match_all('/\*\s*\@param\s+([a-z0-9_]+)\s+\$([a-z0-9_]+)/i',$doc,$matches);
		
		if ( $ct > 0)
		{
			for ( $i = 0; $i < $ct; $i++)
			{
				$param['type'] = $this->_wsdlSafeType( Utility::arrayValue($matches[1],$i) );
				$param['var']  = Utility::arrayValue($matches[2],$i);
				/*
				switch ( $param['type'] ) {
					case 'bool':	$param['type'] = 'boolean'; break;
					case 'int':		$param['type'] = 'integer'; break;
					case 'mixed':	$param['type'] = 'string'; break;
					default:
				}
				*/
				array_push($all['param'],$param);
			}
		}
		
		if ( preg_match("/\*\s*\@return\s+([a-z0-9_]+)\s+([a-z0-9_]+)/",$doc,$matches) )
		{
			$all['return']['type'] = $this->_wsdlSafeType($matches[1]);
			$all['return']['name'] = $matches[2];
		}
		else
		{
			$all['return'] = Array('type' => 'string', 'name' => 'data');
		}
		
		$lines = explode("\n", $doc );
		
		array_shift($lines);
		$short = trim(array_shift($lines),"* \n\r\t");
		
		if ( strlen($short) )
		{
			if ($short[0] != '@')
				$all['shortdoc'] = $short;
		}
		
		$long = '';
		foreach( $lines as $line )
		{
			$line = trim($line,"* \n\r\t");
			
			if ( strlen($line) )
			{
				if ($line[0] == '@')
					break;
				else
					$long .= ' ' . $line;
			}
		}
		
		$all['doc'] = trim($long);
		return $all;
	}
	
	protected function _cache( $class, $wsdl )
	{
		if ( !is_object($this->_cache))
			return true;
			
		$this->_cache[ $class . '.ts' ] = time();
		$this->_cache[ $class ] = $wsdl;
		return true;
	}
	
	protected function _flush( $class )
	{
		if ( !is_object($this->_cache))
			return true;
			
		unset( $this->_cache[$class . '.ts'] );
		unset( $this->_cache[$class]);
		$this->_cache->flush();
		return true;
	}
	
	protected function _isCached( $class )
	{
		if ( isset( $this->_cache[$class . '.ts']) )
		{
			$rc = new ReflectionClass($class);
			
			if ( filemtime( $rc->getFileName() ) < $this->_cache[$class . '.ts'] )
			{
				return $this->_cache[$class];
			}
			else
			{
				unset( $this->_cache[$class . '.ts'] );
				unset( $this->_cache[$class]);
			}
		}
		return false;
	}
	
	protected function _wsdlSafeType( $type )
	{
		if ( is_null($type))
			return 'string';
			
		switch ( strtolower($type) ) {
			case 'bool':	return 'boolean';
			case 'int':		return 'integer';
			case 'mixed':	return 'string';
			case 'array':	return 'string'; 
			default:
				if ($type == '')
					return 'string';
				return $type;
		}
	}
}