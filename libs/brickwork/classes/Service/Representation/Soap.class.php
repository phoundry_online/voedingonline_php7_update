<?php

class Service_Representation_Soap extends Service_Representation
{
	public function out($return = false)
	{
		if ($return) {
			return $this->_reply->content;
		}
			
		header('Content-Type: text/xml; charset=utf-8');
		echo $this->_reply->content;
	}
}