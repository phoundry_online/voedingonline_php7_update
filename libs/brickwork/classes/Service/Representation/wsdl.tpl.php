<wsdl:definitions
    name="<?= ucfirst($service) ?>Info"
    xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
    xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
    xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
    xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
    xmlns:tns="http://namespace.webpower.nl/<?= ucfirst($service) ?>.wsdl"
    xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/"
    targetNamespace="http://namespace.webpower.nl/<?= ucfirst($service) ?>.wsdl">
    <wsdl:types>
        <xs:schema targetNamespace="http://namespace.webpower.nl/<?= ucfirst($service) ?>.xsd" elementFormDefault="qualified"/>
    </wsdl:types>
    <?
    foreach( $operation as $method => $inout )
    {
    	//if ( count($inout['request']) > 0)
    	//{ 
    	?>
    <wsdl:message name="<?= ucfirst($method) ?>Request">
    	<?
    		foreach( $inout['request'] as $var) { ?>
        <wsdl:part name="<?= $var['var'] ?>" type="xs:<?= $var['type'] ?>"/>
        <? 
    	} ?>
    </wsdl:message>
    <? //} ?>
    <wsdl:message name="<?= ucfirst($method) ?>Response">
        <wsdl:part name="<?= $inout['response']['name'] ?>" type="xs:<?= $inout['response']['type'] ?>"/>
    </wsdl:message>
    		<?
    }
    ?>
    <wsdl:portType name="<?= ucfirst($service) ?>PortType">
    <? foreach( $operation as $method => $inout ) { ?>
        <wsdl:operation name="<?= ucfirst($method) ?>">
            <? if ( isset($inout['doc']) ) { ?>
            <wsdl:documentation><?= htmlentities($inout['doc'],ENT_QUOTES,'utf-8') ?></wsdl:documentation>
            <? } 
        	
        	if ( count($inout['request'])) {
        	?>
            <wsdl:input message="tns:<?= ucfirst($method) ?>Request"/>
            <?
        	}
        	?>
            <wsdl:output message="tns:<?= ucfirst($method) ?>Response"/>
        </wsdl:operation>
    <? } ?>
    </wsdl:portType>
    <wsdl:binding name="<?= ucfirst($service) ?>Binding" type="tns:<?= ucfirst($service) ?>PortType">
        <soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
        <? foreach( $operation as $method => $inout) { ?>
        <wsdl:operation name="<?= ucfirst($method) ?>">
            <soap:operation soapAction="urn:xmethods-delayed-quotes#Authenticate" style="rpc"/>
            <? if ( isset($inout['doc']) ) { ?>
            <wsdl:documentation><?= htmlentities($inout['doc'],ENT_QUOTES,'utf-8') ?></wsdl:documentation>
            <? } ?>
            <? if ( count($inout['request'])) { ?>
            <wsdl:input>
                <soap:body
                    use="encoded"
                    encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/>
            </wsdl:input>
            <? } ?>
            <wsdl:output>
                <soap:body
                    use="encoded"
                    encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/>
            </wsdl:output>
        </wsdl:operation>
        <? } ?>
    </wsdl:binding>
    <wsdl:service name="<?= ucfirst($service) ?>Service">
        <wsdl:port name="<?= ucfirst($service) ?>Port" binding="tns:<?= ucfirst($service) ?>Binding">
            <soap:address location="<?= $url ?>"/>
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>