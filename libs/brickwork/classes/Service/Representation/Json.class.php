<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * Outputs the response object as JSON object
 * 
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.0
 */
class Service_Representation_Json extends Service_Representation
{
	/**
	 * Generates the response JSON on the given reply
	 *
	 * @param bool $return
	 * @return string
	 */
	public function out($return = false)
	{
		$json = json_encode($this->_reply);
		if (isset($_GET['_callback'])) {
			$json = $_GET['_callback']."({$json});";
		}
		
		if ($return) {
			return $json;
		}
		
		if (!headers_sent()) {
			header('Content-Type: application/json');
		}
		
		echo $json;
	}
}
