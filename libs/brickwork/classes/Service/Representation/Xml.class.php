<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * XML based output
 * This class will convert the complete reply class to an XML document
 *
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.0
 */
class Service_Representation_XML extends Service_Representation
{
	/**
	 * The reply
	 *
	 * @var DOMDocument
	 */
	protected $_xml;
	
	/**
	 * Generates the response XML on the given reply
	 *
	 * @param bool $return
	 * @return string
	 */
	public function out($return = false)
	{
		$this->_xml = new DOMDocument("1.0", "utf-8");
		$reply = $this->_xml->createElement('response');
		$status = $this->_xml->createAttribute('status');
		$status->appendChild( $this->_xml->createTextNode(!is_null($this->_reply) && $this->_reply->status ? 'ok' : 'error'));
		$reply->appendChild($status);
		
		$content = $this->_xml->createElement('content');
		$messages = $this->_xml->createElement('messages');
				
		if (!is_null($this->_reply)) {
			if (count($this->_reply->messages)) {
				foreach($this->_reply->messages as $txt) {
					$msg = $this->_xml->createElement('message');
					$txt = $this->_xml->createTextNode($txt);
					$msg->appendChild($txt);
					$messages->appendChild($msg);
				}
			}
			
			$this->_reply->setXMLContentOutput();
			$this->_contentAsXml($content, $this->_reply->content);
		}
		
		$reply->appendChild($messages);
		$reply->appendChild($content);
		
		if (TESTMODE && isset($this->_reply->processTime)) {
			$node = $this->_xml->createElement('processTime');
			$node->appendChild( $this->_xml->createTextNode($this->_reply->processTime));
			$reply->appendChild($node);	
		}
		
		$this->_xml->appendChild($reply);
		
		if (TESTMODE) {
			$this->_xml->formatOutput = true;
		}
			
		if ($return) {
			return $this->_xml->saveXML();
		}
			
		if (!headers_sent()) {
			header('Content-Type: text/xml; charset=utf-8');
		}
		echo $this->_xml->saveXML();
	}
	
	/**
	 * Converts a complex datatype as Object/Array to an XML tree
	 * All xml will be appended to the DOMelement $content node
	 * 
	 * @param 	DOMElement 	$content
	 * @param 	mixed 		$data
	 * @todo	Merge with Service_Reply::_contentAsXML
	 */
	protected function _contentAsXML(DOMElement $content, $data)
	{
		if (is_array($data)) {
			$attr = $this->_xml->createAttribute('type');
			$attr->appendChild( $this->_xml->createTextNode("Array"));
			$content->appendChild($attr);
			
			foreach($data as $key => $sub) {
				if (is_numeric($key)) {
					$elm = $this->_xml->createElement('entry');
					$attr= $this->_xml->createAttribute('no');
					$attr->appendChild( $this->_xml->createTextNode($key) );
					$elm->appendChild($attr);
				}
				else {
					$elm = $this->_xml->createElement($key);
				}
					
				$this->_contentAsXML($elm, $sub);
				$content->appendChild($elm);
			}
			
		}
		else if (is_object($data)) {
			//$elm = $this->_xml->createElement( get_class($data) );
			$elm = $content;
			$attr = $this->_xml->createAttribute('type');
			$attr->appendChild($this->_xml->createTextNode(get_class($data)));
			$elm->appendChild($attr);
			
			$vars = get_object_vars($data);
			
			foreach ($vars as $label => $value) {
				if ($label[0] != '_' && substr($label,-1) != '_') {
					$node = $this->_xml->createElement($label);
					$this->_contentAsXML($node,$value);
					$elm->appendChild($node);
				}
			}
			$content->appendChild($elm);
		}
		else if (!is_null($data)) {
			if (is_bool($data)) {
				$txt = $this->_xml->createTextNode( $data ? 'true' : 'false');
				$attr = $this->_xml->createAttribute('type');
				$attr->appendChild( $this->_xml->createTextNode('boolean'));
				$content->appendChild($attr);
			}
			else {
				$txt = $this->_xml->createTextNode(utf8_encode($data));
				//$attr->appendChild( $this->_xml->createTextNode( is_numeric($data) ? 'number' : 'string'));
			}
			$content->appendChild($txt);
		}
	}
}
