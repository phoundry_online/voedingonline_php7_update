<?php
/**
 * Service package
 * 
 * @package Brickwork
 * @subpackage Service
 * @author Peter Eussen
 */

/**
 * Most basic representation: plain text
 * returns two (or more lines). The first line is the status (OK or FAIL).
 * The second line contains the message if a FAIL or content if success
 *
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.0
 */
class Service_Representation_Plain extends Service_Representation
{
	/**
	 * Generates the output
	 *
	 * @param bool $return
	 * @return string
	 */
	public function out($return = false)
	{
		if (!isset($this->_reply) || is_null($this->_reply)) {
			return "State: FAIL\n";
		}
			
		$data = "State: " . ($this->_reply->status ? 'OK' : 'FAIL') . "\n";
		
		if (TESTMODE && isset($this->_reply->processTime)) {
			$data .= 'Process Time: ' . $this->_reply->processTime . "\n";
		}
			
		foreach ($this->_reply->messages as $msg) {
			$data .= 'Message: ' . addslashes($msg) . "\n";
		}
			
		$data .= "Content:\n" . (string) $this->_reply->content;
		
		if ($return) {
			return $data;
		}
		
		if (!headers_sent()) {
			header('Content-Type: text/plain');
		}
		echo $data;
	}	
}