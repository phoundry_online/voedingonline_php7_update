<?php
/**
 * Creates a serialized response
 * For generic purpose the response is transformed into an Array
 * with three elements: status, messages, content which correspond
 *
 * @author Peter Eussen
 * @package Brickwork
 * @subpackage Service
 * @version 1.0
 */
class Service_Representation_Serial extends Service_Representation
{
	/**
	 * Generates the output
	 *
	 * @param bool $return
	 * @return string
	 */
	function out($return = false)
	{
			
		if (!isset($this->_reply) || is_null($this->_reply)) {
			return serialize(null);
		}
			
		$response['status'] = $this->_reply->status;
		$response['messages'] = $this->_reply->messages;
		$response['content'] = $this->_reply->content;
		
		if (TESTMODE && isset($this->_reply->processTime)) {
			$response['processTime'] = $this->_reply->processTime;
		}
			
		if ($return) {
			return serialize($response);
		}
		
		if (!headers_sent()) {
			header('Content-Type: text/plain');
		}
		echo serialize($response);
	}		
}