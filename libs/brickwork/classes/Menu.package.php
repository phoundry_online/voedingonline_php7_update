<?php
/**
* Menu
*
* Menu is en heeft menuitems, het is een iterator en de child items kunnen als
* array doorlopen worden
*
* @package Menu
* @version 1.0.0
* @author Mick van der Most van Spijk
* @last_modfied 2006/07/17
*/
abstract class RecursiveMenu implements Iterator {
	/**
	*
	*
	*/
	public $title;

	/**
	*
	*
	*/
	public $description = '';

	/**
	*
	*
	*/
	public $link = '';

	/**
	*
	*
	*/
	public $target = '_self';

	/**
	*
	*
	*/
	public $type = '';


	/*
	* Hoe diep zitten we?
	*
	*/
	public $selected = FALSE;

	/**
	* Welk type kan een link eigenlijk hebben?
	*
	*/

	public static $_iteration = array();
	public $iteration;

	public static $_depth = 0;
	/**
	* hierarchische container voor menu items
	* @var array
	*/
	public $items = array();

	/**
	* Contructor
	*
	* Automatically generate a menu from a list of items, those items need to be
	* objects with a id defined
	*
	* @var Array
	*/


	/**
	*
	*
	*/
	public function current(): mixed {
		return current($this->items);
	}

	/**
	*
	*
	*/
	public function next(): void{
		next($this->items);
	}

	/**
	*
	*
	*/
	public function key(): mixed {
		return key($this->items);
	}

	/**
	*
	*
	*/
	public function valid(): bool {
		return $this->current() !== false;
	}

	/**
	*
	*
	*/
	public function rewind(): void {
		reset ($this->items);
	}

	public static function getSelectedAtDepth($menu, $nivo=1){
		if ($menu->iteration == $nivo) {
			return $menu->selected(FALSE);
		} else {
			foreach ($menu as $item) {
				return self::getSelectedAtDepth($item, $nivo);
			}
		}
	}

	public static function getSeletedIterationAtDepth($depth=1) {
		if ($this->iteration == $depth) {
			return $this->selected(FALSE)->iteration;
		} else {
			foreach ($this->items as $item) {
				return self::getSeletedIterationAtDepth($item, $depth);
			}
		}
	}

	/**
	* Select a menu item an return it
	*
	* @param mixed id
	* @return mixed selected item
	*/
	public function select($id) {
		foreach ($this->items as $k => $v) {
			if ($v->id == $id) {
				$this->items[$k]->selected = TRUE;
				return $this->items[$k];
			}
		}

		return false;
	}

	/**
	* Get selected menu item
	*
	* Searches the items in this menu for a selected item, if not found the first
	* element is returned. If no elements are found a notice error is raised and
	* false is returned
	*
	* @return mixed selected item
	* @param Boolean return first item of list when specified item was not found
	*/
	public function selected($return_first=TRUE) {
		foreach ($this->items as $k => $item) {
			if ($item->selected) {
				return $item;
			}
		}

		// no item selected, return first
		if ($return_first && isset($this->items[0])) {
			return $this->select($this->items[0]->id);
		}

		// no items selected
		trigger_error('No menu-items found to select', E_USER_NOTICE);
		return false;
	}

	public function __toString() {
		return $this->title;
	}


}
?>
