<?php
/**
* Internationalisatie
*
* @author Mick van der Most van Spijk <mick@webpower.nl>
* @version 1.0.0
* @last_modified 11/16/2006
*/
class i18n {
	/**
	* Laad alle landen
	*
	* Maak connectie met db en haal alle landen op uit de landen tabel.
	* Deze functie wordt door de Country class gebruikt om eenmalig per request alle landdefinities in te laden
	*
	* @see Country
	* @return array
	*/
	public static function getCountries() {
		$sql = '
			SELECT
				code,
				name
			FROM
				brickwork_country
			ORDER BY
				code,
				name
		';

		$res = DB::iQuery($sql);
		if (self::isError($res)){
			throw new Exception($res->msg);
		}

		$countries = array();
		while ($res->current()) {
			$row = $res->current();
			$countries[$row->code] = $row->name;
			$res->next();
		}

		return $countries;
	}

	/**
	* Laad alle landen
	*
	* Maak connectie met db en haal alle landen op uit de landen tabel.
	* Deze functie wordt door de Country class gebruikt om eenmalig per request alle landdefinities in te laden
	*
	* @see Country
	* @return array
	*/
	public static function getCountries2() {
		$sql = '
			SELECT
				iso,
				name_nl
			FROM
				brickwork_country_2
			ORDER BY
				iso,
				name_nl
		';

		$res = DB::iQuery($sql);

		if (self::isError($res)){
			throw new Exception($res->msg);
		}

		$countries = array();
		while ($res->current()) {
			$row = $res->current();
			$countries[$row->iso] = $row->name_nl;
			$res->next();
		}
		
		return $countries;
	}

    /**
     * Test if an object is of type error
     *
     * @return Boolean
     */
    public static function isError(&$e) {
        $v = PHP_VERSION;

        switch (TRUE) {
            // old php
            case ($v < 4.2):
                if ((get_class($e) == 'error')
                    ||	(is_subclass_of($e, 'error'))) {
                    return TRUE;
                }
                break;

            // php 4.2 < 5
            case ($v >= 4.2 && $v < 5):
                return (is_a($e, 'Error'));
                break;

            // php 5
            case ($v >= 5):
                return (is_a($e,  '\WebPower\Phoundry\ErrorHandling\Error') || is_a($e, 'Error'));
                break;
        }

        // not an error
        return FALSE;
    }
}

/**
* IBAN codes
*
* The electronic format of the IBAN is composed of 24 contiguous alphanumeric
* characters.
* The paper representation of the IBAN, which is composed of 24 alphanumeric
* characters, is structured in groups of 4 characters separated by blanks.
*
* @author Mick van der Most van Spijk <mick@webpower.nl>
* @last_modified 12/12/2006
* @see: http://www.ecbs.org/Download/TR201v3.21.pdf
*/
class i18n_IBAN {
	private static $_mapping = array(
		'AD' => '/^AD[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}$/',
		'AT' => '/^AT[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'BE' => '/^BE[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'BA' => '/^BA[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'BG' => '/^BG[0-9]{2}\s?[A-Z]{4}\s?[0-9]{4}\s?[0-9]{2}[A-Z0-9]{2}\s?[A-Z0-9]{4}\s?[A-Z0-9]{2}$/',
		'HR' => '/^HR[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{1}$/',
		'CY' => '/^CY[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}$/',
		'CZ' => '/^CZ[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'DK' => '/^DK[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}$/',
		'EE' => '/^EE[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'FI' => '/^FI[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}$/',
		'FR' => '/^FR[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}[A-Z0-9]{2}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{1}[0-9]{2}$/',
		'DE' => '/^DE[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}$/',
		'GI' => '/^GI[0-9]{2}\s?[A-Z]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{3}$/',
		'GR' => '/^GR[0-9]{2}\s?[0-9]{4}\s?[0-9]{3}[A-Z0-9]{1}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{3}$/',
		'HU' => '/^HU[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'IS' => '/^IS[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}$/',
		'IE' => '/^IE[0-9]{2}\s?[A-Z]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}$/',
		'IT' => '/^IT[0-9]{2}\s?[A-Z]{1}[0-9]{3}\s?[0-9]{4}\s?[0-9]{3}[A-Z0-9]{1}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{3}$/',
		'LV' => '/^LV[0-9]{2}\s?[A-Z]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{1}$/',
		'LI' => '/^LI[0-9]{2}\s?[0-9]{4}\s?[0-9]{1}[A-Z0-9]{3}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{1}$/',
		'LT' => '/^LT[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'LU' => '/^LU[0-9]{2}\s?[0-9]{3}[A-Z0-9]{1}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}$/',
		'MK' => '/^MK[0-9]{2}\s?[0-9]{3}[A-Z0-9]{1}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{1}[0-9]{2}$/',
		'MT' => '/^MT[0-9]{2}\s?[A-Z]{4}\s?[0-9]{4}\s?[0-9]{1}[A-Z0-9]{3}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{3}$/',
		'NL' => '/^NL[0-9]{2}\s?[A-Z]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}$/',
		'NO' => '/^NO[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{3}$/',
		'PL' => '/^PL[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'PT' => '/^PT[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{1}$/',
		'RO' => '/^RO[0-9]{2}\s?[A-Z]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}$/',
		'CS' => '/^CS[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}$/',
		'SK' => '/^SK[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'SI' => '/^SI[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{3}$/',
		'ES' => '/^ES[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'SE' => '/^SE[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/',
		'CH' => '/^CH[0-9]{2}\s?[0-9]{4}\s?[0-9]{1}[A-Z]{3}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{1}$/',
		'TR' => '/^TR[0-9]{2}\s?[0-9]{4}\s?[0-9]{1}[A-Z0-9]{3}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{4}\s?[A-Z0-9]{2}$/',
		'GB' => '/^GB[0-9]{2}\s?[A-Z]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{2}$/',
		'MU' => '/^MU[0-9]{2}\s?[A-Z]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{3}[A-Z]{1}\s?[A-Z]{2}$/',
		'TN' => '/^TN[0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$/'
	);

	public static function check($iso, $iban) {
		if (!array_key_exists($iso, self::$_mapping)) {
			return FALSE;
		}

		return (bool) preg_match(self::$_mapping[$iso], $iban);
	}
}
?>
