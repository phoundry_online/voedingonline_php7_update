<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("Usage of Name 1.0.0 is deprecated", E_USER_ERROR);
}

/**
* Name <<basetype>>
*
* @author Mick van der Most van Spijk
* @author J�rgen Teunis
* @last_modified 21/08/2006
* @version 1.0.0
* @package None
*/
class Name {
	/**
	* 
	* @var String
	* @access public
	*/
	public $firstName;

	/**
	* 
	* @var String
	* @access public
	*/
	public $lastName;

	/**
	* 
	* @var String
	* @access public
	*/
	public $insertion;

	public function __construct($fn, $ln, $i) {
		$this->firstName = $fn;
		$this->lastName = $ln;
		$this->insertion = $i;
	}

	/**
	* Formateer een name presentatie
	*
	*/
	public function strfname($format="%l, %f %i") {
		$str = str_replace('%l', $this->lastName, $format);
		$str = str_replace('%f', $this->firstName, $str);
		$str = str_replace('%i', $this->insertion, $str);
		$str = preg_replace("/ +/", " ", $str); // overtollige whitespace eruit gooien
		return $str;
	}

	public function __toString() {
		return sprintf('%s, %s %s', $this->lastName, $this->firstName, $this->insertion);
	}
}
?>