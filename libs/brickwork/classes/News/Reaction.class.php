<?php
class News_Reaction extends ORM
{
	protected $_table_name = 'module_news_reaction';

	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('datetime', 'DESC');
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
//		'parent' => array('parent_classname', array('own_fields', 'matching_foreign', 'primary_key')),
		'article' => array('News_Article', 'article_id'),
	);
	
	
	/**
	 * Magic getter for the record values
	 *
	 * @throws Exception
	 * @param string $name
	 * @return mixedvar
	 */
	public function offsetGet($name)
	{
		$value = ActiveRecord::offsetGet($name);
		
		switch ($name) {
			case 'ipadress':
				$value = long2ip($value);
			break;
		}
		
		return $value;
	}
	
	/**
	 * Magic setter for the record values
	 *
	 * @param string $name
	 * @param mixedvar $value
	 */
	public function offsetSet($name, $value)
	{
		switch ($name) {
			case 'ipadress':
				$value = ip2long($value);
			break;
		}
		
		return ActiveRecord::offsetSet($name, $value);
	}
}

