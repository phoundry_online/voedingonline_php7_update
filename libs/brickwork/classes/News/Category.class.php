<?php
class News_Category extends ORM
{
	protected $_table_name = 'module_news_category';

	// cache variables
	private $_children;
	private $_articles;
	private $_path;

	/**
	 * Overloaded construct so we can set some variables
	 *
	 * @param int $id
	 * @param bool $loadData
	 */
	public function __construct($id = null, $loadData = true)
	{
		parent::__construct($id, $loadData);

		$this->_children = null;
		$this->_path = null;
	}

	/**
	 * Get the parent folder
	 *
	 * @return News_Category
	 */
	public function getParent()
	{
		$parent = ActiveRecord::factory(get_class($this), $this['parent_id']);
		return $parent->isLoaded() ? $parent : null;
	}

	/**
	 * Get the children categories sorted by prio
	 *
	 * @return ActiveRecordIterator
	 */
	public function getChildren($limit = null, $offset = null)
	{
		if(is_null($this->_children)) {
			if(empty($this['id'])) {
				$this->_children = News::categoryFactory()->
					where('parent_id', 'NULL', 'IS', FALSE)->
					orderBy('prio', 'DESC')->
					limit($limit, $offset)->
					get();
			} else {
				$this->_children = News::categoryFactory()->
					where('parent_id', $this['id'])->
					orderBy('prio', 'DESC')->
					limit($limit, $offset)->
					get();
			}
		}
		return $this->_children;
	}

	/**
	 * Get the articles sorted by prio
	 *
	 * @return ActiveRecordIterator
	 */
	public function getArticles($type = null, $limit = null, $offset = null, &$total_items = null)
	{
		if(!is_array($this->_articles)) {
			$this->_articles = array();
		}

		if(!isset($this->_articles[$type])) {
			$query = News::articleFactory();

			$query->customWhere(
				'(`id` IN (
				SELECT article_id
				FROM `module_news_article_category`
				WHERE `category_id` IN ('.implode(',', $this->getSubIds()).')
				)
				OR
				category_id = '.$this['id'].'
				)'
			);
			switch ($type) {
				case News::MIXED:
				break;
				case News::ARCHIVED:
					$query->where('`offline_datetime`', 'NOW()', '<', false);
					$query->where('archivable', 1);
					break;
				case News::RECENT:
				default:
					$query->where('NOW()', 'online_datetime AND offline_datetime', 'BETWEEN', false);
			}

			$query->orderBy('online_datetime', 'DESC');
			$query->limit($limit, $offset);

			$this->_articles[$type] = $query->get();
			$total_items = $query->getCount();
		}

		return $this->_articles[$type];
	}

	/**
	 * Get the articles of the current category and all subcategories
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param reference $total_items
	 * @return ActiveRecordIterator
	 */
	public function getAllArticles($type = null, $limit = null, $offset = null, &$total_items = null)
	{
		$query = News::articleFactory();
		$query->customWhere(
			'(`id` IN (
			SELECT article_id
			FROM `module_news_article_category`
			WHERE `category_id` IN ('.implode(',', $this->getSubIds()).')
			)
			OR
			category_id = '.$this->_db->quoteValue($this['id']).'
			)'
		);
		switch ($type) {
			case News::MIXED:
			break;
			case News::ARCHIVED:
				$query->where('`offline_datetime`', 'NOW()', '<', false);
				$query->where('archivable', 1);
				break;
			break;
			case News::RECENT:
			default:
				$query->where('NOW()', 'online_datetime AND offline_datetime', 'BETWEEN', false);
		}

		$query->orderBy('online_datetime', 'DESC');
		$query->limit($limit, $offset);

		$articles = $query->get();
		$total_items = $query->getCount();

		return $articles;
	}

	/**
	 * Get the category path up untill the root category
	 *
	 * @return array
	 */
	public function getPath()
	{
		if(is_null($this->_path)) {
			$path = array($this);
			$folder = $this;
			$ids = array($this['id']);
			while($folder->getParent() !== null) {
				$path[] = $folder = $folder->getParent();

				// Recursion Detection
				if(in_array($folder['id'], $ids)) {
					throw new Exception('Recursion detected on category: '.$folder['name']);
				}
				$ids[] = $folder['id'];
			}
			$this->_path = array_reverse($path);
		}
		return $this->_path;
	}

	/**
	 * Get the ids of the path categories
	 *
	 * @return array
	 */
	public function getPathIds()
	{
		$folders = $this->getPath();
		$path_ids = array();
		foreach($folders as $folder) {
			$path_ids[] = $folder['id'];
		}
		return $path_ids;
	}

	public function getSubIds()
	{
		$cats = array();
		$cats[] = (int) $this['id'];
		foreach($this->getChildren() as $child) {
			$cats = array_merge($cats, $child->getSubIds());
		}
		return $cats;
	}

	/**
	 * Delete folder
	 *
	 * @param int $new_parent_id
	 * @return bool
	 */
	public function delete($new_parent_id = null)
	{
		$articles = $this->getArticles();
		$children = $this->getChildren();

		if (null !== $new_parent_id) {
			$new_parent = News::categoryFactory($new_parent_id);
			foreach ($children as $child) {
				$child['parent_id'] = $new_parent_id;
				$child->save();
			}
			foreach ($articles as $article) {
				$article->remove('categories', $this);
				$article->add('categories', $new_parent);
				$article->save();
			}
		} else {
			foreach ($articles as $article) {
				$article->remove('categories', $this);
			}
			foreach ($children as $child) {
				$child->delete();
			}
		}

		return parent::delete();
	}

}
