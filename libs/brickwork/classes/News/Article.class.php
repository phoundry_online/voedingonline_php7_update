<?php
/**
 * News article class representing a article in the database
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 * @property ActiveRecordIterator $categories
 */
class News_Article extends ORM
{
	protected $_table_name = 'module_news_article';
	
	/**
	 * One to many configuration (this->n / childs)
	 *
	 * @var array
	 */
	protected $_one_to_many = array(
		'reactions' => array('News_Reaction', 'article_id'),
	);
	
	public function __construct($id = null, $load = true) {
		$this->_many_to_many['categories'] = array(News::$category_class,
			'module_news_article_category', 'article_id', 'category_id');
		
		parent::__construct($id, $load);
	}
	
	/**
	 * Returns the parent category if just one selected null otherwise
	 * 
	 * @return News_Category|void
	 */
	public function getParent()
	{
		if($this['category_id']) {
			$parent = News::categoryFactory()->
				where('id', $this['category_id'])->get(true);
			if($parent->isLoaded()) {
				return $parent;
			}
		}
		return null;
	}
	
	/**
	 * Is the article archived
	 *
	 * @param int $timestamp
	 * @return bool
	 */
	public function isArchived($timestamp = null)
	{
		if(null === $timestamp) {
			$timestamp = PHP_TIMESTAMP;
		}
		return ($this['archivable']==1 && $timestamp > strtotime($this['offline_datetime']));
	}
	
	/**
	 * Is the article online
	 *
	 * @param int $timestamp
	 * @return bool
	 */
	public function isOnline($timestamp = null)
	{
		if(null === $timestamp) {
			$timestamp = PHP_TIMESTAMP;
		}
		return strtotime($this['online_datetime']) <= $timestamp && 
			strtotime($this['offline_datetime']) > $timestamp;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see classes/ActiveRecord#delete()
	 */
	public function delete($childs = true)
	{
		if($childs) {
			foreach($this->reactions as $reaction) {
				$reaction->delete();
			}
		}
		return parent::delete();
	}
}
