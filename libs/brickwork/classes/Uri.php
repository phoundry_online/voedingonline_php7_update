<?php
/**
 * Class to work effectively with Uri's
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright 2010 Web Power BV, http://www.webpower.nl
 */
class Uri
{
	public $scheme;
	public $host;
	public $port;
	public $user;
	public $pass;
	public $path;
	public $query;
	public $fragment;
	
	/**
	 * Create the Uri from a string
	 * @param string $uri
	 */
	public function __construct($uri)
	{
		$parsed = parse_url($uri);
		foreach ($parsed as $key => $value) {
			if (property_exists($this, $key)) {
				$this->{$key} = $value;
			}
		}
	}
	
	/**
	 * Replace a query param
	 * @param string $key
	 * @param string|array $value
	 */
	public function replaceGet($key, $value)
	{
		parse_str((string) $this->query, $query_string);
		
		$keys = is_array($key) ? $key : explode('[', str_replace(']','', $key));
		$query_string = Utility::setArrayValueRecursive($query_string, $value, $keys);
		$this->query = http_build_query($query_string);
		return $this;
	}
	
	
	/**
	 * Turn the url back into a string
	 * 
	 * @return string
	 */
	public function __toString()
	{
		$url = '';
		if (isset($this->scheme)) {
			$url .= $this->scheme.'://';
		}
		if (isset($this->host)) {
			if (isset($this->user)) {
				$url .= $this->user;
				if (isset($this->pass)) {
					$url .= ":$this->pass";
				}
				$url .= '@';
			}
			$url .= $this->host;
			if (isset($this->port)) {
				$url .= ":$this->port";
			}
		}
		
		if (isset($this->path)) {
			$url .= $this->path;
		}
		
		if (isset($this->query)) {
			$url .= "?$this->query";
		}
		if (isset($this->fragment)) {
			$url .= "#$this->fragment";
		}
		
		return $url;
	}
}