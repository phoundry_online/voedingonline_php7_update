<?php
class DMD_Exception extends Exception
{
	const DUPLICATE = 100;
	const ERROR = 105;
	const MAILERROR = 110;
}