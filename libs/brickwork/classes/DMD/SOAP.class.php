<?php
/**
 * Dm Delivery Soap Interface
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @since	16-01-2009
 * @todo dont extend SoapClient but wrap it instead!
 */
class DMD_SOAP extends SoapClient
{
	// IDS as defined in DMD, ask Arjan for an up to date list!
	const FIRST_HARDBOUNCE_ID = 10;
	const SECOND_HARDBOUNCE_ID = 20;
	const HARDBOUNCE_ID = 30;
	const FIRST_SOFTBOUNCE_ID = 38;
	const SECOND_SOFTBOUNCE_ID = 39;
	const SOFTBOUNCE_ID = 40;
	const TMP_UNSUBSCRIBERS_ID = 49;
	const UNSUBSCRIBERS_ID = 50;
	const SPAMCOMPLAINT_ID = 52;
	const OPTIN_ID = 60;
	const SUBSCRIBERS_ID = 70;
	
	/**
	 * @deprecated
	 */
	const EXCEPTION_DUPLICATE = 100;
	/**
	 * @deprecated
	 */
	const EXCEPTION_ERROR = 105;
	/**
	 * @deprecated
	 */
	const EXCEPTION_MAILERROR = 110;
	
	/**
	 * Login object that has to be send with every request as the first parameter
	 * @var array
	 */
	protected $login;
	
	public function __construct($wsdl, $username, $password, array $options = array())
	{
		$this->setCredentials($username, $password);
		parent::__construct($wsdl, $options);
	}
	
	/**
	 * Overwrite the default __call
	 *
	 * @param string $function_name
	 * @param array $soapParams
	 * @return mixed
	 */
	public function __call($function_name, $soapParams): mixed
	{
		$params = array_merge(array($this->login), $soapParams);
		return $this->__soapCall($function_name, $params);
	}
	
	/**
	 * Set the credentials that will be used for the calls
	 * @param string $username
	 * @param string $password
	 * @return DMD_SOAP $this
	 */
	public function setCredentials($username, $password)
	{
		$this->login = array(
			'username'	=> $username,
			'password'	=> $password
		);
		return $this;
	}
	
	/**
	 * Add a new recipient to the DMdelivery campaign for subscribe.
	 *
	 * @throws DMD_Exception
	 * @param array	$recipientData An associative array (key: name of field, value: value of field) containing recipient data
	 * @param bool $duplicate
	 * @param bool $overwrite
	 * @return int recipientID
	 */
	public function subscribe(array $recipientData, $duplicate = false, $overwrite = true)
	{
		$result = $this->addRecipient(array(self::SUBSCRIBERS_ID),
			$recipientData, $duplicate, $overwrite);
		
		if($result->status !== 'OK') {
			if($result->status === 'DUPLICATE' && !$duplicate && !$overwrite) {
				throw new DMD_Exception('Duplicate subscriber found!',
					DMD_Exception::DUPLICATE);
			}
			if($result->status == 'ERROR') {
				throw new DMD_Exception($result->statusMsg,
					DMD_Exception::ERROR);
			}
		}
		
		$recipient_id = $result->id;
		
		if(is_int($recipient_id)) {
			$group_ids = $this->removeRecipientFromGroups(
				$recipient_id,
				array(
					self::FIRST_HARDBOUNCE_ID,
					self::SECOND_HARDBOUNCE_ID,
					self::HARDBOUNCE_ID,
					self::FIRST_SOFTBOUNCE_ID,
					self::SECOND_SOFTBOUNCE_ID,
					self::SOFTBOUNCE_ID,
					self::OPTIN_ID,
					self::TMP_UNSUBSCRIBERS_ID,
					self::UNSUBSCRIBERS_ID
				)
			);
		}
		
		return $recipient_id;
	}

	/**
	 * Add a new recipient to the DMdelivery campaign for optin.
	 *
	 * @throws DMD_Exception
	 * @param array $recipientData An associative array (key: name of field, value: value of field) containing recipient data
	 * @return int recipienID
	 */
	public function optin($recipientData, $mailing, $duplicate = false,
		$overwrite = true)
	{
		$result = $this->addRecipient(array(self::OPTIN_ID), $recipientData,
			$duplicate, $overwrite);
		
		if($result->status !== 'OK') {
			if($result->status === 'DUPLICATE' && !$duplicate && !$overwrite) {
				throw new DMD_Exception('Duplicate subscriber found!',
					DMD_Exception::DUPLICATE);
			}
			if($result->status === 'ERROR') {
				throw new DMD_Exception($result->statusMsg,
					DMD_Exception::ERROR);
			}
		}
		
		$recipient_id = $result->id;
		
		if(is_int($recipient_id)) {
			$groups = $this->getRecipientGroups($recipient_id);
			
			if($groups && !in_array(self::SPAMCOMPLAINT_ID, $groups)) {
				$res = $this->sendSingleMailing($mailing, $recipient_id);
				if(!$res) {
					throw new DMD_Exception(
						'Could not send single mailing to user '.$recipient_id,
						DMD_Exception::MAILERROR);
				}
			}
		}
		return $recipient_id;
	}
	
	/**
	 * Get the groups the recipient is part of
	 * 
	 * @param int $recipient_id
	 * @return array
	 */
	public function getRecipientGroups($recipient_id)
	{
		return (array) $this->__call("getRecipientGroups", array($recipient_id));
	}
	
	/**
	 * Mark a recipient as unsubscribed
	 *
	 * @param int $recipientID The database id of the recipient
	 * @return array An array of groups (database ids) the recipient is now a member of.
	 */
	public function unsubscribe($recipientID)
	{
		return $this->addRecipientToGroups($recipientID,
			array(self::UNSUBSCRIBERS_ID));
	}
	
	/**
	 * Mark a recipient as temporarily unsubscribed
	 *
	 * @param int $recipientID The database id of the recipient
	 * @return array An array of groups (database ids) the recipient is now a member of.
	 */
	public function tmpUnsubscribe($recipientID)
	{
		return $this->addRecipientToGroups($recipientID,
			array(self::TMP_UNSUBSCRIBERS_ID));
	}
}
