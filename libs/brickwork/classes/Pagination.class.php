<?php
/**
 * Class helping with Pagination calculations
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @since 01-04-2008
 * @version 0.2 // Getter name changes causes incompatibility  with 0.1
 * @version 0.3 // ArrayAccess to the pages
 * 
 * @property int $items The total amount of items available (if set)
 * @property int $total_items see $items
 * @property int $items_per_page The amount of items shown per page
 * @property int $limit see $items_per_page
 * @property int $offset The offset for the current page (for use in mysql)
 * @property int $page The current page (starting from 0)
 * @property int $current_page see $page
 * @property int $first_on_page Index of the first item on the page in the whole result
 * @property int $last_on_page Index of the last item on the page in the whole result
 * @property int $pages Total amount of pages
 * @property int $total_pages see $pages
 * @property int $items_after_page Amount of items available after this page (current page not included)
 * @property int $items_before_page Amount of items in front of this page (current page not included)
 * @property int $next_page Index of the next page (pages are zero indexed)
 * @property int $prev_page Index of the previous page (pages are zero indexed)
 * @property Pagination $next_page_obj The page after the current page
 * @property Pagination $prev_page_obj The page before the current page
 * @property int $next_page_items Amount of items on the next page
 * @property int $prev_page_items Amount of items on the previous page
 */
class Pagination implements Iterator, ArrayAccess
{
	protected $_total_items;
	protected $_limit;
	protected $_current_page;
	
	/**
	 * The current item in the pagination
	 *
	 * @var Pagination
	 */
	protected $_current_iteration;
	
	protected $_get_param;
	
	public function __construct($limit, $curPage, $totalItems=null, $get = null)
	{
		$this->_total_items = $totalItems;
		$this->_limit = $limit;
		$this->_current_page = $curPage>= 0 ? $curPage : 0;
		$this->_get_param = $get;
	}
	
	public function __get($name)
	{
		switch ($name) {
			case 'items':
			case 'total_items':
				if(is_null($this->_total_items)) throw new Exception("Total items isn't set yet use setTotalItems to set it after construction");
				return $this->_total_items;
								
			case 'items_per_page':
			case 'limit':
				return $this->_limit;
				
			case 'offset':
				return $this->limit*$this->current_page;
			
			case 'page':
			case 'current_page':
				return $this->_current_page;
				
			case 'page_nr':
			case 'current_page_nr':
				return $this->current_page+1;
				
			case 'first_on_page':
				return min($this->offset+1, $this->total_items);
			case 'last_on_page':
				return min($this->offset+$this->limit, $this->total_items);
				
			case 'pages':
			case 'total_pages':
				return ceil($this->total_items / $this->limit);
								
			case 'items_after_page':
				return ($this->total_items > ($this->next_page*$this->limit))? $this->total_items - ($this->next_page*$this->limit): 0;
			case 'items_before_page':	
				return ($this->current_page != 0)? $this->current_page*$this->limit: 0;
				
			case 'next_page':
				return $this->current_page+1;
			case 'prev_page':
				return $this->current_page-1;
				
			case 'next_page_obj':
				return new Pagination($this->limit, $this->next_page, $this->_total_items, $this->_get_param);
			case 'prev_page_obj':
				return new Pagination($this->limit, $this->prev_page, $this->_total_items, $this->_get_param);
				
			case 'next_page_items':
				return min($this->limit, $this->items_after_page);
			case 'prev_page_items':
				return min($this->limit, $this->items_before_page);
		}
	}
	
	public function setTotalItems($cnt)
	{
		$this->_total_items = (int) abs($cnt);
		return $this->total_items;
	}
	
	public function setPage($page)
	{
		$this->_current_page = $page>=0 ? $page : 0;
	}
	
	public function qsPart()
	{
		return !is_null($this->_get_param) ? $this->_get_param : strtolower(get_class($this));
	}
	
	public function current(): mixed
	{
		return $this->_current_iteration;
	}
	
	public function key(): mixed
	{
		return $this->_current_iteration->page;
	}
	
	public function next(): void
	{
		$this->_current_iteration->setPage($this->_current_iteration->next_page);
	}
	
	public function rewind(): void
	{
		$this->_current_iteration = new Pagination($this->limit, 0, $this->total_items, $this->_get_param);
	}
	
	public function valid(): bool
	{
		return (($this->_current_iteration->current_page+1) <= $this->total_pages);
	}
	
	public function offsetExists($offset): bool
	{
		$pagination = new Pagination($this->limit, $offset, $this->total_items, $this->_get_param);
		return ($pagination->current_page < $this->total_pages);
	}
	
	public function offsetGet($offset): mixed
	{
		return $this->offsetExists($offset) ? new Pagination($this->limit, $offset, $this->total_items, $this->_get_param) : null;
	}
	
	public function offsetSet($offset, $value): void {}
	public function offsetUnset($offset): void {}
}
