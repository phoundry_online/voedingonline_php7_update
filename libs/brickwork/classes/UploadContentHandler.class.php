<?php
/**
 * Contenthandler for generic Ajax/Flash uploading
 *
 */
class UploadContentHandler extends ContentHandler
{
	protected $action;
	protected $upload_path;
	
	protected $upload_type;
	protected $upload_key;
	
	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
		
		try {
			SessionManager::factory();
			
			$this->upload_type = isset($_REQUEST['upload_type']) ? $_REQUEST['upload_type'] : 'uploads';
			$this->upload_key = isset($_REQUEST['upload_key']) ? $_REQUEST['upload_key'] : '';
			if(empty($this->upload_key)) {
				throw new Exception('No valid key given');
			}
			
			if(!isset($GLOBALS['WEBprefs']['UploadContentHandler']['directory']) || !(is_dir($GLOBALS['WEBprefs']['UploadContentHandler']['directory']) || mkdir($GLOBALS['WEBprefs']['UploadContentHandler']['directory'], 0777, true)))
			{
				throw new Exception('Upload directory not found');
			}
			
			$this->upload_path = $GLOBALS['WEBprefs']['UploadContentHandler']['directory'];
			
			$this->action = array_shift($this->path);
			if(empty($this->action)) $this->action = 'upload';
		}
		catch(Exception $e) {
			header("HTTP/1.0 404 Not Found", true, 404);
			if(defined('TESTMODE') && TESTMODE) {
				die($e);
			}
			else {
				die('404 Not Found');
			}
		}

	}
	
	/**
	 * Run the chosen action
	 */
	public function init()
	{
		if(!is_file(TMP_DIR.'uploadContentHandlerLastRun') ||
			date('Ymd',file_get_contents(TMP_DIR.'uploadContentHandlerLastRun')) < date('Ymd', PHP_TIMESTAMP)
		){
			Utility::sweepDir($this->upload_path, 86400);
			file_put_contents(TMP_DIR.'uploadContentHandlerLastRun', PHP_TIMESTAMP);
		}
		
		try {
			$rfl = new ReflectionMethod($this, $this->action.'Action');
			if ($rfl->isPublic()) {
				$output = $rfl->invokeArgs($this, $this->path);
			}
		}
		catch (Exception $e) {
			header("HTTP/1.0 404 Not Found", true, 404);
			if(defined('TESTMODE') && TESTMODE) {
				die($e->getMessage()." in file:".$e->getFile()." on line:".$e->getLine());
			}
			else {
				die('404 Not Found');
			}
		}
		echo $output;
		exit(0);
	}
	
	public function uploadAction()
	{
		if(empty($_FILES)) {
			throw new Exception('No file uploaded');
		}
		
		$files = UploadContentHandler_File::getFiles($this->upload_key, $this->upload_type);
		
		$collection = new File_Upload_Collection($_FILES);
		$files = array_merge($files, $this->_saveFiles($collection));
		
		UploadContentHandler_File::setFiles($files, $this->upload_key, $this->upload_type);
		
		if(!isset($_REQUEST['ajax'])) {
			trigger_redirect($_SERVER['HTTP_REFERER']);
			return;
		}
		
		return json_encode(UploadContentHandler_File::getFilesAsArray($this->upload_key, $this->upload_type));
	}
	
	protected function _saveFiles(File_Upload_Collection $collection)
	{
		$files = array();
		foreach ($collection as $entry) {
			if ($entry instanceof File_Upload_Collection) {
				$files = array_merge($files, $this->_saveFiles($entry));
			}
			else {
				$new_tmpfile = basename($entry->getTmpName()).
					substr((string) $entry, strrpos((string) $entry, '.'));

				$entry->save($this->upload_path.'/'.$new_tmpfile, true);
				
				$files[] = new UploadContentHandler_File(
					(string) $entry,
					$new_tmpfile,
					$entry->getMimeType(),
					$this->upload_path,
					$GLOBALS['WEBprefs']['UploadContentHandler']['url_thumb'],
					$GLOBALS['WEBprefs']['UploadContentHandler']['url_zoom']
				);
			}
		}
		return $files;
	}
	
	/**
	 * Removes a given file
	 * @return void
	 */
	public function removeAction()
	{
		if(!isset($_REQUEST['key'])) {
			throw new Exception('No file selected');
		}
		
		$files = UploadContentHandler_File::getFiles($this->upload_key, $this->upload_type);
		
		$file = Utility::arrayValue($files, $_REQUEST['key']);
		
		if($file) {
			$file->delete();
			array_splice($files, $_REQUEST['key'], 1);
			UploadContentHandler_File::setFiles($files, $this->upload_key, $this->upload_type);
		}
		
		if(!isset($_REQUEST['ajax'])) {
			trigger_redirect($_SERVER['HTTP_REFERER']);
			return;
		}
		
		return json_encode(UploadContentHandler_File::getFilesAsArray($this->upload_key, $this->upload_type));
	}
	
}
