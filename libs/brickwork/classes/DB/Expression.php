<?php
/**
 * Wrapper om een MySQL functie netjes in de query te kunnen krijgen
 *
 * @author Peter Eussen <peter.eussen@webpower.nl>
 */
class DB_Expression
{
	protected	$_value;

	public function __construct($value)
	{
		$this->_value = (string) $value;
	}

	public function __toString()
	{
		return $this->_value;
	}

}