<?php
/**
 * Class for various database synchronization functions
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
class DB_Sync
{
	/**
	 * @var array Default tables which are ignored
	 */
	public $ignored = array(
		'phoundry_column',
		'phoundry_event',
		'phoundry_filter',
		'phoundry_group',
		'phoundry_group_column',
		'phoundry_group_table',
		'phoundry_lock',
		'phoundry_login',
		'phoundry_menu',
		'phoundry_note',
		'phoundry_notify',
		'phoundry_table',
		'phoundry_user',
		'phoundry_user_group',
		'phoundry_user_shortcut',
		'phoundry_content_node',
		'phoundry_tag',
		'phoundry_tag_content_node',		
	);
	
	public $types = array(
		'int_types' => array(
			'tinyint',
			'smallint',
			'mediumint',
			'int',
			'bigint'
		),
		'text_types' => array(
			'tinytext',
			'text',
			'mediumtext',
			'longtext',
		),
		'blob_types' => array(
			'tinyblob',
			'blob',
			'mediumblob',
			'longblob'
		),
	);
	
	/**
	 * @var resource
	 */
	private $_db;
	
	/**
	 * Construct a new Sync for the given DB
	 * 
	 * @param resource $db optional
	 */
	public function __construct($db = null)
	{
		if (null !== $db) {
			$this->setDb($db);
		}
	}
	
	/**
	 * Sets the database so dumps can be generated
	 * @param resource $db
	 */
	public function setDb($db)
	{
		if (!is_resource($db)) {
			throw new InvalidArgumentException(
				'db is expected to be a resource'
			);
		}
		$this->_db = $db;
	}
	
	/**
	 * Compares another DB_Sync instance with this one returning the diferences
	 * 
	 * @see DB_Sync#diffDumps()
	 * @param DB_Sync $to
	 * @return array 
	 */
	public function diffDb(DB_Sync $to)
	{
		return $this->diff($to->dump());
	}
	
	/**
	 * Dumps all create statements for all tables in the database
	 * 
	 * @param array $ignored
	 * @return array The create statements
	 */
	public function dump(array $ignored = null)
	{
		if (null === $this->_db) {
			throw new RuntimeException("DB_Sync#dump() needs a DB to be set");
		}
		
		$ignored = $ignored ?
			array_merge($ignored, $this->ignored) : $this->ignored;

		$tables = $this->getTableNames();
		
		$create_statements = array();
		foreach ($tables as $table) {
			if (in_array($table, $ignored)) {
				continue;
			}
			
			$create = $this->getCreateStatement($table);
				
			if ($create) {
				$create = preg_replace(
					array(
						'/CREATE TABLE/i',
						'/ENGINE\=MYISAM /i',
						'/AUTO_INCREMENT\=\d* /i'
					),
					array(
						'CREATE TABLE IF NOT EXISTS',
						'',
						'',
					),
					$create
				);
				
				$create_statements[$table] = $create;
			}
		}
		
		return $create_statements;
	}
	
	/**
	 * Compare a dump of the current database to the given dump using
	 * DB_Sync#diffDumps()
	 * 
	 * @param array $to Array generated with DB_Sync#dump()
	 * @return array see DB_Sync#diffDumps()
	 */
	public function diff(array $to)
	{
		return $this->diffDumps($this->dump(), $to);
	}
	
	/**
	 * Compare the $from and $to arrays and generate the required
	 * create update delete and drop statements
	 * 
	 * @param array $from Array generated with DB_Sync#dump
	 * @param array $to Array generated with DB_Sync#dump
	 * @return array [create, update, delete, drop]
	 */
	public function diffDumps(array $from, array $to)
	{
		$create_statements = array();
		$update_statements = array();
		$truncate_statements = array();
		$delete_statements = array();
		$drop_statements = array();
		
		// Parse the statements to arrays
		$from = $this->parseCreateStatements($from);
		$to = $this->parseCreateStatements($to);
		
		// Generate the create table statements for the missing tables
		foreach (array_diff(array_keys($to), array_keys($from)) as $t) {
			$create_statements[$t] = $to[$t]['create_statement'];
		}
		
		// Generate the drop table statemens for the extra tables
		foreach (array_diff(array_keys($from), array_keys($to)) as $t) {
			$drop_statements[$t] = 'DROP TABLE `'.$t.'`';
		}
		
		// Generate the update and delete statements
		foreach (array_keys($from) as $table) {
			// No need to compare tables that will be deleted
			if (isset($drop_statements[$table])) {
				continue;
			}
			
			$updates = array();
			$truncates = array();
			$deletes = array();
			$prev = '';
			foreach ($to[$table]['columns'] as $column_name => $column) {
				if (!isset($from[$table]['columns'][$column_name])) {
					// new column
					$updates[] = 'ADD COLUMN '.$column.' '.
						($prev ? 'AFTER `'.$prev.'`' : 'FIRST');
				} else if ($from[$table]['columns'][$column_name] !== $column) {
					// update column
					$truncating = $this->willTruncate(
						$from[$table]['columns'][$column_name], $column
					);
					if ($truncating) {
						$truncates[] = 'CHANGE `'.$column_name.'` '.$column;
					} else {
						$updates[] = 'CHANGE `'.$column_name.'` '.$column;
					}
				}
				$prev = $column_name;
			}
			
			foreach ($from[$table]['columns'] as $column_name => $column) {
				if (!isset($to[$table]['columns'][$column_name])) {
					// deleted columns
					$deletes[] = 'DROP COLUMN `'.$column_name.'`';
				}
			}
		
			// Keys
			foreach ($to[$table]['keys'] as $key_name => $key) {
				if (!isset($from[$table]['keys'][$key_name])) {
					// new key
					$updates[] = 'ADD '.$key;
				} else if ($from[$table]['keys'][$key_name] !== $key) {
					// update key
					$from_key =
						explode('(', $from[$table]['keys'][$key_name], 2);
					if (count($from_key) !== 2) {
						throw new DB_Exception(
							'Key does not contain a ( : '.
							$from[$table]['keys'][$key_name]
						);
					}
					$from_key = trim($from_key[0]);
					if ("PRIMARY KEY" === $from_key) {
						$updates[] = 'DROP PRIMARY KEY';
					} else {
						$updates[] = 'DROP KEY `'.$key_name.'`';
					}
					$updates[] = 'ADD '.$key;
				}
			}
			
			foreach ($from[$table]['keys'] as $key_name => $key) {
				if (!isset($to[$table]['keys'][$key_name])) {
					// deleted key
					$deletes[] = 'DROP KEY `'.$key_name.'`';
				}
			}
		
			if ($updates) {
				$update_statements[$table] = 'ALTER TABLE `'.$table.'` '.
					implode(', ', $updates);
			}
			if ($truncates) {
				$truncate_statements[$table] = 'ALTER TABLE `'.$table.'`'.
					implode(', ', $truncates);
			}
			if ($deletes) {
				$delete_statements[$table] = 'ALTER TABLE `'.$table.'` '.
					implode(', ', $deletes);
			}
		}
		
		return array(
			'create' => $create_statements,
			'update' => $update_statements,
			'truncate' => $truncate_statements,
			'delete' => $delete_statements,
			'drop' => $drop_statements,
		);
	}
	
	/**
	 * Parses a create statement to a usefull array
	 * 
	 * @param string $create_statement a array as generated by DB_Sync#dump()
	 * @return array|null Null if not parsable
	 */
	public function parseCreate($create_statement)
	{
		$create_statement = trim($create_statement);
		$statements = explode("\n", $create_statement);

		// First line contains the table name between backticks
		if ($statement = array_shift($statements)) {
			$table_name = explode("`", $statement);
			if (count($table_name) === 3) {
				$table_name = $table_name[1];
				$charset = explode("=", array_pop($statements));
				$charset = count($charset) === 2 ? $charset[1] : 'utf8';
				
				$columns = array();
				$keys = array();
				while ($statement = array_shift($statements)) {
					$statement = trim($statement);
					$statement = trim($statement, ",");

					$first_word = substr($statement, 0, strpos($statement, " "));

					switch (strtoupper($first_word)) {
						case "PRIMARY":
							$keys["PRIMARY"] = $statement;
							break;
						case "KEY":
						case "UNIQUE":
						case "FULLTEXT":
							// Current statement is a Index
							$key_name = explode('`', $statement, 3);
							if (count($key_name) === 3) {
								$key_name = $key_name[1];
								$keys[$key_name] = $statement;
							} else {
								throw new DB_Exception(
									'Key statement malformed: '.$statement
								);
							}
							break;
						default:
							// Current statement is a column
							$column_name = explode('`', $statement);
							if (count($column_name) === 3) {
								$column_name = $column_name[1];
								$columns[$column_name] = $statement;
							} else {
								throw new DB_Exception(
									'Column statement malformed: '.$statement
								);
							}
							break;
					}
				}

				return compact(
					'table_name',
					'create_statement',
					'keys',
					'columns'
				);
			}
		}
		
		return null;
	}

	// Parse the statements to arrays
	private function parseCreateStatements(array $statements)
	{
		$t = array(); 
		foreach ($statements as $c) {
			if ($c = $this->parseCreate($c)) {
				$t[$c['table_name']] = $c;
			}
		}
		return $t;
	}
	
	// Parse a column statement line for its type and length at this time
	private function parseColumnStatement($column)
	{
		$matches = array();
		preg_match('/^\s*\`\w+\`\s*(\w+)(\((\d+)\))?/', $column, $matches);
		// $matches[1]; = type
		// $matches[3]; = length [optional]
		return array(
			'type' => isset($matches[1]) ? strtolower($matches[1]) : '',
			'length' => isset($matches[3]) ? (int) $matches[3] : 0
		);
	}
	
	// tells if the column will be truncated, very basic check
	private function willTruncate($from, $to)
	{
		$from = $this->parseColumnStatement($from);
		$to = $this->parseColumnStatement($to);
		
		if ($from['type'] === 'varchar') {
			if ($to['type'] === 'varchar' && $from['length'] < $to['length']) {
				return false;
			}
			// All text types except for tinytext are bigger then varchar
			if (in_array($to['type'], $this->types['text_types']) &&
					$to['type'] !== 'tinytext') {
				return false;
			}
			// All blob types except for tinyblob are bigger then varchar
			if (in_array($to['type'], $this->types['blob_types']) &&
					$to['type'] !== 'tinyblob') {
				return false;
			}
			return true;
		}
		
		foreach ($this->types as $type) {
			$i = array_search($from['type'], $type);
			if ($i !== false && $i < array_search($to['type'], $type)) {
				return false;
			}
		}
		return true;
	}
	
	// Fetch all table names in the current database
	private function getTableNames()
	{
		$tables = array();
		$res = $this->query('SHOW TABLES');
		while ($row = mysql_fetch_row($res)) {
			$tables[] = $row[0];
		}
		return $tables;
	}
	
	// Get the create statement for the given table
	private function getCreateStatement($table)
	{
		$res = $this->query('SHOW CREATE TABLE '.$table);
		$row = mysql_fetch_row($res);
		return $row && isset($row[1]) ? $row[1] : false;
	}
	
	private function query($sql)
	{
		$res = mysql_query($sql, $this->_db);
		if ($res === false) {
			throw new Exception(
				mysql_error($this->_db),
				mysql_errno($this->_db)
			);
		}
		return $res;
	}
}