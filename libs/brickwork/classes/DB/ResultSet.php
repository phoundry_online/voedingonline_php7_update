<?php
/**
 * Database resultset
 * Deze class vervangt de oude 'Result' class waarin de hele result altijd
 * in een array werd geladen. Deze class ondersteund de oude manier nog wel, maar
 * je kan nu ook iteraten over de results in de resultset. Hiermee wordt het wat
 * makkelijker om te werken met grotere sets data zonder tegen memory limits aan
 * te lopen.
 *
 * Daarnaast makat deze class het mogelijk om de oude en nieuwe notatie van
 * attributen te gebruiken:
 * num_rows & numRows levert allebei hetzelfde op
 * etc.
 *
 * @author		Peter Eussen <peter.eussen@webpower.nl>
 * @author		Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @package		Brickwork
 * @version		1.0
 * @copyright	Web Power (http://www.webpower.nl)
 *
 * @property int $affected_rows
 * @property int $affectedRows
 * @property int $num_rows
 * @property int $numRows
 * @property int $lastInsertId
 * @property int $last_insert_oid
 * @property array $result
 */
class DB_ResultSet extends ResultSet
{
	/**
	 * Fetch a result row as an associative array
	 */
	const RETURNTYPE_ASSOC = 1;

	/**
	 * Fetch a result row as an enumerated array
	 */
	const RETURNTYPE_ARRAY = 2;

	/**
	 * Fetch a result row as an object
	 */
	const RETURNTYPE_OBJECT = 3;

	/**
	 * Type as how to return the rows
	 * Default RETURNTYPE_OBJECT
	 * @var int
	 */
	protected $_return_type;

	/**
	 * Return objects of this class if set
	 *
	 * @var string
	 */
	protected $_class_name;

	/**
	 * add these parameters to the default return class;
	 *
	 * @var	Array
	 */
	protected $_class_param;

	/**
	 * The current row, so we only fetch it once
	 * @var Array|Object
	 */
	protected $_cur_item;

	/**
	 * Amount of total rows if SQL_CALC_FOUND_ROWS is used in the query
	 * @var int
	 */
	public $total_rows;

	/**
	 * The last insert id when doing an INSERT query
	 * @var int
	 */
	public $last_insert_id;

	/**
	 * Construct
	 * @param resource $source
	 * @param int $count
	 * @param int $total
	 * @param int $last_insert_id
	 */
	public function __construct($source, $count = null, $total = null,
		$last_insert_id = null)
	{
		parent::__construct($source);
		$this->_return_type = self::RETURNTYPE_OBJECT;
		$this->_class_name = null;
		$this->_class_param = null;
		$this->_cur_item = null;
		$this->_count = $count;
		$this->total_rows = $total;
		$this->last_insert_id = $last_insert_id;
	}

	public function __destruct()
	{
		if ($this->_source) {
			@mysqli_free_result($this->_source);
			unset($this->_source);
		}
	}

	/**
	 * Access to readonly attributes
	 * Special method om num_rows, numRows, affected_rows, affectedRows,
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		switch ($name) {
			case 'affected_rows':
			case 'affectedRows':
			case 'num_rows':
			case 'numRows':
				return $this->_count;
			case 'lastInsertId':
			case 'last_insert_oid':
				return $this->last_insert_id;
			case 'result':
				return (array) $this->getAsObjectArray();
			default:
				throw new InvalidArgumentException(sprintf(
					'Invalid attribute: %s', $name));
		}
	}

	/**
	 * Set a property
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set($name, $value)
	{
		switch ($name) {
			case 'affected_rows':
			case 'affectedRows':
			case 'numRows':
			case 'num_rows':
				$this->_count = $value;
				break;
			case 'lastInsertId':
			case 'last_insert_oid':
				$this->last_insert_id = $value;
				break;
			default:
				throw new InvalidArgumentException( sprintf('Invalid attribute: %s', $name));
		}
	}

	/**
	 * Set the return type of the resultset
	 * @param int $type either RETURNTYPE_ASSOC, RETURNTYPE_ARRAY OR RETURNTYPE_OBJECT
	 * @return DB_ResultSet self
	 */
	public function setReturnType($type)
	{
		$this->_return_type = $type;
		return $this;
	}

	/**
	 * Gets the return type of the resultset
	 * @return int either RETURNTYPE_ASSOC, RETURNTYPE_ARRAY OR RETURNTYPE_OBJECT
	 */
	public function getReturnType()
	{
		return $this->_return_type;
	}

	/**
	 * Sets the type of class you want to return
	 * (ATTENTION: resets the internal pointer)
	 *
	 * @param  string 	$className
	 * @param  array 	$param
	 * @return DB_ResultSet self
	 */
	public function setReturnClass($class_name = null, $param = null)
	{
		if(null !== $class_name && !class_exists($class_name)) {
			throw new InvalidArgumentException(
				$class_name.' is an unknown class');
		}
		$this->rewind();
		$this->_class_name	= $class_name;
		$this->_class_param	= null !== $class_name && is_array($param) && $param ?
			$param : null;
		return $this;
	}

	/**
	 * Returns the name of the class that is used to return the rows
	 * @return string|void
	 */
	public function getReturnClass()
	{
		return $this->_class_name;
	}

	/**
	 * Gets the result as an associative array
	 *
	 * @see ResultSet::getAsArray()
	 */
	public function getAsArray($limit = null)
	{
		$type = $this->getReturnType();
		$this->setReturnType(self::RETURNTYPE_ASSOC);
		$res = parent::getAsArray($limit);
		$this->setReturnType($type);
		return $res;
	}

	/**
	 * Returns an array of objects (basically the same as the ->result attribute)
	 * Returns all, or a subset of the result as an array. You can delimit how much
	 * results you want returned by adding the limit parameter. If left empty, it
	 * will return the whole resultset.
	 *
	 * @see ResultSet::getAsArray()
	 */
	public function getAsObjectArray($limit = null)
	{
		$type = $this->getReturnType();
		$this->setReturnType(self::RETURNTYPE_OBJECT);
		$res = parent::getAsArray($limit);
		$this->setReturnType($type);
		return $res;
	}

	/**
	 * @see Resultset::current()
	 */
	public function current(): mixed
	{
		if (null === $this->_cur_item) {
			$this->_cur_item = $this->_fetchRow();
		}
		return $this->_cur_item;
	}

	/**
	 * @see ResultSet::_setCurrentIndex()
	 */
	protected function _setCurrentIndex($value)
	{
		$this->_cur_item = null;
		return parent::_setCurrentIndex($value);
	}

	/**
	 * @see ResultSet::rewind()
	 */
	public function rewind(): void
	{
		$ret = parent::rewind();
		$this->seek($this->_current_index);
	}

	/**
	 * @see ResultSet::seek()
	 */
	public function seek($offset): void
	{
		// Only seek if the offset really is different from the one seeking to
		// This makes rewinding an empty resultset possible
		parent::seek($offset);
		@mysqli_data_seek($this->_source, $offset);
	}

	/**
	 * Fetches a row from the resultset
	 * Depending on the current return type set either returns an array for each row, or an object.
	 *
	 * @return Object|array|bool Returns a boolean false if there are no more rows to fetch
	 */
	protected function _fetchRow()
	{
		switch ($this->_return_type) {
			case self::RETURNTYPE_ASSOC:
				return mysqli_fetch_assoc($this->_source);

			case self::RETURNTYPE_ARRAY:
				return mysqli_fetch_row($this->_source);

			case self::RETURNTYPE_OBJECT:
			default:
				if($this->_class_name) {
					if($this->_class_param) {
						return mysqli_fetch_object($this->_source, $this->_class_name, $this->_class_param);
					}
					return mysqli_fetch_object($this->_source, $this->_class_name);
				}
				return mysqli_fetch_object($this->_source);
		}

	}
}
