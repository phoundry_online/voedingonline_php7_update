<?php
/**
 * The various database adapters should implement these functions
 * 
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */
interface DB_Adapter
{
	/**
	 * Connect this instance to the given database
	 * 
	 * @param string $host
	 * @param string $user
	 * @param string $pass
	 * @param string $db
	 * @param int $port
	 * @return DB_Adapter|Error
	 */
	public function connect($host, $user, $pass, $db, $port = 3306);
	
	/**
	 * Disconnects this connection from the current database
	 * 
	 * @throws DB_Exception Thrown when there is no open connection
	 * @return bool If the connection was succesfully closed or not
	 */
	public function disconnect();
	
	/**
	 * Query the database with INSERT, DELETE, SELECT and UPDATE statements
	 *
	 * @deprecated use iQuery instead
	 * @throws DB_Exception Thrown when there is no db connection
	 * @return DB_ResultSet|MySQLError
	 */
	public function query($query);
	
	/**
	 * Query the database with INSERT, DELETE, SELECT and UPDATE statements
	 * 
	 * @throws DB_Exception Thrown when given an invalid query
	 * @param string $sql
	 * @return ResultSet
	 */
	public function iQuery($sql);
	
	/**
	 * Set collation for the current open connection
	 *
	 * @param string collation
	 * @return DB_Adapter self
	 */
	public function setCollation($collation);
	
	/**
	 * The current active collation
	 * 
	 * @return string|null null if there is no collation specifically set
	 */
	public function getCollation();
	
	/**
	 * Total time used for queries
	 * 
	 * @return float
	 */
	public function getTotalQueryTime();
	
	/**
	 * Time it took to execute the last runned query
	 * 
	 * @return float
	 */
	public function getQueryTime();
	
	/**
	 * Number of queries executed on this run
	 * 
	 * @return int
	 */
	public function getQueryCount();
	
	/**
	 * Get this connection as a raw PDO instantion
	 * 
	 * @return PDO
	 */
	public function asPDO();
	
	/**
	 * Escape a SQL statement
	 *
	 * @throws DB_Exception Thrown when there is no db connection
	 * @param string $str Text to escape
	 * @param bool $quote Also quote the value
	 * @return string
	 */
	public function quote($str, $quote = false);
	
	/**
	 * Improved quotes 
	 * 
	 * @throws InvalidArgumentException
	 * @param mixedvar $value
	 * @return string|array
	 */
	public function quoteValue($value);
	
	/**
	 * Create Insert Into statement
	 *
	 * @param array $rows Multi deminsional array
	 * @param string $table
	 * @return array Insert id's 
	 */
	public function insertRows(array $rows, $table);
	
	/**
	 * Updates the given rows
	 *
	 * @param array $rows Multi deminsional array
	 * @param string $table
	 * @param array $match_colums Columns to match on
	 * @return bool
	 */
	public function updateRows(array $rows, $table, array $match_colums = array('id'));
	
	/**
	 * Gets a named lock
	 * 
	 * @param string $name Name of the lock
	 * @param int $timeout Timeout in seconds
	 * @return bool If the lock was succesfully obtained
	 */
	public function getLock($name, $timeout = 10);
	
	/**
	 * Releases a named lock
	 * 
	 * @param string $name Name of the lock
	 * @return bool Always true
	 */
	public function releaseLock($name);
	
	/**
	 * Check if a lock is set and by whom
	 * 
	 * @param string $name
	 * @return bool|int The connection identifier that has the lock
	 */
	public function isLocked($name);
	
	/**
	 * Checks if a table exists in the database
	 * 
	 * @param string $name
	 * @return bool
	 */
	public function tableExists($name);
	
	/**
	 * Checks if a column exists for a given table
	 * 
	 * @param string $table
	 * @param string $column
	 * @return bool
	 */
	public function columnExists($table, $column);
	
	/**
	 * Returns the database name of the currently selected database
	 * 
	 * @return string|void
	 */
	public function getDbName();
	
	/**
	 * Returns the details for this connection
	 * 
	 * @return array
	 */
	public function getConnectionDetails();
}