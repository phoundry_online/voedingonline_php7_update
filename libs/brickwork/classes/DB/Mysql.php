<?php
/**
 * DB_Mysql instance, the new DB class
 *
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @copyright Web Power BV, http://www.webpower.nl
 */

class DB_Mysql implements DB_Adapter
{
	/**
	 * @var array The credentials to set up the link
	 */
	protected $_connection_details;

	/**
	 * @var string the current active collation
	 */
	protected $_collation;

	/**
	 * @var resource The mysql link
	 */
	protected $_resource;

	/**
	 * @var int Amount of executed queries
	 */
	protected $_query_count;

	/**
	 * @var float Time spend on querying
	 */
	protected $_time_spend;

	/**
	 * @var float Time spend on the last query
	 */
	protected $_time_spend_last;

	public function __construct()
	{
		$this->_query_count = 0;
		$this->_time_spend = (float) 0;
		$this->_time_spend_last = (float) 0;
	}

	/**
	 * Connect this instance to the given database
	 *
	 * @param string $host
	 * @param string $user
	 * @param string $pass
	 * @param string $db
	 * @param int $port
	 * @return DB_Mysql|MySQLError|ErrorVoedingonline
	 */
	public function connect($host, $user, $pass, $db, $port = 3306)
	{
		if(!is_int($port)) {
			throw new InvalidArgumentException("port needs to be of type int");
		}

		if($connect = @mysqli_connect($host.":".$port, $user, $pass)) {
			if(@mysqli_select_db($connect, $db)) {
				$this->_resource = $connect;
				$this->_connection_details = compact('host', 'user', 'pass',
					'db', 'port');
			}
			else {
				return new MySQLError(mysqli_errno($connect), mysqli_error($connect), __FILE__, __LINE__);
			}
		}
		else {
			return new ErrorVoedingonline(E_USER_ERROR,'Could not connect to database');
		}
		return $this;
	}

    public function mysqli_result($result,$row,$field=0) {

        if ($result===false) return false;
        if ($row>=mysqli_num_rows($result)) return false;
        if (is_string($field) && !(strpos($field,".")===false)) {
            $t_field=explode(".",$field);
            $field=-1;
            $t_fields=mysqli_fetch_fields($result);
            for ($id=0;$id<mysqli_num_fields($result);$id++) {
                if ($t_fields[$id]->table==$t_field[0] && $t_fields[$id]->name==$t_field[1]) {
                    $field=$id;
                    break;
                }
            }
            if ($field==-1) return false;
        }
        mysqli_data_seek($result,$row);
        $line=mysqli_fetch_array($result);
        return isset($line[$field])?$line[$field]:false;
    }

	/**
	 * Disconnects this connection from the current database
	 *
	 * @return bool If the connection was succesfully closed or not
	 */
	public function disconnect()
	{
		if(!$this->_resource) {
			throw new DB_Exception("Cannot disconnect as there is no open connection");
		}
		if(mysqli_close($this->_resource)) {
			unset($this->_resource, $this->_connection_details,
				$this->_collation);
			return true;
		}
		return false;
	}

	/**
	 * Query the database with INSERT, DELETE, SELECT and UPDATE statements
	 *
	 * @deprecated use iQuery instead
	 * @return DB_ResultSet|MySQLError
	 */
	public function query($query)
	{
		if (!$this->_resource) {
			throw new DB_Exception('No database connection');
		}

		try {
			return $this->iQuery($query);
		}
		catch (DB_Exception $e) {
			return new MySQLError($e->getCode(), $e->getMessage(),
				__FILE__, __LINE__);
		}
	}

	/**
	 * Improved query always returns DB_ResultSet in contrarary to query
	 *
	 * @throws DB_Exception Thrown when given an invalid query
	 * @param string $sql
	 * @return DB_ResultSet
	 */
	public function iQuery($sql)
	{
		$connect = $this->_resource;

		if (!$connect) {
			throw new DB_Exception('No database connection');
		}

		$_start_time = false;
		if(defined('DB_LOG_QUERY') && DB_LOG_QUERY > 0){
			$_start_time = microtime(true);
		}

		$res = @mysqli_query($connect, $sql);
		$this->_query_count++;

		if($res === false) {
			throw new DB_Exception("Error in query: ".$sql." mysqlError:".
				mysqli_error($connect), mysqli_errno($connect));
		}

		$count = $total = $last_insert_id = null;

		if(is_resource($res)) {
			$count = mysqli_num_rows($res);
		}
		else if(true === $res) {
			$res = null;
			$count = mysqli_affected_rows($connect);

			// Voor nu maar even zo, want anders moet ik nog meer omgooien ben ik bang. Dit is
			// in ieder geval altijd beter dan wat er eerst was!
			if(in_array(strtoupper(strtok($sql, "\t\r\n ")), array('INSERT','REPLACE'))) {
				$last_insert_id = mysqli_insert_id($connect);
			}
		}

		// kijken naar SQL_CALC_ROWS dan found rows ophalen
		if(stristr($sql, 'SQL_CALC_FOUND_ROWS')) {
			$cnt = mysqli_query($connect, 'SELECT FOUND_ROWS()');
			$total = $this->mysqli_result($cnt, 0);
		}
		else {
			$total = $count;
		}

		$result = new DB_ResultSet($res, $count, $total, $last_insert_id);

		if(false !== $_start_time) {
			$this->_time_spend_last = $_elapsed = microtime(true) - $_start_time;
			$this->_time_spend += $_elapsed;

			if(defined('DB_LOG_QUERY')) {
				switch(DB_LOG_QUERY){
					case 2:
						// log only queries that take more than a second
						$time = (defined('DB_LOG_QUERY_SLOW_TIME') &&
							is_float(DB_LOG_QUERY_SLOW_TIME)) ?
								DB_LOG_QUERY_SLOW_TIME : 0.1;

						if($_elapsed < $time){
							break;
						}
					case 1:
						// log query
						$b = debug_backtrace();

						$i = (isset($b[1]['class']) && $b[1]['class'] == 'DB') ?
							1 : 2;

						$where = array();
						if(isset($b[$i]['object'])){
							$where[] = get_class($b[$i]['object']);
						}
						if(isset($b[$i]['class'])){
							$where[] = $b[$i]['class'];
						}
						if(isset($b[$i]['function'])){
							$where[] = $b[$i]['function'];
						}

						if(!$where) {
							$obj = 'UNKNOWN';
						}
						else {
							$obj = implode(':', $where);
						}
						$file = !empty($b[$i]['file']) ? $b[$i]['file'] : 'UNKNOWN FILE';
						$line = !empty($b[$i]['line']) ? $b[$i]['line'] : 'UNKNOWN LINE';

						$msg = sprintf(
							"Time: %s Number: %d\n".
							"Backtrace: %s, File: %s, Line: %d\n".
							"=============================\n".
							"%s\n".
							"=====================================\n",
							$_elapsed, $this->_query_count,
							print_r($obj, true), $file, $line,
							$sql
						);
						error_log($msg, 3, TMP_DIR.'/DB.query.log');
				}
			}
		}
		// geef resultaat object terug
		return $result;
	}

	/**
	 * Set a collation
	 *
	 * @param String collation
	 * @param Integer connection number
	 * @return bool
	 */
	public function setCollation($collation)
	{
		switch (strtolower($collation)) {
			case 'utf-8':
			case 'utf8':
				$collation = 'utf8';
				break;

			case 'latin':
			case 'latin1':
			case 'iso-8859-1':
				$collation = 'latin1';
				break;

			case 'gb2312':
				$collation = 'gb2312';
				break;

			case 'gbk':
				$collation = 'gbk';
				break;

			case 'koi8-r':
				$collation = 'koi8r';
			break;

			default:
				throw new InvalidArgumentException('Unknown collation '.
					$collation);
		}

		$this->_collation = $collation;

		if(function_exists('mysql_set_charset')) {
			mysqli_set_charset($this->_resource, $collation);
		}
		else {
			mysqli_query($this->_resource, 'SET NAMES ' . $collation);
		}

		return $this;
	}

	/**
	 * The current active collation
	 *
	 * @return string|null null if there is no collation specifically set
	 */
	public function getCollation()
	{
		return $this->_collation;
	}

	/**
	 * Total time used for queries
	 *
	 * @return float
	 */
	public function getTotalQueryTime()
	{
		return $this->_time_spend;
	}

	/**
	 * Time it took to execute the last runned query
	 *
	 * @return float
	 */
	public function getQueryTime()
	{
		return $this->_time_spend_last;
	}

	/**
	 * Number of queries executed on this run
	 *
	 * @return int
	 */
	public function getQueryCount()
	{
		return $this->_query_count;
	}

	/**
	 * Get this connection as a PDO instantion
	 *
	 * @return PDO
	 */
	public function asPDO()
	{
		$c = $this->_connection_details;
		if(!$c) {
			throw new DB_Exception("No open connection");
		}
		$options = array(
			// Fixes PDO to allow SHOW TABLES queries
			PDO::ATTR_EMULATE_PREPARES => true,
			PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		);

		if($col = $this->getCollation()) {
			$options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES {$col}";
		}

		return new PDO("mysql".
			":dbname=".$c['db'].
			";host=".$c['host'].
			";port=".$c['port'],
			$c['user'],
			$c['pass'],
			$options
		);
	}

	/**
	 * Escape a SQL statement
	 *
	 * @param string $str Text to escape
	 * @param bool $quote Also quote the value
	 * @return string
	 */
	public function quote($str, $quote = false)
	{
		if(is_array($str)) {
			throw new InvalidArgumentException('Can not quote an Array');
		}

		if (!$this->_resource) {
			throw new DB_Exception('No open database connection');
		}

		if ($str) {
			$str = mysqli_real_escape_string($this->_resource, $str);
		}

		return $quote ? "'".$str."'" : $str;
	}

	/**
	 * Improved quotes
	 *
	 * @param mixedvar $value
	 * @return string|array
	 */
	public function quoteValue($value)
	{
		if (!$this->_resource) {
			throw new DB_Exception('No database connection');
		}

		if(is_object($value)) {
			if($value instanceof DB_Expression) {
				return $value->__toString();
			}

			if($value instanceof Traversable) {
				$value = iterator_to_array($value);
			}
			else if(method_exists($value, '__toString')) {
				$value = $value->__toString();
			}
			else {
				throw new DB_Exception('Trying to quote a Object which is not '.
					'a Traversable nor has a __toString() method');
			}
		}

		if(is_null($value)) {
			return 'NULL';
		}

		if(is_int($value)) {
			return $value;
		}

		if(is_bool($value)) {
			return (int) $value;
		}

		if(is_float($value)) {
			return number_format($value, null, '.', '');
		}

		if(is_string($value)) {
			return "'".mysqli_real_escape_string($this->_resource, $value)."'";
		}

		if(is_array($value)) {
			return array_map(array(__CLASS__, __METHOD__), $value);
		}

		throw new InvalidArgumentException("Unable to quote type ".gettype($value));
	}

	/**
	 * Create Insert Into statement
	 *
	 * @param array $rows Multi deminsional array
	 * @param string $table
	 * @return array Insert id's
	 */
	public function insertRows(array $rows, $table)
	{
		if(!empty($rows) && (is_array(current($rows)) ||
			is_object(current($rows)))) {

			$ids = array();

			foreach($rows as $row) {
				if(is_object($row)) {
					$row = get_object_vars($row);
				}

				$fields = array();
				$row_values = array();
				foreach($row as $field => $value) {
					if(!is_array($value)) {
						$fields[] = $this->quote($field);
						$row_values[] = $this->quoteValue($value);
					}
				}

				$row_values = "(".implode(", ", $row_values).")";

				$first_run = false;

				$res = $this->iQuery(sprintf('
					INSERT INTO
					%s (%s)
					VALUES
					%s;',
					$this->quote($table),
					"`".implode("`, `", $fields)."`",
					$row_values
				));

				$ids[] = $res->lastInsertId;
			}
			return $ids;
		}

		return array();
	}

	/**
	 * Updates the given rows
	 *
	 * @param array $rows Multi deminsional array
	 * @param string $table
	 * @param array $match_colums Columns to match on
	 * @return bool
	 */
	public function updateRows(array $rows, $table, array $match_colums = array('id'))
	{
		if(empty($match_colums)) {
			throw new InvalidArgumentException('No colums given to match on');
		}
		if(!empty($rows) && (is_array(current($rows)) ||
			is_object(current($rows)))) {

			$sqls = array();
			// Prepare the queries
			foreach($rows as $row) {
				if(is_object($row)) {
					$row = get_object_vars($row);
				}

				$row_values = array();
				foreach($row as $field => $value) {
					if(!is_array($value)) {
						$row_values[] = '`'.$this->quote($field).'` = '.
							$this->quoteValue($value);
					}
				}

				$match = array();

				foreach ($match_colums as $colum) {
					if(!array_key_exists($colum, $row)) {
						throw new InvalidArgumentException('Matching colum could not be found: '.$colum);
					}

					$match[] = '`'.$this->quote($colum).'` = '.$this->quoteValue($row[$colum]);
				}
				$where = empty($match) ? '' : ' WHERE '.implode(' AND ', $match);

				$sqls[] = sprintf('
					UPDATE `%s`
					SET
					%s
					%s
					LIMIT 1;
					',
					$this->quote($table),
					implode(', ', $row_values),
					$where
				);
			}

			// Run the queries
			foreach($sqls as $s) { $this->iQuery($s); }
			return true;
		}
		return false;
	}

	/**
	 * Gets a named lock
	 * @see http://dev.mysql.com/doc/refman/5.0/en/miscellaneous-functions.html#function_get-lock
	 * @param string $name Name of the lock
	 * @param int $timeout Timeout in seconds
	 * @return bool If the lock was succesfully obtained
	 */
	public function getLock($name, $timeout = 10)
	{
		$res = $this->iQuery(sprintf("SELECT get_lock(%s, %d) AS result",
			$this->quoteValue($name), $timeout
		));
		$row = $res->first();

		if(!$row || null === $row->result) {
			throw new DB_Exception('Could not get a lock due to thread being killed');
		}
		return 1 == $row->result;
	}

	/**
	 * Releases a named lock
	 * @see http://dev.mysql.com/doc/refman/5.0/en/miscellaneous-functions.html#function_release-lock
	 * @param string $name Name of the lock
	 * @return bool Always true
	 */
	public function releaseLock($name)
	{
		$this->iQuery(sprintf("DO release_lock(%s)", $this->quoteValue($name)));
		return true;
	}

	/**
	 * Check if a lock is set and by whom
	 * @param string $name
	 * @return bool|int The connection identifier that has the lock
	 */
	public function isLocked($name)
	{
		$res = $this->iQuery(sprintf("SELECT IS_USED_LOCK(%s) AS connection_id",
			$this->quoteValue($name)));

		$row = $res->first();

		return !$row || null === $row->connection_id ?
			false : $row->connection_id;
	}

	/**
	 * Checks if a table exists in the database
	 *
	 * @return bool
	 */
	public function tableExists($name)
	{
		$res = $this->iQuery(sprintf("SHOW TABLES LIKE '%s'", $name));
		return 1 === count($res);
	}

	/**
	 * Checks if a column exists for a given table
	 *
	 * @param string $table
	 * @param string $column
	 * @return bool
	 */
	public function columnExists($table, $column)
	{
		if($this->tableExists($table)) {
			$res = $this->iQuery(sprintf("SHOW COLUMNS FROM %s LIKE '%s'",
				$table, $column));
			return 1 === count($res);
		}
		return false;
	}

	/**
	 * Returns the database name of the currently selected database
	 *
	 * @return string|void
	 */
	public function getDbName()
	{
		return isset($this->_connection_details['db']) ?
			$this->_connection_details['db'] : null;
	}

	/**
	 * Returns the details for this connection
	 *
	 * @return array
	 */
	public function getConnectionDetails()
	{
		return $this->_connection_details;
	}

	/**
	 * Get the link to the database server
	 *
	 * @return resource
	 */
	public function getResource()
	{
		return $this->_resource;
	}
}
