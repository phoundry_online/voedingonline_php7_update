<?php
class ClipEventsContentHandler extends ContentHandler
{
	
	protected $action;
	
	public function __construct($path, $query)
	{
		parent::__construct($path, $query);
		try{
			$this->action = $this->path[0];
			if(empty($this->action)) $this->action = 'js_files';
			
		}
		catch(Exception $e){
			die($e);
		}
	}
	
	/**
	 * Run the chosen action
	 */
	public function init()
	{
		try{
			$rfl = new ReflectionMethod($this, $this->action.'Action');
			$args = $this->path;
			array_shift($args);
			if ($rfl->isPublic()) $rfl->invokeArgs($this, $args);
		}catch (Exception $e){
			die($e->getMessage());
		}
		exit(0);
	}
	
	
	public function clipAction($clip_id, $container_id)
	{
		$videoAr = ActiveRecord::factory('ClipEvents_Video', $clip_id);
		if(!$videoAr->isLoaded()) {
			die();
		}
		
		// Compile the actions
		$actions = array();
		foreach($videoAr->actions as $action) {
			$actions[] = array(
				'id'	=> $action['id'],
				'name'	=> $action->action_type['class_name']."_".$action['id'],
				'code'	=> $action->getJavascript(),
				'data'	=> $action->getData(),
				'start'	=> $action->time['start'],
				'end'	=> $action->time['end']
			);
		}
		
		// Chop actions into groups
		$groups = array();
		foreach ($actions as $action_index => $action) {
			$group_keys = range(intval($action['start']/10), intval($action['end']/10));
			foreach($group_keys as $group) {
				if(!isset($groups[$group]) || !is_array($groups[$group])) $groups[$group] = array();
				$groups[$group][] = $action_index;
			}
		}
			
		$video = array(
			'id' => $videoAr['id'],
			'name' => $videoAr['name'],
			'file' => (null === $videoAr['url']) ? 'http://'.$_SERVER['HTTP_HOST'].'/uploaded'.$videoAr['file'] : $videoAr['url'],
			'width' => $videoAr['width'],
			'height' => $videoAr['height'],
			'actions' => $actions,
			'groups' => $groups,
			'captions' => (count($videoAr->captions) != 0 ? 'http://'.$_SERVER['HTTP_HOST'].'/index.php/clipEvents/captions/'.$videoAr['id'] : null)
		);
			
		ob_start();
		?>
		(function($){
			var video = (<?=json_encode($video)?>);
			if(!$.ClipEvents.data.ClipVideos) {
				$.ClipEvents.data.ClipVideos = [];
			}
			$.ClipEvents.data.ClipVideos[video.id] = video;
		})(jQuery);
		
		jQuery(function($){
			var container = $('#<?=$container_id?>');
		
			if(container.length == 1) {
				$.ClipEvents.LoadedVideoId['<?=$container_id?>'] = '<?=$video['id']?>';
			
				swfobject.embedSWF(
					"http://<?=$_SERVER['HTTP_HOST']?>/brickwork/flash/jwplayer/player.swf",
					"<?=$container_id?>",
					container.width(),
					container.height(),
					"8.0.0",
					"swfobject/expressInstall.swf",
					{
						width : container.width(),
						height: container.height(),
						javascriptid: '<?=$container_id?>',
						enablejs: 'true',
						//file:	'<?=$video['file']?>',
						/*skin : 'http://<?=$_SERVER['HTTP_HOST']?>/clipevents/player/nacht.swf',*/
						autostart : 'true',
						stretching : 'exactfit',
						repeat : 'list',
						controlbar : 'bottom'
					},
					{
						allowfullscreen: 'true',
						allowscriptaccess: 'always',
						wmode: 'transparent'
					},
					{
					  id: "<?=$container_id?>",
					  name: "<?=$container_id?>"
					}
				);
				
			}
			
		});
		
		<?php
		echo ob_get_clean(); exit;
		
		$packer = new JavaScriptPacker(ob_get_clean(), 'Normal');
		echo $packer->pack(); exit;
	}
	
	public function captionsAction($clip_id)
	{
		$captions = ActiveRecord::factory('ClipEvents_Caption')->where('video_id', $clip_id)->get();
		header('Content-type: text/xml');
		?>
		<tt xml:lang="en" xmlns="http://www.w3.org/2006/10/ttaf1" xmlns:tts="http://www.w3.org/2006/10/ttaf1#style">
			<head>
				<layout />
			</head>
			<body>
				<div xml:id="captions">
		<?foreach($captions as $caption){
			printf(
			"\t\t\t".'<p begin="%s" end="%s">%s</p>'."\n",
			
			$caption->getReadableTime('start'),
			$caption->getReadableTime('end'),
			$caption['text']
			);
		}?>
				</div>
			</body>
		</tt>
		<?php
	}
}