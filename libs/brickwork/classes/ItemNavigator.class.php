<?php
if(defined('TESTMODE') && TESTMODE && isset($WEBprefs['allow_deprecated']) && $WEBprefs['allow_deprecated'] === false) {
	trigger_error("ItemNavigator is deprecated in favor of Pagination", E_USER_ERROR);
}

/**
* ItemNavigator
*
* Bladerding om door lijsten heen te lopen. Selecteerd bij de constructie
* het actieve element (current) en vanuitdaar kan verder en terug worden gelopen
* door de lijst.
*
* Handig om bijvoorbeeld een navigeerbalk met vorige en volgende te maken. Laad 
* daarvoor alle ids als eerste parameter en zet de huidige pagina id als tweede
* parameter.
*
* Voor voorbeeld zie onderaan deze class
*
* @author Mick van der Most van Spijk <mick@webpower.nl>
* @author Remco Bos <remco@webpower.nl>
* @copyright &copy; 2006 Web Power
* @package None
* @version 0.1.0
*/
class ItemNavigator {
	/**
	* predefined constants
	*/
	const continuous    = 'item_navigator_continuous';
	const discontinuous = 'item_navigator_discontinuous';

	/**
	* Setting for continuous looping through items. When continuous and
	* the last item is encountered, the iterator resets itself en returns the first
	* item
	*
	* @access public
	* @static
	* @var const
	*/
	public static $continuous = ItemNavigator::discontinuous;

	/**
	* Container for ids
	* @var Array 
	* @access private
	*/
   private $_items = array();

	/**
	* Selected value
	* @var mixed
	* @access private
	*/
	private $_selected_item = FALSE;

	/**
	* Selected key
	* @var mixed
	* @access private
	*/
	private $_selected_key;

	/**
	* Maak een nieuwe ItemNavigator aan
	*
	* @param Array
	* @param mixed
	* @access public
	*/
   public function __construct($array, $selected=FALSE) {
		// chk type
		if (!is_array($array)) {
			trigger_error('Supplied list is not an array', E_USER_ERROR);
		}

		// assign
		$this->_items = $array;

		// set selected
		$this->_selected_item = $selected;

		// move internal pointer to selected item
		if (FALSE !== $this->_selected_item) {
			$this->set_current($this->_selected_item);
		}
   }

	/**
	* move internal pointer to selected item
	* 
	* @param mixed
	* @access public
	* @return void
	*/
	public function set_current($item) {
		while (list($key) = each($this->_items)) {
			// item found, breaking out, thus saving current position
			if ($this->_items[$key] == $item) {
				// found
				$this->_selected_key = $key;
				break;
			}
		}

		// not found, select first item
		//unset($this->_selected_key);
		reset($this->_items);
	}

	/**
	* return current value
	* 
	* @access public
	* @return mixed
	*/
   public function current() {
		return ($this->_items[$this->_selected_key]);
   }

	/**
	* return next value
	*
	* @access public
	* @return mixed
	*/
   public function next() {
		// return next item
		if(isset($this->_selected_key)) {
			foreach ($this->_items as $key => $item) {
				// move iternal pointer
				if ($key == $this->_selected_key) {
					break;
				}
			}
		}

		$item = current($this->_items); 

		// if continuous return first item
		if (self::$continuous == ItemNavigator::continuous && FALSE === $item) {
			$item = reset($this->_items);
		}

		// return the item
		return $item;
   }

	/**
	* return prevoius value
	* 
	* @access public
	* @return mixed
	*/
	public function prev() {
		// reverse list
		$items = array_reverse($this->_items, TRUE);

		// return previous item
		foreach ($items as $key => $item) {
			// move internal pointer
			if ($key == $this->_selected_key) {
				break;
			}
		}

		// fetch item
		$item = current($items);

		// if continuous return first item
		if (self::$continuous == ItemNavigator::continuous && FALSE === $item) {
			$item = reset($items);
		}

		// return item
		return $item;
   }
}

/**
Voorbeeld werking:

$lijst = array(12,23,3,5);

$nav = new ItemNavigator($lijst, 3);

print_r($nav->prev());
print_r($nav->current());
print_r($nav->next());
*/
?>
