<?php
/**
 * Generic session class
 * @author Christiaan Baartse <christiaan.baartse@webpower.nl>
 * @version 2.0
 */
class Session
{
	/**
	 * @var Session_Handler
	 */
	protected $_handler;

	/**
	 * Array where all session variables will be stored in
	 *
	 * @var Array
	 */
	protected $_attribute;

	/**
	 * Flag to indicate if a session was started or not
	 *
	 * @var boolean
	 */
	protected $_session_open;

	/**
	 * @var string The suffix to add to the session name
	 */
	protected $_site_identifier;

	/**
	 * @var array The options for the session
	 */
	protected $_options;

	/**
	 * @var bool If the handlers are succesfully registered
	 */
	protected $_handler_set;

	/**
	 * Initialises the session
	 *
	 * @param string $site_identifier
	 * @param Session_Handler $handler
	 */
	public function __construct($site_identifier = null, Session_Handler $handler = null)
	{
		// File sessions are the default, so we dont need to register any handlers

		if(null !== $handler) {
		    $this->_handler = $handler;
			$this->_handler_set = session_set_save_handler(
				array($this->_handler, 'sessionOpen'),
				array($this->_handler, 'sessionClose'),
				array($this->_handler, 'sessionRead'),
				array($this->_handler, 'sessionWrite'),
				array($this->_handler, 'sessionDestroy'),
				array($this->_handler, 'sessionGarbageCollect')
			);
        }

		$this->_site_identifier = (null !== $site_identifier) ?
			(string) $site_identifier :
			Framework::$site->identifier;

		$this->_attribute = null;
		$this->_session_open	= false;

		session_name('SESS_'.str_replace('.', '_', $this->_site_identifier));
	}

	/**
	 * Tells if a session handler is set
	 * @return bool
	 */
	public function handlerSet()
	{
		return (bool) $this->_handler_set;
	}

	public function handler()
	{
		return $this->_handler;
	}

	/**
	 * Close the session
	 * @return void;
	 */
	public function __destruct()
	{
		return $this->close();
	}

	/**
	 * Starts the session
	 * @return bool
	 */
	public function start()
	{
		if(!$this->_session_open) {
			if(!session_start()) {
				throw new Exception("Session could not be started using session_start()");
			}
			$this->_session_open = true;
			$this->_attribute = &$_SESSION;
		}
		return $this;
	}

	/**
	 * Destroys the session
	 * @return bool
	 */
	public function destroy()
	{
		if($this->_session_open) {
			return session_destroy();
		}
		return false;
	}

	/**
	 * Close the session
	 * @return void
	 */
	public function close()
	{
		if($this->_session_open) {
			session_write_close();
			$this->_session_open = false;
			$this->_attribute = null;
		}
	}

	/**
	 * Tells if the session has started
	 * @return bool
	 */
	public function started()
	{
		return (bool) $this->_session_open;
	}

	/**
	 * Get value from the session starting the session if it hasn't already
	 * @param string $name
	 * @throws AttributeNotFoundException
	 * @return mixedvar
	 */
	public function __get($name)
	{
		if(!$this->started()) {
			$this->start();
		}

		if(!array_key_exists($name, $this->_attribute)) {
			throw new AttributeNotFoundException('Attribute '.$name.' not set');
		}
		return $this->_attribute[$name];
	}

	/**
	 * Set a value in the session starting the session aswell if it hasn't yet
	 * @param string $name
	 * @param mixedvar $value
	 * @throws AttributeNotFoundException
	 * @return void
	 */
	public function __set($name, $value)
	{
		if(!$this->started()) {
			$this->start();
		}
		$this->_attribute[$name] = $value;
	}

	/**
	 * Checks if a named key is set in the session
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		if(!$this->started()) {
			$this->start();
		}
		return isset($this->_attribute[$name]);
	}

	/**
	 * Unsets a named key in the session
	 * @param string $name
	 * @return void
	 */
	public function __unset($name)
	{
		if(!$this->started()) {
			$this->start();
		}
		unset($this->_attribute[$name]);
	}
}
